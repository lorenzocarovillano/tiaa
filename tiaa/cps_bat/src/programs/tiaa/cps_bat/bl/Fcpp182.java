/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:24 PM
**        * FROM NATURAL PROGRAM : Fcpp182
************************************************************
**        * FILE NAME            : Fcpp182.java
**        * CLASS NAME           : Fcpp182
**        * INSTANCE NAME        : Fcpp182
************************************************************
************************************************************************
* PROGRAM  : FCPP182 (NEW VERSION)
* SYSTEM   : CPS
* TITLE    : NEW ANNUITIZATION PAYMENT REGISTER REPORT
* FUNCTION : THIS PROGRAM WILL GENERATE A NEW ANNUITIZATION REPORT
*            BASED ON THE INPUT FILE CREATED BY FCPP182A. THE PROGRAM
*            USES THE SORT-KEY, WHICH WAS POPULATED BY
*            FCPP182A, TO CLASSIFY THE REPORTS.
*
* CREATED  : MARCH 21, 2001
* AUTHOR   : M. MURILLO
*
* 03/06/2006 : LANDRUM  PAYEE MATCH
*              - INCREASED CHECK NBR TO N10.
************************************************************************
* HISTORY
* 04/18/01 M.MURILLO BREAKOUT TPA,IPRO AND P&I FOR INTERNAL ROLLOVERS
* 06/05/01 M.MURILLO RESET RTB AND IVC-FROM-RTB-ROLLOVER IND
* 08/29/01 R.CARREON EXPND SORT-OPTION-CDE FROM A6 TO A7
* 11/16/01 A.CHESLER FIXED RTB AND IVC-FROM-RTB-ROLLOVER TOTALS.
* 11/16/01 A.CHESLER ADDED TOTALS FOR DTRA'S.
* 02/02/02 R.CARREON CHANGED COMPUTE STATEMENT FOR DTRA ACCUMULATION
* 02/27/02 R.CARREON EXCLUDE DTRA CANCELLED, STOPPED OR REDRAWN CHECKS
*                    IN DTRA TOTALS
* 4/2017    : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp182 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private PdaFcpaext2 pdaFcpaext2;
    private PdaFcpa199a pdaFcpa199a;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_N10__R_Field_1;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N7;

    private DbsGroup pnd_Check_Number_N10__R_Field_2;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_A10;
    private DbsField pnd_Check_Datex;

    private DbsGroup pnd_Check_Datex__R_Field_3;
    private DbsField pnd_Check_Datex_Pnd_Check_Date;
    private DbsField pnd_End_Of_Report_Lit;
    private DbsField pnd_A2_Low_Values;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Input_Parm;
    private DbsField pnd_Input_Parm2;
    private DbsField pnd_Header1;
    private DbsField pnd_Header2;
    private DbsField pnd_Header4;
    private DbsField pnd_W_Fld2;
    private DbsField pnd_W_Fld;
    private DbsField pnd_J2;
    private DbsField pnd_I;
    private DbsField pnd_I1;
    private DbsField pnd_Len;
    private DbsField pnd_T1;
    private DbsField pnd_T2;
    private DbsField pnd_Ln;
    private DbsField pnd_Ws_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Sort_Option_Cde;

    private DbsGroup pnd_Ws_Sort_Option_Cde__R_Field_4;
    private DbsField pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_Pref;
    private DbsField pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X;
    private DbsField pnd_Hdr_Sort_Option_Cde;
    private DbsField pnd_Hdr_Sort_Orgn_Cde;
    private DbsField pnd_Prev_Seq_Nbr;
    private DbsField pnd_Sub_Txt;
    private DbsField pnd_Sub_Txt_A;
    private DbsField pnd_Sub_Txt_M;
    private DbsField pnd_Dte_Txt;
    private DbsField pnd_Hdr_Dte;
    private DbsField pnd_Curr_Dte;
    private DbsField pnd_Old_Pymnt_Check_Dte;
    private DbsField pnd_Cf_Txt;
    private DbsField pnd_Guar_Unit_Txt;
    private DbsField pnd_Div_Unit_Txt;
    private DbsField pnd_Month;
    private DbsField pnd_Year;
    private DbsField pnd_Cntrct_Type_Cnt;
    private DbsField pnd_Dte_First_Periodic;
    private DbsField pnd_Old_Sort_Key;

    private DbsGroup pnd_Old_Sort_Key__R_Field_5;
    private DbsField pnd_Old_Sort_Key_Pnd_Old_Cntrct_Orgn_Cde;
    private DbsField pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde;

    private DbsGroup pnd_Old_Sort_Key__R_Field_6;
    private DbsField pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_Pref;
    private DbsField pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X;
    private DbsField pnd_Old_Sort_Key_Pnd_Old_Other;
    private DbsField pnd_Prev_Hdr_Title1;
    private DbsField pnd_Prev_Hdr_Title2;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_Prcss_Seq_Nbr__R_Field_7;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr;
    private DbsField pnd_Summary;
    private DbsField pnd_First;
    private DbsField pnd_Month_End;
    private DbsField pnd_Dvdnd;
    private DbsField pnd_Init;
    private DbsField pnd_Sub_Prt;
    private DbsField pnd_Cancelfslash_Stop;
    private DbsField pnd_Int_Rollover;
    private DbsField pnd_Reg_Pymnt_Printed;
    private DbsField pnd_Int_Rollover_Printed;
    private DbsField pnd_End_Of_Contrct;
    private DbsField pnd_Reset_Page;
    private DbsField rtb;
    private DbsField ivc_From_Rtb_Rollover;
    private DbsField pnd_Use_Prev_Title;
    private DbsField pnd_Use_Prev_Sort_Key_Val;
    private DbsField pnd_Dtl_Recs_Found;

    private DbsGroup pnd_Detail;
    private DbsField pnd_Detail_Pnd_Rtb_Ind;
    private DbsField pnd_Detail_Pnd_Fund_Desc;
    private DbsField pnd_Detail_Pnd_Inv_Acct_Valuat_Period;
    private DbsField pnd_Detail_Pnd_Gross_Amt;
    private DbsField pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Detail_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Detail_Pnd_Rtb_Pymnt_Amt;
    private DbsField pnd_Detail_Pnd_Pymnt_Settlmnt_Dte;

    private DbsGroup pnd_Item;
    private DbsField pnd_Item_Pnd_Gross_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Item_Pnd_Rtb_Pymnt_Amt;

    private DbsGroup pnd_Misc;
    private DbsField pnd_Misc_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Misc_Pnd_Annt;
    private DbsField pnd_Misc_Pnd_Payee;
    private DbsField pnd_Misc_Pnd_Cntrct_Nbr;
    private DbsField pnd_Misc_Cntrct_Hold_Cde;
    private DbsField pnd_Misc_Pymnt_Check_Dte;
    private DbsField pnd_Misc_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Misc_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Misc_Pymnt_Check_Nbr;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Misc_Pnd_Cge_Txt;
    private DbsField pnd_Misc_Pnd_Dtra_Net_Payment;

    private DbsGroup pnd_Dte;
    private DbsField pnd_Dte_Pnd_Gross_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Dte_Pnd_Rtb_Pymnt_Amt;
    private DbsField pnd_Dte_Pnd_Qty;

    private DbsGroup pnd_Sub;
    private DbsField pnd_Sub_Pnd_Gross_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Sub_Pnd_Rtb_Pymnt_Amt;
    private DbsField pnd_Sub_Pnd_Qty;

    private DbsGroup pnd_G_Tot;
    private DbsField pnd_G_Tot_Pnd_Current_A;
    private DbsField pnd_G_Tot_Pnd_Current_M;
    private DbsField pnd_G_Tot_Pnd_Future_A;
    private DbsField pnd_G_Tot_Pnd_Future_M;
    private DbsField pnd_G_Tot_Pnd_C_Qty;
    private DbsField pnd_G_Tot_Pnd_F_Qty;

    private DbsGroup pnd_Sum;
    private DbsField pnd_Sum_Pnd_Current_A;
    private DbsField pnd_Sum_Pnd_Current_M;
    private DbsField pnd_Sum_Pnd_Future_A;
    private DbsField pnd_Sum_Pnd_Future_M;
    private DbsField pnd_Sum_Pnd_C_Qty;
    private DbsField pnd_Sum_Pnd_F_Qty;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_T_Total_A;
    private DbsField pnd_Totals_Pnd_T_Total_M;
    private DbsField pnd_Totals_Pnd_C_Total;
    private DbsField pnd_Totals_Pnd_F_Total;
    private DbsField pnd_Totals_Pnd_T_Total;
    private DbsField pnd_Totals_Pnd_Fdtra_Total;
    private DbsField pnd_Totals_Pnd_Cdtra_Total;
    private DbsField pnd_Totals_Pnd_Tdtra_Total;
    private DbsField pnd_Totals_Pnd_Fin_Dtra_Tc;
    private DbsField pnd_Totals_Pnd_Fin_Dtra_Tf;
    private DbsField pnd_Totals_Pnd_Sum_Dtra_Tc;
    private DbsField pnd_Totals_Pnd_Sum_Dtra_Tf;
    private DbsField pnd_Totals_Pnd_End_Dtrac;
    private DbsField pnd_Totals_Pnd_End_Dtraf;
    private DbsField pnd_Totals_Pnd_Diff_Dtra;
    private DbsField pnd_Totals_Pnd_Cqty;
    private DbsField pnd_Totals_Pnd_Fqty;
    private DbsField pnd_Totals_Pnd_Tqty;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Sort_KeyOld;
    private DbsField readWork01Pymnt_Check_DteOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        localVariables = new DbsRecord();
        pdaFcpaext2 = new PdaFcpaext2(localVariables);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Check_Number_N10 = localVariables.newFieldInRecord("pnd_Check_Number_N10", "#CHECK-NUMBER-N10", FieldType.NUMERIC, 10);

        pnd_Check_Number_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_1", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_N3 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_N10_Pnd_Check_Number_N7 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        pnd_Check_Number_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_2", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_A10 = pnd_Check_Number_N10__R_Field_2.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_A10", "#CHECK-NUMBER-A10", 
            FieldType.STRING, 10);
        pnd_Check_Datex = localVariables.newFieldInRecord("pnd_Check_Datex", "#CHECK-DATEX", FieldType.STRING, 8);

        pnd_Check_Datex__R_Field_3 = localVariables.newGroupInRecord("pnd_Check_Datex__R_Field_3", "REDEFINE", pnd_Check_Datex);
        pnd_Check_Datex_Pnd_Check_Date = pnd_Check_Datex__R_Field_3.newFieldInGroup("pnd_Check_Datex_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_End_Of_Report_Lit = localVariables.newFieldInRecord("pnd_End_Of_Report_Lit", "#END-OF-REPORT-LIT", FieldType.STRING, 25);
        pnd_A2_Low_Values = localVariables.newFieldInRecord("pnd_A2_Low_Values", "#A2-LOW-VALUES", FieldType.STRING, 2);
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 22);
        pnd_Input_Parm2 = localVariables.newFieldInRecord("pnd_Input_Parm2", "#INPUT-PARM2", FieldType.STRING, 55);
        pnd_Header1 = localVariables.newFieldInRecord("pnd_Header1", "#HEADER1", FieldType.STRING, 60);
        pnd_Header2 = localVariables.newFieldInRecord("pnd_Header2", "#HEADER2", FieldType.STRING, 84);
        pnd_Header4 = localVariables.newFieldInRecord("pnd_Header4", "#HEADER4", FieldType.STRING, 60);
        pnd_W_Fld2 = localVariables.newFieldInRecord("pnd_W_Fld2", "#W-FLD2", FieldType.STRING, 42);
        pnd_W_Fld = localVariables.newFieldInRecord("pnd_W_Fld", "#W-FLD", FieldType.STRING, 60);
        pnd_J2 = localVariables.newFieldInRecord("pnd_J2", "#J2", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.INTEGER, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.INTEGER, 2);
        pnd_T1 = localVariables.newFieldInRecord("pnd_T1", "#T1", FieldType.INTEGER, 2);
        pnd_T2 = localVariables.newFieldInRecord("pnd_T2", "#T2", FieldType.INTEGER, 2);
        pnd_Ln = localVariables.newFieldInRecord("pnd_Ln", "#LN", FieldType.INTEGER, 2);
        pnd_Ws_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Ws_Cntrct_Orgn_Cde", "#WS-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Sort_Option_Cde = localVariables.newFieldInRecord("pnd_Ws_Sort_Option_Cde", "#WS-SORT-OPTION-CDE", FieldType.STRING, 7);

        pnd_Ws_Sort_Option_Cde__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Sort_Option_Cde__R_Field_4", "REDEFINE", pnd_Ws_Sort_Option_Cde);
        pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_Pref = pnd_Ws_Sort_Option_Cde__R_Field_4.newFieldInGroup("pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_Pref", 
            "#WS-SORT-OPTION-CDE-PREF", FieldType.STRING, 1);
        pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X = pnd_Ws_Sort_Option_Cde__R_Field_4.newFieldInGroup("pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X", 
            "#WS-SORT-OPTION-CDE-X", FieldType.STRING, 6);
        pnd_Hdr_Sort_Option_Cde = localVariables.newFieldInRecord("pnd_Hdr_Sort_Option_Cde", "#HDR-SORT-OPTION-CDE", FieldType.STRING, 7);
        pnd_Hdr_Sort_Orgn_Cde = localVariables.newFieldInRecord("pnd_Hdr_Sort_Orgn_Cde", "#HDR-SORT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Prev_Seq_Nbr = localVariables.newFieldInRecord("pnd_Prev_Seq_Nbr", "#PREV-SEQ-NBR", FieldType.NUMERIC, 7);
        pnd_Sub_Txt = localVariables.newFieldInRecord("pnd_Sub_Txt", "#SUB-TXT", FieldType.STRING, 60);
        pnd_Sub_Txt_A = localVariables.newFieldInRecord("pnd_Sub_Txt_A", "#SUB-TXT-A", FieldType.STRING, 60);
        pnd_Sub_Txt_M = localVariables.newFieldInRecord("pnd_Sub_Txt_M", "#SUB-TXT-M", FieldType.STRING, 60);
        pnd_Dte_Txt = localVariables.newFieldInRecord("pnd_Dte_Txt", "#DTE-TXT", FieldType.STRING, 24);
        pnd_Hdr_Dte = localVariables.newFieldInRecord("pnd_Hdr_Dte", "#HDR-DTE", FieldType.STRING, 10);
        pnd_Curr_Dte = localVariables.newFieldInRecord("pnd_Curr_Dte", "#CURR-DTE", FieldType.DATE);
        pnd_Old_Pymnt_Check_Dte = localVariables.newFieldInRecord("pnd_Old_Pymnt_Check_Dte", "#OLD-PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Cf_Txt = localVariables.newFieldInRecord("pnd_Cf_Txt", "#CF-TXT", FieldType.STRING, 18);
        pnd_Guar_Unit_Txt = localVariables.newFieldInRecord("pnd_Guar_Unit_Txt", "#GUAR-UNIT-TXT", FieldType.STRING, 15);
        pnd_Div_Unit_Txt = localVariables.newFieldInRecord("pnd_Div_Unit_Txt", "#DIV-UNIT-TXT", FieldType.STRING, 11);
        pnd_Month = localVariables.newFieldInRecord("pnd_Month", "#MONTH", FieldType.STRING, 9);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);
        pnd_Cntrct_Type_Cnt = localVariables.newFieldInRecord("pnd_Cntrct_Type_Cnt", "#CNTRCT-TYPE-CNT", FieldType.NUMERIC, 1);
        pnd_Dte_First_Periodic = localVariables.newFieldInRecord("pnd_Dte_First_Periodic", "#DTE-FIRST-PERIODIC", FieldType.DATE);
        pnd_Old_Sort_Key = localVariables.newFieldInRecord("pnd_Old_Sort_Key", "#OLD-SORT-KEY", FieldType.STRING, 10);

        pnd_Old_Sort_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Old_Sort_Key__R_Field_5", "REDEFINE", pnd_Old_Sort_Key);
        pnd_Old_Sort_Key_Pnd_Old_Cntrct_Orgn_Cde = pnd_Old_Sort_Key__R_Field_5.newFieldInGroup("pnd_Old_Sort_Key_Pnd_Old_Cntrct_Orgn_Cde", "#OLD-CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde = pnd_Old_Sort_Key__R_Field_5.newFieldInGroup("pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde", "#OLD-SORT-OPTION-CDE", 
            FieldType.STRING, 6);

        pnd_Old_Sort_Key__R_Field_6 = pnd_Old_Sort_Key__R_Field_5.newGroupInGroup("pnd_Old_Sort_Key__R_Field_6", "REDEFINE", pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde);
        pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_Pref = pnd_Old_Sort_Key__R_Field_6.newFieldInGroup("pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_Pref", "#OLD-SORT-OPTION-CDE-PREF", 
            FieldType.STRING, 1);
        pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X = pnd_Old_Sort_Key__R_Field_6.newFieldInGroup("pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X", "#OLD-SORT-OPTION-CDE-X", 
            FieldType.STRING, 5);
        pnd_Old_Sort_Key_Pnd_Old_Other = pnd_Old_Sort_Key__R_Field_5.newFieldInGroup("pnd_Old_Sort_Key_Pnd_Old_Other", "#OLD-OTHER", FieldType.STRING, 
            2);
        pnd_Prev_Hdr_Title1 = localVariables.newFieldInRecord("pnd_Prev_Hdr_Title1", "#PREV-HDR-TITLE1", FieldType.STRING, 100);
        pnd_Prev_Hdr_Title2 = localVariables.newFieldInRecord("pnd_Prev_Hdr_Title2", "#PREV-HDR-TITLE2", FieldType.STRING, 100);
        pnd_Pymnt_Prcss_Seq_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Prcss_Seq_Nbr", "#PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_Prcss_Seq_Nbr__R_Field_7 = localVariables.newGroupInRecord("pnd_Pymnt_Prcss_Seq_Nbr__R_Field_7", "REDEFINE", pnd_Pymnt_Prcss_Seq_Nbr);
        pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr = pnd_Pymnt_Prcss_Seq_Nbr__R_Field_7.newFieldInGroup("pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr", "#CURR-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Summary = localVariables.newFieldInRecord("pnd_Summary", "#SUMMARY", FieldType.BOOLEAN, 1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Month_End = localVariables.newFieldInRecord("pnd_Month_End", "#MONTH-END", FieldType.BOOLEAN, 1);
        pnd_Dvdnd = localVariables.newFieldInRecord("pnd_Dvdnd", "#DVDND", FieldType.BOOLEAN, 1);
        pnd_Init = localVariables.newFieldInRecord("pnd_Init", "#INIT", FieldType.BOOLEAN, 1);
        pnd_Sub_Prt = localVariables.newFieldInRecord("pnd_Sub_Prt", "#SUB-PRT", FieldType.BOOLEAN, 1);
        pnd_Cancelfslash_Stop = localVariables.newFieldInRecord("pnd_Cancelfslash_Stop", "#CANCEL/STOP", FieldType.BOOLEAN, 1);
        pnd_Int_Rollover = localVariables.newFieldInRecord("pnd_Int_Rollover", "#INT-ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Reg_Pymnt_Printed = localVariables.newFieldInRecord("pnd_Reg_Pymnt_Printed", "#REG-PYMNT-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Int_Rollover_Printed = localVariables.newFieldInRecord("pnd_Int_Rollover_Printed", "#INT-ROLLOVER-PRINTED", FieldType.BOOLEAN, 1);
        pnd_End_Of_Contrct = localVariables.newFieldInRecord("pnd_End_Of_Contrct", "#END-OF-CONTRCT", FieldType.BOOLEAN, 1);
        pnd_Reset_Page = localVariables.newFieldInRecord("pnd_Reset_Page", "#RESET-PAGE", FieldType.BOOLEAN, 1);
        rtb = localVariables.newFieldInRecord("rtb", "RTB", FieldType.BOOLEAN, 1);
        ivc_From_Rtb_Rollover = localVariables.newFieldInRecord("ivc_From_Rtb_Rollover", "IVC-FROM-RTB-ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Use_Prev_Title = localVariables.newFieldInRecord("pnd_Use_Prev_Title", "#USE-PREV-TITLE", FieldType.BOOLEAN, 1);
        pnd_Use_Prev_Sort_Key_Val = localVariables.newFieldInRecord("pnd_Use_Prev_Sort_Key_Val", "#USE-PREV-SORT-KEY-VAL", FieldType.BOOLEAN, 1);
        pnd_Dtl_Recs_Found = localVariables.newFieldInRecord("pnd_Dtl_Recs_Found", "#DTL-RECS-FOUND", FieldType.BOOLEAN, 1);

        pnd_Detail = localVariables.newGroupArrayInRecord("pnd_Detail", "#DETAIL", new DbsArrayController(1, 40));
        pnd_Detail_Pnd_Rtb_Ind = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Rtb_Ind", "#RTB-IND", FieldType.STRING, 5);
        pnd_Detail_Pnd_Fund_Desc = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Fund_Desc", "#FUND-DESC", FieldType.STRING, 8);
        pnd_Detail_Pnd_Inv_Acct_Valuat_Period = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Inv_Acct_Valuat_Period", "#INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 
            3);
        pnd_Detail_Pnd_Gross_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Detail_Pnd_Inv_Acct_Dpi_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Detail_Pnd_Rtb_Pymnt_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Rtb_Pymnt_Amt", "#RTB-PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Detail_Pnd_Pymnt_Settlmnt_Dte = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Pymnt_Settlmnt_Dte", "#PYMNT-SETTLMNT-DTE", FieldType.STRING, 5);

        pnd_Item = localVariables.newGroupInRecord("pnd_Item", "#ITEM");
        pnd_Item_Pnd_Gross_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Item_Pnd_Inv_Acct_State_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Item_Pnd_Inv_Acct_Dpi_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Item_Pnd_Rtb_Pymnt_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Rtb_Pymnt_Amt", "#RTB-PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Misc = localVariables.newGroupInRecord("pnd_Misc", "#MISC");
        pnd_Misc_Cntrct_Ppcn_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Misc_Pnd_Annt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Annt", "#ANNT", FieldType.STRING, 50);
        pnd_Misc_Pnd_Payee = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Payee", "#PAYEE", FieldType.STRING, 38);
        pnd_Misc_Pnd_Cntrct_Nbr = pnd_Misc.newFieldArrayInGroup("pnd_Misc_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1, 
            6));
        pnd_Misc_Cntrct_Hold_Cde = pnd_Misc.newFieldInGroup("pnd_Misc_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Misc_Pymnt_Check_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Misc_Pymnt_Settlmnt_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Misc_Pymnt_Check_Seq_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Misc_Pymnt_Check_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Misc_Pnd_Inv_Acct_Cntrct_Amt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Cntrct_Amt", "#INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Misc_Pnd_Inv_Acct_Unit_Qty = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Unit_Qty", "#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9, 
            3);
        pnd_Misc_Pnd_Inv_Acct_Unit_Value = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Unit_Value", "#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Misc_Pnd_Cge_Txt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Cge_Txt", "#CGE-TXT", FieldType.STRING, 18);
        pnd_Misc_Pnd_Dtra_Net_Payment = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Dtra_Net_Payment", "#DTRA-NET-PAYMENT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Dte = localVariables.newGroupInRecord("pnd_Dte", "#DTE");
        pnd_Dte_Pnd_Gross_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Dte_Pnd_Inv_Acct_Dpi_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Dte_Pnd_Rtb_Pymnt_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Rtb_Pymnt_Amt", "#RTB-PYMNT-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Dte_Pnd_Qty = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Qty", "#QTY", FieldType.PACKED_DECIMAL, 5);

        pnd_Sub = localVariables.newGroupInRecord("pnd_Sub", "#SUB");
        pnd_Sub_Pnd_Gross_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Sub_Pnd_Inv_Acct_Dpi_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Sub_Pnd_Rtb_Pymnt_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Rtb_Pymnt_Amt", "#RTB-PYMNT-AMT", FieldType.PACKED_DECIMAL, 10, 2);
        pnd_Sub_Pnd_Qty = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Qty", "#QTY", FieldType.PACKED_DECIMAL, 5);

        pnd_G_Tot = localVariables.newGroupArrayInRecord("pnd_G_Tot", "#G-TOT", new DbsArrayController(1, 5));
        pnd_G_Tot_Pnd_Current_A = pnd_G_Tot.newFieldArrayInGroup("pnd_G_Tot_Pnd_Current_A", "#CURRENT-A", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            10));
        pnd_G_Tot_Pnd_Current_M = pnd_G_Tot.newFieldArrayInGroup("pnd_G_Tot_Pnd_Current_M", "#CURRENT-M", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            10));
        pnd_G_Tot_Pnd_Future_A = pnd_G_Tot.newFieldArrayInGroup("pnd_G_Tot_Pnd_Future_A", "#FUTURE-A", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            10));
        pnd_G_Tot_Pnd_Future_M = pnd_G_Tot.newFieldArrayInGroup("pnd_G_Tot_Pnd_Future_M", "#FUTURE-M", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            10));
        pnd_G_Tot_Pnd_C_Qty = pnd_G_Tot.newFieldArrayInGroup("pnd_G_Tot_Pnd_C_Qty", "#C-QTY", FieldType.PACKED_DECIMAL, 5, new DbsArrayController(1, 10));
        pnd_G_Tot_Pnd_F_Qty = pnd_G_Tot.newFieldArrayInGroup("pnd_G_Tot_Pnd_F_Qty", "#F-QTY", FieldType.PACKED_DECIMAL, 5, new DbsArrayController(1, 10));

        pnd_Sum = localVariables.newGroupArrayInRecord("pnd_Sum", "#SUM", new DbsArrayController(1, 5));
        pnd_Sum_Pnd_Current_A = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_Current_A", "#CURRENT-A", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            10));
        pnd_Sum_Pnd_Current_M = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_Current_M", "#CURRENT-M", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            10));
        pnd_Sum_Pnd_Future_A = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_Future_A", "#FUTURE-A", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            10));
        pnd_Sum_Pnd_Future_M = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_Future_M", "#FUTURE-M", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            10));
        pnd_Sum_Pnd_C_Qty = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_C_Qty", "#C-QTY", FieldType.PACKED_DECIMAL, 5, new DbsArrayController(1, 10));
        pnd_Sum_Pnd_F_Qty = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_F_Qty", "#F-QTY", FieldType.PACKED_DECIMAL, 5, new DbsArrayController(1, 10));

        pnd_Totals = localVariables.newGroupInRecord("pnd_Totals", "#TOTALS");
        pnd_Totals_Pnd_T_Total_A = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_T_Total_A", "#T-TOTAL-A", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_T_Total_M = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_T_Total_M", "#T-TOTAL-M", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_C_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_C_Total", "#C-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_F_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_F_Total", "#F-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_T_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_T_Total", "#T-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Fdtra_Total = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Fdtra_Total", "#FDTRA-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Totals_Pnd_Cdtra_Total = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Cdtra_Total", "#CDTRA-TOTAL", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Totals_Pnd_Tdtra_Total = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tdtra_Total", "#TDTRA-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Fin_Dtra_Tc = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Fin_Dtra_Tc", "#FIN-DTRA-TC", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Totals_Pnd_Fin_Dtra_Tf = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Fin_Dtra_Tf", "#FIN-DTRA-TF", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Totals_Pnd_Sum_Dtra_Tc = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Sum_Dtra_Tc", "#SUM-DTRA-TC", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Totals_Pnd_Sum_Dtra_Tf = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Sum_Dtra_Tf", "#SUM-DTRA-TF", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 4));
        pnd_Totals_Pnd_End_Dtrac = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_End_Dtrac", "#END-DTRAC", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Totals_Pnd_End_Dtraf = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_End_Dtraf", "#END-DTRAF", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Totals_Pnd_Diff_Dtra = pnd_Totals.newFieldArrayInGroup("pnd_Totals_Pnd_Diff_Dtra", "#DIFF-DTRA", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            4));
        pnd_Totals_Pnd_Cqty = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Cqty", "#CQTY", FieldType.PACKED_DECIMAL, 5);
        pnd_Totals_Pnd_Fqty = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Fqty", "#FQTY", FieldType.PACKED_DECIMAL, 5);
        pnd_Totals_Pnd_Tqty = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Tqty", "#TQTY", FieldType.PACKED_DECIMAL, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Sort_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Sort_Key_OLD", "Sort_Key_OLD", FieldType.STRING, 10);
        readWork01Pymnt_Check_DteOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pymnt_Check_Dte_OLD", "Pymnt_Check_Dte_OLD", FieldType.DATE);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaCdbatxa.initializeValues();

        localVariables.reset();
        pnd_End_Of_Report_Lit.setInitialValue("***** END OF REPORT *****");
        pnd_A2_Low_Values.setInitialValue("H'0000'");
        pnd_Input_Parm2.setInitialValue(" ");
        pnd_T1.setInitialValue(1);
        pnd_T2.setInitialValue(1);
        pnd_First.setInitialValue(true);
        pnd_Init.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp182() throws Exception
    {
        super("Fcpp182");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp182|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                getReports().definePrinter(2, "R1");                                                                                                                      //Natural: DEFINE PRINTER ( R1 = 1 )
                getReports().definePrinter(3, "R2");                                                                                                                      //Natural: DEFINE PRINTER ( R2 = 2 )
                //*                                                                                                                                                       //Natural: FORMAT ( R1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( R2 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
                //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
                pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                              //Natural: ASSIGN #CUR-LANG := *LANGUAGE
                pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                       //Natural: ASSIGN #CUR-LANG := CDBATXA.#LANG-MAP ( #CUR-LANG )
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                //* *ASSIGN *ERROR-TA = 'INFP9000'
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=MITL'_'"), new SignPosition (true), new InputPrompting(false),                //Natural: INPUT ( AD = MITL'_' SG = ON IP = OFF ZP = OFF ) 'Input Parm:' #INPUT-PARM
                    new ReportZeroPrint (false),"Input Parm:",pnd_Input_Parm);
                if (condition(pnd_Input_Parm.getSubstring(1,12).equals("END-OF-MONTH")))                                                                                  //Natural: IF SUBSTR ( #INPUT-PARM,1,12 ) = 'END-OF-MONTH'
                {
                    pnd_Month_End.setValue(true);                                                                                                                         //Natural: ASSIGN #MONTH-END = TRUE
                }                                                                                                                                                         //Natural: END-IF
                //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
                sub_Get_Check_Formatting_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK FILE 01 SORT-KEY EXT2 ( * )
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(1, pdaFcpaext2.getSort_Key(), pdaFcpaext2.getExt2().getValue("*"))))
                {
                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*                                                                                                                                                   //Natural: AT BREAK OF EXT2.PYMNT-CHECK-DTE
                    //*                                                                                                                                                   //Natural: AT BREAK OF EXT2.CNTRCT-TYPE-CDE
                    //*                                                                                                                                                   //Natural: AT BREAK OF SORT-KEY
                    //*                                                                                                                                                   //Natural: AT BREAK OF SORT-KEY /2/
                    //*  CHECK HEADER
                    if (condition(pdaFcpaext2.getExt2_Cntrct_Orgn_Cde().equals(pnd_A2_Low_Values) && pdaFcpaext2.getExt2_Cntrct_Type_Cde().equals(pnd_A2_Low_Values)))    //Natural: IF EXT2.CNTRCT-ORGN-CDE = #A2-LOW-VALUES AND EXT2.CNTRCT-TYPE-CDE = #A2-LOW-VALUES
                    {
                        pnd_Int_Rollover.reset();                                                                                                                         //Natural: RESET #INT-ROLLOVER
                        if (condition(pdaFcpaext2.getSort_Key_Sort_Option_Cde_Pref().equals("I")))                                                                        //Natural: IF SORT-OPTION-CDE-PREF = 'I'
                        {
                            pnd_Int_Rollover.setValue(true);                                                                                                              //Natural: ASSIGN #INT-ROLLOVER = TRUE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Prev_Hdr_Title1.setValue(pdaFcpaext2.getExt2_Hdr_Title().getValue(1));                                                                        //Natural: MOVE EXT2.HDR-TITLE ( 1 ) TO #PREV-HDR-TITLE1
                        //*  RCC
                        pnd_Prev_Hdr_Title2.setValue(pdaFcpaext2.getExt2_Hdr_Title().getValue(2));                                                                        //Natural: MOVE EXT2.HDR-TITLE ( 2 ) TO #PREV-HDR-TITLE2
                        pnd_Ws_Cntrct_Orgn_Cde.setValue(pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde());                                                                  //Natural: ASSIGN #WS-CNTRCT-ORGN-CDE := SORT-CNTRCT-ORGN-CDE
                                                                                                                                                                          //Natural: PERFORM SET-UP-HEADER
                        sub_Set_Up_Header();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Dtl_Recs_Found.reset();                                                                                                                       //Natural: RESET #DTL-RECS-FOUND
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Cancelfslash_Stop.reset();                                                                                                                        //Natural: RESET #CANCEL/STOP
                    //*  JWO 2010-11
                    if (condition(pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN")     //Natural: IF EXT2.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'CN' OR = 'S' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
                        || pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
                        || pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
                        || pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))
                    {
                        pnd_Cancelfslash_Stop.setValue(true);                                                                                                             //Natural: ASSIGN #CANCEL/STOP = TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(! (pnd_Dtl_Recs_Found.getBoolean())))                                                                                                   //Natural: IF NOT #DTL-RECS-FOUND
                    {
                        pnd_Dtl_Recs_Found.setValue(true);                                                                                                                //Natural: ASSIGN #DTL-RECS-FOUND = TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Init.getBoolean()))                                                                                                                 //Natural: IF #INIT
                    {
                        pnd_Init.reset();                                                                                                                                 //Natural: RESET #INIT
                        if (condition(pdaFcpaext2.getExt2_Pymnt_Ftre_Ind().notEquals("F")))                                                                               //Natural: IF EXT2.PYMNT-FTRE-IND NE 'F'
                        {
                            pnd_Curr_Dte.setValue(pdaFcpaext2.getExt2_Pymnt_Check_Dte());                                                                                 //Natural: ASSIGN #CURR-DTE := EXT2.PYMNT-CHECK-DTE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pnd_Month_End.getBoolean()))                                                                                                        //Natural: IF #MONTH-END
                        {
                            pnd_Month.setValueEdited(pdaFcpaext2.getExt2_Pymnt_Check_Dte(),new ReportEditMask("LLLLLLLLL"));                                              //Natural: MOVE EDITED EXT2.PYMNT-CHECK-DTE ( EM = L ( 9 ) ) TO #MONTH
                            pnd_Year.setValueEdited(pdaFcpaext2.getExt2_Pymnt_Check_Dte(),new ReportEditMask("YYYY"));                                                    //Natural: MOVE EDITED EXT2.PYMNT-CHECK-DTE ( EM = YYYY ) TO #YEAR
                            pnd_Cf_Txt.reset();                                                                                                                           //Natural: RESET #CF-TXT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Cancelfslash_Stop.getBoolean()))                                                                                            //Natural: IF #CANCEL/STOP
                            {
                                pnd_Hdr_Dte.setValueEdited(pdaFcpaext2.getExt2_Pymnt_Intrfce_Dte(),new ReportEditMask("MM/DD/YYYY"));                                     //Natural: MOVE EDITED EXT2.PYMNT-INTRFCE-DTE ( EM = MM/DD/YYYY ) TO #HDR-DTE
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Hdr_Dte.setValueEdited(pdaFcpaext2.getExt2_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                       //Natural: MOVE EDITED EXT2.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #HDR-DTE
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pnd_Curr_Dte.equals(pdaFcpaext2.getExt2_Pymnt_Check_Dte())))                                                                    //Natural: IF #CURR-DTE = EXT2.PYMNT-CHECK-DTE
                            {
                                pnd_Cf_Txt.setValue("(CURRENT)");                                                                                                         //Natural: ASSIGN #CF-TXT := '(CURRENT)'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Cf_Txt.setValue("(FUTURE)");                                                                                                          //Natural: ASSIGN #CF-TXT := '(FUTURE)'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SET-UP-HEADER-2
                        sub_Set_Up_Header_2();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER4
                        sub_Center_Header4();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                 //Natural: IF NOT #INT-ROLLOVER
                        {
                            getReports().getPageNumberDbs(2).setValue(0);                                                                                                 //Natural: ASSIGN *PAGE-NUMBER ( R1 ) = 0
                            getReports().newPage(new ReportSpecification(0));                                                                                             //Natural: NEWPAGE ( R1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().getPageNumberDbs(3).setValue(0);                                                                                                 //Natural: ASSIGN *PAGE-NUMBER ( R2 ) = 0
                            getReports().newPage(new ReportSpecification(0));                                                                                             //Natural: NEWPAGE ( R2 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Pymnt_Prcss_Seq_Nbr.setValue(pdaFcpaext2.getExt2_Pymnt_Prcss_Seq_Nbr());                                                                          //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR = EXT2.PYMNT-PRCSS-SEQ-NBR
                    short decideConditionsMet939 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FIRST
                    if (condition(pnd_First.getBoolean()))
                    {
                        decideConditionsMet939++;
                        pnd_First.reset();                                                                                                                                //Natural: RESET #FIRST
                    }                                                                                                                                                     //Natural: WHEN #PREV-SEQ-NBR NE #CURR-SEQ-NBR
                    else if (condition(pnd_Prev_Seq_Nbr.notEquals(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr)))
                    {
                        decideConditionsMet939++;
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                        sub_Print_Detail_Lines();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet939 > 0))
                    {
                        pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                              //Natural: ASSIGN #PREV-SEQ-NBR = #CURR-SEQ-NBR
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ARRAY-INDEX
                    sub_Determine_Array_Index();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM BUILD-DETAIL-LINES
                    sub_Build_Detail_Lines();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    readWork01Sort_KeyOld.setValue(pdaFcpaext2.getSort_Key());                                                                                            //Natural: END-WORK
                    readWork01Pymnt_Check_DteOld.setValue(pdaFcpaext2.getExt2_Pymnt_Check_Dte());
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (Global.isEscape()) return;
                //* ********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-SORT-KEY
                //* ********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-SORT-KEY-CONTRCT-ORGN
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-HEADER
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-UP-HEADER-2
                //* *********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CENTER-HEADER2
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CENTER-HEADER4
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-DETAIL-LINES
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MISCELLANEOUS-DATA
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUMULATE-AMOUNTS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DTE-SUB-TOTAL
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUB-TOTAL
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-HEADER
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-ARRAY-INDEX
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-LINES
                //* *  'SEQUENCE NO:' #MISC.PYMNT-CHECK-SEQ-NBR(EM=9999999) 2X
                //* *  #CGE-TXT #MISC.PYMNT-CHECK-NBR(EM=9999999) 2X
                //* *  'SEQUENCE NO:' #MISC.PYMNT-CHECK-SEQ-NBR(EM=9999999) 2X
                //* *  #CGE-TXT #MISC.PYMNT-CHECK-NBR(EM=9999999) 2X
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-SUMMARY-DATA
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-TXT-FOR-PRINT-SUMMARY
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PREV-INDEX
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REG-TOTALS
                //* ** END OF CODE ADDED BY AMC 12-21-2001
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-INT-ROLLOVER-TOTALS
                //* *********************** RL BEGIN - PAYEE MATCH ************MAR 06,2006
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
                //*  RL PAYEE MATCH
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //*  RL PAYEE MATCH
                //* ************************** RL END-PAYEE MATCH *************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //*  34T
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( R1 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( R2 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Break_Sort_Key() throws Exception                                                                                                                    //Natural: BREAK-SORT-KEY
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        if (condition(pnd_Dtl_Recs_Found.getBoolean()))                                                                                                                   //Natural: IF #DTL-RECS-FOUND
        {
            pnd_Dtl_Recs_Found.reset();                                                                                                                                   //Natural: RESET #DTL-RECS-FOUND
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-TOTAL
            sub_Print_Sub_Total();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Summary.setValue(true);                                                                                                                                   //Natural: ASSIGN #SUMMARY = TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY
            sub_Print_Summary();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Summary.reset();                                                                                                                                          //Natural: RESET #SUMMARY
            pnd_First.setValue(true);                                                                                                                                     //Natural: ASSIGN #FIRST = TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                             //Natural: IF NOT #INT-ROLLOVER
            {
                getReports().getPageNumberDbs(2).setValue(0);                                                                                                             //Natural: ASSIGN *PAGE-NUMBER ( R1 ) = 0
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( R1 )
                if (condition(Global.isEscape())){return;}
                getReports().skip(0, 5);                                                                                                                                  //Natural: SKIP ( R1 ) 5
                getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"***** NO RECORDS PROCESSED FOR THIS TYPE *****");                       //Natural: WRITE ( R1 ) ///// 45T '***** NO RECORDS PROCESSED FOR THIS TYPE *****'
                if (Global.isEscape()) return;
                getReports().skip(0, 5);                                                                                                                                  //Natural: SKIP ( R1 ) 5
                getReports().write(2, new TabSetting(55),pnd_End_Of_Report_Lit);                                                                                          //Natural: WRITE ( R1 ) 55T #END-OF-REPORT-LIT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().getPageNumberDbs(3).setValue(0);                                                                                                             //Natural: ASSIGN *PAGE-NUMBER ( R2 ) = 0
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( R2 )
                if (condition(Global.isEscape())){return;}
                getReports().skip(0, 5);                                                                                                                                  //Natural: SKIP ( R2 ) 5
                getReports().write(3, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"***** NO RECORDS PROCESSED FOR THIS TYPE *****");                       //Natural: WRITE ( R2 ) ///// 45T '***** NO RECORDS PROCESSED FOR THIS TYPE *****'
                if (Global.isEscape()) return;
                getReports().skip(0, 5);                                                                                                                                  //Natural: SKIP ( R2 ) 5
                getReports().write(3, new TabSetting(55),pnd_End_Of_Report_Lit);                                                                                          //Natural: WRITE ( R2 ) 55T #END-OF-REPORT-LIT
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Item.reset();                                                                                                                                                 //Natural: RESET #ITEM #DTE #INPUT-PARM2 #SUB #SUM ( * )
        pnd_Dte.reset();
        pnd_Input_Parm2.reset();
        pnd_Sub.reset();
        pnd_Sum.getValue("*").reset();
        pnd_Init.setValue(true);                                                                                                                                          //Natural: ASSIGN #INIT = TRUE
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Break_Sort_Key_Contrct_Orgn() throws Exception                                                                                                       //Natural: BREAK-SORT-KEY-CONTRCT-ORGN
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        pnd_End_Of_Contrct.setValue(true);                                                                                                                                //Natural: ASSIGN #END-OF-CONTRCT = TRUE
        pnd_Summary.setValue(true);                                                                                                                                       //Natural: ASSIGN #SUMMARY = TRUE
        if (condition(pnd_Reg_Pymnt_Printed.getBoolean()))                                                                                                                //Natural: IF #REG-PYMNT-PRINTED
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-REG-TOTALS
            sub_Print_Reg_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().skip(0, 5);                                                                                                                                      //Natural: SKIP ( R1 ) 5
            getReports().write(2, new TabSetting(55),pnd_End_Of_Report_Lit);                                                                                              //Natural: WRITE ( R1 ) 55T #END-OF-REPORT-LIT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Int_Rollover_Printed.getBoolean()))                                                                                                             //Natural: IF #INT-ROLLOVER-PRINTED
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-INT-ROLLOVER-TOTALS
            sub_Print_Int_Rollover_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().skip(0, 5);                                                                                                                                      //Natural: SKIP ( R2 ) 5
            getReports().write(3, new TabSetting(55),pnd_End_Of_Report_Lit);                                                                                              //Natural: WRITE ( R2 ) 55T #END-OF-REPORT-LIT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_End_Of_Contrct.reset();                                                                                                                                       //Natural: RESET #END-OF-CONTRCT #SUMMARY #REG-PYMNT-PRINTED #INT-ROLLOVER-PRINTED #TOTALS #G-TOT ( * )
        pnd_Summary.reset();
        pnd_Reg_Pymnt_Printed.reset();
        pnd_Int_Rollover_Printed.reset();
        pnd_Totals.reset();
        pnd_G_Tot.getValue("*").reset();
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Set_Up_Header() throws Exception                                                                                                                     //Natural: SET-UP-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_End_Of_Contrct.getBoolean())))                                                                                                               //Natural: IF NOT #END-OF-CONTRCT
        {
            pnd_Ws_Sort_Option_Cde.setValue(pdaFcpaext2.getSort_Key_Sort_Option_Cde());                                                                                   //Natural: ASSIGN #WS-SORT-OPTION-CDE = SORT-OPTION-CDE
            //*  #WS-CNTRCT-ORGN-CDE := SORT-CNTRCT-ORGN-CDE  /* RCC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Sort_Option_Cde.setValue(pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde);                                                                                    //Natural: ASSIGN #WS-SORT-OPTION-CDE = #OLD-SORT-OPTION-CDE
            //*  #WS-CNTRCT-ORGN-CDE := SORT-CNTRCT-ORGN-CDE  /* RCC
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hdr_Sort_Option_Cde.setValue(pnd_Ws_Sort_Option_Cde, MoveOption.LeftJustified);                                                                               //Natural: MOVE LEFT #WS-SORT-OPTION-CDE TO #HDR-SORT-OPTION-CDE
        pnd_Hdr_Sort_Orgn_Cde.setValue(pnd_Ws_Cntrct_Orgn_Cde, MoveOption.LeftJustified);                                                                                 //Natural: MOVE LEFT #WS-CNTRCT-ORGN-CDE TO #HDR-SORT-ORGN-CDE
        if (condition(pnd_Header1.equals(" ")))                                                                                                                           //Natural: IF #HEADER1 = ' '
        {
            pnd_Header1.setValue("Consolidated Payment System");                                                                                                          //Natural: ASSIGN #HEADER1 := 'Consolidated Payment System'
            pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(60,27)).divide(2));                                                                 //Natural: ASSIGN #LEN := ( 60 - 27 ) / 2
            pnd_W_Fld.moveAll("H'00'");                                                                                                                                   //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
            pnd_Header1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header1));                                                                //Natural: COMPRESS #W-FLD #HEADER1 INTO #HEADER1 LEAVE NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Header1,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                              //Natural: EXAMINE FULL #HEADER1 FOR FULL H'00' REPLACE ' '
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_End_Of_Contrct.getBoolean())))                                                                                                               //Natural: IF NOT #END-OF-CONTRCT
        {
            pnd_Input_Parm2.setValue(DbsUtil.compress(pdaFcpaext2.getExt2_Hdr_Title().getValue(1), pnd_Input_Parm, pdaFcpaext2.getExt2_Hdr_Title().getValue(2),           //Natural: COMPRESS EXT2.HDR-TITLE ( 1 ) #INPUT-PARM EXT2.HDR-TITLE ( 2 ) 'PAYMENT' INTO #INPUT-PARM2
                "PAYMENT"));
            if (condition(pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("TPA") || pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("IPRO") || pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("P&I"))) //Natural: IF SORT-OPTION-CDE-X = 'TPA' OR = 'IPRO' OR = 'P&I'
            {
                //* *    ASSIGN #SUB-TXT = SORT-OPTION-CDE-X   /* RCC
                //*  RCC
                pnd_Sub_Txt.setValue(" ");                                                                                                                                //Natural: ASSIGN #SUB-TXT = ' '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                         //Natural: IF NOT #INT-ROLLOVER
                {
                    pnd_Sub_Txt.setValue("MATURITY BENEFIT ANNUAL");                                                                                                      //Natural: ASSIGN #SUB-TXT = 'MATURITY BENEFIT ANNUAL'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sub_Txt.setValue("MATURITY INTERNAL ROLLOVER ANNUAL");                                                                                            //Natural: ASSIGN #SUB-TXT = 'MATURITY INTERNAL ROLLOVER ANNUAL'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Input_Parm2.setValue(DbsUtil.compress(pnd_Prev_Hdr_Title1, pnd_Input_Parm, "PAYMENT"));                                                                   //Natural: COMPRESS #PREV-HDR-TITLE1 #INPUT-PARM 'PAYMENT' INTO #INPUT-PARM2
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Header2.setValue(DbsUtil.compress(pnd_Input_Parm2, "REGISTER"));                                                                                              //Natural: COMPRESS #INPUT-PARM2 'REGISTER' INTO #HEADER2
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER2
        sub_Center_Header2();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(! (pnd_End_Of_Contrct.getBoolean())))                                                                                                               //Natural: IF NOT #END-OF-CONTRCT
        {
            pnd_Sub_Txt_A.setValue(DbsUtil.compress(pdaFcpaext2.getExt2_Hdr_Title().getValue(2), "ANNUAL"));                                                              //Natural: COMPRESS EXT2.HDR-TITLE ( 2 ) 'ANNUAL' INTO #SUB-TXT-A
            pnd_Sub_Txt_M.setValue(DbsUtil.compress(pdaFcpaext2.getExt2_Hdr_Title().getValue(2), "MONTHLY"));                                                             //Natural: COMPRESS EXT2.HDR-TITLE ( 2 ) 'MONTHLY' INTO #SUB-TXT-M
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Sub_Txt_A.setValue(DbsUtil.compress(pnd_Prev_Hdr_Title2, "ANNUAL"));                                                                                      //Natural: COMPRESS #PREV-HDR-TITLE2 'ANNUAL' INTO #SUB-TXT-A
            pnd_Sub_Txt_M.setValue(DbsUtil.compress(pnd_Prev_Hdr_Title2, "MONTHLY"));                                                                                     //Natural: COMPRESS #PREV-HDR-TITLE2 'MONTHLY' INTO #SUB-TXT-M
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER4
        sub_Center_Header4();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Set_Up_Header_2() throws Exception                                                                                                                   //Natural: SET-UP-HEADER-2
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Month_End.getBoolean()))                                                                                                                        //Natural: IF #MONTH-END
        {
            if (condition(pnd_Month.notEquals(" ")))                                                                                                                      //Natural: IF #MONTH NE ' '
            {
                pnd_Header2.setValue(DbsUtil.compress(pnd_Header2, "for", pnd_Month, pnd_Year));                                                                          //Natural: COMPRESS #HEADER2 'for' #MONTH #YEAR INTO #HEADER2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Hdr_Dte.notEquals(" ")))                                                                                                                    //Natural: IF #HDR-DTE NE ' '
            {
                pnd_Header2.setValue(DbsUtil.compress(pnd_Header2, "for", pnd_Hdr_Dte));                                                                                  //Natural: COMPRESS #HEADER2 'for' #HDR-DTE INTO #HEADER2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER2
        sub_Center_Header2();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Center_Header2() throws Exception                                                                                                                    //Natural: CENTER-HEADER2
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Len.reset();                                                                                                                                                  //Natural: RESET #LEN #W-FLD2
        pnd_W_Fld2.reset();
        DbsUtil.examine(new ExamineSource(pnd_Header2), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                        //Natural: EXAMINE #HEADER2 FOR ' ' GIVING LENGTH #LEN
        pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(84,pnd_Len)).divide(2));                                                                //Natural: COMPUTE #LEN = ( 84 - #LEN ) / 2
        //* *COMPUTE #LEN = (60 - #LEN) / 2
        pnd_W_Fld2.moveAll("H'00'");                                                                                                                                      //Natural: MOVE ALL H'00' TO #W-FLD2 UNTIL #LEN
        pnd_Header2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld2, pnd_Header2));                                                                   //Natural: COMPRESS #W-FLD2 #HEADER2 INTO #HEADER2 LEAVE NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Header2,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                                  //Natural: EXAMINE FULL #HEADER2 FOR FULL H'00' REPLACE ' '
        pnd_Len.reset();                                                                                                                                                  //Natural: RESET #LEN #W-FLD2
        pnd_W_Fld2.reset();
    }
    private void sub_Center_Header4() throws Exception                                                                                                                    //Natural: CENTER-HEADER4
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Len.reset();                                                                                                                                                  //Natural: RESET #LEN #W-FLD
        pnd_W_Fld.reset();
        if (condition(! (pnd_Summary.getBoolean())))                                                                                                                      //Natural: IF NOT #SUMMARY
        {
            pnd_Header4.setValue(pnd_Sub_Txt);                                                                                                                            //Natural: ASSIGN #HEADER4 := #SUB-TXT
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Header4), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                        //Natural: EXAMINE #HEADER4 FOR ' ' GIVING LENGTH #LEN
        pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(60,pnd_Len)).divide(2));                                                                //Natural: ASSIGN #LEN := ( 60 - #LEN ) / 2
        pnd_W_Fld.moveAll("H'00'");                                                                                                                                       //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
        pnd_Header4.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header4));                                                                    //Natural: COMPRESS #W-FLD #HEADER4 INTO #HEADER4 LEAVE NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Header4,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                                  //Natural: EXAMINE FULL #HEADER4 FOR FULL H'00' REPLACE ' '
    }
    private void sub_Build_Detail_Lines() throws Exception                                                                                                                //Natural: BUILD-DETAIL-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  MM 06/05/01
        rtb.reset();                                                                                                                                                      //Natural: RESET RTB IVC-FROM-RTB-ROLLOVER
        ivc_From_Rtb_Rollover.reset();
        if (condition(pdaFcpaext2.getExt2_Cntrct_Pymnt_Type_Ind().equals("N") && pdaFcpaext2.getExt2_Cntrct_Sttlmnt_Type_Ind().equals("X")))                              //Natural: IF EXT2.CNTRCT-PYMNT-TYPE-IND = 'N' AND EXT2.CNTRCT-STTLMNT-TYPE-IND = 'X'
        {
            rtb.setValue(true);                                                                                                                                           //Natural: ASSIGN RTB = TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpaext2.getExt2_Cntrct_Pymnt_Type_Ind().equals("I") && pdaFcpaext2.getExt2_Cntrct_Sttlmnt_Type_Ind().equals("R")))                          //Natural: IF EXT2.CNTRCT-PYMNT-TYPE-IND = 'I' AND EXT2.CNTRCT-STTLMNT-TYPE-IND = 'R'
            {
                ivc_From_Rtb_Rollover.setValue(true);                                                                                                                     //Natural: ASSIGN IVC-FROM-RTB-ROLLOVER = TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO C-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaFcpaext2.getExt2_C_Inv_Acct())); pnd_I.nadd(1))
        {
            pnd_Ln.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #LN
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpaext2.getExt2_Inv_Acct_Cde().getValue(pnd_I));                                                //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT = EXT2.INV-ACCT-CDE ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaFcpaext2.getExt2_Inv_Acct_Valuat_Period().getValue(pnd_I));                              //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD = EXT2.INV-ACCT-VALUAT-PERIOD ( #I )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' #FUND-PDA
            if (condition(Global.isEscape())) return;
            pnd_Detail_Pnd_Fund_Desc.getValue(pnd_Ln).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8());                                                        //Natural: ASSIGN #DETAIL.#FUND-DESC ( #LN ) = #FUND-PDA.#INV-ACCT-DESC-8
            pnd_Detail_Pnd_Gross_Amt.getValue(pnd_Ln).compute(new ComputeParameters(false, pnd_Detail_Pnd_Gross_Amt.getValue(pnd_Ln)), pdaFcpaext2.getExt2_Inv_Acct_Settl_Amt().getValue(pnd_I).add(pdaFcpaext2.getExt2_Inv_Acct_Dvdnd_Amt().getValue(pnd_I))); //Natural: COMPUTE #DETAIL.#GROSS-AMT ( #LN ) = EXT2.INV-ACCT-SETTL-AMT ( #I ) + EXT2.INV-ACCT-DVDND-AMT ( #I )
            pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Ln).setValue(pdaFcpaext2.getExt2_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                                  //Natural: ASSIGN #DETAIL.#INV-ACCT-FDRL-TAX-AMT ( #LN ) = EXT2.INV-ACCT-FDRL-TAX-AMT ( #I )
            pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_Ln).setValue(pdaFcpaext2.getExt2_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                                //Natural: ASSIGN #DETAIL.#INV-ACCT-STATE-TAX-AMT ( #LN ) = EXT2.INV-ACCT-STATE-TAX-AMT ( #I )
            pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_Ln).setValue(pdaFcpaext2.getExt2_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                                //Natural: ASSIGN #DETAIL.#INV-ACCT-LOCAL-TAX-AMT ( #LN ) = EXT2.INV-ACCT-LOCAL-TAX-AMT ( #I )
            pnd_Detail_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_Ln).setValue(pdaFcpaext2.getExt2_Inv_Acct_Dpi_Amt().getValue(pnd_I));                                            //Natural: ASSIGN #DETAIL.#INV-ACCT-DPI-AMT ( #LN ) = EXT2.INV-ACCT-DPI-AMT ( #I )
            pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Ln).setValue(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                //Natural: ASSIGN #DETAIL.#INV-ACCT-NET-PYMNT-AMT ( #LN ) = EXT2.INV-ACCT-NET-PYMNT-AMT ( #I )
            if (condition(! (rtb.getBoolean()) && ! (ivc_From_Rtb_Rollover.getBoolean())))                                                                                //Natural: IF NOT RTB AND NOT IVC-FROM-RTB-ROLLOVER
            {
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                  //Natural: IF #FUND-PDA.#DIVIDENDS
                {
                    if (condition(pnd_Dte_First_Periodic.equals(getZero())))                                                                                              //Natural: IF #DTE-FIRST-PERIODIC = 0
                    {
                        pnd_Dte_First_Periodic.setValue(pdaFcpaext2.getExt2_Pymnt_Settlmnt_Dte());                                                                        //Natural: ASSIGN #DTE-FIRST-PERIODIC = EXT2.PYMNT-SETTLMNT-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaFcpaext2.getExt2_Pymnt_Settlmnt_Dte().equals(pnd_Dte_First_Periodic)))                                                               //Natural: IF EXT2.PYMNT-SETTLMNT-DTE = #DTE-FIRST-PERIODIC
                    {
                        pnd_Misc_Pnd_Inv_Acct_Cntrct_Amt.nadd(pdaFcpaext2.getExt2_Inv_Acct_Settl_Amt().getValue(pnd_I));                                                  //Natural: ADD EXT2.INV-ACCT-SETTL-AMT ( #I ) TO #MISC.#INV-ACCT-CNTRCT-AMT
                        pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt.nadd(pdaFcpaext2.getExt2_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                                                   //Natural: ADD EXT2.INV-ACCT-DVDND-AMT ( #I ) TO #MISC.#INV-ACCT-DVDND-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Detail_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_Ln).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_3());                                     //Natural: ASSIGN #DETAIL.#INV-ACCT-VALUAT-PERIOD ( #LN ) = #FUND-PDA.#VALUAT-DESC-3
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ln.equals(1)))                                                                                                                              //Natural: IF #LN = 1
            {
                                                                                                                                                                          //Natural: PERFORM GET-MISCELLANEOUS-DATA
                sub_Get_Miscellaneous_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(rtb.getBoolean() || ivc_From_Rtb_Rollover.getBoolean()))                                                                                        //Natural: IF RTB OR IVC-FROM-RTB-ROLLOVER
            {
                pnd_Detail_Pnd_Rtb_Ind.getValue(pnd_Ln).setValue("(RTB)");                                                                                                //Natural: ASSIGN #DETAIL.#RTB-IND ( #LN ) = '(RTB)'
                //* ** 2 LINES BELOW CHANGED BY AMC 11-16-2001
                pnd_Detail_Pnd_Rtb_Pymnt_Amt.getValue(pnd_Ln).compute(new ComputeParameters(false, pnd_Detail_Pnd_Rtb_Pymnt_Amt.getValue(pnd_Ln)), pdaFcpaext2.getExt2_Inv_Acct_Settl_Amt().getValue(pnd_I).add(pdaFcpaext2.getExt2_Inv_Acct_Dvdnd_Amt().getValue(pnd_I))); //Natural: ASSIGN #DETAIL.#RTB-PYMNT-AMT ( #LN ) = EXT2.INV-ACCT-SETTL-AMT ( #I ) + EXT2.INV-ACCT-DVDND-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Detail_Pnd_Pymnt_Settlmnt_Dte.getValue(pnd_Ln).setValueEdited(pdaFcpaext2.getExt2_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/YY"));                      //Natural: MOVE EDITED EXT2.PYMNT-SETTLMNT-DTE ( EM = MM/YY ) TO #DETAIL.#PYMNT-SETTLMNT-DTE ( #LN )
            //* ** START OF CODE ADDED BY AMC 11-16-2001 & 12-21-2001
            if (condition(pdaFcpaext2.getExt2_Cntrct_Roll_Dest_Cde().equals("DTRA")))                                                                                     //Natural: IF EXT2.CNTRCT-ROLL-DEST-CDE = 'DTRA'
            {
                pnd_Misc_Pnd_Dtra_Net_Payment.compute(new ComputeParameters(false, pnd_Misc_Pnd_Dtra_Net_Payment), (pdaFcpaext2.getExt2_Inv_Acct_Settl_Amt().getValue(pnd_I).add(pdaFcpaext2.getExt2_Inv_Acct_Dvdnd_Amt().getValue(pnd_I))).subtract((pdaFcpaext2.getExt2_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I).add(pdaFcpaext2.getExt2_Inv_Acct_State_Tax_Amt().getValue(pnd_I)).add(pdaFcpaext2.getExt2_Inv_Acct_Local_Tax_Amt().getValue(pnd_I)))).add(pdaFcpaext2.getExt2_Inv_Acct_Dpi_Amt().getValue(pnd_I))); //Natural: ASSIGN #DTRA-NET-PAYMENT = ( EXT2.INV-ACCT-SETTL-AMT ( #I ) + EXT2.INV-ACCT-DVDND-AMT ( #I ) ) - ( EXT2.INV-ACCT-FDRL-TAX-AMT ( #I ) + EXT2.INV-ACCT-STATE-TAX-AMT ( #I ) + EXT2.INV-ACCT-LOCAL-TAX-AMT ( #I ) ) + EXT2.INV-ACCT-DPI-AMT ( #I )
                //*  FOR CURRENT
                if (condition(pdaFcpaext2.getExt2_Pymnt_Ftre_Ind().notEquals("F")))                                                                                       //Natural: IF EXT2.PYMNT-FTRE-IND NE 'F'
                {
                    //*  CHECK
                    short decideConditionsMet1275 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE EXT2.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
                    if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(1))))
                    {
                        decideConditionsMet1275++;
                        //*         COMPUTE #CDTRA-TOTAL(1) = #DTRA-NET-PAYMENT /* RCC 02-05-2002
                        //*  ROXAN
                        if (condition(pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ")))                                                                   //Natural: IF EXT2.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' '
                        {
                            pnd_Totals_Pnd_Cdtra_Total.getValue(1).nadd(pnd_Misc_Pnd_Dtra_Net_Payment);                                                                   //Natural: ASSIGN #CDTRA-TOTAL ( 1 ) := #CDTRA-TOTAL ( 1 ) + #DTRA-NET-PAYMENT
                            //*  EFT
                            //*  GLOBAL
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 2
                    else if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(2))))
                    {
                        decideConditionsMet1275++;
                        pnd_Totals_Pnd_Cdtra_Total.getValue(2).nadd(pnd_Misc_Pnd_Dtra_Net_Payment);                                                                       //Natural: ASSIGN #CDTRA-TOTAL ( 2 ) := #CDTRA-TOTAL ( 2 ) + #DTRA-NET-PAYMENT
                    }                                                                                                                                                     //Natural: VALUE 3
                    else if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(3))))
                    {
                        decideConditionsMet1275++;
                        pnd_Totals_Pnd_Cdtra_Total.getValue(3).nadd(pnd_Misc_Pnd_Dtra_Net_Payment);                                                                       //Natural: ASSIGN #CDTRA-TOTAL ( 3 ) := #CDTRA-TOTAL ( 3 ) + #DTRA-NET-PAYMENT
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                        //* (2935)
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  FOR FUTURE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  CHECK
                    short decideConditionsMet1295 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE EXT2.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
                    if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(1))))
                    {
                        decideConditionsMet1295++;
                        //*         COMPUTE #FDTRA-TOTAL(1) = #DTRA-NET-PAYMENT /* RCC 02-05-2002
                        //*  ROXAN
                        if (condition(pdaFcpaext2.getExt2_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ")))                                                                   //Natural: IF EXT2.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' '
                        {
                            pnd_Totals_Pnd_Fdtra_Total.getValue(1).nadd(pnd_Misc_Pnd_Dtra_Net_Payment);                                                                   //Natural: ASSIGN #FDTRA-TOTAL ( 1 ) := #FDTRA-TOTAL ( 1 ) + #DTRA-NET-PAYMENT
                            //*  EFT
                            //*  GLOBAL
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 2
                    else if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(2))))
                    {
                        decideConditionsMet1295++;
                        pnd_Totals_Pnd_Fdtra_Total.getValue(2).nadd(pnd_Misc_Pnd_Dtra_Net_Payment);                                                                       //Natural: ASSIGN #FDTRA-TOTAL ( 2 ) := #FDTRA-TOTAL ( 2 ) + #DTRA-NET-PAYMENT
                    }                                                                                                                                                     //Natural: VALUE 3
                    else if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(3))))
                    {
                        decideConditionsMet1295++;
                        pnd_Totals_Pnd_Fdtra_Total.getValue(3).nadd(pnd_Misc_Pnd_Dtra_Net_Payment);                                                                       //Natural: ASSIGN #FDTRA-TOTAL ( 3 ) := #FDTRA-TOTAL ( 3 ) + #DTRA-NET-PAYMENT
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                        //* (2935)
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //* (2930)
                }                                                                                                                                                         //Natural: END-IF
                //* (2890)
            }                                                                                                                                                             //Natural: END-IF
            //* ** END OF CODE ADDED BY AMC 11-16-2001
                                                                                                                                                                          //Natural: PERFORM ACCUMULATE-AMOUNTS
            sub_Accumulate_Amounts();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM BUILD-SUMMARY-DATA
            sub_Build_Summary_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Miscellaneous_Data() throws Exception                                                                                                            //Natural: GET-MISCELLANEOUS-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Misc.setValuesByName(pdaFcpaext2.getExt2_Extr2());                                                                                                            //Natural: MOVE BY NAME EXTR2 TO #MISC
        pnd_Misc_Pnd_Annt.setValue(DbsUtil.compress(pdaFcpaext2.getExt2_Ph_First_Name(), pdaFcpaext2.getExt2_Ph_Middle_Name(), pdaFcpaext2.getExt2_Ph_Last_Name()));      //Natural: COMPRESS EXT2.PH-FIRST-NAME EXT2.PH-MIDDLE-NAME EXT2.PH-LAST-NAME INTO #MISC.#ANNT
        if (condition(DbsUtil.maskMatches(pdaFcpaext2.getExt2_Pymnt_Nme().getValue(1),"'CR '")))                                                                          //Natural: IF EXT2.PYMNT-NME ( 1 ) = MASK ( 'CR ' )
        {
            pnd_Misc_Pnd_Payee.setValue(pdaFcpaext2.getExt2_Pymnt_Addr_Line1_Txt().getValue(1));                                                                          //Natural: ASSIGN #MISC.#PAYEE := EXT2.PYMNT-ADDR-LINE1-TXT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Misc_Pnd_Payee.setValue(pdaFcpaext2.getExt2_Pymnt_Nme().getValue(1));                                                                                     //Natural: ASSIGN #MISC.#PAYEE := EXT2.PYMNT-NME ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Misc_Pnd_Cntrct_Nbr.getValue(1).setValue(pdaFcpaext2.getExt2_Cntrct_Da_Tiaa_1_Nbr());                                                                         //Natural: ASSIGN #CNTRCT-NBR ( 1 ) := EXT2.CNTRCT-DA-TIAA-1-NBR
        pnd_Misc_Pnd_Cntrct_Nbr.getValue(2).setValue(pdaFcpaext2.getExt2_Cntrct_Da_Tiaa_2_Nbr());                                                                         //Natural: ASSIGN #CNTRCT-NBR ( 2 ) := EXT2.CNTRCT-DA-TIAA-2-NBR
        pnd_Misc_Pnd_Cntrct_Nbr.getValue(3).setValue(pdaFcpaext2.getExt2_Cntrct_Da_Tiaa_3_Nbr());                                                                         //Natural: ASSIGN #CNTRCT-NBR ( 3 ) := EXT2.CNTRCT-DA-TIAA-3-NBR
        pnd_Misc_Pnd_Cntrct_Nbr.getValue(4).setValue(pdaFcpaext2.getExt2_Cntrct_Da_Tiaa_4_Nbr());                                                                         //Natural: ASSIGN #CNTRCT-NBR ( 4 ) := EXT2.CNTRCT-DA-TIAA-4-NBR
        pnd_Misc_Pnd_Cntrct_Nbr.getValue(5).setValue(pdaFcpaext2.getExt2_Cntrct_Da_Tiaa_5_Nbr());                                                                         //Natural: ASSIGN #CNTRCT-NBR ( 5 ) := EXT2.CNTRCT-DA-TIAA-5-NBR
        pnd_Misc_Pnd_Cntrct_Nbr.getValue(6).setValue(pdaFcpaext2.getExt2_Cntrct_Da_Cref_1_Nbr());                                                                         //Natural: ASSIGN #CNTRCT-NBR ( 6 ) := EXT2.CNTRCT-DA-CREF-1-NBR
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            pnd_Dvdnd.setValue(true);                                                                                                                                     //Natural: ASSIGN #DVDND = TRUE
            pnd_Guar_Unit_Txt.setValue("GUAR AMOUNT:", MoveOption.RightJustified);                                                                                        //Natural: MOVE RIGHT 'GUAR AMOUNT:' TO #GUAR-UNIT-TXT
            pnd_Div_Unit_Txt.setValue("DIVIDEND:", MoveOption.RightJustified);                                                                                            //Natural: MOVE RIGHT 'DIVIDEND:' TO #DIV-UNIT-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Dvdnd.reset();                                                                                                                                            //Natural: RESET #DVDND
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Misc_Pnd_Cge_Txt.reset();                                                                                                                                     //Natural: RESET #CGE-TXT
        short decideConditionsMet1350 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE SORT-OPTION-CDE;//Natural: VALUE 'IRLC'
        if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("IRLC"))))
        {
            decideConditionsMet1350++;
            pnd_Misc_Pnd_Cge_Txt.setValue("          CLASSIC:");                                                                                                          //Natural: ASSIGN #CGE-TXT := '          CLASSIC:'
        }                                                                                                                                                                 //Natural: VALUE 'IRLR'
        else if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("IRLR"))))
        {
            decideConditionsMet1350++;
            pnd_Misc_Pnd_Cge_Txt.setValue("             ROTH:");                                                                                                          //Natural: ASSIGN #CGE-TXT := '             ROTH:'
        }                                                                                                                                                                 //Natural: VALUE 'IRLX','IP&I','IIPRO','ITPA'
        else if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("IRLX") || pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("IP&I") || pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("IIPRO") 
            || pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("ITPA"))))
        {
            decideConditionsMet1350++;
            pnd_Misc_Pnd_Cge_Txt.setValue("INTERNAL ROLLOVER:");                                                                                                          //Natural: ASSIGN #CGE-TXT := 'INTERNAL ROLLOVER:'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            short decideConditionsMet1358 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE EXT2.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet1358++;
                pnd_Misc_Pnd_Cge_Txt.setValue("            CHECK:");                                                                                                      //Natural: ASSIGN #CGE-TXT := '            CHECK:'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet1358++;
                pnd_Misc_Pnd_Cge_Txt.setValue("              EFT:");                                                                                                      //Natural: ASSIGN #CGE-TXT := '              EFT:'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(3))))
            {
                decideConditionsMet1358++;
                pnd_Misc_Pnd_Cge_Txt.setValue("           GLOBAL:");                                                                                                      //Natural: ASSIGN #CGE-TXT := '           GLOBAL:'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* ******************** RL PAYEE MATCH BEGIN JUN 6 2006 ******************
        pnd_Check_Datex.setValueEdited(pdaFcpaext2.getExt2_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED EXT2.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
        if (condition(pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(2) || pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind().equals(8))) //Natural: IF EXT2.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 3 OR = 8
        {
            pnd_Check_Number_N10.reset();                                                                                                                                 //Natural: RESET #CHECK-NUMBER-N10
            pnd_Check_Number_N10.setValue(pdaFcpaext2.getExt2_Pymnt_Check_Scrty_Nbr());                                                                                   //Natural: ASSIGN #CHECK-NUMBER-N10 := EXT2.PYMNT-CHECK-SCRTY-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Check_Number_N10.reset();                                                                                                                                 //Natural: RESET #CHECK-NUMBER-N10
            //* N7 CHK-NBR - MAKE N10 W PREFIX
            if (condition(pdaFcpaext2.getExt2_Pymnt_Check_Nbr().less(getZero())))                                                                                         //Natural: IF EXT2.PYMNT-CHECK-NBR LT 0
            {
                pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
                pnd_Check_Number_N10_Pnd_Check_Number_N7.compute(new ComputeParameters(false, pnd_Check_Number_N10_Pnd_Check_Number_N7), pdaFcpaext2.getExt2_Pymnt_Check_Nbr().multiply(-1)); //Natural: COMPUTE #CHECK-NUMBER-N7 = EXT2.PYMNT-CHECK-NBR * -1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  VALID OLD N7 CHECK-NBR
                if (condition(pdaFcpaext2.getExt2_Pymnt_Check_Nbr().greater(getZero())))                                                                                  //Natural: IF EXT2.PYMNT-CHECK-NBR GT 0
                {
                    pnd_Check_Number_N10_Pnd_Check_Number_N7.setValue(pdaFcpaext2.getExt2_Pymnt_Check_Nbr());                                                             //Natural: ASSIGN #CHECK-NUMBER-N7 := EXT2.PYMNT-CHECK-NBR
                    if (condition(pnd_Check_Datex_Pnd_Check_Date.greater(20060430)))                                                                                      //Natural: IF #CHECK-DATE GT 20060430
                    {
                        pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                 //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Check_Number_N10_Pnd_Check_Number_N3.reset();                                                                                                 //Natural: RESET #CHECK-NUMBER-N3
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Check_Number_N10_Pnd_Check_Number_A10.reset();                                                                                                    //Natural: RESET #CHECK-NUMBER-A10
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************** RL PAYEE MATCH END ***************************
        pnd_Sub_Pnd_Qty.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #SUB.#QTY
        if (condition(! (pnd_Month_End.getBoolean())))                                                                                                                    //Natural: IF NOT #MONTH-END
        {
            pnd_Dte_Pnd_Qty.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DTE.#QTY
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accumulate_Amounts() throws Exception                                                                                                                //Natural: ACCUMULATE-AMOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Item_Pnd_Gross_Amt.nadd(pnd_Detail_Pnd_Gross_Amt.getValue(pnd_Ln));                                                                                           //Natural: ADD #DETAIL.#GROSS-AMT ( #LN ) TO #ITEM.#GROSS-AMT
        pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Ln));                                                                   //Natural: ADD #DETAIL.#INV-ACCT-FDRL-TAX-AMT ( #LN ) TO #ITEM.#INV-ACCT-FDRL-TAX-AMT
        pnd_Item_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_Ln));                                                                 //Natural: ADD #DETAIL.#INV-ACCT-STATE-TAX-AMT ( #LN ) TO #ITEM.#INV-ACCT-STATE-TAX-AMT
        pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_Ln));                                                                 //Natural: ADD #DETAIL.#INV-ACCT-LOCAL-TAX-AMT ( #LN ) TO #ITEM.#INV-ACCT-LOCAL-TAX-AMT
        pnd_Item_Pnd_Inv_Acct_Dpi_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_Ln));                                                                             //Natural: ADD #DETAIL.#INV-ACCT-DPI-AMT ( #LN ) TO #ITEM.#INV-ACCT-DPI-AMT
        pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Ln));                                                                 //Natural: ADD #DETAIL.#INV-ACCT-NET-PYMNT-AMT ( #LN ) TO #ITEM.#INV-ACCT-NET-PYMNT-AMT
        pnd_Item_Pnd_Rtb_Pymnt_Amt.nadd(pnd_Detail_Pnd_Rtb_Pymnt_Amt.getValue(pnd_Ln));                                                                                   //Natural: ADD #DETAIL.#RTB-PYMNT-AMT ( #LN ) TO #ITEM.#RTB-PYMNT-AMT
        if (condition(! (pnd_Month_End.getBoolean())))                                                                                                                    //Natural: IF NOT #MONTH-END
        {
            pnd_Dte_Pnd_Gross_Amt.nadd(pnd_Detail_Pnd_Gross_Amt.getValue(pnd_Ln));                                                                                        //Natural: ADD #DETAIL.#GROSS-AMT ( #LN ) TO #DTE.#GROSS-AMT
            pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Ln));                                                                //Natural: ADD #DETAIL.#INV-ACCT-FDRL-TAX-AMT ( #LN ) TO #DTE.#INV-ACCT-FDRL-TAX-AMT
            pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_Ln));                                                              //Natural: ADD #DETAIL.#INV-ACCT-STATE-TAX-AMT ( #LN ) TO #DTE.#INV-ACCT-STATE-TAX-AMT
            pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_Ln));                                                              //Natural: ADD #DETAIL.#INV-ACCT-LOCAL-TAX-AMT ( #LN ) TO #DTE.#INV-ACCT-LOCAL-TAX-AMT
            pnd_Dte_Pnd_Inv_Acct_Dpi_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_Ln));                                                                          //Natural: ADD #DETAIL.#INV-ACCT-DPI-AMT ( #LN ) TO #DTE.#INV-ACCT-DPI-AMT
            pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Ln));                                                              //Natural: ADD #DETAIL.#INV-ACCT-NET-PYMNT-AMT ( #LN ) TO #DTE.#INV-ACCT-NET-PYMNT-AMT
            pnd_Dte_Pnd_Rtb_Pymnt_Amt.nadd(pnd_Detail_Pnd_Rtb_Pymnt_Amt.getValue(pnd_Ln));                                                                                //Natural: ADD #DETAIL.#RTB-PYMNT-AMT ( #LN ) TO #DTE.#RTB-PYMNT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sub_Pnd_Gross_Amt.nadd(pnd_Detail_Pnd_Gross_Amt.getValue(pnd_Ln));                                                                                            //Natural: ADD #DETAIL.#GROSS-AMT ( #LN ) TO #SUB.#GROSS-AMT
        pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Ln));                                                                    //Natural: ADD #DETAIL.#INV-ACCT-FDRL-TAX-AMT ( #LN ) TO #SUB.#INV-ACCT-FDRL-TAX-AMT
        pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_Ln));                                                                  //Natural: ADD #DETAIL.#INV-ACCT-STATE-TAX-AMT ( #LN ) TO #SUB.#INV-ACCT-STATE-TAX-AMT
        pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_Ln));                                                                  //Natural: ADD #DETAIL.#INV-ACCT-LOCAL-TAX-AMT ( #LN ) TO #SUB.#INV-ACCT-LOCAL-TAX-AMT
        pnd_Sub_Pnd_Inv_Acct_Dpi_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_Ln));                                                                              //Natural: ADD #DETAIL.#INV-ACCT-DPI-AMT ( #LN ) TO #SUB.#INV-ACCT-DPI-AMT
        pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Ln));                                                                  //Natural: ADD #DETAIL.#INV-ACCT-NET-PYMNT-AMT ( #LN ) TO #SUB.#INV-ACCT-NET-PYMNT-AMT
        pnd_Sub_Pnd_Rtb_Pymnt_Amt.nadd(pnd_Detail_Pnd_Rtb_Pymnt_Amt.getValue(pnd_Ln));                                                                                    //Natural: ADD #DETAIL.#RTB-PYMNT-AMT ( #LN ) TO #SUB.#RTB-PYMNT-AMT
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Print_Dte_Sub_Total() throws Exception                                                                                                               //Natural: PRINT-DTE-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Old_Pymnt_Check_Dte.equals(pnd_Curr_Dte)))                                                                                                      //Natural: IF #OLD-PYMNT-CHECK-DTE = #CURR-DTE
        {
            pnd_Dte_Txt.setValue("CURRENT DATE SUB-TOTALS:");                                                                                                             //Natural: MOVE 'CURRENT DATE SUB-TOTALS:' TO #DTE-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Dte_Txt.setValue("FUTURE DATE SUB-TOTALS:");                                                                                                              //Natural: MOVE 'FUTURE DATE SUB-TOTALS:' TO #DTE-TXT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                                 //Natural: IF NOT #INT-ROLLOVER
        {
            getReports().write(2, new TabSetting(10),pnd_Dte_Txt,NEWLINE,NEWLINE,new TabSetting(10),"GROSS:   ",pnd_Dte_Pnd_Gross_Amt, new ReportEditMask                 //Natural: WRITE ( R1 ) 010T #DTE-TXT // 010T 'GROSS:   ' #DTE.#GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'FED:' #DTE.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'ST:  ' #DTE.#INV-ACCT-STATE-TAX-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'LCL:' #DTE.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZ,ZZ9.99 ) / 010T 'ADDL INTRST:' #DTE.#INV-ACCT-DPI-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'NET:' #DTE.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'RTB:' #DTE.#RTB-PYMNT-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'NUMBER OF CHECKS:' #DTE.#QTY ( EM = ZZ,ZZ9 )
                ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"FED:",pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"ST:  ",pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"LCL:",pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),NEWLINE,new 
                TabSetting(10),"ADDL INTRST:",pnd_Dte_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"NET:",pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"RTB:",pnd_Dte_Pnd_Rtb_Pymnt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"NUMBER OF CHECKS:",pnd_Dte_Pnd_Qty, 
                new ReportEditMask ("ZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( R1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, new TabSetting(10),pnd_Dte_Txt,NEWLINE,NEWLINE,new TabSetting(10),"GROSS:   ",pnd_Dte_Pnd_Gross_Amt, new ReportEditMask                 //Natural: WRITE ( R2 ) 010T #DTE-TXT // 010T 'GROSS:   ' #DTE.#GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'FED:' #DTE.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'ST:  ' #DTE.#INV-ACCT-STATE-TAX-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'LCL:' #DTE.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZ,ZZ9.99 ) / 010T 'ADDL INTRST:' #DTE.#INV-ACCT-DPI-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'NET:' #DTE.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'RTB:' #DTE.#RTB-PYMNT-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 003X 'NUMBER OF PYMNTS:' #DTE.#QTY ( EM = ZZ,ZZ9 )
                ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"FED:",pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"ST:  ",pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"LCL:",pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),NEWLINE,new 
                TabSetting(10),"ADDL INTRST:",pnd_Dte_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"NET:",pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"RTB:",pnd_Dte_Pnd_Rtb_Pymnt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(3),"NUMBER OF PYMNTS:",pnd_Dte_Pnd_Qty, 
                new ReportEditMask ("ZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( R2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dte.reset();                                                                                                                                                  //Natural: RESET #DTE
    }
    private void sub_Print_Sub_Total() throws Exception                                                                                                                   //Natural: PRINT-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        if (condition(! (pnd_Month_End.getBoolean())))                                                                                                                    //Natural: IF NOT #MONTH-END
        {
            pnd_Sub_Prt.setValue(true);                                                                                                                                   //Natural: ASSIGN #SUB-PRT = TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sub_Txt.setValue(DbsUtil.compress(pnd_Sub_Txt, "GRAND TOTALS:"));                                                                                             //Natural: COMPRESS #SUB-TXT 'GRAND TOTALS:' INTO #SUB-TXT
        pnd_Sub_Txt.setValue(pnd_Sub_Txt, MoveOption.RightJustified);                                                                                                     //Natural: MOVE RIGHT #SUB-TXT TO #SUB-TXT
        if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                                 //Natural: IF NOT #INT-ROLLOVER
        {
            if (condition(getReports().getAstLinesLeft(0).less(4)))                                                                                                       //Natural: NEWPAGE ( R1 ) IF LESS THAN 4 LINES LEFT
            {
                getReports().newPage(0);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(2, NEWLINE,new TabSetting(20),pnd_Sub_Pnd_Gross_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new TabSetting(34),pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt,  //Natural: WRITE ( R1 ) / 020T #SUB.#GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 034T #SUB.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 048T #SUB.#INV-ACCT-STATE-TAX-AMT ( EM = Z,ZZZ,ZZ9.99 ) 061T #SUB.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZ,ZZ9.99 ) 072T #SUB.#INV-ACCT-DPI-AMT ( EM = ZZZ,ZZ9.99 ) 119T #SUB.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) / 085T 'RTB:' #DTE.#RTB-PYMNT-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 108T 'NUMBER OF CHECKS:' #SUB.#QTY ( EM = ZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new TabSetting(48),pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(61),pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt, 
                new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(72),pnd_Sub_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(119),pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(85),"RTB:",pnd_Dte_Pnd_Rtb_Pymnt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new 
                TabSetting(108),"NUMBER OF CHECKS:",pnd_Sub_Pnd_Qty, new ReportEditMask ("ZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(getReports().getAstLinesLeft(0).less(4)))                                                                                                       //Natural: NEWPAGE ( R2 ) IF LESS THAN 4 LINES LEFT
            {
                getReports().newPage(0);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(3, NEWLINE,new TabSetting(20),pnd_Sub_Pnd_Gross_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new TabSetting(34),pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt,  //Natural: WRITE ( R2 ) / 020T #SUB.#GROSS-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 034T #SUB.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 048T #SUB.#INV-ACCT-STATE-TAX-AMT ( EM = Z,ZZZ,ZZ9.99 ) 061T #SUB.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZ,ZZ9.99 ) 072T #SUB.#INV-ACCT-DPI-AMT ( EM = ZZZ,ZZ9.99 ) 119T #SUB.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) / 085T 'RTB:' #DTE.#RTB-PYMNT-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 108T 'NUMBER OF PYMNTS:' #SUB.#QTY ( EM = ZZ,ZZ9 )
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new TabSetting(48),pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(61),pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt, 
                new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(72),pnd_Sub_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(119),pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(85),"RTB:",pnd_Dte_Pnd_Rtb_Pymnt_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new 
                TabSetting(108),"NUMBER OF PYMNTS:",pnd_Sub_Pnd_Qty, new ReportEditMask ("ZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sub.reset();                                                                                                                                                  //Natural: RESET #SUB
    }
    private void sub_Print_Detail_Header() throws Exception                                                                                                               //Natural: PRINT-DETAIL-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                                 //Natural: IF NOT #INT-ROLLOVER
        {
            getReports().write(2, new TabSetting(4),"IA",new TabSetting(32),"GROSS",new TabSetting(44),"FEDERAL",new TabSetting(59),"STATE",new TabSetting(70),"LOCAL",new  //Natural: WRITE ( R1 ) 004T 'IA' 032T 'GROSS' 044T 'FEDERAL' 059T 'STATE' 070T 'LOCAL' 076T 'ADDITIONAL' 090T 'DUE' 129T 'NET' / 001T 'CONTRACT' 031T 'AMOUNT' 048T 'TAX' 061T 'TAX' 072T 'TAX' 078T 'INTEREST' 089T 'DATE' 125T 'PAYMENT'
                TabSetting(76),"ADDITIONAL",new TabSetting(90),"DUE",new TabSetting(129),"NET",NEWLINE,new TabSetting(1),"CONTRACT",new TabSetting(31),"AMOUNT",new 
                TabSetting(48),"TAX",new TabSetting(61),"TAX",new TabSetting(72),"TAX",new TabSetting(78),"INTEREST",new TabSetting(89),"DATE",new TabSetting(125),
                "PAYMENT");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, new TabSetting(4),"IA",new TabSetting(32),"GROSS",new TabSetting(44),"FEDERAL",new TabSetting(59),"STATE",new TabSetting(70),"LOCAL",new  //Natural: WRITE ( R2 ) 004T 'IA' 032T 'GROSS' 044T 'FEDERAL' 059T 'STATE' 070T 'LOCAL' 076T 'ADDITIONAL' 090T 'DUE' 129T 'NET' / 001T 'CONTRACT' 031T 'AMOUNT' 048T 'TAX' 061T 'TAX' 072T 'TAX' 078T 'INTEREST' 089T 'DATE' 125T 'PAYMENT'
                TabSetting(76),"ADDITIONAL",new TabSetting(90),"DUE",new TabSetting(129),"NET",NEWLINE,new TabSetting(1),"CONTRACT",new TabSetting(31),"AMOUNT",new 
                TabSetting(48),"TAX",new TabSetting(61),"TAX",new TabSetting(72),"TAX",new TabSetting(78),"INTEREST",new TabSetting(89),"DATE",new TabSetting(125),
                "PAYMENT");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Array_Index() throws Exception                                                                                                             //Natural: DETERMINE-ARRAY-INDEX
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  INT ROLLOVER        /*
        if (condition(pnd_Int_Rollover.getBoolean()))                                                                                                                     //Natural: IF #INT-ROLLOVER
        {
            pnd_T1.setValue(5);                                                                                                                                           //Natural: ASSIGN #T1 = 5
            //*  1=CHK, 2=EFT, 3=GLOBAL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_T1.setValue(pdaFcpaext2.getExt2_Pymnt_Pay_Type_Req_Ind());                                                                                                //Natural: ASSIGN #T1 = EXT2.PYMNT-PAY-TYPE-REQ-IND
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1484 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE EXT2.CNTRCT-ORGN-CDE;//Natural: VALUE 'NZ'
        if (condition((pdaFcpaext2.getExt2_Cntrct_Orgn_Cde().equals("NZ"))))
        {
            decideConditionsMet1484++;
            if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                             //Natural: IF NOT #INT-ROLLOVER
            {
                short decideConditionsMet1487 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE SORT-OPTION-CDE-X;//Natural: VALUE 'P&I'
                if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("P&I"))))
                {
                    decideConditionsMet1487++;
                    pnd_T2.setValue(2);                                                                                                                                   //Natural: ASSIGN #T2 = 2
                }                                                                                                                                                         //Natural: VALUE 'IPRO'
                else if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("IPRO"))))
                {
                    decideConditionsMet1487++;
                    pnd_T2.setValue(3);                                                                                                                                   //Natural: ASSIGN #T2 = 3
                }                                                                                                                                                         //Natural: VALUE 'TPA'
                else if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("TPA"))))
                {
                    decideConditionsMet1487++;
                    //*  REGULAR
                    pnd_T2.setValue(4);                                                                                                                                   //Natural: ASSIGN #T2 = 4
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_T2.setValue(1);                                                                                                                                   //Natural: ASSIGN #T2 = 1
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  INT. ROLLOVER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet1500 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE SORT-OPTION-CDE-X;//Natural: VALUE 'P&I'
                if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("P&I"))))
                {
                    decideConditionsMet1500++;
                    pnd_T2.setValue(8);                                                                                                                                   //Natural: ASSIGN #T2 = 8
                }                                                                                                                                                         //Natural: VALUE 'IPRO'
                else if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("IPRO"))))
                {
                    decideConditionsMet1500++;
                    pnd_T2.setValue(9);                                                                                                                                   //Natural: ASSIGN #T2 = 9
                }                                                                                                                                                         //Natural: VALUE 'TPA'
                else if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde_X().equals("TPA"))))
                {
                    decideConditionsMet1500++;
                    pnd_T2.setValue(10);                                                                                                                                  //Natural: ASSIGN #T2 = 10
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    short decideConditionsMet1508 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE SORT-OPTION-CDE;//Natural: VALUE 'IRLC'
                    if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("IRLC"))))
                    {
                        decideConditionsMet1508++;
                        pnd_T2.setValue(5);                                                                                                                               //Natural: ASSIGN #T2 = 5
                    }                                                                                                                                                     //Natural: VALUE 'IRLR'
                    else if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("IRLR"))))
                    {
                        decideConditionsMet1508++;
                        pnd_T2.setValue(6);                                                                                                                               //Natural: ASSIGN #T2 = 6
                    }                                                                                                                                                     //Natural: VALUE 'IRLX'
                    else if (condition((pdaFcpaext2.getSort_Key_Sort_Option_Cde().equals("IRLX"))))
                    {
                        decideConditionsMet1508++;
                        pnd_T2.setValue(7);                                                                                                                               //Natural: ASSIGN #T2 = 7
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pdaFcpaext2.getExt2_Cntrct_Orgn_Cde().equals("AL"))))
        {
            decideConditionsMet1484++;
            pnd_T2.setValue(1);                                                                                                                                           //Natural: ASSIGN #T2 = 1
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Detail_Lines() throws Exception                                                                                                                //Natural: PRINT-DETAIL-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                                 //Natural: IF NOT #INT-ROLLOVER
        {
            if (condition(getReports().getAstLineCount(2).add(pnd_Ln).add(9).greater(Global.getPAGESIZE())))                                                              //Natural: IF *LINE-COUNT ( R1 ) + #LN + 9 GT *PAGESIZE
            {
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( R1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, new TabSetting(1),pnd_Misc_Cntrct_Ppcn_Nbr,pnd_Misc_Pnd_Annt);                                                                          //Natural: WRITE ( R1 ) 1T #MISC.CNTRCT-PPCN-NBR #MISC.#ANNT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(getReports().getAstLineCount(3).add(pnd_Ln).add(9).greater(Global.getPAGESIZE())))                                                              //Natural: IF *LINE-COUNT ( R2 ) + #LN + 9 GT *PAGESIZE
            {
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( R2 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(3, new TabSetting(1),pnd_Misc_Cntrct_Ppcn_Nbr,pnd_Misc_Pnd_Annt);                                                                          //Natural: WRITE ( R2 ) 1T #MISC.CNTRCT-PPCN-NBR #MISC.#ANNT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #LN
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ln)); pnd_I.nadd(1))
        {
            if (condition(pnd_Detail_Pnd_Gross_Amt.getValue(pnd_I).greater(getZero())))                                                                                   //Natural: IF #DETAIL.#GROSS-AMT ( #I ) GT 0
            {
                //*  /*
                //*  /*
                //*  /*
                if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                         //Natural: IF NOT #INT-ROLLOVER
                {
                    getReports().write(2, new TabSetting(6),pnd_Detail_Pnd_Rtb_Ind.getValue(pnd_I),new TabSetting(12),pnd_Detail_Pnd_Fund_Desc.getValue(pnd_I),new        //Natural: WRITE ( R1 ) 006T #DETAIL.#RTB-IND ( #I ) 012T #DETAIL.#FUND-DESC ( #I ) 021T #DETAIL.#INV-ACCT-VALUAT-PERIOD ( #I ) 025T #DETAIL.#GROSS-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99- ) 041T #DETAIL.#INV-ACCT-FDRL-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99- ) 054T #DETAIL.#INV-ACCT-STATE-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99- ) 066T #DETAIL.#INV-ACCT-LOCAL-TAX-AMT ( #I ) ( EM = ZZ,ZZ9.99- ) 077T #DETAIL.#INV-ACCT-DPI-AMT ( #I ) ( EM = ZZ,ZZ9.99- ) 088T #DETAIL.#PYMNT-SETTLMNT-DTE ( #I ) 120T #DETAIL.#INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99- )
                        TabSetting(21),pnd_Detail_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_I),new TabSetting(25),pnd_Detail_Pnd_Gross_Amt.getValue(pnd_I), 
                        new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(41),pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_I), new ReportEditMask 
                        ("ZZZ,ZZ9.99-"),new TabSetting(54),pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZ9.99-"),new 
                        TabSetting(66),pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZ9.99-"),new TabSetting(77),pnd_Detail_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_I), 
                        new ReportEditMask ("ZZ,ZZ9.99-"),new TabSetting(88),pnd_Detail_Pnd_Pymnt_Settlmnt_Dte.getValue(pnd_I),new TabSetting(120),pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_I), 
                        new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  /*
                    //*  /*
                    //*  /*
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(3, new TabSetting(6),pnd_Detail_Pnd_Rtb_Ind.getValue(pnd_I),new TabSetting(12),pnd_Detail_Pnd_Fund_Desc.getValue(pnd_I),new        //Natural: WRITE ( R2 ) 006T #DETAIL.#RTB-IND ( #I ) 012T #DETAIL.#FUND-DESC ( #I ) 021T #DETAIL.#INV-ACCT-VALUAT-PERIOD ( #I ) 025T #DETAIL.#GROSS-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99- ) 041T #DETAIL.#INV-ACCT-FDRL-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99- ) 054T #DETAIL.#INV-ACCT-STATE-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99- ) 066T #DETAIL.#INV-ACCT-LOCAL-TAX-AMT ( #I ) ( EM = ZZ,ZZ9.99- ) 077T #DETAIL.#INV-ACCT-DPI-AMT ( #I ) ( EM = ZZ,ZZ9.99- ) 088T #DETAIL.#PYMNT-SETTLMNT-DTE ( #I ) 120T #DETAIL.#INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99- )
                        TabSetting(21),pnd_Detail_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_I),new TabSetting(25),pnd_Detail_Pnd_Gross_Amt.getValue(pnd_I), 
                        new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(41),pnd_Detail_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_I), new ReportEditMask 
                        ("ZZZ,ZZ9.99-"),new TabSetting(54),pnd_Detail_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZ9.99-"),new 
                        TabSetting(66),pnd_Detail_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZ9.99-"),new TabSetting(77),pnd_Detail_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_I), 
                        new ReportEditMask ("ZZ,ZZ9.99-"),new TabSetting(88),pnd_Detail_Pnd_Pymnt_Settlmnt_Dte.getValue(pnd_I),new TabSetting(120),pnd_Detail_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_I), 
                        new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                                 //Natural: IF NOT #INT-ROLLOVER
        {
            if (condition(pnd_Dvdnd.getBoolean()))                                                                                                                        //Natural: IF #DVDND
            {
                if (condition(pnd_Misc_Pnd_Inv_Acct_Cntrct_Amt.notEquals(getZero()) || pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt.notEquals(getZero())))                             //Natural: IF #MISC.#INV-ACCT-CNTRCT-AMT NE 0 OR #MISC.#INV-ACCT-DVDND-AMT NE 0
                {
                    getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"),new TabSetting(74),pnd_Guar_Unit_Txt,new            //Natural: WRITE ( R1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * ) 074T #GUAR-UNIT-TXT 090T #MISC.#INV-ACCT-CNTRCT-AMT ( EM = -ZZZZ,ZZ9.99 ) 104T #DIV-UNIT-TXT 116T #MISC.#INV-ACCT-DVDND-AMT ( EM = -ZZZZ,ZZ9.99 )
                        TabSetting(90),pnd_Misc_Pnd_Inv_Acct_Cntrct_Amt, new ReportEditMask ("-ZZZZ,ZZ9.99"),new TabSetting(104),pnd_Div_Unit_Txt,new TabSetting(116),pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt, 
                        new ReportEditMask ("-ZZZZ,ZZ9.99"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                   //Natural: WRITE ( R1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                       //Natural: WRITE ( R1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Dvdnd.getBoolean()))                                                                                                                        //Natural: IF #DVDND
            {
                if (condition(pnd_Misc_Pnd_Inv_Acct_Cntrct_Amt.notEquals(getZero()) || pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt.notEquals(getZero())))                             //Natural: IF #MISC.#INV-ACCT-CNTRCT-AMT NE 0 OR #MISC.#INV-ACCT-DVDND-AMT NE 0
                {
                    getReports().write(3, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"),new TabSetting(74),pnd_Guar_Unit_Txt,new            //Natural: WRITE ( R2 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * ) 074T #GUAR-UNIT-TXT 090T #MISC.#INV-ACCT-CNTRCT-AMT ( EM = -ZZZZ,ZZ9.99 ) 104T #DIV-UNIT-TXT 116T #MISC.#INV-ACCT-DVDND-AMT ( EM = -ZZZZ,ZZ9.99 )
                        TabSetting(90),pnd_Misc_Pnd_Inv_Acct_Cntrct_Amt, new ReportEditMask ("-ZZZZ,ZZ9.99"),new TabSetting(104),pnd_Div_Unit_Txt,new TabSetting(116),pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt, 
                        new ReportEditMask ("-ZZZZ,ZZ9.99"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(3, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                   //Natural: WRITE ( R2 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(3, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                       //Natural: WRITE ( R2 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                                 //Natural: IF NOT #INT-ROLLOVER
        {
            getReports().write(2, "SEQ NO:",pnd_Misc_Pymnt_Check_Seq_Nbr, new ReportEditMask ("9999999"),new ColumnSpacing(2),"PAYMENT DATE:",pnd_Misc_Pymnt_Check_Dte,   //Natural: WRITE ( R1 ) 'SEQ NO:' #MISC.PYMNT-CHECK-SEQ-NBR ( EM = 9999999 ) 2X 'PAYMENT DATE:' #MISC.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) 2X #CHECK-NUMBER-A10 2X 'PAYEE:' #MISC.#PAYEE 'HOLD:' #MISC.CNTRCT-HOLD-CDE / 021T #ITEM.#GROSS-AMT ( EM = Z,ZZZ,ZZ9.99- ) 037T #ITEM.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZZ,ZZ9.99- ) 050T #ITEM.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZ,ZZ9.99- ) 062T #ITEM.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZ,ZZ9.99- ) 073T #ITEM.#INV-ACCT-DPI-AMT ( EM = ZZ,ZZ9.99- ) 120T #ITEM.#INV-ACCT-NET-PYMNT-AMT ( EM = Z,ZZZ,ZZ9.99- ) / '-' ( 132 )
                new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(2),pnd_Check_Number_N10_Pnd_Check_Number_A10,new ColumnSpacing(2),"PAYEE:",pnd_Misc_Pnd_Payee,"HOLD:",pnd_Misc_Cntrct_Hold_Cde,NEWLINE,new 
                TabSetting(21),pnd_Item_Pnd_Gross_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(37),pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask 
                ("ZZZ,ZZ9.99-"),new TabSetting(50),pnd_Item_Pnd_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-"),new TabSetting(62),pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt, 
                new ReportEditMask ("ZZ,ZZ9.99-"),new TabSetting(73),pnd_Item_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZ,ZZ9.99-"),new TabSetting(120),pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt, 
                new ReportEditMask ("Z,ZZZ,ZZ9.99-"),NEWLINE,"-",new RepeatItem(132));
            if (Global.isEscape()) return;
            //*  05-18-98
            if (condition(! (pnd_Reg_Pymnt_Printed.getBoolean())))                                                                                                        //Natural: IF NOT #REG-PYMNT-PRINTED
            {
                pnd_Reg_Pymnt_Printed.setValue(true);                                                                                                                     //Natural: ASSIGN #REG-PYMNT-PRINTED = TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  RL PAYEE MATCH
            //*  RL PAYEE MATCH
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, "SEQ NO:",pnd_Misc_Pymnt_Check_Seq_Nbr, new ReportEditMask ("9999999"),new ColumnSpacing(2),"PAYMENT DATE:",pnd_Misc_Pymnt_Check_Dte,   //Natural: WRITE ( R2 ) 'SEQ NO:' #MISC.PYMNT-CHECK-SEQ-NBR ( EM = 9999999 ) 2X 'PAYMENT DATE:' #MISC.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) 2X #CHECK-NUMBER-A10 2X 'PAYEE:' #MISC.#PAYEE 'HOLD:' #MISC.CNTRCT-HOLD-CDE / 021T #ITEM.#GROSS-AMT ( EM = Z,ZZZ,ZZ9.99- ) 037T #ITEM.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZZ,ZZ9.99- ) 050T #ITEM.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZ,ZZ9.99- ) 062T #ITEM.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZ,ZZ9.99- ) 073T #ITEM.#INV-ACCT-DPI-AMT ( EM = ZZ,ZZ9.99- ) 120T #ITEM.#INV-ACCT-NET-PYMNT-AMT ( EM = Z,ZZZ,ZZ9.99- ) / '-' ( 132 )
                new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(2),pnd_Check_Number_N10_Pnd_Check_Number_A10,new ColumnSpacing(2),"PAYEE:",pnd_Misc_Pnd_Payee,"HOLD:",pnd_Misc_Cntrct_Hold_Cde,NEWLINE,new 
                TabSetting(21),pnd_Item_Pnd_Gross_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(37),pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask 
                ("ZZZ,ZZ9.99-"),new TabSetting(50),pnd_Item_Pnd_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-"),new TabSetting(62),pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt, 
                new ReportEditMask ("ZZ,ZZ9.99-"),new TabSetting(73),pnd_Item_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZ,ZZ9.99-"),new TabSetting(120),pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt, 
                new ReportEditMask ("Z,ZZZ,ZZ9.99-"),NEWLINE,"-",new RepeatItem(132));
            if (Global.isEscape()) return;
            //*  05-18-98
            if (condition(! (pnd_Int_Rollover_Printed.getBoolean())))                                                                                                     //Natural: IF NOT #INT-ROLLOVER-PRINTED
            {
                pnd_Int_Rollover_Printed.setValue(true);                                                                                                                  //Natural: ASSIGN #INT-ROLLOVER-PRINTED = TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Detail.getValue("*").reset();                                                                                                                                 //Natural: RESET #DETAIL ( * ) #MISC #ITEM #MISC.#CNTRCT-NBR ( * ) #LN #DTE-FIRST-PERIODIC
        pnd_Misc.reset();
        pnd_Item.reset();
        pnd_Misc_Pnd_Cntrct_Nbr.getValue("*").reset();
        pnd_Ln.reset();
        pnd_Dte_First_Periodic.reset();
    }
    private void sub_Build_Summary_Data() throws Exception                                                                                                                //Natural: BUILD-SUMMARY-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaFcpaext2.getExt2_Pymnt_Ftre_Ind().equals("F")))                                                                                                  //Natural: IF EXT2.PYMNT-FTRE-IND = 'F'
        {
            if (condition(pnd_Ln.equals(1)))                                                                                                                              //Natural: IF #LN = 1
            {
                pnd_Sum_Pnd_F_Qty.getValue(pnd_T1,pnd_T2).nadd(1);                                                                                                        //Natural: ADD 1 TO #SUM.#F-QTY ( #T1,#T2 )
                pnd_G_Tot_Pnd_F_Qty.getValue(pnd_T1,pnd_T2).nadd(1);                                                                                                      //Natural: ADD 1 TO #G-TOT.#F-QTY ( #T1,#T2 )
                if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                         //Natural: IF NOT #INT-ROLLOVER
                {
                    pnd_Sum_Pnd_F_Qty.getValue(4,pnd_T2).nadd(1);                                                                                                         //Natural: ADD 1 TO #SUM.#F-QTY ( 4,#T2 )
                    pnd_G_Tot_Pnd_F_Qty.getValue(4,pnd_T2).nadd(1);                                                                                                       //Natural: ADD 1 TO #G-TOT.#F-QTY ( 4,#T2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext2.getExt2_Inv_Acct_Valuat_Period().getValue(pnd_I).equals("M")))                                                                      //Natural: IF EXT2.INV-ACCT-VALUAT-PERIOD ( #I ) = 'M'
            {
                pnd_Sum_Pnd_Future_M.getValue(pnd_T1,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                          //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #SUM.#FUTURE-M ( #T1,#T2 )
                pnd_G_Tot_Pnd_Future_M.getValue(pnd_T1,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                        //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #G-TOT.#FUTURE-M ( #T1,#T2 )
                if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                         //Natural: IF NOT #INT-ROLLOVER
                {
                    pnd_Sum_Pnd_Future_M.getValue(4,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                           //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #SUM.#FUTURE-M ( 4,#T2 )
                    pnd_G_Tot_Pnd_Future_M.getValue(4,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                         //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #G-TOT.#FUTURE-M ( 4,#T2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sum_Pnd_Future_A.getValue(pnd_T1,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                          //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #SUM.#FUTURE-A ( #T1,#T2 )
                pnd_G_Tot_Pnd_Future_A.getValue(pnd_T1,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                        //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #G-TOT.#FUTURE-A ( #T1,#T2 )
                if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                         //Natural: IF NOT #INT-ROLLOVER
                {
                    pnd_Sum_Pnd_Future_A.getValue(4,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                           //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #SUM.#FUTURE-A ( 4,#T2 )
                    pnd_G_Tot_Pnd_Future_A.getValue(4,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                         //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #G-TOT.#FUTURE-A ( 4,#T2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ln.equals(1)))                                                                                                                              //Natural: IF #LN = 1
            {
                pnd_Sum_Pnd_C_Qty.getValue(pnd_T1,pnd_T2).nadd(1);                                                                                                        //Natural: ADD 1 TO #SUM.#C-QTY ( #T1,#T2 )
                pnd_G_Tot_Pnd_C_Qty.getValue(pnd_T1,pnd_T2).nadd(1);                                                                                                      //Natural: ADD 1 TO #G-TOT.#C-QTY ( #T1,#T2 )
                if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                         //Natural: IF NOT #INT-ROLLOVER
                {
                    pnd_Sum_Pnd_C_Qty.getValue(4,pnd_T2).nadd(1);                                                                                                         //Natural: ADD 1 TO #SUM.#C-QTY ( 4,#T2 )
                    pnd_G_Tot_Pnd_C_Qty.getValue(4,pnd_T2).nadd(1);                                                                                                       //Natural: ADD 1 TO #G-TOT.#C-QTY ( 4,#T2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext2.getExt2_Inv_Acct_Valuat_Period().getValue(pnd_I).equals("M")))                                                                      //Natural: IF EXT2.INV-ACCT-VALUAT-PERIOD ( #I ) = 'M'
            {
                pnd_Sum_Pnd_Current_M.getValue(pnd_T1,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                         //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #SUM.#CURRENT-M ( #T1,#T2 )
                pnd_G_Tot_Pnd_Current_M.getValue(pnd_T1,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                       //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #G-TOT.#CURRENT-M ( #T1,#T2 )
                if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                         //Natural: IF NOT #INT-ROLLOVER
                {
                    pnd_Sum_Pnd_Current_M.getValue(4,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                          //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #SUM.#CURRENT-M ( 4,#T2 )
                    pnd_G_Tot_Pnd_Current_M.getValue(4,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                        //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #G-TOT.#CURRENT-M ( 4,#T2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sum_Pnd_Current_A.getValue(pnd_T1,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                         //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #SUM.#CURRENT-A ( #T1,#T2 )
                pnd_G_Tot_Pnd_Current_A.getValue(pnd_T1,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                       //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #G-TOT.#CURRENT-A ( #T1,#T2 )
                if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                         //Natural: IF NOT #INT-ROLLOVER
                {
                    pnd_Sum_Pnd_Current_A.getValue(4,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                          //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #SUM.#CURRENT-A ( 4,#T2 )
                    pnd_G_Tot_Pnd_Current_A.getValue(4,pnd_T2).nadd(pdaFcpaext2.getExt2_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                        //Natural: ADD EXT2.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #G-TOT.#CURRENT-A ( 4,#T2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Summary() throws Exception                                                                                                                     //Natural: PRINT-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Header1.equals(" ")))                                                                                                                           //Natural: IF #HEADER1 = ' '
        {
                                                                                                                                                                          //Natural: PERFORM SET-UP-HEADER-2
            sub_Set_Up_Header_2();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  AMC 11-26-01
        pnd_Cf_Txt.reset();                                                                                                                                               //Natural: RESET #CF-TXT
        if (condition(! (pnd_Int_Rollover.getBoolean())))                                                                                                                 //Natural: IF NOT #INT-ROLLOVER
        {
            FOR03:                                                                                                                                                        //Natural: FOR #I1 = 1 TO 4
            for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(4)); pnd_I1.nadd(1))
            {
                short decideConditionsMet1662 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #I1;//Natural: VALUE 1
                if (condition((pnd_I1.equals(1))))
                {
                    decideConditionsMet1662++;
                    pnd_Header4.setValue("CHECK SUMMARY TOTALS");                                                                                                         //Natural: ASSIGN #HEADER4 := 'CHECK SUMMARY TOTALS'
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_I1.equals(2))))
                {
                    decideConditionsMet1662++;
                    pnd_Header4.setValue("EFT SUMMARY TOTALS");                                                                                                           //Natural: ASSIGN #HEADER4 := 'EFT SUMMARY TOTALS'
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_I1.equals(3))))
                {
                    decideConditionsMet1662++;
                    pnd_Header4.setValue("GLOBAL SUMMARY TOTALS");                                                                                                        //Natural: ASSIGN #HEADER4 := 'GLOBAL SUMMARY TOTALS'
                }                                                                                                                                                         //Natural: VALUE 4
                else if (condition((pnd_I1.equals(4))))
                {
                    decideConditionsMet1662++;
                    pnd_Header4.setValue("SUMMARY TOTALS");                                                                                                               //Natural: ASSIGN #HEADER4 := 'SUMMARY TOTALS'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Use_Prev_Sort_Key_Val.setValue(true);                                                                                                                 //Natural: ASSIGN #USE-PREV-SORT-KEY-VAL = TRUE
                                                                                                                                                                          //Natural: PERFORM INIT-TXT-FOR-PRINT-SUMMARY
                sub_Init_Txt_For_Print_Summary();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER4
                sub_Center_Header4();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( R1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, NEWLINE,NEWLINE,new TabSetting(48),"1ST PAYMENT",NEWLINE,new TabSetting(52),"CURRENT",new TabSetting(68),"FUTURE",new               //Natural: WRITE ( R1 ) / / 048T '1ST PAYMENT' / 052T 'CURRENT' 068T 'FUTURE' 086T 'TOTAL' /
                    TabSetting(86),"TOTAL",NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PREV-INDEX
                sub_Determine_Prev_Index();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                pnd_Totals_Pnd_T_Total_A.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total_A), pnd_Sum_Pnd_Current_A.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_Future_A.getValue(pnd_I1, //Natural: COMPUTE #T-TOTAL-A = #SUM.#CURRENT-A ( #I1,#J2 ) + #SUM.#FUTURE-A ( #I1,#J2 )
                    pnd_J2)));
                pnd_Totals_Pnd_T_Total_M.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total_M), pnd_Sum_Pnd_Current_M.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_Future_M.getValue(pnd_I1, //Natural: COMPUTE #T-TOTAL-M = #SUM.#CURRENT-M ( #I1,#J2 ) + #SUM.#FUTURE-M ( #I1,#J2 )
                    pnd_J2)));
                getReports().write(2, pnd_Sub_Txt_A, new AlphanumericLength (33),new TabSetting(45),pnd_Sum_Pnd_Current_A.getValue(pnd_I1,pnd_J2), new                    //Natural: WRITE ( R1 ) #SUB-TXT-A ( AL = 33 ) 045T #SUM.#CURRENT-A ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #SUM.#FUTURE-A ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL-A ( EM = ZZZ,ZZZ,ZZ9.99 )
                    ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_Sum_Pnd_Future_A.getValue(pnd_I1,pnd_J2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(77),pnd_Totals_Pnd_T_Total_A, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(! (pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("TPA") || pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("IPRO")                    //Natural: IF NOT #OLD-SORT-OPTION-CDE-X = 'TPA' OR = 'IPRO' OR = 'P&I'
                    || pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("P&I"))))
                {
                    getReports().write(2, pnd_Sub_Txt_M, new AlphanumericLength (33),new TabSetting(45),pnd_Sum_Pnd_Current_M.getValue(pnd_I1,pnd_J2),                    //Natural: WRITE ( R1 ) #SUB-TXT-M ( AL = 33 ) 045T #SUM.#CURRENT-M ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #SUM.#FUTURE-M ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL-M ( EM = ZZZ,ZZZ,ZZ9.99 )
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_Sum_Pnd_Future_M.getValue(pnd_I1,pnd_J2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                        TabSetting(77),pnd_Totals_Pnd_T_Total_M, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Totals_Pnd_C_Total.reset();                                                                                                                           //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
                pnd_Totals_Pnd_F_Total.reset();
                pnd_Totals_Pnd_T_Total.reset();
                pnd_Totals_Pnd_Cqty.reset();
                pnd_Totals_Pnd_Fqty.reset();
                pnd_Totals_Pnd_Tqty.reset();
                pnd_Totals_Pnd_C_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_C_Total), pnd_Sum_Pnd_Current_A.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_Current_M.getValue(pnd_I1, //Natural: COMPUTE #C-TOTAL = #SUM.#CURRENT-A ( #I1,#J2 ) + #SUM.#CURRENT-M ( #I1,#J2 )
                    pnd_J2)));
                pnd_Totals_Pnd_F_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_F_Total), pnd_Sum_Pnd_Future_A.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_Future_M.getValue(pnd_I1, //Natural: COMPUTE #F-TOTAL = #SUM.#FUTURE-A ( #I1,#J2 ) + #SUM.#FUTURE-M ( #I1,#J2 )
                    pnd_J2)));
                pnd_Totals_Pnd_T_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total), pnd_Totals_Pnd_T_Total_A.add(pnd_Totals_Pnd_T_Total_M));             //Natural: COMPUTE #T-TOTAL = #T-TOTAL-A + #T-TOTAL-M
                pnd_Totals_Pnd_Cqty.setValue(pnd_Sum_Pnd_C_Qty.getValue(pnd_I1,pnd_J2));                                                                                  //Natural: ASSIGN #CQTY = #SUM.#C-QTY ( #I1,#J2 )
                pnd_Totals_Pnd_Fqty.setValue(pnd_Sum_Pnd_F_Qty.getValue(pnd_I1,pnd_J2));                                                                                  //Natural: ASSIGN #FQTY = #SUM.#F-QTY ( #I1,#J2 )
                pnd_Totals_Pnd_Tqty.compute(new ComputeParameters(false, pnd_Totals_Pnd_Tqty), pnd_Sum_Pnd_C_Qty.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_F_Qty.getValue(pnd_I1, //Natural: COMPUTE #TQTY = #SUM.#C-QTY ( #I1,#J2 ) + #SUM.#F-QTY ( #I1,#J2 )
                    pnd_J2)));
                getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(45),pnd_Totals_Pnd_C_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new                //Natural: WRITE ( R1 ) / / / 'TOTAL' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #F-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
                    TabSetting(60),pnd_Totals_Pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_T_Total, new ReportEditMask 
                    ("ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, new TabSetting(50),pnd_Totals_Pnd_Cqty, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(65),pnd_Totals_Pnd_Fqty, new                   //Natural: WRITE ( R1 ) 050T #CQTY ( EM = ZZ,ZZ9 ) 065T #FQTY ( EM = ZZ,ZZ9 ) 082T #TQTY ( EM = ZZ,ZZ9 ) /
                    ReportEditMask ("ZZ,ZZ9"),new TabSetting(82),pnd_Totals_Pnd_Tqty, new ReportEditMask ("ZZ,ZZ9"),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* ** START OF CODE ADDED BY AMC 11-19-2001 & 12-21-2001
                if (condition(pnd_I1.equals(4)))                                                                                                                          //Natural: IF #I1 EQ 4
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Totals_Pnd_End_Dtrac.getValue(pnd_I1).nadd(pnd_Totals_Pnd_Cdtra_Total.getValue(pnd_I1));                                                          //Natural: COMPUTE #END-DTRAC ( #I1 ) = #END-DTRAC ( #I1 ) + #CDTRA-TOTAL ( #I1 )
                    pnd_Totals_Pnd_End_Dtraf.getValue(pnd_I1).nadd(pnd_Totals_Pnd_Fdtra_Total.getValue(pnd_I1));                                                          //Natural: COMPUTE #END-DTRAF ( #I1 ) = #END-DTRAF ( #I1 ) + #FDTRA-TOTAL ( #I1 )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Totals_Pnd_Tdtra_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_Tdtra_Total), pnd_Totals_Pnd_Cdtra_Total.getValue(pnd_I1).add(pnd_Totals_Pnd_Fdtra_Total.getValue(pnd_I1))); //Natural: COMPUTE #TDTRA-TOTAL = #CDTRA-TOTAL ( #I1 ) + #FDTRA-TOTAL ( #I1 )
                getReports().write(2, NEWLINE,"TPA-DTRA TOTAL",new TabSetting(45),pnd_Totals_Pnd_Cdtra_Total.getValue(pnd_I1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( R1 ) / 'TPA-DTRA TOTAL' 045T #CDTRA-TOTAL ( #I1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #FDTRA-TOTAL ( #I1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #TDTRA-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
                    TabSetting(60),pnd_Totals_Pnd_Fdtra_Total.getValue(pnd_I1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_Tdtra_Total, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Totals_Pnd_Tdtra_Total.reset();                                                                                                                       //Natural: RESET #TDTRA-TOTAL
                pnd_Totals_Pnd_Cdtra_Total.getValue(4).nadd(pnd_Totals_Pnd_Cdtra_Total.getValue(pnd_I1));                                                                 //Natural: COMPUTE #CDTRA-TOTAL ( 4 ) = #CDTRA-TOTAL ( 4 ) + #CDTRA-TOTAL ( #I1 )
                pnd_Totals_Pnd_Fdtra_Total.getValue(4).nadd(pnd_Totals_Pnd_Fdtra_Total.getValue(pnd_I1));                                                                 //Natural: COMPUTE #FDTRA-TOTAL ( 4 ) = #FDTRA-TOTAL ( 4 ) + #FDTRA-TOTAL ( #I1 )
                //*  ROXAN
                pnd_Totals_Pnd_Fdtra_Total.getValue(pnd_I1).reset();                                                                                                      //Natural: RESET #FDTRA-TOTAL ( #I1 ) #CDTRA-TOTAL ( #I1 )
                pnd_Totals_Pnd_Cdtra_Total.getValue(pnd_I1).reset();
                //* ** END OF CODE ADDED BY AMC 11-19-2001
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            getReports().skip(0, 5);                                                                                                                                      //Natural: SKIP ( R1 ) 5
            getReports().write(2, new TabSetting(55),pnd_End_Of_Report_Lit);                                                                                              //Natural: WRITE ( R1 ) 55T #END-OF-REPORT-LIT
            if (Global.isEscape()) return;
            //*  PREVIOUS INTERNAL ROLLOVER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_I1.setValue(5);                                                                                                                                           //Natural: ASSIGN #I1 = 5
            pnd_Header4.setValue("INTERNAL ROLLOVER SUMMARY TOTALS");                                                                                                     //Natural: ASSIGN #HEADER4 = 'INTERNAL ROLLOVER SUMMARY TOTALS'
            pnd_Use_Prev_Sort_Key_Val.setValue(true);                                                                                                                     //Natural: ASSIGN #USE-PREV-SORT-KEY-VAL = TRUE
                                                                                                                                                                          //Natural: PERFORM INIT-TXT-FOR-PRINT-SUMMARY
            sub_Init_Txt_For_Print_Summary();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER4
            sub_Center_Header4();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( R2 )
            if (condition(Global.isEscape())){return;}
            getReports().write(3, NEWLINE,NEWLINE,new TabSetting(48),"1ST PAYMENT",NEWLINE,new TabSetting(52),"CURRENT",new TabSetting(68),"FUTURE",new                   //Natural: WRITE ( R2 ) / / 048T '1ST PAYMENT' / 052T 'CURRENT' 068T 'FUTURE' 086T 'TOTAL' /
                TabSetting(86),"TOTAL",NEWLINE);
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PREV-INDEX
            sub_Determine_Prev_Index();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Totals_Pnd_T_Total_A.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total_A), pnd_Sum_Pnd_Current_A.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_Future_A.getValue(pnd_I1, //Natural: COMPUTE #T-TOTAL-A = #SUM.#CURRENT-A ( #I1,#J2 ) + #SUM.#FUTURE-A ( #I1,#J2 )
                pnd_J2)));
            pnd_Totals_Pnd_T_Total_M.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total_M), pnd_Sum_Pnd_Current_M.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_Future_M.getValue(pnd_I1, //Natural: COMPUTE #T-TOTAL-M = #SUM.#CURRENT-M ( #I1,#J2 ) + #SUM.#FUTURE-M ( #I1,#J2 )
                pnd_J2)));
            getReports().write(3, pnd_Sub_Txt_A, new AlphanumericLength (33),new TabSetting(45),pnd_Sum_Pnd_Current_A.getValue(pnd_I1,pnd_J2), new ReportEditMask         //Natural: WRITE ( R2 ) #SUB-TXT-A ( AL = 33 ) 045T #SUM.#CURRENT-A ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #SUM.#FUTURE-A ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL-A ( EM = ZZZ,ZZZ,ZZ9.99 )
                ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_Sum_Pnd_Future_A.getValue(pnd_I1,pnd_J2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_T_Total_A, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            if (condition(! (pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("TPA") || pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("IPRO") ||                     //Natural: IF NOT #OLD-SORT-OPTION-CDE-X = 'TPA' OR = 'IPRO' OR = 'P&I'
                pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("P&I"))))
            {
                getReports().write(3, pnd_Sub_Txt_M, new AlphanumericLength (33),new TabSetting(45),pnd_Sum_Pnd_Current_M.getValue(pnd_I1,pnd_J2), new                    //Natural: WRITE ( R2 ) #SUB-TXT-M ( AL = 33 ) 045T #SUM.#CURRENT-M ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #SUM.#FUTURE-M ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL-M ( EM = ZZZ,ZZZ,ZZ9.99 )
                    ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_Sum_Pnd_Future_M.getValue(pnd_I1,pnd_J2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(77),pnd_Totals_Pnd_T_Total_M, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Totals_Pnd_C_Total.reset();                                                                                                                               //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
            pnd_Totals_Pnd_F_Total.reset();
            pnd_Totals_Pnd_T_Total.reset();
            pnd_Totals_Pnd_Cqty.reset();
            pnd_Totals_Pnd_Fqty.reset();
            pnd_Totals_Pnd_Tqty.reset();
            pnd_Totals_Pnd_C_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_C_Total), pnd_Sum_Pnd_Current_A.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_Current_M.getValue(pnd_I1, //Natural: COMPUTE #C-TOTAL = #SUM.#CURRENT-A ( #I1,#J2 ) + #SUM.#CURRENT-M ( #I1,#J2 )
                pnd_J2)));
            pnd_Totals_Pnd_F_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_F_Total), pnd_Sum_Pnd_Future_A.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_Future_M.getValue(pnd_I1, //Natural: COMPUTE #F-TOTAL = #SUM.#FUTURE-A ( #I1,#J2 ) + #SUM.#FUTURE-M ( #I1,#J2 )
                pnd_J2)));
            pnd_Totals_Pnd_T_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total), pnd_Totals_Pnd_T_Total_A.add(pnd_Totals_Pnd_T_Total_M));                 //Natural: COMPUTE #T-TOTAL = #T-TOTAL-A + #T-TOTAL-M
            pnd_Totals_Pnd_Cqty.setValue(pnd_Sum_Pnd_C_Qty.getValue(pnd_I1,pnd_J2));                                                                                      //Natural: ASSIGN #CQTY = #SUM.#C-QTY ( #I1,#J2 )
            pnd_Totals_Pnd_Fqty.setValue(pnd_Sum_Pnd_F_Qty.getValue(pnd_I1,pnd_J2));                                                                                      //Natural: ASSIGN #FQTY = #SUM.#F-QTY ( #I1,#J2 )
            pnd_Totals_Pnd_Tqty.compute(new ComputeParameters(false, pnd_Totals_Pnd_Tqty), pnd_Sum_Pnd_C_Qty.getValue(pnd_I1,pnd_J2).add(pnd_Sum_Pnd_F_Qty.getValue(pnd_I1, //Natural: COMPUTE #TQTY = #SUM.#C-QTY ( #I1,#J2 ) + #SUM.#F-QTY ( #I1,#J2 )
                pnd_J2)));
            getReports().write(3, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(45),pnd_Totals_Pnd_C_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new                    //Natural: WRITE ( R2 ) / / / 'TOTAL' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #F-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
                TabSetting(60),pnd_Totals_Pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_T_Total, new ReportEditMask 
                ("ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(3, new TabSetting(50),pnd_Totals_Pnd_Cqty, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(65),pnd_Totals_Pnd_Fqty, new ReportEditMask        //Natural: WRITE ( R2 ) 050T #CQTY ( EM = ZZ,ZZ9 ) 065T #FQTY ( EM = ZZ,ZZ9 ) 082T #TQTY ( EM = ZZ,ZZ9 )
                ("ZZ,ZZ9"),new TabSetting(82),pnd_Totals_Pnd_Tqty, new ReportEditMask ("ZZ,ZZ9"));
            if (Global.isEscape()) return;
            getReports().skip(0, 5);                                                                                                                                      //Natural: SKIP ( R2 ) 5
            getReports().write(3, new TabSetting(55),pnd_End_Of_Report_Lit);                                                                                              //Natural: WRITE ( R2 ) 55T #END-OF-REPORT-LIT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Init_Txt_For_Print_Summary() throws Exception                                                                                                        //Natural: INIT-TXT-FOR-PRINT-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Use_Prev_Sort_Key_Val.getBoolean()))                                                                                                            //Natural: IF #USE-PREV-SORT-KEY-VAL
        {
            pnd_Use_Prev_Sort_Key_Val.reset();                                                                                                                            //Natural: RESET #USE-PREV-SORT-KEY-VAL
            pnd_Ws_Cntrct_Orgn_Cde.setValue(pnd_Old_Sort_Key_Pnd_Old_Cntrct_Orgn_Cde);                                                                                    //Natural: ASSIGN #WS-CNTRCT-ORGN-CDE = #OLD-CNTRCT-ORGN-CDE
            pnd_Ws_Sort_Option_Cde.setValue(pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde);                                                                                    //Natural: ASSIGN #WS-SORT-OPTION-CDE = #OLD-SORT-OPTION-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Cntrct_Orgn_Cde.setValue(pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde());                                                                              //Natural: ASSIGN #WS-CNTRCT-ORGN-CDE = SORT-CNTRCT-ORGN-CDE
            pnd_Ws_Sort_Option_Cde.setValue(pdaFcpaext2.getSort_Key_Sort_Option_Cde());                                                                                   //Natural: ASSIGN #WS-SORT-OPTION-CDE = SORT-OPTION-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hdr_Sort_Option_Cde.setValue(pnd_Ws_Sort_Option_Cde, MoveOption.LeftJustified);                                                                               //Natural: MOVE LEFT #WS-SORT-OPTION-CDE TO #HDR-SORT-OPTION-CDE
        pnd_Hdr_Sort_Orgn_Cde.setValue(pnd_Ws_Cntrct_Orgn_Cde, MoveOption.LeftJustified);                                                                                 //Natural: MOVE LEFT #WS-CNTRCT-ORGN-CDE TO #HDR-SORT-ORGN-CDE
        short decideConditionsMet1768 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #WS-CNTRCT-ORGN-CDE;//Natural: VALUE 'NZ'
        if (condition((pnd_Ws_Cntrct_Orgn_Cde.equals("NZ"))))
        {
            decideConditionsMet1768++;
            //*  REGULAR
            if (condition(pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_Pref.equals(" ")))                                                                                //Natural: IF #WS-SORT-OPTION-CDE-PREF = ' '
            {
                if (condition(pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X.equals("TPA") || pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X.equals("IPRO")             //Natural: IF #WS-SORT-OPTION-CDE-X = 'TPA' OR = 'IPRO' OR = 'P&I'
                    || pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X.equals("P&I")))
                {
                    pnd_Sub_Txt.setValue(pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X);                                                                                //Natural: ASSIGN #SUB-TXT := #WS-SORT-OPTION-CDE-X
                    //*  NO MONTHLY TOTALS
                    pnd_Sub_Txt_A.setValue(DbsUtil.compress(pnd_Ws_Sort_Option_Cde, "(ANNUAL)"));                                                                         //Natural: COMPRESS #WS-SORT-OPTION-CDE '(ANNUAL)' INTO #SUB-TXT-A
                    pnd_Sub_Txt_M.setValue(" ");                                                                                                                          //Natural: ASSIGN #SUB-TXT-M := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sub_Txt.setValue("MATURITY BENEFIT ANNUAL");                                                                                                      //Natural: ASSIGN #SUB-TXT := 'MATURITY BENEFIT ANNUAL'
                    pnd_Sub_Txt_A.setValue("MATURITY BENEFIT ANNUAL");                                                                                                    //Natural: ASSIGN #SUB-TXT-A := 'MATURITY BENEFIT ANNUAL'
                    pnd_Sub_Txt_M.setValue("MATURITY BENEFIT MONTHLY");                                                                                                   //Natural: ASSIGN #SUB-TXT-M := 'MATURITY BENEFIT MONTHLY'
                }                                                                                                                                                         //Natural: END-IF
                //*  INT ROLLOVER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X.equals("TPA") || pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X.equals("IPRO")             //Natural: IF #WS-SORT-OPTION-CDE-X = 'TPA' OR = 'IPRO' OR = 'P&I'
                    || pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X.equals("P&I")))
                {
                    pnd_Sub_Txt.setValue(DbsUtil.compress(pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X, "INTERNAL ROLLOVER"));                                         //Natural: COMPRESS #WS-SORT-OPTION-CDE-X 'INTERNAL ROLLOVER' INTO #SUB-TXT
                    //*  NO MONTHLY TOTALS
                    pnd_Sub_Txt_A.setValue(DbsUtil.compress(pnd_Ws_Sort_Option_Cde_Pnd_Ws_Sort_Option_Cde_X, "INTERNAL ROLLOVER (ANNUAL)"));                              //Natural: COMPRESS #WS-SORT-OPTION-CDE-X 'INTERNAL ROLLOVER (ANNUAL)' INTO #SUB-TXT-A
                    pnd_Sub_Txt_M.setValue(" ");                                                                                                                          //Natural: ASSIGN #SUB-TXT-M := ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  INT ROLLOVER OTHERS (IF ANY)
                    short decideConditionsMet1791 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE #WS-SORT-OPTION-CDE;//Natural: VALUE 'IRLC'
                    if (condition((pnd_Ws_Sort_Option_Cde.equals("IRLC"))))
                    {
                        decideConditionsMet1791++;
                        pnd_Sub_Txt.setValue("MATURITY BENEFIT INTERNAL ROLLOVER ANNUAL");                                                                                //Natural: ASSIGN #SUB-TXT := 'MATURITY BENEFIT INTERNAL ROLLOVER ANNUAL'
                        pnd_Sub_Txt_A.setValue("CLASSIC INTERNAL ROLLOVER ANNUAL");                                                                                       //Natural: ASSIGN #SUB-TXT-A := 'CLASSIC INTERNAL ROLLOVER ANNUAL'
                        pnd_Sub_Txt_M.setValue("CLASSIC INTERNAL ROLLOVER MONTHLY");                                                                                      //Natural: ASSIGN #SUB-TXT-M := 'CLASSIC INTERNAL ROLLOVER MONTHLY'
                    }                                                                                                                                                     //Natural: VALUE 'IRLR'
                    else if (condition((pnd_Ws_Sort_Option_Cde.equals("IRLR"))))
                    {
                        decideConditionsMet1791++;
                        pnd_Sub_Txt.setValue("MATURITY BENEFIT INTERNAL ROLLOVER ANNUAL");                                                                                //Natural: ASSIGN #SUB-TXT := 'MATURITY BENEFIT INTERNAL ROLLOVER ANNUAL'
                        pnd_Sub_Txt_A.setValue("ROTH INTERNAL ROLLOVER ANNUAL");                                                                                          //Natural: ASSIGN #SUB-TXT-A := 'ROTH INTERNAL ROLLOVER ANNUAL'
                        pnd_Sub_Txt_M.setValue("ROTH INTERNAL ROLLOVER MONTHLY");                                                                                         //Natural: ASSIGN #SUB-TXT-M := 'ROTH INTERNAL ROLLOVER MONTHLY'
                    }                                                                                                                                                     //Natural: VALUE 'IRLX'
                    else if (condition((pnd_Ws_Sort_Option_Cde.equals("IRLX"))))
                    {
                        decideConditionsMet1791++;
                        pnd_Sub_Txt.setValue("MATURITY BENEFIT INTERNAL ROLLOVER ANNUAL");                                                                                //Natural: ASSIGN #SUB-TXT := 'MATURITY BENEFIT INTERNAL ROLLOVER ANNUAL'
                        pnd_Sub_Txt_A.setValue("OTHER INTERNAL ROLLOVER ANNUAL");                                                                                         //Natural: ASSIGN #SUB-TXT-A := 'OTHER INTERNAL ROLLOVER ANNUAL'
                        pnd_Sub_Txt_M.setValue("OTHER INTERNAL ROLLOVER MONTHLY");                                                                                        //Natural: ASSIGN #SUB-TXT-M := 'OTHER INTERNAL ROLLOVER MONTHLY'
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_Ws_Cntrct_Orgn_Cde.equals("AL"))))
        {
            decideConditionsMet1768++;
            pnd_Sub_Txt.setValue("ANNUITY LOAN ANNUAL");                                                                                                                  //Natural: ASSIGN #SUB-TXT := 'ANNUITY LOAN ANNUAL'
            pnd_Sub_Txt_A.setValue("ANNUITY LOAN ANNUAL");                                                                                                                //Natural: ASSIGN #SUB-TXT-A := 'ANNUITY LOAN ANNUAL'
            pnd_Sub_Txt_M.setValue("ANNUITY LOAN MONTHLY");                                                                                                               //Natural: ASSIGN #SUB-TXT-M := 'ANNUITY LOAN MONTHLY'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Prev_Index() throws Exception                                                                                                              //Natural: DETERMINE-PREV-INDEX
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  REG
        if (condition(pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_Pref.equals(" ")))                                                                                         //Natural: IF #OLD-SORT-OPTION-CDE-PREF = ' '
        {
            short decideConditionsMet1821 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #OLD-SORT-OPTION-CDE-X;//Natural: VALUE 'P&I'
            if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("P&I"))))
            {
                decideConditionsMet1821++;
                pnd_J2.setValue(2);                                                                                                                                       //Natural: ASSIGN #J2 = 2
            }                                                                                                                                                             //Natural: VALUE 'IPRO'
            else if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("IPRO"))))
            {
                decideConditionsMet1821++;
                pnd_J2.setValue(3);                                                                                                                                       //Natural: ASSIGN #J2 = 3
            }                                                                                                                                                             //Natural: VALUE 'TPA'
            else if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("TPA"))))
            {
                decideConditionsMet1821++;
                //*  REGULAR
                pnd_J2.setValue(4);                                                                                                                                       //Natural: ASSIGN #J2 = 4
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_J2.setValue(1);                                                                                                                                       //Natural: ASSIGN #J2 = 1
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  INT. ROLLOVER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet1834 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #OLD-SORT-OPTION-CDE-X;//Natural: VALUE 'P&I'
            if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("P&I"))))
            {
                decideConditionsMet1834++;
                pnd_J2.setValue(8);                                                                                                                                       //Natural: ASSIGN #J2 = 8
            }                                                                                                                                                             //Natural: VALUE 'IPRO'
            else if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("IPRO"))))
            {
                decideConditionsMet1834++;
                pnd_J2.setValue(9);                                                                                                                                       //Natural: ASSIGN #J2 = 9
            }                                                                                                                                                             //Natural: VALUE 'TPA'
            else if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde_X.equals("TPA"))))
            {
                decideConditionsMet1834++;
                pnd_J2.setValue(10);                                                                                                                                      //Natural: ASSIGN #J2 = 10
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                //*  INT ROLLOVER CLASSIC
                short decideConditionsMet1843 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #OLD-SORT-OPTION-CDE;//Natural: VALUE 'IRLC'
                if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde.equals("IRLC"))))
                {
                    decideConditionsMet1843++;
                    //*  INT ROLLOVER ROTH
                    pnd_J2.setValue(5);                                                                                                                                   //Natural: ASSIGN #J2 = 5
                }                                                                                                                                                         //Natural: VALUE 'IRLR'
                else if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde.equals("IRLR"))))
                {
                    decideConditionsMet1843++;
                    //*  INT ROLLOVER OTHERS
                    pnd_J2.setValue(6);                                                                                                                                   //Natural: ASSIGN #J2 = 6
                }                                                                                                                                                         //Natural: VALUE 'IRLX'
                else if (condition((pnd_Old_Sort_Key_Pnd_Old_Sort_Option_Cde.equals("IRLX"))))
                {
                    decideConditionsMet1843++;
                    pnd_J2.setValue(7);                                                                                                                                   //Natural: ASSIGN #J2 = 7
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Reg_Totals() throws Exception                                                                                                                  //Natural: PRINT-REG-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  RCC
        pnd_Cf_Txt.reset();                                                                                                                                               //Natural: RESET #CF-TXT
        pnd_Ws_Cntrct_Orgn_Cde.setValue(pnd_Old_Sort_Key_Pnd_Old_Cntrct_Orgn_Cde);                                                                                        //Natural: ASSIGN #WS-CNTRCT-ORGN-CDE := #OLD-CNTRCT-ORGN-CDE
        //*  RCC
                                                                                                                                                                          //Natural: PERFORM SET-UP-HEADER
        sub_Set_Up_Header();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Ws_Cntrct_Orgn_Cde.setValue(pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde());                                                                                  //Natural: ASSIGN #WS-CNTRCT-ORGN-CDE := SORT-CNTRCT-ORGN-CDE
        pnd_Reset_Page.setValue(true);                                                                                                                                    //Natural: ASSIGN #RESET-PAGE = TRUE
        FOR04:                                                                                                                                                            //Natural: FOR #I1 = 1 TO 4
        for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(4)); pnd_I1.nadd(1))
        {
            short decideConditionsMet1868 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #I1;//Natural: VALUE 1
            if (condition((pnd_I1.equals(1))))
            {
                decideConditionsMet1868++;
                pnd_Header4.setValue("CHECK GRAND TOTALS");                                                                                                               //Natural: ASSIGN #HEADER4 = 'CHECK GRAND TOTALS'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_I1.equals(2))))
            {
                decideConditionsMet1868++;
                pnd_Header4.setValue("EFT GRAND TOTALS");                                                                                                                 //Natural: ASSIGN #HEADER4 = 'EFT GRAND TOTALS'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_I1.equals(3))))
            {
                decideConditionsMet1868++;
                pnd_Header4.setValue("GLOBAL GRAND TOTALS");                                                                                                              //Natural: ASSIGN #HEADER4 = 'GLOBAL GRAND TOTALS'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_I1.equals(4))))
            {
                decideConditionsMet1868++;
                pnd_Header4.setValue("GRAND TOTALS");                                                                                                                     //Natural: ASSIGN #HEADER4 = 'GRAND TOTALS'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  TOTAL
            pnd_Hdr_Sort_Option_Cde.setValue("RREGTOT");                                                                                                                  //Natural: ASSIGN #HDR-SORT-OPTION-CDE = 'RREGTOT'
            //*   ASSIGN #HDR-SORT-OPTION-CDE = 'REGTOT'       /* TOTAL
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER4
            sub_Center_Header4();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            if (condition(pnd_Reset_Page.getBoolean()))                                                                                                                   //Natural: IF #RESET-PAGE
            {
                pnd_Reset_Page.reset();                                                                                                                                   //Natural: RESET #RESET-PAGE
                getReports().getPageNumberDbs(2).setValue(0);                                                                                                             //Natural: ASSIGN *PAGE-NUMBER ( R1 ) = 0
            }                                                                                                                                                             //Natural: END-IF
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( R1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, NEWLINE,NEWLINE,new TabSetting(48),"1ST PAYMENT",NEWLINE,new TabSetting(52),"CURRENT",new TabSetting(68),"FUTURE",new                   //Natural: WRITE ( R1 ) / / 048T '1ST PAYMENT' / 052T 'CURRENT' 068T 'FUTURE' 086T 'TOTAL' /
                TabSetting(86),"TOTAL",NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Totals_Pnd_C_Total.reset();                                                                                                                               //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
            pnd_Totals_Pnd_F_Total.reset();
            pnd_Totals_Pnd_T_Total.reset();
            pnd_Totals_Pnd_Cqty.reset();
            pnd_Totals_Pnd_Fqty.reset();
            pnd_Totals_Pnd_Tqty.reset();
            FOR05:                                                                                                                                                        //Natural: FOR #J2 = 1 TO 4
            for (pnd_J2.setValue(1); condition(pnd_J2.lessOrEqual(4)); pnd_J2.nadd(1))
            {
                //* *    IF #J2 = 2 AND NOT #MONTH-END  /* IF P&I AND NOT MONTH-END
                //* *       ESCAPE TOP
                //* *    END-IF
                short decideConditionsMet1896 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #J2;//Natural: VALUE 1
                if (condition((pnd_J2.equals(1))))
                {
                    decideConditionsMet1896++;
                    pnd_Sub_Txt_A.setValue("MATURITY BENEFIT ANNUAL");                                                                                                    //Natural: ASSIGN #SUB-TXT-A = 'MATURITY BENEFIT ANNUAL'
                    pnd_Sub_Txt_M.setValue("MATURITY BENEFIT MONTHLY");                                                                                                   //Natural: ASSIGN #SUB-TXT-M = 'MATURITY BENEFIT MONTHLY'
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_J2.equals(2))))
                {
                    decideConditionsMet1896++;
                    pnd_Sub_Txt_A.setValue("P&I (ANNUAL)");                                                                                                               //Natural: ASSIGN #SUB-TXT-A = 'P&I (ANNUAL)'
                    pnd_Sub_Txt_M.reset();                                                                                                                                //Natural: RESET #SUB-TXT-M
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_J2.equals(3))))
                {
                    decideConditionsMet1896++;
                    pnd_Sub_Txt_A.setValue("IPRO (ANNUAL)");                                                                                                              //Natural: ASSIGN #SUB-TXT-A = 'IPRO (ANNUAL)'
                    pnd_Sub_Txt_M.reset();                                                                                                                                //Natural: RESET #SUB-TXT-M
                }                                                                                                                                                         //Natural: VALUE 4
                else if (condition((pnd_J2.equals(4))))
                {
                    decideConditionsMet1896++;
                    pnd_Sub_Txt_A.setValue("TPA (ANNUAL)");                                                                                                               //Natural: ASSIGN #SUB-TXT-A = 'TPA (ANNUAL)'
                    pnd_Sub_Txt_M.reset();                                                                                                                                //Natural: RESET #SUB-TXT-M
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Totals_Pnd_T_Total_A.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total_A), pnd_G_Tot_Pnd_Current_A.getValue(pnd_I1,pnd_J2).add(pnd_G_Tot_Pnd_Future_A.getValue(pnd_I1, //Natural: COMPUTE #T-TOTAL-A = #G-TOT.#CURRENT-A ( #I1,#J2 ) + #G-TOT.#FUTURE-A ( #I1,#J2 )
                    pnd_J2)));
                pnd_Totals_Pnd_T_Total_M.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total_M), pnd_G_Tot_Pnd_Current_M.getValue(pnd_I1,pnd_J2).add(pnd_G_Tot_Pnd_Future_M.getValue(pnd_I1, //Natural: COMPUTE #T-TOTAL-M = #G-TOT.#CURRENT-M ( #I1,#J2 ) + #G-TOT.#FUTURE-M ( #I1,#J2 )
                    pnd_J2)));
                getReports().write(2, pnd_Sub_Txt_A, new AlphanumericLength (33),new TabSetting(45),pnd_G_Tot_Pnd_Current_A.getValue(pnd_I1,pnd_J2), new                  //Natural: WRITE ( R1 ) #SUB-TXT-A ( AL = 33 ) 045T #G-TOT.#CURRENT-A ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #G-TOT.#FUTURE-A ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL-A ( EM = ZZZ,ZZZ,ZZ9.99 )
                    ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_G_Tot_Pnd_Future_A.getValue(pnd_I1,pnd_J2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(77),pnd_Totals_Pnd_T_Total_A, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  NO MONTHLY FOR P&I,IPRO,TPA
                if (condition(! (pnd_J2.equals(2) || pnd_J2.equals(3) || pnd_J2.equals(4))))                                                                              //Natural: IF NOT #J2 = 2 OR = 3 OR = 4
                {
                    getReports().write(2, pnd_Sub_Txt_M, new AlphanumericLength (33),new TabSetting(45),pnd_G_Tot_Pnd_Current_M.getValue(pnd_I1,pnd_J2),                  //Natural: WRITE ( R1 ) #SUB-TXT-M ( AL = 33 ) 045T #G-TOT.#CURRENT-M ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #G-TOT.#FUTURE-M ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL-M ( EM = ZZZ,ZZZ,ZZ9.99 ) /
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_G_Tot_Pnd_Future_M.getValue(pnd_I1,pnd_J2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                        TabSetting(77),pnd_Totals_Pnd_T_Total_M, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Totals_Pnd_C_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_C_Total), pnd_Totals_Pnd_C_Total.add(pnd_G_Tot_Pnd_Current_A.getValue(pnd_I1,  //Natural: COMPUTE #C-TOTAL = #C-TOTAL + #G-TOT.#CURRENT-A ( #I1,#J2 ) + #G-TOT.#CURRENT-M ( #I1,#J2 )
                    pnd_J2)).add(pnd_G_Tot_Pnd_Current_M.getValue(pnd_I1,pnd_J2)));
                pnd_Totals_Pnd_F_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_F_Total), pnd_Totals_Pnd_F_Total.add(pnd_G_Tot_Pnd_Future_A.getValue(pnd_I1,   //Natural: COMPUTE #F-TOTAL = #F-TOTAL + #G-TOT.#FUTURE-A ( #I1,#J2 ) + #G-TOT.#FUTURE-M ( #I1,#J2 )
                    pnd_J2)).add(pnd_G_Tot_Pnd_Future_M.getValue(pnd_I1,pnd_J2)));
                pnd_Totals_Pnd_T_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total), pnd_Totals_Pnd_T_Total.add(pnd_Totals_Pnd_T_Total_A).add(pnd_Totals_Pnd_T_Total_M)); //Natural: COMPUTE #T-TOTAL = #T-TOTAL + #T-TOTAL-A + #T-TOTAL-M
                pnd_Totals_Pnd_Cqty.nadd(pnd_G_Tot_Pnd_C_Qty.getValue(pnd_I1,pnd_J2));                                                                                    //Natural: COMPUTE #CQTY = #CQTY + #G-TOT.#C-QTY ( #I1,#J2 )
                pnd_Totals_Pnd_Fqty.nadd(pnd_G_Tot_Pnd_F_Qty.getValue(pnd_I1,pnd_J2));                                                                                    //Natural: COMPUTE #FQTY = #FQTY + #G-TOT.#F-QTY ( #I1,#J2 )
                pnd_Totals_Pnd_Tqty.compute(new ComputeParameters(false, pnd_Totals_Pnd_Tqty), pnd_Totals_Pnd_Tqty.add(pnd_G_Tot_Pnd_C_Qty.getValue(pnd_I1,               //Natural: COMPUTE #TQTY = #TQTY + #G-TOT.#C-QTY ( #I1,#J2 ) + #G-TOT.#F-QTY ( #I1,#J2 )
                    pnd_J2)).add(pnd_G_Tot_Pnd_F_Qty.getValue(pnd_I1,pnd_J2)));
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* ** START OF CODE ADDED BY AMC 12-21-2001
            pnd_Totals_Pnd_Tdtra_Total.reset();                                                                                                                           //Natural: RESET #TDTRA-TOTAL
            pnd_Totals_Pnd_Tdtra_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_Tdtra_Total), pnd_Totals_Pnd_End_Dtrac.getValue(pnd_I1).add(pnd_Totals_Pnd_End_Dtraf.getValue(pnd_I1))); //Natural: COMPUTE #TDTRA-TOTAL = #END-DTRAC ( #I1 ) + #END-DTRAF ( #I1 )
            pnd_Totals_Pnd_Diff_Dtra.getValue(1).compute(new ComputeParameters(false, pnd_Totals_Pnd_Diff_Dtra.getValue(1)), pnd_Totals_Pnd_C_Total.subtract(pnd_Totals_Pnd_End_Dtrac.getValue(pnd_I1))); //Natural: COMPUTE #DIFF-DTRA ( 1 ) = #C-TOTAL - #END-DTRAC ( #I1 )
            pnd_Totals_Pnd_Diff_Dtra.getValue(2).compute(new ComputeParameters(false, pnd_Totals_Pnd_Diff_Dtra.getValue(2)), pnd_Totals_Pnd_F_Total.subtract(pnd_Totals_Pnd_End_Dtraf.getValue(pnd_I1))); //Natural: COMPUTE #DIFF-DTRA ( 2 ) = #F-TOTAL - #END-DTRAF ( #I1 )
            pnd_Totals_Pnd_Diff_Dtra.getValue(3).compute(new ComputeParameters(false, pnd_Totals_Pnd_Diff_Dtra.getValue(3)), pnd_Totals_Pnd_Diff_Dtra.getValue(1).add(pnd_Totals_Pnd_Diff_Dtra.getValue(2))); //Natural: COMPUTE #DIFF-DTRA ( 3 ) = #DIFF-DTRA ( 1 ) + #DIFF-DTRA ( 2 )
            getReports().write(2, "NON DTRA TPA (ANNUAL)",new TabSetting(45),pnd_Totals_Pnd_Diff_Dtra.getValue(1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new              //Natural: WRITE ( R1 ) 'NON DTRA TPA (ANNUAL)' 045T #DIFF-DTRA ( 1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #DIFF-DTRA ( 2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #DIFF-DTRA ( 3 ) ( EM = ZZZ,ZZZ,ZZ9.99 )
                TabSetting(60),pnd_Totals_Pnd_Diff_Dtra.getValue(2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_Diff_Dtra.getValue(3), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, "DTRA TPA (ANNUAL)",new TabSetting(45),pnd_Totals_Pnd_End_Dtrac.getValue(pnd_I1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new             //Natural: WRITE ( R1 ) 'DTRA TPA (ANNUAL)' 045T #END-DTRAC ( #I1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #END-DTRAF ( #I1 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #TDTRA-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 )
                TabSetting(60),pnd_Totals_Pnd_End_Dtraf.getValue(pnd_I1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_Tdtra_Total, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, NEWLINE,NEWLINE,"TOTAL",new TabSetting(45),pnd_Totals_Pnd_C_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_Totals_Pnd_F_Total,  //Natural: WRITE ( R1 ) / / 'TOTAL' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #F-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) / 050T #CQTY ( EM = ZZ,ZZ9 ) 065T #FQTY ( EM = ZZ,ZZ9 ) 082T #TQTY ( EM = ZZ,ZZ9 )
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_T_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(50),pnd_Totals_Pnd_Cqty, 
                new ReportEditMask ("ZZ,ZZ9"),new TabSetting(65),pnd_Totals_Pnd_Fqty, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(82),pnd_Totals_Pnd_Tqty, 
                new ReportEditMask ("ZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Totals_Pnd_End_Dtrac.getValue(4).nadd(pnd_Totals_Pnd_End_Dtrac.getValue(pnd_I1));                                                                         //Natural: COMPUTE #END-DTRAC ( 4 ) = #END-DTRAC ( 4 ) + #END-DTRAC ( #I1 )
            pnd_Totals_Pnd_End_Dtraf.getValue(4).nadd(pnd_Totals_Pnd_End_Dtraf.getValue(pnd_I1));                                                                         //Natural: COMPUTE #END-DTRAF ( 4 ) = #END-DTRAF ( 4 ) + #END-DTRAF ( #I1 )
            //*  ROXAN
            pnd_Totals_Pnd_End_Dtrac.getValue(pnd_I1).reset();                                                                                                            //Natural: RESET #END-DTRAC ( #I1 ) #END-DTRAF ( #I1 )
            pnd_Totals_Pnd_End_Dtraf.getValue(pnd_I1).reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Print_Int_Rollover_Totals() throws Exception                                                                                                         //Natural: PRINT-INT-ROLLOVER-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  RCC
        pnd_Cf_Txt.reset();                                                                                                                                               //Natural: RESET #CF-TXT
        pnd_Ws_Cntrct_Orgn_Cde.setValue(pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde());                                                                                  //Natural: ASSIGN #WS-CNTRCT-ORGN-CDE := SORT-CNTRCT-ORGN-CDE
                                                                                                                                                                          //Natural: PERFORM SET-UP-HEADER
        sub_Set_Up_Header();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_I1.setValue(5);                                                                                                                                               //Natural: ASSIGN #I1 = 5
        pnd_Header4.setValue("INTERNAL ROLLOVER GRAND TOTALS");                                                                                                           //Natural: ASSIGN #HEADER4 = 'INTERNAL ROLLOVER GRAND TOTALS'
        //*  INT ROLL TOTAL
        pnd_Hdr_Sort_Option_Cde.setValue("IRLTOT");                                                                                                                       //Natural: ASSIGN #HDR-SORT-OPTION-CDE = 'IRLTOT'
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER4
        sub_Center_Header4();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().getPageNumberDbs(3).setValue(0);                                                                                                                     //Natural: ASSIGN *PAGE-NUMBER ( R2 ) = 0
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( R2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(3, NEWLINE,NEWLINE,new TabSetting(48),"1ST PAYMENT",NEWLINE,new TabSetting(52),"CURRENT",new TabSetting(68),"FUTURE",new TabSetting(86),       //Natural: WRITE ( R2 ) / / 048T '1ST PAYMENT' / 052T 'CURRENT' 068T 'FUTURE' 086T 'TOTAL' /
            "TOTAL",NEWLINE);
        if (Global.isEscape()) return;
        pnd_Totals_Pnd_C_Total.reset();                                                                                                                                   //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
        pnd_Totals_Pnd_F_Total.reset();
        pnd_Totals_Pnd_T_Total.reset();
        pnd_Totals_Pnd_Cqty.reset();
        pnd_Totals_Pnd_Fqty.reset();
        pnd_Totals_Pnd_Tqty.reset();
        FOR06:                                                                                                                                                            //Natural: FOR #J2 = 5 TO 10
        for (pnd_J2.setValue(5); condition(pnd_J2.lessOrEqual(10)); pnd_J2.nadd(1))
        {
            short decideConditionsMet1966 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #J2;//Natural: VALUE 5
            if (condition((pnd_J2.equals(5))))
            {
                decideConditionsMet1966++;
                pnd_Sub_Txt_A.setValue("CLASSIC INTERNAL ROLLOVER ANNUAL");                                                                                               //Natural: ASSIGN #SUB-TXT-A = 'CLASSIC INTERNAL ROLLOVER ANNUAL'
                pnd_Sub_Txt_M.setValue("CLASSIC INTERNAL ROLLOVER MONTHLY");                                                                                              //Natural: ASSIGN #SUB-TXT-M = 'CLASSIC INTERNAL ROLLOVER MONTHLY'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_J2.equals(6))))
            {
                decideConditionsMet1966++;
                pnd_Sub_Txt_A.setValue("ROTH INTERNAL ROLLOVER ANNUAL");                                                                                                  //Natural: ASSIGN #SUB-TXT-A = 'ROTH INTERNAL ROLLOVER ANNUAL'
                pnd_Sub_Txt_M.setValue("ROTH INTERNAL ROLLOVER MONTHLY");                                                                                                 //Natural: ASSIGN #SUB-TXT-M = 'ROTH INTERNAL ROLLOVER MONTHLY'
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_J2.equals(7))))
            {
                decideConditionsMet1966++;
                pnd_Sub_Txt_A.setValue("OTHER INTERNAL ROLLOVER ANNUAL");                                                                                                 //Natural: ASSIGN #SUB-TXT-A = 'OTHER INTERNAL ROLLOVER ANNUAL'
                pnd_Sub_Txt_M.setValue("OTHER INTERNAL ROLLOVER MONTHLY");                                                                                                //Natural: ASSIGN #SUB-TXT-M = 'OTHER INTERNAL ROLLOVER MONTHLY'
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_J2.equals(8))))
            {
                decideConditionsMet1966++;
                pnd_Sub_Txt_A.setValue("P&I INT. ROLLOVER (ANNUAL)");                                                                                                     //Natural: ASSIGN #SUB-TXT-A = 'P&I INT. ROLLOVER (ANNUAL)'
                pnd_Sub_Txt_M.reset();                                                                                                                                    //Natural: RESET #SUB-TXT-M
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pnd_J2.equals(9))))
            {
                decideConditionsMet1966++;
                pnd_Sub_Txt_A.setValue("IPRO INT. ROLLOVER (ANNUAL)");                                                                                                    //Natural: ASSIGN #SUB-TXT-A = 'IPRO INT. ROLLOVER (ANNUAL)'
                pnd_Sub_Txt_M.reset();                                                                                                                                    //Natural: RESET #SUB-TXT-M
            }                                                                                                                                                             //Natural: VALUE 10
            else if (condition((pnd_J2.equals(10))))
            {
                decideConditionsMet1966++;
                pnd_Sub_Txt_A.setValue("TPA INT. ROLLOVER (ANNUAL)");                                                                                                     //Natural: ASSIGN #SUB-TXT-A = 'TPA INT. ROLLOVER (ANNUAL)'
                pnd_Sub_Txt_M.reset();                                                                                                                                    //Natural: RESET #SUB-TXT-M
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Totals_Pnd_T_Total_A.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total_A), pnd_G_Tot_Pnd_Current_A.getValue(pnd_I1,pnd_J2).add(pnd_G_Tot_Pnd_Future_A.getValue(pnd_I1, //Natural: COMPUTE #T-TOTAL-A = #G-TOT.#CURRENT-A ( #I1,#J2 ) + #G-TOT.#FUTURE-A ( #I1,#J2 )
                pnd_J2)));
            pnd_Totals_Pnd_T_Total_M.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total_M), pnd_G_Tot_Pnd_Current_M.getValue(pnd_I1,pnd_J2).add(pnd_G_Tot_Pnd_Future_M.getValue(pnd_I1, //Natural: COMPUTE #T-TOTAL-M = #G-TOT.#CURRENT-M ( #I1,#J2 ) + #G-TOT.#FUTURE-M ( #I1,#J2 )
                pnd_J2)));
            getReports().write(3, pnd_Sub_Txt_A, new AlphanumericLength (33),new TabSetting(45),pnd_G_Tot_Pnd_Current_A.getValue(pnd_I1,pnd_J2), new ReportEditMask       //Natural: WRITE ( R2 ) #SUB-TXT-A ( AL = 33 ) 045T #G-TOT.#CURRENT-A ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #G-TOT.#FUTURE-A ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL-A ( EM = ZZZ,ZZZ,ZZ9.99 )
                ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_G_Tot_Pnd_Future_A.getValue(pnd_I1,pnd_J2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_T_Total_A, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  NO MONTHLY FOR P&I,IPRO,TPA
            if (condition(! (pnd_J2.equals(8) || pnd_J2.equals(9) || pnd_J2.equals(10))))                                                                                 //Natural: IF NOT #J2 = 8 OR = 9 OR = 10
            {
                getReports().write(3, pnd_Sub_Txt_M, new AlphanumericLength (33),new TabSetting(45),pnd_G_Tot_Pnd_Current_M.getValue(pnd_I1,pnd_J2), new                  //Natural: WRITE ( R2 ) #SUB-TXT-M ( AL = 33 ) 045T #G-TOT.#CURRENT-M ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #G-TOT.#FUTURE-M ( #I1,#J2 ) ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL-M ( EM = ZZZ,ZZZ,ZZ9.99 )
                    ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_G_Tot_Pnd_Future_M.getValue(pnd_I1,pnd_J2), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new 
                    TabSetting(77),pnd_Totals_Pnd_T_Total_M, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Totals_Pnd_C_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_C_Total), pnd_Totals_Pnd_C_Total.add(pnd_G_Tot_Pnd_Current_A.getValue(pnd_I1,      //Natural: COMPUTE #C-TOTAL = #C-TOTAL + #G-TOT.#CURRENT-A ( #I1,#J2 ) + #G-TOT.#CURRENT-M ( #I1,#J2 )
                pnd_J2)).add(pnd_G_Tot_Pnd_Current_M.getValue(pnd_I1,pnd_J2)));
            pnd_Totals_Pnd_F_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_F_Total), pnd_Totals_Pnd_F_Total.add(pnd_G_Tot_Pnd_Future_A.getValue(pnd_I1,       //Natural: COMPUTE #F-TOTAL = #F-TOTAL + #G-TOT.#FUTURE-A ( #I1,#J2 ) + #G-TOT.#FUTURE-M ( #I1,#J2 )
                pnd_J2)).add(pnd_G_Tot_Pnd_Future_M.getValue(pnd_I1,pnd_J2)));
            pnd_Totals_Pnd_T_Total.compute(new ComputeParameters(false, pnd_Totals_Pnd_T_Total), pnd_Totals_Pnd_T_Total.add(pnd_Totals_Pnd_T_Total_A).add(pnd_Totals_Pnd_T_Total_M)); //Natural: COMPUTE #T-TOTAL = #T-TOTAL + #T-TOTAL-A + #T-TOTAL-M
            pnd_Totals_Pnd_Cqty.nadd(pnd_G_Tot_Pnd_C_Qty.getValue(pnd_I1,pnd_J2));                                                                                        //Natural: COMPUTE #CQTY = #CQTY + #G-TOT.#C-QTY ( #I1,#J2 )
            pnd_Totals_Pnd_Fqty.nadd(pnd_G_Tot_Pnd_F_Qty.getValue(pnd_I1,pnd_J2));                                                                                        //Natural: COMPUTE #FQTY = #FQTY + #G-TOT.#F-QTY ( #I1,#J2 )
            pnd_Totals_Pnd_Tqty.compute(new ComputeParameters(false, pnd_Totals_Pnd_Tqty), pnd_Totals_Pnd_Tqty.add(pnd_G_Tot_Pnd_C_Qty.getValue(pnd_I1,                   //Natural: COMPUTE #TQTY = #TQTY + #G-TOT.#C-QTY ( #I1,#J2 ) + #G-TOT.#F-QTY ( #I1,#J2 )
                pnd_J2)).add(pnd_G_Tot_Pnd_F_Qty.getValue(pnd_I1,pnd_J2)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(45),pnd_Totals_Pnd_C_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),pnd_Totals_Pnd_F_Total,  //Natural: WRITE ( R2 ) / / / 'TOTAL' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) 060T #F-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) 077T #T-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99 ) / 050T #CQTY ( EM = ZZ,ZZ9 ) 065T #FQTY ( EM = ZZ,ZZ9 ) 082T #TQTY ( EM = ZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),pnd_Totals_Pnd_T_Total, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(50),pnd_Totals_Pnd_Cqty, 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(65),pnd_Totals_Pnd_Fqty, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(82),pnd_Totals_Pnd_Tqty, 
            new ReportEditMask ("ZZ,ZZ9"));
        if (Global.isEscape()) return;
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET CHECK-NO PREFIX FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new ColumnSpacing(3),Global.getINIT_USER(),new TabSetting(41),pnd_Header1,new          //Natural: WRITE ( R1 ) NOTITLE *PROGRAM 3X *INIT-USER 41T #HEADER1 111T #HDR-SORT-ORGN-CDE 115T #HDR-SORT-OPTION-CDE 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( R1 ) ( AD = L NL = 4 )
                        TabSetting(111),pnd_Hdr_Sort_Orgn_Cde,new TabSetting(115),pnd_Hdr_Sort_Option_Cde,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(2), 
                        new FieldAttributes ("AD=L"), new NumericLength (4));
                    getReports().write(2, Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(25),pnd_Header2,new TabSetting(124),Global.getTIMX(),  //Natural: WRITE ( R1 ) *DATX ( EM = LLL' 'DD', 'YYYY ) 25T #HEADER2 124T *TIMX ( EM = HH':'II' 'AP ) / 41T #HEADER4 #CF-TXT
                        new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(41),pnd_Header4,pnd_Cf_Txt);
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP ( R1 ) 1
                    if (condition(! (pnd_Summary.getBoolean())))                                                                                                          //Natural: IF NOT #SUMMARY
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-HEADER
                        sub_Print_Detail_Header();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Sub_Prt.getBoolean()))                                                                                                              //Natural: IF #SUB-PRT
                    {
                        pnd_Sub_Prt.reset();                                                                                                                              //Natural: RESET #SUB-PRT
                        pnd_Sub_Txt.setValue(DbsUtil.compress(pnd_Sub_Txt, "GRAND TOTALS:"));                                                                             //Natural: COMPRESS #SUB-TXT 'GRAND TOTALS:' INTO #SUB-TXT
                        pnd_Sub_Txt.setValue(pnd_Sub_Txt, MoveOption.RightJustified);                                                                                     //Natural: MOVE RIGHT #SUB-TXT TO #SUB-TXT
                    }                                                                                                                                                     //Natural: END-IF
                    //*  34T
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(3, ReportOption.NOTITLE,Global.getPROGRAM(),new ColumnSpacing(3),Global.getINIT_USER(),new TabSetting(41),pnd_Header1,new          //Natural: WRITE ( R2 ) NOTITLE *PROGRAM 3X *INIT-USER 41T #HEADER1 111T #HDR-SORT-ORGN-CDE 115T #HDR-SORT-OPTION-CDE 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( R2 ) ( AD = L NL = 4 )
                        TabSetting(111),pnd_Hdr_Sort_Orgn_Cde,new TabSetting(115),pnd_Hdr_Sort_Option_Cde,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(3), 
                        new FieldAttributes ("AD=L"), new NumericLength (4));
                    getReports().write(3, Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(25),pnd_Header2,new TabSetting(124),Global.getTIMX(),  //Natural: WRITE ( R2 ) *DATX ( EM = LLL' 'DD', 'YYYY ) 25T #HEADER2 124T *TIMX ( EM = HH':'II' 'AP ) / 41T #HEADER4 #CF-TXT
                        new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(41),pnd_Header4,pnd_Cf_Txt);
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP ( R2 ) 1
                    if (condition(! (pnd_Summary.getBoolean())))                                                                                                          //Natural: IF NOT #SUMMARY
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-HEADER
                        sub_Print_Detail_Header();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Sub_Prt.getBoolean()))                                                                                                              //Natural: IF #SUB-PRT
                    {
                        pnd_Sub_Prt.reset();                                                                                                                              //Natural: RESET #SUB-PRT
                        pnd_Sub_Txt.setValue(DbsUtil.compress(pnd_Sub_Txt, "GRAND TOTALS:"));                                                                             //Natural: COMPRESS #SUB-TXT 'GRAND TOTALS:' INTO #SUB-TXT
                        pnd_Sub_Txt.setValue(pnd_Sub_Txt, MoveOption.RightJustified);                                                                                     //Natural: MOVE RIGHT #SUB-TXT TO #SUB-TXT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pdaFcpaext2_getExt2_Pymnt_Check_DteIsBreak = pdaFcpaext2.getExt2_Pymnt_Check_Dte().isBreak(endOfData);
        boolean pdaFcpaext2_getExt2_Cntrct_Type_CdeIsBreak = pdaFcpaext2.getExt2_Cntrct_Type_Cde().isBreak(endOfData);
        boolean pdaFcpaext2_getSort_KeyIsBreak = pdaFcpaext2.getSort_Key().isBreak(endOfData);
        boolean pdaFcpaext2_getSort_Key2IsBreak = pdaFcpaext2.getSort_Key().isBreak(endOfData);
        if (condition(pdaFcpaext2_getExt2_Pymnt_Check_DteIsBreak || pdaFcpaext2_getExt2_Cntrct_Type_CdeIsBreak || pdaFcpaext2_getSort_KeyIsBreak || pdaFcpaext2_getSort_Key2IsBreak))
        {
            pnd_Old_Sort_Key.setValue(readWork01Sort_KeyOld);                                                                                                             //Natural: MOVE OLD ( SORT-KEY ) TO #OLD-SORT-KEY
            if (condition(pnd_Dtl_Recs_Found.getBoolean()))                                                                                                               //Natural: IF #DTL-RECS-FOUND
            {
                if (condition(! (pnd_Month_End.getBoolean())))                                                                                                            //Natural: IF NOT #MONTH-END
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                    sub_Print_Detail_Lines();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Old_Pymnt_Check_Dte.setValue(readWork01Pymnt_Check_DteOld);                                                                                       //Natural: ASSIGN #OLD-PYMNT-CHECK-DTE = OLD ( EXT2.PYMNT-CHECK-DTE )
                                                                                                                                                                          //Natural: PERFORM PRINT-DTE-SUB-TOTAL
                    sub_Print_Dte_Sub_Total();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_First.setValue(true);                                                                                                                             //Natural: ASSIGN #FIRST = TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpaext2_getExt2_Cntrct_Type_CdeIsBreak || pdaFcpaext2_getSort_KeyIsBreak || pdaFcpaext2_getSort_Key2IsBreak))
        {
            pnd_Old_Sort_Key.setValue(readWork01Sort_KeyOld);                                                                                                             //Natural: MOVE OLD ( SORT-KEY ) TO #OLD-SORT-KEY
            if (condition(pnd_Dtl_Recs_Found.getBoolean()))                                                                                                               //Natural: IF #DTL-RECS-FOUND
            {
                if (condition(pnd_Month_End.getBoolean()))                                                                                                                //Natural: IF #MONTH-END
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                    sub_Print_Detail_Lines();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_First.setValue(true);                                                                                                                                     //Natural: ASSIGN #FIRST = TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpaext2_getSort_KeyIsBreak || pdaFcpaext2_getSort_Key2IsBreak))
        {
            pnd_Old_Sort_Key.setValue(readWork01Sort_KeyOld);                                                                                                             //Natural: MOVE OLD ( SORT-KEY ) TO #OLD-SORT-KEY
                                                                                                                                                                          //Natural: PERFORM BREAK-SORT-KEY
            sub_Break_Sort_Key();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  CONTRCT-ORGN-CDE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpaext2_getSort_KeyIsBreak || pdaFcpaext2_getSort_Key2IsBreak))
        {
            pnd_Old_Sort_Key.setValue(readWork01Sort_KeyOld);                                                                                                             //Natural: MOVE OLD ( SORT-KEY ) TO #OLD-SORT-KEY
                                                                                                                                                                          //Natural: PERFORM BREAK-SORT-KEY-CONTRCT-ORGN
            sub_Break_Sort_Key_Contrct_Orgn();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(3, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
