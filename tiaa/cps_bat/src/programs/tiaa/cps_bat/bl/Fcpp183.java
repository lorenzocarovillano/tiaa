/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:31 PM
**        * FROM NATURAL PROGRAM : Fcpp183
************************************************************
**        * FILE NAME            : Fcpp183.java
**        * CLASS NAME           : Fcpp183
**        * INSTANCE NAME        : Fcpp183
************************************************************
************************************************************************
* PROGRAM  : FCPP183
* SYSTEM   : CPS
* TITLE    : NZ LAYOUT CONVERSION BEFORE PROCESSING FCPP195, FCPP198
* FUNCTION : CONVERTS NZ LAYOUT NZ-EXT OF FCPA371 TO #OUTPUT IN FCPL190
* DATE     : 08/13/1997 - LIN ZHENG
*            FUND INFORMATION IS NOT FILLED IN. IT IS NOT NEEDED.
*
* HISTORY
*
* 5/22/1998 - RIAD LOUTFI - ADDED RETIREMENT LOAN ("AL") PROCESSING.
* 11/1/1999 - ROXAN C.    - ADDED "EW" PROCESSING.
* 12/07/99  - ROXAN C.    - NEW LOCAL FCPAEXT
* 4/2017    : JJG - PIN EXPANSION RESTOW
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp183 extends BLNatBase
{
    // Data Areas
    private LdaFcpl190a ldaFcpl190a;
    private PdaFcpaext pdaFcpaext;

    // Local Variables
    public DbsRecord localVariables;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp183() throws Exception
    {
        super("Fcpp183");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  READ  WORK FILE 01  PYMNT-ADDR-INFO
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 EXT ( * )
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            //*   ACCEPT IF NZ-EXT.PYMNT-INSTMT-NBR = 01
            if (condition(!(pdaFcpaext.getExt_Pymnt_Instmt_Nbr().equals(1))))                                                                                             //Natural: ACCEPT IF EXT.PYMNT-INSTMT-NBR = 01
            {
                continue;
            }
            //*   MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #RPT-EXT.#PYMNT-ADDR
            ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr().setValuesByName(pdaFcpaext.getExt_Extr());                                                                        //Natural: MOVE BY NAME EXTR TO #RPT-EXT.#PYMNT-ADDR
            ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct().reset();                                                                                                              //Natural: RESET #RPT-EXT.C-INV-ACCT
            //*   DECIDE ON FIRST VALUE OF NZ-EXT.CNTRCT-ORGN-CDE
            short decideConditionsMet635 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF EXT.CNTRCT-ORGN-CDE;//Natural: VALUE 'NZ'
            if (condition((pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("NZ"))))
            {
                decideConditionsMet635++;
                getWorkFiles().write(2, true, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr());                                                                               //Natural: WRITE WORK FILE 02 VARIABLE #RPT-EXT.#PYMNT-ADDR
            }                                                                                                                                                             //Natural: VALUE 'AL'
            else if (condition((pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("AL"))))
            {
                decideConditionsMet635++;
                getWorkFiles().write(3, true, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr());                                                                               //Natural: WRITE WORK FILE 03 VARIABLE #RPT-EXT.#PYMNT-ADDR
            }                                                                                                                                                             //Natural: VALUE 'EW'
            else if (condition((pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW"))))
            {
                decideConditionsMet635++;
                getWorkFiles().write(4, true, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr());                                                                               //Natural: WRITE WORK FILE 04 VARIABLE #RPT-EXT.#PYMNT-ADDR
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //
}
