/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:22 PM
**        * FROM NATURAL PROGRAM : Fcpp560
************************************************************
**        * FILE NAME            : Fcpp560.java
**        * CLASS NAME           : Fcpp560
**        * INSTANCE NAME        : Fcpp560
************************************************************
***********************************************************************
* PROGRAM   : FCPP560
* SYSTEM    : CPS
* TITLE     : PAYMENT REGISTERS FOR TREASURY DIVISION
* GENERATED :
* FUNCTION  : GENERATE PAYMENT REGISTERS FOR TREASURY DIV.
*           :
* HISTORY   :
* 06/18/99  : R. CARREON  -  CREATED
* 10/13/99  :             -  EW PROCESSING
* 10/06/99  : A. YOUNG    -  REPLACED LEVEL 2 VARIABLE NAME #PYMNT-ADDR
*           :                WITH LEVEL 1 #RPT-EXT FOR WORK FILE 1.
*
* 03/06/2006 : LANDRUM  PAYEE MATCH
*              - INCREASED CHECK NBR TO N10.
*  4/2017    : JJG - PIN EXPANSION RESTOW
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp560 extends BLNatBase
{
    // Data Areas
    private LdaFcpl190a ldaFcpl190a;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_N10__R_Field_1;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N7;

    private DbsGroup pnd_Check_Number_N10__R_Field_2;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_A10;

    private DbsGroup pnd_Work_Variables;
    private DbsField pnd_Work_Variables_Pnd_Ws_Start;
    private DbsField pnd_Work_Variables_Pnd_Ws_Row;
    private DbsField pnd_Work_Variables_Pnd_Ws_Col;
    private DbsField pnd_Work_Variables_Pnd_Ws_Orgn_Cde;
    private DbsField pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table;
    private DbsField pnd_Work_Variables_Pnd_Ws_Check_Dte;
    private DbsField pnd_Work_Variables_Pnd_Ws_Work_Date;

    private DbsGroup pnd_Work_Variables_Pnd_Ws_Summary;

    private DbsGroup pnd_Work_Variables_Pnd_Ws_Vertical_Array;
    private DbsField pnd_Work_Variables_Pnd_Ws_Record_Count;
    private DbsField pnd_Work_Variables_Pnd_Ws_Check_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        localVariables = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Check_Number_N10 = localVariables.newFieldInRecord("pnd_Check_Number_N10", "#CHECK-NUMBER-N10", FieldType.NUMERIC, 10);

        pnd_Check_Number_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_1", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_N3 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_N10_Pnd_Check_Number_N7 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        pnd_Check_Number_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_2", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_A10 = pnd_Check_Number_N10__R_Field_2.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_A10", "#CHECK-NUMBER-A10", 
            FieldType.STRING, 10);

        pnd_Work_Variables = localVariables.newGroupInRecord("pnd_Work_Variables", "#WORK-VARIABLES");
        pnd_Work_Variables_Pnd_Ws_Start = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Start", "#WS-START", FieldType.NUMERIC, 1);
        pnd_Work_Variables_Pnd_Ws_Row = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Row", "#WS-ROW", FieldType.NUMERIC, 2);
        pnd_Work_Variables_Pnd_Ws_Col = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Col", "#WS-COL", FieldType.NUMERIC, 2);
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Orgn_Cde", "#WS-ORGN-CDE", FieldType.STRING, 
            3);
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table = pnd_Work_Variables.newFieldArrayInGroup("pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table", "#WS-ORGN-CDE-TABLE", 
            FieldType.STRING, 3, new DbsArrayController(1, 12));
        pnd_Work_Variables_Pnd_Ws_Check_Dte = pnd_Work_Variables.newFieldArrayInGroup("pnd_Work_Variables_Pnd_Ws_Check_Dte", "#WS-CHECK-DTE", FieldType.DATE, 
            new DbsArrayController(1, 3));
        pnd_Work_Variables_Pnd_Ws_Work_Date = pnd_Work_Variables.newFieldArrayInGroup("pnd_Work_Variables_Pnd_Ws_Work_Date", "#WS-WORK-DATE", FieldType.STRING, 
            10, new DbsArrayController(1, 3));

        pnd_Work_Variables_Pnd_Ws_Summary = pnd_Work_Variables.newGroupArrayInGroup("pnd_Work_Variables_Pnd_Ws_Summary", "#WS-SUMMARY", new DbsArrayController(1, 
            12));

        pnd_Work_Variables_Pnd_Ws_Vertical_Array = pnd_Work_Variables_Pnd_Ws_Summary.newGroupArrayInGroup("pnd_Work_Variables_Pnd_Ws_Vertical_Array", 
            "#WS-VERTICAL-ARRAY", new DbsArrayController(1, 4));
        pnd_Work_Variables_Pnd_Ws_Record_Count = pnd_Work_Variables_Pnd_Ws_Vertical_Array.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Record_Count", "#WS-RECORD-COUNT", 
            FieldType.NUMERIC, 5);
        pnd_Work_Variables_Pnd_Ws_Check_Amt = pnd_Work_Variables_Pnd_Ws_Vertical_Array.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Check_Amt", "#WS-CHECK-AMT", 
            FieldType.NUMERIC, 12, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_Work_Variables_Pnd_Ws_Start.setInitialValue(0);
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(1).setInitialValue("SS");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(2).setInitialValue("MDO");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(3).setInitialValue("XX");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(4).setInitialValue("DC");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(5).setInitialValue("IA");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(6).setInitialValue("AP");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(7).setInitialValue("XX");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(8).setInitialValue("NZ");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(9).setInitialValue("AL");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(10).setInitialValue("EW");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(11).setInitialValue("OT");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp560() throws Exception
    {
        super("Fcpp560");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue("*").reset();                                                                                                        //Natural: FORMAT ( 1 ) LS = 132 PS = 60;//Natural: FORMAT ( 2 ) LS = 132 PS = 60;//Natural: RESET #WS-CHECK-DTE ( * ) #WS-WORK-DATE ( * )
        pnd_Work_Variables_Pnd_Ws_Work_Date.getValue("*").reset();
        //*  RL
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        //* *READ WORK FILE 1 #PYMNT-ADDR                             /* 10-06-1999
        //*  10-06-19
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #RPT-EXT
        while (condition(getWorkFiles().read(1, ldaFcpl190a.getPnd_Rpt_Ext())))
        {
            CheckAtStartofData349();

            if (condition(!(((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Instmt_Nbr().equals(1) || ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Instmt_Nbr().equals(8)) && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1))))) //Natural: ACCEPT IF ( PYMNT-INSTMT-NBR EQ 01 OR = 8 ) AND PYMNT-PAY-TYPE-REQ-IND EQ 1
            {
                continue;
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*  NEXT READ IS EVALUATED HERE
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().greater(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(1))))                                         //Natural: IF PYMNT-CHECK-DTE > #WS-CHECK-DTE ( 1 )
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().less(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(3))))                                        //Natural: IF PYMNT-CHECK-DTE < #WS-CHECK-DTE ( 3 )
                {
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().equals(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2))))                                  //Natural: IF PYMNT-CHECK-DTE = #WS-CHECK-DTE ( 2 )
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(3).setValue(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2));                                        //Natural: ASSIGN #WS-CHECK-DTE ( 3 ) := #WS-CHECK-DTE ( 2 )
                        pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",3).setValue(pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",2));                          //Natural: ASSIGN #WS-RECORD-COUNT ( *,3 ) := #WS-RECORD-COUNT ( *,2 )
                        pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",3).setValue(pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",2));                                //Natural: ASSIGN #WS-CHECK-AMT ( *,3 ) := #WS-CHECK-AMT ( *,2 )
                        pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",2).reset();                                                                                   //Natural: RESET #WS-RECORD-COUNT ( *,2 ) #WS-CHECK-AMT ( *,2 )
                        pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",2).reset();
                        pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                           //Natural: ASSIGN #WS-CHECK-DTE ( 2 ) := PYMNT-CHECK-DTE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().less(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2))))                                    //Natural: IF PYMNT-CHECK-DTE < #WS-CHECK-DTE ( 2 )
                    {
                        pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(3).setValue(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2));                                        //Natural: ASSIGN #WS-CHECK-DTE ( 3 ) := #WS-CHECK-DTE ( 2 )
                        pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",3).setValue(pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",2));                          //Natural: ASSIGN #WS-RECORD-COUNT ( *,3 ) := #WS-RECORD-COUNT ( *,2 )
                        pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",3).setValue(pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",2));                                //Natural: ASSIGN #WS-CHECK-AMT ( *,3 ) := #WS-CHECK-AMT ( *,2 )
                        pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",2).reset();                                                                                   //Natural: RESET #WS-RECORD-COUNT ( *,2 ) #WS-CHECK-AMT ( *,2 )
                        pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",2).reset();
                        pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                           //Natural: ASSIGN #WS-CHECK-DTE ( 2 ) := PYMNT-CHECK-DTE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2).equals(getZero())))                                                                 //Natural: IF #WS-CHECK-DTE ( 2 ) EQ 0
                        {
                            pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                       //Natural: ASSIGN #WS-CHECK-DTE ( 2 ) := PYMNT-CHECK-DTE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().greater(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2))))                         //Natural: IF PYMNT-CHECK-DTE > #WS-CHECK-DTE ( 2 )
                            {
                                pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(3).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                   //Natural: ASSIGN #WS-CHECK-DTE ( 3 ) := PYMNT-CHECK-DTE
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().less(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(1))))                                        //Natural: IF PYMNT-CHECK-DTE < #WS-CHECK-DTE ( 1 )
                {
                    pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(3).setValue(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2));                                            //Natural: ASSIGN #WS-CHECK-DTE ( 3 ) := #WS-CHECK-DTE ( 2 )
                    pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",3).setValue(pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",2));                              //Natural: ASSIGN #WS-RECORD-COUNT ( *,3 ) := #WS-RECORD-COUNT ( *,2 )
                    pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",3).setValue(pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",2));                                    //Natural: ASSIGN #WS-CHECK-AMT ( *,3 ) := #WS-CHECK-AMT ( *,2 )
                    pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2).setValue(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(1));                                            //Natural: ASSIGN #WS-CHECK-DTE ( 2 ) := #WS-CHECK-DTE ( 1 )
                    pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",2).setValue(pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",1));                              //Natural: ASSIGN #WS-RECORD-COUNT ( *,2 ) := #WS-RECORD-COUNT ( *,1 )
                    pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",2).setValue(pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",1));                                    //Natural: ASSIGN #WS-CHECK-AMT ( *,2 ) := #WS-CHECK-AMT ( *,1 )
                    pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(1).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                               //Natural: ASSIGN #WS-CHECK-DTE ( 1 ) := PYMNT-CHECK-DTE
                    pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",1).reset();                                                                                       //Natural: RESET #WS-RECORD-COUNT ( *,1 ) #WS-CHECK-AMT ( *,1 )
                    pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",1).reset();
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().equals(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(1))))                                          //Natural: IF PYMNT-CHECK-DTE = #WS-CHECK-DTE ( 1 )
            {
                pnd_Work_Variables_Pnd_Ws_Col.setValue(1);                                                                                                                //Natural: ASSIGN #WS-COL := 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().equals(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2))))                                      //Natural: IF PYMNT-CHECK-DTE = #WS-CHECK-DTE ( 2 )
                {
                    pnd_Work_Variables_Pnd_Ws_Col.setValue(2);                                                                                                            //Natural: ASSIGN #WS-COL := 2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().equals(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(3))))                                  //Natural: IF PYMNT-CHECK-DTE = #WS-CHECK-DTE ( 3 )
                    {
                        pnd_Work_Variables_Pnd_Ws_Col.setValue(3);                                                                                                        //Natural: ASSIGN #WS-COL := 3
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("NZ") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AL")  //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS' OR = 'NZ' OR = 'AL' OR = 'DC' OR = 'IA' OR = 'AP' OR = 'EW'
                || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AP") 
                || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("EW")))
            {
                ignore();
                //*  DS, MS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().setValue("OT");                                                                                              //Natural: ASSIGN #RPT-EXT.CNTRCT-ORGN-CDE := 'OT'
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS") && (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30") ||                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS' AND #RPT-EXT.CNTRCT-TYPE-CDE = '30' OR = '31'
                ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31")))))
            {
                pnd_Work_Variables_Pnd_Ws_Orgn_Cde.setValue("MDO");                                                                                                       //Natural: ASSIGN #WS-ORGN-CDE := 'MDO'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Variables_Pnd_Ws_Orgn_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());                                                                //Natural: ASSIGN #WS-ORGN-CDE := #RPT-EXT.CNTRCT-ORGN-CDE
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue("*"),true), new ExamineSearch(pnd_Work_Variables_Pnd_Ws_Orgn_Cde),        //Natural: EXAMINE FULL VALUE OF #WS-ORGN-CDE-TABLE ( * ) FOR #WS-ORGN-CDE GIVING INDEX #WS-ROW
                new ExamineGivingIndex(pnd_Work_Variables_Pnd_Ws_Row));
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(pnd_Work_Variables_Pnd_Ws_Row,pnd_Work_Variables_Pnd_Ws_Col).nadd(1);                                         //Natural: ADD 1 TO #WS-RECORD-COUNT ( #WS-ROW,#WS-COL )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(pnd_Work_Variables_Pnd_Ws_Row,pnd_Work_Variables_Pnd_Ws_Col).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt()); //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( #WS-ROW,#WS-COL )
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS'
            {
                pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,pnd_Work_Variables_Pnd_Ws_Col).nadd(1);                                                                 //Natural: ADD 1 TO #WS-RECORD-COUNT ( 3,#WS-COL )
                pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,pnd_Work_Variables_Pnd_Ws_Col).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                         //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 3,#WS-COL )
                pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,4).nadd(1);                                                                                             //Natural: ADD 1 TO #WS-RECORD-COUNT ( 3,4 )
                pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,4).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                     //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 3,4 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AP"))) //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' OR = 'IA' OR = 'AP'
            {
                pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,pnd_Work_Variables_Pnd_Ws_Col).nadd(1);                                                                 //Natural: ADD 1 TO #WS-RECORD-COUNT ( 7,#WS-COL )
                pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,pnd_Work_Variables_Pnd_Ws_Col).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                         //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 7,#WS-COL )
                pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,4).nadd(1);                                                                                             //Natural: ADD 1 TO #WS-RECORD-COUNT ( 7,4 )
                pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,4).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                     //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 7,4 )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(pnd_Work_Variables_Pnd_Ws_Row,4).nadd(1);                                                                     //Natural: ADD 1 TO #WS-RECORD-COUNT ( #WS-ROW,4 )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(pnd_Work_Variables_Pnd_Ws_Row,4).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                             //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( #WS-ROW,4 )
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,pnd_Work_Variables_Pnd_Ws_Col).nadd(1);                                                                    //Natural: ADD 1 TO #WS-RECORD-COUNT ( 12,#WS-COL )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,pnd_Work_Variables_Pnd_Ws_Col).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                            //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 12,#WS-COL )
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,4).nadd(1);                                                                                                //Natural: ADD 1 TO #WS-RECORD-COUNT ( 12,4 )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,4).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                        //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 12,4 )
                                                                                                                                                                          //Natural: PERFORM WRITE-DETAIL-LINE
            sub_Write_Detail_Line();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*    WRITE SUMMARY TOTALS HERE
        if (condition(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(1).greater(getZero())))                                                                                //Natural: IF #WS-CHECK-DTE ( 1 ) > 0
        {
            pnd_Work_Variables_Pnd_Ws_Work_Date.getValue(1).setValueEdited(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(1),new ReportEditMask("MM/DD/YYYY"));             //Natural: MOVE EDITED #WS-CHECK-DTE ( 1 ) ( EM = MM/DD/YYYY ) TO #WS-WORK-DATE ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2).greater(getZero())))                                                                                //Natural: IF #WS-CHECK-DTE ( 2 ) > 0
        {
            pnd_Work_Variables_Pnd_Ws_Work_Date.getValue(2).setValueEdited(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(2),new ReportEditMask("MM/DD/YYYY"));             //Natural: MOVE EDITED #WS-CHECK-DTE ( 2 ) ( EM = MM/DD/YYYY ) TO #WS-WORK-DATE ( 2 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(3).greater(getZero())))                                                                                //Natural: IF #WS-CHECK-DTE ( 3 ) > 0
        {
            pnd_Work_Variables_Pnd_Ws_Work_Date.getValue(3).setValueEdited(pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(3),new ReportEditMask("MM/DD/YYYY"));             //Natural: MOVE EDITED #WS-CHECK-DTE ( 3 ) ( EM = MM/DD/YYYY ) TO #WS-WORK-DATE ( 3 )
        }                                                                                                                                                                 //Natural: END-IF
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT 001T *INIT-USER '-' *PROGRAM 056T 'CONSOLIDATED PAYMENT SYSTEM' 117T 'PAGE ' *PAGE-NUMBER ( 2 ) ( EM = ZZ9 ) / 001T *DATX '-' *TIMX ( EM = HH':'II' 'AP ) 056T '  TREASURY CHECK REGISTER' / 056T '   SUMMARY BY CHECK DATE' / 064T *DATX ( EM = LLL' 'DD', 'YYYY )
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,new TabSetting(29),pnd_Work_Variables_Pnd_Ws_Work_Date.getValue(1),new TabSetting(57),pnd_Work_Variables_Pnd_Ws_Work_Date.getValue(2),new  //Natural: WRITE ( 2 ) /// 029T #WS-WORK-DATE ( 1 ) 057T #WS-WORK-DATE ( 2 ) 085T #WS-WORK-DATE ( 3 ) 115T 'TOTALS' / 023T '-' ( 23 ) 050T '-' ( 23 ) 078T '-' ( 23 ) 107T '-' ( 23 )
            TabSetting(85),pnd_Work_Variables_Pnd_Ws_Work_Date.getValue(3),new TabSetting(115),"TOTALS",NEWLINE,new TabSetting(23),"-",new RepeatItem(23),new 
            TabSetting(50),"-",new RepeatItem(23),new TabSetting(78),"-",new RepeatItem(23),new TabSetting(107),"-",new RepeatItem(23));
        if (Global.isEscape()) return;
        getReports().write(2, NEWLINE,NEWLINE,new TabSetting(16),"SS",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(1,1), new ReportEditMask         //Natural: WRITE ( 2 ) // 016T 'SS' 022T #WS-RECORD-COUNT ( 1,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 1,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 1,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 1,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 1,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 1,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 1,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 1,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 015T 'MDO' 022T #WS-RECORD-COUNT ( 2,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 2,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 2,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 2,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 2,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 2,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 2,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 2,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 010T 'Single Sum' 022T #WS-RECORD-COUNT ( 3,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 3,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 3,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 3,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 3,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 3,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 3,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 3,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 016T 'DC' 022T #WS-RECORD-COUNT ( 4,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 4,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 4,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 4,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 4,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 4,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 4,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 4,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 016T 'IA' 022T #WS-RECORD-COUNT ( 5,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 5,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 5,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 5,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 5,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 5,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 5,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 5,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 016T 'AP' 022T #WS-RECORD-COUNT ( 6,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 6,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 6,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 6,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 6,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 6,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 6,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 6,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 010T 'Annuity' 022T #WS-RECORD-COUNT ( 7,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 7,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 7,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 7,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 7,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 7,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 7,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 7,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 010T 'ADAM  NZ' 022T #WS-RECORD-COUNT ( 8,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 8,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 8,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 8,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 8,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 8,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 8,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 8,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 010T 'Loan  AL' 022T #WS-RECORD-COUNT ( 9,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 9,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 9,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 9,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 9,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 9,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 9,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 9,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 010T 'E Warrant' 022T #WS-RECORD-COUNT ( 10,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 10,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 10,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 10,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 10,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 10,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 10,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 10,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 010T 'Others  ' 022T #WS-RECORD-COUNT ( 11,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 11,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 11,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 11,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 11,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 11,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 11,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 11,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 010T 'TOTALS  ' 022T #WS-RECORD-COUNT ( 12,1 ) ( EM = ZZ,ZZ9 ) 030T #WS-CHECK-AMT ( 12,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 050T #WS-RECORD-COUNT ( 12,2 ) ( EM = ZZ,ZZ9 ) 057T #WS-CHECK-AMT ( 12,2 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 078T #WS-RECORD-COUNT ( 12,3 ) ( EM = ZZ,ZZ9 ) 085T #WS-CHECK-AMT ( 12,3 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) 106T #WS-RECORD-COUNT ( 12,4 ) ( EM = ZZ,ZZ9 ) 114T #WS-CHECK-AMT ( 12,4 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(1,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(1,2), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(1,2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(1,3), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(1,3), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(1,4), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(1,4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(15),"MDO",new 
            TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(2,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(2,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(2,2), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(2,2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(2,3), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(2,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(2,4), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(2,4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(10),"Single Sum",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,2), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,3), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,4), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new 
            TabSetting(16),"DC",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(4,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(4,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(4,2), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(4,2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(4,3), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(4,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(4,4), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(4,4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(16),"IA",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(5,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(5,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(5,2), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(5,2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(5,3), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(5,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(5,4), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(5,4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(16),"AP",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(6,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(6,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(6,2), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(6,2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(6,3), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(6,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(6,4), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(6,4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(10),"Annuity",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,2), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,3), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,4), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"ADAM  NZ",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(8,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(8,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(8,2), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(8,2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(8,3), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(8,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(8,4), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(8,4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new TabSetting(10),"Loan  AL",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(9,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(9,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(9,2), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(9,2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(9,3), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(9,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(9,4), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(9,4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"E Warrant",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(10,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(10,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(10,2), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(10,2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(10,3), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(10,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(10,4), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(10,4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new TabSetting(10),"Others  ",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(11,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(11,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(11,2), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(11,2), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(11,3), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(11,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(11,4), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(11,4), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"TOTALS  ",new TabSetting(22),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(30),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,2), new ReportEditMask ("ZZ,ZZ9"),new 
            TabSetting(57),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,2), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new TabSetting(78),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,3), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(85),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,3), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),new 
            TabSetting(106),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,4), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(114),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,4), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  -------------------- SUBROUTINES ----------------------------------*
        //*  -------------------------------------------------------------------*
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DETAIL-LINE
        //*  -------------------------------------------------------------------*
        //* *001T #RPT-EXT.PYMNT-CHECK-NBR
        //* *011T #RPT-EXT.PYMNT-CHECK-AMT(EM=Z,ZZZ,ZZ9.99)
        //* *025T #RPT-EXT.PYMNT-CHECK-DTE(EM=MM/DD/YY)
        //* *036T #RPT-EXT.PYMNT-ACCTG-DTE(EM=MM/DD/YY)
        //* *048T #WS-ORGN-CDE
        //* *056T #RPT-EXT.CNTRCT-PPCN-NBR(EM=XXXXXXX-X)
        //* *070T #RPT-EXT.CNTRCT-PAYEE-CDE
        //* *081T #RPT-EXT.ANNT-RSDNCY-CDE
        //* *086T #RPT-EXT.PYMNT-NME(1)
        //* *********************** RL BEGIN - PAYEE MATCH ************MAR 06,2006
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //*  RL PAYEE MATCH
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  RL PAYEE MATCH
        //* ************************** RL END-PAYEE MATCH *************************
        //*  -------------------- SUBROUTINES ----------------------------------*
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 001T *INIT-USER '-' *PROGRAM 056T 'CONSOLIDATED PAYMENT SYSTEM' 117T 'PAGE ' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / 001T *DATX '-' *TIMX ( EM = HH':'II' 'AP ) 056T '  TREASURY CHECK REGISTER' / 064T *DATX ( EM = LLL' 'DD', 'YYYY ) // 001T 'CHECK NO' 014T 'CHECK AMOUNT' 027T 'CHECK DATE' 038T 'ACTG DATE' 049T 'ORGN CDE' 058T 'CONTRACT NO' 071T 'PAYEE CDE' 083T 'GEO' 104T 'ANNUITANT' / 001T '-----------' 014T '------------' 027T '----------' 038T '---------' 049T '--------' 058T '-----------' 071T '---------' 083T '---' 088T '-' ( 42 )
        //* * 011T  'CHECK AMOUNT'
        //* * 024T  'CHECK DATE'
        //* * 035T  'ACTG DATE'
        //* * 046T  'ORGN CDE'
        //* * 055T  'CONTRACT NO'
        //* * 068T  'PAYEE CDE'
        //* * 080T  'GEO'
        //* * 101T 'ANNUITANT'
        //* */001T  '--------'
        //* * 011T  '------------'
        //* * 024T  '----------'
        //* * 035T  '---------'
        //* * 046T  '--------'
        //* * 055T  '-----------'
        //* * 068T  '---------'
        //* * 080T  '---'
        //* * 085T  '-'(45)
    }
    //*  RL PAYEE MATCH
    //*  RL PAYEE MATCH
    private void sub_Write_Detail_Line() throws Exception                                                                                                                 //Natural: WRITE-DETAIL-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Check_Number_N10_Pnd_Check_Number_N7.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr());                                                                  //Natural: ASSIGN #CHECK-NUMBER-N7 := #RPT-EXT.PYMNT-CHECK-NBR
        pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde().equals(" ")))                                                                                         //Natural: IF #RPT-EXT.CNTRCT-PAYEE-CDE = ' '
        {
            if (condition(DbsUtil.maskMatches(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1),"'CR '")))                                                               //Natural: IF #RPT-EXT.PYMNT-NME ( 1 ) = MASK ( 'CR ' )
            {
                ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde().setValue("ALT");                                                                                            //Natural: ASSIGN #RPT-EXT.CNTRCT-PAYEE-CDE := 'ALT'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde().setValue("ANNT");                                                                                           //Natural: ASSIGN #RPT-EXT.CNTRCT-PAYEE-CDE := 'ANNT'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Check_Number_N10_Pnd_Check_Number_A10,new TabSetting(14),ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt(),  //Natural: WRITE ( 1 ) NOTITLE 001T #CHECK-NUMBER-A10 014T #RPT-EXT.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99 ) 028T #RPT-EXT.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) 039T #RPT-EXT.PYMNT-ACCTG-DTE ( EM = MM/DD/YY ) 051T #WS-ORGN-CDE 059T #RPT-EXT.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 073T #RPT-EXT.CNTRCT-PAYEE-CDE 084T #RPT-EXT.ANNT-RSDNCY-CDE 089T #RPT-EXT.PYMNT-NME ( 1 )
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(28),ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(), new ReportEditMask ("MM/DD/YY"),new TabSetting(39),ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte(), 
            new ReportEditMask ("MM/DD/YY"),new TabSetting(51),pnd_Work_Variables_Pnd_Ws_Orgn_Cde,new TabSetting(59),ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr(), 
            new ReportEditMask ("XXXXXXX-X"),new TabSetting(73),ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde(),new TabSetting(84),ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde(),new 
            TabSetting(89),ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1));
        if (Global.isEscape()) return;
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE FCPA110.START-CHECK-PREFIX-N3 TO #CHECK-NUMBER-N3          /*RL
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=60");
        Global.format(2, "LS=132 PS=60");

        getReports().write(2, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(117),"PAGE ",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ9"),NEWLINE,new 
            TabSetting(1),Global.getDATX(),"-",Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),new TabSetting(56),"  TREASURY CHECK REGISTER",NEWLINE,new 
            TabSetting(56),"   SUMMARY BY CHECK DATE",NEWLINE,new TabSetting(64),Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"));
        getReports().write(1, ReportOption.NOTITLE,pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(117),"PAGE ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,new 
            TabSetting(1),Global.getDATX(),"-",Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),new TabSetting(56),"  TREASURY CHECK REGISTER",NEWLINE,new 
            TabSetting(64),Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"CHECK NO",new TabSetting(14),"CHECK AMOUNT",new 
            TabSetting(27),"CHECK DATE",new TabSetting(38),"ACTG DATE",new TabSetting(49),"ORGN CDE",new TabSetting(58),"CONTRACT NO",new TabSetting(71),"PAYEE CDE",new 
            TabSetting(83),"GEO",new TabSetting(104),"ANNUITANT",NEWLINE,new TabSetting(1),"-----------",new TabSetting(14),"------------",new TabSetting(27),"----------",new 
            TabSetting(38),"---------",new TabSetting(49),"--------",new TabSetting(58),"-----------",new TabSetting(71),"---------",new TabSetting(83),"---",new 
            TabSetting(88),"-",new RepeatItem(42));
    }
    private void CheckAtStartofData349() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Work_Variables_Pnd_Ws_Check_Dte.getValue(1).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                       //Natural: ASSIGN #WS-CHECK-DTE ( 1 ) := PYMNT-CHECK-DTE
            pnd_Work_Variables_Pnd_Ws_Orgn_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());                                                                    //Natural: ASSIGN #WS-ORGN-CDE := #RPT-EXT.CNTRCT-ORGN-CDE
            pnd_Work_Variables_Pnd_Ws_Start.setValue(1);                                                                                                                  //Natural: ASSIGN #WS-START := 1
        }                                                                                                                                                                 //Natural: END-START
    }
}
