/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:41 PM
**        * FROM NATURAL PROGRAM : Fcpp257
************************************************************
**        * FILE NAME            : Fcpp257.java
**        * CLASS NAME           : Fcpp257
**        * INSTANCE NAME        : Fcpp257
************************************************************
* FCPP257 - UPDATE STOP-CANCEL-STATUS.
*
* READ A WORK FILE CONTAINING ISNS, GET THE RECORDS, AND UPDATE THE
* CNR-CS-STOP-CANCEL-STATUS AND, POSSIBLY, ASSOCIATED FIELDS.
*
* THE RUN IS CONTROLLED BY WHAT IS ON THE PARAMETER CARD READ FROM
* WORK FILE 1.  THERE ARE 5 FIELDS THAT ARE ON THE CARD.
*
* 1.  RUN TYPE: THIS IS AN 'N' OR AN 'R'.  MOST OF THE TIME, THE VALUE
*     --------  WILL BE AN 'N' (NORMAL) RUN AND WE WILL USE *DATN FOR
*               ANY DATE UPDATING THAT WE DO.  OCCASIONALLY, WE MAY
*               NEED TO USE AN 'R' (REPROCESS) AND THIS WILL GIVE US
*               THE OPTION OF USING *DATN OR A DATE ENTERED ON THE PARM
*               CARD.
*
* 2.  ACTION-TYPE: THIS IS AN 'S' OR A 'T'.  IF WE ARE USING THIS
*     -----------  PROGRAM FOLLOWING ANOTHER PROGRAM THAT SELECTS STOPS
*                  OR CANCELS FOR TRANSMISSION, WE USE AN 'S' (SELECT).
*                  IF WE ARE USING IT IN A JOB THAT TRANSMITS THE VOIDS,
*                  WE USE A 'T' (TRANSMIT).  THIS FIELD TELLS US WHAT
*                  DATE FIELDS TO UPDATE.
*
* 3.  DATE-UPDATE: THIS IS A 'Y' OR AN 'N'.  DO WE WANT TO UPDATE THE
*     -----------  DATE FIELDS OR LEAVE THEM AS IS.  FOR A NORMAL RUN,
*                  WE WILL ALWAYS WANT TO UPDATE THE DATE FIELDS.  FOR
*                  A REPROCESS RUN, WE MAY WANT TO LEAVE THE ORIGINAL
*                  SELECTION AND/OR TRANSMIT DATE AS IS.
*
* 4.  NEW-STOP-UPDATE: THIS IS A 'Y' OR AN 'N'.  DO WE WANT TO MOVE A
*     ---------------  SPACE TO THE CNR-CS-NEW-STOP-IND FIELD?  FOR
*                      NORMAL RUNS, WE ALMOST ALWAYS WILL.
*
* 5.  OVERRIDE-DT: THIS IS EITHER BLANK OR A VALID DATE IN THE FORMAT
*     -----------  CCYYMMDD.  IF A DATE IS PRESENT AND IT'S A NORMAL
*                  RUN, WE CONSIDER IT AN ERROR.  IF A DATE IS PRESENT
*                  AND IT'S A REPROCESS RUN, WE'LL USE THE OVERRIDE-DT
*                  INSTEAD OF *DATN TO UPDATE THE DATE FIELDS.
*
* 4/2017  : JJG - PIN EXPANSION

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp257 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Stats_Cde;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField fcp_Cons_Pymnt_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Amt;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Stop_Select_Dt;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Cancel_Select_Dt;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id;
    private DbsField pnd_Get_Isn;
    private DbsField pnd_Parm_Card_In;

    private DbsGroup pnd_Parm_Card_In__R_Field_1;
    private DbsField pnd_Parm_Card_In_Pnd_Run_Type;
    private DbsField pnd_Parm_Card_In_Pnd_Action_Type;
    private DbsField pnd_Parm_Card_In_Pnd_Date_Update;
    private DbsField pnd_Parm_Card_In_Pnd_New_Stop_Update;
    private DbsField pnd_Parm_Card_In_Pnd_Override_Dt;

    private DbsGroup pnd_Parm_Card_In__R_Field_2;
    private DbsField pnd_Parm_Card_In_Pnd_Override_Dt_N;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Update_Dt;

    private DbsGroup pnd_Update_Dt__R_Field_3;
    private DbsField pnd_Update_Dt_Pnd_Update_Dt_A;
    private DbsField pnd_Time;
    private DbsField pnd_Isns_Read;
    private DbsField pnd_Stops_Updated;
    private DbsField pnd_Cancels_Updated;
    private DbsField pnd_Transmits_Updated;
    private DbsField pnd_Invalid_Flag;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        fcp_Cons_Pymnt_Pymnt_Stats_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_STATS_CDE");
        fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNR_CS_ACTVTY_CDE");
        fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_ACCTG_DTE");
        fcp_Cons_Pymnt_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnt_Pymnt_Check_Amt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        fcp_Cons_Pymnt_Pymnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNR_CS_NEW_STOP_IND");
        fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status", "CNR-CS-STOP-CANCEL-STATUS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNR_CS_STOP_CANCEL_STATUS");
        fcp_Cons_Pymnt_Cnr_Cs_Stop_Select_Dt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Stop_Select_Dt", "CNR-CS-STOP-SELECT-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNR_CS_STOP_SELECT_DT");
        fcp_Cons_Pymnt_Cnr_Cs_Cancel_Select_Dt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Cancel_Select_Dt", "CNR-CS-CANCEL-SELECT-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNR_CS_CANCEL_SELECT_DT");
        fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt", "CNR-CS-TRANSMIT-DT", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNR_CS_TRANSMIT_DT");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_RQUST_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "CNR_CS_RQUST_TME");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_USER_ID");
        registerRecord(vw_fcp_Cons_Pymnt);

        pnd_Get_Isn = localVariables.newFieldInRecord("pnd_Get_Isn", "#GET-ISN", FieldType.NUMERIC, 11);
        pnd_Parm_Card_In = localVariables.newFieldInRecord("pnd_Parm_Card_In", "#PARM-CARD-IN", FieldType.STRING, 80);

        pnd_Parm_Card_In__R_Field_1 = localVariables.newGroupInRecord("pnd_Parm_Card_In__R_Field_1", "REDEFINE", pnd_Parm_Card_In);
        pnd_Parm_Card_In_Pnd_Run_Type = pnd_Parm_Card_In__R_Field_1.newFieldInGroup("pnd_Parm_Card_In_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 1);
        pnd_Parm_Card_In_Pnd_Action_Type = pnd_Parm_Card_In__R_Field_1.newFieldInGroup("pnd_Parm_Card_In_Pnd_Action_Type", "#ACTION-TYPE", FieldType.STRING, 
            1);
        pnd_Parm_Card_In_Pnd_Date_Update = pnd_Parm_Card_In__R_Field_1.newFieldInGroup("pnd_Parm_Card_In_Pnd_Date_Update", "#DATE-UPDATE", FieldType.STRING, 
            1);
        pnd_Parm_Card_In_Pnd_New_Stop_Update = pnd_Parm_Card_In__R_Field_1.newFieldInGroup("pnd_Parm_Card_In_Pnd_New_Stop_Update", "#NEW-STOP-UPDATE", 
            FieldType.STRING, 1);
        pnd_Parm_Card_In_Pnd_Override_Dt = pnd_Parm_Card_In__R_Field_1.newFieldInGroup("pnd_Parm_Card_In_Pnd_Override_Dt", "#OVERRIDE-DT", FieldType.STRING, 
            8);

        pnd_Parm_Card_In__R_Field_2 = pnd_Parm_Card_In__R_Field_1.newGroupInGroup("pnd_Parm_Card_In__R_Field_2", "REDEFINE", pnd_Parm_Card_In_Pnd_Override_Dt);
        pnd_Parm_Card_In_Pnd_Override_Dt_N = pnd_Parm_Card_In__R_Field_2.newFieldInGroup("pnd_Parm_Card_In_Pnd_Override_Dt_N", "#OVERRIDE-DT-N", FieldType.NUMERIC, 
            8);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Update_Dt = localVariables.newFieldInRecord("pnd_Update_Dt", "#UPDATE-DT", FieldType.NUMERIC, 8);

        pnd_Update_Dt__R_Field_3 = localVariables.newGroupInRecord("pnd_Update_Dt__R_Field_3", "REDEFINE", pnd_Update_Dt);
        pnd_Update_Dt_Pnd_Update_Dt_A = pnd_Update_Dt__R_Field_3.newFieldInGroup("pnd_Update_Dt_Pnd_Update_Dt_A", "#UPDATE-DT-A", FieldType.STRING, 8);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.NUMERIC, 4);
        pnd_Isns_Read = localVariables.newFieldInRecord("pnd_Isns_Read", "#ISNS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Stops_Updated = localVariables.newFieldInRecord("pnd_Stops_Updated", "#STOPS-UPDATED", FieldType.PACKED_DECIMAL, 7);
        pnd_Cancels_Updated = localVariables.newFieldInRecord("pnd_Cancels_Updated", "#CANCELS-UPDATED", FieldType.PACKED_DECIMAL, 7);
        pnd_Transmits_Updated = localVariables.newFieldInRecord("pnd_Transmits_Updated", "#TRANSMITS-UPDATED", FieldType.PACKED_DECIMAL, 7);
        pnd_Invalid_Flag = localVariables.newFieldInRecord("pnd_Invalid_Flag", "#INVALID-FLAG", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp257() throws Exception
    {
        super("Fcpp257");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 55 LS = 80;//Natural: FORMAT ( 1 ) PS = 55 LS = 80
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE TITLE LEFT *DATU 7X 'VOID RECONCILIATION PROCESS - UPDATE SELECT STATUS' 6X *PROGRAM / *TIME ( EM = X ( 8 ) ) 7X '--------------------------------------------------' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU 6X 'VOID RECONCILIATION PROCESS - SELECT STATUS UPDATED' 6X *PROGRAM / *TIME ( EM = X ( 8 ) ) 6X '---------------------------------------------------' 6X 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = Z9 ) // 'STOP  NEW                ' 5X '           ACT' / 'STAT STOP ORGN STAT   PIN' 5X 'CHECK NBR  CD   ACCTG DT   RQST DT' 'RQ TM RQ USER' / '---- ---- ---- ----   ---' 5X '---------  ---  --------   -------' '----- -------' //
                                                                                                                                                                          //Natural: PERFORM P1000-RETRIEVE-PARM-INFO
        sub_P1000_Retrieve_Parm_Info();
        if (condition(Global.isEscape())) {return;}
        RW2:                                                                                                                                                              //Natural: READ WORK FILE 2 #GET-ISN
        while (condition(getWorkFiles().read(2, pnd_Get_Isn)))
        {
            pnd_Isns_Read.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #ISNS-READ
            GT1:                                                                                                                                                          //Natural: GET FCP-CONS-PYMNT #GET-ISN
            vw_fcp_Cons_Pymnt.readByID(pnd_Get_Isn.getLong(), "GT1");
            pnd_Time.compute(new ComputeParameters(false, pnd_Time), fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme.divide(1000));                                                       //Natural: COMPUTE #TIME = CNR-CS-RQUST-TME / 1000
            getReports().write(1, new ColumnSpacing(2),fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status,new ColumnSpacing(4),fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind,new               //Natural: WRITE ( 1 ) 2X CNR-CS-STOP-CANCEL-STATUS 4X CNR-CS-NEW-STOP-IND 3X CNTRCT-ORGN-CDE 4X PYMNT-STATS-CDE CNTRCT-UNQ-ID-NBR PYMNT-NBR 3X CNR-CS-ACTVTY-CDE 3X CNR-CS-PYMNT-ACCTG-DTE 2X CNR-CS-RQUST-DTE #TIME ( EM = 99:99 ) CNR-CS-RQUST-USER-ID
                ColumnSpacing(3),fcp_Cons_Pymnt_Cntrct_Orgn_Cde,new ColumnSpacing(4),fcp_Cons_Pymnt_Pymnt_Stats_Cde,fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr,fcp_Cons_Pymnt_Pymnt_Nbr,new 
                ColumnSpacing(3),fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde,new ColumnSpacing(3),fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte,new ColumnSpacing(2),fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte,pnd_Time, 
                new ReportEditMask ("99:99"),fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RW2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RW2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Parm_Card_In_Pnd_Date_Update.equals("Y")))                                                                                                  //Natural: IF #DATE-UPDATE = 'Y'
            {
                if (condition(pnd_Parm_Card_In_Pnd_Action_Type.equals("T")))                                                                                              //Natural: IF #ACTION-TYPE = 'T'
                {
                    fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt.setValue(pnd_Update_Dt);                                                                                            //Natural: ASSIGN CNR-CS-TRANSMIT-DT = #UPDATE-DT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    short decideConditionsMet117 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNR-CS-STOP-CANCEL-STATUS = 'A'
                    if (condition(fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status.equals("A")))
                    {
                        decideConditionsMet117++;
                        fcp_Cons_Pymnt_Cnr_Cs_Stop_Select_Dt.setValue(pnd_Update_Dt);                                                                                     //Natural: ASSIGN CNR-CS-STOP-SELECT-DT = #UPDATE-DT
                    }                                                                                                                                                     //Natural: WHEN CNR-CS-STOP-CANCEL-STATUS = 'B'
                    else if (condition(fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status.equals("B")))
                    {
                        decideConditionsMet117++;
                        fcp_Cons_Pymnt_Cnr_Cs_Cancel_Select_Dt.setValue(pnd_Update_Dt);                                                                                   //Natural: ASSIGN CNR-CS-CANCEL-SELECT-DT = #UPDATE-DT
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Parm_Card_In_Pnd_New_Stop_Update.equals("Y")))                                                                                              //Natural: IF #NEW-STOP-UPDATE = 'Y'
            {
                fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.setValue(" ");                                                                                                         //Natural: ASSIGN CNR-CS-NEW-STOP-IND = ' '
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet132 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNR-CS-STOP-CANCEL-STATUS = 'A'
            if (condition(fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status.equals("A")))
            {
                decideConditionsMet132++;
                fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status.setValue("C");                                                                                                   //Natural: ASSIGN CNR-CS-STOP-CANCEL-STATUS = 'C'
                pnd_Stops_Updated.nadd(1);                                                                                                                                //Natural: ADD 1 TO #STOPS-UPDATED
            }                                                                                                                                                             //Natural: WHEN CNR-CS-STOP-CANCEL-STATUS = 'B'
            else if (condition(fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status.equals("B")))
            {
                decideConditionsMet132++;
                fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status.setValue("C");                                                                                                   //Natural: ASSIGN CNR-CS-STOP-CANCEL-STATUS = 'C'
                pnd_Cancels_Updated.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CANCELS-UPDATED
            }                                                                                                                                                             //Natural: WHEN CNR-CS-STOP-CANCEL-STATUS = 'C'
            else if (condition(fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status.equals("C")))
            {
                decideConditionsMet132++;
                pnd_Transmits_Updated.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TRANSMITS-UPDATED
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Invalid_Flag.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #INVALID-FLAG
            }                                                                                                                                                             //Natural: END-DECIDE
            vw_fcp_Cons_Pymnt.updateDBRow("GT1");                                                                                                                         //Natural: UPDATE ( GT1. )
            pnd_Et_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ET-COUNT
            if (condition(pnd_Et_Count.greater(24)))                                                                                                                      //Natural: IF #ET-COUNT > 24
            {
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
                pnd_Et_Count.reset();                                                                                                                                     //Natural: RESET #ET-COUNT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        RW2_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
                                                                                                                                                                          //Natural: PERFORM P9999-WRITE-CONTROL-TOTALS
        sub_P9999_Write_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.stop();  if (true) return;                                                                                                                                //Natural: STOP
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: P1000-RETRIEVE-PARM-INFO
        //* *****************************************
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: P9999-WRITE-CONTROL-TOTALS
    }
    private void sub_P1000_Retrieve_Parm_Info() throws Exception                                                                                                          //Natural: P1000-RETRIEVE-PARM-INFO
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().read(1, pnd_Parm_Card_In);                                                                                                                         //Natural: READ WORK FILE 1 ONCE #PARM-CARD-IN
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, new ColumnSpacing(24),"********************************",NEWLINE,new ColumnSpacing(24),"*  PARAMETER CARD IS MISSING.  *",NEWLINE,new   //Natural: WRITE 24X '********************************' / 24X '*  PARAMETER CARD IS MISSING.  *' / 24X '*     JOB IS TERMINATING.      *' / 24X '********************************'
                ColumnSpacing(24),"*     JOB IS TERMINATING.      *",NEWLINE,new ColumnSpacing(24),"********************************");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(! (pnd_Parm_Card_In_Pnd_Run_Type.equals("N") || pnd_Parm_Card_In_Pnd_Run_Type.equals("R"))))                                                        //Natural: IF NOT ( #RUN-TYPE = 'N' OR = 'R' )
        {
            getReports().write(0, new ColumnSpacing(20),"***************************************",NEWLINE,new ColumnSpacing(20),"*  PARM RUN TYPE MUST BE 'N' OR 'R'.  *",NEWLINE,new  //Natural: WRITE 20X '***************************************' / 20X '*  PARM RUN TYPE MUST BE "N" OR "R".  *' / 20X '*         JOB IS TERMINATING.         *' / 20X '***************************************'
                ColumnSpacing(20),"*         JOB IS TERMINATING.         *",NEWLINE,new ColumnSpacing(20),"***************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(20),"=",pnd_Parm_Card_In_Pnd_Run_Type);                                                                       //Natural: WRITE / 20X '=' #RUN-TYPE
            if (Global.isEscape()) return;
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Parm_Card_In_Pnd_Action_Type.equals("S") || pnd_Parm_Card_In_Pnd_Action_Type.equals("T"))))                                                  //Natural: IF NOT ( #ACTION-TYPE = 'S' OR = 'T' )
        {
            getReports().write(0, new ColumnSpacing(19),"******************************************",NEWLINE,new ColumnSpacing(19),"*  PARM ACTION TYPE MUST BE 'S' OR 'T'.  *",NEWLINE,new  //Natural: WRITE 19X '******************************************' / 19X '*  PARM ACTION TYPE MUST BE "S" OR "T".  *' / 19X '*           JOB IS TERMINATING.          *' / 19X '******************************************'
                ColumnSpacing(19),"*           JOB IS TERMINATING.          *",NEWLINE,new ColumnSpacing(19),"******************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(19),"=",pnd_Parm_Card_In_Pnd_Action_Type);                                                                    //Natural: WRITE / 19X '=' #ACTION-TYPE
            if (Global.isEscape()) return;
            DbsUtil.terminate(92);  if (true) return;                                                                                                                     //Natural: TERMINATE 92
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Parm_Card_In_Pnd_Date_Update.equals("Y") || pnd_Parm_Card_In_Pnd_Date_Update.equals("N"))))                                                  //Natural: IF NOT ( #DATE-UPDATE = 'Y' OR = 'N' )
        {
            getReports().write(0, new ColumnSpacing(19),"******************************************",NEWLINE,new ColumnSpacing(19),"*  PARM DATE UPDATE MUST BE 'Y' OR 'N'.  *",NEWLINE,new  //Natural: WRITE 19X '******************************************' / 19X '*  PARM DATE UPDATE MUST BE "Y" OR "N".  *' / 19X '*           JOB IS TERMINATING.          *' / 19X '******************************************'
                ColumnSpacing(19),"*           JOB IS TERMINATING.          *",NEWLINE,new ColumnSpacing(19),"******************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(19),"=",pnd_Parm_Card_In_Pnd_Date_Update);                                                                    //Natural: WRITE / 19X '=' #DATE-UPDATE
            if (Global.isEscape()) return;
            DbsUtil.terminate(93);  if (true) return;                                                                                                                     //Natural: TERMINATE 93
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Parm_Card_In_Pnd_New_Stop_Update.equals("Y") || pnd_Parm_Card_In_Pnd_New_Stop_Update.equals("N"))))                                          //Natural: IF NOT ( #NEW-STOP-UPDATE = 'Y' OR = 'N' )
        {
            getReports().write(0, new ColumnSpacing(17),"**********************************************",NEWLINE,new ColumnSpacing(17),"*  PARM NEW STOP UPDATE MUST BE 'Y' OR 'N'.  *",NEWLINE,new  //Natural: WRITE 17X '**********************************************' / 17X '*  PARM NEW STOP UPDATE MUST BE "Y" OR "N".  *' / 17X '*             JOB IS TERMINATING.            *' / 17X '**********************************************'
                ColumnSpacing(17),"*             JOB IS TERMINATING.            *",NEWLINE,new ColumnSpacing(17),"**********************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_In_Pnd_New_Stop_Update);                                                                //Natural: WRITE / 17X '=' #NEW-STOP-UPDATE
            if (Global.isEscape()) return;
            DbsUtil.terminate(94);  if (true) return;                                                                                                                     //Natural: TERMINATE 94
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Card_In_Pnd_Override_Dt.equals(" ") || DbsUtil.maskMatches(pnd_Parm_Card_In_Pnd_Override_Dt,"YYYYMMDD")))                                  //Natural: IF #OVERRIDE-DT = ' ' OR #OVERRIDE-DT = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, new ColumnSpacing(17),"**********************************************",NEWLINE,new ColumnSpacing(17),"*  OVERRIDE DATE MUST BE BLANK OR YYYYMMDD   *",NEWLINE,new  //Natural: WRITE 17X '**********************************************' / 17X '*  OVERRIDE DATE MUST BE BLANK OR YYYYMMDD   *' / 17X '*             JOB IS TERMINATING.            *' / 17X '**********************************************'
                ColumnSpacing(17),"*             JOB IS TERMINATING.            *",NEWLINE,new ColumnSpacing(17),"**********************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_In_Pnd_Override_Dt);                                                                    //Natural: WRITE / 17X '=' #OVERRIDE-DT
            if (Global.isEscape()) return;
            DbsUtil.terminate(95);  if (true) return;                                                                                                                     //Natural: TERMINATE 95
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Card_In_Pnd_Run_Type.equals("N") && pnd_Parm_Card_In_Pnd_Override_Dt.notEquals(" ")))                                                      //Natural: IF #RUN-TYPE = 'N' AND #OVERRIDE-DT NOT = ' '
        {
            getReports().write(0, new ColumnSpacing(17),"**********************************************",NEWLINE,new ColumnSpacing(17),"* CANNOT USE OVERRIDE DATE FOR A NORMAL RUN. *",NEWLINE,new  //Natural: WRITE 17X '**********************************************' / 17X '* CANNOT USE OVERRIDE DATE FOR A NORMAL RUN. *' / 17X '*             JOB IS TERMINATING.            *' / 17X '**********************************************'
                ColumnSpacing(17),"*             JOB IS TERMINATING.            *",NEWLINE,new ColumnSpacing(17),"**********************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_In_Pnd_Run_Type);                                                                       //Natural: WRITE / 17X '=' #RUN-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_In_Pnd_Override_Dt);                                                                    //Natural: WRITE / 17X '=' #OVERRIDE-DT
            if (Global.isEscape()) return;
            DbsUtil.terminate(96);  if (true) return;                                                                                                                     //Natural: TERMINATE 96
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Card_In_Pnd_Override_Dt.notEquals(" ") && pnd_Parm_Card_In_Pnd_Override_Dt_N.greater(Global.getDATN())))                                   //Natural: IF #OVERRIDE-DT NOT = ' ' AND #OVERRIDE-DT-N > *DATN
        {
            getReports().write(0, new ColumnSpacing(17),"**********************************************",NEWLINE,new ColumnSpacing(17),"*  OVERRIDE DATE CANNOT BE IN THE FUTURE.    *",NEWLINE,new  //Natural: WRITE 17X '**********************************************' / 17X '*  OVERRIDE DATE CANNOT BE IN THE FUTURE.    *' / 17X '*             JOB IS TERMINATING.            *' / 17X '**********************************************'
                ColumnSpacing(17),"*             JOB IS TERMINATING.            *",NEWLINE,new ColumnSpacing(17),"**********************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_In_Pnd_Override_Dt);                                                                    //Natural: WRITE / 17X '=' #OVERRIDE-DT
            if (Global.isEscape()) return;
            DbsUtil.terminate(97);  if (true) return;                                                                                                                     //Natural: TERMINATE 97
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Card_In_Pnd_Run_Type.equals("N") && pnd_Parm_Card_In_Pnd_Date_Update.equals("N")))                                                         //Natural: IF #RUN-TYPE = 'N' AND #DATE-UPDATE = 'N'
        {
            getReports().write(0, new ColumnSpacing(17),"**********************************************",NEWLINE,new ColumnSpacing(17),"*  NORMAL RUNS MUST UPDATE THE DATE FIELDS.  *",NEWLINE,new  //Natural: WRITE 17X '**********************************************' / 17X '*  NORMAL RUNS MUST UPDATE THE DATE FIELDS.  *' / 17X '*             JOB IS TERMINATING.            *' / 17X '**********************************************'
                ColumnSpacing(17),"*             JOB IS TERMINATING.            *",NEWLINE,new ColumnSpacing(17),"**********************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_In_Pnd_Run_Type);                                                                       //Natural: WRITE / 17X '=' #RUN-TYPE
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_In_Pnd_Date_Update);                                                                    //Natural: WRITE / 17X '=' #DATE-UPDATE
            if (Global.isEscape()) return;
            DbsUtil.terminate(98);  if (true) return;                                                                                                                     //Natural: TERMINATE 98
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Card_In_Pnd_Override_Dt.equals(" ")))                                                                                                      //Natural: IF #OVERRIDE-DT = ' '
        {
            pnd_Update_Dt.setValue(Global.getDATN());                                                                                                                     //Natural: ASSIGN #UPDATE-DT = *DATN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Update_Dt.setValue(pnd_Parm_Card_In_Pnd_Override_Dt_N);                                                                                                   //Natural: ASSIGN #UPDATE-DT = #OVERRIDE-DT-N
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, new ColumnSpacing(18),"*********************************************",NEWLINE,new ColumnSpacing(18),"*       PARM CARD EDIT WAS SUCCESSFUL       *",NEWLINE,new  //Natural: WRITE 18X '*********************************************' / 18X '*       PARM CARD EDIT WAS SUCCESSFUL       *' / 18X '*                                           *' / 18X '*    THIS RUN WILL USE THE FOLLOWING PARMS: *' / 18X '*                                           *' / 18X '*             RUN TYPE:' #RUN-TYPE 19X '*' / 18X '*          ACTION TYPE:' #ACTION-TYPE 19X '*' / 18X '*  UPDATE DATE FIELDS?:' #DATE-UPDATE 19X '*' / 18X '* UPDATE NEW STOP IND?:' #NEW-STOP-UPDATE 19X '*' / 18X '*   PARM OVERRIDE DATE:' #OVERRIDE-DT 12X '*' / 18X '* DATE USED FOR UPDATE:' #UPDATE-DT-A 12X '*' / 18X '*********************************************'
            ColumnSpacing(18),"*                                           *",NEWLINE,new ColumnSpacing(18),"*    THIS RUN WILL USE THE FOLLOWING PARMS: *",NEWLINE,new 
            ColumnSpacing(18),"*                                           *",NEWLINE,new ColumnSpacing(18),"*             RUN TYPE:",pnd_Parm_Card_In_Pnd_Run_Type,new 
            ColumnSpacing(19),"*",NEWLINE,new ColumnSpacing(18),"*          ACTION TYPE:",pnd_Parm_Card_In_Pnd_Action_Type,new ColumnSpacing(19),"*",NEWLINE,new 
            ColumnSpacing(18),"*  UPDATE DATE FIELDS?:",pnd_Parm_Card_In_Pnd_Date_Update,new ColumnSpacing(19),"*",NEWLINE,new ColumnSpacing(18),"* UPDATE NEW STOP IND?:",pnd_Parm_Card_In_Pnd_New_Stop_Update,new 
            ColumnSpacing(19),"*",NEWLINE,new ColumnSpacing(18),"*   PARM OVERRIDE DATE:",pnd_Parm_Card_In_Pnd_Override_Dt,new ColumnSpacing(12),"*",NEWLINE,new 
            ColumnSpacing(18),"* DATE USED FOR UPDATE:",pnd_Update_Dt_Pnd_Update_Dt_A,new ColumnSpacing(12),"*",NEWLINE,new ColumnSpacing(18),"*********************************************");
        if (Global.isEscape()) return;
        //*  (1690) P1000-RETRIEVE-PARM-INFO */
    }
    private void sub_P9999_Write_Control_Totals() throws Exception                                                                                                        //Natural: P9999-WRITE-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        getReports().write(0, NEWLINE,NEWLINE,new ColumnSpacing(23),"***********************************",NEWLINE,new ColumnSpacing(23),"*   CONTROL TOTALS FOR FCPP257    *",NEWLINE,new  //Natural: WRITE // 23X '***********************************' / 23X '*   CONTROL TOTALS FOR FCPP257    *' / 23X '***********************************'
            ColumnSpacing(23),"***********************************");
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,new ColumnSpacing(24),"           ISNS READ...",pnd_Isns_Read, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new                 //Natural: WRITE // 24X '           ISNS READ...' #ISNS-READ ( EM = Z,ZZZ,ZZ9 ) / 24X '       STOPS UPDATED...' #STOPS-UPDATED ( EM = Z,ZZZ,ZZ9 ) / 24X '     CANCELS UPDATED...' #CANCELS-UPDATED ( EM = Z,ZZZ,ZZ9 ) / 24X '   TRANSMITS UPDATED...' #TRANSMITS-UPDATED ( EM = Z,ZZZ,ZZ9 ) / 24X 'INVALID STATUS FOUND...' #INVALID-FLAG ( EM = Z,ZZZ,ZZ9 )
            ColumnSpacing(24),"       STOPS UPDATED...",pnd_Stops_Updated, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(24),"     CANCELS UPDATED...",pnd_Cancels_Updated, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(24),"   TRANSMITS UPDATED...",pnd_Transmits_Updated, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
            ColumnSpacing(24),"INVALID STATUS FOUND...",pnd_Invalid_Flag, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  (2810) P9999-WRITE-CONTROL-TOTALS */
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=80");
        Global.format(1, "PS=55 LS=80");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(7),"VOID RECONCILIATION PROCESS - UPDATE SELECT STATUS",new 
            ColumnSpacing(6),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(7),"--------------------------------------------------",
            NEWLINE,NEWLINE);
        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(6),"VOID RECONCILIATION PROCESS - SELECT STATUS UPDATED",new 
            ColumnSpacing(6),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(6),"---------------------------------------------------",new 
            ColumnSpacing(6),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("Z9"),NEWLINE,NEWLINE,"STOP  NEW                ",new ColumnSpacing(5),"           ACT",NEWLINE,"STAT STOP ORGN STAT   PIN",new 
            ColumnSpacing(5),"CHECK NBR  CD   ACCTG DT   RQST DT","RQ TM RQ USER",NEWLINE,"---- ---- ---- ----   ---",new ColumnSpacing(5),"---------  ---  --------   -------",
            "----- -------",NEWLINE,NEWLINE);
    }
}
