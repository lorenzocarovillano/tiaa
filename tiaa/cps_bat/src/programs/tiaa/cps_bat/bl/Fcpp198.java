/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:09 PM
**        * FROM NATURAL PROGRAM : Fcpp198
************************************************************
**        * FILE NAME            : Fcpp198.java
**        * CLASS NAME           : Fcpp198
**        * INSTANCE NAME        : Fcpp198
************************************************************
************************************************************************
* PROGRAM  : FCPP198
* SYSTEM   : CPS
* TITLE    : ELECTRONIC FUNDS TRANSFER
* ADAPTED  : AUG 24,94
* FUNCTION : THIS PROGRAM WILL EXTRACT FROM A WORK FILE ALL RECORDS
*            WITH PAYMENT-TYPE-REQUEST-INDICATOR = 2 (EFT's).
*
*
* HISTORY
*
*   ADAPTED  08-24-94 : ALTHEA A. YOUNG  - ADAPTED FROM FCPP280 FOR USE
*                                          IN ONLINE PAYMENT PROCESSING.
* 07-03-95 : A. YOUNG   - REACTIVATED CODE FOR PROCESSING MULTIPLE CHECK
*                         DATES.
* 09-16-96 : L. ZHENG   - HANDLING PA ROLLOVER THROUGH EFT
* 04-18-97 : R. SALGADO - PASS SETTLEMENT DATE FOR PA ROLLOVERS
* 08-13-97 : L. ZHENG   - ELIMINATE PRENOTE RECORDS (KEEP ONLY ONE DUMMY
*                         PRENOTE RECORD IF NO EFT DATA TO BE SENT)
* 10-02-98 : R. CARREON - INCLUDE AL INDICATOR
* 03-20-00 : R. CARREON - ADD DATE FIELD #DUE-DATE-D
* 06-15-00 : A. YOUNG   - REVISED CRITERIOR FOR PA ROLLOVER PROCESSING
*                         BECAUSE CNTRCT-DA-TIAA-1-NBR IS ALSO POPULATED
*                         FOR 'NZ' PAYMENTS.
* 11-30-04 : E. BOTTERI - ADD MOVE W/EDIT MASK WS-EFFECTIVE-ENTRY-DATE-5
*                         FOR RECORD TYPE 5.
* 4/2017    : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp198 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private LdaFcpl190a ldaFcpl190a;
    private LdaFcplcntr ldaFcplcntr;
    private LdaFcpl280 ldaFcpl280;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField effective_Date;
    private DbsField today_Julian;

    private DbsGroup today_Julian__R_Field_1;
    private DbsField today_Julian_Julian_Yy;
    private DbsField today_Julian_Julian_Ddd;
    private DbsField julian_Out;

    private DbsGroup julian_Out__R_Field_2;
    private DbsField julian_Out_Julian_Out_A;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Prime_Counter;
    private DbsField pnd_Eft_Date_Counter;
    private DbsField pnd_Pa_Date_Counter;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Check_Amt;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_3;
    private DbsField pnd_Input_Parm_Pnd_Parm_Eft_Glb;
    private DbsField pnd_Input_Parm_Pnd_Parm_Type;
    private DbsField pnd_Ws_Entry_Hash;

    private DbsGroup pnd_Ws_Entry_Hash__R_Field_4;
    private DbsField pnd_Ws_Entry_Hash__Filler1;
    private DbsField pnd_Ws_Entry_Hash_Pnd_Ws_Entry_Hash_10;
    private DbsField pnd_Ws_Hash_Amount;

    private DbsGroup pnd_Ws_Hash_Amount__R_Field_5;
    private DbsField pnd_Ws_Hash_Amount_Pnd_Ws_Hash_Amount_8;
    private DbsField pnd_G_Hash;

    private DbsGroup pnd_G_Hash__R_Field_6;
    private DbsField pnd_G_Hash__Filler2;
    private DbsField pnd_G_Hash_Pnd_G_Hash_10;
    private DbsField pnd_Ws_Total_Credit;
    private DbsField pnd_Ws_Name;
    private DbsField pnd_Ws_Payment_Date;
    private DbsField pnd_Work_Yyyymmdd;
    private DbsField pnd_Ws_Date_Yyyymmdd;

    private DbsGroup pnd_Ws_Date_Yyyymmdd__R_Field_7;
    private DbsField pnd_Ws_Date_Yyyymmdd__Filler3;
    private DbsField pnd_Ws_Date_Yyyymmdd_Pnd_Ws_Date_Yymmdd;

    private DbsGroup pnd_Ws_Date_Yyyymmdd__R_Field_8;
    private DbsField pnd_Ws_Date_Yyyymmdd_Pnd_Date_Yy;
    private DbsField pnd_Ws_Date_Yyyymmdd_Pnd_Date_Mm;
    private DbsField pnd_Ws_Date_Yyyymmdd_Pnd_Date_Dd;
    private DbsField pnd_Ws_Time_Hhmmsst;

    private DbsGroup pnd_Ws_Time_Hhmmsst__R_Field_9;
    private DbsField pnd_Ws_Time_Hhmmsst_Pnd_Ws_Time_Hhmm;
    private DbsField pnd_Batch_Recs;
    private DbsField pnd_Ws_No_Of_Transactions;
    private DbsField pnd_Ws_Total_Amount;
    private DbsField pnd_Ws_Diff_No_Of_Transactions;
    private DbsField pnd_Ws_Diff_Total_Amount;
    private DbsField pnd_Ws_Cntr_Held;
    private DbsField pnd_Ws_Cntr_Held_Total_Amount;
    private DbsField pnd_Ws_No_Of_Pa_Transactions;
    private DbsField pnd_Ws_Total_Pa_Amount;
    private DbsField pnd_Ws_Total_Transactions;
    private DbsField pnd_Ws_Account_Type;
    private DbsField pnd_Corporate_Name_Addr;

    private DbsGroup pnd_Corporate_Name_Addr__R_Field_10;
    private DbsField pnd_Corporate_Name_Addr__Filler4;
    private DbsField pnd_Corporate_Name_Addr_Pnd_Cna_Update_Contract;
    private DbsField pnd_Corporate_Name_Addr__Filler5;
    private DbsField pnd_Corporate_Name_Addr_Pnd_Cna_Update_Date;
    private DbsField pnd_Corporate_Name_Addr__Filler6;
    private DbsField pnd_Corporate_Name_Addr_Pnd_Cna_Update_Indicator;
    private DbsField pnd_Corporate_Name_Addr__Filler7;
    private DbsField pnd_Corporate_Name_Addr_Pnd_Cna_Aba_Info;
    private DbsField pnd_Corporate_Name_Addr_Pnd_Cna_Acct_Num;
    private DbsField pnd_Corporate_Name_Addr__Filler8;
    private DbsField pnd_Company_Id;
    private DbsField pnd_Company_Name;
    private DbsField pnd_Wrk_Fld;
    private DbsField pnd_Len;
    private DbsField pnd_One_Byte;
    private DbsField pnd_Wrk_Dte;
    private DbsField pnd_G_Total;
    private DbsField pnd_Total_Recs;
    private DbsField pnd_Batch_Cnt;
    private DbsField pnd_Block_Cnt;
    private DbsField pnd_Ws_Remainder;
    private DbsField pnd_Dollar_Amt;
    private DbsField pnd_Pymnt_Dte;
    private DbsField pnd_First;
    private DbsField pnd_Eod;
    private DbsField pnd_Check_Date_Break;
    private DbsField pnd_Pymnt_Amt;
    private DbsField pnd_Due_Date;
    private DbsField pnd_Due_Date_D;
    private DbsField pnd_Convert_Mmddyy;

    private DbsGroup pnd_Convert_Mmddyy__R_Field_11;
    private DbsField pnd_Convert_Mmddyy_Pnd_Convert_Mm;
    private DbsField pnd_Convert_Mmddyy_Pnd_Convert_Dd;
    private DbsField pnd_Convert_Mmddyy_Pnd_Convert_Yy;

    private DbsGroup pnd_Control_Record;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date;

    private DbsGroup pnd_Control_Record__R_Field_12;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Check;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Check;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Eft;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Global;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Global;
    private DbsField pnd_Ws_Last_Name_A22;
    private DbsField pnd_Eft_Or_Pa_Ind;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Ws_Pymnt_Check_AmtSum572;
    private DbsField readWork01Pnd_Ws_Pymnt_Check_AmtSum;
    private DbsField readWork01Pnd_Ws_Pymnt_Check_AmtSum585;
    private DbsField readWork01Pymnt_Check_DteOld;
    private DbsField readWork01Pnd_Ws_Pymnt_Check_AmtSum554;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        ldaFcplcntr = new LdaFcplcntr();
        registerRecord(ldaFcplcntr);
        registerRecord(ldaFcplcntr.getVw_fcp_Cons_Cntrl());
        ldaFcpl280 = new LdaFcpl280();
        registerRecord(ldaFcpl280);

        // Local Variables
        localVariables = new DbsRecord();
        effective_Date = localVariables.newFieldInRecord("effective_Date", "EFFECTIVE-DATE", FieldType.DATE);
        today_Julian = localVariables.newFieldInRecord("today_Julian", "TODAY-JULIAN", FieldType.STRING, 5);

        today_Julian__R_Field_1 = localVariables.newGroupInRecord("today_Julian__R_Field_1", "REDEFINE", today_Julian);
        today_Julian_Julian_Yy = today_Julian__R_Field_1.newFieldInGroup("today_Julian_Julian_Yy", "JULIAN-YY", FieldType.NUMERIC, 2);
        today_Julian_Julian_Ddd = today_Julian__R_Field_1.newFieldInGroup("today_Julian_Julian_Ddd", "JULIAN-DDD", FieldType.NUMERIC, 3);
        julian_Out = localVariables.newFieldInRecord("julian_Out", "JULIAN-OUT", FieldType.NUMERIC, 3);

        julian_Out__R_Field_2 = localVariables.newGroupInRecord("julian_Out__R_Field_2", "REDEFINE", julian_Out);
        julian_Out_Julian_Out_A = julian_Out__R_Field_2.newFieldInGroup("julian_Out_Julian_Out_A", "JULIAN-OUT-A", FieldType.STRING, 3);
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Prime_Counter = localVariables.newFieldInRecord("pnd_Prime_Counter", "#PRIME-COUNTER", FieldType.PACKED_DECIMAL, 7);
        pnd_Eft_Date_Counter = localVariables.newFieldInRecord("pnd_Eft_Date_Counter", "#EFT-DATE-COUNTER", FieldType.PACKED_DECIMAL, 7);
        pnd_Pa_Date_Counter = localVariables.newFieldInRecord("pnd_Pa_Date_Counter", "#PA-DATE-COUNTER", FieldType.PACKED_DECIMAL, 7);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ws_Pymnt_Check_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Check_Amt", "#WS-PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 4);

        pnd_Input_Parm__R_Field_3 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_3", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Parm_Eft_Glb = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Parm_Eft_Glb", "#PARM-EFT-GLB", FieldType.STRING, 
            3);
        pnd_Input_Parm_Pnd_Parm_Type = pnd_Input_Parm__R_Field_3.newFieldInGroup("pnd_Input_Parm_Pnd_Parm_Type", "#PARM-TYPE", FieldType.NUMERIC, 1);
        pnd_Ws_Entry_Hash = localVariables.newFieldInRecord("pnd_Ws_Entry_Hash", "#WS-ENTRY-HASH", FieldType.NUMERIC, 20);

        pnd_Ws_Entry_Hash__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Entry_Hash__R_Field_4", "REDEFINE", pnd_Ws_Entry_Hash);
        pnd_Ws_Entry_Hash__Filler1 = pnd_Ws_Entry_Hash__R_Field_4.newFieldInGroup("pnd_Ws_Entry_Hash__Filler1", "_FILLER1", FieldType.STRING, 10);
        pnd_Ws_Entry_Hash_Pnd_Ws_Entry_Hash_10 = pnd_Ws_Entry_Hash__R_Field_4.newFieldInGroup("pnd_Ws_Entry_Hash_Pnd_Ws_Entry_Hash_10", "#WS-ENTRY-HASH-10", 
            FieldType.NUMERIC, 10);
        pnd_Ws_Hash_Amount = localVariables.newFieldInRecord("pnd_Ws_Hash_Amount", "#WS-HASH-AMOUNT", FieldType.NUMERIC, 9);

        pnd_Ws_Hash_Amount__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Hash_Amount__R_Field_5", "REDEFINE", pnd_Ws_Hash_Amount);
        pnd_Ws_Hash_Amount_Pnd_Ws_Hash_Amount_8 = pnd_Ws_Hash_Amount__R_Field_5.newFieldInGroup("pnd_Ws_Hash_Amount_Pnd_Ws_Hash_Amount_8", "#WS-HASH-AMOUNT-8", 
            FieldType.NUMERIC, 8);
        pnd_G_Hash = localVariables.newFieldInRecord("pnd_G_Hash", "#G-HASH", FieldType.NUMERIC, 20);

        pnd_G_Hash__R_Field_6 = localVariables.newGroupInRecord("pnd_G_Hash__R_Field_6", "REDEFINE", pnd_G_Hash);
        pnd_G_Hash__Filler2 = pnd_G_Hash__R_Field_6.newFieldInGroup("pnd_G_Hash__Filler2", "_FILLER2", FieldType.STRING, 10);
        pnd_G_Hash_Pnd_G_Hash_10 = pnd_G_Hash__R_Field_6.newFieldInGroup("pnd_G_Hash_Pnd_G_Hash_10", "#G-HASH-10", FieldType.NUMERIC, 10);
        pnd_Ws_Total_Credit = localVariables.newFieldInRecord("pnd_Ws_Total_Credit", "#WS-TOTAL-CREDIT", FieldType.NUMERIC, 12, 2);
        pnd_Ws_Name = localVariables.newFieldInRecord("pnd_Ws_Name", "#WS-NAME", FieldType.STRING, 38);
        pnd_Ws_Payment_Date = localVariables.newFieldInRecord("pnd_Ws_Payment_Date", "#WS-PAYMENT-DATE", FieldType.STRING, 8);
        pnd_Work_Yyyymmdd = localVariables.newFieldInRecord("pnd_Work_Yyyymmdd", "#WORK-YYYYMMDD", FieldType.STRING, 8);
        pnd_Ws_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Ws_Date_Yyyymmdd", "#WS-DATE-YYYYMMDD", FieldType.NUMERIC, 8);

        pnd_Ws_Date_Yyyymmdd__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_Date_Yyyymmdd__R_Field_7", "REDEFINE", pnd_Ws_Date_Yyyymmdd);
        pnd_Ws_Date_Yyyymmdd__Filler3 = pnd_Ws_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Ws_Date_Yyyymmdd__Filler3", "_FILLER3", FieldType.STRING, 
            2);
        pnd_Ws_Date_Yyyymmdd_Pnd_Ws_Date_Yymmdd = pnd_Ws_Date_Yyyymmdd__R_Field_7.newFieldInGroup("pnd_Ws_Date_Yyyymmdd_Pnd_Ws_Date_Yymmdd", "#WS-DATE-YYMMDD", 
            FieldType.NUMERIC, 6);

        pnd_Ws_Date_Yyyymmdd__R_Field_8 = pnd_Ws_Date_Yyyymmdd__R_Field_7.newGroupInGroup("pnd_Ws_Date_Yyyymmdd__R_Field_8", "REDEFINE", pnd_Ws_Date_Yyyymmdd_Pnd_Ws_Date_Yymmdd);
        pnd_Ws_Date_Yyyymmdd_Pnd_Date_Yy = pnd_Ws_Date_Yyyymmdd__R_Field_8.newFieldInGroup("pnd_Ws_Date_Yyyymmdd_Pnd_Date_Yy", "#DATE-YY", FieldType.NUMERIC, 
            2);
        pnd_Ws_Date_Yyyymmdd_Pnd_Date_Mm = pnd_Ws_Date_Yyyymmdd__R_Field_8.newFieldInGroup("pnd_Ws_Date_Yyyymmdd_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 
            2);
        pnd_Ws_Date_Yyyymmdd_Pnd_Date_Dd = pnd_Ws_Date_Yyyymmdd__R_Field_8.newFieldInGroup("pnd_Ws_Date_Yyyymmdd_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 
            2);
        pnd_Ws_Time_Hhmmsst = localVariables.newFieldInRecord("pnd_Ws_Time_Hhmmsst", "#WS-TIME-HHMMSST", FieldType.NUMERIC, 7);

        pnd_Ws_Time_Hhmmsst__R_Field_9 = localVariables.newGroupInRecord("pnd_Ws_Time_Hhmmsst__R_Field_9", "REDEFINE", pnd_Ws_Time_Hhmmsst);
        pnd_Ws_Time_Hhmmsst_Pnd_Ws_Time_Hhmm = pnd_Ws_Time_Hhmmsst__R_Field_9.newFieldInGroup("pnd_Ws_Time_Hhmmsst_Pnd_Ws_Time_Hhmm", "#WS-TIME-HHMM", 
            FieldType.NUMERIC, 4);
        pnd_Batch_Recs = localVariables.newFieldInRecord("pnd_Batch_Recs", "#BATCH-RECS", FieldType.NUMERIC, 6);
        pnd_Ws_No_Of_Transactions = localVariables.newFieldInRecord("pnd_Ws_No_Of_Transactions", "#WS-NO-OF-TRANSACTIONS", FieldType.PACKED_DECIMAL, 9);
        pnd_Ws_Total_Amount = localVariables.newFieldInRecord("pnd_Ws_Total_Amount", "#WS-TOTAL-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Diff_No_Of_Transactions = localVariables.newFieldInRecord("pnd_Ws_Diff_No_Of_Transactions", "#WS-DIFF-NO-OF-TRANSACTIONS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Ws_Diff_Total_Amount = localVariables.newFieldInRecord("pnd_Ws_Diff_Total_Amount", "#WS-DIFF-TOTAL-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Cntr_Held = localVariables.newFieldInRecord("pnd_Ws_Cntr_Held", "#WS-CNTR-HELD", FieldType.PACKED_DECIMAL, 9);
        pnd_Ws_Cntr_Held_Total_Amount = localVariables.newFieldInRecord("pnd_Ws_Cntr_Held_Total_Amount", "#WS-CNTR-HELD-TOTAL-AMOUNT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_No_Of_Pa_Transactions = localVariables.newFieldInRecord("pnd_Ws_No_Of_Pa_Transactions", "#WS-NO-OF-PA-TRANSACTIONS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Ws_Total_Pa_Amount = localVariables.newFieldInRecord("pnd_Ws_Total_Pa_Amount", "#WS-TOTAL-PA-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Total_Transactions = localVariables.newFieldInRecord("pnd_Ws_Total_Transactions", "#WS-TOTAL-TRANSACTIONS", FieldType.PACKED_DECIMAL, 9);
        pnd_Ws_Account_Type = localVariables.newFieldInRecord("pnd_Ws_Account_Type", "#WS-ACCOUNT-TYPE", FieldType.STRING, 8);
        pnd_Corporate_Name_Addr = localVariables.newFieldInRecord("pnd_Corporate_Name_Addr", "#CORPORATE-NAME-ADDR", FieldType.STRING, 119);

        pnd_Corporate_Name_Addr__R_Field_10 = localVariables.newGroupInRecord("pnd_Corporate_Name_Addr__R_Field_10", "REDEFINE", pnd_Corporate_Name_Addr);
        pnd_Corporate_Name_Addr__Filler4 = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr__Filler4", "_FILLER4", FieldType.STRING, 
            5);
        pnd_Corporate_Name_Addr_Pnd_Cna_Update_Contract = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr_Pnd_Cna_Update_Contract", 
            "#CNA-UPDATE-CONTRACT", FieldType.STRING, 60);
        pnd_Corporate_Name_Addr__Filler5 = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr__Filler5", "_FILLER5", FieldType.STRING, 
            2);
        pnd_Corporate_Name_Addr_Pnd_Cna_Update_Date = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr_Pnd_Cna_Update_Date", 
            "#CNA-UPDATE-DATE", FieldType.STRING, 6);
        pnd_Corporate_Name_Addr__Filler6 = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr__Filler6", "_FILLER6", FieldType.STRING, 
            5);
        pnd_Corporate_Name_Addr_Pnd_Cna_Update_Indicator = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr_Pnd_Cna_Update_Indicator", 
            "#CNA-UPDATE-INDICATOR", FieldType.NUMERIC, 1);
        pnd_Corporate_Name_Addr__Filler7 = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr__Filler7", "_FILLER7", FieldType.STRING, 
            1);
        pnd_Corporate_Name_Addr_Pnd_Cna_Aba_Info = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr_Pnd_Cna_Aba_Info", "#CNA-ABA-INFO", 
            FieldType.STRING, 21);
        pnd_Corporate_Name_Addr_Pnd_Cna_Acct_Num = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr_Pnd_Cna_Acct_Num", "#CNA-ACCT-NUM", 
            FieldType.STRING, 17);
        pnd_Corporate_Name_Addr__Filler8 = pnd_Corporate_Name_Addr__R_Field_10.newFieldInGroup("pnd_Corporate_Name_Addr__Filler8", "_FILLER8", FieldType.STRING, 
            1);
        pnd_Company_Id = localVariables.newFieldInRecord("pnd_Company_Id", "#COMPANY-ID", FieldType.NUMERIC, 10);
        pnd_Company_Name = localVariables.newFieldInRecord("pnd_Company_Name", "#COMPANY-NAME", FieldType.STRING, 16);
        pnd_Wrk_Fld = localVariables.newFieldInRecord("pnd_Wrk_Fld", "#WRK-FLD", FieldType.STRING, 32);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.INTEGER, 2);
        pnd_One_Byte = localVariables.newFieldInRecord("pnd_One_Byte", "#ONE-BYTE", FieldType.STRING, 1);
        pnd_Wrk_Dte = localVariables.newFieldInRecord("pnd_Wrk_Dte", "#WRK-DTE", FieldType.STRING, 6);
        pnd_G_Total = localVariables.newFieldInRecord("pnd_G_Total", "#G-TOTAL", FieldType.NUMERIC, 12, 2);
        pnd_Total_Recs = localVariables.newFieldInRecord("pnd_Total_Recs", "#TOTAL-RECS", FieldType.PACKED_DECIMAL, 7);
        pnd_Batch_Cnt = localVariables.newFieldInRecord("pnd_Batch_Cnt", "#BATCH-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Block_Cnt = localVariables.newFieldInRecord("pnd_Block_Cnt", "#BLOCK-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ws_Remainder = localVariables.newFieldInRecord("pnd_Ws_Remainder", "#WS-REMAINDER", FieldType.PACKED_DECIMAL, 3);
        pnd_Dollar_Amt = localVariables.newFieldInRecord("pnd_Dollar_Amt", "#DOLLAR-AMT", FieldType.STRING, 17);
        pnd_Pymnt_Dte = localVariables.newFieldInRecord("pnd_Pymnt_Dte", "#PYMNT-DTE", FieldType.DATE);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Eod = localVariables.newFieldInRecord("pnd_Eod", "#EOD", FieldType.BOOLEAN, 1);
        pnd_Check_Date_Break = localVariables.newFieldInRecord("pnd_Check_Date_Break", "#CHECK-DATE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Amt = localVariables.newFieldInRecord("pnd_Pymnt_Amt", "#PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Due_Date = localVariables.newFieldInRecord("pnd_Due_Date", "#DUE-DATE", FieldType.STRING, 6);
        pnd_Due_Date_D = localVariables.newFieldInRecord("pnd_Due_Date_D", "#DUE-DATE-D", FieldType.DATE);
        pnd_Convert_Mmddyy = localVariables.newFieldInRecord("pnd_Convert_Mmddyy", "#CONVERT-MMDDYY", FieldType.STRING, 6);

        pnd_Convert_Mmddyy__R_Field_11 = localVariables.newGroupInRecord("pnd_Convert_Mmddyy__R_Field_11", "REDEFINE", pnd_Convert_Mmddyy);
        pnd_Convert_Mmddyy_Pnd_Convert_Mm = pnd_Convert_Mmddyy__R_Field_11.newFieldInGroup("pnd_Convert_Mmddyy_Pnd_Convert_Mm", "#CONVERT-MM", FieldType.STRING, 
            2);
        pnd_Convert_Mmddyy_Pnd_Convert_Dd = pnd_Convert_Mmddyy__R_Field_11.newFieldInGroup("pnd_Convert_Mmddyy_Pnd_Convert_Dd", "#CONVERT-DD", FieldType.STRING, 
            2);
        pnd_Convert_Mmddyy_Pnd_Convert_Yy = pnd_Convert_Mmddyy__R_Field_11.newFieldInGroup("pnd_Convert_Mmddyy_Pnd_Convert_Yy", "#CONVERT-YY", FieldType.STRING, 
            2);

        pnd_Control_Record = localVariables.newGroupInRecord("pnd_Control_Record", "#CONTROL-RECORD");
        pnd_Control_Record_Pnd_Cntl_Payment_Date = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date", "#CNTL-PAYMENT-DATE", 
            FieldType.STRING, 8);

        pnd_Control_Record__R_Field_12 = pnd_Control_Record.newGroupInGroup("pnd_Control_Record__R_Field_12", "REDEFINE", pnd_Control_Record_Pnd_Cntl_Payment_Date);
        pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2 = pnd_Control_Record__R_Field_12.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2", 
            "#CNTL-PAYMENT-DATE-1-2", FieldType.STRING, 2);
        pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8 = pnd_Control_Record__R_Field_12.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8", 
            "#CNTL-PAYMENT-DATE-3-8", FieldType.STRING, 6);
        pnd_Control_Record_Pnd_Cntl_Cnt_Check = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Check", "#CNTL-CNT-CHECK", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Check = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Check", "#CNTL-NET-AMT-CHECK", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Record_Pnd_Cntl_Cnt_Eft = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Eft", "#CNTL-CNT-EFT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft", "#CNTL-NET-AMT-EFT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Control_Record_Pnd_Cntl_Cnt_Global = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Global", "#CNTL-CNT-GLOBAL", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Global = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Global", "#CNTL-NET-AMT-GLOBAL", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Last_Name_A22 = localVariables.newFieldInRecord("pnd_Ws_Last_Name_A22", "#WS-LAST-NAME-A22", FieldType.STRING, 22);
        pnd_Eft_Or_Pa_Ind = localVariables.newFieldInRecord("pnd_Eft_Or_Pa_Ind", "#EFT-OR-PA-IND", FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Ws_Pymnt_Check_AmtSum572 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Ws_Pymnt_Check_Amt_SUM_572", "Pnd_Ws_Pymnt_Check_Amt_SUM_572", 
            FieldType.PACKED_DECIMAL, 11, 2);
        readWork01Pnd_Ws_Pymnt_Check_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Ws_Pymnt_Check_Amt_SUM", "Pnd_Ws_Pymnt_Check_Amt_SUM", 
            FieldType.PACKED_DECIMAL, 11, 2);
        readWork01Pnd_Ws_Pymnt_Check_AmtSum585 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Ws_Pymnt_Check_Amt_SUM_585", "Pnd_Ws_Pymnt_Check_Amt_SUM_585", 
            FieldType.PACKED_DECIMAL, 11, 2);
        readWork01Pymnt_Check_DteOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pymnt_Check_Dte_OLD", "Pymnt_Check_Dte_OLD", FieldType.DATE);
        readWork01Pnd_Ws_Pymnt_Check_AmtSum554 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Ws_Pymnt_Check_Amt_SUM_554", "Pnd_Ws_Pymnt_Check_Amt_SUM_554", 
            FieldType.PACKED_DECIMAL, 11, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaCdbatxa.initializeValues();
        ldaFcpl190a.initializeValues();
        ldaFcplcntr.initializeValues();
        ldaFcpl280.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_First.setInitialValue(true);
        pnd_Eod.setInitialValue(false);
        pnd_Check_Date_Break.setInitialValue(false);
        pnd_Due_Date.setInitialValue(" ");
        pnd_Convert_Mmddyy.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp198() throws Exception
    {
        super("Fcpp198");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp198|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                //*                                                                                                                                                       //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM = *PROGRAM
                //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
                pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                              //Natural: ASSIGN #CUR-LANG = *LANGUAGE
                pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                       //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=M"),pnd_Input_Parm);                                                          //Natural: INPUT ( AD = M ) #INPUT-PARM
                //*  08-24-94 : A. YOUNG
                //*                                                                                                                                                       //Natural: DECIDE FOR FIRST CONDITION
                short decideConditionsMet485 = 0;                                                                                                                         //Natural: WHEN #PARM-EFT-GLB = 'EFT'
                if (condition(pnd_Input_Parm_Pnd_Parm_Eft_Glb.equals("EFT")))
                {
                    decideConditionsMet485++;
                    pnd_Input_Parm_Pnd_Parm_Type.setValue(2);                                                                                                             //Natural: ASSIGN #PARM-TYPE := 2
                    pnd_Header0_2.setValue("TRANSMITTAL REGISTER FOR CPS ON-LINE PAYMENTS");                                                                              //Natural: ASSIGN #HEADER0-2 := 'TRANSMITTAL REGISTER FOR CPS ON-LINE PAYMENTS'
                }                                                                                                                                                         //Natural: WHEN #PARM-EFT-GLB = 'GLB'
                else if (condition(pnd_Input_Parm_Pnd_Parm_Eft_Glb.equals("GLB")))
                {
                    decideConditionsMet485++;
                    pnd_Input_Parm_Pnd_Parm_Type.setValue(3);                                                                                                             //Natural: ASSIGN #PARM-TYPE := 3
                    pnd_Header0_2.setValue(" GLOBAL TRANSMITTAL REGISTER FOR CPS PAYMENTS");                                                                              //Natural: ASSIGN #HEADER0-2 := ' GLOBAL TRANSMITTAL REGISTER FOR CPS PAYMENTS'
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"NO PAYMENT TYPE PARM CARD FOUND IN JCL.");                                                //Natural: WRITE NOTITLE // 'NO PAYMENT TYPE PARM CARD FOUND IN JCL.'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(77);  if (true) return;                                                                                                             //Natural: TERMINATE 77
                    //*  08-24-94 : A. YOUNG
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: AT TOP OF PAGE ( 1 );//Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //* ***********************
                //*   MAIN PROGRAM LOGIC  *
                //* ***********************
                getWorkFiles().read(13, pnd_Control_Record);                                                                                                              //Natural: READ WORK 13 ONCE #CONTROL-RECORD
                pnd_Wrk_Dte.setValue(pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8);                                                                                       //Natural: ASSIGN #WRK-DTE := #CONTROL-RECORD.#CNTL-PAYMENT-DATE-3-8
                pnd_Ws_Date_Yyyymmdd_Pnd_Ws_Date_Yymmdd.compute(new ComputeParameters(false, pnd_Ws_Date_Yyyymmdd_Pnd_Ws_Date_Yymmdd), pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8.val()); //Natural: ASSIGN #WS-DATE-YYMMDD := VAL ( #CONTROL-RECORD.#CNTL-PAYMENT-DATE-3-8 )
                pnd_Convert_Mmddyy_Pnd_Convert_Dd.setValue(pnd_Ws_Date_Yyyymmdd_Pnd_Date_Dd);                                                                             //Natural: MOVE #DATE-DD TO #CONVERT-DD
                pnd_Convert_Mmddyy_Pnd_Convert_Mm.setValue(pnd_Ws_Date_Yyyymmdd_Pnd_Date_Mm);                                                                             //Natural: MOVE #DATE-MM TO #CONVERT-MM
                pnd_Convert_Mmddyy_Pnd_Convert_Yy.setValue(pnd_Ws_Date_Yyyymmdd_Pnd_Date_Yy);                                                                             //Natural: MOVE #DATE-YY TO #CONVERT-YY
                pnd_Due_Date.setValue(pnd_Convert_Mmddyy);                                                                                                                //Natural: MOVE #CONVERT-MMDDYY TO #DUE-DATE
                //*                                                ROXAN  3/20/2000
                pnd_Due_Date_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Control_Record_Pnd_Cntl_Payment_Date);                                                   //Natural: MOVE EDITED #CNTL-PAYMENT-DATE TO #DUE-DATE-D ( EM = YYYYMMDD )
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Record_Type_1().setValue("1");                                                                                   //Natural: ASSIGN WS-RECORD-TYPE-1 = '1'
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Priority_Code_1().setValue(1);                                                                                   //Natural: ASSIGN WS-PRIORITY-CODE-1 = 01
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Immediate_Destination_1().setValue(" 021000021");                                                                //Natural: ASSIGN WS-IMMEDIATE-DESTINATION-1 = ' 021000021'
                //*  03-09-95 : A YNG
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Immediate_Origin_1().setValue(" 009440001");                                                                     //Natural: ASSIGN WS-IMMEDIATE-ORIGIN-1 = ' 009440001'
                //* *ASSIGN WS-IMMEDIATE-ORIGIN-1      = '9TEST40001'
                ldaFcpl280.getWs_File_Header_Record_1_Ws_File_Creation_Date_1().compute(new ComputeParameters(false, ldaFcpl280.getWs_File_Header_Record_1_Ws_File_Creation_Date_1()),  //Natural: ASSIGN WS-FILE-CREATION-DATE-1 := VAL ( #WRK-DTE )
                    pnd_Wrk_Dte.val());
                pnd_Ws_Time_Hhmmsst.setValue(Global.getTIMN());                                                                                                           //Natural: ASSIGN #WS-TIME-HHMMSST = *TIMN
                ldaFcpl280.getWs_File_Header_Record_1_Ws_File_Creation_Time_1().setValue(pnd_Ws_Time_Hhmmsst_Pnd_Ws_Time_Hhmm);                                           //Natural: ASSIGN WS-FILE-CREATION-TIME-1 = #WS-TIME-HHMM
                ldaFcpl280.getWs_File_Header_Record_1_Ws_File_Id_Modifier_1().setValue("E");                                                                              //Natural: ASSIGN WS-FILE-ID-MODIFIER-1 = 'E'
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Record_Size_1().setValue(94);                                                                                    //Natural: ASSIGN WS-RECORD-SIZE-1 = 094
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Blocking_Factor_1().setValue(10);                                                                                //Natural: ASSIGN WS-BLOCKING-FACTOR-1 = 10
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Format_Code_1().setValue(1);                                                                                     //Natural: ASSIGN WS-FORMAT-CODE-1 = 1
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Destination_1().setValue("CHASE MANHATTAN BANK NA");                                                             //Natural: ASSIGN WS-DESTINATION-1 = 'CHASE MANHATTAN BANK NA'
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Origin_1().setValue("TIAA CREF ANNUITY");                                                                        //Natural: ASSIGN WS-ORIGIN-1 = 'TIAA CREF ANNUITY'
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Reference_Code_1().setValue(" ");                                                                                //Natural: ASSIGN WS-REFERENCE-CODE-1 = ' '
                getWorkFiles().write(12, false, ldaFcpl280.getWs_File_Header_Record_1());                                                                                 //Natural: WRITE WORK FILE 12 WS-FILE-HEADER-RECORD-1
                ldaFcpl280.getWs_File_Header_Record_1().reset();                                                                                                          //Natural: RESET WS-FILE-HEADER-RECORD-1
                pnd_Prime_Counter.reset();                                                                                                                                //Natural: RESET #PRIME-COUNTER
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK 11 #RPT-EXT.#PYMNT-ADDR
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(11, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr())))
                {
                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //BEFORE BREAK PROCESSING                                                                                                                             //Natural: BEFORE BREAK PROCESSING
                    if (condition(!(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(pnd_Input_Parm_Pnd_Parm_Type) && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Cde().equals(" ")  //Natural: ACCEPT IF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = #PARM-TYPE AND #RPT-EXT.CNTRCT-HOLD-CDE = ' ' AND #RPT-EXT.PYMNT-INSTMT-NBR = 1
                        && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Instmt_Nbr().equals(1))))
                    {
                        continue;
                    }
                    //*    IF #RPT-EXT.CNTRCT-DA-TIAA-1-NBR = ' '        /* LZ 09-16-96
                    //*  06-15-2000
                    if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr().notEquals(" ")) && (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS"))))     //Natural: IF ( #RPT-EXT.CNTRCT-DA-TIAA-1-NBR NE ' ' ) AND ( #RPT-EXT.CNTRCT-ORGN-CDE = 'SS' )
                    {
                        //*      MOVE 'EFT' TO #EFT-OR-PA-IND                         /* 06-15-2000
                        //*  06-15-2000
                        pnd_Eft_Or_Pa_Ind.setValue("PA ");                                                                                                                //Natural: MOVE 'PA ' TO #EFT-OR-PA-IND
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*      MOVE 'PA'  TO #EFT-OR-PA-IND                         /* 06-15-2000
                        //*  06-15-2000
                        pnd_Eft_Or_Pa_Ind.setValue("EFT");                                                                                                                //Natural: MOVE 'EFT' TO #EFT-OR-PA-IND
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Ws_Pymnt_Check_Amt.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                                 //Natural: ASSIGN #WS-PYMNT-CHECK-AMT := #RPT-EXT.PYMNT-CHECK-AMT
                    //*  LZ 09-16-96
                    //END-BEFORE                                                                                                                                          //Natural: END-BEFORE
                    //*                                                                                                                                                   //Natural: AT BREAK OF #EFT-OR-PA-IND
                    //*                                                                                                                                                   //Natural: AT BREAK OF #RPT-EXT.PYMNT-CHECK-DTE
                    pnd_Prime_Counter.nadd(1);                                                                                                                            //Natural: ADD 1 TO #PRIME-COUNTER
                    //*  IF #RPT-EXT.CNTRCT-DA-TIAA-1-NBR NE ' ' /* FOR PA ROLLOVER 091696 LZ
                    //*  06-15-2000
                    if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr().notEquals(" ")) && (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS"))))     //Natural: IF ( #RPT-EXT.CNTRCT-DA-TIAA-1-NBR NE ' ' ) AND ( #RPT-EXT.CNTRCT-ORGN-CDE = 'SS' )
                    {
                        pnd_Pa_Date_Counter.nadd(1);                                                                                                                      //Natural: ADD 1 TO #PA-DATE-COUNTER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Eft_Date_Counter.nadd(1);                                                                                                                     //Natural: ADD 1 TO #EFT-DATE-COUNTER
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_First.getBoolean()))                                                                                                                //Natural: IF #FIRST
                    {
                        pnd_First.setValue(false);                                                                                                                        //Natural: ASSIGN #FIRST := FALSE
                        //*  08-24-94 : A. YOUNG
                                                                                                                                                                          //Natural: PERFORM DETERMINE-COMPANY-NAME-ID
                        sub_Determine_Company_Name_Id();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-TYPE-5-RECORD
                        sub_Create_Type_5_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Pymnt_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                                             //Natural: ASSIGN #PYMNT-DTE := #RPT-EXT.PYMNT-CHECK-DTE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Check_Date_Break.getBoolean()))                                                                                                     //Natural: IF #CHECK-DATE-BREAK
                    {
                                                                                                                                                                          //Natural: PERFORM CREATE-TYPE-5-RECORD
                        sub_Create_Type_5_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Batch_Recs.reset();                                                                                                                           //Natural: RESET #BATCH-RECS
                        pnd_Check_Date_Break.setValue(false);                                                                                                             //Natural: ASSIGN #CHECK-DATE-BREAK := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  IF #RPT-EXT.CNTRCT-DA-TIAA-1-NBR NE ' '     /* FOR PA ONLY 09-16-96 LZ
                    //*  06-15-2000
                    if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr().notEquals(" ")) && (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS"))))     //Natural: IF ( #RPT-EXT.CNTRCT-DA-TIAA-1-NBR NE ' ' ) AND ( #RPT-EXT.CNTRCT-ORGN-CDE = 'SS' )
                    {
                        pnd_Ws_No_Of_Pa_Transactions.nadd(1);                                                                                                             //Natural: ADD 1 TO #WS-NO-OF-PA-TRANSACTIONS
                        pnd_Ws_Total_Pa_Amount.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                                        //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-TOTAL-PA-AMOUNT
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Cde().notEquals(" ")))                                                                           //Natural: IF #RPT-EXT.CNTRCT-HOLD-CDE NE ' '
                    {
                        //*    IF #RPT-EXT.CNTRCT-DA-TIAA-1-NBR = ' '   /* FOR EFT ONLY 09-16-96 LZ
                        //*  06-15-2000
                        if (condition(pnd_Eft_Or_Pa_Ind.equals("EFT")))                                                                                                   //Natural: IF #EFT-OR-PA-IND = 'EFT'
                        {
                            pnd_Ws_Cntr_Held.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-CNTR-HELD
                            pnd_Ws_Cntr_Held_Total_Amount.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                             //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CNTR-HELD-TOTAL-AMOUNT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Total_Transactions.nadd(1);                                                                                                                //Natural: ADD 1 TO #WS-TOTAL-TRANSACTIONS
                        //*    IF #RPT-EXT.CNTRCT-DA-TIAA-1-NBR = ' '   /* FOR EFT ONLY 09-16-96 LZ
                        //*  06-15-2000
                        if (condition(pnd_Eft_Or_Pa_Ind.equals("EFT")))                                                                                                   //Natural: IF #EFT-OR-PA-IND = 'EFT'
                        {
                            pnd_Ws_No_Of_Transactions.nadd(1);                                                                                                            //Natural: ADD 1 TO #WS-NO-OF-TRANSACTIONS
                        }                                                                                                                                                 //Natural: END-IF
                        //*  LZ 08-13-97
                                                                                                                                                                          //Natural: PERFORM CREATE-TYPE-6-RECORD
                        sub_Create_Type_6_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //* *  RESET #PRE-NOTE-AMT
                        //* *  ADD 1 TO #PRE-NOTE-CNT
                        //* *  PERFORM WRITE-REPORT
                        //* *  ASSIGN #PRE-NOTE-AMT = #OUTPUT.PYMNT-CHECK-AMT
                        pnd_Pymnt_Amt.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                                             //Natural: ASSIGN #PYMNT-AMT := #RPT-EXT.PYMNT-CHECK-AMT
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                        sub_Write_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //*                                                                                                                                                   //Natural: AT END OF DATA
                    readWork01Pnd_Ws_Pymnt_Check_AmtSum572.nadd(readWork01Pnd_Ws_Pymnt_Check_AmtSum572,pnd_Ws_Pnd_Ws_Pymnt_Check_Amt);                                    //Natural: END-WORK
                    readWork01Pnd_Ws_Pymnt_Check_AmtSum.nadd(readWork01Pnd_Ws_Pymnt_Check_AmtSum,pnd_Ws_Pnd_Ws_Pymnt_Check_Amt);
                    readWork01Pnd_Ws_Pymnt_Check_AmtSum585.nadd(readWork01Pnd_Ws_Pymnt_Check_AmtSum585,pnd_Ws_Pnd_Ws_Pymnt_Check_Amt);
                    readWork01Pymnt_Check_DteOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());
                    readWork01Pnd_Ws_Pymnt_Check_AmtSum554.nadd(readWork01Pnd_Ws_Pymnt_Check_AmtSum554,pnd_Ws_Pnd_Ws_Pymnt_Check_Amt);
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                    pnd_Eod.setValue(true);                                                                                                                               //Natural: ASSIGN #EOD := TRUE
                    if (condition(pnd_Prime_Counter.equals(getZero())))                                                                                                   //Natural: IF #PRIME-COUNTER = 0
                    {
                    }                                                                                                                                                     //Natural: ESCAPE BOTTOM;//Natural: END-IF
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape())){return;}
                    getReports().write(1, ReportOption.NOTITLE,"GRAND TOTAL AMOUNT",new TabSetting(65),readWork01Pnd_Ws_Pymnt_Check_AmtSum);                              //Natural: WRITE ( 1 ) 'GRAND TOTAL AMOUNT' 65T SUM ( #WS-PYMNT-CHECK-AMT )
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //* *IF #PRIME-COUNTER = 0   /* 03-15-95 : A. YOUNG   /* LZ 08-13-97
                //* *  #PRE-NOTE-CNT := 1
                //* *END-IF
                if (condition(pnd_Prime_Counter.equals(getZero())))                                                                                                       //Natural: IF #PRIME-COUNTER = 0
                {
                    ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr().reset();                                                                                                  //Natural: RESET #RPT-EXT.#PYMNT-ADDR
                    pnd_Company_Name.setValue("TIAA-CREF DISTR.");                                                                                                        //Natural: ASSIGN #COMPANY-NAME := 'TIAA-CREF DISTR.'
                    pnd_Company_Id.setValue(9457224010L);                                                                                                                 //Natural: ASSIGN #COMPANY-ID := 9457224010
                    ldaFcpl280.getWs_File_Header_Record_1_Ws_Company_Descriptive_Date_5().moveAll("0");                                                                   //Natural: MOVE ALL '0' TO WS-COMPANY-DESCRIPTIVE-DATE-5
                                                                                                                                                                          //Natural: PERFORM CREATE-TYPE-5-RECORD
                    sub_Create_Type_5_Record();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-DUMMY-PRENOTE-RECORD
                    sub_Create_Dummy_Prenote_Record();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-TYPE-8-RECORD
                    sub_Create_Type_8_Record();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Input_Parm_Pnd_Parm_Type.equals(2)))                                                                                                //Natural: IF #PARM-TYPE = 2
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(49),"NO EFT RECORDS TO BE TRANSMITTED"); //Natural: WRITE ( 1 ) / / / / / / 49T 'NO EFT RECORDS TO BE TRANSMITTED'
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Input_Parm_Pnd_Parm_Type.equals(3)))                                                                                                //Natural: IF #PARM-TYPE = 3
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),"NO GLOBAL RECORDS TO BE TRANSMITTED"); //Natural: WRITE ( 1 ) / / / / / / 48T 'NO GLOBAL RECORDS TO BE TRANSMITTED'
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Record_Type_9().setValue("9");                                                                                   //Natural: ASSIGN WS-RECORD-TYPE-9 = '9'
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Batch_Count_9().setValue(pnd_Batch_Cnt);                                                                         //Natural: ASSIGN WS-BATCH-COUNT-9 = #BATCH-CNT
                pnd_Block_Cnt.compute(new ComputeParameters(false, pnd_Block_Cnt), pnd_Block_Cnt.add(4).add(pnd_Total_Recs));                                             //Natural: ADD 4 #TOTAL-RECS TO #BLOCK-CNT
                pnd_Ws_Remainder.compute(new ComputeParameters(false, pnd_Ws_Remainder), pnd_Block_Cnt.mod(10));                                                          //Natural: DIVIDE 10 INTO #BLOCK-CNT REMAINDER #WS-REMAINDER
                pnd_Block_Cnt.compute(new ComputeParameters(false, pnd_Block_Cnt), pnd_Block_Cnt.divide(10));
                if (condition(pnd_Ws_Remainder.greater(getZero())))                                                                                                       //Natural: IF #WS-REMAINDER > 0
                {
                    pnd_Block_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #BLOCK-CNT
                }                                                                                                                                                         //Natural: END-IF
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Block_Count_9().setValue(pnd_Block_Cnt);                                                                         //Natural: ASSIGN WS-BLOCK-COUNT-9 = #BLOCK-CNT
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Entry_Addenda_Count_9().setValue(pnd_Total_Recs);                                                                //Natural: ASSIGN WS-ENTRY-ADDENDA-COUNT-9 = #TOTAL-RECS
                //*  03-30-95 : A. YOUNG
                if (condition(pnd_Prime_Counter.equals(getZero())))                                                                                                       //Natural: IF #PRIME-COUNTER = 0
                {
                    ldaFcpl280.getWs_File_Header_Record_1_Ws_Entry_Hash_9().setValue(2100002);                                                                            //Natural: ASSIGN WS-ENTRY-HASH-9 := 0002100002
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl280.getWs_File_Header_Record_1_Ws_Entry_Hash_9().setValue(pnd_G_Hash_Pnd_G_Hash_10);                                                           //Natural: ASSIGN WS-ENTRY-HASH-9 = #G-HASH-10
                    //*  03-30-95 : A. YOUNG
                }                                                                                                                                                         //Natural: END-IF
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Total_Debit_9().setValue(0);                                                                                     //Natural: ASSIGN WS-TOTAL-DEBIT-9 = 0
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Total_Credit_9().setValue(pnd_G_Total);                                                                          //Natural: ASSIGN WS-TOTAL-CREDIT-9 = #G-TOTAL
                ldaFcpl280.getWs_File_Header_Record_1_Filler_9().setValue(" ");                                                                                           //Natural: ASSIGN FILLER-9 = ' '
                //*  LZ 09-16-96
                getWorkFiles().write(12, false, ldaFcpl280.getWs_File_Header_Record_1());                                                                                 //Natural: WRITE WORK FILE 12 WS-FILE-HEADER-RECORD-1
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"GRAND TOTAL OF EFT RECORDS    :",pnd_Ws_Total_Transactions, new ReportEditMask ("ZZ,ZZZ,ZZ9"));       //Natural: WRITE ( 1 ) / 'GRAND TOTAL OF EFT RECORDS    :' #WS-TOTAL-TRANSACTIONS ( EM = ZZ,ZZZ,ZZ9 )
                if (Global.isEscape()) return;
                //* */ 'GRAND TOTAL OF PRENOTE RECORDS:'
                //* *#PRE-NOTE-CNT(EM=ZZ,ZZZ,ZZ9)
                //* *#WS-NO-OF-TRANSACTIONS(EM=ZZ,ZZZ,ZZ9)
                //*  EFT
                short decideConditionsMet719 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #PARM-TYPE;//Natural: VALUE 2
                if (condition((pnd_Input_Parm_Pnd_Parm_Type.equals(2))))
                {
                    decideConditionsMet719++;
                    pnd_Ws_Diff_No_Of_Transactions.compute(new ComputeParameters(false, pnd_Ws_Diff_No_Of_Transactions), pnd_Control_Record_Pnd_Cntl_Cnt_Eft.subtract(pnd_Ws_Total_Transactions)); //Natural: ASSIGN #WS-DIFF-NO-OF-TRANSACTIONS := #CONTROL-RECORD.#CNTL-CNT-EFT - #WS-TOTAL-TRANSACTIONS
                    pnd_Ws_Diff_Total_Amount.compute(new ComputeParameters(false, pnd_Ws_Diff_Total_Amount), pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft.subtract(pnd_Ws_Total_Amount).subtract(pnd_Ws_Cntr_Held_Total_Amount).subtract(pnd_Ws_Total_Pa_Amount)); //Natural: ASSIGN #WS-DIFF-TOTAL-AMOUNT := #CONTROL-RECORD.#CNTL-NET-AMT-EFT - #WS-TOTAL-AMOUNT - #WS-CNTR-HELD-TOTAL-AMOUNT - #WS-TOTAL-PA-AMOUNT
                    //*  GLOBAL
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"CPS DATABASE TOTAL AMOUNT:          ",pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft,            //Natural: WRITE ( 2 ) NOTITLE // 'CPS DATABASE TOTAL AMOUNT:          ' #CONTROL-RECORD.#CNTL-NET-AMT-EFT 8X 'NUMBER OF TRANSACTIONS:     ' #CONTROL-RECORD.#CNTL-CNT-EFT // 'FCPP198 TOTAL EFT AMOUNT (NON-HELD):' #WS-TOTAL-AMOUNT 8X 'NUMBER OF EFT TRANSACTIONS: ' #WS-NO-OF-TRANSACTIONS / 'FCPP198 TOTAL EFT AMOUNT (HELD):    ' #WS-CNTR-HELD-TOTAL-AMOUNT 8X 'NUMBER OF EFT TRANSACTIONS: ' #WS-CNTR-HELD / 'FCPP198 TOTAL PA AMOUNT:            ' #WS-TOTAL-PA-AMOUNT 8X 'NUMBER OF PA TRANSACTIONS:  ' #WS-NO-OF-PA-TRANSACTIONS
                        new ReportEditMask ("ZZZZZZZZ9.99"),new ColumnSpacing(8),"NUMBER OF TRANSACTIONS:     ",pnd_Control_Record_Pnd_Cntl_Cnt_Eft, new 
                        ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,"FCPP198 TOTAL EFT AMOUNT (NON-HELD):",pnd_Ws_Total_Amount,new ColumnSpacing(8),"NUMBER OF EFT TRANSACTIONS: ",pnd_Ws_No_Of_Transactions,NEWLINE,"FCPP198 TOTAL EFT AMOUNT (HELD):    ",pnd_Ws_Cntr_Held_Total_Amount,new 
                        ColumnSpacing(8),"NUMBER OF EFT TRANSACTIONS: ",pnd_Ws_Cntr_Held,NEWLINE,"FCPP198 TOTAL PA AMOUNT:            ",pnd_Ws_Total_Pa_Amount,new 
                        ColumnSpacing(8),"NUMBER OF PA TRANSACTIONS:  ",pnd_Ws_No_Of_Pa_Transactions);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_Input_Parm_Pnd_Parm_Type.equals(3))))
                {
                    decideConditionsMet719++;
                    pnd_Ws_Diff_No_Of_Transactions.compute(new ComputeParameters(false, pnd_Ws_Diff_No_Of_Transactions), pnd_Control_Record_Pnd_Cntl_Cnt_Global.subtract(pnd_Ws_No_Of_Transactions).subtract(pnd_Ws_Cntr_Held)); //Natural: SUBTRACT #WS-NO-OF-TRANSACTIONS #WS-CNTR-HELD FROM #CONTROL-RECORD.#CNTL-CNT-GLOBAL GIVING #WS-DIFF-NO-OF-TRANSACTIONS
                    pnd_Ws_Diff_Total_Amount.compute(new ComputeParameters(false, pnd_Ws_Diff_Total_Amount), pnd_Control_Record_Pnd_Cntl_Net_Amt_Global.subtract(pnd_Ws_Total_Amount).subtract(pnd_Ws_Cntr_Held_Total_Amount)); //Natural: SUBTRACT #WS-TOTAL-AMOUNT #WS-CNTR-HELD-TOTAL-AMOUNT FROM #CONTROL-RECORD.#CNTL-NET-AMT-GLOBAL GIVING #WS-DIFF-TOTAL-AMOUNT
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"CPS DATABASE TOTAL AMOUNT:          ",pnd_Control_Record_Pnd_Cntl_Net_Amt_Global,         //Natural: WRITE ( 2 ) NOTITLE // 'CPS DATABASE TOTAL AMOUNT:          ' #CONTROL-RECORD.#CNTL-NET-AMT-GLOBAL 8X 'NUMBER OF TRANSACTIONS:     ' #CONTROL-RECORD.#CNTL-CNT-GLOBAL // 'FCPP198 TOTAL AMOUNT (NON-HELD):    ' #WS-TOTAL-AMOUNT 8X 'NUMBER OF TRANSACTIONS:     ' #WS-NO-OF-TRANSACTIONS / 'FCPP198 TOTAL AMOUNT (HELD):        ' #WS-CNTR-HELD-TOTAL-AMOUNT 8X 'NUMBER OF TRANSACTIONS:     ' #WS-CNTR-HELD
                        new ReportEditMask ("ZZZZZZZZ9.99"),new ColumnSpacing(8),"NUMBER OF TRANSACTIONS:     ",pnd_Control_Record_Pnd_Cntl_Cnt_Global, 
                        new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,"FCPP198 TOTAL AMOUNT (NON-HELD):    ",pnd_Ws_Total_Amount,new ColumnSpacing(8),"NUMBER OF TRANSACTIONS:     ",pnd_Ws_No_Of_Transactions,NEWLINE,"FCPP198 TOTAL AMOUNT (HELD):        ",pnd_Ws_Cntr_Held_Total_Amount,new 
                        ColumnSpacing(8),"NUMBER OF TRANSACTIONS:     ",pnd_Ws_Cntr_Held);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet719 > 0))
                {
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,"DIFFERENCE TOTAL AMOUNT:            ",pnd_Ws_Diff_Total_Amount,new ColumnSpacing(8),              //Natural: WRITE ( 2 ) NOTITLE / 'DIFFERENCE TOTAL AMOUNT:            ' #WS-DIFF-TOTAL-AMOUNT 8X 'NUMBER OF TRANSACTIONS:     ' #WS-DIFF-NO-OF-TRANSACTIONS
                        "NUMBER OF TRANSACTIONS:     ",pnd_Ws_Diff_No_Of_Transactions);
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //* *#HEADER0-2 := 'TRANSMITTAL REGISTER FOR CPS ON-LINE PAYMENTS'
                pnd_Dollar_Amt.setValueEdited(pnd_G_Total,new ReportEditMask("X'$'Z,ZZZ,ZZZ,ZZ9.99"));                                                                    //Natural: MOVE EDITED #G-TOTAL ( EM = X'$'Z,ZZZ,ZZZ,ZZ9.99 ) TO #DOLLAR-AMT
                DbsUtil.examine(new ExamineSource(pnd_Dollar_Amt,true), new ExamineSearch("X", true), new ExamineDelete());                                               //Natural: EXAMINE FULL #DOLLAR-AMT FOR FULL 'X' DELETE
                pnd_Dollar_Amt.setValue(pnd_Dollar_Amt, MoveOption.RightJustified);                                                                                       //Natural: MOVE RIGHT #DOLLAR-AMT TO #DOLLAR-AMT
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape())){return;}
                getReports().write(2, ReportOption.NOTITLE,NEWLINE,"IMMEDIATE",new ColumnSpacing(28),"IMMEDIATE",NEWLINE,new ColumnSpacing(1),"ORIGIN NUMBER:  0094-4000-1",new  //Natural: WRITE ( 2 ) NOTITLE / 'IMMEDIATE' 28X 'IMMEDIATE' / 1X 'ORIGIN NUMBER:  0094-4000-1' 12X 'DESTINATION NUMBER:  0210-0002-1' / 'COMPANY ID:' #COMPANY-ID // 'IMMEDIATE' 28X 'IMMEDIATE' / 1X 'ORIGIN NAME:  TIAA CREF ANNUITY' 8X 'DESTINATION NAME:  CHASE MANHATTAN BANK NA' /// 'TRANSMISSION' 8X 'FILE ID' 8X 'FILE' 8X 'SPECIFICATION' 8X 'EXTERNAL' / 4X 'DATE' 11X 'MODIFIER' 7X 'MEDIA' 12X 'FORMAT' 12X 'FILE ID' // 2X FCP-CONS-CNTRL.CNTL-CYCLE-DTE ( EM = MM-DD-YY ) 13X 'E' 10X '91600' 13X 'FIXED' 11X '(      )' /// 9X 'TOTAL DEBITS' 17X 'TOTAL CREDITS' 17X 'FILE COUNTS' / '-' ( 29 ) 2X '-' ( 29 ) 2X '-' ( 25 ) // 66X 'ENTRY/' / 5X 'NUMBER' 12X 'AMOUNT' 7X 'NUMBER' 12X 'AMOUNT' 5X 'ADDENDA' 11X 'HASH' / 10X '0' 13X '$0.00' 5X #WS-TOTAL-TRANSACTIONS 1X #DOLLAR-AMT 5X #TOTAL-RECS 5X #G-HASH-10 /// 'AUTHORIZED SIGNATURE: ' 28X 'DATE: '
                    ColumnSpacing(12),"DESTINATION NUMBER:  0210-0002-1",NEWLINE,"COMPANY ID:",pnd_Company_Id,NEWLINE,NEWLINE,"IMMEDIATE",new ColumnSpacing(28),"IMMEDIATE",NEWLINE,new 
                    ColumnSpacing(1),"ORIGIN NAME:  TIAA CREF ANNUITY",new ColumnSpacing(8),"DESTINATION NAME:  CHASE MANHATTAN BANK NA",NEWLINE,NEWLINE,NEWLINE,"TRANSMISSION",new 
                    ColumnSpacing(8),"FILE ID",new ColumnSpacing(8),"FILE",new ColumnSpacing(8),"SPECIFICATION",new ColumnSpacing(8),"EXTERNAL",NEWLINE,new 
                    ColumnSpacing(4),"DATE",new ColumnSpacing(11),"MODIFIER",new ColumnSpacing(7),"MEDIA",new ColumnSpacing(12),"FORMAT",new ColumnSpacing(12),"FILE ID",NEWLINE,NEWLINE,new 
                    ColumnSpacing(2),ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cycle_Dte(), new ReportEditMask ("MM-DD-YY"),new ColumnSpacing(13),"E",new ColumnSpacing(10),"91600",new 
                    ColumnSpacing(13),"FIXED",new ColumnSpacing(11),"(      )",NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(9),"TOTAL DEBITS",new ColumnSpacing(17),"TOTAL CREDITS",new 
                    ColumnSpacing(17),"FILE COUNTS",NEWLINE,"-",new RepeatItem(29),new ColumnSpacing(2),"-",new RepeatItem(29),new ColumnSpacing(2),"-",new 
                    RepeatItem(25),NEWLINE,NEWLINE,new ColumnSpacing(66),"ENTRY/",NEWLINE,new ColumnSpacing(5),"NUMBER",new ColumnSpacing(12),"AMOUNT",new 
                    ColumnSpacing(7),"NUMBER",new ColumnSpacing(12),"AMOUNT",new ColumnSpacing(5),"ADDENDA",new ColumnSpacing(11),"HASH",NEWLINE,new ColumnSpacing(10),"0",new 
                    ColumnSpacing(13),"$0.00",new ColumnSpacing(5),pnd_Ws_Total_Transactions,new ColumnSpacing(1),pnd_Dollar_Amt,new ColumnSpacing(5),pnd_Total_Recs,new 
                    ColumnSpacing(5),pnd_G_Hash_Pnd_G_Hash_10,NEWLINE,NEWLINE,NEWLINE,"AUTHORIZED SIGNATURE: ",new ColumnSpacing(28),"DATE: ");
                if (Global.isEscape()) return;
                //* */ 10X '0' 13X '$0.00' 5X #WS-NO-OF-TRANSACTIONS 1X
                if (condition(pnd_Ws_Diff_No_Of_Transactions.notEquals(getZero()) || pnd_Ws_Diff_Total_Amount.notEquals(getZero())))                                      //Natural: IF #WS-DIFF-NO-OF-TRANSACTIONS NOT = 0 OR #WS-DIFF-TOTAL-AMOUNT NOT = 0
                {
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"(ERROR MESSAGE)");                                                                        //Natural: WRITE ( 2 ) NOTITLE // '(ERROR MESSAGE)'
                    if (Global.isEscape()) return;
                    if (condition(pnd_Input_Parm_Pnd_Parm_Type.equals(2)))                                                                                                //Natural: IF #PARM-TYPE = 2
                    {
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"FCPP198 EFT CONTROL TOTALS   DO NOT BALANCE  ",new ColumnSpacing(1),"TO CPS EFT CONTROL TOTALS"); //Natural: WRITE ( 2 ) NOTITLE / 'FCPP198 EFT CONTROL TOTALS   DO NOT BALANCE  ' 1X 'TO CPS EFT CONTROL TOTALS'
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Input_Parm_Pnd_Parm_Type.equals(3)))                                                                                                //Natural: IF #PARM-TYPE = 3
                    {
                        getReports().write(2, ReportOption.NOTITLE,NEWLINE,"FCPP198 GLOBAL CONTROL TOTALS   DO NOT BALANCE  ",new ColumnSpacing(1),"TO CPS GLOBAL CONTROL TOTALS"); //Natural: WRITE ( 2 ) NOTITLE / 'FCPP198 GLOBAL CONTROL TOTALS   DO NOT BALANCE  ' 1X 'TO CPS GLOBAL CONTROL TOTALS'
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*-----------------------------*",NEWLINE,"* CPS SYSTEM HAS BEEN ABORTED *",               //Natural: WRITE ( 2 ) NOTITLE // '*-----------------------------*' / '* CPS SYSTEM HAS BEEN ABORTED *' / '*-----------------------------*'
                        NEWLINE,"*-----------------------------*");
                    if (Global.isEscape()) return;
                    //*  TERMINATE 0099
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-COMPANY-NAME-ID
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TYPE-5-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TYPE-6-RECORD
                //* *WRITE WORK FILE 12 WS-FILE-HEADER-RECORD-1
                //* *ADD 1 TO #BATCH-RECS
                //* *ADD 1 TO #TOTAL-RECS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-TYPE-8-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-DUMMY-PRENOTE-RECORD
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EFT-LEDGER-ENTRIES-REPORT
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
                //* ***********************************************************************
                //*     'ANNUITANT/LAST NAME'   #OUTPUT.PH-LAST-NAME
                //* *2X 'EFT OR/GLB AMOUNT'     #PRE-NOTE-AMT
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: JULIAN-DATE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Determine_Company_Name_Id() throws Exception                                                                                                         //Natural: DETERMINE-COMPANY-NAME-ID
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* ** 08-24-94 : A. YOUNG THIS SUBROUTINE WAS CREATED TO PROVIDE FOR THE
        //* **                     PROCESSING OF VARIOUS ORIGIN CODES.
        //* *IF #OUTPUT.CNTRCT-ORGN-CDE  = 'DS' OR = 'SS'
        pnd_Company_Id.setValue(9457224010L);                                                                                                                             //Natural: ASSIGN #COMPANY-ID = 9457224010
        pnd_Company_Name.setValue("TIAA-CREF DISTR.");                                                                                                                    //Natural: ASSIGN #COMPANY-NAME = 'TIAA-CREF DISTR.'
        //* *ELSE
        //* *   IF #OUTPUT.CNTRCT-ORGN-CDE = 'IA'
        //* *      IGNORE
        //* *   ELSE
        //* *      IF #OUTPUT.CNTRCT-ORGN-CDE = 'MS' OR = 'DC'
        //* *         ASSIGN #COMPANY-ID = 9457224009
        //* *         ASSIGN #COMPANY-NAME = 'TIAA-CREF ANN.'
        //* *      END-IF
        //* *   END-IF
        //* *END-IF
    }
    private void sub_Create_Type_5_Record() throws Exception                                                                                                              //Natural: CREATE-TYPE-5-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Batch_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #BATCH-CNT
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Record_Type_5().setValue("5");                                                                                           //Natural: ASSIGN WS-RECORD-TYPE-5 = '5'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Service_Class_5().setValue(200);                                                                                         //Natural: ASSIGN WS-SERVICE-CLASS-5 = 200
        //* *ASSIGN WS-COMPANY-NAME-5              = 'TIAA-CREF ANN.'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Company_Name_5().setValue(pnd_Company_Name);                                                                             //Natural: ASSIGN WS-COMPANY-NAME-5 = #COMPANY-NAME
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Company_Discretion_Data_5().setValue(" ");                                                                               //Natural: ASSIGN WS-COMPANY-DISCRETION-DATA-5 = ' '
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Company_Id_5().setValue(pnd_Company_Id);                                                                                 //Natural: ASSIGN WS-COMPANY-ID-5 = #COMPANY-ID
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Standard_Entry_Class_5().setValue("PPD");                                                                                //Natural: ASSIGN WS-STANDARD-ENTRY-CLASS-5 = 'PPD'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Company_Entry_Description_5().setValue("ANNUITY");                                                                       //Natural: ASSIGN WS-COMPANY-ENTRY-DESCRIPTION-5 = 'ANNUITY'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Company_Descriptive_Date_5().setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("MMDDYY"));  //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = MMDDYY ) TO WS-COMPANY-DESCRIPTIVE-DATE-5
        //*  03-30-95 : A. YOUNG
        if (condition(pnd_Prime_Counter.equals(getZero())))                                                                                                               //Natural: IF #PRIME-COUNTER = 0
        {
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Effective_Entry_Date_5().setValueEdited(pnd_Ws_Date_Yyyymmdd_Pnd_Ws_Date_Yymmdd,new ReportEditMask("999999"));       //Natural: MOVE EDITED #WS-DATE-YYMMDD ( EM = 999999 ) TO WS-EFFECTIVE-ENTRY-DATE-5
            //*  WS-EFFECTIVE-ENTRY-DATE-5 := #WS-DATE-YYMMDD /* EB, ADD EM= ABOVE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Effective_Entry_Date_5().setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYMMDD"));  //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYMMDD ) TO WS-EFFECTIVE-ENTRY-DATE-5
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Settlement_Date_5().reset();                                                                                             //Natural: RESET WS-SETTLEMENT-DATE-5
                                                                                                                                                                          //Natural: PERFORM JULIAN-DATE
        sub_Julian_Date();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Settlement_Date_5().setValue(julian_Out_Julian_Out_A);                                                                   //Natural: MOVE JULIAN-OUT-A TO WS-SETTLEMENT-DATE-5
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Originator_Status_Code_5().setValue("1");                                                                                //Natural: ASSIGN WS-ORIGINATOR-STATUS-CODE-5 = '1'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Originator_Id_5().setValue("00944000");                                                                                  //Natural: ASSIGN WS-ORIGINATOR-ID-5 = '00944000'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Batch_Number_5().setValue(pnd_Batch_Cnt);                                                                                //Natural: ASSIGN WS-BATCH-NUMBER-5 = #BATCH-CNT
        getWorkFiles().write(12, false, ldaFcpl280.getWs_File_Header_Record_1());                                                                                         //Natural: WRITE WORK FILE 12 WS-FILE-HEADER-RECORD-1
        ldaFcpl280.getWs_File_Header_Record_1().reset();                                                                                                                  //Natural: RESET WS-FILE-HEADER-RECORD-1
    }
    private void sub_Create_Type_6_Record() throws Exception                                                                                                              //Natural: CREATE-TYPE-6-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Batch_Recs.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #BATCH-RECS
        pnd_Total_Recs.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #TOTAL-RECS
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Record_Type_6().setValue("6");                                                                                           //Natural: ASSIGN WS-RECORD-TYPE-6 := '6'
        pnd_Ws_Hash_Amount.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Transit_Id());                                                                                   //Natural: ASSIGN #WS-HASH-AMOUNT := #RPT-EXT.PYMNT-EFT-TRANSIT-ID
        pnd_Ws_Entry_Hash.nadd(pnd_Ws_Hash_Amount_Pnd_Ws_Hash_Amount_8);                                                                                                  //Natural: ADD #WS-HASH-AMOUNT-8 TO #WS-ENTRY-HASH
        pnd_G_Hash.nadd(pnd_Ws_Hash_Amount_Pnd_Ws_Hash_Amount_8);                                                                                                         //Natural: ADD #WS-HASH-AMOUNT-8 TO #G-HASH
        short decideConditionsMet856 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #RPT-EXT.PYMNT-CHK-SAV-IND;//Natural: VALUE 'C'
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Chk_Sav_Ind().equals("C"))))
        {
            decideConditionsMet856++;
            pnd_Ws_Account_Type.setValue("CHECKING");                                                                                                                     //Natural: ASSIGN #WS-ACCOUNT-TYPE := 'CHECKING'
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Transaction_Code_6().setValue(23);                                                                                   //Natural: ASSIGN WS-TRANSACTION-CODE-6 := 23
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Chk_Sav_Ind().equals("S"))))
        {
            decideConditionsMet856++;
            pnd_Ws_Account_Type.setValue("SAVINGS");                                                                                                                      //Natural: ASSIGN #WS-ACCOUNT-TYPE := 'SAVINGS'
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Transaction_Code_6().setValue(33);                                                                                   //Natural: ASSIGN WS-TRANSACTION-CODE-6 := 33
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Ws_Account_Type.setValue("UNKNOWN");                                                                                                                      //Natural: ASSIGN #WS-ACCOUNT-TYPE := 'UNKNOWN'
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Amount_6().reset();                                                                                                      //Natural: RESET WS-AMOUNT-6
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Dfi_Id_N_Check_Digit().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Transit_Id());                                      //Natural: ASSIGN WS-DFI-ID-N-CHECK-DIGIT := #RPT-EXT.PYMNT-EFT-TRANSIT-ID
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Dfi_Acct_Number_6().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr());                                           //Natural: ASSIGN WS-DFI-ACCT-NUMBER-6 := #RPT-EXT.PYMNT-EFT-ACCT-NBR
        //* *WS-INDIVIDUAL-ID-6 := #OUTPUT.CNTRCT-PPCN-NBR
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Individual_Id_6().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr(), //Natural: COMPRESS #RPT-EXT.CNTRCT-PPCN-NBR #RPT-EXT.CNTRCT-PAYEE-CDE INTO WS-INDIVIDUAL-ID-6 LEAVING NO SPACE
            ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde()));
        //*    IF #OUTPUT.PYMNT-NME (1) = MASK('CR ')
        //*      WS-INDIVIDUAL-NAME-6 :=
        //*        SUBSTRING(#OUTPUT.PYMNT-NME (1),4,22)
        //*    ELSE
        //*      WS-INDIVIDUAL-NAME-6 := #OUTPUT.PYMNT-NME (1)
        //*    END-IF
        pnd_Wrk_Fld.reset();                                                                                                                                              //Natural: RESET #WRK-FLD
        pnd_Wrk_Fld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name().getSubstring(1,1));                                                                              //Natural: MOVE SUBSTRING ( #RPT-EXT.PH-MIDDLE-NAME,1,1 ) TO #WRK-FLD
        pnd_Wrk_Fld.setValue(DbsUtil.compress(ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), pnd_Wrk_Fld, ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name()));                       //Natural: COMPRESS #RPT-EXT.PH-FIRST-NAME #WRK-FLD #RPT-EXT.PH-LAST-NAME INTO #WRK-FLD
        DbsUtil.examine(new ExamineSource(pnd_Wrk_Fld), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                        //Natural: EXAMINE #WRK-FLD FOR ' ' GIVING LENGTH #LEN
        if (condition(pnd_Len.greater(22)))                                                                                                                               //Natural: IF #LEN GT 22
        {
            pnd_Wrk_Fld.reset();                                                                                                                                          //Natural: RESET #WRK-FLD
            pnd_One_Byte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name().getSubstring(1,1));                                                                          //Natural: MOVE SUBSTRING ( #RPT-EXT.PH-FIRST-NAME,1,1 ) TO #ONE-BYTE
            pnd_Wrk_Fld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name().getSubstring(1,1));                                                                          //Natural: MOVE SUBSTRING ( #RPT-EXT.PH-MIDDLE-NAME,1,1 ) TO #WRK-FLD
            pnd_Wrk_Fld.setValue(DbsUtil.compress(pnd_One_Byte, pnd_Wrk_Fld, ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name()));                                                 //Natural: COMPRESS #ONE-BYTE #WRK-FLD #RPT-EXT.PH-LAST-NAME INTO #WRK-FLD
        }                                                                                                                                                                 //Natural: END-IF
        //*  RC 10/02/98
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AL")))                                                                                         //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'AL'
        {
            pnd_Wrk_Fld.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "3", pnd_Wrk_Fld));                                                                      //Natural: COMPRESS '3' #WRK-FLD INTO #WRK-FLD LEAVING NO
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Individual_Name_6().setValue(pnd_Wrk_Fld);                                                                               //Natural: ASSIGN WS-INDIVIDUAL-NAME-6 := #WRK-FLD
        //* *IF #RPT-EXT.CNTRCT-DA-TIAA-1-NBR NE ' ' /* FOR PA ROLLOVER 09-16-96 LZ
        //*  06-15-2000
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr().notEquals(" ")) && (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS"))))                 //Natural: IF ( #RPT-EXT.CNTRCT-DA-TIAA-1-NBR NE ' ' ) AND ( #RPT-EXT.CNTRCT-ORGN-CDE = 'SS' )
        {
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Individual_Name_6().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr());                                     //Natural: MOVE #RPT-EXT.CNTRCT-DA-TIAA-1-NBR TO WS-INDIVIDUAL-NAME-6
            //*  04-18-97  SETTLEMENT DATE IN NAME FIELD TOGETHER WITH PA ROLLOVER
            pnd_Work_Yyyymmdd.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD"));                                             //Natural: MOVE EDITED #RPT-EXT.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO #WORK-YYYYMMDD
            setValueToSubstring(pnd_Work_Yyyymmdd,ldaFcpl280.getWs_File_Header_Record_1_Ws_Individual_Name_6(),11,8);                                                     //Natural: MOVE #WORK-YYYYMMDD TO SUBSTRING ( WS-INDIVIDUAL-NAME-6,11,8 )
            setValueToSubstring(ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name(),ldaFcpl280.getWs_File_Header_Record_1_Ws_Individual_Name_6(),19,4);                             //Natural: MOVE #RPT-EXT.PH-LAST-NAME TO SUBSTRING ( WS-INDIVIDUAL-NAME-6,19,4 )
            pnd_Ws_Last_Name_A22.setValue(ldaFcpl280.getWs_File_Header_Record_1_Ws_Individual_Name_6());                                                                  //Natural: MOVE WS-INDIVIDUAL-NAME-6 TO #WS-LAST-NAME-A22
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Last_Name_A22.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name());                                                                                     //Natural: MOVE #RPT-EXT.PH-LAST-NAME TO #WS-LAST-NAME-A22
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Discretionary_Data_6().setValue(" ");                                                                                    //Natural: ASSIGN WS-DISCRETIONARY-DATA-6 := ' '
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Addenda_Record_Indicator_6().setValue(0);                                                                                //Natural: ASSIGN WS-ADDENDA-RECORD-INDICATOR-6 := 0
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Prefix_Trace_6().setValue("00944000");                                                                                   //Natural: ASSIGN WS-PREFIX-TRACE-6 := '00944000'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Trace_Number_6().setValue(pnd_Batch_Recs);                                                                               //Natural: ASSIGN WS-TRACE-NUMBER-6 := #BATCH-RECS
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Suffix_Trace_6().setValue(0);                                                                                            //Natural: ASSIGN WS-SUFFIX-TRACE-6 := 0
        pnd_Ws_Total_Credit.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                                                           //Natural: ASSIGN #WS-TOTAL-CREDIT := #WS-TOTAL-CREDIT + #RPT-EXT.PYMNT-CHECK-AMT
        //* *IF #RPT-EXT.CNTRCT-DA-TIAA-1-NBR = ' '    /* FOR EFT ONLY 09-16-96 LZ
        //*  06-15-2000
        if (condition(pnd_Eft_Or_Pa_Ind.equals("EFT")))                                                                                                                   //Natural: IF #EFT-OR-PA-IND = 'EFT'
        {
            pnd_Ws_Total_Amount.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                                                       //Natural: ASSIGN #WS-TOTAL-AMOUNT := #WS-TOTAL-AMOUNT + #RPT-EXT.PYMNT-CHECK-AMT
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Amount_6().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                       //Natural: ASSIGN WS-AMOUNT-6 := #RPT-EXT.PYMNT-CHECK-AMT
        if (condition(ldaFcpl280.getWs_File_Header_Record_1_Ws_Transaction_Code_6().equals(23)))                                                                          //Natural: IF WS-TRANSACTION-CODE-6 = 23
        {
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Transaction_Code_6().setValue(22);                                                                                   //Natural: ASSIGN WS-TRANSACTION-CODE-6 := 22
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaFcpl280.getWs_File_Header_Record_1_Ws_Transaction_Code_6().equals(33)))                                                                      //Natural: IF WS-TRANSACTION-CODE-6 = 33
            {
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Transaction_Code_6().setValue(32);                                                                               //Natural: ASSIGN WS-TRANSACTION-CODE-6 := 32
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl280.getWs_File_Header_Record_1_Ws_Transaction_Code_6().setValue(0);                                                                                //Natural: ASSIGN WS-TRANSACTION-CODE-6 := 00
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *ADD #WS-HASH-AMOUNT-8 TO #WS-ENTRY-HASH   /* LZ 08-13-97
        //* *ADD #WS-HASH-AMOUNT-8 TO #G-HASH          /* LZ 08-13-97
        getWorkFiles().write(12, false, ldaFcpl280.getWs_File_Header_Record_1());                                                                                         //Natural: WRITE WORK FILE 12 WS-FILE-HEADER-RECORD-1
        ldaFcpl280.getWs_File_Header_Record_1().reset();                                                                                                                  //Natural: RESET WS-FILE-HEADER-RECORD-1
    }
    private void sub_Create_Type_8_Record() throws Exception                                                                                                              //Natural: CREATE-TYPE-8-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Record_Type_8().setValue("8");                                                                                           //Natural: ASSIGN WS-RECORD-TYPE-8 = '8'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Service_Class_8().setValue(200);                                                                                         //Natural: ASSIGN WS-SERVICE-CLASS-8 = 200
        //*  03-15-95 : A. YOUNG
        //*  03-30-95 : A. YOUNG
        if (condition(pnd_Prime_Counter.equals(getZero())))                                                                                                               //Natural: IF #PRIME-COUNTER = 0
        {
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Entry_Addenda_Count_8().setValue(1);                                                                                 //Natural: ASSIGN WS-ENTRY-ADDENDA-COUNT-8 := 1
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Entry_Hash_8().setValue(2100002);                                                                                    //Natural: ASSIGN WS-ENTRY-HASH-8 := 0002100002
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  * 2  /* LZ 08-13-97
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Entry_Addenda_Count_8().setValue(pnd_Batch_Recs);                                                                    //Natural: COMPUTE WS-ENTRY-ADDENDA-COUNT-8 = #BATCH-RECS
            ldaFcpl280.getWs_File_Header_Record_1_Ws_Entry_Hash_8().setValue(pnd_Ws_Entry_Hash_Pnd_Ws_Entry_Hash_10);                                                     //Natural: ASSIGN WS-ENTRY-HASH-8 = #WS-ENTRY-HASH-10
            //*  03-15-95 : A. YOUNG
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Total_Debit_8().setValue(0);                                                                                             //Natural: ASSIGN WS-TOTAL-DEBIT-8 = 0
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Total_Credit_8().setValue(pnd_Ws_Total_Credit);                                                                          //Natural: ASSIGN WS-TOTAL-CREDIT-8 = #WS-TOTAL-CREDIT
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Company_Id_8().setValue(pnd_Company_Id);                                                                                 //Natural: ASSIGN WS-COMPANY-ID-8 = #COMPANY-ID
        ldaFcpl280.getWs_File_Header_Record_1_Filler_8().setValue(" ");                                                                                                   //Natural: ASSIGN FILLER-8 = ' '
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Originator_Id_8().setValue("00944000");                                                                                  //Natural: ASSIGN WS-ORIGINATOR-ID-8 = '00944000'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Batch_Number_8().setValue(pnd_Batch_Cnt);                                                                                //Natural: ASSIGN WS-BATCH-NUMBER-8 = #BATCH-CNT
        getWorkFiles().write(12, false, ldaFcpl280.getWs_File_Header_Record_1());                                                                                         //Natural: WRITE WORK FILE 12 WS-FILE-HEADER-RECORD-1
        pnd_G_Total.nadd(pnd_Ws_Total_Credit);                                                                                                                            //Natural: ADD #WS-TOTAL-CREDIT TO #G-TOTAL
        pnd_Ws_Total_Credit.reset();                                                                                                                                      //Natural: RESET #WS-TOTAL-CREDIT #WS-ENTRY-HASH WS-FILE-HEADER-RECORD-1
        pnd_Ws_Entry_Hash.reset();
        ldaFcpl280.getWs_File_Header_Record_1().reset();
    }
    private void sub_Create_Dummy_Prenote_Record() throws Exception                                                                                                       //Natural: CREATE-DUMMY-PRENOTE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Batch_Recs.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #BATCH-RECS
        pnd_Total_Recs.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #TOTAL-RECS
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Record_Type_6().setValue("6");                                                                                           //Natural: ASSIGN WS-RECORD-TYPE-6 := '6'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Transaction_Code_6().setValue(23);                                                                                       //Natural: ASSIGN WS-TRANSACTION-CODE-6 := 23
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Dfi_Id_N_Check_Digit().setValue(21000021);                                                                               //Natural: ASSIGN WS-DFI-ID-N-CHECK-DIGIT := 021000021
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Dfi_Acct_Number_6().setValue("9102457224");                                                                              //Natural: ASSIGN WS-DFI-ACCT-NUMBER-6 := '9102457224'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Amount_6().setValue(0);                                                                                                  //Natural: ASSIGN WS-AMOUNT-6 := 0
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Individual_Id_6().setValue("9457224010");                                                                                //Natural: ASSIGN WS-INDIVIDUAL-ID-6 := '9457224010'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Individual_Name_6().setValue("TIAA");                                                                                    //Natural: ASSIGN WS-INDIVIDUAL-NAME-6 := 'TIAA'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Discretionary_Data_6().setValue(" ");                                                                                    //Natural: ASSIGN WS-DISCRETIONARY-DATA-6 := ' '
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Addenda_Record_Indicator_6().setValue(0);                                                                                //Natural: ASSIGN WS-ADDENDA-RECORD-INDICATOR-6 := 0
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Prefix_Trace_6().setValue("00944000");                                                                                   //Natural: ASSIGN WS-PREFIX-TRACE-6 := '00944000'
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Trace_Number_6().setValue(pnd_Batch_Recs);                                                                               //Natural: ASSIGN WS-TRACE-NUMBER-6 := #BATCH-RECS
        ldaFcpl280.getWs_File_Header_Record_1_Ws_Suffix_Trace_6().setValue(0);                                                                                            //Natural: ASSIGN WS-SUFFIX-TRACE-6 := 0
        //*  PRENOTE
        getWorkFiles().write(12, false, ldaFcpl280.getWs_File_Header_Record_1());                                                                                         //Natural: WRITE WORK FILE 12 WS-FILE-HEADER-RECORD-1
        pnd_Ws_Total_Credit.setValue(0);                                                                                                                                  //Natural: ASSIGN #WS-TOTAL-CREDIT := 0
        pnd_Ws_Total_Amount.setValue(0);                                                                                                                                  //Natural: ASSIGN #WS-TOTAL-AMOUNT := 0
        ldaFcpl280.getWs_File_Header_Record_1().reset();                                                                                                                  //Natural: RESET WS-FILE-HEADER-RECORD-1
    }
    private void sub_Eft_Ledger_Entries_Report() throws Exception                                                                                                         //Natural: EFT-LEDGER-ENTRIES-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  08-24-94 : A. YOUNG
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new ColumnSpacing(27),"CONSOLIDATED PAYMENT SYSTEM");                                                  //Natural: WRITE ( 2 ) NOTITLE // 27X 'CONSOLIDATED PAYMENT SYSTEM'
        if (Global.isEscape()) return;
        if (condition(pnd_Input_Parm_Pnd_Parm_Type.equals(2)))                                                                                                            //Natural: IF #PARM-TYPE = 2
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(29),"PAYMENT ENTRIES FOR EFT");                                                          //Natural: WRITE ( 2 ) NOTITLE / 29X 'PAYMENT ENTRIES FOR EFT'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Parm_Pnd_Parm_Type.equals(3)))                                                                                                            //Natural: IF #PARM-TYPE = 3
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(29),"PAYMENT ENTRIES FOR GLOBAL");                                                       //Natural: WRITE ( 2 ) NOTITLE / 29X 'PAYMENT ENTRIES FOR GLOBAL'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  08-24-94 : A. YOUNG
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(34),"DUE",pnd_Pymnt_Dte, new ReportEditMask ("MM/DD/YY"));                                   //Natural: WRITE ( 2 ) NOTITLE / 34X 'DUE' #PYMNT-DTE ( EM = MM/DD/YY )
        if (Global.isEscape()) return;
        if (condition(pnd_Input_Parm_Pnd_Parm_Type.equals(2)))                                                                                                            //Natural: IF #PARM-TYPE = 2
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(31),"U.S. PAYMENTS ONLY");                                                               //Natural: WRITE ( 2 ) NOTITLE / 31X 'U.S. PAYMENTS ONLY'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Input_Parm_Pnd_Parm_Type.equals(3)))                                                                                                            //Natural: IF #PARM-TYPE = 3
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(31),"GLOBAL PAYMENTS ONLY");                                                             //Natural: WRITE ( 2 ) NOTITLE / 31X 'GLOBAL PAYMENTS ONLY'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(20),"DR",new ColumnSpacing(48),"CR",NEWLINE,new ColumnSpacing(1),"06006030",NEWLINE,new  //Natural: WRITE ( 2 ) NOTITLE /// 20X 'DR' 48X 'CR' / 1X '06006030' / 20X #WS-TOTAL-CREDIT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 40X '06020010' / 60X #WS-TOTAL-CREDIT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) /// 20X #WS-TOTAL-CREDIT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) / 60X #WS-TOTAL-CREDIT ( EM = Z,ZZZ,ZZZ,ZZ9.99 )
            ColumnSpacing(20),pnd_Ws_Total_Credit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new ColumnSpacing(40),"06020010",NEWLINE,new ColumnSpacing(60),pnd_Ws_Total_Credit, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(20),pnd_Ws_Total_Credit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            ColumnSpacing(60),pnd_Ws_Total_Credit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        //*  A. YOUNG
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
    }
    //*  LZ 09-16-96
    //*  LZ 08-13-97
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "ANNUITANT/LAST NAME",                                                                                                                    //Natural: DISPLAY ( 1 ) 'ANNUITANT/LAST NAME' #WS-LAST-NAME-A22 2X 'ANNUITANT/FIRST NAME' #RPT-EXT.PH-FIRST-NAME ( AL = 14 ) 2X 'ANNUITANT/MIDDLE NAME' #RPT-EXT.PH-MIDDLE-NAME ( AL = 12 ) 2X 'PPCN' #RPT-EXT.CNTRCT-PPCN-NBR 2X 'EFT OR/GLB AMOUNT' #PYMNT-AMT 2X 'BANK ACCOUNT NO.' #RPT-EXT.PYMNT-EFT-ACCT-NBR 2X 'ACCOUNT/TYPE' #WS-ACCOUNT-TYPE 2X 'BANK TRANSIT/ID NO.' #RPT-EXT.PYMNT-EFT-TRANSIT-ID ( EM = 999999999 ) 2X 'ORIGIN/ CODE' #RPT-EXT.CNTRCT-ORGN-CDE
        		pnd_Ws_Last_Name_A22,new ColumnSpacing(2),"ANNUITANT/FIRST NAME",
        		ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), new AlphanumericLength (14),new ColumnSpacing(2),"ANNUITANT/MIDDLE NAME",
        		ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name(), new AlphanumericLength (12),new ColumnSpacing(2),"PPCN",
        		ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr(),new ColumnSpacing(2),"EFT OR/GLB AMOUNT",
        		pnd_Pymnt_Amt,new ColumnSpacing(2),"BANK ACCOUNT NO.",
        		ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr(),new ColumnSpacing(2),"ACCOUNT/TYPE",
        		pnd_Ws_Account_Type,new ColumnSpacing(2),"BANK TRANSIT/ID NO.",
        		ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Transit_Id(), new ReportEditMask ("999999999"),new ColumnSpacing(2),"ORIGIN/ CODE",
        		ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());
        if (Global.isEscape()) return;
    }
    private void sub_Julian_Date() throws Exception                                                                                                                       //Natural: JULIAN-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        today_Julian.setValue(Global.getDATJ());                                                                                                                          //Natural: MOVE *DATJ TO TODAY-JULIAN
        effective_Date.setValueEdited(new ReportEditMask("YYMMDD"),ldaFcpl280.getWs_File_Header_Record_1_Ws_Effective_Entry_Date_5());                                    //Natural: MOVE EDITED WS-EFFECTIVE-ENTRY-DATE-5 TO EFFECTIVE-DATE ( EM = YYMMDD )
        if (condition(effective_Date.lessOrEqual(Global.getDATX())))                                                                                                      //Natural: IF EFFECTIVE-DATE LE *DATX
        {
            julian_Out.setValue(today_Julian_Julian_Ddd);                                                                                                                 //Natural: MOVE JULIAN-DDD TO JULIAN-OUT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            julian_Out.compute(new ComputeParameters(false, julian_Out), (effective_Date.subtract(Global.getDATX())).add(today_Julian_Julian_Ddd));                       //Natural: COMPUTE JULIAN-OUT = ( EFFECTIVE-DATE - *DATX ) + JULIAN-DDD
            if (condition(julian_Out.greaterOrEqual(366)))                                                                                                                //Natural: IF JULIAN-OUT GE 366
            {
                julian_Out.nsubtract(365);                                                                                                                                //Natural: SUBTRACT 365 FROM JULIAN-OUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, ReportOption.NOTITLE,"JULIAN:",julian_Out);                                                                                                 //Natural: WRITE 'JULIAN:' JULIAN-OUT
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *INIT-USER '-' *PROGRAM 41T #HEADER0-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YY ) 43T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YY"),new 
                        TabSetting(43),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    //*  A. YOUNG
                    if (condition(pnd_Eod.getBoolean()))                                                                                                                  //Natural: IF #EOD
                    {
                        //*      3/20/2000
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(60),"DUE",pnd_Due_Date_D, new ReportEditMask ("MM/DD/YY"),              //Natural: WRITE ( 1 ) // 60T 'DUE' #DUE-DATE-D ( EM = MM/DD/YY ) /
                            NEWLINE);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(60),"DUE",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),                 //Natural: WRITE ( 1 ) // 60T 'DUE' #RPT-EXT.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) /
                            new ReportEditMask ("MM/DD/YY"),NEWLINE);
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Eft_Or_Pa_IndIsBreak = pnd_Eft_Or_Pa_Ind.isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().isBreak(endOfData);
        if (condition(pnd_Eft_Or_Pa_IndIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak))
        {
            if (condition(pnd_Input_Parm_Pnd_Parm_Eft_Glb.equals("EFT")))                                                                                                 //Natural: IF #PARM-EFT-GLB = 'EFT'
            {
                if (condition(pnd_Eft_Date_Counter.notEquals(getZero())))                                                                                                 //Natural: IF #EFT-DATE-COUNTER NE 0
                {
                    getReports().write(1, ReportOption.NOTITLE,"EFT DATE SUB-TOTAL",new TabSetting(65),readWork01Pnd_Ws_Pymnt_Check_AmtSum572);                           //Natural: WRITE ( 1 ) 'EFT DATE SUB-TOTAL' 65T SUM ( #WS-PYMNT-CHECK-AMT )
                    if (condition(Global.isEscape())) return;
                    pnd_Eft_Date_Counter.reset();                                                                                                                         //Natural: RESET #EFT-DATE-COUNTER
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Pa_Date_Counter.notEquals(getZero())))                                                                                                  //Natural: IF #PA-DATE-COUNTER NE 0
                {
                    getReports().write(1, ReportOption.NOTITLE,"PA DATE SUB-TOTAL",new TabSetting(65),readWork01Pnd_Ws_Pymnt_Check_AmtSum572);                            //Natural: WRITE ( 1 ) 'PA DATE SUB-TOTAL' 65T SUM ( #WS-PYMNT-CHECK-AMT )
                    if (condition(Global.isEscape())) return;
                    pnd_Pa_Date_Counter.reset();                                                                                                                          //Natural: RESET #PA-DATE-COUNTER
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            readWork01Pnd_Ws_Pymnt_Check_AmtSum572.setDec(new DbsDecimal(0));                                                                                             //Natural: END-BREAK
        }
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak))
        {
            if (condition(pnd_Prime_Counter.notEquals(getZero())))                                                                                                        //Natural: IF #PRIME-COUNTER NE 0
            {
                getReports().write(1, ReportOption.NOTITLE,"DATE SUB-TOTAL",new TabSetting(65),readWork01Pnd_Ws_Pymnt_Check_AmtSum585);                                   //Natural: WRITE ( 1 ) 'DATE SUB-TOTAL' 65T SUM ( #WS-PYMNT-CHECK-AMT )
                if (condition(Global.isEscape())) return;
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                pnd_Check_Date_Break.setValue(true);                                                                                                                      //Natural: ASSIGN #CHECK-DATE-BREAK := TRUE
                pnd_Pymnt_Dte.setValue(readWork01Pymnt_Check_DteOld);                                                                                                     //Natural: ASSIGN #PYMNT-DTE := OLD ( #RPT-EXT.PYMNT-CHECK-DTE )
                                                                                                                                                                          //Natural: PERFORM EFT-LEDGER-ENTRIES-REPORT
                sub_Eft_Ledger_Entries_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-TYPE-8-RECORD
                sub_Create_Type_8_Record();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            readWork01Pnd_Ws_Pymnt_Check_AmtSum585.setDec(new DbsDecimal(0));                                                                                             //Natural: END-BREAK
        }
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, "ANNUITANT/LAST NAME",
        		pnd_Ws_Last_Name_A22,new ColumnSpacing(2),"ANNUITANT/FIRST NAME",
        		ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), new AlphanumericLength (14),new ColumnSpacing(2),"ANNUITANT/MIDDLE NAME",
        		ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name(), new AlphanumericLength (12),new ColumnSpacing(2),"PPCN",
        		ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr(),new ColumnSpacing(2),"EFT OR/GLB AMOUNT",
        		pnd_Pymnt_Amt,new ColumnSpacing(2),"BANK ACCOUNT NO.",
        		ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr(),new ColumnSpacing(2),"ACCOUNT/TYPE",
        		pnd_Ws_Account_Type,new ColumnSpacing(2),"BANK TRANSIT/ID NO.",
        		ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Transit_Id(), new ReportEditMask ("999999999"),new ColumnSpacing(2),"ORIGIN/ CODE",
        		ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());
    }
}
