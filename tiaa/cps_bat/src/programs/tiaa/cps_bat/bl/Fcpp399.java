/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:40 PM
**        * FROM NATURAL PROGRAM : Fcpp399
************************************************************
**        * FILE NAME            : Fcpp399.java
**        * CLASS NAME           : Fcpp399
**        * INSTANCE NAME        : Fcpp399
************************************************************
************************************************************************
* PROGRAM  : FCPP399
* SYSTEM   : CPS
*
*
* FUNCTION : THIS PROGRAM WILL CREATE ALTERNATE CARRIER FACT SHEET FILE
*            TO BE USED IN EFM UPDATE.
* HISTORY
*
* 11/14/97  F.ORTIZ  EXPAND INPUT RECORD ACFS-DATA TO 4 BYTES
*                    TO INCLUDE FLAGS TO IDENTIFY STS AND INITIAL PAYMNT
*
*  03/12/99 :  R. CARREON
*              RESTOW - ADDITION OF PYMNT-SPOUSE-PAY-STATS IN FCPL378
*                       ADDITION OF THE 24TH OCCUR IN FCPL236
*
* 01/07/2000 : LEON GURTOVNIK
*              RESTOW TO ACCEPT CHNG IN FCPL236 (ADDED 25TH OCCURANCE)
*    11/2002 : R CARREON   STOW - EGTRRA CHANGES IN LDA
*    4/2017: JJG - PIN EXPANSION CHANGES
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp399 extends BLNatBase
{
    // Data Areas
    private LdaFcpl378 ldaFcpl378;
    private LdaFcpl236 ldaFcpl236;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Record;
    private DbsField pnd_Ws_Pnd_I_Key;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd;
    private DbsField pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd;
    private DbsField pnd_Ws_Pnd_I_Rec_Det_1;
    private DbsField pnd_Ws_Pnd_I_Rec_Det_2;
    private DbsField pnd_Ws_Pnd_Timestmp;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Timestmp_A;
    private DbsField pnd_Ws_Pnd_Acfs_Data;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Acfs_Good_Letter;
    private DbsField pnd_Ws_Pnd_Acfs_Good_Multi;
    private DbsField pnd_Ws_Pnd_Acfs_Sts;
    private DbsField pnd_Ws_Pnd_Acfs_Initial_Pymnt;
    private DbsField pnd_Ws_Pnd_Acfs_Dpi;
    private DbsField pnd_Ws_Pnd_Acfs_Pymnt_Nme;
    private DbsField pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Ws_Pnd_Acfs_Invalid_Reasons;
    private DbsField pnd_Ws_Pnd_Type_Display;
    private DbsField pnd_Ws_Pnd_Bad_Contract_Display;

    private DbsGroup pnd_Ws_Pnd_Record_Counts;
    private DbsField pnd_Ws_Pnd_Records_All;
    private DbsField pnd_Ws_Pnd_Records_10;
    private DbsField pnd_Ws_Pnd_Records_20;
    private DbsField pnd_Ws_Pnd_Records_30;
    private DbsField pnd_Ws_Pnd_Records_40;
    private DbsField pnd_Ws_Pnd_Records_45;
    private DbsField pnd_Ws_Pnd_Records_Other;
    private DbsField pnd_Ws_Pnd_Payments_Processed;
    private DbsField pnd_Ws_Pnd_Page_Number;
    private DbsField pnd_Ws_Pnd_Type_Sub;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Accept_Ind;
    private DbsField pnd_Pymnt_Split_Reasn_Cde;

    private DbsGroup pnd_Pymnt_Split_Reasn_Cde__R_Field_4;
    private DbsField pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde;
    private DbsField pnd_Pymnt_Split_Reasn_Cde_Pnd_Pymnt_Init_Rqst_Ind;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl378 = new LdaFcpl378();
        registerRecord(ldaFcpl378);
        ldaFcpl236 = new LdaFcpl236();
        registerRecord(ldaFcpl236);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Record = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Record", "#INPUT-RECORD");
        pnd_Ws_Pnd_I_Key = pnd_Ws_Pnd_Input_Record.newFieldInGroup("pnd_Ws_Pnd_I_Key", "#I-KEY", FieldType.STRING, 41);

        pnd_Ws__R_Field_1 = pnd_Ws_Pnd_Input_Record.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_I_Key);
        pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd", "#I-KEY-REC-LVL-#", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd", "#I-KEY-REC-OCCUR-#", FieldType.NUMERIC, 
            2);
        pnd_Ws_Pnd_I_Rec_Det_1 = pnd_Ws_Pnd_Input_Record.newFieldInGroup("pnd_Ws_Pnd_I_Rec_Det_1", "#I-REC-DET-1", FieldType.STRING, 250);
        pnd_Ws_Pnd_I_Rec_Det_2 = pnd_Ws_Pnd_Input_Record.newFieldInGroup("pnd_Ws_Pnd_I_Rec_Det_2", "#I-REC-DET-2", FieldType.STRING, 209);
        pnd_Ws_Pnd_Timestmp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timestmp", "#TIMESTMP", FieldType.BINARY, 8);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Timestmp);
        pnd_Ws_Pnd_Timestmp_A = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Timestmp_A", "#TIMESTMP-A", FieldType.STRING, 8);
        pnd_Ws_Pnd_Acfs_Data = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Data", "#ACFS-DATA", FieldType.STRING, 4);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Acfs_Data);
        pnd_Ws_Pnd_Acfs_Good_Letter = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Acfs_Good_Letter", "#ACFS-GOOD-LETTER", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Acfs_Good_Multi = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Acfs_Good_Multi", "#ACFS-GOOD-MULTI", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Acfs_Sts = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Acfs_Sts", "#ACFS-STS", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Acfs_Initial_Pymnt = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Acfs_Initial_Pymnt", "#ACFS-INITIAL-PYMNT", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Acfs_Dpi = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Dpi", "#ACFS-DPI", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Acfs_Pymnt_Nme = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Pymnt_Nme", "#ACFS-PYMNT-NME", FieldType.STRING, 38);
        pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line1_Txt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line1_Txt", "#ACFS-PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35);
        pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line2_Txt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line2_Txt", "#ACFS-PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35);
        pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line3_Txt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line3_Txt", "#ACFS-PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35);
        pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line4_Txt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line4_Txt", "#ACFS-PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35);
        pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line5_Txt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line5_Txt", "#ACFS-PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35);
        pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line6_Txt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line6_Txt", "#ACFS-PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35);
        pnd_Ws_Pnd_Acfs_Invalid_Reasons = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Acfs_Invalid_Reasons", "#ACFS-INVALID-REASONS", FieldType.STRING, 50, 
            new DbsArrayController(1, 10));
        pnd_Ws_Pnd_Type_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        pnd_Ws_Pnd_Bad_Contract_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Bad_Contract_Display", "#BAD-CONTRACT-DISPLAY", FieldType.STRING, 62);

        pnd_Ws_Pnd_Record_Counts = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Record_Counts", "#RECORD-COUNTS");
        pnd_Ws_Pnd_Records_All = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_All", "#RECORDS-ALL", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 2));
        pnd_Ws_Pnd_Records_10 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_10", "#RECORDS-10", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 2));
        pnd_Ws_Pnd_Records_20 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_20", "#RECORDS-20", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 2));
        pnd_Ws_Pnd_Records_30 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_30", "#RECORDS-30", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 2));
        pnd_Ws_Pnd_Records_40 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_40", "#RECORDS-40", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 2));
        pnd_Ws_Pnd_Records_45 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_45", "#RECORDS-45", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 2));
        pnd_Ws_Pnd_Records_Other = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_Other", "#RECORDS-OTHER", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Payments_Processed = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Payments_Processed", "#PAYMENTS-PROCESSED", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Page_Number = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_Type_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Type_Sub", "#TYPE-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Accept_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accept_Ind", "#ACCEPT-IND", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Split_Reasn_Cde = localVariables.newFieldInRecord("pnd_Pymnt_Split_Reasn_Cde", "#PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);

        pnd_Pymnt_Split_Reasn_Cde__R_Field_4 = localVariables.newGroupInRecord("pnd_Pymnt_Split_Reasn_Cde__R_Field_4", "REDEFINE", pnd_Pymnt_Split_Reasn_Cde);
        pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde = pnd_Pymnt_Split_Reasn_Cde__R_Field_4.newFieldInGroup("pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde", 
            "#SWAT-FRQNCY-CDE", FieldType.STRING, 1);
        pnd_Pymnt_Split_Reasn_Cde_Pnd_Pymnt_Init_Rqst_Ind = pnd_Pymnt_Split_Reasn_Cde__R_Field_4.newFieldInGroup("pnd_Pymnt_Split_Reasn_Cde_Pnd_Pymnt_Init_Rqst_Ind", 
            "#PYMNT-INIT-RQST-IND", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl378.initializeValues();
        ldaFcpl236.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Bad_Contract_Display.setInitialValue("SOURCE ACCUMULATION DOES NOT EQUAL TO CONTRACTUAL ACCUMULATION");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp399() throws Exception
    {
        super("Fcpp399");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) LS = 133 PS = 58 ZP = ON;//Natural: FORMAT ( 1 ) LS = 133 PS = 58 ZP = ON;//Natural: FORMAT ( 2 ) LS = 121 PS = 20 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'SINGLE SUM ACFS BATCH CONTROL REPORT' 120T 'REPORT: RPT1' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-RECORD
        while (condition(getWorkFiles().read(1, pnd_Ws_Pnd_Input_Record)))
        {
            short decideConditionsMet324 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #I-KEY-REC-LVL-#;//Natural: VALUE 10
            if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(10))))
            {
                decideConditionsMet324++;
                ldaFcpl378.getPnd_Pymnt_Ext_Key().setValue(pnd_Ws_Pnd_I_Key);                                                                                             //Natural: MOVE #I-KEY TO #PYMNT-EXT-KEY
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1().setValue(pnd_Ws_Pnd_I_Rec_Det_1);                                                            //Natural: MOVE #I-REC-DET-1 TO #REC-TYPE-10-DET-1
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2().setValue(pnd_Ws_Pnd_I_Rec_Det_2);                                                            //Natural: MOVE #I-REC-DET-2 TO #REC-TYPE-10-DET-2
                if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().getBoolean()))                                                                              //Natural: IF #ACFS
                {
                    pnd_Ws_Pnd_Accept_Ind.setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #ACCEPT-IND
                    pnd_Ws_Pnd_Payments_Processed.nadd(1);                                                                                                                //Natural: ADD 1 TO #PAYMENTS-PROCESSED
                    pnd_Ws_Pnd_I.setValue(2);                                                                                                                             //Natural: MOVE 2 TO #I
                    pnd_Ws_Pnd_Acfs_Sts.reset();                                                                                                                          //Natural: RESET #ACFS-STS #ACFS-INITIAL-PYMNT
                    pnd_Ws_Pnd_Acfs_Initial_Pymnt.reset();
                    pnd_Pymnt_Split_Reasn_Cde.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Split_Reasn_Cde());                                                     //Natural: MOVE #REC-TYPE-10-DETAIL.PYMNT-SPLIT-REASN-CDE TO #PYMNT-SPLIT-REASN-CDE
                    //*  STS
                    if (condition(pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("M")))                                                                             //Natural: IF #SWAT-FRQNCY-CDE = 'M'
                    {
                        pnd_Ws_Pnd_Acfs_Sts.setValue(true);                                                                                                               //Natural: ASSIGN #ACFS-STS := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Pymnt_Split_Reasn_Cde_Pnd_Pymnt_Init_Rqst_Ind.equals("Y")))                                                                         //Natural: IF #PYMNT-INIT-RQST-IND = 'Y'
                    {
                        pnd_Ws_Pnd_Acfs_Initial_Pymnt.setValue(true);                                                                                                     //Natural: ASSIGN #ACFS-INITIAL-PYMNT := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-10
                    sub_Process_Record_Type_10();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Accept_Ind.setValue(false);                                                                                                                //Natural: MOVE FALSE TO #ACCEPT-IND
                    pnd_Ws_Pnd_I.setValue(1);                                                                                                                             //Natural: MOVE 1 TO #I
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Records_10.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-10 ( #I )
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(20))))
            {
                decideConditionsMet324++;
                pnd_Ws_Pnd_Records_20.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-20 ( #I )
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-20
                sub_Process_Record_Type_20();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(30))))
            {
                decideConditionsMet324++;
                pnd_Ws_Pnd_Records_30.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-30 ( #I )
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-30
                sub_Process_Record_Type_30();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 40
            else if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(40))))
            {
                decideConditionsMet324++;
                pnd_Ws_Pnd_Records_40.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-40 ( #I )
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-40
                sub_Process_Record_Type_40();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 45
            else if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(45))))
            {
                decideConditionsMet324++;
                pnd_Ws_Pnd_Records_45.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-45 ( #I )
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-45
                sub_Process_Record_Type_45();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Records_Other.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                  //Natural: ADD 1 TO #RECORDS-OTHER ( #I )
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Records_10.getValue(1).nadd(pnd_Ws_Pnd_Records_10.getValue(2));                                                                                        //Natural: ADD #RECORDS-10 ( 2 ) TO #RECORDS-10 ( 1 )
        pnd_Ws_Pnd_Records_20.getValue(1).nadd(pnd_Ws_Pnd_Records_20.getValue(2));                                                                                        //Natural: ADD #RECORDS-20 ( 2 ) TO #RECORDS-20 ( 1 )
        pnd_Ws_Pnd_Records_30.getValue(1).nadd(pnd_Ws_Pnd_Records_30.getValue(2));                                                                                        //Natural: ADD #RECORDS-30 ( 2 ) TO #RECORDS-30 ( 1 )
        pnd_Ws_Pnd_Records_40.getValue(1).nadd(pnd_Ws_Pnd_Records_40.getValue(2));                                                                                        //Natural: ADD #RECORDS-40 ( 2 ) TO #RECORDS-40 ( 1 )
        pnd_Ws_Pnd_Records_45.getValue(1).nadd(pnd_Ws_Pnd_Records_45.getValue(2));                                                                                        //Natural: ADD #RECORDS-45 ( 2 ) TO #RECORDS-45 ( 1 )
        pnd_Ws_Pnd_Records_Other.getValue(1).nadd(pnd_Ws_Pnd_Records_Other.getValue(2));                                                                                  //Natural: ADD #RECORDS-OTHER ( 2 ) TO #RECORDS-OTHER ( 1 )
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 2
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(2)); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Records_All.getValue(pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_Ws_Pnd_Records_All.getValue(pnd_Ws_Pnd_I)), pnd_Ws_Pnd_Records_10.getValue(pnd_Ws_Pnd_I).add(pnd_Ws_Pnd_Records_20.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_30.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_40.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_45.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_Other.getValue(pnd_Ws_Pnd_I))); //Natural: ADD #RECORDS-10 ( #I ) #RECORDS-20 ( #I ) #RECORDS-30 ( #I ) #RECORDS-40 ( #I ) #RECORDS-45 ( #I ) #RECORDS-OTHER ( #I ) GIVING #RECORDS-ALL ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(14),"COUNTERS AT END OF PROGRAM:",NEWLINE,new TabSetting(17),"RECORDS READ:",NEWLINE,new TabSetting(17),"...............ALL:",pnd_Ws_Pnd_Records_All.getValue(1),  //Natural: WRITE ( 1 ) 14T 'COUNTERS AT END OF PROGRAM:' / 17T 'RECORDS READ:' / 17T '...............ALL:' #RECORDS-ALL ( 1 ) / 17T '...........TYPE 10:' #RECORDS-10 ( 1 ) / 17T '...........TYPE 20:' #RECORDS-20 ( 1 ) / 17T '...........TYPE 30:' #RECORDS-30 ( 1 ) / 17T '...........TYPE 40:' #RECORDS-40 ( 1 ) / 17T '...........TYPE 45:' #RECORDS-45 ( 1 ) / 17T '.............OTHER:' #RECORDS-OTHER ( 1 ) // 17T 'RECORDS ACCEPTED (ACFS):' / 17T '...............ALL:' #RECORDS-ALL ( 2 ) / 17T '...........TYPE 10:' #RECORDS-10 ( 2 ) / 17T '...........TYPE 20:' #RECORDS-20 ( 2 ) / 17T '...........TYPE 30:' #RECORDS-30 ( 2 ) / 17T '...........TYPE 40:' #RECORDS-40 ( 2 ) / 17T '...........TYPE 45:' #RECORDS-45 ( 2 ) / 17T '.............OTHER:' #RECORDS-OTHER ( 2 ) // 17T 'PAYMENTS PROCESSED:' #PAYMENTS-PROCESSED //// 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************'
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"...........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"...........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),".............OTHER:",pnd_Ws_Pnd_Records_Other.getValue(1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"RECORDS ACCEPTED (ACFS):",NEWLINE,new TabSetting(17),"...............ALL:",pnd_Ws_Pnd_Records_All.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"...........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"...........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),".............OTHER:",pnd_Ws_Pnd_Records_Other.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"PAYMENTS PROCESSED:",pnd_Ws_Pnd_Payments_Processed, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new 
            TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************");
        if (Global.isEscape()) return;
        //*  WRITE A HEADER RECORD FOR EFM UPDATE FILE.
        pnd_Ws_Pnd_Timestmp.setValue(Global.getTIMESTMP());                                                                                                               //Natural: MOVE *TIMESTMP TO #TIMESTMP
        getReports().write(2, ReportOption.NOTITLE,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),new TabSetting(81),pnd_Ws_Pnd_Timestmp_A,new            //Natural: WRITE ( 2 ) *TIMX ( EM = MM/DD/YYYY�HH:II:SS.T ) 81T #TIMESTMP-A 97T 'HD ACFS001'
            TabSetting(97),"HD ACFS001");
        if (Global.isEscape()) return;
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-10
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-20
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-30
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-40
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-45
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-END-LINE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-INVALID-REASONS
    }
    private void sub_Process_Record_Type_10() throws Exception                                                                                                            //Natural: PROCESS-RECORD-TYPE-10
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        DbsUtil.examine(new ExamineSource(ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde().getValue("*")), new ExamineSearch(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Lob_Cde()),  //Natural: EXAMINE #FCPL236-LOB-CDE ( * ) FOR CNTRCT-LOB-CDE GIVING INDEX IN #TYPE-SUB
            new ExamineGivingIndex(pnd_Ws_Pnd_Type_Sub));
        if (condition(pnd_Ws_Pnd_Type_Sub.equals(getZero())))                                                                                                             //Natural: IF #TYPE-SUB = 0
        {
            pnd_Ws_Pnd_Type_Display.setValue("UNKNOWN");                                                                                                                  //Natural: MOVE 'UNKNOWN' TO #TYPE-DISPLAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Type_Display.setValue(ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data().getValue(pnd_Ws_Pnd_Type_Sub));                                         //Natural: MOVE #WARRANT-HDR-DATA ( #TYPE-SUB ) TO #TYPE-DISPLAY
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Record_Type_20() throws Exception                                                                                                            //Natural: PROCESS-RECORD-TYPE-20
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        if (condition(pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd.equals(1)))                                                                                                          //Natural: IF #I-KEY-REC-OCCUR-# = 1
        {
            pnd_Ws_Pnd_Acfs_Dpi.reset();                                                                                                                                  //Natural: RESET #ACFS-DPI
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1().setValue(pnd_Ws_Pnd_I_Rec_Det_1);                                                                    //Natural: MOVE #I-REC-DET-1 TO #REC-TYPE-20-DET-1
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2().setValue(pnd_Ws_Pnd_I_Rec_Det_2);                                                                    //Natural: MOVE #I-REC-DET-2 TO #REC-TYPE-20-DET-2
        pnd_Ws_Pnd_Acfs_Dpi.nadd(ldaFcpl378.getPnd_Rec_Type_20_Detail_Inv_Acct_Dpi_Amt());                                                                                //Natural: ADD INV-ACCT-DPI-AMT TO #ACFS-DPI
    }
    private void sub_Process_Record_Type_30() throws Exception                                                                                                            //Natural: PROCESS-RECORD-TYPE-30
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_1().setValue(pnd_Ws_Pnd_I_Rec_Det_1);                                                                    //Natural: MOVE #I-REC-DET-1 TO #REC-TYPE-30-DET-1
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_2().setValue(pnd_Ws_Pnd_I_Rec_Det_2);                                                                    //Natural: MOVE #I-REC-DET-2 TO #REC-TYPE-30-DET-2
        if (condition(pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd.equals(1)))                                                                                                          //Natural: IF #I-KEY-REC-OCCUR-# = 1
        {
            pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line1_Txt.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line1_Txt());                                                   //Natural: MOVE PYMNT-ADDR-LINE1-TXT TO #ACFS-PYMNT-ADDR-LINE1-TXT
            pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line2_Txt.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line2_Txt());                                                   //Natural: MOVE PYMNT-ADDR-LINE2-TXT TO #ACFS-PYMNT-ADDR-LINE2-TXT
            pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line3_Txt.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line3_Txt());                                                   //Natural: MOVE PYMNT-ADDR-LINE3-TXT TO #ACFS-PYMNT-ADDR-LINE3-TXT
            pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line4_Txt.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line4_Txt());                                                   //Natural: MOVE PYMNT-ADDR-LINE4-TXT TO #ACFS-PYMNT-ADDR-LINE4-TXT
            pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line5_Txt.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line5_Txt());                                                   //Natural: MOVE PYMNT-ADDR-LINE5-TXT TO #ACFS-PYMNT-ADDR-LINE5-TXT
            pnd_Ws_Pnd_Acfs_Pymnt_Addr_Line6_Txt.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Addr_Line6_Txt());                                                   //Natural: MOVE PYMNT-ADDR-LINE6-TXT TO #ACFS-PYMNT-ADDR-LINE6-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Acfs_Pymnt_Nme.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Nme());                                                                         //Natural: MOVE PYMNT-NME TO #ACFS-PYMNT-NME
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Record_Type_40() throws Exception                                                                                                            //Natural: PROCESS-RECORD-TYPE-40
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1().setValue(pnd_Ws_Pnd_I_Rec_Det_1);                                                                    //Natural: MOVE #I-REC-DET-1 TO #REC-TYPE-40-DET-1
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2().setValue(pnd_Ws_Pnd_I_Rec_Det_2);                                                                    //Natural: MOVE #I-REC-DET-2 TO #REC-TYPE-40-DET-2
        if (condition(pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd.equals(1)))                                                                                                          //Natural: IF #I-KEY-REC-OCCUR-# = 1
        {
            pnd_Ws_Pnd_Acfs_Good_Letter.setValue(ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean());                                                    //Natural: MOVE #GOOD-LETTER TO #ACFS-GOOD-LETTER
            pnd_Ws_Pnd_Acfs_Good_Multi.setValue(false);                                                                                                                   //Natural: MOVE FALSE TO #ACFS-GOOD-MULTI
            if (condition(ldaFcpl378.getPnd_Rec_Type_40_Detail_Cntrct_Good_Contract().getBoolean()))                                                                      //Natural: IF CNTRCT-GOOD-CONTRACT
            {
                pnd_Ws_Pnd_Bad_Contract_Display.reset();                                                                                                                  //Natural: RESET #BAD-CONTRACT-DISPLAY
                if (condition(ldaFcpl378.getPnd_Rec_Type_40_Detail_Cntrct_Multi_Payee().getBoolean()))                                                                    //Natural: IF CNTRCT-MULTI-PAYEE
                {
                    pnd_Ws_Pnd_Acfs_Good_Multi.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #ACFS-GOOD-MULTI
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Bad_Contract_Display.resetInitial();                                                                                                           //Natural: RESET INITIAL #BAD-CONTRACT-DISPLAY
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PAGE-NUMBER
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm399.class));                                                                           //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM399'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl378.getPnd_Rec_Type_40_Detail_Plan_Type().equals(" ")))                                                                                      //Natural: IF PLAN-TYPE = ' '
        {
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Plan_Type().setValue("UNKNOWN");                                                                                         //Natural: MOVE 'UNKNOWN' TO PLAN-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm399a.class));                                                                              //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM399A'
        if (condition(pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd.equals(ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top()) && ! (ldaFcpl378.getPnd_Rec_Type_40_Detail_Cntrct_Invalid_Cond_Ind().getBoolean()))) //Natural: IF #I-KEY-REC-OCCUR-# = #PLAN-DATA-TOP AND NOT CNTRCT-INVALID-COND-IND
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-END-LINE
            sub_Write_End_Line();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Record_Type_45() throws Exception                                                                                                            //Natural: PROCESS-RECORD-TYPE-45
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_1().setValue(pnd_Ws_Pnd_I_Rec_Det_1);                                                                    //Natural: MOVE #I-REC-DET-1 TO #REC-TYPE-45-DET-1
        ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Rec_Type_45_Det_2().setValue(pnd_Ws_Pnd_I_Rec_Det_2);                                                                    //Natural: MOVE #I-REC-DET-2 TO #REC-TYPE-45-DET-2
        if (condition(pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd.equals(1)))                                                                                                          //Natural: IF #I-KEY-REC-OCCUR-# = 1
        {
            pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue("*").reset();                                                                                                        //Natural: RESET #ACFS-INVALID-REASONS ( * )
            pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(1,":",8).setValue(ldaFcpl378.getPnd_Rec_Type_45_Detail_Invalid_Reasons().getValue("*"));                             //Natural: MOVE INVALID-REASONS ( * ) TO #ACFS-INVALID-REASONS ( 1:8 )
            if (condition(ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Invalid_Reasons_Top().lessOrEqual(8)))                                                                 //Natural: IF #INVALID-REASONS-TOP LE 8
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-INVALID-REASONS
                sub_Write_Invalid_Reasons();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Acfs_Invalid_Reasons.getValue(9,":",10).setValue(ldaFcpl378.getPnd_Rec_Type_45_Detail_Invalid_Reasons().getValue(1,":",2));                        //Natural: MOVE INVALID-REASONS ( 1:2 ) TO #ACFS-INVALID-REASONS ( 9:10 )
                                                                                                                                                                          //Natural: PERFORM WRITE-INVALID-REASONS
            sub_Write_Invalid_Reasons();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_End_Line() throws Exception                                                                                                                    //Natural: WRITE-END-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(81),ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Reqst_Log_Dte_Time(),ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde(),ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Unq_Id_Nbr(),  //Natural: WRITE ( 2 ) NOTITLE 81T #REC-TYPE-10-DETAIL.PYMNT-REQST-LOG-DTE-TIME #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE #REC-TYPE-10-DETAIL.CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) '99' #ACFS-DATA
            new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Page_Number, new ReportEditMask ("99999"),"99",pnd_Ws_Pnd_Acfs_Data);
        if (Global.isEscape()) return;
    }
    private void sub_Write_Invalid_Reasons() throws Exception                                                                                                             //Natural: WRITE-INVALID-REASONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm399b.class));                                                                              //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM399B'
                                                                                                                                                                          //Natural: PERFORM WRITE-END-LINE
        sub_Write_End_Line();
        if (condition(Global.isEscape())) {return;}
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=58 ZP=ON");
        Global.format(1, "LS=133 PS=58 ZP=ON");
        Global.format(2, "LS=121 PS=20 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(49),"SINGLE SUM ACFS BATCH CONTROL REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
}
