/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:29 PM
**        * FROM NATURAL PROGRAM : Fcpp233c
************************************************************
**        * FILE NAME            : Fcpp233c.java
**        * CLASS NAME           : Fcpp233c
**        * INSTANCE NAME        : Fcpp233c
************************************************************
***********************************************************************
* PROGRAM   : FCPP233C
* SYSTEM    : CPS
* TITLE     : TAX REPORT
* GENERATED :
* FUNCTION  : PRINT TAX REPORT
*           :
* HISTORY   :
* 08/05/99  : R. CARREON  -  CREATED
* 10/21/99  : ILSE BOHM   - CREATE HEADER FIELD FOR CHECK DATE.
*           :
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp233c extends BLNatBase
{
    // Data Areas
    private LdaFcpl233a ldaFcpl233a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Heading_Pymnt_Check_Dte;
    private DbsField pnd_Write_Data;
    private DbsField pnd_Ws_Soc_Sec;
    private DbsField pnd_Ws_Gross_Amt;
    private DbsField pnd_Ws_Taxable_Amt;
    private DbsField pnd_Ws_Reason;
    private DbsField pnd_Ws_Date;

    private DbsGroup pnd_Ws_Date__R_Field_1;
    private DbsField pnd_Ws_Date_Pnd_Ws_Date_1;
    private DbsField pnd_Ws_Date_Pnd_Ws_Date_2;
    private DbsField pnd_Ws_Date_Alpha;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl233a = new LdaFcpl233a();
        registerRecord(ldaFcpl233a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Heading_Pymnt_Check_Dte = localVariables.newFieldInRecord("pnd_Heading_Pymnt_Check_Dte", "#HEADING-PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Write_Data = localVariables.newFieldInRecord("pnd_Write_Data", "#WRITE-DATA", FieldType.BOOLEAN, 1);
        pnd_Ws_Soc_Sec = localVariables.newFieldInRecord("pnd_Ws_Soc_Sec", "#WS-SOC-SEC", FieldType.STRING, 11);
        pnd_Ws_Gross_Amt = localVariables.newFieldInRecord("pnd_Ws_Gross_Amt", "#WS-GROSS-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Taxable_Amt = localVariables.newFieldInRecord("pnd_Ws_Taxable_Amt", "#WS-TAXABLE-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Reason = localVariables.newFieldInRecord("pnd_Ws_Reason", "#WS-REASON", FieldType.STRING, 11);
        pnd_Ws_Date = localVariables.newFieldInRecord("pnd_Ws_Date", "#WS-DATE", FieldType.NUMERIC, 9);

        pnd_Ws_Date__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Date__R_Field_1", "REDEFINE", pnd_Ws_Date);
        pnd_Ws_Date_Pnd_Ws_Date_1 = pnd_Ws_Date__R_Field_1.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Date_1", "#WS-DATE-1", FieldType.STRING, 1);
        pnd_Ws_Date_Pnd_Ws_Date_2 = pnd_Ws_Date__R_Field_1.newFieldInGroup("pnd_Ws_Date_Pnd_Ws_Date_2", "#WS-DATE-2", FieldType.STRING, 8);
        pnd_Ws_Date_Alpha = localVariables.newFieldInRecord("pnd_Ws_Date_Alpha", "#WS-DATE-ALPHA", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl233a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp233c() throws Exception
    {
        super("Fcpp233c");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 64
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 FCPL233A
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl233a.getFcpl233a())))
        {
            CheckAtStartofData82();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA;//Natural: AT BREAK OF #SRT-COMPANY-CDE
            //*   PRINT DETAIL LINE HERE
            if (condition(ldaFcpl233a.getFcpl233a_Cntrct_Orgn_Cde().equals(" ")))                                                                                         //Natural: IF CNTRCT-ORGN-CDE = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl233a.getFcpl233a_Annt_Soc_Sec_Ind().equals(getZero())))                                                                                  //Natural: IF ANNT-SOC-SEC-IND = 0
            {
                pnd_Ws_Soc_Sec.setValueEdited(ldaFcpl233a.getFcpl233a_Annt_Soc_Sec_Nbr(),new ReportEditMask("999-99-9999"));                                              //Natural: MOVE EDITED ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) TO #WS-SOC-SEC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Soc_Sec.setValueEdited(ldaFcpl233a.getFcpl233a_Annt_Soc_Sec_Nbr(),new ReportEditMask("9-99999999"));                                               //Natural: MOVE EDITED ANNT-SOC-SEC-NBR ( EM = 99-99999999 ) TO #WS-SOC-SEC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Gross_Amt.compute(new ComputeParameters(false, pnd_Ws_Gross_Amt), ldaFcpl233a.getFcpl233a_Inv_Acct_Settl_Amt().add(ldaFcpl233a.getFcpl233a_Inv_Acct_Dvdnd_Amt())); //Natural: ASSIGN #WS-GROSS-AMT := INV-ACCT-SETTL-AMT + INV-ACCT-DVDND-AMT
            pnd_Ws_Taxable_Amt.compute(new ComputeParameters(false, pnd_Ws_Taxable_Amt), pnd_Ws_Gross_Amt.subtract(ldaFcpl233a.getFcpl233a_Inv_Acct_Ivc_Amt()));          //Natural: ASSIGN #WS-TAXABLE-AMT := #WS-GROSS-AMT - INV-ACCT-IVC-AMT
            if (condition(ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN")))                                                                          //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SN'
            {
                pnd_Ws_Reason.setValue("STOP NO RDR");                                                                                                                    //Natural: ASSIGN #WS-REASON := 'STOP NO RDR'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN")))                                                                          //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN'
            {
                pnd_Ws_Reason.setValue("CANC NO RDR");                                                                                                                    //Natural: ASSIGN #WS-REASON := 'CANC NO RDR'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP")))                                                                          //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CP'
            {
                pnd_Ws_Reason.setValue("CANCEL PEND");                                                                                                                    //Natural: ASSIGN #WS-REASON := 'CANCEL PEND'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP")))                                                                          //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SP'
            {
                pnd_Ws_Reason.setValue("STOP PEND");                                                                                                                      //Natural: ASSIGN #WS-REASON := 'STOP PEND'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP")))                                                                          //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'RP'
            {
                pnd_Ws_Reason.setValue("RDRW PEND");                                                                                                                      //Natural: ASSIGN #WS-REASON := 'RDRW PEND'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))                                                                          //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'PR'
            {
                pnd_Ws_Reason.setValue("PEND RTRN");                                                                                                                      //Natural: ASSIGN #WS-REASON := 'PEND RTRN'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R") || ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" R")))   //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R' OR = ' R'
            {
                pnd_Ws_Reason.setValue("ADDRESS RDR");                                                                                                                    //Natural: ASSIGN #WS-REASON := 'ADDRESS RDR'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Date.compute(new ComputeParameters(false, pnd_Ws_Date), DbsField.subtract(100000000,ldaFcpl233a.getFcpl233a_Cnr_Orgnl_Invrse_Dte()));                  //Natural: ASSIGN #WS-DATE := 100000000 - CNR-ORGNL-INVRSE-DTE
            pnd_Ws_Date_Alpha.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Date_Pnd_Ws_Date_2);                                                                   //Natural: MOVE EDITED #WS-DATE-2 TO #WS-DATE-ALPHA ( EM = YYYYMMDD )
            if (condition(getReports().getAstLinesLeft(1).less(8)))                                                                                                       //Natural: NEWPAGE ( 1 ) LESS THAN 8 LINES
            {
                getReports().newPage(1);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }
            getReports().write(1, NEWLINE,new TabSetting(1),pnd_Ws_Soc_Sec,new TabSetting(13),ldaFcpl233a.getFcpl233a_Cntrct_Ppcn_Nbr(), new ReportEditMask               //Natural: WRITE ( 1 ) / 001T #WS-SOC-SEC 013T CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 023T CNTRCT-ORGN-CDE 026T #WS-DATE-ALPHA ( EM = MM/DD/YY ) 035T #PARTICIPANT-NAME ( AL = 24 ) 060T #WS-GROSS-AMT ( EM = ZZZ,ZZZ.99 ) 071T INV-ACCT-IVC-AMT ( EM = ZZZ,ZZZ.99 ) 082T #WS-TAXABLE-AMT ( EM = ZZZ,ZZZ.99 ) 093T INV-ACCT-FDRL-TAX-AMT ( EM = ZZZ,ZZZ.99 ) 104T INV-ACCT-STATE-TAX-AMT ( EM = ZZ,ZZZ.99 ) 114T INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZ.99 ) 121T #WS-REASON
                ("XXXXXXX-X"),new TabSetting(23),ldaFcpl233a.getFcpl233a_Cntrct_Orgn_Cde(),new TabSetting(26),pnd_Ws_Date_Alpha, new ReportEditMask ("MM/DD/YY"),new 
                TabSetting(35),ldaFcpl233a.getFcpl233a_Pnd_Participant_Name(), new AlphanumericLength (24),new TabSetting(60),pnd_Ws_Gross_Amt, new ReportEditMask 
                ("ZZZ,ZZZ.99"),new TabSetting(71),ldaFcpl233a.getFcpl233a_Inv_Acct_Ivc_Amt(), new ReportEditMask ("ZZZ,ZZZ.99"),new TabSetting(82),pnd_Ws_Taxable_Amt, 
                new ReportEditMask ("ZZZ,ZZZ.99"),new TabSetting(93),ldaFcpl233a.getFcpl233a_Inv_Acct_Fdrl_Tax_Amt(), new ReportEditMask ("ZZZ,ZZZ.99"),new 
                TabSetting(104),ldaFcpl233a.getFcpl233a_Inv_Acct_State_Tax_Amt(), new ReportEditMask ("ZZ,ZZZ.99"),new TabSetting(114),ldaFcpl233a.getFcpl233a_Inv_Acct_Local_Tax_Amt(), 
                new ReportEditMask ("ZZZ.99"),new TabSetting(121),pnd_Ws_Reason);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaFcpl233a.getFcpl233a_Pnd_Current_Address().greater(" ")))                                                                                    //Natural: IF #CURRENT-ADDRESS > ' '
            {
                getReports().write(1, new TabSetting(26),"CURRENT  ADDR :",new TabSetting(42),ldaFcpl233a.getFcpl233a_Pnd_Current_Address(), new AlphanumericLength       //Natural: WRITE ( 1 ) 026T 'CURRENT  ADDR :' 042T #CURRENT-ADDRESS ( AL = 50 )
                    (50));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(1, new TabSetting(26),"PREVIOUS ADDR :",new TabSetting(42),ldaFcpl233a.getFcpl233a_Pnd_Previous_Address(), new AlphanumericLength      //Natural: WRITE ( 1 ) 026T 'PREVIOUS ADDR :' 042T #PREVIOUS-ADDRESS ( AL = 50 )
                    (50));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Write_Data.setValue(true);                                                                                                                                //Natural: ASSIGN #WRITE-DATA := TRUE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //*  -------------------------------------------------------------------*
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START-NEW-REPORT
        //*  -------------------------------------------------------------------*
        //*  -------------------- SUBROUTINES ----------------------------------*
        //*  IB 10/21/99
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 001T *DATU '-' 012T *TIMX ( EM = HH':'IIAP ) 030T #SRT-COMPANY-CDE 053T 'CONSOLIDATED PAYMENT SYSTEM' 118T 'PAGE:' *PAGE-NUMBER ( 1 ) ( AD = L ) / 001T *INIT-USER '-' 012T *PROGRAM 047T 'DAILY CANCEL/STOP AND REDRAW TAX REPORT' / 001T *LIBRARY-ID 059T 'FOR ' #HEADING-PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) // 023T 'O' / 001T 'TAX-ID' 013T 'CONTRACT' 023T 'R' 026T 'ORIGINAL' 064T 'GROSS' 077T 'IVC' 085T 'TAXABLE' 096T 'FEDERAL' 108T 'STATE' 115T 'LOCAL' / 001T 'SSN' 014T 'NUMBER ' 023T 'G' 026T 'PAYMENT' 035T 'PARTICIPANT NAME' 064T 'AMOUNT' 074T 'AMOUNT' 085T 'AMOUNT' 098T '  TAX  ' 109T ' TAX ' 116T 'TAX ' 121T 'REASON' / 001T '-' ( 11 ) 013T '-' ( 09 ) 023T '--' 026T '--------' 035T '-' ( 24 ) 060T '-' ( 10 ) 071T '-' ( 10 ) 082T '-' ( 10 ) 093T '-' ( 10 ) 104T '-' ( 9 ) 114T '------' 121T '-' ( 11 )
    }
    private void sub_Start_New_Report() throws Exception                                                                                                                  //Natural: START-NEW-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl233a_getFcpl233a_Pnd_Srt_Company_CdeIsBreak = ldaFcpl233a.getFcpl233a_Pnd_Srt_Company_Cde().isBreak(endOfData);
        if (condition(ldaFcpl233a_getFcpl233a_Pnd_Srt_Company_CdeIsBreak))
        {
            if (condition(! (pnd_Write_Data.getBoolean())))                                                                                                               //Natural: IF NOT #WRITE-DATA
            {
                getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(41),"*** THERE WAS NO PAYMENT FOR THIS COMPANY CODE ***");                           //Natural: WRITE ( 1 ) //// 41T '*** THERE WAS NO PAYMENT FOR THIS COMPANY CODE ***'
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(59),"  END OF REPORT ");                                                                         //Natural: WRITE ( 1 ) /// 59T '  END OF REPORT '
            if (condition(Global.isEscape())) return;
            getReports().getPageNumberDbs(1).reset();                                                                                                                     //Natural: RESET *PAGE-NUMBER ( 1 )
            pnd_Write_Data.setValue(false);                                                                                                                               //Natural: ASSIGN #WRITE-DATA := FALSE
            pnd_Heading_Pymnt_Check_Dte.setValue(ldaFcpl233a.getFcpl233a_Pymnt_Check_Dte());                                                                              //Natural: MOVE FCPL233A.PYMNT-CHECK-DTE TO #HEADING-PYMNT-CHECK-DTE
                                                                                                                                                                          //Natural: PERFORM START-NEW-REPORT
            sub_Start_New_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=64");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getDATU(),"-",new TabSetting(12),Global.getTIMX(), 
            new ReportEditMask ("HH':'IIAP"),new TabSetting(30),ldaFcpl233a.getFcpl233a_Pnd_Srt_Company_Cde(),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(118),"PAGE:",getReports().getPageNumberDbs(1), new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(1),Global.getINIT_USER(),"-",new 
            TabSetting(12),Global.getPROGRAM(),new TabSetting(47),"DAILY CANCEL/STOP AND REDRAW TAX REPORT",NEWLINE,new TabSetting(1),Global.getLIBRARY_ID(),new 
            TabSetting(59),"FOR ",pnd_Heading_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(23),"O",NEWLINE,new TabSetting(1),"TAX-ID",new 
            TabSetting(13),"CONTRACT",new TabSetting(23),"R",new TabSetting(26),"ORIGINAL",new TabSetting(64),"GROSS",new TabSetting(77),"IVC",new TabSetting(85),"TAXABLE",new 
            TabSetting(96),"FEDERAL",new TabSetting(108),"STATE",new TabSetting(115),"LOCAL",NEWLINE,new TabSetting(1),"SSN",new TabSetting(14),"NUMBER ",new 
            TabSetting(23),"G",new TabSetting(26),"PAYMENT",new TabSetting(35),"PARTICIPANT NAME",new TabSetting(64),"AMOUNT",new TabSetting(74),"AMOUNT",new 
            TabSetting(85),"AMOUNT",new TabSetting(98),"  TAX  ",new TabSetting(109)," TAX ",new TabSetting(116),"TAX ",new TabSetting(121),"REASON",NEWLINE,new 
            TabSetting(1),"-",new RepeatItem(11),new TabSetting(13),"-",new RepeatItem(9),new TabSetting(23),"--",new TabSetting(26),"--------",new TabSetting(35),"-",new 
            RepeatItem(24),new TabSetting(60),"-",new RepeatItem(10),new TabSetting(71),"-",new RepeatItem(10),new TabSetting(82),"-",new RepeatItem(10),new 
            TabSetting(93),"-",new RepeatItem(10),new TabSetting(104),"-",new RepeatItem(9),new TabSetting(114),"------",new TabSetting(121),"-",new RepeatItem(11));
    }
    private void CheckAtStartofData82() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Heading_Pymnt_Check_Dte.setValue(ldaFcpl233a.getFcpl233a_Pymnt_Check_Dte());                                                                              //Natural: MOVE FCPL233A.PYMNT-CHECK-DTE TO #HEADING-PYMNT-CHECK-DTE
            getReports().write(1, " ");                                                                                                                                   //Natural: WRITE ( 1 ) ' '
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-START
    }
}
