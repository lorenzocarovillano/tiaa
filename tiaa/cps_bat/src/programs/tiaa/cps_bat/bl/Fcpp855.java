/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:51 PM
**        * FROM NATURAL PROGRAM : Fcpp855
************************************************************
**        * FILE NAME            : Fcpp855.java
**        * CLASS NAME           : Fcpp855
**        * INSTANCE NAME        : Fcpp855
************************************************************
************************************************************************
*
* PROGRAM   : FCPP855
*
* SYSTEM    : CPS
* GENERATED : ON 03/17/2005 BY RAMANA ANNE
*
* FUNCTION  : SPLIT GA PAYMENT REGISTERS TO SEPARATE HARVARD REPORT. THE
*             NEW FILE WILL BE HANDLED BY FCPP856 (CLONE OF FCPP813).
*
* HISTORY   :
* RAMANA ANNE : 12/30/05
*             : ADD NEW LOCAL DATA AREA FOR PAYEE-MATCH PROJECT
*
* 04/22/08  : LCW - RESTOWED FOR ROTH 403B. ROTH-MAJOR1
*
* 01/29/10  : JWO - ADD NEW HARVARD CONTRACT RANGES
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 04/01/2014 :FENDAYA    - CREF REA PROJECT. RESTOW THE MODULE TO
*                          PICK UP LATEST VERSION OF NECA4000.
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
* 11/20/2018 J.OSTEEN - ADD CONTRACT RANGE W053 -- JWO1
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp855 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa803c pdaFcpa803c;
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cntr;
    private DbsField pnd_Cnt_Pers_Staff;
    private DbsField pnd_Cnt_Pers_Hourly;
    private DbsField pnd_Cnt_President;
    private DbsField pnd_Cnt_Mstr_Fclty_A;
    private DbsField pnd_Cnt_Mstr_Fclty_B;
    private DbsField pnd_Cnt_Mstr_Staff;
    private DbsField pnd_Cnt_Mstr_Hourly;
    private DbsField pnd_Cnt_Spec_Staff;
    private DbsField pnd_Cnt_Spec_Hourly;
    private DbsField pnd_Cnt_Brid_Staff;
    private DbsField pnd_Cnt_Brid_Hourly;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa803c = new PdaFcpa803c(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables
        pnd_Cntr = localVariables.newFieldInRecord("pnd_Cntr", "#CNTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Pers_Staff = localVariables.newFieldInRecord("pnd_Cnt_Pers_Staff", "#CNT-PERS-STAFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Pers_Hourly = localVariables.newFieldInRecord("pnd_Cnt_Pers_Hourly", "#CNT-PERS-HOURLY", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_President = localVariables.newFieldInRecord("pnd_Cnt_President", "#CNT-PRESIDENT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Mstr_Fclty_A = localVariables.newFieldInRecord("pnd_Cnt_Mstr_Fclty_A", "#CNT-MSTR-FCLTY-A", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Mstr_Fclty_B = localVariables.newFieldInRecord("pnd_Cnt_Mstr_Fclty_B", "#CNT-MSTR-FCLTY-B", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Mstr_Staff = localVariables.newFieldInRecord("pnd_Cnt_Mstr_Staff", "#CNT-MSTR-STAFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Mstr_Hourly = localVariables.newFieldInRecord("pnd_Cnt_Mstr_Hourly", "#CNT-MSTR-HOURLY", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Spec_Staff = localVariables.newFieldInRecord("pnd_Cnt_Spec_Staff", "#CNT-SPEC-STAFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Spec_Hourly = localVariables.newFieldInRecord("pnd_Cnt_Spec_Hourly", "#CNT-SPEC-HOURLY", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Brid_Staff = localVariables.newFieldInRecord("pnd_Cnt_Brid_Staff", "#CNT-BRID-STAFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Brid_Hourly = localVariables.newFieldInRecord("pnd_Cnt_Brid_Hourly", "#CNT-BRID-HOURLY", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp855() throws Exception
    {
        super("Fcpp855");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP855", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 140 PS = 55
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  READ WORK FILE 1 WF-PYMNT-ADDR-REC /* RA
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CHECK-SORT-FIELDS.PYMNT-NBR WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            short decideConditionsMet1510 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF CNTRCT-PPCN-NBR;//Natural: VALUE 'W0764500' : 'W0764749'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0764500' : 'W0764749"))))
            {
                decideConditionsMet1510++;
                //*  JWO1 11/20/18
                pnd_Cnt_Pers_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-PERS-STAFF
                //*      WRITE WORK FILE 3 WF-PYMNT-ADDR-REC
                //*    VALUE 'W0541000' : 'W0549999'
            }                                                                                                                                                             //Natural: VALUE 'W0530000' : 'W0549999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0530000' : 'W0549999"))))
            {
                decideConditionsMet1510++;
                //*  JWO 01/29/10
                pnd_Cnt_Pers_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-PERS-STAFF
                //*      WRITE WORK FILE 3 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0764750' : 'W0764999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0764750' : 'W0764999"))))
            {
                decideConditionsMet1510++;
                pnd_Cnt_Pers_Hourly.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-PERS-HOURLY
                //*      WRITE WORK FILE 3 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0765000' : 'W0765499'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0765000' : 'W0765499"))))
            {
                decideConditionsMet1510++;
                pnd_Cnt_President.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CNT-PRESIDENT
                //*      WRITE WORK FILE 3 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0765500' : 'W0767599'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0765500' : 'W0767599"))))
            {
                decideConditionsMet1510++;
                pnd_Cnt_Mstr_Fclty_A.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CNT-MSTR-FCLTY-A
                //*      WRITE WORK FILE 3 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0767600' : 'W0769999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0767600' : 'W0769999"))))
            {
                decideConditionsMet1510++;
                pnd_Cnt_Mstr_Fclty_B.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CNT-MSTR-FCLTY-B
                //*      WRITE WORK FILE 3 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0770000' : 'W0789999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0770000' : 'W0789999"))))
            {
                decideConditionsMet1510++;
                pnd_Cnt_Mstr_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-MSTR-STAFF
                //*      WRITE WORK FILE 3 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0790000' : 'W0799999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0790000' : 'W0799999"))))
            {
                decideConditionsMet1510++;
                //*  JWO 01/29/10
                pnd_Cnt_Mstr_Hourly.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-MSTR-HOURLY
                //*      WRITE WORK FILE 3 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0550000' : 'W0554999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0550000' : 'W0554999"))))
            {
                decideConditionsMet1510++;
                //*  JWO 01/29/10
                //*  JWO 01/29/10
                pnd_Cnt_Spec_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-SPEC-STAFF
                //*      WRITE WORK FILE 4 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0555000' : 'W0557999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0555000' : 'W0557999"))))
            {
                decideConditionsMet1510++;
                //*  JWO 01/29/10
                //*  JWO 01/29/10
                pnd_Cnt_Brid_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-BRID-STAFF
                //*      WRITE WORK FILE 4 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0558000' : 'W0558999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0558000' : 'W0558999"))))
            {
                decideConditionsMet1510++;
                //*  JWO 01/29/10
                //*  JWO 01/29/10
                pnd_Cnt_Spec_Hourly.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-SPEC-HOURLY
                //*      WRITE WORK FILE 4 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: VALUE 'W0559000' : 'W0559999'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().equals("W0559000' : 'W0559999"))))
            {
                decideConditionsMet1510++;
                //*  JWO 01/29/10
                pnd_Cnt_Brid_Hourly.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-BRID-HOURLY
                //*  JWO 01/29/10
                getWorkFiles().write(4, false, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                                                      //Natural: WRITE WORK FILE 4 WF-PYMNT-ADDR-REC
            }                                                                                                                                                             //Natural: ANY VALUE
            if (condition(decideConditionsMet1510 > 0))
            {
                getWorkFiles().write(2, false, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                                                      //Natural: WRITE WORK FILE 2 WF-PYMNT-ADDR-REC
                pnd_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNTR
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *READ WORK 3 WF-PYMNT-ADDR-REC
        //* *  WRITE WORK FILE 2 WF-PYMNT-ADDR-REC
        //* *END-WORK
        //* *READ WORK 4 WF-PYMNT-ADDR-REC
        //* *  WRITE WORK FILE 2 WF-PYMNT-ADDR-REC
        //* *END-WORK
        //*  JWO 01/29/10
        //*  JWO 01/29/10
        //*  JWO 01/29/10
        //*  JWO 01/29/10
        getReports().write(0, "PERSONAL STAFF...:",pnd_Cnt_Pers_Staff,NEWLINE,"PERSONAL HOURLY..:",pnd_Cnt_Pers_Hourly,NEWLINE,"PRESIDENT/FELLOWS:",pnd_Cnt_President,    //Natural: WRITE 'PERSONAL STAFF...:' #CNT-PERS-STAFF / 'PERSONAL HOURLY..:' #CNT-PERS-HOURLY / 'PRESIDENT/FELLOWS:' #CNT-PRESIDENT / 'MASTER FCLTY A...:' #CNT-MSTR-FCLTY-A / 'MASTER FCLTY B...:' #CNT-MSTR-FCLTY-B / 'MASTER STAFF.....:' #CNT-MSTR-STAFF / 'MASTER HOURLY....:' #CNT-MSTR-HOURLY / 'SPECIAL STAFF....:' #CNT-SPEC-STAFF / 'BRIDGE STAFF.....:' #CNT-SPEC-HOURLY / 'SPECIAL HOURLY...:' #CNT-BRID-STAFF / 'BRIDGE HOURLY....:' #CNT-BRID-HOURLY // 'TOTAL NUMBER OF HARVARD RECORDS WRITTEN:' #CNTR
            NEWLINE,"MASTER FCLTY A...:",pnd_Cnt_Mstr_Fclty_A,NEWLINE,"MASTER FCLTY B...:",pnd_Cnt_Mstr_Fclty_B,NEWLINE,"MASTER STAFF.....:",pnd_Cnt_Mstr_Staff,
            NEWLINE,"MASTER HOURLY....:",pnd_Cnt_Mstr_Hourly,NEWLINE,"SPECIAL STAFF....:",pnd_Cnt_Spec_Staff,NEWLINE,"BRIDGE STAFF.....:",pnd_Cnt_Spec_Hourly,
            NEWLINE,"SPECIAL HOURLY...:",pnd_Cnt_Brid_Staff,NEWLINE,"BRIDGE HOURLY....:",pnd_Cnt_Brid_Hourly,NEWLINE,NEWLINE,"TOTAL NUMBER OF HARVARD RECORDS WRITTEN:",
            pnd_Cntr);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=140 PS=55");
    }
}
