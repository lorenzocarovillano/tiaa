/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:05 PM
**        * FROM NATURAL PROGRAM : Fcpp808
************************************************************
**        * FILE NAME            : Fcpp808.java
**        * CLASS NAME           : Fcpp808
**        * INSTANCE NAME        : Fcpp808
************************************************************
************************************************************************
*
* PROGRAM   : FCPP808
*
* SYSTEM    : CPS
* TITLE     : DEDUCTIONS ERROR REPORTS
* GENERATED :
*
* FUNCTION  : GENERATE REPORTS OF ALL VOLUNTARY DEDUCTIONS FROM
*             PENDED CONTRACTS.
*             1 - LONG TERM CARE
*             2 - PERSONAL ANNUITY
*             3 - PERSONAL ANNUITY SELECT      JH 5/26/2000
*             4 - UNIVERSAL LIFE               JH 5/26/2000
*             5 - ALL OTHERS
* HISTORY
* --------
* 10/20/98 DC,: ADDING DED. CODES 8, 9, 10 TO COUNTERS AND REPORT #3
*          LB   FOR THE TIME BEING DED. CODES 9 AND 10 ARE NOT USED
*
* 05/26/00 JH : ADDED PA SELECT (CODE 9) AS REPORT (03) AND
*               UNIVERSAL LIFE (CODE 10) AS REPORT (04).  CHANGED
*               OTHER DEDUCTIONS FROM REPORT (03) TO (05).
*
* 05/05/03 RC : RESTOW. FCPA800 WAS EXPANDED
*
* 05/06/08 : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp808 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_No_Ltc_Ded;
    private DbsField pnd_No_Pa_Ded;
    private DbsField pnd_No_Pasel_Ded;
    private DbsField pnd_No_Ulife_Ded;
    private DbsField pnd_No_Other_Ded;

    private DbsGroup pnd_Deductions;
    private DbsField pnd_Deductions_Pnd_Bc_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Ltc_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Med_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Gl_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Ovp_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Nysut_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Pa_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Mutfnd_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Pasel_Ded_Amt;
    private DbsField pnd_Deductions_Pnd_Ulife_Ded_Amt;

    private DbsGroup pnd_Deductions__R_Field_1;
    private DbsField pnd_Deductions_Pnd_Ded_Amt;
    private DbsField pnd_Ded_Nbrs;

    private DbsGroup pnd_Ded_Nbrs__R_Field_2;
    private DbsField pnd_Ded_Nbrs_Pnd_Ded_Nbr;
    private DbsField pnd_Index;
    private DbsField pnd_Indx;
    private DbsField pnd_Others_Ded_Amt;
    private DbsField pnd_Deduction_Amt;
    private DbsField pnd_Gross_Amt;
    private DbsField pnd_Annty_Ins_Type;
    private DbsField pnd_Annty_Type;
    private DbsField pnd_Insurance_Option;
    private DbsField pnd_Life_Contingency;
    private DbsField pnd_Trailer_Message;
    private DbsField pnd_Trailer;
    private DbsField pnd_Header_Message;
    private DbsField pnd_Header;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);

        // Local Variables
        pnd_No_Ltc_Ded = localVariables.newFieldInRecord("pnd_No_Ltc_Ded", "#NO-LTC-DED", FieldType.BOOLEAN, 1);
        pnd_No_Pa_Ded = localVariables.newFieldInRecord("pnd_No_Pa_Ded", "#NO-PA-DED", FieldType.BOOLEAN, 1);
        pnd_No_Pasel_Ded = localVariables.newFieldInRecord("pnd_No_Pasel_Ded", "#NO-PASEL-DED", FieldType.BOOLEAN, 1);
        pnd_No_Ulife_Ded = localVariables.newFieldInRecord("pnd_No_Ulife_Ded", "#NO-ULIFE-DED", FieldType.BOOLEAN, 1);
        pnd_No_Other_Ded = localVariables.newFieldInRecord("pnd_No_Other_Ded", "#NO-OTHER-DED", FieldType.BOOLEAN, 1);

        pnd_Deductions = localVariables.newGroupInRecord("pnd_Deductions", "#DEDUCTIONS");
        pnd_Deductions_Pnd_Bc_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Bc_Ded_Amt", "#BC-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Deductions_Pnd_Ltc_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Ltc_Ded_Amt", "#LTC-DED-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Deductions_Pnd_Med_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Med_Ded_Amt", "#MED-DED-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Deductions_Pnd_Gl_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Gl_Ded_Amt", "#GL-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Deductions_Pnd_Ovp_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Ovp_Ded_Amt", "#OVP-DED-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Deductions_Pnd_Nysut_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Nysut_Ded_Amt", "#NYSUT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Deductions_Pnd_Pa_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Pa_Ded_Amt", "#PA-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Deductions_Pnd_Mutfnd_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Mutfnd_Ded_Amt", "#MUTFND-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Deductions_Pnd_Pasel_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Pasel_Ded_Amt", "#PASEL-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Deductions_Pnd_Ulife_Ded_Amt = pnd_Deductions.newFieldInGroup("pnd_Deductions_Pnd_Ulife_Ded_Amt", "#ULIFE-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Deductions__R_Field_1 = localVariables.newGroupInRecord("pnd_Deductions__R_Field_1", "REDEFINE", pnd_Deductions);
        pnd_Deductions_Pnd_Ded_Amt = pnd_Deductions__R_Field_1.newFieldArrayInGroup("pnd_Deductions_Pnd_Ded_Amt", "#DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 10));
        pnd_Ded_Nbrs = localVariables.newFieldInRecord("pnd_Ded_Nbrs", "#DED-NBRS", FieldType.STRING, 10);

        pnd_Ded_Nbrs__R_Field_2 = localVariables.newGroupInRecord("pnd_Ded_Nbrs__R_Field_2", "REDEFINE", pnd_Ded_Nbrs);
        pnd_Ded_Nbrs_Pnd_Ded_Nbr = pnd_Ded_Nbrs__R_Field_2.newFieldArrayInGroup("pnd_Ded_Nbrs_Pnd_Ded_Nbr", "#DED-NBR", FieldType.STRING, 1, new DbsArrayController(1, 
            10));
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.NUMERIC, 2);
        pnd_Indx = localVariables.newFieldInRecord("pnd_Indx", "#INDX", FieldType.NUMERIC, 3);
        pnd_Others_Ded_Amt = localVariables.newFieldInRecord("pnd_Others_Ded_Amt", "#OTHERS-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Deduction_Amt = localVariables.newFieldInRecord("pnd_Deduction_Amt", "#DEDUCTION-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Gross_Amt = localVariables.newFieldInRecord("pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Annty_Ins_Type = localVariables.newFieldInRecord("pnd_Annty_Ins_Type", "#ANNTY-INS-TYPE", FieldType.STRING, 4);
        pnd_Annty_Type = localVariables.newFieldInRecord("pnd_Annty_Type", "#ANNTY-TYPE", FieldType.STRING, 4);
        pnd_Insurance_Option = localVariables.newFieldInRecord("pnd_Insurance_Option", "#INSURANCE-OPTION", FieldType.STRING, 4);
        pnd_Life_Contingency = localVariables.newFieldInRecord("pnd_Life_Contingency", "#LIFE-CONTINGENCY", FieldType.STRING, 4);
        pnd_Trailer_Message = localVariables.newFieldInRecord("pnd_Trailer_Message", "#TRAILER-MESSAGE", FieldType.STRING, 26);
        pnd_Trailer = localVariables.newFieldInRecord("pnd_Trailer", "#TRAILER", FieldType.STRING, 50);
        pnd_Header_Message = localVariables.newFieldInRecord("pnd_Header_Message", "#HEADER-MESSAGE", FieldType.STRING, 22);
        pnd_Header = localVariables.newFieldInRecord("pnd_Header", "#HEADER", FieldType.STRING, 50);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_No_Ltc_Ded.setInitialValue(true);
        pnd_No_Pa_Ded.setInitialValue(true);
        pnd_No_Pasel_Ded.setInitialValue(true);
        pnd_No_Ulife_Ded.setInitialValue(true);
        pnd_No_Other_Ded.setInitialValue(true);
        pnd_Ded_Nbrs.setInitialValue(" 2    7X90");
        pnd_Trailer_Message.setInitialValue("deduction errors to report");
        pnd_Header_Message.setInitialValue("DEDUCTION ERROR REPORT");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp808() throws Exception
    {
        super("Fcpp808");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP808", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        getReports().atTopOfPage(atTopEventRpt3, 3);
        getReports().atTopOfPage(atTopEventRpt4, 4);
        getReports().atTopOfPage(atTopEventRpt5, 5);
        setupReports();
        //*  002  LONG TERM CARE
        //*  007  PERSONAL ANNUITY                                                                                                                                        //Natural: FORMAT ( 01 ) LS = 132 PS = 55
        //*  009  PA SELECT       JH 5/26/2000                                                                                                                            //Natural: FORMAT ( 02 ) LS = 132 PS = 55
        //*  010  UNIVERSAL LIFE  JH 5/26/2000                                                                                                                            //Natural: FORMAT ( 03 ) LS = 132 PS = 55
        //*       ALL OTHERS      JH 5/26/2000                                                                                                                            //Natural: FORMAT ( 04 ) LS = 132 PS = 55
        //*                                                                                                                                                               //Natural: FORMAT ( 05 ) LS = 132 PS = 55
        //*  --------------------------------------------------                                                                                                           //Natural: ON ERROR
        //*  --------                                                                                                                                                     //Natural: AT TOP OF PAGE ( 1 )
        //*  --------                                                                                                                                                     //Natural: AT TOP OF PAGE ( 2 )
        //*  --------                                                                                                                                                     //Natural: AT TOP OF PAGE ( 3 )
        //*  --------                                                                                                                                                     //Natural: AT TOP OF PAGE ( 4 )
        //*  ==================================================                                                                                                           //Natural: AT TOP OF PAGE ( 5 )
        //*  READ PAYMENT/NAME AND ADDRESS WORK FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            pnd_Deductions.reset();                                                                                                                                       //Natural: RESET #DEDUCTIONS #OTHERS-DED-AMT
            pnd_Others_Ded_Amt.reset();
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 10
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(10)); pnd_Index.nadd(1))
            {
                //*  **************************************************
                //* *  ADD  (#INDEX + 1.01) TO PYMNT-DED-AMT (#INDEX)
                //* *  ADD  (#INDEX + 1)    TO PYMNT-DED-CDE (#INDEX)
                //*  **************************************************
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Index).equals(getZero())))                                                     //Natural: IF PYMNT-DED-AMT ( #INDEX ) = 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_Indx.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index));                                                                   //Natural: ASSIGN #INDX := PYMNT-DED-CDE ( #INDEX )
                if (condition(pnd_Indx.greaterOrEqual(1) && pnd_Indx.lessOrEqual(10)))                                                                                    //Natural: IF #INDX = 1 THRU 10
                {
                    pnd_Deductions_Pnd_Ded_Amt.getValue(pnd_Indx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Index));                              //Natural: ADD PYMNT-DED-AMT ( #INDEX ) TO #DED-AMT ( #INDX )
                    //*  INACTIVE DED.
                    if (condition(pnd_Ded_Nbrs_Pnd_Ded_Nbr.getValue(pnd_Indx).equals(" ")))                                                                               //Natural: IF #DED-NBR ( #INDX ) = ' '
                    {
                        pnd_Others_Ded_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Index));                                                     //Natural: ADD PYMNT-DED-AMT ( #INDEX ) TO #OTHERS-DED-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Others_Ded_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Index));                                                         //Natural: ADD PYMNT-DED-AMT ( #INDEX ) TO #OTHERS-DED-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ---------------------------------------------
            pnd_Deduction_Amt.compute(new ComputeParameters(false, pnd_Deduction_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").add(getZero()));     //Natural: ASSIGN #DEDUCTION-AMT := PYMNT-DED-AMT ( * ) + 0
            pnd_Gross_Amt.compute(new ComputeParameters(false, pnd_Gross_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #GROSS-AMT := INV-ACCT-SETTL-AMT ( 1:INV-ACCT-COUNT ) + 0
            //*  TRANSLATE LEDGER FIELDS
            //* ***********************************************************************
            //*  PROGRAM   : FCPP807
            //*  SYSTEM    : CPS
            //*  TITLE     : TRANSLATE LEDGER CODES
            //*  GENERATED :
            //*  FUNCTION  : USED BY:
            //*            :     FCPP810
            //*            :
            //*  HISTORY   :
            //* ***********************************************************************
            pnd_Annty_Ins_Type.reset();                                                                                                                                   //Natural: RESET #ANNTY-INS-TYPE #ANNTY-TYPE #INSURANCE-OPTION #LIFE-CONTINGENCY
            pnd_Annty_Type.reset();
            pnd_Insurance_Option.reset();
            pnd_Life_Contingency.reset();
            short decideConditionsMet626 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-INS-TYPE;//Natural: VALUE 'A'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("A"))))
            {
                decideConditionsMet626++;
                pnd_Annty_Ins_Type.setValue("Ann ");                                                                                                                      //Natural: MOVE 'Ann ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("I"))))
            {
                decideConditionsMet626++;
                pnd_Annty_Ins_Type.setValue("Ins ");                                                                                                                      //Natural: MOVE 'Ins ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("G"))))
            {
                decideConditionsMet626++;
                pnd_Annty_Ins_Type.setValue("Grp ");                                                                                                                      //Natural: MOVE 'Grp ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet636 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-TYPE-CDE;//Natural: VALUE 'M'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M"))))
            {
                decideConditionsMet636++;
                pnd_Annty_Type.setValue("Mat ");                                                                                                                          //Natural: MOVE 'Mat ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("D"))))
            {
                decideConditionsMet636++;
                pnd_Annty_Type.setValue("Dth ");                                                                                                                          //Natural: MOVE 'Dth ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("P"))))
            {
                decideConditionsMet636++;
                pnd_Annty_Type.setValue("Par ");                                                                                                                          //Natural: MOVE 'Par ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("N"))))
            {
                decideConditionsMet636++;
                pnd_Annty_Type.setValue("NPar");                                                                                                                          //Natural: MOVE 'NPar' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet648 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-INSURANCE-OPTION;//Natural: VALUE 'G'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("G"))))
            {
                decideConditionsMet648++;
                pnd_Insurance_Option.setValue("Grp ");                                                                                                                    //Natural: MOVE 'Grp ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("C"))))
            {
                decideConditionsMet648++;
                pnd_Insurance_Option.setValue("Coll");                                                                                                                    //Natural: MOVE 'Coll' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("I"))))
            {
                decideConditionsMet648++;
                pnd_Insurance_Option.setValue("Ind ");                                                                                                                    //Natural: MOVE 'Ind ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("P"))))
            {
                decideConditionsMet648++;
                pnd_Insurance_Option.setValue("PA  ");                                                                                                                    //Natural: MOVE 'PA  ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet660 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-LIFE-CONTINGENCY;//Natural: VALUE 'Y'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency().equals("Y"))))
            {
                decideConditionsMet660++;
                pnd_Life_Contingency.setValue("Yes ");                                                                                                                    //Natural: MOVE 'Yes ' TO #LIFE-CONTINGENCY
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency().equals("N"))))
            {
                decideConditionsMet660++;
                pnd_Life_Contingency.setValue("No  ");                                                                                                                    //Natural: MOVE 'No  ' TO #LIFE-CONTINGENCY
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Deductions_Pnd_Ltc_Ded_Amt.greater(getZero())))                                                                                             //Natural: IF #LTC-DED-AMT > 0
            {
                //*  ??????????????
                getReports().display(1, new ReportMatrixColumnUnderline("-"),"/Contract#",                                                                                //Natural: DISPLAY ( 01 ) ( UC = - ) '/Contract#' CNTRCT-PPCN-NBR '/Combine #' CNTRCT-CMBN-NBR 'Long Term/Care' #LTC-DED-AMT 'Total/Deductions' #DEDUCTION-AMT 'Total/Gross Amt' #GROSS-AMT 'Fund' INV-ACCT-CDE-ALPHA ( 1 ) 'Ann/Ins' #ANNTY-INS-TYPE 'Cntrct/Type' #ANNTY-TYPE 'Prod/Line' #INSURANCE-OPTION 'Life/Cont' #LIFE-CONTINGENCY
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Long Term/Care",
                		pnd_Deductions_Pnd_Ltc_Ded_Amt,"Total/Deductions",
                		pnd_Deduction_Amt,"Total/Gross Amt",
                		pnd_Gross_Amt,"Fund",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1),"Ann/Ins",
                		pnd_Annty_Ins_Type,"Cntrct/Type",
                		pnd_Annty_Type,"Prod/Line",
                		pnd_Insurance_Option,"Life/Cont",
                		pnd_Life_Contingency);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_No_Ltc_Ded.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #NO-LTC-DED
            }                                                                                                                                                             //Natural: END-IF
            //*  --------
            if (condition(pnd_Deductions_Pnd_Pa_Ded_Amt.greater(getZero())))                                                                                              //Natural: IF #PA-DED-AMT > 0
            {
                //*  ??????????????
                getReports().display(2, new ReportMatrixColumnUnderline("-"),"/Contract#",                                                                                //Natural: DISPLAY ( 02 ) ( UC = - ) '/Contract#' CNTRCT-PPCN-NBR '/Combine #' CNTRCT-CMBN-NBR 'Personal/Annty' #PA-DED-AMT 'Total/Deductions' #DEDUCTION-AMT 'Total/Gross Amt' #GROSS-AMT 'Fund' INV-ACCT-CDE-ALPHA ( 1 ) 'Ann/Ins' #ANNTY-INS-TYPE 'Cntrct/Type' #ANNTY-TYPE 'Prod/Line' #INSURANCE-OPTION 'Life/Cont' #LIFE-CONTINGENCY
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Personal/Annty",
                		pnd_Deductions_Pnd_Pa_Ded_Amt,"Total/Deductions",
                		pnd_Deduction_Amt,"Total/Gross Amt",
                		pnd_Gross_Amt,"Fund",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1),"Ann/Ins",
                		pnd_Annty_Ins_Type,"Cntrct/Type",
                		pnd_Annty_Type,"Prod/Line",
                		pnd_Insurance_Option,"Life/Cont",
                		pnd_Life_Contingency);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_No_Pa_Ded.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO #NO-PA-DED
            }                                                                                                                                                             //Natural: END-IF
            //*  --------
            if (condition(pnd_Deductions_Pnd_Pasel_Ded_Amt.greater(getZero())))                                                                                           //Natural: IF #PASEL-DED-AMT > 0
            {
                //*  ??????????????
                getReports().display(3, new ReportMatrixColumnUnderline("-"),"/Contract#",                                                                                //Natural: DISPLAY ( 03 ) ( UC = - ) '/Contract#' CNTRCT-PPCN-NBR '/Combine #' CNTRCT-CMBN-NBR 'PA/Select' #PASEL-DED-AMT 'Total/Deductions' #DEDUCTION-AMT 'Total/Gross Amt' #GROSS-AMT 'Fund' INV-ACCT-CDE-ALPHA ( 1 ) 'Ann/Ins' #ANNTY-INS-TYPE 'Cntrct/Type' #ANNTY-TYPE 'Prod/Line' #INSURANCE-OPTION 'Life/Cont' #LIFE-CONTINGENCY
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"PA/Select",
                		pnd_Deductions_Pnd_Pasel_Ded_Amt,"Total/Deductions",
                		pnd_Deduction_Amt,"Total/Gross Amt",
                		pnd_Gross_Amt,"Fund",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1),"Ann/Ins",
                		pnd_Annty_Ins_Type,"Cntrct/Type",
                		pnd_Annty_Type,"Prod/Line",
                		pnd_Insurance_Option,"Life/Cont",
                		pnd_Life_Contingency);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_No_Pasel_Ded.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-PASEL-DED
            }                                                                                                                                                             //Natural: END-IF
            //*  --------
            if (condition(pnd_Deductions_Pnd_Ulife_Ded_Amt.greater(getZero())))                                                                                           //Natural: IF #ULIFE-DED-AMT > 0
            {
                //*  ??????????????
                getReports().display(4, new ReportMatrixColumnUnderline("-"),"/Contract#",                                                                                //Natural: DISPLAY ( 04 ) ( UC = - ) '/Contract#' CNTRCT-PPCN-NBR '/Combine #' CNTRCT-CMBN-NBR 'Universal/Life' #ULIFE-DED-AMT 'Total/Deductions' #DEDUCTION-AMT 'Total/Gross Amt' #GROSS-AMT 'Fund' INV-ACCT-CDE-ALPHA ( 1 ) 'Ann/Ins' #ANNTY-INS-TYPE 'Cntrct/Type' #ANNTY-TYPE 'Prod/Line' #INSURANCE-OPTION 'Life/Cont' #LIFE-CONTINGENCY
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Universal/Life",
                		pnd_Deductions_Pnd_Ulife_Ded_Amt,"Total/Deductions",
                		pnd_Deduction_Amt,"Total/Gross Amt",
                		pnd_Gross_Amt,"Fund",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1),"Ann/Ins",
                		pnd_Annty_Ins_Type,"Cntrct/Type",
                		pnd_Annty_Type,"Prod/Line",
                		pnd_Insurance_Option,"Life/Cont",
                		pnd_Life_Contingency);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_No_Ulife_Ded.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-ULIFE-DED
            }                                                                                                                                                             //Natural: END-IF
            //*  --------
            if (condition(pnd_Others_Ded_Amt.greater(getZero()) || pnd_Deductions_Pnd_Mutfnd_Ded_Amt.greater(getZero())))                                                 //Natural: IF #OTHERS-DED-AMT > 0 OR #MUTFND-DED-AMT > 0
            {
                //*  *******************************************************************
                //*  IF #BC-DED-AMT    > 0 OR #MED-DED-AMT    > 0 OR #GL-DED-AMT > 0 OR
                //*      #OVP-DED-AMT  > 0 OR #NYSUT-DED-AMT  > 0 OR #MUTFND-DED-AMT > 0
                //* *> OR #009-DED-AMT > 0 OR #010-DED-AMT    > 0 /* DED CODES 9, 10 (LB)
                //*    COMPUTE #OTHERS-DED-AMT = #OTHERS-DED-AMT
                //*      + #BC-DED-AMT
                //*      + #MED-DED-AMT
                //*      + #GL-DED-AMT
                //*      + #OVP-DED-AMT
                //*      + #NYSUT-DED-AMT
                //*  *******************************************************************
                //*  (LB)
                //*  ??????????????
                getReports().display(5, new ReportMatrixColumnUnderline("-"),"/Contract#",                                                                                //Natural: DISPLAY ( 05 ) ( UC = - ) '/Contract#' CNTRCT-PPCN-NBR '/Combine #' CNTRCT-CMBN-NBR 'Blue/Cross' #BC-DED-AMT ( EM = Z ( 5 ) 9.99 HC = R ) 'Major /Medical' #MED-DED-AMT ( EM = Z ( 5 ) 9.99 HC = R ) 'Group/Life' #GL-DED-AMT ( EM = Z ( 5 ) 9.99 HC = R ) 'Over /Payment' #OVP-DED-AMT ( EM = Z ( 5 ) 9.99 HC = R ) '/NYSUT' #NYSUT-DED-AMT ( EM = Z ( 5 ) 9.99 HC = R ) 'Total  /Deductions' #DEDUCTION-AMT ( HC = R ) 'Mutual/Fund ' #MUTFND-DED-AMT ( EM = Z ( 5 ) 9.99 HC = R ) 'Total  /Gross Amt' #GROSS-AMT ( HC = R ) 'Fu/nd' INV-ACCT-CDE-ALPHA ( 1 ) 'Ann/Ins' #ANNTY-INS-TYPE 'Ctrt/Type' #ANNTY-TYPE
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Blue/Cross",
                		pnd_Deductions_Pnd_Bc_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Major /Medical",
                		pnd_Deductions_Pnd_Med_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Group/Life",
                		pnd_Deductions_Pnd_Gl_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Over /Payment",
                		pnd_Deductions_Pnd_Ovp_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "/NYSUT",
                		pnd_Deductions_Pnd_Nysut_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Total  /Deductions",
                		pnd_Deduction_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Mutual/Fund ",
                		pnd_Deductions_Pnd_Mutfnd_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),
                    "Total  /Gross Amt",
                		pnd_Gross_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Fu/nd",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1),"Ann/Ins",
                		pnd_Annty_Ins_Type,"Ctrt/Type",
                		pnd_Annty_Type);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *    'Prod/Line'          #INSURANCE-OPTION
                //* *    'Life/Cont'          #LIFE-CONTINGENCY
                //* *  WRITE (05) NOTITLE NOHDR  CNTRCT-CMBN-NBR
                pnd_No_Other_Ded.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NO-OTHER-DED
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  ----------------------------------------------------
        if (condition(pnd_No_Ltc_Ded.getBoolean()))                                                                                                                       //Natural: IF #NO-LTC-DED
        {
            pnd_Trailer.setValue(DbsUtil.compress("No Long Term Care", pnd_Trailer_Message));                                                                             //Natural: COMPRESS 'No Long Term Care' #TRAILER-MESSAGE INTO #TRAILER
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 1 ) //// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***' 50T #TRAILER 103T '***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***",new TabSetting(50),pnd_Trailer,new 
                TabSetting(103),"***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_No_Pa_Ded.getBoolean()))                                                                                                                        //Natural: IF #NO-PA-DED
        {
            pnd_Trailer.setValue(DbsUtil.compress("No Personal Annuity", pnd_Trailer_Message));                                                                           //Natural: COMPRESS 'No Personal Annuity' #TRAILER-MESSAGE INTO #TRAILER
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 2 ) //// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***' 50T #TRAILER 103T '***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***",new TabSetting(50),pnd_Trailer,new 
                TabSetting(103),"***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_No_Pasel_Ded.getBoolean()))                                                                                                                     //Natural: IF #NO-PASEL-DED
        {
            pnd_Trailer.setValue(DbsUtil.compress("No PA Select", pnd_Trailer_Message));                                                                                  //Natural: COMPRESS 'No PA Select' #TRAILER-MESSAGE INTO #TRAILER
            getReports().write(3, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 3 ) //// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***' 50T #TRAILER 103T '***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***",new TabSetting(50),pnd_Trailer,new 
                TabSetting(103),"***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_No_Ulife_Ded.getBoolean()))                                                                                                                     //Natural: IF #NO-ULIFE-DED
        {
            pnd_Trailer.setValue(DbsUtil.compress("No Universal Life", pnd_Trailer_Message));                                                                             //Natural: COMPRESS 'No Universal Life' #TRAILER-MESSAGE INTO #TRAILER
            getReports().write(4, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 4 ) //// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***' 50T #TRAILER 103T '***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***",new TabSetting(50),pnd_Trailer,new 
                TabSetting(103),"***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_No_Other_Ded.getBoolean()))                                                                                                                     //Natural: IF #NO-OTHER-DED
        {
            pnd_Trailer.setValue(DbsUtil.compress("No Other", pnd_Trailer_Message));                                                                                      //Natural: COMPRESS 'No Other' #TRAILER-MESSAGE INTO #TRAILER
            getReports().write(5, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 5 ) //// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***' 50T #TRAILER 103T '***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***",new TabSetting(50),pnd_Trailer,new 
                TabSetting(103),"***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------------------------------------------------------------
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Header.setValue(DbsUtil.compress("    LONG TERM CARE", pnd_Header_Message));                                                                      //Natural: COMPRESS '    LONG TERM CARE' #HEADER-MESSAGE INTO #HEADER
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),"1",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new     //Natural: WRITE ( 1 ) NOTITLE *INIT-USER '-' *PROGRAM '1' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 42T #HEADER / PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask 
                        ("LLL' 'DD', 'YYYY"),new TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(42),pnd_Header,NEWLINE,pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
                        new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Header.setValue(DbsUtil.compress("  PERSONAL ANNUITY", pnd_Header_Message));                                                                      //Natural: COMPRESS '  PERSONAL ANNUITY' #HEADER-MESSAGE INTO #HEADER
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),"2",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new     //Natural: WRITE ( 2 ) NOTITLE *INIT-USER '-' *PROGRAM '2' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 42T #HEADER / PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask 
                        ("LLL' 'DD', 'YYYY"),new TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(42),pnd_Header,NEWLINE,pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
                        new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt3 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Header.setValue(DbsUtil.compress(" PERSONAL ANNUITY SELECT", pnd_Header_Message));                                                                //Natural: COMPRESS ' PERSONAL ANNUITY SELECT' #HEADER-MESSAGE INTO #HEADER
                    getReports().write(3, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),"3",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new     //Natural: WRITE ( 3 ) NOTITLE *INIT-USER '-' *PROGRAM '3' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 3 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 42T #HEADER / PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask 
                        ("LLL' 'DD', 'YYYY"),new TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(42),pnd_Header,NEWLINE,pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
                        new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt4 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Header.setValue(DbsUtil.compress("    UNIVERSAL LIFE", pnd_Header_Message));                                                                      //Natural: COMPRESS '    UNIVERSAL LIFE' #HEADER-MESSAGE INTO #HEADER
                    getReports().write(4, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),"4",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new     //Natural: WRITE ( 4 ) NOTITLE *INIT-USER '-' *PROGRAM '4' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 4 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 42T #HEADER / PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask 
                        ("LLL' 'DD', 'YYYY"),new TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(42),pnd_Header,NEWLINE,pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
                        new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt5 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    pnd_Header.setValue(DbsUtil.compress("     (ALL OTHERS)", pnd_Header_Message));                                                                       //Natural: COMPRESS '     (ALL OTHERS)' #HEADER-MESSAGE INTO #HEADER
                    getReports().write(5, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),"5",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new     //Natural: WRITE ( 5 ) NOTITLE *INIT-USER '-' *PROGRAM '5' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 5 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 42T #HEADER / PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask 
                        ("LLL' 'DD', 'YYYY"),new TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(42),pnd_Header,NEWLINE,pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
                        new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"***",NEWLINE,"***",Global.getPROGRAM(),           //Natural: WRITE // '**************************************************************' / '***' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '***' / '**************************************************************'
            "  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),
            NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,"***",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");
        Global.format(3, "LS=132 PS=55");
        Global.format(4, "LS=132 PS=55");
        Global.format(5, "LS=132 PS=55");

        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Long Term/Care",
        		pnd_Deductions_Pnd_Ltc_Ded_Amt,"Total/Deductions",
        		pnd_Deduction_Amt,"Total/Gross Amt",
        		pnd_Gross_Amt,"Fund",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha(),"Ann/Ins",
        		pnd_Annty_Ins_Type,"Cntrct/Type",
        		pnd_Annty_Type,"Prod/Line",
        		pnd_Insurance_Option,"Life/Cont",
        		pnd_Life_Contingency);
        getReports().setDisplayColumns(2, new ReportMatrixColumnUnderline("-"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Personal/Annty",
        		pnd_Deductions_Pnd_Pa_Ded_Amt,"Total/Deductions",
        		pnd_Deduction_Amt,"Total/Gross Amt",
        		pnd_Gross_Amt,"Fund",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha(),"Ann/Ins",
        		pnd_Annty_Ins_Type,"Cntrct/Type",
        		pnd_Annty_Type,"Prod/Line",
        		pnd_Insurance_Option,"Life/Cont",
        		pnd_Life_Contingency);
        getReports().setDisplayColumns(3, new ReportMatrixColumnUnderline("-"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"PA/Select",
        		pnd_Deductions_Pnd_Pasel_Ded_Amt,"Total/Deductions",
        		pnd_Deduction_Amt,"Total/Gross Amt",
        		pnd_Gross_Amt,"Fund",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha(),"Ann/Ins",
        		pnd_Annty_Ins_Type,"Cntrct/Type",
        		pnd_Annty_Type,"Prod/Line",
        		pnd_Insurance_Option,"Life/Cont",
        		pnd_Life_Contingency);
        getReports().setDisplayColumns(4, new ReportMatrixColumnUnderline("-"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Universal/Life",
        		pnd_Deductions_Pnd_Ulife_Ded_Amt,"Total/Deductions",
        		pnd_Deduction_Amt,"Total/Gross Amt",
        		pnd_Gross_Amt,"Fund",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha(),"Ann/Ins",
        		pnd_Annty_Ins_Type,"Cntrct/Type",
        		pnd_Annty_Type,"Prod/Line",
        		pnd_Insurance_Option,"Life/Cont",
        		pnd_Life_Contingency);
        getReports().setDisplayColumns(5, new ReportMatrixColumnUnderline("-"),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"/Combine #",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Blue/Cross",
        		pnd_Deductions_Pnd_Bc_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Major /Medical",
            
        		pnd_Deductions_Pnd_Med_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Group/Life",
            
        		pnd_Deductions_Pnd_Gl_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Over /Payment",
            
        		pnd_Deductions_Pnd_Ovp_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"/NYSUT",
            
        		pnd_Deductions_Pnd_Nysut_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Total  /Deductions",
            
        		pnd_Deduction_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Mutual/Fund ",
        		pnd_Deductions_Pnd_Mutfnd_Ded_Amt, new ReportEditMask ("ZZZZZ9.99"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Total  /Gross Amt",
            
        		pnd_Gross_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Fu/nd",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha(),"Ann/Ins",
        		pnd_Annty_Ins_Type,"Ctrt/Type",
        		pnd_Annty_Type);
    }
}
