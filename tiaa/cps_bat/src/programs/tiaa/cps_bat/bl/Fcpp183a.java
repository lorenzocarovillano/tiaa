/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:32 PM
**        * FROM NATURAL PROGRAM : Fcpp183a
************************************************************
**        * FILE NAME            : Fcpp183a.java
**        * CLASS NAME           : Fcpp183a
**        * INSTANCE NAME        : Fcpp183a
************************************************************
************************************************************************
* PROGRAM    : FCPP183A
* SYSTEM     : CPS
* TITLE      : NZ LAYOUT CONVERSION.
* FUNCTION   : CONVERTS NZ LAYOUT FROM "FCPA371" TO "FCPL190A".
*            :
* HISTORY    :
*            :
* 10/13/1999 : A. YOUNG   - CREATED.
* 12/28/1999 : A. YOUNG   - REVISED TO REPLACE 'FCPA371' WITH 'FCPAEXT'.
* 04/22/2003 : R. CARREON   STOW. FCPAEXT WAS EXPANDED
* 4/2017     : JJG - PIN EXPANSION RESTOW
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp183a extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl190a ldaFcpl190a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField i;
    private DbsField j;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);

        // Local Variables
        i = localVariables.newFieldInRecord("i", "I", FieldType.NUMERIC, 2);
        j = localVariables.newFieldInRecord("j", "J", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp183a() throws Exception
    {
        super("Fcpp183a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        READWORK01:                                                                                                                                                       //Natural: READ WORK 01 EXT ( * )
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr().setValuesByName(pdaFcpaext.getExt_Extr());                                                                        //Natural: MOVE BY NAME EXTR TO #RPT-EXT.#PYMNT-ADDR
                                                                                                                                                                          //Natural: PERFORM PROCESS-INV-ACCT-DATA
            sub_Process_Inv_Acct_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, true, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr(), ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Inv_Info().getValue(1,":",i));                      //Natural: WRITE WORK FILE 02 VARIABLE #RPT-EXT.#PYMNT-ADDR #RPT-EXT.#INV-INFO ( 1:I )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *-----------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INV-ACCT-DATA
        //* *-----------------------------------**
        //* *----------**
    }
    private void sub_Process_Inv_Acct_Data() throws Exception                                                                                                             //Natural: PROCESS-INV-ACCT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR I 1 #RPT-EXT.C-INV-ACCT
        for (i.setValue(1); condition(i.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); i.nadd(1))
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Valuat_Period().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Valuat_Period().getValue(i));                             //Natural: ASSIGN #RPT-EXT.INV-ACCT-VALUAT-PERIOD ( I ) := EXT.INV-ACCT-VALUAT-PERIOD ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Cde().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Cde().getValue(i));                                                 //Natural: ASSIGN #RPT-EXT.INV-ACCT-CDE ( I ) := EXT.INV-ACCT-CDE ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Qty().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Unit_Qty().getValue(i));                                       //Natural: ASSIGN #RPT-EXT.INV-ACCT-UNIT-QTY ( I ) := EXT.INV-ACCT-UNIT-QTY ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Value().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Unit_Value().getValue(i));                                   //Natural: ASSIGN #RPT-EXT.INV-ACCT-UNIT-VALUE ( I ) := EXT.INV-ACCT-UNIT-VALUE ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(i));                                     //Natural: ASSIGN #RPT-EXT.INV-ACCT-SETTL-AMT ( I ) := EXT.INV-ACCT-SETTL-AMT ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue(i));                                         //Natural: ASSIGN #RPT-EXT.INV-ACCT-IVC-AMT ( I ) := EXT.INV-ACCT-IVC-AMT ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dci_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue(i));                                         //Natural: ASSIGN #RPT-EXT.INV-ACCT-DCI-AMT ( I ) := EXT.INV-ACCT-DCI-AMT ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(i));                                         //Natural: ASSIGN #RPT-EXT.INV-ACCT-DPI-AMT ( I ) := EXT.INV-ACCT-DPI-AMT ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(i));                                     //Natural: ASSIGN #RPT-EXT.INV-ACCT-DVDND-AMT ( I ) := EXT.INV-ACCT-DVDND-AMT ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(i));                               //Natural: ASSIGN #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( I ) := EXT.INV-ACCT-FDRL-TAX-AMT ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(i));                             //Natural: ASSIGN #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( I ) := EXT.INV-ACCT-STATE-TAX-AMT ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(i));                             //Natural: ASSIGN #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( I ) := EXT.INV-ACCT-LOCAL-TAX-AMT ( I )
            ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(i).setValue(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(i));                             //Natural: ASSIGN #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( I ) := EXT.INV-ACCT-NET-PYMNT-AMT ( I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpaext.getExt_C_Pymnt_Ded_Grp().notEquals(getZero())))                                                                                          //Natural: IF EXT.C-PYMNT-DED-GRP NE 0
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions().setValue(pdaFcpaext.getExt_C_Pymnt_Ded_Grp());                                                               //Natural: ASSIGN #RPT-EXT.#CNTR-DEDUCTIONS := EXT.C-PYMNT-DED-GRP
            FOR02:                                                                                                                                                        //Natural: FOR J 1 EXT.C-PYMNT-DED-GRP
            for (j.setValue(1); condition(j.lessOrEqual(pdaFcpaext.getExt_C_Pymnt_Ded_Grp())); j.nadd(1))
            {
                ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Cde().getValue(j).setValue(pdaFcpaext.getExt_Pymnt_Ded_Cde().getValue(j));                                           //Natural: ASSIGN #RPT-EXT.PYMNT-DED-CDE ( J ) := EXT.PYMNT-DED-CDE ( J )
                ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(j).setValue(pdaFcpaext.getExt_Pymnt_Ded_Amt().getValue(j));                                           //Natural: ASSIGN #RPT-EXT.PYMNT-DED-AMT ( J ) := EXT.PYMNT-DED-AMT ( J )
                ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Payee_Cde().getValue(j).setValue(pdaFcpaext.getExt_Pymnt_Ded_Payee_Cde().getValue(j));                               //Natural: ASSIGN #RPT-EXT.PYMNT-DED-PAYEE-CDE ( J ) := EXT.PYMNT-DED-PAYEE-CDE ( J )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }

    //
}
