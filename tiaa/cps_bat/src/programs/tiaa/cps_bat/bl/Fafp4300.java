/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:14:37 PM
**        * FROM NATURAL PROGRAM : Fafp4300
************************************************************
**        * FILE NAME            : Fafp4300.java
**        * CLASS NAME           : Fafp4300
**        * INSTANCE NAME        : Fafp4300
************************************************************
************************************************************************
* PROGRAM  : FAFP4300
* GENERATED: AUGUST 17, 2010
* SYSTEM   : RECONNET
* PURPOSE  : THIS MODULE EXPANDS DATES AND UNPACKS NUMERIC FIELDS
*
* REMARKS  : CLONED FROM FAFP4200
**********************  MAINTENANCE LOG ********************************
*  D A T E     PROGRAMMER     D E S C R I P T I O N
* 7/17/2010   F. KAUNITZ     UNPACK
* 4/2017      JJG            PIN EXPANSION CHANGES
*********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fafp4300 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField in;

    private DbsGroup in__R_Field_1;
    private DbsField in_Cntrct_Orgn_Cde;
    private DbsField in_Cntrct_Cancel_Rdrw_Ind;
    private DbsField in_Pymnt_Acctg_Dte;
    private DbsField in_Inv_Acct_Isa;
    private DbsField in_Inv_Acct_Cde_Num;
    private DbsField in_Cntrct_Check_Crrncy_Cde;
    private DbsField in_Inv_Acct_Ledgr_Nbr;
    private DbsField in_Pymnt_Check_Dte;
    private DbsField in_Pymnt_Settlmnt_Dte;
    private DbsField in_Ph_Last_Name;
    private DbsField in_Ph_First_Name;
    private DbsField in_Ph_Middle_Name;
    private DbsField in_Cntrct_Ppcn_Nbr;
    private DbsField in_Cntrct_Cref_Nbr;
    private DbsField in_Cntrct_Payee_Cde;
    private DbsField in_Cntrct_Unq_Id_Nbr;
    private DbsField in_Pymnt_Corp_Wpid;
    private DbsField in_Pymnt_Prcss_Seq_Nbr;
    private DbsField in_Pymnt_Cycle_Dte;
    private DbsField in_Pymnt_Intrfce_Dte;
    private DbsField in_Inv_Acct_Ledgr_Amt;
    private DbsField in_Inv_Acct_Ledgr_Ind;
    private DbsField in_Cntrct_Annty_Ins_Type;
    private DbsField in_Ledger_Filler;
    private DbsField wk_Trans_Date;
    private DbsField out;

    private DbsGroup out__R_Field_2;
    private DbsField out_Cntrct_Orgn_Cde;
    private DbsField out_Cntrct_Cancel_Rdrw_Ind;
    private DbsField out_Pymnt_Acctg_Dte;
    private DbsField out_Inv_Acct_Isa;
    private DbsField out_Inv_Acct_Cde;
    private DbsField out_Inv_Acct_Cde_Num;
    private DbsField out_Cntrct_Check_Crrncy_Cde;
    private DbsField out_Inv_Acct_Ledgr_Nbr;
    private DbsField out_Pymnt_Check_Dte;
    private DbsField out_Pymnt_Settlmnt_Dte;
    private DbsField out_Ph_Last_Name;
    private DbsField out_Ph_First_Name;
    private DbsField out_Ph_Middle_Name;
    private DbsField out_Cntrct_Ppcn_Nbr;
    private DbsField out_Cntrct_Cref_Nbr;
    private DbsField out_Cntrct_Payee_Cde;
    private DbsField out_Cntrct_Unq_Id_Nbr;
    private DbsField out_Pymnt_Corp_Wpid;
    private DbsField out_Pymnt_Prcss_Seq_Nbr;
    private DbsField out_Pymnt_Cycle_Dte;
    private DbsField out_Pymnt_Intrfce_Dte;
    private DbsField out_Inv_Acct_Ledgr_Amt;
    private DbsField out_Inv_Acct_Ledgr_Ind;
    private DbsField out_Cntrct_Annty_Ins_Type;
    private DbsField out_Ledger_Filler;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        in = localVariables.newFieldArrayInRecord("in", "IN", FieldType.STRING, 1, new DbsArrayController(1, 154));

        in__R_Field_1 = localVariables.newGroupInRecord("in__R_Field_1", "REDEFINE", in);
        in_Cntrct_Orgn_Cde = in__R_Field_1.newFieldInGroup("in_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        in_Cntrct_Cancel_Rdrw_Ind = in__R_Field_1.newFieldInGroup("in_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        in_Pymnt_Acctg_Dte = in__R_Field_1.newFieldInGroup("in_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        in_Inv_Acct_Isa = in__R_Field_1.newFieldInGroup("in_Inv_Acct_Isa", "INV-ACCT-ISA", FieldType.STRING, 5);
        in_Inv_Acct_Cde_Num = in__R_Field_1.newFieldInGroup("in_Inv_Acct_Cde_Num", "INV-ACCT-CDE-NUM", FieldType.NUMERIC, 2);
        in_Cntrct_Check_Crrncy_Cde = in__R_Field_1.newFieldInGroup("in_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        in_Inv_Acct_Ledgr_Nbr = in__R_Field_1.newFieldInGroup("in_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", FieldType.STRING, 15);
        in_Pymnt_Check_Dte = in__R_Field_1.newFieldInGroup("in_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        in_Pymnt_Settlmnt_Dte = in__R_Field_1.newFieldInGroup("in_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        in_Ph_Last_Name = in__R_Field_1.newFieldInGroup("in_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        in_Ph_First_Name = in__R_Field_1.newFieldInGroup("in_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        in_Ph_Middle_Name = in__R_Field_1.newFieldInGroup("in_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        in_Cntrct_Ppcn_Nbr = in__R_Field_1.newFieldInGroup("in_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        in_Cntrct_Cref_Nbr = in__R_Field_1.newFieldInGroup("in_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        in_Cntrct_Payee_Cde = in__R_Field_1.newFieldInGroup("in_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        in_Cntrct_Unq_Id_Nbr = in__R_Field_1.newFieldInGroup("in_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        in_Pymnt_Corp_Wpid = in__R_Field_1.newFieldInGroup("in_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        in_Pymnt_Prcss_Seq_Nbr = in__R_Field_1.newFieldInGroup("in_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        in_Pymnt_Cycle_Dte = in__R_Field_1.newFieldInGroup("in_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        in_Pymnt_Intrfce_Dte = in__R_Field_1.newFieldInGroup("in_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        in_Inv_Acct_Ledgr_Amt = in__R_Field_1.newFieldInGroup("in_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        in_Inv_Acct_Ledgr_Ind = in__R_Field_1.newFieldInGroup("in_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", FieldType.STRING, 1);
        in_Cntrct_Annty_Ins_Type = in__R_Field_1.newFieldInGroup("in_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        in_Ledger_Filler = in__R_Field_1.newFieldInGroup("in_Ledger_Filler", "LEDGER-FILLER", FieldType.STRING, 11);
        wk_Trans_Date = localVariables.newFieldInRecord("wk_Trans_Date", "WK-TRANS-DATE", FieldType.STRING, 8);
        out = localVariables.newFieldArrayInRecord("out", "OUT", FieldType.STRING, 1, new DbsArrayController(1, 183));

        out__R_Field_2 = localVariables.newGroupInRecord("out__R_Field_2", "REDEFINE", out);
        out_Cntrct_Orgn_Cde = out__R_Field_2.newFieldInGroup("out_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        out_Cntrct_Cancel_Rdrw_Ind = out__R_Field_2.newFieldInGroup("out_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        out_Pymnt_Acctg_Dte = out__R_Field_2.newFieldInGroup("out_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.STRING, 8);
        out_Inv_Acct_Isa = out__R_Field_2.newFieldInGroup("out_Inv_Acct_Isa", "INV-ACCT-ISA", FieldType.STRING, 5);
        out_Inv_Acct_Cde = out__R_Field_2.newFieldInGroup("out_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);
        out_Inv_Acct_Cde_Num = out__R_Field_2.newFieldInGroup("out_Inv_Acct_Cde_Num", "INV-ACCT-CDE-NUM", FieldType.NUMERIC, 2);
        out_Cntrct_Check_Crrncy_Cde = out__R_Field_2.newFieldInGroup("out_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        out_Inv_Acct_Ledgr_Nbr = out__R_Field_2.newFieldInGroup("out_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", FieldType.STRING, 15);
        out_Pymnt_Check_Dte = out__R_Field_2.newFieldInGroup("out_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.STRING, 8);
        out_Pymnt_Settlmnt_Dte = out__R_Field_2.newFieldInGroup("out_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.STRING, 8);
        out_Ph_Last_Name = out__R_Field_2.newFieldInGroup("out_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        out_Ph_First_Name = out__R_Field_2.newFieldInGroup("out_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        out_Ph_Middle_Name = out__R_Field_2.newFieldInGroup("out_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        out_Cntrct_Ppcn_Nbr = out__R_Field_2.newFieldInGroup("out_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        out_Cntrct_Cref_Nbr = out__R_Field_2.newFieldInGroup("out_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        out_Cntrct_Payee_Cde = out__R_Field_2.newFieldInGroup("out_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        out_Cntrct_Unq_Id_Nbr = out__R_Field_2.newFieldInGroup("out_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        out_Pymnt_Corp_Wpid = out__R_Field_2.newFieldInGroup("out_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        out_Pymnt_Prcss_Seq_Nbr = out__R_Field_2.newFieldInGroup("out_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        out_Pymnt_Cycle_Dte = out__R_Field_2.newFieldInGroup("out_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.STRING, 8);
        out_Pymnt_Intrfce_Dte = out__R_Field_2.newFieldInGroup("out_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.STRING, 8);
        out_Inv_Acct_Ledgr_Amt = out__R_Field_2.newFieldInGroup("out_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", FieldType.STRING, 13);
        out_Inv_Acct_Ledgr_Ind = out__R_Field_2.newFieldInGroup("out_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", FieldType.STRING, 1);
        out_Cntrct_Annty_Ins_Type = out__R_Field_2.newFieldInGroup("out_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        out_Ledger_Filler = out__R_Field_2.newFieldInGroup("out_Ledger_Filler", "LEDGER-FILLER", FieldType.STRING, 11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fafp4300() throws Exception
    {
        super("Fafp4300");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  ----------------------------------------------------------------------
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 IN ( * )
        while (condition(getWorkFiles().read(1, in.getValue("*"))))
        {
            out.getValue("*").reset();                                                                                                                                    //Natural: RESET OUT ( * )
                                                                                                                                                                          //Natural: PERFORM MOVE-FIELDS
            sub_Move_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, false, out.getValue("*"));                                                                                                            //Natural: WRITE WORK FILE 2 OUT ( * )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *******************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-FIELDS
    }
    private void sub_Move_Fields() throws Exception                                                                                                                       //Natural: MOVE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        out_Cntrct_Orgn_Cde.setValue(in_Cntrct_Orgn_Cde);                                                                                                                 //Natural: MOVE IN.CNTRCT-ORGN-CDE TO OUT.CNTRCT-ORGN-CDE
        out_Cntrct_Cancel_Rdrw_Ind.setValue(in_Cntrct_Cancel_Rdrw_Ind);                                                                                                   //Natural: MOVE IN.CNTRCT-CANCEL-RDRW-IND TO OUT.CNTRCT-CANCEL-RDRW-IND
        wk_Trans_Date.setValueEdited(in_Pymnt_Acctg_Dte,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED IN.PYMNT-ACCTG-DTE ( EM = YYYYMMDD ) TO WK-TRANS-DATE
        out_Pymnt_Acctg_Dte.setValue(wk_Trans_Date);                                                                                                                      //Natural: MOVE WK-TRANS-DATE TO OUT.PYMNT-ACCTG-DTE
        out_Inv_Acct_Isa.setValue(in_Inv_Acct_Isa);                                                                                                                       //Natural: MOVE IN.INV-ACCT-ISA TO OUT.INV-ACCT-ISA
        out_Inv_Acct_Cde_Num.setValue(in_Inv_Acct_Cde_Num);                                                                                                               //Natural: MOVE IN.INV-ACCT-CDE-NUM TO OUT.INV-ACCT-CDE-NUM
        out_Cntrct_Check_Crrncy_Cde.setValue(in_Cntrct_Check_Crrncy_Cde);                                                                                                 //Natural: MOVE IN.CNTRCT-CHECK-CRRNCY-CDE TO OUT.CNTRCT-CHECK-CRRNCY-CDE
        out_Inv_Acct_Ledgr_Nbr.setValue(in_Inv_Acct_Ledgr_Nbr);                                                                                                           //Natural: MOVE IN.INV-ACCT-LEDGR-NBR TO OUT.INV-ACCT-LEDGR-NBR
        wk_Trans_Date.setValueEdited(in_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED IN.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO WK-TRANS-DATE
        out_Pymnt_Check_Dte.setValue(wk_Trans_Date);                                                                                                                      //Natural: MOVE WK-TRANS-DATE TO OUT.PYMNT-CHECK-DTE
        wk_Trans_Date.setValueEdited(in_Pymnt_Settlmnt_Dte,new ReportEditMask("YYYYMMDD"));                                                                               //Natural: MOVE EDITED IN.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO WK-TRANS-DATE
        out_Pymnt_Settlmnt_Dte.setValue(wk_Trans_Date);                                                                                                                   //Natural: MOVE WK-TRANS-DATE TO OUT.PYMNT-SETTLMNT-DTE
        out_Ph_Last_Name.setValue(in_Ph_Last_Name);                                                                                                                       //Natural: MOVE IN.PH-LAST-NAME TO OUT.PH-LAST-NAME
        out_Ph_First_Name.setValue(in_Ph_First_Name);                                                                                                                     //Natural: MOVE IN.PH-FIRST-NAME TO OUT.PH-FIRST-NAME
        out_Ph_Middle_Name.setValue(in_Ph_Middle_Name);                                                                                                                   //Natural: MOVE IN.PH-MIDDLE-NAME TO OUT.PH-MIDDLE-NAME
        out_Cntrct_Ppcn_Nbr.setValue(in_Cntrct_Ppcn_Nbr);                                                                                                                 //Natural: MOVE IN.CNTRCT-PPCN-NBR TO OUT.CNTRCT-PPCN-NBR
        out_Cntrct_Cref_Nbr.setValue(in_Cntrct_Cref_Nbr);                                                                                                                 //Natural: MOVE IN.CNTRCT-CREF-NBR TO OUT.CNTRCT-CREF-NBR
        out_Cntrct_Payee_Cde.setValue(in_Cntrct_Payee_Cde);                                                                                                               //Natural: MOVE IN.CNTRCT-PAYEE-CDE TO OUT.CNTRCT-PAYEE-CDE
        out_Cntrct_Unq_Id_Nbr.setValue(in_Cntrct_Unq_Id_Nbr);                                                                                                             //Natural: MOVE IN.CNTRCT-UNQ-ID-NBR TO OUT.CNTRCT-UNQ-ID-NBR
        out_Pymnt_Corp_Wpid.setValue(in_Pymnt_Corp_Wpid);                                                                                                                 //Natural: MOVE IN.PYMNT-CORP-WPID TO OUT.PYMNT-CORP-WPID
        out_Pymnt_Prcss_Seq_Nbr.setValue(in_Pymnt_Prcss_Seq_Nbr);                                                                                                         //Natural: MOVE IN.PYMNT-PRCSS-SEQ-NBR TO OUT.PYMNT-PRCSS-SEQ-NBR
        wk_Trans_Date.setValueEdited(in_Pymnt_Cycle_Dte,new ReportEditMask("YYYYMMDD"));                                                                                  //Natural: MOVE EDITED IN.PYMNT-CYCLE-DTE ( EM = YYYYMMDD ) TO WK-TRANS-DATE
        out_Pymnt_Cycle_Dte.setValue(wk_Trans_Date);                                                                                                                      //Natural: MOVE WK-TRANS-DATE TO OUT.PYMNT-CYCLE-DTE
        wk_Trans_Date.setValueEdited(in_Pymnt_Intrfce_Dte,new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED IN.PYMNT-INTRFCE-DTE ( EM = YYYYMMDD ) TO WK-TRANS-DATE
        out_Pymnt_Intrfce_Dte.setValue(wk_Trans_Date);                                                                                                                    //Natural: MOVE WK-TRANS-DATE TO OUT.PYMNT-INTRFCE-DTE
        out_Inv_Acct_Ledgr_Amt.setValueEdited(in_Inv_Acct_Ledgr_Amt,new ReportEditMask("-999999999.99"));                                                                 //Natural: MOVE EDITED IN.INV-ACCT-LEDGR-AMT ( EM = -999999999.99 ) TO OUT.INV-ACCT-LEDGR-AMT
        out_Inv_Acct_Ledgr_Amt.setValue(out_Inv_Acct_Ledgr_Amt, MoveOption.RightJustified);                                                                               //Natural: MOVE RIGHT JUSTIFIED OUT.INV-ACCT-LEDGR-AMT TO OUT.INV-ACCT-LEDGR-AMT
        out_Inv_Acct_Ledgr_Ind.setValue(in_Inv_Acct_Ledgr_Ind);                                                                                                           //Natural: MOVE IN.INV-ACCT-LEDGR-IND TO OUT.INV-ACCT-LEDGR-IND
        out_Cntrct_Annty_Ins_Type.setValue(in_Cntrct_Annty_Ins_Type);                                                                                                     //Natural: MOVE IN.CNTRCT-ANNTY-INS-TYPE TO OUT.CNTRCT-ANNTY-INS-TYPE
        out_Ledger_Filler.setValue(in_Ledger_Filler);                                                                                                                     //Natural: MOVE IN.LEDGER-FILLER TO OUT.LEDGER-FILLER
    }

    //
}
