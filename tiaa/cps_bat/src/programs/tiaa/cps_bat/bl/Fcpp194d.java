/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:54 PM
**        * FROM NATURAL PROGRAM : Fcpp194d
************************************************************
**        * FILE NAME            : Fcpp194d.java
**        * CLASS NAME           : Fcpp194d
**        * INSTANCE NAME        : Fcpp194d
************************************************************
** NON REGENERATABLE
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: MONTHLY SETTLEMENT REPORT
**SAG SYSTEM: CPS
**SAG GDA: FCPG000
**SAG LOGICAL-NAMES(1): PRT1
**SAG REPORT-HEADING(1): CONSOLIDATED PAYMENT SYSTEM
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): CSR REPORT
**SAG PRINT-FILE(2): 1
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM WILL GENERATE A CSR REPORT
**SAG DA-NAME: FCPL190
**SAG MODEL-1: X
************************************************************************
* PROGRAM  : FCPP194D
* SYSTEM   : CPS
* TITLE    : CANCEL, STOP, REDRAW REPORT
* GENERATED: SEP 14,93 AT 09:16 AM
* FUNCTION : THIS PROGRAM WILL GENERATE A CANCEL / STOP / REDRAW REPORT
*
*
*
* HISTORY
*
* 04/21/99 : A. YOUNG     - REVISED DC DEDUCTION PROCESSING. FOR MULTI-
*                           FUNDED  PAYMENTS.
*
* 05/18/98 : A. YOUNG     - REVISED TO PROCESS 'AL' (ANNUITY LOAN).
*                         - ADDED BREAK PROCESSING FOR CSR INDICATOR
*                           IN ORDER TO COMBINE CANCEL, STOP, AND REDRAW
*                           REGISTERS.
*
* 06/12/96 : A. YOUNG     - REVISED TO UTILIZE FUND CODE TABLE FCPL199A.
*
* 06/05/96 : A. YOUNG     - INCREASED ALL ACCUMULATOR FIELDS.
*                           DECIMALS ---> (P15.2)
*                           COUNTERS ---> (P9)
*
* 08/24/94  ADAPTED FROM FCPP298 BY A. YOUNG FOR USE IN THE
*           ON-LINE PROCESSING STREAM.
*
* 02/15/94  MODIFIED BY JUN TINIO TO ITEMIZE DEDUCTIONS BY INSTALLMENT
*           DATE.
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
* 11/09/01  BEFORE DECIDE ON LINE 3315 #TI = 1.
*           IF #RPT-EXT.CNTRCT-ORGN-CDE DOES NOT MEET THE DECIDE
*           CONDITION THERE WILL BE A VALUE IN #TI.
*
* 12/28/2001 ILSE BOHM - ARRAY EXPANDED FROM 40 TO 80 FOR DETAILS
*                THIS PROGRAM MUST BE RE-WRITTEN WITHOUT THIS
*                        ARRAY !!!
*
* 03/06/2006 : LANDRUM  PAYEE MATCH
*              - INCREASED CHECK NBR TO N10.
* 04/14/2015 : MUKHERR  - INCREASED DETAIL-LINE FROM 80 TO 999
*              TAG - MUKHERR1
* 4/2017    : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp194d extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl190a ldaFcpl190a;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_N10__R_Field_1;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N7;

    private DbsGroup pnd_Check_Number_N10__R_Field_2;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_A10;
    private DbsField pnd_Check_Datex;

    private DbsGroup pnd_Check_Datex__R_Field_3;
    private DbsField pnd_Check_Datex_Pnd_Check_Date;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Input_Cycle;
    private DbsField pnd_Header1;
    private DbsField pnd_Header2;
    private DbsField pnd_Header2_Tmp;
    private DbsField pnd_Header3;
    private DbsField pnd_Header4;
    private DbsField pnd_Settl_System;
    private DbsField pnd_Csr_Actvty;
    private DbsField pnd_W_Fld;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_X;
    private DbsField pnd_Len;
    private DbsField pnd_Summary;
    private DbsField pnd_Dollar_I;
    private DbsField pnd_Dollar_I_Max;
    private DbsField pnd_Pi;
    private DbsField pnd_Ti;
    private DbsField pnd_Ti_Max;
    private DbsField pnd_Start_Index;
    private DbsField pnd_End_Index;
    private DbsField pnd_Ln;
    private DbsField pnd_Ln1;
    private DbsField pnd_First;
    private DbsField pnd_Month_End;
    private DbsField pnd_Type_Sub_Prt;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_Prcss_Seq_Nbr__R_Field_4;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Nbr;
    private DbsField pnd_Prev_Seq_Nbr;
    private DbsField pnd_Orgn_Sub_Txt;
    private DbsField pnd_Type_Sub_Txt;
    private DbsField pnd_Sub_Txt;
    private DbsField pnd_Dte_Txt;
    private DbsField pnd_No_Guar_Stck;
    private DbsField pnd_Init;
    private DbsField pnd_Hdr_Dte;
    private DbsField pnd_Old_Cntrct_Orgn_Cde;
    private DbsField pnd_Old_Csr_Cde;
    private DbsField pnd_O_Ftre_Ind;
    private DbsField pnd_Cf_Txt;

    private DbsGroup pnd_Dtl_Lines;
    private DbsField pnd_Dtl_Lines_Pnd_Dividends;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Gross_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Ded_Exp;
    private DbsField pnd_Dtl_Lines_Pnd_Ovrpymnt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt;

    private DbsGroup pnd_Item;
    private DbsField pnd_Item_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Item_Pnd_Gross_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Item_Pnd_Ded_Exp;
    private DbsField pnd_Item_Pnd_Ovrpymnt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt;

    private DbsGroup pnd_Misc;
    private DbsField pnd_Misc_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Misc_Pnd_Annt;
    private DbsField pnd_Misc_Pnd_Payee;
    private DbsField pnd_Misc_Pnd_Cntrct_Nbr;
    private DbsField pnd_Misc_Cntrct_Hold_Cde;
    private DbsField pnd_Misc_Pymnt_Check_Dte;
    private DbsField pnd_Misc_Pymnt_Acctg_Dte;
    private DbsField pnd_Misc_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Misc_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Misc_Pymnt_Check_Nbr;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Misc_Pnd_Cge_Txt;
    private DbsField pnd_Misc_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Misc_Pymnt_Intrfce_Dte;
    private DbsField pnd_Misc_Due_Date;

    private DbsGroup pnd_Dte;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Dte_Pnd_Gross_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Ded_Exp;
    private DbsField pnd_Dte_Pnd_Ovrpymnt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Dte_Pnd_Qty;

    private DbsGroup pnd_Type_Sub;
    private DbsField pnd_Type_Sub_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Type_Sub_Pnd_Gross_Amt;
    private DbsField pnd_Type_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Type_Sub_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Type_Sub_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Type_Sub_Pnd_Ded_Exp;
    private DbsField pnd_Type_Sub_Pnd_Ovrpymnt;
    private DbsField pnd_Type_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Type_Sub_Pnd_Qty;

    private DbsGroup pnd_Sum;
    private DbsField pnd_Sum_Pnd_Current;
    private DbsField pnd_Sum_Pnd_Future;
    private DbsField pnd_Sum_Pnd_C_Qty;
    private DbsField pnd_Sum_Pnd_F_Qty;

    private DbsGroup pnd_G_Total;
    private DbsField pnd_G_Total_Pnd_Current;
    private DbsField pnd_G_Total_Pnd_Future;
    private DbsField pnd_G_Total_Pnd_C_Qty;
    private DbsField pnd_G_Total_Pnd_F_Qty;

    private DbsGroup pnd_Us_Can;
    private DbsField pnd_Us_Can_Pnd_Current;
    private DbsField pnd_Us_Can_Pnd_Future;
    private DbsField pnd_Us_Can_Pnd_C_Qty;
    private DbsField pnd_Us_Can_Pnd_F_Qty;
    private DbsField pnd_C_Total;
    private DbsField pnd_F_Total;
    private DbsField pnd_T_Total;
    private DbsField pnd_Cqty;
    private DbsField pnd_Fqty;
    private DbsField pnd_Tqty;
    private DbsField pnd_Month;
    private DbsField pnd_Year;
    private DbsField pnd_Printed_1;
    private DbsField pnd_Data;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pymnt_Ftre_IndOld;
    private DbsField readWork01Cntrct_Orgn_CdeOld;
    private DbsField readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Check_Number_N10 = localVariables.newFieldInRecord("pnd_Check_Number_N10", "#CHECK-NUMBER-N10", FieldType.NUMERIC, 10);

        pnd_Check_Number_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_1", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_N3 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_N10_Pnd_Check_Number_N7 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        pnd_Check_Number_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_2", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_A10 = pnd_Check_Number_N10__R_Field_2.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_A10", "#CHECK-NUMBER-A10", 
            FieldType.STRING, 10);
        pnd_Check_Datex = localVariables.newFieldInRecord("pnd_Check_Datex", "#CHECK-DATEX", FieldType.STRING, 8);

        pnd_Check_Datex__R_Field_3 = localVariables.newGroupInRecord("pnd_Check_Datex__R_Field_3", "REDEFINE", pnd_Check_Datex);
        pnd_Check_Datex_Pnd_Check_Date = pnd_Check_Datex__R_Field_3.newFieldInGroup("pnd_Check_Datex_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Input_Cycle = localVariables.newFieldInRecord("pnd_Input_Cycle", "#INPUT-CYCLE", FieldType.STRING, 5);
        pnd_Header1 = localVariables.newFieldInRecord("pnd_Header1", "#HEADER1", FieldType.STRING, 60);
        pnd_Header2 = localVariables.newFieldInRecord("pnd_Header2", "#HEADER2", FieldType.STRING, 90);
        pnd_Header2_Tmp = localVariables.newFieldInRecord("pnd_Header2_Tmp", "#HEADER2-TMP", FieldType.STRING, 20);
        pnd_Header3 = localVariables.newFieldInRecord("pnd_Header3", "#HEADER3", FieldType.STRING, 90);
        pnd_Header4 = localVariables.newFieldInRecord("pnd_Header4", "#HEADER4", FieldType.STRING, 60);
        pnd_Settl_System = localVariables.newFieldInRecord("pnd_Settl_System", "#SETTL-SYSTEM", FieldType.STRING, 13);
        pnd_Csr_Actvty = localVariables.newFieldInRecord("pnd_Csr_Actvty", "#CSR-ACTVTY", FieldType.STRING, 16);
        pnd_W_Fld = localVariables.newFieldInRecord("pnd_W_Fld", "#W-FLD", FieldType.STRING, 60);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.INTEGER, 2);
        pnd_Summary = localVariables.newFieldInRecord("pnd_Summary", "#SUMMARY", FieldType.BOOLEAN, 1);
        pnd_Dollar_I = localVariables.newFieldInRecord("pnd_Dollar_I", "#$I", FieldType.INTEGER, 2);
        pnd_Dollar_I_Max = localVariables.newFieldInRecord("pnd_Dollar_I_Max", "#$I-MAX", FieldType.INTEGER, 2);
        pnd_Pi = localVariables.newFieldInRecord("pnd_Pi", "#PI", FieldType.INTEGER, 2);
        pnd_Ti = localVariables.newFieldInRecord("pnd_Ti", "#TI", FieldType.INTEGER, 2);
        pnd_Ti_Max = localVariables.newFieldInRecord("pnd_Ti_Max", "#TI-MAX", FieldType.INTEGER, 2);
        pnd_Start_Index = localVariables.newFieldInRecord("pnd_Start_Index", "#START-INDEX", FieldType.INTEGER, 2);
        pnd_End_Index = localVariables.newFieldInRecord("pnd_End_Index", "#END-INDEX", FieldType.INTEGER, 2);
        pnd_Ln = localVariables.newFieldInRecord("pnd_Ln", "#LN", FieldType.PACKED_DECIMAL, 4);
        pnd_Ln1 = localVariables.newFieldInRecord("pnd_Ln1", "#LN1", FieldType.PACKED_DECIMAL, 4);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Month_End = localVariables.newFieldInRecord("pnd_Month_End", "#MONTH-END", FieldType.BOOLEAN, 1);
        pnd_Type_Sub_Prt = localVariables.newFieldInRecord("pnd_Type_Sub_Prt", "#TYPE-SUB-PRT", FieldType.BOOLEAN, 1);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Prcss_Seq_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Prcss_Seq_Nbr", "#PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_Prcss_Seq_Nbr__R_Field_4 = localVariables.newGroupInRecord("pnd_Pymnt_Prcss_Seq_Nbr__R_Field_4", "REDEFINE", pnd_Pymnt_Prcss_Seq_Nbr);
        pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr = pnd_Pymnt_Prcss_Seq_Nbr__R_Field_4.newFieldInGroup("pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr", "#CURR-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Nbr = pnd_Pymnt_Prcss_Seq_Nbr__R_Field_4.newFieldInGroup("pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Nbr", "#INSTMNT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Seq_Nbr = localVariables.newFieldInRecord("pnd_Prev_Seq_Nbr", "#PREV-SEQ-NBR", FieldType.NUMERIC, 7);
        pnd_Orgn_Sub_Txt = localVariables.newFieldInRecord("pnd_Orgn_Sub_Txt", "#ORGN-SUB-TXT", FieldType.STRING, 60);
        pnd_Type_Sub_Txt = localVariables.newFieldInRecord("pnd_Type_Sub_Txt", "#TYPE-SUB-TXT", FieldType.STRING, 60);
        pnd_Sub_Txt = localVariables.newFieldInRecord("pnd_Sub_Txt", "#SUB-TXT", FieldType.STRING, 60);
        pnd_Dte_Txt = localVariables.newFieldInRecord("pnd_Dte_Txt", "#DTE-TXT", FieldType.STRING, 60);
        pnd_No_Guar_Stck = localVariables.newFieldInRecord("pnd_No_Guar_Stck", "#NO-GUAR-STCK", FieldType.BOOLEAN, 1);
        pnd_Init = localVariables.newFieldInRecord("pnd_Init", "#INIT", FieldType.BOOLEAN, 1);
        pnd_Hdr_Dte = localVariables.newFieldInRecord("pnd_Hdr_Dte", "#HDR-DTE", FieldType.STRING, 10);
        pnd_Old_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Old_Cntrct_Orgn_Cde", "#OLD-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Old_Csr_Cde = localVariables.newFieldInRecord("pnd_Old_Csr_Cde", "#OLD-CSR-CDE", FieldType.STRING, 2);
        pnd_O_Ftre_Ind = localVariables.newFieldInRecord("pnd_O_Ftre_Ind", "#O-FTRE-IND", FieldType.STRING, 1);
        pnd_Cf_Txt = localVariables.newFieldInRecord("pnd_Cf_Txt", "#CF-TXT", FieldType.STRING, 18);

        pnd_Dtl_Lines = localVariables.newGroupArrayInRecord("pnd_Dtl_Lines", "#DTL-LINES", new DbsArrayController(1, 9999));
        pnd_Dtl_Lines_Pnd_Dividends = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Dividends", "#DIVIDENDS", FieldType.BOOLEAN, 1);
        pnd_Dtl_Lines_Pnd_Inv_Acct = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct", "#INV-ACCT", FieldType.STRING, 8);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period", "#INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 3);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Qty = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Qty", "#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Value = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Value", "#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            10, 4);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Settl_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Dvdnd_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Dpi_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            6, 2);
        pnd_Dtl_Lines_Pnd_Gross_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Dtl_Lines_Pnd_Ded_Exp = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Ded_Exp", "#DED-EXP", FieldType.PACKED_DECIMAL, 6, 2);
        pnd_Dtl_Lines_Pnd_Ovrpymnt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Ovrpymnt", "#OVRPYMNT", FieldType.PACKED_DECIMAL, 6, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Item = localVariables.newGroupInRecord("pnd_Item", "#ITEM");
        pnd_Item_Pnd_Inv_Acct_Dpi_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 6, 2);
        pnd_Item_Pnd_Gross_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Item_Pnd_Inv_Acct_State_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Item_Pnd_Ded_Exp = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Ded_Exp", "#DED-EXP", FieldType.PACKED_DECIMAL, 6, 2);
        pnd_Item_Pnd_Ovrpymnt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Ovrpymnt", "#OVRPYMNT", FieldType.PACKED_DECIMAL, 6, 2);
        pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Misc = localVariables.newGroupInRecord("pnd_Misc", "#MISC");
        pnd_Misc_Cntrct_Ppcn_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Misc_Pnd_Annt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Annt", "#ANNT", FieldType.STRING, 50);
        pnd_Misc_Pnd_Payee = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Payee", "#PAYEE", FieldType.STRING, 38);
        pnd_Misc_Pnd_Cntrct_Nbr = pnd_Misc.newFieldArrayInGroup("pnd_Misc_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1, 
            5));
        pnd_Misc_Cntrct_Hold_Cde = pnd_Misc.newFieldInGroup("pnd_Misc_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Misc_Pymnt_Check_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Misc_Pymnt_Acctg_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Misc_Pymnt_Settlmnt_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Misc_Pymnt_Check_Seq_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Misc_Pymnt_Check_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Misc_Pnd_Inv_Acct_Settl_Amt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Misc_Pnd_Cge_Txt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Cge_Txt", "#CGE-TXT", FieldType.STRING, 7);
        pnd_Misc_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Misc.newFieldInGroup("pnd_Misc_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        pnd_Misc_Pymnt_Intrfce_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Misc_Due_Date = pnd_Misc.newFieldInGroup("pnd_Misc_Due_Date", "DUE-DATE", FieldType.STRING, 8);

        pnd_Dte = localVariables.newGroupInRecord("pnd_Dte", "#DTE");
        pnd_Dte_Pnd_Inv_Acct_Dpi_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Dte_Pnd_Gross_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Dte_Pnd_Ded_Exp = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Ded_Exp", "#DED-EXP", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Dte_Pnd_Ovrpymnt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Ovrpymnt", "#OVRPYMNT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Dte_Pnd_Qty = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Qty", "#QTY", FieldType.PACKED_DECIMAL, 9);

        pnd_Type_Sub = localVariables.newGroupInRecord("pnd_Type_Sub", "#TYPE-SUB");
        pnd_Type_Sub_Pnd_Inv_Acct_Dpi_Amt = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Type_Sub_Pnd_Gross_Amt = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Type_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Type_Sub_Pnd_Inv_Acct_State_Tax_Amt = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Type_Sub_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Type_Sub_Pnd_Ded_Exp = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Ded_Exp", "#DED-EXP", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Type_Sub_Pnd_Ovrpymnt = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Ovrpymnt", "#OVRPYMNT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Type_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Type_Sub_Pnd_Qty = pnd_Type_Sub.newFieldInGroup("pnd_Type_Sub_Pnd_Qty", "#QTY", FieldType.PACKED_DECIMAL, 9);

        pnd_Sum = localVariables.newGroupArrayInRecord("pnd_Sum", "#SUM", new DbsArrayController(1, 6, 1, 2));
        pnd_Sum_Pnd_Current = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_Current", "#CURRENT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            3));
        pnd_Sum_Pnd_Future = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_Future", "#FUTURE", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 3));
        pnd_Sum_Pnd_C_Qty = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_C_Qty", "#C-QTY", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 3));
        pnd_Sum_Pnd_F_Qty = pnd_Sum.newFieldArrayInGroup("pnd_Sum_Pnd_F_Qty", "#F-QTY", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 3));

        pnd_G_Total = localVariables.newGroupArrayInRecord("pnd_G_Total", "#G-TOTAL", new DbsArrayController(1, 6, 1, 3));
        pnd_G_Total_Pnd_Current = pnd_G_Total.newFieldInGroup("pnd_G_Total_Pnd_Current", "#CURRENT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_G_Total_Pnd_Future = pnd_G_Total.newFieldInGroup("pnd_G_Total_Pnd_Future", "#FUTURE", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_G_Total_Pnd_C_Qty = pnd_G_Total.newFieldInGroup("pnd_G_Total_Pnd_C_Qty", "#C-QTY", FieldType.PACKED_DECIMAL, 9);
        pnd_G_Total_Pnd_F_Qty = pnd_G_Total.newFieldInGroup("pnd_G_Total_Pnd_F_Qty", "#F-QTY", FieldType.PACKED_DECIMAL, 9);

        pnd_Us_Can = localVariables.newGroupArrayInRecord("pnd_Us_Can", "#US-CAN", new DbsArrayController(1, 6));
        pnd_Us_Can_Pnd_Current = pnd_Us_Can.newFieldArrayInGroup("pnd_Us_Can_Pnd_Current", "#CURRENT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_Us_Can_Pnd_Future = pnd_Us_Can.newFieldArrayInGroup("pnd_Us_Can_Pnd_Future", "#FUTURE", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 
            2));
        pnd_Us_Can_Pnd_C_Qty = pnd_Us_Can.newFieldArrayInGroup("pnd_Us_Can_Pnd_C_Qty", "#C-QTY", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            2));
        pnd_Us_Can_Pnd_F_Qty = pnd_Us_Can.newFieldArrayInGroup("pnd_Us_Can_Pnd_F_Qty", "#F-QTY", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            2));
        pnd_C_Total = localVariables.newFieldInRecord("pnd_C_Total", "#C-TOTAL", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_F_Total = localVariables.newFieldInRecord("pnd_F_Total", "#F-TOTAL", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_T_Total = localVariables.newFieldInRecord("pnd_T_Total", "#T-TOTAL", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Cqty = localVariables.newFieldInRecord("pnd_Cqty", "#CQTY", FieldType.PACKED_DECIMAL, 9);
        pnd_Fqty = localVariables.newFieldInRecord("pnd_Fqty", "#FQTY", FieldType.PACKED_DECIMAL, 9);
        pnd_Tqty = localVariables.newFieldInRecord("pnd_Tqty", "#TQTY", FieldType.PACKED_DECIMAL, 9);
        pnd_Month = localVariables.newFieldInRecord("pnd_Month", "#MONTH", FieldType.STRING, 9);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);
        pnd_Printed_1 = localVariables.newFieldInRecord("pnd_Printed_1", "#PRINTED-1", FieldType.BOOLEAN, 1);
        pnd_Data = localVariables.newFieldInRecord("pnd_Data", "#DATA", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pymnt_Ftre_IndOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pymnt_Ftre_Ind_OLD", "Pymnt_Ftre_Ind_OLD", FieldType.STRING, 1);
        readWork01Cntrct_Orgn_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cntrct_Orgn_Cde_OLD", "Cntrct_Orgn_Cde_OLD", FieldType.STRING, 
            2);
        readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cntrct_Cancel_Rdrw_Actvty_Cde_OLD", "Cntrct_Cancel_Rdrw_Actvty_Cde_OLD", 
            FieldType.STRING, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaCdbatxa.initializeValues();
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_Header1.setInitialValue(" ");
        pnd_Header2.setInitialValue(" ");
        pnd_Header2_Tmp.setInitialValue(" ");
        pnd_Header3.setInitialValue(" ");
        pnd_Header4.setInitialValue(" ");
        pnd_Settl_System.setInitialValue(" ");
        pnd_Csr_Actvty.setInitialValue("   CAN / RDW    ");
        pnd_Dollar_I.setInitialValue(1);
        pnd_Dollar_I_Max.setInitialValue(1);
        pnd_First.setInitialValue(true);
        pnd_Type_Sub_Prt.setInitialValue(false);
        pnd_Init.setInitialValue(true);
        pnd_Old_Cntrct_Orgn_Cde.setInitialValue(" ");
        pnd_Old_Csr_Cde.setInitialValue(" ");
        pnd_O_Ftre_Ind.setInitialValue(" ");
        pnd_Printed_1.setInitialValue(false);
        pnd_Data.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp194d() throws Exception
    {
        super("Fcpp194d");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp194d|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                getReports().definePrinter(2, "PRT1");                                                                                                                    //Natural: DEFINE PRINTER ( PRT1 = 1 )
                //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.                                                                               //Natural: FORMAT ( PRT1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
                pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                              //Natural: ASSIGN #CUR-LANG := *LANGUAGE
                pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                       //Natural: ASSIGN #CUR-LANG := CDBATXA.#LANG-MAP ( #CUR-LANG )
                pnd_Header1.setValue("Consolidated Payment System");                                                                                                      //Natural: ASSIGN #HEADER1 := 'Consolidated Payment System'
                pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(60,27)).divide(2));                                                             //Natural: ASSIGN #LEN := ( 60 - 27 ) / 2
                pnd_W_Fld.moveAll("H'00'");                                                                                                                               //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
                pnd_Header1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header1));                                                            //Natural: COMPRESS #W-FLD #HEADER1 INTO #HEADER1 LEAVE NO SPACE
                DbsUtil.examine(new ExamineSource(pnd_Header1,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                          //Natural: EXAMINE FULL #HEADER1 FOR FULL H'00' REPLACE ' '
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: AT TOP OF PAGE ( PRT1 );//Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA = 'INFP9000'
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=MITL'_'"), new SignPosition (true), new InputPrompting(false),                //Natural: INPUT ( AD = MITL'_' SG = ON IP = OFF ZP = OFF ) 'Input Parm:' #INPUT-CYCLE
                    new ReportZeroPrint (false),"Input Parm:",pnd_Input_Cycle);
                //*  05-18-98
                short decideConditionsMet630 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE *INIT-USER;//Natural: VALUE 'P1420CPD'
                if (condition((Global.getINIT_USER().equals("P1420CPD"))))
                {
                    decideConditionsMet630++;
                    pnd_Header2_Tmp.setValue("CYCLE");                                                                                                                    //Natural: ASSIGN #HEADER2-TMP := 'CYCLE'
                }                                                                                                                                                         //Natural: VALUE 'P1440CPD'
                else if (condition((Global.getINIT_USER().equals("P1440CPD"))))
                {
                    decideConditionsMet630++;
                    pnd_Header2_Tmp.setValue("LOGICAL END-OF-DAY");                                                                                                       //Natural: ASSIGN #HEADER2-TMP := 'LOGICAL END-OF-DAY'
                }                                                                                                                                                         //Natural: VALUE 'P1460CPD'
                else if (condition((Global.getINIT_USER().equals("P1460CPD"))))
                {
                    decideConditionsMet630++;
                    pnd_Header2_Tmp.setValue("PHYSICAL END-OF-DAY");                                                                                                      //Natural: ASSIGN #HEADER2-TMP := 'PHYSICAL END-OF-DAY'
                }                                                                                                                                                         //Natural: VALUE 'P1480CPM'
                else if (condition((Global.getINIT_USER().equals("P1480CPM"))))
                {
                    decideConditionsMet630++;
                    pnd_Header2_Tmp.setValue("MONTH-END");                                                                                                                //Natural: ASSIGN #HEADER2-TMP := 'MONTH-END'
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet630 > 0))
                {
                    if (condition(pnd_Input_Cycle.equals("MONTH")))                                                                                                       //Natural: IF #INPUT-CYCLE = 'MONTH'
                    {
                        pnd_Month_End.setValue(true);                                                                                                                     //Natural: ASSIGN #MONTH-END := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Month_End.setValue(false);                                                                                                                    //Natural: ASSIGN #MONTH-END := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Header2_Tmp.setValue("CYCLE");                                                                                                                    //Natural: ASSIGN #HEADER2-TMP := 'CYCLE'
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
                sub_Get_Check_Formatting_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  READ THE EXTRACT FILE
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK 14 #RPT-EXT
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(14, ldaFcpl190a.getPnd_Rpt_Ext())))
                {
                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*  05-18-98
                    //*  LEON 'CN SN'
                    //*  JWO 2010-11
                    //*  05-18-98
                    if (condition(!(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")  //Natural: ACCEPT IF #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'R' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'RP' OR = 'SP' OR = 'PR'
                        || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") 
                        || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") 
                        || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
                        || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
                    {
                        continue;
                    }
                    //*                                                                                                                                                   //Natural: AT BREAK OF #RPT-EXT.PYMNT-INTRFCE-DTE;//Natural: AT BREAK OF #RPT-EXT.PYMNT-CHECK-DTE;//Natural: AT BREAK #RPT-EXT.PYMNT-FTRE-IND;//Natural: AT BREAK OF #RPT-EXT.CNTRCT-TYPE-CDE;//Natural: AT BREAK OF #RPT-EXT.CNTRCT-CRRNCY-CDE;//Natural: AT BREAK OF #RPT-EXT.CNTRCT-ORGN-CDE;//Natural: AT BREAK #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
                    pnd_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr());                                                                   //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := #RPT-EXT.PYMNT-PRCSS-SEQ-NBR
                    pnd_Data.setValue(true);                                                                                                                              //Natural: ASSIGN #DATA := TRUE
                    if (condition(pnd_Init.getBoolean()))                                                                                                                 //Natural: IF #INIT
                    {
                        pnd_Init.setValue(false);                                                                                                                         //Natural: ASSIGN #INIT := FALSE
                        pnd_Dollar_I.compute(new ComputeParameters(false, pnd_Dollar_I), ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Crrncy_Cde().val());                           //Natural: COMPUTE #$I = VAL ( #RPT-EXT.CNTRCT-CRRNCY-CDE )
                        pnd_Dollar_I_Max.setValue(pnd_Dollar_I);                                                                                                          //Natural: ASSIGN #$I-MAX := #$I
                        pnd_O_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());                                                                             //Natural: ASSIGN #O-FTRE-IND := #RPT-EXT.PYMNT-FTRE-IND
                        //*  05-18-98
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CSR-TYPE
                        sub_Determine_Csr_Type();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  02-13-95 : A. YOUNG
                                                                                                                                                                          //Natural: PERFORM DETERMINE-SETTL-SYSTEM
                        sub_Determine_Settl_System();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
                        sub_Initialize_Header();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INCREMENT-INDEX
                        sub_Increment_Index();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Old_Cntrct_Orgn_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());                                                                   //Natural: ASSIGN #OLD-CNTRCT-ORGN-CDE := #RPT-EXT.CNTRCT-ORGN-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_First.getBoolean()))                                                                                                                //Natural: IF #FIRST
                    {
                        pnd_First.setValue(false);                                                                                                                        //Natural: ASSIGN #FIRST := FALSE
                        pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                              //Natural: ASSIGN #PREV-SEQ-NBR := #CURR-SEQ-NBR
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Prev_Seq_Nbr.equals(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr)))                                                                     //Natural: IF #PREV-SEQ-NBR = #CURR-SEQ-NBR
                    {
                                                                                                                                                                          //Natural: PERFORM BUILD-DETAIL-LINES
                        sub_Build_Detail_Lines();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ITEM-SUB-TOTALS
                        sub_Calculate_Item_Sub_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                        sub_Print_Detail_Lines();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM BUILD-DETAIL-LINES
                        sub_Build_Detail_Lines();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                                  //Natural: ASSIGN #PREV-SEQ-NBR := #CURR-SEQ-NBR
                    readWork01Pymnt_Ftre_IndOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());                                                                    //Natural: END-WORK
                    readWork01Cntrct_Orgn_CdeOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());
                    readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde());
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (Global.isEscape()) return;
                //*  05-18-98
                if (condition(! (pnd_Data.getBoolean())))                                                                                                                 //Natural: IF NOT #DATA
                {
                                                                                                                                                                          //Natural: PERFORM SUMMARY-EVALUATION
                    sub_Summary_Evaluation();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-HEADER
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CENTER-HEADER-2-3
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CENTER-SUB-HEADER
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-DETAIL-LINES
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MISCELLANEOUS-DATA
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-ITEM-SUB-TOTALS
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DTE-SUB-TOTAL
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TYPE-SUB-TOTAL
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-HEADER
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INCREMENT-INDEX
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-LINES
                //* *#CGE-TXT #MISC.PYMNT-CHECK-NBR(EM=9999999) 2X
                //* *'PAYEE:' #MISC.#PAYEE 2X
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-SUMMARY-DATA
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-SETTL-SYSTEM
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CSR-TYPE
                //* *
                //* *VALUE 'C'  #CSR-ACTVTY := 'CANCELLED'
                //* *VALUE 'R'  #CSR-ACTVTY := ' REDRAWN '
                //* *VALUE 'S'  #CSR-ACTVTY := ' STOPPED '
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-TXT
                //* **------------------------------------------------------------------***
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-EVALUATION
                //* **------------------------------------------------------------------***
                //* **------------------------------------------------------------------***
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-VARIABLES
                //* *********************** RL BEGIN - PAYEE MATCH ************MAR 06,2006
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
                //*  RL PAYEE MATCH
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //*  RL PAYEE MATCH
                //* ************************** RL END-PAYEE MATCH *************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    //*  05-18-98
    private void sub_Initialize_Header() throws Exception                                                                                                                 //Natural: INITIALIZE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Header2.setValue("SETTLEMENTS");                                                                                                                              //Natural: ASSIGN #HEADER2 := 'SETTLEMENTS'
        pnd_Header3.setValue("SETTLEMENTS - CANADIAN");                                                                                                                   //Natural: ASSIGN #HEADER3 := 'SETTLEMENTS - CANADIAN'
        if (condition(pnd_Month_End.getBoolean()))                                                                                                                        //Natural: IF #MONTH-END
        {
            pnd_Month.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("LLLLLLLLL"));                                                       //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = L ( 9 ) ) TO #MONTH
            pnd_Year.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYY"));                                                             //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYY ) TO #YEAR
            pnd_Header2.setValue(DbsUtil.compress(pnd_Settl_System, pnd_Header2, pnd_Header2_Tmp, pnd_Csr_Actvty, "PAYMENT REGISTER", pnd_Month, pnd_Year));              //Natural: COMPRESS #SETTL-SYSTEM #HEADER2 #HEADER2-TMP #CSR-ACTVTY 'PAYMENT REGISTER' #MONTH #YEAR INTO #HEADER2
            pnd_Header3.setValue(DbsUtil.compress(pnd_Settl_System, pnd_Header3, pnd_Header2_Tmp, pnd_Csr_Actvty, "PAYMENT REGISTER", pnd_Month, pnd_Year));              //Natural: COMPRESS #SETTL-SYSTEM #HEADER3 #HEADER2-TMP #CSR-ACTVTY 'PAYMENT REGISTER' #MONTH #YEAR INTO #HEADER3
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(! (pnd_Summary.getBoolean())))                                                                                                                  //Natural: IF NOT #SUMMARY
            {
                pnd_Hdr_Dte.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                                //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #HDR-DTE
                pnd_Header2.setValue(DbsUtil.compress(pnd_Settl_System, pnd_Header2, pnd_Header2_Tmp, pnd_Csr_Actvty, "PAYMENT REGISTER", "For", pnd_Hdr_Dte));           //Natural: COMPRESS #SETTL-SYSTEM #HEADER2 #HEADER2-TMP #CSR-ACTVTY 'PAYMENT REGISTER' 'For' #HDR-DTE INTO #HEADER2
                pnd_Header3.setValue(DbsUtil.compress(pnd_Settl_System, pnd_Header3, pnd_Header2_Tmp, pnd_Csr_Actvty, "PAYMENT REGISTER", "For", pnd_Hdr_Dte));           //Natural: COMPRESS #SETTL-SYSTEM #HEADER3 #HEADER2-TMP #CSR-ACTVTY 'PAYMENT REGISTER' 'For' #HDR-DTE INTO #HEADER3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Header2.setValue(DbsUtil.compress(pnd_Settl_System, pnd_Header2, pnd_Header2_Tmp, pnd_Csr_Actvty, "PAYMENT REGISTER"));                               //Natural: COMPRESS #SETTL-SYSTEM #HEADER2 #HEADER2-TMP #CSR-ACTVTY 'PAYMENT REGISTER' INTO #HEADER2
                pnd_Header3.setValue(DbsUtil.compress(pnd_Settl_System, pnd_Header3, pnd_Header2_Tmp, pnd_Csr_Actvty, "PAYMENT REGISTER"));                               //Natural: COMPRESS #SETTL-SYSTEM #HEADER3 #HEADER2-TMP #CSR-ACTVTY 'PAYMENT REGISTER' INTO #HEADER3
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER-2-3
        sub_Center_Header_2_3();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(! (pnd_Summary.getBoolean())))                                                                                                                      //Natural: IF NOT #SUMMARY
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().equals("F")))                                                                                       //Natural: IF #RPT-EXT.PYMNT-FTRE-IND = 'F'
            {
                pnd_Cf_Txt.setValue("(FUTURE)");                                                                                                                          //Natural: ASSIGN #CF-TXT := '(FUTURE)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cf_Txt.setValue("(CURRENT)");                                                                                                                         //Natural: ASSIGN #CF-TXT := '(CURRENT)'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Center_Header_2_3() throws Exception                                                                                                                 //Natural: CENTER-HEADER-2-3
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Len.reset();                                                                                                                                                  //Natural: RESET #LEN #W-FLD
        pnd_W_Fld.reset();
        DbsUtil.examine(new ExamineSource(pnd_Header2), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                        //Natural: EXAMINE #HEADER2 FOR ' ' GIVING LENGTH #LEN
        pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(90,pnd_Len)).divide(2));                                                                //Natural: COMPUTE #LEN = ( 90 - #LEN ) / 2
        pnd_W_Fld.moveAll("H'00'");                                                                                                                                       //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
        pnd_Header2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header2));                                                                    //Natural: COMPRESS #W-FLD #HEADER2 INTO #HEADER2 LEAVE NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Header2,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                                  //Natural: EXAMINE FULL #HEADER2 FOR FULL H'00' REPLACE ' '
        pnd_Len.reset();                                                                                                                                                  //Natural: RESET #LEN #W-FLD
        pnd_W_Fld.reset();
        DbsUtil.examine(new ExamineSource(pnd_Header3), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                        //Natural: EXAMINE #HEADER3 FOR ' ' GIVING LENGTH #LEN
        pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(90,pnd_Len)).divide(2));                                                                //Natural: COMPUTE #LEN = ( 90 - #LEN ) / 2
        pnd_W_Fld.moveAll("H'00'");                                                                                                                                       //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
        pnd_Header3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header3));                                                                    //Natural: COMPRESS #W-FLD #HEADER3 INTO #HEADER3 LEAVE NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Header3,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                                  //Natural: EXAMINE FULL #HEADER3 FOR FULL H'00' REPLACE ' '
    }
    private void sub_Center_Sub_Header() throws Exception                                                                                                                 //Natural: CENTER-SUB-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Len.reset();                                                                                                                                                  //Natural: RESET #LEN #W-FLD
        pnd_W_Fld.reset();
        //*  05-18-98
        if (condition(! (pnd_Summary.getBoolean())))                                                                                                                      //Natural: IF NOT #SUMMARY
        {
            pnd_Header4.setValue(pnd_Sub_Txt);                                                                                                                            //Natural: ASSIGN #HEADER4 := #SUB-TXT
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Header4), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                        //Natural: EXAMINE #HEADER4 FOR ' ' GIVING LENGTH #LEN
        pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(60,pnd_Len)).divide(2));                                                                //Natural: ASSIGN #LEN := ( 60 - #LEN ) / 2
        pnd_W_Fld.moveAll("H'00'");                                                                                                                                       //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
        pnd_Header4.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header4));                                                                    //Natural: COMPRESS #W-FLD #HEADER4 INTO #HEADER4 LEAVE NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Header4,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                                  //Natural: EXAMINE FULL #HEADER4 FOR FULL H'00' REPLACE ' '
    }
    private void sub_Build_Detail_Lines() throws Exception                                                                                                                //Natural: BUILD-DETAIL-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  04-21-99: A. YOUNG
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().notEquals("DC")) && (ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions().notEquals(getZero()))))         //Natural: IF ( #RPT-EXT.CNTRCT-ORGN-CDE NE 'DC' ) AND ( #RPT-EXT.#CNTR-DEDUCTIONS NE 0 )
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 #RPT-EXT.#CNTR-DEDUCTIONS
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions())); pnd_I.nadd(1))
            {
                pnd_Ln1.nadd(1);                                                                                                                                          //Natural: ASSIGN #LN1 := #LN1 + 1
                pnd_Dtl_Lines_Pnd_Ded_Exp.getValue(pnd_Ln1).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(pnd_I));                                         //Natural: ASSIGN #DTL-LINES.#DED-EXP ( #LN1 ) := #RPT-EXT.PYMNT-DED-AMT ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO #RPT-EXT.C-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); pnd_I.nadd(1))
        {
            pnd_Ln.nadd(1);                                                                                                                                               //Natural: ASSIGN #LN := #LN + 1
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Cde().getValue(pnd_I));                                         //Natural: ASSIGN #INV-ACCT-INPUT := #RPT-EXT.INV-ACCT-CDE ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Valuat_Period().getValue(pnd_I));                       //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := #RPT-EXT.INV-ACCT-VALUAT-PERIOD ( #I )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' #FUND-PDA
            if (condition(Global.isEscape())) return;
            pnd_Dtl_Lines_Pnd_Inv_Acct.getValue(pnd_Ln).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #INV-ACCT-DESC-8 TO #DTL-LINES.#INV-ACCT ( #LN )
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") && ! (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean())))                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' AND NOT #FUND-PDA.#DIVIDENDS
            {
                pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_Ln).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_3());                                      //Natural: ASSIGN #DTL-LINES.#INV-ACCT-VALUAT-PERIOD ( #LN ) := #FUND-PDA.#VALUAT-DESC-3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_Ln).reset();                                                                                        //Natural: RESET #DTL-LINES.#INV-ACCT-VALUAT-PERIOD ( #LN )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dtl_Lines_Pnd_Dividends.getValue(pnd_Ln).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean());                                              //Natural: ASSIGN #DTL-LINES.#DIVIDENDS ( #LN ) := #FUND-PDA.#DIVIDENDS
            pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_Ln).compute(new ComputeParameters(false, pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_Ln)), ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I))); //Natural: ASSIGN #DTL-LINES.#GROSS-AMT ( #LN ) := #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) + #RPT-EXT.INV-ACCT-DVDND-AMT ( #I )
            pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                        //Natural: ASSIGN #DTL-LINES.#INV-ACCT-FDRL-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
            pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                      //Natural: ASSIGN #DTL-LINES.#INV-ACCT-STATE-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I )
            pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                      //Natural: ASSIGN #DTL-LINES.#INV-ACCT-LOCAL-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
            pnd_Dtl_Lines_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_Ln).compute(new ComputeParameters(false, pnd_Dtl_Lines_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_Ln)),                //Natural: ASSIGN #DTL-LINES.#INV-ACCT-DPI-AMT ( #LN ) := #RPT-EXT.INV-ACCT-DCI-AMT ( #I ) + #RPT-EXT.INV-ACCT-DPI-AMT ( #I )
                ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dci_Amt().getValue(pnd_I).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I)));
            pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                      //Natural: ASSIGN #DTL-LINES.#INV-ACCT-NET-PYMNT-AMT ( #LN ) := #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
            //*  04-21-99
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC")) && (ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions().notEquals(getZero()))))        //Natural: IF ( #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' ) AND ( #RPT-EXT.#CNTR-DEDUCTIONS NE 0 )
            {
                pnd_Dtl_Lines_Pnd_Ovrpymnt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt().getValue(pnd_I));                                   //Natural: ASSIGN #DTL-LINES.#OVRPYMNT ( #LN ) := #RPT-EXT.##INV-DED-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Dtl_Lines_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                          //Natural: ASSIGN #DTL-LINES.#INV-ACCT-SETTL-AMT ( #LN ) := #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
                pnd_Dtl_Lines_Pnd_Inv_Acct_Dvdnd_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                          //Natural: ASSIGN #DTL-LINES.#INV-ACCT-DVDND-AMT ( #LN ) := #RPT-EXT.INV-ACCT-DVDND-AMT ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Qty.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Qty().getValue(pnd_I));                            //Natural: ASSIGN #DTL-LINES.#INV-ACCT-UNIT-QTY ( #LN ) := #RPT-EXT.INV-ACCT-UNIT-QTY ( #I )
                pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Value.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Value().getValue(pnd_I));                        //Natural: ASSIGN #DTL-LINES.#INV-ACCT-UNIT-VALUE ( #LN ) := #RPT-EXT.INV-ACCT-UNIT-VALUE ( #I )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dtl_Lines_Pnd_Ded_Exp.getValue(pnd_Ln).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I));                                               //Natural: ASSIGN #DTL-LINES.#DED-EXP ( #LN ) := #DTL-LINES.#DED-EXP ( #LN ) + #RPT-EXT.INV-ACCT-EXP-AMT ( #I )
            if (condition(pnd_Ln.equals(1)))                                                                                                                              //Natural: IF #LN = 1
            {
                                                                                                                                                                          //Natural: PERFORM GET-MISCELLANEOUS-DATA
                sub_Get_Miscellaneous_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BUILD-SUMMARY-DATA
            sub_Build_Summary_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Ln1.greater(pnd_Ln)))                                                                                                                           //Natural: IF #LN1 > #LN
        {
            pnd_Ln.setValue(pnd_Ln1);                                                                                                                                     //Natural: ASSIGN #LN := #LN1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Miscellaneous_Data() throws Exception                                                                                                            //Natural: GET-MISCELLANEOUS-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Misc.setValuesByName(ldaFcpl190a.getPnd_Rpt_Ext());                                                                                                           //Natural: MOVE BY NAME #RPT-EXT TO #MISC
        //* ********************* RL PAYEE MATCH BEGIN MAY 26, 2006 ***************
        pnd_Check_Datex.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
        pnd_Check_Number_N10_Pnd_Check_Number_A10.reset();                                                                                                                //Natural: RESET #CHECK-NUMBER-A10
        if (condition(! (ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2) || ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8))))                //Natural: IF NOT ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8 )
        {
            //* N7 CHK-NBR - MAKE N10 W PREFIX
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().less(getZero())))                                                                                  //Natural: IF #RPT-EXT.PYMNT-CHECK-NBR LT 0
            {
                pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
                pnd_Check_Number_N10_Pnd_Check_Number_N7.compute(new ComputeParameters(false, pnd_Check_Number_N10_Pnd_Check_Number_N7), ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().multiply(-1)); //Natural: COMPUTE #CHECK-NUMBER-N7 = #RPT-EXT.PYMNT-CHECK-NBR * -1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  VALID OLD N7 CHECK-NBR
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().greater(getZero())))                                                                           //Natural: IF #RPT-EXT.PYMNT-CHECK-NBR GT 0
                {
                    pnd_Check_Number_N10_Pnd_Check_Number_N7.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr());                                                      //Natural: ASSIGN #CHECK-NUMBER-N7 := #RPT-EXT.PYMNT-CHECK-NBR
                    pnd_Check_Number_N10_Pnd_Check_Number_N3.reset();                                                                                                     //Natural: RESET #CHECK-NUMBER-N3
                    if (condition(pnd_Check_Datex_Pnd_Check_Date.greater(20060430)))                                                                                      //Natural: IF #CHECK-DATE GT 20060430
                    {
                        pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                 //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Check_Number_N10_Pnd_Check_Number_N3.reset();                                                                                                 //Natural: RESET #CHECK-NUMBER-N3
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Check_Number_N10_Pnd_Check_Number_A10.reset();                                                                                                    //Natural: RESET #CHECK-NUMBER-A10
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Check_Number_N10_Pnd_Check_Number_A10.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr());                                                       //Natural: ASSIGN #CHECK-NUMBER-A10 = #RPT-EXT.PYMNT-CHECK-SCRTY-NBR
        }                                                                                                                                                                 //Natural: END-IF
        //*    IF NOT (#RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8)
        //*      RESET #CHECK-NUMBER-A10
        //*      IF #RPT-EXT.PYMNT-CHECK-NBR GT 0
        //*        #CHECK-NUMBER-N3  :=  FCPA110.START-CHECK-PREFIX-N3
        //*      END-IF
        //*      IF #RPT-EXT.PYMNT-CHECK-NBR LT 0
        //*        COMPUTE #CHECK-NUMBER-N7 =  #RPT-EXT.PYMNT-CHECK-NBR * -1
        //*      ELSE
        //*        IF #RPT-EXT.PYMNT-CHECK-NBR GT 0
        //*          #CHECK-NUMBER-N3  :=  FCPA110.START-CHECK-PREFIX-N3
        //*          #CHECK-NUMBER-N7 :=  #RPT-EXT.PYMNT-CHECK-NBR
        //*        END-IF
        //*      END-IF
        //*    ELSE
        //*      ASSIGN #CHECK-NUMBER-A10 = #RPT-EXT.PYMNT-CHECK-SCRTY-NBR
        //*    END-IF
        //* *********************** RL PAYEE MATCH END ****************************
        pnd_Misc_Pnd_Annt.setValue(DbsUtil.compress(ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name(), ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name())); //Natural: COMPRESS #RPT-EXT.PH-FIRST-NAME #RPT-EXT.PH-MIDDLE-NAME #RPT-EXT.PH-LAST-NAME INTO #MISC.#ANNT
        if (condition(DbsUtil.maskMatches(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1),"'CR '")))                                                                   //Natural: IF #RPT-EXT.PYMNT-NME ( 1 ) = MASK ( 'CR ' )
        {
            pnd_Misc_Pnd_Payee.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(1));                                                                   //Natural: ASSIGN #MISC.#PAYEE := #RPT-EXT.PYMNT-ADDR-LINE1-TXT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Misc_Pnd_Payee.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1));                                                                              //Natural: ASSIGN #MISC.#PAYEE := #RPT-EXT.PYMNT-NME ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))                                                                                         //Natural: IF #FUND-PDA.#COMPANY-CDE = 'T'
        {
            pnd_Tiaa.setValue(true);                                                                                                                                      //Natural: ASSIGN #TIAA := TRUE
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(1).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 1 ) := #RPT-EXT.CNTRCT-DA-TIAA-1-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(2).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_2_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 2 ) := #RPT-EXT.CNTRCT-DA-TIAA-2-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(3).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_3_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 3 ) := #RPT-EXT.CNTRCT-DA-TIAA-3-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(4).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_4_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 4 ) := #RPT-EXT.CNTRCT-DA-TIAA-4-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(5).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_5_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 5 ) := #RPT-EXT.CNTRCT-DA-TIAA-5-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tiaa.reset();                                                                                                                                             //Natural: RESET #TIAA
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(1).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_1_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 1 ) := #RPT-EXT.CNTRCT-DA-CREF-1-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(2).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_2_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 2 ) := #RPT-EXT.CNTRCT-DA-CREF-2-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(3).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_3_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 3 ) := #RPT-EXT.CNTRCT-DA-CREF-3-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(4).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_4_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 4 ) := #RPT-EXT.CNTRCT-DA-CREF-4-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(5).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_5_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 5 ) := #RPT-EXT.CNTRCT-DA-CREF-5-NBR
        }                                                                                                                                                                 //Natural: END-IF
        //*  RL WAS (A9)
        short decideConditionsMet1011 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet1011++;
            pnd_Misc_Pnd_Cge_Txt.setValue(" CHECK:");                                                                                                                     //Natural: ASSIGN #CGE-TXT := ' CHECK:'
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet1011++;
            pnd_Misc_Pnd_Cge_Txt.setValue("   EFT:");                                                                                                                     //Natural: ASSIGN #CGE-TXT := '   EFT:'
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(3))))
        {
            decideConditionsMet1011++;
            pnd_Misc_Pnd_Cge_Txt.setValue("GLOBAL:");                                                                                                                     //Natural: ASSIGN #CGE-TXT := 'GLOBAL:'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Misc_Pnd_Cge_Txt.reset();                                                                                                                                 //Natural: RESET #CGE-TXT
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().equals("N") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("X")))                //Natural: IF #RPT-EXT.CNTRCT-PYMNT-TYPE-IND = 'N' AND #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND = 'X'
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("20")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = '20'
            {
                pnd_Misc_Due_Date.setValue("TTB");                                                                                                                        //Natural: ASSIGN #MISC.DUE-DATE := 'TTB'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Due_Date.setValue("RTB");                                                                                                                        //Natural: ASSIGN #MISC.DUE-DATE := 'RTB'
                pnd_No_Guar_Stck.setValue(true);                                                                                                                          //Natural: ASSIGN #NO-GUAR-STCK := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Misc_Due_Date.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YY"));                                             //Natural: MOVE EDITED #RPT-EXT.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) TO #MISC.DUE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Type_Sub_Pnd_Qty.nadd(1);                                                                                                                                     //Natural: ASSIGN #TYPE-SUB.#QTY := #TYPE-SUB.#QTY + 1
        if (condition(! (pnd_Month_End.getBoolean())))                                                                                                                    //Natural: IF NOT #MONTH-END
        {
            pnd_Dte_Pnd_Qty.nadd(1);                                                                                                                                      //Natural: ASSIGN #DTE.#QTY := #DTE.#QTY + 1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calculate_Item_Sub_Totals() throws Exception                                                                                                         //Natural: CALCULATE-ITEM-SUB-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Item_Pnd_Gross_Amt.nadd(pnd_Dtl_Lines_Pnd_Gross_Amt.getValue("*"));                                                                                           //Natural: ASSIGN #ITEM.#GROSS-AMT := #ITEM.#GROSS-AMT + #DTL-LINES.#GROSS-AMT ( * )
        pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue("*"));                                                                   //Natural: ASSIGN #ITEM.#INV-ACCT-FDRL-TAX-AMT := #ITEM.#INV-ACCT-FDRL-TAX-AMT + #DTL-LINES.#INV-ACCT-FDRL-TAX-AMT ( * )
        pnd_Item_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt.getValue("*"));                                                                 //Natural: ASSIGN #ITEM.#INV-ACCT-STATE-TAX-AMT := #ITEM.#INV-ACCT-STATE-TAX-AMT + #DTL-LINES.#INV-ACCT-STATE-TAX-AMT ( * )
        pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt.getValue("*"));                                                                 //Natural: ASSIGN #ITEM.#INV-ACCT-LOCAL-TAX-AMT := #ITEM.#INV-ACCT-LOCAL-TAX-AMT + #DTL-LINES.#INV-ACCT-LOCAL-TAX-AMT ( * )
        pnd_Item_Pnd_Ded_Exp.nadd(pnd_Dtl_Lines_Pnd_Ded_Exp.getValue("*"));                                                                                               //Natural: ASSIGN #ITEM.#DED-EXP := #ITEM.#DED-EXP + #DTL-LINES.#DED-EXP ( * )
        pnd_Item_Pnd_Ovrpymnt.nadd(pnd_Dtl_Lines_Pnd_Ovrpymnt.getValue("*"));                                                                                             //Natural: ASSIGN #ITEM.#OVRPYMNT := #ITEM.#OVRPYMNT + #DTL-LINES.#OVRPYMNT ( * )
        pnd_Item_Pnd_Inv_Acct_Dpi_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_Dpi_Amt.getValue("*"));                                                                             //Natural: ASSIGN #ITEM.#INV-ACCT-DPI-AMT := #ITEM.#INV-ACCT-DPI-AMT + #DTL-LINES.#INV-ACCT-DPI-AMT ( * )
        pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue("*"));                                                                 //Natural: ASSIGN #ITEM.#INV-ACCT-NET-PYMNT-AMT := #ITEM.#INV-ACCT-NET-PYMNT-AMT + #DTL-LINES.#INV-ACCT-NET-PYMNT-AMT ( * )
        if (condition(! (pnd_Month_End.getBoolean())))                                                                                                                    //Natural: IF NOT #MONTH-END
        {
            pnd_Dte_Pnd_Gross_Amt.nadd(pnd_Item_Pnd_Gross_Amt);                                                                                                           //Natural: ASSIGN #DTE.#GROSS-AMT := #DTE.#GROSS-AMT + #ITEM.#GROSS-AMT
            pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt);                                                                                   //Natural: ASSIGN #DTE.#INV-ACCT-FDRL-TAX-AMT := #DTE.#INV-ACCT-FDRL-TAX-AMT + #ITEM.#INV-ACCT-FDRL-TAX-AMT
            pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_State_Tax_Amt);                                                                                 //Natural: ASSIGN #DTE.#INV-ACCT-STATE-TAX-AMT := #DTE.#INV-ACCT-STATE-TAX-AMT + #ITEM.#INV-ACCT-STATE-TAX-AMT
            pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt);                                                                                 //Natural: ASSIGN #DTE.#INV-ACCT-LOCAL-TAX-AMT := #DTE.#INV-ACCT-LOCAL-TAX-AMT + #ITEM.#INV-ACCT-LOCAL-TAX-AMT
            pnd_Dte_Pnd_Ded_Exp.nadd(pnd_Item_Pnd_Ded_Exp);                                                                                                               //Natural: ASSIGN #DTE.#DED-EXP := #DTE.#DED-EXP + #ITEM.#DED-EXP
            pnd_Dte_Pnd_Ovrpymnt.nadd(pnd_Item_Pnd_Ovrpymnt);                                                                                                             //Natural: ASSIGN #DTE.#OVRPYMNT := #DTE.#OVRPYMNT + #ITEM.#OVRPYMNT
            pnd_Dte_Pnd_Inv_Acct_Dpi_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Dpi_Amt);                                                                                             //Natural: ASSIGN #DTE.#INV-ACCT-DPI-AMT := #DTE.#INV-ACCT-DPI-AMT + #ITEM.#INV-ACCT-DPI-AMT
            pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt);                                                                                 //Natural: ASSIGN #DTE.#INV-ACCT-NET-PYMNT-AMT := #DTE.#INV-ACCT-NET-PYMNT-AMT + #ITEM.#INV-ACCT-NET-PYMNT-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Type_Sub_Pnd_Gross_Amt.nadd(pnd_Item_Pnd_Gross_Amt);                                                                                                          //Natural: ASSIGN #TYPE-SUB.#GROSS-AMT := #TYPE-SUB.#GROSS-AMT + #ITEM.#GROSS-AMT
        pnd_Type_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt);                                                                                  //Natural: ASSIGN #TYPE-SUB.#INV-ACCT-FDRL-TAX-AMT := #TYPE-SUB.#INV-ACCT-FDRL-TAX-AMT + #ITEM.#INV-ACCT-FDRL-TAX-AMT
        pnd_Type_Sub_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_State_Tax_Amt);                                                                                //Natural: ASSIGN #TYPE-SUB.#INV-ACCT-STATE-TAX-AMT := #TYPE-SUB.#INV-ACCT-STATE-TAX-AMT + #ITEM.#INV-ACCT-STATE-TAX-AMT
        pnd_Type_Sub_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt);                                                                                //Natural: ASSIGN #TYPE-SUB.#INV-ACCT-LOCAL-TAX-AMT := #TYPE-SUB.#INV-ACCT-LOCAL-TAX-AMT + #ITEM.#INV-ACCT-LOCAL-TAX-AMT
        pnd_Type_Sub_Pnd_Ded_Exp.nadd(pnd_Item_Pnd_Ded_Exp);                                                                                                              //Natural: ASSIGN #TYPE-SUB.#DED-EXP := #TYPE-SUB.#DED-EXP + #ITEM.#DED-EXP
        pnd_Type_Sub_Pnd_Ovrpymnt.nadd(pnd_Item_Pnd_Ovrpymnt);                                                                                                            //Natural: ASSIGN #TYPE-SUB.#OVRPYMNT := #TYPE-SUB.#OVRPYMNT + #ITEM.#OVRPYMNT
        pnd_Type_Sub_Pnd_Inv_Acct_Dpi_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Dpi_Amt);                                                                                            //Natural: ASSIGN #TYPE-SUB.#INV-ACCT-DPI-AMT := #TYPE-SUB.#INV-ACCT-DPI-AMT + #ITEM.#INV-ACCT-DPI-AMT
        pnd_Type_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt);                                                                                //Natural: ASSIGN #TYPE-SUB.#INV-ACCT-NET-PYMNT-AMT := #TYPE-SUB.#INV-ACCT-NET-PYMNT-AMT + #ITEM.#INV-ACCT-NET-PYMNT-AMT
    }
    private void sub_Print_Dte_Sub_Total() throws Exception                                                                                                               //Natural: PRINT-DTE-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_O_Ftre_Ind.equals("F")))                                                                                                                        //Natural: IF #O-FTRE-IND = 'F'
        {
            pnd_Dte_Txt.setValue("FUTURE SUB-TOTALS:");                                                                                                                   //Natural: MOVE 'FUTURE SUB-TOTALS:' TO #DTE-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Dte_Txt.setValue("CURRENT DATE SUB-TOTALS:");                                                                                                             //Natural: MOVE 'CURRENT DATE SUB-TOTALS:' TO #DTE-TXT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                           //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 6 LINES LEFT
        {
            getReports().newPage(0);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(2, NEWLINE,pnd_Dte_Txt,NEWLINE,NEWLINE,"GROSS:",new TabSetting(8),pnd_Dte_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new         //Natural: WRITE ( PRT1 ) / #DTE-TXT // 'GROSS:' 008T #DTE.#GROSS-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 028T 'DPI/DCI:' 037T #DTE.#INV-ACCT-DPI-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 057T 'FED:' 062T #DTE.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 082T 'ST:' 086T #DTE.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 106T 'LCL:' 111T #DTE.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) / 'DED:' 008T #DTE.#DED-EXP ( EM = ZZZZZZZZZZZZZZ9.99 ) 028T 'OVER:' 037T #DTE.#OVRPYMNT ( EM = ZZZZZZZZZZZZZZ9.99 ) 057T 'NET:' 062T #DTE.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 082T 'NUMBER OF CHECKS:' #DTE.#QTY ( EM = ZZZZZZZZ9 )
            TabSetting(28),"DPI/DCI:",new TabSetting(37),pnd_Dte_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(57),"FED:",new 
            TabSetting(62),pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(82),"ST:",new TabSetting(86),pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt, 
            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(106),"LCL:",new TabSetting(111),pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt, new ReportEditMask 
            ("ZZZZZZZZZZZZZZ9.99"),NEWLINE,"DED:",new TabSetting(8),pnd_Dte_Pnd_Ded_Exp, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(28),"OVER:",new 
            TabSetting(37),pnd_Dte_Pnd_Ovrpymnt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(57),"NET:",new TabSetting(62),pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt, 
            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(82),"NUMBER OF CHECKS:",pnd_Dte_Pnd_Qty, new ReportEditMask ("ZZZZZZZZ9"));
        if (Global.isEscape()) return;
        pnd_Dte.reset();                                                                                                                                                  //Natural: RESET #DTE
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRT1 )
        if (condition(Global.isEscape())){return;}
        pnd_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr());                                                                               //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := #RPT-EXT.PYMNT-PRCSS-SEQ-NBR
        pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                                              //Natural: ASSIGN #PREV-SEQ-NBR := #CURR-SEQ-NBR
    }
    private void sub_Print_Type_Sub_Total() throws Exception                                                                                                              //Natural: PRINT-TYPE-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Type_Sub_Txt.setValue(DbsUtil.compress(pnd_Sub_Txt, "TOTALS"));                                                                                               //Natural: COMPRESS #SUB-TXT 'TOTALS' INTO #TYPE-SUB-TXT
        if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                           //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 6 LINES LEFT
        {
            getReports().newPage(0);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(2, NEWLINE,pnd_Type_Sub_Txt,NEWLINE,NEWLINE,"GROSS:",new TabSetting(8),pnd_Type_Sub_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new  //Natural: WRITE ( PRT1 ) / #TYPE-SUB-TXT // 'GROSS:' 008T #TYPE-SUB.#GROSS-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 028T 'DPI/DCI:' 037T #TYPE-SUB.#INV-ACCT-DPI-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 057T 'FED:' 062T #TYPE-SUB.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 082T 'ST:' 086T #TYPE-SUB.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 106T 'LCL:' 111T #TYPE-SUB.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) / 'DED:' 008T #TYPE-SUB.#DED-EXP ( EM = ZZZZZZZZZZZZZZ9.99 ) 028T 'OVER:' 037T #TYPE-SUB.#OVRPYMNT ( EM = ZZZZZZZZZZZZZZ9.99 ) 057T 'NET:' 062T #TYPE-SUB.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 082T 'NUMBER OF CHECKS:' #TYPE-SUB.#QTY ( EM = ZZZZZZZZ9 )
            TabSetting(28),"DPI/DCI:",new TabSetting(37),pnd_Type_Sub_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(57),"FED:",new 
            TabSetting(62),pnd_Type_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(82),"ST:",new TabSetting(86),pnd_Type_Sub_Pnd_Inv_Acct_State_Tax_Amt, 
            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(106),"LCL:",new TabSetting(111),pnd_Type_Sub_Pnd_Inv_Acct_Local_Tax_Amt, new ReportEditMask 
            ("ZZZZZZZZZZZZZZ9.99"),NEWLINE,"DED:",new TabSetting(8),pnd_Type_Sub_Pnd_Ded_Exp, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(28),"OVER:",new 
            TabSetting(37),pnd_Type_Sub_Pnd_Ovrpymnt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(57),"NET:",new TabSetting(62),pnd_Type_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt, 
            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(82),"NUMBER OF CHECKS:",pnd_Type_Sub_Pnd_Qty, new ReportEditMask ("ZZZZZZZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRT1 )
        if (condition(Global.isEscape())){return;}
        pnd_Type_Sub.reset();                                                                                                                                             //Natural: RESET #TYPE-SUB #TYPE-SUB-TXT
        pnd_Type_Sub_Txt.reset();
        pnd_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr());                                                                               //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := #RPT-EXT.PYMNT-PRCSS-SEQ-NBR
        pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                                              //Natural: ASSIGN #PREV-SEQ-NBR := #CURR-SEQ-NBR
    }
    private void sub_Print_Detail_Header() throws Exception                                                                                                               //Natural: PRINT-DETAIL-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(2, new TabSetting(4),"IA",new TabSetting(24),"SETTL AMT /",new TabSetting(41),"UNIT",new TabSetting(50),"DPI /",new TabSetting(63),"GROSS",new //Natural: WRITE ( PRT1 ) 004T 'IA' 024T 'SETTL AMT /' 041T 'UNIT' 050T 'DPI /' 063T 'GROSS' 074T 'FEDERAL' 087T 'STATE' 097T 'LOCAL' 104T 'DEDUCT/' 113T 'OVRPYMT' 130T 'NET' / 001T 'CONTRACT' 012T 'DIVIDEND' 024T 'UNIT VALUE' 038T 'QUANTITY' 050T 'DCI' 062T 'AMOUNT' 078T 'TAX' 089T 'TAX' 099T 'TAX' 104T 'EXPENSE' 114T 'DEDUCT' 126T 'PAYMENT' /
            TabSetting(74),"FEDERAL",new TabSetting(87),"STATE",new TabSetting(97),"LOCAL",new TabSetting(104),"DEDUCT/",new TabSetting(113),"OVRPYMT",new 
            TabSetting(130),"NET",NEWLINE,new TabSetting(1),"CONTRACT",new TabSetting(12),"DIVIDEND",new TabSetting(24),"UNIT VALUE",new TabSetting(38),"QUANTITY",new 
            TabSetting(50),"DCI",new TabSetting(62),"AMOUNT",new TabSetting(78),"TAX",new TabSetting(89),"TAX",new TabSetting(99),"TAX",new TabSetting(104),"EXPENSE",new 
            TabSetting(114),"DEDUCT",new TabSetting(126),"PAYMENT",NEWLINE);
        if (Global.isEscape()) return;
    }
    //*  11-09-01 AMC
    private void sub_Increment_Index() throws Exception                                                                                                                   //Natural: INCREMENT-INDEX
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ti.setValue(1);                                                                                                                                               //Natural: ASSIGN #TI := 1
        short decideConditionsMet1095 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-EXT.CNTRCT-ORGN-CDE = 'DC'
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC")))
        {
            decideConditionsMet1095++;
            short decideConditionsMet1097 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #RPT-EXT.CNTRCT-TYPE-CDE;//Natural: VALUE 'L '
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L "))))
            {
                decideConditionsMet1097++;
                pnd_Ti.setValue(1);                                                                                                                                       //Natural: ASSIGN #TI := 1
                pnd_Sub_Txt.setValue("LUMP SUM DEATH");                                                                                                                   //Natural: ASSIGN #SUB-TXT := 'LUMP SUM DEATH'
            }                                                                                                                                                             //Natural: VALUE 'PP'
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("PP"))))
            {
                decideConditionsMet1097++;
                pnd_Ti.setValue(2);                                                                                                                                       //Natural: ASSIGN #TI := 2
                pnd_Sub_Txt.setValue("SUPPLEMENTAL DEATH");                                                                                                               //Natural: ASSIGN #SUB-TXT := 'SUPPLEMENTAL DEATH'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #RPT-EXT.CNTRCT-ORGN-CDE = 'DS'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DS")))
        {
            decideConditionsMet1095++;
            short decideConditionsMet1108 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #RPT-EXT.CNTRCT-TYPE-CDE;//Natural: VALUE 'C '
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("C "))))
            {
                decideConditionsMet1108++;
                pnd_Ti.setValue(1);                                                                                                                                       //Natural: ASSIGN #TI := 1
                pnd_Sub_Txt.setValue("CASHOUT");                                                                                                                          //Natural: ASSIGN #SUB-TXT := 'CASHOUT'
            }                                                                                                                                                             //Natural: VALUE 'L '
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L "))))
            {
                decideConditionsMet1108++;
                pnd_Ti.setValue(2);                                                                                                                                       //Natural: ASSIGN #TI := 2
                pnd_Sub_Txt.setValue("LUMP SUM");                                                                                                                         //Natural: ASSIGN #SUB-TXT := 'LUMP SUM'
            }                                                                                                                                                             //Natural: VALUE 'R '
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("R "))))
            {
                decideConditionsMet1108++;
                pnd_Ti.setValue(3);                                                                                                                                       //Natural: ASSIGN #TI := 3
                pnd_Sub_Txt.setValue("REPURCHASE");                                                                                                                       //Natural: ASSIGN #SUB-TXT := 'REPURCHASE'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #RPT-EXT.CNTRCT-ORGN-CDE = 'IA'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA")))
        {
            decideConditionsMet1095++;
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("PP")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = 'PP'
            {
                pnd_Ti.setValue(1);                                                                                                                                       //Natural: ASSIGN #TI := 1
                pnd_Sub_Txt.setValue("PERIODIC PAYMENT");                                                                                                                 //Natural: ASSIGN #SUB-TXT := 'PERIODIC PAYMENT'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #RPT-EXT.CNTRCT-ORGN-CDE = 'MS'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("MS")))
        {
            decideConditionsMet1095++;
            short decideConditionsMet1127 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #RPT-EXT.CNTRCT-TYPE-CDE;//Natural: VALUE '10'
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("10"))))
            {
                decideConditionsMet1127++;
                pnd_Ti.setValue(1);                                                                                                                                       //Natural: ASSIGN #TI := 1
                pnd_Sub_Txt.setValue("MATURITY");                                                                                                                         //Natural: ASSIGN #SUB-TXT := 'MATURITY'
            }                                                                                                                                                             //Natural: VALUE '20'
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("20"))))
            {
                decideConditionsMet1127++;
                pnd_Ti.setValue(2);                                                                                                                                       //Natural: ASSIGN #TI := 2
                pnd_Sub_Txt.setValue("TPA");                                                                                                                              //Natural: ASSIGN #SUB-TXT := 'TPA'
            }                                                                                                                                                             //Natural: VALUE '30'
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30"))))
            {
                decideConditionsMet1127++;
                pnd_Ti.setValue(3);                                                                                                                                       //Natural: ASSIGN #TI := 3
                pnd_Sub_Txt.setValue("MDO INITIAL");                                                                                                                      //Natural: ASSIGN #SUB-TXT := 'MDO INITIAL'
            }                                                                                                                                                             //Natural: VALUE '31'
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31"))))
            {
                decideConditionsMet1127++;
                pnd_Ti.setValue(4);                                                                                                                                       //Natural: ASSIGN #TI := 4
                pnd_Sub_Txt.setValue("MDO RECURRING");                                                                                                                    //Natural: ASSIGN #SUB-TXT := 'MDO RECURRING'
            }                                                                                                                                                             //Natural: VALUE '40'
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("40"))))
            {
                decideConditionsMet1127++;
                pnd_Ti.setValue(5);                                                                                                                                       //Natural: ASSIGN #TI := 5
                pnd_Sub_Txt.setValue("LUMP SUM DEATH");                                                                                                                   //Natural: ASSIGN #SUB-TXT := 'LUMP SUM DEATH'
            }                                                                                                                                                             //Natural: VALUE '50'
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("50"))))
            {
                decideConditionsMet1127++;
                pnd_Ti.setValue(6);                                                                                                                                       //Natural: ASSIGN #TI := 6
                pnd_Sub_Txt.setValue("SUPPLEMENTAL DEATH");                                                                                                               //Natural: ASSIGN #SUB-TXT := 'SUPPLEMENTAL DEATH'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #RPT-EXT.CNTRCT-ORGN-CDE = 'SS'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS")))
        {
            decideConditionsMet1095++;
            short decideConditionsMet1150 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #RPT-EXT.CNTRCT-TYPE-CDE;//Natural: VALUE 'L'
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L"))))
            {
                decideConditionsMet1150++;
                pnd_Ti.setValue(1);                                                                                                                                       //Natural: ASSIGN #TI := 1
                pnd_Sub_Txt.setValue("LUMP SUM");                                                                                                                         //Natural: ASSIGN #SUB-TXT := 'LUMP SUM'
            }                                                                                                                                                             //Natural: VALUE '30'
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30"))))
            {
                decideConditionsMet1150++;
                pnd_Ti.setValue(2);                                                                                                                                       //Natural: ASSIGN #TI := 2
                pnd_Sub_Txt.setValue("MDO INITIAL");                                                                                                                      //Natural: ASSIGN #SUB-TXT := 'MDO INITIAL'
            }                                                                                                                                                             //Natural: VALUE '31'
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31"))))
            {
                decideConditionsMet1150++;
                pnd_Ti.setValue(3);                                                                                                                                       //Natural: ASSIGN #TI := 3
                pnd_Sub_Txt.setValue("MDO RECURRING");                                                                                                                    //Natural: ASSIGN #SUB-TXT := 'MDO RECURRING'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
                //*  05-18-98
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #RPT-EXT.CNTRCT-ORGN-CDE = 'AL'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AL")))
        {
            decideConditionsMet1095++;
            pnd_Ti.setValue(1);                                                                                                                                           //Natural: ASSIGN #TI := 1
            pnd_Sub_Txt.setValue("ANNUITY LOAN");                                                                                                                         //Natural: ASSIGN #SUB-TXT := 'ANNUITY LOAN'
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Detail_Lines() throws Exception                                                                                                                //Natural: PRINT-DETAIL-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLineCount(2).add(pnd_Ln).add(9).greater(Global.getPAGESIZE())))                                                                  //Natural: IF *LINE-COUNT ( PRT1 ) + #LN + 9 GT *PAGESIZE
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( PRT1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, new TabSetting(1),pnd_Misc_Cntrct_Ppcn_Nbr,pnd_Misc_Pnd_Annt);                                                                              //Natural: WRITE ( PRT1 ) 1T #MISC.CNTRCT-PPCN-NBR #MISC.#ANNT
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO #LN
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ln)); pnd_I.nadd(1))
        {
            if (condition(pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_I).greater(getZero())))                                                                                //Natural: IF #DTL-LINES.#GROSS-AMT ( #I ) GT 0
            {
                if (condition(pnd_Dtl_Lines_Pnd_Dividends.getValue(pnd_I).getBoolean()))                                                                                  //Natural: IF #DTL-LINES.#DIVIDENDS ( #I )
                {
                    getReports().write(2, new TabSetting(1),pnd_Dtl_Lines_Pnd_Inv_Acct.getValue(pnd_I),new TabSetting(10),pnd_Dtl_Lines_Pnd_Inv_Acct_Dvdnd_Amt.getValue(pnd_I),  //Natural: WRITE ( PRT1 ) 001T #DTL-LINES.#INV-ACCT ( #I ) 010T #DTL-LINES.#INV-ACCT-DVDND-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 021T #DTL-LINES.#INV-ACCT-SETTL-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 047T #DTL-LINES.#INV-ACCT-DPI-AMT ( #I ) ( EM = Z,ZZ9.99 ) 056T #DTL-LINES.#GROSS-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 069T #DTL-LINES.#INV-ACCT-FDRL-TAX-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 082T #DTL-LINES.#INV-ACCT-STATE-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 093T #DTL-LINES.#INV-ACCT-LOCAL-TAX-AMT ( #I ) ( EM = ZZ,ZZ9.99 ) 103T #DTL-LINES.#DED-EXP ( #I ) ( EM = Z,ZZ9.99 ) 112T #DTL-LINES.#OVRPYMNT ( #I ) ( EM = Z,ZZ9.99 ) 121T #DTL-LINES.#INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 )
                        new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(21),pnd_Dtl_Lines_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new 
                        TabSetting(47),pnd_Dtl_Lines_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_I), new ReportEditMask ("Z,ZZ9.99"),new TabSetting(56),pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_I), 
                        new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(69),pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_I), new ReportEditMask 
                        ("Z,ZZZ,ZZ9.99"),new TabSetting(82),pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZ9.99"),new 
                        TabSetting(93),pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZ9.99"),new TabSetting(103),pnd_Dtl_Lines_Pnd_Ded_Exp.getValue(pnd_I), 
                        new ReportEditMask ("Z,ZZ9.99"),new TabSetting(112),pnd_Dtl_Lines_Pnd_Ovrpymnt.getValue(pnd_I), new ReportEditMask ("Z,ZZ9.99"),new 
                        TabSetting(121),pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(2, new TabSetting(1),pnd_Dtl_Lines_Pnd_Inv_Acct.getValue(pnd_I),pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_I),new       //Natural: WRITE ( PRT1 ) 001T #DTL-LINES.#INV-ACCT ( #I ) #DTL-LINES.#INV-ACCT-VALUAT-PERIOD ( #I ) 022T #DTL-LINES.#INV-ACCT-UNIT-VALUE ( #I ) ( EM = ZZZ,ZZ9.9999 ) 035T #DTL-LINES.#INV-ACCT-UNIT-QTY ( #I ) ( EM = ZZZ,ZZ9.999 ) 047T #DTL-LINES.#INV-ACCT-DPI-AMT ( #I ) ( EM = Z,ZZ9.99 ) 056T #DTL-LINES.#GROSS-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 069T #DTL-LINES.#INV-ACCT-FDRL-TAX-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 082T #DTL-LINES.#INV-ACCT-STATE-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 093T #DTL-LINES.#INV-ACCT-LOCAL-TAX-AMT ( #I ) ( EM = ZZ,ZZ9.99 ) 103T #DTL-LINES.#DED-EXP ( #I ) ( EM = Z,ZZ9.99 ) 112T #DTL-LINES.#OVRPYMNT ( #I ) ( EM = Z,ZZ9.99 ) 121T #DTL-LINES.#INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 )
                        TabSetting(22),pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Value.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZ9.9999"),new TabSetting(35),pnd_Dtl_Lines_Pnd_Inv_Acct_Unit_Qty.getValue(pnd_I), 
                        new ReportEditMask ("ZZZ,ZZ9.999"),new TabSetting(47),pnd_Dtl_Lines_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_I), new ReportEditMask ("Z,ZZ9.99"),new 
                        TabSetting(56),pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(69),pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_I), 
                        new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(82),pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_I), new ReportEditMask 
                        ("ZZZ,ZZ9.99"),new TabSetting(93),pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZ9.99"),new 
                        TabSetting(103),pnd_Dtl_Lines_Pnd_Ded_Exp.getValue(pnd_I), new ReportEditMask ("Z,ZZ9.99"),new TabSetting(112),pnd_Dtl_Lines_Pnd_Ovrpymnt.getValue(pnd_I), 
                        new ReportEditMask ("Z,ZZ9.99"),new TabSetting(121),pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_I), new ReportEditMask 
                        ("Z,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Dtl_Lines_Pnd_Ded_Exp.getValue(pnd_I).notEquals(new DbsDecimal("0.00")) || pnd_Dtl_Lines_Pnd_Ovrpymnt.getValue(pnd_I).notEquals(new     //Natural: IF #DTL-LINES.#DED-EXP ( #I ) NE 0.00 OR #DTL-LINES.#OVRPYMNT ( #I ) NE 0.00
                    DbsDecimal("0.00"))))
                {
                    getReports().write(2, new TabSetting(103),pnd_Dtl_Lines_Pnd_Ded_Exp.getValue(pnd_I), new ReportEditMask ("Z,ZZ9.99"),new TabSetting(112),pnd_Dtl_Lines_Pnd_Ovrpymnt.getValue(pnd_I),  //Natural: WRITE ( PRT1 ) 103T #DTL-LINES.#DED-EXP ( #I ) ( EM = Z,ZZ9.99 ) 112T #DTL-LINES.#OVRPYMNT ( #I ) ( EM = Z,ZZ9.99 )
                        new ReportEditMask ("Z,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                               //Natural: WRITE ( PRT1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
        if (Global.isEscape()) return;
        getReports().write(2, "CROSS REF:",pnd_Misc_Pymnt_Check_Seq_Nbr, new ReportEditMask ("9999999"),new ColumnSpacing(2));                                            //Natural: WRITE ( PRT1 ) 'CROSS REF:' #MISC.PYMNT-CHECK-SEQ-NBR ( EM = 9999999 ) 2X
        if (Global.isEscape()) return;
        //*  05-18-98
        if (condition(Global.getINIT_USER().equals("P1460CPD")))                                                                                                          //Natural: IF *INIT-USER = 'P1460CPD'
        {
            getReports().write(2, "CHECK DATE: ",pnd_Misc_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(2));                                         //Natural: WRITE ( PRT1 ) 'CHECK DATE: ' #MISC.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) 2X
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, "ACCNTG DATE:",pnd_Misc_Pymnt_Acctg_Dte, new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(2));                                         //Natural: WRITE ( PRT1 ) 'ACCNTG DATE:' #MISC.PYMNT-ACCTG-DTE ( EM = MM/DD/YY ) 2X
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE
        if (condition(pnd_Misc_Pymnt_Check_Nbr.greater(getZero())))                                                                                                       //Natural: IF #MISC.PYMNT-CHECK-NBR GT 0
        {
            pnd_Check_Number_N10_Pnd_Check_Number_N7.setValue(pnd_Misc_Pymnt_Check_Nbr);                                                                                  //Natural: ASSIGN #CHECK-NUMBER-N7 := #MISC.PYMNT-CHECK-NBR
            pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                             //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
            //*  RL PAYEE MATCH
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  RL PAYEE MATCH
            pnd_Check_Number_N10.reset();                                                                                                                                 //Natural: RESET #CHECK-NUMBER-N10
            //*  RL PAYEE MATCH
            //*  05-18-98
            //*  RL PAYEE MATCH
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, pnd_Misc_Pnd_Cge_Txt,pnd_Check_Number_N10,new ColumnSpacing(1),"PAYEE:",pnd_Misc_Pnd_Payee,new ColumnSpacing(2),"HOLD CODE:",pnd_Misc_Cntrct_Hold_Cde,NEWLINE,"CSR STATUS:",pnd_Misc_Cntrct_Cancel_Rdrw_Actvty_Cde,new  //Natural: WRITE ( PRT1 ) #CGE-TXT #CHECK-NUMBER-N10 1X 'PAYEE:' #MISC.#PAYEE 2X 'HOLD CODE:' #MISC.CNTRCT-HOLD-CDE / 'CSR STATUS:' #MISC.CNTRCT-CANCEL-RDRW-ACTVTY-CDE 9X 'DUE DATE:' #MISC.DUE-DATE / 047T #ITEM.#INV-ACCT-DPI-AMT ( EM = ZZ,ZZ9.99 ) 056T #ITEM.#GROSS-AMT ( EM = Z,ZZZ,ZZ9.99 ) 071T #ITEM.#INV-ACCT-FDRL-TAX-AMT ( EM = Z,ZZZ,ZZ9.99 ) 082T #ITEM.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZ,ZZ9.99 ) 093T #ITEM.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZ,ZZ9.99 ) 103T #ITEM.#DED-EXP ( EM = Z,ZZ9.99 ) 112T #ITEM.#OVRPYMNT ( EM = Z,ZZ9.99 ) 121T #ITEM.#INV-ACCT-NET-PYMNT-AMT ( EM = Z,ZZZ,ZZ9.99 ) / '-' ( 132 )
            ColumnSpacing(9),"DUE DATE:",pnd_Misc_Due_Date,NEWLINE,new TabSetting(47),pnd_Item_Pnd_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZ,ZZ9.99"),new 
            TabSetting(56),pnd_Item_Pnd_Gross_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(71),pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask 
            ("Z,ZZZ,ZZ9.99"),new TabSetting(82),pnd_Item_Pnd_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(93),pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt, 
            new ReportEditMask ("ZZ,ZZ9.99"),new TabSetting(103),pnd_Item_Pnd_Ded_Exp, new ReportEditMask ("Z,ZZ9.99"),new TabSetting(112),pnd_Item_Pnd_Ovrpymnt, 
            new ReportEditMask ("Z,ZZ9.99"),new TabSetting(121),pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),NEWLINE,"-",new 
            RepeatItem(132));
        if (Global.isEscape()) return;
        //*  05-18-98
        if (condition(! (pnd_Printed_1.getBoolean())))                                                                                                                    //Natural: IF NOT #PRINTED-1
        {
            pnd_Printed_1.setValue(true);                                                                                                                                 //Natural: ASSIGN #PRINTED-1 := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dtl_Lines.getValue("*").reset();                                                                                                                              //Natural: RESET #DTL-LINES ( * ) #MISC #ITEM #MISC.#CNTRCT-NBR ( * ) #LN #NO-GUAR-STCK
        pnd_Misc.reset();
        pnd_Item.reset();
        pnd_Misc_Pnd_Cntrct_Nbr.getValue("*").reset();
        pnd_Ln.reset();
        pnd_No_Guar_Stck.reset();
    }
    private void sub_Build_Summary_Data() throws Exception                                                                                                                //Natural: BUILD-SUMMARY-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().greaterOrEqual(1) && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().lessOrEqual(3)))       //Natural: IF ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 1 THRU 3 )
        {
            pnd_Pi.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind());                                                                                         //Natural: ASSIGN #PI := #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().equals("F")))                                                                                       //Natural: IF #RPT-EXT.PYMNT-FTRE-IND = 'F'
            {
                pnd_Sum_Pnd_Future.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                        //Natural: ASSIGN #SUM.#FUTURE ( #TI,#$I,#PI ) := #SUM.#FUTURE ( #TI,#$I,#PI ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                pnd_G_Total_Pnd_Future.getValue(pnd_Ti,pnd_Pi).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                 //Natural: ASSIGN #G-TOTAL.#FUTURE ( #TI,#PI ) := #G-TOTAL.#FUTURE ( #TI,#PI ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                if (condition(pnd_Ln.equals(1)))                                                                                                                          //Natural: IF #LN = 1
                {
                    pnd_Sum_Pnd_F_Qty.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi).nadd(1);                                                                                       //Natural: ASSIGN #SUM.#F-QTY ( #TI,#$I,#PI ) := #SUM.#F-QTY ( #TI,#$I,#PI ) + 1
                    pnd_G_Total_Pnd_F_Qty.getValue(pnd_Ti,pnd_Pi).nadd(1);                                                                                                //Natural: ASSIGN #G-TOTAL.#F-QTY ( #TI,#PI ) := #G-TOTAL.#F-QTY ( #TI,#PI ) + 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Sum_Pnd_Current.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                       //Natural: ASSIGN #SUM.#CURRENT ( #TI,#$I,#PI ) := #SUM.#CURRENT ( #TI,#$I,#PI ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                pnd_G_Total_Pnd_Current.getValue(pnd_Ti,pnd_Pi).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                //Natural: ASSIGN #G-TOTAL.#CURRENT ( #TI,#PI ) := #G-TOTAL.#CURRENT ( #TI,#PI ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                if (condition(pnd_Ln.equals(1)))                                                                                                                          //Natural: IF #LN = 1
                {
                    pnd_Sum_Pnd_C_Qty.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi).nadd(1);                                                                                       //Natural: ASSIGN #SUM.#C-QTY ( #TI,#$I,#PI ) := #SUM.#C-QTY ( #TI,#$I,#PI ) + 1
                    pnd_G_Total_Pnd_C_Qty.getValue(pnd_Ti,pnd_Pi).nadd(1);                                                                                                //Natural: ASSIGN #G-TOTAL.#C-QTY ( #TI,#PI ) := #G-TOTAL.#C-QTY ( #TI,#PI ) + 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Summary() throws Exception                                                                                                                     //Natural: PRINT-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  05-18-98
        //*  05-18-98
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
        sub_Initialize_Header();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Start_Index.setValue(1);                                                                                                                                      //Natural: ASSIGN #START-INDEX := 1
        pnd_End_Index.setValue(3);                                                                                                                                        //Natural: ASSIGN #END-INDEX := 3
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRT1 )
        if (condition(Global.isEscape())){return;}
        FOR05:                                                                                                                                                            //Natural: FOR #PI #START-INDEX TO #END-INDEX
        for (pnd_Pi.setValue(pnd_Start_Index); condition(pnd_Pi.lessOrEqual(pnd_End_Index)); pnd_Pi.nadd(1))
        {
            short decideConditionsMet1256 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #PI;//Natural: VALUE 1
            if (condition((pnd_Pi.equals(1))))
            {
                decideConditionsMet1256++;
                pnd_Header4.setValue("CHECK SUMMARY TOTALS");                                                                                                             //Natural: ASSIGN #HEADER4 := 'CHECK SUMMARY TOTALS'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Pi.equals(2))))
            {
                decideConditionsMet1256++;
                pnd_Header4.setValue("EFT SUMMARY TOTALS");                                                                                                               //Natural: ASSIGN #HEADER4 := 'EFT SUMMARY TOTALS'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Pi.equals(3))))
            {
                decideConditionsMet1256++;
                pnd_Header4.setValue("GLOBAL SUMMARY TOTALS");                                                                                                            //Natural: ASSIGN #HEADER4 := 'GLOBAL SUMMARY TOTALS'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1256 > 0))
            {
                //*  05-18-98
                                                                                                                                                                          //Natural: PERFORM CENTER-SUB-HEADER
                sub_Center_Sub_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                getReports().write(2, NEWLINE,NEWLINE,new TabSetting(60),"CURRENT",new TabSetting(86),"FUTURE",new TabSetting(112),"TOTAL",NEWLINE);                      //Natural: WRITE ( PRT1 ) / / 060T 'CURRENT' 086T 'FUTURE' 112T 'TOTAL' /
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
                //*  05-18-98
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR06:                                                                                                                                                        //Natural: FOR #TI #START-INDEX TO #TI-MAX
            for (pnd_Ti.setValue(pnd_Start_Index); condition(pnd_Ti.lessOrEqual(pnd_Ti_Max)); pnd_Ti.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TXT
                sub_Initialize_Txt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_Sum_Pnd_Current.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi).add(pnd_Sum_Pnd_Future.getValue(pnd_Ti, //Natural: ASSIGN #T-TOTAL := #SUM.#CURRENT ( #TI,#$I,#PI ) + #SUM.#FUTURE ( #TI,#$I,#PI )
                    pnd_Dollar_I,pnd_Pi)));
                pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Sum_Pnd_C_Qty.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi).add(pnd_Sum_Pnd_F_Qty.getValue(pnd_Ti,    //Natural: ASSIGN #TQTY := #SUM.#C-QTY ( #TI,#$I,#PI ) + #SUM.#F-QTY ( #TI,#$I,#PI )
                    pnd_Dollar_I,pnd_Pi)));
                pnd_Us_Can_Pnd_Current.getValue(pnd_Ti,pnd_Dollar_I).nadd(pnd_Sum_Pnd_Current.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi));                                      //Natural: ASSIGN #US-CAN.#CURRENT ( #TI,#$I ) := #US-CAN.#CURRENT ( #TI,#$I ) + #SUM.#CURRENT ( #TI,#$I,#PI )
                pnd_Us_Can_Pnd_Future.getValue(pnd_Ti,pnd_Dollar_I).nadd(pnd_Sum_Pnd_Future.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi));                                        //Natural: ASSIGN #US-CAN.#FUTURE ( #TI,#$I ) := #US-CAN.#FUTURE ( #TI,#$I ) + #SUM.#FUTURE ( #TI,#$I,#PI )
                pnd_Us_Can_Pnd_C_Qty.getValue(pnd_Ti,pnd_Dollar_I).nadd(pnd_Sum_Pnd_C_Qty.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi));                                          //Natural: ASSIGN #US-CAN.#C-QTY ( #TI,#$I ) := #US-CAN.#C-QTY ( #TI,#$I ) + #SUM.#C-QTY ( #TI,#$I,#PI )
                pnd_Us_Can_Pnd_F_Qty.getValue(pnd_Ti,pnd_Dollar_I).nadd(pnd_Sum_Pnd_F_Qty.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi));                                          //Natural: ASSIGN #US-CAN.#F-QTY ( #TI,#$I ) := #US-CAN.#F-QTY ( #TI,#$I ) + #SUM.#F-QTY ( #TI,#$I,#PI )
                getReports().write(2, pnd_Sub_Txt, new AlphanumericLength (30),new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_Sum_Pnd_Current.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi),  //Natural: WRITE ( PRT1 ) #SUB-TXT ( AL = 30 ) 032T 'CHECK AMOUNT' 045T #SUM.#CURRENT ( #TI,#$I,#PI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #SUM.#FUTURE ( #TI,#$I,#PI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #SUM.#C-QTY ( #TI,#$I,#PI ) ( EM = ZZZ,ZZZ,ZZ9 ) 078T #SUM.#F-QTY ( #TI,#$I,#PI ) ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 ) / /
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Sum_Pnd_Future.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(35),"CHECK QTY",new 
                    TabSetting(53),pnd_Sum_Pnd_C_Qty.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(78),pnd_Sum_Pnd_F_Qty.getValue(pnd_Ti,pnd_Dollar_I,pnd_Pi), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_C_Total.reset();                                                                                                                                          //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
            pnd_F_Total.reset();
            pnd_T_Total.reset();
            pnd_Cqty.reset();
            pnd_Fqty.reset();
            pnd_Tqty.reset();
            pnd_C_Total.nadd(pnd_Sum_Pnd_Current.getValue("*",pnd_Dollar_I,pnd_Pi));                                                                                      //Natural: ASSIGN #C-TOTAL := #C-TOTAL + #SUM.#CURRENT ( *,#$I,#PI )
            pnd_F_Total.nadd(pnd_Sum_Pnd_Future.getValue("*",pnd_Dollar_I,pnd_Pi));                                                                                       //Natural: ASSIGN #F-TOTAL := #F-TOTAL + #SUM.#FUTURE ( *,#$I,#PI )
            pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_F_Total.add(pnd_C_Total));                                                                 //Natural: ASSIGN #T-TOTAL := #F-TOTAL + #C-TOTAL
            pnd_Cqty.nadd(pnd_Sum_Pnd_C_Qty.getValue("*",pnd_Dollar_I,pnd_Pi));                                                                                           //Natural: ASSIGN #CQTY := #CQTY + #SUM.#C-QTY ( *,#$I,#PI )
            pnd_Fqty.nadd(pnd_Sum_Pnd_F_Qty.getValue("*",pnd_Dollar_I,pnd_Pi));                                                                                           //Natural: ASSIGN #FQTY := #FQTY + #SUM.#F-QTY ( *,#$I,#PI )
            pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Fqty.add(pnd_Cqty));                                                                             //Natural: ASSIGN #TQTY := #FQTY + #CQTY
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_C_Total, new ReportEditMask                    //Natural: WRITE ( PRT1 ) / / / 'TOTAL' 032T 'CHECK AMOUNT' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #F-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #CQTY ( EM = ZZZ,ZZZ,ZZ9 ) 078T #FQTY ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 )
                ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(35),"CHECK QTY",new TabSetting(53),pnd_Cqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
                TabSetting(78),pnd_Fqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( PRT1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Dollar_I.equals(1)))                                                                                                                            //Natural: IF #$I = 1
        {
            pnd_Header4.setValue("US SUMMARY TOTALS");                                                                                                                    //Natural: ASSIGN #HEADER4 := 'US SUMMARY TOTALS'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Header4.setValue("CANADIAN SUMMARY TOTALS");                                                                                                              //Natural: ASSIGN #HEADER4 := 'CANADIAN SUMMARY TOTALS'
        }                                                                                                                                                                 //Natural: END-IF
        //*  05-18-98
        //*  05-18-98
                                                                                                                                                                          //Natural: PERFORM CENTER-SUB-HEADER
        sub_Center_Sub_Header();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Start_Index.setValue(1);                                                                                                                                      //Natural: ASSIGN #START-INDEX := 1
        getReports().write(2, NEWLINE,NEWLINE,new TabSetting(60),"CURRENT",new TabSetting(86),"FUTURE",new TabSetting(112),"TOTAL",NEWLINE);                              //Natural: WRITE ( PRT1 ) / / 060T 'CURRENT' 086T 'FUTURE' 112T 'TOTAL' /
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #TI #START-INDEX TO #TI-MAX
        for (pnd_Ti.setValue(pnd_Start_Index); condition(pnd_Ti.lessOrEqual(pnd_Ti_Max)); pnd_Ti.nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TXT
            sub_Initialize_Txt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_Us_Can_Pnd_Current.getValue(pnd_Ti,pnd_Dollar_I).add(pnd_Us_Can_Pnd_Future.getValue(pnd_Ti, //Natural: ASSIGN #T-TOTAL := #US-CAN.#CURRENT ( #TI,#$I ) + #US-CAN.#FUTURE ( #TI,#$I )
                pnd_Dollar_I)));
            pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Us_Can_Pnd_C_Qty.getValue(pnd_Ti,pnd_Dollar_I).add(pnd_Us_Can_Pnd_F_Qty.getValue(pnd_Ti,         //Natural: ASSIGN #TQTY := #US-CAN.#C-QTY ( #TI,#$I ) + #US-CAN.#F-QTY ( #TI,#$I )
                pnd_Dollar_I)));
            getReports().write(2, pnd_Sub_Txt, new AlphanumericLength (30),new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_Us_Can_Pnd_Current.getValue(pnd_Ti,pnd_Dollar_I),  //Natural: WRITE ( PRT1 ) #SUB-TXT ( AL = 30 ) 032T 'CHECK AMOUNT' 045T #US-CAN.#CURRENT ( #TI,#$I ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #US-CAN.#FUTURE ( #TI,#$I ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #US-CAN.#C-QTY ( #TI,#$I ) ( EM = ZZZ,ZZZ,ZZ9 ) 078T #US-CAN.#F-QTY ( #TI,#$I ) ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 ) / /
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Us_Can_Pnd_Future.getValue(pnd_Ti,pnd_Dollar_I), new ReportEditMask 
                ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(35),"CHECK QTY",new 
                TabSetting(53),pnd_Us_Can_Pnd_C_Qty.getValue(pnd_Ti,pnd_Dollar_I), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(78),pnd_Us_Can_Pnd_F_Qty.getValue(pnd_Ti,pnd_Dollar_I), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_C_Total.reset();                                                                                                                                              //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
        pnd_F_Total.reset();
        pnd_T_Total.reset();
        pnd_Cqty.reset();
        pnd_Fqty.reset();
        pnd_Tqty.reset();
        pnd_C_Total.nadd(pnd_Us_Can_Pnd_Current.getValue("*",pnd_Dollar_I));                                                                                              //Natural: ASSIGN #C-TOTAL := #C-TOTAL + #US-CAN.#CURRENT ( *,#$I )
        pnd_F_Total.nadd(pnd_Us_Can_Pnd_Future.getValue("*",pnd_Dollar_I));                                                                                               //Natural: ASSIGN #F-TOTAL := #F-TOTAL + #US-CAN.#FUTURE ( *,#$I )
        pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_F_Total.add(pnd_C_Total));                                                                     //Natural: ASSIGN #T-TOTAL := #F-TOTAL + #C-TOTAL
        pnd_Cqty.nadd(pnd_Us_Can_Pnd_C_Qty.getValue("*",pnd_Dollar_I));                                                                                                   //Natural: ASSIGN #CQTY := #CQTY + #US-CAN.#C-QTY ( *,#$I )
        pnd_Fqty.nadd(pnd_Us_Can_Pnd_F_Qty.getValue("*",pnd_Dollar_I));                                                                                                   //Natural: ASSIGN #FQTY := #FQTY + #US-CAN.#F-QTY ( *,#$I )
        pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Fqty.add(pnd_Cqty));                                                                                 //Natural: ASSIGN #TQTY := #FQTY + #CQTY
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_C_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( PRT1 ) / / / 'TOTAL' 032T 'CHECK AMOUNT' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #F-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #CQTY ( EM = ZZZ,ZZZ,ZZ9 ) 078T #FQTY ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 )
            TabSetting(70),pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(35),"CHECK QTY",new TabSetting(53),pnd_Cqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(78),pnd_Fqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
            TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRT1 )
        if (condition(Global.isEscape())){return;}
        //*  05-18-98
        if (condition(pnd_Dollar_I_Max.equals(2)))                                                                                                                        //Natural: IF #$I-MAX = 2
        {
            FOR08:                                                                                                                                                        //Natural: FOR #PI #START-INDEX TO #END-INDEX
            for (pnd_Pi.setValue(pnd_Start_Index); condition(pnd_Pi.lessOrEqual(pnd_End_Index)); pnd_Pi.nadd(1))
            {
                short decideConditionsMet1320 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #PI;//Natural: VALUE 1
                if (condition((pnd_Pi.equals(1))))
                {
                    decideConditionsMet1320++;
                    pnd_Header4.setValue("CHECK SUMMARY TOTALS - COMBINED");                                                                                              //Natural: ASSIGN #HEADER4 := 'CHECK SUMMARY TOTALS - COMBINED'
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_Pi.equals(2))))
                {
                    decideConditionsMet1320++;
                    pnd_Header4.setValue("EFT SUMMARY TOTALS - COMBINED");                                                                                                //Natural: ASSIGN #HEADER4 := 'EFT SUMMARY TOTALS - COMBINED'
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_Pi.equals(3))))
                {
                    decideConditionsMet1320++;
                    pnd_Header4.setValue("GLOBAL SUMMARY TOTALS - COMBINED");                                                                                             //Natural: ASSIGN #HEADER4 := 'GLOBAL SUMMARY TOTALS - COMBINED'
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet1320 > 0))
                {
                    //*  05-18-98
                                                                                                                                                                          //Natural: PERFORM CENTER-SUB-HEADER
                    sub_Center_Sub_Header();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Len.reset();                                                                                                                                      //Natural: RESET #LEN #W-FLD
                    pnd_W_Fld.reset();
                    DbsUtil.examine(new ExamineSource(pnd_Header2,true), new ExamineSearch("Canadian"), new ExamineDelete());                                             //Natural: EXAMINE FULL #HEADER2 FOR 'Canadian' DELETE
                    DbsUtil.examine(new ExamineSource(pnd_Header3,true), new ExamineSearch("Canadian"), new ExamineDelete());                                             //Natural: EXAMINE FULL #HEADER3 FOR 'Canadian' DELETE
                    DbsUtil.examine(new ExamineSource(pnd_Header2), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                            //Natural: EXAMINE #HEADER2 FOR ' ' GIVING LENGTH #LEN
                    pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(74,pnd_Len)).divide(2));                                                    //Natural: COMPUTE #LEN = ( 74 - #LEN ) / 2
                    if (condition(pnd_Len.greaterOrEqual(1)))                                                                                                             //Natural: IF #LEN GE 1
                    {
                        pnd_W_Fld.moveAll("H'00'");                                                                                                                       //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Header2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header2));                                                        //Natural: COMPRESS #W-FLD #HEADER2 INTO #HEADER2 LEAVE NO SPACE
                    DbsUtil.examine(new ExamineSource(pnd_Header2,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                      //Natural: EXAMINE FULL #HEADER2 FOR FULL H'00' REPLACE ' '
                    pnd_Len.reset();                                                                                                                                      //Natural: RESET #LEN #W-FLD
                    pnd_W_Fld.reset();
                    DbsUtil.examine(new ExamineSource(pnd_Header3), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                            //Natural: EXAMINE #HEADER3 FOR ' ' GIVING LENGTH #LEN
                    pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(74,pnd_Len)).divide(2));                                                    //Natural: COMPUTE #LEN = ( 74 - #LEN ) / 2
                    if (condition(pnd_Len.greaterOrEqual(1)))                                                                                                             //Natural: IF #LEN GE 1
                    {
                        pnd_W_Fld.moveAll("H'00'");                                                                                                                       //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Header3.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header3));                                                        //Natural: COMPRESS #W-FLD #HEADER3 INTO #HEADER3 LEAVE NO SPACE
                    DbsUtil.examine(new ExamineSource(pnd_Header3,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                      //Natural: EXAMINE FULL #HEADER3 FOR FULL H'00' REPLACE ' '
                    getReports().write(2, NEWLINE,NEWLINE,new TabSetting(60),"CURRENT",new TabSetting(86),"FUTURE",new TabSetting(112),"TOTAL",NEWLINE);                  //Natural: WRITE ( PRT1 ) / / 060T 'CURRENT' 086T 'FUTURE' 112T 'TOTAL' /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                    //*  05-18-98
                }                                                                                                                                                         //Natural: END-DECIDE
                FOR09:                                                                                                                                                    //Natural: FOR #TI #START-INDEX TO #TI-MAX
                for (pnd_Ti.setValue(pnd_Start_Index); condition(pnd_Ti.lessOrEqual(pnd_Ti_Max)); pnd_Ti.nadd(1))
                {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TXT
                    sub_Initialize_Txt();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_G_Total_Pnd_Current.getValue(pnd_Ti,pnd_Pi).add(pnd_G_Total_Pnd_Future.getValue(pnd_Ti, //Natural: ASSIGN #T-TOTAL := #G-TOTAL.#CURRENT ( #TI,#PI ) + #G-TOTAL.#FUTURE ( #TI,#PI )
                        pnd_Pi)));
                    pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_G_Total_Pnd_C_Qty.getValue(pnd_Ti,pnd_Pi).add(pnd_G_Total_Pnd_F_Qty.getValue(pnd_Ti,     //Natural: ASSIGN #TQTY := #G-TOTAL.#C-QTY ( #TI,#PI ) + #G-TOTAL.#F-QTY ( #TI,#PI )
                        pnd_Pi)));
                    getReports().write(2, pnd_Sub_Txt, new AlphanumericLength (30),new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_G_Total_Pnd_Current.getValue(pnd_Ti,pnd_Pi),  //Natural: WRITE ( PRT1 ) #SUB-TXT ( AL = 30 ) 032T 'CHECK AMOUNT' 045T #G-TOTAL.#CURRENT ( #TI,#PI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #G-TOTAL.#FUTURE ( #TI,#PI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #G-TOTAL.#C-QTY ( #TI,#PI ) ( EM = ZZZ,ZZZ,ZZ9 ) 078T #G-TOTAL.#F-QTY ( #TI,#PI ) ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 ) / /
                        new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_G_Total_Pnd_Future.getValue(pnd_Ti,pnd_Pi), new ReportEditMask 
                        ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(35),"CHECK QTY",new 
                        TabSetting(53),pnd_G_Total_Pnd_C_Qty.getValue(pnd_Ti,pnd_Pi), new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(78),pnd_G_Total_Pnd_F_Qty.getValue(pnd_Ti,pnd_Pi), 
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_C_Total.reset();                                                                                                                                      //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
                pnd_F_Total.reset();
                pnd_T_Total.reset();
                pnd_Cqty.reset();
                pnd_Fqty.reset();
                pnd_Tqty.reset();
                pnd_C_Total.nadd(pnd_G_Total_Pnd_Current.getValue("*",pnd_Pi));                                                                                           //Natural: ASSIGN #C-TOTAL := #C-TOTAL + #G-TOTAL.#CURRENT ( *,#PI )
                pnd_F_Total.nadd(pnd_G_Total_Pnd_Future.getValue("*",pnd_Pi));                                                                                            //Natural: ASSIGN #F-TOTAL := #F-TOTAL + #G-TOTAL.#FUTURE ( *,#PI )
                pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_F_Total.add(pnd_C_Total));                                                             //Natural: ASSIGN #T-TOTAL := #F-TOTAL + #C-TOTAL
                pnd_Cqty.nadd(pnd_G_Total_Pnd_C_Qty.getValue("*",pnd_Pi));                                                                                                //Natural: ASSIGN #CQTY := #CQTY + #G-TOTAL.#C-QTY ( *,#PI )
                pnd_Fqty.nadd(pnd_G_Total_Pnd_F_Qty.getValue("*",pnd_Pi));                                                                                                //Natural: ASSIGN #FQTY := #FQTY + #G-TOTAL.#F-QTY ( *,#PI )
                pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Fqty.add(pnd_Cqty));                                                                         //Natural: ASSIGN #TQTY := #FQTY + #CQTY
                getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_C_Total, new ReportEditMask                //Natural: WRITE ( PRT1 ) / / / 'TOTAL' 032T 'CHECK AMOUNT' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #F-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #CQTY ( EM = ZZZ,ZZZ,ZZ9 ) 078T #FQTY ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 )
                    ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(35),"CHECK QTY",new TabSetting(53),pnd_Cqty, new ReportEditMask 
                    ("ZZZ,ZZZ,ZZ9"),new TabSetting(78),pnd_Fqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( PRT1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Header4.setValue("SUMMARY TOTALS - COMBINED");                                                                                                            //Natural: ASSIGN #HEADER4 := 'SUMMARY TOTALS - COMBINED'
            //*  05-18-98
            //*  05-18-98
                                                                                                                                                                          //Natural: PERFORM CENTER-SUB-HEADER
            sub_Center_Sub_Header();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(2, NEWLINE,NEWLINE,new TabSetting(60),"CURRENT",new TabSetting(86),"FUTURE",new TabSetting(112),"TOTAL",NEWLINE);                          //Natural: WRITE ( PRT1 ) / / 060T 'CURRENT' 086T 'FUTURE' 112T 'TOTAL' /
            if (Global.isEscape()) return;
            FOR10:                                                                                                                                                        //Natural: FOR #TI #START-INDEX TO #TI-MAX
            for (pnd_Ti.setValue(pnd_Start_Index); condition(pnd_Ti.lessOrEqual(pnd_Ti_Max)); pnd_Ti.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TXT
                sub_Initialize_Txt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                pnd_C_Total.reset();                                                                                                                                      //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
                pnd_F_Total.reset();
                pnd_T_Total.reset();
                pnd_Cqty.reset();
                pnd_Fqty.reset();
                pnd_Tqty.reset();
                pnd_C_Total.nadd(pnd_G_Total_Pnd_Current.getValue(pnd_Ti,"*"));                                                                                           //Natural: ASSIGN #C-TOTAL := #C-TOTAL + #G-TOTAL.#CURRENT ( #TI,* )
                pnd_Cqty.nadd(pnd_G_Total_Pnd_C_Qty.getValue(pnd_Ti,"*"));                                                                                                //Natural: ASSIGN #CQTY := #CQTY + #G-TOTAL.#C-QTY ( #TI,* )
                pnd_F_Total.nadd(pnd_G_Total_Pnd_Future.getValue(pnd_Ti,"*"));                                                                                            //Natural: ASSIGN #F-TOTAL := #F-TOTAL + #G-TOTAL.#FUTURE ( #TI,* )
                pnd_Fqty.nadd(pnd_G_Total_Pnd_F_Qty.getValue(pnd_Ti,"*"));                                                                                                //Natural: ASSIGN #FQTY := #FQTY + #G-TOTAL.#F-QTY ( #TI,* )
                pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_C_Total.add(pnd_F_Total));                                                             //Natural: ASSIGN #T-TOTAL := #C-TOTAL + #F-TOTAL
                pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Cqty.add(pnd_Fqty));                                                                         //Natural: ASSIGN #TQTY := #CQTY + #FQTY
                getReports().write(2, pnd_Sub_Txt, new AlphanumericLength (30),new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_C_Total, new ReportEditMask       //Natural: WRITE ( PRT1 ) #SUB-TXT ( AL = 30 ) 032T 'CHECK AMOUNT' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #F-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #CQTY ( EM = ZZZ,ZZZ,ZZ9 ) 078T #FQTY ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 ) / /
                    ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(35),"CHECK QTY",new TabSetting(53),pnd_Cqty, new ReportEditMask 
                    ("ZZZ,ZZZ,ZZ9"),new TabSetting(78),pnd_Fqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),
                    NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_C_Total.reset();                                                                                                                                          //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
            pnd_F_Total.reset();
            pnd_T_Total.reset();
            pnd_Cqty.reset();
            pnd_Fqty.reset();
            pnd_Tqty.reset();
            pnd_C_Total.nadd(pnd_G_Total_Pnd_Current.getValue("*","*"));                                                                                                  //Natural: ASSIGN #C-TOTAL := #C-TOTAL + #G-TOTAL.#CURRENT ( *,* )
            pnd_F_Total.nadd(pnd_G_Total_Pnd_Future.getValue("*","*"));                                                                                                   //Natural: ASSIGN #F-TOTAL := #F-TOTAL + #G-TOTAL.#FUTURE ( *,* )
            pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_F_Total.add(pnd_C_Total));                                                                 //Natural: ASSIGN #T-TOTAL := #F-TOTAL + #C-TOTAL
            pnd_Cqty.nadd(pnd_G_Total_Pnd_C_Qty.getValue("*","*"));                                                                                                       //Natural: ASSIGN #CQTY := #CQTY + #G-TOTAL.#C-QTY ( *,* )
            pnd_Fqty.nadd(pnd_G_Total_Pnd_F_Qty.getValue("*","*"));                                                                                                       //Natural: ASSIGN #FQTY := #FQTY + #G-TOTAL.#F-QTY ( *,* )
            pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Fqty.add(pnd_Cqty));                                                                             //Natural: ASSIGN #TQTY := #FQTY + #CQTY
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_C_Total, new ReportEditMask                    //Natural: WRITE ( PRT1 ) / / / 'TOTAL' 032T 'CHECK AMOUNT' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #F-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #CQTY ( EM = ZZZ,ZZZ,ZZ9 ) 078T #FQTY ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 )
                ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(35),"CHECK QTY",new TabSetting(53),pnd_Cqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
                TabSetting(78),pnd_Fqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  05-18-98
        pnd_C_Total.reset();                                                                                                                                              //Natural: RESET #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY #SUM ( *,* ) #G-TOTAL ( *,* ) #US-CAN ( * )
        pnd_F_Total.reset();
        pnd_T_Total.reset();
        pnd_Cqty.reset();
        pnd_Fqty.reset();
        pnd_Tqty.reset();
        pnd_Sum.getValue("*","*").reset();
        pnd_G_Total.getValue("*","*").reset();
        pnd_Us_Can.getValue("*").reset();
        pnd_Summary.setValue(false);                                                                                                                                      //Natural: ASSIGN #SUMMARY := FALSE
    }
    private void sub_Determine_Settl_System() throws Exception                                                                                                            //Natural: DETERMINE-SETTL-SYSTEM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  05-18-98
        short decideConditionsMet1403 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #RPT-EXT.CNTRCT-ORGN-CDE;//Natural: VALUE 'AL'
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AL"))))
        {
            decideConditionsMet1403++;
            pnd_Settl_System.setValue("ANNUITY LOAN");                                                                                                                    //Natural: ASSIGN #SETTL-SYSTEM := 'ANNUITY LOAN'
            pnd_Ti_Max.setValue(1);                                                                                                                                       //Natural: ASSIGN #TI-MAX := 1
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet1403++;
            pnd_Settl_System.setValue("IA DEATH");                                                                                                                        //Natural: ASSIGN #SETTL-SYSTEM := 'IA DEATH'
            pnd_Ti_Max.setValue(2);                                                                                                                                       //Natural: ASSIGN #TI-MAX := 2
        }                                                                                                                                                                 //Natural: VALUE 'DS'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DS"))))
        {
            decideConditionsMet1403++;
            pnd_Settl_System.setValue("DAILY");                                                                                                                           //Natural: ASSIGN #SETTL-SYSTEM := 'DAILY'
            pnd_Ti_Max.setValue(3);                                                                                                                                       //Natural: ASSIGN #TI-MAX := 3
        }                                                                                                                                                                 //Natural: VALUE 'IA'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA"))))
        {
            decideConditionsMet1403++;
            pnd_Settl_System.setValue("IMMED. ANNTY.");                                                                                                                   //Natural: ASSIGN #SETTL-SYSTEM := 'IMMED. ANNTY.'
            pnd_Ti_Max.setValue(1);                                                                                                                                       //Natural: ASSIGN #TI-MAX := 1
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("MS"))))
        {
            decideConditionsMet1403++;
            pnd_Settl_System.setValue("MONTHLY");                                                                                                                         //Natural: ASSIGN #SETTL-SYSTEM := 'MONTHLY'
            pnd_Ti_Max.setValue(6);                                                                                                                                       //Natural: ASSIGN #TI-MAX := 6
        }                                                                                                                                                                 //Natural: VALUE 'SS'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS"))))
        {
            decideConditionsMet1403++;
            pnd_Settl_System.setValue("SINGLE SUM");                                                                                                                      //Natural: ASSIGN #SETTL-SYSTEM := 'SINGLE SUM'
            pnd_Ti_Max.setValue(3);                                                                                                                                       //Natural: ASSIGN #TI-MAX := 3
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,"INVALID ORIGIN CODE",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde(),"ENCOUNTERED FOR CONTRACT",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr()); //Natural: WRITE NOTITLE 'INVALID ORIGIN CODE' #RPT-EXT.CNTRCT-ORGN-CDE 'ENCOUNTERED FOR CONTRACT' #RPT-EXT.CNTRCT-PPCN-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Csr_Type() throws Exception                                                                                                                //Natural: DETERMINE-CSR-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  05-18-98
        //*  LEON  08-10-99
        //*  JWO 2010-11
        //*  JWO 2011-01
        //*  JWO 2011-01
        //*  LEON  08-10-99
        //*  JWO 2010-11
        short decideConditionsMet1438 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C'
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("CANCELLED       ");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'CANCELLED       '
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("REDRAWN         ");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'REDRAWN         '
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("STOPPED         ");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'STOPPED         '
        }                                                                                                                                                                 //Natural: VALUE 'CN'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("CANCEL NO REDRAW");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'CANCEL NO REDRAW'
        }                                                                                                                                                                 //Natural: VALUE 'CP'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("CANCEL NO REDRAW");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'CANCEL NO REDRAW'
        }                                                                                                                                                                 //Natural: VALUE 'RP'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("CANCEL NO REDRAW");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'CANCEL NO REDRAW'
        }                                                                                                                                                                 //Natural: VALUE 'PR'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("CANCEL NO REDRAW");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'CANCEL NO REDRAW'
        }                                                                                                                                                                 //Natural: VALUE 'SN'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("STOP NO REDRAW  ");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'STOP NO REDRAW  '
        }                                                                                                                                                                 //Natural: VALUE 'SP'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
        {
            decideConditionsMet1438++;
            pnd_Csr_Actvty.setValue("STOP NO REDRAW  ");                                                                                                                  //Natural: ASSIGN #CSR-ACTVTY := 'STOP NO REDRAW  '
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,"INVALID 'CSR' ACTIVITY CODE DETECTED FOR CONTRACT",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr());                 //Natural: WRITE NOTITLE 'INVALID "CSR" ACTIVITY CODE DETECTED FOR CONTRACT' #RPT-EXT.CNTRCT-PPCN-NBR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Initialize_Txt() throws Exception                                                                                                                    //Natural: INITIALIZE-TXT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  05-18-98
        short decideConditionsMet1464 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #OLD-CNTRCT-ORGN-CDE = 'AL'
        if (condition(pnd_Old_Cntrct_Orgn_Cde.equals("AL")))
        {
            decideConditionsMet1464++;
            if (condition(pnd_Ti.equals(1)))                                                                                                                              //Natural: IF #TI = 1
            {
                pnd_Sub_Txt.setValue("ANNUITY LOAN");                                                                                                                     //Natural: ASSIGN #SUB-TXT := 'ANNUITY LOAN'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #OLD-CNTRCT-ORGN-CDE = 'DC'
        else if (condition(pnd_Old_Cntrct_Orgn_Cde.equals("DC")))
        {
            decideConditionsMet1464++;
            short decideConditionsMet1470 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #TI;//Natural: VALUE 1
            if (condition((pnd_Ti.equals(1))))
            {
                decideConditionsMet1470++;
                pnd_Sub_Txt.setValue("LUMP SUM DEATH");                                                                                                                   //Natural: ASSIGN #SUB-TXT := 'LUMP SUM DEATH'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ti.equals(2))))
            {
                decideConditionsMet1470++;
                pnd_Sub_Txt.setValue("SUPPLEMENTAL DEATH");                                                                                                               //Natural: ASSIGN #SUB-TXT := 'SUPPLEMENTAL DEATH'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #OLD-CNTRCT-ORGN-CDE = 'DS'
        else if (condition(pnd_Old_Cntrct_Orgn_Cde.equals("DS")))
        {
            decideConditionsMet1464++;
            short decideConditionsMet1479 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #TI;//Natural: VALUE 1
            if (condition((pnd_Ti.equals(1))))
            {
                decideConditionsMet1479++;
                pnd_Sub_Txt.setValue("CASHOUT");                                                                                                                          //Natural: ASSIGN #SUB-TXT := 'CASHOUT'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ti.equals(2))))
            {
                decideConditionsMet1479++;
                pnd_Sub_Txt.setValue("LUMP SUM");                                                                                                                         //Natural: ASSIGN #SUB-TXT := 'LUMP SUM'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ti.equals(3))))
            {
                decideConditionsMet1479++;
                pnd_Sub_Txt.setValue("REPURCHASE");                                                                                                                       //Natural: ASSIGN #SUB-TXT := 'REPURCHASE'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #OLD-CNTRCT-ORGN-CDE = 'IA'
        else if (condition(pnd_Old_Cntrct_Orgn_Cde.equals("IA")))
        {
            decideConditionsMet1464++;
            if (condition(pnd_Ti.equals(1)))                                                                                                                              //Natural: IF #TI = 1
            {
                pnd_Sub_Txt.setValue("PERIODIC PAYMENT");                                                                                                                 //Natural: ASSIGN #SUB-TXT := 'PERIODIC PAYMENT'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #OLD-CNTRCT-ORGN-CDE = 'MS'
        else if (condition(pnd_Old_Cntrct_Orgn_Cde.equals("MS")))
        {
            decideConditionsMet1464++;
            short decideConditionsMet1494 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #TI;//Natural: VALUE 1
            if (condition((pnd_Ti.equals(1))))
            {
                decideConditionsMet1494++;
                pnd_Sub_Txt.setValue("MATURITY");                                                                                                                         //Natural: ASSIGN #SUB-TXT := 'MATURITY'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ti.equals(2))))
            {
                decideConditionsMet1494++;
                pnd_Sub_Txt.setValue("TPA");                                                                                                                              //Natural: ASSIGN #SUB-TXT := 'TPA'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ti.equals(3))))
            {
                decideConditionsMet1494++;
                pnd_Sub_Txt.setValue("MDO INITIAL");                                                                                                                      //Natural: ASSIGN #SUB-TXT := 'MDO INITIAL'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Ti.equals(4))))
            {
                decideConditionsMet1494++;
                pnd_Sub_Txt.setValue("MDO RECURRING");                                                                                                                    //Natural: ASSIGN #SUB-TXT := 'MDO RECURRING'
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Ti.equals(5))))
            {
                decideConditionsMet1494++;
                pnd_Sub_Txt.setValue("LUMP SUM DEATH");                                                                                                                   //Natural: ASSIGN #SUB-TXT := 'LUMP SUM DEATH'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_Ti.equals(6))))
            {
                decideConditionsMet1494++;
                pnd_Sub_Txt.setValue("SUPPLEMENTAL DEATH");                                                                                                               //Natural: ASSIGN #SUB-TXT := 'SUPPLEMENTAL DEATH'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN #OLD-CNTRCT-ORGN-CDE = 'SS'
        else if (condition(pnd_Old_Cntrct_Orgn_Cde.equals("SS")))
        {
            decideConditionsMet1464++;
            short decideConditionsMet1511 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #TI;//Natural: VALUE 1
            if (condition((pnd_Ti.equals(1))))
            {
                decideConditionsMet1511++;
                pnd_Sub_Txt.setValue("LUMP SUM");                                                                                                                         //Natural: ASSIGN #SUB-TXT := 'LUMP SUM'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ti.equals(2))))
            {
                decideConditionsMet1511++;
                pnd_Sub_Txt.setValue("MDO INITIAL");                                                                                                                      //Natural: ASSIGN #SUB-TXT := 'MDO INITIAL'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ti.equals(3))))
            {
                decideConditionsMet1511++;
                pnd_Sub_Txt.setValue("MDO RECURRING");                                                                                                                    //Natural: ASSIGN #SUB-TXT := 'MDO RECURRING'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  05-18-98
    private void sub_Summary_Evaluation() throws Exception                                                                                                                //Natural: SUMMARY-EVALUATION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Summary.setValue(true);                                                                                                                                       //Natural: ASSIGN #SUMMARY := TRUE
        short decideConditionsMet1529 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #PRINTED-1
        if (condition(pnd_Printed_1.getBoolean()))
        {
            decideConditionsMet1529++;
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY
            sub_Print_Summary();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NOT #PRINTED-1
        if (condition(! ((pnd_Printed_1.getBoolean()))))
        {
            decideConditionsMet1529++;
            pnd_Header2.setValue(DbsUtil.compress(pnd_Header2_Tmp, pnd_Csr_Actvty, "PAYMENT REGISTER"));                                                                  //Natural: COMPRESS #HEADER2-TMP #CSR-ACTVTY 'PAYMENT REGISTER' INTO #HEADER2
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER-2-3
            sub_Center_Header_2_3();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(33),                //Natural: WRITE ( PRT1 ) NOTITLE ////////// 33T '********** NO' #CSR-ACTVTY 'PAYMENTS PROCESSED' 'IN THIS CYCLE. **********'
                "********** NO",pnd_Csr_Actvty,"PAYMENTS PROCESSED","IN THIS CYCLE. **********");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1529 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    //*  05-18-98
    private void sub_Reset_Variables() throws Exception                                                                                                                   //Natural: RESET-VARIABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* **------------------------------------------------------------------***
        pnd_Pymnt_Prcss_Seq_Nbr.reset();                                                                                                                                  //Natural: RESET #PYMNT-PRCSS-SEQ-NBR #PREV-SEQ-NBR #CF-TXT #PRINTED-1 #ITEM #NO-GUAR-STCK #DTE #MISC #TYPE-SUB #DTL-LINES ( * ) #SUM ( *,* ) #G-TOTAL ( *,* ) #US-CAN ( * ) #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY #TYPE-SUB-PRT #TIAA #HEADER2 #HEADER3 #HEADER4 #SUMMARY
        pnd_Prev_Seq_Nbr.reset();
        pnd_Cf_Txt.reset();
        pnd_Printed_1.reset();
        pnd_Item.reset();
        pnd_No_Guar_Stck.reset();
        pnd_Dte.reset();
        pnd_Misc.reset();
        pnd_Type_Sub.reset();
        pnd_Dtl_Lines.getValue("*").reset();
        pnd_Sum.getValue("*","*").reset();
        pnd_G_Total.getValue("*","*").reset();
        pnd_Us_Can.getValue("*").reset();
        pnd_C_Total.reset();
        pnd_F_Total.reset();
        pnd_T_Total.reset();
        pnd_Cqty.reset();
        pnd_Fqty.reset();
        pnd_Tqty.reset();
        pnd_Type_Sub_Prt.reset();
        pnd_Tiaa.reset();
        pnd_Header2.reset();
        pnd_Header3.reset();
        pnd_Header4.reset();
        pnd_Summary.reset();
        pnd_Init.resetInitial();                                                                                                                                          //Natural: RESET INITIAL #INIT #FIRST
        pnd_First.resetInitial();
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE FCPA110.START-CHECK-PREFIX-N3 TO #CHECK-NUMBER-N3          /*RL
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(! (pnd_Summary.getBoolean())))                                                                                                          //Natural: IF NOT #SUMMARY
                    {
                                                                                                                                                                          //Natural: PERFORM CENTER-SUB-HEADER
                        sub_Center_Sub_Header();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(2),  //Natural: WRITE ( PRT1 ) NOTITLE *PROGRAM 41T #HEADER1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( PRT1 ) ( NL = 4 )
                        new NumericLength (4));
                    if (condition(pnd_Dollar_I.equals(1)))                                                                                                                //Natural: IF #$I = 1
                    {
                        getReports().write(2, Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(26),pnd_Header2,new TabSetting(124),Global.getTIMX(),  //Natural: WRITE ( PRT1 ) *DATX ( EM = LLL' 'DD', 'YYYY ) 26T #HEADER2 124T *TIMX ( EM = HH':'II' 'AP ) /
                            new ReportEditMask ("HH':'II' 'AP"),NEWLINE);
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(26),pnd_Header3,new TabSetting(124),Global.getTIMX(),  //Natural: WRITE ( PRT1 ) *DATX ( EM = LLL' 'DD', 'YYYY ) 26T #HEADER3 124T *TIMX ( EM = HH':'II' 'AP ) /
                            new ReportEditMask ("HH':'II' 'AP"),NEWLINE);
                    }                                                                                                                                                     //Natural: END-IF
                    //*  02-13-95 : A. YOUNG
                    if (condition(! (pnd_Summary.getBoolean())))                                                                                                          //Natural: IF NOT #SUMMARY
                    {
                        getReports().write(2, new TabSetting(41),pnd_Header4,pnd_Cf_Txt);                                                                                 //Natural: WRITE ( PRT1 ) 41T #HEADER4 #CF-TXT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, new TabSetting(41),pnd_Header4);                                                                                            //Natural: WRITE ( PRT1 ) 41T #HEADER4
                        //*  02-13-95 : A. YOUNG
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP ( PRT1 ) 1
                    if (condition(! (pnd_Summary.getBoolean())))                                                                                                          //Natural: IF NOT #SUMMARY
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-HEADER
                        sub_Print_Detail_Header();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Intrfce_DteIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Ftre_IndIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Crrncy_CdeIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Crrncy_Cde().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().isBreak(endOfData);
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Intrfce_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Ftre_IndIsBreak 
            || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Crrncy_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak 
            || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
            if (condition(! (pnd_Month_End.getBoolean())))                                                                                                                //Natural: IF NOT #MONTH-END
            {
                if (condition(pnd_Input_Cycle.equals("LEDGR")))                                                                                                           //Natural: IF #INPUT-CYCLE = 'LEDGR'
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ITEM-SUB-TOTALS
                    sub_Calculate_Item_Sub_Totals();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                    sub_Print_Detail_Lines();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DTE-SUB-TOTAL
                    sub_Print_Dte_Sub_Total();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Ftre_IndIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak 
            || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Crrncy_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
            if (condition(! (pnd_Month_End.getBoolean())))                                                                                                                //Natural: IF NOT #MONTH-END
            {
                if (condition(pnd_Input_Cycle.equals("DAILY") || pnd_Input_Cycle.equals("DAYND")))                                                                        //Natural: IF #INPUT-CYCLE = 'DAILY' OR = 'DAYND'
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ITEM-SUB-TOTALS
                    sub_Calculate_Item_Sub_Totals();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                    sub_Print_Detail_Lines();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DTE-SUB-TOTAL
                    sub_Print_Dte_Sub_Total();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Ftre_IndIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Crrncy_CdeIsBreak 
            || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
            pnd_O_Ftre_Ind.setValue(readWork01Pymnt_Ftre_IndOld);                                                                                                         //Natural: ASSIGN #O-FTRE-IND := OLD ( #RPT-EXT.PYMNT-FTRE-IND )
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Crrncy_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak 
            || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
            if (condition(pnd_Month_End.getBoolean()))                                                                                                                    //Natural: IF #MONTH-END
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ITEM-SUB-TOTALS
                sub_Calculate_Item_Sub_Totals();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                sub_Print_Detail_Lines();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-TYPE-SUB-TOTAL
            sub_Print_Type_Sub_Total();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INCREMENT-INDEX
            sub_Increment_Index();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_First.setValue(true);                                                                                                                                     //Natural: ASSIGN #FIRST := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Crrncy_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
            //*  05-18-98
            if (condition(! (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().isBreak())))                                                                      //Natural: IF NOT BREAK OF #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            {
                pnd_Dollar_I.compute(new ComputeParameters(false, pnd_Dollar_I), ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Crrncy_Cde().val());                                   //Natural: COMPUTE #$I = VAL ( #RPT-EXT.CNTRCT-CRRNCY-CDE )
                pnd_Dollar_I_Max.setValue(pnd_Dollar_I);                                                                                                                  //Natural: ASSIGN #$I-MAX := #$I
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
            pnd_Old_Cntrct_Orgn_Cde.setValue(readWork01Cntrct_Orgn_CdeOld);                                                                                               //Natural: ASSIGN #OLD-CNTRCT-ORGN-CDE := OLD ( #RPT-EXT.CNTRCT-ORGN-CDE )
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 #$I-MAX
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Dollar_I_Max)); pnd_I.nadd(1))
            {
                                                                                                                                                                          //Natural: PERFORM SUMMARY-EVALUATION
                sub_Summary_Evaluation();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape())) return;
            if (condition(! (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().isBreak())))                                                                      //Natural: IF NOT BREAK OF #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-SETTL-SYSTEM
                sub_Determine_Settl_System();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
                sub_Initialize_Header();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INCREMENT-INDEX
                sub_Increment_Index();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  05-18-98
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM RESET-VARIABLES
            sub_Reset_Variables();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Old_Csr_Cde.setValue(readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld);                                                                                         //Natural: ASSIGN #OLD-CSR-CDE := OLD ( #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE )
            //*  05-18-98
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
