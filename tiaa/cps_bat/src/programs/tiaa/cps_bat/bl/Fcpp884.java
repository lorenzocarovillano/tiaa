/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:17 PM
**        * FROM NATURAL PROGRAM : Fcpp884
************************************************************
**        * FILE NAME            : Fcpp884.java
**        * CLASS NAME           : Fcpp884
**        * INSTANCE NAME        : Fcpp884
************************************************************
************************************************************************
* PROGRAM   : FCPP884
* SYSTEM    : CPS
* TITLE     : PAYMENT REGISTERS
* GENERATED :
* FUNCTION  : GENERATE PAYMENT REGISTER FOR AP REDRAW/CANCEL/STOP CHECKS
************************************************************************
* NOTE: WHEN MAKING CHANGES TO THIS MODULE, PLEASE CHECK TO SEE IF THE
*       SAME CHANGES NEED TO BE MADE TO FCPP884M.  THAT MODULE WAS
*       CLONED FROM THIS ONE SO THAT THE MONTHLY JOBSTREAM COULD WRITE
*       A LARGER RECORD WITHOUT DISTURBING THIS MODULE.  THIS PROGRAM
*       RUNS IN THE 1400 JOBSTREAM AND DOES NOT CURRENTLY NEED TO WRITE
*       ROTH FIELDS.
*
*       AER - 5/15/08
************************************************************************
*           :
* HISTORY   : CHANGED THE PROGRAM TO FIX THE CONTRACT AMT COLUMN IN THE
*           : REDRAW PAYMENT REGISTER   G. PHILIP 3/28/98
*
* 12/22/98  : A. YOUNG  - REVISED CODE TO COMBINE CANCEL, STOP, AND
*                         REDRAW REGISTERS.
*
* 08/05/99   - LEON GURTOVNIK
*              MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*              NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
* 06/06/2006 : LANDRUM  PAYEE MATCH
*              - INCREASED CHECK NBR TO N10.
*
* 05/15/2008 : AER - ROTH-MAJOR1 - CHANGED FCPA800 TO FCPA800D
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 4/2017   : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp884 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800d pdaFcpa800d;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Pymnt_Nbr;

    private DbsGroup pnd_Pymnt_Nbr__R_Field_1;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Type;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Num;

    private DbsGroup pnd_Pymnt_Nbr__R_Field_2;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7;
    private DbsField pnd_Input_Parm;
    private DbsField pnd_Index;
    private DbsField pnd_Contract_Amt;
    private DbsField pnd_Dvdnd_Amt;
    private DbsField pnd_Tax_Amt;
    private DbsField pnd_Ded_Amt;
    private DbsField pnd_Net_Amt;
    private DbsField pnd_Total_Lit;

    private DbsGroup totals;
    private DbsField totals_Pnd_T_Count;
    private DbsField totals_Pnd_T_Cntrct;
    private DbsField totals_Pnd_T_Dvdnd;
    private DbsField totals_Pnd_T_Tax;
    private DbsField totals_Pnd_T_Ded;
    private DbsField totals_Pnd_T_Net;
    private DbsField pnd_Csr_Desc;
    private DbsField pnd_Hdr_Desc;
    private DbsField pnd_Hdr_Dte;
    private DbsField pnd_Save_Pymnt_Num;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Pymnt_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Nbr", "#PYMNT-NBR", FieldType.STRING, 11);

        pnd_Pymnt_Nbr__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_Nbr__R_Field_1", "REDEFINE", pnd_Pymnt_Nbr);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Type = pnd_Pymnt_Nbr__R_Field_1.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Type", "#PYMNT-TYPE", FieldType.STRING, 1);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num = pnd_Pymnt_Nbr__R_Field_1.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Num", "#PYMNT-NUM", FieldType.NUMERIC, 10);

        pnd_Pymnt_Nbr__R_Field_2 = pnd_Pymnt_Nbr__R_Field_1.newGroupInGroup("pnd_Pymnt_Nbr__R_Field_2", "REDEFINE", pnd_Pymnt_Nbr_Pnd_Pymnt_Num);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3 = pnd_Pymnt_Nbr__R_Field_2.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3", "#PYMNT-NUM-N3", FieldType.NUMERIC, 
            3);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7 = pnd_Pymnt_Nbr__R_Field_2.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7", "#PYMNT-NUM-N7", FieldType.NUMERIC, 
            7);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 20);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Contract_Amt = localVariables.newFieldInRecord("pnd_Contract_Amt", "#CONTRACT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Dvdnd_Amt", "#DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ded_Amt = localVariables.newFieldInRecord("pnd_Ded_Amt", "#DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Net_Amt = localVariables.newFieldInRecord("pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Lit = localVariables.newFieldInRecord("pnd_Total_Lit", "#TOTAL-LIT", FieldType.STRING, 25);

        totals = localVariables.newGroupArrayInRecord("totals", "TOTALS", new DbsArrayController(1, 2));
        totals_Pnd_T_Count = totals.newFieldInGroup("totals_Pnd_T_Count", "#T-COUNT", FieldType.PACKED_DECIMAL, 7);
        totals_Pnd_T_Cntrct = totals.newFieldInGroup("totals_Pnd_T_Cntrct", "#T-CNTRCT", FieldType.PACKED_DECIMAL, 11, 2);
        totals_Pnd_T_Dvdnd = totals.newFieldInGroup("totals_Pnd_T_Dvdnd", "#T-DVDND", FieldType.PACKED_DECIMAL, 11, 2);
        totals_Pnd_T_Tax = totals.newFieldInGroup("totals_Pnd_T_Tax", "#T-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        totals_Pnd_T_Ded = totals.newFieldInGroup("totals_Pnd_T_Ded", "#T-DED", FieldType.PACKED_DECIMAL, 11, 2);
        totals_Pnd_T_Net = totals.newFieldInGroup("totals_Pnd_T_Net", "#T-NET", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Csr_Desc = localVariables.newFieldInRecord("pnd_Csr_Desc", "#CSR-DESC", FieldType.STRING, 16);
        pnd_Hdr_Desc = localVariables.newFieldInRecord("pnd_Hdr_Desc", "#HDR-DESC", FieldType.STRING, 70);
        pnd_Hdr_Dte = localVariables.newFieldInRecord("pnd_Hdr_Dte", "#HDR-DTE", FieldType.STRING, 18);
        pnd_Save_Pymnt_Num = localVariables.newFieldInRecord("pnd_Save_Pymnt_Num", "#SAVE-PYMNT-NUM", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp884() throws Exception
    {
        super("Fcpp884");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp884|Main");
        OnErrorManager.pushEvent("FCPP884", onError);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 01 ) LS = 132 PS = 55;//Natural: FORMAT ( 02 ) LS = 132 PS = 55
                //*  12-22-98 : A. YOUNG
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=MITL'_'"), new SignPosition (true), new InputPrompting(false),                //Natural: INPUT ( AD = MITL'_' SG = ON IP = OFF ZP = OFF ) 'Input Parm:' #INPUT-PARM
                    new ReportZeroPrint (false),"Input Parm:",pnd_Input_Parm);
                //*                                                                                                                                                       //Natural: ON ERROR
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 45T #HDR-DESC //
                //*  RL PAYEE MATCH MAY 26, 2006
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
                sub_Get_Check_Formatting_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  READ PAYMENT/NAME AND ADDRESS WORK FILE
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(1, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
                {
                    CheckAtStartofData605();

                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*                                                                                                                                                   //Natural: AT START OF DATA
                    //*                                                                                                                                                   //Natural: AT BREAK OF WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NUM
                    //*  12-22-98 : A. YOUNG
                    //*                                                                                                                                                   //Natural: AT BREAK OF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
                    pnd_Dvdnd_Amt.reset();                                                                                                                                //Natural: RESET #DVDND-AMT
                    FOR01:                                                                                                                                                //Natural: FOR #INDEX 1 INV-ACCT-COUNT
                    for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
                    {
                        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG")  //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T' OR = 'TG' OR = 'G '
                            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G ")))
                        {
                            pnd_Dvdnd_Amt.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                                //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Dvdnd_Amt.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #DVDND-AMT
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *  THIS IF STATEMENT COMM OUT AND REPLACED BY THE FOLLOWING FOR LOOP
                    //* *  TO FIX REDRAW PAYMENT REGISTER, COL "contract amt" G.P 3/28/98
                    //* *#CONTRACT-AMT :=
                    //* *  WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT (1:INV-ACCT-COUNT) + 0
                    //*  ADDED NEW. WAS ACCUMULATING G.P 10/9/98
                    //*  NEW G.PHILIP  3/28/98
                    pnd_Contract_Amt.reset();                                                                                                                             //Natural: RESET #CONTRACT-AMT
                    FOR02:                                                                                                                                                //Natural: FOR #INDEX 1 INV-ACCT-COUNT
                    for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
                    {
                        //*  VF 06/99
                        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(pnd_Index).equals("T")))                                               //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( #INDEX ) = 'T'
                        {
                            //*  REAL ESTATE SHOULDN't be added
                            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).notEquals("R")))                                      //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) NE 'R'
                            {
                                pnd_Contract_Amt.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Index));                                        //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( #INDEX ) TO #CONTRACT-AMT
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,                   //Natural: ASSIGN #TAX-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT ) + 0
                        ":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
                    pnd_Tax_Amt.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));        //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #TAX-AMT
                    pnd_Tax_Amt.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));        //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #TAX-AMT
                    pnd_Ded_Amt.compute(new ComputeParameters(false, pnd_Ded_Amt), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").add(getZero()));        //Natural: ASSIGN #DED-AMT := WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) + 0
                    pnd_Net_Amt.compute(new ComputeParameters(false, pnd_Net_Amt), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,                  //Natural: ASSIGN #NET-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT ) + 0
                        ":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
                    //* ********************* RL PAYEE MATCH BEGIN MAY 26 2006 ****************
                    //*  CHECK
                    if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                   //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 1
                    {
                        pnd_Index.setValue(1);                                                                                                                            //Natural: MOVE 1 TO #INDEX
                        pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("C");                                                                                                       //Natural: MOVE 'C' TO #PYMNT-TYPE
                        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().less(getZero())))                                                                //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR LT 0
                        {
                            pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                       //Natural: ASSIGN #PYMNT-NUM-N3 := FCPA110.START-CHECK-PREFIX-N3
                            pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.compute(new ComputeParameters(false, pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().multiply(-1)); //Natural: COMPUTE #PYMNT-NUM-N7 = WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR * -1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().greater(getZero())))                                                         //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR GT 0
                            {
                                pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.reset();                                                                                                   //Natural: RESET #PYMNT-NUM-N3
                                pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                              //Natural: ASSIGN #PYMNT-NUM-N7 := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Save_Pymnt_Num.less(getZero())))                                                                                        //Natural: IF #SAVE-PYMNT-NUM LT 0
                                {
                                    pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                               //Natural: ASSIGN #PYMNT-NUM-N3 := FCPA110.START-CHECK-PREFIX-N3
                                    pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.compute(new ComputeParameters(false, pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7), pnd_Save_Pymnt_Num.multiply(-1)); //Natural: COMPUTE #PYMNT-NUM-N7 = #SAVE-PYMNT-NUM * -1
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(pnd_Save_Pymnt_Num.greater(getZero())))                                                                                 //Natural: IF #SAVE-PYMNT-NUM GT 0
                                    {
                                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.reset();                                                                                           //Natural: RESET #PYMNT-NUM-N3
                                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.setValue(pnd_Save_Pymnt_Num);                                                                      //Natural: ASSIGN #PYMNT-NUM-N7 := #SAVE-PYMNT-NUM
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        //*  N10
                                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num.reset();                                                                                              //Natural: RESET #PYMNT-NUM
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //* ******************** RL MAY 26 2006  END **************************
                        //*     VF
                        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                     //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 01
                        {
                            totals_Pnd_T_Count.getValue(pnd_Index).nadd(1);                                                                                               //Natural: ADD 1 TO #T-COUNT ( #INDEX )
                        }                                                                                                                                                 //Natural: END-IF
                        totals_Pnd_T_Cntrct.getValue(pnd_Index).nadd(pnd_Contract_Amt);                                                                                   //Natural: ADD #CONTRACT-AMT TO #T-CNTRCT ( #INDEX )
                        totals_Pnd_T_Dvdnd.getValue(pnd_Index).nadd(pnd_Dvdnd_Amt);                                                                                       //Natural: ADD #DVDND-AMT TO #T-DVDND ( #INDEX )
                        totals_Pnd_T_Tax.getValue(pnd_Index).nadd(pnd_Tax_Amt);                                                                                           //Natural: ADD #TAX-AMT TO #T-TAX ( #INDEX )
                        totals_Pnd_T_Ded.getValue(pnd_Index).nadd(pnd_Ded_Amt);                                                                                           //Natural: ADD #DED-AMT TO #T-DED ( #INDEX )
                        totals_Pnd_T_Net.getValue(pnd_Index).nadd(pnd_Net_Amt);                                                                                           //Natural: ADD #NET-AMT TO #T-NET ( #INDEX )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (A11) RL PAYEE-MATCH
                    getReports().display(1, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                       //Natural: DISPLAY ( 01 ) ( UC = - ) 'Contract/Number' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR 'Combine/Contract Num' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR 'Geo/Cde' WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE 'Contract/Amount' #CONTRACT-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'CREF Amount/or Dividend' #DVDND-AMT ( EM = Z,ZZZ,ZZ9.99- ) ' /Taxes' #TAX-AMT ( EM = ZZZ,ZZ9.99- ) ' /Deductns' #DED-AMT ( EM = ZZ,ZZ9.99- ) ' /Net Payment' #NET-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Combined/Check Amount' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'PAYMENT/NUMBER' #PYMNT-NBR ' /Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ( AL = 10 )
                    		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
                    		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
                    		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"Contract/Amount",
                    		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
                    		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
                    		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
                    		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
                    		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
                    		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"PAYMENT/NUMBER",
                    		pnd_Pymnt_Nbr," /Payee",
                    		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), new AlphanumericLength (10));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *    'Payment/Number'          #PYMNT-NBR
                    //* *    ' /Payee'                 WF-PYMNT-ADDR-GRP.PYMNT-NME (1) (AL=13)
                    //*  AT END OF DATA
                    //*    PERFORM PRINT-TOTALS
                    //*  END-ENDDATA
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (Global.isEscape()) return;
                //*  IF #T-COUNT(1) < 1
                //*    WRITE (01) ///
                //*      30T '********** NO DATA FOUND **********'
                //*  END-IF
                //* *---------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-HEADER
                //* *-------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-VARIABLES
                //* *----------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTALS
                //* *----------------------------
                //* ********************** RL MAY 26, 2006 BEGIN ************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
                //*  RL PAYEE MATCH
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //*  RL PAYEE MATCH
                //* ************************** RL END-PAYEE MATCH *************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    //*  12-22-98
    private void sub_Initialize_Header() throws Exception                                                                                                                 //Natural: INITIALIZE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  LEON 08-10-99
        //*  JWO 2010-99
        //*  LEON 08-10-99
        //*  JWO 2010-99
        //*  JWO 2010-99
        //*  JWO 2010-99
        short decideConditionsMet773 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C'
        if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("CANCELLED       ");                                                                                                                    //Natural: ASSIGN #CSR-DESC := 'CANCELLED       '
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("REDRAWN         ");                                                                                                                    //Natural: ASSIGN #CSR-DESC := 'REDRAWN         '
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("STOPPED         ");                                                                                                                    //Natural: ASSIGN #CSR-DESC := 'STOPPED         '
        }                                                                                                                                                                 //Natural: VALUE 'CN'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("CANCEL NO REDRAW");                                                                                                                    //Natural: ASSIGN #CSR-DESC := 'CANCEL NO REDRAW'
        }                                                                                                                                                                 //Natural: VALUE 'CP'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("CANCEL PEND");                                                                                                                         //Natural: ASSIGN #CSR-DESC := 'CANCEL PEND'
        }                                                                                                                                                                 //Natural: VALUE 'SN'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("STOP NO REDRAW  ");                                                                                                                    //Natural: ASSIGN #CSR-DESC := 'STOP NO REDRAW  '
        }                                                                                                                                                                 //Natural: VALUE 'SP'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("STOP PEND  ");                                                                                                                         //Natural: ASSIGN #CSR-DESC := 'STOP PEND  '
        }                                                                                                                                                                 //Natural: VALUE 'RP'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("REDRAW PEND  ");                                                                                                                       //Natural: ASSIGN #CSR-DESC := 'REDRAW PEND  '
        }                                                                                                                                                                 //Natural: VALUE 'PR'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
        {
            decideConditionsMet773++;
            pnd_Csr_Desc.setValue("PENDED RETURN");                                                                                                                       //Natural: ASSIGN #CSR-DESC := 'PENDED RETURN'
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet773 > 0))
        {
            pnd_Hdr_Desc.setValue(DbsUtil.compress(pnd_Csr_Desc, "PAYMENT REGISTER"));                                                                                    //Natural: COMPRESS #CSR-DESC 'PAYMENT REGISTER' INTO #HDR-DESC
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Input_Parm.getSubstring(1,12).equals("END-OF-MONTH")))                                                                                          //Natural: IF SUBSTR ( #INPUT-PARM,1,12 ) = 'END-OF-MONTH'
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().notEquals(getZero())))                                                                       //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE NE 0
            {
                pnd_Hdr_Dte.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("LLLLLLLLL', 'YYYY"));                                   //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) TO #HDR-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hdr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("LLLLLLLLL' 'DD', 'YYYY"));                                                                    //Natural: MOVE EDITED *DATX ( EM = LLLLLLLLL' 'DD', 'YYYY ) TO #HDR-DTE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hdr_Desc.setValue(DbsUtil.compress(pnd_Hdr_Desc, "for", pnd_Hdr_Dte));                                                                                        //Natural: COMPRESS #HDR-DESC 'for' #HDR-DTE INTO #HDR-DESC
    }
    //*  12-22-98
    private void sub_Reset_Variables() throws Exception                                                                                                                   //Natural: RESET-VARIABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        //*  RL PAYEE MATCH
        pnd_Contract_Amt.reset();                                                                                                                                         //Natural: RESET #CONTRACT-AMT #DVDND-AMT #TAX-AMT #DED-AMT #NET-AMT #TOTAL-LIT TOTALS ( * ) #PYMNT-NBR #CSR-DESC #HDR-DESC #HDR-DTE #SAVE-PYMNT-NUM
        pnd_Dvdnd_Amt.reset();
        pnd_Tax_Amt.reset();
        pnd_Ded_Amt.reset();
        pnd_Net_Amt.reset();
        pnd_Total_Lit.reset();
        totals.getValue("*").reset();
        pnd_Pymnt_Nbr.reset();
        pnd_Csr_Desc.reset();
        pnd_Hdr_Desc.reset();
        pnd_Hdr_Dte.reset();
        pnd_Save_Pymnt_Num.reset();
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        totals_Pnd_T_Count.getValue(2).setValue(totals_Pnd_T_Count.getValue(1));                                                                                          //Natural: ASSIGN #T-COUNT ( 2 ) := #T-COUNT ( 1 )
        totals_Pnd_T_Cntrct.getValue(2).setValue(totals_Pnd_T_Cntrct.getValue(1));                                                                                        //Natural: ASSIGN #T-CNTRCT ( 2 ) := #T-CNTRCT ( 1 )
        totals_Pnd_T_Dvdnd.getValue(2).setValue(totals_Pnd_T_Dvdnd.getValue(1));                                                                                          //Natural: ASSIGN #T-DVDND ( 2 ) := #T-DVDND ( 1 )
        totals_Pnd_T_Tax.getValue(2).setValue(totals_Pnd_T_Tax.getValue(1));                                                                                              //Natural: ASSIGN #T-TAX ( 2 ) := #T-TAX ( 1 )
        totals_Pnd_T_Ded.getValue(2).setValue(totals_Pnd_T_Ded.getValue(1));                                                                                              //Natural: ASSIGN #T-DED ( 2 ) := #T-DED ( 1 )
        totals_Pnd_T_Net.getValue(2).setValue(totals_Pnd_T_Net.getValue(1));                                                                                              //Natural: ASSIGN #T-NET ( 2 ) := #T-NET ( 1 )
        //*  EJECT  (01)
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(30),"     Total            Total                            Total   ",NEWLINE,new                    //Natural: WRITE ( 01 ) /// 30T '     Total            Total                            Total   ' / 30T 'Contract Amount   CREF or Dvdnd     Total Taxes      Deductions' '    Net Payment    Total Count'
            TabSetting(30),"Contract Amount   CREF or Dvdnd     Total Taxes      Deductions","    Net Payment    Total Count");
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #INDEX 1 2
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(2)); pnd_Index.nadd(1))
        {
            //*  CHECK
            short decideConditionsMet827 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #INDEX;//Natural: VALUE 1
            if (condition((pnd_Index.equals(1))))
            {
                decideConditionsMet827++;
                pnd_Total_Lit.setValue("Total Check Payments    ");                                                                                                       //Natural: MOVE 'Total Check Payments    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Index.equals(2))))
            {
                decideConditionsMet827++;
                pnd_Total_Lit.setValue("Grand Total Payments    ");                                                                                                       //Natural: MOVE 'Grand Total Payments    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,pnd_Total_Lit,new ColumnSpacing(5),totals_Pnd_T_Cntrct.getValue(pnd_Index), new ReportEditMask                  //Natural: WRITE ( 01 ) /// #TOTAL-LIT 5X #T-CNTRCT ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 1X #T-DVDND ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 1X #T-TAX ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 1X #T-DED ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 1X #T-NET ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 5X #T-COUNT ( #INDEX ) ( EM = Z,ZZZ,ZZ9 )
                ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Dvdnd.getValue(pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Tax.getValue(pnd_Index), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Ded.getValue(pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
                ColumnSpacing(1),totals_Pnd_T_Net.getValue(pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(5),totals_Pnd_T_Count.getValue(pnd_Index), 
                new ReportEditMask ("Z,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  VF 06/99
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pdaFcpa800d_getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NumIsBreak = pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num().isBreak(endOfData);
        boolean pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak = pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().isBreak(endOfData);
        if (condition(pdaFcpa800d_getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_NumIsBreak || pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
            //*  12-22-98 : A. YOUNG
            if (condition(! (pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().isBreak())))                                                                //Natural: IF NOT BREAK OF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            {
                pnd_Save_Pymnt_Num.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                          //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #SAVE-PYMNT-NUM
                //*  CHECK
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                       //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 1
                {
                    pnd_Index.setValue(1);                                                                                                                                //Natural: MOVE 1 TO #INDEX
                    //*          ADD 1  TO #T-COUNT  (#INDEX)   /* VF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
            sub_Print_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESET-VARIABLES
            sub_Reset_Variables();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
            sub_Initialize_Header();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Save_Pymnt_Num.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                              //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #SAVE-PYMNT-NUM
            //*  CHECK
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                           //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 1
            {
                pnd_Index.setValue(1);                                                                                                                                    //Natural: MOVE 1 TO #INDEX
                //*        ADD  1 TO #T-COUNT (#INDEX)      /* VF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(45),pnd_Hdr_Desc,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
        		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"Contract/Amount",
        		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"PAYMENT/NUMBER",
        		pnd_Pymnt_Nbr," /Payee",
        		pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Nme(), new AlphanumericLength (10));
    }
    private void CheckAtStartofData605() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*  12-22-98
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
            sub_Initialize_Header();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Save_Pymnt_Num.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                              //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #SAVE-PYMNT-NUM
        }                                                                                                                                                                 //Natural: END-START
    }
}
