/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:21 PM
**        * FROM NATURAL PROGRAM : Fcpp380
************************************************************
**        * FILE NAME            : Fcpp380.java
**        * CLASS NAME           : Fcpp380
**        * INSTANCE NAME        : Fcpp380
************************************************************
************************************************************************
* PROGRAM  : FCPP380
* SYSTEM   : CPS
* TITLE    : ANNUITANT STATEMENT
* GENERATED: JUL 26,93 AT 10:04 AM
* FUNCTION : THIS PROGRAM WILL BUILD ANNUITANT STATEMENT.
*
* HISTORY  : 05/03/1994   RITA SALGADO
*          : ADD SUSPENSE NOOTIFICATION FOR SUSPENSE REPURCHASE
*
*          : 05/26/98     R. CARREON
*          : ADD PROCESSING OF IRA
*
*          : 03/12/99     R. CARREON
*            RESTOW - CHANGE IN FCPL378
*
*          : 03/18/02     T. MCGEE
*            REPAIR THREE PAGE XEROX COMMAND "DUP"
*
*          : 11/02        ROXAN
*            FCPL378 EXPANDED FOR EGTRRA
************************************************************************
* R. LANDRUM    01/05/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
* 4/2017 : JJG - PIN EXPANSION RESTOW
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp380 extends BLNatBase
{
    // Data Areas
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpasscn pdaFcpasscn;
    private PdaFcpassc1 pdaFcpassc1;
    private PdaFcpassc2 pdaFcpassc2;
    private LdaFcplssc1 ldaFcplssc1;
    private LdaFcpl378 ldaFcpl378;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;
    private LdaFcpl876b ldaFcpl876b;
    private LdaFcplbar1 ldaFcplbar1;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_1;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Next_Check_Nbr;
    private DbsField pnd_Ws_Next_Eft_Nbr;
    private DbsField pnd_Ws_Next_Global_Nbr;
    private DbsField pnd_Ws_Next_Int_Rollover_Nbr;
    private DbsField pnd_Ws_Current_Pymnt_Id;
    private DbsField pnd_Ws_Current_Pymnt_Seq_Nbr;
    private DbsField pnd_Abend_Cde;
    private DbsField pnd_Ws_First_Xerox;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Save_S_D_M_Plex;
    private DbsField pnd_Ws_Format_Notification_Letter;
    private DbsField pnd_Ws_Printed_Institution_Amt;
    private DbsField pnd_Ws_Bottom_Part;
    private DbsField pnd_Ws_Jde;
    private DbsField pnd_Ws_Format;
    private DbsField pnd_Ws_Forms;
    private DbsField pnd_Ws_Feed;
    private DbsField pnd_Ws_Max;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ws_Rec_1;
    private DbsField pnd_Ws_Pnd_Run_Type;
    private DbsField pnd_Ws_Pnd_Bar_Name;
    private DbsField pnd_Ws_Pnd_Check_Ind;
    private DbsField pnd_Ws_Pnd_Statement_Ind;
    private DbsField pnd_Ws_Pnd_Break_If_Acfs;
    private DbsField pnd_Ws_Pnd_Ws_Acfs;
    private DbsField pnd_Ws_Pnd_Pymnt_Break_Ind;
    private DbsField pnd_Ws_Pnd_Bar_Sub;
    private DbsField pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Work_Rec;

    private DbsGroup pnd_Ws_Work_Rec__R_Field_2;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Key;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2;
    private DbsField pnd_Ws_Header;
    private DbsField pnd_Ws_Occurs;
    private DbsField pnd_Ws_Name_N_Address;

    private DbsGroup pnd_Ws_Name_N_Address__R_Field_3;

    private DbsGroup pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2;
    private DbsField pnd_Ws_Name_N_Address__Filler1;

    private DbsGroup pnd_Ws_Name_N_Address_Nme_N_Addr;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Nme;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Ws_Name_N_Address__Filler2;
    private DbsField pnd_Ws_Name_N_Address__Filler3;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Ws_Name_N_Address__Filler4;
    private DbsField pnd_First_Page;
    private DbsField pnd_Last_Page;
    private DbsField pnd_Ws_Ira_Int;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Pymnt_Break_IndOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpa110 = new PdaFcpa110(localVariables);
        pdaFcpasscn = new PdaFcpasscn(localVariables);
        pdaFcpassc1 = new PdaFcpassc1(localVariables);
        pdaFcpassc2 = new PdaFcpassc2(localVariables);
        ldaFcplssc1 = new LdaFcplssc1();
        registerRecord(ldaFcplssc1);
        ldaFcpl378 = new LdaFcpl378();
        registerRecord(ldaFcpl378);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);
        ldaFcpl876b = new LdaFcpl876b();
        registerRecord(ldaFcpl876b);
        ldaFcplbar1 = new LdaFcplbar1();
        registerRecord(ldaFcplbar1);

        // Local Variables
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_1", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Next_Check_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Nbr", "#WS-NEXT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Eft_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Eft_Nbr", "#WS-NEXT-EFT-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Global_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Global_Nbr", "#WS-NEXT-GLOBAL-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Int_Rollover_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Int_Rollover_Nbr", "#WS-NEXT-INT-ROLLOVER-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Current_Pymnt_Id = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Id", "#WS-CURRENT-PYMNT-ID", FieldType.NUMERIC, 7);
        pnd_Ws_Current_Pymnt_Seq_Nbr = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Seq_Nbr", "#WS-CURRENT-PYMNT-SEQ-NBR", FieldType.NUMERIC, 
            7);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);
        pnd_Ws_First_Xerox = localVariables.newFieldInRecord("pnd_Ws_First_Xerox", "#WS-FIRST-XEROX", FieldType.STRING, 1);
        pnd_Ws_Side_Printed = localVariables.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Cntr_Inv_Acct = localVariables.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Sim_Dup_Multiplex_Written = localVariables.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Save_S_D_M_Plex = localVariables.newFieldInRecord("pnd_Ws_Save_S_D_M_Plex", "#WS-SAVE-S-D-M-PLEX", FieldType.STRING, 1);
        pnd_Ws_Format_Notification_Letter = localVariables.newFieldInRecord("pnd_Ws_Format_Notification_Letter", "#WS-FORMAT-NOTIFICATION-LETTER", FieldType.STRING, 
            1);
        pnd_Ws_Printed_Institution_Amt = localVariables.newFieldInRecord("pnd_Ws_Printed_Institution_Amt", "#WS-PRINTED-INSTITUTION-AMT", FieldType.STRING, 
            1);
        pnd_Ws_Bottom_Part = localVariables.newFieldInRecord("pnd_Ws_Bottom_Part", "#WS-BOTTOM-PART", FieldType.STRING, 1);
        pnd_Ws_Jde = localVariables.newFieldInRecord("pnd_Ws_Jde", "#WS-JDE", FieldType.STRING, 3);
        pnd_Ws_Format = localVariables.newFieldInRecord("pnd_Ws_Format", "#WS-FORMAT", FieldType.STRING, 6);
        pnd_Ws_Forms = localVariables.newFieldInRecord("pnd_Ws_Forms", "#WS-FORMS", FieldType.STRING, 6);
        pnd_Ws_Feed = localVariables.newFieldInRecord("pnd_Ws_Feed", "#WS-FEED", FieldType.STRING, 6);
        pnd_Ws_Max = localVariables.newFieldInRecord("pnd_Ws_Max", "#WS-MAX", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ws_Rec_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_Ws_Pnd_Run_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 8);
        pnd_Ws_Pnd_Bar_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Bar_Name", "#BAR-NAME", FieldType.STRING, 38);
        pnd_Ws_Pnd_Check_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Check_Ind", "#CHECK-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Statement_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Statement_Ind", "#STATEMENT-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Break_If_Acfs = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Break_If_Acfs", "#BREAK-IF-ACFS", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ws_Acfs = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Acfs", "#WS-ACFS", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Pymnt_Break_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Break_Ind", "#PYMNT-BREAK-IND", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Bar_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Bar_Sub", "#BAR-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt", "#WS-INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Ws_Work_Rec = localVariables.newFieldArrayInRecord("pnd_Ws_Work_Rec", "#WS-WORK-REC", FieldType.STRING, 250, new DbsArrayController(1, 2));

        pnd_Ws_Work_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Work_Rec__R_Field_2", "REDEFINE", pnd_Ws_Work_Rec);
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Key = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Key", "#WS-WORK-REC-KEY", FieldType.STRING, 
            41);
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1 = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1", "#WS-WORK-REC-DET-1", 
            FieldType.STRING, 250);
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2 = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2", "#WS-WORK-REC-DET-2", 
            FieldType.STRING, 209);
        pnd_Ws_Header = localVariables.newFieldArrayInRecord("pnd_Ws_Header", "#WS-HEADER", FieldType.STRING, 250, new DbsArrayController(1, 2));
        pnd_Ws_Occurs = localVariables.newFieldArrayInRecord("pnd_Ws_Occurs", "#WS-OCCURS", FieldType.STRING, 250, new DbsArrayController(1, 80));
        pnd_Ws_Name_N_Address = localVariables.newFieldArrayInRecord("pnd_Ws_Name_N_Address", "#WS-NAME-N-ADDRESS", FieldType.STRING, 250, new DbsArrayController(1, 
            4));

        pnd_Ws_Name_N_Address__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Name_N_Address__R_Field_3", "REDEFINE", pnd_Ws_Name_N_Address);

        pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2 = pnd_Ws_Name_N_Address__R_Field_3.newGroupArrayInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2", 
            "#WS-NAME-N-ADDR-LEVEL2", new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address__Filler1 = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address__Filler1", "_FILLER1", 
            FieldType.STRING, 111);

        pnd_Ws_Name_N_Address_Nme_N_Addr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newGroupInGroup("pnd_Ws_Name_N_Address_Nme_N_Addr", "NME-N-ADDR");
        pnd_Ws_Name_N_Address_Pymnt_Nme = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address__Filler2 = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address__Filler2", "_FILLER2", FieldType.STRING, 
            57);
        pnd_Ws_Name_N_Address__Filler3 = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address__Filler3", "_FILLER3", 
            FieldType.STRING, 9);
        pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr", 
            "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        pnd_Ws_Name_N_Address__Filler4 = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address__Filler4", "_FILLER4", 
            FieldType.STRING, 54);
        pnd_First_Page = localVariables.newFieldInRecord("pnd_First_Page", "#FIRST-PAGE", FieldType.BOOLEAN, 1);
        pnd_Last_Page = localVariables.newFieldInRecord("pnd_Last_Page", "#LAST-PAGE", FieldType.BOOLEAN, 1);
        pnd_Ws_Ira_Int = localVariables.newFieldInRecord("pnd_Ws_Ira_Int", "#WS-IRA-INT", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Pymnt_Break_IndOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Pymnt_Break_Ind_OLD", "Pnd_Pymnt_Break_Ind_OLD", FieldType.PACKED_DECIMAL, 
            7);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcplssc1.initializeValues();
        ldaFcpl378.initializeValues();
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();
        ldaFcpl876b.initializeValues();
        ldaFcplbar1.initializeValues();

        localVariables.reset();
        pnd_Ws_First_Xerox.setInitialValue("Y");
        pnd_Ws_Cntr_Inv_Acct.setInitialValue(0);
        pnd_Ws_Format_Notification_Letter.setInitialValue("N");
        pnd_Ws_Printed_Institution_Amt.setInitialValue("N");
        pnd_Ws_Bottom_Part.setInitialValue(" ");
        pnd_Ws_Jde.setInitialValue(" ");
        pnd_Ws_Format.setInitialValue(" ");
        pnd_Ws_Forms.setInitialValue(" ");
        pnd_Ws_Feed.setInitialValue(" ");
        pnd_Ws_Max.setInitialValue(0);
        pnd_I.setInitialValue(0);
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Rec_1.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp380() throws Exception
    {
        super("Fcpp380");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 15 ) LS = 133 PS = 58 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        getWorkFiles().read(2, pnd_Ws_Pnd_Run_Type);                                                                                                                      //Natural: READ WORK FILE 2 ONCE #RUN-TYPE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'RUN TYPE' PARAMETER FILE",new TabSetting(77),"***");                                                 //Natural: WRITE '***' 25T 'MISSING "RUN TYPE" PARAMETER FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(pnd_Ws_Pnd_Run_Type.equals("HELD") || pnd_Ws_Pnd_Run_Type.equals("HOLD")))                                                                          //Natural: IF #RUN-TYPE = 'HELD' OR = 'HOLD'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().read(4, pdaFcpassc2.getPnd_Fcpassc2());                                                                                                        //Natural: READ WORK FILE 4 ONCE #FCPASSC2
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"MISSING 'HOLD CHECK' CONTROL FILE",new TabSetting(77),"***");                                             //Natural: WRITE '***' 25T 'MISSING "HOLD CHECK" CONTROL FILE' 77T '***'
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(50);  if (true) return;                                                                                                                 //Natural: TERMINATE 50
            }                                                                                                                                                             //Natural: END-ENDFILE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().read(6, ldaFcpl876.getPnd_Fcpl876());                                                                                                              //Natural: READ WORK FILE 6 ONCE #FCPL876
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'NEXT CHECK' FILE",new TabSetting(77),"***");                                                         //Natural: WRITE '***' 25T 'MISSING "NEXT CHECK" FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-ENDFILE
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue("*").setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Barcode().getValue("*"));                               //Natural: ASSIGN #BARCODE-LDA.#BAR-BARCODE ( * ) := #FCPL876.#BAR-BARCODE ( * )
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes().setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes());                                                       //Natural: ASSIGN #BARCODE-LDA.#BAR-ENVELOPES := #FCPL876.#BAR-ENVELOPES
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().getBoolean());                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := #FCPL876.#BAR-NEW-RUN
        pnd_Ws_Current_Pymnt_Seq_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                                           //Natural: ASSIGN #WS-CURRENT-PYMNT-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
        pnd_Ws_Next_Check_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                                      //Natural: ASSIGN #WS-NEXT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
        pnd_Ws_Next_Eft_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr());                                                                                  //Natural: ASSIGN #WS-NEXT-EFT-NBR := #FCPL876.PYMNT-CHECK-SCRTY-NBR
        pnd_Ws_Next_Global_Nbr.setValue(1);                                                                                                                               //Natural: ASSIGN #WS-NEXT-GLOBAL-NBR := 1
        pnd_Ws_Next_Int_Rollover_Nbr.setValue(1);                                                                                                                         //Natural: ASSIGN #WS-NEXT-INT-ROLLOVER-NBR := 1
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPASSC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 6 )
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
        //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"at start of program:",NEWLINE,"The program is going to use the following:",NEWLINE,                   //Natural: WRITE *PROGRAM *TIME 'at start of program:' / 'The program is going to use the following:' / 'start assigned check   number          :' #WS-NEXT-CHECK-NBR / 'start assigned EFT     number          :' #WS-NEXT-EFT-NBR / 'start assigned Global  number          :' #WS-NEXT-GLOBAL-NBR / 'start assigned Internal Rollover number:' #WS-NEXT-INT-ROLLOVER-NBR / 'start sequence number..................:' #WS-CURRENT-PYMNT-SEQ-NBR
            "start assigned check   number          :",pnd_Ws_Next_Check_Nbr,NEWLINE,"start assigned EFT     number          :",pnd_Ws_Next_Eft_Nbr,NEWLINE,
            "start assigned Global  number          :",pnd_Ws_Next_Global_Nbr,NEWLINE,"start assigned Internal Rollover number:",pnd_Ws_Next_Int_Rollover_Nbr,
            NEWLINE,"start sequence number..................:",pnd_Ws_Current_Pymnt_Seq_Nbr);
        if (Global.isEscape()) return;
        //*  ADJUST CURRENT-PYMNT-SEQ-NBR FOR THE FIRST TIME
        pnd_Ws_Current_Pymnt_Seq_Nbr.nsubtract(1);                                                                                                                        //Natural: SUBTRACT 1 FROM #WS-CURRENT-PYMNT-SEQ-NBR
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #PYMNT-EXT-KEY #WS-WORK-REC-DET-1 #WS-WORK-REC-DET-2
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl378.getPnd_Pymnt_Ext_Key(), pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1, pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2)))
        {
            CheckAtStartofData676();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            if (condition(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(10)))                                                                              //Natural: IF #KEY-REC-LVL-# = 10
            {
                pnd_Ws_Pnd_Pymnt_Break_Ind.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PYMNT-BREAK-IND
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #PYMNT-BREAK-IND
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
            sub_Move_Small_Rec_To_Large_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Pnd_Pymnt_Break_IndOld.setValue(pnd_Ws_Pnd_Pymnt_Break_Ind);                                                                                        //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Current_Pymnt_Seq_Nbr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-CURRENT-PYMNT-SEQ-NBR
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"Counters etc, at end of program:",NEWLINE,"Next check  number...........:",pnd_Ws_Next_Check_Nbr,     //Natural: WRITE *PROGRAM *TIME 'Counters etc, at end of program:' / 'Next check  number...........:' #WS-NEXT-CHECK-NBR / 'Next EFT    number..............:' #WS-NEXT-EFT-NBR / 'Next Global number..............:' #WS-NEXT-GLOBAL-NBR / 'Next Internal Rollover number...:' #WS-NEXT-INT-ROLLOVER-NBR / 'Next payment sequence number....:' #WS-CURRENT-PYMNT-SEQ-NBR
            NEWLINE,"Next EFT    number..............:",pnd_Ws_Next_Eft_Nbr,NEWLINE,"Next Global number..............:",pnd_Ws_Next_Global_Nbr,NEWLINE,"Next Internal Rollover number...:",
            pnd_Ws_Next_Int_Rollover_Nbr,NEWLINE,"Next payment sequence number....:",pnd_Ws_Current_Pymnt_Seq_Nbr);
        if (Global.isEscape()) return;
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Current_Pymnt_Seq_Nbr);                                                                           //Natural: ASSIGN #FCPL876.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().setValue(pnd_Ws_Next_Check_Nbr);                                                                                      //Natural: ASSIGN #FCPL876.PYMNT-CHECK-NBR := #WS-NEXT-CHECK-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().setValue(pnd_Ws_Next_Eft_Nbr);                                                                                  //Natural: ASSIGN #FCPL876.PYMNT-CHECK-SCRTY-NBR := #WS-NEXT-EFT-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Last_Rollover_Nbr().setValue(pnd_Ws_Next_Int_Rollover_Nbr);                                                                       //Natural: ASSIGN #FCPL876.PYMNT-LAST-ROLLOVER-NBR := #WS-NEXT-INT-ROLLOVER-NBR
        ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Barcode().getValue("*").setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue("*"));                               //Natural: ASSIGN #FCPL876.#BAR-BARCODE ( * ) := #BARCODE-LDA.#BAR-BARCODE ( * )
        ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes());                                                       //Natural: ASSIGN #FCPL876.#BAR-ENVELOPES := #BARCODE-LDA.#BAR-ENVELOPES
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(7, false, ldaFcpl876.getPnd_Fcpl876());                                                                                                      //Natural: WRITE WORK FILE 07 #FCPL876
        getReports().write(0, NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION ****");                                                                                    //Natural: WRITE // '****** END OF PROGRAM EXECUTION ****'
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-SMALL-REC-TO-LARGE-REC
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PYMNT-ID
        //* *-----------------------------------
        //*  DETERMINE THE PAYMENT IDENTIFIER OF THE CURRENT PAYMENT
        //*  WHETHER  IT IS A CHECK, AN EFT OR A GLOBAL PAY
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATEMENT
        //* *  COMPRESS '12' #WS-CHECK-NBR-A10 *PROGRAM ' 4410'
        //*      ' $$XEROX FORMAT=DABAC,FORMS=BLANK,FEED=BOND,END;'
        //*      ' $$XEROX FORMAT=IAPERB,FORMS=IAMULT,FEED=BOND,END;'
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DOCUMENT
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-XEROX-COMMANDS
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-CHECK
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SENT-TO-FORM
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-NOTIFICATION
        //* *COMPRESS '12' #WS-CHECK-NBR-A10 *PROGRAM ' 6940'
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BODY-OF-STATEMENT
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BARCODE-PROCESSING
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-CHECK-SEQ-NUMBERS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-DATA
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-DATA
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLANK-PAGE
        //* *------------
        //* *********************** RL BEGIN - PAYEE MATCH ************************
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //* ************************** RL END-PAYEE MATCH *************************
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T 'PROCESS SINGLE SUM AND MDO CHECKS' 68T 'REPORT: RPT0' / 36T #RUN-TYPE //
        //*  RL PAYEE MATCH
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  RL PAYEE MATCH
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Move_Small_Rec_To_Large_Rec() throws Exception                                                                                                       //Natural: MOVE-SMALL-REC-TO-LARGE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------------
        //*  HEADER REC
        short decideConditionsMet809 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #KEY-REC-LVL-#;//Natural: VALUE 10
        if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(10))))
        {
            decideConditionsMet809++;
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1().setValue(pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1);                                                 //Natural: MOVE #WS-WORK-REC-DET-1 TO #REC-TYPE-10-DET-1
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2().setValue(pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2);                                                 //Natural: MOVE #WS-WORK-REC-DET-2 TO #REC-TYPE-10-DET-2
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PYMNT-ID
            sub_Determine_Pymnt_Id();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-SEQ-NUMBERS
            sub_Assign_Check_Seq_Numbers();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1());                                                 //Natural: MOVE #REC-TYPE-10-DET-1 TO #WS-WORK-REC-DET-1
            pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2());                                                 //Natural: MOVE #REC-TYPE-10-DET-2 TO #WS-WORK-REC-DET-2
            pnd_Ws_Header.getValue("*").setValue(pnd_Ws_Work_Rec.getValue("*"));                                                                                          //Natural: MOVE #WS-WORK-REC ( * ) TO #WS-HEADER ( * )
            //*  SET CONTROL RECORD PYMNT INDEXES
            //*  OCCURS REC
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
            sub_Set_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 20
        else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(20))))
        {
            decideConditionsMet809++;
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I > 0
            {
                pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_J.add(1));                                                                                         //Natural: COMPUTE #I = #J + 1
                pnd_J.nadd(2);                                                                                                                                            //Natural: COMPUTE #J = #J + 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_I.setValue(1);                                                                                                                                        //Natural: ASSIGN #I := 1
                pnd_J.setValue(2);                                                                                                                                        //Natural: ASSIGN #J := 2
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-SEQ-NUMBERS
            sub_Assign_Check_Seq_Numbers();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Occurs.getValue(pnd_I).setValue(pnd_Ws_Work_Rec.getValue(1));                                                                                          //Natural: MOVE #WS-WORK-REC ( 1 ) TO #WS-OCCURS ( #I )
            pnd_Ws_Occurs.getValue(pnd_J).setValue(pnd_Ws_Work_Rec.getValue(2));                                                                                          //Natural: MOVE #WS-WORK-REC ( 2 ) TO #WS-OCCURS ( #J )
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1().setValue(pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1);                                                 //Natural: MOVE #WS-WORK-REC-DET-1 TO #REC-TYPE-20-DET-1
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2().setValue(pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2);                                                 //Natural: MOVE #WS-WORK-REC-DET-2 TO #REC-TYPE-20-DET-2
            pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt.nadd(ldaFcpl378.getPnd_Rec_Type_20_Detail_Inv_Acct_Dpi_Amt());                                                                 //Natural: ADD #REC-TYPE-20-DETAIL.INV-ACCT-DPI-AMT TO #WS-INV-ACCT-DPI-AMT
            pnd_Ws_Cntr_Inv_Acct.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WS-CNTR-INV-ACCT
            //*  NAME/ADDR REC
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
            sub_Accum_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 30
        else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(30))))
        {
            decideConditionsMet809++;
            if (condition(pnd_K.greater(getZero())))                                                                                                                      //Natural: IF #K > 0
            {
                pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_L.add(1));                                                                                         //Natural: COMPUTE #K = #L + 1
                pnd_L.nadd(2);                                                                                                                                            //Natural: COMPUTE #L = #L + 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_K.setValue(1);                                                                                                                                        //Natural: ASSIGN #K = 1
                pnd_L.setValue(2);                                                                                                                                        //Natural: ASSIGN #L = 2
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-SEQ-NUMBERS
            sub_Assign_Check_Seq_Numbers();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Name_N_Address.getValue(pnd_K).setValue(pnd_Ws_Work_Rec.getValue(1));                                                                                  //Natural: MOVE #WS-WORK-REC ( 1 ) TO #WS-NAME-N-ADDRESS ( #K )
            pnd_Ws_Name_N_Address.getValue(pnd_L).setValue(pnd_Ws_Work_Rec.getValue(2));                                                                                  //Natural: MOVE #WS-WORK-REC ( 2 ) TO #WS-NAME-N-ADDRESS ( #L )
        }                                                                                                                                                                 //Natural: VALUE 40
        else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(40))))
        {
            decideConditionsMet809++;
            if (condition(pnd_Ws_Ira_Int.equals("Y")))                                                                                                                    //Natural: IF #WS-IRA-INT = 'Y'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Break_If_Acfs.getBoolean()))                                                                                                         //Natural: IF #BREAK-IF-ACFS
            {
                                                                                                                                                                          //Natural: PERFORM FORMAT-DOCUMENT
                sub_Format_Document();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Pnd_Break_If_Acfs.setValue(false);                                                                                                                 //Natural: MOVE FALSE TO #BREAK-IF-ACFS
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1().setValue(pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1);                                                 //Natural: MOVE #WS-WORK-REC-DET-1 TO #REC-TYPE-40-DET-1
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2().setValue(pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2);                                                 //Natural: MOVE #WS-WORK-REC-DET-2 TO #REC-TYPE-40-DET-2
            if (condition((ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean() || (! (ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean())  //Natural: IF #GOOD-LETTER OR NOT #GOOD-LETTER AND #KEY-REC-OCCUR-# = 01
                && ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().equals(1)))))
            {
                ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp().setValuesByName(pnd_Ws_Name_N_Address_Nme_N_Addr.getValue(1));                          //Natural: MOVE BY NAME #WS-NAME-N-ADDRESS.NME-N-ADDR ( 1 ) TO #REC-TYPE-30-DETAIL.#PYMNT-NME-AND-ADDR-GRP
                ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Nme().setValue(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(2));                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( 2 ) TO #REC-TYPE-30-DETAIL.PYMNT-NME
                ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Eft_Acct_Nbr().setValue(pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr.getValue(1));                                 //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR ( 1 ) TO #REC-TYPE-30-DETAIL.PYMNT-EFT-ACCT-NBR
                DbsUtil.callnat(Fcpnacfs.class , getCurrentProcessState(), ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10(),  //Natural: CALLNAT 'FCPNACFS' USING #PYMNT-EXT-KEY #RECORD-TYPE-10 #RECORD-TYPE-30 #RECORD-TYPE-40 #WS-INV-ACCT-DPI-AMT
                    ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30(), ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Record_Type_40(), pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt);
                if (condition(Global.isEscape())) return;
                pnd_Ws_Pnd_Ws_Acfs.setValue(true);                                                                                                                        //Natural: MOVE TRUE TO #WS-ACFS
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
                sub_Barcode_Processing();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Pnd_Ws_Acfs.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #WS-ACFS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet809 > 0))
        {
            if (condition(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().lessOrEqual(30)))                                                                         //Natural: IF #KEY-REC-LVL-# <= 30
            {
                getWorkFiles().write(9, false, pnd_Ws_Work_Rec.getValue("*"));                                                                                            //Natural: WRITE WORK FILE 09 #WS-WORK-REC ( * )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Pymnt_Id() throws Exception                                                                                                                //Natural: DETERMINE-PYMNT-ID
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Ira_Int.setValue(" ");                                                                                                                                     //Natural: ASSIGN #WS-IRA-INT := ' '
        if (condition(((! (((ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ANNT") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT1"))  //Natural: IF NOT ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ANNT' OR = 'ALT1' OR = 'ALT2' ) AND #REC-TYPE-10-DETAIL.CNTRCT-TYPE-CDE = 'R' AND #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND = 6
            || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT2"))) && ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Type_Cde().equals("R")) 
            && ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(6))))
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr().reset();                                                                                            //Natural: RESET #KEY-PYMNT-CHECK-NBR #KEY-PYMNT-CHECK-SEQ-NBR #REC-TYPE-10-DETAIL.PYMNT-CHECK-NBR #REC-TYPE-10-DETAIL.PYMNT-CHECK-SCRTY-NBR #REC-TYPE-10-DETAIL.PYMNT-CHECK-SEQ-NBR #WS-CURRENT-PYMNT-ID
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr().reset();
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Nbr().reset();
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr().reset();
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr().reset();
            pnd_Ws_Current_Pymnt_Id.reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Current_Pymnt_Seq_Nbr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-CURRENT-PYMNT-SEQ-NBR
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Current_Pymnt_Seq_Nbr);                                                            //Natural: ASSIGN #REC-TYPE-10-DETAIL.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
            //*  CHECKS
            short decideConditionsMet899 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet899++;
                if (condition(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code().equals("0000")))                                                               //Natural: IF #KEY-CONTRACT-HOLD-CODE = '0000'
                {
                    ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                           //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                            //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                ldaFcpl876a.getPnd_Fcpl876a_Cntrct_Orgn_Cde().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde());                                           //Natural: ASSIGN #FCPL876A.CNTRCT-ORGN-CDE := #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE
                ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Current_Pymnt_Seq_Nbr);                                                                 //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
                pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Check_Nbr);                                                                                                  //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-CHECK-NBR
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Nbr().setValue(pnd_Ws_Next_Check_Nbr);                                                                   //Natural: ASSIGN #REC-TYPE-10-DETAIL.PYMNT-CHECK-NBR := #WS-NEXT-CHECK-NBR
                ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().setValue(pnd_Ws_Next_Check_Nbr);                                                                            //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-NBR := #WS-NEXT-CHECK-NBR
                //*  RL ?
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Next_Check_Nbr);                                                                                 //Natural: MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
                //*  RL ?
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                //*  RL ?
                //*  RL ?
                pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                               //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
                pnd_Ws_Next_Check_Nbr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #WS-NEXT-CHECK-NBR
                //*  EFT
                getWorkFiles().write(8, false, ldaFcpl876a.getPnd_Fcpl876a());                                                                                            //Natural: WRITE WORK FILE 8 #FCPL876A
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet899++;
                pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Eft_Nbr);                                                                                                    //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-EFT-NBR
                //*  GLOBAL PAY
                pnd_Ws_Next_Eft_Nbr.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-NEXT-EFT-NBR
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr().setValue(pnd_Ws_Current_Pymnt_Id);                                                           //Natural: ASSIGN #REC-TYPE-10-DETAIL.PYMNT-CHECK-SCRTY-NBR := #WS-CURRENT-PYMNT-ID
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(3))))
            {
                decideConditionsMet899++;
                pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Global_Nbr);                                                                                                 //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-GLOBAL-NBR
                //*  INTERNAL ROLLOVER
                pnd_Ws_Next_Global_Nbr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #WS-NEXT-GLOBAL-NBR
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(8))))
            {
                decideConditionsMet899++;
                pnd_Ws_Ira_Int.setValue("Y");                                                                                                                             //Natural: ASSIGN #WS-IRA-INT := 'Y'
                pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Int_Rollover_Nbr);                                                                                           //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-INT-ROLLOVER-NBR
                pnd_Ws_Next_Int_Rollover_Nbr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-NEXT-INT-ROLLOVER-NBR
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr().setValue(pnd_Ws_Current_Pymnt_Id);                                                           //Natural: ASSIGN #REC-TYPE-10-DETAIL.PYMNT-CHECK-SCRTY-NBR := #WS-CURRENT-PYMNT-ID
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"- INVALID PAYMENT METHOD:",ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind(),new              //Natural: WRITE '***' 25T '- INVALID PAYMENT METHOD:' #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND 77T '***'
                    TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(53);  if (true) return;                                                                                                                 //Natural: TERMINATE 53
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Statement() throws Exception                                                                                                                  //Natural: FORMAT-STATEMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  C=CHECK
        if (condition(pnd_Ws_Bottom_Part.equals("C")))                                                                                                                    //Natural: IF #WS-BOTTOM-PART = 'C'
        {
            //* *ASSIGN #WS-FORMAT = 'SSDA1'        /* RL
            //*  RL
            pnd_Ws_Format.setValue("SSDA1X");                                                                                                                             //Natural: ASSIGN #WS-FORMAT = 'SSDA1X'
            //* *ASSIGN #WS-FORMS  = 'SSWC'         /* RL
            //*  RL
            pnd_Ws_Forms.setValue("MCMWIC");                                                                                                                              //Natural: ASSIGN #WS-FORMS = 'MCMWIC'
            pnd_Ws_Feed.setValue("CHECKP");                                                                                                                               //Natural: ASSIGN #WS-FEED = 'CHECKP'
            pnd_Ws_Max.setValue(13);                                                                                                                                      //Natural: ASSIGN #WS-MAX = 13
                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
            sub_Format_Xerox_Commands();
            if (condition(Global.isEscape())) {return;}
            //* ******************* RL BEGIN POS-PAY/PAYEE MATCH **********************
            //* *MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
            //* *MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
            //* *MOVE EDITED #WS-CHECK-NBR-N10 (EM=9999999999) TO
            //* *  #WS-CHECK-NBR-A10
            pnd_Ws_Pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "12", pnd_Ws_Check_Nbr_A10));                                                    //Natural: COMPRESS '12' #WS-CHECK-NBR-A10 INTO #WS-REC-1 LEAVING NO SPACE
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            //* ***************** RL END POS-PAY/PAYEE MATCH *********************
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  S=SENT TO FORM
            if (condition(pnd_Ws_Bottom_Part.equals("S")))                                                                                                                //Natural: IF #WS-BOTTOM-PART = 'S'
            {
                pnd_Ws_Format.setValue("SSDA2");                                                                                                                          //Natural: ASSIGN #WS-FORMAT = 'SSDA2'
                pnd_Ws_Forms.setValue("SSWOC");                                                                                                                           //Natural: ASSIGN #WS-FORMS = 'SSWOC'
                pnd_Ws_Feed.setValue("BOND");                                                                                                                             //Natural: ASSIGN #WS-FEED = 'BOND'
                pnd_Ws_Max.setValue(10);                                                                                                                                  //Natural: ASSIGN #WS-MAX = 10
                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
                sub_Format_Xerox_Commands();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  FORMAT HEADER OF STATEMENT
        //*  STANDARD TEXT FOR STATEMENT
        DbsUtil.callnat(Fcpn236.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN236' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        pnd_Ws_Side_Printed.setValue("1");                                                                                                                                //Natural: ASSIGN #WS-SIDE-PRINTED = '1'
        //*  FORMAT BODY OF STATEMENT
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
        sub_Body_Of_Statement();
        if (condition(Global.isEscape())) {return;}
        //*  TRAILER OF STATEMENT
        //*  TIAA-CREFF REPROTS ALL TAXABLE PYMENTS TO GOV...
        DbsUtil.callnat(Fcpn280.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN280' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT
            pnd_Ws_Cntr_Inv_Acct);
        if (condition(Global.isEscape())) return;
        //*  C=CHECK
        if (condition(pnd_Ws_Bottom_Part.equals("C")))                                                                                                                    //Natural: IF #WS-BOTTOM-PART = 'C'
        {
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
            sub_Format_Check();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  S=SENT TO FORM
            if (condition(pnd_Ws_Bottom_Part.equals("S")))                                                                                                                //Natural: IF #WS-BOTTOM-PART = 'S'
            {
                                                                                                                                                                          //Natural: PERFORM SENT-TO-FORM
                sub_Sent_To_Form();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("2") || pnd_Ws_Save_S_D_M_Plex.equals("3")))                                                                          //Natural: IF #WS-SAVE-S-D-M-PLEX = '2' OR = '3'
        {
            pnd_Ws_Side_Printed.setValue("2");                                                                                                                            //Natural: ASSIGN #WS-SIDE-PRINTED = '2'
            //*  FORMAT BODY
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
            sub_Body_Of_Statement();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* * MULTI-PLEX NEED TO BE TESTED
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("3")))                                                                                                                //Natural: IF #WS-SAVE-S-D-M-PLEX = '3'
        {
            pnd_Ws_Side_Printed.setValue("3");                                                                                                                            //Natural: ASSIGN #WS-SIDE-PRINTED = '3'
            if (condition(pnd_Ws_Side_Printed.equals("3")))                                                                                                               //Natural: IF #WS-SIDE-PRINTED = '3'
            {
                pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                        //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                      //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=IAMONB,FORMS=IAMULT,FEED=BOND,END;");                                                                       //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=IAMONB,FORMS=IAMULT,FEED=BOND,END;'
                getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                      //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                        //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                      //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Last_Page.setValue(true);                                                                                                                                 //Natural: ASSIGN #LAST-PAGE := TRUE
            //*  FORMAT BODY
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
            sub_Body_Of_Statement();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
            sub_Barcode_Processing();
            if (condition(Global.isEscape())) {return;}
            pnd_Last_Page.setValue(false);                                                                                                                                //Natural: ASSIGN #LAST-PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Document() throws Exception                                                                                                                   //Natural: FORMAT-DOCUMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(2)))                                                                           //Natural: IF #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND = 2
        {
            //*  S=SENT TO FORM
            pnd_Ws_Bottom_Part.setValue("S");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
            sub_Format_Statement();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ANNT")))                                                                            //Natural: IF #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ANNT'
        {
            if (condition(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(2).equals(" ") && pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt.getValue(2).equals(" ")                   //Natural: IF #WS-NAME-N-ADDRESS.PYMNT-NME ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( 2 ) = ' '
                && pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt.getValue(2).equals(" ") && pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt.getValue(2).equals(" ") 
                && pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt.getValue(2).equals(" ") && pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt.getValue(2).equals(" ") 
                && pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt.getValue(2).equals(" ")))
            {
                //*  C=CHECK
                pnd_Ws_Bottom_Part.setValue("C");                                                                                                                         //Natural: ASSIGN #WS-BOTTOM-PART = 'C'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
                sub_Format_Statement();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  S=SENT TO FORM
                pnd_Ws_Bottom_Part.setValue("S");                                                                                                                         //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
                sub_Format_Statement();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-NOTIFICATION
                sub_Format_Notification();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
                sub_Format_Check();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Format_Notification_Letter.setValue("N");                                                                                                          //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT1") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT2")))  //Natural: IF #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ALT1' OR = 'ALT2'
        {
            //*  S=SENT TO FORM
            pnd_Ws_Bottom_Part.setValue("S");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
            sub_Format_Statement();
            if (condition(Global.isEscape())) {return;}
            //*  C=CHECK
            pnd_Ws_Bottom_Part.setValue("C");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'C'
                                                                                                                                                                          //Natural: PERFORM FORMAT-NOTIFICATION
            sub_Format_Notification();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
            sub_Format_Check();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Format_Notification_Letter.setValue("N");                                                                                                              //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'
        }                                                                                                                                                                 //Natural: END-IF
        //*   ROUTINE TO PRINT SUSPENSE NOTIFICATION                  /* RCS
        //*   (ADDRESS MOVED TO ALIGN WITH WINDOW ENVELOPE            /* RCS
        //*  RCS
        //*  RCS
        if (condition(! (ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ANNT") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT1") //Natural: IF NOT ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ANNT' OR = 'ALT1' OR = 'ALT2' )
            || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT2"))))
        {
            //*  RCS
            if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Type_Cde().equals("R")))                                                                            //Natural: IF #REC-TYPE-10-DETAIL.CNTRCT-TYPE-CDE = 'R'
            {
                //*  RCS
                if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(6)))                                                                   //Natural: IF #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND = 6
                {
                    //*  RCS
                    pnd_Ws_Bottom_Part.setValue("S");                                                                                                                     //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                    //*  RCS
                    pnd_Ws_Name_N_Address.getValue(3).setValue(pnd_Ws_Name_N_Address.getValue(1));                                                                        //Natural: MOVE #WS-NAME-N-ADDRESS ( 1 ) TO #WS-NAME-N-ADDRESS ( 3 )
                    //*  RCS
                    pnd_Ws_Name_N_Address.getValue(4).setValue(pnd_Ws_Name_N_Address.getValue(2));                                                                        //Natural: MOVE #WS-NAME-N-ADDRESS ( 2 ) TO #WS-NAME-N-ADDRESS ( 4 )
                    //*  RCS
                    pnd_Ws_Name_N_Address_Nme_N_Addr.getValue(1).reset();                                                                                                 //Natural: RESET NME-N-ADDR ( 1 )
                    //* ***  PERFORM FORMAT-STATEMENT                             /* RCS
                    //*  RCS
                                                                                                                                                                          //Natural: PERFORM FORMAT-NOTIFICATION
                    sub_Format_Notification();
                    if (condition(Global.isEscape())) {return;}
                    //*  RCS
                    pnd_Ws_Format_Notification_Letter.setValue("N");                                                                                                      //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'
                    pnd_I.setValue(2);                                                                                                                                    //Natural: ASSIGN #I = 2
                    //*  RCS
                    //*  RCS
                    //*  RCS
                    //*  RCS
                    //*  RCS
                    DbsUtil.callnat(Fcpn253.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),  //Natural: CALLNAT 'FCPN253' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #I
                        pnd_I);
                    if (condition(Global.isEscape())) return;
                    //* **** PERFORM SENT-TO-FORM                                 /* RCS
                    //*  RCS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM FORMAT-NOTIFICATION
                    sub_Format_Notification();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
                    sub_Format_Check();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ws_Format_Notification_Letter.setValue("N");                                                                                                      //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "ERROR: ","=",ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde()," HAS ","=",ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind()); //Natural: WRITE 'ERROR: ' '=' #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE ' HAS ' '=' #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND
                if (Global.isEscape()) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Xerox_Commands() throws Exception                                                                                                             //Natural: FORMAT-XEROX-COMMANDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        if (condition(pnd_Ws_Sim_Dup_Multiplex_Written.notEquals(pnd_Ws_Save_S_D_M_Plex)))                                                                                //Natural: IF #WS-SIM-DUP-MULTIPLEX-WRITTEN NE #WS-SAVE-S-D-M-PLEX
        {
            pnd_Ws_Sim_Dup_Multiplex_Written.setValue(pnd_Ws_Save_S_D_M_Plex);                                                                                            //Natural: ASSIGN #WS-SIM-DUP-MULTIPLEX-WRITTEN := #WS-SAVE-S-D-M-PLEX
            short decideConditionsMet1106 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #WS-SAVE-S-D-M-PLEX;//Natural: VALUE '1'
            if (condition((pnd_Ws_Save_S_D_M_Plex.equals("1"))))
            {
                decideConditionsMet1106++;
                pnd_Ws_Jde.setValue("SIM");                                                                                                                               //Natural: ASSIGN #WS-JDE = 'SIM'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Ws_Save_S_D_M_Plex.equals("2"))))
            {
                decideConditionsMet1106++;
                pnd_Ws_Jde.setValue("DUP");                                                                                                                               //Natural: ASSIGN #WS-JDE = 'DUP'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                //*  T. MCGEE 03/18/02
                pnd_Ws_Jde.setValue("DUP");                                                                                                                               //Natural: ASSIGN #WS-JDE = 'DUP'
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " $$XEROX JDE=CHK", pnd_Ws_Jde, ",JDL=CHECK,FORMAT=", pnd_Ws_Format,             //Natural: COMPRESS ' $$XEROX JDE=CHK' #WS-JDE ',JDL=CHECK,FORMAT=' #WS-FORMAT ',FORMS=' #WS-FORMS ',FEED=' #WS-FEED ',END;' INTO #WS-REC-1 LEAVING NO SPACE
                ",FORMS=", pnd_Ws_Forms, ",FEED=", pnd_Ws_Feed, ",END;"));
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Check() throws Exception                                                                                                                      //Natural: FORMAT-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Ws_Pnd_Check_Ind.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #CHECK-IND #FIRST-PAGE
        pnd_First_Page.setValue(true);
        pnd_Ws_Pnd_Statement_Ind.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #STATEMENT-IND
        //*  RL
        DbsUtil.callnat(Fcpn380.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN380' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) FCPA110 #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pdaFcpa110.getFcpa110(), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Ws_Rec_1.setValue("+3");                                                                                                                               //Natural: MOVE '+3' TO #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        //*  JFT - TO DETERMINE IF LAST PAGE - CONSISTENT WITH FCPP378 WHERE
        //*        PAGE OR SIDE IS CALCULATED
        if (condition(((pnd_Ws_Cntr_Inv_Acct.multiply(5)).add(2)).lessOrEqual(51)))                                                                                       //Natural: IF ( ( #WS-CNTR-INV-ACCT * 5 ) + 2 ) LE 51
        {
            pnd_Last_Page.setValue(true);                                                                                                                                 //Natural: ASSIGN #LAST-PAGE := TRUE
            //* *ELSE
            //* *  #LAST-PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
        //* *MOVE '+2'      TO #WS-REC-1   /* RL PAYEE MATCH
        //* *WRITE WORK FILE 8 #WS-REC-1 /* RLPAYEE MATCH
        if (condition(pnd_Ws_Save_S_D_M_Plex.notEquals("1") && pnd_Ws_Format_Notification_Letter.notEquals("Y")))                                                         //Natural: IF #WS-SAVE-S-D-M-PLEX NE '1' AND #WS-FORMAT-NOTIFICATION-LETTER NE 'Y'
        {
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=DABAC,FORMS=BLANK,END;");                                                                                       //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=DABAC,FORMS=BLANK,END;'
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Sent_To_Form() throws Exception                                                                                                                      //Natural: SENT-TO-FORM
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        pnd_Ws_Pnd_Check_Ind.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #CHECK-IND
        pnd_Ws_Pnd_Statement_Ind.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #STATEMENT-IND #FIRST-PAGE
        pnd_First_Page.setValue(true);
        DbsUtil.callnat(Fcpn260.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"));         //Natural: CALLNAT 'FCPN260' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * )
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Ws_Rec_1.setValue("+3");                                                                                                                               //Natural: MOVE '+3' TO #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        //*  JFT - TO DETERMINE IF LAST PAGE - CONSISTENT WITH FCPP378 WHERE
        //*        PAGE OR SIDE IS CALCULATED
        if (condition(((pnd_Ws_Cntr_Inv_Acct.multiply(5)).add(2)).lessOrEqual(83)))                                                                                       //Natural: IF ( ( #WS-CNTR-INV-ACCT * 5 ) + 2 ) LE 83
        {
            pnd_Last_Page.setValue(true);                                                                                                                                 //Natural: ASSIGN #LAST-PAGE := TRUE
            //* *ELSE
            //* *  #LAST-PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Save_S_D_M_Plex.notEquals("1")))                                                                                                             //Natural: IF #WS-SAVE-S-D-M-PLEX NE '1'
        {
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=DABAC,FORMS=BLANK,END;");                                                                                       //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=DABAC,FORMS=BLANK,END;'
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Notification() throws Exception                                                                                                               //Natural: FORMAT-NOTIFICATION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Format_Notification_Letter.setValue("Y");                                                                                                                  //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'Y'
        if (condition(pnd_Ws_First_Xerox.equals("Y")))                                                                                                                    //Natural: IF #WS-FIRST-XEROX = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        //* *SIGN #WS-REC-1 = ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSLET,' -  /*RL
        //* *'FORMS=SSLET,FEED=CHECKP,END;'                                    /*RL
        //* RL
        //* RL
        pnd_Ws_Pnd_Ws_Rec_1.setValue(" $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSLETX,FORMS=MCMLET,FEED=CHECKP,END;");                                                        //Natural: ASSIGN #WS-REC-1 = ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSLETX,FORMS=MCMLET,FEED=CHECKP,END;'
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1.reset();                                                                                                                                      //Natural: RESET #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        //* ***************** RL BEGIN POS-PAY/PAYEE MATCH *********************
        //* *MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //* *MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //* *MOVE EDITED #WS-CHECK-NBR-N10 (EM=9999999999) TO
        //* *  #WS-CHECK-NBR-A10
        //*  *PROGRAM
        pnd_Ws_Pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "12", pnd_Ws_Check_Nbr_A10));                                                        //Natural: COMPRESS '12' #WS-CHECK-NBR-A10 INTO #WS-REC-1 LEAVING NO SPACE
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        //* ***************** RL END POS-PAY/PAYEE MATCH *********************
        if (condition(pnd_Ws_First_Xerox.equals("Y")))                                                                                                                    //Natural: IF #WS-FIRST-XEROX = 'Y'
        {
            pnd_Ws_First_Xerox.setValue("N");                                                                                                                             //Natural: ASSIGN #WS-FIRST-XEROX = 'N'
            //*  IGNORE
            //* *ELSE                          /* RL 11PM JAN 31
            //* *  ASSIGN #WS-REC-1 = ' '      /* RL
            //* *  WRITE WORK FILE 8 #WS-REC-1 /* RL
        }                                                                                                                                                                 //Natural: END-IF
        //*  FORMAT HEADER
        DbsUtil.callnat(Fcpn236.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN236' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Fcpn230.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN230' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Body_Of_Statement() throws Exception                                                                                                                 //Natural: BODY-OF-STATEMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  FORMAT BODY
        DbsUtil.callnat(Fcpn246.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN246' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-SIDE-PRINTED #WS-PRINTED-INSTITUTION-AMT
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written, pnd_Ws_Side_Printed, pnd_Ws_Printed_Institution_Amt);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Barcode_Processing() throws Exception                                                                                                                //Natural: BARCODE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        short decideConditionsMet1224 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STATEMENT-IND
        if (condition(pnd_Ws_Pnd_Statement_Ind.getBoolean()))
        {
            decideConditionsMet1224++;
            pnd_Ws_Pnd_Bar_Sub.setValue(2);                                                                                                                               //Natural: MOVE 2 TO #BAR-SUB
        }                                                                                                                                                                 //Natural: WHEN #CHECK-IND
        else if (condition(pnd_Ws_Pnd_Check_Ind.getBoolean()))
        {
            decideConditionsMet1224++;
            pnd_Ws_Pnd_Bar_Sub.setValue(1);                                                                                                                               //Natural: MOVE 1 TO #BAR-SUB
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Ws_Acfs.getBoolean()))                                                                                                                   //Natural: IF #WS-ACFS
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_First_Page.getBoolean()))                                                                                                                   //Natural: IF #FIRST-PAGE
            {
                pnd_First_Page.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-PAGE := FALSE
                //*  FIRST PAGE OF ENVELOPE
                ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Demand_Feed().setValue(true);                                                                                      //Natural: MOVE TRUE TO #BAR-DEMAND-FEED
                ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().nadd(1);                                                                                              //Natural: ADD 1 TO #BAR-ENV-ID-NUM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  NOT ACFS
        //*  STATEMENT
        //*  CHECK
        //*  PRINTING ACFS
        //*  BAD ACFS AND FIRST ACFS
        //*  LAST ACFS
        if (condition((((! (ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().getBoolean()) || pnd_Ws_Pnd_Statement_Ind.getBoolean()) && pnd_Last_Page.getBoolean())        //Natural: IF ( ( NOT #ACFS OR #STATEMENT-IND ) AND #LAST-PAGE ) OR #CHECK-IND AND #WS-ACFS AND ( NOT #GOOD-LETTER OR #KEY-REC-OCCUR-# = #PLAN-DATA-TOP )
            || ((pnd_Ws_Pnd_Check_Ind.getBoolean() && pnd_Ws_Pnd_Ws_Acfs.getBoolean()) && (! (ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean()) 
            || ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().equals(ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top()))))))
        {
            //*  LAST  PAGE OF ENVELOPE
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Set_Last_Page().setValue(true);                                                                                        //Natural: MOVE TRUE TO #BAR-SET-LAST-PAGE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(pnd_Ws_Pnd_Bar_Sub).getSubstring(1,3).equals("CR ")))                                                      //Natural: IF SUBSTR ( #WS-NAME-N-ADDRESS.PYMNT-NME ( #BAR-SUB ) ,1,3 ) = 'CR '
        {
            pnd_Ws_Pnd_Bar_Name.setValue(pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt.getValue(pnd_Ws_Pnd_Bar_Sub));                                                        //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( #BAR-SUB ) TO #BAR-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Bar_Name.setValue(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(pnd_Ws_Pnd_Bar_Sub));                                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( #BAR-SUB ) TO #BAR-NAME
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpnbar.class , getCurrentProcessState(), ldaFcplbar1.getPnd_Barcode_Lda());                                                                      //Natural: CALLNAT 'FCPNBAR' #BARCODE-LDA
        if (condition(Global.isEscape())) return;
        FOR01:                                                                                                                                                            //Natural: FOR #BAR-SUB = 1 TO 17
        for (pnd_Ws_Pnd_Bar_Sub.setValue(1); condition(pnd_Ws_Pnd_Bar_Sub.lessOrEqual(17)); pnd_Ws_Pnd_Bar_Sub.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Bar_Sub.equals(1)))                                                                                                                  //Natural: IF #BAR-SUB = 1
            {
                pnd_Ws_Pnd_Ws_Rec_1.setValue("1");                                                                                                                        //Natural: MOVE '1' TO #WS-REC-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                        //Natural: MOVE ' ' TO #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Ws_Acfs.getBoolean()))                                                                                                               //Natural: IF #WS-ACFS
            {
                setValueToSubstring("2",pnd_Ws_Pnd_Ws_Rec_1,2,1);                                                                                                         //Natural: MOVE '2' TO SUBSTR ( #WS-REC-1,2,1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                setValueToSubstring("<",pnd_Ws_Pnd_Ws_Rec_1,2,1);                                                                                                         //Natural: MOVE '<' TO SUBSTR ( #WS-REC-1,2,1 )
            }                                                                                                                                                             //Natural: END-IF
            setValueToSubstring(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue(pnd_Ws_Pnd_Bar_Sub),pnd_Ws_Pnd_Ws_Rec_1,3,1);                                   //Natural: MOVE #BARCODE-LDA.#BAR-BARCODE ( #BAR-SUB ) TO SUBSTR ( #WS-REC-1,3,1 )
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Set_Last_Page().getBoolean()))                                                                               //Natural: IF #BAR-SET-LAST-PAGE
        {
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee().setValue(pnd_Ws_Pnd_Bar_Name);                                                                                    //Natural: ASSIGN #FCPL876B.#ADDRESSEE := #BAR-NAME
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id());                                                       //Natural: ASSIGN #FCPL876B.#BAR-ENV-ID := #BARCODE-LDA.#BAR-ENV-ID
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde());                                               //Natural: ASSIGN #FCPL876B.CNTRCT-ORGN-CDE := #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Ppcn_Nbr());                                               //Natural: ASSIGN #FCPL876B.CNTRCT-PPCN-NBR := #REC-TYPE-10-DETAIL.CNTRCT-PPCN-NBR
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde());                                             //Natural: ASSIGN #FCPL876B.CNTRCT-PAYEE-CDE := #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde().setValue(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code());                                         //Natural: ASSIGN #FCPL876B.CNTRCT-HOLD-CDE := #KEY-CONTRACT-HOLD-CODE
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter());                         //Natural: ASSIGN #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER := #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER
            if (condition(pnd_Ws_Pnd_Check_Ind.getBoolean()))                                                                                                             //Natural: IF #CHECK-IND
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr().setValue(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr());                                                    //Natural: ASSIGN #FCPL876B.PYMNT-CHECK-NBR := #FCPL876A.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Pymnt_Check_Nbr().reset();                                                                                                //Natural: RESET #FCPL876B.#PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes());                                                 //Natural: ASSIGN #FCPL876B.#BAR-ENVELOPES := #BARCODE-LDA.#BAR-ENVELOPES
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Pages_In_Env());                                           //Natural: ASSIGN #FCPL876B.#BAR-PAGES-IN-ENV := #BARCODE-LDA.#BAR-PAGES-IN-ENV
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().setValue(pnd_Ws_Pnd_Check_Ind.getBoolean());                                                                          //Natural: ASSIGN #FCPL876B.#CHECK := #CHECK-IND
            getWorkFiles().write(8, false, ldaFcpl876b.getPnd_Fcpl876b());                                                                                                //Natural: WRITE WORK FILE 8 #FCPL876B
        }                                                                                                                                                                 //Natural: END-IF
        //*  CODE BELLOW IS A TEMP CODE
        //*  DISPLAY
        //*  'L/VL'       #KEY-REC-LVL-#
        //*  'O/CC'       #KEY-REC-OCCUR-#
        //*  '/HOLD'      #KEY-CONTRACT-HOLD-CODE
        //*  'S/D'        #KEY-SIMPLEX-DUPLEX-MULTIPLEX
        //*  '/PPCN'      #REC-TYPE-10-DETAIL.CNTRCT-PPCN-NBR(AL=8)
        //*  '/CREF'      #REC-TYPE-10-DETAIL.CNTRCT-CREF-NBR(AL=8)
        //*  '/PAYEE'     #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE
        //*  'N/R'        #BARCODE-LDA.#BAR-NEW-RUN  (EM=F/T)
        //*  'C/H'        #CHECK-IND                 (EM=F/T)
        //*  'S/T'        #STATEMENT-IND             (EM=F/T)
        //*  'E/I'        #BARCODE-LDA.#BAR-BARCODE(1)
        //*  'B/1'        #BARCODE-LDA.#BAR-BARCODE(2)
        //*  'E/S'        #BARCODE-LDA.#BAR-BARCODE(3)
        //*  'B/2'        #BARCODE-LDA.#BAR-BARCODE(4)
        //*  'B/3'        #BARCODE-LDA.#BAR-BARCODE(5)
        //*  'B/4'        #BARCODE-LDA.#BAR-BARCODE(6)
        //*  'B/5'        #BARCODE-LDA.#BAR-BARCODE(7)
        //*  'P/C'        #BARCODE-LDA.#BAR-BARCODE(8)
        //*  'E/C'        #BARCODE-LDA.#BAR-BARCODE(9)
        //*  '/PIN'       #BARCODE-LDA.#BAR-ENV-ID
        //*  'S/I'        #BARCODE-LDA.#BAR-BARCODE(17)
        //*  '/ENV'       #BARCODE-LDA.#BAR-ENVELOPES
        //*  'P/E'        #BARCODE-LDA.#BAR-PAGES-IN-ENV
        //*  '/NAME'      #BAR-NAME
        //*  CODE ABOVE IS A TEMP CODE
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Barcode_Input().resetInitial();                                                                                                //Natural: RESET INITIAL #BARCODE-INPUT
    }
    private void sub_Assign_Check_Seq_Numbers() throws Exception                                                                                                          //Natural: ASSIGN-CHECK-SEQ-NUMBERS
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        if (condition(pnd_Ws_Current_Pymnt_Id.equals(getZero())))                                                                                                         //Natural: IF #WS-CURRENT-PYMNT-ID = 0
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr().reset();                                                                                            //Natural: RESET #KEY-PYMNT-CHECK-NBR #KEY-PYMNT-CHECK-SEQ-NBR
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr().reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr().setValue(pnd_Ws_Current_Pymnt_Id);                                                                  //Natural: MOVE #WS-CURRENT-PYMNT-ID TO #KEY-PYMNT-CHECK-NBR
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Current_Pymnt_Seq_Nbr);                                                         //Natural: MOVE #WS-CURRENT-PYMNT-SEQ-NBR TO #KEY-PYMNT-CHECK-SEQ-NBR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Key.setValue(ldaFcpl378.getPnd_Pymnt_Ext_Key());                                                                                  //Natural: MOVE #PYMNT-EXT-KEY TO #WS-WORK-REC-KEY
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------
        if (condition(pnd_Ws_Pnd_Run_Type.equals("HELD") || pnd_Ws_Pnd_Run_Type.equals("HOLD")))                                                                          //Natural: IF #RUN-TYPE = 'HELD' OR = 'HOLD'
        {
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(true);                                                                                                   //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := TRUE
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().reset();                                                                                                        //Natural: RESET #FCPL876.#BAR-ENVELOPES
            getWorkFiles().write(3, false, pdaFcpassc2.getPnd_Fcpassc2());                                                                                                //Natural: WRITE WORK FILE 3 #FCPASSC2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().getBoolean());                                          //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := #BARCODE-LDA.#BAR-NEW-RUN
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes());                                                   //Natural: ASSIGN #FCPL876.#BAR-ENVELOPES := #BARCODE-LDA.#BAR-ENVELOPES
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(45).setValue(true);                                                                                    //Natural: MOVE TRUE TO #FCPACRPT.#TRUTH-TABLE ( 45 ) #FCPACRPT.#TRUTH-TABLE ( 48:50 )
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(48,":",50).setValue(true);
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("   ANNUITANT STATEMENT CONTROL REPORT");                                                                    //Natural: ASSIGN #FCPACRPT.#TITLE := '   ANNUITANT STATEMENT CONTROL REPORT'
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                   //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
            DbsUtil.callnat(Fcpnssc2.class , getCurrentProcessState(), pdaFcpassc2.getPnd_Fcpassc2(), pdaFcpacrpt.getPnd_Fcpacrpt());                                     //Natural: CALLNAT 'FCPNSSC2' USING #FCPASSC2 #FCPACRPT
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Control_Data() throws Exception                                                                                                                //Natural: ACCUM-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        ldaFcplssc1.getPnd_Fcplssc1_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplssc1.getPnd_Fcplssc1_Pnd_Max_Fund()).setValuesByName(ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct()); //Natural: MOVE BY NAME #INV-ACCT TO #CNTRL-INV-ACCT ( 1:#MAX-FUND )
        DbsUtil.callnat(Fcpnssc1.class , getCurrentProcessState(), pdaFcpassc1.getPnd_Fcpassc1(), pdaFcpassc2.getPnd_Fcpassc2(), ldaFcplssc1.getPnd_Fcplssc1_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNSSC1' USING #FCPASSC1 #FCPASSC2 #FCPLSSC1.#MAX-FUND #FCPLSSC1.#CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ldaFcplssc1.getPnd_Fcplssc1_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplssc1.getPnd_Fcplssc1_Pnd_Max_Fund()));
        if (condition(Global.isEscape())) return;
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                                  //Natural: ASSIGN #NEW-PYMNT-IND := FALSE
    }
    private void sub_Set_Control_Data() throws Exception                                                                                                                  //Natural: SET-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pdaFcpasscn.getPnd_Fcpasscn().setValuesByName(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10());                                                         //Natural: MOVE BY NAME #RECORD-TYPE-10 TO #FCPASSCN
        DbsUtil.callnat(Fcpnsscn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpasscn.getPnd_Fcpasscn(), pdaFcpassc1.getPnd_Fcpassc1());          //Natural: CALLNAT 'FCPNSSCN' #FCPACRPT #FCPASSCN #FCPASSC1
        if (condition(Global.isEscape())) return;
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #J #K #L #WS-CNTR-INV-ACCT #WS-BOTTOM-PART #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-INV-ACCT-DPI-AMT #LAST-PAGE
        pnd_J.reset();
        pnd_K.reset();
        pnd_L.reset();
        pnd_Ws_Cntr_Inv_Acct.reset();
        pnd_Ws_Bottom_Part.reset();
        pnd_Ws_Sim_Dup_Multiplex_Written.reset();
        pnd_Ws_Header.getValue("*").reset();
        pnd_Ws_Occurs.getValue("*").reset();
        pnd_Ws_Name_N_Address.getValue("*").reset();
        pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt.reset();
        pnd_Last_Page.reset();
        pnd_Ws_Printed_Institution_Amt.setValue("N");                                                                                                                     //Natural: ASSIGN #WS-PRINTED-INSTITUTION-AMT = 'N'
        pnd_Ws_Save_S_D_M_Plex.setValue(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex());                                                              //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX = #KEY-SIMPLEX-DUPLEX-MULTIPLEX
        pnd_Ws_Pnd_Break_If_Acfs.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #BREAK-IF-ACFS #FIRST-PAGE
        pnd_First_Page.setValue(true);
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #NEW-PYMNT-IND := TRUE
    }
    private void sub_Blank_Page() throws Exception                                                                                                                        //Natural: BLANK-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *OMPRESS ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSDA1,FORMS=SSWC,'  /*RL
        //* RL
        pnd_Ws_Pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSDA1X,FORMS=MCMWIC,", "FEED=BOND,END;"));     //Natural: COMPRESS ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSDA1X,FORMS=MCMWIC,' 'FEED=BOND,END;' INTO #WS-REC-1 LEAVING NO SPACE
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 17
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(17)); pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Ws_Rec_1.setValue("11");                                                                                                                           //Natural: ASSIGN #WS-REC-1 := '11'
            if (condition(pnd_I.equals(12)))                                                                                                                              //Natural: IF #I = 12
            {
                pnd_Ws_Pnd_Ws_Rec_1.setValue("11THIS PAGE IS LEFT BLANK");                                                                                                //Natural: ASSIGN #WS-REC-1 := '11THIS PAGE IS LEFT BLANK'
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                      //Natural: RESET #BAR-ENV-ID-NUM
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Ws_Pnd_Pymnt_Break_IndIsBreak = pnd_Ws_Pnd_Pymnt_Break_Ind.isBreak(endOfData);
        if (condition(pnd_Ws_Pnd_Pymnt_Break_IndIsBreak))
        {
            if (condition(pnd_Ws_Pnd_Break_If_Acfs.getBoolean()))                                                                                                         //Natural: IF #BREAK-IF-ACFS
            {
                //*  BYPASS INTERNAL
                if (condition(pnd_Ws_Ira_Int.equals(" ")))                                                                                                                //Natural: IF #WS-IRA-INT = ' '
                {
                                                                                                                                                                          //Natural: PERFORM FORMAT-DOCUMENT
                    sub_Format_Document();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  NOT EOF
            if (condition(pnd_Ws_Pnd_Pymnt_Break_Ind.notEquals(readWork01Pnd_Pymnt_Break_IndOld)))                                                                        //Natural: IF #PYMNT-BREAK-IND NE OLD ( #PYMNT-BREAK-IND )
            {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                sub_Initialize_Fields();
                if (condition(Global.isEscape())) {return;}
                //*  FOR IRA
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Ira_Int.setValue(" ");                                                                                                                                 //Natural: ASSIGN #WS-IRA-INT := ' '
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(15, "LS=133 PS=58 ZP=ON");

        getReports().write(0, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(24),"PROCESS SINGLE SUM AND MDO CHECKS",new TabSetting(68),"REPORT: RPT0",NEWLINE,new 
            TabSetting(36),pnd_Ws_Pnd_Run_Type,NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData676() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
            sub_Initialize_Fields();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
