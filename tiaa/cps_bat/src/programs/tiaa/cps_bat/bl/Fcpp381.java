/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:24 PM
**        * FROM NATURAL PROGRAM : Fcpp381
************************************************************
**        * FILE NAME            : Fcpp381.java
**        * CLASS NAME           : Fcpp381
**        * INSTANCE NAME        : Fcpp381
************************************************************
************************************************************************
* PROGRAM  : FCPP381
* SYSTEM   : CPS
* TITLE    : EXTRACT
* GENERATED: JUL 29,93 AT 04:58 PM
* FUNCTION : THIS PROGRAM EXTRACTS RECORDS FROM THE DATABASE, FOR A
*            SPECIFIED PERIOD (DATE), FOR THE MONTHLY SETTLEMENT,
*            CHECK PRINTING PROCESS.
*            WHILE CREATING THE EXTRACT, THE PROGRAM ALSO DETERMINES
*            THE CHARACTERISTIC OF THE PRINTIGN DOCUMENT. I.E., IS IT
*            GOING TO BE A SIMPLEX, A DUPLEX, A TRIPLEX, ETC. IN
*            ADDITION, FOR EACH EXTRACTED RECORD, IT DETERMINES THE
*            PAGE ON WHICH THE LINE IS TO BE PRINTED.
*
*            LATER IN THE PROCESS, THE EXTRACTED RECORDS,
*            ARE SORTED AND PRINTED.
* HISTORY:
*            ORIGINAL DATE: 10/1/93 AUTHOR: LEON SILBERSTEIN.
*
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
* 09/11/00 : A. YOUNG    -  RE-COMPILED DUE TO FCPLPMNT.
*    11/02 : R. CARREON  -  STOW. EGTRRA CHANGES IN LDA.
*    12/02 : R. CARREON  -  CALL TO FCPN115 REPLACED BY CPWN115
*                           ALL REFERENCE TO FCPA115 CHANGED TO CPWA115
* 05/14/08 : RESTOW FOR ROTH (AER) - ROTH-MAJOR1
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
* 08/23/2019 : REPLACING PDQ MODULES WITH ADS MODULES I.E.
*              PDQA360,PDQA362,PDQN360,PDQN362 TO
*              ADSA360,ADSA362,ADSN360,ADSN362 RESPECTIVELY. -TAG:VIKRAM
* 01/15/2020 : CTS CPS SUNSET (CHG694525) TAG: CTS-0115
*              MODIFED TO PROCESS STOPS ONLY.IT WILL NOT REISSUE CHECKS.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp381 extends BLNatBase
{
    // Data Areas
    private PdaAdsa360 pdaAdsa360;
    private PdaAdsa362 pdaAdsa362;
    private PdaCpwa115 pdaCpwa115;
    private LdaFcpladdr ldaFcpladdr;
    private LdaFcplcntu ldaFcplcntu;
    private LdaFcplpmnt ldaFcplpmnt;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Abend_Code;
    private DbsField pnd_Processing_Futures;
    private DbsField pnd_Ci1;
    private DbsField pnd_Ci2;
    private DbsField pnd_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_C_Bypassed;
    private DbsField pnd_C_Selected_Pymnts;
    private DbsField pnd_C_Rcrd_10;
    private DbsField pnd_C_Rcrd_20;
    private DbsField pnd_C_Rcrd_30;
    private DbsField pnd_Program;
    private DbsField pnd_Nas_Start_Key;

    private DbsGroup pnd_Nas_Start_Key__R_Field_1;
    private DbsField pnd_Nas_Start_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Nas_Start_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Nas_Start_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Nas_Start_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Nas_Start_Key_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Payment_Date;
    private DbsField pnd_Inst_Tiaa_Amt;
    private DbsField pnd_Inst_Cref_Amt;
    private DbsField pnd_Ws_Gross_Amt;
    private DbsField pnd_Cntl_Isn;

    private DbsGroup pnd_Ct_Cntrct_Typ;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Type_Code;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Cnt;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Net_Amt;

    private DbsGroup pnd_Ct_Pay_Typ;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Pay_Code;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Cnt;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Gross_Amt;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Net_Amt;
    private DbsField pnd_Date_Numeric;

    private DbsGroup pnd_Date_Numeric__R_Field_2;
    private DbsField pnd_Date_Numeric_Pnd_Date_Alpha;
    private DbsField pnd_Ws_Check_Date;

    private DbsGroup pnd_Ws_Check_Date__R_Field_3;
    private DbsField pnd_Ws_Check_Date_Filler;
    private DbsField pnd_Ws_Check_Date_Pnd_Ws_Inverse_7_Bytes;
    private DbsField pnd_Ws_Prime_Key_Start;
    private DbsField pnd_Ws_Prime_Key_End;
    private DbsField pnd_Ws_Prime_Key;

    private DbsGroup pnd_Ws_Prime_Key__R_Field_4;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Prime_Key_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Futures_Date_Ccyymmdd;

    private DbsGroup pnd_Futures_Date_Ccyymmdd__R_Field_5;
    private DbsField pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Ccyy;
    private DbsField pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Mm;
    private DbsField pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Dd;

    private DbsGroup pnd_Futures_Date_Ccyymmdd__R_Field_6;
    private DbsField pnd_Futures_Date_Ccyymmdd__Filler1;
    private DbsField pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Yymmdd;
    private DbsField pnd_Isn;
    private DbsField pnd_Counter;
    private DbsField pnd_Counter_Ms;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_I;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_Ws_Cntr_Deductions;
    private DbsField pnd_Ws_Total_Lines_Of_Deductions;

    private DbsGroup pnd_Ws_Key;
    private DbsField pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Work_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_7;
    private DbsField pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Seq_Nbr;
    private DbsField pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Installment_Nbr;
    private DbsField pnd_Ws_First_Time;
    private DbsField pnd_Bypass_A_Pymnt;
    private DbsField pnd_Ws_Inv_Acct_Filler;
    private DbsField pnd_Ws_Name_N_Address_Filler;
    private DbsField pnd_At_Top_Of_Body;
    private DbsField pnd_Ws_Lines_In_Grp;
    private DbsField pnd_Lines_Remained_On_Page;
    private DbsField pnd_Ws_T1_Lines_On_Page;
    private DbsField pnd_Ws_Current_X_Plex;

    private DbsGroup pnd_Ws_Header_Record;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Header_Record__R_Field_8;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Header_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Header_Record_Filler;
    private DbsField pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;

    private DbsGroup pnd_Ws_Name_N_Address;

    private DbsGroup pnd_Ws_Name_N_Address_Ph_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_Last_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_First_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_Middle_Name;

    private DbsGroup pnd_Ws_Occurs;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct;

    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Side;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fed_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Occurs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Occurs_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Deductions;

    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Deductions;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Amt;

    private DbsGroup pnd_Ws_Occurs_Pnd_Pyhdr_Data;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaAdsa360 = new PdaAdsa360(localVariables);
        pdaAdsa362 = new PdaAdsa362(localVariables);
        pdaCpwa115 = new PdaCpwa115(localVariables);
        ldaFcpladdr = new LdaFcpladdr();
        registerRecord(ldaFcpladdr);
        registerRecord(ldaFcpladdr.getVw_fcp_Cons_Addr());
        ldaFcplcntu = new LdaFcplcntu();
        registerRecord(ldaFcplcntu);
        registerRecord(ldaFcplcntu.getVw_fcp_Cons_Cntrl());
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());

        // Local Variables
        pnd_Abend_Code = localVariables.newFieldInRecord("pnd_Abend_Code", "#ABEND-CODE", FieldType.INTEGER, 2);
        pnd_Processing_Futures = localVariables.newFieldInRecord("pnd_Processing_Futures", "#PROCESSING-FUTURES", FieldType.BOOLEAN, 1);
        pnd_Ci1 = localVariables.newFieldInRecord("pnd_Ci1", "#CI1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ci2 = localVariables.newFieldInRecord("pnd_Ci2", "#CI2", FieldType.PACKED_DECIMAL, 3);
        pnd_Cnr_Orgnl_Invrse_Dte = localVariables.newFieldInRecord("pnd_Cnr_Orgnl_Invrse_Dte", "#CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_C_Bypassed = localVariables.newFieldInRecord("pnd_C_Bypassed", "#C-BYPASSED", FieldType.PACKED_DECIMAL, 9);
        pnd_C_Selected_Pymnts = localVariables.newFieldInRecord("pnd_C_Selected_Pymnts", "#C-SELECTED-PYMNTS", FieldType.INTEGER, 4);
        pnd_C_Rcrd_10 = localVariables.newFieldInRecord("pnd_C_Rcrd_10", "#C-RCRD-10", FieldType.INTEGER, 4);
        pnd_C_Rcrd_20 = localVariables.newFieldInRecord("pnd_C_Rcrd_20", "#C-RCRD-20", FieldType.INTEGER, 4);
        pnd_C_Rcrd_30 = localVariables.newFieldInRecord("pnd_C_Rcrd_30", "#C-RCRD-30", FieldType.INTEGER, 4);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Nas_Start_Key = localVariables.newFieldInRecord("pnd_Nas_Start_Key", "#NAS-START-KEY", FieldType.STRING, 31);

        pnd_Nas_Start_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Nas_Start_Key__R_Field_1", "REDEFINE", pnd_Nas_Start_Key);
        pnd_Nas_Start_Key_Cntrct_Ppcn_Nbr = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Nas_Start_Key_Cntrct_Payee_Cde = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Nas_Start_Key_Cntrct_Orgn_Cde = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Nas_Start_Key_Cntrct_Invrse_Dte = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Nas_Start_Key_Pymnt_Prcss_Seq_Nbr = pnd_Nas_Start_Key__R_Field_1.newFieldInGroup("pnd_Nas_Start_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Payment_Date = localVariables.newFieldInRecord("pnd_Payment_Date", "#PAYMENT-DATE", FieldType.STRING, 8);
        pnd_Inst_Tiaa_Amt = localVariables.newFieldInRecord("pnd_Inst_Tiaa_Amt", "#INST-TIAA-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Inst_Cref_Amt = localVariables.newFieldInRecord("pnd_Inst_Cref_Amt", "#INST-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Gross_Amt = localVariables.newFieldInRecord("pnd_Ws_Gross_Amt", "#WS-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Isn = localVariables.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.NUMERIC, 10);

        pnd_Ct_Cntrct_Typ = localVariables.newGroupArrayInRecord("pnd_Ct_Cntrct_Typ", "#CT-CNTRCT-TYP", new DbsArrayController(1, 7));
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Type_Code", "#TYPE-CODE", FieldType.STRING, 2);
        pnd_Ct_Cntrct_Typ_Pnd_Cnt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ct_Cntrct_Typ_Pnd_Net_Amt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Ct_Pay_Typ = localVariables.newGroupArrayInRecord("pnd_Ct_Pay_Typ", "#CT-PAY-TYP", new DbsArrayController(1, 3));
        pnd_Ct_Pay_Typ_Pnd_Pay_Code = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Pay_Code", "#PAY-CODE", FieldType.STRING, 1);
        pnd_Ct_Pay_Typ_Pnd_Cnt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Pay_Typ_Pnd_Gross_Amt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ct_Pay_Typ_Pnd_Net_Amt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Date_Numeric = localVariables.newFieldInRecord("pnd_Date_Numeric", "#DATE-NUMERIC", FieldType.NUMERIC, 8);

        pnd_Date_Numeric__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Numeric__R_Field_2", "REDEFINE", pnd_Date_Numeric);
        pnd_Date_Numeric_Pnd_Date_Alpha = pnd_Date_Numeric__R_Field_2.newFieldInGroup("pnd_Date_Numeric_Pnd_Date_Alpha", "#DATE-ALPHA", FieldType.STRING, 
            8);
        pnd_Ws_Check_Date = localVariables.newFieldInRecord("pnd_Ws_Check_Date", "#WS-CHECK-DATE", FieldType.NUMERIC, 8);

        pnd_Ws_Check_Date__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Check_Date__R_Field_3", "REDEFINE", pnd_Ws_Check_Date);
        pnd_Ws_Check_Date_Filler = pnd_Ws_Check_Date__R_Field_3.newFieldInGroup("pnd_Ws_Check_Date_Filler", "FILLER", FieldType.STRING, 1);
        pnd_Ws_Check_Date_Pnd_Ws_Inverse_7_Bytes = pnd_Ws_Check_Date__R_Field_3.newFieldInGroup("pnd_Ws_Check_Date_Pnd_Ws_Inverse_7_Bytes", "#WS-INVERSE-7-BYTES", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Prime_Key_Start = localVariables.newFieldInRecord("pnd_Ws_Prime_Key_Start", "#WS-PRIME-KEY-START", FieldType.STRING, 4);
        pnd_Ws_Prime_Key_End = localVariables.newFieldInRecord("pnd_Ws_Prime_Key_End", "#WS-PRIME-KEY-END", FieldType.STRING, 4);
        pnd_Ws_Prime_Key = localVariables.newFieldInRecord("pnd_Ws_Prime_Key", "#WS-PRIME-KEY", FieldType.STRING, 11);

        pnd_Ws_Prime_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Prime_Key__R_Field_4", "REDEFINE", pnd_Ws_Prime_Key);
        pnd_Ws_Prime_Key_Cntrct_Orgn_Cde = pnd_Ws_Prime_Key__R_Field_4.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Prime_Key_Pymnt_Stats_Cde = pnd_Ws_Prime_Key__R_Field_4.newFieldInGroup("pnd_Ws_Prime_Key_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Prime_Key_Cntrct_Invrse_Dte = pnd_Ws_Prime_Key__R_Field_4.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Futures_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_Futures_Date_Ccyymmdd", "#FUTURES-DATE-CCYYMMDD", FieldType.STRING, 8);

        pnd_Futures_Date_Ccyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_Futures_Date_Ccyymmdd__R_Field_5", "REDEFINE", pnd_Futures_Date_Ccyymmdd);
        pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Ccyy = pnd_Futures_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Ccyy", 
            "#FUTURES-DATE-CCYY", FieldType.NUMERIC, 4);
        pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Mm = pnd_Futures_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Mm", 
            "#FUTURES-DATE-MM", FieldType.NUMERIC, 2);
        pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Dd = pnd_Futures_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Dd", 
            "#FUTURES-DATE-DD", FieldType.NUMERIC, 2);

        pnd_Futures_Date_Ccyymmdd__R_Field_6 = localVariables.newGroupInRecord("pnd_Futures_Date_Ccyymmdd__R_Field_6", "REDEFINE", pnd_Futures_Date_Ccyymmdd);
        pnd_Futures_Date_Ccyymmdd__Filler1 = pnd_Futures_Date_Ccyymmdd__R_Field_6.newFieldInGroup("pnd_Futures_Date_Ccyymmdd__Filler1", "_FILLER1", FieldType.STRING, 
            2);
        pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Yymmdd = pnd_Futures_Date_Ccyymmdd__R_Field_6.newFieldInGroup("pnd_Futures_Date_Ccyymmdd_Pnd_Futures_Date_Yymmdd", 
            "#FUTURES-DATE-YYMMDD", FieldType.STRING, 6);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 10);
        pnd_Counter_Ms = localVariables.newFieldInRecord("pnd_Counter_Ms", "#COUNTER-MS", FieldType.PACKED_DECIMAL, 10);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.INTEGER, 2);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_Ws_Cntr_Deductions = localVariables.newFieldInRecord("pnd_Ws_Cntr_Deductions", "#WS-CNTR-DEDUCTIONS", FieldType.NUMERIC, 2);
        pnd_Ws_Total_Lines_Of_Deductions = localVariables.newFieldInRecord("pnd_Ws_Total_Lines_Of_Deductions", "#WS-TOTAL-LINES-OF-DEDUCTIONS", FieldType.NUMERIC, 
            3);

        pnd_Ws_Key = localVariables.newGroupInRecord("pnd_Ws_Key", "#WS-KEY");
        pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 
            2);
        pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 
            2);
        pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex", "#WS-SIMPLEX-DUPLEX-MULTIPLEX", 
            FieldType.STRING, 1);
        pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code", "#WS-SAVE-CONTRACT-HOLD-CODE", 
            FieldType.STRING, 4);
        pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Work_Pymnt_Prcss_Seq_Nbr = localVariables.newFieldInRecord("pnd_Work_Pymnt_Prcss_Seq_Nbr", "#WORK-PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);

        pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_7 = localVariables.newGroupInRecord("pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_7", "REDEFINE", pnd_Work_Pymnt_Prcss_Seq_Nbr);
        pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Seq_Nbr = pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_7.newFieldInGroup("pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Seq_Nbr", 
            "#WORK-SEQ-NBR", FieldType.NUMERIC, 7);
        pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Installment_Nbr = pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_7.newFieldInGroup("pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Installment_Nbr", 
            "#WORK-INSTALLMENT-NBR", FieldType.NUMERIC, 2);
        pnd_Ws_First_Time = localVariables.newFieldInRecord("pnd_Ws_First_Time", "#WS-FIRST-TIME", FieldType.BOOLEAN, 1);
        pnd_Bypass_A_Pymnt = localVariables.newFieldInRecord("pnd_Bypass_A_Pymnt", "#BYPASS-A-PYMNT", FieldType.BOOLEAN, 1);
        pnd_Ws_Inv_Acct_Filler = localVariables.newFieldInRecord("pnd_Ws_Inv_Acct_Filler", "#WS-INV-ACCT-FILLER", FieldType.STRING, 180);
        pnd_Ws_Name_N_Address_Filler = localVariables.newFieldInRecord("pnd_Ws_Name_N_Address_Filler", "#WS-NAME-N-ADDRESS-FILLER", FieldType.STRING, 
            8);
        pnd_At_Top_Of_Body = localVariables.newFieldInRecord("pnd_At_Top_Of_Body", "#AT-TOP-OF-BODY", FieldType.BOOLEAN, 1);
        pnd_Ws_Lines_In_Grp = localVariables.newFieldInRecord("pnd_Ws_Lines_In_Grp", "#WS-LINES-IN-GRP", FieldType.PACKED_DECIMAL, 3);
        pnd_Lines_Remained_On_Page = localVariables.newFieldInRecord("pnd_Lines_Remained_On_Page", "#LINES-REMAINED-ON-PAGE", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_T1_Lines_On_Page = localVariables.newFieldArrayInRecord("pnd_Ws_T1_Lines_On_Page", "#WS-T1-LINES-ON-PAGE", FieldType.PACKED_DECIMAL, 3, 
            new DbsArrayController(1, 3));
        pnd_Ws_Current_X_Plex = localVariables.newFieldInRecord("pnd_Ws_Current_X_Plex", "#WS-CURRENT-X-PLEX", FieldType.NUMERIC, 1);

        pnd_Ws_Header_Record = localVariables.newGroupInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD");
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 
            1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", 
            FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 
            3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", 
            FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);

        pnd_Ws_Header_Record__R_Field_8 = pnd_Ws_Header_Record.newGroupInGroup("pnd_Ws_Header_Record__R_Field_8", "REDEFINE", pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record__R_Field_8.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record__R_Field_8.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", 
            FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Ws_Header_Record_Pymnt_Intrfce_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Filler = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Filler", "FILLER", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", "#WS-HEADER-FILLER", 
            FieldType.STRING, 121);

        pnd_Ws_Name_N_Address = localVariables.newGroupInRecord("pnd_Ws_Name_N_Address", "#WS-NAME-N-ADDRESS");

        pnd_Ws_Name_N_Address_Ph_Name = pnd_Ws_Name_N_Address.newGroupInGroup("pnd_Ws_Name_N_Address_Ph_Name", "PH-NAME");
        pnd_Ws_Name_N_Address_Ph_Last_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Ws_Name_N_Address_Ph_First_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Ws_Name_N_Address_Ph_Middle_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_Middle_Name", "PH-MIDDLE-NAME", 
            FieldType.STRING, 12);

        pnd_Ws_Occurs = localVariables.newGroupInRecord("pnd_Ws_Occurs", "#WS-OCCURS");
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct = pnd_Ws_Occurs.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 3);

        pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry = pnd_Ws_Occurs.newGroupArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry", "#WS-OCCURS-ENTRY", new DbsArrayController(1, 
            40));
        pnd_Ws_Occurs_Pnd_Ws_Side = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Side", "#WS-SIDE", FieldType.NUMERIC, 1);
        pnd_Ws_Occurs_Inv_Acct_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Ws_Occurs_Inv_Acct_Unit_Value = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9, 4);
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_State_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Local_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Occurs_Cntrct_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Option_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Occurs_Cntrct_Mode_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Cntr_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", 
            FieldType.NUMERIC, 3);

        pnd_Ws_Occurs_Pnd_Ws_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newGroupArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Deductions", "#WS-DEDUCTIONS", 
            new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Cde = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Occurs_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8);
        pnd_Ws_Occurs_Pymnt_Ded_Amt = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldInGroup("pnd_Ws_Occurs_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Ws_Occurs_Pnd_Pyhdr_Data = pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.newGroupInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Data", "#PYHDR-DATA");
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte", "#PYHDR-PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde", "#PYHDR-ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde", "#PYHDR-ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpladdr.initializeValues();
        ldaFcplcntu.initializeValues();
        ldaFcplpmnt.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_2.setInitialValue("MONTHLY SETTLEMENT BATCH EXTRACT CONTROL REPORT");
        pnd_Payment_Date.setInitialValue(" ");
        pnd_Inst_Tiaa_Amt.setInitialValue(0);
        pnd_Inst_Cref_Amt.setInitialValue(0);
        pnd_Ws_Prime_Key_Start.setInitialValue("H'0000C400'");
        pnd_Ws_Prime_Key_End.setInitialValue("H'0000C4FF'");
        pnd_C.setInitialValue(0);
        pnd_D.setInitialValue(0);
        pnd_I.setInitialValue(1);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_Ws_Cntr_Deductions.setInitialValue(0);
        pnd_Ws_Total_Lines_Of_Deductions.setInitialValue(0);
        pnd_Ws_Inv_Acct_Filler.setInitialValue(" ");
        pnd_Ws_Name_N_Address_Filler.setInitialValue(" ");
        pnd_Ws_T1_Lines_On_Page.getValue(1).setInitialValue(13);
        pnd_Ws_T1_Lines_On_Page.getValue(2).setInitialValue(49);
        pnd_Ws_T1_Lines_On_Page.getValue(3).setInitialValue(81);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp381() throws Exception
    {
        super("Fcpp381");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        pnd_Ws_First_Time.setValue(true);                                                                                                                                 //Natural: ASSIGN #WS-FIRST-TIME := TRUE
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(1).setValue("10");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 1 ) := '10'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(2).setValue("20");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 2 ) := '20'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(3).setValue("30");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 3 ) := '30'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(4).setValue("31");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 4 ) := '31'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(5).setValue("40");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 5 ) := '40'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(6).setValue("50");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 6 ) := '50'
        //* * START-OF-PROGRAM
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-CNTRL BY CNTL-ORG-CDE-INVRSE-DTE = 'CM'
        (
        "READ01",
        new Wc[] { new Wc("CNTL_ORG_CDE_INVRSE_DTE", ">=", "CM", WcType.BY) },
        new Oc[] { new Oc("CNTL_ORG_CDE_INVRSE_DTE", "ASC") },
        1
        );
        READ01:
        while (condition(ldaFcplcntu.getVw_fcp_Cons_Cntrl().readNextRow("READ01")))
        {
            if (condition(ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("CM")))                                                                                    //Natural: IF FCP-CONS-CNTRL.CNTL-ORGN-CDE = 'CM'
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"ERROR:  control record found for prior job");                            //Natural: WRITE *PROGRAM *TIME 'ERROR:  control record found for prior job'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Type_Cde().getValue("*").reset();                                                                                          //Natural: RESET FCP-CONS-CNTRL.CNTL-TYPE-CDE ( * ) FCP-CONS-CNTRL.CNTL-CNT ( * ) FCP-CONS-CNTRL.CNTL-GROSS-AMT ( * ) FCP-CONS-CNTRL.CNTL-NET-AMT ( * )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue("*").reset();
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue("*").reset();
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* *READ (1) FCP-CONS-CNTRL
        //* *    BY CNTL-ORG-CDE-INVRSE-DTE = 'MS'
        //*  .......... USE CONTROL FILE DATA TO SETUP START KEY FOR PROCESSING
        //* *  #WS-PRIME-KEY.PYMNT-CHECK-DTE := CNTL-CHECK-DTE
        //* *  #WS-PRIME-KEY.CNTRCT-ORGN-CDE := CNTL-ORGN-CDE
        pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.setValue("MS");                                                                                                                  //Natural: ASSIGN #WS-PRIME-KEY.CNTRCT-ORGN-CDE := 'MS'
        pnd_Ws_Prime_Key_Pymnt_Stats_Cde.setValue("D");                                                                                                                   //Natural: ASSIGN #WS-PRIME-KEY.PYMNT-STATS-CDE := 'D'
        //* *  #CNTL-ISN := *ISN
        //* *END-READ
        //*  MAKE SURE CONTROL RECORD WAS FOUND
        //* *IF #WS-PRIME-KEY = ' '
        //* *  #ABEND-CODE := 77
        //* *  WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
        //* *  WRITE 'Reason: NO CONTROL RECORD found'
        //* *  TERMINATE #ABEND-CODE
        //* *END-IF
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* *ASSIGN *ERROR-TA = 'INFP9000'
        //*           ************************
        //*           *  MAIN PROGRAM LOGIC  *
        //*           ************************
        pnd_Isn.reset();                                                                                                                                                  //Natural: RESET #ISN #COUNTER
        pnd_Counter.reset();
        pnd_Processing_Futures.setValue(false);                                                                                                                           //Natural: ASSIGN #PROCESSING-FUTURES := FALSE
        pnd_Bypass_A_Pymnt.setValue(false);                                                                                                                               //Natural: ASSIGN #BYPASS-A-PYMNT := FALSE
        setValueToSubstring("MS",pnd_Ws_Prime_Key_Start,1,2);                                                                                                             //Natural: MOVE 'MS' TO SUBSTRING ( #WS-PRIME-KEY-START,1,2 )
        setValueToSubstring("MS",pnd_Ws_Prime_Key_End,1,2);                                                                                                               //Natural: MOVE 'MS' TO SUBSTRING ( #WS-PRIME-KEY-END,1,2 )
                                                                                                                                                                          //Natural: PERFORM READ-PYMNTS
        sub_Read_Pymnts();
        if (condition(Global.isEscape())) {return;}
        pnd_Counter_Ms.setValue(pnd_Counter);                                                                                                                             //Natural: MOVE #COUNTER TO #COUNTER-MS
        getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"at end of 'current date' payments:",NEWLINE,"Number of M.S.     records read....:", //Natural: WRITE *PROGRAM *TIME 'at end of "current date" payments:' / 'Number of M.S.     records read....:'#COUNTER-MS / 'Number of payments selected:' #C-SELECTED-PYMNTS
            pnd_Counter_Ms,NEWLINE,"Number of payments selected:",pnd_C_Selected_Pymnts);
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"AT END OF program PROCESSING:",NEWLINE,"NUMBER OF RECORD TYPE 10:",              //Natural: WRITE *PROGRAM *TIME 'AT END OF program PROCESSING:' / 'NUMBER OF RECORD TYPE 10:' #C-RCRD-10 / 'NUMBER OF RECORD TYPE 20:' #C-RCRD-20 / 'NUMBER OF RECORD TYPE 30:' #C-RCRD-30
            pnd_C_Rcrd_10,NEWLINE,"NUMBER OF RECORD TYPE 20:",pnd_C_Rcrd_20,NEWLINE,"NUMBER OF RECORD TYPE 30:",pnd_C_Rcrd_30);
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REC
        sub_Write_Control_Rec();
        if (condition(Global.isEscape())) {return;}
        //* *  READ-PYMNTS.
        //* *READ        FCP-CONS-PYMNT BY CHK-DTE-ORG-CURR-TYP-SEQ
        //* *    STARTING FROM #WS-PRIME-KEY
        //* *READ FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ
        //* *STARTING FROM #WS-PRIME-KEY
        //* *IF #WS-PRIME-KEY.PYMNT-CHECK-DTE NE FCP-CONS-PYMNT.PYMNT-CHECK-DTE
        //* *    OR #WS-PRIME-KEY.CNTRCT-ORGN-CDE NE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        //* *    OR FCP-CONS-PYMNT.CNTRCT-CHECK-CRRNCY-CDE NE '1'
        //* **** OR FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND NE 1
        //* *  IF #WS-PRIME-KEY.CNTRCT-ORGN-CDE NE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        //* *      OR FCP-CONS-PYMNT.PYMNT-STATS-CDE NE 'D'
        //* *      OR FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE GT #PARM-INVRSE-DTE
        //* *    ESCAPE BOTTOM (READ-PYMNTS.)
        //* *  END-IF
        //* *  #ISN := *ISN
        //* *  #COUNTER := *COUNTER
        //* *  IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
        //* *  IF FCP-CONS-PYMNT.PYMNT-STATS-CDE EQ 'W'
        //* *    IF FCP-CONS-PYMNT.PYMNT-STATS-CDE EQ 'D'
        //* *      #BYPASS-A-PYMNT := FALSE
        //* *  ELSE /* THE PAYMENT WAS ALREADY PROCESSED.
        //* *    IF FCP-CONS-PYMNT.PYMNT-FTRE-IND = 'C'
        //* *        /* PREVENT REPROCESSING OF "current" PAYMENTS
        //* *      #ABEND-CODE := 79
        //* *      WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
        //* *      WRITE 'Reason: current payment records already processed'
        //* *      WRITE '='  FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        //* *        / '='  FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        //* *        / '=' FCP-CONS-PYMNT.PYMNT-INSTMT-NBR
        //* *        / '=' FCP-CONS-PYMNT.PYMNT-STATS-CDE
        //* *      TERMINATE #ABEND-CODE
        //* *      ELSE
        //* *        #BYPASS-A-PYMNT := TRUE
        //* *    END-IF
        //* *    END-IF
        //* *  END-IF
        //* *  IF #BYPASS-A-PYMNT
        //* *    ADD 1 TO #C-BYPASSED
        //* *    ESCAPE TOP /* (READ-PYMNTS.)
        //* *  END-IF
        //* **
        //* *  PERFORM ACCUM-FOR-CONTROL
        //* *  PERFORM EVALUATE-A-PYMNT-RECORD
        //* *END-READ /*
        //* *WRITE *PROGRAM *TIME 'at end of "current date" payments:'
        //* *  / 'Nunber of records read....:'#COUNTER
        //* *  / 'Nunber of records bypassed:' #C-BYPASSED
        //*  ....  WRAP THE LAST PAYMENT
        //* *IF NOT #WS-FIRST-TIME
        //* *  PERFORM WRAP-A-PAYMENT
        //* *END-IF
        //*  PROCESS "FUTURE" RECORDS, IF EXIST
        //* *#PROCESSING-FUTURES := TRUE
        //* *RESET #C-BYPASSED
        //*  ...... CALCULATE "FUTURES" DATE
        //* *MOVE EDITED FCP-CONS-CNTRL.CNTL-CHECK-DTE (EM=YYYYMMDD)
        //* *  TO #FUTURES-DATE-CCYYMMDD
        //* *IF  #FUTURES-DATE-MM  GT 11
        //* *  ADD 1 TO #FUTURES-DATE-CCYY
        //* *  #FUTURES-DATE-MM := 1
        //* *  #FUTURES-DATE-DD := 1
        //* *ELSE
        //* *  ADD 1 TO  #FUTURES-DATE-MM
        //* *  #FUTURES-DATE-DD := 1
        //* *END-IF
        //* *MOVE EDITED #FUTURES-DATE-YYMMDD
        //* *  TO   #WS-PRIME-KEY.PYMNT-CHECK-DTE (EM=YYMMDD)
        //* *#WS-FIRST-TIME := TRUE /* AVOID SEQ-NUM BREAK ON FIRST "FUTURES"
        //* *RESET
        //* *  #ISN
        //* *  #COUNTER
        //* *#BYPASS-A-PYMNT := FALSE
        //* *  READ-FUTURE-PYMNTS.
        //* *READ        FCP-CONS-PYMNT BY CHK-DTE-ORG-CURR-TYP-SEQ
        //* *    STARTING FROM #WS-PRIME-KEY
        //* *  IF #WS-PRIME-KEY.PYMNT-CHECK-DTE NE FCP-CONS-PYMNT.PYMNT-CHECK-DTE
        //* *    OR #WS-PRIME-KEY.CNTRCT-ORGN-CDE NE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        //* *      OR FCP-CONS-PYMNT.CNTRCT-CHECK-CRRNCY-CDE NE '1'
        //* **   OR FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND NE 1
        //* *    ESCAPE BOTTOM (READ-FUTURE-PYMNTS.)
        //* *  END-IF
        //* *  IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
        //* *    IF     FCP-CONS-PYMNT.PYMNT-STATS-CDE NE 'W'
        //* *        OR FCP-CONS-PYMNT.PYMNT-FTRE-IND NE 'F' /* FUTURE PAYMENT
        //* *      #BYPASS-A-PYMNT := TRUE
        //* *    ELSE /* NON PROCESSED FUTURE RECORD
        //* *      #BYPASS-A-PYMNT := FALSE
        //* *    END-IF
        //* *  END-IF
        //* *  IF #BYPASS-A-PYMNT
        //* *    ADD 1 TO #C-BYPASSED
        //* *    ESCAPE TOP /* (READ-FUTURE-PYMNTS.)
        //* *  END-IF
        //* *  #ISN := *ISN
        //* *  #COUNTER := *COUNTER
        //* *  PERFORM ACCUM-FOR-CONTROL
        //* *  PERFORM EVALUATE-A-PYMNT-RECORD
        //* *END-READ /*
        //*  ....  WRAP THE LAST PAYMENT
        //* *IF NOT #WS-FIRST-TIME
        //* *  PERFORM WRAP-A-PAYMENT
        //* *END-IF
        //*  ..... SHOW PROGRAM TOTALS AND STATISTICS
        //* *WRITE *PROGRAM *TIME 'AT END OF FUTURE PAYMENTS PROCESSING:'
        //* *  / 'Nunber of "FUTURE" payments read:' #COUNTER
        //* *  / 'Nunber of "FUTURE" payments bypassed:' #C-BYPASSED
        //* *WRITE *PROGRAM *TIME 'AT END OF program PROCESSING:'
        //* *  / 'NUMBER OF RECORD TYPE 10:' #C-RCRD-10
        //* *  / 'NUMBER OF RECORD TYPE 20:' #C-RCRD-20
        //* *  / 'NUMBER OF RECORD TYPE 30:' #C-RCRD-30
        //* *CALLNAT 'FCPN715'
        //* *  #PROGRAM /* INVOKE CONTROL REPORT VIA THIS PROGRAM
        //* *  #CNTL-ISN        /* ISN OF CONTROL RECORD USED
        //* *  #CT-CNTRCT-TYP (*)
        //* *  #CT-PAY-TYP    (*)
        //* *PERFORM WRITE-CONTROL-REC
        //* *WRITE *PROGRAM *TIME 'END OF  PROGRAM RUN'
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PYMNTS
        //* ****************************
        //* *  STARTING FROM #WS-PRIME-KEY
        //*  ======================================================================
        //* *DEFINE SUBROUTINE ACCUM-FOR-CONTROL
        //*  ======================================================================
        //*  DETERMINE INDECIES OF CONTROL COUNTERS
        //*  #CI1 = CONTROLS BY CONTRACT TYPE
        //*  #CI2 = CONTROLS BY PAYMENT TYPE (CHECK, EFT, GLOBAL PAY)
        //*  ---------------------------------------------------------------------
        //*  ............. DETERMINE INDEX BY CONTRACT TYPE
        //* *DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
        //* *  VALUE '10' #CI1 := 1
        //* *  VALUE '20' #CI1 := 2
        //* *  VALUE '30' #CI1 := 3
        //* *  VALUE '31' #CI1 := 4
        //* *  VALUE '40' #CI1 := 5
        //* *  VALUE '50' #CI1 := 6
        //*   ANY VALUE
        //*    IF #PROCESSING-FUTURES
        //*      ADD 6 TO #CI1
        //*    END-IF
        //* *  NONE VALUE
        //* *    ESCAPE TOP
        //* *END-DECIDE
        //*  .....IF CANADIAN ACCUM SEPARATELY
        //* *IF FCP-CONS-PYMNT.CNTRCT-CRRNCY-CDE = '2'
        //* *  #CI1 := 7
        //* *END-IF
        //*  ............... DETERMINE INDEX BY PAY TYPE
        //* *DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND
        //* *  VALUE 1 #CI2 := 1 /* CHECKS
        //* *  VALUE 2 #CI2 := 2 /* EFT
        //* *  VALUE 3 #CI2 := 3 /* GLOBALS
        //* *  NONE VALUE
        //* *    ESCAPE TOP
        //* *END-DECIDE
        //*  ...... ADD FOR CONTROL TOTALS
        //* *RESET #WS-GROSS-AMT
        //* *ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT(*) TO #WS-GROSS-AMT
        //* *ADD FCP-CONS-PYMNT.INV-ACCT-DVDND-AMT(*) TO #WS-GROSS-AMT
        //* *ADD FCP-CONS-PYMNT.INV-ACCT-DPI-AMT(*) TO #WS-GROSS-AMT
        //*  ...... ADD FOR CONTROL TOTALS BY CONTRACT TYPE
        //* *ADD #WS-GROSS-AMT TO
        //* *  #CT-CNTRCT-TYP.#GROSS-AMT (#CI1)
        //*  ...... ADD FOR CONTROL TOTALS BY PAY TYPE
        //* *ADD #WS-GROSS-AMT TO
        //* *  #CT-PAY-TYP.#GROSS-AMT (#CI2)
        //* *IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
        //* *  ADD 1 TO #CT-CNTRCT-TYP.#CNT (#CI1)
        //* *  ADD 1 TO #CT-PAY-TYP.#CNT    (#CI2)
        //* *  ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT
        //* *    TO #CT-CNTRCT-TYP.#NET-AMT (#CI1)
        //* *  ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT
        //* *    TO #CT-PAY-TYP.#NET-AMT    (#CI2)
        //* *END-IF
        //* *END-SUBROUTINE /* ACCUM-FOR-CONTROL
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-FOR-CONTROL
        //*  VALUE 'C', 'CN'
        //*  VALUE 'S', 'SN'
        //*  ...... ADD FOR CONTROL TOTALS
        //*  ...... ADD FOR CONTROL TOTALS BY CONTRACT TYPE
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REC
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: EVALUATE-A-PYMNT-RECORD
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRAP-A-PAYMENT
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-PYMNT-RCRD
        //*  ======================================================================
        //*  SCAN THE RECORD AND FOR EACH "element" TO BE PRINTED
        //*  DETERMINE HOW MANY LINES ARE IN THE "group of lines" TO BE PRINTED.
        //*  ALSO, DETERMINE ON WHICH PRINT PAGE ARE THE "lines" TO BE PRINTED.
        //*  TO DETERMINE THE NUMBER OF LINES REQUIRED FOR A "group of lines"
        //*  NOTE THE FOLLOWING:
        //*   1. THE DEDUCTIONS ARE TO BE PRINTED FOLLOWING THE FIRST
        //*      INVESTMENT ACCOUNT TAX DATA.
        //*   2. WHEN PRINTING A "group of lines", WHICH IS NOT AT THE TOP OF
        //*      THE BODY, THERE IS A NEED TO ALLOCATE A SEPARATING SPACE
        //*      LINE (TO SEPARATE FROM THE PREVIOUS GROUP).
        //*  TO DETERMINE ON WHICH PAGE TO PRINT THE "group of lines"
        //*  NOTE THE FOLLOWING:
        //*   1. FOR EACH PAGE (1ST, 2ND, ETC) THERE IS A PREDEFINED NUMBER OF
        //*      "body lines".
        //*   2. A "group of Lines" HAS TO FIT ON THE SAME PAGE,
        //*      OTHERWISE THE PROGRAM ABENDS.
        //*  ----------------------------------------------------------------------
        //*  ....... FOR ALL FUNDS IN RECORD
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ON-WHICH-SIDE-OF-PAPER
        //* ***
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-INV-ACCT-FIELDS
        //*  ======================================================================
        //*  FEW DATA ELEMENTS WHICH ARE NORMALLY KEPT IN THE HEADER-RECORD
        //*  AND ARE COMMON TO ALL PAYMENTS AND "groups of printed lines",
        //*  DATA ELEMENTS WHICH ARE COMMON TO ALL PAYMENTS AND "Groups Of
        //*  PRINTED LINES", are normally kept in the HEADER-RECORD.
        //*  HOWEVER, FEW OF THESE DATA ELEMENTS, MAY CHANGE FROM ONE PAYMENTS
        //*  RECORD TO ANOTHER.  THEREFORE, THIS DATA IS KEPT AT THE OCCURS TABLE
        //*  ATTACHED TO THE INV_ACCT DATA.  ALSO THE 1ST INV-ACCT OF EACH PAYMENT
        //*  RECORD IS INDICATED SO IN THE OCCURS TABLE.
        //*  ----------------------------------------------------------------------
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-OCCURS-RECORD
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-OCCURS-RECORD-2
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
        //* **   FCP-CONS-ADDR.PH-NAME
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NAME-N-ADDR-REC-2
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-MAILING-NAME
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAME-N-ADDRESS
        //*  ======================================================================
        //*  GET THE ADDRESS FOR THE CURRENT PAYMENT RECIEPIENT
        //*  THE RECORD IS KEPT IN THE VIEW AREA AND IS USED AT WRAP-A-PAYMENT
        //*  TO WRITE OUT ADDRESS RELATED DATA
        //*  ----------------------------------------------------------------------
        //*  ........... SET UP THE START KEY FOR NAME AND ADDRESS
        //*  ..... IF THE ADDRESS RECORD IS MISSING - WE TERMINATE ?
    }
    private void sub_Read_Pymnts() throws Exception                                                                                                                       //Natural: READ-PYMNTS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_First_Time.setValue(true);                                                                                                                                 //Natural: ASSIGN #WS-FIRST-TIME := TRUE
        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ = #WS-PRIME-KEY-START THRU #WS-PRIME-KEY-END
        (
        "RP",
        new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Prime_Key_Start, "And", WcType.BY) ,
        new Wc("ORGN_STATUS_INVRSE_SEQ", "<=", pnd_Ws_Prime_Key_End, WcType.BY) },
        new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
        );
        RP:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("RP")))
        {
            //*  CTS-0115 >>
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                     //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Counter.setValue(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER());                                                                                     //Natural: ASSIGN #COUNTER := *COUNTER
                                                                                                                                                                          //Natural: PERFORM ACCUM-FOR-CONTROL
            sub_Accum_For_Control();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RP"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RP"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM EVALUATE-A-PYMNT-RECORD
            sub_Evaluate_A_Pymnt_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RP"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RP"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-READ
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM WRAP-A-PAYMENT
            sub_Wrap_A_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        pnd_Counter.setValue(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER());                                                                                         //Natural: MOVE *COUNTER ( RP. ) TO #COUNTER
    }
    private void sub_Accum_For_Control() throws Exception                                                                                                                 //Natural: ACCUM-FOR-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  DETERMINE INDECIES OF CONTROL COUNTERS
        //*  #CI1 = CONTROLS BY CONTRACT TYPE
        //*  #CI2 = CONTROLS BY PAYMENT TYPE (CHECK, EFT, GLOBAL PAY)
        //*  ---------------------------------------------------------------------
        //*  ............. DETERMINE INDEX BY CONTRACT TYPE
        short decideConditionsMet1136 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE ' '
        if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" "))))
        {
            decideConditionsMet1136++;
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("DC")))                                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'DC'
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("L")))                                                                               //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'L'
                {
                    pnd_Ci1.setValue(1);                                                                                                                                  //Natural: ASSIGN #CI1 := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("PP")))                                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'PP'
                    {
                        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ftre_Ind().notEquals("F")))                                                                     //Natural: IF FCP-CONS-PYMNT.PYMNT-FTRE-IND NE 'F'
                        {
                            pnd_Ci1.setValue(2);                                                                                                                          //Natural: ASSIGN #CI1 := 2
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ci1.setValue(3);                                                                                                                          //Natural: ASSIGN #CI1 := 3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  JWO 2010-11
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'C', 'CN', 'CP', 'RP', 'PR'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
        {
            decideConditionsMet1136++;
            if (condition(((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("L") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("PP"))                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'L' OR = 'PP' AND FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'DC'
                && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("DC"))))
            {
                pnd_Ci1.setValue(12);                                                                                                                                     //Natural: ASSIGN #CI1 := 12
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((((((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("10") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("20"))             //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '10' OR = '20' OR = '30' OR = '31' OR = '40' OR = '50' AND FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'MS'
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("30")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("31")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("40")) 
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("50")) && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("MS"))))
            {
                pnd_Ci1.setValue(25);                                                                                                                                     //Natural: ASSIGN #CI1 := 25
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
        {
            decideConditionsMet1136++;
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("DC")))                                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'DC'
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("L")))                                                                               //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'L'
                {
                    pnd_Ci1.setValue(14);                                                                                                                                 //Natural: ASSIGN #CI1 := 14
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("PP")))                                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'PP'
                    {
                        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ftre_Ind().notEquals("F")))                                                                     //Natural: IF FCP-CONS-PYMNT.PYMNT-FTRE-IND NE 'F'
                        {
                            pnd_Ci1.setValue(15);                                                                                                                         //Natural: ASSIGN #CI1 := 15
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ci1.setValue(16);                                                                                                                         //Natural: ASSIGN #CI1 := 16
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("MS")))                                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'MS'
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("10")))                                                                              //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '10'
                {
                    pnd_Ci1.setValue(27);                                                                                                                                 //Natural: ASSIGN #CI1 := 27
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("20")))                                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '20'
                    {
                        pnd_Ci1.setValue(28);                                                                                                                             //Natural: ASSIGN #CI1 := 28
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("30")))                                                                      //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '30'
                        {
                            pnd_Ci1.setValue(29);                                                                                                                         //Natural: ASSIGN #CI1 := 29
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("31")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '31'
                            {
                                pnd_Ci1.setValue(30);                                                                                                                     //Natural: ASSIGN #CI1 := 30
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("40")))                                                              //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '40'
                                {
                                    pnd_Ci1.setValue(31);                                                                                                                 //Natural: ASSIGN #CI1 := 31
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("50")))                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '50'
                                    {
                                        pnd_Ci1.setValue(32);                                                                                                             //Natural: ASSIGN #CI1 := 32
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  JWO-2010-11
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'S', 'SN', 'SP'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
        {
            decideConditionsMet1136++;
            if (condition(((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("L") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("PP"))                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = 'L' OR = 'PP' AND FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'DC'
                && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("DC"))))
            {
                pnd_Ci1.setValue(13);                                                                                                                                     //Natural: ASSIGN #CI1 := 13
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((((((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("10") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("20"))             //Natural: IF FCP-CONS-PYMNT.CNTRCT-TYPE-CDE = '10' OR = '20' OR = '30' OR = '31' OR = '40' OR = '50' AND FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'MS'
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("30")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("31")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("40")) 
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde().equals("50")) && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("MS"))))
            {
                pnd_Ci1.setValue(26);                                                                                                                                     //Natural: ASSIGN #CI1 := 26
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet1213 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet1213++;
            //*  CHECKS AND REDRAWN CHECKS
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" "))) //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R' OR = ' '
            {
                pnd_Ci2.setValue(48);                                                                                                                                     //Natural: ASSIGN #CI2 := 48
                //*  EFT
                //*  GLOBALS TMM-2001-11
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet1213++;
            pnd_Ci2.setValue(49);                                                                                                                                         //Natural: ASSIGN #CI2 := 49
        }                                                                                                                                                                 //Natural: VALUE 3,4,9
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(3) || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(4) 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(9))))
        {
            decideConditionsMet1213++;
            pnd_Ci2.setValue(50);                                                                                                                                         //Natural: ASSIGN #CI2 := 50
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT(*) TO
        //* *  FCP-CONS-CNTRL.CNTL-GROSS-AMT(#CI2)
        //*  CANCELS        /* LEON 'CN' 08-05-99
        //*  CANCELS        /* JWO 2010-11
        //*  STOPS          /* LEON 'SN' 08-05-99
        //*  STOPS   /* JWO 2010-11
        short decideConditionsMet1236 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'CN'
        if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN"))))
        {
            decideConditionsMet1236++;
            pnd_Ci2.setValue(46);                                                                                                                                         //Natural: ASSIGN #CI2 := 46
        }                                                                                                                                                                 //Natural: VALUE 'CP', 'RP', 'PR'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
        {
            decideConditionsMet1236++;
            pnd_Ci2.setValue(46);                                                                                                                                         //Natural: ASSIGN #CI2 := 46
        }                                                                                                                                                                 //Natural: VALUE 'S', 'SN'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN"))))
        {
            decideConditionsMet1236++;
            pnd_Ci2.setValue(47);                                                                                                                                         //Natural: ASSIGN #CI2 := 47
        }                                                                                                                                                                 //Natural: VALUE 'SP'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
        {
            decideConditionsMet1236++;
            pnd_Ci2.setValue(47);                                                                                                                                         //Natural: ASSIGN #CI2 := 47
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Type_Cde().getValue(pnd_Ci1).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde());                                        //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-TYPE-CDE ( #CI1 ) := FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(pnd_Ci1).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue("*"));                          //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( * ) TO FCP-CONS-CNTRL.CNTL-GROSS-AMT ( #CI1 )
        //*  ...... ADD FOR CONTROL TOTALS BY PAY TYPE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(pnd_Ci2).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue("*"));                          //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( * ) TO FCP-CONS-CNTRL.CNTL-GROSS-AMT ( #CI2 )
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                        //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
        {
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(pnd_Ci1).nadd(1);                                                                                           //Natural: ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT ( #CI1 )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(pnd_Ci2).nadd(1);                                                                                           //Natural: ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT ( #CI2 )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(pnd_Ci2).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                         //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO FCP-CONS-CNTRL.CNTL-NET-AMT ( #CI2 )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(pnd_Ci1).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                         //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO FCP-CONS-CNTRL.CNTL-NET-AMT ( #CI1 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  ACCUM-FOR-CONTROL
    }
    private void sub_Write_Control_Rec() throws Exception                                                                                                                 //Natural: WRITE-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  LLNAT 'FCPN115' USING FCPA115  /* REPLACED BY CPWN115  - ROXAN 12/02
        DbsUtil.callnat(Cpwn115.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                               //Natural: CALLNAT 'CPWN115' USING CPWA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa115.getCpwa115_Error_Code().notEquals(getZero())))                                                                                           //Natural: IF CPWA115.ERROR-CODE NE 0
        {
            getReports().write(0, ReportOption.NOTITLE,"ERROR: FCPP370 detects invalid RETURN-CODE of",pdaCpwa115.getCpwa115_Error_Code(),"from",pdaCpwa115.getCpwa115_Error_Program()); //Natural: WRITE 'ERROR: FCPP370 detects invalid RETURN-CODE of' CPWA115.ERROR-CODE 'from' CPWA115.ERROR-PROGRAM
            if (Global.isEscape()) return;
            //*  STOP
            DbsUtil.terminate(80);  if (true) return;                                                                                                                     //Natural: TERMINATE 80
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().setValue("CM");                                                                                                     //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-ORGN-CDE := 'CM'
        pnd_Date_Numeric.setValue(pdaCpwa115.getCpwa115_Interface_Date());                                                                                                //Natural: ASSIGN #DATE-NUMERIC := CPWA115.INTERFACE-DATE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cycle_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Numeric_Pnd_Date_Alpha);                                    //Natural: MOVE EDITED #DATE-ALPHA TO FCP-CONS-CNTRL.CNTL-CYCLE-DTE ( EM = YYYYMMDD )
        pnd_Date_Numeric.setValue(pdaCpwa115.getCpwa115_Payment_Date());                                                                                                  //Natural: ASSIGN #DATE-NUMERIC := CPWA115.PAYMENT-DATE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Numeric_Pnd_Date_Alpha);                                    //Natural: MOVE EDITED #DATE-ALPHA TO FCP-CONS-CNTRL.CNTL-CHECK-DTE ( EM = YYYYMMDD )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte().compute(new ComputeParameters(false, ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte()), (DbsField.subtract(100000000, //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-INVRSE-DTE := ( 100000000 - CPWA115.PAYMENT-DATE )
            pdaCpwa115.getCpwa115_Payment_Date())));
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().insertDBRow();                                                                                                                 //Natural: STORE FCP-CONS-CNTRL
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Evaluate_A_Pymnt_Record() throws Exception                                                                                                           //Natural: EVALUATE-A-PYMNT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  ........... IF NEW PAYMENT
        if (condition(pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num.notEquals(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num())))                                         //Natural: IF #WS-SAVE-PYMNT-PRCSS-SEQ-NUM NE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
        {
            //*  IF FIRST PAYMENT BEING PROCESSED
            if (condition(pnd_Ws_First_Time.getBoolean()))                                                                                                                //Natural: IF #WS-FIRST-TIME
            {
                pnd_Ws_First_Time.setValue(false);                                                                                                                        //Natural: ASSIGN #WS-FIRST-TIME := FALSE
                //*   WRAP THE PREVIOUS PAYMENT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM WRAP-A-PAYMENT
                sub_Wrap_A_Payment();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
            sub_Initialize_Fields();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-NAME-N-ADDRESS
            sub_Read_Name_N_Address();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  ........... PROCESS A PAYMENT RECORD
                                                                                                                                                                          //Natural: PERFORM PROCESS-PYMNT-RCRD
        sub_Process_Pymnt_Rcrd();
        if (condition(Global.isEscape())) {return;}
        //*  EVALUATE-A-PYMNT-RECORD
    }
    private void sub_Wrap_A_Payment() throws Exception                                                                                                                    //Natural: WRAP-A-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  WRITE OUT THE PREVIOUS PAYMENT RECORDS FROM THE WORKING STORAGE TABLES
        //*  ----------------------------------------------------------------------
        //* *--> INSERTED 03-31-95 BY FRANK
        //* *--> THE FIELD #WS-KEY.#WS-PYMNT-CHECK-NBR IS USE TO HOLD THE CHECK
        //* *--> PAYMENT DATE (INVERSE) BECAUSE WE CAN HAVE MULTIPLE SEQUENCE
        //* *--> NUMBERED RECORDS WITH 'DIFFERENT' CHECK PAYMENT DATE.
        //* *--> THE INVERSE DATE AND SEQUENCE NUMBER MAKES IT UNIQUE.
        //* *-----------------------------------------------------------------*
        //* *ADD 1 TO #C-RCRD-10
        //* *ASSIGN #WS-RECORD-LEVEL-NMBR = 10
        //* *ASSIGN #WS-RECORD-OCCUR-NMBR = 1
        //* *WRITE WORK FILE 8
        //* *  #WS-KEY
        //* *  #WS-HEADER-RECORD
        //* *PERFORM SEPARATE-N-WRITE-OCCURS-RECORD
        //* *PERFORM SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
        //*  IB 9/16/99
        pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte.setValue(pnd_Cnr_Orgnl_Invrse_Dte);                                                                                     //Natural: MOVE #CNR-ORGNL-INVRSE-DTE TO #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
        if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                    //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
        {
            pnd_Ws_Check_Date.setValue(pnd_Ws_Header_Record_Cntrct_Invrse_Dte);                                                                                           //Natural: ASSIGN #WS-CHECK-DATE := #WS-HEADER-RECORD.CNTRCT-INVRSE-DTE
            pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr.setValue(pnd_Ws_Check_Date_Pnd_Ws_Inverse_7_Bytes);                                                                         //Natural: ASSIGN #WS-KEY.#WS-PYMNT-CHECK-NBR := #WS-INVERSE-7-BYTES
            pnd_C_Rcrd_10.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-RCRD-10
            //* *WRITE 'FCPP381 REDRAW HERE-' #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
            //* *#WS-HEADER-RECORD.CNTRCT-PAYEE-CDE
            //* *#WS-HEADER-RECORD.CNTRCT-ORGN-CDE
            //* *#WS-HEADER-RECORD.CNTRCT-TYPE-CDE
            pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(10);                                                                                                             //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 10
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(1);                                                                                                              //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = 1
            getWorkFiles().write(8, false, pnd_Ws_Key, pnd_Ws_Header_Record);                                                                                             //Natural: WRITE WORK FILE 8 #WS-KEY #WS-HEADER-RECORD
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-OCCURS-RECORD
            sub_Separate_N_Write_Occurs_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
            sub_Separate_N_Write_Name_N_Addr_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  LEON  08-05-99
            //*  JWO 2010-11
            if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S")                //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
                || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN") || 
                pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP") 
                || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR")))
            {
                //* *WRITE 'FCPP381 CANCEL HERE-' #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
                //* *#WS-HEADER-RECORD.CNTRCT-PAYEE-CDE
                //* *#WS-HEADER-RECORD.CNTRCT-ORGN-CDE
                //* *#WS-HEADER-RECORD.CNTRCT-TYPE-CDE
                pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(10);                                                                                                         //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 10
                pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(1);                                                                                                          //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = 1
                getWorkFiles().write(2, false, pnd_Ws_Key, pnd_Ws_Header_Record);                                                                                         //Natural: WRITE WORK FILE 2 #WS-KEY #WS-HEADER-RECORD
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-OCCURS-RECORD-2
                sub_Separate_N_Write_Occurs_Record_2();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-NAME-N-ADDR-REC-2
                sub_Write_Name_N_Addr_Rec_2();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRAP-A-PAYMENT
    }
    private void sub_Process_Pymnt_Rcrd() throws Exception                                                                                                                //Natural: PROCESS-PYMNT-RCRD
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #L 1 TO C*INV-ACCT
        for (pnd_L.setValue(1); condition(pnd_L.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())); pnd_L.nadd(1))
        {
            //*  POINT TO NEXT WORKING STORAGE TABLE ENTRY
            pnd_K.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #K
            //*  MOVE DATA INTO TABLE ENTRY
                                                                                                                                                                          //Natural: PERFORM MOVE-INV-ACCT-FIELDS
            sub_Move_Inv_Acct_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PLACE IT ON THE PAPER
                                                                                                                                                                          //Natural: PERFORM PRINT-ON-WHICH-SIDE-OF-PAPER
            sub_Print_On_Which_Side_Of_Paper();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  PROCESS-PYMNT-RCRD
    }
    private void sub_Print_On_Which_Side_Of_Paper() throws Exception                                                                                                      //Natural: PRINT-ON-WHICH-SIDE-OF-PAPER
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  ----------------------------------------------------------------------
        //*  ........ 1 DEDUCTION GROUP FOR THE PAYMENT RECORD
        //*  THE FIRST INVS-ACCT FOR THIS RECORD
        if (condition(pnd_L.equals(1)))                                                                                                                                   //Natural: IF #L = 1
        {
            pnd_Ws_Lines_In_Grp.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                                                        //Natural: ASSIGN #WS-LINES-IN-GRP := FCP-CONS-PYMNT.C*PYMNT-DED-GRP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Lines_In_Grp.reset();                                                                                                                                  //Natural: RESET #WS-LINES-IN-GRP
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... NOW COUNT THE TAX "elements"
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_L).notEquals(getZero())))                                                        //Natural: IF FCP-CONS-PYMNT.INV-ACCT-FDRL-TAX-AMT ( #L ) NE 0
        {
            pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WS-LINES-IN-GRP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Tax_Amt().getValue(pnd_L).notEquals(getZero())))                                                       //Natural: IF FCP-CONS-PYMNT.INV-ACCT-STATE-TAX-AMT ( #L ) NE 0
        {
            pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WS-LINES-IN-GRP
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Tax_Amt().getValue(pnd_L).notEquals(getZero())))                                                       //Natural: IF FCP-CONS-PYMNT.INV-ACCT-LOCAL-TAX-AMT ( #L ) NE 0
        {
            pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WS-LINES-IN-GRP
        }                                                                                                                                                                 //Natural: END-IF
        //*   IF FIRST "group" IN THE BODY AREA (SIMPLEX, DUPLEX, OR ANY)
        if (condition(pnd_At_Top_Of_Body.getBoolean()))                                                                                                                   //Natural: IF #AT-TOP-OF-BODY
        {
            pnd_At_Top_Of_Body.setValue(false);                                                                                                                           //Natural: ASSIGN #AT-TOP-OF-BODY := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Lines_In_Grp.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WS-LINES-IN-GRP
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... MINIMUM 3 LINES PER PRINTING GROUP
        if (condition(pnd_Ws_Lines_In_Grp.less(3)))                                                                                                                       //Natural: IF #WS-LINES-IN-GRP LT 3
        {
            pnd_Ws_Lines_In_Grp.setValue(3);                                                                                                                              //Natural: ASSIGN #WS-LINES-IN-GRP := 3
        }                                                                                                                                                                 //Natural: END-IF
        //*  ..... IF FITS PRINT ON THIS PAGE, OHTERWISE PRINT ON FOLLOWING PAGE
        if (condition(pnd_Lines_Remained_On_Page.less(pnd_Ws_Lines_In_Grp)))                                                                                              //Natural: IF #LINES-REMAINED-ON-PAGE LT #WS-LINES-IN-GRP
        {
            //*  ...........  TURN TO THE NEXT PAGE
            pnd_Ws_Current_X_Plex.nadd(1);                                                                                                                                //Natural: ADD 1 TO #WS-CURRENT-X-PLEX
            pnd_Lines_Remained_On_Page.setValue(pnd_Ws_T1_Lines_On_Page.getValue(pnd_Ws_Current_X_Plex));                                                                 //Natural: ASSIGN #LINES-REMAINED-ON-PAGE := #WS-T1-LINES-ON-PAGE ( #WS-CURRENT-X-PLEX )
            //*  NO NEED FOR SPACE FROM PREV GRP
            pnd_Ws_Lines_In_Grp.nsubtract(1);                                                                                                                             //Natural: SUBTRACT 1 FROM #WS-LINES-IN-GRP
            if (condition(pnd_Lines_Remained_On_Page.less(pnd_Ws_Lines_In_Grp)))                                                                                          //Natural: IF #LINES-REMAINED-ON-PAGE LT #WS-LINES-IN-GRP
            {
                pnd_Abend_Code.setValue(80);                                                                                                                              //Natural: ASSIGN #ABEND-CODE := 80
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"PROGRAM TERMINATES WITH ABEND CODE:",pnd_Abend_Code);                                     //Natural: WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
                if (Global.isEscape()) return;
                getReports().write(0, ReportOption.NOTITLE,"Reason: Printing group does not fit on a single page");                                                       //Natural: WRITE 'Reason: Printing group does not fit on a single page'
                if (Global.isEscape()) return;
                DbsUtil.terminate(pnd_Abend_Code);  if (true) return;                                                                                                     //Natural: TERMINATE #ABEND-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_K).setValue(pnd_Ws_Current_X_Plex);                                                                                        //Natural: ASSIGN #WS-SIDE ( #K ) := #WS-CURRENT-X-PLEX
        pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex.setValue(pnd_Ws_Current_X_Plex);                                                                                       //Natural: ASSIGN #WS-SIMPLEX-DUPLEX-MULTIPLEX := #WS-CURRENT-X-PLEX
        pnd_Lines_Remained_On_Page.nsubtract(pnd_Ws_Lines_In_Grp);                                                                                                        //Natural: SUBTRACT #WS-LINES-IN-GRP FROM #LINES-REMAINED-ON-PAGE
        //*  PRINT-ON-WHICH-SIDE-OF-PAPER
    }
    private void sub_Move_Inv_Acct_Fields() throws Exception                                                                                                              //Natural: MOVE-INV-ACCT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Settlmnt_Dte());                                          //Natural: ASSIGN #WS-OCCURS.#PYHDR-PYMNT-SETTLMNT-DTE ( #K ) := FCP-CONS-PYMNT.PYMNT-SETTLMNT-DTE
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Annt_Ctznshp_Cde());                                              //Natural: ASSIGN #WS-OCCURS.#PYHDR-ANNT-CTZNSHP-CDE ( #K ) := FCP-CONS-PYMNT.ANNT-CTZNSHP-CDE
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Annt_Rsdncy_Cde());                                                //Natural: ASSIGN #WS-OCCURS.#PYHDR-ANNT-RSDNCY-CDE ( #K ) := FCP-CONS-PYMNT.ANNT-RSDNCY-CDE
        pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_L));                                                //Natural: ASSIGN #WS-OCCURS.INV-ACCT-CDE ( #K ) := FCP-CONS-PYMNT.INV-ACCT-CDE ( #L )
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Unit_Qty().getValue(pnd_L));                                      //Natural: ASSIGN #WS-OCCURS.INV-ACCT-UNIT-QTY ( #K ) := FCP-CONS-PYMNT.INV-ACCT-UNIT-QTY ( #L )
        pnd_Ws_Occurs_Inv_Acct_Unit_Value.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Unit_Value().getValue(pnd_L));                                  //Natural: ASSIGN #WS-OCCURS.INV-ACCT-UNIT-VALUE ( #K ) := FCP-CONS-PYMNT.INV-ACCT-UNIT-VALUE ( #L )
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue(pnd_L));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-SETTL-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fed_Cde().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-FED-CDE ( #K ) := FCP-CONS-PYMNT.INV-ACCT-FED-CDE ( #L )
        pnd_Ws_Occurs_Inv_Acct_State_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Cde().getValue(pnd_L));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-STATE-CDE ( #K ) := FCP-CONS-PYMNT.INV-ACCT-STATE-CDE ( #L )
        pnd_Ws_Occurs_Inv_Acct_Local_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Cde().getValue(pnd_L));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-LOCAL-CDE ( #K ) := FCP-CONS-PYMNT.INV-ACCT-LOCAL-CDE ( #L )
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Ivc_Amt().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-IVC-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-IVC-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dci_Amt().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DCI-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-DCI-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dpi_Amt().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DPI-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-DPI-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Start_Accum_Amt().getValue(pnd_L));                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-START-ACCUM-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-START-ACCUM-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_End_Accum_Amt().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-END-ACCUM-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dvdnd_Amt().getValue(pnd_L));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DVDND-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-DVDND-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Ivc_Ind().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-IVC-IND ( #K ) := FCP-CONS-PYMNT.INV-ACCT-IVC-IND ( #L )
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Adj_Ivc_Amt().getValue(pnd_L));                                //Natural: ASSIGN #WS-OCCURS.INV-ACCT-ADJ-IVC-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-ADJ-IVC-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Valuat_Period().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-VALUAT-PERIOD ( #K ) := FCP-CONS-PYMNT.INV-ACCT-VALUAT-PERIOD ( #L )
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_L));                              //Natural: ASSIGN #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-FDRL-TAX-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Tax_Amt().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-STATE-TAX-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Tax_Amt().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-LOCAL-TAX-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Exp_Amt().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-EXP-AMT ( #K ) := FCP-CONS-PYMNT.INV-ACCT-EXP-AMT ( #L )
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-PPCN-NBR ( #K ) := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        pnd_Ws_Occurs_Cntrct_Payee_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                        //Natural: ASSIGN #WS-OCCURS.CNTRCT-PAYEE-CDE ( #K ) := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind());                                              //Natural: ASSIGN #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #K ) := FCP-CONS-PYMNT.CNTRCT-PYMNT-TYPE-IND
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind());                                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #K ) := FCP-CONS-PYMNT.CNTRCT-STTLMNT-TYPE-IND
        pnd_Ws_Occurs_Cntrct_Option_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Option_Cde());                                                      //Natural: ASSIGN #WS-OCCURS.CNTRCT-OPTION-CDE ( #K ) := FCP-CONS-PYMNT.CNTRCT-OPTION-CDE
        pnd_Ws_Occurs_Cntrct_Mode_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Mode_Cde());                                                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-MODE-CDE ( #K ) := FCP-CONS-PYMNT.CNTRCT-MODE-CDE
        //*  .....KEEP DEDUCTION GROUP ONLY ONCE FOR THE PAYMENT RECORD
        //*  THE FIRST INVS-ACCT FOR THIS RECORD
        if (condition(pnd_L.equals(1)))                                                                                                                                   //Natural: IF #L = 1
        {
            pnd_Ws_Occurs_Pnd_Cntr_Deductions.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                          //Natural: ASSIGN #CNTR-DEDUCTIONS ( #K ) := C*PYMNT-DED-GRP
            pnd_Ws_Occurs_Pymnt_Ded_Cde.getValue(pnd_K,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue("*"));                                        //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-CDE ( #K,* ) := FCP-CONS-PYMNT.PYMNT-DED-CDE ( * )
            pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde.getValue(pnd_K,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Payee_Cde().getValue("*"));                            //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-PAYEE-CDE ( #K,* ) := FCP-CONS-PYMNT.PYMNT-DED-PAYEE-CDE ( * )
            pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_K,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Amt().getValue("*"));                                        //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-AMT ( #K,* ) := FCP-CONS-PYMNT.PYMNT-DED-AMT ( * )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Occurs_Pnd_Cntr_Deductions.getValue(pnd_K).reset();                                                                                                    //Natural: RESET #CNTR-DEDUCTIONS ( #K ) #WS-OCCURS.PYMNT-DED-CDE ( #K,* ) #WS-OCCURS.PYMNT-DED-PAYEE-CDE ( #K,* ) #WS-OCCURS.PYMNT-DED-AMT ( #K,* )
            pnd_Ws_Occurs_Pymnt_Ded_Cde.getValue(pnd_K,"*").reset();
            pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde.getValue(pnd_K,"*").reset();
            pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_K,"*").reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Separate_N_Write_Occurs_Record() throws Exception                                                                                                    //Natural: SEPARATE-N-WRITE-OCCURS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(20);                                                                                                                 //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 20
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct.setValue(pnd_K);                                                                                                                  //Natural: ASSIGN #CNTR-INV-ACCT = #K
        pnd_C_Rcrd_20.nadd(pnd_K);                                                                                                                                        //Natural: ADD #K TO #C-RCRD-20
        FOR02:                                                                                                                                                            //Natural: FOR #C = 1 TO #K
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_K)); pnd_C.nadd(1))
        {
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_C);                                                                                                          //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #C
            getWorkFiles().write(8, false, pnd_Ws_Key, pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct, pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.getValue(pnd_C), pnd_Ws_Inv_Acct_Filler);       //Natural: WRITE WORK FILE 8 #WS-KEY #CNTR-INV-ACCT #WS-OCCURS.#WS-OCCURS-ENTRY ( #C ) #WS-INV-ACCT-FILLER
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Separate_N_Write_Occurs_Record_2() throws Exception                                                                                                  //Natural: SEPARATE-N-WRITE-OCCURS-RECORD-2
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(20);                                                                                                                 //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 20
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct.setValue(pnd_K);                                                                                                                  //Natural: ASSIGN #CNTR-INV-ACCT = #K
        pnd_C_Rcrd_20.nadd(pnd_K);                                                                                                                                        //Natural: ADD #K TO #C-RCRD-20
        FOR03:                                                                                                                                                            //Natural: FOR #C = 1 TO #K
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_K)); pnd_C.nadd(1))
        {
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_C);                                                                                                          //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #C
            getWorkFiles().write(2, false, pnd_Ws_Key, pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct, pnd_Ws_Occurs_Pnd_Ws_Occurs_Entry.getValue(pnd_C), pnd_Ws_Inv_Acct_Filler);       //Natural: WRITE WORK FILE 2 #WS-KEY #CNTR-INV-ACCT #WS-OCCURS.#WS-OCCURS-ENTRY ( #C ) #WS-INV-ACCT-FILLER
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Separate_N_Write_Name_N_Addr_Record() throws Exception                                                                                               //Natural: SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
                                                                                                                                                                          //Natural: PERFORM GEN-MAILING-NAME
        sub_Gen_Mailing_Name();
        if (condition(Global.isEscape())) {return;}
        FOR04:                                                                                                                                                            //Natural: FOR #D = 1 TO FCP-CONS-ADDR.C*PYMNT-NME-AND-ADDR-GRP
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp())); pnd_D.nadd(1))
        {
            if (condition(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(pnd_D).notEquals(" ")))                                                                       //Natural: IF FCP-CONS-ADDR.PYMNT-NME ( #D ) NE ' '
            {
                pnd_C_Rcrd_30.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #C-RCRD-30
                pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(30);                                                                                                         //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 30
                pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_D);                                                                                                      //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #D
                getWorkFiles().write(8, false, pnd_Ws_Key, ldaFcpladdr.getFcp_Cons_Addr_Rcrd_Typ(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr(),  //Natural: WRITE WORK FILE 8 #WS-KEY FCP-CONS-ADDR.RCRD-TYP FCP-CONS-ADDR.CNTRCT-ORGN-CDE FCP-CONS-ADDR.CNTRCT-PPCN-NBR FCP-CONS-ADDR.CNTRCT-PAYEE-CDE FCP-CONS-ADDR.CNTRCT-INVRSE-DTE FCP-CONS-ADDR.PYMNT-PRCSS-SEQ-NBR #WS-NAME-N-ADDRESS.PH-NAME FCP-CONS-ADDR.PYMNT-NME ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE2-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE3-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE4-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE5-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE6-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-ZIP-CDE ( #D ) FCP-CONS-ADDR.PYMNT-POSTL-DATA ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-TYPE-IND ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LAST-CHG-DTE ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LAST-CHG-TME ( #D ) FCP-CONS-ADDR.PYMNT-EFT-TRANSIT-ID FCP-CONS-ADDR.PYMNT-EFT-ACCT-NBR FCP-CONS-ADDR.PYMNT-CHK-SAV-IND FCP-CONS-ADDR.PYMNT-DECEASED-NME FCP-CONS-ADDR.CNTRCT-HOLD-TME #WS-NAME-N-ADDRESS-FILLER
                    ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Invrse_Dte(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr(), 
                    pnd_Ws_Name_N_Address_Ph_Name, ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line1_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line2_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line3_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line4_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line5_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line6_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Zip_Cde().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Postl_Data().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Type_Ind().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Eft_Transit_Id(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Eft_Acct_Nbr(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Chk_Sav_Ind(), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Deceased_Nme(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Hold_Tme(), pnd_Ws_Name_N_Address_Filler);
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Name_N_Addr_Rec_2() throws Exception                                                                                                           //Natural: WRITE-NAME-N-ADDR-REC-2
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
                                                                                                                                                                          //Natural: PERFORM GEN-MAILING-NAME
        sub_Gen_Mailing_Name();
        if (condition(Global.isEscape())) {return;}
        FOR05:                                                                                                                                                            //Natural: FOR #D = 1 TO FCP-CONS-ADDR.C*PYMNT-NME-AND-ADDR-GRP
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp())); pnd_D.nadd(1))
        {
            if (condition(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(pnd_D).notEquals(" ")))                                                                       //Natural: IF FCP-CONS-ADDR.PYMNT-NME ( #D ) NE ' '
            {
                pnd_C_Rcrd_30.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #C-RCRD-30
                pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(30);                                                                                                         //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 30
                pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_D);                                                                                                      //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #D
                getWorkFiles().write(2, false, pnd_Ws_Key, ldaFcpladdr.getFcp_Cons_Addr_Rcrd_Typ(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr(),  //Natural: WRITE WORK FILE 2 #WS-KEY FCP-CONS-ADDR.RCRD-TYP FCP-CONS-ADDR.CNTRCT-ORGN-CDE FCP-CONS-ADDR.CNTRCT-PPCN-NBR FCP-CONS-ADDR.CNTRCT-PAYEE-CDE FCP-CONS-ADDR.CNTRCT-INVRSE-DTE FCP-CONS-ADDR.PYMNT-PRCSS-SEQ-NBR #WS-NAME-N-ADDRESS.PH-NAME FCP-CONS-ADDR.PYMNT-NME ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE2-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE3-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE4-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE5-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LINE6-TXT ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-ZIP-CDE ( #D ) FCP-CONS-ADDR.PYMNT-POSTL-DATA ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-TYPE-IND ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LAST-CHG-DTE ( #D ) FCP-CONS-ADDR.PYMNT-ADDR-LAST-CHG-TME ( #D ) FCP-CONS-ADDR.PYMNT-EFT-TRANSIT-ID FCP-CONS-ADDR.PYMNT-EFT-ACCT-NBR FCP-CONS-ADDR.PYMNT-CHK-SAV-IND FCP-CONS-ADDR.PYMNT-DECEASED-NME FCP-CONS-ADDR.CNTRCT-HOLD-TME #WS-NAME-N-ADDRESS-FILLER
                    ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Invrse_Dte(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr(), 
                    pnd_Ws_Name_N_Address_Ph_Name, ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line1_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line2_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line3_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line4_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line5_Txt().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line6_Txt().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Zip_Cde().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Postl_Data().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Type_Ind().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Last_Chg_Dte().getValue(pnd_D), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Last_Chg_Tme().getValue(pnd_D), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Eft_Transit_Id(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Eft_Acct_Nbr(), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Chk_Sav_Ind(), 
                    ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Deceased_Nme(), ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Hold_Tme(), pnd_Ws_Name_N_Address_Filler);
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Gen_Mailing_Name() throws Exception                                                                                                                  //Natural: GEN-MAILING-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  GENERATE A MAILING NAME SO THAT VARIOUS COMPONENET LIKE JR., II.,
        //*  PROF., DR., ETC DO NOT APPEAR IN A NON SENSEABLE SEQUENCE.
        //*  GENERATE THE MAILING NAME COMPONENTS OUT OF:
        //*  1. THE NAME COMPONENTS FROM THE RECORD
        //*  2. NAME MESSAGING ALGORITHEM USED BY THE "name and address" TEAM
        //*     IN ORDER TO GENERATE THE "Mailing Name"
        //*  ----------------------------------------------------------------------
        pdaAdsa362.getPnd_In_Middle_Name().reset();                                                                                                                       //Natural: RESET #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE #IN-DATA #OUT-DATA
        pdaAdsa362.getPnd_Out_Middle_Name().reset();
        pdaAdsa362.getPnd_Out_Suffix().reset();
        pdaAdsa362.getPnd_Out_Prefix().reset();
        pdaAdsa362.getPnd_Out_Return_Code().reset();
        pdaAdsa360.getPnd_In_Data().reset();
        pdaAdsa360.getPnd_Out_Data().reset();
        pnd_Ws_Name_N_Address_Ph_First_Name.setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_First_Name());                                                                       //Natural: ASSIGN #WS-NAME-N-ADDRESS.PH-FIRST-NAME := FCP-CONS-ADDR.PH-FIRST-NAME
        pnd_Ws_Name_N_Address_Ph_Middle_Name.setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_Middle_Name());                                                                     //Natural: ASSIGN #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME := FCP-CONS-ADDR.PH-MIDDLE-NAME
        pnd_Ws_Name_N_Address_Ph_Last_Name.setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_Last_Name());                                                                         //Natural: ASSIGN #WS-NAME-N-ADDRESS.PH-LAST-NAME := FCP-CONS-ADDR.PH-LAST-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_First_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_First_Name());                                                                //Natural: ASSIGN #FIRST-NAME = FCP-CONS-ADDR.PH-FIRST-NAME
        pdaAdsa362.getPnd_In_Middle_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_Middle_Name());                                                                       //Natural: ASSIGN #IN-MIDDLE-NAME = FCP-CONS-ADDR.PH-MIDDLE-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_Last_Name().setValue(ldaFcpladdr.getFcp_Cons_Addr_Ph_Last_Name());                                                                  //Natural: ASSIGN #LAST-NAME = FCP-CONS-ADDR.PH-LAST-NAME
        //* *CALLNAT 'PDQN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX/*VIKRAM
        //*  VIKRAM
        DbsUtil.callnat(Adsn362.class , getCurrentProcessState(), pdaAdsa362.getPnd_In_Middle_Name(), pdaAdsa362.getPnd_Out_Middle_Name(), pdaAdsa362.getPnd_Out_Suffix(),  //Natural: CALLNAT 'ADSN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE
            pdaAdsa362.getPnd_Out_Prefix(), pdaAdsa362.getPnd_Out_Return_Code());
        if (condition(Global.isEscape())) return;
        pdaAdsa360.getPnd_In_Data_Pnd_Prefix().setValue(pdaAdsa362.getPnd_Out_Prefix());                                                                                  //Natural: ASSIGN #PREFIX = #OUT-PREFIX
        pdaAdsa360.getPnd_In_Data_Pnd_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                                        //Natural: ASSIGN #MIDDLE-NAME = #OUT-MIDDLE-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_Suffix().setValue(pdaAdsa362.getPnd_Out_Suffix());                                                                                  //Natural: ASSIGN #SUFFIX = #OUT-SUFFIX
        pdaAdsa360.getPnd_In_Data_Pnd_Length().setValue(38);                                                                                                              //Natural: ASSIGN #LENGTH = 38
        pdaAdsa360.getPnd_In_Data_Pnd_Format().setValue("1");                                                                                                             //Natural: ASSIGN #FORMAT = '1'
        //* *CALLNAT 'PDQN360' #IN-DATA #OUT-DATA /* VIKRAM
        //*  VIKRAM
        DbsUtil.callnat(Adsn360.class , getCurrentProcessState(), pdaAdsa360.getPnd_In_Data(), pdaAdsa360.getPnd_Out_Data());                                             //Natural: CALLNAT 'ADSN360' #IN-DATA #OUT-DATA
        if (condition(Global.isEscape())) return;
        pnd_Ws_Name_N_Address_Ph_First_Name.setValue(DbsUtil.compress(pdaAdsa362.getPnd_Out_Prefix(), pnd_Ws_Name_N_Address_Ph_First_Name));                              //Natural: COMPRESS #OUT-PREFIX #WS-NAME-N-ADDRESS.PH-FIRST-NAME INTO #WS-NAME-N-ADDRESS.PH-FIRST-NAME
        pnd_Ws_Name_N_Address_Ph_Middle_Name.setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                                               //Natural: ASSIGN #WS-NAME-N-ADDRESS.PH-MIDDLE-NAME = #OUT-MIDDLE-NAME
        pnd_Ws_Name_N_Address_Ph_Last_Name.setValue(DbsUtil.compress(pnd_Ws_Name_N_Address_Ph_Last_Name, pdaAdsa362.getPnd_Out_Suffix()));                                //Natural: COMPRESS #WS-NAME-N-ADDRESS.PH-LAST-NAME #OUT-SUFFIX INTO #WS-NAME-N-ADDRESS.PH-LAST-NAME
        //*  GEN-MAILING-NAME
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  INITIALIZE FIELDS FOR PROCESSING OF A PAYMENT
        //*  ----------------------------------------------------------------------
        pnd_Ws_Gross_Amt.reset();                                                                                                                                         //Natural: RESET #WS-GROSS-AMT #WS-OCCURS #K #WS-TOTAL-LINES-OF-DEDUCTIONS
        pnd_Ws_Occurs.reset();
        pnd_K.reset();
        pnd_Ws_Total_Lines_Of_Deductions.reset();
        pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                         //Natural: ASSIGN #WS-SAVE-PYMNT-PRCSS-SEQ-NUM := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
        pnd_Ws_Header_Record.setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                         //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #WS-HEADER-RECORD
        //*  IB 9/16/99
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                        //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 1
        {
            pnd_Cnr_Orgnl_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Orgnl_Invrse_Dte());                                                                      //Natural: MOVE FCP-CONS-PYMNT.CNR-ORGNL-INVRSE-DTE TO #CNR-ORGNL-INVRSE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Hold_Cde().equals(" ")))                                                                                       //Natural: IF FCP-CONS-PYMNT.CNTRCT-HOLD-CDE EQ ' '
        {
            pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code.setValue("0000");                                                                                                   //Natural: ASSIGN #WS-SAVE-CONTRACT-HOLD-CODE := '0000'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Hold_Cde());                                                          //Natural: ASSIGN #WS-SAVE-CONTRACT-HOLD-CODE := FCP-CONS-PYMNT.CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr());                                                                      //Natural: ASSIGN #WS-CNTRCT-CMBN-NBR := FCP-CONS-PYMNT.CNTRCT-CMBN-NBR
        pnd_At_Top_Of_Body.setValue(true);                                                                                                                                //Natural: ASSIGN #AT-TOP-OF-BODY := TRUE
        pnd_Ws_Current_X_Plex.setValue(1);                                                                                                                                //Natural: ASSIGN #WS-CURRENT-X-PLEX := 1
        pnd_Lines_Remained_On_Page.setValue(pnd_Ws_T1_Lines_On_Page.getValue(1));                                                                                         //Natural: ASSIGN #LINES-REMAINED-ON-PAGE := #WS-T1-LINES-ON-PAGE ( 1 )
    }
    private void sub_Read_Name_N_Address() throws Exception                                                                                                               //Natural: READ-NAME-N-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Nas_Start_Key_Cntrct_Ppcn_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                      //Natural: ASSIGN #NAS-START-KEY.CNTRCT-PPCN-NBR := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        pnd_Nas_Start_Key_Cntrct_Payee_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                                    //Natural: ASSIGN #NAS-START-KEY.CNTRCT-PAYEE-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        pnd_Nas_Start_Key_Cntrct_Orgn_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                      //Natural: ASSIGN #NAS-START-KEY.CNTRCT-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        pnd_Nas_Start_Key_Cntrct_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                                  //Natural: ASSIGN #NAS-START-KEY.CNTRCT-INVRSE-DTE := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
        pnd_Nas_Start_Key_Pymnt_Prcss_Seq_Nbr.setValue(pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num);                                                                       //Natural: ASSIGN #NAS-START-KEY.PYMNT-PRCSS-SEQ-NBR := #WS-SAVE-PYMNT-PRCSS-SEQ-NUM
        //*  ..........  NOW READ THE NAME AND ADDRESS FILE
        ldaFcpladdr.getVw_fcp_Cons_Addr().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #NAS-START-KEY
        (
        "READ_NAME_N_ADDRESS",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Nas_Start_Key, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        READ_NAME_N_ADDRESS:
        while (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().readNextRow("READ_NAME_N_ADDRESS")))
        {
            //*  ..... IF THE ADDRESS RECORD IS FOUND - WE ARE HAPPY
            if (condition((ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr())) && (ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde()))  //Natural: IF ( FCP-CONS-ADDR.CNTRCT-PPCN-NBR = FCP-CONS-PYMNT.CNTRCT-PPCN-NBR ) AND ( FCP-CONS-ADDR.CNTRCT-PAYEE-CDE = FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE ) AND ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE = FCP-CONS-PYMNT.CNTRCT-ORGN-CDE )
                && (ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde()))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Abend_Code.setValue(78);                                                                                                                              //Natural: ASSIGN #ABEND-CODE := 78
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"PROGRAM TERMINATES WITH ABEND CODE:",pnd_Abend_Code);                                     //Natural: WRITE *PROGRAM 'PROGRAM TERMINATES WITH ABEND CODE:' #ABEND-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_NAME_N_ADDRESS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_NAME_N_ADDRESS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,"Reason: NO ADDRESS RECORD found");                                                                            //Natural: WRITE 'Reason: NO ADDRESS RECORD found'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_NAME_N_ADDRESS"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_NAME_N_ADDRESS"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(pnd_Abend_Code);  if (true) return;                                                                                                     //Natural: TERMINATE #ABEND-CODE
            }                                                                                                                                                             //Natural: END-IF
            //*  READ-NAME-N-ADDRESS
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),"Page",getReports().getPageNumberDbs(0),  //Natural: WRITE NOTITLE *PROGRAM 41T #HEADER0-1 124T 'Page' *PAGE-NUMBER ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
