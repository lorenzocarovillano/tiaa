/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:14:47 PM
**        * FROM NATURAL PROGRAM : Fcpp120a
************************************************************
**        * FILE NAME            : Fcpp120a.java
**        * CLASS NAME           : Fcpp120a
**        * INSTANCE NAME        : Fcpp120a
************************************************************
***********************************************************************
* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
* PROGRAM  : FCPP120A CLONE OF FCPP120
*
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS LEDGER REPORTING EXTRACT
* CREATED  : 01/15/2020 CTS CPS SUNSET (CHG694525)
*
* FUNCTION : THIS PROGRAM WILL READ FCP-CONS-LEDGER FILE
*            AND CREATE A REPORTING EXTRACT FOR CPS LEDGER REPORTING
*            MODULES. PAYMENT AND ADDRESS INFORMATION WILL BE ATTACHED
*            TO EXTRACT RECORDS.
* HISTORY  :
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp120a extends BLNatBase
{
    // Data Areas
    private PdaFcpapmnt pdaFcpapmnt;
    private PdaFcpaaddr pdaFcpaaddr;
    private PdaFcpaext pdaFcpaext;
    private PdaFcpa800d pdaFcpa800d;
    private LdaFcplldgr ldaFcplldgr;
    private LdaFcpl121 ldaFcpl121;
    private LdaFcpl190a ldaFcpl190a;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl199b ldaFcpl199b;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Input_Date;
    private DbsField pnd_Ws_Pnd_Input_Parm;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Input_Orgn;
    private DbsField pnd_Ws__Filler1;
    private DbsField pnd_Ws_Pnd_Run_Type;
    private DbsField pnd_Ws_Pnd_Ledger_Superde_Start;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_S_Ldgr_Cntrct_Rcrd_Typ;
    private DbsField pnd_Ws_Pnd_S_Ldgr_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Pnd_S_Ldgr_Filler;
    private DbsField pnd_Ws_Pnd_Temp_Date_Alpha;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Temp_Date;
    private DbsField pnd_Ws_Pnd_C_Inv_Acct;
    private DbsField pnd_Ws_Pnd_Ledger_Records_Read;
    private DbsField pnd_Ws_Pnd_Ledger_Read;
    private DbsField pnd_Ws_Pnd_Ledger_Written;
    private DbsField pnd_Ws_Pnd_Paymnt_Written;
    private DbsField pnd_Ws_Pnd_Nz_Paymnt_Written;
    private DbsField pnd_Ws_Pnd_Al_Paymnt_Written;
    private DbsField pnd_Ws_Pnd_Ap_Paymnt_Written;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_X;
    private DbsField pnd_Ws_Pnd_Isa_Sub;

    private DbsGroup pnd_Lgr_Ext;
    private DbsField pnd_Lgr_Ext_Cntrct_Orgn_Cde;
    private DbsField pnd_Lgr_Ext_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Lgr_Ext_Pymnt_Acctg_Dte;
    private DbsField pnd_Lgr_Ext_Inv_Acct_Isa;
    private DbsField pnd_Lgr_Ext_Inv_Acct_Cde;

    private DbsGroup pnd_Lgr_Ext__R_Field_4;
    private DbsField pnd_Lgr_Ext_Inv_Acct_Cde_Num;
    private DbsField pnd_Lgr_Ext_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Lgr_Ext_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Lgr_Ext_Pymnt_Check_Dte;
    private DbsField pnd_Lgr_Ext_Pymnt_Settlmnt_Dte;

    private DbsGroup pnd_Lgr_Ext_Ph_Name;
    private DbsField pnd_Lgr_Ext_Ph_Last_Name;
    private DbsField pnd_Lgr_Ext_Ph_First_Name;
    private DbsField pnd_Lgr_Ext_Ph_Middle_Name;
    private DbsField pnd_Lgr_Ext_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Lgr_Ext_Cntrct_Cref_Nbr;
    private DbsField pnd_Lgr_Ext_Cntrct_Payee_Cde;
    private DbsField pnd_Lgr_Ext_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Lgr_Ext_Pymnt_Corp_Wpid;
    private DbsField pnd_Lgr_Ext_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Lgr_Ext_Pymnt_Cycle_Dte;
    private DbsField pnd_Lgr_Ext_Pymnt_Intrfce_Dte;
    private DbsField pnd_Lgr_Ext_Inv_Acct_Ledgr_Amt;
    private DbsField pnd_Lgr_Ext_Inv_Acct_Ledgr_Ind;
    private DbsField pnd_Lgr_Ext_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Lgr_Ext_Pnd_Ledger_Filler;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpapmnt = new PdaFcpapmnt(localVariables);
        pdaFcpaaddr = new PdaFcpaaddr(localVariables);
        pdaFcpaext = new PdaFcpaext(localVariables);
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        ldaFcplldgr = new LdaFcplldgr();
        registerRecord(ldaFcplldgr);
        registerRecord(ldaFcplldgr.getVw_fcp_Cons_Ledger());
        ldaFcpl121 = new LdaFcpl121();
        registerRecord(ldaFcpl121);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Input_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Input_Date", "#INPUT-DATE", FieldType.DATE);
        pnd_Ws_Pnd_Input_Parm = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 10);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Input_Parm);
        pnd_Ws_Pnd_Input_Orgn = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Input_Orgn", "#INPUT-ORGN", FieldType.STRING, 2);
        pnd_Ws__Filler1 = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Ws_Pnd_Run_Type = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 7);
        pnd_Ws_Pnd_Ledger_Superde_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ledger_Superde_Start", "#LEDGER-SUPERDE-START", FieldType.STRING, 6);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Ledger_Superde_Start);
        pnd_Ws_Pnd_S_Ldgr_Cntrct_Rcrd_Typ = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_S_Ldgr_Cntrct_Rcrd_Typ", "#S-LDGR-CNTRCT-RCRD-TYP", FieldType.STRING, 
            1);
        pnd_Ws_Pnd_S_Ldgr_Pymnt_Intrfce_Dte = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_S_Ldgr_Pymnt_Intrfce_Dte", "#S-LDGR-PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Ws_Pnd_S_Ldgr_Filler = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_S_Ldgr_Filler", "#S-LDGR-FILLER", FieldType.STRING, 1);
        pnd_Ws_Pnd_Temp_Date_Alpha = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Date_Alpha", "#TEMP-DATE-ALPHA", FieldType.STRING, 8);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Temp_Date_Alpha);
        pnd_Ws_Pnd_Temp_Date = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 8);
        pnd_Ws_Pnd_C_Inv_Acct = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_C_Inv_Acct", "#C-INV-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Ledger_Records_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ledger_Records_Read", "#LEDGER-RECORDS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ledger_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ledger_Read", "#LEDGER-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ledger_Written = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ledger_Written", "#LEDGER-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Paymnt_Written = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Paymnt_Written", "#PAYMNT-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Nz_Paymnt_Written = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Nz_Paymnt_Written", "#NZ-PAYMNT-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Al_Paymnt_Written = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Al_Paymnt_Written", "#AL-PAYMNT-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ap_Paymnt_Written = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ap_Paymnt_Written", "#AP-PAYMNT-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_X", "#X", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Isa_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isa_Sub", "#ISA-SUB", FieldType.PACKED_DECIMAL, 7);

        pnd_Lgr_Ext = localVariables.newGroupInRecord("pnd_Lgr_Ext", "#LGR-EXT");
        pnd_Lgr_Ext_Cntrct_Orgn_Cde = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Lgr_Ext_Cntrct_Cancel_Rdrw_Ind = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 
            1);
        pnd_Lgr_Ext_Pymnt_Acctg_Dte = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.STRING, 8);
        pnd_Lgr_Ext_Inv_Acct_Isa = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Inv_Acct_Isa", "INV-ACCT-ISA", FieldType.STRING, 5);
        pnd_Lgr_Ext_Inv_Acct_Cde = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);

        pnd_Lgr_Ext__R_Field_4 = pnd_Lgr_Ext.newGroupInGroup("pnd_Lgr_Ext__R_Field_4", "REDEFINE", pnd_Lgr_Ext_Inv_Acct_Cde);
        pnd_Lgr_Ext_Inv_Acct_Cde_Num = pnd_Lgr_Ext__R_Field_4.newFieldInGroup("pnd_Lgr_Ext_Inv_Acct_Cde_Num", "INV-ACCT-CDE-NUM", FieldType.NUMERIC, 2);
        pnd_Lgr_Ext_Cntrct_Check_Crrncy_Cde = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 
            1);
        pnd_Lgr_Ext_Inv_Acct_Ledgr_Nbr = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", FieldType.STRING, 15);
        pnd_Lgr_Ext_Pymnt_Check_Dte = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.STRING, 8);
        pnd_Lgr_Ext_Pymnt_Settlmnt_Dte = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.STRING, 8);

        pnd_Lgr_Ext_Ph_Name = pnd_Lgr_Ext.newGroupInGroup("pnd_Lgr_Ext_Ph_Name", "PH-NAME");
        pnd_Lgr_Ext_Ph_Last_Name = pnd_Lgr_Ext_Ph_Name.newFieldInGroup("pnd_Lgr_Ext_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Lgr_Ext_Ph_First_Name = pnd_Lgr_Ext_Ph_Name.newFieldInGroup("pnd_Lgr_Ext_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Lgr_Ext_Ph_Middle_Name = pnd_Lgr_Ext_Ph_Name.newFieldInGroup("pnd_Lgr_Ext_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Lgr_Ext_Cntrct_Ppcn_Nbr = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Lgr_Ext_Cntrct_Cref_Nbr = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 10);
        pnd_Lgr_Ext_Cntrct_Payee_Cde = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Lgr_Ext_Cntrct_Unq_Id_Nbr = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Lgr_Ext_Pymnt_Corp_Wpid = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Pymnt_Corp_Wpid", "PYMNT-CORP-WPID", FieldType.STRING, 6);
        pnd_Lgr_Ext_Pymnt_Prcss_Seq_Nbr = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pnd_Lgr_Ext_Pymnt_Cycle_Dte = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.STRING, 8);
        pnd_Lgr_Ext_Pymnt_Intrfce_Dte = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.STRING, 8);
        pnd_Lgr_Ext_Inv_Acct_Ledgr_Amt = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Lgr_Ext_Inv_Acct_Ledgr_Ind = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", FieldType.STRING, 1);
        pnd_Lgr_Ext_Cntrct_Annty_Ins_Type = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 
            1);
        pnd_Lgr_Ext_Pnd_Ledger_Filler = pnd_Lgr_Ext.newFieldInGroup("pnd_Lgr_Ext_Pnd_Ledger_Filler", "#LEDGER-FILLER", FieldType.STRING, 11);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplldgr.initializeValues();
        ldaFcpl121.initializeValues();
        ldaFcpl190a.initializeValues();
        ldaFcpl199b.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Ledger_Superde_Start.setInitialValue("5");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp120a() throws Exception
    {
        super("Fcpp120a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp120a|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Input_Parm);                                                                                //Natural: INPUT #INPUT-PARM
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, pnd_Ws_Pnd_Input_Date, new FieldAttributes ("AD='_'"), new ReportEditMask ("YYYYMMDD"));               //Natural: INPUT #INPUT-DATE ( AD = '_' EM = YYYYMMDD )
                if (condition(pnd_Ws_Pnd_Input_Date.equals(getZero())))                                                                                                   //Natural: IF #INPUT-DATE = 0
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(25),"INPUT INTERFACE DATE WAS NOT SUPPLIED",new TabSetting(77),"***");                                     //Natural: WRITE '***' 25T 'INPUT INTERFACE DATE WAS NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(50);  if (true) return;                                                                                                             //Natural: TERMINATE 50
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Abend_Ind().setValue(true);                                                                                        //Natural: MOVE TRUE TO PYMNT-ABEND-IND ADDR-ABEND-IND CURRENT-ADDR
                pdaFcpaaddr.getAddr_Work_Fields_Addr_Abend_Ind().setValue(true);
                pdaFcpaaddr.getAddr_Work_Fields_Current_Addr().setValue(true);
                pnd_Ws_Pnd_S_Ldgr_Pymnt_Intrfce_Dte.setValue(pnd_Ws_Pnd_Input_Date);                                                                                      //Natural: ASSIGN #S-LDGR-PYMNT-INTRFCE-DTE := #INPUT-DATE
                pnd_Ws_Pnd_S_Ldgr_Filler.setValue("H'00'");                                                                                                               //Natural: ASSIGN #S-LDGR-FILLER := H'00'
                pdaFcpaext.getExt_Pymnt_Ivc_Amt().reset();                                                                                                                //Natural: RESET EXT.PYMNT-IVC-AMT
                READWORK01:                                                                                                                                               //Natural: READ WORK 01 RECORD #LGR-EXT
                while (condition(getWorkFiles().read(1, pnd_Lgr_Ext)))
                {
                    if (condition(pnd_Lgr_Ext_Cntrct_Ppcn_Nbr.equals("IE254520") || pnd_Lgr_Ext_Cntrct_Ppcn_Nbr.equals("IE2545201")))                                     //Natural: IF #LGR-EXT.CNTRCT-PPCN-NBR = 'IE254520' OR = 'IE2545201'
                    {
                        getReports().write(0, Global.getPROGRAM()," BYPASSING IE254520 ");                                                                                //Natural: WRITE *PROGRAM ' BYPASSING IE254520 '
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(0, Global.getPROGRAM(),"=",pnd_Ws_Pnd_Ledger_Records_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));                                  //Natural: WRITE *PROGRAM '=' #LEDGER-RECORDS-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Pnd_Input_Orgn.equals("NZ")))                                                                                                    //Natural: IF #INPUT-ORGN = 'NZ'
                    {
                        if (condition(pnd_Ws_Pnd_Run_Type.equals("LEDGER")))                                                                                              //Natural: IF #RUN-TYPE = 'LEDGER'
                        {
                            if (condition(!(pnd_Lgr_Ext_Cntrct_Orgn_Cde.equals("NZ"))))                                                                                   //Natural: ACCEPT IF #LGR-EXT.CNTRCT-ORGN-CDE = 'NZ'
                            {
                                continue;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(!(pnd_Lgr_Ext_Cntrct_Orgn_Cde.equals("NZ") || pnd_Lgr_Ext_Cntrct_Orgn_Cde.equals("AL"))))                                       //Natural: ACCEPT IF #LGR-EXT.CNTRCT-ORGN-CDE = 'NZ' OR = 'AL'
                            {
                                continue;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Ws_Pnd_Input_Orgn.equals("AL")))                                                                                                //Natural: IF #INPUT-ORGN = 'AL'
                        {
                            if (condition(!(pnd_Lgr_Ext_Cntrct_Orgn_Cde.equals("AL"))))                                                                                   //Natural: ACCEPT IF #LGR-EXT.CNTRCT-ORGN-CDE = 'AL'
                            {
                                continue;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(!(pnd_Lgr_Ext_Cntrct_Orgn_Cde.notEquals("NZ") && pnd_Lgr_Ext_Cntrct_Orgn_Cde.notEquals("AL"))))                                 //Natural: ACCEPT IF #LGR-EXT.CNTRCT-ORGN-CDE NE 'NZ' AND #LGR-EXT.CNTRCT-ORGN-CDE NE 'AL'
                            {
                                continue;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*   PROCESS BOTH "NZ" & "AL"
                    if (condition(pnd_Ws_Pnd_Input_Orgn.equals("NZ") || pnd_Ws_Pnd_Input_Orgn.equals("AL")))                                                              //Natural: IF #INPUT-ORGN = 'NZ' OR = 'AL'
                    {
                        if (condition(pnd_Ws_Pnd_Run_Type.equals("LEDGER")))                                                                                              //Natural: IF #RUN-TYPE = 'LEDGER'
                        {
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-LEDGER-EXTRACT
                            sub_Move_To_Ledger_Extract();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-LEDGER-EXTRACT
                        sub_Move_To_Ledger_Extract();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TO-LEDGER-EXTRACT
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ISA-CODE
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TO-PAYMNT-EXTRACT
                //* *----------------------------------------- LEON 07-17-00 -----
                //* *---------------------------------------------------------
                //* ******
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TO-NZ-PAYMNT-EXTRACT
                //* ****************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-TO-AP-PAYMNT-EXTRACT
                //* ****************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PYMNT-RECORD
                //* *---------------------------------
                //* ****************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDR-RECORD
                //* *--------------------------------
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'LEDGER REPORTING EXTRACT' 120T 'REPORT: RPT1' //
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Move_To_Ledger_Extract() throws Exception                                                                                                            //Natural: MOVE-TO-LEDGER-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        ldaFcpl121.getPnd_Ledger_Extract().reset();                                                                                                                       //Natural: RESET #LEDGER-EXTRACT
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr().setValue(pnd_Lgr_Ext_Cntrct_Ppcn_Nbr);                                                                         //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-PPCN-NBR := #LGR-EXT.CNTRCT-PPCN-NBR
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Payee_Cde().setValue(pnd_Lgr_Ext_Cntrct_Payee_Cde);                                                                       //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-PAYEE-CDE := #LGR-EXT.CNTRCT-PAYEE-CDE
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde().setValue(pnd_Lgr_Ext_Cntrct_Orgn_Cde);                                                                         //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-ORGN-CDE := #LGR-EXT.CNTRCT-ORGN-CDE
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Unq_Id_Nbr().setValue(pnd_Lgr_Ext_Cntrct_Unq_Id_Nbr);                                                                     //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-UNQ-ID-NBR := #LGR-EXT.CNTRCT-UNQ-ID-NBR
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cref_Nbr().setValue(pnd_Lgr_Ext_Cntrct_Cref_Nbr);                                                                         //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-CREF-NBR := #LGR-EXT.CNTRCT-CREF-NBR
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Lgr_Ext_Pymnt_Check_Dte);                                    //Natural: MOVE EDITED #LGR-EXT.PYMNT-CHECK-DTE TO #LEDGER-EXTRACT.PYMNT-CHECK-DTE ( EM = YYYYMMDD )
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Prcss_Seq_Nbr().setValue(pnd_Lgr_Ext_Pymnt_Prcss_Seq_Nbr);                                                                 //Natural: ASSIGN #LEDGER-EXTRACT.PYMNT-PRCSS-SEQ-NBR := #LGR-EXT.PYMNT-PRCSS-SEQ-NBR
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Lgr_Ext_Pymnt_Intrfce_Dte);                                //Natural: MOVE EDITED #LGR-EXT.PYMNT-INTRFCE-DTE TO #LEDGER-EXTRACT.PYMNT-INTRFCE-DTE ( EM = YYYYMMDD )
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Acctg_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Lgr_Ext_Pymnt_Acctg_Dte);                                    //Natural: MOVE EDITED #LGR-EXT.PYMNT-ACCTG-DTE TO #LEDGER-EXTRACT.PYMNT-ACCTG-DTE ( EM = YYYYMMDD )
        ldaFcpl121.getPnd_Ledger_Extract_Ph_Last_Name().setValue(pnd_Lgr_Ext_Ph_Last_Name);                                                                               //Natural: ASSIGN #LEDGER-EXTRACT.PH-LAST-NAME := #LGR-EXT.PH-LAST-NAME
        ldaFcpl121.getPnd_Ledger_Extract_Ph_First_Name().setValue(pnd_Lgr_Ext_Ph_First_Name);                                                                             //Natural: ASSIGN #LEDGER-EXTRACT.PH-FIRST-NAME := #LGR-EXT.PH-FIRST-NAME
        ldaFcpl121.getPnd_Ledger_Extract_Ph_Middle_Name().setValue(pnd_Lgr_Ext_Ph_Middle_Name);                                                                           //Natural: ASSIGN #LEDGER-EXTRACT.PH-MIDDLE-NAME := #LGR-EXT.PH-MIDDLE-NAME
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Cycle_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Lgr_Ext_Pymnt_Cycle_Dte);                                    //Natural: MOVE EDITED #LGR-EXT.PYMNT-CYCLE-DTE TO #LEDGER-EXTRACT.PYMNT-CYCLE-DTE ( EM = YYYYMMDD )
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Corp_Wpid().setValue(pnd_Lgr_Ext_Pymnt_Corp_Wpid);                                                                         //Natural: ASSIGN #LEDGER-EXTRACT.PYMNT-CORP-WPID := #LGR-EXT.PYMNT-CORP-WPID
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Check_Crrncy_Cde().setValue(pnd_Lgr_Ext_Cntrct_Check_Crrncy_Cde);                                                         //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-CHECK-CRRNCY-CDE := #LGR-EXT.CNTRCT-CHECK-CRRNCY-CDE
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Lgr_Ext_Pymnt_Settlmnt_Dte);                              //Natural: MOVE EDITED #LGR-EXT.PYMNT-SETTLMNT-DTE TO #LEDGER-EXTRACT.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD )
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Annty_Ins_Type().setValue(pnd_Lgr_Ext_Cntrct_Annty_Ins_Type);                                                             //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-ANNTY-INS-TYPE := #LGR-EXT.CNTRCT-ANNTY-INS-TYPE
        ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde().setValue(pnd_Lgr_Ext_Inv_Acct_Cde);                                                                               //Natural: ASSIGN #LEDGER-EXTRACT.INV-ACCT-CDE := #LGR-EXT.INV-ACCT-CDE
        ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr().setValue(pnd_Lgr_Ext_Inv_Acct_Ledgr_Nbr);                                                                   //Natural: ASSIGN #LEDGER-EXTRACT.INV-ACCT-LEDGR-NBR := #LGR-EXT.INV-ACCT-LEDGR-NBR
        ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Amt().setValue(pnd_Lgr_Ext_Inv_Acct_Ledgr_Amt);                                                                   //Natural: ASSIGN #LEDGER-EXTRACT.INV-ACCT-LEDGR-AMT := #LGR-EXT.INV-ACCT-LEDGR-AMT
        ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Ind().setValue(pnd_Lgr_Ext_Inv_Acct_Ledgr_Ind);                                                                   //Natural: ASSIGN #LEDGER-EXTRACT.INV-ACCT-LEDGR-IND := #LGR-EXT.INV-ACCT-LEDGR-IND
                                                                                                                                                                          //Natural: PERFORM GET-ISA-CODE
        sub_Get_Isa_Code();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getWorkFiles().write(2, false, ldaFcpl121.getPnd_Ledger_Extract());                                                                                               //Natural: WRITE WORK FILE 02 #LEDGER-EXTRACT
        //* MOVE-TO-LEDGER-EXTRACT
    }
    private void sub_Get_Isa_Code() throws Exception                                                                                                                      //Natural: GET-ISA-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        pnd_Ws_Pnd_Isa_Sub.setValue(pnd_Lgr_Ext_Inv_Acct_Cde_Num);                                                                                                        //Natural: MOVE #LGR-EXT.INV-ACCT-CDE-NUM TO #ISA-SUB
        //*  PD
        getReports().write(0, "#ISA-SUB-->",pnd_Ws_Pnd_Isa_Sub);                                                                                                          //Natural: WRITE '#ISA-SUB-->' #ISA-SUB
        if (Global.isEscape()) return;
        ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Isa_Sub.getInt() + 1));                     //Natural: MOVE #ISA-LDA.#ISA-CDE ( #ISA-SUB ) TO #LEDGER-EXTRACT.INV-ACCT-ISA
        //* GET-ISA-CODE
    }
    private void sub_Move_To_Paymnt_Extract() throws Exception                                                                                                            //Natural: MOVE-TO-PAYMNT-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        ldaFcpl190a.getPnd_Rpt_Ext().reset();                                                                                                                             //Natural: RESET #RPT-EXT
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Ppcn_Nbr());                                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-PPCN-NBR := PYMNT.CNTRCT-PPCN-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Payee_Cde());                                                                  //Natural: ASSIGN #RPT-EXT.CNTRCT-PAYEE-CDE := PYMNT.CNTRCT-PAYEE-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde());                                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-ORGN-CDE := PYMNT.CNTRCT-ORGN-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Unq_Id_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Unq_Id_Nbr());                                                                //Natural: ASSIGN #RPT-EXT.CNTRCT-UNQ-ID-NBR := PYMNT.CNTRCT-UNQ-ID-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cref_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Cref_Nbr());                                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-CREF-NBR := PYMNT.CNTRCT-CREF-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().setValue(pdaFcpapmnt.getPymnt_Pymnt_Check_Dte());                                                                    //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-DTE := PYMNT.PYMNT-CHECK-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr().setValue(pdaFcpapmnt.getPymnt_Pymnt_Prcss_Seq_Nbr());                                                            //Natural: ASSIGN #RPT-EXT.PYMNT-PRCSS-SEQ-NBR := PYMNT.PYMNT-PRCSS-SEQ-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Intrfce_Dte());                                                      //Natural: ASSIGN #RPT-EXT.PYMNT-INTRFCE-DTE := FCP-CONS-LEDGER.PYMNT-INTRFCE-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Acctg_Dte());                                                          //Natural: ASSIGN #RPT-EXT.PYMNT-ACCTG-DTE := FCP-CONS-LEDGER.PYMNT-ACCTG-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name().setValue(pdaFcpaaddr.getAddr_Ph_Last_Name());                                                                           //Natural: ASSIGN #RPT-EXT.PH-LAST-NAME := ADDR.PH-LAST-NAME
        ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name().setValue(pdaFcpaaddr.getAddr_Ph_First_Name());                                                                         //Natural: ASSIGN #RPT-EXT.PH-FIRST-NAME := ADDR.PH-FIRST-NAME
        ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name().setValue(pdaFcpaaddr.getAddr_Ph_Middle_Name());                                                                       //Natural: ASSIGN #RPT-EXT.PH-MIDDLE-NAME := ADDR.PH-MIDDLE-NAME
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Invrse_Dte().setValue(pdaFcpapmnt.getPymnt_Cntrct_Invrse_Dte());                                                                //Natural: ASSIGN #RPT-EXT.CNTRCT-INVRSE-DTE := PYMNT.CNTRCT-INVRSE-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Crrncy_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Crrncy_Cde());                                                                //Natural: ASSIGN #RPT-EXT.CNTRCT-CRRNCY-CDE := PYMNT.CNTRCT-CRRNCY-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Qlfied_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Qlfied_Cde());                                                                //Natural: ASSIGN #RPT-EXT.CNTRCT-QLFIED-CDE := PYMNT.CNTRCT-QLFIED-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().setValue(pdaFcpapmnt.getPymnt_Cntrct_Pymnt_Type_Ind());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-PYMNT-TYPE-IND := PYMNT.CNTRCT-PYMNT-TYPE-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().setValue(pdaFcpapmnt.getPymnt_Pymnt_Ftre_Ind());                                                                      //Natural: ASSIGN #RPT-EXT.PYMNT-FTRE-IND := PYMNT.PYMNT-FTRE-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().setValue(pdaFcpapmnt.getPymnt_Pymnt_Pay_Type_Req_Ind());                                                      //Natural: ASSIGN #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND := PYMNT.PYMNT-PAY-TYPE-REQ-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Type_Cde());                                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-TYPE-CDE := PYMNT.CNTRCT-TYPE-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Lob_Cde());                                                                      //Natural: ASSIGN #RPT-EXT.CNTRCT-LOB-CDE := PYMNT.CNTRCT-LOB-CDE
        if (condition(pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde().equals("SS")))                                                                                               //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'SS'
        {
            short decideConditionsMet1612 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF PYMNT.CNTRCT-LOB-CDE;//Natural: VALUE 'RA'
            if (condition((pdaFcpapmnt.getPymnt_Cntrct_Lob_Cde().equals("RA"))))
            {
                decideConditionsMet1612++;
                ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().setValue("XRA ");                                                                                             //Natural: ASSIGN #RPT-EXT.CNTRCT-LOB-CDE := 'XRA '
            }                                                                                                                                                             //Natural: VALUE 'RAD'
            else if (condition((pdaFcpapmnt.getPymnt_Cntrct_Lob_Cde().equals("RAD"))))
            {
                decideConditionsMet1612++;
                ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().setValue("XRAD");                                                                                             //Natural: ASSIGN #RPT-EXT.CNTRCT-LOB-CDE := 'XRAD'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Option_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Option_Cde());                                                                //Natural: ASSIGN #RPT-EXT.CNTRCT-OPTION-CDE := PYMNT.CNTRCT-OPTION-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Mode_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Mode_Cde());                                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-MODE-CDE := PYMNT.CNTRCT-MODE-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Hold_Cde());                                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-HOLD-CDE := PYMNT.CNTRCT-HOLD-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Grp().setValue(pdaFcpapmnt.getPymnt_Cntrct_Hold_Grp());                                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-HOLD-GRP := PYMNT.CNTRCT-HOLD-GRP
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Tiaa_1_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-TIAA-1-NBR := PYMNT.CNTRCT-DA-TIAA-1-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_2_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Tiaa_2_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-TIAA-2-NBR := PYMNT.CNTRCT-DA-TIAA-2-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_3_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Tiaa_3_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-TIAA-3-NBR := PYMNT.CNTRCT-DA-TIAA-3-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_4_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Tiaa_4_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-TIAA-4-NBR := PYMNT.CNTRCT-DA-TIAA-4-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_5_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Tiaa_5_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-TIAA-5-NBR := PYMNT.CNTRCT-DA-TIAA-5-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_1_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Cref_1_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-CREF-1-NBR := PYMNT.CNTRCT-DA-CREF-1-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_2_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Cref_2_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-CREF-2-NBR := PYMNT.CNTRCT-DA-CREF-2-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_3_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Cref_3_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-CREF-3-NBR := PYMNT.CNTRCT-DA-CREF-3-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_4_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Cref_4_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-CREF-4-NBR := PYMNT.CNTRCT-DA-CREF-4-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_5_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Da_Cref_5_Nbr());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-CREF-5-NBR := PYMNT.CNTRCT-DA-CREF-5-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Annt_Soc_Sec_Nbr().setValue(pdaFcpapmnt.getPymnt_Annt_Soc_Sec_Nbr());                                                                  //Natural: ASSIGN #RPT-EXT.ANNT-SOC-SEC-NBR := PYMNT.ANNT-SOC-SEC-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Annt_Ctznshp_Cde().setValue(pdaFcpapmnt.getPymnt_Annt_Ctznshp_Cde());                                                                  //Natural: ASSIGN #RPT-EXT.ANNT-CTZNSHP-CDE := PYMNT.ANNT-CTZNSHP-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde().setValue(pdaFcpapmnt.getPymnt_Annt_Rsdncy_Cde());                                                                    //Natural: ASSIGN #RPT-EXT.ANNT-RSDNCY-CDE := PYMNT.ANNT-RSDNCY-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Dte().setValue(pdaFcpapmnt.getPymnt_Pymnt_Eft_Dte());                                                                        //Natural: ASSIGN #RPT-EXT.PYMNT-EFT-DTE := PYMNT.PYMNT-EFT-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte().setValue(pdaFcpapmnt.getPymnt_Pymnt_Settlmnt_Dte());                                                              //Natural: ASSIGN #RPT-EXT.PYMNT-SETTLMNT-DTE := PYMNT.PYMNT-SETTLMNT-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().setValue(pdaFcpapmnt.getPymnt_Pymnt_Check_Nbr());                                                                    //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-NBR := PYMNT.PYMNT-CHECK-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt().setValue(pdaFcpapmnt.getPymnt_Pymnt_Check_Amt());                                                                    //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-AMT := PYMNT.PYMNT-CHECK-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Seq_Nbr().setValue(pdaFcpapmnt.getPymnt_Pymnt_Check_Seq_Nbr());                                                            //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-SEQ-NBR := PYMNT.PYMNT-CHECK-SEQ-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cmbn_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Cmbn_Nbr());                                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-CMBN-NBR := PYMNT.CNTRCT-CMBN-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().setValue(pdaFcpapmnt.getPymnt_Cntrct_Annty_Ins_Type());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-ANNTY-INS-TYPE := PYMNT.CNTRCT-ANNTY-INS-TYPE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Type_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Annty_Type_Cde());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-ANNTY-TYPE-CDE := PYMNT.CNTRCT-ANNTY-TYPE-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Insurance_Option().setValue(pdaFcpapmnt.getPymnt_Cntrct_Insurance_Option());                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-INSURANCE-OPTION := PYMNT.CNTRCT-INSURANCE-OPTION
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Life_Contingency().setValue(pdaFcpapmnt.getPymnt_Cntrct_Life_Contingency());                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-LIFE-CONTINGENCY := PYMNT.CNTRCT-LIFE-CONTINGENCY
        ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions().setValue(pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp());                                                                //Natural: ASSIGN #RPT-EXT.#CNTR-DEDUCTIONS := PYMNT.C-PYMNT-DED-GRP
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions().notEquals(getZero())))                                                                             //Natural: IF #RPT-EXT.#CNTR-DEDUCTIONS NE 0
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Cde().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions()).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Cde().getValue(1, //Natural: ASSIGN #RPT-EXT.PYMNT-DED-CDE ( 1:#CNTR-DEDUCTIONS ) := PYMNT.PYMNT-DED-CDE ( 1:#CNTR-DEDUCTIONS )
                ":",ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions()));
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Payee_Cde().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions()).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Payee_Cde().getValue(1, //Natural: ASSIGN #RPT-EXT.PYMNT-DED-PAYEE-CDE ( 1:#CNTR-DEDUCTIONS ) := PYMNT.PYMNT-DED-PAYEE-CDE ( 1:#CNTR-DEDUCTIONS )
                ":",ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions()));
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions()).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Amt().getValue(1, //Natural: ASSIGN #RPT-EXT.PYMNT-DED-AMT ( 1:#CNTR-DEDUCTIONS ) := PYMNT.PYMNT-DED-AMT ( 1:#CNTR-DEDUCTIONS )
                ":",ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions()));
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().setValue(pdaFcpapmnt.getPymnt_Cntrct_Sttlmnt_Type_Ind());                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND := PYMNT.CNTRCT-STTLMNT-TYPE-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Payee_Tx_Elct_Trggr().setValue(pdaFcpapmnt.getPymnt_Pymnt_Payee_Tx_Elct_Trggr());                                                //Natural: ASSIGN #RPT-EXT.PYMNT-PAYEE-TX-ELCT-TRGGR := PYMNT.PYMNT-PAYEE-TX-ELCT-TRGGR
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr().setValue(pdaFcpapmnt.getPymnt_Pymnt_Check_Scrty_Nbr());                                                        //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-SCRTY-NBR := PYMNT.PYMNT-CHECK-SCRTY-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Ind().setValue(pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Ind());                                                      //Natural: ASSIGN #RPT-EXT.CNTRCT-CANCEL-RDRW-IND := PYMNT.CNTRCT-CANCEL-RDRW-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Method_Cde().setValue(pdaFcpapmnt.getPymnt_Pymnt_Method_Cde());                                                                  //Natural: ASSIGN #RPT-EXT.PYMNT-METHOD-CDE := PYMNT.PYMNT-METHOD-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Annt_Name().setValue(DbsUtil.compress(ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name(),         //Natural: COMPRESS #RPT-EXT.PH-FIRST-NAME #RPT-EXT.PH-MIDDLE-NAME #RPT-EXT.PH-LAST-NAME INTO #RPT-EXT.ANNT-NAME
            ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name()));
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Nme().getValue("*"));                                                     //Natural: ASSIGN #RPT-EXT.PYMNT-NME ( * ) := ADDR.PYMNT-NME ( * )
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Addr_Line1_Txt().getValue("*"));                               //Natural: ASSIGN #RPT-EXT.PYMNT-ADDR-LINE1-TXT ( * ) := ADDR.PYMNT-ADDR-LINE1-TXT ( * )
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Transit_Id().setValue(pdaFcpaaddr.getAddr_Pymnt_Eft_Transit_Id());                                                           //Natural: ASSIGN #RPT-EXT.PYMNT-EFT-TRANSIT-ID := ADDR.PYMNT-EFT-TRANSIT-ID
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr().setValue(pdaFcpaaddr.getAddr_Pymnt_Eft_Acct_Nbr());                                                               //Natural: ASSIGN #RPT-EXT.PYMNT-EFT-ACCT-NBR := ADDR.PYMNT-EFT-ACCT-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Chk_Sav_Ind().setValue(pdaFcpaaddr.getAddr_Pymnt_Chk_Sav_Ind());                                                                 //Natural: ASSIGN #RPT-EXT.PYMNT-CHK-SAV-IND := ADDR.PYMNT-CHK-SAV-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Cancel_Rdrw_Actvty_Cde());                              //Natural: ASSIGN #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE := FCP-CONS-LEDGER.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        pnd_Ws_Pnd_I.setValue(pdaFcpapmnt.getPymnt_C_Inv_Acct());                                                                                                         //Natural: ASSIGN #I := #RPT-EXT.C-INV-ACCT := PYMNT.C-INV-ACCT
        ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct().setValue(pdaFcpapmnt.getPymnt_C_Inv_Acct());
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Cde().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Cde().getValue(1,":",pnd_Ws_Pnd_I));                //Natural: ASSIGN #RPT-EXT.INV-ACCT-CDE ( 1:#I ) := PYMNT.INV-ACCT-CDE ( 1:#I )
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Qty().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Unit_Qty().getValue(1,":",pnd_Ws_Pnd_I));      //Natural: ASSIGN #RPT-EXT.INV-ACCT-UNIT-QTY ( 1:#I ) := PYMNT.INV-ACCT-UNIT-QTY ( 1:#I )
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Value().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Unit_Value().getValue(1,":",                 //Natural: ASSIGN #RPT-EXT.INV-ACCT-UNIT-VALUE ( 1:#I ) := PYMNT.INV-ACCT-UNIT-VALUE ( 1:#I )
            pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Settl_Amt().getValue(1,":",                   //Natural: ASSIGN #RPT-EXT.INV-ACCT-SETTL-AMT ( 1:#I ) := PYMNT.INV-ACCT-SETTL-AMT ( 1:#I )
            pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fed_Cde().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Fed_Cde().getValue(1,":",pnd_Ws_Pnd_I));        //Natural: ASSIGN #RPT-EXT.INV-ACCT-FED-CDE ( 1:#I ) := PYMNT.INV-ACCT-FED-CDE ( 1:#I )
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Cde().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_State_Cde().getValue(1,":",                   //Natural: ASSIGN #RPT-EXT.INV-ACCT-STATE-CDE ( 1:#I ) := PYMNT.INV-ACCT-STATE-CDE ( 1:#I )
            pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Cde().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Local_Cde().getValue(1,":",                   //Natural: ASSIGN #RPT-EXT.INV-ACCT-LOCAL-CDE ( 1:#I ) := PYMNT.INV-ACCT-LOCAL-CDE ( 1:#I )
            pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Ivc_Amt().getValue(1,":",pnd_Ws_Pnd_I));        //Natural: ASSIGN #RPT-EXT.INV-ACCT-IVC-AMT ( 1:#I ) := PYMNT.INV-ACCT-IVC-AMT ( 1:#I )
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dci_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dci_Amt().getValue(1,":",pnd_Ws_Pnd_I));        //Natural: ASSIGN #RPT-EXT.INV-ACCT-DCI-AMT ( 1:#I ) := PYMNT.INV-ACCT-DCI-AMT ( 1:#I )
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Pnd_I));        //Natural: ASSIGN #RPT-EXT.INV-ACCT-DPI-AMT ( 1:#I ) := PYMNT.INV-ACCT-DPI-AMT ( 1:#I )
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Exp_Amt().getValue(1,":",pnd_Ws_Pnd_I));        //Natural: ASSIGN #RPT-EXT.INV-ACCT-EXP-AMT ( 1:#I ) := PYMNT.INV-ACCT-EXP-AMT ( 1:#I )
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dvdnd_Amt().getValue(1,":",                   //Natural: ASSIGN #RPT-EXT.INV-ACCT-DVDND-AMT ( 1:#I ) := PYMNT.INV-ACCT-DVDND-AMT ( 1:#I )
            pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(1,                 //Natural: ASSIGN #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-FDRL-TAX-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Valuat_Period().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Valuat_Period().getValue(1,               //Natural: ASSIGN #RPT-EXT.INV-ACCT-VALUAT-PERIOD ( 1:#I ) := PYMNT.INV-ACCT-VALUAT-PERIOD ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_State_Tax_Amt().getValue(1,               //Natural: ASSIGN #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-STATE-TAX-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Local_Tax_Amt().getValue(1,               //Natural: ASSIGN #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-LOCAL-TAX-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Net_Pymnt_Amt().getValue(1,               //Natural: ASSIGN #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( 1:#I ) := PYMNT.INV-ACCT-NET-PYMNT-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        //* * 04-21-99 : REVISE DEDUCTION PROCESSING FOR MULTI-FUNDED 'DC'.
        if (condition(pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde().equals("DC")))                                                                                               //Natural: IF PYMNT.CNTRCT-ORGN-CDE EQ 'DC'
        {
            FOR01:                                                                                                                                                        //Natural: FOR #X 1 #I
            for (pnd_Ws_Pnd_X.setValue(1); condition(pnd_Ws_Pnd_X.lessOrEqual(pnd_Ws_Pnd_I)); pnd_Ws_Pnd_X.nadd(1))
            {
                ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt().getValue(pnd_Ws_Pnd_X).compute(new ComputeParameters(false, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt().getValue(pnd_Ws_Pnd_X)),  //Natural: ASSIGN #RPT-EXT.##INV-DED-AMT ( #X ) := PYMNT.INV-ACCT-SETTL-AMT ( #X ) + PYMNT.INV-ACCT-DVDND-AMT ( #X ) + PYMNT.INV-ACCT-DCI-AMT ( #X ) + PYMNT.INV-ACCT-DPI-AMT ( #X ) - PYMNT.INV-ACCT-EXP-AMT ( #X ) - PYMNT.INV-ACCT-FDRL-TAX-AMT ( #X ) - PYMNT.INV-ACCT-STATE-TAX-AMT ( #X ) - PYMNT.INV-ACCT-LOCAL-TAX-AMT ( #X ) - PYMNT.INV-ACCT-NET-PYMNT-AMT ( #X )
                    pdaFcpapmnt.getPymnt_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_X).add(pdaFcpapmnt.getPymnt_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_X)).add(pdaFcpapmnt.getPymnt_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Pnd_X)).add(pdaFcpapmnt.getPymnt_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_X)).subtract(pdaFcpapmnt.getPymnt_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Pnd_X)).subtract(pdaFcpapmnt.getPymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(pdaFcpapmnt.getPymnt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(pdaFcpapmnt.getPymnt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(pdaFcpapmnt.getPymnt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_X)));
                ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Cde().getValue(pnd_Ws_Pnd_X).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Cde().getValue(1));                       //Natural: ASSIGN #RPT-EXT.##INV-DED-CDE ( #X ) := PYMNT.PYMNT-DED-CDE ( 1 )
                ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Payee_Cde().getValue(pnd_Ws_Pnd_X).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Payee_Cde().getValue(1));           //Natural: ASSIGN #RPT-EXT.##INV-DED-PAYEE-CDE ( #X ) := PYMNT.PYMNT-DED-PAYEE-CDE ( 1 )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Paymnt_Written.nadd(1);                                                                                                                                //Natural: ADD 1 TO #PAYMNT-WRITTEN
        //* MOVE-TO-PAYMNT-EXTRACT
    }
    private void sub_Move_To_Nz_Paymnt_Extract() throws Exception                                                                                                         //Natural: MOVE-TO-NZ-PAYMNT-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        //* SET NZ-PYMNT-ADDR
        pdaFcpaext.getExt_Extr().reset();                                                                                                                                 //Natural: RESET EXTR
        //*  FROM FCPAPMNT TO FCPAEXT
        pdaFcpaext.getExt_Extr().setValuesByName(pdaFcpapmnt.getPymnt());                                                                                                 //Natural: MOVE BY NAME PYMNT TO EXT.EXTR
        //*  RTB WILL NOT BE PASSED - DERIVE WHEN NEEDED BASIS ONLY  /* ROXAN 12/7
        //*  IF      PYMNT.CNTRCT-PYMNT-TYPE-IND     = 'N'         /*
        //*      AND PYMNT.CNTRCT-STTLMNT-TYPE-IND   = 'X'         /*
        //*    NZ-EXT.RTB                           := TRUE        /*
        //*  ELSE                                                  /*
        //*    IF      PYMNT.CNTRCT-PYMNT-TYPE-IND   = 'I'         /*
        //*        AND PYMNT.CNTRCT-STTLMNT-TYPE-IND = 'R'         /*
        //*      NZ-EXT.IVC-FROM-RTB-ROLLOVER       := TRUE        /*
        //*    END-IF                                              /*
        //*  END-IF                                                /*
        //*  NZ-EXT.CNTRCT-DA-NBR(1)             := PYMNT.CNTRCT-DA-TIAA-1-NBR /*
        //*  NZ-EXT.CNTRCT-DA-NBR(2)             := PYMNT.CNTRCT-DA-TIAA-2-NBR /*
        //*  NZ-EXT.CNTRCT-DA-NBR(3)             := PYMNT.CNTRCT-DA-TIAA-3-NBR /*
        //*  NZ-EXT.CNTRCT-DA-NBR(4)             := PYMNT.CNTRCT-DA-TIAA-4-NBR /*
        //*  NZ-EXT.CNTRCT-DA-NBR(5)             := PYMNT.CNTRCT-DA-TIAA-5-NBR /*
        //*  NZ-EXT.CNTRCT-DA-NBR(6)             := PYMNT.CNTRCT-DA-CREF-1-NBR /*
        if (condition(pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp().notEquals(getZero())))                                                                                       //Natural: IF PYMNT.C-PYMNT-DED-GRP NE 0
        {
            pnd_Ws_Pnd_I.setValue(pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp());                                                                                                //Natural: ASSIGN #I := EXT.C-PYMNT-DED-GRP := PYMNT.C-PYMNT-DED-GRP
            pdaFcpaext.getExt_C_Pymnt_Ded_Grp().setValue(pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp());
            pdaFcpaext.getExt_Pymnt_Ded_Cde().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Cde().getValue(1,":",pnd_Ws_Pnd_I));                   //Natural: ASSIGN EXT.PYMNT-DED-CDE ( 1:#I ) := PYMNT.PYMNT-DED-CDE ( 1:#I )
            pdaFcpaext.getExt_Pymnt_Ded_Payee_Cde().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Payee_Cde().getValue(1,":",pnd_Ws_Pnd_I));       //Natural: ASSIGN EXT.PYMNT-DED-PAYEE-CDE ( 1:#I ) := PYMNT.PYMNT-DED-PAYEE-CDE ( 1:#I )
            pdaFcpaext.getExt_Pymnt_Ded_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Amt().getValue(1,":",pnd_Ws_Pnd_I));                   //Natural: ASSIGN EXT.PYMNT-DED-AMT ( 1:#I ) := PYMNT.PYMNT-DED-AMT ( 1:#I )
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaext.getExt_Ph_Last_Name().setValue(pdaFcpaaddr.getAddr_Ph_Last_Name());                                                                                    //Natural: ASSIGN EXT.PH-LAST-NAME := ADDR.PH-LAST-NAME
        pdaFcpaext.getExt_Ph_First_Name().setValue(pdaFcpaaddr.getAddr_Ph_First_Name());                                                                                  //Natural: ASSIGN EXT.PH-FIRST-NAME := ADDR.PH-FIRST-NAME
        pdaFcpaext.getExt_Ph_Middle_Name().setValue(pdaFcpaaddr.getAddr_Ph_Middle_Name());                                                                                //Natural: ASSIGN EXT.PH-MIDDLE-NAME := ADDR.PH-MIDDLE-NAME
        pdaFcpaext.getExt_Pymnt_Nme().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Nme().getValue("*"));                                                              //Natural: ASSIGN EXT.PYMNT-NME ( * ) := ADDR.PYMNT-NME ( * )
        pdaFcpaext.getExt_Pymnt_Addr_Line1_Txt().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Addr_Line1_Txt().getValue("*"));                                        //Natural: ASSIGN EXT.PYMNT-ADDR-LINE1-TXT ( * ) := ADDR.PYMNT-ADDR-LINE1-TXT ( * )
        pdaFcpaext.getExt_Pymnt_Addr_Line2_Txt().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Addr_Line2_Txt().getValue("*"));                                        //Natural: ASSIGN EXT.PYMNT-ADDR-LINE2-TXT ( * ) := ADDR.PYMNT-ADDR-LINE2-TXT ( * )
        pdaFcpaext.getExt_Pymnt_Addr_Line3_Txt().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Addr_Line3_Txt().getValue("*"));                                        //Natural: ASSIGN EXT.PYMNT-ADDR-LINE3-TXT ( * ) := ADDR.PYMNT-ADDR-LINE3-TXT ( * )
        pdaFcpaext.getExt_Pymnt_Addr_Line4_Txt().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Addr_Line4_Txt().getValue("*"));                                        //Natural: ASSIGN EXT.PYMNT-ADDR-LINE4-TXT ( * ) := ADDR.PYMNT-ADDR-LINE4-TXT ( * )
        pdaFcpaext.getExt_Pymnt_Addr_Line5_Txt().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Addr_Line5_Txt().getValue("*"));                                        //Natural: ASSIGN EXT.PYMNT-ADDR-LINE5-TXT ( * ) := ADDR.PYMNT-ADDR-LINE5-TXT ( * )
        pdaFcpaext.getExt_Pymnt_Addr_Line6_Txt().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Addr_Line6_Txt().getValue("*"));                                        //Natural: ASSIGN EXT.PYMNT-ADDR-LINE6-TXT ( * ) := ADDR.PYMNT-ADDR-LINE6-TXT ( * )
        pdaFcpaext.getExt_Pymnt_Addr_Zip_Cde().getValue("*").setValue(pdaFcpaaddr.getAddr_Pymnt_Addr_Zip_Cde().getValue("*"));                                            //Natural: ASSIGN EXT.PYMNT-ADDR-ZIP-CDE ( * ) := ADDR.PYMNT-ADDR-ZIP-CDE ( * )
        pdaFcpaext.getExt_Pymnt_Eft_Transit_Id().setValue(pdaFcpaaddr.getAddr_Pymnt_Eft_Transit_Id());                                                                    //Natural: ASSIGN EXT.PYMNT-EFT-TRANSIT-ID := ADDR.PYMNT-EFT-TRANSIT-ID
        pdaFcpaext.getExt_Pymnt_Eft_Acct_Nbr().setValue(pdaFcpaaddr.getAddr_Pymnt_Eft_Acct_Nbr());                                                                        //Natural: ASSIGN EXT.PYMNT-EFT-ACCT-NBR := ADDR.PYMNT-EFT-ACCT-NBR
        pdaFcpaext.getExt_Pymnt_Chk_Sav_Ind().setValue(pdaFcpaaddr.getAddr_Pymnt_Chk_Sav_Ind());                                                                          //Natural: ASSIGN EXT.PYMNT-CHK-SAV-IND := ADDR.PYMNT-CHK-SAV-IND
        pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Cancel_Rdrw_Actvty_Cde());                                       //Natural: ASSIGN EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE := FCP-CONS-LEDGER.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        pnd_Ws_Pnd_I.setValue(pdaFcpapmnt.getPymnt_C_Inv_Acct());                                                                                                         //Natural: ASSIGN #I := #C-INV-ACCT := EXT.C-INV-ACCT := PYMNT.C-INV-ACCT
        pnd_Ws_Pnd_C_Inv_Acct.setValue(pdaFcpapmnt.getPymnt_C_Inv_Acct());
        pdaFcpaext.getExt_C_Inv_Acct().setValue(pdaFcpapmnt.getPymnt_C_Inv_Acct());
        pdaFcpaext.getExt_Inv_Acct_Unit_Qty().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Unit_Qty().getValue(1,":",pnd_Ws_Pnd_I));               //Natural: ASSIGN EXT.INV-ACCT-UNIT-QTY ( 1:#I ) := PYMNT.INV-ACCT-UNIT-QTY ( 1:#I )
        pdaFcpaext.getExt_Inv_Acct_Unit_Value().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Unit_Value().getValue(1,":",pnd_Ws_Pnd_I));           //Natural: ASSIGN EXT.INV-ACCT-UNIT-VALUE ( 1:#I ) := PYMNT.INV-ACCT-UNIT-VALUE ( 1:#I )
        pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I));             //Natural: ASSIGN EXT.INV-ACCT-SETTL-AMT ( 1:#I ) := PYMNT.INV-ACCT-SETTL-AMT ( 1:#I )
        pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Ivc_Amt().getValue(1,":",pnd_Ws_Pnd_I));                 //Natural: ASSIGN EXT.INV-ACCT-IVC-AMT ( 1:#I ) := PYMNT.INV-ACCT-IVC-AMT ( 1:#I )
        pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dci_Amt().getValue(1,":",pnd_Ws_Pnd_I));                 //Natural: ASSIGN EXT.INV-ACCT-DCI-AMT ( 1:#I ) := PYMNT.INV-ACCT-DCI-AMT ( 1:#I )
        pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Pnd_I));                 //Natural: ASSIGN EXT.INV-ACCT-DPI-AMT ( 1:#I ) := PYMNT.INV-ACCT-DPI-AMT ( 1:#I )
        pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Pnd_I));             //Natural: ASSIGN EXT.INV-ACCT-DVDND-AMT ( 1:#I ) := PYMNT.INV-ACCT-DVDND-AMT ( 1:#I )
        pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I));       //Natural: ASSIGN EXT.INV-ACCT-FDRL-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-FDRL-TAX-AMT ( 1:#I )
        pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_State_Tax_Amt().getValue(1,":",                    //Natural: ASSIGN EXT.INV-ACCT-STATE-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-STATE-TAX-AMT ( 1:#I )
            pnd_Ws_Pnd_I));
        pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Local_Tax_Amt().getValue(1,":",                    //Natural: ASSIGN EXT.INV-ACCT-LOCAL-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-LOCAL-TAX-AMT ( 1:#I )
            pnd_Ws_Pnd_I));
        pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",                    //Natural: ASSIGN EXT.INV-ACCT-NET-PYMNT-AMT ( 1:#I ) := PYMNT.INV-ACCT-NET-PYMNT-AMT ( 1:#I )
            pnd_Ws_Pnd_I));
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO PYMNT.C-INV-ACCT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpapmnt.getPymnt_C_Inv_Acct())); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_I));                                        //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := EXT.INV-ACCT-CDE ( #I ) := PYMNT.INV-ACCT-CDE ( #I )
            pdaFcpaext.getExt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_I));
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I));                      //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := EXT.INV-ACCT-VALUAT-PERIOD ( #I ) := PYMNT.INV-ACCT-VALUAT-PERIOD ( #I )
            pdaFcpaext.getExt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I));
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            pdaFcpaext.getExt_Inv_Acct_Company().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde());                                          //Natural: ASSIGN EXT.INV-ACCT-COMPANY ( #I ) := #FUND-PDA.#COMPANY-CDE
            pdaFcpaext.getExt_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Num());                                           //Natural: ASSIGN EXT.INV-ACCT-CDE-N ( #I ) := #FUND-PDA.#INV-ACCT-NUM
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Orgn_Cde().equals("NZ")))                                                                                     //Natural: IF FCP-CONS-LEDGER.CNTRCT-ORGN-CDE = 'NZ'
        {
            pnd_Ws_Pnd_Nz_Paymnt_Written.nadd(1);                                                                                                                         //Natural: ADD 1 TO #NZ-PAYMNT-WRITTEN
            getWorkFiles().write(3, true, pdaFcpaext.getExt().getValue("*"));                                                                                             //Natural: WRITE WORK FILE 03 VARIABLE EXT ( * )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Orgn_Cde().equals("AL")))                                                                                     //Natural: IF FCP-CONS-LEDGER.CNTRCT-ORGN-CDE = 'AL'
        {
            pnd_Ws_Pnd_Al_Paymnt_Written.nadd(1);                                                                                                                         //Natural: ADD 1 TO #AL-PAYMNT-WRITTEN
            getWorkFiles().write(5, true, pdaFcpaext.getExt().getValue("*"));                                                                                             //Natural: WRITE WORK FILE 05 VARIABLE EXT ( * )
        }                                                                                                                                                                 //Natural: END-IF
        //* MOVE-TO-NZ-PAYMNT-EXTRACT
    }
    private void sub_Move_To_Ap_Paymnt_Extract() throws Exception                                                                                                         //Natural: MOVE-TO-AP-PAYMNT-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().reset();                                                                                                     //Natural: RESET WF-PYMNT-ADDR-REC
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info().setValuesByName(pdaFcpaaddr.getAddr());                                                                        //Natural: MOVE BY NAME ADDR TO WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO
        //*  FROM FCPAPMNT TO FCPA800
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info().setValuesByName(pdaFcpapmnt.getPymnt());                                                                       //Natural: MOVE BY NAME PYMNT TO WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO
        if (condition(pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp().notEquals(getZero())))                                                                                       //Natural: IF PYMNT.C-PYMNT-DED-GRP NE 0
        {
            pnd_Ws_Pnd_I.setValue(pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp());                                                                                                //Natural: ASSIGN #I := WF-PYMNT-ADDR-GRP.C-PYMNT-DED-GRP := PYMNT.C-PYMNT-DED-GRP
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp().setValue(pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp());
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Cde().getValue(1,":",                   //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( 1:#I ) := PYMNT.PYMNT-DED-CDE ( 1:#I )
                pnd_Ws_Pnd_I));
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Payee_Cde().getValue(1,           //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-DED-PAYEE-CDE ( 1:#I ) := PYMNT.PYMNT-DED-PAYEE-CDE ( 1:#I )
                ":",pnd_Ws_Pnd_I));
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Pymnt_Ded_Amt().getValue(1,":",                   //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( 1:#I ) := PYMNT.PYMNT-DED-AMT ( 1:#I )
                pnd_Ws_Pnd_I));
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_I.setValue(pdaFcpapmnt.getPymnt_C_Inv_Acct());                                                                                                         //Natural: ASSIGN #I := INV-ACCT-COUNT := PYMNT.C-INV-ACCT
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count().setValue(pdaFcpapmnt.getPymnt_C_Inv_Acct());
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Unit_Qty().getValue(1,                   //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-UNIT-QTY ( 1:#I ) := PYMNT.INV-ACCT-UNIT-QTY ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Unit_Value().getValue(1,               //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-UNIT-VALUE ( 1:#I ) := PYMNT.INV-ACCT-UNIT-VALUE ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Settl_Amt().getValue(1,                //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#I ) := PYMNT.INV-ACCT-SETTL-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I)),  //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#I ) := PYMNT.INV-ACCT-SETTL-AMT ( 1:#I ) + PYMNT.INV-ACCT-DVDND-AMT ( 1:#I )
            pdaFcpapmnt.getPymnt_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I).add(pdaFcpapmnt.getPymnt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Pnd_I)));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Ivc_Amt().getValue(1,":",                 //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-IVC-AMT ( 1:#I ) := PYMNT.INV-ACCT-IVC-AMT ( 1:#I )
            pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dci_Amt().getValue(1,":",                 //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-DCI-AMT ( 1:#I ) := PYMNT.INV-ACCT-DCI-AMT ( 1:#I )
            pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dpi_Amt().getValue(1,":",                 //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-DPI-AMT ( 1:#I ) := PYMNT.INV-ACCT-DPI-AMT ( 1:#I )
            pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(1,           //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-FDRL-TAX-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_State_Tax_Amt().getValue(1,         //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-STATE-TAX-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Local_Tax_Amt().getValue(1,         //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:#I ) := PYMNT.INV-ACCT-LOCAL-TAX-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Dvdnd_Amt().getValue(1,                 //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#I ) := PYMNT.INV-ACCT-DVDND-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Net_Pymnt_Amt().getValue(1,         //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#I ) := PYMNT.INV-ACCT-NET-PYMNT-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I)),  //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#I ) := PYMNT.INV-ACCT-SETTL-AMT ( 1:#I ) + PYMNT.INV-ACCT-DVDND-AMT ( 1:#I )
            pdaFcpapmnt.getPymnt_Inv_Acct_Settl_Amt().getValue(1,":",pnd_Ws_Pnd_I).add(pdaFcpapmnt.getPymnt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Ws_Pnd_I)));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1,":",pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Settl_Amt().getValue(1,                //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#I ) := PYMNT.INV-ACCT-SETTL-AMT ( 1:#I )
            ":",pnd_Ws_Pnd_I));
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO PYMNT.C-INV-ACCT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpapmnt.getPymnt_C_Inv_Acct())); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-VALUAT-PERIOD ( #I ) := #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := PYMNT.INV-ACCT-VALUAT-PERIOD ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I));
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_I));            //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #I ) := #FUND-PDA.#INV-ACCT-INPUT := PYMNT.INV-ACCT-CDE ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpapmnt.getPymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_I));
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Num());                            //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-N ( #I ) := #FUND-PDA.#INV-ACCT-NUM
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde());                           //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( #I ) := #FUND-PDA.#COMPANY-CDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Ap_Paymnt_Written.nadd(1);                                                                                                                             //Natural: ADD 1 TO #AP-PAYMNT-WRITTEN
        getWorkFiles().write(4, true, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); //Natural: WRITE WORK FILE 04 VARIABLE WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO WF-PYMNT-ADDR-GRP.INV-INFO ( 1:INV-ACCT-COUNT )
        //* MOVE-TO-AP-PAYMNT-EXTRACT
    }
    private void sub_Get_Pymnt_Record() throws Exception                                                                                                                  //Natural: GET-PYMNT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpapmnt.getPymnt_Cntrct_Ppcn_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Ppcn_Nbr());                                                                //Natural: ASSIGN PYMNT.CNTRCT-PPCN-NBR := FCP-CONS-LEDGER.CNTRCT-PPCN-NBR
        pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Orgn_Cde());                                                                //Natural: ASSIGN PYMNT.CNTRCT-ORGN-CDE := FCP-CONS-LEDGER.CNTRCT-ORGN-CDE
        pdaFcpapmnt.getPymnt_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr());                                                        //Natural: ASSIGN PYMNT.PYMNT-PRCSS-SEQ-NBR := FCP-CONS-LEDGER.PYMNT-PRCSS-SEQ-NBR
        pnd_Ws_Pnd_Temp_Date_Alpha.setValueEdited(ldaFcplldgr.getFcp_Cons_Ledger_Cntl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED FCP-CONS-LEDGER.CNTL-CHECK-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-ALPHA
        pdaFcpapmnt.getPymnt_Cntrct_Invrse_Dte().compute(new ComputeParameters(false, pdaFcpapmnt.getPymnt_Cntrct_Invrse_Dte()), DbsField.subtract(100000000,             //Natural: SUBTRACT #TEMP-DATE FROM 100000000 GIVING PYMNT.CNTRCT-INVRSE-DTE
            pnd_Ws_Pnd_Temp_Date));
        DbsUtil.callnat(Fcpnpmnt.class , getCurrentProcessState(), pdaFcpapmnt.getPymnt_Work_Fields(), pdaFcpapmnt.getPymnt());                                           //Natural: CALLNAT 'FCPNPMNT' PYMNT-WORK-FIELDS PYMNT
        if (condition(Global.isEscape())) return;
        //* GET-PYMNT-RECORD
    }
    private void sub_Get_Addr_Record() throws Exception                                                                                                                   //Natural: GET-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpaaddr.getAddr_Cntrct_Ppcn_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Ppcn_Nbr());                                                                           //Natural: ASSIGN ADDR.CNTRCT-PPCN-NBR := PYMNT.CNTRCT-PPCN-NBR
        pdaFcpaaddr.getAddr_Cntrct_Payee_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Payee_Cde());                                                                         //Natural: ASSIGN ADDR.CNTRCT-PAYEE-CDE := PYMNT.CNTRCT-PAYEE-CDE
        pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde());                                                                           //Natural: ASSIGN ADDR.CNTRCT-ORGN-CDE := PYMNT.CNTRCT-ORGN-CDE
        pdaFcpaaddr.getAddr_Cntrct_Invrse_Dte().setValue(pdaFcpapmnt.getPymnt_Cntrct_Invrse_Dte());                                                                       //Natural: ASSIGN ADDR.CNTRCT-INVRSE-DTE := PYMNT.CNTRCT-INVRSE-DTE
        pdaFcpaaddr.getAddr_Pymnt_Prcss_Seq_Nbr().setValue(pdaFcpapmnt.getPymnt_Pymnt_Prcss_Seq_Num());                                                                   //Natural: ASSIGN ADDR.PYMNT-PRCSS-SEQ-NBR := PYMNT.PYMNT-PRCSS-SEQ-NUM
        pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr().setValue(pdaFcpapmnt.getPymnt_Cntrct_Cmbn_Nbr());                                                               //Natural: ASSIGN ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR := PYMNT.CNTRCT-CMBN-NBR
        DbsUtil.callnat(Fcpnaddr.class , getCurrentProcessState(), pdaFcpaaddr.getAddr_Work_Fields(), pdaFcpaaddr.getAddr());                                             //Natural: CALLNAT 'FCPNADDR' ADDR-WORK-FIELDS ADDR
        if (condition(Global.isEscape())) return;
        //* GET-ADDR-RECORD
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(55),"LEDGER REPORTING EXTRACT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
}
