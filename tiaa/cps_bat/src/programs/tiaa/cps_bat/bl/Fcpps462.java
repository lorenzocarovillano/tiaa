/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:49 PM
**        * FROM NATURAL PROGRAM : Fcpps462
************************************************************
**        * FILE NAME            : Fcpps462.java
**        * CLASS NAME           : Fcpps462
**        * INSTANCE NAME        : Fcpps462
************************************************************
***********************************************************************
* PROGRAM  : FCPPS462
*
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* FUNCTION : THIS PROGRAM CALLS FCPNS462 TO CREATE S462 EXTRACT FOR
*            IA MONTHLY
* DATE     : 01/15/2020
* CHANGES  : CTS CPS SUNSET (CHG694525)
**********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpps462 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Gtn_W9_Ben_Ind;
    private DbsField pnd_Gtn_Fed_Tax_Type;
    private DbsField pnd_Gtn_Fed_Tax_Amt;
    private DbsField pnd_Gtn_Fed_Canadian_Amt;
    private DbsField pnd_Gtn_Elec_Cde;
    private DbsField pnd_Gtn_Cntrct_Money_Source;
    private DbsField pnd_Gtn_Hardship_Cde;
    private DbsField pnd_Dist;
    private DbsField pnd_Pymnt_Deceased_Nme;
    private DbsField s462_Ret_Code;
    private DbsField s462_Ret_Msg;
    private DbsField pnd_Read_Count;

    private DbsGroup pnd_Read_Count__R_Field_1;
    private DbsField pnd_Read_Count_Pnd_Read_Count_Filler;
    private DbsField pnd_Read_Count_Pnd_Read_Count_Alpha_L3;
    private DbsField read_Rec;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);

        // Local Variables
        pnd_Gtn_W9_Ben_Ind = localVariables.newFieldInRecord("pnd_Gtn_W9_Ben_Ind", "#GTN-W9-BEN-IND", FieldType.STRING, 1);
        pnd_Gtn_Fed_Tax_Type = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Tax_Type", "#GTN-FED-TAX-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            2));
        pnd_Gtn_Fed_Tax_Amt = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Tax_Amt", "#GTN-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            2));
        pnd_Gtn_Fed_Canadian_Amt = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Canadian_Amt", "#GTN-FED-CANADIAN-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 2));
        pnd_Gtn_Elec_Cde = localVariables.newFieldInRecord("pnd_Gtn_Elec_Cde", "#GTN-ELEC-CDE", FieldType.NUMERIC, 3);
        pnd_Gtn_Cntrct_Money_Source = localVariables.newFieldInRecord("pnd_Gtn_Cntrct_Money_Source", "#GTN-CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        pnd_Gtn_Hardship_Cde = localVariables.newFieldInRecord("pnd_Gtn_Hardship_Cde", "#GTN-HARDSHIP-CDE", FieldType.STRING, 1);
        pnd_Dist = localVariables.newFieldInRecord("pnd_Dist", "#DIST", FieldType.STRING, 2);
        pnd_Pymnt_Deceased_Nme = localVariables.newFieldInRecord("pnd_Pymnt_Deceased_Nme", "#PYMNT-DECEASED-NME", FieldType.STRING, 38);
        s462_Ret_Code = localVariables.newFieldInRecord("s462_Ret_Code", "S462-RET-CODE", FieldType.STRING, 4);
        s462_Ret_Msg = localVariables.newFieldInRecord("s462_Ret_Msg", "S462-RET-MSG", FieldType.STRING, 60);
        pnd_Read_Count = localVariables.newFieldInRecord("pnd_Read_Count", "#READ-COUNT", FieldType.NUMERIC, 10);

        pnd_Read_Count__R_Field_1 = localVariables.newGroupInRecord("pnd_Read_Count__R_Field_1", "REDEFINE", pnd_Read_Count);
        pnd_Read_Count_Pnd_Read_Count_Filler = pnd_Read_Count__R_Field_1.newFieldInGroup("pnd_Read_Count_Pnd_Read_Count_Filler", "#READ-COUNT-FILLER", 
            FieldType.STRING, 7);
        pnd_Read_Count_Pnd_Read_Count_Alpha_L3 = pnd_Read_Count__R_Field_1.newFieldInGroup("pnd_Read_Count_Pnd_Read_Count_Alpha_L3", "#READ-COUNT-ALPHA-L3", 
            FieldType.STRING, 3);
        read_Rec = localVariables.newFieldInRecord("read_Rec", "READ-REC", FieldType.STRING, 4746);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpps462() throws Exception
    {
        super("Fcpps462");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC #GTN-W9-BEN-IND #GTN-FED-TAX-TYPE ( * ) #GTN-FED-TAX-AMT ( * ) #GTN-FED-CANADIAN-AMT ( * ) #GTN-ELEC-CDE #GTN-CNTRCT-MONEY-SOURCE #GTN-HARDSHIP-CDE
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec(), pnd_Gtn_W9_Ben_Ind, pnd_Gtn_Fed_Tax_Type.getValue("*"), 
            pnd_Gtn_Fed_Tax_Amt.getValue("*"), pnd_Gtn_Fed_Canadian_Amt.getValue("*"), pnd_Gtn_Elec_Cde, pnd_Gtn_Cntrct_Money_Source, pnd_Gtn_Hardship_Cde)))
        {
            pnd_Read_Count.nadd(1);                                                                                                                                       //Natural: ASSIGN #READ-COUNT := #READ-COUNT + 1
            pnd_Pymnt_Deceased_Nme.reset();                                                                                                                               //Natural: RESET #PYMNT-DECEASED-NME
            DbsUtil.callnat(Fcpns462.class , getCurrentProcessState(), pdaFcpa800.getWf_Pymnt_Addr_Grp().getValue("*"), pnd_Gtn_W9_Ben_Ind, pnd_Gtn_Fed_Tax_Type.getValue("*"),  //Natural: CALLNAT 'FCPNS462' USING WF-PYMNT-ADDR-GRP ( * ) #GTN-W9-BEN-IND #GTN-FED-TAX-TYPE ( * ) #GTN-FED-TAX-AMT ( * ) #GTN-FED-CANADIAN-AMT ( * ) #GTN-ELEC-CDE #GTN-CNTRCT-MONEY-SOURCE #GTN-HARDSHIP-CDE #DIST #PYMNT-DECEASED-NME S462-RET-CODE S462-RET-MSG
                pnd_Gtn_Fed_Tax_Amt.getValue("*"), pnd_Gtn_Fed_Canadian_Amt.getValue("*"), pnd_Gtn_Elec_Cde, pnd_Gtn_Cntrct_Money_Source, pnd_Gtn_Hardship_Cde, 
                pnd_Dist, pnd_Pymnt_Deceased_Nme, s462_Ret_Code, s462_Ret_Msg);
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pnd_Read_Count_Pnd_Read_Count_Alpha_L3.equals("501")))                                                                                          //Natural: IF #READ-COUNT-ALPHA-L3 = '501'
            {
                getReports().write(0, "PROCESSING REC#: ",pnd_Read_Count);                                                                                                //Natural: WRITE 'PROCESSING REC#: ' #READ-COUNT
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //
}
