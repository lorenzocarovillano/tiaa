/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:34 PM
**        * FROM NATURAL PROGRAM : Fcpp187
************************************************************
**        * FILE NAME            : Fcpp187.java
**        * CLASS NAME           : Fcpp187
**        * INSTANCE NAME        : Fcpp187
************************************************************
************************************************************************
* PROGRAM  : FCPP187
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : HOLD CODE REPORT FOR ADAM
* CREATED  : 07/15/97   LIN ZHENG
* FUNCTION : PRODUCES HOLD CODE REPORT FOR NEW ANNUITIZATION.
*          : THIS PROGRAM HAS ABILITY TO PRODUCE HOLD CODE REPORTS FOR
*          : ORIGINS OTHER THAN NZ.  TO DO THIS, JUST MODIFY FCPL187.
*          :
* HISTORY  :
* 05-18-98 : A. YOUNG - IRA / ANNUITY LOAN PROCESSING.  SECOND REGISTER
*          :            PRINTED FOR INTERNAL ROLLOVERS ONLY.
*
* 99/05/05 : LEON GURTOVNIK - ADD LOGIC FOR OV00 & USPS HOLD
* 99/12/07 : ROXAN C.       - EW & FCPAEXT
* 00/03/24 : ROXAN C.       - NEW HOLD CODES ADDED IN THE LOCAL (FCPL187
* 03/04/22 : ROXAN C.       - STOW. FCPAEXT WAS EXPANDED
* 03/06/2006 : LANDRUM  PAYEE MATCH
*              - INCREASED CHECK NBR TO N10.
* 4/2017     : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp187 extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl187 ldaFcpl187;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_N10__R_Field_1;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N7;

    private DbsGroup pnd_Check_Number_N10__R_Field_2;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_A10;
    private DbsField pnd_Old_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Pay_Type_Req_Ind;
    private DbsField pnd_Orgn_Idx;
    private DbsField pnd_Hold_Dept_Idx;
    private DbsField pnd_Hold_Type_Idx;
    private DbsField pnd_Tmp_Orgn_Idx;
    private DbsField pnd_Tmp_Hold_Dept_Idx;

    private DbsGroup pnd_Hdr_Desc;
    private DbsField pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn;
    private DbsField pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Dept;
    private DbsField pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type;

    private DbsGroup pnd_Hold_Type_Totals;
    private DbsField pnd_Hold_Type_Totals_Pnd_Cnt_Hold_Type;
    private DbsField pnd_Hold_Type_Totals_Pnd_Amt_Hold_Type;

    private DbsGroup pnd_Hold_Type_Totals_Redraw;
    private DbsField pnd_Hold_Type_Totals_Redraw_Pnd_Cnt_Hold_Type_Redraw;
    private DbsField pnd_Hold_Type_Totals_Redraw_Pnd_Amt_Hold_Type_Redraw;

    private DbsGroup pnd_Hold_Dept_Totals;
    private DbsField pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept;
    private DbsField pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept;

    private DbsGroup pnd_Orgn_Totals;
    private DbsField pnd_Orgn_Totals_Pnd_Cnt_Orgn;
    private DbsField pnd_Orgn_Totals_Pnd_Amt_Orgn;
    private DbsField pnd_Break_Orgn;
    private DbsField pnd_Break_Hold_Dept;
    private DbsField pnd_Break_Hold_Type;
    private DbsField pnd_Int_Roll_Mode;
    private DbsField pnd_Break_Int_Roll;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Cntrct_Hold_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl187 = new LdaFcpl187();
        registerRecord(ldaFcpl187);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Check_Number_N10 = localVariables.newFieldInRecord("pnd_Check_Number_N10", "#CHECK-NUMBER-N10", FieldType.NUMERIC, 10);

        pnd_Check_Number_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_1", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_N3 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_N10_Pnd_Check_Number_N7 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        pnd_Check_Number_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_2", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_A10 = pnd_Check_Number_N10__R_Field_2.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_A10", "#CHECK-NUMBER-A10", 
            FieldType.STRING, 10);
        pnd_Old_Cntrct_Hold_Cde = localVariables.newFieldInRecord("pnd_Old_Cntrct_Hold_Cde", "#OLD-CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Ws_Pay_Type_Req_Ind = localVariables.newFieldInRecord("pnd_Ws_Pay_Type_Req_Ind", "#WS-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Orgn_Idx = localVariables.newFieldInRecord("pnd_Orgn_Idx", "#ORGN-IDX", FieldType.INTEGER, 1);
        pnd_Hold_Dept_Idx = localVariables.newFieldInRecord("pnd_Hold_Dept_Idx", "#HOLD-DEPT-IDX", FieldType.INTEGER, 1);
        pnd_Hold_Type_Idx = localVariables.newFieldInRecord("pnd_Hold_Type_Idx", "#HOLD-TYPE-IDX", FieldType.INTEGER, 2);
        pnd_Tmp_Orgn_Idx = localVariables.newFieldInRecord("pnd_Tmp_Orgn_Idx", "#TMP-ORGN-IDX", FieldType.INTEGER, 1);
        pnd_Tmp_Hold_Dept_Idx = localVariables.newFieldInRecord("pnd_Tmp_Hold_Dept_Idx", "#TMP-HOLD-DEPT-IDX", FieldType.INTEGER, 1);

        pnd_Hdr_Desc = localVariables.newGroupInRecord("pnd_Hdr_Desc", "#HDR-DESC");
        pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn = pnd_Hdr_Desc.newFieldInGroup("pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn", "#HDR-DESC-ORGN", FieldType.STRING, 40);
        pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Dept = pnd_Hdr_Desc.newFieldInGroup("pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Dept", "#HDR-DESC-HOLD-DEPT", FieldType.STRING, 
            60);
        pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type = pnd_Hdr_Desc.newFieldInGroup("pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type", "#HDR-DESC-HOLD-TYPE", FieldType.STRING, 
            70);

        pnd_Hold_Type_Totals = localVariables.newGroupInRecord("pnd_Hold_Type_Totals", "#HOLD-TYPE-TOTALS");
        pnd_Hold_Type_Totals_Pnd_Cnt_Hold_Type = pnd_Hold_Type_Totals.newFieldInGroup("pnd_Hold_Type_Totals_Pnd_Cnt_Hold_Type", "#CNT-HOLD-TYPE", FieldType.NUMERIC, 
            7);
        pnd_Hold_Type_Totals_Pnd_Amt_Hold_Type = pnd_Hold_Type_Totals.newFieldInGroup("pnd_Hold_Type_Totals_Pnd_Amt_Hold_Type", "#AMT-HOLD-TYPE", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Hold_Type_Totals_Redraw = localVariables.newGroupInRecord("pnd_Hold_Type_Totals_Redraw", "#HOLD-TYPE-TOTALS-REDRAW");
        pnd_Hold_Type_Totals_Redraw_Pnd_Cnt_Hold_Type_Redraw = pnd_Hold_Type_Totals_Redraw.newFieldInGroup("pnd_Hold_Type_Totals_Redraw_Pnd_Cnt_Hold_Type_Redraw", 
            "#CNT-HOLD-TYPE-REDRAW", FieldType.NUMERIC, 7);
        pnd_Hold_Type_Totals_Redraw_Pnd_Amt_Hold_Type_Redraw = pnd_Hold_Type_Totals_Redraw.newFieldInGroup("pnd_Hold_Type_Totals_Redraw_Pnd_Amt_Hold_Type_Redraw", 
            "#AMT-HOLD-TYPE-REDRAW", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Hold_Dept_Totals = localVariables.newGroupInRecord("pnd_Hold_Dept_Totals", "#HOLD-DEPT-TOTALS");
        pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept = pnd_Hold_Dept_Totals.newFieldInGroup("pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept", "#CNT-HOLD-DEPT", FieldType.NUMERIC, 
            7);
        pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept = pnd_Hold_Dept_Totals.newFieldInGroup("pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept", "#AMT-HOLD-DEPT", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Orgn_Totals = localVariables.newGroupInRecord("pnd_Orgn_Totals", "#ORGN-TOTALS");
        pnd_Orgn_Totals_Pnd_Cnt_Orgn = pnd_Orgn_Totals.newFieldInGroup("pnd_Orgn_Totals_Pnd_Cnt_Orgn", "#CNT-ORGN", FieldType.NUMERIC, 7);
        pnd_Orgn_Totals_Pnd_Amt_Orgn = pnd_Orgn_Totals.newFieldInGroup("pnd_Orgn_Totals_Pnd_Amt_Orgn", "#AMT-ORGN", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Break_Orgn = localVariables.newFieldInRecord("pnd_Break_Orgn", "#BREAK-ORGN", FieldType.BOOLEAN, 1);
        pnd_Break_Hold_Dept = localVariables.newFieldInRecord("pnd_Break_Hold_Dept", "#BREAK-HOLD-DEPT", FieldType.BOOLEAN, 1);
        pnd_Break_Hold_Type = localVariables.newFieldInRecord("pnd_Break_Hold_Type", "#BREAK-HOLD-TYPE", FieldType.BOOLEAN, 1);
        pnd_Int_Roll_Mode = localVariables.newFieldInRecord("pnd_Int_Roll_Mode", "#INT-ROLL-MODE", FieldType.BOOLEAN, 1);
        pnd_Break_Int_Roll = localVariables.newFieldInRecord("pnd_Break_Int_Roll", "#BREAK-INT-ROLL", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Cntrct_Hold_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cntrct_Hold_Cde_OLD", "Cntrct_Hold_Cde_OLD", FieldType.STRING, 
            4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl187.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp187() throws Exception
    {
        super("Fcpp187");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  05-18-98                                                                                                                                                     //Natural: FORMAT ( 1 ) PS = 60 LS = 132
        //*                                                                                                                                                               //Natural: FORMAT ( 2 ) PS = 60 LS = 132
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        //*  D WORK FILE 1 PYMNT-ADDR-INFO INV-INFO(*)               /* ROXAN
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 EXT ( * )
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            CheckAtStartofData645();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  RL PAYEE MATCH
            //*  RL PAYEE MATCH
            //*  RL PAYEE MATCH
            pnd_Check_Number_N10_Pnd_Check_Number_A10.reset();                                                                                                            //Natural: RESET #CHECK-NUMBER-A10
            pnd_Check_Number_N10_Pnd_Check_Number_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                                       //Natural: ASSIGN #CHECK-NUMBER-N7 := PYMNT-CHECK-NBR
            pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                             //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
            pnd_Hold_Type_Totals_Pnd_Cnt_Hold_Type.nadd(1);                                                                                                               //Natural: ADD 1 TO #CNT-HOLD-TYPE
            pnd_Hold_Type_Totals_Pnd_Amt_Hold_Type.nadd(pdaFcpaext.getExt_Pymnt_Check_Amt());                                                                             //Natural: ADD PYMNT-CHECK-AMT TO #AMT-HOLD-TYPE
            if (condition(DbsUtil.maskMatches(pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde(),"'R'.")))                                                                 //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = MASK ( 'R'. )
            {
                pnd_Hold_Type_Totals_Redraw_Pnd_Cnt_Hold_Type_Redraw.nadd(1);                                                                                             //Natural: ADD 1 TO #CNT-HOLD-TYPE-REDRAW
                pnd_Hold_Type_Totals_Redraw_Pnd_Amt_Hold_Type_Redraw.nadd(pdaFcpaext.getExt_Pymnt_Check_Amt());                                                           //Natural: ADD PYMNT-CHECK-AMT TO #AMT-HOLD-TYPE-REDRAW
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF CNTRCT-HOLD-TYPE
            //*                                                                                                                                                           //Natural: AT BREAK OF CNTRCT-HOLD-DEPT
            //*                                                                                                                                                           //Natural: AT BREAK OF CNTRCT-ORGN-CDE
            //*  05-18-98
            if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8)) && (pnd_Ws_Pay_Type_Req_Ind.notEquals(8))))                                              //Natural: IF ( PYMNT-PAY-TYPE-REQ-IND = 8 ) AND ( #WS-PAY-TYPE-REQ-IND NE 8 )
            {
                pnd_Break_Int_Roll.setValue(true);                                                                                                                        //Natural: MOVE TRUE TO #BREAK-INT-ROLL
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-TYPE-TOTALS
                sub_Write_Hold_Type_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept.nadd(pnd_Hold_Type_Totals_Pnd_Cnt_Hold_Type);                                                                      //Natural: ADD #CNT-HOLD-TYPE TO #CNT-HOLD-DEPT
                pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept.nadd(pnd_Hold_Type_Totals_Pnd_Amt_Hold_Type);                                                                      //Natural: ADD #AMT-HOLD-TYPE TO #AMT-HOLD-DEPT
                pnd_Hold_Type_Totals.reset();                                                                                                                             //Natural: RESET #HOLD-TYPE-TOTALS #HOLD-TYPE-TOTALS-REDRAW
                pnd_Hold_Type_Totals_Redraw.reset();
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-DEPT-TOTALS
                sub_Write_Hold_Dept_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Orgn_Totals_Pnd_Cnt_Orgn.nadd(pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept);                                                                                //Natural: ADD #CNT-HOLD-DEPT TO #CNT-ORGN
                pnd_Orgn_Totals_Pnd_Amt_Orgn.nadd(pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept);                                                                                //Natural: ADD #AMT-HOLD-DEPT TO #AMT-ORGN
                pnd_Hold_Dept_Totals.reset();                                                                                                                             //Natural: RESET #HOLD-DEPT-TOTALS
                                                                                                                                                                          //Natural: PERFORM WRITE-ORGN-TOTALS
                sub_Write_Orgn_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  #ORGN-IDX
                pnd_Orgn_Totals.reset();                                                                                                                                  //Natural: RESET #ORGN-TOTALS
                pnd_Int_Roll_Mode.setValue(true);                                                                                                                         //Natural: ASSIGN #INT-ROLL-MODE := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  05-18-98
            if (condition(pnd_Break_Orgn.getBoolean() || pnd_Break_Int_Roll.getBoolean()))                                                                                //Natural: IF #BREAK-ORGN OR #BREAK-INT-ROLL
            {
                //*  05-18-98
                pnd_Break_Orgn.reset();                                                                                                                                   //Natural: RESET #BREAK-ORGN #BREAK-INT-ROLL
                pnd_Break_Int_Roll.reset();
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HDR-ORGN-DESC
                sub_Determine_Hdr_Orgn_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HDR-HOLD-DEPT-DESC
                sub_Determine_Hdr_Hold_Dept_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HDR-HOLD-TYPE-DESC
                sub_Determine_Hdr_Hold_Type_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  05-18-98
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  05-18-98
                    if (condition(! (pnd_Break_Int_Roll.getBoolean())))                                                                                                   //Natural: IF NOT #BREAK-INT-ROLL
                    {
                        getReports().newPage(new ReportSpecification(2));                                                                                                 //Natural: NEWPAGE ( 2 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Break_Hold_Dept.getBoolean()))                                                                                                              //Natural: IF #BREAK-HOLD-DEPT
            {
                pnd_Break_Hold_Dept.reset();                                                                                                                              //Natural: RESET #BREAK-HOLD-DEPT
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HDR-HOLD-DEPT-DESC
                sub_Determine_Hdr_Hold_Dept_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  05-18-98
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Break_Hold_Type.getBoolean()))                                                                                                              //Natural: IF #BREAK-HOLD-TYPE
            {
                pnd_Break_Hold_Type.reset();                                                                                                                              //Natural: RESET #BREAK-HOLD-TYPE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HDR-HOLD-TYPE-DESC
                sub_Determine_Hdr_Hold_Type_Desc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  05-18-98
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 1 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 2 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-RECORD
            sub_Write_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Cntrct_Hold_CdeOld.setValue(pdaFcpaext.getExt_Cntrct_Hold_Cde());                                                                                   //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-RECORD
        //* *  'CHECK/NUMBER'          PYMNT-CHECK-NBR (EM=9999999)
        //* *  'CHECK/NUMBER'          PYMNT-CHECK-NBR (EM=9999999)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-HDR-ORGN-DESC
        //* *  'CHK#:'  PYMNT-CHECK-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-HDR-HOLD-DEPT-DESC
        //* *  'CHK#:'     PYMNT-CHECK-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-HDR-HOLD-TYPE-DESC
        //* *    'CHK#:'     PYMNT-CHECK-NBR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HOLD-TYPE-TOTALS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HOLD-DEPT-TOTALS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ORGN-TOTALS
        //* *********************** RL BEGIN - PAYEE MATCH ************MAR 06,2006
        //* *********************** RL BEGIN - PAYEE MATCH ************MAR 06,2006
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //*  RL PAYEE MATCH
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  RL PAYEE MATCH
        //* ************************** RL END-PAYEE MATCH *************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Write_Record() throws Exception                                                                                                                      //Natural: WRITE-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  05-18-98
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            //*  RL PAYEE MATCH
            getReports().display(1, "HOLD/CODE",                                                                                                                          //Natural: DISPLAY ( 1 ) 'HOLD/CODE' CNTRCT-HOLD-CDE 'PAYMENT/DATE' PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) 'ANNUITANT/LAST NAME' PH-LAST-NAME 'ANNUITANT/FIRST NAME' PH-FIRST-NAME 'ANNUITANT/MIDDLE NAME' PH-MIDDLE-NAME 'PIN/NUMBER' CNTRCT-UNQ-ID-NBR 'PPCN' CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 'CHECK/NUMBER' #CHECK-NUMBER-A10 'AMOUNT' PYMNT-CHECK-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) 'REDRAW/STATUS' CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            		pdaFcpaext.getExt_Cntrct_Hold_Cde(),"PAYMENT/DATE",
            		pdaFcpaext.getExt_Pymnt_Check_Dte(), new ReportEditMask ("MM/DD/YYYY"),"ANNUITANT/LAST NAME",
            		pdaFcpaext.getExt_Ph_Last_Name(),"ANNUITANT/FIRST NAME",
            		pdaFcpaext.getExt_Ph_First_Name(),"ANNUITANT/MIDDLE NAME",
            		pdaFcpaext.getExt_Ph_Middle_Name(),"PIN/NUMBER",
            		pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr(),"PPCN",
            		pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"CHECK/NUMBER",
            		pnd_Check_Number_N10_Pnd_Check_Number_A10,"AMOUNT",
            		pdaFcpaext.getExt_Pymnt_Check_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"REDRAW/STATUS",
            		pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  RL PAYEE MATCH
            getReports().display(2, "HOLD/CODE",                                                                                                                          //Natural: DISPLAY ( 2 ) 'HOLD/CODE' CNTRCT-HOLD-CDE 'PAYMENT/DATE' PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) 'ANNUITANT/LAST NAME' PH-LAST-NAME 'ANNUITANT/FIRST NAME' PH-FIRST-NAME 'ANNUITANT/MIDDLE NAME' PH-MIDDLE-NAME 'PIN/NUMBER' CNTRCT-UNQ-ID-NBR 'PPCN' CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 'CHECK/NUMBER' #CHECK-NUMBER-A10 'AMOUNT' PYMNT-CHECK-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) 'REDRAW/STATUS' CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            		pdaFcpaext.getExt_Cntrct_Hold_Cde(),"PAYMENT/DATE",
            		pdaFcpaext.getExt_Pymnt_Check_Dte(), new ReportEditMask ("MM/DD/YYYY"),"ANNUITANT/LAST NAME",
            		pdaFcpaext.getExt_Ph_Last_Name(),"ANNUITANT/FIRST NAME",
            		pdaFcpaext.getExt_Ph_First_Name(),"ANNUITANT/MIDDLE NAME",
            		pdaFcpaext.getExt_Ph_Middle_Name(),"PIN/NUMBER",
            		pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr(),"PPCN",
            		pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"CHECK/NUMBER",
            		pnd_Check_Number_N10_Pnd_Check_Number_A10,"AMOUNT",
            		pdaFcpaext.getExt_Pymnt_Check_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"REDRAW/STATUS",
            		pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Hdr_Orgn_Desc() throws Exception                                                                                                           //Natural: DETERMINE-HDR-ORGN-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn.reset();                                                                                                                           //Natural: RESET #HDR-DESC-ORGN
        DbsUtil.examine(new ExamineSource(ldaFcpl187.getPnd_Cntrct_Orgn_Cde().getValue("*")), new ExamineSearch(pdaFcpaext.getExt_Cntrct_Orgn_Cde()),                     //Natural: EXAMINE #CNTRCT-ORGN-CDE ( * ) FOR CNTRCT-ORGN-CDE GIVING INDEX #ORGN-IDX
            new ExamineGivingIndex(pnd_Orgn_Idx));
        if (condition(pnd_Orgn_Idx.less(1)))                                                                                                                              //Natural: IF #ORGN-IDX < 1
        {
            //*  RL PAYEE MATCH
            getReports().write(0, "*****WARNING: ORIGIN CDE NOT FOUND IN FCPL187 FOR RECORD:",NEWLINE,"ORGN:",pdaFcpaext.getExt_Cntrct_Orgn_Cde(),"PPCN:",                //Natural: WRITE '*****WARNING: ORIGIN CDE NOT FOUND IN FCPL187 FOR RECORD:' / 'ORGN:' CNTRCT-ORGN-CDE 'PPCN:' CNTRCT-PPCN-NBR 'PAYEE:' CNTRCT-PAYEE-CDE 'CHK#:' #CHECK-NUMBER-A10
                pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(),"PAYEE:",pdaFcpaext.getExt_Cntrct_Payee_Cde(),"CHK#:",pnd_Check_Number_N10_Pnd_Check_Number_A10);
            if (Global.isEscape()) return;
            pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn.setValue(DbsUtil.compress(pdaFcpaext.getExt_Cntrct_Orgn_Cde(), "HOLD CODE REPORT"));                                           //Natural: COMPRESS CNTRCT-ORGN-CDE 'HOLD CODE REPORT' INTO #HDR-DESC-ORGN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  05-18-98
            if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                            //Natural: IF NOT #INT-ROLL-MODE
            {
                pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn.setValue(DbsUtil.compress(ldaFcpl187.getPnd_Cntrct_Orgn_Desc().getValue(pnd_Orgn_Idx), "HOLD CODE REPORT"));               //Natural: COMPRESS #CNTRCT-ORGN-DESC ( #ORGN-IDX ) 'HOLD CODE REPORT' INTO #HDR-DESC-ORGN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn.setValue(DbsUtil.compress(ldaFcpl187.getPnd_Cntrct_Orgn_Desc().getValue(pnd_Orgn_Idx), "INTERNAL ROLLOVER HOLD CODE REPORT")); //Natural: COMPRESS #CNTRCT-ORGN-DESC ( #ORGN-IDX ) 'INTERNAL ROLLOVER HOLD CODE REPORT' INTO #HDR-DESC-ORGN
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Hdr_Hold_Dept_Desc() throws Exception                                                                                                      //Natural: DETERMINE-HDR-HOLD-DEPT-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Dept.reset();                                                                                                                      //Natural: RESET #HDR-DESC-HOLD-DEPT
        if (condition(pnd_Orgn_Idx.greaterOrEqual(1)))                                                                                                                    //Natural: IF #ORGN-IDX >= 1
        {
            DbsUtil.examine(new ExamineSource(ldaFcpl187.getPnd_Cntrct_Hold_Dept_Cde().getValue(pnd_Orgn_Idx,"*")), new ExamineSearch(pdaFcpaext.getExt_Cntrct_Hold_Dept()),  //Natural: EXAMINE #CNTRCT-HOLD-DEPT-CDE ( #ORGN-IDX,* ) FOR CNTRCT-HOLD-DEPT GIVING INDEX #TMP-ORGN-IDX #HOLD-DEPT-IDX
                new ExamineGivingIndex(pnd_Tmp_Orgn_Idx, pnd_Hold_Dept_Idx));
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tmp_Orgn_Idx.less(1) || pnd_Hold_Dept_Idx.less(1)))                                                                                             //Natural: IF #TMP-ORGN-IDX < 1 OR #HOLD-DEPT-IDX < 1
        {
            //*  RL PAYEE MATCH
            getReports().write(0, "*****WARNING: HOLD DEPT NOT FOUND IN FCPL187 FOR RECORD:",NEWLINE,"ORGN:",pdaFcpaext.getExt_Cntrct_Orgn_Cde(),"PPCN:",                 //Natural: WRITE '*****WARNING: HOLD DEPT NOT FOUND IN FCPL187 FOR RECORD:' / 'ORGN:' CNTRCT-ORGN-CDE 'PPCN:' CNTRCT-PPCN-NBR 'PAYEE:' CNTRCT-PAYEE-CDE 'CHK#:' #CHECK-NUMBER-A10 'HOLD-CDE:' CNTRCT-HOLD-CDE
                pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(),"PAYEE:",pdaFcpaext.getExt_Cntrct_Payee_Cde(),"CHK#:",pnd_Check_Number_N10_Pnd_Check_Number_A10,"HOLD-CDE:",
                pdaFcpaext.getExt_Cntrct_Hold_Cde());
            if (Global.isEscape()) return;
            pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Dept.setValue(DbsUtil.compress("REQUESTING DEPT:", pdaFcpaext.getExt_Cntrct_Hold_Dept()));                                     //Natural: COMPRESS 'REQUESTING DEPT:' CNTRCT-HOLD-DEPT INTO #HDR-DESC-HOLD-DEPT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Dept.setValue(DbsUtil.compress("REQUESTING DEPT:", ldaFcpl187.getPnd_Cntrct_Hold_Dept_Desc().getValue(pnd_Orgn_Idx,            //Natural: COMPRESS 'REQUESTING DEPT:' #CNTRCT-HOLD-DEPT-DESC ( #ORGN-IDX,#HOLD-DEPT-IDX ) INTO #HDR-DESC-HOLD-DEPT
                pnd_Hold_Dept_Idx)));
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Hdr_Hold_Type_Desc() throws Exception                                                                                                      //Natural: DETERMINE-HDR-HOLD-TYPE-DESC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type.reset();                                                                                                                      //Natural: RESET #HDR-DESC-HOLD-TYPE
        //* *------------* LEON *-------*
        if (condition(pdaFcpaext.getExt_Cntrct_Hold_Cde().equals("OV00") || pdaFcpaext.getExt_Cntrct_Hold_Cde().equals("USPS")))                                          //Natural: IF CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
        {
            pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type.setValue(DbsUtil.compress("HOLD CODE DESCRIPTION: MAIL DISTRIB. SERVICES"));                                              //Natural: COMPRESS 'HOLD CODE DESCRIPTION: MAIL DISTRIB. SERVICES' INTO #HDR-DESC-HOLD-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* *---------------------------*
            if (condition(pnd_Orgn_Idx.greaterOrEqual(1) && pnd_Hold_Dept_Idx.greaterOrEqual(1)))                                                                         //Natural: IF #ORGN-IDX >= 1 AND #HOLD-DEPT-IDX >= 1
            {
                DbsUtil.examine(new ExamineSource(ldaFcpl187.getPnd_Cntrct_Hold_Type_Cde().getValue(pnd_Orgn_Idx,pnd_Hold_Dept_Idx,"*")), new ExamineSearch(pdaFcpaext.getExt_Cntrct_Hold_Type()),  //Natural: EXAMINE #CNTRCT-HOLD-TYPE-CDE ( #ORGN-IDX,#HOLD-DEPT-IDX,* ) FOR CNTRCT-HOLD-TYPE GIVING INDEX #TMP-ORGN-IDX #TMP-HOLD-DEPT-IDX #HOLD-TYPE-IDX
                    new ExamineGivingIndex(pnd_Tmp_Orgn_Idx, pnd_Tmp_Hold_Dept_Idx, pnd_Hold_Type_Idx));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Tmp_Orgn_Idx.less(1) || pnd_Tmp_Hold_Dept_Idx.less(1) || pnd_Hold_Type_Idx.less(1)))                                                        //Natural: IF #TMP-ORGN-IDX < 1 OR #TMP-HOLD-DEPT-IDX < 1 OR #HOLD-TYPE-IDX < 1
            {
                //*  RL PAYEE MATCH
                getReports().write(0, "*****WARNING: HOLD TYPE NOT FOUND IN FCPL187 FOR RECORD:",NEWLINE,"ORGN:",pdaFcpaext.getExt_Cntrct_Orgn_Cde(),"PPCN:",             //Natural: WRITE '*****WARNING: HOLD TYPE NOT FOUND IN FCPL187 FOR RECORD:' / 'ORGN:' CNTRCT-ORGN-CDE 'PPCN:' CNTRCT-PPCN-NBR 'PAYEE:' CNTRCT-PAYEE-CDE 'CHK#:' #CHECK-NUMBER-A10 'HOLD-CDE:' CNTRCT-HOLD-CDE
                    pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(),"PAYEE:",pdaFcpaext.getExt_Cntrct_Payee_Cde(),"CHK#:",pnd_Check_Number_N10_Pnd_Check_Number_A10,
                    "HOLD-CDE:",pdaFcpaext.getExt_Cntrct_Hold_Cde());
                if (Global.isEscape()) return;
                pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type.setValue(DbsUtil.compress("HOLD CODE DESCRIPTION:", pdaFcpaext.getExt_Cntrct_Hold_Type()));                           //Natural: COMPRESS 'HOLD CODE DESCRIPTION:' CNTRCT-HOLD-TYPE INTO #HDR-DESC-HOLD-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type.setValue(DbsUtil.compress("HOLD CODE DESCRIPTION:", ldaFcpl187.getPnd_Cntrct_Hold_Type_Desc().getValue(pnd_Orgn_Idx,  //Natural: COMPRESS 'HOLD CODE DESCRIPTION:' #CNTRCT-HOLD-TYPE-DESC ( #ORGN-IDX,#HOLD-DEPT-IDX,#HOLD-TYPE-IDX ) INTO #HDR-DESC-HOLD-TYPE
                    pnd_Hold_Dept_Idx,pnd_Hold_Type_Idx)));
            }                                                                                                                                                             //Natural: END-IF
            //*  LEON
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Hold_Type_Totals() throws Exception                                                                                                            //Natural: WRITE-HOLD-TYPE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  05-18-98
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(15),pnd_Old_Cntrct_Hold_Cde,"TOTALS:",new TabSetting(40),"NUMBER OF HOLDS",pnd_Hold_Type_Totals_Pnd_Cnt_Hold_Type,new  //Natural: WRITE ( 1 ) //15T #OLD-CNTRCT-HOLD-CDE 'TOTALS:' 40T 'NUMBER OF HOLDS' #CNT-HOLD-TYPE 67T 'AMOUNT' #AMT-HOLD-TYPE ( EM = ZZZ,ZZZ,ZZ9.99- ) //15T 'REDRAW STATUS TOTALS:' 40T 'NUMBER OF HOLDS' #CNT-HOLD-TYPE-REDRAW 67T 'AMOUNT' #AMT-HOLD-TYPE-REDRAW ( EM = ZZZ,ZZZ,ZZ9.99- )
                TabSetting(67),"AMOUNT",pnd_Hold_Type_Totals_Pnd_Amt_Hold_Type, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,new TabSetting(15),"REDRAW STATUS TOTALS:",new 
                TabSetting(40),"NUMBER OF HOLDS",pnd_Hold_Type_Totals_Redraw_Pnd_Cnt_Hold_Type_Redraw,new TabSetting(67),"AMOUNT",pnd_Hold_Type_Totals_Redraw_Pnd_Amt_Hold_Type_Redraw, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(15),pnd_Old_Cntrct_Hold_Cde,"TOTALS:",new TabSetting(40),"NUMBER OF HOLDS",pnd_Hold_Type_Totals_Pnd_Cnt_Hold_Type,new  //Natural: WRITE ( 2 ) //15T #OLD-CNTRCT-HOLD-CDE 'TOTALS:' 40T 'NUMBER OF HOLDS' #CNT-HOLD-TYPE 67T 'AMOUNT' #AMT-HOLD-TYPE ( EM = ZZZ,ZZZ,ZZ9.99- ) //15T 'REDRAW STATUS TOTALS:' 40T 'NUMBER OF HOLDS' #CNT-HOLD-TYPE-REDRAW 67T 'AMOUNT' #AMT-HOLD-TYPE-REDRAW ( EM = ZZZ,ZZZ,ZZ9.99- )
                TabSetting(67),"AMOUNT",pnd_Hold_Type_Totals_Pnd_Amt_Hold_Type, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,new TabSetting(15),"REDRAW STATUS TOTALS:",new 
                TabSetting(40),"NUMBER OF HOLDS",pnd_Hold_Type_Totals_Redraw_Pnd_Cnt_Hold_Type_Redraw,new TabSetting(67),"AMOUNT",pnd_Hold_Type_Totals_Redraw_Pnd_Amt_Hold_Type_Redraw, 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Hold_Dept_Totals() throws Exception                                                                                                            //Natural: WRITE-HOLD-DEPT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  05-18-98
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(15),"DEPT TOTALS:",new TabSetting(40),"NUMBER OF HOLDS",pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept,new  //Natural: WRITE ( 1 ) //15T 'DEPT TOTALS:' 40T 'NUMBER OF HOLDS' #CNT-HOLD-DEPT 67T 'AMOUNT' #AMT-HOLD-DEPT ( EM = ZZZ,ZZZ,ZZ9.99- )
                TabSetting(67),"AMOUNT",pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(15),"DEPT TOTALS:",new TabSetting(40),"NUMBER OF HOLDS",pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept,new  //Natural: WRITE ( 2 ) //15T 'DEPT TOTALS:' 40T 'NUMBER OF HOLDS' #CNT-HOLD-DEPT 67T 'AMOUNT' #AMT-HOLD-DEPT ( EM = ZZZ,ZZZ,ZZ9.99- )
                TabSetting(67),"AMOUNT",pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Orgn_Totals() throws Exception                                                                                                                 //Natural: WRITE-ORGN-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  05-18-98
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(15),"ORIGIN TOTALS:",new TabSetting(40),"NUMBER OF HOLDS",pnd_Orgn_Totals_Pnd_Cnt_Orgn,new  //Natural: WRITE ( 1 ) //15T 'ORIGIN TOTALS:' 40T 'NUMBER OF HOLDS' #CNT-ORGN 67T 'AMOUNT' #AMT-ORGN ( EM = ZZZ,ZZZ,ZZ9.99- )
                TabSetting(67),"AMOUNT",pnd_Orgn_Totals_Pnd_Amt_Orgn, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(15),"ORIGIN TOTALS:",new TabSetting(40),"NUMBER OF HOLDS",pnd_Orgn_Totals_Pnd_Cnt_Orgn,new  //Natural: WRITE ( 2 ) //15T 'ORIGIN TOTALS:' 40T 'NUMBER OF HOLDS' #CNT-ORGN 67T 'AMOUNT' #AMT-ORGN ( EM = ZZZ,ZZZ,ZZ9.99- )
                TabSetting(67),"AMOUNT",pnd_Orgn_Totals_Pnd_Amt_Orgn, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET CHECK-NO PREFIX FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 053T 'CONSOLIDATED PAYMENT SYSTEM' 110T 'PAGE:' *PAGE-NUMBER ( 1 ) / 'RUN DATE:' *DATU 055T 'ON-LINE PAYMENT SYSTEM' 110T 'RUN TIME:' *TIMX //055T #HDR-DESC-ORGN //045T #HDR-DESC-HOLD-TYPE //045T #HDR-DESC-HOLD-DEPT //
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(1),NEWLINE,"RUN DATE:",Global.getDATU(),new TabSetting(55),"ON-LINE PAYMENT SYSTEM",new 
                        TabSetting(110),"RUN TIME:",Global.getTIMX(),NEWLINE,NEWLINE,new TabSetting(55),pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn,NEWLINE,NEWLINE,new 
                        TabSetting(45),pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type,NEWLINE,NEWLINE,new TabSetting(45),pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Dept,NEWLINE,
                        NEWLINE);
                    //*  05-18-98
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new  //Natural: WRITE ( 2 ) NOTITLE NOHDR *INIT-USER '-' *PROGRAM 053T 'CONSOLIDATED PAYMENT SYSTEM' 110T 'PAGE:' *PAGE-NUMBER ( 2 ) / 'RUN DATE:' *DATU 055T 'ON-LINE PAYMENT SYSTEM' 110T 'RUN TIME:' *TIMX //055T #HDR-DESC-ORGN //045T #HDR-DESC-HOLD-TYPE //045T #HDR-DESC-HOLD-DEPT //
                        TabSetting(110),"PAGE:",getReports().getPageNumberDbs(2),NEWLINE,"RUN DATE:",Global.getDATU(),new TabSetting(55),"ON-LINE PAYMENT SYSTEM",new 
                        TabSetting(110),"RUN TIME:",Global.getTIMX(),NEWLINE,NEWLINE,new TabSetting(55),pnd_Hdr_Desc_Pnd_Hdr_Desc_Orgn,NEWLINE,NEWLINE,new 
                        TabSetting(45),pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Type,NEWLINE,NEWLINE,new TabSetting(45),pnd_Hdr_Desc_Pnd_Hdr_Desc_Hold_Dept,NEWLINE,
                        NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pdaFcpaext_getExt_Cntrct_Hold_TypeIsBreak = pdaFcpaext.getExt_Cntrct_Hold_Type().isBreak(endOfData);
        boolean pdaFcpaext_getExt_Cntrct_Hold_DeptIsBreak = pdaFcpaext.getExt_Cntrct_Hold_Dept().isBreak(endOfData);
        boolean pdaFcpaext_getExt_Cntrct_Orgn_CdeIsBreak = pdaFcpaext.getExt_Cntrct_Orgn_Cde().isBreak(endOfData);
        if (condition(pdaFcpaext_getExt_Cntrct_Hold_TypeIsBreak || pdaFcpaext_getExt_Cntrct_Hold_DeptIsBreak || pdaFcpaext_getExt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Break_Hold_Type.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #BREAK-HOLD-TYPE
            pnd_Old_Cntrct_Hold_Cde.setValue(readWork01Cntrct_Hold_CdeOld);                                                                                               //Natural: ASSIGN #OLD-CNTRCT-HOLD-CDE := OLD ( CNTRCT-HOLD-CDE )
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-TYPE-TOTALS
            sub_Write_Hold_Type_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept.nadd(pnd_Hold_Type_Totals_Pnd_Cnt_Hold_Type);                                                                          //Natural: ADD #CNT-HOLD-TYPE TO #CNT-HOLD-DEPT
            pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept.nadd(pnd_Hold_Type_Totals_Pnd_Amt_Hold_Type);                                                                          //Natural: ADD #AMT-HOLD-TYPE TO #AMT-HOLD-DEPT
            pnd_Hold_Type_Totals.reset();                                                                                                                                 //Natural: RESET #HOLD-TYPE-TOTALS #HOLD-TYPE-TOTALS-REDRAW
            pnd_Hold_Type_Totals_Redraw.reset();
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpaext_getExt_Cntrct_Hold_DeptIsBreak || pdaFcpaext_getExt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Break_Hold_Dept.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #BREAK-HOLD-DEPT
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-DEPT-TOTALS
            sub_Write_Hold_Dept_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_Orgn_Totals_Pnd_Cnt_Orgn.nadd(pnd_Hold_Dept_Totals_Pnd_Cnt_Hold_Dept);                                                                                    //Natural: ADD #CNT-HOLD-DEPT TO #CNT-ORGN
            pnd_Orgn_Totals_Pnd_Amt_Orgn.nadd(pnd_Hold_Dept_Totals_Pnd_Amt_Hold_Dept);                                                                                    //Natural: ADD #AMT-HOLD-DEPT TO #AMT-ORGN
            pnd_Hold_Dept_Totals.reset();                                                                                                                                 //Natural: RESET #HOLD-DEPT-TOTALS
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpaext_getExt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Break_Orgn.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO #BREAK-ORGN
                                                                                                                                                                          //Natural: PERFORM WRITE-ORGN-TOTALS
            sub_Write_Orgn_Totals();
            if (condition(Global.isEscape())) {return;}
            //*  #ORGN-IDX
            pnd_Orgn_Totals.reset();                                                                                                                                      //Natural: RESET #ORGN-TOTALS
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");

        getReports().setDisplayColumns(1, "HOLD/CODE",
        		pdaFcpaext.getExt_Cntrct_Hold_Cde(),"PAYMENT/DATE",
        		pdaFcpaext.getExt_Pymnt_Check_Dte(), new ReportEditMask ("MM/DD/YYYY"),"ANNUITANT/LAST NAME",
        		pdaFcpaext.getExt_Ph_Last_Name(),"ANNUITANT/FIRST NAME",
        		pdaFcpaext.getExt_Ph_First_Name(),"ANNUITANT/MIDDLE NAME",
        		pdaFcpaext.getExt_Ph_Middle_Name(),"PIN/NUMBER",
        		pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr(),"PPCN",
        		pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"CHECK/NUMBER",
        		pnd_Check_Number_N10_Pnd_Check_Number_A10,"AMOUNT",
        		pdaFcpaext.getExt_Pymnt_Check_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"REDRAW/STATUS",
        		pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde());
        getReports().setDisplayColumns(2, "HOLD/CODE",
        		pdaFcpaext.getExt_Cntrct_Hold_Cde(),"PAYMENT/DATE",
        		pdaFcpaext.getExt_Pymnt_Check_Dte(), new ReportEditMask ("MM/DD/YYYY"),"ANNUITANT/LAST NAME",
        		pdaFcpaext.getExt_Ph_Last_Name(),"ANNUITANT/FIRST NAME",
        		pdaFcpaext.getExt_Ph_First_Name(),"ANNUITANT/MIDDLE NAME",
        		pdaFcpaext.getExt_Ph_Middle_Name(),"PIN/NUMBER",
        		pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr(),"PPCN",
        		pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"CHECK/NUMBER",
        		pnd_Check_Number_N10_Pnd_Check_Number_A10,"AMOUNT",
        		pdaFcpaext.getExt_Pymnt_Check_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),"REDRAW/STATUS",
        		pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde());
    }
    private void CheckAtStartofData645() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*  05-18-98
            if (condition(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                                                          //Natural: IF PYMNT-PAY-TYPE-REQ-IND = 8
            {
                pnd_Int_Roll_Mode.setValue(true);                                                                                                                         //Natural: ASSIGN #INT-ROLL-MODE := TRUE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HDR-ORGN-DESC
            sub_Determine_Hdr_Orgn_Desc();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HDR-HOLD-DEPT-DESC
            sub_Determine_Hdr_Hold_Dept_Desc();
            if (condition(Global.isEscape())) {return;}
            //*  05-18-98
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HDR-HOLD-TYPE-DESC
            sub_Determine_Hdr_Hold_Type_Desc();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Pay_Type_Req_Ind.setValue(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind());                                                                                 //Natural: ASSIGN #WS-PAY-TYPE-REQ-IND := PYMNT-PAY-TYPE-REQ-IND
        }                                                                                                                                                                 //Natural: END-START
    }
}
