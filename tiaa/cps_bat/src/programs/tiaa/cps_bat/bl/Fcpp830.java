/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:34 PM
**        * FROM NATURAL PROGRAM : Fcpp830
************************************************************
**        * FILE NAME            : Fcpp830.java
**        * CLASS NAME           : Fcpp830
**        * INSTANCE NAME        : Fcpp830
************************************************************
************************************************************************
* PROGRAM  : FCPP830
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS "AP" LEDGER INTERFACE FILE
* CREATED  : 03/18/96
*
* HISTORY  : 04/03/1997    RITA SALGADO
*          : - GET ISA CODE FROM FCPL199B INSTEAD OF USING FCPN801
*          : - GET LEDGER DESCRIPTION USING ISA NBR INSTEAD OF FUND CODE
*          :
*          : 05/22/00 MCGEE - STOW - FCPL199B CHANGE
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp830 extends BLNatBase
{
    // Data Areas
    private LdaFcpl801c ldaFcpl801c;
    private PdaFcpa121 pdaFcpa121;
    private LdaFcpl199b ldaFcpl199b;
    private LdaFcpl830 ldaFcpl830;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Cr_Amt;
    private DbsField pnd_Db_Amt;
    private DbsField pnd_Cr_Tot;
    private DbsField pnd_Db_Tot;
    private DbsField pnd_Isa_Sub_A;

    private DbsGroup pnd_Isa_Sub_A__R_Field_1;
    private DbsField pnd_Isa_Sub_A_Pnd_Isa_Sub;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Inv_Acct_Ledgr_NbrOld;
    private DbsField readWork01Pnd_Cr_AmtSum146;
    private DbsField readWork01Pnd_Cr_AmtSum;
    private DbsField readWork01Pnd_Db_AmtSum146;
    private DbsField readWork01Pnd_Db_AmtSum;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl801c = new LdaFcpl801c();
        registerRecord(ldaFcpl801c);
        localVariables = new DbsRecord();
        pdaFcpa121 = new PdaFcpa121(localVariables);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);
        ldaFcpl830 = new LdaFcpl830();
        registerRecord(ldaFcpl830);

        // Local Variables
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cr_Amt = localVariables.newFieldInRecord("pnd_Cr_Amt", "#CR-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Db_Amt = localVariables.newFieldInRecord("pnd_Db_Amt", "#DB-AMT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Cr_Tot = localVariables.newFieldInRecord("pnd_Cr_Tot", "#CR-TOT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Db_Tot = localVariables.newFieldInRecord("pnd_Db_Tot", "#DB-TOT", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Isa_Sub_A = localVariables.newFieldInRecord("pnd_Isa_Sub_A", "#ISA-SUB-A", FieldType.STRING, 2);

        pnd_Isa_Sub_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Isa_Sub_A__R_Field_1", "REDEFINE", pnd_Isa_Sub_A);
        pnd_Isa_Sub_A_Pnd_Isa_Sub = pnd_Isa_Sub_A__R_Field_1.newFieldInGroup("pnd_Isa_Sub_A_Pnd_Isa_Sub", "#ISA-SUB", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Inv_Acct_Ledgr_NbrOld = internalLoopRecord.newFieldInRecord("ReadWork01_Inv_Acct_Ledgr_Nbr_OLD", "Inv_Acct_Ledgr_Nbr_OLD", FieldType.STRING, 
            15);
        readWork01Pnd_Cr_AmtSum146 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM_146", "Pnd_Cr_Amt_SUM_146", FieldType.PACKED_DECIMAL, 
            15, 2);
        readWork01Pnd_Cr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM", "Pnd_Cr_Amt_SUM", FieldType.PACKED_DECIMAL, 15, 2);
        readWork01Pnd_Db_AmtSum146 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Db_Amt_SUM_146", "Pnd_Db_Amt_SUM_146", FieldType.PACKED_DECIMAL, 
            15, 2);
        readWork01Pnd_Db_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Db_Amt_SUM", "Pnd_Db_Amt_SUM", FieldType.PACKED_DECIMAL, 15, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl801c.initializeValues();
        ldaFcpl199b.initializeValues();
        ldaFcpl830.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp830() throws Exception
    {
        super("Fcpp830");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) PS = 58 LS = 132
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 59T 'CONTROL REPORT' 120T 'REPORT: RPT1' //
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #LEDGER-EXT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl801c.getPnd_Ledger_Ext())))
        {
            CheckAtStartofData121();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            ldaFcpl830.getPnd_Output_Gl_Dtl().reset();                                                                                                                    //Natural: RESET #OUTPUT-GL-DTL
            pnd_Rec_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-CNT
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            //* *    MOVE #LEDGER-EXT.CNTRCT-ANNTY-INS-TYPE TO #CNTRCT-ANNTY-INS-TYPE
            pnd_Isa_Sub_A.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash());                                                                                         //Natural: MOVE #LEDGER-EXT.INV-ISA-HASH TO #ISA-SUB-A
            pnd_Cr_Amt.reset();                                                                                                                                           //Natural: RESET #CR-AMT #DB-AMT
            pnd_Db_Amt.reset();
            if (condition(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind().equals("C")))                                                                                //Natural: IF #LEDGER-EXT.INV-ACCT-LEDGR-IND = 'C'
            {
                pnd_Cr_Amt.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                                                                                  //Natural: MOVE #LEDGER-EXT.INV-ACCT-LEDGR-AMT TO #CR-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind().equals("D")))                                                                            //Natural: IF #LEDGER-EXT.INV-ACCT-LEDGR-IND = 'D'
                {
                    pnd_Db_Amt.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                                                                              //Natural: MOVE #LEDGER-EXT.INV-ACCT-LEDGR-AMT TO #DB-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //* *AT BREAK OF #COMPANY-KEY
            //* *  IF OLD(#COMPANY-KEY) NE #COMPANY-KEY
            //* *    PERFORM GET-COMPANY
            //* *  END-IF
            //* *END-BREAK
            //*                                                                                                                                                           //Natural: AT BREAK OF #LEDGER-EXT.INV-ACCT-LEDGR-NBR
            ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Cntrct_Ppcn_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Ppcn_Nbr());                                            //Natural: MOVE #LEDGER-EXT.CNTRCT-PPCN-NBR TO #O-CNTRCT-PPCN-NBR
            ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Cntrct_Payee_Cde().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Payee_Cde());                                          //Natural: MOVE #LEDGER-EXT.CNTRCT-PAYEE-CDE TO #O-CNTRCT-PAYEE-CDE
            ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Cntrct_Mode_Cde().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Mode_Cde());                                            //Natural: MOVE #LEDGER-EXT.CNTRCT-MODE-CDE TO #O-CNTRCT-MODE-CDE
            ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Pymnt_Acctg_Dte().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Acctg_Dte());                                            //Natural: MOVE #LEDGER-EXT.PYMNT-ACCTG-DTE TO #O-PYMNT-ACCTG-DTE
            ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr());                                      //Natural: MOVE #LEDGER-EXT.INV-ACCT-LEDGR-NBR TO #O-INV-ACCT-LEDGR-NBR
            ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Inv_Pymnt_Ded_Cde().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Pymnt_Ded_Cde());                                        //Natural: MOVE #LEDGER-EXT.INV-PYMNT-DED-CDE TO #O-INV-PYMNT-DED-CDE
            ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Desc().setValue(pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc());                                        //Natural: MOVE #FCPA121.INV-ACCT-LEDGR-DESC TO #O-INV-ACCT-LEDGR-DESC
            ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Gl_Company().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Isa_Sub_A_Pnd_Isa_Sub.getInt()              //Natural: MOVE #ISA-LDA.#ISA-CDE ( #ISA-SUB ) TO #O-GL-COMPANY
                + 1));
            if (condition(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind().equals("C")))                                                                                //Natural: IF #LEDGER-EXT.INV-ACCT-LEDGR-IND = 'C'
            {
                ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Cr_Amt().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                               //Natural: MOVE #LEDGER-EXT.INV-ACCT-LEDGR-AMT TO #O-INV-ACCT-LEDGR-CR-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind().equals("D")))                                                                            //Natural: IF #LEDGER-EXT.INV-ACCT-LEDGR-IND = 'D'
                {
                    ldaFcpl830.getPnd_Output_Gl_Dtl_Pnd_O_Inv_Acct_Ledgr_Db_Amt().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                           //Natural: MOVE #LEDGER-EXT.INV-ACCT-LEDGR-AMT TO #O-INV-ACCT-LEDGR-DB-AMT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(2, false, ldaFcpl830.getPnd_Output_Gl_Dtl());                                                                                            //Natural: WRITE WORK FILE 2 #OUTPUT-GL-DTL
            readWork01Inv_Acct_Ledgr_NbrOld.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr());                                                                 //Natural: END-WORK
            readWork01Pnd_Cr_AmtSum146.nadd(readWork01Pnd_Cr_AmtSum146,pnd_Cr_Amt);
            readWork01Pnd_Cr_AmtSum.nadd(readWork01Pnd_Cr_AmtSum,pnd_Cr_Amt);
            readWork01Pnd_Db_AmtSum146.nadd(readWork01Pnd_Db_AmtSum146,pnd_Db_Amt);
            readWork01Pnd_Db_AmtSum.nadd(readWork01Pnd_Db_AmtSum,pnd_Db_Amt);
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        getReports().write(1, "-",new RepeatItem(15),new TabSetting(17),"-",new RepeatItem(21),new TabSetting(39),"-",new RepeatItem(21),NEWLINE,"TOTAL",new              //Natural: WRITE ( 1 ) '-' ( 15 ) 17T '-' ( 21 ) 39T '-' ( 21 ) / 'TOTAL' 17T #CR-TOT 39T #DB-TOT
            TabSetting(17),pnd_Cr_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99-"),new TabSetting(39),pnd_Db_Tot, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-GL-ACCT-DESC
    }
    private void sub_Get_Gl_Acct_Desc() throws Exception                                                                                                                  //Natural: GET-GL-ACCT-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pdaFcpa121.getPnd_Fcpa121().reset();                                                                                                                              //Natural: RESET #FCPA121
        pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr());                                                      //Natural: ASSIGN #FCPA121.INV-ACCT-LEDGR-NBR := #LEDGER-EXT.INV-ACCT-LEDGR-NBR
        pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Isa().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Isa_Sub_A_Pnd_Isa_Sub.getInt() + 1));                     //Natural: ASSIGN #FCPA121.INV-ACCT-ISA := #ISA-LDA.#ISA-CDE ( #ISA-SUB )
        DbsUtil.callnat(Fcpn121.class , getCurrentProcessState(), pdaFcpa121.getPnd_Fcpa121());                                                                           //Natural: CALLNAT 'FCPN121' USING #FCPA121
        if (condition(Global.isEscape())) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl801c_getPnd_Ledger_Ext_Inv_Acct_Ledgr_NbrIsBreak = ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr().isBreak(endOfData);
        if (condition(ldaFcpl801c_getPnd_Ledger_Ext_Inv_Acct_Ledgr_NbrIsBreak))
        {
            if (condition(readWork01Inv_Acct_Ledgr_NbrOld.notEquals(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr())))                                                 //Natural: IF OLD ( #LEDGER-EXT.INV-ACCT-LEDGR-NBR ) NE #LEDGER-EXT.INV-ACCT-LEDGR-NBR
            {
                                                                                                                                                                          //Natural: PERFORM GET-GL-ACCT-DESC
                sub_Get_Gl_Acct_Desc();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cr_Tot.nadd(readWork01Pnd_Cr_AmtSum146);                                                                                                                  //Natural: ADD SUM ( #CR-AMT ) TO #CR-TOT
            pnd_Db_Tot.nadd(readWork01Pnd_Db_AmtSum146);                                                                                                                  //Natural: ADD SUM ( #DB-AMT ) TO #DB-TOT
            getReports().display(1, "LEDGER ACCOUNT#",                                                                                                                    //Natural: DISPLAY ( 1 ) 'LEDGER ACCOUNT#' OLD ( #LEDGER-EXT.INV-ACCT-LEDGR-NBR ) 'CREDIT AMOUNT' SUM ( #CR-AMT ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99- ) 'DEBIT AMOUNT' SUM ( #DB-AMT ) ( EM = Z,ZZZ,ZZZ,ZZZ,ZZ9.99- )
            		readWork01Inv_Acct_Ledgr_NbrOld,"CREDIT AMOUNT",
            		readWork01Pnd_Cr_AmtSum, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99-"),"DEBIT AMOUNT",
            		readWork01Pnd_Db_AmtSum, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape())) return;
            readWork01Pnd_Cr_AmtSum146.setDec(new DbsDecimal(0));                                                                                                         //Natural: END-BREAK
            readWork01Pnd_Db_AmtSum146.setDec(new DbsDecimal(0));
        }
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(59),"CONTROL REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "LEDGER ACCOUNT#",
        		readWork01Inv_Acct_Ledgr_NbrOld,"CREDIT AMOUNT",
        		readWork01Pnd_Cr_AmtSum, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99-"),"DEBIT AMOUNT",
        		readWork01Pnd_Db_AmtSum, new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZ9.99-"));
    }
    private void CheckAtStartofData121() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //* *    MOVE #LEDGER-EXT.CNTRCT-ANNTY-INS-TYPE TO #CNTRCT-ANNTY-INS-TYPE
            pnd_Isa_Sub_A.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash());                                                                                         //Natural: MOVE #LEDGER-EXT.INV-ISA-HASH TO #ISA-SUB-A
                                                                                                                                                                          //Natural: PERFORM GET-GL-ACCT-DESC
            sub_Get_Gl_Acct_Desc();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
