/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:40 PM
**        * FROM NATURAL PROGRAM : Fcpp256
************************************************************
**        * FILE NAME            : Fcpp256.java
**        * CLASS NAME           : Fcpp256
**        * INSTANCE NAME        : Fcpp256
************************************************************
* FCPP256 - PREVENT DUPLICATE VOID TRANSMISSION.
*
* MATCH PROCESS PROGRAM WHICH CHECKS THE VOIDS CREATED TODAY AGAINST
* THOSE CREATED ON PRIOR DAYS.
*
* IF NEW-VOID REC NOT ON OLD-VOID FILE
*    WRITE NEW-VOID REC TO NEW-VOID-OUTPUT
*
* IF NEW-VOID REC IS ON THE OLD-VOID FILE
*    DISPLAY ON DUPLICATE REPORT
*    DO NOT WRITE TO OUTPUT FILE
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp256 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_New_Void_Rec;

    private DbsGroup pnd_New_Void_Rec__R_Field_1;
    private DbsField pnd_New_Void_Rec_Pnd_Customer_Id_Num_D;
    private DbsField pnd_New_Void_Rec_Pnd_Record_Type;
    private DbsField pnd_New_Void_Rec_Pnd_Bank_Num;
    private DbsField pnd_New_Void_Rec_Pnd_New_Account_Num;
    private DbsField pnd_New_Void_Rec_Pnd_Run_Date;
    private DbsField pnd_New_Void_Rec_Pnd_Run_Time;
    private DbsField pnd_New_Void_Rec_Pnd_Input_Type;
    private DbsField pnd_New_Void_Rec_Pnd_Stop_Type;
    private DbsField pnd_New_Void_Rec_Pnd_New_Begin_Check_Num;
    private DbsField pnd_New_Void_Rec_Pnd_Ending_Check_Num;
    private DbsField pnd_New_Void_Rec_Pnd_Check_Amt;
    private DbsField pnd_New_Void_Rec_Pnd_Check_Issue_Date_N;
    private DbsField pnd_New_Void_Rec_Pnd_Payee_Name;
    private DbsField pnd_New_Void_Rec_Pnd_Reason;
    private DbsField pnd_New_Void_Rec_Pnd_Auto_Check_Paid_Inquiry;
    private DbsField pnd_New_Void_Rec_Pnd_Stop_Duration_Years;
    private DbsField pnd_New_Void_Rec_Pnd_User_Id;
    private DbsField pnd_Old_Void_Rec;

    private DbsGroup pnd_Old_Void_Rec__R_Field_2;
    private DbsField pnd_Old_Void_Rec_Pnd_Old_Account_Num;
    private DbsField pnd_Old_Void_Rec_Pnd_Old_Begin_Check_Num;
    private DbsField pnd_Old_Void_Rec_Pnd_Filler1;
    private DbsField pnd_New_Void_Match_Key;

    private DbsGroup pnd_New_Void_Match_Key__R_Field_3;
    private DbsField pnd_New_Void_Match_Key_Pnd_New_Match_Account;
    private DbsField pnd_New_Void_Match_Key_Pnd_New_Match_Check_Num;
    private DbsField pnd_Old_Void_Match_Key;

    private DbsGroup pnd_Old_Void_Match_Key__R_Field_4;
    private DbsField pnd_Old_Void_Match_Key_Pnd_Old_Match_Account;
    private DbsField pnd_Old_Void_Match_Key_Pnd_Old_Match_Check_Num;
    private DbsField pnd_Last_New_Void_Key;
    private DbsField pnd_Last_Old_Void_Key;
    private DbsField pnd_Old_In;
    private DbsField pnd_Old_Unmatched;
    private DbsField pnd_Old_Matched;
    private DbsField pnd_New_In;
    private DbsField pnd_New_Out;
    private DbsField pnd_New_Dups;
    private DbsField pnd_New_Eof;
    private DbsField pnd_Old_Eof;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        pnd_New_Void_Rec = localVariables.newFieldInRecord("pnd_New_Void_Rec", "#NEW-VOID-REC", FieldType.STRING, 115);

        pnd_New_Void_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_New_Void_Rec__R_Field_1", "REDEFINE", pnd_New_Void_Rec);
        pnd_New_Void_Rec_Pnd_Customer_Id_Num_D = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Customer_Id_Num_D", "#CUSTOMER-ID-NUM-D", 
            FieldType.NUMERIC, 8);
        pnd_New_Void_Rec_Pnd_Record_Type = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_New_Void_Rec_Pnd_Bank_Num = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Bank_Num", "#BANK-NUM", FieldType.NUMERIC, 3);
        pnd_New_Void_Rec_Pnd_New_Account_Num = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_New_Account_Num", "#NEW-ACCOUNT-NUM", 
            FieldType.NUMERIC, 13);
        pnd_New_Void_Rec_Pnd_Run_Date = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Run_Date", "#RUN-DATE", FieldType.NUMERIC, 8);
        pnd_New_Void_Rec_Pnd_Run_Time = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Run_Time", "#RUN-TIME", FieldType.NUMERIC, 6);
        pnd_New_Void_Rec_Pnd_Input_Type = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Input_Type", "#INPUT-TYPE", FieldType.STRING, 
            2);
        pnd_New_Void_Rec_Pnd_Stop_Type = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Stop_Type", "#STOP-TYPE", FieldType.STRING, 
            1);
        pnd_New_Void_Rec_Pnd_New_Begin_Check_Num = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_New_Begin_Check_Num", "#NEW-BEGIN-CHECK-NUM", 
            FieldType.NUMERIC, 11);
        pnd_New_Void_Rec_Pnd_Ending_Check_Num = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Ending_Check_Num", "#ENDING-CHECK-NUM", 
            FieldType.NUMERIC, 11);
        pnd_New_Void_Rec_Pnd_Check_Amt = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Check_Amt", "#CHECK-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_New_Void_Rec_Pnd_Check_Issue_Date_N = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Check_Issue_Date_N", "#CHECK-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);
        pnd_New_Void_Rec_Pnd_Payee_Name = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Payee_Name", "#PAYEE-NAME", FieldType.STRING, 
            10);
        pnd_New_Void_Rec_Pnd_Reason = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Reason", "#REASON", FieldType.STRING, 10);
        pnd_New_Void_Rec_Pnd_Auto_Check_Paid_Inquiry = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Auto_Check_Paid_Inquiry", "#AUTO-CHECK-PAID-INQUIRY", 
            FieldType.STRING, 1);
        pnd_New_Void_Rec_Pnd_Stop_Duration_Years = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_Stop_Duration_Years", "#STOP-DURATION-YEARS", 
            FieldType.NUMERIC, 1);
        pnd_New_Void_Rec_Pnd_User_Id = pnd_New_Void_Rec__R_Field_1.newFieldInGroup("pnd_New_Void_Rec_Pnd_User_Id", "#USER-ID", FieldType.STRING, 6);
        pnd_Old_Void_Rec = localVariables.newFieldInRecord("pnd_Old_Void_Rec", "#OLD-VOID-REC", FieldType.STRING, 150);

        pnd_Old_Void_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Old_Void_Rec__R_Field_2", "REDEFINE", pnd_Old_Void_Rec);
        pnd_Old_Void_Rec_Pnd_Old_Account_Num = pnd_Old_Void_Rec__R_Field_2.newFieldInGroup("pnd_Old_Void_Rec_Pnd_Old_Account_Num", "#OLD-ACCOUNT-NUM", 
            FieldType.NUMERIC, 13);
        pnd_Old_Void_Rec_Pnd_Old_Begin_Check_Num = pnd_Old_Void_Rec__R_Field_2.newFieldInGroup("pnd_Old_Void_Rec_Pnd_Old_Begin_Check_Num", "#OLD-BEGIN-CHECK-NUM", 
            FieldType.NUMERIC, 10);
        pnd_Old_Void_Rec_Pnd_Filler1 = pnd_Old_Void_Rec__R_Field_2.newFieldInGroup("pnd_Old_Void_Rec_Pnd_Filler1", "#FILLER1", FieldType.STRING, 127);
        pnd_New_Void_Match_Key = localVariables.newFieldInRecord("pnd_New_Void_Match_Key", "#NEW-VOID-MATCH-KEY", FieldType.STRING, 24);

        pnd_New_Void_Match_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_New_Void_Match_Key__R_Field_3", "REDEFINE", pnd_New_Void_Match_Key);
        pnd_New_Void_Match_Key_Pnd_New_Match_Account = pnd_New_Void_Match_Key__R_Field_3.newFieldInGroup("pnd_New_Void_Match_Key_Pnd_New_Match_Account", 
            "#NEW-MATCH-ACCOUNT", FieldType.NUMERIC, 13);
        pnd_New_Void_Match_Key_Pnd_New_Match_Check_Num = pnd_New_Void_Match_Key__R_Field_3.newFieldInGroup("pnd_New_Void_Match_Key_Pnd_New_Match_Check_Num", 
            "#NEW-MATCH-CHECK-NUM", FieldType.NUMERIC, 11);
        pnd_Old_Void_Match_Key = localVariables.newFieldInRecord("pnd_Old_Void_Match_Key", "#OLD-VOID-MATCH-KEY", FieldType.STRING, 24);

        pnd_Old_Void_Match_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Old_Void_Match_Key__R_Field_4", "REDEFINE", pnd_Old_Void_Match_Key);
        pnd_Old_Void_Match_Key_Pnd_Old_Match_Account = pnd_Old_Void_Match_Key__R_Field_4.newFieldInGroup("pnd_Old_Void_Match_Key_Pnd_Old_Match_Account", 
            "#OLD-MATCH-ACCOUNT", FieldType.NUMERIC, 13);
        pnd_Old_Void_Match_Key_Pnd_Old_Match_Check_Num = pnd_Old_Void_Match_Key__R_Field_4.newFieldInGroup("pnd_Old_Void_Match_Key_Pnd_Old_Match_Check_Num", 
            "#OLD-MATCH-CHECK-NUM", FieldType.NUMERIC, 11);
        pnd_Last_New_Void_Key = localVariables.newFieldInRecord("pnd_Last_New_Void_Key", "#LAST-NEW-VOID-KEY", FieldType.STRING, 24);
        pnd_Last_Old_Void_Key = localVariables.newFieldInRecord("pnd_Last_Old_Void_Key", "#LAST-OLD-VOID-KEY", FieldType.STRING, 24);
        pnd_Old_In = localVariables.newFieldInRecord("pnd_Old_In", "#OLD-IN", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Unmatched = localVariables.newFieldInRecord("pnd_Old_Unmatched", "#OLD-UNMATCHED", FieldType.PACKED_DECIMAL, 7);
        pnd_Old_Matched = localVariables.newFieldInRecord("pnd_Old_Matched", "#OLD-MATCHED", FieldType.PACKED_DECIMAL, 7);
        pnd_New_In = localVariables.newFieldInRecord("pnd_New_In", "#NEW-IN", FieldType.PACKED_DECIMAL, 7);
        pnd_New_Out = localVariables.newFieldInRecord("pnd_New_Out", "#NEW-OUT", FieldType.PACKED_DECIMAL, 7);
        pnd_New_Dups = localVariables.newFieldInRecord("pnd_New_Dups", "#NEW-DUPS", FieldType.PACKED_DECIMAL, 7);
        pnd_New_Eof = localVariables.newFieldInRecord("pnd_New_Eof", "#NEW-EOF", FieldType.BOOLEAN, 1);
        pnd_Old_Eof = localVariables.newFieldInRecord("pnd_Old_Eof", "#OLD-EOF", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_New_Eof.setInitialValue(false);
        pnd_Old_Eof.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp256() throws Exception
    {
        super("Fcpp256");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 80 PS = 55;//Natural: FORMAT ( 1 ) LS = 80 PS = 55
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE TITLE LEFT *DATU 15X 'DUPLICATE VOID ELIMINATION PROCESS' 13X *PROGRAM / *TIME ( EM = X ( 8 ) ) 15X '----------------------------------' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU 15X 'DUPLICATE VOID ELIMINATION PROCESS' 13X *PROGRAM / *TIME ( EM = X ( 8 ) ) 15X '----------------------------------' //
        pnd_New_Void_Match_Key_Pnd_New_Match_Account.reset();                                                                                                             //Natural: RESET #NEW-MATCH-ACCOUNT #NEW-MATCH-CHECK-NUM #OLD-MATCH-ACCOUNT #OLD-MATCH-CHECK-NUM
        pnd_New_Void_Match_Key_Pnd_New_Match_Check_Num.reset();
        pnd_Old_Void_Match_Key_Pnd_Old_Match_Account.reset();
        pnd_Old_Void_Match_Key_Pnd_Old_Match_Check_Num.reset();
        //*  DO PRIMING READS
                                                                                                                                                                          //Natural: PERFORM #1000-READ-OLD-VOID
        sub_Pnd_1000_Read_Old_Void();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #2000-READ-NEW-VOID
        sub_Pnd_2000_Read_New_Void();
        if (condition(Global.isEscape())) {return;}
        RP1:                                                                                                                                                              //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_New_Eof.getBoolean() && pnd_Old_Eof.getBoolean())) {break;}                                                                                 //Natural: UNTIL #NEW-EOF AND #OLD-EOF
            if (condition(pnd_Old_Void_Match_Key.less(pnd_New_Void_Match_Key)))                                                                                           //Natural: IF #OLD-VOID-MATCH-KEY < #NEW-VOID-MATCH-KEY
            {
                pnd_Old_Unmatched.nadd(1);                                                                                                                                //Natural: ADD 1 TO #OLD-UNMATCHED
                                                                                                                                                                          //Natural: PERFORM #1000-READ-OLD-VOID
                sub_Pnd_1000_Read_Old_Void();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Old_Void_Match_Key.greater(pnd_New_Void_Match_Key)))                                                                                        //Natural: IF #OLD-VOID-MATCH-KEY > #NEW-VOID-MATCH-KEY
            {
                                                                                                                                                                          //Natural: PERFORM #3000-WRITE-NONDUP-NEW-VOID
                sub_Pnd_3000_Write_Nondup_New_Void();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #2000-READ-NEW-VOID
                sub_Pnd_2000_Read_New_Void();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Old_Void_Match_Key.equals(pnd_New_Void_Match_Key)))                                                                                         //Natural: IF #OLD-VOID-MATCH-KEY = #NEW-VOID-MATCH-KEY
            {
                if (condition(pnd_Old_Void_Match_Key.equals("999999999999999999999999")))                                                                                 //Natural: IF #OLD-VOID-MATCH-KEY = '999999999999999999999999'
                {
                    if (true) break RP1;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RP1. )
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM #4000-WRITE-DUP-REPORT
                sub_Pnd_4000_Write_Dup_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Old_Matched.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #OLD-MATCHED
                                                                                                                                                                          //Natural: PERFORM #1000-READ-OLD-VOID
                sub_Pnd_1000_Read_Old_Void();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #2000-READ-NEW-VOID
                sub_Pnd_2000_Read_New_Void();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  (RP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getWorkFiles().close(3);                                                                                                                                          //Natural: CLOSE WORK FILE 3
        getReports().write(0, " ",NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE ' ' //
        if (Global.isEscape()) return;
        getReports().write(0, new ColumnSpacing(33),"CONTROL TOTALS",NEWLINE,new ColumnSpacing(33),"--------------",NEWLINE,NEWLINE,new ColumnSpacing(24),"OLD VOID RECS READ.....",pnd_Old_In,  //Natural: WRITE 33X 'CONTROL TOTALS' / 33X '--------------' // 24X 'OLD VOID RECS READ.....' #OLD-IN ( EM = Z,ZZZ,ZZ9 ) / 24X 'NEW VOID RECS READ.....' #NEW-IN ( EM = Z,ZZZ,ZZ9 ) / 24X 'DUPLICATE VOID RECS....' #NEW-DUPS ( EM = Z,ZZZ,ZZ9 ) / 24X 'NEW VOID RECS WRITTEN..' #NEW-OUT ( EM = Z,ZZZ,ZZ9 ) / 24X 'OLD VOID MATCHED.......' #OLD-MATCHED ( EM = Z,ZZZ,ZZ9 ) / 24X 'OLD VOID UNMATCHED.....' #OLD-UNMATCHED ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(24),"NEW VOID RECS READ.....",pnd_New_In, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
            ColumnSpacing(24),"DUPLICATE VOID RECS....",pnd_New_Dups, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(24),"NEW VOID RECS WRITTEN..",pnd_New_Out, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(24),"OLD VOID MATCHED.......",pnd_Old_Matched, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
            ColumnSpacing(24),"OLD VOID UNMATCHED.....",pnd_Old_Unmatched, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        if (condition(pnd_New_Dups.equals(getZero())))                                                                                                                    //Natural: IF #NEW-DUPS = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(19),"***** NO DUPLICATE VOID RECORDS FOUND *****");                                   //Natural: WRITE ( 1 ) //// 19X '***** NO DUPLICATE VOID RECORDS FOUND *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.stop();  if (true) return;                                                                                                                                //Natural: STOP
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #1000-READ-OLD-VOID
        //* ************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #2000-READ-NEW-VOID
        //* ************************************
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #3000-WRITE-NONDUP-NEW-VOID
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #4000-WRITE-DUP-REPORT
    }
    private void sub_Pnd_1000_Read_Old_Void() throws Exception                                                                                                            //Natural: #1000-READ-OLD-VOID
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().read(1, pnd_Old_Void_Rec);                                                                                                                         //Natural: READ WORK FILE 1 ONCE RECORD #OLD-VOID-REC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Old_Eof.setValue(true);                                                                                                                                   //Natural: ASSIGN #OLD-EOF = TRUE
            pnd_Old_Void_Match_Key_Pnd_Old_Match_Account.setValue(9999999999999L);                                                                                        //Natural: ASSIGN #OLD-MATCH-ACCOUNT = 9999999999999
            pnd_Old_Void_Match_Key_Pnd_Old_Match_Check_Num.setValue(99999999999L);                                                                                        //Natural: ASSIGN #OLD-MATCH-CHECK-NUM = 99999999999
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Old_In.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #OLD-IN
        pnd_Old_Void_Match_Key_Pnd_Old_Match_Account.setValue(pnd_Old_Void_Rec_Pnd_Old_Account_Num);                                                                      //Natural: ASSIGN #OLD-MATCH-ACCOUNT = #OLD-ACCOUNT-NUM
        pnd_Old_Void_Match_Key_Pnd_Old_Match_Check_Num.setValue(pnd_Old_Void_Rec_Pnd_Old_Begin_Check_Num);                                                                //Natural: ASSIGN #OLD-MATCH-CHECK-NUM = #OLD-BEGIN-CHECK-NUM
        if (condition(pnd_Old_Void_Match_Key.lessOrEqual(pnd_Last_Old_Void_Key)))                                                                                         //Natural: IF #OLD-VOID-MATCH-KEY NOT > #LAST-OLD-VOID-KEY
        {
            getReports().write(0, new ColumnSpacing(19),"*****************************************",NEWLINE,new ColumnSpacing(19),"*** OLD VOID FILE IS OUT OF SEQUENCE. ***",NEWLINE,NEWLINE,new  //Natural: WRITE 19X '*****************************************' / 19X '*** OLD VOID FILE IS OUT OF SEQUENCE. ***' // 19X '*** CORRECT ERROR AND RESTART.        ***' // 19X '***' '=' *PROGRAM 5X // 19X '*** DATE:' *DATU // 19X '*** TIME:' *TIME // 19X '*** RECORD COUNT =' #OLD-IN // 19X '*** LAST REC KEY =' #LAST-OLD-VOID-KEY / 19X '*** THIS REC KEY =' #OLD-VOID-MATCH-KEY / 19X '*****************************************' /// 14X '***************************************************' / 14X '*** NOTE: THIS CAN ONLY HAPPEN IF THE SORT STEP ***' / 14X '***       THAT CREATES WORK FILE 1 HAS NOT BEEN ***' / 14X '***       EXECUTED SUCCESSFULLY.                ***' / 14X '***************************************************'
                ColumnSpacing(19),"*** CORRECT ERROR AND RESTART.        ***",NEWLINE,NEWLINE,new ColumnSpacing(19),"***","PROG: ",Global.getPROGRAM(),new 
                ColumnSpacing(5),NEWLINE,NEWLINE,new ColumnSpacing(19),"*** DATE:",Global.getDATU(),NEWLINE,NEWLINE,new ColumnSpacing(19),"*** TIME:",Global.getTIME(),NEWLINE,NEWLINE,new 
                ColumnSpacing(19),"*** RECORD COUNT =",pnd_Old_In,NEWLINE,NEWLINE,new ColumnSpacing(19),"*** LAST REC KEY =",pnd_Last_Old_Void_Key,NEWLINE,new 
                ColumnSpacing(19),"*** THIS REC KEY =",pnd_Old_Void_Match_Key,NEWLINE,new ColumnSpacing(19),"*****************************************",NEWLINE,NEWLINE,NEWLINE,new 
                ColumnSpacing(14),"***************************************************",NEWLINE,new ColumnSpacing(14),"*** NOTE: THIS CAN ONLY HAPPEN IF THE SORT STEP ***",NEWLINE,new 
                ColumnSpacing(14),"***       THAT CREATES WORK FILE 1 HAS NOT BEEN ***",NEWLINE,new ColumnSpacing(14),"***       EXECUTED SUCCESSFULLY.                ***",NEWLINE,new 
                ColumnSpacing(14),"***************************************************");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Last_Old_Void_Key.setValue(pnd_Old_Void_Match_Key);                                                                                                       //Natural: MOVE #OLD-VOID-MATCH-KEY TO #LAST-OLD-VOID-KEY
        }                                                                                                                                                                 //Natural: END-IF
        //*  (1340) #1000-READ-OLD-VOID */
    }
    private void sub_Pnd_2000_Read_New_Void() throws Exception                                                                                                            //Natural: #2000-READ-NEW-VOID
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().read(2, pnd_New_Void_Rec);                                                                                                                         //Natural: READ WORK FILE 2 ONCE RECORD #NEW-VOID-REC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_New_Eof.setValue(true);                                                                                                                                   //Natural: ASSIGN #NEW-EOF = TRUE
            pnd_New_Void_Match_Key_Pnd_New_Match_Account.setValue(9999999999999L);                                                                                        //Natural: ASSIGN #NEW-MATCH-ACCOUNT = 9999999999999
            pnd_New_Void_Match_Key_Pnd_New_Match_Check_Num.setValue(99999999999L);                                                                                        //Natural: ASSIGN #NEW-MATCH-CHECK-NUM = 99999999999
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_New_In.nadd(1);                                                                                                                                               //Natural: ADD 1 TO #NEW-IN
        pnd_New_Void_Match_Key_Pnd_New_Match_Account.setValue(pnd_New_Void_Rec_Pnd_New_Account_Num);                                                                      //Natural: ASSIGN #NEW-MATCH-ACCOUNT = #NEW-ACCOUNT-NUM
        pnd_New_Void_Match_Key_Pnd_New_Match_Check_Num.setValue(pnd_New_Void_Rec_Pnd_New_Begin_Check_Num);                                                                //Natural: ASSIGN #NEW-MATCH-CHECK-NUM = #NEW-BEGIN-CHECK-NUM
        if (condition(pnd_New_Void_Match_Key.lessOrEqual(pnd_Last_New_Void_Key)))                                                                                         //Natural: IF #NEW-VOID-MATCH-KEY NOT > #LAST-NEW-VOID-KEY
        {
            getReports().write(0, new ColumnSpacing(19),"*****************************************",NEWLINE,new ColumnSpacing(19),"*** NEW VOID FILE IS OUT OF SEQUENCE. ***",NEWLINE,NEWLINE,new  //Natural: WRITE 19X '*****************************************' / 19X '*** NEW VOID FILE IS OUT OF SEQUENCE. ***' // 19X '*** CORRECT ERROR AND RESTART.        ***' // 19X '***' '=' *PROGRAM 5X // 19X '*** DATE:' *DATU // 19X '*** TIME:' *TIME // 19X '*** RECORD COUNT =' #NEW-IN // 19X '*** LAST REC KEY =' #LAST-NEW-VOID-KEY / 19X '*** THIS REC KEY =' #NEW-VOID-MATCH-KEY / 19X '*****************************************' /// 14X '***************************************************' / 14X '*** NOTE: THIS CAN ONLY HAPPEN IF THE SORT STEP ***' / 14X '***       THAT CREATES WORK FILE 2 HAS NOT BEEN ***' / 14X '***       EXECUTED SUCCESSFULLY.                ***' / 14X '***************************************************'
                ColumnSpacing(19),"*** CORRECT ERROR AND RESTART.        ***",NEWLINE,NEWLINE,new ColumnSpacing(19),"***","PROG: ",Global.getPROGRAM(),new 
                ColumnSpacing(5),NEWLINE,NEWLINE,new ColumnSpacing(19),"*** DATE:",Global.getDATU(),NEWLINE,NEWLINE,new ColumnSpacing(19),"*** TIME:",Global.getTIME(),NEWLINE,NEWLINE,new 
                ColumnSpacing(19),"*** RECORD COUNT =",pnd_New_In,NEWLINE,NEWLINE,new ColumnSpacing(19),"*** LAST REC KEY =",pnd_Last_New_Void_Key,NEWLINE,new 
                ColumnSpacing(19),"*** THIS REC KEY =",pnd_New_Void_Match_Key,NEWLINE,new ColumnSpacing(19),"*****************************************",NEWLINE,NEWLINE,NEWLINE,new 
                ColumnSpacing(14),"***************************************************",NEWLINE,new ColumnSpacing(14),"*** NOTE: THIS CAN ONLY HAPPEN IF THE SORT STEP ***",NEWLINE,new 
                ColumnSpacing(14),"***       THAT CREATES WORK FILE 2 HAS NOT BEEN ***",NEWLINE,new ColumnSpacing(14),"***       EXECUTED SUCCESSFULLY.                ***",NEWLINE,new 
                ColumnSpacing(14),"***************************************************");
            if (Global.isEscape()) return;
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Last_New_Void_Key.setValue(pnd_New_Void_Match_Key);                                                                                                       //Natural: MOVE #NEW-VOID-MATCH-KEY TO #LAST-NEW-VOID-KEY
        }                                                                                                                                                                 //Natural: END-IF
        //*  (1720) #2000-READ-NEW-VOID */
    }
    private void sub_Pnd_3000_Write_Nondup_New_Void() throws Exception                                                                                                    //Natural: #3000-WRITE-NONDUP-NEW-VOID
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        getWorkFiles().write(3, false, pnd_New_Void_Rec);                                                                                                                 //Natural: WRITE WORK FILE 3 #NEW-VOID-REC
        pnd_New_Out.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #NEW-OUT
        //*  (2100) #3000-WRITE-NONDUP-NEW-VOID */
    }
    private void sub_Pnd_4000_Write_Dup_Report() throws Exception                                                                                                         //Natural: #4000-WRITE-DUP-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getReports().display(1, new ColumnSpacing(9),"ACCOUNT #",                                                                                                         //Natural: DISPLAY ( 1 ) 9X 'ACCOUNT #' #NEW-ACCOUNT-NUM 'CHECK #' #NEW-BEGIN-CHECK-NUM 'CUST ID' #CUSTOMER-ID-NUM-D 'STOP/TYPE' #STOP-TYPE 'NAME' #PAYEE-NAME 'USER' #USER-ID
        		pnd_New_Void_Rec_Pnd_New_Account_Num,"CHECK #",
        		pnd_New_Void_Rec_Pnd_New_Begin_Check_Num,"CUST ID",
        		pnd_New_Void_Rec_Pnd_Customer_Id_Num_D,"STOP/TYPE",
        		pnd_New_Void_Rec_Pnd_Stop_Type,"NAME",
        		pnd_New_Void_Rec_Pnd_Payee_Name,"USER",
        		pnd_New_Void_Rec_Pnd_User_Id);
        if (Global.isEscape()) return;
        pnd_New_Dups.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #NEW-DUPS
        //*  (2180) #4000-WRITE-DUP-REPORT */
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=80 PS=55");
        Global.format(1, "LS=80 PS=55");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(15),"DUPLICATE VOID ELIMINATION PROCESS",new 
            ColumnSpacing(13),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(15),"----------------------------------",
            NEWLINE,NEWLINE);
        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(15),"DUPLICATE VOID ELIMINATION PROCESS",new 
            ColumnSpacing(13),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(15),"----------------------------------",
            NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ColumnSpacing(9),"ACCOUNT #",
        		pnd_New_Void_Rec_Pnd_New_Account_Num,"CHECK #",
        		pnd_New_Void_Rec_Pnd_New_Begin_Check_Num,"CUST ID",
        		pnd_New_Void_Rec_Pnd_Customer_Id_Num_D,"STOP/TYPE",
        		pnd_New_Void_Rec_Pnd_Stop_Type,"NAME",
        		pnd_New_Void_Rec_Pnd_Payee_Name,"USER",
        		pnd_New_Void_Rec_Pnd_User_Id);
    }
}
