/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:14:55 PM
**        * FROM NATURAL PROGRAM : Fcpp120w
************************************************************
**        * FILE NAME            : Fcpp120w.java
**        * CLASS NAME           : Fcpp120w
**        * INSTANCE NAME        : Fcpp120w
************************************************************
***********************************************************************
* PROGRAM  : FCPP120W
*          : FCPP120Y
*
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* CREATED  : 01/15/2020 CTS CPS SUNSET (CHG694525)
* FUNCTION : THIS PROGRAM WILL READ  LEDGER EXTRACT FILE
*
* CHANGES  :
**********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp120w extends BLNatBase
{
    // Data Areas
    private PdaFcpapmnt pdaFcpapmnt;
    private LdaFcplpmna ldaFcplpmna;
    private LdaFcplldgr ldaFcplldgr;
    private LdaFcpl801d ldaFcpl801d;
    private LdaFcplpmnt ldaFcplpmnt;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pay;
    private DbsField arcpay;
    private DbsField pnd_Process;
    private DbsField pnd_Count;
    private DbsField pnd_Max;
    private DbsField pnd_Input_Date;
    private DbsField pnd_Ledger_Superde_Start;

    private DbsGroup pnd_Ledger_Superde_Start__R_Field_1;
    private DbsField pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Cntrct_Rcrd_Typ;
    private DbsField pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Filler;
    private DbsField pnd_Temp_Date_Alpha;

    private DbsGroup pnd_Temp_Date_Alpha__R_Field_2;
    private DbsField pnd_Temp_Date_Alpha_Pnd_Temp_Date;
    private DbsField pnd_C_Inv_Acct;
    private DbsField pnd_Ledger_Records_Read;
    private DbsField pnd_Ledger_Read;
    private DbsField pnd_Ledger_Written;
    private DbsField pnd_Paymnt_Written;
    private DbsField pnd_I;

    private DbsGroup pnd_Prev_Grp;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Pymnt_Check_Dte;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Pymnt_Acctg_Dte;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Cntrct_Payee_Cde;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Cntrct_Orgn_Cde;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Cntrct_Company_Cde;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Cntrct_Mode_Cde;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Ledger_Grp_2;
    private DbsField pnd_Prev_Grp_Pnd_Prev_Misc_Fields;
    private DbsField pnd_Temp_Date1;
    private DbsField pnd_Dup_Count;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_S__R_Field_3;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;
    private DbsField pnd_Trans_Ind_Parm;

    private DbsGroup pnd_Trans_Ind_Parm__R_Field_4;
    private DbsField pnd_Trans_Ind_Parm_Pnd_Trans_Ind;
    private DbsField pnd_Trans_Ind_Parm_Pnd_Trans_Fil;
    private DbsField pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date;
    private DbsField pnd_Text;
    private DbsField pnd_All;
    private DbsField pnd_Only_Nz;
    private DbsField pnd_System_Date_Ccyymmdd;

    private DbsGroup pnd_System_Date_Ccyymmdd__R_Field_5;
    private DbsField pnd_System_Date_Ccyymmdd_Pnd_System_Date_Ccyymmdd_A;
    private DbsField pnd_Nostate_Cnt;
    private DbsField pnd_Nostate;
    private DbsField pnd_Print_Chk_Dte;
    private DbsField pnd_Print_Stl_Dte;
    private DbsField pnd_Print_Act_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpapmnt = new PdaFcpapmnt(localVariables);
        ldaFcplpmna = new LdaFcplpmna();
        registerRecord(ldaFcplpmna);
        registerRecord(ldaFcplpmna.getVw_fcp_Arch_Pymnt());
        ldaFcplldgr = new LdaFcplldgr();
        registerRecord(ldaFcplldgr);
        registerRecord(ldaFcplldgr.getVw_fcp_Cons_Ledger());
        ldaFcpl801d = new LdaFcpl801d();
        registerRecord(ldaFcpl801d);
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());

        // Local Variables
        pay = localVariables.newFieldInRecord("pay", "PAY", FieldType.BOOLEAN, 1);
        arcpay = localVariables.newFieldInRecord("arcpay", "ARCPAY", FieldType.BOOLEAN, 1);
        pnd_Process = localVariables.newFieldInRecord("pnd_Process", "#PROCESS", FieldType.BOOLEAN, 1);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 10);
        pnd_Max = localVariables.newFieldInRecord("pnd_Max", "#MAX", FieldType.NUMERIC, 10);
        pnd_Input_Date = localVariables.newFieldInRecord("pnd_Input_Date", "#INPUT-DATE", FieldType.DATE);
        pnd_Ledger_Superde_Start = localVariables.newFieldInRecord("pnd_Ledger_Superde_Start", "#LEDGER-SUPERDE-START", FieldType.STRING, 6);

        pnd_Ledger_Superde_Start__R_Field_1 = localVariables.newGroupInRecord("pnd_Ledger_Superde_Start__R_Field_1", "REDEFINE", pnd_Ledger_Superde_Start);
        pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Cntrct_Rcrd_Typ = pnd_Ledger_Superde_Start__R_Field_1.newFieldInGroup("pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Cntrct_Rcrd_Typ", 
            "#S-LDGR-CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte = pnd_Ledger_Superde_Start__R_Field_1.newFieldInGroup("pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte", 
            "#S-LDGR-PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Filler = pnd_Ledger_Superde_Start__R_Field_1.newFieldInGroup("pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Filler", 
            "#S-LDGR-FILLER", FieldType.STRING, 1);
        pnd_Temp_Date_Alpha = localVariables.newFieldInRecord("pnd_Temp_Date_Alpha", "#TEMP-DATE-ALPHA", FieldType.STRING, 8);

        pnd_Temp_Date_Alpha__R_Field_2 = localVariables.newGroupInRecord("pnd_Temp_Date_Alpha__R_Field_2", "REDEFINE", pnd_Temp_Date_Alpha);
        pnd_Temp_Date_Alpha_Pnd_Temp_Date = pnd_Temp_Date_Alpha__R_Field_2.newFieldInGroup("pnd_Temp_Date_Alpha_Pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 
            8);
        pnd_C_Inv_Acct = localVariables.newFieldInRecord("pnd_C_Inv_Acct", "#C-INV-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ledger_Records_Read = localVariables.newFieldInRecord("pnd_Ledger_Records_Read", "#LEDGER-RECORDS-READ", FieldType.PACKED_DECIMAL, 14);
        pnd_Ledger_Read = localVariables.newFieldInRecord("pnd_Ledger_Read", "#LEDGER-READ", FieldType.PACKED_DECIMAL, 14);
        pnd_Ledger_Written = localVariables.newFieldInRecord("pnd_Ledger_Written", "#LEDGER-WRITTEN", FieldType.PACKED_DECIMAL, 14);
        pnd_Paymnt_Written = localVariables.newFieldInRecord("pnd_Paymnt_Written", "#PAYMNT-WRITTEN", FieldType.PACKED_DECIMAL, 14);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);

        pnd_Prev_Grp = localVariables.newGroupInRecord("pnd_Prev_Grp", "#PREV-GRP");
        pnd_Prev_Grp_Pnd_Prev_Cntrct_Ppcn_Nbr = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Cntrct_Ppcn_Nbr", "#PREV-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Prev_Grp_Pnd_Prev_Pymnt_Check_Dte = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Pymnt_Check_Dte", "#PREV-PYMNT-CHECK-DTE", FieldType.STRING, 
            8);
        pnd_Prev_Grp_Pnd_Prev_Pymnt_Settlmnt_Dte = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Pymnt_Settlmnt_Dte", "#PREV-PYMNT-SETTLMNT-DTE", 
            FieldType.STRING, 8);
        pnd_Prev_Grp_Pnd_Prev_Pymnt_Acctg_Dte = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Pymnt_Acctg_Dte", "#PREV-PYMNT-ACCTG-DTE", FieldType.STRING, 
            8);
        pnd_Prev_Grp_Pnd_Prev_Cntrct_Payee_Cde = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Cntrct_Payee_Cde", "#PREV-CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Prev_Grp_Pnd_Prev_Cntrct_Orgn_Cde = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Cntrct_Orgn_Cde", "#PREV-CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Prev_Grp_Pnd_Prev_Cntrct_Company_Cde = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Cntrct_Company_Cde", "#PREV-CNTRCT-COMPANY-CDE", 
            FieldType.STRING, 1);
        pnd_Prev_Grp_Pnd_Prev_Cntrct_Mode_Cde = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Cntrct_Mode_Cde", "#PREV-CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Prev_Grp_Pnd_Prev_Cntrct_Annty_Ins_Type = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Cntrct_Annty_Ins_Type", "#PREV-CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1);
        pnd_Prev_Grp_Pnd_Prev_Ledger_Grp_2 = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Ledger_Grp_2", "#PREV-LEDGER-GRP-2", FieldType.STRING, 
            51);
        pnd_Prev_Grp_Pnd_Prev_Misc_Fields = pnd_Prev_Grp.newFieldInGroup("pnd_Prev_Grp_Pnd_Prev_Misc_Fields", "#PREV-MISC-FIELDS", FieldType.STRING, 10);
        pnd_Temp_Date1 = localVariables.newFieldInRecord("pnd_Temp_Date1", "#TEMP-DATE1", FieldType.DATE);
        pnd_Dup_Count = localVariables.newFieldInRecord("pnd_Dup_Count", "#DUP-COUNT", FieldType.NUMERIC, 4);

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_S__R_Field_3 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_3", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_3.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 29);
        pnd_Trans_Ind_Parm = localVariables.newFieldInRecord("pnd_Trans_Ind_Parm", "#TRANS-IND-PARM", FieldType.STRING, 40);

        pnd_Trans_Ind_Parm__R_Field_4 = localVariables.newGroupInRecord("pnd_Trans_Ind_Parm__R_Field_4", "REDEFINE", pnd_Trans_Ind_Parm);
        pnd_Trans_Ind_Parm_Pnd_Trans_Ind = pnd_Trans_Ind_Parm__R_Field_4.newFieldInGroup("pnd_Trans_Ind_Parm_Pnd_Trans_Ind", "#TRANS-IND", FieldType.STRING, 
            2);
        pnd_Trans_Ind_Parm_Pnd_Trans_Fil = pnd_Trans_Ind_Parm__R_Field_4.newFieldInGroup("pnd_Trans_Ind_Parm_Pnd_Trans_Fil", "#TRANS-FIL", FieldType.STRING, 
            30);
        pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date = pnd_Trans_Ind_Parm__R_Field_4.newFieldInGroup("pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date", "#TRANS-TEST-DATE", 
            FieldType.STRING, 8);
        pnd_Text = localVariables.newFieldInRecord("pnd_Text", "#TEXT", FieldType.STRING, 30);
        pnd_All = localVariables.newFieldInRecord("pnd_All", "#ALL", FieldType.BOOLEAN, 1);
        pnd_Only_Nz = localVariables.newFieldInRecord("pnd_Only_Nz", "#ONLY-NZ", FieldType.BOOLEAN, 1);
        pnd_System_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_System_Date_Ccyymmdd", "#SYSTEM-DATE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_System_Date_Ccyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_System_Date_Ccyymmdd__R_Field_5", "REDEFINE", pnd_System_Date_Ccyymmdd);
        pnd_System_Date_Ccyymmdd_Pnd_System_Date_Ccyymmdd_A = pnd_System_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_System_Date_Ccyymmdd_Pnd_System_Date_Ccyymmdd_A", 
            "#SYSTEM-DATE-CCYYMMDD-A", FieldType.STRING, 8);
        pnd_Nostate_Cnt = localVariables.newFieldInRecord("pnd_Nostate_Cnt", "#NOSTATE-CNT", FieldType.NUMERIC, 6);
        pnd_Nostate = localVariables.newFieldInRecord("pnd_Nostate", "#NOSTATE", FieldType.BOOLEAN, 1);
        pnd_Print_Chk_Dte = localVariables.newFieldInRecord("pnd_Print_Chk_Dte", "#PRINT-CHK-DTE", FieldType.STRING, 10);
        pnd_Print_Stl_Dte = localVariables.newFieldInRecord("pnd_Print_Stl_Dte", "#PRINT-STL-DTE", FieldType.STRING, 10);
        pnd_Print_Act_Dte = localVariables.newFieldInRecord("pnd_Print_Act_Dte", "#PRINT-ACT-DTE", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmna.initializeValues();
        ldaFcplldgr.initializeValues();
        ldaFcpl801d.initializeValues();
        ldaFcplpmnt.initializeValues();

        localVariables.reset();
        pnd_Ledger_Superde_Start.setInitialValue("5");
        pnd_All.setInitialValue(false);
        pnd_Only_Nz.setInitialValue(false);
        pnd_Nostate.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp120w() throws Exception
    {
        super("Fcpp120w");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp120w|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                pnd_Max.setValue(10);                                                                                                                                     //Natural: MOVE 10 TO #MAX
                //* ********************************************************                                                                                              //Natural: FORMAT ( 00 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON
                //*  MAIN PROGRAM *
                //* ********************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Date, new FieldAttributes ("AD='_'"), new ReportEditMask ("YYYYMMDD"));                      //Natural: INPUT #INPUT-DATE ( AD = '_' EM = YYYYMMDD )
                if (condition(pnd_Input_Date.equals(getZero())))                                                                                                          //Natural: IF #INPUT-DATE = 0
                {
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"INPUT INTERFACE DATE WAS NOT SUPPLIED",new TabSetting(77),"***");                //Natural: WRITE ( 1 ) '***' 25T 'INPUT INTERFACE DATE WAS NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, pnd_Trans_Ind_Parm);                                                                                   //Natural: INPUT #TRANS-IND-PARM
                if (condition(pnd_Trans_Ind_Parm_Pnd_Trans_Ind.equals("  ")))                                                                                             //Natural: IF #TRANS-IND = '  '
                {
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"TRANSACTION INDICATOR NOT SUPPLIED",new TabSetting(77),"***");                   //Natural: WRITE ( 1 ) '***' 25T 'TRANSACTION INDICATOR NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Trans_Ind_Parm_Pnd_Trans_Ind.equals("NZ")))                                                                                             //Natural: IF #TRANS-IND = 'NZ'
                {
                    pnd_Text.setValue("NZ transactions only");                                                                                                            //Natural: ASSIGN #TEXT := 'NZ transactions only'
                    pnd_Only_Nz.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #ONLY-NZ
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Text.setValue("ALL transactions except NZ");                                                                                                      //Natural: ASSIGN #TEXT := 'ALL transactions except NZ'
                    pnd_All.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #ALL
                    //*  IN ORDER TO TEST THIS CONDITION A DATE WILL BE ADDED TO THE PARMCARD
                    //*  IN TEST ENVIRONMENT ONLY STARTING IN POSITION 33 THRU 40
                    if (condition(pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date.notEquals(" ")))                                                                                 //Natural: IF #TRANS-TEST-DATE NE ' '
                    {
                        pnd_Input_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date);                                             //Natural: MOVE EDITED #TRANS-TEST-DATE TO #INPUT-DATE ( EM = YYYYMMDD )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_System_Date_Ccyymmdd.setValue(Global.getDATN());                                                                                              //Natural: ASSIGN #SYSTEM-DATE-CCYYMMDD := *DATN
                        pnd_Input_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_System_Date_Ccyymmdd_Pnd_System_Date_Ccyymmdd_A);                                //Natural: MOVE EDITED #SYSTEM-DATE-CCYYMMDD-A TO #INPUT-DATE ( EM = YYYYMMDD )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "= ",pnd_Input_Date);                                                                                                               //Natural: WRITE '= ' #INPUT-DATE
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Trans_Ind_Parm_Pnd_Trans_Ind);                                                                                              //Natural: WRITE '=' #TRANS-IND
                if (Global.isEscape()) return;
                pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte.setValue(pnd_Input_Date);                                                                           //Natural: ASSIGN #S-LDGR-PYMNT-INTRFCE-DTE := #INPUT-DATE
                pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Filler.setValue("H'00'");                                                                                             //Natural: ASSIGN #S-LDGR-FILLER := H'00'
                pnd_Prev_Grp.reset();                                                                                                                                     //Natural: RESET #PREV-GRP
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 01 RECORD #LEDGER-EXT-2
                while (condition(getWorkFiles().read(1, ldaFcpl801d.getPnd_Ledger_Ext_2())))
                {
                    //*  EXTRACT ONLY NZ TRANSACTIONS
                    if (condition(pnd_Only_Nz.getBoolean()))                                                                                                              //Natural: IF #ONLY-NZ
                    {
                        if (condition(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Orgn_Cde().notEquals("NZ")))                                                                 //Natural: IF #LEDGER-EXT-2.CNTRCT-ORGN-CDE NE 'NZ'
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  EXTRACT ALL EXCEPT NZ TRANS
                    if (condition(pnd_All.getBoolean()))                                                                                                                  //Natural: IF #ALL
                    {
                        if (condition(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Orgn_Cde().equals("NZ")))                                                                    //Natural: IF #LEDGER-EXT-2.CNTRCT-ORGN-CDE = 'NZ'
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Ppcn_Nbr().equals(pnd_Prev_Grp_Pnd_Prev_Cntrct_Ppcn_Nbr) && ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Check_Dte().equals(pnd_Prev_Grp_Pnd_Prev_Pymnt_Check_Dte)  //Natural: IF #LEDGER-EXT-2.CNTRCT-PPCN-NBR = #PREV-CNTRCT-PPCN-NBR AND #LEDGER-EXT-2.PYMNT-CHECK-DTE = #PREV-PYMNT-CHECK-DTE AND #LEDGER-EXT-2.CNTRCT-ORGN-CDE = #PREV-CNTRCT-ORGN-CDE AND #LEDGER-EXT-2.CNTRCT-PAYEE-CDE = #PREV-CNTRCT-PAYEE-CDE AND #LEDGER-EXT-2.PYMNT-SETTLMNT-DTE = #PREV-PYMNT-SETTLMNT-DTE AND #LEDGER-EXT-2.PYMNT-ACCTG-DTE = #PREV-PYMNT-ACCTG-DTE AND #LEDGER-EXT-2.CNTRCT-COMPANY-CDE = #PREV-CNTRCT-COMPANY-CDE
                        && ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Orgn_Cde().equals(pnd_Prev_Grp_Pnd_Prev_Cntrct_Orgn_Cde) && ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Payee_Cde().equals(pnd_Prev_Grp_Pnd_Prev_Cntrct_Payee_Cde) 
                        && ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Settlmnt_Dte().equals(pnd_Prev_Grp_Pnd_Prev_Pymnt_Settlmnt_Dte) && ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Acctg_Dte().equals(pnd_Prev_Grp_Pnd_Prev_Pymnt_Acctg_Dte) 
                        && ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Company_Cde().equals(pnd_Prev_Grp_Pnd_Prev_Cntrct_Company_Cde)))
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ledger_Records_Read.nadd(1);                                                                                                                  //Natural: ADD 1 TO #LEDGER-RECORDS-READ
                        pnd_Prev_Grp_Pnd_Prev_Cntrct_Ppcn_Nbr.setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Ppcn_Nbr());                                                //Natural: MOVE #LEDGER-EXT-2.CNTRCT-PPCN-NBR TO #PREV-CNTRCT-PPCN-NBR
                        pnd_Prev_Grp_Pnd_Prev_Pymnt_Check_Dte.setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Check_Dte());                                                //Natural: MOVE #LEDGER-EXT-2.PYMNT-CHECK-DTE TO #PREV-PYMNT-CHECK-DTE
                        pnd_Prev_Grp_Pnd_Prev_Cntrct_Orgn_Cde.setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Orgn_Cde());                                                //Natural: MOVE #LEDGER-EXT-2.CNTRCT-ORGN-CDE TO #PREV-CNTRCT-ORGN-CDE
                        pnd_Prev_Grp_Pnd_Prev_Cntrct_Payee_Cde.setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Payee_Cde());                                              //Natural: MOVE #LEDGER-EXT-2.CNTRCT-PAYEE-CDE TO #PREV-CNTRCT-PAYEE-CDE
                        pnd_Prev_Grp_Pnd_Prev_Pymnt_Settlmnt_Dte.setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Settlmnt_Dte());                                          //Natural: MOVE #LEDGER-EXT-2.PYMNT-SETTLMNT-DTE TO #PREV-PYMNT-SETTLMNT-DTE
                        pnd_Prev_Grp_Pnd_Prev_Pymnt_Acctg_Dte.setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Acctg_Dte());                                                //Natural: MOVE #LEDGER-EXT-2.PYMNT-ACCTG-DTE TO #PREV-PYMNT-ACCTG-DTE
                        pnd_Prev_Grp_Pnd_Prev_Cntrct_Company_Cde.setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Company_Cde());                                          //Natural: MOVE #LEDGER-EXT-2.CNTRCT-COMPANY-CDE TO #PREV-CNTRCT-COMPANY-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Nostate.setValue(false);                                                                                                                          //Natural: MOVE FALSE TO #NOSTATE
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-LEDGER-EXTRACT
                    sub_Move_To_Ledger_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //*  ----------------------------------------------------------------------
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 01 ) 3 LINES
                getReports().write(1, ReportOption.NOTITLE,"Total payments with no state: ",pnd_Nostate_Cnt);                                                             //Natural: WRITE ( 01 ) 'Total payments with no state: ' #NOSTATE-CNT
                if (Global.isEscape()) return;
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 01 ) 3 LINES
                getReports().write(1, new FieldAttributes ("AD=I"),ReportOption.NOTITLE,new TabSetting(7),"EXTRACT FOR THE INTERFACE DATE :", new FieldAttributes         //Natural: WRITE ( 01 ) ( AD = I ) 7T 'EXTRACT FOR THE INTERFACE DATE :' ( AD = I ) #INPUT-DATE ( EM = MM/DD/YYYY ) / 7T 'EXTRACT FOR TRANSACTIONS       :' ( AD = I ) #TEXT / 7T 'NUMBER OF LEDGER RECORDS READ  :' ( AD = I ) #LEDGER-RECORDS-READ / 7T '    (ACTUAL LEDGERS PROCESSED) :' ( AD = I ) #LEDGER-READ / 7T 'LEDGER  EXTRACT RECORDS WRITTEN:' ( AD = I ) #LEDGER-WRITTEN /
                    ("AD=I"),pnd_Input_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(7),"EXTRACT FOR TRANSACTIONS       :", new FieldAttributes 
                    ("AD=I"),pnd_Text,NEWLINE,new TabSetting(7),"NUMBER OF LEDGER RECORDS READ  :", new FieldAttributes ("AD=I"),pnd_Ledger_Records_Read, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"    (ACTUAL LEDGERS PROCESSED) :", new FieldAttributes ("AD=I"),pnd_Ledger_Read, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"LEDGER  EXTRACT RECORDS WRITTEN:", new FieldAttributes ("AD=I"),pnd_Ledger_Written, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE);
                if (Global.isEscape()) return;
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 01 ) 3 LINES
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE);                    //Natural: WRITE ( 01 ) 7T '************ E N D   O F   T H E   R E P O R T   ************' /
                if (Global.isEscape()) return;
                //* *----------------------------
                //*  ---------------------------------
                //* *--------------------------------
                //* ******************
                //* *--------------------------------                                                                                                                     //Natural: AT TOP OF PAGE ( 1 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Move_To_Ledger_Extract() throws Exception                                                                                                            //Natural: MOVE-TO-LEDGER-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        short decideConditionsMet1035 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #LEDGER-EXT-2.ANNT-RSDNCY-CDE = MASK ( NN )
        if (condition(DbsUtil.maskMatches(ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Rsdncy_Cde(),"NN")))
        {
            decideConditionsMet1035++;
            ignore();
        }                                                                                                                                                                 //Natural: WHEN #LEDGER-EXT-2.ANNT-RSDNCY-CDE = '  '
        else if (condition(ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Rsdncy_Cde().equals("  ")))
        {
            decideConditionsMet1035++;
            pnd_Nostate.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO #NOSTATE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ledger_Read.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #LEDGER-READ
        if (condition(pnd_Nostate.getBoolean()))                                                                                                                          //Natural: IF #NOSTATE
        {
            pnd_Nostate_Cnt.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #NOSTATE-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ledger_Written.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #LEDGER-WRITTEN
        //* MOVE-TO-LEDGER-EXTRACT
    }
    //*  RS6
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        pnd_Temp_Date1.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Check_Dte());                                                  //Natural: MOVE EDITED #LEDGER-EXT-2.PYMNT-CHECK-DTE TO #TEMP-DATE1 ( EM = YYYYMMDD )
        pnd_Print_Chk_Dte.setValueEdited(pnd_Temp_Date1,new ReportEditMask("MM/DD/YYYY"));                                                                                //Natural: MOVE EDITED #TEMP-DATE1 ( EM = MM/DD/YYYY ) TO #PRINT-CHK-DTE
        pnd_Temp_Date1.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Acctg_Dte());                                                  //Natural: MOVE EDITED #LEDGER-EXT-2.PYMNT-ACCTG-DTE TO #TEMP-DATE1 ( EM = YYYYMMDD )
        pnd_Print_Act_Dte.setValueEdited(pnd_Temp_Date1,new ReportEditMask("MM/DD/YYYY"));                                                                                //Natural: MOVE EDITED #TEMP-DATE1 ( EM = MM/DD/YYYY ) TO #PRINT-ACT-DTE
        pnd_Temp_Date1.setValueEdited(new ReportEditMask("YYYYMMDD"),ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Settlmnt_Dte());                                               //Natural: MOVE EDITED #LEDGER-EXT-2.PYMNT-SETTLMNT-DTE TO #TEMP-DATE1 ( EM = YYYYMMDD )
        pnd_Print_Stl_Dte.setValueEdited(pnd_Temp_Date1,new ReportEditMask("MM/DD/YYYY"));                                                                                //Natural: MOVE EDITED #TEMP-DATE1 ( EM = MM/DD/YYYY ) TO #PRINT-STL-DTE
        getReports().write(1, ReportOption.NOTITLE,ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Ppcn_Nbr(),new ColumnSpacing(1),pnd_Print_Stl_Dte,new ColumnSpacing(1),pnd_Print_Chk_Dte,pnd_Print_Act_Dte,new  //Natural: WRITE ( 1 ) #LEDGER-EXT-2.CNTRCT-PPCN-NBR 1X #PRINT-STL-DTE 1X #PRINT-CHK-DTE #PRINT-ACT-DTE 1X #LEDGER-EXT-2.INV-ACCT-LEDGR-NBR 1X #LEDGER-EXT-2.CNTRCT-COMPANY-CDE 1X #LEDGER-EXT-2.INV-ACCT-LEDGR-AMT
            ColumnSpacing(1),ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Nbr(),new ColumnSpacing(1),ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Company_Cde(),new 
            ColumnSpacing(1),ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Amt());
        if (Global.isEscape()) return;
        //* WRITE-REPORT
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  WRITE (01) TITLE LEFT
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(53),"Consolidated Payment System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Consolidated Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'Ledger Reporting Extract' / 'DATE:' *DATU 38X 'Transactions Missing State Code' / 'Contract   Settlmnt     Check       Acctg    Ledger     Comp          Ledger   ' / ' Number      Date       Date        Date     Account    Code          Amount   ' / '-------------------------------------------------------------------------------' //
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(55),"Ledger Reporting Extract",NEWLINE,"DATE:",Global.getDATU(),new ColumnSpacing(38),"Transactions Missing State Code",
                        NEWLINE,"Contract   Settlmnt     Check       Acctg    Ledger     Comp          Ledger   ",NEWLINE," Number      Date       Date        Date     Account    Code          Amount   ",
                        NEWLINE,"-------------------------------------------------------------------------------",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
    }
}
