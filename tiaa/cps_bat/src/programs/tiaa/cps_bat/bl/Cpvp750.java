/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:25:27 PM
**        * FROM NATURAL PROGRAM : Cpvp750
************************************************************
**        * FILE NAME            : Cpvp750.java
**        * CLASS NAME           : Cpvp750
**        * INSTANCE NAME        : Cpvp750
************************************************************
************************************************************************
* CONSOLIDATED PAYMENT INQUIRY SYSTEM
*   PROGRAM TO UPDATE PAID CHECK FILE
*   FROM INPUT PC FILE TO CPS DATA BASE
*
*       PROGRAM NAME : CPVP750
*
*       DATE WRITTEN : 05/11/11
*
*       AUTHOR       : FAIZAL ALLIE
*                      CLONED FROM CPVP750
************************************************************************
* HISTORY
* 07/26/23  MB     EFT/WIRE REVERSALS
* 02/05/2013 J. OSTEEN - STORE ISN OF UPDATED RECORD ON REFERENCE TABLE
*                        CISNS FOR LATER FEED TO ODS. TAG JWO1
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpvp750 extends BLNatBase
{
    // Data Areas
    private PdaCpoa110 pdaCpoa110;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Check_Crrncy_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Crrncy_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind;
    private DbsField fcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind;
    private DbsField fcp_Cons_Pymnt_Pymnt_Stats_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Cmbne_Ind;
    private DbsField fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Type_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Lob_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Cref_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Option_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Mode_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Pymnt_Dest_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Hold_Cde;
    private DbsField fcp_Cons_Pymnt_Annt_Soc_Sec_Nbr;
    private DbsField fcp_Cons_Pymnt_Annt_Ctznshp_Cde;
    private DbsField fcp_Cons_Pymnt_Annt_Rsdncy_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Eft_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Amt;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Seq_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Annot_Ind;
    private DbsField fcp_Cons_Pymnt_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Reqst_Nbr;
    private DbsField fcp_Cons_Pymnt_Notused1;
    private DbsField positive_Pay_Rec;

    private DbsGroup positive_Pay_Rec__R_Field_1;

    private DbsGroup positive_Pay_Rec_Positive_Pay_Data;
    private DbsField positive_Pay_Rec_Pp_Cust_Id;
    private DbsField positive_Pay_Rec_Pp_Rec_Type;
    private DbsField positive_Pay_Rec_Pp_Bank_No;
    private DbsField positive_Pay_Rec_Pp_Acct_No;
    private DbsField positive_Pay_Rec_Pp_Date;

    private DbsGroup positive_Pay_Rec__R_Field_2;
    private DbsField positive_Pay_Rec_Pp_Yyyy;
    private DbsField positive_Pay_Rec_Pp_Mmdd;
    private DbsField positive_Pay_Rec_Pp_Time;
    private DbsField positive_Pay_Rec_Pp_Input;
    private DbsField positive_Pay_Rec_Pp_Stop;
    private DbsField positive_Pay_Rec_Pp_Chk_Nbr_B;

    private DbsGroup positive_Pay_Rec__R_Field_3;
    private DbsField positive_Pay_Rec_Pp_Chk_Nbr_Fill;
    private DbsField positive_Pay_Rec_Pp_Chk_Nbr_B_1;
    private DbsField positive_Pay_Rec_Pp_Chk_Nbr_E;

    private DbsGroup positive_Pay_Rec__R_Field_4;
    private DbsField positive_Pay_Rec_Pp_Chk_Nbr_E_Fill;
    private DbsField positive_Pay_Rec_Pp_Chk_Nbr_10;

    private DbsGroup positive_Pay_Rec__R_Field_5;
    private DbsField positive_Pay_Rec_Fill;
    private DbsField positive_Pay_Rec_Pp_Chk_Nbr_7;
    private DbsField positive_Pay_Rec_Pp_Check_Amt;

    private DbsGroup positive_Pay_Rec__R_Field_6;
    private DbsField positive_Pay_Rec_Pp_Check_Amt_1;
    private DbsField positive_Pay_Rec_Pp_Check_Amt_2;
    private DbsField positive_Pay_Rec_Pp_Iss_Date;
    private DbsField positive_Pay_Rec_Pp_Payee_Name;
    private DbsField positive_Pay_Rec_Pp_Reson;
    private DbsField positive_Pay_Rec_Pp_Auto_Chk;
    private DbsField positive_Pay_Rec_Pp_Stop_Dur;
    private DbsField positive_Pay_Rec_Pp_User_Id;
    private DbsField positive_Pay_Rec_Pp_Corr_Ind;

    private DbsGroup positive_Pay_Rec__R_Field_7;

    private DbsGroup positive_Pay_Rec_Positive_Pay_Trl;
    private DbsField positive_Pay_Rec_Pp_Trl_Cust_Id;
    private DbsField positive_Pay_Rec_Pp_Trl_Rec_Type;
    private DbsField positive_Pay_Rec_Pp_Trl_Fill;
    private DbsField positive_Pay_Rec_Pp_Trl_Amt;
    private DbsField pnd_Rec_Upd;
    private DbsField pnd_Rec_Rej;
    private DbsField pnd_Rec_Byp;
    private DbsField pnd_Rec_Stop;
    private DbsField pnd_Amt_Upd;
    private DbsField pnd_Amt_Byp;
    private DbsField pnd_Amt_Rej;
    private DbsField pnd_Amt_Stop;
    private DbsField pnd_Trl_Found;
    private DbsField pnd_Found;
    private DbsField pnd_Paid;
    private DbsField pnd_Isn;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Hdr_Date;
    private DbsField pnd_Paid_Date;

    private DbsGroup pnd_Paid_Date__R_Field_8;
    private DbsField pnd_Paid_Date_Pnd_Paid_Date_Yyyy;
    private DbsField pnd_Paid_Date_Pnd_Paid_Date_Mmdd;
    private DbsField pnd_Date;
    private DbsField pnd_Bank_Name;
    private DbsField pnd_Pp_Acct_No;

    private DbsGroup bank_Table;
    private DbsField bank_Table_Bank_Source_Code;
    private DbsField bank_Table_Bank_Routing;
    private DbsField bank_Table_Bank_Account;
    private DbsField bank_Table_Bank_Account_Name;
    private DbsField bank_Table_Bank_Account_Cde;
    private DbsField bank_Table_Bank_Account_Desc;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Count;
    private DbsField annot_Key;

    private DbsGroup annot_Key__R_Field_9;
    private DbsField annot_Key_Key_Ppcn_Nbr;
    private DbsField annot_Key_Key_Invrse_Dte;
    private DbsField annot_Key_Key_Orgn_Cde;
    private DbsField annot_Key_Key_Seq_Nbr;

    private DataAccessProgramView vw_annot;
    private DbsField annot_Cntrct_Rcrd_Typ;
    private DbsField annot_Cntrct_Ppcn_Nbr;
    private DbsField annot_Cntrct_Orgn_Cde;
    private DbsField annot_Cntrct_Invrse_Dte;
    private DbsField annot_Pymnt_Reqst_Nbr;
    private DbsField annot_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup annot__R_Field_10;
    private DbsField annot_Pymnt_Prcss_Seq_Num;
    private DbsField annot_Pymnt_Instmt_Nbr;
    private DbsField annot_Pymnt_Rmrk_Line1_Txt;
    private DbsField annot_Pymnt_Rmrk_Line2_Txt;
    private DbsField annot_Count_Castpymnt_Annot_Grp;

    private DbsGroup annot__R_Field_11;
    private DbsField annot_Cpnd_Pymnt_Annot_Grp;

    private DbsGroup annot_Pymnt_Annot_Grp;
    private DbsField annot_Pymnt_Annot_Dte;
    private DbsField annot_Pymnt_Annot_Id_Nbr;
    private DbsField annot_Pymnt_Annot_Flag_Ind;
    private DbsField annot_Cntrct_Hold_Tme;
    private DbsField annot_Count_Castpymnt_Annot_Expand_Grp;

    private DbsGroup annot__R_Field_12;
    private DbsField annot_Cpnd_Pymnt_Annot_Expand_Grp;

    private DbsGroup annot_Pymnt_Annot_Expand_Grp;
    private DbsField annot_Pymnt_Annot_Expand_Cmnt;
    private DbsField annot_Pymnt_Annot_Expand_Date;
    private DbsField annot_Pymnt_Annot_Expand_Time;
    private DbsField annot_Pymnt_Annot_Expand_Uid;
    private DbsField pnd_Cmnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpoa110 = new PdaCpoa110(localVariables);

        // Local Variables

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnt_Cntrct_Check_Crrncy_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        fcp_Cons_Pymnt_Cntrct_Crrncy_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        fcp_Cons_Pymnt_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_TYPE_IND");
        fcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_STTLMNT_TYPE_IND");
        fcp_Cons_Pymnt_Pymnt_Stats_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_STATS_CDE");
        fcp_Cons_Pymnt_Pymnt_Cmbne_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_CMBNE_IND");
        fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        fcp_Cons_Pymnt_Cntrct_Type_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        fcp_Cons_Pymnt_Cntrct_Lob_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_LOB_CDE");
        fcp_Cons_Pymnt_Cntrct_Cref_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_CREF_NBR");
        fcp_Cons_Pymnt_Cntrct_Option_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTION_CDE");
        fcp_Cons_Pymnt_Cntrct_Mode_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_CDE");
        fcp_Cons_Pymnt_Cntrct_Pymnt_Dest_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_DEST_CDE");
        fcp_Cons_Pymnt_Cntrct_Hold_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        fcp_Cons_Pymnt_Annt_Soc_Sec_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "ANNT_SOC_SEC_NBR");
        fcp_Cons_Pymnt_Annt_Ctznshp_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "ANNT_CTZNSHP_CDE");
        fcp_Cons_Pymnt_Annt_Rsdncy_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ANNT_RSDNCY_CDE");
        fcp_Cons_Pymnt_Pymnt_Eft_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_EFT_DTE");
        fcp_Cons_Pymnt_Pymnt_Check_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_DTE");
        fcp_Cons_Pymnt_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        fcp_Cons_Pymnt_Pymnt_Check_Amt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        fcp_Cons_Pymnt_Pymnt_Check_Seq_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SEQ_NBR");
        fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "CNTRCT_CMBN_NBR");
        fcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        fcp_Cons_Pymnt_Pymnt_Annot_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_ANNOT_IND");
        fcp_Cons_Pymnt_Cntrct_Invrse_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Pymnt_Pymnt_Reqst_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "PYMNT_REQST_NBR");
        fcp_Cons_Pymnt_Notused1 = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Notused1", "NOTUSED1", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NOTUSED1");
        registerRecord(vw_fcp_Cons_Pymnt);

        positive_Pay_Rec = localVariables.newFieldInRecord("positive_Pay_Rec", "POSITIVE-PAY-REC", FieldType.STRING, 126);

        positive_Pay_Rec__R_Field_1 = localVariables.newGroupInRecord("positive_Pay_Rec__R_Field_1", "REDEFINE", positive_Pay_Rec);

        positive_Pay_Rec_Positive_Pay_Data = positive_Pay_Rec__R_Field_1.newGroupInGroup("positive_Pay_Rec_Positive_Pay_Data", "POSITIVE-PAY-DATA");
        positive_Pay_Rec_Pp_Cust_Id = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Cust_Id", "PP-CUST-ID", FieldType.STRING, 
            8);
        positive_Pay_Rec_Pp_Rec_Type = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Rec_Type", "PP-REC-TYPE", FieldType.STRING, 
            1);
        positive_Pay_Rec_Pp_Bank_No = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Bank_No", "PP-BANK-NO", FieldType.STRING, 
            3);
        positive_Pay_Rec_Pp_Acct_No = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Acct_No", "PP-ACCT-NO", FieldType.STRING, 
            13);
        positive_Pay_Rec_Pp_Date = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Date", "PP-DATE", FieldType.NUMERIC, 8);

        positive_Pay_Rec__R_Field_2 = positive_Pay_Rec_Positive_Pay_Data.newGroupInGroup("positive_Pay_Rec__R_Field_2", "REDEFINE", positive_Pay_Rec_Pp_Date);
        positive_Pay_Rec_Pp_Yyyy = positive_Pay_Rec__R_Field_2.newFieldInGroup("positive_Pay_Rec_Pp_Yyyy", "PP-YYYY", FieldType.STRING, 4);
        positive_Pay_Rec_Pp_Mmdd = positive_Pay_Rec__R_Field_2.newFieldInGroup("positive_Pay_Rec_Pp_Mmdd", "PP-MMDD", FieldType.STRING, 4);
        positive_Pay_Rec_Pp_Time = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Time", "PP-TIME", FieldType.STRING, 6);
        positive_Pay_Rec_Pp_Input = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Input", "PP-INPUT", FieldType.STRING, 2);
        positive_Pay_Rec_Pp_Stop = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Stop", "PP-STOP", FieldType.STRING, 1);
        positive_Pay_Rec_Pp_Chk_Nbr_B = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Chk_Nbr_B", "PP-CHK-NBR-B", FieldType.STRING, 
            11);

        positive_Pay_Rec__R_Field_3 = positive_Pay_Rec_Positive_Pay_Data.newGroupInGroup("positive_Pay_Rec__R_Field_3", "REDEFINE", positive_Pay_Rec_Pp_Chk_Nbr_B);
        positive_Pay_Rec_Pp_Chk_Nbr_Fill = positive_Pay_Rec__R_Field_3.newFieldInGroup("positive_Pay_Rec_Pp_Chk_Nbr_Fill", "PP-CHK-NBR-FILL", FieldType.STRING, 
            1);
        positive_Pay_Rec_Pp_Chk_Nbr_B_1 = positive_Pay_Rec__R_Field_3.newFieldInGroup("positive_Pay_Rec_Pp_Chk_Nbr_B_1", "PP-CHK-NBR-B-1", FieldType.NUMERIC, 
            10);
        positive_Pay_Rec_Pp_Chk_Nbr_E = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Chk_Nbr_E", "PP-CHK-NBR-E", FieldType.STRING, 
            11);

        positive_Pay_Rec__R_Field_4 = positive_Pay_Rec_Positive_Pay_Data.newGroupInGroup("positive_Pay_Rec__R_Field_4", "REDEFINE", positive_Pay_Rec_Pp_Chk_Nbr_E);
        positive_Pay_Rec_Pp_Chk_Nbr_E_Fill = positive_Pay_Rec__R_Field_4.newFieldInGroup("positive_Pay_Rec_Pp_Chk_Nbr_E_Fill", "PP-CHK-NBR-E-FILL", FieldType.STRING, 
            1);
        positive_Pay_Rec_Pp_Chk_Nbr_10 = positive_Pay_Rec__R_Field_4.newFieldInGroup("positive_Pay_Rec_Pp_Chk_Nbr_10", "PP-CHK-NBR-10", FieldType.NUMERIC, 
            10);

        positive_Pay_Rec__R_Field_5 = positive_Pay_Rec__R_Field_4.newGroupInGroup("positive_Pay_Rec__R_Field_5", "REDEFINE", positive_Pay_Rec_Pp_Chk_Nbr_10);
        positive_Pay_Rec_Fill = positive_Pay_Rec__R_Field_5.newFieldInGroup("positive_Pay_Rec_Fill", "FILL", FieldType.NUMERIC, 3);
        positive_Pay_Rec_Pp_Chk_Nbr_7 = positive_Pay_Rec__R_Field_5.newFieldInGroup("positive_Pay_Rec_Pp_Chk_Nbr_7", "PP-CHK-NBR-7", FieldType.NUMERIC, 
            7);
        positive_Pay_Rec_Pp_Check_Amt = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Check_Amt", "PP-CHECK-AMT", FieldType.STRING, 
            15);

        positive_Pay_Rec__R_Field_6 = positive_Pay_Rec_Positive_Pay_Data.newGroupInGroup("positive_Pay_Rec__R_Field_6", "REDEFINE", positive_Pay_Rec_Pp_Check_Amt);
        positive_Pay_Rec_Pp_Check_Amt_1 = positive_Pay_Rec__R_Field_6.newFieldInGroup("positive_Pay_Rec_Pp_Check_Amt_1", "PP-CHECK-AMT-1", FieldType.NUMERIC, 
            4);
        positive_Pay_Rec_Pp_Check_Amt_2 = positive_Pay_Rec__R_Field_6.newFieldInGroup("positive_Pay_Rec_Pp_Check_Amt_2", "PP-CHECK-AMT-2", FieldType.NUMERIC, 
            11, 2);
        positive_Pay_Rec_Pp_Iss_Date = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Iss_Date", "PP-ISS-DATE", FieldType.STRING, 
            8);
        positive_Pay_Rec_Pp_Payee_Name = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Payee_Name", "PP-PAYEE-NAME", FieldType.STRING, 
            10);
        positive_Pay_Rec_Pp_Reson = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Reson", "PP-RESON", FieldType.STRING, 10);
        positive_Pay_Rec_Pp_Auto_Chk = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Auto_Chk", "PP-AUTO-CHK", FieldType.STRING, 
            1);
        positive_Pay_Rec_Pp_Stop_Dur = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Stop_Dur", "PP-STOP-DUR", FieldType.NUMERIC, 
            1);
        positive_Pay_Rec_Pp_User_Id = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_User_Id", "PP-USER-ID", FieldType.NUMERIC, 
            6);
        positive_Pay_Rec_Pp_Corr_Ind = positive_Pay_Rec_Positive_Pay_Data.newFieldInGroup("positive_Pay_Rec_Pp_Corr_Ind", "PP-CORR-IND", FieldType.STRING, 
            6);

        positive_Pay_Rec__R_Field_7 = localVariables.newGroupInRecord("positive_Pay_Rec__R_Field_7", "REDEFINE", positive_Pay_Rec);

        positive_Pay_Rec_Positive_Pay_Trl = positive_Pay_Rec__R_Field_7.newGroupInGroup("positive_Pay_Rec_Positive_Pay_Trl", "POSITIVE-PAY-TRL");
        positive_Pay_Rec_Pp_Trl_Cust_Id = positive_Pay_Rec_Positive_Pay_Trl.newFieldInGroup("positive_Pay_Rec_Pp_Trl_Cust_Id", "PP-TRL-CUST-ID", FieldType.STRING, 
            8);
        positive_Pay_Rec_Pp_Trl_Rec_Type = positive_Pay_Rec_Positive_Pay_Trl.newFieldInGroup("positive_Pay_Rec_Pp_Trl_Rec_Type", "PP-TRL-REC-TYPE", FieldType.STRING, 
            1);
        positive_Pay_Rec_Pp_Trl_Fill = positive_Pay_Rec_Positive_Pay_Trl.newFieldInGroup("positive_Pay_Rec_Pp_Trl_Fill", "PP-TRL-FILL", FieldType.STRING, 
            30);
        positive_Pay_Rec_Pp_Trl_Amt = positive_Pay_Rec_Positive_Pay_Trl.newFieldInGroup("positive_Pay_Rec_Pp_Trl_Amt", "PP-TRL-AMT", FieldType.NUMERIC, 
            7);
        pnd_Rec_Upd = localVariables.newFieldInRecord("pnd_Rec_Upd", "#REC-UPD", FieldType.NUMERIC, 7);
        pnd_Rec_Rej = localVariables.newFieldInRecord("pnd_Rec_Rej", "#REC-REJ", FieldType.NUMERIC, 7);
        pnd_Rec_Byp = localVariables.newFieldInRecord("pnd_Rec_Byp", "#REC-BYP", FieldType.NUMERIC, 7);
        pnd_Rec_Stop = localVariables.newFieldInRecord("pnd_Rec_Stop", "#REC-STOP", FieldType.NUMERIC, 7);
        pnd_Amt_Upd = localVariables.newFieldInRecord("pnd_Amt_Upd", "#AMT-UPD", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Byp = localVariables.newFieldInRecord("pnd_Amt_Byp", "#AMT-BYP", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Rej = localVariables.newFieldInRecord("pnd_Amt_Rej", "#AMT-REJ", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Stop = localVariables.newFieldInRecord("pnd_Amt_Stop", "#AMT-STOP", FieldType.NUMERIC, 12, 2);
        pnd_Trl_Found = localVariables.newFieldInRecord("pnd_Trl_Found", "#TRL-FOUND", FieldType.STRING, 1);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.STRING, 1);
        pnd_Paid = localVariables.newFieldInRecord("pnd_Paid", "#PAID", FieldType.STRING, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.NUMERIC, 11);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Hdr_Date = localVariables.newFieldInRecord("pnd_Hdr_Date", "#HDR-DATE", FieldType.DATE);
        pnd_Paid_Date = localVariables.newFieldInRecord("pnd_Paid_Date", "#PAID-DATE", FieldType.STRING, 8);

        pnd_Paid_Date__R_Field_8 = localVariables.newGroupInRecord("pnd_Paid_Date__R_Field_8", "REDEFINE", pnd_Paid_Date);
        pnd_Paid_Date_Pnd_Paid_Date_Yyyy = pnd_Paid_Date__R_Field_8.newFieldInGroup("pnd_Paid_Date_Pnd_Paid_Date_Yyyy", "#PAID-DATE-YYYY", FieldType.STRING, 
            4);
        pnd_Paid_Date_Pnd_Paid_Date_Mmdd = pnd_Paid_Date__R_Field_8.newFieldInGroup("pnd_Paid_Date_Pnd_Paid_Date_Mmdd", "#PAID-DATE-MMDD", FieldType.STRING, 
            4);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Bank_Name = localVariables.newFieldInRecord("pnd_Bank_Name", "#BANK-NAME", FieldType.STRING, 10);
        pnd_Pp_Acct_No = localVariables.newFieldInRecord("pnd_Pp_Acct_No", "#PP-ACCT-NO", FieldType.STRING, 13);

        bank_Table = localVariables.newGroupInRecord("bank_Table", "BANK-TABLE");
        bank_Table_Bank_Source_Code = bank_Table.newFieldArrayInGroup("bank_Table_Bank_Source_Code", "BANK-SOURCE-CODE", FieldType.STRING, 5, new DbsArrayController(1, 
            50));
        bank_Table_Bank_Routing = bank_Table.newFieldArrayInGroup("bank_Table_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9, new DbsArrayController(1, 
            50));
        bank_Table_Bank_Account = bank_Table.newFieldArrayInGroup("bank_Table_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21, new DbsArrayController(1, 
            50));
        bank_Table_Bank_Account_Name = bank_Table.newFieldArrayInGroup("bank_Table_Bank_Account_Name", "BANK-ACCOUNT-NAME", FieldType.STRING, 50, new 
            DbsArrayController(1, 50));
        bank_Table_Bank_Account_Cde = bank_Table.newFieldArrayInGroup("bank_Table_Bank_Account_Cde", "BANK-ACCOUNT-CDE", FieldType.STRING, 10, new DbsArrayController(1, 
            50));
        bank_Table_Bank_Account_Desc = bank_Table.newFieldArrayInGroup("bank_Table_Bank_Account_Desc", "BANK-ACCOUNT-DESC", FieldType.STRING, 50, new 
            DbsArrayController(1, 50));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 2);
        annot_Key = localVariables.newFieldInRecord("annot_Key", "ANNOT-KEY", FieldType.STRING, 29);

        annot_Key__R_Field_9 = localVariables.newGroupInRecord("annot_Key__R_Field_9", "REDEFINE", annot_Key);
        annot_Key_Key_Ppcn_Nbr = annot_Key__R_Field_9.newFieldInGroup("annot_Key_Key_Ppcn_Nbr", "KEY-PPCN-NBR", FieldType.STRING, 10);
        annot_Key_Key_Invrse_Dte = annot_Key__R_Field_9.newFieldInGroup("annot_Key_Key_Invrse_Dte", "KEY-INVRSE-DTE", FieldType.NUMERIC, 8);
        annot_Key_Key_Orgn_Cde = annot_Key__R_Field_9.newFieldInGroup("annot_Key_Key_Orgn_Cde", "KEY-ORGN-CDE", FieldType.STRING, 2);
        annot_Key_Key_Seq_Nbr = annot_Key__R_Field_9.newFieldInGroup("annot_Key_Key_Seq_Nbr", "KEY-SEQ-NBR", FieldType.NUMERIC, 9);

        vw_annot = new DataAccessProgramView(new NameInfo("vw_annot", "ANNOT"), "FCP_CONS_ANNOT", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ANNOT"));
        annot_Cntrct_Rcrd_Typ = vw_annot.getRecord().newFieldInGroup("annot_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_RCRD_TYP");
        annot_Cntrct_Ppcn_Nbr = vw_annot.getRecord().newFieldInGroup("annot_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        annot_Cntrct_Orgn_Cde = vw_annot.getRecord().newFieldInGroup("annot_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        annot_Cntrct_Invrse_Dte = vw_annot.getRecord().newFieldInGroup("annot_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        annot_Pymnt_Reqst_Nbr = vw_annot.getRecord().newFieldInGroup("annot_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        annot_Pymnt_Prcss_Seq_Nbr = vw_annot.getRecord().newFieldInGroup("annot_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_PRCSS_SEQ_NBR");

        annot__R_Field_10 = vw_annot.getRecord().newGroupInGroup("annot__R_Field_10", "REDEFINE", annot_Pymnt_Prcss_Seq_Nbr);
        annot_Pymnt_Prcss_Seq_Num = annot__R_Field_10.newFieldInGroup("annot_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        annot_Pymnt_Instmt_Nbr = annot__R_Field_10.newFieldInGroup("annot_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        annot_Pymnt_Rmrk_Line1_Txt = vw_annot.getRecord().newFieldInGroup("annot_Pymnt_Rmrk_Line1_Txt", "PYMNT-RMRK-LINE1-TXT", FieldType.STRING, 70, 
            RepeatingFieldStrategy.None, "PYMNT_RMRK_LINE1_TXT");
        annot_Pymnt_Rmrk_Line2_Txt = vw_annot.getRecord().newFieldInGroup("annot_Pymnt_Rmrk_Line2_Txt", "PYMNT-RMRK-LINE2-TXT", FieldType.STRING, 70, 
            RepeatingFieldStrategy.None, "PYMNT_RMRK_LINE2_TXT");
        annot_Count_Castpymnt_Annot_Grp = vw_annot.getRecord().newFieldInGroup("annot_Count_Castpymnt_Annot_Grp", "C*PYMNT-ANNOT-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");

        annot__R_Field_11 = vw_annot.getRecord().newGroupInGroup("annot__R_Field_11", "REDEFINE", annot_Count_Castpymnt_Annot_Grp);
        annot_Cpnd_Pymnt_Annot_Grp = annot__R_Field_11.newFieldInGroup("annot_Cpnd_Pymnt_Annot_Grp", "C#PYMNT-ANNOT-GRP", FieldType.NUMERIC, 3);

        annot_Pymnt_Annot_Grp = vw_annot.getRecord().newGroupInGroup("annot_Pymnt_Annot_Grp", "PYMNT-ANNOT-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        annot_Pymnt_Annot_Dte = annot_Pymnt_Annot_Grp.newFieldArrayInGroup("annot_Pymnt_Annot_Dte", "PYMNT-ANNOT-DTE", FieldType.DATE, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_DTE", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        annot_Pymnt_Annot_Id_Nbr = annot_Pymnt_Annot_Grp.newFieldArrayInGroup("annot_Pymnt_Annot_Id_Nbr", "PYMNT-ANNOT-ID-NBR", FieldType.STRING, 3, new 
            DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_ID_NBR", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        annot_Pymnt_Annot_Flag_Ind = annot_Pymnt_Annot_Grp.newFieldArrayInGroup("annot_Pymnt_Annot_Flag_Ind", "PYMNT-ANNOT-FLAG-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_FLAG_IND", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        annot_Cntrct_Hold_Tme = vw_annot.getRecord().newFieldInGroup("annot_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_TME");
        annot_Count_Castpymnt_Annot_Expand_Grp = vw_annot.getRecord().newFieldInGroup("annot_Count_Castpymnt_Annot_Expand_Grp", "C*PYMNT-ANNOT-EXPAND-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");

        annot__R_Field_12 = vw_annot.getRecord().newGroupInGroup("annot__R_Field_12", "REDEFINE", annot_Count_Castpymnt_Annot_Expand_Grp);
        annot_Cpnd_Pymnt_Annot_Expand_Grp = annot__R_Field_12.newFieldInGroup("annot_Cpnd_Pymnt_Annot_Expand_Grp", "C#PYMNT-ANNOT-EXPAND-GRP", FieldType.NUMERIC, 
            3);

        annot_Pymnt_Annot_Expand_Grp = vw_annot.getRecord().newGroupInGroup("annot_Pymnt_Annot_Expand_Grp", "PYMNT-ANNOT-EXPAND-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        annot_Pymnt_Annot_Expand_Cmnt = annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("annot_Pymnt_Annot_Expand_Cmnt", "PYMNT-ANNOT-EXPAND-CMNT", 
            FieldType.STRING, 50, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_CMNT", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        annot_Pymnt_Annot_Expand_Date = annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("annot_Pymnt_Annot_Expand_Date", "PYMNT-ANNOT-EXPAND-DATE", 
            FieldType.STRING, 8, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_DATE", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        annot_Pymnt_Annot_Expand_Time = annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("annot_Pymnt_Annot_Expand_Time", "PYMNT-ANNOT-EXPAND-TIME", 
            FieldType.STRING, 4, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_TIME", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        annot_Pymnt_Annot_Expand_Uid = annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("annot_Pymnt_Annot_Expand_Uid", "PYMNT-ANNOT-EXPAND-UID", FieldType.STRING, 
            8, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_UID", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        registerRecord(vw_annot);

        pnd_Cmnt = localVariables.newFieldInRecord("pnd_Cmnt", "#CMNT", FieldType.STRING, 50);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();
        vw_annot.reset();

        localVariables.reset();
        pnd_Bank_Name.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cpvp750() throws Exception
    {
        super("Cpvp750");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: FORMAT LS = 95 PS = 40;//Natural: FORMAT ( 1 ) LS = 80;//Natural: READ WORK FILE 1 POSITIVE-PAY-REC
        while (condition(getWorkFiles().read(1, positive_Pay_Rec)))
        {
            CheckAtStartofData305();

            pnd_Et_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ET-COUNT
            //*                                                                                                                                                           //Natural: AT START OF DATA
            if (condition(positive_Pay_Rec_Pp_Cust_Id.notEquals("00013241")))                                                                                             //Natural: IF PP-CUST-ID NOT = '00013241'
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-REC-RTN
                sub_Error_Rec_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(positive_Pay_Rec_Pp_Rec_Type.equals("D")))                                                                                                      //Natural: IF PP-REC-TYPE = 'D'
            {
                                                                                                                                                                          //Natural: PERFORM PAY-DATA-RTN
                sub_Pay_Data_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(positive_Pay_Rec_Pp_Rec_Type.equals("T")))                                                                                                  //Natural: IF PP-REC-TYPE = 'T'
                {
                                                                                                                                                                          //Natural: PERFORM PAY-TRL-RTN
                    sub_Pay_Trl_Rtn();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(32),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,  //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 32T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = ZZZ,ZZZ,ZZ9.99 ) 60T 'INVALID RECORD TYPE  = ' PP-REC-TYPE //
                        new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"INVALID RECORD TYPE  = ",positive_Pay_Rec_Pp_Rec_Type,NEWLINE,NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Rec_Rej.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-REJ
                    pnd_Amt_Rej.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                    //Natural: ADD PP-CHECK-AMT-2 TO #AMT-REJ
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Trl_Found.equals("Y")))                                                                                                                         //Natural: IF #TRL-FOUND = 'Y'
        {
            if (condition(positive_Pay_Rec_Pp_Trl_Amt.equals((pnd_Rec_Upd.add(pnd_Rec_Rej)))))                                                                            //Natural: IF PP-TRL-AMT = ( #REC-UPD + #REC-REJ )
            {
                getReports().write(0, new TabSetting(5),"CHECKS UPDATED:         ",pnd_Rec_Upd, new ReportEditMask ("ZZZZZ"),new TabSetting(51),pnd_Amt_Upd,              //Natural: WRITE 5T 'CHECKS UPDATED:         ' #REC-UPD ( EM = ZZZZZ ) 51T #AMT-UPD ( EM = ZZZ,ZZZ,ZZ9.99 ) // 5T 'CHECKS  REJECTED:       ' #REC-REJ ( EM = ZZZZZ ) 51T #AMT-REJ ( EM = ZZZ,ZZZ,ZZ9.99 ) // 5T 'CHECK FILE TRAILER:     ' PP-TRL-AMT ( EM = ZZZZZ ) //
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(5),"CHECKS  REJECTED:       ",pnd_Rec_Rej, new ReportEditMask ("ZZZZZ"),new 
                    TabSetting(51),pnd_Amt_Rej, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(5),"CHECK FILE TRAILER:     ",positive_Pay_Rec_Pp_Trl_Amt, 
                    new ReportEditMask ("ZZZZZ"),NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, new TabSetting(5),"CHECKS UPDATED:         ",pnd_Rec_Upd, new ReportEditMask ("ZZZZZ"),new TabSetting(51),pnd_Amt_Upd,              //Natural: WRITE 5T 'CHECKS UPDATED:         ' #REC-UPD ( EM = ZZZZZ ) 51T #AMT-UPD ( EM = ZZZ,ZZZ,ZZ9.99 ) // 5T 'CHECKS  REJECT:         ' #REC-REJ ( EM = ZZZZZ ) 51T #AMT-REJ ( EM = ZZZ,ZZZ,ZZ9.99 ) // 5T 'CHECK FILE TRAILER:     ' PP-TRL-AMT ( EM = ZZZZZ ) //
                    new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(5),"CHECKS  REJECT:         ",pnd_Rec_Rej, new ReportEditMask ("ZZZZZ"),new 
                    TabSetting(51),pnd_Amt_Rej, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(5),"CHECK FILE TRAILER:     ",positive_Pay_Rec_Pp_Trl_Amt, 
                    new ReportEditMask ("ZZZZZ"),NEWLINE,NEWLINE);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //* *ELSE
            //* *  WRITE
            //* *    5T '** NO TRAILER FOUND ON PAID CHECK FILE **'  //
            //* *    5T '** NO TRAILER FOUND ON PAID CHECK FILE **'  //
            //* *    5T 'CHECKS UPDATED:         '  #REC-UPD (EM=ZZZZZ)
            //* *    51T  #AMT-UPD (EM=ZZZ,ZZZ,ZZ9.99) //
            //* *    5T  'CHECKS  REJECT:         '  #REC-REJ (EM=ZZZZZ)
            //* *    51T  #AMT-REJ (EM=ZZZ,ZZZ,ZZ9.99)          //
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, new TabSetting(15),"---------------------------------------------",NEWLINE,new TabSetting(1),"TOTAL CHECKS:",new TabSetting(15),pnd_Rec_Upd,  //Natural: WRITE ( 1 ) 15T '---------------------------------------------' /1T 'TOTAL CHECKS:' 15T #REC-UPD ( EM = ZZZ9 ) 20T #AMT-UPD ( EM = ZZZ,ZZZ,ZZ9.99 ) //
            new ReportEditMask ("ZZZ9"),new TabSetting(20),pnd_Amt_Upd, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PAY-DATA-RTN
        //*     IF   PP-ACCT-NO = '2079950055011'          TMM 04/01/2005
        //*       OR PP-ACCT-NO = '2079950067025'          TMM 04/01/2005
        //* **************************** *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PAY-UPDATE
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-STATUS-CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PAY-TRL-RTN
        //* ***********************************************************************
        //* *
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-REC-RTN
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 001T *DATU '-' 012T *TIMX ( EM = HH':'IIAP ) 027T 'CONSOLIDATED PAYMENT SYSTEM ' 068T 'PAGE:' *PAGE-NUMBER ( 1 ) ( AD = L ) / 01T 'PCP1885D - ' *PROGRAM 029T 'STOPS AND CANCELS CONTROL REPORT FOR RECORDS UPDATED FOR' #DATE ( EM = MM/DD/YYYY ) / 1T 'ACCOUNT NUMBER:' #PP-ACCT-NO /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE TITLE LEFT / 001T *DATU '-' 012T *TIMX ( EM = HH':'IIAP ) 037T 'CONSOLIDATED PAYMENT SYSTEM ' 080T 'PAGE:' *PAGE-NUMBER ( AD = L ) / 01T 'PCP1885D - ' *PROGRAM 029T 'STOPS AND CANCELS CONTROL REPORT FOR ' #DATE ( EM = MM/DD/YYYY ) // 1T 'ACCOUNT NUMBER:' #PP-ACCT-NO //
        //*   005T  #BANK-RPT-HDNG /
        //* *** 34T 'COUNT' 52T 'AMOUNT' //
        //* * ---------------------------------------- TMM 04/01/2005 ------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-BANK-ACCOUNTS
        //*  ------------------------------------------- TMM 04/01/2005 -----------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LOAD-BANK-TABLE
        //*  --------------------------------------------- TMM 04/01/2005 ---------
        //*  --------------------------------------------- JWO 09/2012 ---------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ANNOTATE-PAYMENT
        //*  --------------------------------------------- JWO 09/2012 ---------
    }
    private void sub_Pay_Data_Rtn() throws Exception                                                                                                                      //Natural: PAY-DATA-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(positive_Pay_Rec_Pp_Cust_Id.equals("00013241")))                                                                                                    //Natural: IF PP-CUST-ID = '00013241'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(32),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,     //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 32T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = ZZZ,ZZZ,ZZ9.99 ) 60T 'INVALID CUSTOMER ID# =' PP-ACCT-NO /
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"INVALID CUSTOMER ID# =",positive_Pay_Rec_Pp_Acct_No,NEWLINE);
            if (Global.isEscape()) return;
            pnd_Rec_Rej.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-REJ
            pnd_Amt_Rej.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                            //Natural: ADD PP-CHECK-AMT-2 TO #AMT-REJ
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(positive_Pay_Rec_Pp_Bank_No.equals("075")))                                                                                                         //Natural: IF PP-BANK-NO = '075'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(30),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,     //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 30T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = ZZZ,ZZZ,ZZ9.99 ) 60T 'INVALID BANK# =' PP-BANK-NO /
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"INVALID BANK# =",positive_Pay_Rec_Pp_Bank_No,NEWLINE);
            if (Global.isEscape()) return;
            pnd_Rec_Rej.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-REJ
            pnd_Amt_Rej.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                            //Natural: ADD PP-CHECK-AMT-2 TO #AMT-REJ
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------------------------------------- TMM 04/01/2005 -------
        DbsUtil.examine(new ExamineSource(bank_Table_Bank_Account.getValue("*")), new ExamineSearch(positive_Pay_Rec_Pp_Acct_No), new ExamineGivingNumber(pnd_Count));    //Natural: EXAMINE BANK-TABLE.BANK-ACCOUNT ( * ) FOR PP-ACCT-NO GIVING #COUNT
        if (condition(pnd_Count.greater(getZero())))                                                                                                                      //Natural: IF #COUNT GT 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(30),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,     //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 30T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = ZZZ,ZZZ,ZZ9.99 ) 60T 'INVALID ACCT# =' PP-ACCT-NO /
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"INVALID ACCT# =",positive_Pay_Rec_Pp_Acct_No,NEWLINE);
            if (Global.isEscape()) return;
            pnd_Rec_Rej.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-REJ
            pnd_Amt_Rej.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                            //Natural: ADD PP-CHECK-AMT-2 TO #AMT-REJ
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(positive_Pay_Rec_Pp_Input.equals("BS")))                                                                                                            //Natural: IF PP-INPUT = 'BS'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(30),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,     //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 30T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = ZZZ,ZZZ,ZZ9.99 ) 60T 'INVALID INPUT TYPE =' PP-INPUT /
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"INVALID INPUT TYPE =",positive_Pay_Rec_Pp_Input,NEWLINE);
            if (Global.isEscape()) return;
            pnd_Rec_Rej.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-REJ
            pnd_Amt_Rej.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                            //Natural: ADD PP-CHECK-AMT-2 TO #AMT-REJ
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*     MB   7/26
        if (condition(positive_Pay_Rec_Pp_Stop.equals("V") || positive_Pay_Rec_Pp_Stop.equals("S") || positive_Pay_Rec_Pp_Stop.equals("R")))                              //Natural: IF POSITIVE-PAY-REC.PP-STOP = 'V' OR = 'S' OR = 'R'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(30),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,     //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 30T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = ZZZ,ZZZ,ZZ9.99 ) 60T 'INVALID STOP TYPE =' PP-STOP /
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"INVALID STOP TYPE =",positive_Pay_Rec_Pp_Stop,NEWLINE);
            if (Global.isEscape()) return;
            pnd_Rec_Rej.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-REJ
            pnd_Amt_Rej.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                            //Natural: ADD PP-CHECK-AMT-2 TO #AMT-REJ
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PAY-UPDATE
        sub_Pay_Update();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Found.notEquals("Y")))                                                                                                                          //Natural: IF #FOUND NOT = 'Y'
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        UPD1:                                                                                                                                                             //Natural: GET FCP-CONS-PYMNT #ISN
        vw_fcp_Cons_Pymnt.readByID(pnd_Isn.getLong(), "UPD1");
        //*     VOID
        //*     CANCEL
        //*  JWO 09/2012
        //*     STOP
        //*  JWO 09/2012
        //*    EFT REVERSAL      MB 7/26
        //*  JWO 09/2012
        short decideConditionsMet448 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE POSITIVE-PAY-REC.PP-STOP;//Natural: VALUE 'V'
        if (condition((positive_Pay_Rec_Pp_Stop.equals("V"))))
        {
            decideConditionsMet448++;
            fcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.setValue("C");                                                                                                   //Natural: ASSIGN FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE := 'C'
            pnd_Cmnt.setValue("PAYMENT CANCELED BY OMNIPRO ACTION");                                                                                                      //Natural: ASSIGN #CMNT := 'PAYMENT CANCELED BY OMNIPRO ACTION'
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((positive_Pay_Rec_Pp_Stop.equals("S"))))
        {
            decideConditionsMet448++;
            fcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.setValue("S");                                                                                                   //Natural: ASSIGN FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE := 'S'
            pnd_Cmnt.setValue("PAYMENT STOPPED BY OMNIPRO ACTION");                                                                                                       //Natural: ASSIGN #CMNT := 'PAYMENT STOPPED BY OMNIPRO ACTION'
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((positive_Pay_Rec_Pp_Stop.equals("R"))))
        {
            decideConditionsMet448++;
            fcp_Cons_Pymnt_Notused1.setValue("EFT REV");                                                                                                                  //Natural: ASSIGN FCP-CONS-PYMNT.NOTUSED1 := 'EFT REV'
            pnd_Cmnt.setValue("PAYMENT REVERSED BY OMNIPRO ACTION");                                                                                                      //Natural: ASSIGN #CMNT := 'PAYMENT REVERSED BY OMNIPRO ACTION'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
            //*  JWO 09/2012
        }                                                                                                                                                                 //Natural: END-DECIDE
        fcp_Cons_Pymnt_Pymnt_Annot_Ind.setValue("Y");                                                                                                                     //Natural: ASSIGN FCP-CONS-PYMNT.PYMNT-ANNOT-IND := 'Y'
        vw_fcp_Cons_Pymnt.updateDBRow("UPD1");                                                                                                                            //Natural: UPDATE ( UPD1. )
        //*  JWO1 02/2013
        DbsUtil.callnat(Cponisns.class , getCurrentProcessState(), pnd_Isn);                                                                                              //Natural: CALLNAT 'CPONISNS' #ISN
        if (condition(Global.isEscape())) return;
        //*  JWO 09/2012
                                                                                                                                                                          //Natural: PERFORM ANNOTATE-PAYMENT
        sub_Annotate_Payment();
        if (condition(Global.isEscape())) {return;}
        pnd_Rec_Upd.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #REC-UPD
        pnd_Amt_Upd.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                                //Natural: ADD PP-CHECK-AMT-2 TO #AMT-UPD
        getReports().display(1, "      ",                                                                                                                                 //Natural: DISPLAY ( 1 ) '      ' 'CHECK NUMBER' PP-CHK-NBR-10 'CHECK AMOUNT' PP-CHECK-AMT-2 ( EM = ZZZ,ZZZ,ZZ9.99 ) 'STOP/CANCEL/IND' PP-STOP 'ORGIN CODE' CNTRCT-ORGN-CDE /
        		"CHECK NUMBER",
        		positive_Pay_Rec_Pp_Chk_Nbr_10,"CHECK AMOUNT",
        		positive_Pay_Rec_Pp_Check_Amt_2, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"STOP/CANCEL/IND",
        		positive_Pay_Rec_Pp_Stop,"ORGIN CODE",
        		fcp_Cons_Pymnt_Cntrct_Orgn_Cde,NEWLINE);
        if (Global.isEscape()) return;
        if (condition(pnd_Et_Count.greater(99)))                                                                                                                          //Natural: IF #ET-COUNT > 99
        {
            pnd_Et_Count.reset();                                                                                                                                         //Natural: RESET #ET-COUNT
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
        //*  PAY-DATA-RTN
    }
    private void sub_Pay_Update() throws Exception                                                                                                                        //Natural: PAY-UPDATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Found.reset();                                                                                                                                                //Natural: RESET #FOUND
        vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                               //Natural: FIND FCP-CONS-PYMNT WITH PYMNT-NBR = PP-CHK-NBR-10 WHERE FCP-CONS-PYMNT.PYMNT-CHECK-AMT = PP-CHECK-AMT-2
        (
        "FIND01",
        new Wc[] { new Wc("PYMNT_NBR", "=", positive_Pay_Rec_Pp_Chk_Nbr_10, WcType.WITH) ,
        new Wc("PYMNT_CHECK_AMT", "=", positive_Pay_Rec_Pp_Check_Amt_2, WcType.WHERE) }
        );
        FIND01:
        while (condition(vw_fcp_Cons_Pymnt.readNextRow("FIND01", true)))
        {
            vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
            if (condition(vw_fcp_Cons_Pymnt.getAstCOUNTER().equals(0)))                                                                                                   //Natural: IF NO RECORDS FOUND
            {
                vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                       //Natural: FIND FCP-CONS-PYMNT WITH PYMNT-CHECK-NBR = PP-CHK-NBR-7 WHERE FCP-CONS-PYMNT.PYMNT-CHECK-AMT = PP-CHECK-AMT-2
                (
                "FIND02",
                new Wc[] { new Wc("PYMNT_CHECK_NBR", "=", positive_Pay_Rec_Pp_Chk_Nbr_7, WcType.WITH) ,
                new Wc("PYMNT_CHECK_AMT", "=", positive_Pay_Rec_Pp_Check_Amt_2, WcType.WHERE) }
                );
                FIND02:
                while (condition(vw_fcp_Cons_Pymnt.readNextRow("FIND02", true)))
                {
                    vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
                    if (condition(vw_fcp_Cons_Pymnt.getAstCOUNTER().equals(0)))                                                                                           //Natural: IF NO RECORDS FOUND
                    {
                        pnd_Rec_Rej.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-REJ
                        pnd_Amt_Rej.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                //Natural: ADD PP-CHECK-AMT-2 TO #AMT-REJ
                        getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(30),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,  //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 30T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = ZZZ,ZZZ,ZZ9.99 ) 60T 'NO RECORD FOUND  ' /
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"NO RECORD FOUND  ",NEWLINE);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM VALIDATE-STATUS-CODE
                    sub_Validate_Status_Code();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pnd_Found.notEquals("Y")))                                                                                                              //Natural: IF #FOUND NOT = 'Y'
                    {
                        if (true) return;                                                                                                                                 //Natural: ESCAPE ROUTINE
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Isn.setValue(vw_fcp_Cons_Pymnt.getAstISN("Find02"));                                                                                              //Natural: ASSIGN #ISN := *ISN
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-NOREC
                                                                                                                                                                          //Natural: PERFORM VALIDATE-STATUS-CODE
            sub_Validate_Status_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Found.notEquals("Y")))                                                                                                                      //Natural: IF #FOUND NOT = 'Y'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn.setValue(vw_fcp_Cons_Pymnt.getAstISN("Find01"));                                                                                                      //Natural: ASSIGN #ISN := *ISN
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  PAY-DATA-RTN
    }
    private void sub_Validate_Status_Code() throws Exception                                                                                                              //Natural: VALIDATE-STATUS-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        if (condition(fcp_Cons_Pymnt_Pymnt_Stats_Cde.equals("P")))                                                                                                        //Natural: IF FCP-CONS-PYMNT.PYMNT-STATS-CDE = 'P'
        {
            pnd_Found.setValue("Y");                                                                                                                                      //Natural: ASSIGN #FOUND := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rec_Rej.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-REJ
            getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(30),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,     //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 30T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = Z,ZZZ,ZZ9.99 ) 60T 'STATS-CDE = ' PYMNT-STATS-CDE /
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(60),"STATS-CDE = ",fcp_Cons_Pymnt_Pymnt_Stats_Cde,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(fcp_Cons_Pymnt_Notused1.equals(" ")))                                                                                                               //Natural: IF FCP-CONS-PYMNT.NOTUSED1 = ' '
        {
            pnd_Found.setValue("Y");                                                                                                                                      //Natural: ASSIGN #FOUND := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Found.reset();                                                                                                                                            //Natural: RESET #FOUND
            pnd_Rec_Rej.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-REJ
            getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(30),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,     //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 30T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = Z,ZZZ,ZZ9.99 ) 60T 'PREVIOUSLY REJECTED' /
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(60),"PREVIOUSLY REJECTED",NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Pay_Trl_Rtn() throws Exception                                                                                                                       //Natural: PAY-TRL-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Trl_Found.setValue("Y");                                                                                                                                      //Natural: ASSIGN #TRL-FOUND := 'Y'
        if (condition(pnd_Et_Count.equals(1)))                                                                                                                            //Natural: IF #ET-COUNT = 1
        {
            getReports().write(0, new TabSetting(5),"** FILE HAS ONLY TRAILER RECORD  **",NEWLINE,new TabSetting(5),"** FILE HAS ONLY TRAILER RECORD  **",NEWLINE,new     //Natural: WRITE 5T '** FILE HAS ONLY TRAILER RECORD  **' / 5T '** FILE HAS ONLY TRAILER RECORD  **' / 5T '** FILE HAS ONLY TRAILER RECORD  **' //
                TabSetting(5),"** FILE HAS ONLY TRAILER RECORD  **",NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            DbsUtil.terminate();  if (true) return;                                                                                                                       //Natural: TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Rec_Rtn() throws Exception                                                                                                                     //Natural: ERROR-REC-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *
        if (condition(positive_Pay_Rec_Pp_Rec_Type.equals("T")))                                                                                                          //Natural: IF PP-REC-TYPE = 'T'
        {
            pnd_Trl_Found.reset();                                                                                                                                        //Natural: RESET #TRL-FOUND
            getReports().write(0, NEWLINE,NEWLINE,new TabSetting(5),"*** INVALID CUSTOMER ID# FOR TRAILER RECORD *** =",positive_Pay_Rec_Pp_Acct_No,NEWLINE,              //Natural: WRITE // 5T '*** INVALID CUSTOMER ID# FOR TRAILER RECORD *** =' PP-ACCT-NO //
                NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, new TabSetting(2),"CHECK NUMBER =",positive_Pay_Rec_Pp_Chk_Nbr_10,new TabSetting(30),"CHECK AMT =",positive_Pay_Rec_Pp_Check_Amt_2,     //Natural: WRITE 2T 'CHECK NUMBER =' PP-CHK-NBR-10 30T 'CHECK AMT =' PP-CHECK-AMT-2 ( EM = Z,ZZZ,ZZ9.99 ) 60T 'INVALID CUSTOMER ID# =' PP-ACCT-NO //
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(60),"INVALID CUSTOMER ID# =",positive_Pay_Rec_Pp_Acct_No,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            pnd_Rec_Rej.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-REJ
            pnd_Amt_Rej.nadd(positive_Pay_Rec_Pp_Check_Amt_2);                                                                                                            //Natural: ADD PP-CHECK-AMT-2 TO #AMT-REJ
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        //*  ERROR-REC-RTN
    }
    private void sub_Load_Bank_Accounts() throws Exception                                                                                                                //Natural: LOAD-BANK-ACCOUNTS
    {
        if (BLNatReinput.isReinput()) return;

        pdaCpoa110.getCpoa110_Cpoa110_Return_Code().setValue("00");                                                                                                       //Natural: MOVE '00' TO CPOA110-RETURN-CODE
        pdaCpoa110.getCpoa110_Cpoa110_Source_Code().setValue("A    ");                                                                                                    //Natural: ASSIGN CPOA110-SOURCE-CODE := 'A    '
        pdaCpoa110.getCpoa110_Cpoa110_Function().setValue("NEXT");                                                                                                        //Natural: ASSIGN CPOA110-FUNCTION := 'NEXT'
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pdaCpoa110.getCpoa110_Cpoa110_Return_Code().notEquals("00"))) {break;}                                                                          //Natural: UNTIL CPOA110-RETURN-CODE NE '00'
            DbsUtil.callnat(Cpon110.class , getCurrentProcessState(), pdaCpoa110.getCpoa110());                                                                           //Natural: CALLNAT 'CPON110' CPOA110
            if (condition(Global.isEscape())) return;
            pdaCpoa110.getCpoa110_Cpoa110_Source_Code().setValue(pdaCpoa110.getCpoa110_Bank_Source_Code());                                                               //Natural: ASSIGN CPOA110-SOURCE-CODE := CPOA110.BANK-SOURCE-CODE
            //* * LIMIT TO OMNI/TOPS ONLY
            if (condition(pdaCpoa110.getCpoa110_Rt_Table_Id().equals("CLOGO") && pdaCpoa110.getCpoa110_Cpoa110_Return_Code().equals("00") && (pdaCpoa110.getCpoa110_Bank_Account_Cde().equals("OMNI")  //Natural: IF CPOA110.RT-TABLE-ID EQ 'CLOGO' AND CPOA110-RETURN-CODE EQ '00' AND ( CPOA110.BANK-ACCOUNT-CDE EQ 'OMNI' OR CPOA110.RT-SHORT-KEY EQ 'TOPS' )
                || pdaCpoa110.getCpoa110_Rt_Short_Key().equals("TOPS"))))
            {
                                                                                                                                                                          //Natural: PERFORM LOAD-BANK-TABLE
                sub_Load_Bank_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Load_Bank_Table() throws Exception                                                                                                                   //Natural: LOAD-BANK-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_I.nadd(1);                                                                                                                                                    //Natural: ADD 1 TO #I
        bank_Table_Bank_Source_Code.getValue(pnd_I).setValue(pdaCpoa110.getCpoa110_Bank_Source_Code());                                                                   //Natural: ASSIGN BANK-TABLE.BANK-SOURCE-CODE ( #I ) := CPOA110.BANK-SOURCE-CODE
        bank_Table_Bank_Routing.getValue(pnd_I).setValue(pdaCpoa110.getCpoa110_Bank_Routing());                                                                           //Natural: ASSIGN BANK-TABLE.BANK-ROUTING ( #I ) := CPOA110.BANK-ROUTING
        bank_Table_Bank_Account.getValue(pnd_I).setValue(pdaCpoa110.getCpoa110_Bank_Account());                                                                           //Natural: ASSIGN BANK-TABLE.BANK-ACCOUNT ( #I ) := CPOA110.BANK-ACCOUNT
        bank_Table_Bank_Account_Name.getValue(pnd_I).setValue(pdaCpoa110.getCpoa110_Bank_Account_Name());                                                                 //Natural: ASSIGN BANK-TABLE.BANK-ACCOUNT-NAME ( #I ) := CPOA110.BANK-ACCOUNT-NAME
        bank_Table_Bank_Account_Cde.getValue(pnd_I).setValue(pdaCpoa110.getCpoa110_Bank_Account_Cde());                                                                   //Natural: ASSIGN BANK-TABLE.BANK-ACCOUNT-CDE ( #I ) := CPOA110.BANK-ACCOUNT-CDE
        bank_Table_Bank_Account_Desc.getValue(pnd_I).setValue(pdaCpoa110.getCpoa110_Bank_Account_Desc());                                                                 //Natural: ASSIGN BANK-TABLE.BANK-ACCOUNT-DESC ( #I ) := CPOA110.BANK-ACCOUNT-DESC
    }
    private void sub_Annotate_Payment() throws Exception                                                                                                                  //Natural: ANNOTATE-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        annot_Key_Key_Ppcn_Nbr.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                                                  //Natural: ASSIGN KEY-PPCN-NBR := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        annot_Key_Key_Invrse_Dte.setValue(fcp_Cons_Pymnt_Cntrct_Invrse_Dte);                                                                                              //Natural: ASSIGN KEY-INVRSE-DTE := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
        annot_Key_Key_Orgn_Cde.setValue(fcp_Cons_Pymnt_Cntrct_Orgn_Cde);                                                                                                  //Natural: ASSIGN KEY-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        annot_Key_Key_Seq_Nbr.setValue(fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr);                                                                                               //Natural: ASSIGN KEY-SEQ-NBR := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NBR
        vw_annot.startDatabaseFind                                                                                                                                        //Natural: FIND ( 1 ) ANNOT WITH PPCN-INV-ORGN-PRCSS-INST = ANNOT-KEY WHERE CNTRCT-RCRD-TYP = '4'
        (
        "FD1",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", "=", annot_Key, WcType.WITH) ,
        new Wc("CNTRCT_RCRD_TYP", "=", "4", WcType.WHERE) },
        1
        );
        FD1:
        while (condition(vw_annot.readNextRow("FD1", true)))
        {
            vw_annot.setIfNotFoundControlFlag(false);
            if (condition(vw_annot.getAstCOUNTER().equals(0)))                                                                                                            //Natural: IF NO RECORDS FOUND
            {
                annot_Cntrct_Rcrd_Typ.setValue("4");                                                                                                                      //Natural: ASSIGN ANNOT.CNTRCT-RCRD-TYP := '4'
                annot_Cntrct_Ppcn_Nbr.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                                           //Natural: ASSIGN ANNOT.CNTRCT-PPCN-NBR := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
                annot_Cntrct_Invrse_Dte.setValue(fcp_Cons_Pymnt_Cntrct_Invrse_Dte);                                                                                       //Natural: ASSIGN ANNOT.CNTRCT-INVRSE-DTE := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
                annot_Pymnt_Prcss_Seq_Nbr.setValue(fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr);                                                                                   //Natural: ASSIGN ANNOT.PYMNT-PRCSS-SEQ-NBR := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NBR
                annot_Cntrct_Orgn_Cde.setValue(fcp_Cons_Pymnt_Cntrct_Orgn_Cde);                                                                                           //Natural: ASSIGN ANNOT.CNTRCT-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
                annot_Pymnt_Reqst_Nbr.setValue(fcp_Cons_Pymnt_Pymnt_Reqst_Nbr);                                                                                           //Natural: ASSIGN ANNOT.PYMNT-REQST-NBR := FCP-CONS-PYMNT.PYMNT-REQST-NBR
                annot_Pymnt_Annot_Dte.getValue(7).setValue(Global.getDATX());                                                                                             //Natural: MOVE *DATX TO ANNOT.PYMNT-ANNOT-DTE ( 7 )
                annot_Pymnt_Annot_Id_Nbr.getValue(7).setValue("SYS");                                                                                                     //Natural: MOVE 'SYS' TO ANNOT.PYMNT-ANNOT-ID-NBR ( 7 )
                annot_Pymnt_Annot_Flag_Ind.getValue(7).setValue("*");                                                                                                     //Natural: MOVE '*' TO ANNOT.PYMNT-ANNOT-FLAG-IND ( 7 )
                annot_Pymnt_Annot_Expand_Cmnt.getValue(1).setValue(pnd_Cmnt);                                                                                             //Natural: MOVE #CMNT TO ANNOT.PYMNT-ANNOT-EXPAND-CMNT ( 1 )
                annot_Pymnt_Annot_Expand_Date.getValue(1).setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                                //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO PYMNT-ANNOT-EXPAND-DATE ( 1 )
                annot_Pymnt_Annot_Expand_Time.getValue(1).setValueEdited(Global.getTIMX(),new ReportEditMask("HHII"));                                                    //Natural: MOVE EDITED *TIMX ( EM = HHII ) TO PYMNT-ANNOT-EXPAND-TIME ( 1 )
                annot_Pymnt_Annot_Expand_Uid.getValue(1).setValue(Global.getUSER());                                                                                      //Natural: ASSIGN ANNOT.PYMNT-ANNOT-EXPAND-UID ( 1 ) := *USER
                vw_annot.insertDBRow();                                                                                                                                   //Natural: STORE ANNOT
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            FOR01:                                                                                                                                                        //Natural: FOR #I 1 99
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(99)); pnd_I.nadd(1))
            {
                if (condition(annot_Pymnt_Annot_Expand_Cmnt.getValue(pnd_I).equals("              ")))                                                                    //Natural: IF PYMNT-ANNOT-EXPAND-CMNT ( #I ) = '              '
                {
                    annot_Pymnt_Annot_Dte.getValue(7).setValue(Global.getDATX());                                                                                         //Natural: MOVE *DATX TO PYMNT-ANNOT-DTE ( 7 )
                    annot_Pymnt_Annot_Id_Nbr.getValue(7).setValue("SYS");                                                                                                 //Natural: MOVE 'SYS' TO PYMNT-ANNOT-ID-NBR ( 7 )
                    annot_Pymnt_Annot_Flag_Ind.getValue(7).setValue("*");                                                                                                 //Natural: MOVE '*' TO PYMNT-ANNOT-FLAG-IND ( 7 )
                    annot_Pymnt_Annot_Expand_Cmnt.getValue(pnd_I).setValue(pnd_Cmnt);                                                                                     //Natural: MOVE #CMNT TO PYMNT-ANNOT-EXPAND-CMNT ( #I )
                    annot_Pymnt_Annot_Expand_Date.getValue(pnd_I).setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED *DATX ( EM = YYYYMMDD ) TO PYMNT-ANNOT-EXPAND-DATE ( #I )
                    annot_Pymnt_Annot_Expand_Time.getValue(pnd_I).setValueEdited(Global.getTIMX(),new ReportEditMask("HHII"));                                            //Natural: MOVE EDITED *TIMX ( EM = HHII ) TO PYMNT-ANNOT-EXPAND-TIME ( #I )
                    annot_Pymnt_Annot_Expand_Uid.getValue(pnd_I).setValue(Global.getUSER());                                                                              //Natural: MOVE *USER TO PYMNT-ANNOT-EXPAND-UID ( #I )
                    vw_annot.updateDBRow("FD1");                                                                                                                          //Natural: UPDATE ( FD1. )
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=95 PS=40");
        Global.format(1, "LS=80");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getDATU(),"-",new TabSetting(12),Global.getTIMX(), 
            new ReportEditMask ("HH':'IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM ",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(1), 
            new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(1),"PCP1885D - ",Global.getPROGRAM(),new TabSetting(29),"STOPS AND CANCELS CONTROL REPORT FOR RECORDS UPDATED FOR",pnd_Date, 
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(1),"ACCOUNT NUMBER:",pnd_Pp_Acct_No,NEWLINE);
        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,new TabSetting(1),Global.getDATU(),"-",new TabSetting(12),Global.getTIMX(), 
            new ReportEditMask ("HH':'IIAP"),new TabSetting(37),"CONSOLIDATED PAYMENT SYSTEM ",new TabSetting(80),"PAGE:",getReports().getPageNumberDbs(0), 
            new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(1),"PCP1885D - ",Global.getPROGRAM(),new TabSetting(29),"STOPS AND CANCELS CONTROL REPORT FOR ",pnd_Date, 
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(1),"ACCOUNT NUMBER:",pnd_Pp_Acct_No,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "      ",
        		"CHECK NUMBER",
        		positive_Pay_Rec_Pp_Chk_Nbr_10,"CHECK AMOUNT",
        		positive_Pay_Rec_Pp_Check_Amt_2, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),"STOP/CANCEL/IND",
        		positive_Pay_Rec_Pp_Stop,"ORGIN CODE",
        		fcp_Cons_Pymnt_Cntrct_Orgn_Cde,NEWLINE);
    }
    private void CheckAtStartofData305() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Paid_Date_Pnd_Paid_Date_Mmdd.setValue(positive_Pay_Rec_Pp_Mmdd);                                                                                          //Natural: ASSIGN #PAID-DATE-MMDD := PP-MMDD
            pnd_Paid_Date_Pnd_Paid_Date_Yyyy.setValue(positive_Pay_Rec_Pp_Yyyy);                                                                                          //Natural: ASSIGN #PAID-DATE-YYYY := PP-YYYY
            pnd_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Paid_Date);                                                                                        //Natural: MOVE EDITED #PAID-DATE TO #DATE ( EM = YYYYMMDD )
            pnd_Pp_Acct_No.setValue(positive_Pay_Rec_Pp_Acct_No);                                                                                                         //Natural: ASSIGN #PP-ACCT-NO := PP-ACCT-NO
            //*  TMM 04/01/2005
                                                                                                                                                                          //Natural: PERFORM LOAD-BANK-ACCOUNTS
            sub_Load_Bank_Accounts();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
