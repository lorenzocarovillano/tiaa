/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:02 PM
**        * FROM NATURAL PROGRAM : Fcpp196
************************************************************
**        * FILE NAME            : Fcpp196.java
**        * CLASS NAME           : Fcpp196
**        * INSTANCE NAME        : Fcpp196
************************************************************
************************************************************************
* PROGRAM  : FCPP196
* SYSTEM   : CPS
* TITLE    : EXTRACT-FOR-SORT FILE FOR "payments hold report"
* DATE     : NOV 13,93
* FUNCTION : THIS PROGRAM IS THE FIRST STEP IN A TWO PROGRAM JOB
*            STREAM THAT PRODUCES A REPORT OF ALL CHECKS THAT HAVE
*            BEEN HELD.
*
*            THIS PROGRAM READS THE EXTRACT FILES FROM (IA, DAILY
*            SETTLEMENT, MONTHLY SETTELEMENT). IT EXTRACTS FROM THE
*            INPUT RECORDS THE DATA VITAL FOR THE HELD REPORT.
*            THE EXTRACTED RECORDS ARE SHORTENED (AS COMPARE TO THE
*            PARENT EXTRACT), AND PREFIXED WITH THE DATA ELEMENTS USED
*            AS A SORT KEY:
*              1. ASCENDING - HOLDING DEPARTMENT CODE
*              2. ASCENDING - HOLD REASON CODE
*              3. ASCENDING - ANNUITENT NAME
*              4. ASECNDING - CHECK NUMBER
**----------------------------------------------------------------------
* HISTORY
*
* > 08-24-94 : A. YOUNG     - ADAPTED FROM FCPP350 FOR ON-LINE PAYMENT
*                             PROCESSING.
*                           - MODIFIED WORK FILE NUMBERS.
*                           - READ WORK FILE USING #OUTPUT.
*                           - ADDED FIELDS TO #HOLD-EXTRACT
*
* 05/22/1998 - RIAD LOUTFI - ADDED RETIREMENT LOAN ("AL") PROCESSING.
* 03/23/2000 - R. CARREON  - REJECT ORGN CDE 'EW'
* 4/2017    : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp196 extends BLNatBase
{
    // Data Areas
    private LdaFcpl190a ldaFcpl190a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Hold_Extract;
    private DbsField pnd_Hold_Extract_Cntrct_Hold_Cde;

    private DbsGroup pnd_Hold_Extract__R_Field_1;
    private DbsField pnd_Hold_Extract_Cntrct_Hold_Cde_Type;
    private DbsField pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt;
    private DbsField pnd_Hold_Extract_Cntrct_Hold_Grp;

    private DbsGroup pnd_Hold_Extract_Ph_Name;
    private DbsField pnd_Hold_Extract_Ph_Last_Name;
    private DbsField pnd_Hold_Extract_Ph_First_Name;
    private DbsField pnd_Hold_Extract_Ph_Middle_Name;
    private DbsField pnd_Hold_Extract_Pymnt_Check_Nbr;
    private DbsField pnd_Hold_Extract_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Hold_Extract_Cntrct_Payee_Cde;
    private DbsField pnd_Hold_Extract_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Hold_Extract_Cntrct_Orgn_Cde;
    private DbsField pnd_Hold_Extract_Pymnt_Check_Dte;
    private DbsField pnd_Hold_Extract_Pymnt_Eft_Dte;
    private DbsField pnd_Hold_Extract_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Hold_Extract_Pymnt_Check_Amt;
    private DbsField pnd_Hold_Extract_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Hold_Extract_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Hold_Extract_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde;

    private DbsGroup pnd_Counts;
    private DbsField pnd_Counts_Pnd_Cnt_Input_Records;
    private DbsField pnd_Counts_Pnd_Cnt_Hold_Records;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Hold_Extract = localVariables.newGroupInRecord("pnd_Hold_Extract", "#HOLD-EXTRACT");
        pnd_Hold_Extract_Cntrct_Hold_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);

        pnd_Hold_Extract__R_Field_1 = pnd_Hold_Extract.newGroupInGroup("pnd_Hold_Extract__R_Field_1", "REDEFINE", pnd_Hold_Extract_Cntrct_Hold_Cde);
        pnd_Hold_Extract_Cntrct_Hold_Cde_Type = pnd_Hold_Extract__R_Field_1.newFieldInGroup("pnd_Hold_Extract_Cntrct_Hold_Cde_Type", "CNTRCT-HOLD-CDE-TYPE", 
            FieldType.STRING, 2);
        pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt = pnd_Hold_Extract__R_Field_1.newFieldInGroup("pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt", "CNTRCT-HOLD-CDE-REQ-DPT", 
            FieldType.STRING, 2);
        pnd_Hold_Extract_Cntrct_Hold_Grp = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);

        pnd_Hold_Extract_Ph_Name = pnd_Hold_Extract.newGroupInGroup("pnd_Hold_Extract_Ph_Name", "PH-NAME");
        pnd_Hold_Extract_Ph_Last_Name = pnd_Hold_Extract_Ph_Name.newFieldInGroup("pnd_Hold_Extract_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Hold_Extract_Ph_First_Name = pnd_Hold_Extract_Ph_Name.newFieldInGroup("pnd_Hold_Extract_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            16);
        pnd_Hold_Extract_Ph_Middle_Name = pnd_Hold_Extract_Ph_Name.newFieldInGroup("pnd_Hold_Extract_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 
            16);
        pnd_Hold_Extract_Pymnt_Check_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Hold_Extract_Cntrct_Ppcn_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Hold_Extract_Cntrct_Payee_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Hold_Extract_Cntrct_Check_Crrncy_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Hold_Extract_Cntrct_Orgn_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Hold_Extract_Pymnt_Check_Dte = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Hold_Extract_Pymnt_Eft_Dte = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pnd_Hold_Extract_Pymnt_Prcss_Seq_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        pnd_Hold_Extract_Pymnt_Check_Amt = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Hold_Extract_Pymnt_Settlmnt_Dte = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Hold_Extract_Pymnt_Check_Seq_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Hold_Extract_Cntrct_Cmbn_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);

        pnd_Counts = localVariables.newGroupInRecord("pnd_Counts", "#COUNTS");
        pnd_Counts_Pnd_Cnt_Input_Records = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Cnt_Input_Records", "#CNT-INPUT-RECORDS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Counts_Pnd_Cnt_Hold_Records = pnd_Counts.newFieldInGroup("pnd_Counts_Pnd_Cnt_Hold_Records", "#CNT-HOLD-RECORDS", FieldType.PACKED_DECIMAL, 
            7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp196() throws Exception
    {
        super("Fcpp196");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        getReports().write(0, Global.getPROGRAM(),"starting at:",Global.getTIME());                                                                                       //Natural: WRITE ( 0 ) *PROGRAM 'starting at:' *TIME
        if (Global.isEscape()) return;
        getReports().write(1, Global.getPROGRAM(),"starting at:",Global.getTIME());                                                                                       //Natural: WRITE ( 1 ) *PROGRAM 'starting at:' *TIME
        if (Global.isEscape()) return;
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 08 RECORD #RPT-EXT.#PYMNT-ADDR
        while (condition(getWorkFiles().read(8, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr())))
        {
            pnd_Counts_Pnd_Cnt_Input_Records.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CNT-INPUT-RECORDS
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("NZ") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AL") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("EW"))) //Natural: REJECT IF #RPT-EXT.CNTRCT-ORGN-CDE = 'NZ' OR = 'AL' OR = 'EW'
            {
                continue;
            }
            if (condition(!(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Instmt_Nbr().equals(1) && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Cde().notEquals(" "))))                     //Natural: ACCEPT IF #RPT-EXT.PYMNT-INSTMT-NBR = 1 AND #RPT-EXT.CNTRCT-HOLD-CDE NE ' '
            {
                continue;
            }
            pnd_Hold_Extract.setValuesByName(ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr());                                                                                //Natural: MOVE BY NAME #RPT-EXT.#PYMNT-ADDR TO #HOLD-EXTRACT
            pnd_Counts_Pnd_Cnt_Hold_Records.nadd(1);                                                                                                                      //Natural: ADD 1 TO #CNT-HOLD-RECORDS
            getWorkFiles().write(9, false, pnd_Hold_Extract);                                                                                                             //Natural: WRITE WORK FILE 09 #HOLD-EXTRACT
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, Global.getPROGRAM(),"ending at:",Global.getTIME(),NEWLINE,"Number of records read          :",pnd_Counts_Pnd_Cnt_Input_Records,             //Natural: WRITE ( 0 ) *PROGRAM 'ending at:' *TIME / 'Number of records read          :' #CNT-INPUT-RECORDS / 'Number of hold records extracted:' #CNT-HOLD-RECORDS
            NEWLINE,"Number of hold records extracted:",pnd_Counts_Pnd_Cnt_Hold_Records);
        if (Global.isEscape()) return;
        getReports().write(1, Global.getPROGRAM(),"ending at:",Global.getTIME(),NEWLINE,"Number of records read          :",pnd_Counts_Pnd_Cnt_Input_Records,             //Natural: WRITE ( 1 ) *PROGRAM 'ending at:' *TIME / 'Number of records read          :' #CNT-INPUT-RECORDS / 'Number of hold records extracted:' #CNT-HOLD-RECORDS
            NEWLINE,"Number of hold records extracted:",pnd_Counts_Pnd_Cnt_Hold_Records);
        if (Global.isEscape()) return;
    }

    //
}
