/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:36 PM
**        * FROM NATURAL PROGRAM : Fcpp190
************************************************************
**        * FILE NAME            : Fcpp190.java
**        * CLASS NAME           : Fcpp190
**        * INSTANCE NAME        : Fcpp190
************************************************************
***********************************************************************
** PROGRAM:     FCPP190                                              **
** SYSTEM:      CPS - CONSOLIDATED PAYMENT SYSTEM                    **
** DATE:        09-07-94                                             **
** AUTHOR:      ALTHEA A. YOUNG                                      **
** DESCRIPTION: THIS PROGRAM READS THE NEW 500 BYTE EXTRACT AND      **
**              CONVERTS IT TO REPORT FORMAT.                        **
**                                                                   **
***********************************************************************
** HISTORY:                                                          **
** 08/06/96 - RESET DEDUCTION FIELDS                  RITA SALGADO   **
** 08/15/96 - RECONSTRUCT THE PROGRAM LOGIC FOR SS CONTROL PROCESSING**
**            & INCLUDE MDO IN THE REPORT             LIN ZHENG      **
** 09/24/96 - HANDLING PERSONAL ANNUITY THROUGH EFT   LIN ZHENG      **
** 11/22/96 - HANDLE BOTH SS & DC/CI/CM CONTROL PROCESSING -LIN ZHENG**
** 08/13/97 - HANDLE ONLY MDO PA THROUGH EFT &        LIN ZHENG      **
**            OBTAIN NZ CONTROL DATA FOR FCPP195, FCPP198            **
** 09/09/97 - OBTAIN AP CONTROL DATA (AP REDRAWS) FOR FCPP195        **
** 05/18/98 - OBTAIN 'AL' (ANNUITY LOAN) CONTROL DATA.   A. A. YOUNG **
** 04/21/99 - REVISE 'DC' DED. PROCESSING.               A. A. YOUNG **
** 08/05/99 - LEON GURTOVNIK                                         **
**             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP **
**             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE **
** 08/20/99 - REVISED TO POPULATE #RPT-EXT.ANNT-SOC-SEC-IND,         **
** A. YOUNG   #RPT-EXT.ANNT-SOC-SEC-NBR, #RPT-EXT.CNTRCT-CMBN-NBR,   **
**            AND #RPT-EXT.CNTRCT-UNQ-ID-NBR.                        **
** 10/04/99 - R. CARREON                                             **
**            CHANGE DEDUCTION OVERPAYMENT COMPUTATION               **
** 02/10/00 - R. CARREON                                             **
**            ADD EW PROCESSING FOR CONTROL RECORD                   **
** 05/09/01 - MARIA ACLAN                                            **
**            CORRECT DEDUCTION TOTALS                               **
* 4/2017     : JJG - PIN EXPANSION RESTOW                            **
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp190 extends BLNatBase
{
    // Data Areas
    private LdaFcplcntr ldaFcplcntr;
    private LdaFcpl190 ldaFcpl190;
    private LdaFcpl190a ldaFcpl190a;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpasscn pdaFcpasscn;
    private PdaFcpassc1 pdaFcpassc1;
    private PdaFcpassc2 pdaFcpassc2;
    private LdaFcplssc1 ldaFcplssc1;
    private PdaFcpacntl pdaFcpacntl;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Record_Counter;
    private DbsField pnd_Occurs_Index;
    private DbsField pnd_Ded_Ctr;
    private DbsField pnd_Ndx;
    private DbsField i;
    private DbsField j;
    private DbsField k;
    private DbsField l;
    private DbsField x;
    private DbsField pnd_Dc_Correct;
    private DbsField pnd_Ia_Csr_Correct;
    private DbsField pnd_Ms_Csr_Correct;
    private DbsField pnd_Write_Work_Sw;
    private DbsField pnd_Cm_Control;
    private DbsField pnd_Dc_Control;
    private DbsField pnd_Ss_Control;
    private DbsField pnd_Printed;
    private DbsField pnd_Ci_Control;
    private DbsField pnd_Date_Obtained;
    private DbsField pnd_Nz_Control_Data_Obtained;
    private DbsField pnd_Ap_Control_Data_Obtained;
    private DbsField pnd_Al_Control_Data_Obtained;
    private DbsField pnd_Ew_Control_Data_Obtained;
    private DbsField pnd_Pymnt_Settl_Amt;
    private DbsField pnd_Pymnt_Net_Amt;

    private DbsGroup pnd_Pymnt_Totals;
    private DbsField pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total;
    private DbsField pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total;
    private DbsField pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total;
    private DbsField pnd_Ws_Origin_Cd;
    private DbsField pnd_Ws_Invrse_Dt;
    private DbsField pnd_Ws_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Cancel_Rdrw_Act;
    private DbsField pnd_Pymnt_Ded_Cde_Alpha;
    private DbsField pnd_Ext_Pymnt_Ded_Cde_A;
    private DbsField pnd_Cntl_Message_1;
    private DbsField pnd_Orgn_Literal;
    private DbsField pnd_Csr_Literal;
    private DbsField pnd_Orgn_Csr_Literal;

    private DbsGroup pnd_Control_Record;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date;

    private DbsGroup pnd_Control_Record__R_Field_1;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Check;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Check;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Eft;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Int_Roll;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Int_Roll;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplcntr = new LdaFcplcntr();
        registerRecord(ldaFcplcntr);
        registerRecord(ldaFcplcntr.getVw_fcp_Cons_Cntrl());
        ldaFcpl190 = new LdaFcpl190();
        registerRecord(ldaFcpl190);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        localVariables = new DbsRecord();
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpasscn = new PdaFcpasscn(localVariables);
        pdaFcpassc1 = new PdaFcpassc1(localVariables);
        pdaFcpassc2 = new PdaFcpassc2(localVariables);
        ldaFcplssc1 = new LdaFcplssc1();
        registerRecord(ldaFcplssc1);
        pdaFcpacntl = new PdaFcpacntl(localVariables);

        // Local Variables
        pnd_Record_Counter = localVariables.newFieldInRecord("pnd_Record_Counter", "#RECORD-COUNTER", FieldType.NUMERIC, 9);
        pnd_Occurs_Index = localVariables.newFieldInRecord("pnd_Occurs_Index", "#OCCURS-INDEX", FieldType.NUMERIC, 3);
        pnd_Ded_Ctr = localVariables.newFieldInRecord("pnd_Ded_Ctr", "#DED-CTR", FieldType.NUMERIC, 3);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 3);
        i = localVariables.newFieldInRecord("i", "I", FieldType.NUMERIC, 3);
        j = localVariables.newFieldInRecord("j", "J", FieldType.NUMERIC, 3);
        k = localVariables.newFieldInRecord("k", "K", FieldType.NUMERIC, 3);
        l = localVariables.newFieldInRecord("l", "L", FieldType.NUMERIC, 3);
        x = localVariables.newFieldInRecord("x", "X", FieldType.NUMERIC, 3);
        pnd_Dc_Correct = localVariables.newFieldInRecord("pnd_Dc_Correct", "#DC-CORRECT", FieldType.BOOLEAN, 1);
        pnd_Ia_Csr_Correct = localVariables.newFieldInRecord("pnd_Ia_Csr_Correct", "#IA-CSR-CORRECT", FieldType.BOOLEAN, 1);
        pnd_Ms_Csr_Correct = localVariables.newFieldInRecord("pnd_Ms_Csr_Correct", "#MS-CSR-CORRECT", FieldType.BOOLEAN, 1);
        pnd_Write_Work_Sw = localVariables.newFieldInRecord("pnd_Write_Work_Sw", "#WRITE-WORK-SW", FieldType.BOOLEAN, 1);
        pnd_Cm_Control = localVariables.newFieldInRecord("pnd_Cm_Control", "#CM-CONTROL", FieldType.BOOLEAN, 1);
        pnd_Dc_Control = localVariables.newFieldInRecord("pnd_Dc_Control", "#DC-CONTROL", FieldType.BOOLEAN, 1);
        pnd_Ss_Control = localVariables.newFieldInRecord("pnd_Ss_Control", "#SS-CONTROL", FieldType.BOOLEAN, 1);
        pnd_Printed = localVariables.newFieldInRecord("pnd_Printed", "#PRINTED", FieldType.BOOLEAN, 1);
        pnd_Ci_Control = localVariables.newFieldInRecord("pnd_Ci_Control", "#CI-CONTROL", FieldType.BOOLEAN, 1);
        pnd_Date_Obtained = localVariables.newFieldInRecord("pnd_Date_Obtained", "#DATE-OBTAINED", FieldType.BOOLEAN, 1);
        pnd_Nz_Control_Data_Obtained = localVariables.newFieldInRecord("pnd_Nz_Control_Data_Obtained", "#NZ-CONTROL-DATA-OBTAINED", FieldType.BOOLEAN, 
            1);
        pnd_Ap_Control_Data_Obtained = localVariables.newFieldInRecord("pnd_Ap_Control_Data_Obtained", "#AP-CONTROL-DATA-OBTAINED", FieldType.BOOLEAN, 
            1);
        pnd_Al_Control_Data_Obtained = localVariables.newFieldInRecord("pnd_Al_Control_Data_Obtained", "#AL-CONTROL-DATA-OBTAINED", FieldType.BOOLEAN, 
            1);
        pnd_Ew_Control_Data_Obtained = localVariables.newFieldInRecord("pnd_Ew_Control_Data_Obtained", "#EW-CONTROL-DATA-OBTAINED", FieldType.BOOLEAN, 
            1);
        pnd_Pymnt_Settl_Amt = localVariables.newFieldInRecord("pnd_Pymnt_Settl_Amt", "#PYMNT-SETTL-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pymnt_Net_Amt = localVariables.newFieldInRecord("pnd_Pymnt_Net_Amt", "#PYMNT-NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Pymnt_Totals = localVariables.newGroupArrayInRecord("pnd_Pymnt_Totals", "#PYMNT-TOTALS", new DbsArrayController(1, 5, 1, 3, 1, 3));
        pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total = pnd_Pymnt_Totals.newFieldInGroup("pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total", "#PYMNT-CNT-TOTAL", FieldType.NUMERIC, 
            9);
        pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total = pnd_Pymnt_Totals.newFieldInGroup("pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total", "#PYMNT-GROSS-TOTAL", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total = pnd_Pymnt_Totals.newFieldInGroup("pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total", "#PYMNT-NET-TOTAL", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Origin_Cd = localVariables.newFieldInRecord("pnd_Ws_Origin_Cd", "#WS-ORIGIN-CD", FieldType.STRING, 2);
        pnd_Ws_Invrse_Dt = localVariables.newFieldInRecord("pnd_Ws_Invrse_Dt", "#WS-INVRSE-DT", FieldType.NUMERIC, 8);
        pnd_Ws_Pay_Type_Req_Ind = localVariables.newFieldInRecord("pnd_Ws_Pay_Type_Req_Ind", "#WS-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Cancel_Rdrw_Ind = localVariables.newFieldInRecord("pnd_Ws_Cancel_Rdrw_Ind", "#WS-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Ws_Cancel_Rdrw_Act = localVariables.newFieldInRecord("pnd_Ws_Cancel_Rdrw_Act", "#WS-CANCEL-RDRW-ACT", FieldType.STRING, 2);
        pnd_Pymnt_Ded_Cde_Alpha = localVariables.newFieldArrayInRecord("pnd_Pymnt_Ded_Cde_Alpha", "#PYMNT-DED-CDE-ALPHA", FieldType.STRING, 3, new DbsArrayController(1, 
            10));
        pnd_Ext_Pymnt_Ded_Cde_A = localVariables.newFieldInRecord("pnd_Ext_Pymnt_Ded_Cde_A", "#EXT-PYMNT-DED-CDE-A", FieldType.STRING, 3);
        pnd_Cntl_Message_1 = localVariables.newFieldInRecord("pnd_Cntl_Message_1", "#CNTL-MESSAGE-1", FieldType.STRING, 43);
        pnd_Orgn_Literal = localVariables.newFieldInRecord("pnd_Orgn_Literal", "#ORGN-LITERAL", FieldType.STRING, 16);
        pnd_Csr_Literal = localVariables.newFieldInRecord("pnd_Csr_Literal", "#CSR-LITERAL", FieldType.STRING, 7);
        pnd_Orgn_Csr_Literal = localVariables.newFieldInRecord("pnd_Orgn_Csr_Literal", "#ORGN-CSR-LITERAL", FieldType.STRING, 16);

        pnd_Control_Record = localVariables.newGroupInRecord("pnd_Control_Record", "#CONTROL-RECORD");
        pnd_Control_Record_Pnd_Cntl_Payment_Date = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date", "#CNTL-PAYMENT-DATE", 
            FieldType.STRING, 8);

        pnd_Control_Record__R_Field_1 = pnd_Control_Record.newGroupInGroup("pnd_Control_Record__R_Field_1", "REDEFINE", pnd_Control_Record_Pnd_Cntl_Payment_Date);
        pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2 = pnd_Control_Record__R_Field_1.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2", "#CNTL-PAYMENT-DATE-1-2", 
            FieldType.STRING, 2);
        pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8 = pnd_Control_Record__R_Field_1.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8", "#CNTL-PAYMENT-DATE-3-8", 
            FieldType.STRING, 6);
        pnd_Control_Record_Pnd_Cntl_Cnt_Check = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Check", "#CNTL-CNT-CHECK", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Check = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Check", "#CNTL-NET-AMT-CHECK", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Record_Pnd_Cntl_Cnt_Eft = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Eft", "#CNTL-CNT-EFT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft", "#CNTL-NET-AMT-EFT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Control_Record_Pnd_Cntl_Cnt_Int_Roll = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Int_Roll", "#CNTL-CNT-INT-ROLL", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Int_Roll = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Int_Roll", "#CNTL-NET-AMT-INT-ROLL", 
            FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplcntr.initializeValues();
        ldaFcpl190.initializeValues();
        ldaFcpl190a.initializeValues();
        ldaFcplssc1.initializeValues();

        localVariables.reset();
        pnd_Dc_Correct.setInitialValue(true);
        pnd_Ia_Csr_Correct.setInitialValue(true);
        pnd_Ms_Csr_Correct.setInitialValue(true);
        pnd_Write_Work_Sw.setInitialValue(false);
        pnd_Cm_Control.setInitialValue(false);
        pnd_Dc_Control.setInitialValue(false);
        pnd_Ss_Control.setInitialValue(false);
        pnd_Printed.setInitialValue(false);
        pnd_Ci_Control.setInitialValue(false);
        pnd_Cntl_Message_1.setInitialValue(" ");
        pnd_Orgn_Literal.setInitialValue(" ");
        pnd_Csr_Literal.setInitialValue(" ");
        pnd_Orgn_Csr_Literal.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp190() throws Exception
    {
        super("Fcpp190");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 15 ) LS = 133 PS = 58 ZP = ON
        getReports().definePrinter(2, "'CMPRT01'");                                                                                                                       //Natural: DEFINE PRINTER ( 1 ) OUTPUT 'CMPRT01'
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: AT TOP OF PAGE ( 1 );//Natural: MOVE 'INFP9000' TO *ERROR-TA
        //*  LZ
        //*  05-18-98
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPASSC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 6 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 8 )
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(8).setValue(true);
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #EXTRACT-RECORD
        while (condition(getWorkFiles().read(1, ldaFcpl190.getPnd_Extract_Record())))
        {
            CheckAtStartofData668();

            pnd_Record_Counter.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #RECORD-COUNTER
            short decideConditionsMet683 = 0;                                                                                                                             //Natural: AT START OF DATA;//Natural: AT END OF DATA;//Natural: DECIDE ON FIRST VALUE #EXTRACT-RECORD.#RECORD-LEVEL-NMBR;//Natural: VALUE 10
            if (condition((ldaFcpl190.getPnd_Extract_Record_Pnd_Record_Level_Nmbr().equals(10))))
            {
                decideConditionsMet683++;
                if (condition(pnd_Write_Work_Sw.getBoolean()))                                                                                                            //Natural: IF #WRITE-WORK-SW
                {
                    getWorkFiles().write(2, true, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr(), ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Inv_Detail().getValue(1,                    //Natural: WRITE WORK FILE 2 VARIABLE #RPT-EXT.#PYMNT-ADDR #RPT-EXT.#INV-DETAIL ( 1:C-INV-ACCT )
                        ":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));
                    ldaFcpl190a.getPnd_Rpt_Ext().reset();                                                                                                                 //Natural: RESET #RPT-EXT #OCCURS-INDEX #DED-CTR
                    pnd_Occurs_Index.reset();
                    pnd_Ded_Ctr.reset();
                    pnd_Write_Work_Sw.setValue(false);                                                                                                                    //Natural: ASSIGN #WRITE-WORK-SW := FALSE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde().notEquals(pnd_Ws_Origin_Cd)))                                                            //Natural: IF #EXTRACT-RECORD.CNTRCT-ORGN-CDE NE #WS-ORIGIN-CD
                {
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-SWITCH
                    sub_Set_Control_Switch();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  08/15/96
                                                                                                                                                                          //Natural: PERFORM PROCESS-HEADER-REC
                sub_Process_Header_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpassc2.getPnd_Fcpassc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                           //Natural: ASSIGN #NEW-PYMNT-IND := TRUE
                //*  08/15/96
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
                sub_Set_Control_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((ldaFcpl190.getPnd_Extract_Record_Pnd_Record_Level_Nmbr().equals(20))))
            {
                decideConditionsMet683++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-INV-OCCURS-REC
                sub_Process_Inv_Occurs_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  08/15/96
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
                sub_Accum_Control_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((ldaFcpl190.getPnd_Extract_Record_Pnd_Record_Level_Nmbr().equals(30))))
            {
                decideConditionsMet683++;
                //*  05-18-98
                if (condition((ldaFcpl190.getPnd_Extract_Record_Pnd_Na_Record_Occur_Nmbr().equals(1)) && (pnd_Ws_Pay_Type_Req_Ind.notEquals(8))))                         //Natural: IF ( #NA-RECORD-OCCUR-NMBR = 1 ) AND ( #WS-PAY-TYPE-REQ-IND NE 8 )
                {
                                                                                                                                                                          //Natural: PERFORM CONTROL-CALCULATIONS
                    sub_Control_Calculations();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-NAME-ADDR-REC
                sub_Process_Name_Addr_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            getWorkFiles().write(2, true, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr(), ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Inv_Detail().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); //Natural: WRITE WORK FILE 2 VARIABLE #RPT-EXT.#PYMNT-ADDR #RPT-EXT.#INV-DETAIL ( 1:C-INV-ACCT )
                                                                                                                                                                          //Natural: PERFORM BUILD-CONTROL-RECORD
            sub_Build_Control_Record();
            if (condition(Global.isEscape())) {return;}
            getWorkFiles().write(13, false, pnd_Control_Record);                                                                                                          //Natural: WRITE WORK FILE 13 #CONTROL-RECORD
                                                                                                                                                                          //Natural: PERFORM PRINT-PROCESSING-SUMMARY
            sub_Print_Processing_Summary();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CONTROL-TOTAL-COMPARISON-DC-CI-CM
            sub_Control_Total_Comparison_Dc_Ci_Cm();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        if (condition(pnd_Record_Counter.equals(getZero())))                                                                                                              //Natural: IF #RECORD-COUNTER = 0
        {
                                                                                                                                                                          //Natural: PERFORM BUILD-CONTROL-RECORD
            sub_Build_Control_Record();
            if (condition(Global.isEscape())) {return;}
            getWorkFiles().write(13, false, pnd_Control_Record);                                                                                                          //Natural: WRITE WORK FILE 13 #CONTROL-RECORD
                                                                                                                                                                          //Natural: PERFORM PRINT-PROCESSING-SUMMARY
            sub_Print_Processing_Summary();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CONTROL-TOTAL-COMPARISON-DC-CI-CM
            sub_Control_Total_Comparison_Dc_Ci_Cm();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  LZ
                                                                                                                                                                          //Natural: PERFORM PRINT-SS-CONTROL-REPORT
        sub_Print_Ss_Control_Report();
        if (condition(Global.isEscape())) {return;}
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-HEADER-REC
        //* ***********************************************************************
        //* *MOVE BY NAME #EXTRACT-RECORD TO #RPT-EXT
        //*  08/20/99 - A. YOUNG
        //* *----------------------------------------------------- LEON 07/14/00
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INV-OCCURS-REC
        //*  #RPT-EXT.##INV-SETTLMNT-DTE(#OCCURS-INDEX) :=
        //*    #EXTRACT-RECORD.##INV-SETTLMNT-DTE-START
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-NAME-ADDR-REC
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROL-CALCULATIONS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PROCESSING-SUMMARY
        //*  L E V E L   2   S U B R O U T I N E S
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-SWITCH
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-CONTROL-RECORD
        //* *** OBTAIN CONTROL DATA FOR MISCELLANEOUS ORIGINS NOT INCLUDED IN WF 01
        //* *** (CONTROLS ARE NEEDED FOR POSITIVE PAY AND EFT FILE GENERATION.)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-CONTROL-DATA
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-AP-CONTROL-DATA
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-DATA
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-DATA
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SS-CONTROL-REPORT
        //* ****************************************
        //* * T E M P #FCPACRPT.#NO-ABEND       := FALSE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* ***********************************************************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROL-TOTAL-COMPARISON-DC-CI-CM
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-COLUMN-HEADINGS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-ORGN-CSR-LITERAL
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ERROR-MESSAGE
    }
    private void sub_Process_Header_Rec() throws Exception                                                                                                                //Natural: PROCESS-HEADER-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Origin_Cd.setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde());                                                                                    //Natural: ASSIGN #WS-ORIGIN-CD := #RPT-EXT.CNTRCT-ORGN-CDE := #EXTRACT-RECORD.CNTRCT-ORGN-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde());
        pnd_Ws_Invrse_Dt.setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Invrse_Dte());                                                                                  //Natural: ASSIGN #WS-INVRSE-DT := #RPT-EXT.CNTRCT-INVRSE-DTE := #EXTRACT-RECORD.CNTRCT-INVRSE-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Invrse_Dte().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Invrse_Dte());
        pnd_Ws_Pay_Type_Req_Ind.setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Pay_Type_Req_Ind());                                                                      //Natural: ASSIGN #WS-PAY-TYPE-REQ-IND := #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND := #EXTRACT-RECORD.PYMNT-PAY-TYPE-REQ-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Pay_Type_Req_Ind());
        pnd_Ws_Cancel_Rdrw_Act.setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde());                                                                //Natural: ASSIGN #WS-CANCEL-RDRW-ACT := #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE := #EXTRACT-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde());
        pnd_Ws_Cancel_Rdrw_Ind.setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Ind());                                                                       //Natural: ASSIGN #WS-CANCEL-RDRW-IND := #RPT-EXT.CNTRCT-CANCEL-RDRW-IND := #EXTRACT-RECORD.CNTRCT-CANCEL-RDRW-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Ind());
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Check_Dte());                                                        //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-DTE := #EXTRACT-RECORD.PYMNT-CHECK-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Dte().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Eft_Dte());                                                            //Natural: ASSIGN #RPT-EXT.PYMNT-EFT-DTE := #EXTRACT-RECORD.PYMNT-EFT-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ftre_Ind());                                                          //Natural: ASSIGN #RPT-EXT.PYMNT-FTRE-IND := #EXTRACT-RECORD.PYMNT-FTRE-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Ppcn_Nbr());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-PPCN-NBR := #EXTRACT-RECORD.CNTRCT-PPCN-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cref_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cref_Nbr());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-CREF-NBR := #EXTRACT-RECORD.CNTRCT-CREF-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Rqst_Settl_Dte().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Rqst_Settl_Dte());                                            //Natural: ASSIGN #RPT-EXT.CNTRCT-RQST-SETTL-DTE := #EXTRACT-RECORD.CNTRCT-RQST-SETTL-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Settlmnt_Dte());                                                  //Natural: ASSIGN #RPT-EXT.PYMNT-SETTLMNT-DTE := #EXTRACT-RECORD.PYMNT-SETTLMNT-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Type_Cde());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-TYPE-CDE := #EXTRACT-RECORD.CNTRCT-TYPE-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Lob_Cde());                                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-LOB-CDE := #EXTRACT-RECORD.CNTRCT-LOB-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Annt_Soc_Sec_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Annt_Soc_Sec_Ind());                                                      //Natural: ASSIGN #RPT-EXT.ANNT-SOC-SEC-IND := #EXTRACT-RECORD.ANNT-SOC-SEC-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Annt_Soc_Sec_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Annt_Soc_Sec_Nbr());                                                      //Natural: ASSIGN #RPT-EXT.ANNT-SOC-SEC-NBR := #EXTRACT-RECORD.ANNT-SOC-SEC-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cmbn_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cmbn_Nbr());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-CMBN-NBR := #EXTRACT-RECORD.CNTRCT-CMBN-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Unq_Id_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Unq_Id_Nbr());                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-UNQ-ID-NBR := #EXTRACT-RECORD.CNTRCT-UNQ-ID-NBR
        if (condition((ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde().equals("SS") && (ldaFcpl190.getPnd_Extract_Record_Cntrct_Lob_Cde().equals("RA")                 //Natural: IF ( #EXTRACT-RECORD.CNTRCT-ORGN-CDE = 'SS' ) AND ( #EXTRACT-RECORD.CNTRCT-LOB-CDE = 'RA' OR = 'RAD' )
            || ldaFcpl190.getPnd_Extract_Record_Cntrct_Lob_Cde().equals("RAD")))))
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "X", ldaFcpl190.getPnd_Extract_Record_Cntrct_Lob_Cde())); //Natural: COMPRESS 'X' #EXTRACT-RECORD.CNTRCT-LOB-CDE INTO #RPT-EXT.CNTRCT-LOB-CDE LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Hold_Cde());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-HOLD-CDE := #EXTRACT-RECORD.CNTRCT-HOLD-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Grp().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Hold_Grp());                                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-HOLD-GRP := #EXTRACT-RECORD.CNTRCT-HOLD-GRP
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Crrncy_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Crrncy_Cde());                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-CRRNCY-CDE := #EXTRACT-RECORD.CNTRCT-CRRNCY-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Check_Amt());                                                        //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-AMT := #EXTRACT-RECORD.PYMNT-CHECK-AMT
        if (condition(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF #EXTRACT-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Nbr());                                            //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-NBR := #EXTRACT-RECORD.#HDR-PYMNT-CHECK-NBR
            //*  05-18-98
            if (condition(ldaFcpl190.getPnd_Extract_Record_Pymnt_Pay_Type_Req_Ind().equals(2) || ldaFcpl190.getPnd_Extract_Record_Pymnt_Pay_Type_Req_Ind().equals(8)))    //Natural: IF #EXTRACT-RECORD.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8
            {
                ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Nbr());                                  //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-SCRTY-NBR := #EXTRACT-RECORD.#HDR-PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Seq_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Pnd_Hdr_Pymnt_Check_Seq_Nbr());                                    //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-SEQ-NBR := #EXTRACT-RECORD.#HDR-PYMNT-CHECK-SEQ-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Check_Nbr());                                                    //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-NBR := #EXTRACT-RECORD.PYMNT-CHECK-NBR
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Check_Scrty_Nbr());                                        //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-SCRTY-NBR := #EXTRACT-RECORD.PYMNT-CHECK-SCRTY-NBR
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Seq_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Check_Seq_Nbr());                                            //Natural: ASSIGN #RPT-EXT.PYMNT-CHECK-SEQ-NBR := #EXTRACT-RECORD.PYMNT-CHECK-SEQ-NBR
            //*  05-18-98
            //*  05-18-98
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Prcss_Seq_Nbr());                                                //Natural: ASSIGN #RPT-EXT.PYMNT-PRCSS-SEQ-NBR := #EXTRACT-RECORD.PYMNT-PRCSS-SEQ-NBR
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Pay_Type_Req_Ind());                                          //Natural: ASSIGN #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND := #EXTRACT-RECORD.PYMNT-PAY-TYPE-REQ-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Pymnt_Type_Ind());                                            //Natural: ASSIGN #RPT-EXT.CNTRCT-PYMNT-TYPE-IND := #EXTRACT-RECORD.CNTRCT-PYMNT-TYPE-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Sttlmnt_Type_Ind());                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND := #EXTRACT-RECORD.CNTRCT-STTLMNT-TYPE-IND
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Roll_Dest_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Roll_Dest_Cde());                                              //Natural: ASSIGN #RPT-EXT.CNTRCT-ROLL-DEST-CDE := #EXTRACT-RECORD.CNTRCT-ROLL-DEST-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Qlfied_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Qlfied_Cde());                                                    //Natural: ASSIGN #RPT-EXT.CNTRCT-QLFIED-CDE := #EXTRACT-RECORD.CNTRCT-QLFIED-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Annt_Rsdncy_Cde());                                                        //Natural: ASSIGN #RPT-EXT.ANNT-RSDNCY-CDE := #EXTRACT-RECORD.ANNT-RSDNCY-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Acctg_Dte());                                                        //Natural: ASSIGN #RPT-EXT.PYMNT-ACCTG-DTE := #EXTRACT-RECORD.PYMNT-ACCTG-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Intrfce_Dte());                                                    //Natural: ASSIGN #RPT-EXT.PYMNT-INTRFCE-DTE := #EXTRACT-RECORD.PYMNT-INTRFCE-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Cnr_Orgnl_Invrse_Dte().setValue(ldaFcpl190.getPnd_Extract_Record_Cnr_Orgnl_Invrse_Dte());                                              //Natural: ASSIGN #RPT-EXT.CNR-ORGNL-INVRSE-DTE := #EXTRACT-RECORD.CNR-ORGNL-INVRSE-DTE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Annty_Ins_Type());                                            //Natural: ASSIGN #RPT-EXT.CNTRCT-ANNTY-INS-TYPE := #EXTRACT-RECORD.CNTRCT-ANNTY-INS-TYPE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Type_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Annty_Type_Cde());                                            //Natural: ASSIGN #RPT-EXT.CNTRCT-ANNTY-TYPE-CDE := #EXTRACT-RECORD.CNTRCT-ANNTY-TYPE-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Insurance_Option().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Insurance_Option());                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-INSURANCE-OPTION := #EXTRACT-RECORD.CNTRCT-INSURANCE-OPTION
        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Life_Contingency().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Life_Contingency());                                        //Natural: ASSIGN #RPT-EXT.CNTRCT-LIFE-CONTINGENCY := #EXTRACT-RECORD.CNTRCT-LIFE-CONTINGENCY
        //* *-------------------------------------------------------------------
        if (condition(ldaFcpl190.getPnd_Extract_Record_Cntrct_Type_Cde().equals("30") || ldaFcpl190.getPnd_Extract_Record_Cntrct_Type_Cde().equals("31")))                //Natural: IF #EXTRACT-RECORD.CNTRCT-TYPE-CDE = '30' OR = '31'
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Da_Tiaa_1_Nbr());                                          //Natural: ASSIGN #RPT-EXT.CNTRCT-DA-TIAA-1-NBR := #EXTRACT-RECORD.CNTRCT-DA-TIAA-1-NBR
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Inv_Occurs_Rec() throws Exception                                                                                                            //Natural: PROCESS-INV-OCCURS-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Occurs_Index.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #OCCURS-INDEX
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Valuat_Period().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Valuat_Period());               //Natural: ASSIGN #RPT-EXT.INV-ACCT-VALUAT-PERIOD ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-VALUAT-PERIOD
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Cde().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Cde());                                   //Natural: ASSIGN #RPT-EXT.INV-ACCT-CDE ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Qty().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Unit_Qty());                         //Natural: ASSIGN #RPT-EXT.INV-ACCT-UNIT-QTY ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-UNIT-QTY
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Value().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Unit_Value());                     //Natural: ASSIGN #RPT-EXT.INV-ACCT-UNIT-VALUE ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-UNIT-VALUE
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Settl_Amt());                       //Natural: ASSIGN #RPT-EXT.INV-ACCT-SETTL-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-SETTL-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Ivc_Amt());                           //Natural: ASSIGN #RPT-EXT.INV-ACCT-IVC-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-IVC-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dci_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dci_Amt());                           //Natural: ASSIGN #RPT-EXT.INV-ACCT-DCI-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-DCI-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dpi_Amt());                           //Natural: ASSIGN #RPT-EXT.INV-ACCT-DPI-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-DPI-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dvdnd_Amt());                       //Natural: ASSIGN #RPT-EXT.INV-ACCT-DVDND-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-DVDND-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fed_Cde().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Fed_Cde());                           //Natural: ASSIGN #RPT-EXT.INV-ACCT-FED-CDE ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-FED-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Fdrl_Tax_Amt());                 //Natural: ASSIGN #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-FDRL-TAX-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Cde().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_State_Cde());                       //Natural: ASSIGN #RPT-EXT.INV-ACCT-STATE-CDE ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-STATE-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_State_Tax_Amt());               //Natural: ASSIGN #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-STATE-TAX-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Cde().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Local_Cde());                       //Natural: ASSIGN #RPT-EXT.INV-ACCT-LOCAL-CDE ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-LOCAL-CDE
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Local_Tax_Amt());               //Natural: ASSIGN #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-LOCAL-TAX-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Exp_Amt());                           //Natural: ASSIGN #RPT-EXT.INV-ACCT-EXP-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-EXP-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt());               //Natural: ASSIGN #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-NET-PYMNT-AMT
        ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct().setValue(ldaFcpl190.getPnd_Extract_Record_Pnd_Cntr_Inv_Acct());                                                           //Natural: ASSIGN #RPT-EXT.C-INV-ACCT := #EXTRACT-RECORD.#CNTR-INV-ACCT
        //*  IF (#OCCURS-INDEX = 1) AND (#EXTRACT-RECORD.#CNTR-DEDUCTIONS > 0)
        //*   I := #RPT-EXT.#CNTR-DEDUCTIONS := #EXTRACT-RECORD.#CNTR-DEDUCTIONS
        //*   #RPT-EXT.PYMNT-DED-CDE(1:I)    := #EXTRACT-RECORD.PYMNT-DED-CDE(1:I)
        //*   #RPT-EXT.PYMNT-DED-AMT(1:I)    := #EXTRACT-RECORD.PYMNT-DED-AMT(1:I)
        //*   #RPT-EXT.PYMNT-DED-PAYEE-CDE          (1:I) :=
        //*     #EXTRACT-RECORD.PYMNT-DED-PAYEE-CDE (1:I)
        //*  END-IF
        //* ************** MLA 05/09/01  START OF CHANGES******************
        if (condition(ldaFcpl190.getPnd_Extract_Record_Pnd_Cntr_Deductions().greater(getZero())))                                                                         //Natural: IF #EXTRACT-RECORD.#CNTR-DEDUCTIONS > 0
        {
            x.reset();                                                                                                                                                    //Natural: RESET X #NDX
            pnd_Ndx.reset();
            FOR01:                                                                                                                                                        //Natural: FOR X 1 TO #EXTRACT-RECORD.#CNTR-DEDUCTIONS
            for (x.setValue(1); condition(x.lessOrEqual(ldaFcpl190.getPnd_Extract_Record_Pnd_Cntr_Deductions())); x.nadd(1))
            {
                if (condition(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ded_Cde().getValue(x).notEquals(getZero())))                                                         //Natural: IF #EXTRACT-RECORD.PYMNT-DED-CDE ( X ) NE 0
                {
                    pnd_Pymnt_Ded_Cde_Alpha.getValue("*").setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Cde().getValue("*"));                                             //Natural: ASSIGN #PYMNT-DED-CDE-ALPHA ( * ) := #RPT-EXT.PYMNT-DED-CDE ( * )
                    pnd_Ext_Pymnt_Ded_Cde_A.setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ded_Cde().getValue(x));                                                       //Natural: ASSIGN #EXT-PYMNT-DED-CDE-A := #EXTRACT-RECORD.PYMNT-DED-CDE ( X )
                    DbsUtil.examine(new ExamineSource(pnd_Pymnt_Ded_Cde_Alpha.getValue("*"),true), new ExamineSearch(pnd_Ext_Pymnt_Ded_Cde_A), new ExamineGivingIndex(pnd_Ndx)); //Natural: EXAMINE FULL VALUE OF #PYMNT-DED-CDE-ALPHA ( * ) FOR #EXT-PYMNT-DED-CDE-A GIVING INDEX #NDX
                    if (condition(pnd_Ndx.greater(getZero())))                                                                                                            //Natural: IF #NDX GT 0
                    {
                        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(pnd_Ndx).nadd(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ded_Amt().getValue(x));                  //Natural: ADD #EXTRACT-RECORD.PYMNT-DED-AMT ( X ) TO #RPT-EXT.PYMNT-DED-AMT ( #NDX )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ded_Ctr.nadd(1);                                                                                                                              //Natural: ADD 1 TO #DED-CTR
                        if (condition(pnd_Ded_Ctr.greater(10)))                                                                                                           //Natural: IF #DED-CTR GT 10
                        {
                            getReports().write(0, ReportOption.NOTITLE,"Index execeeded the limit","PPCN No. ",ldaFcpl190.getPnd_Extract_Record_Cntrct_Ppcn_Nbr());       //Natural: WRITE 'Index execeeded the limit' 'PPCN No. ' #EXTRACT-RECORD.CNTRCT-PPCN-NBR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions().setValue(pnd_Ded_Ctr);                                                                       //Natural: ASSIGN #RPT-EXT.#CNTR-DEDUCTIONS := #DED-CTR
                            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Cde().getValue(pnd_Ded_Ctr).setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ded_Cde().getValue(x));      //Natural: ASSIGN #RPT-EXT.PYMNT-DED-CDE ( #DED-CTR ) := #EXTRACT-RECORD.PYMNT-DED-CDE ( X )
                            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(pnd_Ded_Ctr).setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ded_Amt().getValue(x));      //Natural: ASSIGN #RPT-EXT.PYMNT-DED-AMT ( #DED-CTR ) := #EXTRACT-RECORD.PYMNT-DED-AMT ( X )
                            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Payee_Cde().getValue(pnd_Ded_Ctr).setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ded_Payee_Cde().getValue(x)); //Natural: ASSIGN #RPT-EXT.PYMNT-DED-PAYEE-CDE ( #DED-CTR ) := #EXTRACT-RECORD.PYMNT-DED-PAYEE-CDE ( X )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ************** MLA 05/09/01  END OF CHANGES ******************
        //*  04-21-99
        if (condition(pnd_Ws_Origin_Cd.equals("DC")))                                                                                                                     //Natural: IF #WS-ORIGIN-CD EQ 'DC'
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Cde().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ded_Cde().getValue(1));           //Natural: ASSIGN #RPT-EXT.##INV-DED-CDE ( #OCCURS-INDEX ) := #EXTRACT-RECORD.PYMNT-DED-CDE ( 1 )
            ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Payee_Cde().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Ded_Payee_Cde().getValue(1)); //Natural: ASSIGN #RPT-EXT.##INV-DED-PAYEE-CDE ( #OCCURS-INDEX ) := #EXTRACT-RECORD.PYMNT-DED-PAYEE-CDE ( 1 )
            ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt().getValue(pnd_Occurs_Index).compute(new ComputeParameters(false, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt().getValue(pnd_Occurs_Index)),  //Natural: ASSIGN #RPT-EXT.##INV-DED-AMT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.INV-ACCT-SETTL-AMT + #EXTRACT-RECORD.INV-ACCT-DVDND-AMT + #EXTRACT-RECORD.INV-ACCT-DCI-AMT + #EXTRACT-RECORD.INV-ACCT-DPI-AMT - #EXTRACT-RECORD.INV-ACCT-EXP-AMT - #EXTRACT-RECORD.INV-ACCT-FDRL-TAX-AMT - #EXTRACT-RECORD.INV-ACCT-STATE-TAX-AMT - #EXTRACT-RECORD.INV-ACCT-LOCAL-TAX-AMT - #EXTRACT-RECORD.INV-ACCT-NET-PYMNT-AMT
                ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Settl_Amt().add(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dvdnd_Amt()).add(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dci_Amt()).add(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dpi_Amt()).subtract(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Exp_Amt()).subtract(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Fdrl_Tax_Amt()).subtract(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_State_Tax_Amt()).subtract(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Local_Tax_Amt()).subtract(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt()));
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Name_Addr_Rec() throws Exception                                                                                                             //Natural: PROCESS-NAME-ADDR-REC
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Occurs_Index.setValue(ldaFcpl190.getPnd_Extract_Record_Pnd_Na_Record_Occur_Nmbr());                                                                           //Natural: ASSIGN #OCCURS-INDEX := #NA-RECORD-OCCUR-NMBR
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Nme());                                         //Natural: ASSIGN #RPT-EXT.PYMNT-NME ( #OCCURS-INDEX ) := #EXTRACT-RECORD.PYMNT-NME
        ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(pnd_Occurs_Index).setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Addr_Line1_Txt());                   //Natural: ASSIGN #RPT-EXT.PYMNT-ADDR-LINE1-TXT ( #OCCURS-INDEX ) := #EXTRACT-RECORD.PYMNT-ADDR-LINE1-TXT
        if (condition(ldaFcpl190.getPnd_Extract_Record_Pnd_Na_Record_Occur_Nmbr().equals(1)))                                                                             //Natural: IF #NA-RECORD-OCCUR-NMBR = 1
        {
            ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name().setValue(ldaFcpl190.getPnd_Extract_Record_Ph_Last_Name());                                                          //Natural: ASSIGN #RPT-EXT.PH-LAST-NAME := #EXTRACT-RECORD.PH-LAST-NAME
            ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name().setValue(ldaFcpl190.getPnd_Extract_Record_Ph_First_Name());                                                        //Natural: ASSIGN #RPT-EXT.PH-FIRST-NAME := #EXTRACT-RECORD.PH-FIRST-NAME
            ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name().setValue(ldaFcpl190.getPnd_Extract_Record_Ph_Middle_Name());                                                      //Natural: ASSIGN #RPT-EXT.PH-MIDDLE-NAME := #EXTRACT-RECORD.PH-MIDDLE-NAME
            ldaFcpl190a.getPnd_Rpt_Ext_Annt_Name().setValue(DbsUtil.compress(ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name(),     //Natural: COMPRESS #RPT-EXT.PH-FIRST-NAME #RPT-EXT.PH-MIDDLE-NAME #RPT-EXT.PH-LAST-NAME INTO #RPT-EXT.ANNT-NAME
                ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name()));
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Transit_Id().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Eft_Transit_Id());                                          //Natural: ASSIGN #RPT-EXT.PYMNT-EFT-TRANSIT-ID := #EXTRACT-RECORD.PYMNT-EFT-TRANSIT-ID
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Eft_Acct_Nbr());                                              //Natural: ASSIGN #RPT-EXT.PYMNT-EFT-ACCT-NBR := #EXTRACT-RECORD.PYMNT-EFT-ACCT-NBR
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Chk_Sav_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Chk_Sav_Ind());                                                //Natural: ASSIGN #RPT-EXT.PYMNT-CHK-SAV-IND := #EXTRACT-RECORD.PYMNT-CHK-SAV-IND
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Write_Work_Sw.setValue(true);                                                                                                                                 //Natural: ASSIGN #WRITE-WORK-SW := TRUE
    }
    private void sub_Control_Calculations() throws Exception                                                                                                              //Natural: CONTROL-CALCULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  IA DEATH
        //*  IA CAN-REDRAW
        //*  MS CAN-REDRAW
        short decideConditionsMet975 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #RPT-EXT.CNTRCT-ORGN-CDE;//Natural: VALUE 'DC'
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet975++;
            l.setValue(1);                                                                                                                                                //Natural: ASSIGN L := 1
        }                                                                                                                                                                 //Natural: VALUE 'IA'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA"))))
        {
            decideConditionsMet975++;
            l.setValue(2);                                                                                                                                                //Natural: ASSIGN L := 2
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("MS"))))
        {
            decideConditionsMet975++;
            l.setValue(3);                                                                                                                                                //Natural: ASSIGN L := 3
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet975 > 0))
        {
            //*  REGULAR PAYMENT
            //*  REDRAWN PAYMENT
            //*  LEON
            //*  JWO 2010-11
            //*  CANCELLED PAYMENT
            //*  LEON
            //*  JWO 2010-11
            //*  STOPPED PAYMENT
            short decideConditionsMet991 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' '
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ")))
            {
                decideConditionsMet991++;
                j.setValue(1);                                                                                                                                            //Natural: ASSIGN J := 1
            }                                                                                                                                                             //Natural: WHEN #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))
            {
                decideConditionsMet991++;
                j.setValue(2);                                                                                                                                            //Natural: ASSIGN J := 2
            }                                                                                                                                                             //Natural: WHEN #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'CN' OR = 'CP' OR = 'RP' OR = 'PR'
            else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") 
                || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
                || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))
            {
                decideConditionsMet991++;
                j.setValue(4);                                                                                                                                            //Natural: ASSIGN J := 4
            }                                                                                                                                                             //Natural: WHEN #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'S' OR = 'SN' OR = 'SP'
            else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
                || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP")))
            {
                decideConditionsMet991++;
                j.setValue(5);                                                                                                                                            //Natural: ASSIGN J := 5
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet991 > 0))
            {
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,2,l).nadd(1);                                                                                             //Natural: ADD 1 TO #PYMNT-CNT-TOTAL ( J,2,L )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,2,l).nadd(pnd_Pymnt_Settl_Amt);                                                                         //Natural: ADD #PYMNT-SETTL-AMT TO #PYMNT-GROSS-TOTAL ( J,2,L )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,2,l).nadd(pnd_Pymnt_Net_Amt);                                                                             //Natural: ADD #PYMNT-NET-AMT TO #PYMNT-NET-TOTAL ( J,2,L )
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
                {
                    pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(3,2,1).nadd(1);                                                                                         //Natural: ADD 1 TO #PYMNT-CNT-TOTAL ( 3,2,1 )
                    pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(3,2,1).nadd(pnd_Pymnt_Settl_Amt);                                                                     //Natural: ADD #PYMNT-SETTL-AMT TO #PYMNT-GROSS-TOTAL ( 3,2,1 )
                    pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(3,2,1).nadd(pnd_Pymnt_Net_Amt);                                                                         //Natural: ADD #PYMNT-NET-AMT TO #PYMNT-NET-TOTAL ( 3,2,1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Pymnt_Settl_Amt.reset();                                                                                                                                      //Natural: RESET #PYMNT-SETTL-AMT #PYMNT-NET-AMT
        pnd_Pymnt_Net_Amt.reset();
    }
    private void sub_Print_Processing_Summary() throws Exception                                                                                                          //Natural: PRINT-PROCESSING-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF IA DEATH RECORDS READ: .....................",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(1,2,1),  //Natural: WRITE ( 1 ) NOTITLE // 35T 'NUMBER OF IA DEATH RECORDS READ: .....................' 3X #PYMNT-CNT-TOTAL ( 1,2,1 ) //35T 'NUMBER OF IA DEATH REDRAW RECORDS READ:...............' 3X #PYMNT-CNT-TOTAL ( 2,2,1 ) //35T 'NUMBER OF IA DEATH CANCEL RECORDS READ:...............' 3X #PYMNT-CNT-TOTAL ( 4,2,1 ) //35T 'NUMBER OF IA DEATH STOP RECORDS READ:.................' 3X #PYMNT-CNT-TOTAL ( 5,2,1 ) //35T 'NUMBER OF IA REDRAW RECORDS READ:.....................' 3X #PYMNT-CNT-TOTAL ( 2,2,2 ) //35T 'NUMBER OF IA CANCEL RECORDS READ:.....................' 3X #PYMNT-CNT-TOTAL ( 4,2,2 ) //35T 'NUMBER OF IA STOP RECORDS READ:.......................' 3X #PYMNT-CNT-TOTAL ( 5,2,2 ) //35T 'NUMBER OF MONTHLY SETTLEMENTS REDRAW RECORDS READ:....' 3X #PYMNT-CNT-TOTAL ( 2,2,3 ) //35T 'NUMBER OF MONTHLY SETTLEMENTS CANCEL RECORDS READ:....' 3X #PYMNT-CNT-TOTAL ( 4,2,3 ) //35T 'NUMBER OF MONTHLY SETTLEMENTS STOP RECORDS READ:......' 3X #PYMNT-CNT-TOTAL ( 5,2,3 )
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF IA DEATH REDRAW RECORDS READ:...............",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(2,2,1), 
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF IA DEATH CANCEL RECORDS READ:...............",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(4,2,1), 
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF IA DEATH STOP RECORDS READ:.................",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(5,2,1), 
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF IA REDRAW RECORDS READ:.....................",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(2,2,2), 
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF IA CANCEL RECORDS READ:.....................",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(4,2,2), 
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF IA STOP RECORDS READ:.......................",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(5,2,2), 
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF MONTHLY SETTLEMENTS REDRAW RECORDS READ:....",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(2,2,3), 
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF MONTHLY SETTLEMENTS CANCEL RECORDS READ:....",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(4,2,3), 
            new ReportEditMask ("ZZZZZZZZ9"),NEWLINE,NEWLINE,new TabSetting(35),"NUMBER OF MONTHLY SETTLEMENTS STOP RECORDS READ:......",new ColumnSpacing(3),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(5,2,3), 
            new ReportEditMask ("ZZZZZZZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Set_Control_Switch() throws Exception                                                                                                                //Natural: SET-CONTROL-SWITCH
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1029 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #EXTRACT-RECORD.CNTRCT-ORGN-CDE;//Natural: VALUE 'MS'
        if (condition((ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde().equals("MS"))))
        {
            decideConditionsMet1029++;
            pnd_Cm_Control.setValue(true);                                                                                                                                //Natural: ASSIGN #CM-CONTROL := TRUE
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet1029++;
            pnd_Dc_Control.setValue(true);                                                                                                                                //Natural: ASSIGN #DC-CONTROL := TRUE
        }                                                                                                                                                                 //Natural: VALUE 'SS', 'DS'
        else if (condition((ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde().equals("SS") || ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde().equals("DS"))))
        {
            decideConditionsMet1029++;
            pnd_Ss_Control.setValue(true);                                                                                                                                //Natural: ASSIGN #SS-CONTROL := TRUE
        }                                                                                                                                                                 //Natural: VALUE 'IA'
        else if (condition((ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde().equals("IA"))))
        {
            decideConditionsMet1029++;
            pnd_Ci_Control.setValue(true);                                                                                                                                //Natural: ASSIGN #CI-CONTROL := TRUE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"INVALID ORIGIN CODE",ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde(),"ENCOUNTERED FOR CONTRACT", //Natural: WRITE NOTITLE // 'INVALID ORIGIN CODE' #EXTRACT-RECORD.CNTRCT-ORGN-CDE 'ENCOUNTERED FOR CONTRACT' #EXTRACT-RECORD.CNTRCT-PPCN-NBR
                ldaFcpl190.getPnd_Extract_Record_Cntrct_Ppcn_Nbr());
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Build_Control_Record() throws Exception                                                                                                              //Natural: BUILD-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1045 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #SS-CONTROL OR #RECORD-COUNTER = 0
        if (condition(pnd_Ss_Control.getBoolean() || pnd_Record_Counter.equals(getZero())))
        {
            decideConditionsMet1045++;
            ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde().setValue("SS");                                                                                          //Natural: ASSIGN #CNTL-ORGN-CDE := 'SS'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CONTROL-DATA
            sub_Obtain_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #DC-CONTROL
        if (condition(pnd_Dc_Control.getBoolean()))
        {
            decideConditionsMet1045++;
            ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde().setValue("DC");                                                                                          //Natural: ASSIGN #CNTL-ORGN-CDE := 'DC'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CONTROL-DATA
            sub_Obtain_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #CI-CONTROL
        if (condition(pnd_Ci_Control.getBoolean()))
        {
            decideConditionsMet1045++;
            ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde().setValue("CI");                                                                                          //Natural: ASSIGN #CNTL-ORGN-CDE := 'CI'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CONTROL-DATA
            sub_Obtain_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN #CM-CONTROL
        if (condition(pnd_Cm_Control.getBoolean()))
        {
            decideConditionsMet1045++;
            ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde().setValue("CM");                                                                                          //Natural: ASSIGN #CNTL-ORGN-CDE := 'CM'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CONTROL-DATA
            sub_Obtain_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NOT #NZ-CONTROL-DATA-OBTAINED
        if (condition(! ((pnd_Nz_Control_Data_Obtained.getBoolean()))))
        {
            decideConditionsMet1045++;
            ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde().setValue("NZ");                                                                                          //Natural: ASSIGN #CNTL-ORGN-CDE := 'NZ'
            //*  05-18-98
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CONTROL-DATA
            sub_Obtain_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NOT #AL-CONTROL-DATA-OBTAINED
        if (condition(! ((pnd_Al_Control_Data_Obtained.getBoolean()))))
        {
            decideConditionsMet1045++;
            ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde().setValue("AL");                                                                                          //Natural: ASSIGN #CNTL-ORGN-CDE := 'AL'
            //*  02-10-00
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CONTROL-DATA
            sub_Obtain_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NOT #EW-CONTROL-DATA-OBTAINED
        if (condition(! ((pnd_Ew_Control_Data_Obtained.getBoolean()))))
        {
            decideConditionsMet1045++;
            ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde().setValue("EW");                                                                                          //Natural: ASSIGN #CNTL-ORGN-CDE := 'EW'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-CONTROL-DATA
            sub_Obtain_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NOT #AP-CONTROL-DATA-OBTAINED
        if (condition(! ((pnd_Ap_Control_Data_Obtained.getBoolean()))))
        {
            decideConditionsMet1045++;
            ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde().setValue("AP");                                                                                          //Natural: ASSIGN #CNTL-ORGN-CDE := 'AP'
                                                                                                                                                                          //Natural: PERFORM OBTAIN-AP-CONTROL-DATA
            sub_Obtain_Ap_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1045 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Obtain_Control_Data() throws Exception                                                                                                               //Natural: OBTAIN-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        ldaFcplcntr.getVw_fcp_Cons_Cntrl().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-CNTRL BY FCP-CONS-CNTRL.CNTL-ORG-CDE-INVRSE-DTE = #CNTL-ORGN-CDE
        (
        "READ02",
        new Wc[] { new Wc("CNTL_ORG_CDE_INVRSE_DTE", ">=", ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde(), WcType.BY) },
        new Oc[] { new Oc("CNTL_ORG_CDE_INVRSE_DTE", "ASC") },
        1
        );
        READ02:
        while (condition(ldaFcplcntr.getVw_fcp_Cons_Cntrl().readNextRow("READ02")))
        {
            if (condition(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().notEquals(ldaFcpl190.getPnd_Cntl_Super_Des_Pnd_Cntl_Orgn_Cde())))                                 //Natural: IF FCP-CONS-CNTRL.CNTL-ORGN-CDE NE #CNTL-ORGN-CDE
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  02/10/00
            short decideConditionsMet1086 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE FCP-CONS-CNTRL.CNTL-ORGN-CDE;//Natural: VALUE 'EW'
            if (condition((ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("EW"))))
            {
                decideConditionsMet1086++;
                //*  05/18/98
                pnd_Ew_Control_Data_Obtained.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #EW-CONTROL-DATA-OBTAINED
            }                                                                                                                                                             //Natural: VALUE 'AL'
            else if (condition((ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("AL"))))
            {
                decideConditionsMet1086++;
                //*  LZ 08/13/97
                pnd_Al_Control_Data_Obtained.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #AL-CONTROL-DATA-OBTAINED
            }                                                                                                                                                             //Natural: VALUE 'NZ'
            else if (condition((ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("NZ"))))
            {
                decideConditionsMet1086++;
                pnd_Nz_Control_Data_Obtained.setValue(true);                                                                                                              //Natural: MOVE TRUE TO #NZ-CONTROL-DATA-OBTAINED
            }                                                                                                                                                             //Natural: VALUE 'SS'
            else if (condition((ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("SS"))))
            {
                decideConditionsMet1086++;
                ignore();
            }                                                                                                                                                             //Natural: VALUE 'DC'
            else if (condition((ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("DC"))))
            {
                decideConditionsMet1086++;
                k.setValue(1);                                                                                                                                            //Natural: ASSIGN K := 1
                //*  DC PYMNTS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(1,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(1,":",3));                                    //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 1:3 ) TO #PYMNT-CNT-TOTAL ( 1,1,1 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(1,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(1,":",3));                            //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 1:3 ) TO #PYMNT-GROSS-TOTAL ( 1,1,1 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(1,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(1,":",3));                                //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 1:3 ) TO #PYMNT-NET-TOTAL ( 1,1,1 )
                //*  DC REDRAWS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(2,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(14,":",16));                                  //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 14:16 ) TO #PYMNT-CNT-TOTAL ( 2,1,1 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(2,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(14,":",16));                          //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 14:16 ) TO #PYMNT-GROSS-TOTAL ( 2,1,1 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(2,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(14,":",16));                              //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 14:16 ) TO #PYMNT-NET-TOTAL ( 2,1,1 )
                //*  DC CANCELS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(4,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(12));                                         //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 12 ) TO #PYMNT-CNT-TOTAL ( 4,1,1 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(4,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(12));                                 //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 12 ) TO #PYMNT-GROSS-TOTAL ( 4,1,1 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(4,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(12));                                     //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 12 ) TO #PYMNT-NET-TOTAL ( 4,1,1 )
                //*  DC STOPS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(5,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(13));                                         //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 13 ) TO #PYMNT-CNT-TOTAL ( 5,1,1 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(5,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(13));                                 //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 13 ) TO #PYMNT-GROSS-TOTAL ( 5,1,1 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(5,1,1).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(13));                                     //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 13 ) TO #PYMNT-NET-TOTAL ( 5,1,1 )
            }                                                                                                                                                             //Natural: VALUE 'CI'
            else if (condition((ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("CI"))))
            {
                decideConditionsMet1086++;
                k.setValue(2);                                                                                                                                            //Natural: ASSIGN K := 2
                //*  IA REDRAWS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(2,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(3));                                          //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 3 ) TO #PYMNT-CNT-TOTAL ( 2,1,2 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(2,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(3));                                  //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 3 ) TO #PYMNT-GROSS-TOTAL ( 2,1,2 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(2,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(3));                                      //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 3 ) TO #PYMNT-NET-TOTAL ( 2,1,2 )
                //*  IA CANCELS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(4,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(1));                                          //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 1 ) TO #PYMNT-CNT-TOTAL ( 4,1,2 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(4,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(1));                                  //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 1 ) TO #PYMNT-GROSS-TOTAL ( 4,1,2 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(4,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(1));                                      //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 1 ) TO #PYMNT-NET-TOTAL ( 4,1,2 )
                //*  IA STOPS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(5,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(2));                                          //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 2 ) TO #PYMNT-CNT-TOTAL ( 5,1,2 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(5,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(2));                                  //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 2 ) TO #PYMNT-GROSS-TOTAL ( 5,1,2 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(5,1,2).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(2));                                      //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 2 ) TO #PYMNT-NET-TOTAL ( 5,1,2 )
            }                                                                                                                                                             //Natural: VALUE 'CM'
            else if (condition((ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("CM"))))
            {
                decideConditionsMet1086++;
                k.setValue(3);                                                                                                                                            //Natural: ASSIGN K := 3
                //*  MS REDRAWS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(2,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(27,":",32));                                  //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 27:32 ) TO #PYMNT-CNT-TOTAL ( 2,1,3 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(2,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(27,":",32));                          //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 27:32 ) TO #PYMNT-GROSS-TOTAL ( 2,1,3 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(2,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(27,":",32));                              //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 27:32 ) TO #PYMNT-NET-TOTAL ( 2,1,3 )
                //*  MS CANCELS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(4,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(25));                                         //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 25 ) TO #PYMNT-CNT-TOTAL ( 4,1,3 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(4,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(25));                                 //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 25 ) TO #PYMNT-GROSS-TOTAL ( 4,1,3 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(4,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(25));                                     //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 25 ) TO #PYMNT-NET-TOTAL ( 4,1,3 )
                //*  MS STOPS
                pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(5,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(26));                                         //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 26 ) TO #PYMNT-CNT-TOTAL ( 5,1,3 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(5,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(26));                                 //Natural: ADD FCP-CONS-CNTRL.CNTL-GROSS-AMT ( 26 ) TO #PYMNT-GROSS-TOTAL ( 5,1,3 )
                pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(5,1,3).nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(26));                                     //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 26 ) TO #PYMNT-NET-TOTAL ( 5,1,3 )
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1086 > 0))
            {
                //*  05-18-98
                if (condition(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("DC") || ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("CI")                    //Natural: IF ( FCP-CONS-CNTRL.CNTL-ORGN-CDE = 'DC' OR = 'CI' OR = 'CM' )
                    || ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("CM")))
                {
                    //* *** ACCUMULATE TOTAL PAYMENTS AND REDRAWS FOR 'DC', 'CI', AND 'CM'
                    //* *** IN ONE BUCKET.
                    //*  TOT PMT & RDW
                    pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(3,1,1).nadd(pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(1,":",2,1,k));                                //Natural: ADD #PYMNT-CNT-TOTAL ( 1:2,1,K ) TO #PYMNT-CNT-TOTAL ( 3,1,1 )
                    //*  TOT PMT & RDW
                    pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(3,1,1).nadd(pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(1,":",2,1,k));                            //Natural: ADD #PYMNT-GROSS-TOTAL ( 1:2,1,K ) TO #PYMNT-GROSS-TOTAL ( 3,1,1 )
                    pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(3,1,1).nadd(pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(1,":",2,1,k));                                //Natural: ADD #PYMNT-NET-TOTAL ( 1:2,1,K ) TO #PYMNT-NET-TOTAL ( 3,1,1 )
                }                                                                                                                                                         //Natural: END-IF
                //* *** INCREMENT CHECK TOTALS - WF 13 CNTRL DATA
                pnd_Control_Record_Pnd_Cntl_Cnt_Check.nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(48));                                                        //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 48 ) TO #CNTL-CNT-CHECK
                pnd_Control_Record_Pnd_Cntl_Net_Amt_Check.nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(48));                                                //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 48 ) TO #CNTL-NET-AMT-CHECK
                //* *** INCREMENT EFT TOTALS - WF 13 CNTRL DATA
                pnd_Control_Record_Pnd_Cntl_Cnt_Eft.nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(49));                                                          //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 49 ) TO #CNTL-CNT-EFT
                pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft.nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(49));                                                  //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 49 ) TO #CNTL-NET-AMT-EFT
                //* *** INCREMENT INT-ROLL TOTALS - WF 13 CNTRL DATA
                pnd_Control_Record_Pnd_Cntl_Cnt_Int_Roll.nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(50));                                                     //Natural: ADD FCP-CONS-CNTRL.CNTL-CNT ( 50 ) TO #CNTL-CNT-INT-ROLL
                pnd_Control_Record_Pnd_Cntl_Net_Amt_Int_Roll.nadd(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(50));                                             //Natural: ADD FCP-CONS-CNTRL.CNTL-NET-AMT ( 50 ) TO #CNTL-NET-AMT-INT-ROLL
                //* *** GET RUN DATE
                if (condition(! (pnd_Date_Obtained.getBoolean())))                                                                                                        //Natural: IF NOT #DATE-OBTAINED
                {
                    pnd_Control_Record_Pnd_Cntl_Payment_Date.setValueEdited(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Check_Dte(),new ReportEditMask("YYYYMMDD"));               //Natural: MOVE EDITED FCP-CONS-CNTRL.CNTL-CHECK-DTE ( EM = YYYYMMDD ) TO #CONTROL-RECORD.#CNTL-PAYMENT-DATE
                    pnd_Date_Obtained.setValue(true);                                                                                                                     //Natural: ASSIGN #DATE-OBTAINED := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Obtain_Ap_Control_Data() throws Exception                                                                                                            //Natural: OBTAIN-AP-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        getWorkFiles().read(15, pdaFcpacntl.getCntl());                                                                                                                   //Natural: READ WORK FILE 15 ONCE CNTL
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Ap_Control_Data_Obtained.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #AP-CONTROL-DATA-OBTAINED
        pnd_Control_Record_Pnd_Cntl_Cnt_Check.nadd(pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue(1));                                                                     //Natural: ADD CNTL.CNTL-PYMNT-CNT ( 1 ) TO #CNTL-CNT-CHECK
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Check.nadd(pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(1));                                                                   //Natural: ADD CNTL.CNTL-NET-AMT ( 1 ) TO #CNTL-NET-AMT-CHECK
    }
    private void sub_Accum_Control_Data() throws Exception                                                                                                                //Natural: ACCUM-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  11/22/96
        if (condition(pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Orgn_Cde().equals("SS") || pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Orgn_Cde().equals("DS")))                          //Natural: IF #FCPASSCN.CNTRCT-ORGN-CDE = 'SS' OR = 'DS'
        {
            ldaFcplssc1.getPnd_Fcplssc1_Inv_Acct_Settl_Amt().getValue(1).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Settl_Amt());                                 //Natural: MOVE #EXTRACT-RECORD.INV-ACCT-SETTL-AMT TO #FCPLSSC1.INV-ACCT-SETTL-AMT ( 1 )
            ldaFcplssc1.getPnd_Fcplssc1_Inv_Acct_Dvdnd_Amt().getValue(1).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dvdnd_Amt());                                 //Natural: MOVE #EXTRACT-RECORD.INV-ACCT-DVDND-AMT TO #FCPLSSC1.INV-ACCT-DVDND-AMT ( 1 )
            ldaFcplssc1.getPnd_Fcplssc1_Inv_Acct_Dci_Amt().getValue(1).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dci_Amt());                                     //Natural: MOVE #EXTRACT-RECORD.INV-ACCT-DCI-AMT TO #FCPLSSC1.INV-ACCT-DCI-AMT ( 1 )
            ldaFcplssc1.getPnd_Fcplssc1_Inv_Acct_Dpi_Amt().getValue(1).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Dpi_Amt());                                     //Natural: MOVE #EXTRACT-RECORD.INV-ACCT-DPI-AMT TO #FCPLSSC1.INV-ACCT-DPI-AMT ( 1 )
            ldaFcplssc1.getPnd_Fcplssc1_Inv_Acct_Net_Pymnt_Amt().getValue(1).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt());                         //Natural: MOVE #EXTRACT-RECORD.INV-ACCT-NET-PYMNT-AMT TO #FCPLSSC1.INV-ACCT-NET-PYMNT-AMT ( 1 )
            ldaFcplssc1.getPnd_Fcplssc1_Inv_Acct_Fdrl_Tax_Amt().getValue(1).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Fdrl_Tax_Amt());                           //Natural: MOVE #EXTRACT-RECORD.INV-ACCT-FDRL-TAX-AMT TO #FCPLSSC1.INV-ACCT-FDRL-TAX-AMT ( 1 )
            ldaFcplssc1.getPnd_Fcplssc1_Inv_Acct_State_Tax_Amt().getValue(1).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_State_Tax_Amt());                         //Natural: MOVE #EXTRACT-RECORD.INV-ACCT-STATE-TAX-AMT TO #FCPLSSC1.INV-ACCT-STATE-TAX-AMT ( 1 )
            ldaFcplssc1.getPnd_Fcplssc1_Inv_Acct_Local_Tax_Amt().getValue(1).setValue(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Local_Tax_Amt());                         //Natural: MOVE #EXTRACT-RECORD.INV-ACCT-LOCAL-TAX-AMT TO #FCPLSSC1.INV-ACCT-LOCAL-TAX-AMT ( 1 )
            DbsUtil.callnat(Fcpnssc1.class , getCurrentProcessState(), pdaFcpassc1.getPnd_Fcpassc1(), pdaFcpassc2.getPnd_Fcpassc2(), ldaFcplssc1.getPnd_Fcplssc1_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNSSC1' USING #FCPASSC1 #FCPASSC2 #FCPLSSC1.#MAX-FUND #FCPLSSC1.#CNTRL-INV-ACCT ( 1:#MAX-FUND )
                ldaFcplssc1.getPnd_Fcplssc1_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplssc1.getPnd_Fcplssc1_Pnd_Max_Fund()));
            if (condition(Global.isEscape())) return;
            pdaFcpassc2.getPnd_Fcpassc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                              //Natural: ASSIGN #NEW-PYMNT-IND := FALSE
            //*  DC/IA/MS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pymnt_Settl_Amt.nadd(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Settl_Amt());                                                                              //Natural: ADD #EXTRACT-RECORD.INV-ACCT-SETTL-AMT TO #PYMNT-SETTL-AMT
            pnd_Pymnt_Net_Amt.nadd(ldaFcpl190.getPnd_Extract_Record_Inv_Acct_Net_Pymnt_Amt());                                                                            //Natural: ADD #EXTRACT-RECORD.INV-ACCT-NET-PYMNT-AMT TO #PYMNT-NET-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Set_Control_Data() throws Exception                                                                                                                  //Natural: SET-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pdaFcpasscn.getPnd_Fcpasscn_Pymnt_Pay_Type_Req_Ind().setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Pay_Type_Req_Ind());                                         //Natural: MOVE #EXTRACT-RECORD.PYMNT-PAY-TYPE-REQ-IND TO #FCPASSCN.PYMNT-PAY-TYPE-REQ-IND
        pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Orgn_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde());                                                       //Natural: MOVE #EXTRACT-RECORD.CNTRCT-ORGN-CDE TO #FCPASSCN.CNTRCT-ORGN-CDE
        pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Type_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Type_Cde());                                                       //Natural: MOVE #EXTRACT-RECORD.CNTRCT-TYPE-CDE TO #FCPASSCN.CNTRCT-TYPE-CDE
        pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde());                           //Natural: MOVE #EXTRACT-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE TO #FCPASSCN.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        //*  11/22/96
        if (condition(pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Orgn_Cde().equals("SS") || pdaFcpasscn.getPnd_Fcpasscn_Cntrct_Orgn_Cde().equals("DS")))                          //Natural: IF #FCPASSCN.CNTRCT-ORGN-CDE = 'SS' OR = 'DS'
        {
            DbsUtil.callnat(Fcpnsscn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpasscn.getPnd_Fcpasscn(), pdaFcpassc1.getPnd_Fcpassc1());      //Natural: CALLNAT 'FCPNSSCN' USING #FCPACRPT #FCPASSCN #FCPASSC1
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Ss_Control_Report() throws Exception                                                                                                           //Natural: PRINT-SS-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue("*").setValue(true);                                                                                       //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( * ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue(" SINGLE SUM BATCH UPDATE CONTROL REPORT");                                                                      //Natural: ASSIGN #FCPACRPT.#TITLE := ' SINGLE SUM BATCH UPDATE CONTROL REPORT'
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(true);                                                                                                        //Natural: ASSIGN #FCPACRPT.#NO-ABEND := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
        DbsUtil.callnat(Fcpnssc2.class , getCurrentProcessState(), pdaFcpassc2.getPnd_Fcpassc2(), pdaFcpacrpt.getPnd_Fcpacrpt());                                         //Natural: CALLNAT 'FCPNSSC2' USING #FCPASSC2 #FCPACRPT
        if (condition(Global.isEscape())) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Control_Total_Comparison_Dc_Ci_Cm() throws Exception                                                                                                 //Natural: CONTROL-TOTAL-COMPARISON-DC-CI-CM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-COLUMN-HEADINGS
        sub_Print_Column_Headings();
        if (condition(Global.isEscape())) {return;}
        FOR02:                                                                                                                                                            //Natural: FOR J 1 5
        for (j.setValue(1); condition(j.lessOrEqual(5)); j.nadd(1))
        {
            if (condition(j.equals(3) || j.equals(4)))                                                                                                                    //Natural: IF J = 3 OR = 4
            {
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 1 ) 1
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR L 1 3
            for (l.setValue(1); condition(l.lessOrEqual(3)); l.nadd(1))
            {
                if (condition((pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,1,l).greater(getZero())) || (pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,              //Natural: IF ( #PYMNT-CNT-TOTAL ( J,1,L ) > 0 ) OR ( #PYMNT-CNT-TOTAL ( J,2,L ) > 0 )
                    2,l).greater(getZero()))))
                {
                    if (condition((pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,1,l).notEquals(pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,2,l)))                  //Natural: IF ( #PYMNT-CNT-TOTAL ( J,1,L ) NE #PYMNT-CNT-TOTAL ( J,2,L ) ) OR ( #PYMNT-GROSS-TOTAL ( J,1,L ) NE #PYMNT-GROSS-TOTAL ( J,2,L ) ) OR ( #PYMNT-NET-TOTAL ( J,1,L ) NE #PYMNT-NET-TOTAL ( J,2,L ) )
                        || (pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,1,l).notEquals(pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,2,l))) || 
                        (pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,1,l).notEquals(pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,2,l)))))
                    {
                        pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,3,l).compute(new ComputeParameters(false, pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,3,l)),  //Natural: ASSIGN #PYMNT-CNT-TOTAL ( J,3,L ) := #PYMNT-CNT-TOTAL ( J,1,L ) - #PYMNT-CNT-TOTAL ( J,2,L )
                            pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,1,l).subtract(pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,2,l)));
                        pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,3,l).compute(new ComputeParameters(false, pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,3,l)),  //Natural: ASSIGN #PYMNT-GROSS-TOTAL ( J,3,L ) := #PYMNT-GROSS-TOTAL ( J,1,L ) - #PYMNT-GROSS-TOTAL ( J,2,L )
                            pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,1,l).subtract(pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,2,l)));
                        pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,3,l).compute(new ComputeParameters(false, pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,3,l)),  //Natural: ASSIGN #PYMNT-NET-TOTAL ( J,3,L ) := #PYMNT-NET-TOTAL ( J,1,L ) - #PYMNT-NET-TOTAL ( J,2,L )
                            pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,1,l).subtract(pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,2,l)));
                        if (condition(l.equals(1)))                                                                                                                       //Natural: IF L = 1
                        {
                            pnd_Dc_Correct.setValue(false);                                                                                                               //Natural: ASSIGN #DC-CORRECT := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(l.equals(2)))                                                                                                                       //Natural: IF L = 2
                        {
                            pnd_Ia_Csr_Correct.setValue(false);                                                                                                           //Natural: ASSIGN #IA-CSR-CORRECT := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(l.equals(3)))                                                                                                                       //Natural: IF L = 3
                        {
                            pnd_Ms_Csr_Correct.setValue(false);                                                                                                           //Natural: ASSIGN #MS-CSR-CORRECT := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ORGN-CSR-LITERAL
                    sub_Determine_Orgn_Csr_Literal();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                    sub_Print_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition((j.equals(3)) && (l.equals(1))))                                                                                                        //Natural: IF ( J = 3 ) AND ( L = 1 )
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Dc_Correct.equals(false) || pnd_Ia_Csr_Correct.equals(false) || pnd_Ms_Csr_Correct.equals(false)))                                              //Natural: IF #DC-CORRECT = FALSE OR #IA-CSR-CORRECT = FALSE OR #MS-CSR-CORRECT = FALSE
        {
            pnd_Cntl_Message_1.setValue("CONTROL AND EXTRACT TOTALS ARE MIS-MATCHED.");                                                                                   //Natural: MOVE 'CONTROL AND EXTRACT TOTALS ARE MIS-MATCHED.' TO #CNTL-MESSAGE-1
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-MESSAGE
            sub_Print_Error_Message();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 0099
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Printed.getBoolean())))                                                                                                                      //Natural: IF NOT #PRINTED
        {
            j.setValue(3);                                                                                                                                                //Natural: ASSIGN J := 3
            l.setValue(1);                                                                                                                                                //Natural: ASSIGN L := 1
            pnd_Orgn_Csr_Literal.setValue("TOTAL PYMNTS    ");                                                                                                            //Natural: ASSIGN #ORGN-CSR-LITERAL := 'TOTAL PYMNTS    '
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
            sub_Print_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Column_Headings() throws Exception                                                                                                             //Natural: PRINT-COLUMN-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"-",new RepeatItem(132),NEWLINE,new TabSetting(35),"CONTROL",new TabSetting(57),":",new                //Natural: WRITE ( 1 ) NOTITLE // '-' ( 132 ) / 35T 'CONTROL' 57T ':' 73T 'EXTRACT' 95T ':' 110T 'DIFFERENCE' / '-' ( 132 ) ///31T'CONTRACTUAL' 68T'CONTRACTUAL' 106T'CONTRACTUAL' / 21T'  COUNT      GROSS          NET' 58T '  COUNT      GROSS          NET' 96T '  COUNT      GROSS          NET' / 21T '--------- ------------ ------------' 58T '--------- ------------ ------------' 95T '--------- ------------ ------------' /
            TabSetting(73),"EXTRACT",new TabSetting(95),":",new TabSetting(110),"DIFFERENCE",NEWLINE,"-",new RepeatItem(132),NEWLINE,NEWLINE,NEWLINE,new 
            TabSetting(31),"CONTRACTUAL",new TabSetting(68),"CONTRACTUAL",new TabSetting(106),"CONTRACTUAL",NEWLINE,new TabSetting(21),"  COUNT      GROSS          NET",new 
            TabSetting(58),"  COUNT      GROSS          NET",new TabSetting(96),"  COUNT      GROSS          NET",NEWLINE,new TabSetting(21),"--------- ------------ ------------",new 
            TabSetting(58),"--------- ------------ ------------",new TabSetting(95),"--------- ------------ ------------",NEWLINE);
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Orgn_Csr_Literal() throws Exception                                                                                                        //Natural: DETERMINE-ORGN-CSR-LITERAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1321 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE J;//Natural: VALUE 1
        if (condition((j.equals(1))))
        {
            decideConditionsMet1321++;
            pnd_Csr_Literal.setValue("PYMNTS ");                                                                                                                          //Natural: ASSIGN #CSR-LITERAL := 'PYMNTS '
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((j.equals(2))))
        {
            decideConditionsMet1321++;
            pnd_Csr_Literal.setValue("REDRAWS");                                                                                                                          //Natural: ASSIGN #CSR-LITERAL := 'REDRAWS'
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((j.equals(3))))
        {
            decideConditionsMet1321++;
            pnd_Orgn_Literal.setValue("TOTAL   ");                                                                                                                        //Natural: ASSIGN #ORGN-LITERAL := 'TOTAL   '
            pnd_Csr_Literal.setValue("PYMNTS ");                                                                                                                          //Natural: ASSIGN #CSR-LITERAL := 'PYMNTS '
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((j.equals(4))))
        {
            decideConditionsMet1321++;
            pnd_Csr_Literal.setValue("CANCELS");                                                                                                                          //Natural: ASSIGN #CSR-LITERAL := 'CANCELS'
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((j.equals(5))))
        {
            decideConditionsMet1321++;
            pnd_Csr_Literal.setValue("STOPS  ");                                                                                                                          //Natural: ASSIGN #CSR-LITERAL := 'STOPS  '
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet1321 > 0))
        {
            if (condition(j.notEquals(3)))                                                                                                                                //Natural: IF J NE 3
            {
                if (condition(l.equals(1)))                                                                                                                               //Natural: IF L = 1
                {
                    pnd_Orgn_Literal.setValue("IA DEATH");                                                                                                                //Natural: ASSIGN #ORGN-LITERAL := 'IA DEATH'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(l.equals(2)))                                                                                                                               //Natural: IF L = 2
                {
                    pnd_Orgn_Literal.setValue("IA      ");                                                                                                                //Natural: ASSIGN #ORGN-LITERAL := 'IA      '
                }                                                                                                                                                         //Natural: END-IF
                if (condition(l.equals(3)))                                                                                                                               //Natural: IF L = 3
                {
                    pnd_Orgn_Literal.setValue("MS      ");                                                                                                                //Natural: ASSIGN #ORGN-LITERAL := 'MS      '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Orgn_Csr_Literal.setValue(DbsUtil.compress(pnd_Orgn_Literal, pnd_Csr_Literal));                                                                           //Natural: COMPRESS #ORGN-LITERAL #CSR-LITERAL INTO #ORGN-CSR-LITERAL
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,pnd_Orgn_Csr_Literal,new TabSetting(21),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,1,l), new ReportEditMask       //Natural: WRITE ( 1 ) NOTITLE #ORGN-CSR-LITERAL 21T #PYMNT-CNT-TOTAL ( J,1,L ) 31T #PYMNT-GROSS-TOTAL ( J,1,L ) 44T #PYMNT-NET-TOTAL ( J,1,L ) 58T #PYMNT-CNT-TOTAL ( J,2,L ) 68T #PYMNT-GROSS-TOTAL ( J,2,L ) 81T #PYMNT-NET-TOTAL ( J,2,L ) 95T #PYMNT-CNT-TOTAL ( J,3,L ) 105T #PYMNT-GROSS-TOTAL ( J,3,L ) 118T #PYMNT-NET-TOTAL ( J,3,L ) /
            ("ZZZZZZZZ9"),new TabSetting(31),pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,1,l), new ReportEditMask ("ZZZZZZZZ9.99"),new TabSetting(44),pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,1,l), 
            new ReportEditMask ("ZZZZZZZZ9.99"),new TabSetting(58),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,2,l), new ReportEditMask ("ZZZZZZZZ9"),new 
            TabSetting(68),pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,2,l), new ReportEditMask ("ZZZZZZZZ9.99"),new TabSetting(81),pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,2,l), 
            new ReportEditMask ("ZZZZZZZZ9.99"),new TabSetting(95),pnd_Pymnt_Totals_Pnd_Pymnt_Cnt_Total.getValue(j,3,l), new ReportEditMask ("ZZZZZZZZ9"),new 
            TabSetting(105),pnd_Pymnt_Totals_Pnd_Pymnt_Gross_Total.getValue(j,3,l), new ReportEditMask ("ZZZZZZZZ9.99"),new TabSetting(118),pnd_Pymnt_Totals_Pnd_Pymnt_Net_Total.getValue(j,3,l), 
            new ReportEditMask ("ZZZZZZZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        pnd_Printed.setValue(true);                                                                                                                                       //Natural: ASSIGN #PRINTED := TRUE
    }
    private void sub_Print_Error_Message() throws Exception                                                                                                               //Natural: PRINT-ERROR-MESSAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*ERROR****ERROR****ERROR****ERROR****ERROR*",NEWLINE,new           //Natural: WRITE ( 1 ) NOTITLE /// / 45T '*ERROR****ERROR****ERROR****ERROR****ERROR*' / 45T '****                                   ****' / 45T #CNTL-MESSAGE-1 / 45T '****                                   ****' / 45T '*ERROR****ERROR****ERROR****ERROR****ERROR*'
            TabSetting(45),"****                                   ****",NEWLINE,new TabSetting(45),pnd_Cntl_Message_1,NEWLINE,new TabSetting(45),"****                                   ****",NEWLINE,new 
            TabSetting(45),"*ERROR****ERROR****ERROR****ERROR****ERROR*");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),"           CONSOLIDATED PAYMENT SYSTEM            ",new  //Natural: WRITE ( 1 ) NOTITLE *INIT-USER '-' *PROGRAM 41T '           CONSOLIDATED PAYMENT SYSTEM            ' 122T *DATX ( EM = LLL' 'DD','YY ) / 39T 'On-line Payment Extract Report for IA Death, IA and MS' 122T *TIMX ( EM = HH':'II' 'AP ) / 41T '                 Control Report                   ' //
                        TabSetting(122),Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),NEWLINE,new TabSetting(39),"On-line Payment Extract Report for IA Death, IA and MS",new 
                        TabSetting(122),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(41),"                 Control Report                   ",
                        NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(15, "LS=133 PS=58 ZP=ON");
    }
    private void CheckAtStartofData668() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-SWITCH
            sub_Set_Control_Switch();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Origin_Cd.setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Orgn_Cde());                                                                                //Natural: ASSIGN #WS-ORIGIN-CD := #EXTRACT-RECORD.CNTRCT-ORGN-CDE
            pnd_Ws_Pay_Type_Req_Ind.setValue(ldaFcpl190.getPnd_Extract_Record_Pymnt_Pay_Type_Req_Ind());                                                                  //Natural: ASSIGN #WS-PAY-TYPE-REQ-IND := #EXTRACT-RECORD.PYMNT-PAY-TYPE-REQ-IND
            pnd_Ws_Cancel_Rdrw_Ind.setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Ind());                                                                   //Natural: ASSIGN #WS-CANCEL-RDRW-IND := #EXTRACT-RECORD.CNTRCT-CANCEL-RDRW-IND
            pnd_Ws_Cancel_Rdrw_Act.setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Cancel_Rdrw_Actvty_Cde());                                                            //Natural: ASSIGN #WS-CANCEL-RDRW-ACT := #EXTRACT-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            pnd_Ws_Invrse_Dt.setValue(ldaFcpl190.getPnd_Extract_Record_Cntrct_Invrse_Dte());                                                                              //Natural: ASSIGN #WS-INVRSE-DT := #EXTRACT-RECORD.CNTRCT-INVRSE-DTE
        }                                                                                                                                                                 //Natural: END-START
    }
}
