/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:14:39 PM
**        * FROM NATURAL PROGRAM : Fcpp0702
************************************************************
**        * FILE NAME            : Fcpp0702.java
**        * CLASS NAME           : Fcpp0702
**        * INSTANCE NAME        : Fcpp0702
************************************************************
**--------------------------------------------------------------------**
** PROGRAM    :  FCPP0702                                             **
** APPLICATION:  CONSOLIDATED PAYMENT SYSTEM                          **
**            :  INVESTMENT SOLUTIONS - RETIREE INCOME WEB (DETAIL)   **
** DATE       :  2011/05/12                                           **
** DESCRIPTION:  GET 2 YEARS WORTH OF INFORMATION FOR THE MONTHLY RUN **
**            :  AND PREVIOUS DAY  DATA ON A DAILY RUN                **
** AUTHOR     :  R.CARREON                                            **
**            :                                                       **
**            :                                                       **
** HISTORY    :  F.ALLIE MODIFIED PROGRAM TO OBTAIN PREVIOUS DAYS DATA**
**                                                                    **
**            :  COGNIZANT - 07/08/2014- INC2495502                   **
**                         - MODIFIED THE START DATE TO 01/01/2014    **
**                         - TAG CTS                                  **
**            :  COGNIZANT - 5/29/2015 - PRB69289                     **
**                         - MODFICATION OF CODE CALCULATE START DATE **
**                           WHEN THE INPUT DATE IS 1ST OF JULY       **
**                         - TAG MUKHERR                              **
**--------------------------------------------------------------------**

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp0702 extends BLNatBase
{
    // Data Areas
    private LdaFcpl702a ldaFcpl702a;
    private LdaFcpl702b ldaFcpl702b;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Datd;
    private DbsField pnd_Day;
    private DbsField pnd_Date_A;

    private DbsGroup pnd_Date_A__R_Field_1;
    private DbsField pnd_Date_A_Pnd_Datn;
    private DbsField pnd_Date;
    private DbsField pnd_Date_Start;

    private DbsGroup pnd_Date_Start__R_Field_2;
    private DbsField pnd_Date_Start_Pnd_S_Yy;
    private DbsField pnd_Date_Start_Pnd_S_Mm;
    private DbsField pnd_Date_Start_Pnd_S_Dd;
    private DbsField pnd_Date_End;

    private DbsGroup pnd_Date_End__R_Field_3;
    private DbsField pnd_Date_End_Pnd_E_Yy;
    private DbsField pnd_Date_End_Pnd_E_Mm;
    private DbsField pnd_Date_End_Pnd_E_Dd;
    private DbsField pnd_Start_Invrse_Date;
    private DbsField pnd_End_Invrse_Date;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Orgn_Status_Invrse_Seq;

    private DbsGroup pnd_Orgn_Status_Invrse_Seq__R_Field_4;
    private DbsField pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde;
    private DbsField pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde;
    private DbsField pnd_Orgn_Status_Invrse_Seq_Cntrct_Invrse_Dte;

    private DbsGroup pnd_Orgn_Table;
    private DbsField pnd_Orgn_Table_Pnd_Orgn1;
    private DbsField pnd_Orgn_Table_Pnd_Orgn2;
    private DbsField pnd_Orgn_Table_Pnd_Orgn3;
    private DbsField pnd_Orgn_Table_Pnd_Orgn4;
    private DbsField pnd_Orgn_Table_Pnd_Orgn5;
    private DbsField pnd_Orgn_Table_Pnd_Orgn6;
    private DbsField pnd_Orgn_Table_Pnd_Orgn7;
    private DbsField pnd_Orgn_Table_Pnd_Orgn8;
    private DbsField pnd_Orgn_Table_Pnd_Orgn9;
    private DbsField pnd_Orgn_Table_Pnd_Orgn10;
    private DbsField pnd_Orgn_Table_Pnd_Orgn11;
    private DbsField pnd_Orgn_Table_Pnd_Orgn12;
    private DbsField pnd_Orgn_Table_Pnd_Orgn13;
    private DbsField pnd_Orgn_Table_Pnd_Orgn14;
    private DbsField pnd_Orgn_Table_Pnd_Orgn15;

    private DbsGroup pnd_Orgn_Table__R_Field_5;
    private DbsField pnd_Orgn_Table_Pnd_Orgn_Array;
    private DbsField pnd_Stat_Array;
    private DbsField pnd_R;
    private DbsField pnd_A;
    private DbsField pnd_Year;
    private DbsField pnd_Addr_Key;

    private DbsGroup pnd_Addr_Key__R_Field_6;
    private DbsField pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde;
    private DbsField pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde;
    private DbsField pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte;
    private DbsField pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl702a = new LdaFcpl702a();
        registerRecord(ldaFcpl702a);
        registerRecord(ldaFcpl702a.getVw_pay());
        registerRecord(ldaFcpl702a.getVw_addr());
        ldaFcpl702b = new LdaFcpl702b();
        registerRecord(ldaFcpl702b);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Datd = localVariables.newFieldInRecord("pnd_Datd", "#DATD", FieldType.DATE);
        pnd_Day = localVariables.newFieldInRecord("pnd_Day", "#DAY", FieldType.STRING, 10);
        pnd_Date_A = localVariables.newFieldInRecord("pnd_Date_A", "#DATE-A", FieldType.STRING, 8);

        pnd_Date_A__R_Field_1 = localVariables.newGroupInRecord("pnd_Date_A__R_Field_1", "REDEFINE", pnd_Date_A);
        pnd_Date_A_Pnd_Datn = pnd_Date_A__R_Field_1.newFieldInGroup("pnd_Date_A_Pnd_Datn", "#DATN", FieldType.NUMERIC, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);
        pnd_Date_Start = localVariables.newFieldInRecord("pnd_Date_Start", "#DATE-START", FieldType.NUMERIC, 8);

        pnd_Date_Start__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Start__R_Field_2", "REDEFINE", pnd_Date_Start);
        pnd_Date_Start_Pnd_S_Yy = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_S_Yy", "#S-YY", FieldType.NUMERIC, 4);
        pnd_Date_Start_Pnd_S_Mm = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_S_Mm", "#S-MM", FieldType.NUMERIC, 2);
        pnd_Date_Start_Pnd_S_Dd = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_S_Dd", "#S-DD", FieldType.NUMERIC, 2);
        pnd_Date_End = localVariables.newFieldInRecord("pnd_Date_End", "#DATE-END", FieldType.NUMERIC, 8);

        pnd_Date_End__R_Field_3 = localVariables.newGroupInRecord("pnd_Date_End__R_Field_3", "REDEFINE", pnd_Date_End);
        pnd_Date_End_Pnd_E_Yy = pnd_Date_End__R_Field_3.newFieldInGroup("pnd_Date_End_Pnd_E_Yy", "#E-YY", FieldType.NUMERIC, 4);
        pnd_Date_End_Pnd_E_Mm = pnd_Date_End__R_Field_3.newFieldInGroup("pnd_Date_End_Pnd_E_Mm", "#E-MM", FieldType.NUMERIC, 2);
        pnd_Date_End_Pnd_E_Dd = pnd_Date_End__R_Field_3.newFieldInGroup("pnd_Date_End_Pnd_E_Dd", "#E-DD", FieldType.NUMERIC, 2);
        pnd_Start_Invrse_Date = localVariables.newFieldInRecord("pnd_Start_Invrse_Date", "#START-INVRSE-DATE", FieldType.NUMERIC, 8);
        pnd_End_Invrse_Date = localVariables.newFieldInRecord("pnd_End_Invrse_Date", "#END-INVRSE-DATE", FieldType.NUMERIC, 8);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 10);
        pnd_Orgn_Status_Invrse_Seq = localVariables.newFieldInRecord("pnd_Orgn_Status_Invrse_Seq", "#ORGN-STATUS-INVRSE-SEQ", FieldType.STRING, 11);

        pnd_Orgn_Status_Invrse_Seq__R_Field_4 = localVariables.newGroupInRecord("pnd_Orgn_Status_Invrse_Seq__R_Field_4", "REDEFINE", pnd_Orgn_Status_Invrse_Seq);
        pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde = pnd_Orgn_Status_Invrse_Seq__R_Field_4.newFieldInGroup("pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde", 
            "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde = pnd_Orgn_Status_Invrse_Seq__R_Field_4.newFieldInGroup("pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde", 
            "PYMNT-STATS-CDE", FieldType.STRING, 1);
        pnd_Orgn_Status_Invrse_Seq_Cntrct_Invrse_Dte = pnd_Orgn_Status_Invrse_Seq__R_Field_4.newFieldInGroup("pnd_Orgn_Status_Invrse_Seq_Cntrct_Invrse_Dte", 
            "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);

        pnd_Orgn_Table = localVariables.newGroupInRecord("pnd_Orgn_Table", "#ORGN-TABLE");
        pnd_Orgn_Table_Pnd_Orgn1 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn1", "#ORGN1", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn2 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn2", "#ORGN2", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn3 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn3", "#ORGN3", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn4 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn4", "#ORGN4", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn5 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn5", "#ORGN5", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn6 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn6", "#ORGN6", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn7 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn7", "#ORGN7", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn8 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn8", "#ORGN8", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn9 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn9", "#ORGN9", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn10 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn10", "#ORGN10", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn11 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn11", "#ORGN11", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn12 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn12", "#ORGN12", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn13 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn13", "#ORGN13", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn14 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn14", "#ORGN14", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn15 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn15", "#ORGN15", FieldType.STRING, 2);

        pnd_Orgn_Table__R_Field_5 = localVariables.newGroupInRecord("pnd_Orgn_Table__R_Field_5", "REDEFINE", pnd_Orgn_Table);
        pnd_Orgn_Table_Pnd_Orgn_Array = pnd_Orgn_Table__R_Field_5.newFieldArrayInGroup("pnd_Orgn_Table_Pnd_Orgn_Array", "#ORGN-ARRAY", FieldType.STRING, 
            2, new DbsArrayController(1, 15));
        pnd_Stat_Array = localVariables.newFieldArrayInRecord("pnd_Stat_Array", "#STAT-ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.NUMERIC, 4);
        pnd_Addr_Key = localVariables.newFieldInRecord("pnd_Addr_Key", "#ADDR-KEY", FieldType.STRING, 31);

        pnd_Addr_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Addr_Key__R_Field_6", "REDEFINE", pnd_Addr_Key);
        pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr = pnd_Addr_Key__R_Field_6.newFieldInGroup("pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr", "#A-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde = pnd_Addr_Key__R_Field_6.newFieldInGroup("pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde", "#A-CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde = pnd_Addr_Key__R_Field_6.newFieldInGroup("pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde", "#A-CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte = pnd_Addr_Key__R_Field_6.newFieldInGroup("pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte", "#A-CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr = pnd_Addr_Key__R_Field_6.newFieldInGroup("pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr", "#A-PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl702a.initializeValues();
        ldaFcpl702b.initializeValues();

        localVariables.reset();
        pnd_Orgn_Table_Pnd_Orgn1.setInitialValue("AP");
        pnd_Orgn_Table_Pnd_Orgn2.setInitialValue("OP");
        pnd_Orgn_Table_Pnd_Orgn3.setInitialValue("IR");
        pnd_Orgn_Table_Pnd_Orgn4.setInitialValue("AD");
        pnd_Orgn_Table_Pnd_Orgn5.setInitialValue("SS");
        pnd_Orgn_Table_Pnd_Orgn6.setInitialValue("SI");
        pnd_Orgn_Table_Pnd_Orgn7.setInitialValue("AL");
        pnd_Orgn_Table_Pnd_Orgn8.setInitialValue("DC");
        pnd_Orgn_Table_Pnd_Orgn9.setInitialValue("VT");
        pnd_Orgn_Table_Pnd_Orgn10.setInitialValue("MS");
        pnd_Orgn_Table_Pnd_Orgn11.setInitialValue("DS");
        pnd_Orgn_Table_Pnd_Orgn12.setInitialValue("IA");
        pnd_Orgn_Table_Pnd_Orgn13.setInitialValue("NZ");
        pnd_Orgn_Table_Pnd_Orgn14.setInitialValue("EW");
        pnd_Orgn_Table_Pnd_Orgn15.setInitialValue("NV");
        pnd_Stat_Array.getValue(1).setInitialValue("D");
        pnd_Stat_Array.getValue(2).setInitialValue("C");
        pnd_Stat_Array.getValue(3).setInitialValue("P");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp0702() throws Exception
    {
        super("Fcpp0702");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pnd_Datd.setValue(Global.getDATX());                                                                                                                              //Natural: ASSIGN #DATD := *DATX
        pnd_Date_A.setValueEdited(pnd_Datd,new ReportEditMask("YYYYMMDD"));                                                                                               //Natural: MOVE EDITED #DATD ( EM = YYYYMMDD ) TO #DATE-A
        pnd_Date_Start.setValue(pnd_Date_A_Pnd_Datn);                                                                                                                     //Natural: ASSIGN #DATE-START := #DATN
        //*  #DATE-END := 20120101        /* CTS
        //*  #DATE-END := 20140101        /* MUKHERR
        //*  << MUKHERR
        short decideConditionsMet211 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #S-MM;//Natural: VALUE 01,02,03,04,05,06
        if (condition((pnd_Date_Start_Pnd_S_Mm.equals(1) || pnd_Date_Start_Pnd_S_Mm.equals(2) || pnd_Date_Start_Pnd_S_Mm.equals(3) || pnd_Date_Start_Pnd_S_Mm.equals(4) 
            || pnd_Date_Start_Pnd_S_Mm.equals(5) || pnd_Date_Start_Pnd_S_Mm.equals(6))))
        {
            decideConditionsMet211++;
            pnd_Date_End_Pnd_E_Mm.setValue(1);                                                                                                                            //Natural: ASSIGN #E-MM := 01
            pnd_Date_End_Pnd_E_Dd.setValue(1);                                                                                                                            //Natural: ASSIGN #E-DD := 01
            pnd_Date_End_Pnd_E_Yy.compute(new ComputeParameters(false, pnd_Date_End_Pnd_E_Yy), pnd_Date_Start_Pnd_S_Yy.subtract(1));                                      //Natural: ASSIGN #E-YY := #S-YY - 1
        }                                                                                                                                                                 //Natural: VALUE 07,08,09,10,11,12
        else if (condition((pnd_Date_Start_Pnd_S_Mm.equals(7) || pnd_Date_Start_Pnd_S_Mm.equals(8) || pnd_Date_Start_Pnd_S_Mm.equals(9) || pnd_Date_Start_Pnd_S_Mm.equals(10) 
            || pnd_Date_Start_Pnd_S_Mm.equals(11) || pnd_Date_Start_Pnd_S_Mm.equals(12))))
        {
            decideConditionsMet211++;
            pnd_Date_End_Pnd_E_Mm.setValue(1);                                                                                                                            //Natural: ASSIGN #E-MM := 01
            pnd_Date_End_Pnd_E_Dd.setValue(1);                                                                                                                            //Natural: ASSIGN #E-DD := 01
            pnd_Date_End_Pnd_E_Yy.setValue(pnd_Date_Start_Pnd_S_Yy);                                                                                                      //Natural: ASSIGN #E-YY := #S-YY
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
            //*  >> MUKHERR
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Start_Invrse_Date.compute(new ComputeParameters(false, pnd_Start_Invrse_Date), DbsField.subtract(100000000,pnd_Date_Start));                                  //Natural: ASSIGN #START-INVRSE-DATE := 100000000 - #DATE-START
        pnd_End_Invrse_Date.compute(new ComputeParameters(false, pnd_End_Invrse_Date), DbsField.subtract(100000000,pnd_Date_End));                                        //Natural: ASSIGN #END-INVRSE-DATE := 100000000 - #DATE-END
        pnd_Orgn_Status_Invrse_Seq_Cntrct_Invrse_Dte.setValue(pnd_Start_Invrse_Date);                                                                                     //Natural: ASSIGN #ORGN-STATUS-INVRSE-SEQ.CNTRCT-INVRSE-DTE := #START-INVRSE-DATE
        getReports().write(0, "system date ",Global.getDATX(),"=",pnd_Day,"=",pnd_Date_Start,"=",pnd_Date_End);                                                           //Natural: WRITE 'system date ' *DATX '=' #DAY '=' #DATE-START '=' #DATE-END
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Start_Invrse_Date,NEWLINE,"=",pnd_End_Invrse_Date,NEWLINE,"=",pnd_Date_Start,NEWLINE,"=",pnd_Date_End,NEWLINE);                     //Natural: WRITE '=' #START-INVRSE-DATE / '='#END-INVRSE-DATE / '=' #DATE-START / '='#DATE-END /
        if (Global.isEscape()) return;
        getReports().write(0, " ENTERING THE LOOP:");                                                                                                                     //Natural: WRITE ' ENTERING THE LOOP:'
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #R 1 15
        for (pnd_R.setValue(1); condition(pnd_R.lessOrEqual(15)); pnd_R.nadd(1))
        {
            if (condition(pnd_Orgn_Table_Pnd_Orgn_Array.getValue(pnd_R).equals(" ")))                                                                                     //Natural: IF #ORGN-ARRAY ( #R ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(0, "=",pnd_Orgn_Table_Pnd_Orgn_Array.getValue(pnd_R),pnd_R);                                                                               //Natural: WRITE '=' #ORGN-ARRAY ( #R ) #R
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde.setValue(pnd_Orgn_Table_Pnd_Orgn_Array.getValue(pnd_R));                                                           //Natural: MOVE #ORGN-ARRAY ( #R ) TO #ORGN-STATUS-INVRSE-SEQ.CNTRCT-ORGN-CDE
            FOR02:                                                                                                                                                        //Natural: FOR #A 1 10
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(10)); pnd_A.nadd(1))
            {
                if (condition(pnd_Stat_Array.getValue(pnd_A).equals(" ")))                                                                                                //Natural: IF #STAT-ARRAY ( #A ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "=",pnd_Stat_Array.getValue(pnd_A),pnd_A);                                                                                          //Natural: WRITE '=' #STAT-ARRAY ( #A ) #A
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde.setValue(pnd_Stat_Array.getValue(pnd_A));                                                                      //Natural: MOVE #STAT-ARRAY ( #A ) TO #ORGN-STATUS-INVRSE-SEQ.PYMNT-STATS-CDE
                ldaFcpl702a.getVw_pay().startDatabaseRead                                                                                                                 //Natural: READ PAY BY ORGN-STATUS-INVRSE-SEQ STARTING FROM #ORGN-STATUS-INVRSE-SEQ
                (
                "PAY1",
                new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Orgn_Status_Invrse_Seq, WcType.BY) },
                new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
                );
                PAY1:
                while (condition(ldaFcpl702a.getVw_pay().readNextRow("PAY1")))
                {
                    if (condition(ldaFcpl702a.getPay_Cntrct_Invrse_Dte().greater(pnd_End_Invrse_Date) || ldaFcpl702a.getPay_Pymnt_Stats_Cde().notEquals(pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde)  //Natural: IF PAY.CNTRCT-INVRSE-DTE > #END-INVRSE-DATE OR PAY.PYMNT-STATS-CDE NE #ORGN-STATUS-INVRSE-SEQ.PYMNT-STATS-CDE OR PAY.CNTRCT-ORGN-CDE NE #ORGN-STATUS-INVRSE-SEQ.CNTRCT-ORGN-CDE
                        || ldaFcpl702a.getPay_Cntrct_Orgn_Cde().notEquals(pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde)))
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcpl702a.getPay_Cntrct_Ppcn_Nbr().equals(" ")))                                                                                      //Natural: IF PAY.CNTRCT-PPCN-NBR = ' '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Date.compute(new ComputeParameters(false, pnd_Date), DbsField.subtract(100000000,ldaFcpl702a.getPay_Cntrct_Invrse_Dte()));                        //Natural: ASSIGN #DATE := 100000000 - PAY.CNTRCT-INVRSE-DTE
                                                                                                                                                                          //Natural: PERFORM GET-ADDRESS-INFO
                    sub_Get_Address_Info();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("PAY1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("PAY1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    ldaFcpl702b.getPayout().setValuesByName(ldaFcpl702a.getVw_pay());                                                                                     //Natural: MOVE BY NAME PAY TO PAYOUT
                    //*      MOVE BY NAME ADDR TO PAYOUT
                    ldaFcpl702b.getPayout_Cntrct_Settl_Amt().setValue(ldaFcpl702a.getPay_Cntrct_Settl_Amt());                                                             //Natural: ASSIGN PAYOUT.CNTRCT-SETTL-AMT := PAY.CNTRCT-SETTL-AMT
                    ldaFcpl702b.getPayout_Pymnt_Check_Amt().setValue(ldaFcpl702a.getPay_Pymnt_Check_Amt());                                                               //Natural: ASSIGN PAYOUT.PYMNT-CHECK-AMT := PAY.PYMNT-CHECK-AMT
                    ldaFcpl702b.getPayout_Ph_Last_Name().setValue(ldaFcpl702a.getAddr_Ph_Last_Name());                                                                    //Natural: ASSIGN PAYOUT.PH-LAST-NAME := ADDR.PH-LAST-NAME
                    ldaFcpl702b.getPayout_Ph_First_Name().setValue(ldaFcpl702a.getAddr_Ph_First_Name());                                                                  //Natural: ASSIGN PAYOUT.PH-FIRST-NAME := ADDR.PH-FIRST-NAME
                    ldaFcpl702b.getPayout_Ph_Middle_Name().setValue(ldaFcpl702a.getAddr_Ph_Middle_Name());                                                                //Natural: ASSIGN PAYOUT.PH-MIDDLE-NAME := ADDR.PH-MIDDLE-NAME
                    ldaFcpl702b.getPayout_Pymnt_Nme().setValue(ldaFcpl702a.getAddr_Pymnt_Nme().getValue(1));                                                              //Natural: ASSIGN PAYOUT.PYMNT-NME := ADDR.PYMNT-NME ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Addr_Line1_Txt().setValue(ldaFcpl702a.getAddr_Pymnt_Addr_Line1_Txt().getValue(1));                                        //Natural: ASSIGN PAYOUT.PYMNT-ADDR-LINE1-TXT := ADDR.PYMNT-ADDR-LINE1-TXT ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Addr_Line2_Txt().setValue(ldaFcpl702a.getAddr_Pymnt_Addr_Line2_Txt().getValue(1));                                        //Natural: ASSIGN PAYOUT.PYMNT-ADDR-LINE2-TXT := ADDR.PYMNT-ADDR-LINE2-TXT ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Addr_Line3_Txt().setValue(ldaFcpl702a.getAddr_Pymnt_Addr_Line3_Txt().getValue(1));                                        //Natural: ASSIGN PAYOUT.PYMNT-ADDR-LINE3-TXT := ADDR.PYMNT-ADDR-LINE3-TXT ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Addr_Line4_Txt().setValue(ldaFcpl702a.getAddr_Pymnt_Addr_Line4_Txt().getValue(1));                                        //Natural: ASSIGN PAYOUT.PYMNT-ADDR-LINE4-TXT := ADDR.PYMNT-ADDR-LINE4-TXT ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Addr_Line5_Txt().setValue(ldaFcpl702a.getAddr_Pymnt_Addr_Line5_Txt().getValue(1));                                        //Natural: ASSIGN PAYOUT.PYMNT-ADDR-LINE5-TXT := ADDR.PYMNT-ADDR-LINE5-TXT ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Addr_Line6_Txt().setValue(ldaFcpl702a.getAddr_Pymnt_Addr_Line6_Txt().getValue(1));                                        //Natural: ASSIGN PAYOUT.PYMNT-ADDR-LINE6-TXT := ADDR.PYMNT-ADDR-LINE6-TXT ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Addr_Zip_Cde().setValue(ldaFcpl702a.getAddr_Pymnt_Addr_Zip_Cde().getValue(1));                                            //Natural: ASSIGN PAYOUT.PYMNT-ADDR-ZIP-CDE := ADDR.PYMNT-ADDR-ZIP-CDE ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Postl_Data().setValue(ldaFcpl702a.getAddr_Pymnt_Postl_Data().getValue(1));                                                //Natural: ASSIGN PAYOUT.PYMNT-POSTL-DATA := ADDR.PYMNT-POSTL-DATA ( 1 )
                    ldaFcpl702b.getPayout_Pymnt_Eft_Transit_Id().setValue(ldaFcpl702a.getAddr_Pymnt_Eft_Transit_Id());                                                    //Natural: ASSIGN PAYOUT.PYMNT-EFT-TRANSIT-ID := ADDR.PYMNT-EFT-TRANSIT-ID
                    ldaFcpl702b.getPayout_Pymnt_Eft_Acct_Nbr().setValue(ldaFcpl702a.getAddr_Pymnt_Eft_Acct_Nbr());                                                        //Natural: ASSIGN PAYOUT.PYMNT-EFT-ACCT-NBR := ADDR.PYMNT-EFT-ACCT-NBR
                    ldaFcpl702b.getPayout_Pymnt_Chk_Sav_Ind().setValue(ldaFcpl702a.getAddr_Pymnt_Chk_Sav_Ind());                                                          //Natural: ASSIGN PAYOUT.PYMNT-CHK-SAV-IND := ADDR.PYMNT-CHK-SAV-IND
                    ldaFcpl702b.getPayout_Pymnt_Eft_Dte().setValueEdited(ldaFcpl702a.getPay_Pymnt_Eft_Dte(),new ReportEditMask("YYYYMMDD"));                              //Natural: MOVE EDITED PAY.PYMNT-EFT-DTE ( EM = YYYYMMDD ) TO PAYOUT.PYMNT-EFT-DTE
                    ldaFcpl702b.getPayout_Pymnt_Check_Dte().setValueEdited(ldaFcpl702a.getPay_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                          //Natural: MOVE EDITED PAY.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO PAYOUT.PYMNT-CHECK-DTE
                    pnd_Rec_Process.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-PROCESS
                    //*      WRITE
                    //*        PAYOUT.CNTRCT-ORGN-CDE
                    //*        PAYOUT.CNTRCT-PPCN-NBR
                    //*        PAYOUT.CNTRCT-CMBN-NBR
                    //*        PAYOUT.PYMNT-CHECK-DTE
                    //*        PAYOUT.CNTRCT-INVRSE-DTE
                    //*        PAYOUT.PYMNT-STATS-CDE
                    //*        PAYOUT.PH-LAST-NAME
                    //*        #DATE
                    getWorkFiles().write(1, false, ldaFcpl702b.getPayout());                                                                                              //Natural: WRITE WORK FILE 1 PAYOUT
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "Number of records written to work file:",pnd_Rec_Process);                                                                                 //Natural: WRITE 'Number of records written to work file:' #REC-PROCESS
        if (Global.isEscape()) return;
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDRESS-INFO
        //*  --------------------------------------------------------------------
    }
    private void sub_Get_Address_Info() throws Exception                                                                                                                  //Natural: GET-ADDRESS-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------------------
        ldaFcpl702a.getVw_addr().reset();                                                                                                                                 //Natural: RESET ADDR
        if (condition(ldaFcpl702a.getPay_Pymnt_Reqst_Nbr().notEquals(" ")))                                                                                               //Natural: IF PAY.PYMNT-REQST-NBR NE ' '
        {
            ldaFcpl702a.getVw_addr().startDatabaseFind                                                                                                                    //Natural: FIND ADDR WITH PYMNT-REQST-NBR = PAY.PYMNT-REQST-NBR
            (
            "FIND01",
            new Wc[] { new Wc("PYMNT_REQST_NBR", "=", ldaFcpl702a.getPay_Pymnt_Reqst_Nbr(), WcType.WITH) }
            );
            FIND01:
            while (condition(ldaFcpl702a.getVw_addr().readNextRow("FIND01")))
            {
                ldaFcpl702a.getVw_addr().setIfNotFoundControlFlag(false);
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr.setValue(ldaFcpl702a.getPay_Cntrct_Cmbn_Nbr());                                                                            //Natural: ASSIGN #A-CNTRCT-PPCN-NBR := PAY.CNTRCT-CMBN-NBR
            pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde.setValue(ldaFcpl702a.getPay_Cntrct_Payee_Cde());                                                                          //Natural: ASSIGN #A-CNTRCT-PAYEE-CDE := PAY.CNTRCT-PAYEE-CDE
            pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde.setValue(ldaFcpl702a.getPay_Cntrct_Orgn_Cde());                                                                            //Natural: ASSIGN #A-CNTRCT-ORGN-CDE := PAY.CNTRCT-ORGN-CDE
            pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte.setValue(ldaFcpl702a.getPay_Cntrct_Invrse_Dte());                                                                        //Natural: ASSIGN #A-CNTRCT-INVRSE-DTE := PAY.CNTRCT-INVRSE-DTE
            if (condition(ldaFcpl702a.getPay_Cntrct_Orgn_Cde().equals("VT")))                                                                                             //Natural: IF PAY.CNTRCT-ORGN-CDE = 'VT'
            {
                pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcpl702a.getPay_Pymnt_Prcss_Seq_Num());                                                                //Natural: ASSIGN #A-PYMNT-PRCSS-SEQ-NBR := PAY.PYMNT-PRCSS-SEQ-NUM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr.setValue(0);                                                                                                       //Natural: ASSIGN #A-PYMNT-PRCSS-SEQ-NBR := 0
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl702a.getVw_addr().startDatabaseRead                                                                                                                    //Natural: READ ( 1 ) ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #ADDR-KEY
            (
            "READ01",
            new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Addr_Key, WcType.BY) },
            new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
            1
            );
            READ01:
            while (condition(ldaFcpl702a.getVw_addr().readNextRow("READ01")))
            {
                if (condition(pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr.equals(ldaFcpl702a.getAddr_Cntrct_Ppcn_Nbr()) && pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde.equals(ldaFcpl702a.getAddr_Cntrct_Payee_Cde())  //Natural: IF #A-CNTRCT-PPCN-NBR = ADDR.CNTRCT-PPCN-NBR AND #A-CNTRCT-PAYEE-CDE = ADDR.CNTRCT-PAYEE-CDE AND #A-CNTRCT-ORGN-CDE = ADDR.CNTRCT-ORGN-CDE
                    && pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde.equals(ldaFcpl702a.getAddr_Cntrct_Orgn_Cde())))
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl702a.getVw_addr().reset();                                                                                                                     //Natural: RESET ADDR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
