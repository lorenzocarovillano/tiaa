/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:25 PM
**        * FROM NATURAL PROGRAM : Fcpp818
************************************************************
**        * FILE NAME            : Fcpp818.java
**        * CLASS NAME           : Fcpp818
**        * INSTANCE NAME        : Fcpp818
************************************************************
************************************************************************
* PROGRAM  : FCPP818
* SYSTEM   : CPS
* TITLE    : IAR RESTRUCTURE
* FUNCTION : REMOVE EXTRA FIELDS FROM THE STATEMENTS FILE.
* HISTORY  : 04/18/03 - ROXAN C. - RESTOW. FCPA800 WAS EXPANDED.
*          : 05/15/08 - AER - ROTH-MAJOR1 - CHANGED FCPA800 TO FCPA800D
************************************************************************
* NOTE: WHEN MAKING CHANGES TO THIS MODULE, PLEASE CHECK TO SEE IF THE
*       SAME CHANGES NEED TO BE MADE TO FCPP818M.  THAT MODULE WAS
*       CLONED FROM THIS ONE SO THAT THE MONTHLY JOBSTREAM COULD WRITE
*       A LARGER RECORD WITHOUT DISTURBING THIS MODULE.  THIS PROGRAM
*       RUNS IN THE 1400 JOBSTREAM AND DOES NOT CURRENTLY NEED TO WRITE
*       ROTH FIELDS.
*
*       AER - 5/15/08
* 12/2015  :J.OSTEEN - RE-COMPILED TO PICK UP MODIFIED FCPA801B
*  4/2017  :JJG - RESTOW PIN EXPANSION
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp818 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800d pdaFcpa800d;
    private PdaFcpa801b pdaFcpa801b;
    private PdaFcpa801c pdaFcpa801c;

    // Local Variables
    public DbsRecord localVariables;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        pdaFcpa801b = new PdaFcpa801b(localVariables);
        pdaFcpa801c = new PdaFcpa801c(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp818() throws Exception
    {
        super("Fcpp818");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 57T 'IAR CONTROL REPORT' 120T 'REPORT: RPT1' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CHECK-SORT-FIELDS PYMNT-ADDR-INFO #CHECK-FIELDS INV-INFO ( * )
        while (condition(getWorkFiles().read(1, pdaFcpa801c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa801b.getPnd_Check_Fields(), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            getWorkFiles().write(2, true, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",                 //Natural: WRITE WORK FILE 2 VARIABLE PYMNT-ADDR-INFO INV-INFO ( 1:INV-ACCT-COUNT )
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(57),"IAR CONTROL REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
}
