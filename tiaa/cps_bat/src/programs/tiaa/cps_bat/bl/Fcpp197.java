/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:07 PM
**        * FROM NATURAL PROGRAM : Fcpp197
************************************************************
**        * FILE NAME            : Fcpp197.java
**        * CLASS NAME           : Fcpp197
**        * INSTANCE NAME        : Fcpp197
************************************************************
************************************************************************
* PROGRAM  : FCPP197
* SYSTEM   : CPS
* TITLE    : REPORT OF PAYMENT(S) HELD
* DATE     : NOV 13,93
* FUNCTION : THIS PROGRAM PRODUCES A REPORT OF ALL CHECKS THAT HAVE
*            BEEN HELD.
*            HELD CHECKS ARE LISTED BY REQUESTING DEPARTMENT WITH THE
*            TOTAL OF CHECKS HELD AND THE AMOUNT SHOWN BY DEPARTMENT
*            WITH A GRAND TOTAL FOR ALL DEPARTMENTS.
*            IF THERE ARE NO CHECKS HELD FOR THE CYCLE BEING RUN, THE
*            REPORT WILL SHOW THE FOLLOWING MESSAGE:
*            "NO PAYMENTS HELD".
*
*            INPUT TO THIS PROGRAM IS CREATED BY PROGRAM "ZCPPHLD1",
*            WHICH EXTRACTS IT FROM THE IA, DAILY OR MONTHLY EXTRACTS.
*            THIS EXTRACT IS THEN SORTED AS FOLLOWS:
*    1. ASCENDING - HOLDING DEPARTMENT CODE
*    2. ASCENDING - HOLD REASON CODE
*    3. ASCENDING - ANNUITANT NAME
*    4. ASCENDING - CHECK NUMBER
**----------------------------------------------------------------------
* HISTORY
*
* > 99/10/22 - ILSE BOHM
*   ENSURE THAT REPORT (2) IS ALWAYS CREATED.
*
* > 99/05/05 - LEON GURTOVNIK
*            - ADD LOGIC FOR OV00 & USPS HOLD CODES
*            - CORRECT EXAMINE STATEMENT
*
* > 94/02/07 - LEON SILBERSTEIN:
*   TO CHECK IF THERE WAS AN EMPTY INPUT FILE INSPECT
*    #C-INPUT-HOLD-RCRDS LE 0, RATHER THAN
*    #C-GRAND-HOLD-RCRDS LE 0,
*
* > 94/03/17 - LEON SILBERSTEIN:
*    IF    #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT = 'CT'
*    THEN  #DEPT-NAME := 'Benefit Control'
*
* > 94/08/24 - ALTHEA A. YOUNG: ADAPTED FROM FCPP360 FOR USE IN ON-LINE
*                               PAYMENT PROCESSING STREAM.
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp197 extends BLNatBase
{
    // Data Areas
    private LdaFcpl197 ldaFcpl197;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Hold_Extract;
    private DbsField pnd_Hold_Extract_Cntrct_Hold_Cde;

    private DbsGroup pnd_Hold_Extract__R_Field_1;
    private DbsField pnd_Hold_Extract_Cntrct_Hold_Cde_Type;
    private DbsField pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt;
    private DbsField pnd_Hold_Extract_Cntrct_Hold_Grp;
    private DbsField pnd_Hold_Extract_Ph_Name;

    private DbsGroup pnd_Hold_Extract__R_Field_2;
    private DbsField pnd_Hold_Extract_Ph_Last_Name;
    private DbsField pnd_Hold_Extract_Ph_First_Name;
    private DbsField pnd_Hold_Extract_Ph_Middle_Name;
    private DbsField pnd_Hold_Extract_Pymnt_Check_Nbr;
    private DbsField pnd_Hold_Extract_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Hold_Extract_Cntrct_Payee_Cde;
    private DbsField pnd_Hold_Extract_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Hold_Extract_Cntrct_Orgn_Cde;
    private DbsField pnd_Hold_Extract_Pymnt_Check_Dte;
    private DbsField pnd_Hold_Extract_Pymnt_Eft_Dte;
    private DbsField pnd_Hold_Extract_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Hold_Extract_Pymnt_Check_Amt;
    private DbsField pnd_Hold_Extract_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Hold_Extract_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Hold_Extract_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Program;
    private DbsField pnd_Datu;
    private DbsField pnd_Time;
    private DbsField pnd_Rpt_Heading_01;
    private DbsField pnd_Rpt_Heading_02;
    private DbsField pnd_Wrk_Fld;
    private DbsField pnd_Ws_Hold_Req_Dept;
    private DbsField pnd_Ws_Csr;
    private DbsField pnd_Ws_Origin;
    private DbsField pnd_Condition_Code;

    private DbsGroup pnd_Countes;
    private DbsField pnd_Countes_Pnd_C_Input_Hold_Rcrds;

    private DbsGroup pnd_Countes_Pnd_C_Dept;
    private DbsField pnd_Countes_Pnd_C_Dept_Hold_Rcrds;
    private DbsField pnd_Countes_Pnd_C_Dept_Hold_Amt;

    private DbsGroup pnd_Countes_Pnd_C_Csr;
    private DbsField pnd_Countes_Pnd_C_Csr_Hold_Rcrds;
    private DbsField pnd_Countes_Pnd_C_Csr_Hold_Amt;

    private DbsGroup pnd_Countes_Pnd_C_Orgn;
    private DbsField pnd_Countes_Pnd_C_Orgn_Hold_Rcrds;
    private DbsField pnd_Countes_Pnd_C_Orgn_Hold_Amt;

    private DbsGroup pnd_Countes_Pnd_C_Grand;
    private DbsField pnd_Countes_Pnd_C_Grand_Hold_Rcrds;
    private DbsField pnd_Countes_Pnd_C_Grand_Hold_Amt;
    private DbsField pnd_Dept_Name;
    private DbsField pnd_I;
    private DbsField pnd_Length;
    private DbsField pnd_Orgn_Total;
    private DbsField pnd_Eof;
    private DbsField pnd_Grand_Total;
    private DbsField pnd_Leon_Usps_Records_Total;
    private DbsField pnd_Leon_Usps_Paymnt_Total;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl197 = new LdaFcpl197();
        registerRecord(ldaFcpl197);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Hold_Extract = localVariables.newGroupInRecord("pnd_Hold_Extract", "#HOLD-EXTRACT");
        pnd_Hold_Extract_Cntrct_Hold_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);

        pnd_Hold_Extract__R_Field_1 = pnd_Hold_Extract.newGroupInGroup("pnd_Hold_Extract__R_Field_1", "REDEFINE", pnd_Hold_Extract_Cntrct_Hold_Cde);
        pnd_Hold_Extract_Cntrct_Hold_Cde_Type = pnd_Hold_Extract__R_Field_1.newFieldInGroup("pnd_Hold_Extract_Cntrct_Hold_Cde_Type", "CNTRCT-HOLD-CDE-TYPE", 
            FieldType.STRING, 2);
        pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt = pnd_Hold_Extract__R_Field_1.newFieldInGroup("pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt", "CNTRCT-HOLD-CDE-REQ-DPT", 
            FieldType.STRING, 2);
        pnd_Hold_Extract_Cntrct_Hold_Grp = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3);
        pnd_Hold_Extract_Ph_Name = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Ph_Name", "PH-NAME", FieldType.STRING, 48);

        pnd_Hold_Extract__R_Field_2 = pnd_Hold_Extract.newGroupInGroup("pnd_Hold_Extract__R_Field_2", "REDEFINE", pnd_Hold_Extract_Ph_Name);
        pnd_Hold_Extract_Ph_Last_Name = pnd_Hold_Extract__R_Field_2.newFieldInGroup("pnd_Hold_Extract_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Hold_Extract_Ph_First_Name = pnd_Hold_Extract__R_Field_2.newFieldInGroup("pnd_Hold_Extract_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            16);
        pnd_Hold_Extract_Ph_Middle_Name = pnd_Hold_Extract__R_Field_2.newFieldInGroup("pnd_Hold_Extract_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 
            16);
        pnd_Hold_Extract_Pymnt_Check_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Hold_Extract_Cntrct_Ppcn_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Hold_Extract_Cntrct_Payee_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Hold_Extract_Cntrct_Check_Crrncy_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Hold_Extract_Cntrct_Orgn_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Hold_Extract_Pymnt_Check_Dte = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Hold_Extract_Pymnt_Eft_Dte = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pnd_Hold_Extract_Pymnt_Prcss_Seq_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        pnd_Hold_Extract_Pymnt_Check_Amt = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Hold_Extract_Pymnt_Settlmnt_Dte = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Hold_Extract_Pymnt_Check_Seq_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Hold_Extract_Cntrct_Cmbn_Nbr = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Hold_Extract.newFieldInGroup("pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Datu = localVariables.newFieldInRecord("pnd_Datu", "#DATU", FieldType.STRING, 8);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.STRING, 10);
        pnd_Rpt_Heading_01 = localVariables.newFieldInRecord("pnd_Rpt_Heading_01", "#RPT-HEADING-01", FieldType.STRING, 40);
        pnd_Rpt_Heading_02 = localVariables.newFieldInRecord("pnd_Rpt_Heading_02", "#RPT-HEADING-02", FieldType.STRING, 40);
        pnd_Wrk_Fld = localVariables.newFieldInRecord("pnd_Wrk_Fld", "#WRK-FLD", FieldType.STRING, 40);
        pnd_Ws_Hold_Req_Dept = localVariables.newFieldInRecord("pnd_Ws_Hold_Req_Dept", "#WS-HOLD-REQ-DEPT", FieldType.STRING, 2);
        pnd_Ws_Csr = localVariables.newFieldInRecord("pnd_Ws_Csr", "#WS-CSR", FieldType.STRING, 2);
        pnd_Ws_Origin = localVariables.newFieldInRecord("pnd_Ws_Origin", "#WS-ORIGIN", FieldType.STRING, 2);
        pnd_Condition_Code = localVariables.newFieldInRecord("pnd_Condition_Code", "#CONDITION-CODE", FieldType.INTEGER, 2);

        pnd_Countes = localVariables.newGroupInRecord("pnd_Countes", "#COUNTES");
        pnd_Countes_Pnd_C_Input_Hold_Rcrds = pnd_Countes.newFieldInGroup("pnd_Countes_Pnd_C_Input_Hold_Rcrds", "#C-INPUT-HOLD-RCRDS", FieldType.PACKED_DECIMAL, 
            7);

        pnd_Countes_Pnd_C_Dept = pnd_Countes.newGroupInGroup("pnd_Countes_Pnd_C_Dept", "#C-DEPT");
        pnd_Countes_Pnd_C_Dept_Hold_Rcrds = pnd_Countes_Pnd_C_Dept.newFieldInGroup("pnd_Countes_Pnd_C_Dept_Hold_Rcrds", "#C-DEPT-HOLD-RCRDS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Countes_Pnd_C_Dept_Hold_Amt = pnd_Countes_Pnd_C_Dept.newFieldInGroup("pnd_Countes_Pnd_C_Dept_Hold_Amt", "#C-DEPT-HOLD-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Countes_Pnd_C_Csr = pnd_Countes.newGroupInGroup("pnd_Countes_Pnd_C_Csr", "#C-CSR");
        pnd_Countes_Pnd_C_Csr_Hold_Rcrds = pnd_Countes_Pnd_C_Csr.newFieldInGroup("pnd_Countes_Pnd_C_Csr_Hold_Rcrds", "#C-CSR-HOLD-RCRDS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Countes_Pnd_C_Csr_Hold_Amt = pnd_Countes_Pnd_C_Csr.newFieldInGroup("pnd_Countes_Pnd_C_Csr_Hold_Amt", "#C-CSR-HOLD-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Countes_Pnd_C_Orgn = pnd_Countes.newGroupInGroup("pnd_Countes_Pnd_C_Orgn", "#C-ORGN");
        pnd_Countes_Pnd_C_Orgn_Hold_Rcrds = pnd_Countes_Pnd_C_Orgn.newFieldInGroup("pnd_Countes_Pnd_C_Orgn_Hold_Rcrds", "#C-ORGN-HOLD-RCRDS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Countes_Pnd_C_Orgn_Hold_Amt = pnd_Countes_Pnd_C_Orgn.newFieldInGroup("pnd_Countes_Pnd_C_Orgn_Hold_Amt", "#C-ORGN-HOLD-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Countes_Pnd_C_Grand = pnd_Countes.newGroupInGroup("pnd_Countes_Pnd_C_Grand", "#C-GRAND");
        pnd_Countes_Pnd_C_Grand_Hold_Rcrds = pnd_Countes_Pnd_C_Grand.newFieldInGroup("pnd_Countes_Pnd_C_Grand_Hold_Rcrds", "#C-GRAND-HOLD-RCRDS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Countes_Pnd_C_Grand_Hold_Amt = pnd_Countes_Pnd_C_Grand.newFieldInGroup("pnd_Countes_Pnd_C_Grand_Hold_Amt", "#C-GRAND-HOLD-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Dept_Name = localVariables.newFieldInRecord("pnd_Dept_Name", "#DEPT-NAME", FieldType.STRING, 22);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_Length = localVariables.newFieldInRecord("pnd_Length", "#LENGTH", FieldType.INTEGER, 2);
        pnd_Orgn_Total = localVariables.newFieldInRecord("pnd_Orgn_Total", "#ORGN-TOTAL", FieldType.BOOLEAN, 1);
        pnd_Eof = localVariables.newFieldInRecord("pnd_Eof", "#EOF", FieldType.BOOLEAN, 1);
        pnd_Grand_Total = localVariables.newFieldInRecord("pnd_Grand_Total", "#GRAND-TOTAL", FieldType.BOOLEAN, 1);
        pnd_Leon_Usps_Records_Total = localVariables.newFieldInRecord("pnd_Leon_Usps_Records_Total", "#LEON-USPS-RECORDS-TOTAL", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Leon_Usps_Paymnt_Total = localVariables.newFieldInRecord("pnd_Leon_Usps_Paymnt_Total", "#LEON-USPS-PAYMNT-TOTAL", FieldType.PACKED_DECIMAL, 
            11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl197.initializeValues();

        localVariables.reset();
        pnd_Wrk_Fld.setInitialValue(" ");
        pnd_Ws_Csr.setInitialValue(" ");
        pnd_Ws_Origin.setInitialValue(" ");
        pnd_Length.setInitialValue(0);
        pnd_Orgn_Total.setInitialValue(false);
        pnd_Eof.setInitialValue(false);
        pnd_Grand_Total.setInitialValue(false);
        pnd_Leon_Usps_Records_Total.setInitialValue(0);
        pnd_Leon_Usps_Paymnt_Total.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp197() throws Exception
    {
        super("Fcpp197");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        getReports().write(0, Global.getPROGRAM(),"starting at:",Global.getTIME());                                                                                       //Natural: FORMAT LS = 132 PS = 55;//Natural: FORMAT ( 1 ) LS = 132 PS = 55;//Natural: FORMAT ( 2 ) LS = 132 PS = 55;//Natural: WRITE *PROGRAM 'starting at:' *TIME
        if (Global.isEscape()) return;
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM := *PROGRAM
        pnd_Datu.setValue(Global.getDATU());                                                                                                                              //Natural: ASSIGN #DATU := *DATU
        pnd_Time.setValue(Global.getTIME());                                                                                                                              //Natural: ASSIGN #TIME := *TIME
        //*  ............. INITIALIZE
        pnd_Ws_Hold_Req_Dept.moveAll("H'00'");                                                                                                                            //Natural: MOVE ALL H'00' TO #WS-HOLD-REQ-DEPT
        //*  ......... MAIN LINE .........
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK 10 #HOLD-EXTRACT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(10, pnd_Hold_Extract)))
        {
            CheckAtStartofData149();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  08-24-95 : A. YOUNG
            pnd_Countes_Pnd_C_Input_Hold_Rcrds.nadd(1);                                                                                                                   //Natural: ADD 1 TO #C-INPUT-HOLD-RCRDS
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            //*                                                                                                                                                           //Natural: AT BREAK OF #HOLD-EXTRACT.CNTRCT-ORGN-CDE
            //*  AT BREAK OF #HOLD-EXTRACT.PYMNT-PAY-TYPE-REQ-IND           /* 05-18-98
            //*    #ORGN-TOTAL := TRUE
            //*    PERFORM PRINT-ORIGIN-TOTALS
            //*    #ORGN-TOTAL := FALSE
            //*    PERFORM DETERMINE-DAILY-OR-MONTHLY
            //*    PERFORM SETUP-A-DEPT
            //*  END-BREAK
            short decideConditionsMet182 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT EQ #WS-HOLD-REQ-DEPT
            if (condition(pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals(pnd_Ws_Hold_Req_Dept)))
            {
                decideConditionsMet182++;
                                                                                                                                                                          //Natural: PERFORM PROCESS-A-HOLD
                sub_Process_A_Hold();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT GT #WS-HOLD-REQ-DEPT
            else if (condition(pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.greater(pnd_Ws_Hold_Req_Dept)))
            {
                decideConditionsMet182++;
                if (condition(pnd_Countes_Pnd_C_Input_Hold_Rcrds.notEquals(1)))                                                                                           //Natural: IF #C-INPUT-HOLD-RCRDS NE 1
                {
                                                                                                                                                                          //Natural: PERFORM WRAP-A-DEPT
                    sub_Wrap_A_Dept();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SETUP-A-DEPT
                sub_Setup_A_Dept();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PROCESS-A-HOLD
                sub_Process_A_Hold();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT LT #WS-HOLD-REQ-DEPT
            else if (condition(pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.less(pnd_Ws_Hold_Req_Dept)))
            {
                decideConditionsMet182++;
                getReports().write(0, Global.getPROGRAM(),"E02 - Records are out of sort sequence.",NEWLINE,new ColumnSpacing(6),"Previous record contract hold code:",pnd_Ws_Hold_Req_Dept,NEWLINE,new  //Natural: WRITE *PROGRAM 'E02 - Records are out of sort sequence.' / 6X 'Previous record contract hold code:' #WS-HOLD-REQ-DEPT / 6X 'Current  record contract hold code:' #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT / 'record content is:'
                    ColumnSpacing(6),"Current  record contract hold code:",pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt,NEWLINE,"record content is:");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SHOW-REC
                sub_Show_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Condition_Code.setValue(41);                                                                                                                          //Natural: ASSIGN #CONDITION-CODE := 41
                getReports().write(0, "Program abends with condition code:",pnd_Condition_Code);                                                                          //Natural: WRITE 'Program abends with condition code:' #CONDITION-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  IMPOSSIBLE
                DbsUtil.terminate(pnd_Condition_Code);  if (true) return;                                                                                                 //Natural: TERMINATE #CONDITION-CODE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                getReports().write(0, Global.getPROGRAM(),"E03 - impossible programming condition",NEWLINE,new ColumnSpacing(6),"Previous record contract hold code:",pnd_Ws_Hold_Req_Dept,NEWLINE,new  //Natural: WRITE *PROGRAM 'E03 - impossible programming condition' / 6X 'Previous record contract hold code:' #WS-HOLD-REQ-DEPT / 6X 'Current  record contract hold code:' #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT / 'record content is:'
                    ColumnSpacing(6),"Current  record contract hold code:",pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt,NEWLINE,"record content is:");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SHOW-REC
                sub_Show_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Condition_Code.setValue(42);                                                                                                                          //Natural: ASSIGN #CONDITION-CODE := 42
                getReports().write(0, "Program abends with condition code:",pnd_Condition_Code);                                                                          //Natural: WRITE 'Program abends with condition code:' #CONDITION-CODE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(pnd_Condition_Code);  if (true) return;                                                                                                 //Natural: TERMINATE #CONDITION-CODE
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ******* LEON 7/12/99******
            if (condition(!(pnd_Hold_Extract_Cntrct_Hold_Cde.equals("OV00") || pnd_Hold_Extract_Cntrct_Hold_Cde.equals("USPS"))))                                         //Natural: ACCEPT IF #HOLD-EXTRACT.CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
            {
                continue;
            }
            pnd_Leon_Usps_Paymnt_Total.nadd(pnd_Hold_Extract_Pymnt_Check_Amt);                                                                                            //Natural: ADD #HOLD-EXTRACT.PYMNT-CHECK-AMT TO #LEON-USPS-PAYMNT-TOTAL
            pnd_Leon_Usps_Records_Total.nadd(1);                                                                                                                          //Natural: ADD 1 TO #LEON-USPS-RECORDS-TOTAL
            getSort().writeSortInData(pnd_Hold_Extract_Cntrct_Hold_Cde, pnd_Hold_Extract_Pymnt_Check_Dte, pnd_Hold_Extract_Ph_Name, pnd_Hold_Extract_Cntrct_Ppcn_Nbr,     //Natural: END-ALL
                pnd_Hold_Extract_Pymnt_Check_Nbr, pnd_Hold_Extract_Pymnt_Check_Amt, pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde);
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Hold_Extract_Cntrct_Hold_Cde);                                                                                                             //Natural: SORT BY #HOLD-EXTRACT.CNTRCT-HOLD-CDE USING #HOLD-EXTRACT.PYMNT-CHECK-DTE #HOLD-EXTRACT.PH-NAME #HOLD-EXTRACT.CNTRCT-PPCN-NBR #HOLD-EXTRACT.PYMNT-CHECK-NBR #HOLD-EXTRACT.PYMNT-CHECK-AMT #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Hold_Extract_Cntrct_Hold_Cde, pnd_Hold_Extract_Pymnt_Check_Dte, pnd_Hold_Extract_Ph_Name, pnd_Hold_Extract_Cntrct_Ppcn_Nbr, 
            pnd_Hold_Extract_Pymnt_Check_Nbr, pnd_Hold_Extract_Pymnt_Check_Amt, pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde)))
        {
            getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),pnd_Hold_Extract_Cntrct_Hold_Cde,new TabSetting(12),pnd_Hold_Extract_Pymnt_Check_Dte,            //Natural: WRITE ( 2 ) 01T #HOLD-EXTRACT.CNTRCT-HOLD-CDE 12T #HOLD-EXTRACT.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) 21T #HOLD-EXTRACT.PH-NAME 72T #HOLD-EXTRACT.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 82T #HOLD-EXTRACT.PYMNT-CHECK-NBR ( EM = 9999999 ) 92T #HOLD-EXTRACT.PYMNT-CHECK-AMT ( EM = ZZ,ZZZ,ZZZ.99- ) 112T #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
                new ReportEditMask ("MM/DD/YY"),new TabSetting(21),pnd_Hold_Extract_Ph_Name,new TabSetting(72),pnd_Hold_Extract_Cntrct_Ppcn_Nbr, new ReportEditMask 
                ("XXXXXXX-X"),new TabSetting(82),pnd_Hold_Extract_Pymnt_Check_Nbr, new ReportEditMask ("9999999"),new TabSetting(92),pnd_Hold_Extract_Pymnt_Check_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"),new TabSetting(112),pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAtEndOfData()))
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,"         GRAND TOTALS:  NUMBER OF HOLDS: ",pnd_Leon_Usps_Records_Total, new FieldAttributes       //Natural: WRITE ( 2 ) // '         GRAND TOTALS:  NUMBER OF HOLDS: ' #LEON-USPS-RECORDS-TOTAL ( AD = L EM = Z,ZZZ,ZZ9 ) '    AMOUNT ' #LEON-USPS-PAYMNT-TOTAL ( AD = L EM = Z,ZZZ,ZZ9.99- )
                ("AD=L"), new ReportEditMask ("Z,ZZZ,ZZ9"),"    AMOUNT ",pnd_Leon_Usps_Paymnt_Total, new FieldAttributes ("AD=L"), new ReportEditMask ("Z,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //* ***************************************
        //* ** END-WORK   /*  ORIG 07/12/99
        //*  IB 10/22/99
        if (condition(pnd_Leon_Usps_Records_Total.lessOrEqual(getZero())))                                                                                                //Natural: IF #LEON-USPS-RECORDS-TOTAL LE 0
        {
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(50),"***  NO Expedited HOLDS in this Cycle  ***");                  //Natural: WRITE ( 2 ) //// 50T '***  NO Expedited HOLDS in this Cycle  ***'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  08-24-94 : A. YOUNG
        getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(50),"****   End of Expedited HOLDS Report  ****");                      //Natural: WRITE ( 2 ) //// 50T '****   End of Expedited HOLDS Report  ****'
        if (Global.isEscape()) return;
        pnd_Eof.setValue(true);                                                                                                                                           //Natural: ASSIGN #EOF := TRUE
        if (condition(pnd_Countes_Pnd_C_Input_Hold_Rcrds.lessOrEqual(getZero())))                                                                                         //Natural: IF #C-INPUT-HOLD-RCRDS LE 0
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(50),"*** NO RECORDS HELD IN THIS CYCLE ***");                       //Natural: WRITE ( 1 ) //// 50T '*** NO RECORDS HELD IN THIS CYCLE ***'
            if (Global.isEscape()) return;
            //*  08-24-94 : A. YOUNG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  PERFORM PRINT-ORIGIN-TOTALS
            //*  PERFORM WRAP-A-DEPT
            pnd_Grand_Total.setValue(true);                                                                                                                               //Natural: ASSIGN #GRAND-TOTAL := TRUE
            //*  08-24-94 : A. YOUNG
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(20),"GRAND TOTALS:      NUMBER OF HOLDS",pnd_Countes_Pnd_C_Grand_Hold_Rcrds,  //Natural: WRITE ( 1 ) ///// 20T 'GRAND TOTALS:      NUMBER OF HOLDS' #C-GRAND-HOLD-RCRDS ( AD = L EM = Z,ZZZ,ZZ9 ) 79T 'AMOUNT' #C-GRAND-HOLD-AMT ( AD = L EM = ZZ,ZZZ,ZZZ.99- )
                new FieldAttributes ("AD=L"), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(79),"AMOUNT",pnd_Countes_Pnd_C_Grand_Hold_Amt, new FieldAttributes 
                ("AD=L"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, Global.getPROGRAM(),"ending at:",Global.getTIME(),NEWLINE,"Number of hold records:",pnd_Countes_Pnd_C_Input_Hold_Rcrds);                    //Natural: WRITE *PROGRAM 'ending at:' *TIME / 'Number of hold records:' #C-INPUT-HOLD-RCRDS
        if (Global.isEscape()) return;
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //* **** LEON 7/12/99***
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        //* **************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-A-DEPT
        //*  .......... SET "BREAKING" DEPT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-DAILY-OR-MONTHLY
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-REQUESTING-DEPT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRAP-A-DEPT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-A-HOLD
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SHOW-REC
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CSR-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ORIGIN-TOTALS
    }
    private void sub_Setup_A_Dept() throws Exception                                                                                                                      //Natural: SETUP-A-DEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ..... AT START DETERMINE IF IT IS A "daily" OR "monthly" PYMNTS RPRT
        //* *IF #C-INPUT-HOLD-RCRDS EQ 1
        //* *  PERFORM  DETERMINE-DAILY-OR-MONTHLY
        //* *END-IF
        //*  ........ RESET COUNTERS, ETC.
        if (condition(! (pnd_Eof.getBoolean())))                                                                                                                          //Natural: IF NOT #EOF
        {
            pnd_Countes_Pnd_C_Dept_Hold_Rcrds.reset();                                                                                                                    //Natural: RESET #C-DEPT-HOLD-RCRDS #C-DEPT-HOLD-AMT
            pnd_Countes_Pnd_C_Dept_Hold_Amt.reset();
            //*  ........ INCORPORATE DEPT NAME INTO REPORT HEADINGS
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REQUESTING-DEPT
            sub_Determine_Requesting_Dept();
            if (condition(Global.isEscape())) {return;}
            //*  ......... FORCE A NEWPAGE (1ST TIME IS ALREADY ON NEW PAGE)
            if (condition(pnd_Countes_Pnd_C_Input_Hold_Rcrds.notEquals(1)))                                                                                               //Natural: IF #C-INPUT-HOLD-RCRDS NE 1
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Hold_Req_Dept.setValue(pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt);                                                                                      //Natural: ASSIGN #WS-HOLD-REQ-DEPT := #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT
        }                                                                                                                                                                 //Natural: END-IF
        //*  SETUP-A-DEPT
    }
    private void sub_Determine_Daily_Or_Monthly() throws Exception                                                                                                        //Natural: DETERMINE-DAILY-OR-MONTHLY
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_Eof.getBoolean())))                                                                                                                          //Natural: IF NOT #EOF
        {
            pnd_Rpt_Heading_01.setValue("            HOLD CODE REPORT");                                                                                                  //Natural: ASSIGN #RPT-HEADING-01 := '            HOLD CODE REPORT'
            short decideConditionsMet343 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #HOLD-EXTRACT.CNTRCT-ORGN-CDE;//Natural: VALUE 'DS'
            if (condition((pnd_Hold_Extract_Cntrct_Orgn_Cde.equals("DS"))))
            {
                decideConditionsMet343++;
                pnd_Rpt_Heading_01.setValue("          DAILY SETTL. REDRAW");                                                                                             //Natural: ASSIGN #RPT-HEADING-01 := '          DAILY SETTL. REDRAW'
            }                                                                                                                                                             //Natural: VALUE 'AP'
            else if (condition((pnd_Hold_Extract_Cntrct_Orgn_Cde.equals("AP"))))
            {
                decideConditionsMet343++;
                pnd_Rpt_Heading_01.setValue("      ANNUITY PAYMENTS REDRAW");                                                                                             //Natural: ASSIGN #RPT-HEADING-01 := '      ANNUITY PAYMENTS REDRAW'
            }                                                                                                                                                             //Natural: VALUE 'IA'
            else if (condition((pnd_Hold_Extract_Cntrct_Orgn_Cde.equals("IA"))))
            {
                decideConditionsMet343++;
                pnd_Rpt_Heading_01.setValue("         IMMED. ANNUITY REDRAW");                                                                                            //Natural: ASSIGN #RPT-HEADING-01 := '         IMMED. ANNUITY REDRAW'
            }                                                                                                                                                             //Natural: VALUE 'SS'
            else if (condition((pnd_Hold_Extract_Cntrct_Orgn_Cde.equals("SS"))))
            {
                decideConditionsMet343++;
                if (condition(pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ")))                                                                                //Natural: IF #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' '
                {
                    pnd_Rpt_Heading_01.setValue("            SINGLE SUM / MDO");                                                                                          //Natural: ASSIGN #RPT-HEADING-01 := '            SINGLE SUM / MDO'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rpt_Heading_01.setValue("        SINGLE SUM / MDO REDRAW");                                                                                       //Natural: ASSIGN #RPT-HEADING-01 := '        SINGLE SUM / MDO REDRAW'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'DC'
            else if (condition((pnd_Hold_Extract_Cntrct_Orgn_Cde.equals("DC"))))
            {
                decideConditionsMet343++;
                if (condition(pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ")))                                                                                //Natural: IF #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' '
                {
                    pnd_Rpt_Heading_01.setValue("          IA DEATH SETTLEMENTS");                                                                                        //Natural: ASSIGN #RPT-HEADING-01 := '          IA DEATH SETTLEMENTS'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rpt_Heading_01.setValue("      IA DEATH SETTLEMENTS REDRAW");                                                                                     //Natural: ASSIGN #RPT-HEADING-01 := '      IA DEATH SETTLEMENTS REDRAW'
                    //*  05-18-98
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'MS'
            else if (condition((pnd_Hold_Extract_Cntrct_Orgn_Cde.equals("MS"))))
            {
                decideConditionsMet343++;
                pnd_Rpt_Heading_01.setValue("         MONTHLY SETTL. REDRAW");                                                                                            //Natural: ASSIGN #RPT-HEADING-01 := '         MONTHLY SETTL. REDRAW'
            }                                                                                                                                                             //Natural: VALUE 'AL'
            else if (condition((pnd_Hold_Extract_Cntrct_Orgn_Cde.equals("AL"))))
            {
                decideConditionsMet343++;
                if (condition(pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ")))                                                                                //Natural: IF #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' '
                {
                    pnd_Rpt_Heading_01.setValue("                ANNUITY LOAN");                                                                                          //Natural: ASSIGN #RPT-HEADING-01 := '                ANNUITY LOAN'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rpt_Heading_01.setValue("            ANNUITY LOAN REDRAW");                                                                                       //Natural: ASSIGN #RPT-HEADING-01 := '            ANNUITY LOAN REDRAW'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'EW'
            else if (condition((pnd_Hold_Extract_Cntrct_Orgn_Cde.equals("EW"))))
            {
                decideConditionsMet343++;
                pnd_Rpt_Heading_01.setValue("           ELECTRONIC WARRANTS");                                                                                            //Natural: ASSIGN #RPT-HEADING-01 := '           ELECTRONIC WARRANTS'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet343 > 0))
            {
                pnd_Ws_Origin.setValue(pnd_Hold_Extract_Cntrct_Orgn_Cde);                                                                                                 //Natural: ASSIGN #WS-ORIGIN := #HOLD-EXTRACT.CNTRCT-ORGN-CDE
                pnd_Ws_Csr.setValue(pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde);                                                                                      //Natural: ASSIGN #WS-CSR := #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
                pnd_Ws_Hold_Req_Dept.setValue(pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt);                                                                                  //Natural: ASSIGN #WS-HOLD-REQ-DEPT := #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                getReports().write(0, Global.getPROGRAM(),"E01 - program encountered an 'invalid' origin code.",NEWLINE,"record content is:");                            //Natural: WRITE *PROGRAM 'E01 - program encountered an "invalid" origin code.' / 'record content is:'
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM SHOW-REC
                sub_Show_Rec();
                if (condition(Global.isEscape())) {return;}
                pnd_Condition_Code.setValue(40);                                                                                                                          //Natural: ASSIGN #CONDITION-CODE := 40
                getReports().write(0, "Program abends with condition code:",pnd_Condition_Code);                                                                          //Natural: WRITE 'Program abends with condition code:' #CONDITION-CODE
                if (Global.isEscape()) return;
                DbsUtil.terminate(pnd_Condition_Code);  if (true) return;                                                                                                 //Natural: TERMINATE #CONDITION-CODE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DETERMINE-DAILY-OR-MONTHLY
    }
    private void sub_Determine_Requesting_Dept() throws Exception                                                                                                         //Natural: DETERMINE-REQUESTING-DEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ......... DETERMINE DEPARTMENT NAME
        //*  08-24-94 : A. YOUNG
        //*  08-24-94 : A. YOUNG
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #WRK-FLD #DEPT-NAME #RPT-HEADING-02 #LENGTH
        pnd_Wrk_Fld.reset();
        pnd_Dept_Name.reset();
        pnd_Rpt_Heading_02.reset();
        pnd_Length.reset();
        //* **ORG*EXAMINE #USER-GROUP(*) FOR #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT
        //*  LEON G
        //*  LEON G
        DbsUtil.examine(new ExamineSource(ldaFcpl197.getPnd_User_Group().getValue("*"),true), new ExamineSearch(pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt,                 //Natural: EXAMINE FULL VALUE OF #USER-GROUP ( * ) FOR FULL VALUE OF #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT GIVING INDEX #I
            true), new ExamineGivingIndex(pnd_I));
        if (condition(pnd_I.greater(getZero())))                                                                                                                          //Natural: IF #I GT 0
        {
            pnd_Dept_Name.setValue(ldaFcpl197.getPnd_User_Group_Desc().getValue(pnd_I));                                                                                  //Natural: ASSIGN #DEPT-NAME := #USER-GROUP-DESC ( #I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ........ PROVIDE FOR AUTOMATIC SYSTEM HOLDS:
            //*             'CKNE' = HOLD: MS NET AMT. NOT = TO GROSS - NET.
            //*             'INST' = HOLD:
            //*             'LSIN' = HOLD: DS LUMP SUM PAYMENT GOING TO AN INSTITUTION.
            //*             'ACCT' = HOLD: BANK ACCT WARRENT VS EFT DONT MATCH
            //*  ..... LEON G..............
            //*             'BCU ' = 'U '
            //*             'USPS' = 'PS'
            //*             'OV00' = '00'
            //*  LEON 5/5/99
            if (condition(! (pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals("NE") || pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals("ST") || pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals("IN")  //Natural: IF NOT ( #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT = 'NE' OR = 'ST' OR = 'IN' OR = 'CT' OR = 'U ' OR = 'PS' OR = '00' )
                || pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals("CT") || pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals("U ") || pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals("PS") 
                || pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals("00"))))
            {
                getReports().write(0, Global.getPROGRAM(),"Invalid Requesting department code encountered:","=",pnd_Hold_Extract_Cntrct_Hold_Cde,NEWLINE,                 //Natural: WRITE *PROGRAM 'Invalid Requesting department code encountered:' '=' #HOLD-EXTRACT.CNTRCT-HOLD-CDE / 'The program continues processing.' / 'The report for the invalid department is printed on page:' *PAGE-NUMBER ( 1 )
                    "The program continues processing.",NEWLINE,"The report for the invalid department is printed on page:",getReports().getPageNumberDbs(1));
                if (Global.isEscape()) return;
                //*  SPACE
                if (condition(pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt.equals(" ")))                                                                                      //Natural: IF #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT EQ ' '
                {
                    //*  08-24-94 : A. YOUNG
                    pnd_Dept_Name.setValue("++INVALID BLANK++");                                                                                                          //Natural: MOVE '++INVALID BLANK++' TO #DEPT-NAME
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  08-24-94 : A. YOUNG
                    pnd_Dept_Name.setValue(DbsUtil.compress(CompressOption.WithDelimiters, '*', "++INVALID CODE", pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt));             //Natural: COMPRESS '++INVALID CODE' #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT INTO #DEPT-NAME WITH DELIMITER '*'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* **ORIG**  #DEPT-NAME := 'BENEFIT*CONTROL'                /*  05/05/99
                //*  LEON
                //*  LEON
                if (condition(pnd_Hold_Extract_Cntrct_Hold_Cde.equals("OV00") || pnd_Hold_Extract_Cntrct_Hold_Cde.equals("USPS")))                                        //Natural: IF #HOLD-EXTRACT.CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
                {
                    pnd_Dept_Name.setValue("MAIL DISTRIB. SERVICES");                                                                                                     //Natural: ASSIGN #DEPT-NAME := 'MAIL DISTRIB. SERVICES'
                    //*  LEON
                    //*  ORIG
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Dept_Name.setValue("BENEFIT*CONTROL");                                                                                                            //Natural: ASSIGN #DEPT-NAME := 'BENEFIT*CONTROL'
                    //*  LEON
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ** 08-24-95 : A. YOUNG - CENTER DEPT NAME HEADING
        pnd_Rpt_Heading_02.setValue(DbsUtil.compress(CompressOption.WithDelimiters, '*', "REQUESTING DEPT:", pnd_Dept_Name));                                             //Natural: COMPRESS 'REQUESTING DEPT:' #DEPT-NAME INTO #RPT-HEADING-02 WITH DELIMITER '*'
        DbsUtil.examine(new ExamineSource(pnd_Rpt_Heading_02,true), new ExamineSearch(" "), new ExamineGivingLength(pnd_Length));                                         //Natural: EXAMINE FULL #RPT-HEADING-02 FOR ' ' GIVING LENGTH #LENGTH
        pnd_Length.compute(new ComputeParameters(false, pnd_Length), (DbsField.subtract(40,pnd_Length)).divide(2));                                                       //Natural: ASSIGN #LENGTH := ( 40 - #LENGTH ) / 2
        pnd_Wrk_Fld.moveAll("H'00'");                                                                                                                                     //Natural: MOVE ALL H'00' TO #WRK-FLD UNTIL #LENGTH
        pnd_Rpt_Heading_02.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Wrk_Fld, pnd_Rpt_Heading_02));                                                    //Natural: COMPRESS #WRK-FLD #RPT-HEADING-02 INTO #RPT-HEADING-02 LEAVING NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Rpt_Heading_02,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                           //Natural: EXAMINE FULL #RPT-HEADING-02 FOR FULL H'00' REPLACE ' '
        DbsUtil.examine(new ExamineSource(pnd_Rpt_Heading_02,true), new ExamineSearch("*"), new ExamineReplace(" "));                                                     //Natural: EXAMINE FULL #RPT-HEADING-02 FOR '*' REPLACE ' '
        //*  DETERMINE-REQUESTING-DEPT
    }
    private void sub_Wrap_A_Dept() throws Exception                                                                                                                       //Natural: WRAP-A-DEPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  ........ WRITE DEPT TOTALS
        //*  08-24-94 : A. YOUNG
        if (condition(getReports().getAstLinesLeft(1).less(4)))                                                                                                           //Natural: NEWPAGE ( 1 ) IF LESS THAN 4 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(20),"DEPARTMENT TOTALS: NUMBER OF HOLDS",pnd_Countes_Pnd_C_Dept_Hold_Rcrds,                     //Natural: WRITE ( 1 ) / 20T 'DEPARTMENT TOTALS: NUMBER OF HOLDS' #C-DEPT-HOLD-RCRDS ( AD = L EM = Z,ZZZ,ZZ9 ) 79T 'AMOUNT' #C-DEPT-HOLD-AMT ( AD = L EM = ZZ,ZZZ,ZZZ.99- )
            new FieldAttributes ("AD=L"), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(79),"AMOUNT",pnd_Countes_Pnd_C_Dept_Hold_Amt, new FieldAttributes 
            ("AD=L"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  ........ ACCUM DEPT TOTALS TO THE GRAND TOTALS
        //*  07-10-95 : A. YOUNG
        pnd_Countes_Pnd_C_Csr_Hold_Rcrds.nadd(pnd_Countes_Pnd_C_Dept_Hold_Rcrds);                                                                                         //Natural: ADD #C-DEPT-HOLD-RCRDS TO #C-CSR-HOLD-RCRDS
        //*  07-10-95 : A. YOUNG
        pnd_Countes_Pnd_C_Csr_Hold_Amt.nadd(pnd_Countes_Pnd_C_Dept_Hold_Amt);                                                                                             //Natural: ADD #C-DEPT-HOLD-AMT TO #C-CSR-HOLD-AMT
        //*  08-24-94 : A. YOUNG
        pnd_Countes_Pnd_C_Orgn_Hold_Rcrds.nadd(pnd_Countes_Pnd_C_Dept_Hold_Rcrds);                                                                                        //Natural: ADD #C-DEPT-HOLD-RCRDS TO #C-ORGN-HOLD-RCRDS
        //*  08-24-94 : A. YOUNG
        pnd_Countes_Pnd_C_Orgn_Hold_Amt.nadd(pnd_Countes_Pnd_C_Dept_Hold_Amt);                                                                                            //Natural: ADD #C-DEPT-HOLD-AMT TO #C-ORGN-HOLD-AMT
        pnd_Countes_Pnd_C_Grand_Hold_Rcrds.nadd(pnd_Countes_Pnd_C_Dept_Hold_Rcrds);                                                                                       //Natural: ADD #C-DEPT-HOLD-RCRDS TO #C-GRAND-HOLD-RCRDS
        pnd_Countes_Pnd_C_Grand_Hold_Amt.nadd(pnd_Countes_Pnd_C_Dept_Hold_Amt);                                                                                           //Natural: ADD #C-DEPT-HOLD-AMT TO #C-GRAND-HOLD-AMT
        //*  WRAP-A-DEPT
    }
    private void sub_Process_A_Hold() throws Exception                                                                                                                    //Natural: PROCESS-A-HOLD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Countes_Pnd_C_Dept_Hold_Rcrds.nadd(1);                                                                                                                        //Natural: ADD 1 TO #C-DEPT-HOLD-RCRDS
        pnd_Countes_Pnd_C_Dept_Hold_Amt.nadd(pnd_Hold_Extract_Pymnt_Check_Amt);                                                                                           //Natural: ADD #HOLD-EXTRACT.PYMNT-CHECK-AMT TO #C-DEPT-HOLD-AMT
        //*  08-24-94 : A. YOUNG
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),pnd_Hold_Extract_Cntrct_Hold_Cde,new TabSetting(12),pnd_Hold_Extract_Pymnt_Check_Dte,                //Natural: WRITE ( 1 ) 01T #HOLD-EXTRACT.CNTRCT-HOLD-CDE 12T #HOLD-EXTRACT.PYMNT-CHECK-DTE ( EM = MM/DD/YY ) 21T #HOLD-EXTRACT.PH-NAME 72T #HOLD-EXTRACT.CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 82T #HOLD-EXTRACT.PYMNT-CHECK-NBR ( EM = 9999999 ) 92T #HOLD-EXTRACT.PYMNT-CHECK-AMT ( EM = ZZ,ZZZ,ZZZ.99- ) 112T #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            new ReportEditMask ("MM/DD/YY"),new TabSetting(21),pnd_Hold_Extract_Ph_Name,new TabSetting(72),pnd_Hold_Extract_Cntrct_Ppcn_Nbr, new ReportEditMask 
            ("XXXXXXX-X"),new TabSetting(82),pnd_Hold_Extract_Pymnt_Check_Nbr, new ReportEditMask ("9999999"),new TabSetting(92),pnd_Hold_Extract_Pymnt_Check_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"),new TabSetting(112),pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde);
        if (Global.isEscape()) return;
        //* PROCESS-A-HOLD
    }
    private void sub_Show_Rec() throws Exception                                                                                                                          //Natural: SHOW-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  (D)
        //*  (D)
        //*  (D)
        getReports().write(0, Global.getPROGRAM(),"hold extract record to follow:",NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Cntrct_Hold_Cde_Type,NEWLINE,new     //Natural: WRITE *PROGRAM 'hold extract record to follow:' / 3X '=' #HOLD-EXTRACT.CNTRCT-HOLD-CDE-TYPE / 3X '=' #HOLD-EXTRACT.CNTRCT-HOLD-CDE-REQ-DPT / 3X '=' #HOLD-EXTRACT.CNTRCT-HOLD-GRP / 3X '=' #HOLD-EXTRACT.PH-NAME / 3X '=' #HOLD-EXTRACT.PYMNT-CHECK-NBR / 3X '=' #HOLD-EXTRACT.CNTRCT-PPCN-NBR / 3X '=' #HOLD-EXTRACT.CNTRCT-PAYEE-CDE / 3X '=' #HOLD-EXTRACT.CNTRCT-CHECK-CRRNCY-CDE / 3X '=' #HOLD-EXTRACT.CNTRCT-ORGN-CDE / 3X '=' #HOLD-EXTRACT.PYMNT-CHECK-DTE / 3X '=' #HOLD-EXTRACT.PYMNT-EFT-DTE / 3X '=' #HOLD-EXTRACT.PYMNT-PRCSS-SEQ-NBR / 3X '=' #HOLD-EXTRACT.PYMNT-CHECK-AMT / 3X '=' #HOLD-EXTRACT.PYMNT-SETTLMNT-DTE / 3X '=' #HOLD-EXTRACT.PYMNT-CHECK-SEQ-NBR / 3X '=' #HOLD-EXTRACT.CNTRCT-CMBN-NBR / 3X '=' #HOLD-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            ColumnSpacing(3),"=",pnd_Hold_Extract_Cntrct_Hold_Cde_Req_Dpt,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Cntrct_Hold_Grp,NEWLINE,new 
            ColumnSpacing(3),"=",pnd_Hold_Extract_Ph_Name,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Pymnt_Check_Nbr,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Cntrct_Ppcn_Nbr,NEWLINE,new 
            ColumnSpacing(3),"=",pnd_Hold_Extract_Cntrct_Payee_Cde,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Cntrct_Check_Crrncy_Cde,NEWLINE,new 
            ColumnSpacing(3),"=",pnd_Hold_Extract_Cntrct_Orgn_Cde,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Pymnt_Check_Dte,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Pymnt_Eft_Dte,NEWLINE,new 
            ColumnSpacing(3),"=",pnd_Hold_Extract_Pymnt_Prcss_Seq_Nbr,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Pymnt_Check_Amt,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Pymnt_Settlmnt_Dte,NEWLINE,new 
            ColumnSpacing(3),"=",pnd_Hold_Extract_Pymnt_Check_Seq_Nbr,NEWLINE,new ColumnSpacing(3),"=",pnd_Hold_Extract_Cntrct_Cmbn_Nbr,NEWLINE,new ColumnSpacing(3),
            "=",pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde);
        if (Global.isEscape()) return;
        //*  SHOW-REC
    }
    //*  08-24-94 : A. YOUNG
    private void sub_Print_Csr_Totals() throws Exception                                                                                                                  //Natural: PRINT-CSR-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *IF NOT #EOF
        //* *   #CSR-TOTAL := TRUE
        //* *END-IF
        if (condition(getReports().getAstLinesLeft(1).less(4)))                                                                                                           //Natural: NEWPAGE ( 1 ) IF LESS THAN 4 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(20),"CSR STATUS TOTALS: NUMBER OF HOLDS",pnd_Countes_Pnd_C_Csr_Hold_Rcrds, new                  //Natural: WRITE ( 1 ) / 20T 'CSR STATUS TOTALS: NUMBER OF HOLDS' #C-CSR-HOLD-RCRDS ( AD = L EM = Z,ZZZ,ZZ9 ) 79T 'AMOUNT' #C-CSR-HOLD-AMT ( AD = L EM = ZZ,ZZZ,ZZZ.99- )
            FieldAttributes ("AD=L"), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(79),"AMOUNT",pnd_Countes_Pnd_C_Csr_Hold_Amt, new FieldAttributes ("AD=L"), 
            new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  08-24-94 : A. YOUNG
        pnd_Countes_Pnd_C_Csr_Hold_Rcrds.reset();                                                                                                                         //Natural: RESET #C-CSR-HOLD-RCRDS #C-CSR-HOLD-AMT
        pnd_Countes_Pnd_C_Csr_Hold_Amt.reset();
        //*  PRINT-CSR-TOTALS
    }
    //*  08-24-94 : A. YOUNG
    private void sub_Print_Origin_Totals() throws Exception                                                                                                               //Natural: PRINT-ORIGIN-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_Eof.getBoolean())))                                                                                                                          //Natural: IF NOT #EOF
        {
            pnd_Orgn_Total.setValue(true);                                                                                                                                //Natural: ASSIGN #ORGN-TOTAL := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLinesLeft(1).less(4)))                                                                                                           //Natural: NEWPAGE ( 1 ) IF LESS THAN 4 LINES LEFT
        {
            getReports().newPage(1);
            if (condition(Global.isEscape())){return;}
        }
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,new TabSetting(20),"ORIGIN TOTALS:     NUMBER OF HOLDS",pnd_Countes_Pnd_C_Orgn_Hold_Rcrds,                     //Natural: WRITE ( 1 ) / 20T 'ORIGIN TOTALS:     NUMBER OF HOLDS' #C-ORGN-HOLD-RCRDS ( AD = L EM = Z,ZZZ,ZZ9 ) 79T 'AMOUNT' #C-ORGN-HOLD-AMT ( AD = L EM = ZZ,ZZZ,ZZZ.99- )
            new FieldAttributes ("AD=L"), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(79),"AMOUNT",pnd_Countes_Pnd_C_Orgn_Hold_Amt, new FieldAttributes 
            ("AD=L"), new ReportEditMask ("ZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //*  08-24-94 : A. YOUNG
        pnd_Countes_Pnd_C_Orgn_Hold_Rcrds.reset();                                                                                                                        //Natural: RESET #C-ORGN-HOLD-RCRDS #C-ORGN-HOLD-AMT
        pnd_Countes_Pnd_C_Orgn_Hold_Amt.reset();
        //*  PRINT-ORIGIN-TOTALS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  08-24-94 : A. YOUNG
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(3),pnd_Program,new TabSetting(110),"PAGE:",new TabSetting(116),getReports().getPageNumberDbs(1),NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE NOHDR 003T #PROGRAM 110T 'PAGE:' 116T *PAGE-NUMBER ( 1 ) / 110T 'RUN DATE:' 120T #DATU / 110T 'RUN TIME:' 120T #TIME / 53T 'CONSOLIDATED PAYMENT SYSTEM' / 55T 'ON-LINE PAYMENT SYSTEM' /
                        TabSetting(110),"RUN DATE:",new TabSetting(120),pnd_Datu,NEWLINE,new TabSetting(110),"RUN TIME:",new TabSetting(120),pnd_Time,NEWLINE,new 
                        TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",NEWLINE,new TabSetting(55),"ON-LINE PAYMENT SYSTEM",NEWLINE);
                    //*  08-24-94 : A. YOUNG
                    if (condition(! (pnd_Grand_Total.getBoolean())))                                                                                                      //Natural: IF NOT #GRAND-TOTAL
                    {
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(47),pnd_Rpt_Heading_01,NEWLINE,NEWLINE,new TabSetting(47),pnd_Rpt_Heading_02,           //Natural: WRITE ( 1 ) NOTITLE 047T #RPT-HEADING-01 // 047T #RPT-HEADING-02 //
                            NEWLINE,NEWLINE);
                        //*     IF #DEPT-TOTAL
                        //*        WRITE (1) NOTITLE
                        //*     END-IF
                        //*  08-24-95 : A. YOUNG
                        //*  08-24-94 : A. YOUNG
                        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),"HOLD",new TabSetting(12),"PAYMENT",new TabSetting(32),"ANNUITANT",new               //Natural: WRITE ( 1 ) NOTITLE 001T 'HOLD' 012T 'PAYMENT' 032T 'ANNUITANT' 072T 'PPCN' 082T 'CHECK' 096T 'AMOUNT' 111T 'CSR' / 001T 'CODE' 012T 'DATE' 034T 'NAME' 082T 'NUMBER' 110T 'STATUS' //
                            TabSetting(72),"PPCN",new TabSetting(82),"CHECK",new TabSetting(96),"AMOUNT",new TabSetting(111),"CSR",NEWLINE,new TabSetting(1),"CODE",new 
                            TabSetting(12),"DATE",new TabSetting(34),"NAME",new TabSetting(82),"NUMBER",new TabSetting(110),"STATUS",NEWLINE,NEWLINE);
                        //*  08-24-95 : A. YOUNG
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(3),pnd_Program,new TabSetting(110),"PAGE:",new TabSetting(116),getReports().getPageNumberDbs(2),NEWLINE,new  //Natural: WRITE ( 2 ) NOTITLE NOHDR 003T #PROGRAM 110T 'PAGE:' 116T *PAGE-NUMBER ( 2 ) / 110T 'RUN DATE:' 120T #DATU / 110T 'RUN TIME:' 120T #TIME / 53T 'CONSOLIDATED PAYMENT SYSTEM' / 55T 'ON-LINE PAYMENT SYSTEM' // 47T 'REQUESTING DEPT: MAIL DISTRIB. SERVICES' /
                        TabSetting(110),"RUN DATE:",new TabSetting(120),pnd_Datu,NEWLINE,new TabSetting(110),"RUN TIME:",new TabSetting(120),pnd_Time,NEWLINE,new 
                        TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",NEWLINE,new TabSetting(55),"ON-LINE PAYMENT SYSTEM",NEWLINE,NEWLINE,new TabSetting(47),
                        "REQUESTING DEPT: MAIL DISTRIB. SERVICES",NEWLINE);
                    getReports().write(2, ReportOption.NOTITLE,new TabSetting(1),"HOLD",new TabSetting(12),"PAYMENT",new TabSetting(32),"ANNUITANT",new                   //Natural: WRITE ( 2 ) NOTITLE 001T 'HOLD' 012T 'PAYMENT' 032T 'ANNUITANT' 072T 'PPCN' 082T 'CHECK' 096T 'AMOUNT' 111T 'CSR' / 001T 'CODE' 012T 'DATE' 034T 'NAME' 082T 'NUMBER' 110T 'STATUS' //
                        TabSetting(72),"PPCN",new TabSetting(82),"CHECK",new TabSetting(96),"AMOUNT",new TabSetting(111),"CSR",NEWLINE,new TabSetting(1),"CODE",new 
                        TabSetting(12),"DATE",new TabSetting(34),"NAME",new TabSetting(82),"NUMBER",new TabSetting(110),"STATUS",NEWLINE,NEWLINE);
                    //*  (2)
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak = pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde.isBreak(endOfData);
        boolean pnd_Hold_Extract_Cntrct_Orgn_CdeIsBreak = pnd_Hold_Extract_Cntrct_Orgn_Cde.isBreak(endOfData);
        if (condition(pnd_Hold_Extract_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak || pnd_Hold_Extract_Cntrct_Orgn_CdeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM WRAP-A-DEPT
            sub_Wrap_A_Dept();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-CSR-TOTALS
            sub_Print_Csr_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(pnd_Hold_Extract_Cntrct_Orgn_Cde.isBreak()))                                                                                                    //Natural: IF BREAK OF #HOLD-EXTRACT.CNTRCT-ORGN-CDE
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DAILY-OR-MONTHLY
                sub_Determine_Daily_Or_Monthly();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  08-24-94 : A. YOUNG
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Hold_Extract_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Orgn_Total.setValue(true);                                                                                                                                //Natural: ASSIGN #ORGN-TOTAL := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-ORIGIN-TOTALS
            sub_Print_Origin_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_Orgn_Total.setValue(false);                                                                                                                               //Natural: ASSIGN #ORGN-TOTAL := FALSE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DAILY-OR-MONTHLY
            sub_Determine_Daily_Or_Monthly();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SETUP-A-DEPT
            sub_Setup_A_Dept();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=55");
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");
    }
    private void CheckAtStartofData149() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-DAILY-OR-MONTHLY
            sub_Determine_Daily_Or_Monthly();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SETUP-A-DEPT
            sub_Setup_A_Dept();
            if (condition(Global.isEscape())) {return;}
            //*  A. YOUNG
        }                                                                                                                                                                 //Natural: END-START
    }
}
