/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:59 PM
**        * FROM NATURAL PROGRAM : Fcpp806
************************************************************
**        * FILE NAME            : Fcpp806.java
**        * CLASS NAME           : Fcpp806
**        * INSTANCE NAME        : Fcpp806
************************************************************
************************************************************************
*
* PROGRAM   : FCPP806
*
* SYSTEM    : CPS
* TITLE     : DIVIDEND FOR COLLEGE REPORT
* GENERATED :
*
* FUNCTION  : GENERATE A REPORT OF ALL CONTRACTS WHERE DIVIDENDS ARE
*           : PAID TO THEIR RESPECTIVE COLLEGES.
*           :
* HISTORY   :
*           : 05/02/03  ROXANNE CARREON
*           : RESTOW. FCPA800 EXPANDED
*
* 05/06/08 : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SSI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp806 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpacntl pdaFcpacntl;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Index;
    private DbsField pnd_Addr_Ct;
    private DbsField pnd_Fund_Ct;
    private DbsField pnd_Print_Ct;
    private DbsField pnd_Name_Line;
    private DbsField pnd_Coll_Dvdnd_Amt;
    private DbsField pnd_Total_Dvdnd_Amt;
    private DbsField pnd_Annty_Ins_Type;
    private DbsField pnd_Annty_Type;
    private DbsField pnd_Insurance_Option;
    private DbsField pnd_Life_Contingency;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpacntl = new PdaFcpacntl(localVariables);

        // Local Variables
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Addr_Ct = localVariables.newFieldInRecord("pnd_Addr_Ct", "#ADDR-CT", FieldType.PACKED_DECIMAL, 2);
        pnd_Fund_Ct = localVariables.newFieldInRecord("pnd_Fund_Ct", "#FUND-CT", FieldType.PACKED_DECIMAL, 2);
        pnd_Print_Ct = localVariables.newFieldInRecord("pnd_Print_Ct", "#PRINT-CT", FieldType.PACKED_DECIMAL, 2);
        pnd_Name_Line = localVariables.newFieldInRecord("pnd_Name_Line", "#NAME-LINE", FieldType.STRING, 35);
        pnd_Coll_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Coll_Dvdnd_Amt", "#COLL-DVDND-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Total_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Total_Dvdnd_Amt", "#TOTAL-DVDND-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Annty_Ins_Type = localVariables.newFieldInRecord("pnd_Annty_Ins_Type", "#ANNTY-INS-TYPE", FieldType.STRING, 4);
        pnd_Annty_Type = localVariables.newFieldInRecord("pnd_Annty_Type", "#ANNTY-TYPE", FieldType.STRING, 4);
        pnd_Insurance_Option = localVariables.newFieldInRecord("pnd_Insurance_Option", "#INSURANCE-OPTION", FieldType.STRING, 4);
        pnd_Life_Contingency = localVariables.newFieldInRecord("pnd_Life_Contingency", "#LIFE-CONTINGENCY", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp806() throws Exception
    {
        super("Fcpp806");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP806", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 55
        //*                                                                                                                                                               //Natural: ON ERROR
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 CNTL
        while (condition(getWorkFiles().read(1, pdaFcpacntl.getCntl())))
        {
            //*  READ PAYMENT/NAME AND ADDRESS WORK FILE
            READWORK02:                                                                                                                                                   //Natural: READ WORK FILE 2 WF-PYMNT-ADDR-REC
            while (condition(getWorkFiles().read(2, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
            {
                getSort().writeSortInData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),                    //Natural: END-ALL
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name(), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Middle_Name(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(1), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(1), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(1), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde().getValue(1), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde(), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count(), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(3), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(6), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(9), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(12), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(15), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(18), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(21), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(24), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(27), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(30), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(33), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(36), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(39), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(2), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(4), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(6), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(8), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(10), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(12), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(14), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(16), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(18), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(20), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(22), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(24), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(26), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(28), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(30), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(32), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(34), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(36), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(38), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(40));
            }                                                                                                                                                             //Natural: END-WORK
            READWORK02_Exit:
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                  //Natural: SORT BY CNTRCT-DVDND-PAYEE-CDE CNTRCT-PPCN-NBR USING CNTRCT-CMBN-NBR PH-LAST-NAME PH-FIRST-NAME PH-MIDDLE-NAME PYMNT-NME ( 1 ) PYMNT-ADDR-LINE1-TXT ( 1 ) PYMNT-ADDR-LINE2-TXT ( 1 ) PYMNT-ADDR-LINE3-TXT ( 1 ) PYMNT-ADDR-LINE4-TXT ( 1 ) PYMNT-ADDR-LINE5-TXT ( 1 ) PYMNT-ADDR-LINE6-TXT ( 1 ) PYMNT-ADDR-ZIP-CDE ( 1 ) CNTRCT-MODE-CDE CNTRCT-ANNTY-INS-TYPE CNTRCT-ANNTY-TYPE-CDE CNTRCT-INSURANCE-OPTION CNTRCT-LIFE-CONTINGENCY INV-ACCT-COUNT INV-ACCT-CDE ( * ) INV-ACCT-DVDND-AMT ( * )
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Middle_Name(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(6), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(12), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(18), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(24), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(30), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(36), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(2), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(8), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(14), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(20), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(26), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(32), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(38), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(40))))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *PROGRAM 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 48T 'DIVIDEND PAYMENT FOR COLLEGE CODE' CNTRCT-DVDND-PAYEE-CDE / 56T CNTL.CNTL-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  TRANSLATE LEDGER FIELDS
            //* ***********************************************************************
            //*  PROGRAM   : FCPP807
            //*  SYSTEM    : CPS
            //*  TITLE     : TRANSLATE LEDGER CODES
            //*  GENERATED :
            //*  FUNCTION  : USED BY:
            //*            :     FCPP810
            //*            :
            //*  HISTORY   :
            //* ***********************************************************************
            pnd_Annty_Ins_Type.reset();                                                                                                                                   //Natural: RESET #ANNTY-INS-TYPE #ANNTY-TYPE #INSURANCE-OPTION #LIFE-CONTINGENCY
            pnd_Annty_Type.reset();
            pnd_Insurance_Option.reset();
            pnd_Life_Contingency.reset();
            short decideConditionsMet543 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-INS-TYPE;//Natural: VALUE 'A'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("A"))))
            {
                decideConditionsMet543++;
                pnd_Annty_Ins_Type.setValue("Ann ");                                                                                                                      //Natural: MOVE 'Ann ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("I"))))
            {
                decideConditionsMet543++;
                pnd_Annty_Ins_Type.setValue("Ins ");                                                                                                                      //Natural: MOVE 'Ins ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("G"))))
            {
                decideConditionsMet543++;
                pnd_Annty_Ins_Type.setValue("Grp ");                                                                                                                      //Natural: MOVE 'Grp ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet553 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-TYPE-CDE;//Natural: VALUE 'M'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M"))))
            {
                decideConditionsMet553++;
                pnd_Annty_Type.setValue("Mat ");                                                                                                                          //Natural: MOVE 'Mat ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("D"))))
            {
                decideConditionsMet553++;
                pnd_Annty_Type.setValue("Dth ");                                                                                                                          //Natural: MOVE 'Dth ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("P"))))
            {
                decideConditionsMet553++;
                pnd_Annty_Type.setValue("Par ");                                                                                                                          //Natural: MOVE 'Par ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("N"))))
            {
                decideConditionsMet553++;
                pnd_Annty_Type.setValue("NPar");                                                                                                                          //Natural: MOVE 'NPar' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet565 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-INSURANCE-OPTION;//Natural: VALUE 'G'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("G"))))
            {
                decideConditionsMet565++;
                pnd_Insurance_Option.setValue("Grp ");                                                                                                                    //Natural: MOVE 'Grp ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("C"))))
            {
                decideConditionsMet565++;
                pnd_Insurance_Option.setValue("Coll");                                                                                                                    //Natural: MOVE 'Coll' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("I"))))
            {
                decideConditionsMet565++;
                pnd_Insurance_Option.setValue("Ind ");                                                                                                                    //Natural: MOVE 'Ind ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("P"))))
            {
                decideConditionsMet565++;
                pnd_Insurance_Option.setValue("PA  ");                                                                                                                    //Natural: MOVE 'PA  ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet577 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-LIFE-CONTINGENCY;//Natural: VALUE 'Y'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency().equals("Y"))))
            {
                decideConditionsMet577++;
                pnd_Life_Contingency.setValue("Yes ");                                                                                                                    //Natural: MOVE 'Yes ' TO #LIFE-CONTINGENCY
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency().equals("N"))))
            {
                decideConditionsMet577++;
                pnd_Life_Contingency.setValue("No  ");                                                                                                                    //Natural: MOVE 'No  ' TO #LIFE-CONTINGENCY
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Name_Line.setValue(DbsUtil.compress(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name()));                    //Natural: COMPRESS PH-FIRST-NAME PH-LAST-NAME INTO #NAME-LINE
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 INV-ACCT-COUNT
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index).equals(getZero())))                                                //Natural: IF INV-ACCT-DVDND-AMT ( #INDEX ) = 0
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(1, new ReportMatrixColumnUnderline("-")," /Contract Num",                                                                            //Natural: DISPLAY ( 01 ) ( UC = - ) ' /Contract Num' CNTRCT-PPCN-NBR 'Combine/Contract Num' CNTRCT-CMBN-NBR ' /Annuitant Name' #NAME-LINE ' /Mode' CNTRCT-MODE-CDE 'Ann/Ins' #ANNTY-INS-TYPE 'Cntr/Typr' #ANNTY-TYPE 'Prod/Line' #INSURANCE-OPTION 'Life/Cont.' #LIFE-CONTINGENCY ' /Fund' INV-ACCT-CDE-ALPHA ( #INDEX ) 'Dividend/Amount' INV-ACCT-DVDND-AMT ( #INDEX ) ( EM = Z,ZZZ,ZZZ9.99- )
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr()," /Annuitant Name",
                		pnd_Name_Line," /Mode",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde(),"Ann/Ins",
                		pnd_Annty_Ins_Type,"Cntr/Typr",
                		pnd_Annty_Type,"Prod/Line",
                		pnd_Insurance_Option,"Life/Cont.",
                		pnd_Life_Contingency," /Fund",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index),"Dividend/Amount",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index), new ReportEditMask ("Z,ZZZ,ZZZ9.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Coll_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                                        //Natural: ADD INV-ACCT-DVDND-AMT ( #INDEX ) TO #COLL-DVDND-AMT
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF CNTRCT-DVDND-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        if (condition(pnd_Total_Dvdnd_Amt.equals(getZero())))                                                                                                             //Natural: IF #TOTAL-DVDND-AMT = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 01 ) ///// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***       No College Dividends for this Period            ***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***       No College Dividends for this Period            ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(65),"Report Total",new ReportTAsterisk(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1)),pnd_Total_Dvdnd_Amt,  //Natural: WRITE ( 01 ) /// 65T 'Report Total' T*INV-ACCT-DVDND-AMT ( 1 ) #TOTAL-DVDND-AMT ( EM = Z,ZZZ,ZZZ9.99- )
                new ReportEditMask ("Z,ZZZ,ZZZ9.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pdaFcpa800_getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_CdeIsBreak = pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde().isBreak(endOfData);
        if (condition(pdaFcpa800_getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_CdeIsBreak))
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(65),"College Total",new ReportTAsterisk(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1)),pnd_Coll_Dvdnd_Amt,  //Natural: WRITE ( 01 ) /// 65T 'College Total' T*INV-ACCT-DVDND-AMT ( 1 ) #COLL-DVDND-AMT ( EM = Z,ZZZ,ZZZ9.99- )
                new ReportEditMask ("Z,ZZZ,ZZZ9.99-"));
            if (condition(Global.isEscape())) return;
            pnd_Total_Dvdnd_Amt.nadd(pnd_Coll_Dvdnd_Amt);                                                                                                                 //Natural: ADD #COLL-DVDND-AMT TO #TOTAL-DVDND-AMT
            pnd_Coll_Dvdnd_Amt.reset();                                                                                                                                   //Natural: RESET #COLL-DVDND-AMT
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(48),"DIVIDEND PAYMENT FOR COLLEGE CODE",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Cde(),NEWLINE,new 
            TabSetting(56),pdaFcpacntl.getCntl_Cntl_Check_Dte(), new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-")," /Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr()," /Annuitant Name",
        		pnd_Name_Line," /Mode",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde(),"Ann/Ins",
        		pnd_Annty_Ins_Type,"Cntr/Typr",
        		pnd_Annty_Type,"Prod/Line",
        		pnd_Insurance_Option,"Life/Cont.",
        		pnd_Life_Contingency," /Fund",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha(),"Dividend/Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt(), new ReportEditMask ("Z,ZZZ,ZZZ9.99-"));
    }
}
