/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:31 PM
**        * FROM NATURAL PROGRAM : Fcpp662
************************************************************
**        * FILE NAME            : Fcpp662.java
**        * CLASS NAME           : Fcpp662
**        * INSTANCE NAME        : Fcpp662
************************************************************
************************************************************************
* PROGRAM  : FCPP662
* SYSTEM   : CPS
* TITLE    : CONSOLIDATED PAYMENT SYSTEM
* AUTHOR   : VALERIE FULTON
* FUNCTION : THIS PROGRAM CREATES A STATISTICAL REPORT OF ALL PAYMENTS
*            IN THE CPS DATABASE FOR A GIVEN PERIOD.
*
* 02/05/98  -FCP-CONS-PYMNT CPS PAYMENT FILE
*
************************************************************************
* NOTE: WHEN MAKING CHANGES TO THIS MODULE, PLEASE CHECK TO SEE IF THE
*       SAME CHANGES NEED TO BE MADE TO FCPP662M.  THAT MODULE WAS
*       CLONED FROM THIS ONE SO THAT THE MONTHLY JOBSTREAM COULD WRITE
*       A LARGER RECORD WITHOUT DISTURBING THIS MODULE.  THIS PROGRAM
*       RUNS IN THE 1400 JOBSTREAM AND DOES NOT CURRENTLY NEED TO WRITE
*       ROTH FIELDS.
*
*       AER - 5/15/08
************************************************************************
* NOTES, ETC:
*
* 11/09/99 : A. YOUNG - REVISED TO PROCESS ELECTRONIC WARRANTS (EW)
*                       RECORDS.
* 09/11/00 : A. YOUNG -  RE-COMPILED DUE TO FCPLPMNT.
*    11/02 : R. CARREON  STOW, EGTRRA CHANGES IN LDA
*    10/04 : T. MCGEE -  FIX MONTHLY PROCEDURE (MARKED TMM)
*    05/08 : AER - ROTH-MAJOR1 - CHANGE FCPA800 TO FCPA800D
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
* 4/2017  : JJG - PIN EXPANSION RESTOW
************************************************************************
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp662 extends BLNatBase
{
    // Data Areas
    private LdaFcplpmnt ldaFcplpmnt;
    private LdaFcpl662 ldaFcpl662;
    private PdaFcpa800d pdaFcpa800d;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Output_Line_1;
    private DbsField pnd_Output_Line_1_Pnd_Date;
    private DbsField pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt;
    private DbsField pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap;
    private DbsField pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap;
    private DbsField pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap;
    private DbsField pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt;
    private DbsField pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap;
    private DbsField pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap;
    private DbsField pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap;

    private DbsGroup pnd_Grand_Total;
    private DbsField pnd_Grand_Total_Pnd_G_T;
    private DbsField pnd_Grand_Total_Pnd_Tot_Ck_Count;
    private DbsField pnd_Grand_Total_Pnd_Tot_Ck_Amount;
    private DbsField pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Cnt;
    private DbsField pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Amount;
    private DbsField pnd_Grand_Total_Pnd_Tot_Eft_Count;
    private DbsField pnd_Grand_Total_Pnd_Tot_Eft_Amount;
    private DbsField pnd_Grand_Total_Pnd_Tot_Non_Ck_Count;
    private DbsField pnd_Grand_Total_Pnd_Tot_Non_Ck_Amount;
    private DbsField pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T;
    private DbsField pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T;

    private DbsGroup pnd_Grand_Total_F;
    private DbsField pnd_Grand_Total_F_Pnd_G_T_F;
    private DbsField pnd_Grand_Total_F_Pnd_Tot_Ck_Count_F;
    private DbsField pnd_Grand_Total_F_Pnd_Tot_Ck_Amount_F;
    private DbsField pnd_Grand_Total_F_Pnd_Tot_Redraw_Ck_Cnt_F;
    private DbsField pnd_Grand_Total_F_Pnd_Tot_Redraw_Ck_Amount_F;
    private DbsField pnd_Grand_Total_F_Pnd_Tot_Eft_Count_F;
    private DbsField pnd_Grand_Total_F_Pnd_Tot_Eft_Amount_F;
    private DbsField pnd_Grand_Total_F_Pnd_Tot_Non_Ck_Count_F;
    private DbsField pnd_Grand_Total_F_Pnd_Tot_Non_Ck_Amount_F;

    private DbsGroup pnd_Summary_Line_1;
    private DbsField pnd_Summary_Line_1_Pnd_Date_Sl;
    private DbsField pnd_Summary_Line_1_Pnd_Total_Check_Ap_Cnt_Sl;
    private DbsField pnd_Summary_Line_1_Pnd_Total_Check_Amt_Sl;
    private DbsField pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Redrawn_Sl;
    private DbsField pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Redrawn_Sl;
    private DbsField pnd_Summary_Line_1_Pnd_Total_Eft_Sl_Cnt;
    private DbsField pnd_Summary_Line_1_Pnd_Total_Eft_Amt_Sl;
    private DbsField pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Sl;
    private DbsField pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Sl;

    private DbsGroup pnd_Summary_Line_Tot;
    private DbsField pnd_Summary_Line_Tot_Pnd_Total_Check_Ap_Cnt_Sl_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Total_Check_Amt_Sl_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Redrawn_Sl_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Redrawn_Sl_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Total_Eft_Sl_Cnt_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Total_Eft_Amt_Sl_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Non_Ck_Sl_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Non_Ck_Sl_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Cnt_T;
    private DbsField pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Amt_T;

    private DbsGroup pnd_Summary_Line_Org_Cde;
    private DbsField pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt;
    private DbsField pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt;
    private DbsField pnd_Grand_Total_Count;
    private DbsField pnd_Out_Dte;
    private DbsField pnd_Space_Line;
    private DbsField pnd_Monthly_Or_Daily;
    private DbsField pnd_Hold_Dte;
    private DbsField pnd_Ws_Start_Dte_Key;

    private DbsGroup pnd_Ws_Start_Dte_Key__R_Field_1;
    private DbsField pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a;
    private DbsField pnd_End_Dte;
    private DbsField pnd_End_Dte_A;
    private DbsField pnd_Start_Dte_1_20;
    private DbsField pnd_Title;
    private DbsField pnd_Date_T;
    private DbsField pnd_Start_Dte_1;

    private DbsGroup pnd_Start_Dte_1__R_Field_2;
    private DbsField pnd_Start_Dte_1_Pnd_Start_Yyyy;
    private DbsField pnd_Start_Dte_1_Pnd_Start_Mm;
    private DbsField pnd_Start_Dte_1_Pnd_Start_Dd;
    private DbsField pnd_Gross_Amount;
    private DbsField pnd_Ws_Key;

    private DbsGroup pnd_Ws_Key__R_Field_3;
    private DbsField pnd_Ws_Key_Pnd_Ws_Dte;
    private DbsField pnd_Num;
    private DbsField pnd_A;
    private DbsField pnd_G;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_End_1;
    private DbsField pnd_End_2;
    private DbsField pnd_End_Dte_B;
    private DbsField pnd_Start_1;
    private DbsField pnd_Start_2;
    private DbsField pnd_Start_Dte_1b;

    private DbsGroup pnd_Hold_Cde_Tbl;
    private DbsField pnd_Hold_Cde_Tbl_Pnd_Origin_Cde;

    private DbsGroup pnd_Hold_Cde_Tbl_Pnd_Hold_Cde_Count;
    private DbsField pnd_Hold_Cde_Tbl_Pnd_Hold_Cde;
    private DbsField pnd_Hold_Cde_Tbl_Pnd_Hold_Count;
    private DbsField pnd_H;
    private DbsField pnd_P;
    private DbsField pnd_Hold_Total;
    private DbsField pnd_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        ldaFcpl662 = new LdaFcpl662();
        registerRecord(ldaFcpl662);
        localVariables = new DbsRecord();
        pdaFcpa800d = new PdaFcpa800d(localVariables);

        // Local Variables

        pnd_Output_Line_1 = localVariables.newGroupInRecord("pnd_Output_Line_1", "#OUTPUT-LINE-1");
        pnd_Output_Line_1_Pnd_Date = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Date", "#DATE", FieldType.DATE, new DbsArrayController(1, 
            20));
        pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt", "#TOTAL-CHECK-AP-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap", "#TOTAL-CHECK-AMT-AP", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap", "#PYMNT-CNT-REDRAWN-AP", 
            FieldType.PACKED_DECIMAL, 5, new DbsArrayController(1, 20));
        pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap", "#PYMNT-AMNT-REDRAWN-AP", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt", "#TOTAL-EFT-AP-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap", "#TOTAL-EFT-AMT-AP", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap", "#PYMNT-CNT-NON-CK-AP", 
            FieldType.PACKED_DECIMAL, 5, new DbsArrayController(1, 20));
        pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap = pnd_Output_Line_1.newFieldArrayInGroup("pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap", "#PYMNT-AMNT-NON-CK-AP", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));

        pnd_Grand_Total = localVariables.newGroupInRecord("pnd_Grand_Total", "#GRAND-TOTAL");
        pnd_Grand_Total_Pnd_G_T = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_G_T", "#G-T", FieldType.STRING, 11);
        pnd_Grand_Total_Pnd_Tot_Ck_Count = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Ck_Count", "#TOT-CK-COUNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Grand_Total_Pnd_Tot_Ck_Amount = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Ck_Amount", "#TOT-CK-AMOUNT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Cnt = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Cnt", "#TOT-REDRAW-CK-CNT", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Amount = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Amount", "#TOT-REDRAW-CK-AMOUNT", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Grand_Total_Pnd_Tot_Eft_Count = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Eft_Count", "#TOT-EFT-COUNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Grand_Total_Pnd_Tot_Eft_Amount = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Eft_Amount", "#TOT-EFT-AMOUNT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Grand_Total_Pnd_Tot_Non_Ck_Count = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Non_Ck_Count", "#TOT-NON-CK-COUNT", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Grand_Total_Pnd_Tot_Non_Ck_Amount = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Non_Ck_Amount", "#TOT-NON-CK-AMOUNT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T", "#TOT-ORG-CDE-CNT-T", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T = pnd_Grand_Total.newFieldInGroup("pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T", "#TOT-ORG-CDE-AMT-T", FieldType.PACKED_DECIMAL, 
            12, 2);

        pnd_Grand_Total_F = localVariables.newGroupInRecord("pnd_Grand_Total_F", "#GRAND-TOTAL-F");
        pnd_Grand_Total_F_Pnd_G_T_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_G_T_F", "#G-T-F", FieldType.STRING, 11);
        pnd_Grand_Total_F_Pnd_Tot_Ck_Count_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_Tot_Ck_Count_F", "#TOT-CK-COUNT-F", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Grand_Total_F_Pnd_Tot_Ck_Amount_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_Tot_Ck_Amount_F", "#TOT-CK-AMOUNT-F", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Grand_Total_F_Pnd_Tot_Redraw_Ck_Cnt_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_Tot_Redraw_Ck_Cnt_F", "#TOT-REDRAW-CK-CNT-F", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Grand_Total_F_Pnd_Tot_Redraw_Ck_Amount_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_Tot_Redraw_Ck_Amount_F", "#TOT-REDRAW-CK-AMOUNT-F", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Grand_Total_F_Pnd_Tot_Eft_Count_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_Tot_Eft_Count_F", "#TOT-EFT-COUNT-F", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Grand_Total_F_Pnd_Tot_Eft_Amount_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_Tot_Eft_Amount_F", "#TOT-EFT-AMOUNT-F", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Grand_Total_F_Pnd_Tot_Non_Ck_Count_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_Tot_Non_Ck_Count_F", "#TOT-NON-CK-COUNT-F", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Grand_Total_F_Pnd_Tot_Non_Ck_Amount_F = pnd_Grand_Total_F.newFieldInGroup("pnd_Grand_Total_F_Pnd_Tot_Non_Ck_Amount_F", "#TOT-NON-CK-AMOUNT-F", 
            FieldType.PACKED_DECIMAL, 12, 2);

        pnd_Summary_Line_1 = localVariables.newGroupInRecord("pnd_Summary_Line_1", "#SUMMARY-LINE-1");
        pnd_Summary_Line_1_Pnd_Date_Sl = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Date_Sl", "#DATE-SL", FieldType.DATE, new DbsArrayController(1, 
            20));
        pnd_Summary_Line_1_Pnd_Total_Check_Ap_Cnt_Sl = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Total_Check_Ap_Cnt_Sl", "#TOTAL-CHECK-AP-CNT-SL", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Summary_Line_1_Pnd_Total_Check_Amt_Sl = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Total_Check_Amt_Sl", "#TOTAL-CHECK-AMT-SL", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Redrawn_Sl = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Redrawn_Sl", "#PYMNT-CNT-REDRAWN-SL", 
            FieldType.PACKED_DECIMAL, 5, new DbsArrayController(1, 20));
        pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Redrawn_Sl = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Redrawn_Sl", "#PYMNT-AMNT-REDRAWN-SL", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Summary_Line_1_Pnd_Total_Eft_Sl_Cnt = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Total_Eft_Sl_Cnt", "#TOTAL-EFT-SL-CNT", 
            FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Summary_Line_1_Pnd_Total_Eft_Amt_Sl = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Total_Eft_Amt_Sl", "#TOTAL-EFT-AMT-SL", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Sl = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Sl", "#PYMNT-CNT-NON-CK-SL", 
            FieldType.PACKED_DECIMAL, 5, new DbsArrayController(1, 20));
        pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Sl = pnd_Summary_Line_1.newFieldArrayInGroup("pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Sl", "#PYMNT-AMNT-NON-CK-SL", 
            FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));

        pnd_Summary_Line_Tot = localVariables.newGroupInRecord("pnd_Summary_Line_Tot", "#SUMMARY-LINE-TOT");
        pnd_Summary_Line_Tot_Pnd_Total_Check_Ap_Cnt_Sl_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Total_Check_Ap_Cnt_Sl_T", "#TOTAL-CHECK-AP-CNT-SL-T", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Summary_Line_Tot_Pnd_Total_Check_Amt_Sl_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Total_Check_Amt_Sl_T", "#TOTAL-CHECK-AMT-SL-T", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Redrawn_Sl_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Redrawn_Sl_T", "#PYMNT-CNT-REDRAWN-SL-T", 
            FieldType.PACKED_DECIMAL, 5);
        pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Redrawn_Sl_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Redrawn_Sl_T", "#PYMNT-AMNT-REDRAWN-SL-T", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Summary_Line_Tot_Pnd_Total_Eft_Sl_Cnt_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Total_Eft_Sl_Cnt_T", "#TOTAL-EFT-SL-CNT-T", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Summary_Line_Tot_Pnd_Total_Eft_Amt_Sl_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Total_Eft_Amt_Sl_T", "#TOTAL-EFT-AMT-SL-T", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Non_Ck_Sl_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Non_Ck_Sl_T", "#PYMNT-CNT-NON-CK-SL-T", 
            FieldType.PACKED_DECIMAL, 5);
        pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Non_Ck_Sl_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Non_Ck_Sl_T", "#PYMNT-AMNT-NON-CK-SL-T", 
            FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Cnt_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Cnt_T", "#TOTAL-ORG-CDE-CNT-T", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Amt_T = pnd_Summary_Line_Tot.newFieldInGroup("pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Amt_T", "#TOTAL-ORG-CDE-AMT-T", 
            FieldType.PACKED_DECIMAL, 12, 2);

        pnd_Summary_Line_Org_Cde = localVariables.newGroupInRecord("pnd_Summary_Line_Org_Cde", "#SUMMARY-LINE-ORG-CDE");
        pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt = pnd_Summary_Line_Org_Cde.newFieldArrayInGroup("pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt", 
            "#TOTAL-ORG-CDE-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 20));
        pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt = pnd_Summary_Line_Org_Cde.newFieldArrayInGroup("pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt", 
            "#TOTAL-ORG-CDE-AMT", FieldType.PACKED_DECIMAL, 12, 2, new DbsArrayController(1, 20));
        pnd_Grand_Total_Count = localVariables.newFieldInRecord("pnd_Grand_Total_Count", "#GRAND-TOTAL-COUNT", FieldType.NUMERIC, 4);
        pnd_Out_Dte = localVariables.newFieldInRecord("pnd_Out_Dte", "#OUT-DTE", FieldType.STRING, 20);
        pnd_Space_Line = localVariables.newFieldInRecord("pnd_Space_Line", "#SPACE-LINE", FieldType.STRING, 130);
        pnd_Monthly_Or_Daily = localVariables.newFieldInRecord("pnd_Monthly_Or_Daily", "#MONTHLY-OR-DAILY", FieldType.STRING, 8);
        pnd_Hold_Dte = localVariables.newFieldInRecord("pnd_Hold_Dte", "#HOLD-DTE", FieldType.DATE);
        pnd_Ws_Start_Dte_Key = localVariables.newFieldInRecord("pnd_Ws_Start_Dte_Key", "#WS-START-DTE-KEY", FieldType.STRING, 4);

        pnd_Ws_Start_Dte_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Start_Dte_Key__R_Field_1", "REDEFINE", pnd_Ws_Start_Dte_Key);
        pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a = pnd_Ws_Start_Dte_Key__R_Field_1.newFieldInGroup("pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a", "#START-DTE-1A", 
            FieldType.DATE);
        pnd_End_Dte = localVariables.newFieldInRecord("pnd_End_Dte", "#END-DTE", FieldType.DATE);
        pnd_End_Dte_A = localVariables.newFieldInRecord("pnd_End_Dte_A", "#END-DTE-A", FieldType.STRING, 8);
        pnd_Start_Dte_1_20 = localVariables.newFieldInRecord("pnd_Start_Dte_1_20", "#START-DTE-1-20", FieldType.STRING, 22);
        pnd_Title = localVariables.newFieldInRecord("pnd_Title", "#TITLE", FieldType.STRING, 29);
        pnd_Date_T = localVariables.newFieldInRecord("pnd_Date_T", "#DATE-T", FieldType.STRING, 4);
        pnd_Start_Dte_1 = localVariables.newFieldInRecord("pnd_Start_Dte_1", "#START-DTE-1", FieldType.STRING, 8);

        pnd_Start_Dte_1__R_Field_2 = localVariables.newGroupInRecord("pnd_Start_Dte_1__R_Field_2", "REDEFINE", pnd_Start_Dte_1);
        pnd_Start_Dte_1_Pnd_Start_Yyyy = pnd_Start_Dte_1__R_Field_2.newFieldInGroup("pnd_Start_Dte_1_Pnd_Start_Yyyy", "#START-YYYY", FieldType.NUMERIC, 
            4);
        pnd_Start_Dte_1_Pnd_Start_Mm = pnd_Start_Dte_1__R_Field_2.newFieldInGroup("pnd_Start_Dte_1_Pnd_Start_Mm", "#START-MM", FieldType.NUMERIC, 2);
        pnd_Start_Dte_1_Pnd_Start_Dd = pnd_Start_Dte_1__R_Field_2.newFieldInGroup("pnd_Start_Dte_1_Pnd_Start_Dd", "#START-DD", FieldType.NUMERIC, 2);
        pnd_Gross_Amount = localVariables.newFieldInRecord("pnd_Gross_Amount", "#GROSS-AMOUNT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Key = localVariables.newFieldInRecord("pnd_Ws_Key", "#WS-KEY", FieldType.STRING, 4);

        pnd_Ws_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Key__R_Field_3", "REDEFINE", pnd_Ws_Key);
        pnd_Ws_Key_Pnd_Ws_Dte = pnd_Ws_Key__R_Field_3.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Dte", "#WS-DTE", FieldType.DATE);
        pnd_Num = localVariables.newFieldInRecord("pnd_Num", "#NUM", FieldType.NUMERIC, 1);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_G = localVariables.newFieldInRecord("pnd_G", "#G", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_End_1 = localVariables.newFieldInRecord("pnd_End_1", "#END-1", FieldType.DATE);
        pnd_End_2 = localVariables.newFieldInRecord("pnd_End_2", "#END-2", FieldType.STRING, 8);
        pnd_End_Dte_B = localVariables.newFieldInRecord("pnd_End_Dte_B", "#END-DTE-B", FieldType.STRING, 8);
        pnd_Start_1 = localVariables.newFieldInRecord("pnd_Start_1", "#START-1", FieldType.STRING, 6);
        pnd_Start_2 = localVariables.newFieldInRecord("pnd_Start_2", "#START-2", FieldType.STRING, 8);
        pnd_Start_Dte_1b = localVariables.newFieldInRecord("pnd_Start_Dte_1b", "#START-DTE-1B", FieldType.DATE);

        pnd_Hold_Cde_Tbl = localVariables.newGroupArrayInRecord("pnd_Hold_Cde_Tbl", "#HOLD-CDE-TBL", new DbsArrayController(1, 20));
        pnd_Hold_Cde_Tbl_Pnd_Origin_Cde = pnd_Hold_Cde_Tbl.newFieldInGroup("pnd_Hold_Cde_Tbl_Pnd_Origin_Cde", "#ORIGIN-CDE", FieldType.STRING, 2);

        pnd_Hold_Cde_Tbl_Pnd_Hold_Cde_Count = pnd_Hold_Cde_Tbl.newGroupArrayInGroup("pnd_Hold_Cde_Tbl_Pnd_Hold_Cde_Count", "#HOLD-CDE-COUNT", new DbsArrayController(1, 
            30));
        pnd_Hold_Cde_Tbl_Pnd_Hold_Cde = pnd_Hold_Cde_Tbl_Pnd_Hold_Cde_Count.newFieldInGroup("pnd_Hold_Cde_Tbl_Pnd_Hold_Cde", "#HOLD-CDE", FieldType.STRING, 
            4);
        pnd_Hold_Cde_Tbl_Pnd_Hold_Count = pnd_Hold_Cde_Tbl_Pnd_Hold_Cde_Count.newFieldInGroup("pnd_Hold_Cde_Tbl_Pnd_Hold_Count", "#HOLD-COUNT", FieldType.NUMERIC, 
            5);
        pnd_H = localVariables.newFieldInRecord("pnd_H", "#H", FieldType.NUMERIC, 2);
        pnd_P = localVariables.newFieldInRecord("pnd_P", "#P", FieldType.NUMERIC, 2);
        pnd_Hold_Total = localVariables.newFieldInRecord("pnd_Hold_Total", "#HOLD-TOTAL", FieldType.NUMERIC, 7);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmnt.initializeValues();
        ldaFcpl662.initializeValues();

        localVariables.reset();
        pnd_Grand_Total_Pnd_G_T.setInitialValue("TOTAL");
        pnd_Grand_Total_F_Pnd_G_T_F.setInitialValue("GRAND TOTAL");
        pnd_Space_Line.setInitialValue(" ");
        pnd_Monthly_Or_Daily.setInitialValue(" ");
        pnd_Title.setInitialValue("GROSS PAYMENT STATISTICS FOR ");
        pnd_Date_T.setInitialValue("DATE");
        pnd_Num.setInitialValue(0);
        pnd_G.setInitialValue(1);
        pnd_I.setInitialValue(1);
        pnd_X.setInitialValue(0);
        pnd_H.setInitialValue(0);
        pnd_P.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp662() throws Exception
    {
        super("Fcpp662");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp662|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT UC = *;//Natural: FORMAT ( 1 ) PS = 60 LS = 132;//Natural: FORMAT ( 2 ) PS = 60 LS = 132
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Monthly_Or_Daily, new FieldAttributes ("AD='_'"));                                                 //Natural: INPUT #MONTHLY-OR-DAILY ( AD = '_' )
                pnd_Title.setValue("GROSS PAYMENT STATISTICS FOR ");                                                                                                      //Natural: MOVE 'GROSS PAYMENT STATISTICS FOR ' TO #TITLE
                pnd_Date_T.setValue("DATE");                                                                                                                              //Natural: MOVE 'DATE' TO #DATE-T
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 1 )
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 2 )
                                                                                                                                                                          //Natural: PERFORM INIT-HOLD-CDE-TBL
                sub_Init_Hold_Cde_Tbl();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                if (condition(pnd_Monthly_Or_Daily.equals("DAILY")))                                                                                                      //Natural: IF #MONTHLY-OR-DAILY = 'DAILY'
                {
                    pnd_Start_Dte_1.compute(new ComputeParameters(false, pnd_Start_Dte_1), Global.getDATX().subtract(1));                                                 //Natural: ASSIGN #START-DTE-1 := *DATX - 1
                    //*  #START-DTE-1 :=  '03/02/98'
                    pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a.setValueEdited(new ReportEditMask("MM/DD/YY"),pnd_Start_Dte_1);                                                 //Natural: MOVE EDITED #START-DTE-1 TO #START-DTE-1A ( EM = MM/DD/YY )
                    pnd_End_Dte.setValueEdited(new ReportEditMask("MM/DD/YY"),pnd_Start_Dte_1);                                                                           //Natural: MOVE EDITED #START-DTE-1 TO #END-DTE ( EM = MM/DD/YY )
                    pnd_Start_Dte_1_20.setValueEdited(pnd_End_Dte,new ReportEditMask("LLLLLLLLL', 'DD' 'YYYY"));                                                          //Natural: MOVE EDITED #END-DTE ( EM = LLLLLLLLL', 'DD' 'YYYY ) TO #START-DTE-1-20
                    pnd_Output_Line_1_Pnd_Date.getValue(1).setValue(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a);                                                               //Natural: ASSIGN #DATE ( 1 ) := #START-DTE-1A
                                                                                                                                                                          //Natural: PERFORM READ-DAILY-MONTHLY
                    sub_Read_Daily_Monthly();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCUM-GRAND-TOTALS
                    sub_Accum_Grand_Totals();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORT
                    sub_Summary_Report();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-CDE
                    sub_Write_Hold_Cde();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Monthly_Or_Daily.equals("MONTHLY")))                                                                                                //Natural: IF #MONTHLY-OR-DAILY = 'MONTHLY'
                    {
                        pnd_End_Dte_A.setValueEdited(Global.getDATX(),new ReportEditMask("YYYYMM01"));                                                                    //Natural: MOVE EDITED *DATX ( EM = YYYYMM01 ) TO #END-DTE-A
                        //*    MOVE EDITED *DATX (EM=YYYY0701)  TO #END-DTE-A
                        pnd_End_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_End_Dte_A);                                                                         //Natural: MOVE EDITED #END-DTE-A TO #END-DTE ( EM = YYYYMMDD )
                        pnd_End_Dte.nsubtract(1);                                                                                                                         //Natural: ASSIGN #END-DTE := #END-DTE - 1
                        pnd_Start_Dte_1_20.setValueEdited(pnd_End_Dte,new ReportEditMask("LLLLLLLLL', 'YYYY"));                                                           //Natural: MOVE EDITED #END-DTE ( EM = LLLLLLLLL', 'YYYY ) TO #START-DTE-1-20
                        pnd_Start_Dte_1.setValueEdited(pnd_End_Dte,new ReportEditMask("YYYYMM01"));                                                                       //Natural: MOVE EDITED #END-DTE ( EM = YYYYMM01 ) TO #START-DTE-1
                        pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Start_Dte_1);                                             //Natural: MOVE EDITED #START-DTE-1 TO #START-DTE-1A ( EM = YYYYMMDD )
                                                                                                                                                                          //Natural: PERFORM READ-PYMNT-ADDR-FLAT
                        sub_Read_Pymnt_Addr_Flat();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        //*  TMM REPAIR P4000CPM
                                                                                                                                                                          //Natural: PERFORM ACCUM-GRAND-TOTALS
                        sub_Accum_Grand_Totals();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORT
                        sub_Summary_Report();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Output_Line_1_Pnd_Date.getValue(1).setValue(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a);                                                           //Natural: ASSIGN #DATE ( 1 ) := #START-DTE-1A
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-CDE
                        sub_Write_Hold_Cde();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM DAY-OR-MONTH
                        sub_Day_Or_Month();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-CDE
                        sub_Write_Hold_Cde();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-HOLD-CDE-TBL
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DAY-OR-MONTH
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DAY-ROUTINE
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MONTH-ROUTINE
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-DAILY-MONTHLY
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PYMNT-ADDR-FLAT
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-HOLD-DATE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-PAYMENTS
                //*  VALUE 3
                //*  VALUE 3, 6
                //* **********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COUNT-HOLD-CDE
                //* **********************************************************************
                //* **********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-GROSS-AMOUNT
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-REPORT
                //* *
                getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                               //Natural: WRITE ( 1 ) #SPACE-LINE
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                               //Natural: WRITE ( 1 ) #SPACE-LINE
                if (Global.isEscape()) return;
                //* **********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT
                //* * SUMMARY REPORT ORGIN CODE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-OUTPUT-FEILDS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-GRAND-TOTALS
                //* * TOTAL
                //* * GRAND TOTAL
                //* * SUMMARY REPORT
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HOLD-CDE
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HOLD-COUNTERS
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PAGE-BRAKE
                //* ***********************************************************************
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Init_Hold_Cde_Tbl() throws Exception                                                                                                                 //Natural: INIT-HOLD-CDE-TBL
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #H 1 THRU 20
        for (pnd_H.setValue(1); condition(pnd_H.lessOrEqual(20)); pnd_H.nadd(1))
        {
            if (condition(ldaFcpl662.getCps_Origin_Cdes().getValue(pnd_H).equals(" ")))                                                                                   //Natural: IF CPS-ORIGIN-CDES ( #H ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Cde_Tbl_Pnd_Origin_Cde.getValue(pnd_H).setValue(ldaFcpl662.getCps_Origin_Cdes().getValue(pnd_H));                                                //Natural: MOVE CPS-ORIGIN-CDES ( #H ) TO #ORIGIN-CDE ( #H )
                pnd_X.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #X
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Day_Or_Month() throws Exception                                                                                                                      //Natural: DAY-OR-MONTH
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.examine(new ExamineSource(pnd_Monthly_Or_Daily,true), new ExamineSearch(" "), new ExamineGivingNumber(pnd_Num));                                          //Natural: EXAMINE FULL #MONTHLY-OR-DAILY FOR ' ' GIVING #NUM
        if (condition(pnd_Num.equals(getZero())))                                                                                                                         //Natural: IF #NUM = 0
        {
                                                                                                                                                                          //Natural: PERFORM DAY-ROUTINE
            sub_Day_Routine();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM MONTH-ROUTINE
            sub_Month_Routine();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Day_Routine() throws Exception                                                                                                                       //Natural: DAY-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Start_Dte_1.setValue(pnd_Monthly_Or_Daily);                                                                                                                   //Natural: ASSIGN #START-DTE-1 := #MONTHLY-OR-DAILY
        pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Start_Dte_1);                                                             //Natural: MOVE EDITED #START-DTE-1 TO #START-DTE-1A ( EM = MMDDYYYY )
        pnd_End_Dte.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Start_Dte_1);                                                                                       //Natural: MOVE EDITED #START-DTE-1 TO #END-DTE ( EM = MMDDYYYY )
        pnd_Start_Dte_1_20.setValueEdited(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a,new ReportEditMask("LLLLLLLLL', 'DD' 'YYYY"));                                            //Natural: MOVE EDITED #START-DTE-1A ( EM = LLLLLLLLL', 'DD' 'YYYY ) TO #START-DTE-1-20
                                                                                                                                                                          //Natural: PERFORM READ-DAILY-MONTHLY
        sub_Read_Daily_Monthly();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Output_Line_1_Pnd_Date.getValue(1).setValue(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a);                                                                           //Natural: ASSIGN #DATE ( 1 ) := #START-DTE-1A
                                                                                                                                                                          //Natural: PERFORM ACCUM-GRAND-TOTALS
        sub_Accum_Grand_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORT
        sub_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Month_Routine() throws Exception                                                                                                                     //Natural: MONTH-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Start_1.setValue(pnd_Monthly_Or_Daily);                                                                                                                       //Natural: ASSIGN #START-1 := #MONTHLY-OR-DAILY
        pnd_Start_Dte_1b.setValueEdited(new ReportEditMask("MMYYYY"),pnd_Start_1);                                                                                        //Natural: MOVE EDITED #START-1 TO #START-DTE-1B ( EM = MMYYYY )
        pnd_Start_2.setValueEdited(pnd_Start_Dte_1b,new ReportEditMask("MM01YYYY"));                                                                                      //Natural: MOVE EDITED #START-DTE-1B ( EM = MM01YYYY ) TO #START-2
        pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Start_2);                                                                 //Natural: MOVE EDITED #START-2 TO #START-DTE-1A ( EM = MMDDYYYY )
        pnd_End_Dte.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_Start_2);                                                                                           //Natural: MOVE EDITED #START-2 TO #END-DTE ( EM = MMDDYYYY )
        pnd_End_Dte.nadd(31);                                                                                                                                             //Natural: ASSIGN #END-DTE := #END-DTE + 31
        pnd_End_1.setValue(pnd_End_Dte);                                                                                                                                  //Natural: MOVE #END-DTE TO #END-1
        pnd_End_2.setValueEdited(pnd_End_1,new ReportEditMask("MM01YYYY"));                                                                                               //Natural: MOVE EDITED #END-1 ( EM = MM01YYYY ) TO #END-2
        pnd_End_Dte.setValueEdited(new ReportEditMask("MMDDYYYY"),pnd_End_2);                                                                                             //Natural: MOVE EDITED #END-2 TO #END-DTE ( EM = MMDDYYYY )
        pnd_End_Dte.nsubtract(1);                                                                                                                                         //Natural: ASSIGN #END-DTE := #END-DTE - 1
        pnd_Start_Dte_1_20.setValueEdited(pnd_End_Dte,new ReportEditMask("LLLLLLLLL', 'YYYY"));                                                                           //Natural: MOVE EDITED #END-DTE ( EM = LLLLLLLLL', 'YYYY ) TO #START-DTE-1-20
        pnd_Monthly_Or_Daily.setValue("MONTHLY");                                                                                                                         //Natural: MOVE 'MONTHLY' TO #MONTHLY-OR-DAILY
                                                                                                                                                                          //Natural: PERFORM READ-DAILY-MONTHLY
        sub_Read_Daily_Monthly();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM SUMMARY-REPORT
        sub_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Output_Line_1_Pnd_Date.getValue(1).setValue(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a);                                                                           //Natural: ASSIGN #DATE ( 1 ) := #START-DTE-1A
    }
    private void sub_Read_Daily_Monthly() throws Exception                                                                                                                //Natural: READ-DAILY-MONTHLY
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNT BY CHK-DTE-ORG-CURR-TYP-SEQ STARTING FROM #WS-START-DTE-KEY
        (
        "RD1",
        new Wc[] { new Wc("CHK_DTE_ORG_CURR_TYP_SEQ", ">=", pnd_Ws_Start_Dte_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CHK_DTE_ORG_CURR_TYP_SEQ", "ASC") }
        );
        RD1:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("RD1")))
        {
            //*  ACCEPT IF PYMNT-INSTMT-NMR = 01
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                     //Natural: MOVE BY NAME FCP-CONS-PYMNT TO WF-PYMNT-ADDR-GRP.WF-PYMNT-ADDR-REC
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().equals(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a) && pnd_Monthly_Or_Daily.equals("DAILY")))      //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE = #START-DTE-1A AND #MONTHLY-OR-DAILY = 'DAILY'
            {
                                                                                                                                                                          //Natural: PERFORM ACCUM-PAYMENTS
                sub_Accum_Payments();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().equals(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a) && pnd_Monthly_Or_Daily.equals("MONTHLY")))    //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE = #START-DTE-1A AND #MONTHLY-OR-DAILY = 'MONTHLY'
            {
                                                                                                                                                                          //Natural: PERFORM ACCUM-PAYMENTS
                sub_Accum_Payments();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().greater(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a) && pnd_Monthly_Or_Daily.equals("MONTHLY"))) //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE > #START-DTE-1A AND #MONTHLY-OR-DAILY = 'MONTHLY'
                {
                                                                                                                                                                          //Natural: PERFORM CHECK-HOLD-DATE
                    sub_Check_Hold_Date();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCUM-PAYMENTS
                    sub_Accum_Payments();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().equals(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a)))                                              //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE = #START-DTE-1A
            {
                if (condition(pnd_Monthly_Or_Daily.notEquals("MONTHLY")))                                                                                                 //Natural: IF #MONTHLY-OR-DAILY NE 'MONTHLY'
                {
                    if (condition(pnd_Monthly_Or_Daily.notEquals("DAILY")))                                                                                               //Natural: IF #MONTHLY-OR-DAILY NE 'DAILY'
                    {
                                                                                                                                                                          //Natural: PERFORM ACCUM-PAYMENTS
                        sub_Accum_Payments();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().greater(pnd_End_Dte)))                                                                       //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE > #END-DTE
            {
                if (true) break RD1;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD1. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Read_Pymnt_Addr_Flat() throws Exception                                                                                                              //Natural: READ-PYMNT-ADDR-FLAT
    {
        if (BLNatReinput.isReinput()) return;

        RD2:                                                                                                                                                              //Natural: READ WORK FILE 1 RECORD WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().equals(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a) && pnd_Monthly_Or_Daily.equals("MONTHLY")))    //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE = #START-DTE-1A AND #MONTHLY-OR-DAILY = 'MONTHLY'
            {
                                                                                                                                                                          //Natural: PERFORM ACCUM-PAYMENTS
                sub_Accum_Payments();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().greater(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a) && pnd_Monthly_Or_Daily.equals("MONTHLY"))) //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE > #START-DTE-1A AND #MONTHLY-OR-DAILY = 'MONTHLY'
                {
                                                                                                                                                                          //Natural: PERFORM CHECK-HOLD-DATE
                    sub_Check_Hold_Date();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCUM-PAYMENTS
                    sub_Accum_Payments();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD2"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD2"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().greater(pnd_End_Dte)))                                                                       //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE > #END-DTE
            {
                if (true) break RD2;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RD2. )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        RD2_Exit:
        if (Global.isEscape()) return;
    }
    private void sub_Check_Hold_Date() throws Exception                                                                                                                   //Natural: CHECK-HOLD-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte().greater(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a)))                                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE > #START-DTE-1A
        {
            pnd_Output_Line_1_Pnd_Date.getValue(1).setValue(pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a);                                                                       //Natural: ASSIGN #DATE ( 1 ) := #START-DTE-1A
            getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                                   //Natural: WRITE ( 1 ) #SPACE-LINE
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                                   //Natural: WRITE ( 1 ) #SPACE-LINE
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
            sub_Write_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCUM-GRAND-TOTALS
            sub_Accum_Grand_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Start_Dte_Key_Pnd_Start_Dte_1a.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte());                                                           //Natural: ASSIGN #START-DTE-1A := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE
                                                                                                                                                                          //Natural: PERFORM INIT-OUTPUT-FEILDS
            sub_Init_Output_Feilds();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Payments() throws Exception                                                                                                                    //Natural: ACCUM-PAYMENTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.examine(new ExamineSource(ldaFcpl662.getCps_Origin_Cdes().getValue("*")), new ExamineSearch(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde()),          //Natural: EXAMINE CPS-ORIGIN-CDES ( * ) FOR WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE GIVING INDEX #I
            new ExamineGivingIndex(pnd_I));
        if (condition(pnd_I.equals(getZero())))                                                                                                                           //Natural: IF #I = 0
        {
            Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                              //Natural: ESCAPE BOTTOM
            if (true) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().notEquals(" ")))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE NE ' '
        {
                                                                                                                                                                          //Natural: PERFORM COUNT-HOLD-CDE
            sub_Count_Hold_Cde();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1118 = 0;                                                                                                                                //Natural: DECIDE ON EVERY VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1)))
        {
            decideConditionsMet1118++;
                                                                                                                                                                          //Natural: PERFORM ACCUM-GROSS-AMOUNT
            sub_Accum_Gross_Amount();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap.getValue(pnd_I).nadd(pnd_Gross_Amount);                                                                              //Natural: ADD #GROSS-AMOUNT TO #TOTAL-CHECK-AMT-AP ( #I )
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 01
            {
                pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt.getValue(pnd_I).nadd(1);                                                                                         //Natural: ADD 1 TO #TOTAL-CHECK-AP-CNT ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2)))
        {
            decideConditionsMet1118++;
                                                                                                                                                                          //Natural: PERFORM ACCUM-GROSS-AMOUNT
            sub_Accum_Gross_Amount();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap.getValue(pnd_I).nadd(pnd_Gross_Amount);                                                                                //Natural: ADD #GROSS-AMOUNT TO #TOTAL-EFT-AMT-AP ( #I )
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 01
            {
                pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt.getValue(pnd_I).nadd(1);                                                                                           //Natural: ADD 1 TO #TOTAL-EFT-AP-CNT ( #I )
                //*  TMM-2001-11
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 3,6,4,9
        if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3)) || (pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(6)) 
            || (pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4)) || (pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9))))
        {
            decideConditionsMet1118++;
                                                                                                                                                                          //Natural: PERFORM ACCUM-GROSS-AMOUNT
            sub_Accum_Gross_Amount();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I).nadd(pnd_Gross_Amount);                                                                            //Natural: ADD #GROSS-AMOUNT TO #PYMNT-AMNT-NON-CK-AP ( #I )
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 01
            {
                pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I).nadd(1);                                                                                        //Natural: ADD 1 TO #PYMNT-CNT-NON-CK-AP ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 8
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8)))
        {
            decideConditionsMet1118++;
                                                                                                                                                                          //Natural: PERFORM ACCUM-GROSS-AMOUNT
            sub_Accum_Gross_Amount();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I).nadd(pnd_Gross_Amount);                                                                            //Natural: ADD #GROSS-AMOUNT TO #PYMNT-AMNT-NON-CK-AP ( #I )
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 01
            {
                pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I).nadd(1);                                                                                        //Natural: ADD 1 TO #PYMNT-CNT-NON-CK-AP ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 1
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1)))
        {
            decideConditionsMet1118++;
                                                                                                                                                                          //Natural: PERFORM ACCUM-GROSS-AMOUNT
            sub_Accum_Gross_Amount();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                  //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap.getValue(pnd_I).nadd(pnd_Gross_Amount);                                                                       //Natural: ADD #GROSS-AMOUNT TO #PYMNT-AMNT-REDRAWN-AP ( #I )
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                             //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 01
                {
                    pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap.getValue(pnd_I).nadd(1);                                                                                   //Natural: ADD 1 TO #PYMNT-CNT-REDRAWN-AP ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        if (condition(decideConditionsMet1118 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Count_Hold_Cde() throws Exception                                                                                                                    //Natural: COUNT-HOLD-CDE
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #H 1 THRU 30
        for (pnd_H.setValue(1); condition(pnd_H.lessOrEqual(30)); pnd_H.nadd(1))
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(pnd_Hold_Cde_Tbl_Pnd_Hold_Cde.getValue(pnd_I,pnd_H))))                                //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = #HOLD-CDE ( #I,#H )
            {
                pnd_Hold_Cde_Tbl_Pnd_Hold_Count.getValue(pnd_I,pnd_H).nadd(1);                                                                                            //Natural: ADD 1 TO #HOLD-COUNT ( #I,#H )
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Hold_Cde_Tbl_Pnd_Hold_Cde.getValue(pnd_I,pnd_H).equals(" ")))                                                                           //Natural: IF #HOLD-CDE ( #I,#H ) = ' '
                {
                    pnd_Hold_Cde_Tbl_Pnd_Hold_Cde.getValue(pnd_I,pnd_H).setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());                                     //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE TO #HOLD-CDE ( #I,#H )
                    pnd_Hold_Cde_Tbl_Pnd_Hold_Count.getValue(pnd_I,pnd_H).nadd(1);                                                                                        //Natural: ADD 1 TO #HOLD-COUNT ( #I,#H )
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Gross_Amount() throws Exception                                                                                                                //Natural: ACCUM-GROSS-AMOUNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************************************
        pnd_Gross_Amount.setValue(0);                                                                                                                                     //Natural: MOVE 0 TO #GROSS-AMOUNT
        FOR03:                                                                                                                                                            //Natural: FOR #G 1 TO 40
        for (pnd_G.setValue(1); condition(pnd_G.lessOrEqual(40)); pnd_G.nadd(1))
        {
            pnd_Gross_Amount.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_G));                                                                 //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #G ) TO #GROSS-AMOUNT
            pnd_Gross_Amount.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_G));                                                                 //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #G ) TO #GROSS-AMOUNT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Summary_Report() throws Exception                                                                                                                    //Natural: SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Monthly_Or_Daily.equals("DAILY")))                                                                                                              //Natural: IF #MONTHLY-OR-DAILY = 'DAILY'
        {
            pnd_Title.setValue("GROSS PAYMENT STATISTICS FOR ");                                                                                                          //Natural: MOVE 'GROSS PAYMENT STATISTICS FOR ' TO #TITLE
            pnd_Date_T.setValue("    ");                                                                                                                                  //Natural: MOVE '    ' TO #DATE-T
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Monthly_Or_Daily.equals("MONTHLY")))                                                                                                        //Natural: IF #MONTHLY-OR-DAILY = 'MONTHLY'
            {
                pnd_Title.setValue("GROSS MONTH-END SUMMARY ");                                                                                                           //Natural: MOVE 'GROSS MONTH-END SUMMARY ' TO #TITLE
                pnd_Date_T.setValue("    ");                                                                                                                              //Natural: MOVE '    ' TO #DATE-T
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Title.setValue("GROSS PAYMENT STATISTICS FOR ");                                                                                                      //Natural: MOVE 'GROSS PAYMENT STATISTICS FOR ' TO #TITLE
                pnd_Date_T.setValue("    ");                                                                                                                              //Natural: MOVE '    ' TO #DATE-T
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR04:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I)),  //Natural: ADD #TOTAL-CHECK-AP-CNT-SL ( #I ) #TOTAL-EFT-SL-CNT ( #I ) #PYMNT-CNT-NON-CK-SL ( #I ) TO #TOTAL-ORG-CDE-CNT ( #I )
                pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I).add(pnd_Summary_Line_1_Pnd_Total_Check_Ap_Cnt_Sl.getValue(pnd_I)).add(pnd_Summary_Line_1_Pnd_Total_Eft_Sl_Cnt.getValue(pnd_I)).add(pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Sl.getValue(pnd_I)));
            pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I)),  //Natural: ADD #TOTAL-CHECK-AMT-SL ( #I ) #TOTAL-EFT-AMT-SL ( #I ) #PYMNT-AMNT-NON-CK-SL ( #I ) TO #TOTAL-ORG-CDE-AMT ( #I )
                pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I).add(pnd_Summary_Line_1_Pnd_Total_Check_Amt_Sl.getValue(pnd_I)).add(pnd_Summary_Line_1_Pnd_Total_Eft_Amt_Sl.getValue(pnd_I)).add(pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Sl.getValue(pnd_I)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,ldaFcpl662.getCps_Origin_Cdes().getValue(pnd_I),new ColumnSpacing(1),pnd_Summary_Line_1_Pnd_Total_Check_Ap_Cnt_Sl.getValue(pnd_I),  //Natural: WRITE ( 1 ) CPS-ORIGIN-CDES ( #I ) 1X #TOTAL-CHECK-AP-CNT-SL ( #I ) 1X #TOTAL-CHECK-AMT-SL ( #I ) 1X #TOTAL-EFT-SL-CNT ( #I ) 1X #TOTAL-EFT-AMT-SL ( #I ) 1X #PYMNT-CNT-NON-CK-SL ( #I ) 1X #PYMNT-AMNT-NON-CK-SL ( #I ) 1X #TOTAL-ORG-CDE-CNT ( #I ) 1X #TOTAL-ORG-CDE-AMT ( #I ) 1X #PYMNT-CNT-REDRAWN-SL ( #I ) 1X #PYMNT-AMNT-REDRAWN-SL ( #I )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_1_Pnd_Total_Check_Amt_Sl.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(1),pnd_Summary_Line_1_Pnd_Total_Eft_Sl_Cnt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_1_Pnd_Total_Eft_Amt_Sl.getValue(pnd_I), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Sl.getValue(pnd_I), new ReportEditMask 
                ("ZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Sl.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(1),pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Redrawn_Sl.getValue(pnd_I), new ReportEditMask 
                ("ZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Redrawn_Sl.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* * SUMMARY REPORT TOTAL LINE
            pnd_Summary_Line_Tot_Pnd_Total_Check_Ap_Cnt_Sl_T.nadd(pnd_Summary_Line_1_Pnd_Total_Check_Ap_Cnt_Sl.getValue(pnd_I));                                          //Natural: ADD #TOTAL-CHECK-AP-CNT-SL ( #I ) TO #TOTAL-CHECK-AP-CNT-SL-T
            pnd_Summary_Line_Tot_Pnd_Total_Check_Amt_Sl_T.nadd(pnd_Summary_Line_1_Pnd_Total_Check_Amt_Sl.getValue(pnd_I));                                                //Natural: ADD #TOTAL-CHECK-AMT-SL ( #I ) TO #TOTAL-CHECK-AMT-SL-T
            pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Redrawn_Sl_T.nadd(pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Redrawn_Sl.getValue(pnd_I));                                            //Natural: ADD #PYMNT-CNT-REDRAWN-SL ( #I ) TO #PYMNT-CNT-REDRAWN-SL-T
            pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Redrawn_Sl_T.nadd(pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Redrawn_Sl.getValue(pnd_I));                                          //Natural: ADD #PYMNT-AMNT-REDRAWN-SL ( #I ) TO #PYMNT-AMNT-REDRAWN-SL-T
            pnd_Summary_Line_Tot_Pnd_Total_Eft_Sl_Cnt_T.nadd(pnd_Summary_Line_1_Pnd_Total_Eft_Sl_Cnt.getValue(pnd_I));                                                    //Natural: ADD #TOTAL-EFT-SL-CNT ( #I ) TO #TOTAL-EFT-SL-CNT-T
            pnd_Summary_Line_Tot_Pnd_Total_Eft_Amt_Sl_T.nadd(pnd_Summary_Line_1_Pnd_Total_Eft_Amt_Sl.getValue(pnd_I));                                                    //Natural: ADD #TOTAL-EFT-AMT-SL ( #I ) TO #TOTAL-EFT-AMT-SL-T
            pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Non_Ck_Sl_T.nadd(pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Sl.getValue(pnd_I));                                              //Natural: ADD #PYMNT-CNT-NON-CK-SL ( #I ) TO #PYMNT-CNT-NON-CK-SL-T
            pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Non_Ck_Sl_T.nadd(pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Sl.getValue(pnd_I));                                            //Natural: ADD #PYMNT-AMNT-NON-CK-SL ( #I ) TO #PYMNT-AMNT-NON-CK-SL-T
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *
        pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Cnt_T.compute(new ComputeParameters(false, pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Cnt_T), pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Cnt_T.add(pnd_Summary_Line_Tot_Pnd_Total_Check_Ap_Cnt_Sl_T).add(pnd_Summary_Line_Tot_Pnd_Total_Eft_Sl_Cnt_T).add(pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Non_Ck_Sl_T)); //Natural: ADD #TOTAL-CHECK-AP-CNT-SL-T #TOTAL-EFT-SL-CNT-T #PYMNT-CNT-NON-CK-SL-T TO #TOTAL-ORG-CDE-CNT-T
        pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Amt_T.compute(new ComputeParameters(false, pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Amt_T), pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Amt_T.add(pnd_Summary_Line_Tot_Pnd_Total_Check_Amt_Sl_T).add(pnd_Summary_Line_Tot_Pnd_Total_Eft_Amt_Sl_T).add(pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Non_Ck_Sl_T)); //Natural: ADD #TOTAL-CHECK-AMT-SL-T #TOTAL-EFT-AMT-SL-T #PYMNT-AMNT-NON-CK-SL-T TO #TOTAL-ORG-CDE-AMT-T
        getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                                       //Natural: WRITE ( 1 ) #SPACE-LINE
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(3),pnd_Summary_Line_Tot_Pnd_Total_Check_Ap_Cnt_Sl_T, new ReportEditMask ("Z,ZZZ,ZZ9"),new            //Natural: WRITE ( 1 ) 3X #TOTAL-CHECK-AP-CNT-SL-T 1X #TOTAL-CHECK-AMT-SL-T 1X #TOTAL-EFT-SL-CNT-T 1X #TOTAL-EFT-AMT-SL-T 1X #PYMNT-CNT-NON-CK-SL-T 1X #PYMNT-AMNT-NON-CK-SL-T 1X #TOTAL-ORG-CDE-CNT-T 1X #TOTAL-ORG-CDE-AMT-T 1X #PYMNT-CNT-REDRAWN-SL-T 1X #PYMNT-AMNT-REDRAWN-SL-T
            ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Total_Check_Amt_Sl_T, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Total_Eft_Sl_Cnt_T, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Total_Eft_Amt_Sl_T, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new 
            ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Non_Ck_Sl_T, new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Non_Ck_Sl_T, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Cnt_T, new ReportEditMask ("Z,ZZZ,ZZ9"),new 
            ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Total_Org_Cde_Amt_T, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Pymnt_Cnt_Redrawn_Sl_T, 
            new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_Tot_Pnd_Pymnt_Amnt_Redrawn_Sl_T, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
    }
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        if (condition(pnd_Grand_Total_Count.equals(3)))                                                                                                                   //Natural: IF #GRAND-TOTAL-COUNT = 3
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            pnd_Grand_Total_Count.setValue(0);                                                                                                                            //Natural: MOVE 0 TO #GRAND-TOTAL-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        FOR06:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I)),  //Natural: ADD #TOTAL-CHECK-AP-CNT ( #I ) #TOTAL-EFT-AP-CNT ( #I ) #PYMNT-CNT-NON-CK-AP ( #I ) TO #TOTAL-ORG-CDE-CNT ( #I )
                pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I).add(pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt.getValue(pnd_I)).add(pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt.getValue(pnd_I)).add(pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I)));
            pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I)),  //Natural: ADD #TOTAL-CHECK-AMT-AP ( #I ) #TOTAL-EFT-AMT-AP ( #I ) #PYMNT-AMNT-NON-CK-AP ( #I ) TO #TOTAL-ORG-CDE-AMT ( #I )
                pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I).add(pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap.getValue(pnd_I)).add(pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap.getValue(pnd_I)).add(pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *
        getReports().write(1, ReportOption.NOTITLE,pnd_Output_Line_1_Pnd_Date.getValue(1), new ReportEditMask ("MM/DD/YY"));                                              //Natural: WRITE ( 1 ) #DATE ( 1 )
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,ldaFcpl662.getCps_Origin_Cdes().getValue(pnd_I),new ColumnSpacing(1),pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt.getValue(pnd_I),  //Natural: WRITE ( 1 ) CPS-ORIGIN-CDES ( #I ) 1X #TOTAL-CHECK-AP-CNT ( #I ) 1X #TOTAL-CHECK-AMT-AP ( #I ) 1X #TOTAL-EFT-AP-CNT ( #I ) 1X #TOTAL-EFT-AMT-AP ( #I ) 1X #PYMNT-CNT-NON-CK-AP ( #I ) 1X #PYMNT-AMNT-NON-CK-AP ( #I ) 1X #TOTAL-ORG-CDE-CNT ( #I ) 1X #TOTAL-ORG-CDE-AMT ( #I ) 1X #PYMNT-CNT-REDRAWN-AP ( #I ) 1X #PYMNT-AMNT-REDRAWN-AP ( #I )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(1),pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap.getValue(pnd_I), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I), new ReportEditMask 
                ("ZZ,ZZ9"),new ColumnSpacing(1),pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(1),pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap.getValue(pnd_I), new ReportEditMask 
                ("ZZ,ZZ9"),new ColumnSpacing(1),pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *
    }
    private void sub_Init_Output_Feilds() throws Exception                                                                                                                //Natural: INIT-OUTPUT-FEILDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Grand_Total_Pnd_Tot_Ck_Count.setValue(0);                                                                                                                     //Natural: MOVE 0 TO #TOT-CK-COUNT #TOT-CK-AMOUNT #TOT-REDRAW-CK-CNT #TOT-REDRAW-CK-AMOUNT #TOT-EFT-COUNT #TOT-EFT-AMOUNT #TOT-NON-CK-COUNT #TOT-NON-CK-AMOUNT
        pnd_Grand_Total_Pnd_Tot_Ck_Amount.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Cnt.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Amount.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Eft_Count.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Eft_Amount.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Non_Ck_Count.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Non_Ck_Amount.setValue(0);
        FOR08:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt.getValue(pnd_I).setValue(0);                                                                                         //Natural: MOVE 0 TO #TOTAL-CHECK-AP-CNT ( #I ) #TOTAL-CHECK-AMT-AP ( #I ) #PYMNT-CNT-REDRAWN-AP ( #I ) #PYMNT-AMNT-REDRAWN-AP ( #I ) #TOTAL-EFT-AP-CNT ( #I ) #TOTAL-EFT-AMT-AP ( #I ) #PYMNT-CNT-NON-CK-AP ( #I ) #PYMNT-AMNT-NON-CK-AP ( #I ) #TOTAL-ORG-CDE-CNT ( #I ) #TOTAL-ORG-CDE-AMT ( #I )
            pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap.getValue(pnd_I).setValue(0);
            pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap.getValue(pnd_I).setValue(0);
            pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap.getValue(pnd_I).setValue(0);
            pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt.getValue(pnd_I).setValue(0);
            pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap.getValue(pnd_I).setValue(0);
            pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I).setValue(0);
            pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I).setValue(0);
            pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I).setValue(0);
            pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I).setValue(0);
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Grand_Totals() throws Exception                                                                                                                //Natural: ACCUM-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Grand_Total_Pnd_Tot_Ck_Count.setValue(0);                                                                                                                     //Natural: MOVE 0 TO #TOT-CK-COUNT #TOT-CK-AMOUNT #TOT-REDRAW-CK-CNT #TOT-REDRAW-CK-AMOUNT #TOT-EFT-COUNT #TOT-EFT-AMOUNT #TOT-NON-CK-COUNT #TOT-NON-CK-AMOUNT #TOT-ORG-CDE-CNT-T #TOT-ORG-CDE-AMT-T
        pnd_Grand_Total_Pnd_Tot_Ck_Amount.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Cnt.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Amount.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Eft_Count.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Eft_Amount.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Non_Ck_Count.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Non_Ck_Amount.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T.setValue(0);
        pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T.setValue(0);
        FOR09:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            pnd_Grand_Total_Pnd_Tot_Ck_Count.nadd(pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt.getValue(pnd_I));                                                              //Natural: ADD #TOTAL-CHECK-AP-CNT ( #I ) TO #TOT-CK-COUNT
            pnd_Grand_Total_Pnd_Tot_Ck_Amount.nadd(pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap.getValue(pnd_I));                                                             //Natural: ADD #TOTAL-CHECK-AMT-AP ( #I ) TO #TOT-CK-AMOUNT
            pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Cnt.nadd(pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap.getValue(pnd_I));                                                       //Natural: ADD #PYMNT-CNT-REDRAWN-AP ( #I ) TO #TOT-REDRAW-CK-CNT
            pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Amount.nadd(pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap.getValue(pnd_I));                                                   //Natural: ADD #PYMNT-AMNT-REDRAWN-AP ( #I ) TO #TOT-REDRAW-CK-AMOUNT
            pnd_Grand_Total_Pnd_Tot_Eft_Count.nadd(pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt.getValue(pnd_I));                                                               //Natural: ADD #TOTAL-EFT-AP-CNT ( #I ) TO #TOT-EFT-COUNT
            pnd_Grand_Total_Pnd_Tot_Eft_Amount.nadd(pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap.getValue(pnd_I));                                                              //Natural: ADD #TOTAL-EFT-AMT-AP ( #I ) TO #TOT-EFT-AMOUNT
            pnd_Grand_Total_Pnd_Tot_Non_Ck_Count.nadd(pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I));                                                         //Natural: ADD #PYMNT-CNT-NON-CK-AP ( #I ) TO #TOT-NON-CK-COUNT
            pnd_Grand_Total_Pnd_Tot_Non_Ck_Amount.nadd(pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I));                                                       //Natural: ADD #PYMNT-AMNT-NON-CK-AP ( #I ) TO #TOT-NON-CK-AMOUNT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            pnd_Grand_Total_F_Pnd_Tot_Ck_Count_F.nadd(pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt.getValue(pnd_I));                                                          //Natural: ADD #TOTAL-CHECK-AP-CNT ( #I ) TO #TOT-CK-COUNT-F
            pnd_Grand_Total_F_Pnd_Tot_Ck_Amount_F.nadd(pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap.getValue(pnd_I));                                                         //Natural: ADD #TOTAL-CHECK-AMT-AP ( #I ) TO #TOT-CK-AMOUNT-F
            pnd_Grand_Total_F_Pnd_Tot_Redraw_Ck_Cnt_F.nadd(pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap.getValue(pnd_I));                                                   //Natural: ADD #PYMNT-CNT-REDRAWN-AP ( #I ) TO #TOT-REDRAW-CK-CNT-F
            pnd_Grand_Total_F_Pnd_Tot_Redraw_Ck_Amount_F.nadd(pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap.getValue(pnd_I));                                               //Natural: ADD #PYMNT-AMNT-REDRAWN-AP ( #I ) TO #TOT-REDRAW-CK-AMOUNT-F
            pnd_Grand_Total_F_Pnd_Tot_Eft_Count_F.nadd(pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt.getValue(pnd_I));                                                           //Natural: ADD #TOTAL-EFT-AP-CNT ( #I ) TO #TOT-EFT-COUNT-F
            pnd_Grand_Total_F_Pnd_Tot_Eft_Amount_F.nadd(pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap.getValue(pnd_I));                                                          //Natural: ADD #TOTAL-EFT-AMT-AP ( #I ) TO #TOT-EFT-AMOUNT-F
            pnd_Grand_Total_F_Pnd_Tot_Non_Ck_Count_F.nadd(pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I));                                                     //Natural: ADD #PYMNT-CNT-NON-CK-AP ( #I ) TO #TOT-NON-CK-COUNT-F
            pnd_Grand_Total_F_Pnd_Tot_Non_Ck_Amount_F.nadd(pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I));                                                   //Natural: ADD #PYMNT-AMNT-NON-CK-AP ( #I ) TO #TOT-NON-CK-AMOUNT-F
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR11:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
            pnd_Summary_Line_1_Pnd_Total_Check_Ap_Cnt_Sl.getValue(pnd_I).nadd(pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt.getValue(pnd_I));                                  //Natural: ADD #TOTAL-CHECK-AP-CNT ( #I ) TO #TOTAL-CHECK-AP-CNT-SL ( #I )
            pnd_Summary_Line_1_Pnd_Total_Check_Amt_Sl.getValue(pnd_I).nadd(pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap.getValue(pnd_I));                                     //Natural: ADD #TOTAL-CHECK-AMT-AP ( #I ) TO #TOTAL-CHECK-AMT-SL ( #I )
            pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Redrawn_Sl.getValue(pnd_I).nadd(pnd_Output_Line_1_Pnd_Pymnt_Cnt_Redrawn_Ap.getValue(pnd_I));                                 //Natural: ADD #PYMNT-CNT-REDRAWN-AP ( #I ) TO #PYMNT-CNT-REDRAWN-SL ( #I )
            pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Redrawn_Sl.getValue(pnd_I).nadd(pnd_Output_Line_1_Pnd_Pymnt_Amnt_Redrawn_Ap.getValue(pnd_I));                               //Natural: ADD #PYMNT-AMNT-REDRAWN-AP ( #I ) TO #PYMNT-AMNT-REDRAWN-SL ( #I )
            pnd_Summary_Line_1_Pnd_Total_Eft_Sl_Cnt.getValue(pnd_I).nadd(pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt.getValue(pnd_I));                                         //Natural: ADD #TOTAL-EFT-AP-CNT ( #I ) TO #TOTAL-EFT-SL-CNT ( #I )
            pnd_Summary_Line_1_Pnd_Total_Eft_Amt_Sl.getValue(pnd_I).nadd(pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap.getValue(pnd_I));                                         //Natural: ADD #TOTAL-EFT-AMT-AP ( #I ) TO #TOTAL-EFT-AMT-SL ( #I )
            pnd_Summary_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Sl.getValue(pnd_I).nadd(pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I));                                   //Natural: ADD #PYMNT-CNT-NON-CK-AP ( #I ) TO #PYMNT-CNT-NON-CK-SL ( #I )
            pnd_Summary_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Sl.getValue(pnd_I).nadd(pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I));                                 //Natural: ADD #PYMNT-AMNT-NON-CK-AP ( #I ) TO #PYMNT-AMNT-NON-CK-SL ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* * SUMMARY REPORT ORGIN CODE
        if (condition(pnd_Monthly_Or_Daily.equals("MONTHLY")))                                                                                                            //Natural: IF #MONTHLY-OR-DAILY = 'MONTHLY'
        {
            FOR12:                                                                                                                                                        //Natural: FOR #I 1 TO #X
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
            {
                pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I)),  //Natural: ADD #TOTAL-CHECK-AP-CNT ( #I ) #TOTAL-EFT-AP-CNT ( #I ) #PYMNT-CNT-NON-CK-AP ( #I ) TO #TOTAL-ORG-CDE-CNT ( #I )
                    pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Cnt.getValue(pnd_I).add(pnd_Output_Line_1_Pnd_Total_Check_Ap_Cnt.getValue(pnd_I)).add(pnd_Output_Line_1_Pnd_Total_Eft_Ap_Cnt.getValue(pnd_I)).add(pnd_Output_Line_1_Pnd_Pymnt_Cnt_Non_Ck_Ap.getValue(pnd_I)));
                pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I).compute(new ComputeParameters(false, pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I)),  //Natural: ADD #TOTAL-CHECK-AMT-AP ( #I ) #TOTAL-EFT-AMT-AP ( #I ) #PYMNT-AMNT-NON-CK-AP ( #I ) TO #TOTAL-ORG-CDE-AMT ( #I )
                    pnd_Summary_Line_Org_Cde_Pnd_Total_Org_Cde_Amt.getValue(pnd_I).add(pnd_Output_Line_1_Pnd_Total_Check_Amt_Ap.getValue(pnd_I)).add(pnd_Output_Line_1_Pnd_Total_Eft_Amt_Ap.getValue(pnd_I)).add(pnd_Output_Line_1_Pnd_Pymnt_Amnt_Non_Ck_Ap.getValue(pnd_I)));
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T.compute(new ComputeParameters(false, pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T), pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T.add(pnd_Grand_Total_Pnd_Tot_Ck_Count).add(pnd_Grand_Total_Pnd_Tot_Eft_Count).add(pnd_Grand_Total_Pnd_Tot_Non_Ck_Count)); //Natural: ADD #TOT-CK-COUNT #TOT-EFT-COUNT #TOT-NON-CK-COUNT TO #TOT-ORG-CDE-CNT-T
        pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T.compute(new ComputeParameters(false, pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T), pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T.add(pnd_Grand_Total_Pnd_Tot_Ck_Amount).add(pnd_Grand_Total_Pnd_Tot_Eft_Amount).add(pnd_Grand_Total_Pnd_Tot_Non_Ck_Amount)); //Natural: ADD #TOT-CK-AMOUNT #TOT-EFT-AMOUNT #TOT-NON-CK-AMOUNT TO #TOT-ORG-CDE-AMT-T
        //* *
        if (condition(pnd_Monthly_Or_Daily.equals("MONTHLY")))                                                                                                            //Natural: IF #MONTHLY-OR-DAILY = 'MONTHLY'
        {
            getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(3),pnd_Grand_Total_Pnd_Tot_Ck_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Ck_Amount,  //Natural: WRITE ( 1 ) 3X #TOT-CK-COUNT 1X #TOT-CK-AMOUNT 1X #TOT-EFT-COUNT 1X #TOT-EFT-AMOUNT 1X #TOT-NON-CK-COUNT 1X #TOT-NON-CK-AMOUNT 1X #TOT-ORG-CDE-CNT-T 1X #TOT-ORG-CDE-AMT-T 1X #TOT-REDRAW-CK-CNT 1X #TOT-REDRAW-CK-AMOUNT
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Eft_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Eft_Amount, 
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Non_Ck_Count, new ReportEditMask ("Z,ZZZ,ZZ9"),new 
                ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Non_Ck_Amount, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T, 
                new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T, new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"),new 
                ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Cnt, new ReportEditMask ("Z,ZZZ,ZZ9"),new ColumnSpacing(1),pnd_Grand_Total_Pnd_Tot_Redraw_Ck_Amount, 
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99-"));
            if (Global.isEscape()) return;
            getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                                   //Natural: WRITE ( 1 ) #SPACE-LINE
            if (Global.isEscape()) return;
            pnd_Grand_Total_Count.nadd(1);                                                                                                                                //Natural: ADD 1 TO #GRAND-TOTAL-COUNT
            pnd_Grand_Total_Pnd_Tot_Org_Cde_Cnt_T.setValue(0);                                                                                                            //Natural: MOVE 0 TO #TOT-ORG-CDE-CNT-T #TOT-ORG-CDE-AMT-T
            pnd_Grand_Total_Pnd_Tot_Org_Cde_Amt_T.setValue(0);
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Hold_Cde() throws Exception                                                                                                                    //Natural: WRITE-HOLD-CDE
    {
        if (BLNatReinput.isReinput()) return;

        FOR13:                                                                                                                                                            //Natural: FOR #I 1 TO #X
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_X)); pnd_I.nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM PAGE-BRAKE
            sub_Page_Brake();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            if (condition(getReports().getAstLinesLeft(2).less(pnd_P)))                                                                                                   //Natural: NEWPAGE ( 2 ) WHEN LESS THAN #P LINES LEFT
            {
                getReports().newPage(2);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }
            getReports().write(2, ReportOption.NOTITLE,"ORIGIN =   ",pnd_Hold_Cde_Tbl_Pnd_Origin_Cde.getValue(pnd_I),NEWLINE,NEWLINE,"  HOLD CODE",NEWLINE);              //Natural: WRITE ( 2 ) 'ORIGIN =   ' #ORIGIN-CDE ( #I ) // '  HOLD CODE' /
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-HOLD-COUNTERS
            sub_Write_Hold_Counters();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            getReports().write(2, ReportOption.NOTITLE,pnd_Space_Line);                                                                                                   //Natural: WRITE ( 2 ) #SPACE-LINE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, ReportOption.NOTITLE,pnd_Space_Line);                                                                                                   //Natural: WRITE ( 2 ) #SPACE-LINE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Hold_Counters() throws Exception                                                                                                               //Natural: WRITE-HOLD-COUNTERS
    {
        if (BLNatReinput.isReinput()) return;

        FOR14:                                                                                                                                                            //Natural: FOR #H 1 TO 30
        for (pnd_H.setValue(1); condition(pnd_H.lessOrEqual(30)); pnd_H.nadd(1))
        {
            if (condition(pnd_Hold_Cde_Tbl_Pnd_Hold_Cde.getValue(pnd_I,pnd_H).equals(" ")))                                                                               //Natural: IF #HOLD-CDE ( #I,#H ) = ' '
            {
                getReports().write(2, ReportOption.NOTITLE,pnd_Space_Line);                                                                                               //Natural: WRITE ( 2 ) #SPACE-LINE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(4),"TOTAL ",pnd_Hold_Total);                                                                 //Natural: WRITE ( 2 ) 4X 'TOTAL ' #HOLD-TOTAL
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,pnd_Space_Line);                                                                                               //Natural: WRITE ( 2 ) #SPACE-LINE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, ReportOption.NOTITLE,pnd_Space_Line);                                                                                               //Natural: WRITE ( 2 ) #SPACE-LINE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Hold_Total.setValue(0);                                                                                                                               //Natural: MOVE 0 TO #HOLD-TOTAL
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Hold_Total.nadd(pnd_Hold_Cde_Tbl_Pnd_Hold_Count.getValue(pnd_I,pnd_H));                                                                               //Natural: ADD #HOLD-COUNT ( #I,#H ) TO #HOLD-TOTAL
                getReports().write(2, ReportOption.NOTITLE,new ColumnSpacing(4),pnd_Hold_Cde_Tbl_Pnd_Hold_Cde.getValue(pnd_I,pnd_H),new ColumnSpacing(5),                 //Natural: WRITE ( 2 ) 4X #HOLD-CDE ( #I,#H ) 5X #HOLD-COUNT ( #I,#H )
                    pnd_Hold_Cde_Tbl_Pnd_Hold_Count.getValue(pnd_I,pnd_H));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Page_Brake() throws Exception                                                                                                                        //Natural: PAGE-BRAKE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_P.setValue(8);                                                                                                                                                //Natural: MOVE 8 TO #P
        FOR15:                                                                                                                                                            //Natural: FOR #H 1 TO 30
        for (pnd_H.setValue(1); condition(pnd_H.lessOrEqual(30)); pnd_H.nadd(1))
        {
            if (condition(pnd_Hold_Cde_Tbl_Pnd_Hold_Cde.getValue(pnd_I,pnd_H).equals(" ")))                                                                               //Natural: IF #HOLD-CDE ( #I,#H ) = ' '
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_P.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #P
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 1 ) #SPACE-LINE
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATX(), new ReportEditMask ("MM/DD/YY"),"-",Global.getTIMX(), new ReportEditMask                 //Natural: WRITE ( 1 ) NOTITLE *DATX ( EM = MM/DD/YY ) '-' *TIMX ( EM = HH':'II' 'AP ) 55T 'CONSOLIDATED PAYMENT SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *INIT-USER '-' *PROGRAM 55T #TITLE #START-DTE-1-20 /
                        ("HH':'II' 'AP"),new TabSetting(55),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(121),"PAGE",getReports().getPageNumberDbs(1), new 
                        NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(55),pnd_Title,pnd_Start_Dte_1_20,NEWLINE);
                    getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 1 ) #SPACE-LINE
                    getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 1 ) #SPACE-LINE
                    getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 1 ) #SPACE-LINE
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(12),"CHECKS", new FieldAttributes ("AD=U"),new ColumnSpacing(25),"EFTS",new              //Natural: WRITE ( 1 ) 12X 'CHECKS' ( U ) 25X 'EFTS' 14X 'NON-CHECK/EFTS' ( U ) 15X 'TOTALS ***' 10X 'REDRAWN CHECKS'
                        ColumnSpacing(14),"NON-CHECK/EFTS", new FieldAttributes ("AD=U"),new ColumnSpacing(15),"TOTALS ***",new ColumnSpacing(10),"REDRAWN CHECKS");
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(2),"__________________________",new ColumnSpacing(3),"________________________",new      //Natural: WRITE ( 1 ) 2X '__________________________' 3X '________________________' 1X '_______________________' 4X '_______________________' 1X '_______________________'
                        ColumnSpacing(1),"_______________________",new ColumnSpacing(4),"_______________________",new ColumnSpacing(1),"_______________________");
                    getReports().write(1, ReportOption.NOTITLE,new ColumnSpacing(5),"PAYMENT",new ColumnSpacing(9),"  GROSS",new ColumnSpacing(4),"PAYMENT",new           //Natural: WRITE ( 1 ) 5X 'PAYMENT' 9X '  GROSS' 4X 'PAYMENT' 9X '  GROSS' 1X 'PAYMENT' 9X '  GROSS' 4X 'PAYMENT' 9X '  GROSS' 1X 'PAYMENT' 10X ' GROSS' / 7X 'COUNT' 10X 'AMOUNT' 6X 'COUNT' 10X 'AMOUNT' 3X 'COUNT' 10X 'AMOUNT' 6X 'COUNT' 10X 'AMOUNT' 3X 'COUNT' 10X 'AMOUNT'
                        ColumnSpacing(9),"  GROSS",new ColumnSpacing(1),"PAYMENT",new ColumnSpacing(9),"  GROSS",new ColumnSpacing(4),"PAYMENT",new ColumnSpacing(9),"  GROSS",new 
                        ColumnSpacing(1),"PAYMENT",new ColumnSpacing(10)," GROSS",NEWLINE,new ColumnSpacing(7),"COUNT",new ColumnSpacing(10),"AMOUNT",new 
                        ColumnSpacing(6),"COUNT",new ColumnSpacing(10),"AMOUNT",new ColumnSpacing(3),"COUNT",new ColumnSpacing(10),"AMOUNT",new ColumnSpacing(6),"COUNT",new 
                        ColumnSpacing(10),"AMOUNT",new ColumnSpacing(3),"COUNT",new ColumnSpacing(10),"AMOUNT");
                    getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 1 ) #SPACE-LINE
                    getReports().write(1, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 1 ) #SPACE-LINE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 2 ) #SPACE-LINE
                    getReports().write(2, ReportOption.NOTITLE,Global.getDATX(), new ReportEditMask ("MM/DD/YY"),"-",Global.getTIMX(), new ReportEditMask                 //Natural: WRITE ( 2 ) NOTITLE *DATX ( EM = MM/DD/YY ) '-' *TIMX ( EM = HH':'II' 'AP ) 55T 'CONSOLIDATED PAYMENT SYSTEM' 121T 'PAGE' *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *INIT-USER '-' *PROGRAM 55T 'HOLD CODE STATISTICS FOR' #START-DTE-1-20 /
                        ("HH':'II' 'AP"),new TabSetting(55),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(121),"PAGE",getReports().getPageNumberDbs(1), new 
                        NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(55),"HOLD CODE STATISTICS FOR",pnd_Start_Dte_1_20,NEWLINE);
                    //*    55T #TITLE #START-DTE-1-20 /
                    getReports().write(2, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 2 ) #SPACE-LINE
                    getReports().write(2, ReportOption.NOTITLE,pnd_Space_Line);                                                                                           //Natural: WRITE ( 2 ) #SPACE-LINE
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "UC=*");
        Global.format(1, "PS=60 LS=132");
        Global.format(2, "PS=60 LS=132");
    }
}
