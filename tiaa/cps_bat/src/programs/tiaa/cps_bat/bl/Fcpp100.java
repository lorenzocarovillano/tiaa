/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:14:40 PM
**        * FROM NATURAL PROGRAM : Fcpp100
************************************************************
**        * FILE NAME            : Fcpp100.java
**        * CLASS NAME           : Fcpp100
**        * INSTANCE NAME        : Fcpp100
************************************************************
************************************************************************
** PROGRAM : FCPP100
** SYSTEM  : CONSOLIDATED PAYMENT SYSTEM
** AUTHOR  : COGNIZANT, MUKHERR
** FUNCTION: PRODUCE THE NEXT BUSINESS DATE BY FETCHING THE
**           EXTERNALISATION TABLE AND UPDATE THE CONTROL PARM
**           PROD.CNTL.PARMLIB(P1465CP1)
************************************************************************
** MODIFICATION LOG :
** 03/23/2015 MUKHERR - THE CONTROL PARM P1465CP1, WILL CONTAIN 8 BYTES
**                      OF DATE AND REST 72 BYTES SPACE INSTEAD OF NULL
**                      VALUES PASSED. TAG --> MUKHERR1
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp100 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_new_Ext_Cntrl_Cal_View;
    private DbsField new_Ext_Cntrl_Cal_View_Nec_Table_Cde;
    private DbsField new_Ext_Cntrl_Cal_View_Nec_For_Dte;
    private DbsField new_Ext_Cntrl_Cal_View_Nec_Business_Day_Ind;
    private DbsField new_Ext_Cntrl_Cal_View_Nec_Next_Business_Dte;
    private DbsField new_Ext_Cntrl_Cal_View_Nec_Curr_Business_Dte;
    private DbsField cal_Super_Key;

    private DbsGroup cal_Super_Key__R_Field_1;
    private DbsField cal_Super_Key_Cal_Key_Table_Cde;
    private DbsField cal_Super_Key_Cal_Key_For_Dte;
    private DbsField date_D;
    private DbsField date_X;

    private DbsGroup date_X__R_Field_2;
    private DbsField date_X_Date_N;
    private DbsField output_Date;

    private DbsGroup output_Date__R_Field_3;
    private DbsField output_Date_Next_Business_Dte;
    private DbsField output_Date_Filler;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_new_Ext_Cntrl_Cal_View = new DataAccessProgramView(new NameInfo("vw_new_Ext_Cntrl_Cal_View", "NEW-EXT-CNTRL-CAL-VIEW"), "NEW_EXT_CNTRL_CAL", 
            "NEW_EXT_CNTRL");
        new_Ext_Cntrl_Cal_View_Nec_Table_Cde = vw_new_Ext_Cntrl_Cal_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cal_View_Nec_Table_Cde", "NEC-TABLE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        new_Ext_Cntrl_Cal_View_Nec_For_Dte = vw_new_Ext_Cntrl_Cal_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cal_View_Nec_For_Dte", "NEC-FOR-DTE", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "NEC_FOR_DTE");
        new_Ext_Cntrl_Cal_View_Nec_Business_Day_Ind = vw_new_Ext_Cntrl_Cal_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cal_View_Nec_Business_Day_Ind", 
            "NEC-BUSINESS-DAY-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "NEC_BUSINESS_DAY_IND");
        new_Ext_Cntrl_Cal_View_Nec_Next_Business_Dte = vw_new_Ext_Cntrl_Cal_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cal_View_Nec_Next_Business_Dte", 
            "NEC-NEXT-BUSINESS-DTE", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "NEC_NEXT_BUSINESS_DTE");
        new_Ext_Cntrl_Cal_View_Nec_Curr_Business_Dte = vw_new_Ext_Cntrl_Cal_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Cal_View_Nec_Curr_Business_Dte", 
            "NEC-CURR-BUSINESS-DTE", FieldType.PACKED_DECIMAL, 8, RepeatingFieldStrategy.None, "NEC_CURR_BUSINESS_DTE");
        registerRecord(vw_new_Ext_Cntrl_Cal_View);

        cal_Super_Key = localVariables.newFieldInRecord("cal_Super_Key", "CAL-SUPER-KEY", FieldType.STRING, 11);

        cal_Super_Key__R_Field_1 = localVariables.newGroupInRecord("cal_Super_Key__R_Field_1", "REDEFINE", cal_Super_Key);
        cal_Super_Key_Cal_Key_Table_Cde = cal_Super_Key__R_Field_1.newFieldInGroup("cal_Super_Key_Cal_Key_Table_Cde", "CAL-KEY-TABLE-CDE", FieldType.STRING, 
            3);
        cal_Super_Key_Cal_Key_For_Dte = cal_Super_Key__R_Field_1.newFieldInGroup("cal_Super_Key_Cal_Key_For_Dte", "CAL-KEY-FOR-DTE", FieldType.NUMERIC, 
            8);
        date_D = localVariables.newFieldInRecord("date_D", "DATE-D", FieldType.DATE);
        date_X = localVariables.newFieldInRecord("date_X", "DATE-X", FieldType.STRING, 8);

        date_X__R_Field_2 = localVariables.newGroupInRecord("date_X__R_Field_2", "REDEFINE", date_X);
        date_X_Date_N = date_X__R_Field_2.newFieldInGroup("date_X_Date_N", "DATE-N", FieldType.NUMERIC, 8);
        output_Date = localVariables.newFieldInRecord("output_Date", "OUTPUT-DATE", FieldType.STRING, 80);

        output_Date__R_Field_3 = localVariables.newGroupInRecord("output_Date__R_Field_3", "REDEFINE", output_Date);
        output_Date_Next_Business_Dte = output_Date__R_Field_3.newFieldInGroup("output_Date_Next_Business_Dte", "NEXT-BUSINESS-DTE", FieldType.NUMERIC, 
            8);
        output_Date_Filler = output_Date__R_Field_3.newFieldInGroup("output_Date_Filler", "FILLER", FieldType.STRING, 72);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_new_Ext_Cntrl_Cal_View.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp100() throws Exception
    {
        super("Fcpp100");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
                                                                                                                                                                          //Natural: PERFORM READ-NEC-CAL-TBL
        sub_Read_Nec_Cal_Tbl();
        if (condition(Global.isEscape())) {return;}
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NEC-CAL-TBL
    }
    private void sub_Read_Nec_Cal_Tbl() throws Exception                                                                                                                  //Natural: READ-NEC-CAL-TBL
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        //*  FUNCTION SET TO CALENDER
        cal_Super_Key_Cal_Key_Table_Cde.setValue("CAL");                                                                                                                  //Natural: MOVE 'CAL' TO CAL-KEY-TABLE-CDE
        //*  CURRENT SYS DATE
        date_D.setValue(Global.getDATX());                                                                                                                                //Natural: MOVE *DATX TO DATE-D
        date_X.setValueEdited(date_D,new ReportEditMask("YYYYMMDD"));                                                                                                     //Natural: MOVE EDITED DATE-D ( EM = YYYYMMDD ) TO DATE-X
        //*  KEY
        cal_Super_Key_Cal_Key_For_Dte.setValue(date_X_Date_N);                                                                                                            //Natural: MOVE DATE-N TO CAL-KEY-FOR-DTE
        vw_new_Ext_Cntrl_Cal_View.startDatabaseRead                                                                                                                       //Natural: READ ( 1 ) NEW-EXT-CNTRL-CAL-VIEW WITH NEC-CAL-SUPER = CAL-SUPER-KEY
        (
        "READ01",
        new Wc[] { new Wc("NEC_CAL_SUPER", ">=", cal_Super_Key, WcType.BY) },
        new Oc[] { new Oc("NEC_CAL_SUPER", "ASC") },
        1
        );
        READ01:
        while (condition(vw_new_Ext_Cntrl_Cal_View.readNextRow("READ01")))
        {
            if (condition(new_Ext_Cntrl_Cal_View_Nec_Business_Day_Ind.equals("Y")))                                                                                       //Natural: IF NEC-BUSINESS-DAY-IND = 'Y'
            {
                //*  NEXT DATE
                output_Date_Next_Business_Dte.setValue(new_Ext_Cntrl_Cal_View_Nec_Next_Business_Dte);                                                                     //Natural: MOVE NEC-NEXT-BUSINESS-DTE TO NEXT-BUSINESS-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(new_Ext_Cntrl_Cal_View_Nec_Business_Day_Ind.equals("N")))                                                                                   //Natural: IF NEC-BUSINESS-DAY-IND = 'N'
                {
                    //* NEXT-CURR-DATE
                    output_Date_Next_Business_Dte.setValue(new_Ext_Cntrl_Cal_View_Nec_Curr_Business_Dte);                                                                 //Natural: MOVE NEC-CURR-BUSINESS-DTE TO NEXT-BUSINESS-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE WORK FILE 1 NEXT-BUSINESS-DTE /* MUKHERR1 <NULL VAL PASSED DEF>
            //*  MUKHERR1
            output_Date_Filler.setValue(" ");                                                                                                                             //Natural: MOVE ' ' TO FILLER
            //*  MUKHERR1
            getWorkFiles().write(1, false, output_Date);                                                                                                                  //Natural: WRITE WORK FILE 1 OUTPUT-DATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
