/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:13 PM
**        * FROM NATURAL PROGRAM : Fcpp377
************************************************************
**        * FILE NAME            : Fcpp377.java
**        * CLASS NAME           : Fcpp377
**        * INSTANCE NAME        : Fcpp377
************************************************************
**    THIS PROGRAM IS  NOT  CONSTRUCTABLE
**
**
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: ANNUITANT STATEMENT
**SAG SYSTEM: CPS
**SAG GDA: FCPG000
**SAG USER-DEFINED-LS(0): 133
**SAG USER-DEFINED-PS(0): 060
**SAG REPORT-HEADING(1): CONSOLIDATED PAYMENT SYSTEM
**SAG PRINT-FILE(1): 0
**SAG REPORT-HEADING(2): ANNUITANT STATEMENT
**SAG PRINT-FILE(2): 0
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM WILL BUILD ANNUITANT STATEMENT.
**SAG PRIMARY-FILE: FCP-CONS-PYMNT-SEQ
************************************************************************
* PROGRAM  : FCPP377 (FCPP220 NAME COMMENT CHANGED PIN EXPANSION)
* SYSTEM   : CPS
* TITLE    : ANNUITANT STATEMENT
* GENERATED: JUL 26,93 AT 10:04 AM
* FUNCTION : THIS PROGRAM WILL BUILD ANNUITANT STATEMENT.
*
* HISTORY
* 01/15/2006   R. LANDRUM
*              POS-PAY, PAYEE MATCH. ADD STOP POINT FOR CHECK NUMBER ON
*              STATEMENT BODY & CHECK FACE, 10 DIGIT MICR LINE.
*
*************************** NOTE !!! **********************************
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR THIS PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTETERNALLY.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp377 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private PdaFcpa110 pdaFcpa110;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Prime_Counter;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Run_Type;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_Ws_Rec_9;
    private DbsField pnd_Ws_First_Xerox;
    private DbsField pnd_Ws_First_Time_In;
    private DbsField pnd_Ws_First_Blank_Line;
    private DbsField pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_One;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Save_S_D_M_Plex;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_1;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_Ws_Work_Rec;

    private DbsGroup pnd_Ws_Work_Rec__R_Field_2;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Filler_A;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Filler_B;
    private DbsField pnd_Ws_Header;
    private DbsField pnd_Ws_Occurs;
    private DbsField pnd_Ws_Name_N_Address;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        localVariables = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(localVariables);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);

        // Local Variables
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Prime_Counter = localVariables.newFieldInRecord("pnd_Prime_Counter", "#PRIME-COUNTER", FieldType.PACKED_DECIMAL, 7);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Run_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 8);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_Ws_Rec_9 = localVariables.newFieldInRecord("pnd_Ws_Rec_9", "#WS-REC-9", FieldType.STRING, 143);
        pnd_Ws_First_Xerox = localVariables.newFieldInRecord("pnd_Ws_First_Xerox", "#WS-FIRST-XEROX", FieldType.STRING, 1);
        pnd_Ws_First_Time_In = localVariables.newFieldInRecord("pnd_Ws_First_Time_In", "#WS-FIRST-TIME-IN", FieldType.STRING, 1);
        pnd_Ws_First_Blank_Line = localVariables.newFieldInRecord("pnd_Ws_First_Blank_Line", "#WS-FIRST-BLANK-LINE", FieldType.STRING, 1);
        pnd_Ws_Save_Pymnt_Prcss_Seq_Num = localVariables.newFieldInRecord("pnd_Ws_Save_Pymnt_Prcss_Seq_Num", "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Ws_One = localVariables.newFieldInRecord("pnd_Ws_One", "#WS-ONE", FieldType.PACKED_DECIMAL, 1);
        pnd_Ws_Side_Printed = localVariables.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Cntr_Inv_Acct = localVariables.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Sim_Dup_Multiplex_Written = localVariables.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Save_S_D_M_Plex = localVariables.newFieldInRecord("pnd_Ws_Save_S_D_M_Plex", "#WS-SAVE-S-D-M-PLEX", FieldType.STRING, 1);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_1", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_Ws_Work_Rec = localVariables.newFieldArrayInRecord("pnd_Ws_Work_Rec", "#WS-WORK-REC", FieldType.STRING, 250, new DbsArrayController(1, 2));

        pnd_Ws_Work_Rec__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Work_Rec__R_Field_2", "REDEFINE", pnd_Ws_Work_Rec);
        pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num", "#WS-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code", "#WS-CONTRACT-HOLD-CODE", 
            FieldType.STRING, 4);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Work_Rec_Pnd_Ws_Filler_A = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Filler_A", "#WS-FILLER-A", FieldType.STRING, 
            217);
        pnd_Ws_Work_Rec_Pnd_Ws_Filler_B = pnd_Ws_Work_Rec__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Filler_B", "#WS-FILLER-B", FieldType.STRING, 
            242);
        pnd_Ws_Header = localVariables.newFieldArrayInRecord("pnd_Ws_Header", "#WS-HEADER", FieldType.STRING, 250, new DbsArrayController(1, 2));
        pnd_Ws_Occurs = localVariables.newFieldArrayInRecord("pnd_Ws_Occurs", "#WS-OCCURS", FieldType.STRING, 250, new DbsArrayController(1, 80));
        pnd_Ws_Name_N_Address = localVariables.newFieldArrayInRecord("pnd_Ws_Name_N_Address", "#WS-NAME-N-ADDRESS", FieldType.STRING, 250, new DbsArrayController(1, 
            4));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCdbatxa.initializeValues();
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();

        localVariables.reset();
        pnd_Ws_Rec_1.setInitialValue(" ");
        pnd_Ws_Rec_9.setInitialValue(" ");
        pnd_Ws_First_Xerox.setInitialValue("Y");
        pnd_Ws_First_Time_In.setInitialValue("Y");
        pnd_Ws_First_Blank_Line.setInitialValue("Y");
        pnd_Ws_One.setInitialValue(1);
        pnd_Ws_Cntr_Inv_Acct.setInitialValue(0);
        pnd_I.setInitialValue(0);
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp377() throws Exception
    {
        super("Fcpp377");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 80 ZP = ON;//Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 23T 'PROCESS IA CANCEL AND REDRAW CHECKS' 68T 'REPORT: RPT0' / 36T #RUN-TYPE
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        getWorkFiles().read(2, pnd_Ws_Pnd_Run_Type);                                                                                                                      //Natural: READ WORK FILE 2 ONCE #RUN-TYPE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'RUN TYPE' PARAMETER FILE",new TabSetting(77),"***");                                                 //Natural: WRITE '***' 25T 'MISSING "RUN TYPE" PARAMETER FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  RL
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().read(6, ldaFcpl876.getPnd_Fcpl876());                                                                                                              //Natural: READ WORK FILE 6 ONCE #FCPL876
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'NEXT CHECK' FILE",new TabSetting(77),"***");                                                         //Natural: WRITE '***' 25T 'MISSING "NEXT CHECK" FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-ENDFILE
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"at start of program:",NEWLINE,"The program is going to use the following:",NEWLINE,                   //Natural: WRITE *PROGRAM *TIME 'at start of program:' / 'The program is going to use the following:' / 'start assigned check   number:' #FCPL876.PYMNT-CHECK-NBR / 'start sequence number........:' #FCPL876.PYMNT-CHECK-SEQ-NBR
            "start assigned check   number:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr(),NEWLINE,"start sequence number........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());
        if (Global.isEscape()) return;
        pnd_Prime_Counter.reset();                                                                                                                                        //Natural: RESET #PRIME-COUNTER
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WS-WORK-REC ( * )
        while (condition(getWorkFiles().read(1, pnd_Ws_Work_Rec.getValue("*"))))
        {
            if (condition(pnd_Prime_Counter.equals(getZero())))                                                                                                           //Natural: IF #PRIME-COUNTER = 0
            {
                pnd_Prime_Counter.nadd(1);                                                                                                                                //Natural: ADD 1 TO #PRIME-COUNTER
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_First_Time_In.equals("Y")))                                                                                                              //Natural: IF #WS-FIRST-TIME-IN = 'Y'
            {
                pnd_Ws_First_Time_In.setValue("N");                                                                                                                       //Natural: ASSIGN #WS-FIRST-TIME-IN = 'N'
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                sub_Initialize_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Save_Pymnt_Prcss_Seq_Num.equals(pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num)))                                                            //Natural: IF #WS-SAVE-PYMNT-PRCSS-SEQ-NUM = #WS-PYMNT-PRCSS-SEQ-NUM
            {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-N-SEQ-NMBRS
                sub_Assign_Check_N_Seq_Nmbrs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
                sub_Move_Small_Rec_To_Large_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Save_Pymnt_Prcss_Seq_Num.notEquals(pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num)))                                                         //Natural: IF #WS-SAVE-PYMNT-PRCSS-SEQ-NUM NE #WS-PYMNT-PRCSS-SEQ-NUM
            {
                                                                                                                                                                          //Natural: PERFORM FORMAT-INTEGRATED-PUBLISHING-RECORDS
                sub_Format_Integrated_Publishing_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                sub_Initialize_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().nadd(1);                                                                                                      //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-NBR
                //*  RL ?
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                           //Natural: MOVE #FCPL876.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                //*  RL ?
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                //*  RL ?
                //*  RL ?
                pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                               //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
                ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nadd(1);                                                                                                  //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SEQ-NBR
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-N-SEQ-NMBRS
                sub_Assign_Check_N_Seq_Nmbrs();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
                sub_Move_Small_Rec_To_Large_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (condition(pnd_Prime_Counter.equals(getZero())))                                                                                                               //Natural: IF #PRIME-COUNTER = 0
        {
            getReports().write(0, "-",new RepeatItem(50));                                                                                                                //Natural: WRITE '-' ( 50 )
            if (Global.isEscape()) return;
            getReports().write(0, "NO CHECKS TO BE PRINTED");                                                                                                             //Natural: WRITE 'NO CHECKS TO BE PRINTED'
            if (Global.isEscape()) return;
            getReports().write(0, "NO CHECKS TO BE PRINTED");                                                                                                             //Natural: WRITE 'NO CHECKS TO BE PRINTED'
            if (Global.isEscape()) return;
            getReports().write(0, "-",new RepeatItem(50));                                                                                                                //Natural: WRITE '-' ( 50 )
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  PROCESS LAST-REC
                                                                                                                                                                          //Natural: PERFORM FORMAT-INTEGRATED-PUBLISHING-RECORDS
            sub_Format_Integrated_Publishing_Records();
            if (condition(Global.isEscape())) {return;}
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().nadd(1);                                                                                                          //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-NBR
            //*  RL ?
            pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                               //Natural: MOVE #FCPL876.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
            //*  RL ?
            pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                             //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
            //*  RL ?
            //*  RL ?
            pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                                   //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nadd(1);                                                                                                      //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SEQ-NBR
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(7, false, ldaFcpl876.getPnd_Fcpl876());                                                                                                      //Natural: WRITE WORK FILE 7 #FCPL876
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,Global.getPROGRAM(),Global.getTIME(),"Counters etc, at end of CANCEL/REDRAW IA program:",NEWLINE,                   //Natural: WRITE /// *PROGRAM *TIME 'Counters etc, at end of CANCEL/REDRAW IA program:' / 'Next check   number..........:' #FCPL876.PYMNT-CHECK-NBR / 'Next payment sequence number.:' #FCPL876.PYMNT-CHECK-SEQ-NBR /// '****** END OF PROGRAM EXECUTION ****'
            "Next check   number..........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr(),NEWLINE,"Next payment sequence number.:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr(),
            NEWLINE,NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION ****");
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-SMALL-REC-TO-LARGE-REC
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-INTEGRATED-PUBLISHING-RECORDS
        //* *COMPRESS '12' #WS-CHECK-NBR-A10 *PROGRAM ' 2570'
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-CHECK-N-SEQ-NMBRS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-XEROX-COMMANDS
        //* *      ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=PER,FORMS=PRO2AA,'
        //* *--> INSERTED 03-02-95 BY FRANK
        //*        'FEED=MAIN,END;'
        //* *      ' $$XEROX JDE=CHKDUP,JDL=CHECK,FORMAT=PER,FORMS=PRO2AA,'
        //* *--> INSERTED 03-02-95 BY FRANK
        //*        'FEED=MAIN,END;'
        //* *    ' $$XEROX FORMAT=PER,FORMS=PRO2AA,END;'
        //* *********************** RL BEGIN - PAYEE MATCH ************************
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //* ************************** RL END-PAYEE MATCH *************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Move_Small_Rec_To_Large_Rec() throws Exception                                                                                                       //Natural: MOVE-SMALL-REC-TO-LARGE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  HEADER REC
        short decideConditionsMet383 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-RECORD-LEVEL-NMBR;//Natural: VALUE 10
        if (condition((pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(10))))
        {
            decideConditionsMet383++;
            pnd_Ws_Header.getValue("*").setValue(pnd_Ws_Work_Rec.getValue("*"));                                                                                          //Natural: MOVE #WS-WORK-REC ( * ) TO #WS-HEADER ( * )
            if (condition(pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code.equals("0000")))                                                                                      //Natural: IF #WS-CONTRACT-HOLD-CODE = '0000'
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                               //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                                //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl876a.getPnd_Fcpl876a_Cntrct_Orgn_Cde().setValue(pnd_Ws_Work_Rec.getValue(1).getSubstring(66,2));                                                       //Natural: MOVE SUBSTR ( #WS-WORK-REC ( 1 ) ,66,2 ) TO #FCPL876A.CNTRCT-ORGN-CDE
            ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                  //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
            ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                          //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
            //*  OCCURS REC
            getWorkFiles().write(8, false, ldaFcpl876a.getPnd_Fcpl876a());                                                                                                //Natural: WRITE WORK FILE 8 #FCPL876A
        }                                                                                                                                                                 //Natural: VALUE 20
        else if (condition((pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(20))))
        {
            decideConditionsMet383++;
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I GT 0
            {
                pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_J.add(1));                                                                                         //Natural: COMPUTE #I = #J + 1
                pnd_J.nadd(2);                                                                                                                                            //Natural: COMPUTE #J = #J + 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_I.setValue(1);                                                                                                                                        //Natural: ASSIGN #I = 1
                pnd_J.setValue(2);                                                                                                                                        //Natural: ASSIGN #J = 2
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Occurs.getValue(pnd_I).setValue(pnd_Ws_Work_Rec.getValue(1));                                                                                          //Natural: MOVE #WS-WORK-REC ( 1 ) TO #WS-OCCURS ( #I )
            pnd_Ws_Occurs.getValue(pnd_J).setValue(pnd_Ws_Work_Rec.getValue(2));                                                                                          //Natural: MOVE #WS-WORK-REC ( 2 ) TO #WS-OCCURS ( #J )
            //*  NAME/ADDR REC
            pnd_Ws_Cntr_Inv_Acct.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WS-CNTR-INV-ACCT
        }                                                                                                                                                                 //Natural: VALUE 30
        else if (condition((pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(30))))
        {
            decideConditionsMet383++;
            if (condition(pnd_K.greater(getZero())))                                                                                                                      //Natural: IF #K GT 0
            {
                pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_L.add(1));                                                                                         //Natural: COMPUTE #K = #L + 1
                pnd_L.nadd(2);                                                                                                                                            //Natural: COMPUTE #L = #L + 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_K.setValue(1);                                                                                                                                        //Natural: ASSIGN #K = 1
                pnd_L.setValue(2);                                                                                                                                        //Natural: ASSIGN #L = 2
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Name_N_Address.getValue(pnd_K).setValue(pnd_Ws_Work_Rec.getValue(1));                                                                                  //Natural: MOVE #WS-WORK-REC ( 1 ) TO #WS-NAME-N-ADDRESS ( #K )
            pnd_Ws_Name_N_Address.getValue(pnd_L).setValue(pnd_Ws_Work_Rec.getValue(2));                                                                                  //Natural: MOVE #WS-WORK-REC ( 2 ) TO #WS-NAME-N-ADDRESS ( #L )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Format_Integrated_Publishing_Records() throws Exception                                                                                              //Natural: FORMAT-INTEGRATED-PUBLISHING-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
        sub_Format_Xerox_Commands();
        if (condition(Global.isEscape())) {return;}
        //* ******************* RL BEGIN POS-PAY/PAYEE MATCH **********************
        //*  RL ?
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                   //Natural: MOVE #FCPL876.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //*  RL ?
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //*  RL ?
        //*  RL ?
        pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                                       //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
        pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "12", pnd_Ws_Check_Nbr_A10));                                                               //Natural: COMPRESS '12' #WS-CHECK-NBR-A10 INTO #WS-REC-1 LEAVING NO SPACE
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        //* ***************** RL END POS-PAY/PAYEE MATCH *********************
        //*  FORMAT HEADER
        //*  STANDARD TEXT FOR STATEMENT
        DbsUtil.callnat(Fcpn235.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN235' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        pnd_Ws_Side_Printed.setValue("1");                                                                                                                                //Natural: ASSIGN #WS-SIDE-PRINTED = '1'
        //*  FORMAT BODY
        DbsUtil.callnat(Fcpn245.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN245' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-SIDE-PRINTED
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written, pnd_Ws_Side_Printed);
        if (condition(Global.isEscape())) return;
        //*  TRAILER
        //*  TIAA-CREFF REPROTS ALL TAXABLE PYMENTS TO GOV...
        DbsUtil.callnat(Fcpn280.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN280' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT
            pnd_Ws_Cntr_Inv_Acct);
        if (condition(Global.isEscape())) return;
        //*  FORMAT CHECK
        //*  RL CHECK&SEQ NBR & OTHER BANK SPECIFIC DATA FOR CHECK PRINT
        DbsUtil.callnat(Fcpn250.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN250' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) FCPA110 #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pdaFcpa110.getFcpa110(), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        //*  SIMPLEX
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("1")))                                                                                                                //Natural: IF #WS-SAVE-S-D-M-PLEX = '1'
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+3"));                                                                                 //Natural: COMPRESS '+3' INTO #WS-REC-1 LEAVING NO SPACE
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ASSIGN #WS-REC-1 = ' '
            //*  WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=PERBAC,FORMS=BLANK,END;");                                                                                             //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=PERBAC,FORMS=BLANK,END;'
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            //*  ASSIGN #WS-REC-1 = ' '
            //*  WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("2") || pnd_Ws_Save_S_D_M_Plex.equals("3")))                                                                          //Natural: IF #WS-SAVE-S-D-M-PLEX = '2' OR = '3'
        {
            pnd_Ws_Side_Printed.setValue("2");                                                                                                                            //Natural: ASSIGN #WS-SIDE-PRINTED = '2'
            DbsUtil.callnat(Fcpn245.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),      //Natural: CALLNAT 'FCPN245' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-SIDE-PRINTED
                pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written, pnd_Ws_Side_Printed);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* * MULTIPLEX TO BE TESTED
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("3")))                                                                                                                //Natural: IF #WS-SAVE-S-D-M-PLEX = '3'
        {
            pnd_Ws_Side_Printed.setValue("3");                                                                                                                            //Natural: ASSIGN #WS-SIDE-PRINTED = '3'
            if (condition(pnd_Ws_Side_Printed.equals("3")))                                                                                                               //Natural: IF #WS-SIDE-PRINTED = '3'
            {
                pnd_Ws_Rec_1.setValue(" ");                                                                                                                               //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=PERBAC,FORMS=BLANK,FEED=AUX,END;");                                                                                //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=PERBAC,FORMS=BLANK,FEED=AUX,END;'
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(" ");                                                                                                                               //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.callnat(Fcpn245.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),      //Natural: CALLNAT 'FCPN245' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-SIDE-PRINTED
                pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written, pnd_Ws_Side_Printed);
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #J #K #L #WS-CNTR-INV-ACCT
        pnd_J.reset();
        pnd_K.reset();
        pnd_L.reset();
        pnd_Ws_Cntr_Inv_Acct.reset();
        pnd_Ws_Header.getValue("*").reset();                                                                                                                              //Natural: RESET #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * )
        pnd_Ws_Occurs.getValue("*").reset();
        pnd_Ws_Name_N_Address.getValue("*").reset();
        pnd_Ws_Save_Pymnt_Prcss_Seq_Num.setValue(pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num);                                                                             //Natural: ASSIGN #WS-SAVE-PYMNT-PRCSS-SEQ-NUM = #WS-PYMNT-PRCSS-SEQ-NUM
        pnd_Ws_Save_S_D_M_Plex.setValue(pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex);                                                                                 //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX = #WS-SIMPLEX-DUPLEX-MULTIPLEX
    }
    private void sub_Assign_Check_N_Seq_Nmbrs() throws Exception                                                                                                          //Natural: ASSIGN-CHECK-N-SEQ-NMBRS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                     //Natural: ASSIGN #WS-PYMNT-CHECK-NBR = #FCPL876.PYMNT-CHECK-NBR
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                             //Natural: ASSIGN #WS-PYMNT-CHECK-SEQ-NBR = #FCPL876.PYMNT-CHECK-SEQ-NBR
        getWorkFiles().write(9, false, pnd_Ws_Work_Rec.getValue("*"));                                                                                                    //Natural: WRITE WORK FILE 9 #WS-WORK-REC ( * )
    }
    private void sub_Format_Xerox_Commands() throws Exception                                                                                                             //Natural: FORMAT-XEROX-COMMANDS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Ws_Sim_Dup_Multiplex_Written.notEquals(pnd_Ws_Save_S_D_M_Plex)))                                                                                //Natural: IF #WS-SIM-DUP-MULTIPLEX-WRITTEN NE #WS-SAVE-S-D-M-PLEX
        {
            pnd_Ws_Sim_Dup_Multiplex_Written.setValue(pnd_Ws_Save_S_D_M_Plex);                                                                                            //Natural: ASSIGN #WS-SIM-DUP-MULTIPLEX-WRITTEN = #WS-SAVE-S-D-M-PLEX
            short decideConditionsMet505 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WS-SAVE-S-D-M-PLEX;//Natural: VALUE '1'
            if (condition((pnd_Ws_Save_S_D_M_Plex.equals("1"))))
            {
                decideConditionsMet505++;
                //* RL
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=PERX,FORMS=MCMWIC,", "FEED=CHECKP,END;"));    //Natural: COMPRESS ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=PERX,FORMS=MCMWIC,' 'FEED=CHECKP,END;' INTO #WS-REC-1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Ws_Save_S_D_M_Plex.equals("2"))))
            {
                decideConditionsMet505++;
                //* RL
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=PERX,FORMS=MCMWIC,", "FEED=CHECKP,END;"));    //Natural: COMPRESS ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=PERX,FORMS=MCMWIC,' 'FEED=CHECKP,END;' INTO #WS-REC-1 LEAVING NO SPACE
            }                                                                                                                                                             //Natural: ANY VALUE
            if (condition(decideConditionsMet505 > 0))
            {
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(pnd_Ws_First_Blank_Line.equals("Y")))                                                                                                           //Natural: IF #WS-FIRST-BLANK-LINE = 'Y'
            {
                pnd_Ws_First_Blank_Line.setValue("N");                                                                                                                    //Natural: ASSIGN #WS-FIRST-BLANK-LINE := 'N'
                pnd_Ws_Rec_1.reset();                                                                                                                                     //Natural: RESET #WS-REC-1
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ws_Save_S_D_M_Plex.equals("2")))                                                                                                            //Natural: IF #WS-SAVE-S-D-M-PLEX = '2'
            {
                //*  RL
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " $$XEROX FORMAT=PERX,FORMS=MCMWIC,END;"));                                         //Natural: COMPRESS ' $$XEROX FORMAT=PERX,FORMS=MCMWIC,END;' INTO #WS-REC-1 LEAVING NO SPACE
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET CHECK DATA FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        //*  RL #BARCODE-LDA.#BAR-NEW-RUN           := TRUE
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* *SET                                        #BAR-ENV-ID-NUM  /* RL
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=80 ZP=ON");

        getReports().write(0, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(23),"PROCESS IA CANCEL AND REDRAW CHECKS",new TabSetting(68),"REPORT: RPT0",NEWLINE,new 
            TabSetting(36),pnd_Ws_Pnd_Run_Type);
    }
}
