/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:45 PM
**        * FROM NATURAL PROGRAM : Fcpp194
************************************************************
**        * FILE NAME            : Fcpp194.java
**        * CLASS NAME           : Fcpp194
**        * INSTANCE NAME        : Fcpp194
************************************************************
** NON REGENERATABLE
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: DAILY SETTLEMENT REPORT
**SAG SYSTEM: CPS
**SAG GDA: FCPG190
**SAG LOGICAL-NAMES(1): PRT1
**SAG REPORT-HEADING(1): CONSOLIDATED PAYMENT SYSTEM
**SAG PRINT-FILE(1): 1
**SAG REPORT-HEADING(2): DAILY SETTLEMENT REPORT
**SAG PRINT-FILE(2): 1
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM WILL GENERATE A DAILY SETTLEMENT REPORT
**SAG DA-NAME: FCPL190
**SAG MODEL-1: X
**SAG USER-FIELD-DEFINITION(1): #INPUT-PARM               A080TF
************************************************************************
* PROGRAM  : FCPP194
* SYSTEM   : CPS
* TITLE    : RA CASH REPORT
* REVISED  : AUG 24,94
* FUNCTION : THIS PROGRAM WILL GENERATE AN END-OF-DAY REGISTER FOR
*            ON-LINE RA CASH, OR DS CANCEL / REDRAW PAYMENTS OR
*            A REGISTER FOR ON-LINE SINGLE SUM PAYMENTS BASED UPON
*            INPUT PARAMETER.
*
* HISTORY
*
* 01/07/2000 : LEON GURTOVNIK
*              RESTOW TO ACCEPT CHNG IN FCPL236 (ADDED 25TH OCCURANCE)
*
* 05/05/99 : A. YOUNG     - REVISED TO PRINT TOTALS FOR DIRECT TRANSFERS
*
* 12/10/98 : A. YOUNG     - CORRECTED MDO TOTAL SEQUENCE.
*
* 05/18/98 : A. YOUNG     - REVISED TO PROCESS IRA (CLASSIC, ROTH).
*                         - INTERNAL ROLLOVER REGISTER PRINTED
*                           SEPERATELY.
*
* 07/18/96 : L. ZHENG     - INCLUDED MDO IN THE REPORTS
*
* 06/05/96 : A. YOUNG     - REVISED TO UTILIZE FUND CODE TABLE FCPL199A.
*
* 06/05/96 : A. YOUNG     - INCREASED ALL ACCUMULATOR FIELDS.
*                           DECIMALS ---> (P15.2)
*                           COUNTERS ---> (P9)
*
* 08/14/95 : A. YOUNG     - BACKED OUT PREVIOUS LOB REVISIONS; MODIFIED
*                           LOB CLASSIFICATIONS AS PER USER (ACCOUNTING)
*                           REQUEST.
*                         - CHANGED 'RA' AND 'RAD' LOB CODES TO 'XRA'
*                           AND 'XRAD' TO ENSURE CORRECT SORTING.
* 02/13/95 : A. YOUNG     - ADDED INTERFACE DATE FOR LEDGER REPORT
*                           RECONCILIATION ONLY.
* 08/24/94 : A. YOUNG     - ADAPTED FROM FCPP296.
*                         - COMMENTED OUT INTERNAL SORT; EXTERNAL SORT
*                           PERFORMED.
* 05/26/2006 : LANDRUM PAYEE MATCH. N7 CHECK NBR CAN BE N7, NEG OR POS.
*            - INCREASED CHECK NBR FROM N7 TO N10. FORMAT FOR REPORTS
* 4/2017     : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp194 extends BLNatBase
{
    // Data Areas
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl190a ldaFcpl190a;
    private LdaCdbatxa ldaCdbatxa;
    private PdaTbldcoda pdaTbldcoda;
    private PdaFcppda_M pdaFcppda_M;
    private LdaFcpl236 ldaFcpl236;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_N10__R_Field_1;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N7;

    private DbsGroup pnd_Check_Number_N10__R_Field_2;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_A10;
    private DbsField pnd_Check_Datex;

    private DbsGroup pnd_Check_Datex__R_Field_3;
    private DbsField pnd_Check_Datex_Pnd_Check_Date;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Input_Parm;
    private DbsField pnd_Input_Orgn;
    private DbsField pnd_Input_Ind;
    private DbsField pnd_Header1_1;
    private DbsField pnd_Header1_2;
    private DbsField pnd_Header1_2_Tmp;
    private DbsField pnd_Header1_3;
    private DbsField pnd_Header1_W;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K1;
    private DbsField pnd_K2;
    private DbsField pnd_Z;
    private DbsField pnd_Mode_Index;
    private DbsField pnd_Misc_Tot_Index;
    private DbsField pnd_Array_Size;
    private DbsField pnd_Len;
    private DbsField pnd_End_Report;
    private DbsField pnd_Summary;
    private DbsField pnd_State_Cde;
    private DbsField pnd_Pymnt_Typ_Cde;
    private DbsField pnd_Annt;
    private DbsField pnd_Payee;
    private DbsField pnd_Pymnt_Typ_Cde_Txt;
    private DbsField pnd_Pymnt_Check_Num;
    private DbsField pnd_Null_Dte;
    private DbsField pnd_Pymnt_Check_Dte;
    private DbsField pnd_Pymnt_Acctg_Dte;
    private DbsField pnd_Pymnt_Intrfce_Dte;
    private DbsField pnd_Pay_Type_Req_Ind;
    private DbsField pnd_Phrase_Num;
    private DbsField pnd_Old_Lob_Cde;
    private DbsField pnd_Detail_Date;
    private DbsField pnd_Date_Type;
    private DbsField pnd_W_Fld;
    private DbsField pnd_Category_Txt;
    private DbsField pnd_Cntrct_Type_Cde;
    private DbsField pnd_Sum_I_Max;
    private DbsField pnd_Sum_I;
    private DbsField pnd_X;
    private DbsField pnd_Header_Centered;
    private DbsField pnd_First;
    private DbsField pnd_Found;
    private DbsField pnd_Pymnt_Check_Mo;
    private DbsField pnd_Pymnt_Check_Yr;

    private DbsGroup pnd_T_Vars;
    private DbsField pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt;
    private DbsField pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt;
    private DbsField pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt;
    private DbsField pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_T_Vars_Pnd_T_Qty;

    private DbsGroup pnd_D_Vars;
    private DbsField pnd_D_Vars_Pnd_D_Inv_Acct_Settl_Amt;
    private DbsField pnd_D_Vars_Pnd_D_Inv_Acct_Exp_Amt;
    private DbsField pnd_D_Vars_Pnd_D_Inv_Acct_Dpi_Amt;
    private DbsField pnd_D_Vars_Pnd_D_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_D_Vars_Pnd_D_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_D_Vars_Pnd_D_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_D_Vars_Pnd_D_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_D_Vars_Pnd_D_Pymnt_Check_Amt;

    private DbsGroup pnd_Cntrct_Type_Sum;
    private DbsField pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt;
    private DbsField pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt;
    private DbsField pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Cntrct_Type_Sum_Pnd_Qty;

    private DbsGroup pnd_Gt_Cntrct_Type_Sum;
    private DbsField pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt;
    private DbsField pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt;
    private DbsField pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty;

    private DbsGroup pnd_Misc_Grand_Totals;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Max_Occurs;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Category_Hdr;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Category_Txt;

    private DbsGroup pnd_Misc_Grand_Totals_Pnd_Misc_Gt;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Settl_Amt;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Exp_Amt;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Misc_Grand_Totals_Pnd_Misc_Qty;
    private DbsField pnd_Printed_1;
    private DbsField pnd_Printed_2;
    private DbsField pnd_Mdo;
    private DbsField pnd_Direct_Trans;

    private DbsGroup pnd_Break_Flags;
    private DbsField pnd_Break_Flags_Pnd_Date_Break;
    private DbsField pnd_Break_Flags_Pnd_Lob_Break;
    private DbsField pnd_Sub_Total;
    private DbsField pnd_Sub_Prt;
    private DbsField pnd_Sub_Det_Prt;
    private DbsField pnd_Pay_Type_Req_Break;
    private DbsField pnd_Int_Roll_Mode;
    private DbsField pnd_Eod;
    private DbsField pnd_I_Start;
    private DbsField pnd_I_End;
    private DbsField pnd_J_Start;
    private DbsField pnd_J_End;
    private DbsField pnd_Lob_Cde;
    private DbsField pnd_Lob_Sub;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pymnt_Intrfce_DteOld;
    private DbsField readWork01Pymnt_Check_DteOld;
    private DbsField readWork01Cntrct_Type_CdeOld;
    private DbsField readWork01Cntrct_Lob_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        pdaTbldcoda = new PdaTbldcoda(localVariables);
        pdaFcppda_M = new PdaFcppda_M(localVariables);
        ldaFcpl236 = new LdaFcpl236();
        registerRecord(ldaFcpl236);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Check_Number_N10 = localVariables.newFieldInRecord("pnd_Check_Number_N10", "#CHECK-NUMBER-N10", FieldType.NUMERIC, 10);

        pnd_Check_Number_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_1", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_N3 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_N10_Pnd_Check_Number_N7 = pnd_Check_Number_N10__R_Field_1.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        pnd_Check_Number_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_2", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_A10 = pnd_Check_Number_N10__R_Field_2.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_A10", "#CHECK-NUMBER-A10", 
            FieldType.STRING, 10);
        pnd_Check_Datex = localVariables.newFieldInRecord("pnd_Check_Datex", "#CHECK-DATEX", FieldType.STRING, 8);

        pnd_Check_Datex__R_Field_3 = localVariables.newGroupInRecord("pnd_Check_Datex__R_Field_3", "REDEFINE", pnd_Check_Datex);
        pnd_Check_Datex_Pnd_Check_Date = pnd_Check_Datex__R_Field_3.newFieldInGroup("pnd_Check_Datex_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 6);
        pnd_Input_Orgn = localVariables.newFieldInRecord("pnd_Input_Orgn", "#INPUT-ORGN", FieldType.STRING, 2);
        pnd_Input_Ind = localVariables.newFieldInRecord("pnd_Input_Ind", "#INPUT-IND", FieldType.STRING, 1);
        pnd_Header1_1 = localVariables.newFieldInRecord("pnd_Header1_1", "#HEADER1-1", FieldType.STRING, 50);
        pnd_Header1_2 = localVariables.newFieldInRecord("pnd_Header1_2", "#HEADER1-2", FieldType.STRING, 69);
        pnd_Header1_2_Tmp = localVariables.newFieldInRecord("pnd_Header1_2_Tmp", "#HEADER1-2-TMP", FieldType.STRING, 41);
        pnd_Header1_3 = localVariables.newFieldInRecord("pnd_Header1_3", "#HEADER1-3", FieldType.STRING, 50);
        pnd_Header1_W = localVariables.newFieldInRecord("pnd_Header1_W", "#HEADER1-W", FieldType.STRING, 56);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K1 = localVariables.newFieldInRecord("pnd_K1", "#K1", FieldType.INTEGER, 2);
        pnd_K2 = localVariables.newFieldInRecord("pnd_K2", "#K2", FieldType.INTEGER, 2);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.INTEGER, 2);
        pnd_Mode_Index = localVariables.newFieldInRecord("pnd_Mode_Index", "#MODE-INDEX", FieldType.INTEGER, 2);
        pnd_Misc_Tot_Index = localVariables.newFieldInRecord("pnd_Misc_Tot_Index", "#MISC-TOT-INDEX", FieldType.INTEGER, 2);
        pnd_Array_Size = localVariables.newFieldInRecord("pnd_Array_Size", "#ARRAY-SIZE", FieldType.INTEGER, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.INTEGER, 2);
        pnd_End_Report = localVariables.newFieldInRecord("pnd_End_Report", "#END-REPORT", FieldType.BOOLEAN, 1);
        pnd_Summary = localVariables.newFieldInRecord("pnd_Summary", "#SUMMARY", FieldType.BOOLEAN, 1);
        pnd_State_Cde = localVariables.newFieldInRecord("pnd_State_Cde", "#STATE-CDE", FieldType.STRING, 2);
        pnd_Pymnt_Typ_Cde = localVariables.newFieldInRecord("pnd_Pymnt_Typ_Cde", "#PYMNT-TYP-CDE", FieldType.STRING, 1);
        pnd_Annt = localVariables.newFieldInRecord("pnd_Annt", "#ANNT", FieldType.STRING, 48);
        pnd_Payee = localVariables.newFieldInRecord("pnd_Payee", "#PAYEE", FieldType.STRING, 38);
        pnd_Pymnt_Typ_Cde_Txt = localVariables.newFieldInRecord("pnd_Pymnt_Typ_Cde_Txt", "#PYMNT-TYP-CDE-TXT", FieldType.STRING, 11);
        pnd_Pymnt_Check_Num = localVariables.newFieldInRecord("pnd_Pymnt_Check_Num", "#PYMNT-CHECK-NUM", FieldType.STRING, 10);
        pnd_Null_Dte = localVariables.newFieldInRecord("pnd_Null_Dte", "#NULL-DTE", FieldType.DATE);
        pnd_Pymnt_Check_Dte = localVariables.newFieldInRecord("pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Pymnt_Acctg_Dte = localVariables.newFieldInRecord("pnd_Pymnt_Acctg_Dte", "#PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Pymnt_Intrfce_Dte = localVariables.newFieldInRecord("pnd_Pymnt_Intrfce_Dte", "#PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Pay_Type_Req_Ind = localVariables.newFieldInRecord("pnd_Pay_Type_Req_Ind", "#PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Phrase_Num = localVariables.newFieldInRecord("pnd_Phrase_Num", "#PHRASE-NUM", FieldType.NUMERIC, 1);
        pnd_Old_Lob_Cde = localVariables.newFieldInRecord("pnd_Old_Lob_Cde", "#OLD-LOB-CDE", FieldType.STRING, 4);
        pnd_Detail_Date = localVariables.newFieldInRecord("pnd_Detail_Date", "#DETAIL-DATE", FieldType.DATE);
        pnd_Date_Type = localVariables.newFieldInRecord("pnd_Date_Type", "#DATE-TYPE", FieldType.STRING, 13);
        pnd_W_Fld = localVariables.newFieldInRecord("pnd_W_Fld", "#W-FLD", FieldType.STRING, 50);
        pnd_Category_Txt = localVariables.newFieldInRecord("pnd_Category_Txt", "#CATEGORY-TXT", FieldType.STRING, 14);
        pnd_Cntrct_Type_Cde = localVariables.newFieldArrayInRecord("pnd_Cntrct_Type_Cde", "#CNTRCT-TYPE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            6));
        pnd_Sum_I_Max = localVariables.newFieldInRecord("pnd_Sum_I_Max", "#SUM-I-MAX", FieldType.NUMERIC, 3);
        pnd_Sum_I = localVariables.newFieldInRecord("pnd_Sum_I", "#SUM-I", FieldType.NUMERIC, 3);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Header_Centered = localVariables.newFieldInRecord("pnd_Header_Centered", "#HEADER-CENTERED", FieldType.BOOLEAN, 1);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Check_Mo = localVariables.newFieldInRecord("pnd_Pymnt_Check_Mo", "#PYMNT-CHECK-MO", FieldType.STRING, 9);
        pnd_Pymnt_Check_Yr = localVariables.newFieldInRecord("pnd_Pymnt_Check_Yr", "#PYMNT-CHECK-YR", FieldType.STRING, 4);

        pnd_T_Vars = localVariables.newGroupInRecord("pnd_T_Vars", "#T-VARS");
        pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt = pnd_T_Vars.newFieldInGroup("pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt", "#T-INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt = pnd_T_Vars.newFieldInGroup("pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt", "#T-INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt = pnd_T_Vars.newFieldInGroup("pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt", "#T-INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt = pnd_T_Vars.newFieldInGroup("pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt", "#T-INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt = pnd_T_Vars.newFieldInGroup("pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt", "#T-INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt = pnd_T_Vars.newFieldInGroup("pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt", "#T-INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt = pnd_T_Vars.newFieldInGroup("pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt", "#T-INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_T_Vars_Pnd_T_Qty = pnd_T_Vars.newFieldInGroup("pnd_T_Vars_Pnd_T_Qty", "#T-QTY", FieldType.PACKED_DECIMAL, 9);

        pnd_D_Vars = localVariables.newGroupInRecord("pnd_D_Vars", "#D-VARS");
        pnd_D_Vars_Pnd_D_Inv_Acct_Settl_Amt = pnd_D_Vars.newFieldInGroup("pnd_D_Vars_Pnd_D_Inv_Acct_Settl_Amt", "#D-INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_D_Vars_Pnd_D_Inv_Acct_Exp_Amt = pnd_D_Vars.newFieldInGroup("pnd_D_Vars_Pnd_D_Inv_Acct_Exp_Amt", "#D-INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_D_Vars_Pnd_D_Inv_Acct_Dpi_Amt = pnd_D_Vars.newFieldInGroup("pnd_D_Vars_Pnd_D_Inv_Acct_Dpi_Amt", "#D-INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_D_Vars_Pnd_D_Inv_Acct_Fdrl_Tax_Amt = pnd_D_Vars.newFieldInGroup("pnd_D_Vars_Pnd_D_Inv_Acct_Fdrl_Tax_Amt", "#D-INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_D_Vars_Pnd_D_Inv_Acct_State_Tax_Amt = pnd_D_Vars.newFieldInGroup("pnd_D_Vars_Pnd_D_Inv_Acct_State_Tax_Amt", "#D-INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_D_Vars_Pnd_D_Inv_Acct_Local_Tax_Amt = pnd_D_Vars.newFieldInGroup("pnd_D_Vars_Pnd_D_Inv_Acct_Local_Tax_Amt", "#D-INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_D_Vars_Pnd_D_Inv_Acct_Net_Pymnt_Amt = pnd_D_Vars.newFieldInGroup("pnd_D_Vars_Pnd_D_Inv_Acct_Net_Pymnt_Amt", "#D-INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_D_Vars_Pnd_D_Pymnt_Check_Amt = pnd_D_Vars.newFieldInGroup("pnd_D_Vars_Pnd_D_Pymnt_Check_Amt", "#D-PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);

        pnd_Cntrct_Type_Sum = localVariables.newGroupArrayInRecord("pnd_Cntrct_Type_Sum", "#CNTRCT-TYPE-SUM", new DbsArrayController(1, 5, 1, 6));
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt = pnd_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt", "#S-INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 4));
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt = pnd_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt", "#S-INV-ACCT-EXP-AMT", 
            FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 4));
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt = pnd_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt", "#S-INV-ACCT-DPI-AMT", 
            FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 4));
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt = pnd_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt", 
            "#S-INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 4));
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt = pnd_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt", 
            "#S-INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 4));
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt = pnd_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt", 
            "#S-INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 4));
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt = pnd_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt", 
            "#S-INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 4));
        pnd_Cntrct_Type_Sum_Pnd_Qty = pnd_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Cntrct_Type_Sum_Pnd_Qty", "#QTY", FieldType.PACKED_DECIMAL, 9, new 
            DbsArrayController(1, 4));

        pnd_Gt_Cntrct_Type_Sum = localVariables.newGroupArrayInRecord("pnd_Gt_Cntrct_Type_Sum", "#GT-CNTRCT-TYPE-SUM", new DbsArrayController(1, 5, 1, 
            6));
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt = pnd_Gt_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt", 
            "#GT-INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 4));
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt = pnd_Gt_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt", 
            "#GT-INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 4));
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt = pnd_Gt_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt", 
            "#GT-INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 4));
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt = pnd_Gt_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt", 
            "#GT-INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 4));
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt = pnd_Gt_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt", 
            "#GT-INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 4));
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt = pnd_Gt_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt", 
            "#GT-INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 4));
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt = pnd_Gt_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt", 
            "#GT-INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 4));
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty = pnd_Gt_Cntrct_Type_Sum.newFieldArrayInGroup("pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty", "#GT-QTY", FieldType.PACKED_DECIMAL, 
            9, new DbsArrayController(1, 4));

        pnd_Misc_Grand_Totals = localVariables.newGroupInRecord("pnd_Misc_Grand_Totals", "#MISC-GRAND-TOTALS");
        pnd_Misc_Grand_Totals_Pnd_Misc_Max_Occurs = pnd_Misc_Grand_Totals.newFieldInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Max_Occurs", "#MISC-MAX-OCCURS", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Misc_Grand_Totals_Pnd_Misc_Category_Hdr = pnd_Misc_Grand_Totals.newFieldInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Category_Hdr", "#MISC-CATEGORY-HDR", 
            FieldType.STRING, 25);
        pnd_Misc_Grand_Totals_Pnd_Misc_Category_Txt = pnd_Misc_Grand_Totals.newFieldInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Category_Txt", "#MISC-CATEGORY-TXT", 
            FieldType.STRING, 14);

        pnd_Misc_Grand_Totals_Pnd_Misc_Gt = pnd_Misc_Grand_Totals.newGroupArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Gt", "#MISC-GT", new DbsArrayController(1, 
            2));
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Settl_Amt = pnd_Misc_Grand_Totals_Pnd_Misc_Gt.newFieldArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Settl_Amt", 
            "#MISC-INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 1));
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Exp_Amt = pnd_Misc_Grand_Totals_Pnd_Misc_Gt.newFieldArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Exp_Amt", 
            "#MISC-INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 1));
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Dpi_Amt = pnd_Misc_Grand_Totals_Pnd_Misc_Gt.newFieldArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Dpi_Amt", 
            "#MISC-INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 1));
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Fdrl_Tax_Amt = pnd_Misc_Grand_Totals_Pnd_Misc_Gt.newFieldArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Fdrl_Tax_Amt", 
            "#MISC-INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 1));
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_State_Tax_Amt = pnd_Misc_Grand_Totals_Pnd_Misc_Gt.newFieldArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_State_Tax_Amt", 
            "#MISC-INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 1));
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Local_Tax_Amt = pnd_Misc_Grand_Totals_Pnd_Misc_Gt.newFieldArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Local_Tax_Amt", 
            "#MISC-INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 8, 2, new DbsArrayController(1, 1));
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Net_Pymnt_Amt = pnd_Misc_Grand_Totals_Pnd_Misc_Gt.newFieldArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Net_Pymnt_Amt", 
            "#MISC-INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 17, 2, new DbsArrayController(1, 1));
        pnd_Misc_Grand_Totals_Pnd_Misc_Qty = pnd_Misc_Grand_Totals_Pnd_Misc_Gt.newFieldArrayInGroup("pnd_Misc_Grand_Totals_Pnd_Misc_Qty", "#MISC-QTY", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 1));
        pnd_Printed_1 = localVariables.newFieldInRecord("pnd_Printed_1", "#PRINTED-1", FieldType.BOOLEAN, 1);
        pnd_Printed_2 = localVariables.newFieldInRecord("pnd_Printed_2", "#PRINTED-2", FieldType.BOOLEAN, 1);
        pnd_Mdo = localVariables.newFieldInRecord("pnd_Mdo", "#MDO", FieldType.BOOLEAN, 1);
        pnd_Direct_Trans = localVariables.newFieldInRecord("pnd_Direct_Trans", "#DIRECT-TRANS", FieldType.BOOLEAN, 1);

        pnd_Break_Flags = localVariables.newGroupInRecord("pnd_Break_Flags", "#BREAK-FLAGS");
        pnd_Break_Flags_Pnd_Date_Break = pnd_Break_Flags.newFieldInGroup("pnd_Break_Flags_Pnd_Date_Break", "#DATE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Break_Flags_Pnd_Lob_Break = pnd_Break_Flags.newFieldInGroup("pnd_Break_Flags_Pnd_Lob_Break", "#LOB-BREAK", FieldType.BOOLEAN, 1);
        pnd_Sub_Total = localVariables.newFieldInRecord("pnd_Sub_Total", "#SUB-TOTAL", FieldType.BOOLEAN, 1);
        pnd_Sub_Prt = localVariables.newFieldInRecord("pnd_Sub_Prt", "#SUB-PRT", FieldType.BOOLEAN, 1);
        pnd_Sub_Det_Prt = localVariables.newFieldInRecord("pnd_Sub_Det_Prt", "#SUB-DET-PRT", FieldType.BOOLEAN, 1);
        pnd_Pay_Type_Req_Break = localVariables.newFieldInRecord("pnd_Pay_Type_Req_Break", "#PAY-TYPE-REQ-BREAK", FieldType.BOOLEAN, 1);
        pnd_Int_Roll_Mode = localVariables.newFieldInRecord("pnd_Int_Roll_Mode", "#INT-ROLL-MODE", FieldType.BOOLEAN, 1);
        pnd_Eod = localVariables.newFieldInRecord("pnd_Eod", "#EOD", FieldType.BOOLEAN, 1);
        pnd_I_Start = localVariables.newFieldInRecord("pnd_I_Start", "#I-START", FieldType.NUMERIC, 2);
        pnd_I_End = localVariables.newFieldInRecord("pnd_I_End", "#I-END", FieldType.NUMERIC, 2);
        pnd_J_Start = localVariables.newFieldInRecord("pnd_J_Start", "#J-START", FieldType.NUMERIC, 2);
        pnd_J_End = localVariables.newFieldInRecord("pnd_J_End", "#J-END", FieldType.NUMERIC, 2);
        pnd_Lob_Cde = localVariables.newFieldInRecord("pnd_Lob_Cde", "#LOB-CDE", FieldType.STRING, 4);
        pnd_Lob_Sub = localVariables.newFieldInRecord("pnd_Lob_Sub", "#LOB-SUB", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pymnt_Intrfce_DteOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pymnt_Intrfce_Dte_OLD", "Pymnt_Intrfce_Dte_OLD", FieldType.DATE);
        readWork01Pymnt_Check_DteOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pymnt_Check_Dte_OLD", "Pymnt_Check_Dte_OLD", FieldType.DATE);
        readWork01Cntrct_Type_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cntrct_Type_Cde_OLD", "Cntrct_Type_Cde_OLD", FieldType.STRING, 
            2);
        readWork01Cntrct_Lob_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cntrct_Lob_Cde_OLD", "Cntrct_Lob_Cde_OLD", FieldType.STRING, 4);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl190a.initializeValues();
        ldaCdbatxa.initializeValues();
        ldaFcpl236.initializeValues();

        localVariables.reset();
        pnd_Input_Orgn.setInitialValue(" ");
        pnd_Input_Ind.setInitialValue(" ");
        pnd_Header1_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header1_3.setInitialValue(" ");
        pnd_Z.setInitialValue(1);
        pnd_Mode_Index.setInitialValue(1);
        pnd_Misc_Tot_Index.setInitialValue(1);
        pnd_Summary.setInitialValue(false);
        pnd_Date_Type.setInitialValue(" ");
        pnd_Sum_I_Max.setInitialValue(1);
        pnd_Sum_I.setInitialValue(1);
        pnd_X.setInitialValue(1);
        pnd_First.setInitialValue(true);
        pnd_Found.setInitialValue(false);
        pnd_Pymnt_Check_Mo.setInitialValue(" ");
        pnd_Pymnt_Check_Yr.setInitialValue(" ");
        pnd_Misc_Grand_Totals_Pnd_Misc_Max_Occurs.setInitialValue(1);
        pnd_Misc_Grand_Totals_Pnd_Misc_Category_Hdr.setInitialValue(" ");
        pnd_Misc_Grand_Totals_Pnd_Misc_Category_Txt.setInitialValue(" ");
        pnd_Printed_1.setInitialValue(false);
        pnd_Printed_2.setInitialValue(false);
        pnd_Mdo.setInitialValue(false);
        pnd_Direct_Trans.setInitialValue(false);
        pnd_I_Start.setInitialValue(1);
        pnd_I_End.setInitialValue(1);
        pnd_J_Start.setInitialValue(1);
        pnd_J_End.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp194() throws Exception
    {
        super("Fcpp194");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp194|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                getReports().definePrinter(2, "PRT1");                                                                                                                    //Natural: DEFINE PRINTER ( PRT1 = 1 )
                getReports().definePrinter(3, "PRT2");                                                                                                                    //Natural: DEFINE PRINTER ( PRT2 = 2 )
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: FORMAT ( PRT1 ) LS = 134 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( PRT2 ) LS = 134 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: ASSIGN #PROGRAM = *PROGRAM
                //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
                pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                              //Natural: ASSIGN #CUR-LANG = *LANGUAGE
                pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                       //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA = 'INFP9000'
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=MITL'_'"), new SignPosition (true), new InputPrompting(false),                //Natural: INPUT ( AD = MITL'_' SG = ON IP = OFF ZP = OFF ) 'INPUT PARM:' #INPUT-PARM
                    new ReportZeroPrint (false),"INPUT PARM:",pnd_Input_Parm);
                //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
                sub_Get_Check_Formatting_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                short decideConditionsMet639 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE *INIT-USER;//Natural: VALUE 'P1420CPD'
                if (condition((Global.getINIT_USER().equals("P1420CPD"))))
                {
                    decideConditionsMet639++;
                    pnd_Header1_2_Tmp.setValue("CYCLE PAYMENT REGISTER");                                                                                                 //Natural: ASSIGN #HEADER1-2-TMP := 'CYCLE PAYMENT REGISTER'
                }                                                                                                                                                         //Natural: VALUE 'P1440CPD'
                else if (condition((Global.getINIT_USER().equals("P1440CPD"))))
                {
                    decideConditionsMet639++;
                    pnd_Header1_2_Tmp.setValue("LOGICAL END-OF-DAY PAYMENT REGISTER");                                                                                    //Natural: ASSIGN #HEADER1-2-TMP := 'LOGICAL END-OF-DAY PAYMENT REGISTER'
                }                                                                                                                                                         //Natural: VALUE 'P1460CPD'
                else if (condition((Global.getINIT_USER().equals("P1460CPD"))))
                {
                    decideConditionsMet639++;
                    pnd_Header1_2_Tmp.setValue("PHYSICAL END-OF-DAY PAYMENT REGISTER");                                                                                   //Natural: ASSIGN #HEADER1-2-TMP := 'PHYSICAL END-OF-DAY PAYMENT REGISTER'
                }                                                                                                                                                         //Natural: VALUE 'P1480CPM'
                else if (condition((Global.getINIT_USER().equals("P1480CPM"))))
                {
                    decideConditionsMet639++;
                    pnd_Header1_2_Tmp.setValue("MONTH-END PAYMENT REGISTER");                                                                                             //Natural: ASSIGN #HEADER1-2-TMP := 'MONTH-END PAYMENT REGISTER'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Header1_2_Tmp.setValue("REGISTER");                                                                                                               //Natural: ASSIGN #HEADER1-2-TMP := 'REGISTER'
                }                                                                                                                                                         //Natural: END-DECIDE
                pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(3);                                                                                                       //Natural: ASSIGN TBLDCODA.#TABLE-ID = 3
                pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("V");                                                                                                         //Natural: ASSIGN TBLDCODA.#MODE = 'V'
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( PRT1 )
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: AT TOP OF PAGE ( PRT2 );//Natural: READ WORK 4 #RPT-EXT
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(4, ldaFcpl190a.getPnd_Rpt_Ext())))
                {
                    CheckAtStartofData679();

                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    pnd_Found.setValue(true);                                                                                                                             //Natural: ASSIGN #FOUND := TRUE
                    //*                                                                                                                                                   //Natural: AT START OF DATA
                    //*                                                                                                                                                   //Natural: AT END OF DATA
                    //*                                                                                                                                                   //Natural: AT BREAK OF #RPT-EXT.PYMNT-INTRFCE-DTE;//Natural: AT BREAK OF #RPT-EXT.PYMNT-CHECK-DTE;//Natural: AT BREAK OF #RPT-EXT.CNTRCT-LOB-CDE;//Natural: AT BREAK OF #RPT-EXT.CNTRCT-TYPE-CDE
                    if (condition(pnd_Break_Flags_Pnd_Date_Break.getBoolean() || pnd_Break_Flags_Pnd_Lob_Break.getBoolean()))                                             //Natural: IF #DATE-BREAK OR #LOB-BREAK
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-TOTAL
                        sub_Print_Sub_Total();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Break_Flags.reset();                                                                                                                          //Natural: RESET #BREAK-FLAGS
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8)) && (pnd_Pay_Type_Req_Ind.notEquals(8))))                                //Natural: IF ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 8 ) AND ( #PAY-TYPE-REQ-IND NE 8 )
                    {
                        pnd_Pay_Type_Req_Break.setValue(true);                                                                                                            //Natural: ASSIGN #PAY-TYPE-REQ-BREAK := TRUE
                        pnd_Pay_Type_Req_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind());                                                               //Natural: ASSIGN #PAY-TYPE-REQ-IND := #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND
                        if (condition(! (pnd_Sub_Prt.getBoolean())))                                                                                                      //Natural: IF NOT #SUB-PRT
                        {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-TOTAL
                            sub_Print_Sub_Total();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Sub_Prt.setValue(false);                                                                                                                  //Natural: ASSIGN #SUB-PRT := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(! (pnd_Sub_Det_Prt.getBoolean())))                                                                                                  //Natural: IF NOT #SUB-DET-PRT
                        {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-TOTAL-DETAIL
                            sub_Print_Sub_Total_Detail();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Sub_Det_Prt.setValue(false);                                                                                                              //Natural: ASSIGN #SUB-DET-PRT := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Summary.setValue(true);                                                                                                                       //Natural: ASSIGN #SUMMARY := #END-REPORT := TRUE
                        pnd_End_Report.setValue(true);
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-DETAIL
                        sub_Print_Summary_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_First.setValue(true);                                                                                                                         //Natural: ASSIGN #FIRST := #INT-ROLL-MODE := TRUE
                        pnd_Int_Roll_Mode.setValue(true);
                        pnd_Summary.setValue(false);                                                                                                                      //Natural: ASSIGN #SUMMARY := #END-REPORT := #PAY-TYPE-REQ-BREAK := #MDO := FALSE
                        pnd_End_Report.setValue(false);
                        pnd_Pay_Type_Req_Break.setValue(false);
                        pnd_Mdo.setValue(false);
                        pnd_Sum_I.setValue(1);                                                                                                                            //Natural: ASSIGN #SUM-I := #SUM-I-MAX := 1
                        pnd_Sum_I_Max.setValue(1);
                        pnd_Cntrct_Type_Sum.getValue("*","*").reset();                                                                                                    //Natural: RESET #CNTRCT-TYPE-SUM ( *,* ) #GT-CNTRCT-TYPE-SUM ( *,* ) #CNTRCT-TYPE-CDE ( * )
                        pnd_Gt_Cntrct_Type_Sum.getValue("*","*").reset();
                        pnd_Cntrct_Type_Cde.getValue("*").reset();
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_First.getBoolean()))                                                                                                                //Natural: IF #FIRST
                    {
                        if (condition((pnd_Input_Parm.equals("RAMNTH")) && (pnd_Pymnt_Check_Mo.equals(" "))))                                                             //Natural: IF ( #INPUT-PARM = 'RAMNTH' ) AND ( #PYMNT-CHECK-MO = ' ' )
                        {
                            pnd_Pymnt_Check_Mo.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("LLLLLLLLL"));                              //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = LLLLLLLLL ) TO #PYMNT-CHECK-MO
                            pnd_Pymnt_Check_Yr.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYY"));                                   //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYY ) TO #PYMNT-CHECK-YR
                            pnd_Header1_2_Tmp.setValue(DbsUtil.compress(pnd_Header1_2_Tmp, pnd_Pymnt_Check_Mo, pnd_Pymnt_Check_Yr));                                      //Natural: COMPRESS #HEADER1-2-TMP #PYMNT-CHECK-MO #PYMNT-CHECK-YR INTO #HEADER1-2-TMP
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HEADER1-2
                        sub_Determine_Header1_2();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(pnd_Int_Roll_Mode.getBoolean()))                                                                                                    //Natural: IF #INT-ROLL-MODE
                        {
                            getReports().newPage(new ReportSpecification(0));                                                                                             //Natural: NEWPAGE ( PRT2 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde());                                                   //Natural: ASSIGN #CNTRCT-TYPE-CDE ( #SUM-I ) := #RPT-EXT.CNTRCT-TYPE-CDE
                        pnd_Old_Lob_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde());                                                                            //Natural: ASSIGN #OLD-LOB-CDE := #RPT-EXT.CNTRCT-LOB-CDE
                        pnd_Pymnt_Check_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                                       //Natural: ASSIGN #PYMNT-CHECK-DTE := #RPT-EXT.PYMNT-CHECK-DTE
                        pnd_Pymnt_Intrfce_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte());                                                                   //Natural: ASSIGN #PYMNT-INTRFCE-DTE := #RPT-EXT.PYMNT-INTRFCE-DTE
                        pnd_Pay_Type_Req_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind());                                                               //Natural: ASSIGN #PAY-TYPE-REQ-IND := #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND
                        pnd_First.setValue(false);                                                                                                                        //Natural: ASSIGN #FIRST := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31"))               //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = '30' OR = '31' AND NOT #MDO
                        && ! (pnd_Mdo.getBoolean()))))
                    {
                        pnd_Mdo.setValue(true);                                                                                                                           //Natural: ASSIGN #MDO := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM TEXT-AND-COUNTER
                    sub_Text_And_Counter();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Array_Size.setValue(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct());                                                                                     //Natural: ASSIGN #ARRAY-SIZE := #RPT-EXT.C-INV-ACCT
                    FOR01:                                                                                                                                                //Natural: FOR #I 1 TO #ARRAY-SIZE
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Array_Size)); pnd_I.nadd(1))
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
                        sub_Print_Detail();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    readWork01Pymnt_Intrfce_DteOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte());                                                              //Natural: END-WORK
                    readWork01Pymnt_Check_DteOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());
                    readWork01Cntrct_Type_CdeOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde());
                    readWork01Cntrct_Lob_CdeOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde());
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                    pnd_Eod.setValue(true);                                                                                                                               //Natural: ASSIGN #EOD := TRUE
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                pnd_Summary.setValue(true);                                                                                                                               //Natural: ASSIGN #SUMMARY := #END-REPORT := TRUE
                pnd_End_Report.setValue(true);
                //*  SS / MDO PAYMENTS
                short decideConditionsMet807 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PRINTED-1 AND NOT #PRINTED-2
                if (condition(pnd_Printed_1.getBoolean() && ! (pnd_Printed_2.getBoolean())))
                {
                    decideConditionsMet807++;
                    pnd_Header1_2.setValue(DbsUtil.compress("SINGLE SUM / MDO", pnd_Header1_2_Tmp));                                                                      //Natural: COMPRESS 'SINGLE SUM / MDO' #HEADER1-2-TMP INTO #HEADER1-2
                    //*  ONLY SS PAYMENTS
                    if (condition((((pnd_Cntrct_Type_Cde.getValue("*").equals("C") || pnd_Cntrct_Type_Cde.getValue("*").equals("R")) || pnd_Cntrct_Type_Cde.getValue("*").equals("L"))  //Natural: IF #CNTRCT-TYPE-CDE ( * ) = 'C' OR = 'R' OR = 'L' AND NOT ( #CNTRCT-TYPE-CDE ( * ) = '30' OR = '31' )
                        && ! ((pnd_Cntrct_Type_Cde.getValue("*").equals("30") || pnd_Cntrct_Type_Cde.getValue("*").equals("31"))))))
                    {
                        pnd_Header1_2.setValue(DbsUtil.compress("SINGLE SUM", pnd_Header1_2_Tmp));                                                                        //Natural: COMPRESS 'SINGLE SUM' #HEADER1-2-TMP INTO #HEADER1-2
                    }                                                                                                                                                     //Natural: END-IF
                    //*  ONLY MDO PRINTED
                    if (condition((! (((pnd_Cntrct_Type_Cde.getValue("*").equals("C") || pnd_Cntrct_Type_Cde.getValue("*").equals("R")) || pnd_Cntrct_Type_Cde.getValue("*").equals("L")))  //Natural: IF NOT ( #CNTRCT-TYPE-CDE ( * ) = 'C' OR = 'R' OR = 'L' ) AND #CNTRCT-TYPE-CDE ( * ) = '30' OR = '31'
                        && (pnd_Cntrct_Type_Cde.getValue("*").equals("30") || pnd_Cntrct_Type_Cde.getValue("*").equals("31")))))
                    {
                        pnd_Header1_2.setValue(DbsUtil.compress("MINIMUM DISTRIBUTION OPTION", pnd_Header1_2_Tmp));                                                       //Natural: COMPRESS 'MINIMUM DISTRIBUTION OPTION' #HEADER1-2-TMP INTO #HEADER1-2
                        //*  INTERNAL ROLLOVERS
                        //*  PYMNTS AND INT. ROLL.
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN ( #PRINTED-2 AND NOT #PRINTED-1 ) OR ( #PRINTED-1 AND #PRINTED-2 )
                else if (condition(((pnd_Printed_2.getBoolean() && ! (pnd_Printed_1.getBoolean())) || (pnd_Printed_1.getBoolean() && pnd_Printed_2.getBoolean()))))
                {
                    decideConditionsMet807++;
                    pnd_Header1_2.setValue(DbsUtil.compress("INTERNAL ROLLOVER", pnd_Header1_2_Tmp));                                                                     //Natural: COMPRESS 'INTERNAL ROLLOVER' #HEADER1-2-TMP INTO #HEADER1-2
                }                                                                                                                                                         //Natural: WHEN ANY
                if (condition(decideConditionsMet807 > 0))
                {
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER1-2
                    sub_Center_Header1_2();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-DETAIL
                    sub_Print_Summary_Detail();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                short decideConditionsMet829 = 0;                                                                                                                         //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN NOT #PRINTED-1
                if (condition(! ((pnd_Printed_1.getBoolean()))))
                {
                    decideConditionsMet829++;
                    pnd_Int_Roll_Mode.setValue(false);                                                                                                                    //Natural: ASSIGN #INT-ROLL-MODE := FALSE
                    pnd_Header1_2.setValue(DbsUtil.compress("SINGLE SUM / MDO", pnd_Header1_2_Tmp));                                                                      //Natural: COMPRESS 'SINGLE SUM / MDO' #HEADER1-2-TMP INTO #HEADER1-2
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER1-2
                    sub_Center_Header1_2();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().skip(0, 10);                                                                                                                             //Natural: SKIP ( PRT1 ) 10
                    getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(37),"************* NO SINGLE SUM / MDO RECORDS PROCESSED *************");        //Natural: WRITE ( PRT1 ) / / / / 37T '************* NO SINGLE SUM / MDO RECORDS PROCESSED *************'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN NOT #PRINTED-2
                if (condition(! ((pnd_Printed_2.getBoolean()))))
                {
                    decideConditionsMet829++;
                    pnd_Header1_2.setValue(DbsUtil.compress("INTERNAL ROLLOVER", pnd_Header1_2_Tmp));                                                                     //Natural: COMPRESS 'INTERNAL ROLLOVER' #HEADER1-2-TMP INTO #HEADER1-2
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER1-2
                    sub_Center_Header1_2();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().skip(0, 10);                                                                                                                             //Natural: SKIP ( PRT2 ) 10
                    getReports().write(3, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(37),"********** NO SINGLE SUM INTERNAL ROLLOVERS PROCESSED **********");         //Natural: WRITE ( PRT2 ) / / / / 37T '********** NO SINGLE SUM INTERNAL ROLLOVERS PROCESSED **********'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: WHEN NONE
                if (condition(decideConditionsMet829 == 0))
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY-HEADER
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-HEADER
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL
                //* *          061T #PYMNT-CHECK-NUM(EM=9999999)
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUB-TOTAL
                //* ***********************************************************************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TEXT-AND-COUNTER
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CATEGORY-TXT
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-CATEGORY-TXT
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPRESS-CATEGORY-TXT
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-SUMMARY-DETAIL
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-MISC-GT-DETAILS
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUB-TOTAL-DETAIL
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY-DETAIL
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-AND-PRINT-SS-TOTALS
                //*  CALCULATE AND PRINT GRAND TOTAL FOR SINGLE SUM
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-MISCELLANEOUS-TOTALS
                //* *******************************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-HEADER1-2
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CENTER-HEADER1-2
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-HEADER1-W
                //* ********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-LOB-DESC
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INDEX-RANGE
                //* *********************** RL BEGIN - PAYEE MATCH ************MAR 06,2006
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
                //*  RL PAYEE MATCH
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //*  RL PAYEE MATCH
                //* ************************** RL END-PAYEE MATCH *************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Print_Summary_Header() throws Exception                                                                                                              //Natural: PRINT-SUMMARY-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().skip(0, 1);                                                                                                                                      //Natural: SKIP ( PRT1 ) 1
            getReports().write(2, new TabSetting(24),"SETTLEMENT",new TabSetting(48),"ADDTNL",new TabSetting(55),"*************** TAX DEDUCTIONS ****************",NEWLINE,new  //Natural: WRITE ( PRT1 ) 024T 'SETTLEMENT' 048T 'ADDTNL' 055T '*************** TAX DEDUCTIONS ****************' / 026T 'VALUE' 037T 'EXP CHG' 049T 'INT' 055T 'FEDERAL                  STATE            LOCAL' 111T 'NET AMOUNT' 123T 'QUANTITY'
                TabSetting(26),"VALUE",new TabSetting(37),"EXP CHG",new TabSetting(49),"INT",new TabSetting(55),"FEDERAL                  STATE            LOCAL",new 
                TabSetting(111),"NET AMOUNT",new TabSetting(123),"QUANTITY");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().skip(0, 1);                                                                                                                                      //Natural: SKIP ( PRT2 ) 1
            getReports().write(3, new TabSetting(24),"SETTLEMENT",new TabSetting(48),"ADDTNL",new TabSetting(55),"*************** TAX DEDUCTIONS ****************",NEWLINE,new  //Natural: WRITE ( PRT2 ) 024T 'SETTLEMENT' 048T 'ADDTNL' 055T '*************** TAX DEDUCTIONS ****************' / 026T 'VALUE' 037T 'EXP CHG' 049T 'INT' 055T 'FEDERAL                  STATE            LOCAL' 111T 'NET AMOUNT' 123T 'QUANTITY'
                TabSetting(26),"VALUE",new TabSetting(37),"EXP CHG",new TabSetting(49),"INT",new TabSetting(55),"FEDERAL                  STATE            LOCAL",new 
                TabSetting(111),"NET AMOUNT",new TabSetting(123),"QUANTITY");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Detail_Header() throws Exception                                                                                                               //Natural: PRINT-DETAIL-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  08-24-94 : A. YOUNG - PAYMENT DATE ADDED TO HEADER TO ACCOMODATE
        //*                        MULTIPLE CHECK DATE PROCESSING.
        //*  02-13-95 : A. YOUNG - INTERFACE DATE ADDED FOR LEDGER REPORT ONLY.
        if (condition(pnd_Input_Parm.equals("RAMNTH")))                                                                                                                   //Natural: IF #INPUT-PARM = 'RAMNTH'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Input_Parm.equals("RACASH")))                                                                                                               //Natural: IF #INPUT-PARM = 'RACASH'
            {
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    getReports().write(2, "PAYMENT DATE:",pnd_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YY"),NEWLINE,NEWLINE);                                          //Natural: WRITE ( PRT1 ) 'PAYMENT DATE:' #PYMNT-CHECK-DTE ( EM = MM/DD/YY ) //
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(3, "PAYMENT DATE:",pnd_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YY"),NEWLINE,NEWLINE);                                          //Natural: WRITE ( PRT2 ) 'PAYMENT DATE:' #PYMNT-CHECK-DTE ( EM = MM/DD/YY ) //
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    getReports().write(2, "INTERFACE DATE:",pnd_Pymnt_Intrfce_Dte, new ReportEditMask ("MM/DD/YY"),NEWLINE,NEWLINE);                                      //Natural: WRITE ( PRT1 ) 'INTERFACE DATE:' #PYMNT-INTRFCE-DTE ( EM = MM/DD/YY ) //
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(3, "INTERFACE DATE:",pnd_Pymnt_Intrfce_Dte, new ReportEditMask ("MM/DD/YY"),NEWLINE,NEWLINE);                                      //Natural: WRITE ( PRT2 ) 'INTERFACE DATE:' #PYMNT-INTRFCE-DTE ( EM = MM/DD/YY ) //
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().skip(0, 1);                                                                                                                                      //Natural: SKIP ( PRT1 ) 1
            getReports().write(2, new TabSetting(111),"U.S. CURRENCY CONTRACT",NEWLINE,new TabSetting(20),"SETTLEMENT/",new TabSetting(120),"PAYMENT",new                 //Natural: WRITE ( PRT1 ) 111T 'U.S. CURRENCY CONTRACT' / 020T 'SETTLEMENT/' 120T 'PAYMENT' 132T 'T' / 003T 'PPCN/' 011T 'CERT.' 020T 'REPURCHASE' 033T '   ' 050T 'ADDITIONAL' 068T '************* TAX DEDUCTIONS **************' 121T 'AMOUNT' 132T 'Y' / 003T 'PRODUCT' 011T 'NUMBER' 022T 'VALUE' 032T '    ' 038T 'EXP CHG' 051T 'INTEREST' 065T 'RC' 068T '*  FEDERAL       STATE            LOCAL   *' 113T 'NET AMOUNT' 132T 'P' /
                TabSetting(132),"T",NEWLINE,new TabSetting(3),"PPCN/",new TabSetting(11),"CERT.",new TabSetting(20),"REPURCHASE",new TabSetting(33),"   ",new 
                TabSetting(50),"ADDITIONAL",new TabSetting(68),"************* TAX DEDUCTIONS **************",new TabSetting(121),"AMOUNT",new TabSetting(132),"Y",NEWLINE,new 
                TabSetting(3),"PRODUCT",new TabSetting(11),"NUMBER",new TabSetting(22),"VALUE",new TabSetting(32),"    ",new TabSetting(38),"EXP CHG",new 
                TabSetting(51),"INTEREST",new TabSetting(65),"RC",new TabSetting(68),"*  FEDERAL       STATE            LOCAL   *",new TabSetting(113),"NET AMOUNT",new 
                TabSetting(132),"P",NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().skip(0, 1);                                                                                                                                      //Natural: SKIP ( PRT2 ) 1
            getReports().write(3, new TabSetting(111),"U.S. CURRENCY CONTRACT",NEWLINE,new TabSetting(20),"SETTLEMENT/",new TabSetting(120),"PAYMENT",new                 //Natural: WRITE ( PRT2 ) 111T 'U.S. CURRENCY CONTRACT' / 020T 'SETTLEMENT/' 120T 'PAYMENT' 132T 'T' / 003T 'PPCN/' 011T 'CERT.' 020T 'REPURCHASE' 033T '   ' 050T 'ADDITIONAL' 068T '************* TAX DEDUCTIONS **************' 121T 'AMOUNT' 132T 'Y' / 003T 'PRODUCT' 011T 'NUMBER' 022T 'VALUE' 032T '    ' 038T 'EXP CHG' 051T 'INTEREST' 065T 'RC' 068T '*  FEDERAL       STATE            LOCAL   *' 113T 'NET AMOUNT' 132T 'P' /
                TabSetting(132),"T",NEWLINE,new TabSetting(3),"PPCN/",new TabSetting(11),"CERT.",new TabSetting(20),"REPURCHASE",new TabSetting(33),"   ",new 
                TabSetting(50),"ADDITIONAL",new TabSetting(68),"************* TAX DEDUCTIONS **************",new TabSetting(121),"AMOUNT",new TabSetting(132),"Y",NEWLINE,new 
                TabSetting(3),"PRODUCT",new TabSetting(11),"NUMBER",new TabSetting(22),"VALUE",new TabSetting(32),"    ",new TabSetting(38),"EXP CHG",new 
                TabSetting(51),"INTEREST",new TabSetting(65),"RC",new TabSetting(68),"*  FEDERAL       STATE            LOCAL   *",new TabSetting(113),"NET AMOUNT",new 
                TabSetting(132),"P",NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Detail() throws Exception                                                                                                                      //Natural: PRINT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Found.getBoolean()))                                                                                                                            //Natural: IF #FOUND
        {
            short decideConditionsMet983 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN ( #INT-ROLL-MODE ) OR NOT #INT-ROLL-MODE
            if (condition(pnd_Int_Roll_Mode.getBoolean() || ! (pnd_Int_Roll_Mode.getBoolean())))
            {
                decideConditionsMet983++;
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Cde().getValue(pnd_I));                                     //Natural: ASSIGN #INV-ACCT-INPUT := #RPT-EXT.INV-ACCT-CDE ( #I )
                DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                //Natural: CALLNAT 'FCPN199A' #FUND-PDA
                if (condition(Global.isEscape())) return;
            }                                                                                                                                                             //Natural: WHEN NOT #INT-ROLL-MODE
            if (condition(! ((pnd_Int_Roll_Mode.getBoolean()))))
            {
                decideConditionsMet983++;
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    if (condition(getReports().getAstLineCount(2).add(pnd_Array_Size).add(15).greater(Global.getPAGESIZE())))                                             //Natural: IF *LINE-COUNT ( PRT1 ) + #ARRAY-SIZE + 15 GT *PAGESIZE
                    {
                        getReports().newPage(new ReportSpecification(0));                                                                                                 //Natural: NEWPAGE ( PRT1 )
                        if (condition(Global.isEscape())){return;}
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr(), new AlphanumericLength (8),ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cref_Nbr());         //Natural: WRITE ( PRT1 ) #RPT-EXT.CNTRCT-PPCN-NBR ( AL = 8 ) #RPT-EXT.CNTRCT-CREF-NBR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(2, new TabSetting(3),pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(),new TabSetting(18),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I),  //Natural: WRITE ( PRT1 ) 003T #INV-ACCT-DESC-8 018T #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 037T #RPT-EXT.INV-ACCT-EXP-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 051T #RPT-EXT.INV-ACCT-DPI-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 065T #RPT-EXT.ANNT-RSDNCY-CDE 068T #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 082T #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 095T #STATE-CDE 100T #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 114T #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 )
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(37),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I), new ReportEditMask 
                    ("ZZZ,ZZ9.99"),new TabSetting(51),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I), new ReportEditMask ("ZZZ,ZZ9.99"),new 
                    TabSetting(65),ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde(),new TabSetting(68),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I), 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(82),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I), new ReportEditMask 
                    ("Z,ZZZ,ZZ9.99"),new TabSetting(95),pnd_State_Cde,new TabSetting(100),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(114),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I), new ReportEditMask 
                    ("Z,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                pnd_D_Vars_Pnd_D_Inv_Acct_Settl_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                                                //Natural: COMPUTE #D-INV-ACCT-SETTL-AMT = #D-INV-ACCT-SETTL-AMT + #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Exp_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I));                                                    //Natural: COMPUTE #D-INV-ACCT-EXP-AMT = #D-INV-ACCT-EXP-AMT + #RPT-EXT.INV-ACCT-EXP-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Dpi_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I));                                                    //Natural: COMPUTE #D-INV-ACCT-DPI-AMT = #D-INV-ACCT-DPI-AMT + #RPT-EXT.INV-ACCT-DPI-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Fdrl_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                                          //Natural: COMPUTE #D-INV-ACCT-FDRL-TAX-AMT = #D-INV-ACCT-FDRL-TAX-AMT + #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_State_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                                        //Natural: COMPUTE #D-INV-ACCT-STATE-TAX-AMT = #D-INV-ACCT-STATE-TAX-AMT + #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Local_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                                        //Natural: COMPUTE #D-INV-ACCT-LOCAL-TAX-AMT = #D-INV-ACCT-LOCAL-TAX-AMT + #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Net_Pymnt_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                        //Natural: COMPUTE #D-INV-ACCT-NET-PYMNT-AMT = #D-INV-ACCT-NET-PYMNT-AMT + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                if (condition(! (pnd_Printed_1.getBoolean())))                                                                                                            //Natural: IF NOT #PRINTED-1
                {
                    pnd_Printed_1.setValue(true);                                                                                                                         //Natural: ASSIGN #PRINTED-1 := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN #INT-ROLL-MODE
            if (condition(pnd_Int_Roll_Mode.getBoolean()))
            {
                decideConditionsMet983++;
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    if (condition(getReports().getAstLineCount(3).add(pnd_Array_Size).add(15).greater(Global.getPAGESIZE())))                                             //Natural: IF *LINE-COUNT ( PRT2 ) + #ARRAY-SIZE + 15 GT *PAGESIZE
                    {
                        getReports().newPage(new ReportSpecification(0));                                                                                                 //Natural: NEWPAGE ( PRT2 )
                        if (condition(Global.isEscape())){return;}
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(3, ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr(), new AlphanumericLength (8),ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cref_Nbr());         //Natural: WRITE ( PRT2 ) #RPT-EXT.CNTRCT-PPCN-NBR ( AL = 8 ) #RPT-EXT.CNTRCT-CREF-NBR
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(3, new TabSetting(3),pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(),new TabSetting(18),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I),  //Natural: WRITE ( PRT2 ) 003T #INV-ACCT-DESC-8 018T #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 037T #RPT-EXT.INV-ACCT-EXP-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 051T #RPT-EXT.INV-ACCT-DPI-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 065T #RPT-EXT.ANNT-RSDNCY-CDE 068T #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 082T #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 095T #STATE-CDE 100T #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 114T #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 )
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(37),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I), new ReportEditMask 
                    ("ZZZ,ZZ9.99"),new TabSetting(51),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I), new ReportEditMask ("ZZZ,ZZ9.99"),new 
                    TabSetting(65),ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde(),new TabSetting(68),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I), 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(82),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I), new ReportEditMask 
                    ("Z,ZZZ,ZZ9.99"),new TabSetting(95),pnd_State_Cde,new TabSetting(100),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I), 
                    new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(114),ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I), new ReportEditMask 
                    ("Z,ZZZ,ZZ9.99"));
                if (Global.isEscape()) return;
                pnd_D_Vars_Pnd_D_Inv_Acct_Settl_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                                                //Natural: COMPUTE #D-INV-ACCT-SETTL-AMT = #D-INV-ACCT-SETTL-AMT + #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Exp_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I));                                                    //Natural: COMPUTE #D-INV-ACCT-EXP-AMT = #D-INV-ACCT-EXP-AMT + #RPT-EXT.INV-ACCT-EXP-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Dpi_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I));                                                    //Natural: COMPUTE #D-INV-ACCT-DPI-AMT = #D-INV-ACCT-DPI-AMT + #RPT-EXT.INV-ACCT-DPI-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Fdrl_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                                          //Natural: COMPUTE #D-INV-ACCT-FDRL-TAX-AMT = #D-INV-ACCT-FDRL-TAX-AMT + #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_State_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                                        //Natural: COMPUTE #D-INV-ACCT-STATE-TAX-AMT = #D-INV-ACCT-STATE-TAX-AMT + #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Local_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                                        //Natural: COMPUTE #D-INV-ACCT-LOCAL-TAX-AMT = #D-INV-ACCT-LOCAL-TAX-AMT + #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
                pnd_D_Vars_Pnd_D_Inv_Acct_Net_Pymnt_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                        //Natural: COMPUTE #D-INV-ACCT-NET-PYMNT-AMT = #D-INV-ACCT-NET-PYMNT-AMT + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                if (condition(! (pnd_Printed_2.getBoolean())))                                                                                                            //Natural: IF NOT #PRINTED-2
                {
                    pnd_Printed_2.setValue(true);                                                                                                                         //Natural: ASSIGN #PRINTED-2 := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet983 > 0))
            {
                if (condition(pnd_I.equals(pnd_Array_Size)))                                                                                                              //Natural: IF #I = #ARRAY-SIZE
                {
                    pnd_Pymnt_Acctg_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte());                                                                           //Natural: ASSIGN #PYMNT-ACCTG-DTE := #RPT-EXT.PYMNT-ACCTG-DTE
                    pnd_Annt.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Name());                                                                                            //Natural: ASSIGN #ANNT := #RPT-EXT.ANNT-NAME
                    if (condition(DbsUtil.maskMatches(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1),"'CR '")))                                                       //Natural: IF #RPT-EXT.PYMNT-NME ( 1 ) = MASK ( 'CR ' )
                    {
                        pnd_Payee.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(1));                                                                //Natural: ASSIGN #PAYEE := #RPT-EXT.PYMNT-ADDR-LINE1-TXT ( 1 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Payee.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1));                                                                           //Natural: ASSIGN #PAYEE := #RPT-EXT.PYMNT-NME ( 1 )
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().equals("N")))                                                                        //Natural: IF #RPT-EXT.CNTRCT-PYMNT-TYPE-IND = 'N'
                    {
                        ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().reset();                                                                                       //Natural: RESET #RPT-EXT.CNTRCT-PYMNT-TYPE-IND
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Input_Parm.equals("RAMNTH")))                                                                                                       //Natural: IF #INPUT-PARM = 'RAMNTH'
                    {
                        pnd_Date_Type.setValue("PAYMENT DATE:");                                                                                                          //Natural: ASSIGN #DATE-TYPE := 'PAYMENT DATE:'
                        pnd_Detail_Date.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                                           //Natural: ASSIGN #DETAIL-DATE := #RPT-EXT.PYMNT-CHECK-DTE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Input_Parm.equals("RACASH")))                                                                                                   //Natural: IF #INPUT-PARM = 'RACASH'
                        {
                            pnd_Date_Type.setValue("ACCTING DATE:");                                                                                                      //Natural: ASSIGN #DATE-TYPE := 'ACCTING DATE:'
                            pnd_Detail_Date.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte());                                                                       //Natural: ASSIGN #DETAIL-DATE := #RPT-EXT.PYMNT-ACCTG-DTE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Date_Type.setValue("CHECK DATE:");                                                                                                        //Natural: ASSIGN #DATE-TYPE := 'CHECK DATE:'
                            pnd_Detail_Date.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                                       //Natural: ASSIGN #DETAIL-DATE := #RPT-EXT.PYMNT-CHECK-DTE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RL PAYEE MATCH
                    //*  RL PAYEE MATCH
                    //*  RL PAYEE MATCH
                    //*  RL PAYEE MATCH
                    if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                    //Natural: IF NOT #INT-ROLL-MODE
                    {
                        getReports().write(2, new TabSetting(118),ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new                   //Natural: WRITE ( PRT1 ) 118T #RPT-EXT.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99 ) 132T #PYMNT-TYP-CDE / 001T 'ANNT-' #ANNT / 002T 'PAY-' #PAYEE #RPT-EXT.CNTRCT-PYMNT-TYPE-IND // 001T 'SEQUENCE NO.' 014T #RPT-EXT.PYMNT-CHECK-SEQ-NBR ( EM = 9999999 ) 023T #DATE-TYPE #DETAIL-DATE ( EM = MM/DD/YY ) 049T #PYMNT-TYP-CDE-TXT 061T #CHECK-NUMBER-A10 074T 'HOLD CODE:' 086T #RPT-EXT.CNTRCT-HOLD-CDE 092T 'EFFECTIVE DATE:' #RPT-EXT.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) / 001T '-' ( 132 )
                            TabSetting(132),pnd_Pymnt_Typ_Cde,NEWLINE,new TabSetting(1),"ANNT-",pnd_Annt,NEWLINE,new TabSetting(2),"PAY-",pnd_Payee,ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind(),NEWLINE,NEWLINE,new 
                            TabSetting(1),"SEQUENCE NO.",new TabSetting(14),ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Seq_Nbr(), new ReportEditMask ("9999999"),new 
                            TabSetting(23),pnd_Date_Type,pnd_Detail_Date, new ReportEditMask ("MM/DD/YY"),new TabSetting(49),pnd_Pymnt_Typ_Cde_Txt,new TabSetting(61),pnd_Check_Number_N10_Pnd_Check_Number_A10,new 
                            TabSetting(74),"HOLD CODE:",new TabSetting(86),ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Cde(),new TabSetting(92),"EFFECTIVE DATE:",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte(), 
                            new ReportEditMask ("MM/DD/YY"),NEWLINE,new TabSetting(1),"-",new RepeatItem(132));
                        if (Global.isEscape()) return;
                        pnd_D_Vars_Pnd_D_Pymnt_Check_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                              //Natural: COMPUTE #D-PYMNT-CHECK-AMT = #D-PYMNT-CHECK-AMT + #RPT-EXT.PYMNT-CHECK-AMT
                        //*  RL PAYEE MATCH
                        //*  RL PAYEE MATCH
                        //*  RL PAYEE MATCH
                        //*  RL PAYEE MATCH
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(3, new TabSetting(118),ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new                   //Natural: WRITE ( PRT2 ) 118T #RPT-EXT.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99 ) 132T #PYMNT-TYP-CDE / 001T 'ANNT-' #ANNT / 002T 'PAY-' #PAYEE #RPT-EXT.CNTRCT-PYMNT-TYPE-IND // 001T 'SEQUENCE NO.' 014T #RPT-EXT.PYMNT-CHECK-SEQ-NBR ( EM = 9999999 ) 023T #DATE-TYPE #DETAIL-DATE ( EM = MM/DD/YY ) 049T #PYMNT-TYP-CDE-TXT 061T #CHECK-NUMBER-A10 074T 'HOLD CODE:' 086T #RPT-EXT.CNTRCT-HOLD-CDE 092T 'EFFECTIVE DATE:' #RPT-EXT.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YY ) / 001T '-' ( 132 )
                            TabSetting(132),pnd_Pymnt_Typ_Cde,NEWLINE,new TabSetting(1),"ANNT-",pnd_Annt,NEWLINE,new TabSetting(2),"PAY-",pnd_Payee,ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind(),NEWLINE,NEWLINE,new 
                            TabSetting(1),"SEQUENCE NO.",new TabSetting(14),ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Seq_Nbr(), new ReportEditMask ("9999999"),new 
                            TabSetting(23),pnd_Date_Type,pnd_Detail_Date, new ReportEditMask ("MM/DD/YY"),new TabSetting(49),pnd_Pymnt_Typ_Cde_Txt,new TabSetting(61),pnd_Check_Number_N10_Pnd_Check_Number_A10,new 
                            TabSetting(74),"HOLD CODE:",new TabSetting(86),ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Cde(),new TabSetting(92),"EFFECTIVE DATE:",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte(), 
                            new ReportEditMask ("MM/DD/YY"),NEWLINE,new TabSetting(1),"-",new RepeatItem(132));
                        if (Global.isEscape()) return;
                        pnd_D_Vars_Pnd_D_Pymnt_Check_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                              //Natural: COMPUTE #D-PYMNT-CHECK-AMT = #D-PYMNT-CHECK-AMT + #RPT-EXT.PYMNT-CHECK-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CREATE-SUMMARY-DETAIL
                sub_Create_Summary_Detail();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet983 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Sub_Total() throws Exception                                                                                                                   //Natural: PRINT-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Sub_Total.setValue(true);                                                                                                                                     //Natural: ASSIGN #SUB-TOTAL := TRUE
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                       //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 6 LINES LEFT
            {
                getReports().newPage(0);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(2, NEWLINE,"SUB-TOTALS:",NEWLINE,NEWLINE,"SETTL:",new TabSetting(8),pnd_D_Vars_Pnd_D_Inv_Acct_Settl_Amt, new ReportEditMask                //Natural: WRITE ( PRT1 ) / 'SUB-TOTALS:' // 'SETTL:' 008T #D-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 028T 'EXP:' 042T #D-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 053T 'DPI:' 067T #D-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 078T 'FED:' 083T #D-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 103T 'ST:' 107T #D-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) / 'LCL:' 017T #D-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 028T 'NET:' 033T #D-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 053T 'TOTAL CHECK AMT:' #D-PYMNT-CHECK-AMT ( EM = ZZZZZZZZZZZZZZ9.99 )
                ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(28),"EXP:",new TabSetting(42),pnd_D_Vars_Pnd_D_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new 
                TabSetting(53),"DPI:",new TabSetting(67),pnd_D_Vars_Pnd_D_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(78),"FED:",new 
                TabSetting(83),pnd_D_Vars_Pnd_D_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(103),"ST:",new TabSetting(107),pnd_D_Vars_Pnd_D_Inv_Acct_State_Tax_Amt, 
                new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),NEWLINE,"LCL:",new TabSetting(17),pnd_D_Vars_Pnd_D_Inv_Acct_Local_Tax_Amt, new ReportEditMask 
                ("ZZZZZ9.99"),new TabSetting(28),"NET:",new TabSetting(33),pnd_D_Vars_Pnd_D_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                TabSetting(53),"TOTAL CHECK AMT:",pnd_D_Vars_Pnd_D_Pymnt_Check_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Sub_Total.setValue(false);                                                                                                                                //Natural: ASSIGN #SUB-TOTAL := FALSE
            if (condition(! (pnd_Pay_Type_Req_Break.getBoolean())))                                                                                                       //Natural: IF NOT #PAY-TYPE-REQ-BREAK
            {
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( PRT1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Summary.setValue(true);                                                                                                                               //Natural: ASSIGN #SUMMARY := TRUE
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( PRT1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                       //Natural: NEWPAGE ( PRT2 ) IF LESS THAN 6 LINES LEFT
            {
                getReports().newPage(0);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(3, NEWLINE,"SUB-TOTALS:",NEWLINE,NEWLINE,"SETTL:",new TabSetting(8),pnd_D_Vars_Pnd_D_Inv_Acct_Settl_Amt, new ReportEditMask                //Natural: WRITE ( PRT2 ) / 'SUB-TOTALS:' // 'SETTL:' 008T #D-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 028T 'EXP:' 042T #D-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 053T 'DPI:' 067T #D-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 078T 'FED:' 083T #D-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 103T 'ST:' 107T #D-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) / 'LCL:' 017T #D-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 028T 'NET:' 033T #D-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 053T 'TOTAL CHECK AMT:' #D-PYMNT-CHECK-AMT ( EM = ZZZZZZZZZZZZZZ9.99 )
                ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(28),"EXP:",new TabSetting(42),pnd_D_Vars_Pnd_D_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new 
                TabSetting(53),"DPI:",new TabSetting(67),pnd_D_Vars_Pnd_D_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(78),"FED:",new 
                TabSetting(83),pnd_D_Vars_Pnd_D_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(103),"ST:",new TabSetting(107),pnd_D_Vars_Pnd_D_Inv_Acct_State_Tax_Amt, 
                new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),NEWLINE,"LCL:",new TabSetting(17),pnd_D_Vars_Pnd_D_Inv_Acct_Local_Tax_Amt, new ReportEditMask 
                ("ZZZZZ9.99"),new TabSetting(28),"NET:",new TabSetting(33),pnd_D_Vars_Pnd_D_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                TabSetting(53),"TOTAL CHECK AMT:",pnd_D_Vars_Pnd_D_Pymnt_Check_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Sub_Total.setValue(false);                                                                                                                                //Natural: ASSIGN #SUB-TOTAL := FALSE
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( PRT2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Sub_Prt.getBoolean())))                                                                                                                      //Natural: IF NOT #SUB-PRT
        {
            pnd_Sub_Prt.setValue(true);                                                                                                                                   //Natural: ASSIGN #SUB-PRT := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_D_Vars.reset();                                                                                                                                               //Natural: RESET #D-VARS
    }
    private void sub_Text_And_Counter() throws Exception                                                                                                                  //Natural: TEXT-AND-COUNTER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1103 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet1103++;
            pnd_Pymnt_Typ_Cde.setValue("C");                                                                                                                              //Natural: ASSIGN #PYMNT-TYP-CDE = 'C'
            pnd_Pymnt_Typ_Cde_Txt.setValue("  CHECK NO.");                                                                                                                //Natural: ASSIGN #PYMNT-TYP-CDE-TXT = '  CHECK NO.'
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet1103++;
            pnd_Pymnt_Typ_Cde.setValue("E");                                                                                                                              //Natural: ASSIGN #PYMNT-TYP-CDE = 'E'
            pnd_Pymnt_Typ_Cde_Txt.setValue("    EFT NO.");                                                                                                                //Natural: ASSIGN #PYMNT-TYP-CDE-TXT = '    EFT NO.'
            if (condition(pnd_Pymnt_Check_Dte.equals(pnd_Null_Dte)))                                                                                                      //Natural: IF #PYMNT-CHECK-DTE = #NULL-DTE
            {
                pnd_Pymnt_Check_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Dte());                                                                                 //Natural: ASSIGN #PYMNT-CHECK-DTE = #RPT-EXT.PYMNT-EFT-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(3))))
        {
            decideConditionsMet1103++;
            pnd_Pymnt_Typ_Cde.setValue("G");                                                                                                                              //Natural: ASSIGN #PYMNT-TYP-CDE = 'G'
            pnd_Pymnt_Typ_Cde_Txt.setValue(" GLBPAY NO.");                                                                                                                //Natural: ASSIGN #PYMNT-TYP-CDE-TXT = ' GLBPAY NO.'
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8))))
        {
            decideConditionsMet1103++;
            pnd_Pymnt_Typ_Cde.setValue("X");                                                                                                                              //Natural: ASSIGN #PYMNT-TYP-CDE = 'X'
            if (condition(! (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("H") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("J")         //Natural: IF NOT ( #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND = 'H' OR = 'J' OR = 'T' )
                || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("T"))))
            {
                pnd_Pymnt_Typ_Cde_Txt.setValue("CLASSIC NO.");                                                                                                            //Natural: ASSIGN #PYMNT-TYP-CDE-TXT = 'CLASSIC NO.'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Typ_Cde_Txt.setValue("   ROTH NO.");                                                                                                            //Natural: ASSIGN #PYMNT-TYP-CDE-TXT = '   ROTH NO.'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet1103 > 0))
        {
            //* ******************** RL PAYEE MATCH BEGIN MAY 26 2006 *****************
            pnd_Check_Datex.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                  //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
            pnd_Pymnt_Check_Num.reset();                                                                                                                                  //Natural: RESET #PYMNT-CHECK-NUM #CHECK-NUMBER-A10
            pnd_Check_Number_N10_Pnd_Check_Number_A10.reset();
            if (condition(! (ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2) || ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8))))            //Natural: IF NOT ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8 )
            {
                //* N7 CHK-NBR - MAKE N10 W PREFIX
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().less(getZero())))                                                                              //Natural: IF #RPT-EXT.PYMNT-CHECK-NBR LT 0
                {
                    pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                     //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
                    pnd_Check_Number_N10_Pnd_Check_Number_N7.compute(new ComputeParameters(false, pnd_Check_Number_N10_Pnd_Check_Number_N7), ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().multiply(-1)); //Natural: COMPUTE #CHECK-NUMBER-N7 = #RPT-EXT.PYMNT-CHECK-NBR * -1
                    pnd_Pymnt_Check_Num.setValue(pnd_Check_Number_N10_Pnd_Check_Number_A10);                                                                              //Natural: ASSIGN #PYMNT-CHECK-NUM = #CHECK-NUMBER-A10
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  VALID OLD N7 CHECK-NBR
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().greater(getZero())))                                                                       //Natural: IF #RPT-EXT.PYMNT-CHECK-NBR GT 0
                    {
                        pnd_Check_Number_N10_Pnd_Check_Number_N7.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr());                                                  //Natural: ASSIGN #CHECK-NUMBER-N7 := #RPT-EXT.PYMNT-CHECK-NBR
                        pnd_Check_Number_N10_Pnd_Check_Number_N3.reset();                                                                                                 //Natural: RESET #CHECK-NUMBER-N3
                        if (condition(pnd_Check_Datex_Pnd_Check_Date.greater(20060430)))                                                                                  //Natural: IF #CHECK-DATE GT 20060430
                        {
                            pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                             //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Check_Number_N10_Pnd_Check_Number_N3.reset();                                                                                             //Natural: RESET #CHECK-NUMBER-N3
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Pymnt_Check_Num.setValue(pnd_Check_Number_N10_Pnd_Check_Number_A10);                                                                          //Natural: ASSIGN #PYMNT-CHECK-NUM = #CHECK-NUMBER-A10
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Check_Number_N10_Pnd_Check_Number_A10.reset();                                                                                                //Natural: RESET #CHECK-NUMBER-A10
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt_Check_Num.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr());                                                                         //Natural: ASSIGN #PYMNT-CHECK-NUM = #RPT-EXT.PYMNT-CHECK-SCRTY-NBR
                pnd_Check_Number_N10_Pnd_Check_Number_A10.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr());                                                   //Natural: ASSIGN #CHECK-NUMBER-A10 = #RPT-EXT.PYMNT-CHECK-SCRTY-NBR
            }                                                                                                                                                             //Natural: END-IF
            //* *********************** RL PAYEE MATCH END ***************************
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().notEquals(8)))                                                                              //Natural: IF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND NE 8
            {
                pnd_K1.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind());                                                                                     //Natural: ASSIGN #K1 = #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_K1.setValue(4);                                                                                                                                       //Natural: ASSIGN #K1 := 4
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L")))                                                                                      //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = 'L'
            {
                short decideConditionsMet1159 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #RPT-EXT.CNTRCT-LOB-CDE;//Natural: VALUE 'GRA', 'GRAD'
                if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("GRA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("GRAD"))))
                {
                    decideConditionsMet1159++;
                    pnd_K2.setValue(1);                                                                                                                                   //Natural: ASSIGN #K2 = 1
                }                                                                                                                                                         //Natural: VALUE 'GSRA', 'GSRD', 'SRA', 'SRAD'
                else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("GSRA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("GSRD") 
                    || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("SRA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("SRAD"))))
                {
                    decideConditionsMet1159++;
                    pnd_K2.setValue(2);                                                                                                                                   //Natural: ASSIGN #K2 = 2
                }                                                                                                                                                         //Natural: VALUE 'XRA', 'XRAD'
                else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("XRA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("XRAD"))))
                {
                    decideConditionsMet1159++;
                    pnd_K2.setValue(3);                                                                                                                                   //Natural: ASSIGN #K2 = 3
                }                                                                                                                                                         //Natural: VALUE 'IRA', 'IRAD'
                else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("IRA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("IRAD"))))
                {
                    decideConditionsMet1159++;
                    //*  IRA
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().notEquals(8)))                                                                      //Natural: IF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND NE 8
                    {
                        pnd_K2.setValue(4);                                                                                                                               //Natural: ASSIGN #K2 := 4
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  CLASSIC
                        if (condition(! (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("H") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("J")))) //Natural: IF NOT ( #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND = 'H' OR = 'J' )
                        {
                            pnd_K2.setValue(4);                                                                                                                           //Natural: ASSIGN #K2 := 4
                            //*  ROTH
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_K2.setValue(5);                                                                                                                           //Natural: ASSIGN #K2 := 5
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet1159 > 0))
                {
                    if (condition((! ((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("IRA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals("IRAD")))          //Natural: IF ( NOT ( #RPT-EXT.CNTRCT-LOB-CDE = 'IRA' OR = 'IRAD' ) ) AND ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 8 )
                        && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8))))
                    {
                        pnd_K2.setValue(4);                                                                                                                               //Natural: ASSIGN #K2 := 4
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    //*  RTB
                    //*  CLASSIC
                    if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().equals("N")) && (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("X")))) //Natural: IF ( #RPT-EXT.CNTRCT-PYMNT-TYPE-IND = 'N' ) AND ( #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND = 'X' )
                    {
                        pnd_K2.setValue(4);                                                                                                                               //Natural: ASSIGN #K2 := 4
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_K2.setValue(1);                                                                                                                                       //Natural: ASSIGN #K2 = 1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().getSubstring(4,1).equals("D")))                                                                     //Natural: IF SUBSTR ( #RPT-EXT.CNTRCT-LOB-CDE,4,1 ) = 'D'
            {
                pnd_Direct_Trans.setValue(true);                                                                                                                          //Natural: ASSIGN #DIRECT-TRANS := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Direct_Trans.setValue(false);                                                                                                                         //Natural: ASSIGN #DIRECT-TRANS := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Pymnt_Typ_Cde.reset();                                                                                                                                    //Natural: RESET #PYMNT-TYP-CDE #PYMNT-TYP-CDE-TXT
            pnd_Pymnt_Typ_Cde_Txt.reset();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde().greaterOrEqual("01") && ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde().lessOrEqual("86")))               //Natural: IF #RPT-EXT.ANNT-RSDNCY-CDE = '01' THRU '86'
        {
            pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde());                                                                    //Natural: ASSIGN TBLDCODA.#CODE = #RPT-EXT.ANNT-RSDNCY-CDE
            DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pdaFcppda_M.getMsg_Info_Sub());                                          //Natural: CALLNAT 'TBLDCOD' TBLDCODA MSG-INFO-SUB
            if (condition(Global.isEscape())) return;
            pnd_State_Cde.setValue(pdaTbldcoda.getTbldcoda_Pnd_Target());                                                                                                 //Natural: ASSIGN #STATE-CDE = TBLDCODA.#TARGET
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_State_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde());                                                                                         //Natural: ASSIGN #STATE-CDE = #RPT-EXT.ANNT-RSDNCY-CDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_K1.greaterOrEqual(1) && pnd_K1.lessOrEqual(4)))                                                                                                 //Natural: IF #K1 = 1 THRU 4
        {
            //* *DISPLAY                                                 /* TEMPORARY
            //* *  'ORG'    #RPT-EXT.CNTRCT-ORGN-CDE
            //* *  'CHK DT' #RPT-EXT.PYMNT-CHECK-DTE (EM=MM/DD/YY)
            //* *  'ACC DT' #RPT-EXT.PYMNT-ACCTG-DTE (EM=MM/DD/YY)
            //* *  'CNTRCT' #RPT-EXT.CNTRCT-PPCN-NBR
            //* *  'LOB'    #RPT-EXT.CNTRCT-LOB-CDE
            //* *  'CT'     #RPT-EXT.CNTRCT-TYPE-CDE
            //* *  'PT'     #RPT-EXT.CNTRCT-PYMNT-TYPE-IND
            //* *  'ST'     #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND
            //* *  'ROLL'   #RPT-EXT.CNTRCT-ROLL-DEST-CDE
            //* *  '$'      #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND
            //* *  'MODE'   #INT-ROLL-MODE
            //* *  '='      #SUM-I '=' #K1 '=' #K2
            pnd_Cntrct_Type_Sum_Pnd_Qty.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(1);                                                                                        //Natural: COMPUTE #CNTRCT-TYPE-SUM.#QTY ( #SUM-I,#K2,#K1 ) = #CNTRCT-TYPE-SUM.#QTY ( #SUM-I,#K2,#K1 ) + 1
            pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(1);                                                                                  //Natural: COMPUTE #GT-CNTRCT-TYPE-SUM.#GT-QTY ( #SUM-I,#K2,#K1 ) = #GT-CNTRCT-TYPE-SUM.#GT-QTY ( #SUM-I,#K2,#K1 ) + 1
            short decideConditionsMet1227 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN NOT #INT-ROLL-MODE
            if (condition(! ((pnd_Int_Roll_Mode.getBoolean()))))
            {
                decideConditionsMet1227++;
                pnd_Mode_Index.setValue(1);                                                                                                                               //Natural: ASSIGN #MODE-INDEX := 1
            }                                                                                                                                                             //Natural: WHEN #INT-ROLL-MODE
            if (condition(pnd_Int_Roll_Mode.getBoolean()))
            {
                decideConditionsMet1227++;
                pnd_Mode_Index.setValue(2);                                                                                                                               //Natural: ASSIGN #MODE-INDEX := 2
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1227 > 0))
            {
                if (condition(pnd_Direct_Trans.getBoolean()))                                                                                                             //Natural: IF #DIRECT-TRANS
                {
                    pnd_Misc_Tot_Index.setValue(1);                                                                                                                       //Natural: ASSIGN #MISC-TOT-INDEX := 1
                    pnd_Misc_Grand_Totals_Pnd_Misc_Qty.getValue(pnd_Mode_Index,pnd_Misc_Tot_Index).nadd(1);                                                               //Natural: ADD 1 TO #MISC-QTY ( #MODE-INDEX,#MISC-TOT-INDEX )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet1227 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Category_Txt() throws Exception                                                                                                                      //Natural: CATEGORY-TXT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1247 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #J;//Natural: VALUE 1
        if (condition((pnd_J.equals(1))))
        {
            decideConditionsMet1247++;
            pnd_Category_Txt.setValue("CHK");                                                                                                                             //Natural: ASSIGN #CATEGORY-TXT = 'CHK'
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_J.equals(2))))
        {
            decideConditionsMet1247++;
            pnd_Category_Txt.setValue("EFT");                                                                                                                             //Natural: ASSIGN #CATEGORY-TXT = 'EFT'
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_J.equals(3))))
        {
            decideConditionsMet1247++;
            pnd_Category_Txt.setValue("GLB");                                                                                                                             //Natural: ASSIGN #CATEGORY-TXT = 'GLB'
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_J.equals(4))))
        {
            decideConditionsMet1247++;
            pnd_Category_Txt.setValue("ROLL");                                                                                                                            //Natural: ASSIGN #CATEGORY-TXT = 'ROLL'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Category_Txt.reset();                                                                                                                                     //Natural: RESET #CATEGORY-TXT
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Initialize_Category_Txt() throws Exception                                                                                                           //Natural: INITIALIZE-CATEGORY-TXT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1263 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #I;//Natural: VALUE 1
        if (condition((pnd_I.equals(1))))
        {
            decideConditionsMet1263++;
            pnd_Category_Txt.setValue("GRA TOTAL   ");                                                                                                                    //Natural: ASSIGN #CATEGORY-TXT := 'GRA TOTAL   '
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_I.equals(2))))
        {
            decideConditionsMet1263++;
            pnd_Category_Txt.setValue("SRA/GSRA TOT");                                                                                                                    //Natural: ASSIGN #CATEGORY-TXT := 'SRA/GSRA TOT'
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_I.equals(3))))
        {
            decideConditionsMet1263++;
            pnd_Category_Txt.setValue("RA TOTAL    ");                                                                                                                    //Natural: ASSIGN #CATEGORY-TXT := 'RA TOTAL    '
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_I.equals(4))))
        {
            decideConditionsMet1263++;
            if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                            //Natural: IF NOT #INT-ROLL-MODE
            {
                pnd_Category_Txt.setValue("IRA TOTAL   ");                                                                                                                //Natural: ASSIGN #CATEGORY-TXT := 'IRA TOTAL   '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Category_Txt.setValue("CLASSIC TOT ");                                                                                                                //Natural: ASSIGN #CATEGORY-TXT := 'CLASSIC TOT '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pnd_I.equals(5))))
        {
            decideConditionsMet1263++;
            if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                            //Natural: IF NOT #INT-ROLL-MODE
            {
                pnd_Category_Txt.setValue("IRA TOTAL   ");                                                                                                                //Natural: ASSIGN #CATEGORY-TXT := 'IRA TOTAL   '
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Category_Txt.setValue("ROTH TOTAL  ");                                                                                                                //Natural: ASSIGN #CATEGORY-TXT := 'ROTH TOTAL  '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Category_Txt.setValue("MISC. TOTAL ");                                                                                                                    //Natural: ASSIGN #CATEGORY-TXT := 'MISC. TOTAL '
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Compress_Category_Txt() throws Exception                                                                                                             //Natural: COMPRESS-CATEGORY-TXT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        short decideConditionsMet1290 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #I;//Natural: VALUE 1
        if (condition((pnd_I.equals(1))))
        {
            decideConditionsMet1290++;
            pnd_Category_Txt.setValue(DbsUtil.compress("GRA", pnd_Category_Txt));                                                                                         //Natural: COMPRESS 'GRA' #CATEGORY-TXT INTO #CATEGORY-TXT
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_I.equals(2))))
        {
            decideConditionsMet1290++;
            pnd_Category_Txt.setValue(DbsUtil.compress("SRA/GSRA", pnd_Category_Txt));                                                                                    //Natural: COMPRESS 'SRA/GSRA' #CATEGORY-TXT INTO #CATEGORY-TXT
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_I.equals(3))))
        {
            decideConditionsMet1290++;
            pnd_Category_Txt.setValue(DbsUtil.compress("RA", pnd_Category_Txt));                                                                                          //Natural: COMPRESS 'RA' #CATEGORY-TXT INTO #CATEGORY-TXT
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((pnd_I.equals(4))))
        {
            decideConditionsMet1290++;
            if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                            //Natural: IF NOT #INT-ROLL-MODE
            {
                pnd_Category_Txt.setValue(DbsUtil.compress("IRA", pnd_Category_Txt));                                                                                     //Natural: COMPRESS 'IRA' #CATEGORY-TXT INTO #CATEGORY-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Category_Txt.setValue(DbsUtil.compress("CLASSIC", pnd_Category_Txt));                                                                                 //Natural: COMPRESS 'CLASSIC' #CATEGORY-TXT INTO #CATEGORY-TXT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 5
        else if (condition((pnd_I.equals(5))))
        {
            decideConditionsMet1290++;
            if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                            //Natural: IF NOT #INT-ROLL-MODE
            {
                pnd_Category_Txt.setValue(DbsUtil.compress("IRA", pnd_Category_Txt));                                                                                     //Natural: COMPRESS 'IRA' #CATEGORY-TXT INTO #CATEGORY-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Category_Txt.setValue(DbsUtil.compress("ROTH", pnd_Category_Txt));                                                                                    //Natural: COMPRESS 'ROTH' #CATEGORY-TXT INTO #CATEGORY-TXT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Create_Summary_Detail() throws Exception                                                                                                             //Natural: CREATE-SUMMARY-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));             //Natural: COMPUTE #CNTRCT-TYPE-SUM.#S-INV-ACCT-SETTL-AMT ( #SUM-I,#K2,#K1 ) = #CNTRCT-TYPE-SUM.#S-INV-ACCT-SETTL-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I));                 //Natural: COMPUTE #CNTRCT-TYPE-SUM.#S-INV-ACCT-EXP-AMT ( #SUM-I,#K2,#K1 ) = #CNTRCT-TYPE-SUM.#S-INV-ACCT-EXP-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-EXP-AMT ( #I )
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I));                 //Natural: COMPUTE #CNTRCT-TYPE-SUM.#S-INV-ACCT-DPI-AMT ( #SUM-I,#K2,#K1 ) = #CNTRCT-TYPE-SUM.#S-INV-ACCT-DPI-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-DPI-AMT ( #I )
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));       //Natural: COMPUTE #CNTRCT-TYPE-SUM.#S-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#K2,#K1 ) = #CNTRCT-TYPE-SUM.#S-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I));     //Natural: COMPUTE #CNTRCT-TYPE-SUM.#S-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#K2,#K1 ) = #CNTRCT-TYPE-SUM.#S-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I )
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));     //Natural: COMPUTE #CNTRCT-TYPE-SUM.#S-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#K2,#K1 ) = #CNTRCT-TYPE-SUM.#S-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
        pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));     //Natural: COMPUTE #CNTRCT-TYPE-SUM.#S-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#K2,#K1 ) = #CNTRCT-TYPE-SUM.#S-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
        //* ** 08-24-94 : A. YOUNG - CREATE GRAND TOTAL DETAILS
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));         //Natural: COMPUTE #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-SETTL-AMT ( #SUM-I,#K2,#K1 ) = #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-SETTL-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I));             //Natural: COMPUTE #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-EXP-AMT ( #SUM-I,#K2,#K1 ) = #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-EXP-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-EXP-AMT ( #I )
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I));             //Natural: COMPUTE #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-DPI-AMT ( #SUM-I,#K2,#K1 ) = #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-DPI-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-DPI-AMT ( #I )
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));   //Natural: COMPUTE #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#K2,#K1 ) = #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I)); //Natural: COMPUTE #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#K2,#K1 ) = #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I )
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I)); //Natural: COMPUTE #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#K2,#K1 ) = #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
        pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,pnd_K2,pnd_K1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I)); //Natural: COMPUTE #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#K2,#K1 ) = #GT-CNTRCT-TYPE-SUM.#GT-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#K2,#K1 ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
        //* ** 05-05-99 : A. YOUNG - CREATE MISCELLANEOUS GRAND TOTAL DETAILS
        short decideConditionsMet1334 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN NOT #INT-ROLL-MODE
        if (condition(! ((pnd_Int_Roll_Mode.getBoolean()))))
        {
            decideConditionsMet1334++;
            pnd_Mode_Index.setValue(1);                                                                                                                                   //Natural: ASSIGN #MODE-INDEX := 1
        }                                                                                                                                                                 //Natural: WHEN #INT-ROLL-MODE
        if (condition(pnd_Int_Roll_Mode.getBoolean()))
        {
            decideConditionsMet1334++;
            pnd_Mode_Index.setValue(2);                                                                                                                                   //Natural: ASSIGN #MODE-INDEX := 2
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1334 > 0))
        {
            if (condition(pnd_Direct_Trans.getBoolean()))                                                                                                                 //Natural: IF #DIRECT-TRANS
            {
                pnd_Misc_Tot_Index.setValue(1);                                                                                                                           //Natural: ASSIGN #MISC-TOT-INDEX := 1
                                                                                                                                                                          //Natural: PERFORM CREATE-MISC-GT-DETAILS
                sub_Create_Misc_Gt_Details();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1334 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Create_Misc_Gt_Details() throws Exception                                                                                                            //Natural: CREATE-MISC-GT-DETAILS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Settl_Amt.getValue(pnd_Mode_Index,pnd_Misc_Tot_Index).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I)); //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO #MISC-INV-ACCT-SETTL-AMT ( #MODE-INDEX,#MISC-TOT-INDEX )
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Exp_Amt.getValue(pnd_Mode_Index,pnd_Misc_Tot_Index).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I));  //Natural: ADD #RPT-EXT.INV-ACCT-EXP-AMT ( #I ) TO #MISC-INV-ACCT-EXP-AMT ( #MODE-INDEX,#MISC-TOT-INDEX )
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Dpi_Amt.getValue(pnd_Mode_Index,pnd_Misc_Tot_Index).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I));  //Natural: ADD #RPT-EXT.INV-ACCT-DPI-AMT ( #I ) TO #MISC-INV-ACCT-DPI-AMT ( #MODE-INDEX,#MISC-TOT-INDEX )
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Mode_Index,pnd_Misc_Tot_Index).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I)); //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO #MISC-INV-ACCT-FDRL-TAX-AMT ( #MODE-INDEX,#MISC-TOT-INDEX )
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_State_Tax_Amt.getValue(pnd_Mode_Index,pnd_Misc_Tot_Index).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I)); //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) TO #MISC-INV-ACCT-STATE-TAX-AMT ( #MODE-INDEX,#MISC-TOT-INDEX )
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Local_Tax_Amt.getValue(pnd_Mode_Index,pnd_Misc_Tot_Index).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I)); //Natural: ADD #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) TO #MISC-INV-ACCT-LOCAL-TAX-AMT ( #MODE-INDEX,#MISC-TOT-INDEX )
        pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Mode_Index,pnd_Misc_Tot_Index).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I)); //Natural: ADD #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I ) TO #MISC-INV-ACCT-NET-PYMNT-AMT ( #MODE-INDEX,#MISC-TOT-INDEX )
    }
    private void sub_Print_Sub_Total_Detail() throws Exception                                                                                                            //Natural: PRINT-SUB-TOTAL-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Summary.setValue(true);                                                                                                                                       //Natural: ASSIGN #SUMMARY = TRUE
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( PRT1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( PRT2 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INDEX-RANGE
        sub_Determine_Index_Range();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pnd_Found.getBoolean()))                                                                                                                            //Natural: IF #FOUND
        {
            //* *IF #GT-QTY (#SUM-I,*,*) > 0
            if (condition(pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("L")))                                                                                           //Natural: IF #CNTRCT-TYPE-CDE ( #SUM-I ) = 'L'
            {
                FOR02:                                                                                                                                                    //Natural: FOR #I #I-START #I-END
                for (pnd_I.setValue(pnd_I_Start); condition(pnd_I.lessOrEqual(pnd_I_End)); pnd_I.nadd(1))
                {
                    if (condition(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,pnd_I,"*").greater(getZero())))                                                    //Natural: IF #GT-QTY ( #SUM-I,#I,* ) > 0
                    {
                        FOR03:                                                                                                                                            //Natural: FOR #J #J-START #J-END
                        for (pnd_J.setValue(pnd_J_Start); condition(pnd_J.lessOrEqual(pnd_J_End)); pnd_J.nadd(1))
                        {
                            if (condition(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,"*",pnd_J).greater(getZero())))                                            //Natural: IF #GT-QTY ( #SUM-I,*,#J ) > 0
                            {
                                                                                                                                                                          //Natural: PERFORM CATEGORY-TXT
                                sub_Category_Txt();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM COMPRESS-CATEGORY-TXT
                                sub_Compress_Category_Txt();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J));                   //Natural: COMPUTE #T-INV-ACCT-SETTL-AMT = #T-INV-ACCT-SETTL-AMT + #S-INV-ACCT-SETTL-AMT ( #SUM-I,#I,#J )
                                pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J));                       //Natural: COMPUTE #T-INV-ACCT-EXP-AMT = #T-INV-ACCT-EXP-AMT + #S-INV-ACCT-EXP-AMT ( #SUM-I,#I,#J )
                                pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J));                       //Natural: COMPUTE #T-INV-ACCT-DPI-AMT = #T-INV-ACCT-DPI-AMT + #S-INV-ACCT-DPI-AMT ( #SUM-I,#I,#J )
                                pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J));             //Natural: COMPUTE #T-INV-ACCT-FDRL-TAX-AMT = #T-INV-ACCT-FDRL-TAX-AMT + #S-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#I,#J )
                                pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,pnd_I,                   //Natural: COMPUTE #T-INV-ACCT-STATE-TAX-AMT = #T-INV-ACCT-STATE-TAX-AMT + #S-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#I,#J )
                                    pnd_J));
                                pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,pnd_I,                   //Natural: COMPUTE #T-INV-ACCT-LOCAL-TAX-AMT = #T-INV-ACCT-LOCAL-TAX-AMT + #S-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#I,#J )
                                    pnd_J));
                                pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,pnd_I,                   //Natural: COMPUTE #T-INV-ACCT-NET-PYMNT-AMT = #T-INV-ACCT-NET-PYMNT-AMT + #S-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#I,#J )
                                    pnd_J));
                                pnd_T_Vars_Pnd_T_Qty.nadd(pnd_Cntrct_Type_Sum_Pnd_Qty.getValue(pnd_Sum_I,pnd_I,pnd_J));                                                   //Natural: COMPUTE #T-QTY = #T-QTY + #QTY ( #SUM-I,#I,#J )
                                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                                {
                                    getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J),  //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #S-INV-ACCT-SETTL-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #S-INV-ACCT-EXP-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 045T #S-INV-ACCT-DPI-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 055T #S-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #S-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #S-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 103T #S-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #QTY ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZ ) /
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(122),pnd_Cntrct_Type_Sum_Pnd_Qty.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZZZZZ"),NEWLINE);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    getReports().write(3, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J),  //Natural: WRITE ( PRT2 ) 001T #CATEGORY-TXT 016T #S-INV-ACCT-SETTL-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #S-INV-ACCT-EXP-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 045T #S-INV-ACCT-DPI-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 055T #S-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #S-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #S-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 103T #S-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #QTY ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZ ) /
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(122),pnd_Cntrct_Type_Sum_Pnd_Qty.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                        new ReportEditMask ("ZZZZZZZZZ"),NEWLINE);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-CATEGORY-TXT
                        sub_Initialize_Category_Txt();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                //Natural: IF NOT #INT-ROLL-MODE
                        {
                            getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask           //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ ) / /
                                ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                                new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                                TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                                new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                                TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"),NEWLINE,NEWLINE);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(3, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask           //Natural: WRITE ( PRT2 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ ) / /
                                ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                                new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                                TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                                new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                                TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"),NEWLINE,NEWLINE);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_T_Vars.reset();                                                                                                                               //Natural: RESET #T-VARS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            FOR04:                                                                                                                                                        //Natural: FOR #J #J-START #J-END
            for (pnd_J.setValue(pnd_J_Start); condition(pnd_J.lessOrEqual(pnd_J_End)); pnd_J.nadd(1))
            {
                if (condition(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,"*",pnd_J).greater(getZero())))                                                        //Natural: IF #GT-QTY ( #SUM-I,*,#J ) > 0
                {
                                                                                                                                                                          //Natural: PERFORM CATEGORY-TXT
                    sub_Category_Txt();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Mdo.getBoolean()))                                                                                                                  //Natural: IF #MDO
                    {
                        pnd_Category_Txt.setValue(DbsUtil.compress("MDO", pnd_Category_Txt));                                                                             //Natural: COMPRESS 'MDO' #CATEGORY-TXT INTO #CATEGORY-TXT
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,"*",pnd_J));                                 //Natural: COMPUTE #T-INV-ACCT-SETTL-AMT = #T-INV-ACCT-SETTL-AMT + #S-INV-ACCT-SETTL-AMT ( #SUM-I,*,#J )
                    pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,"*",pnd_J));                                     //Natural: COMPUTE #T-INV-ACCT-EXP-AMT = #T-INV-ACCT-EXP-AMT + #S-INV-ACCT-EXP-AMT ( #SUM-I,*,#J )
                    pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,"*",pnd_J));                                     //Natural: COMPUTE #T-INV-ACCT-DPI-AMT = #T-INV-ACCT-DPI-AMT + #S-INV-ACCT-DPI-AMT ( #SUM-I,*,#J )
                    pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,"*",pnd_J));                           //Natural: COMPUTE #T-INV-ACCT-FDRL-TAX-AMT = #T-INV-ACCT-FDRL-TAX-AMT + #S-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,*,#J )
                    pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,"*",pnd_J));                         //Natural: COMPUTE #T-INV-ACCT-STATE-TAX-AMT = #T-INV-ACCT-STATE-TAX-AMT + #S-INV-ACCT-STATE-TAX-AMT ( #SUM-I,*,#J )
                    pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,"*",pnd_J));                         //Natural: COMPUTE #T-INV-ACCT-LOCAL-TAX-AMT = #T-INV-ACCT-LOCAL-TAX-AMT + #S-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,*,#J )
                    pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,"*",pnd_J));                         //Natural: COMPUTE #T-INV-ACCT-NET-PYMNT-AMT = #T-INV-ACCT-NET-PYMNT-AMT + #S-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,*,#J )
                    pnd_T_Vars_Pnd_T_Qty.nadd(pnd_Cntrct_Type_Sum_Pnd_Qty.getValue(pnd_Sum_I,"*",pnd_J));                                                                 //Natural: COMPUTE #T-QTY = #T-QTY + #QTY ( #SUM-I,*,#J )
                    if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                    //Natural: IF NOT #INT-ROLL-MODE
                    {
                        getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask               //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ ) /
                            ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                            TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                            TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"),NEWLINE);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(3, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask               //Natural: WRITE ( PRT2 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ ) /
                            ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                            TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                            TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"),NEWLINE);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_T_Vars.reset();                                                                                                                                   //Natural: RESET #T-VARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,"*","*"));                                           //Natural: COMPUTE #T-INV-ACCT-SETTL-AMT = #T-INV-ACCT-SETTL-AMT + #S-INV-ACCT-SETTL-AMT ( #SUM-I,*,* )
            pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,"*","*"));                                               //Natural: COMPUTE #T-INV-ACCT-EXP-AMT = #T-INV-ACCT-EXP-AMT + #S-INV-ACCT-EXP-AMT ( #SUM-I,*,* )
            pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,"*","*"));                                               //Natural: COMPUTE #T-INV-ACCT-DPI-AMT = #T-INV-ACCT-DPI-AMT + #S-INV-ACCT-DPI-AMT ( #SUM-I,*,* )
            pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,"*","*"));                                     //Natural: COMPUTE #T-INV-ACCT-FDRL-TAX-AMT = #T-INV-ACCT-FDRL-TAX-AMT + #S-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,*,* )
            pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,"*","*"));                                   //Natural: COMPUTE #T-INV-ACCT-STATE-TAX-AMT = #T-INV-ACCT-STATE-TAX-AMT + #S-INV-ACCT-STATE-TAX-AMT ( #SUM-I,*,* )
            pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,"*","*"));                                   //Natural: COMPUTE #T-INV-ACCT-LOCAL-TAX-AMT = #T-INV-ACCT-LOCAL-TAX-AMT + #S-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,*,* )
            pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Cntrct_Type_Sum_Pnd_S_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,"*","*"));                                   //Natural: COMPUTE #T-INV-ACCT-NET-PYMNT-AMT = #T-INV-ACCT-NET-PYMNT-AMT + #S-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,*,* )
            pnd_T_Vars_Pnd_T_Qty.nadd(pnd_Cntrct_Type_Sum_Pnd_Qty.getValue(pnd_Sum_I,"*","*"));                                                                           //Natural: COMPUTE #T-QTY = #T-QTY + #QTY ( #SUM-I,*,* )
            if (condition(pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("L")))                                                                                           //Natural: IF #CNTRCT-TYPE-CDE ( #SUM-I ) = 'L'
            {
                pnd_Category_Txt.setValue("GRAND TOTAL");                                                                                                                 //Natural: ASSIGN #CATEGORY-TXT = 'GRAND TOTAL'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Category_Txt.setValue("TOTAL");                                                                                                                       //Natural: ASSIGN #CATEGORY-TXT = 'TOTAL'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                            //Natural: IF NOT #INT-ROLL-MODE
            {
                getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new  //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ )
                    TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                    new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                    TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                    new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                    TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(3, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new  //Natural: WRITE ( PRT2 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ )
                    TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                    new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                    TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                    new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                    TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Sub_Det_Prt.getBoolean())))                                                                                                              //Natural: IF NOT #SUB-DET-PRT
            {
                pnd_Sub_Det_Prt.setValue(true);                                                                                                                           //Natural: ASSIGN #SUB-DET-PRT := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pnd_T_Vars.reset();                                                                                                                                           //Natural: RESET #T-VARS
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Summary_Detail() throws Exception                                                                                                              //Natural: PRINT-SUMMARY-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INDEX-RANGE
        sub_Determine_Index_Range();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        if (condition(pnd_Found.getBoolean()))                                                                                                                            //Natural: IF #FOUND
        {
            FOR05:                                                                                                                                                        //Natural: FOR #SUM-I 1 #SUM-I-MAX
            for (pnd_Sum_I.setValue(1); condition(pnd_Sum_I.lessOrEqual(pnd_Sum_I_Max)); pnd_Sum_I.nadd(1))
            {
                if (condition(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,"*","*").greater(getZero())))                                                          //Natural: IF #GT-QTY ( #SUM-I,*,* ) > 0
                {
                    short decideConditionsMet1479 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF #CNTRCT-TYPE-CDE ( #SUM-I );//Natural: VALUE 'C'
                    if (condition((pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("C"))))
                    {
                        decideConditionsMet1479++;
                        getReports().write(2, NEWLINE,"SINGLE SUM CASHOUT PAYMENTS",NEWLINE);                                                                             //Natural: WRITE ( PRT1 ) / 'SINGLE SUM CASHOUT PAYMENTS' /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE 'L'
                    else if (condition((pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("L"))))
                    {
                        decideConditionsMet1479++;
                        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                //Natural: IF NOT #INT-ROLL-MODE
                        {
                            getReports().newPage(new ReportSpecification(0));                                                                                             //Natural: NEWPAGE ( PRT1 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(2, NEWLINE,"SINGLE SUM LUMPSUM PAYMENTS",NEWLINE);                                                                         //Natural: WRITE ( PRT1 ) / 'SINGLE SUM LUMPSUM PAYMENTS' /
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().newPage(new ReportSpecification(0));                                                                                             //Natural: NEWPAGE ( PRT2 )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            getReports().write(3, NEWLINE,"SINGLE SUM INTERNAL ROLLOVERS",NEWLINE);                                                                       //Natural: WRITE ( PRT2 ) / 'SINGLE SUM INTERNAL ROLLOVERS' /
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 'R'
                    else if (condition((pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("R"))))
                    {
                        decideConditionsMet1479++;
                        getReports().write(2, NEWLINE,"SINGLE SUM REPURCHASE PAYMENTS",NEWLINE);                                                                          //Natural: WRITE ( PRT1 ) / 'SINGLE SUM REPURCHASE PAYMENTS' /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: VALUE '30', '31'
                    else if (condition((pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("30") || pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("31"))))
                    {
                        decideConditionsMet1479++;
                        //* * PRINT SINGLE SUM GRAND TOTALS BEFORE PRINTING MOD TOTALS
                        if (condition(pnd_Cntrct_Type_Cde.getValue("*").equals("C") || pnd_Cntrct_Type_Cde.getValue("*").equals("L") || pnd_Cntrct_Type_Cde.getValue("*").equals("R"))) //Natural: IF #CNTRCT-TYPE-CDE ( * ) = 'C' OR = 'L' OR = 'R'
                        {
                                                                                                                                                                          //Natural: PERFORM CREATE-AND-PRINT-SS-TOTALS
                            sub_Create_And_Print_Ss_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"MINIMUM DISTRIBUTION OPTION PAYMENTS",NEWLINE);                                                    //Natural: WRITE ( PRT1 ) /// 'MINIMUM DISTRIBUTION OPTION PAYMENTS' /
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    FOR06:                                                                                                                                                //Natural: FOR #I #I-START #I-END
                    for (pnd_I.setValue(pnd_I_Start); condition(pnd_I.lessOrEqual(pnd_I_End)); pnd_I.nadd(1))
                    {
                        if (condition(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,pnd_I,"*").greater(getZero())))                                                //Natural: IF #GT-QTY ( #SUM-I,#I,* ) > 0
                        {
                            FOR07:                                                                                                                                        //Natural: FOR #J #J-START #J-END
                            for (pnd_J.setValue(pnd_J_Start); condition(pnd_J.lessOrEqual(pnd_J_End)); pnd_J.nadd(1))
                            {
                                if (condition(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,"*",pnd_J).greater(getZero())))                                        //Natural: IF #GT-QTY ( #SUM-I,*,#J ) > 0
                                {
                                    //*  PRINT CHECK/EFT/GLB/SUB AMOUNTS FOR SGRA / GSRA, IRA , RA OR MDO
                                                                                                                                                                          //Natural: PERFORM CATEGORY-TXT
                                    sub_Category_Txt();
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    if (condition(Map.getDoInput())) {return;}
                                    //*  SS
                                    if (condition(! (pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("30") || pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("31"))))      //Natural: IF NOT ( #CNTRCT-TYPE-CDE ( #SUM-I ) = '30' OR = '31' )
                                    {
                                                                                                                                                                          //Natural: PERFORM COMPRESS-CATEGORY-TXT
                                        sub_Compress_Category_Txt();
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom())) break;
                                            else if (condition(Global.isEscapeBottomImmediate())) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                        if (condition(Map.getDoInput())) {return;}
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        pnd_Category_Txt.setValue(DbsUtil.compress("MDO", pnd_Category_Txt));                                                             //Natural: COMPRESS 'MDO' #CATEGORY-TXT INTO #CATEGORY-TXT
                                    }                                                                                                                                     //Natural: END-IF
                                    if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                    //Natural: IF NOT #INT-ROLL-MODE
                                    {
                                        getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J),  //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #GT-INV-ACCT-SETTL-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #GT-INV-ACCT-EXP-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 045T #GT-INV-ACCT-DPI-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 055T #GT-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #GT-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #GT-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 103T #GT-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #GT-QTY ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZ ) /
                                            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(122),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZZZZZ"),NEWLINE);
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom())) break;
                                            else if (condition(Global.isEscapeBottomImmediate())) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                    }                                                                                                                                     //Natural: ELSE
                                    else if (condition())
                                    {
                                        getReports().write(3, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J),  //Natural: WRITE ( PRT2 ) 001T #CATEGORY-TXT 016T #GT-INV-ACCT-SETTL-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #GT-INV-ACCT-EXP-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 045T #GT-INV-ACCT-DPI-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 055T #GT-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #GT-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #GT-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZ9.99 ) 103T #GT-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #GT-QTY ( #SUM-I,#I,#J ) ( EM = ZZZZZZZZZ ) /
                                            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(122),pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,pnd_I,pnd_J), 
                                            new ReportEditMask ("ZZZZZZZZZ"),NEWLINE);
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom())) break;
                                            else if (condition(Global.isEscapeBottomImmediate())) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                    }                                                                                                                                     //Natural: END-IF
                                    //*  CALCULATE GRA / (G)SRA/IRA / GLB / INT OR MDO TOTALS
                                    pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt.getValue(pnd_Sum_I,pnd_I,                   //Natural: COMPUTE #T-INV-ACCT-SETTL-AMT = #T-INV-ACCT-SETTL-AMT + #GT-INV-ACCT-SETTL-AMT ( #SUM-I,#I,#J )
                                        pnd_J));
                                    pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J));               //Natural: COMPUTE #T-INV-ACCT-EXP-AMT = #T-INV-ACCT-EXP-AMT + #GT-INV-ACCT-EXP-AMT ( #SUM-I,#I,#J )
                                    pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt.getValue(pnd_Sum_I,pnd_I,pnd_J));               //Natural: COMPUTE #T-INV-ACCT-DPI-AMT = #T-INV-ACCT-DPI-AMT + #GT-INV-ACCT-DPI-AMT ( #SUM-I,#I,#J )
                                    pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Sum_I,                   //Natural: COMPUTE #T-INV-ACCT-FDRL-TAX-AMT = #T-INV-ACCT-FDRL-TAX-AMT + #GT-INV-ACCT-FDRL-TAX-AMT ( #SUM-I,#I,#J )
                                        pnd_I,pnd_J));
                                    pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt.getValue(pnd_Sum_I,                 //Natural: COMPUTE #T-INV-ACCT-STATE-TAX-AMT = #T-INV-ACCT-STATE-TAX-AMT + #GT-INV-ACCT-STATE-TAX-AMT ( #SUM-I,#I,#J )
                                        pnd_I,pnd_J));
                                    pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt.getValue(pnd_Sum_I,                 //Natural: COMPUTE #T-INV-ACCT-LOCAL-TAX-AMT = #T-INV-ACCT-LOCAL-TAX-AMT + #GT-INV-ACCT-LOCAL-TAX-AMT ( #SUM-I,#I,#J )
                                        pnd_I,pnd_J));
                                    pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Sum_I,                 //Natural: COMPUTE #T-INV-ACCT-NET-PYMNT-AMT = #T-INV-ACCT-NET-PYMNT-AMT + #GT-INV-ACCT-NET-PYMNT-AMT ( #SUM-I,#I,#J )
                                        pnd_I,pnd_J));
                                    pnd_T_Vars_Pnd_T_Qty.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_Sum_I,pnd_I,pnd_J));                                         //Natural: COMPUTE #T-QTY = #T-QTY + #GT-QTY ( #SUM-I,#I,#J )
                                }                                                                                                                                         //Natural: END-IF
                                //*  FOR #J
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            //*  PRINT GRA / SRA/GSRA / IRA / GLB / INT OR MDO TOTALS
                            //*  SS
                            if (condition(! (pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("30") || pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("31"))))              //Natural: IF NOT ( #CNTRCT-TYPE-CDE ( #SUM-I ) = '30' OR = '31' )
                            {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-CATEGORY-TXT
                                sub_Initialize_Category_Txt();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                                {
                                    getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new                  //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZ ) / /
                                        ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new 
                                        TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask 
                                        ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, new ReportEditMask ("ZZZZZ9.99"),new 
                                        TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(122),pnd_T_Vars_Pnd_T_Qty, 
                                        new ReportEditMask ("ZZZZZ"),NEWLINE,NEWLINE);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    //*              RESET #T-VARS
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    getReports().write(3, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new                  //Natural: WRITE ( PRT2 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZ ) / /
                                        ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new 
                                        TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask 
                                        ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, new ReportEditMask ("ZZZZZ9.99"),new 
                                        TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(122),pnd_T_Vars_Pnd_T_Qty, 
                                        new ReportEditMask ("ZZZZZ"),NEWLINE,NEWLINE);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                //*  MDO
                                if (condition(pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("30") || pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("31")))              //Natural: IF #CNTRCT-TYPE-CDE ( #SUM-I ) = '30' OR = '31'
                                {
                                    pnd_Category_Txt.setValue("MDO TOTAL     ");                                                                                          //Natural: ASSIGN #CATEGORY-TXT := 'MDO TOTAL     '
                                    getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new                  //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZ ) / /
                                        ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new 
                                        TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, 
                                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask 
                                        ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, new ReportEditMask ("ZZZZZ9.99"),new 
                                        TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(122),pnd_T_Vars_Pnd_T_Qty, 
                                        new ReportEditMask ("ZZZZZ"),NEWLINE,NEWLINE);
                                    if (condition(Global.isEscape()))
                                    {
                                        if (condition(Global.isEscapeBottom())) break;
                                        else if (condition(Global.isEscapeBottomImmediate())) break;
                                        else if (condition(Global.isEscapeTop())) continue;
                                        else if (condition(Global.isEscapeRoutine())) return;
                                        else break;
                                    }
                                    //*  PRINT MISCELLANEOUS GRAND TOTALS
                                    if (condition(pnd_Misc_Grand_Totals_Pnd_Misc_Qty.getValue("*","*").notEquals(getZero())))                                             //Natural: IF #MISC-QTY ( *,* ) NE 0
                                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-MISCELLANEOUS-TOTALS
                                        sub_Print_Miscellaneous_Totals();
                                        if (condition(Global.isEscape()))
                                        {
                                            if (condition(Global.isEscapeBottom())) break;
                                            else if (condition(Global.isEscapeBottomImmediate())) break;
                                            else if (condition(Global.isEscapeTop())) continue;
                                            else if (condition(Global.isEscapeRoutine())) return;
                                            else break;
                                        }
                                        if (condition(Map.getDoInput())) {return;}
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            pnd_T_Vars.reset();                                                                                                                           //Natural: RESET #T-VARS
                        }                                                                                                                                                 //Natural: END-IF
                        //*  FOR #I
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  FOR #SUM-I
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  ONLY SS RECORD FOUND
        if (condition((((pnd_Cntrct_Type_Cde.getValue("*").equals("C") || pnd_Cntrct_Type_Cde.getValue("*").equals("L")) || pnd_Cntrct_Type_Cde.getValue("*").equals("R"))  //Natural: IF ( #CNTRCT-TYPE-CDE ( * ) = 'C' OR = 'L' OR = 'R' ) AND NOT ( #CNTRCT-TYPE-CDE ( * ) = '30' OR = '31' )
            && ! ((pnd_Cntrct_Type_Cde.getValue("*").equals("30") || pnd_Cntrct_Type_Cde.getValue("*").equals("31"))))))
        {
            //* * PRINT SINGLE SUM GRAND TOTALS
                                                                                                                                                                          //Natural: PERFORM CREATE-AND-PRINT-SS-TOTALS
            sub_Create_And_Print_Ss_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  PRINT MISCELLANEOUS GRAND TOTALS
            if (condition(pnd_Misc_Grand_Totals_Pnd_Misc_Qty.getValue("*","*").notEquals(getZero())))                                                                     //Natural: IF #MISC-QTY ( *,* ) NE 0
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-MISCELLANEOUS-TOTALS
                sub_Print_Miscellaneous_Totals();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Create_And_Print_Ss_Totals() throws Exception                                                                                                        //Natural: CREATE-AND-PRINT-SS-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //*  CALCULATE & PRINT CHECK / EFT / GLB / INT TOTALS FOR SINGLE SUM
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().write(2, NEWLINE,"SINGLE SUM PAYMENTS",NEWLINE);                                                                                                 //Natural: WRITE ( PRT1 ) / 'SINGLE SUM PAYMENTS' /
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, NEWLINE,"SINGLE SUM INTERNAL ROLLOVERS",NEWLINE);                                                                                       //Natural: WRITE ( PRT2 ) / 'SINGLE SUM INTERNAL ROLLOVERS' /
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INDEX-RANGE
        sub_Determine_Index_Range();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        FOR08:                                                                                                                                                            //Natural: FOR #J #J-START #J-END
        for (pnd_J.setValue(pnd_J_Start); condition(pnd_J.lessOrEqual(pnd_J_End)); pnd_J.nadd(1))
        {
            if (condition(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue("*","*",pnd_J).greater(getZero())))                                                                  //Natural: IF #GT-QTY ( *,*,#J ) > 0
            {
                FOR09:                                                                                                                                                    //Natural: FOR #X 1 #SUM-I-MAX
                for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Sum_I_Max)); pnd_X.nadd(1))
                {
                    //*  FOR SS ONLY
                    if (condition((pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_X,"*","*").greater(getZero()) && ! ((pnd_Cntrct_Type_Cde.getValue(pnd_X).equals("30")   //Natural: IF #GT-QTY ( #X,*,* ) > 0 AND NOT ( #CNTRCT-TYPE-CDE ( #X ) = '30' OR = '31' )
                        || pnd_Cntrct_Type_Cde.getValue(pnd_X).equals("31"))))))
                    {
                        pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt.getValue(pnd_X,"*",pnd_J));                             //Natural: COMPUTE #T-INV-ACCT-SETTL-AMT = #T-INV-ACCT-SETTL-AMT + #GT-INV-ACCT-SETTL-AMT ( #X,*,#J )
                        pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt.getValue(pnd_X,"*",pnd_J));                                 //Natural: COMPUTE #T-INV-ACCT-EXP-AMT = #T-INV-ACCT-EXP-AMT + #GT-INV-ACCT-EXP-AMT ( #X,*,#J )
                        pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt.getValue(pnd_X,"*",pnd_J));                                 //Natural: COMPUTE #T-INV-ACCT-DPI-AMT = #T-INV-ACCT-DPI-AMT + #GT-INV-ACCT-DPI-AMT ( #X,*,#J )
                        pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_X,"*",pnd_J));                       //Natural: COMPUTE #T-INV-ACCT-FDRL-TAX-AMT = #T-INV-ACCT-FDRL-TAX-AMT + #GT-INV-ACCT-FDRL-TAX-AMT ( #X,*,#J )
                        pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt.getValue(pnd_X,"*",pnd_J));                     //Natural: COMPUTE #T-INV-ACCT-STATE-TAX-AMT = #T-INV-ACCT-STATE-TAX-AMT + #GT-INV-ACCT-STATE-TAX-AMT ( #X,*,#J )
                        pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt.getValue(pnd_X,"*",pnd_J));                     //Natural: COMPUTE #T-INV-ACCT-LOCAL-TAX-AMT = #T-INV-ACCT-LOCAL-TAX-AMT + #GT-INV-ACCT-LOCAL-TAX-AMT ( #X,*,#J )
                        pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_X,"*",pnd_J));                     //Natural: COMPUTE #T-INV-ACCT-NET-PYMNT-AMT = #T-INV-ACCT-NET-PYMNT-AMT + #GT-INV-ACCT-NET-PYMNT-AMT ( #X,*,#J )
                        pnd_T_Vars_Pnd_T_Qty.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_X,"*",pnd_J));                                                           //Natural: COMPUTE #T-QTY = #T-QTY + #GT-QTY ( #X,*,#J )
                    }                                                                                                                                                     //Natural: END-IF
                    //*  FOR #X
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM CATEGORY-TXT
                sub_Category_Txt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    pnd_Category_Txt.setValue(DbsUtil.compress("SINGLE SUM", pnd_Category_Txt));                                                                          //Natural: COMPRESS 'SINGLE SUM' #CATEGORY-TXT INTO #CATEGORY-TXT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Category_Txt.setValue(DbsUtil.compress("INTERNAL", pnd_Category_Txt));                                                                            //Natural: COMPRESS 'INTERNAL' #CATEGORY-TXT INTO #CATEGORY-TXT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask                   //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ ) /
                        ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                        TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                        TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"),NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(3, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask                   //Natural: WRITE ( PRT2 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ ) /
                        ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                        TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                        TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"),NEWLINE);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_T_Vars.reset();                                                                                                                                       //Natural: RESET #T-VARS
            }                                                                                                                                                             //Natural: END-IF
            //*  FOR #J
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR10:                                                                                                                                                            //Natural: FOR #X 1 #SUM-I-MAX
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(pnd_Sum_I_Max)); pnd_X.nadd(1))
        {
            //*  FOR SS ONLY
            if (condition((pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_X,"*","*").greater(getZero()) && ! ((pnd_Cntrct_Type_Cde.getValue(pnd_X).equals("30")           //Natural: IF #GT-QTY ( #X,*,* ) > 0 AND NOT ( #CNTRCT-TYPE-CDE ( #X ) = '30' OR = '31' )
                || pnd_Cntrct_Type_Cde.getValue(pnd_X).equals("31"))))))
            {
                pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Settl_Amt.getValue(pnd_X,"*","*"));                                       //Natural: COMPUTE #T-INV-ACCT-SETTL-AMT = #T-INV-ACCT-SETTL-AMT + #GT-INV-ACCT-SETTL-AMT ( #X,*,* )
                pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Exp_Amt.getValue(pnd_X,"*","*"));                                           //Natural: COMPUTE #T-INV-ACCT-EXP-AMT = #T-INV-ACCT-EXP-AMT + #GT-INV-ACCT-EXP-AMT ( #X,*,* )
                pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Dpi_Amt.getValue(pnd_X,"*","*"));                                           //Natural: COMPUTE #T-INV-ACCT-DPI-AMT = #T-INV-ACCT-DPI-AMT + #GT-INV-ACCT-DPI-AMT ( #X,*,* )
                pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_X,"*","*"));                                 //Natural: COMPUTE #T-INV-ACCT-FDRL-TAX-AMT = #T-INV-ACCT-FDRL-TAX-AMT + #GT-INV-ACCT-FDRL-TAX-AMT ( #X,*,* )
                pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_State_Tax_Amt.getValue(pnd_X,"*","*"));                               //Natural: COMPUTE #T-INV-ACCT-STATE-TAX-AMT = #T-INV-ACCT-STATE-TAX-AMT + #GT-INV-ACCT-STATE-TAX-AMT ( #X,*,* )
                pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Local_Tax_Amt.getValue(pnd_X,"*","*"));                               //Natural: COMPUTE #T-INV-ACCT-LOCAL-TAX-AMT = #T-INV-ACCT-LOCAL-TAX-AMT + #GT-INV-ACCT-LOCAL-TAX-AMT ( #X,*,* )
                pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_X,"*","*"));                               //Natural: COMPUTE #T-INV-ACCT-NET-PYMNT-AMT = #T-INV-ACCT-NET-PYMNT-AMT + #GT-INV-ACCT-NET-PYMNT-AMT ( #X,*,* )
                pnd_T_Vars_Pnd_T_Qty.nadd(pnd_Gt_Cntrct_Type_Sum_Pnd_Gt_Qty.getValue(pnd_X,"*","*"));                                                                     //Natural: COMPUTE #T-QTY = #T-QTY + #GT-QTY ( #X,*,* )
            }                                                                                                                                                             //Natural: END-IF
            //*  FOR #SUM-I
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            pnd_Category_Txt.setValue("SINGLE SUM TOT");                                                                                                                  //Natural: ASSIGN #CATEGORY-TXT := 'SINGLE SUM TOT'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Category_Txt.setValue("INT ROLL TOTAL");                                                                                                                  //Natural: ASSIGN #CATEGORY-TXT := 'INT ROLL TOTAL'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().write(2, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new  //Natural: WRITE ( PRT1 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ )
                TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, new TabSetting(1),pnd_Category_Txt,new TabSetting(16),pnd_T_Vars_Pnd_T_Inv_Acct_Settl_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new  //Natural: WRITE ( PRT2 ) 001T #CATEGORY-TXT 016T #T-INV-ACCT-SETTL-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #T-INV-ACCT-EXP-AMT ( EM = ZZZZZ9.99 ) 045T #T-INV-ACCT-DPI-AMT ( EM = ZZZZZ9.99 ) 055T #T-INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #T-INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #T-INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZ9.99 ) 103T #T-INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 122T #T-QTY ( EM = ZZZZZZZZZ )
                TabSetting(35),pnd_T_Vars_Pnd_T_Inv_Acct_Exp_Amt, new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_T_Vars_Pnd_T_Inv_Acct_Dpi_Amt, 
                new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_T_Vars_Pnd_T_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                TabSetting(74),pnd_T_Vars_Pnd_T_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_T_Vars_Pnd_T_Inv_Acct_Local_Tax_Amt, 
                new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_T_Vars_Pnd_T_Inv_Acct_Net_Pymnt_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                TabSetting(122),pnd_T_Vars_Pnd_T_Qty, new ReportEditMask ("ZZZZZZZZZ"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pnd_T_Vars.reset();                                                                                                                                               //Natural: RESET #T-VARS
    }
    private void sub_Print_Miscellaneous_Totals() throws Exception                                                                                                        //Natural: PRINT-MISCELLANEOUS-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR11:                                                                                                                                                            //Natural: FOR #Z 1 #MISC-MAX-OCCURS
        for (pnd_Z.setValue(1); condition(pnd_Z.lessOrEqual(pnd_Misc_Grand_Totals_Pnd_Misc_Max_Occurs)); pnd_Z.nadd(1))
        {
            if (condition(pnd_Misc_Grand_Totals_Pnd_Misc_Qty.getValue("*",pnd_Z).notEquals(getZero())))                                                                   //Natural: IF #MISC-QTY ( *,#Z ) NE 0
            {
                short decideConditionsMet1676 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #Z;//Natural: VALUE 1
                if (condition((pnd_Z.equals(1))))
                {
                    decideConditionsMet1676++;
                    pnd_Misc_Grand_Totals_Pnd_Misc_Category_Hdr.setValue("DIRECT TRANSFER PAYMENTS ");                                                                    //Natural: ASSIGN #MISC-CATEGORY-HDR := 'DIRECT TRANSFER PAYMENTS '
                    pnd_Misc_Grand_Totals_Pnd_Misc_Category_Txt.setValue("DIR TRAN TOTAL");                                                                               //Natural: ASSIGN #MISC-CATEGORY-TXT := 'DIR TRAN TOTAL'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    if (condition(getReports().getAstLinesLeft(0).less(5)))                                                                                               //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 5 LINES
                    {
                        getReports().newPage(0);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }
                    getReports().write(2, NEWLINE,NEWLINE,NEWLINE,pnd_Misc_Grand_Totals_Pnd_Misc_Category_Hdr,NEWLINE,NEWLINE,new TabSetting(1),pnd_Misc_Grand_Totals_Pnd_Misc_Category_Txt,new  //Natural: WRITE ( PRT1 ) /// #MISC-CATEGORY-HDR // 001T #MISC-CATEGORY-TXT 016T #MISC-INV-ACCT-SETTL-AMT ( 1,#Z ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #MISC-INV-ACCT-EXP-AMT ( 1,#Z ) ( EM = ZZZZZ9.99 ) 045T #MISC-INV-ACCT-DPI-AMT ( 1,#Z ) ( EM = ZZZZZ9.99 ) 055T #MISC-INV-ACCT-FDRL-TAX-AMT ( 1,#Z ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #MISC-INV-ACCT-STATE-TAX-AMT ( 1,#Z ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #MISC-INV-ACCT-LOCAL-TAX-AMT ( 1,#Z ) ( EM = ZZZZZ9.99 ) 103T #MISC-INV-ACCT-NET-PYMNT-AMT ( 1,#Z ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 126T #MISC-QTY ( 1,#Z ) ( EM = ZZZZ9 )
                        TabSetting(16),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Settl_Amt.getValue(1,pnd_Z), new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                        TabSetting(35),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Exp_Amt.getValue(1,pnd_Z), new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Dpi_Amt.getValue(1,pnd_Z), 
                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Fdrl_Tax_Amt.getValue(1,pnd_Z), new 
                        ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_State_Tax_Amt.getValue(1,pnd_Z), 
                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Local_Tax_Amt.getValue(1,pnd_Z), 
                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Net_Pymnt_Amt.getValue(1,pnd_Z), new 
                        ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(126),pnd_Misc_Grand_Totals_Pnd_Misc_Qty.getValue(1,pnd_Z), new ReportEditMask 
                        ("ZZZZ9"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(getReports().getAstLinesLeft(0).less(5)))                                                                                               //Natural: NEWPAGE ( PRT2 ) IF LESS THAN 5 LINES
                    {
                        getReports().newPage(0);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }
                    getReports().write(3, NEWLINE,NEWLINE,NEWLINE,pnd_Misc_Grand_Totals_Pnd_Misc_Category_Hdr,NEWLINE,NEWLINE,new TabSetting(1),pnd_Misc_Grand_Totals_Pnd_Misc_Category_Txt,new  //Natural: WRITE ( PRT2 ) /// #MISC-CATEGORY-HDR // 001T #MISC-CATEGORY-TXT 016T #MISC-INV-ACCT-SETTL-AMT ( 2,#Z ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 035T #MISC-INV-ACCT-EXP-AMT ( 2,#Z ) ( EM = ZZZZZ9.99 ) 045T #MISC-INV-ACCT-DPI-AMT ( 2,#Z ) ( EM = ZZZZZ9.99 ) 055T #MISC-INV-ACCT-FDRL-TAX-AMT ( 2,#Z ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 074T #MISC-INV-ACCT-STATE-TAX-AMT ( 2,#Z ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 093T #MISC-INV-ACCT-LOCAL-TAX-AMT ( 2,#Z ) ( EM = ZZZZZ9.99 ) 103T #MISC-INV-ACCT-NET-PYMNT-AMT ( 2,#Z ) ( EM = ZZZZZZZZZZZZZZ9.99 ) 126T #MISC-QTY ( 2,#Z ) ( EM = ZZZZ9 )
                        TabSetting(16),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Settl_Amt.getValue(2,pnd_Z), new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new 
                        TabSetting(35),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Exp_Amt.getValue(2,pnd_Z), new ReportEditMask ("ZZZZZ9.99"),new TabSetting(45),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Dpi_Amt.getValue(2,pnd_Z), 
                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(55),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Fdrl_Tax_Amt.getValue(2,pnd_Z), new 
                        ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(74),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_State_Tax_Amt.getValue(2,pnd_Z), 
                        new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(93),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Local_Tax_Amt.getValue(2,pnd_Z), 
                        new ReportEditMask ("ZZZZZ9.99"),new TabSetting(103),pnd_Misc_Grand_Totals_Pnd_Misc_Inv_Acct_Net_Pymnt_Amt.getValue(2,pnd_Z), new 
                        ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(126),pnd_Misc_Grand_Totals_Pnd_Misc_Qty.getValue(2,pnd_Z), new ReportEditMask 
                        ("ZZZZ9"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Determine_Header1_2() throws Exception                                                                                                               //Natural: DETERMINE-HEADER1-2
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pnd_Header1_2.reset();                                                                                                                                            //Natural: RESET #HEADER1-2
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31")))                            //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = '30' OR = '31'
        {
            pnd_Header1_2.setValue(DbsUtil.compress("MINIMUM DISTRIBUTION OPTION", pnd_Header1_2_Tmp));                                                                   //Natural: COMPRESS 'MINIMUM DISTRIBUTION OPTION' #HEADER1-2-TMP INTO #HEADER1-2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                            //Natural: IF NOT #INT-ROLL-MODE
            {
                pnd_Header1_2.setValue(DbsUtil.compress("SINGLE SUM", pnd_Header1_2_Tmp));                                                                                //Natural: COMPRESS 'SINGLE SUM' #HEADER1-2-TMP INTO #HEADER1-2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Header1_2.setValue(DbsUtil.compress("SINGLE SUM INTERNAL ROLLOVER", pnd_Header1_2_Tmp));                                                              //Natural: COMPRESS 'SINGLE SUM INTERNAL ROLLOVER' #HEADER1-2-TMP INTO #HEADER1-2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CENTER-HEADER1-2
        sub_Center_Header1_2();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Center_Header1_2() throws Exception                                                                                                                  //Natural: CENTER-HEADER1-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Header1_2.setValue(pnd_Header1_2, MoveOption.LeftJustified);                                                                                                  //Natural: MOVE LEFT #HEADER1-2 TO #HEADER1-2
        pnd_Len.reset();                                                                                                                                                  //Natural: RESET #LEN #W-FLD
        pnd_W_Fld.reset();
        DbsUtil.examine(new ExamineSource(pnd_Header1_2,true), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                                 //Natural: EXAMINE FULL #HEADER1-2 FOR ' ' GIVING LENGTH #LEN
        pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(69,pnd_Len)).divide(2));                                                                //Natural: COMPUTE #LEN = ( 69 - #LEN ) / 2
        pnd_W_Fld.moveAll("*");                                                                                                                                           //Natural: MOVE ALL '*' TO #W-FLD UNTIL #LEN
        pnd_Header1_2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header1_2));                                                                //Natural: COMPRESS #W-FLD #HEADER1-2 INTO #HEADER1-2 LEAVE NO
        DbsUtil.examine(new ExamineSource(pnd_Header1_2), new ExamineSearch("*"), new ExamineReplace(" "));                                                               //Natural: EXAMINE #HEADER1-2 FOR '*' REPLACE ' '
    }
    private void sub_Determine_Header1_W() throws Exception                                                                                                               //Natural: DETERMINE-HEADER1-W
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(! (pnd_End_Report.getBoolean())))                                                                                                                   //Natural: IF NOT #END-REPORT
        {
            short decideConditionsMet1723 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #CNTRCT-TYPE-CDE ( #SUM-I );//Natural: VALUE 'C'
            if (condition((pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("C"))))
            {
                decideConditionsMet1723++;
                pnd_Header1_W.setValue("                      SRA CASHOUT PAYMENT");                                                                                      //Natural: ASSIGN #HEADER1-W := '                      SRA CASHOUT PAYMENT'
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("R"))))
            {
                decideConditionsMet1723++;
                pnd_Header1_W.setValue("                      REPURCHASE PAYMENT");                                                                                       //Natural: ASSIGN #HEADER1-W := '                      REPURCHASE PAYMENT'
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("L"))))
            {
                decideConditionsMet1723++;
                if (condition(! (pnd_Summary.getBoolean())))                                                                                                              //Natural: IF NOT #SUMMARY
                {
                    if (condition(! (pnd_Sub_Total.getBoolean())))                                                                                                        //Natural: IF NOT #SUB-TOTAL
                    {
                        pnd_Lob_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde());                                                                                //Natural: MOVE #RPT-EXT.CNTRCT-LOB-CDE TO #LOB-CDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Lob_Cde.setValue(pnd_Old_Lob_Cde);                                                                                                            //Natural: MOVE #OLD-LOB-CDE TO #LOB-CDE
                    }                                                                                                                                                     //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM OBTAIN-LOB-DESC
                    sub_Obtain_Lob_Desc();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                    //Natural: IF NOT #INT-ROLL-MODE
                    {
                        pnd_Header1_W.setValue("                LUMP SUM PAYMENT SUMMARY");                                                                               //Natural: ASSIGN #HEADER1-W := '                LUMP SUM PAYMENT SUMMARY'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Header1_W.setValue("               INTERNAL ROLLOVER SUMMARY");                                                                               //Natural: ASSIGN #HEADER1-W := '               INTERNAL ROLLOVER SUMMARY'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE '30', '31'
            else if (condition((pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("30") || pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).equals("31"))))
            {
                decideConditionsMet1723++;
                pnd_Lob_Cde.setValue(pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I));                                                                                            //Natural: MOVE #CNTRCT-TYPE-CDE ( #SUM-I ) TO #LOB-CDE
                                                                                                                                                                          //Natural: PERFORM OBTAIN-LOB-DESC
                sub_Obtain_Lob_Desc();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Header1_W.setValue("                         SUMMARY");                                                                                                   //Natural: ASSIGN #HEADER1-W = '                         SUMMARY'
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Obtain_Lob_Desc() throws Exception                                                                                                                   //Natural: OBTAIN-LOB-DESC
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        //*  SPECIAL HANDLING FOR XRA, XRAD
        if (condition(pnd_Lob_Cde.getSubstring(1,1).equals("X")))                                                                                                         //Natural: IF SUBSTRING ( #LOB-CDE,1,1 ) = 'X'
        {
            pnd_Lob_Cde.setValue(pnd_Lob_Cde.getSubstring(2,3));                                                                                                          //Natural: MOVE SUBSTRING ( #LOB-CDE,2,3 ) TO #LOB-CDE
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde().getValue("*"),true), new ExamineSearch(pnd_Lob_Cde, true),                  //Natural: EXAMINE FULL #FCPL236-LOB-CDE ( * ) FOR FULL #LOB-CDE GIVING INDEX IN #LOB-SUB
            new ExamineGivingIndex(pnd_Lob_Sub));
        if (condition(pnd_Lob_Sub.notEquals(getZero())))                                                                                                                  //Natural: IF #LOB-SUB NE 0
        {
            pnd_Header1_W.setValue(ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data().getValue(pnd_Lob_Sub));                                                           //Natural: MOVE #WARRANT-HDR-DATA ( #LOB-SUB ) TO #HEADER1-W
            if (condition(pnd_Int_Roll_Mode.getBoolean()))                                                                                                                //Natural: IF #INT-ROLL-MODE
            {
                DbsUtil.examine(new ExamineSource(pnd_Header1_W,true), new ExamineSearch("WITHDRAWAL PAYMENT", true), new ExamineGivingNumber(pnd_Phrase_Num));           //Natural: EXAMINE FULL #HEADER1-W FOR FULL 'WITHDRAWAL PAYMENT' GIVING NUMBER #PHRASE-NUM
                if (condition(pnd_Phrase_Num.greater(getZero())))                                                                                                         //Natural: IF #PHRASE-NUM > 0
                {
                    DbsUtil.examine(new ExamineSource(pnd_Header1_W,true), new ExamineSearch("WITHDRAWAL PAYMENT", true), new ExamineReplace("INTERNAL ROLLOVER "));      //Natural: EXAMINE FULL #HEADER1-W FOR FULL 'WITHDRAWAL PAYMENT' REPLACE 'INTERNAL ROLLOVER '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Header1_W.setValue(DbsUtil.compress(pnd_Header1_W, "INTERNAL ROLLOVER"));                                                                         //Natural: COMPRESS #HEADER1-W 'INTERNAL ROLLOVER' INTO #HEADER1-W
                }                                                                                                                                                         //Natural: END-IF
                pnd_Phrase_Num.reset();                                                                                                                                   //Natural: RESET #PHRASE-NUM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Header1_W.setValue("                        UNKNOWN");                                                                                                    //Natural: MOVE '                        UNKNOWN' TO #HEADER1-W
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Index_Range() throws Exception                                                                                                             //Natural: DETERMINE-INDEX-RANGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  GRA
        //*  IRA
        //*  CHECK
        //*  EFTS
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            pnd_I_Start.setValue(1);                                                                                                                                      //Natural: ASSIGN #I-START := 1
            pnd_I_End.setValue(4);                                                                                                                                        //Natural: ASSIGN #I-END := 4
            pnd_J_Start.setValue(1);                                                                                                                                      //Natural: ASSIGN #J-START := 1
            pnd_J_End.setValue(2);                                                                                                                                        //Natural: ASSIGN #J-END := 2
            //*  CLASSIC
            //*  ROTH
            //*  INT ROLL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_I_Start.setValue(4);                                                                                                                                      //Natural: ASSIGN #I-START := 4
            pnd_I_End.setValue(5);                                                                                                                                        //Natural: ASSIGN #I-END := 5
            pnd_J_Start.setValue(4);                                                                                                                                      //Natural: ASSIGN #J-START := #J-END := 4
            pnd_J_End.setValue(4);
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET CHECK-NO PREFIX FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HEADER1-W
                    sub_Determine_Header1_W();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(2),  //Natural: WRITE ( PRT1 ) NOTITLE *INIT-USER '-' *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( PRT1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 31T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) / 38T #HEADER1-W
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
                        TabSetting(31),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(38),
                        pnd_Header1_W);
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP ( PRT1 ) 1
                    if (condition(pnd_Summary.getBoolean()))                                                                                                              //Natural: IF #SUMMARY
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-HEADER
                        sub_Print_Summary_Header();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-HEADER
                        sub_Print_Detail_Header();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HEADER1-W
                    sub_Determine_Header1_W();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(3, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),pnd_Header1_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(3),  //Natural: WRITE ( PRT2 ) NOTITLE *INIT-USER '-' *PROGRAM 41T #HEADER1-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( PRT2 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 31T #HEADER1-2 124T *TIMX ( EM = HH':'II' 'AP ) / 38T #HEADER1-W
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
                        TabSetting(31),pnd_Header1_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(38),
                        pnd_Header1_W);
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP ( PRT2 ) 1
                    if (condition(pnd_Summary.getBoolean()))                                                                                                              //Natural: IF #SUMMARY
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-HEADER
                        sub_Print_Summary_Header();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-HEADER
                        sub_Print_Detail_Header();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Intrfce_DteIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Lob_CdeIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().isBreak(endOfData);
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Intrfce_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Lob_CdeIsBreak 
            || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak))
        {
            if (condition(pnd_Input_Parm.equals("RALEDG") && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte().notEquals(readWork01Pymnt_Intrfce_DteOld)))                   //Natural: IF #INPUT-PARM = 'RALEDG' AND #RPT-EXT.PYMNT-INTRFCE-DTE NE OLD ( #RPT-EXT.PYMNT-INTRFCE-DTE )
            {
                pnd_Pymnt_Intrfce_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte());                                                                           //Natural: ASSIGN #PYMNT-INTRFCE-DTE := #RPT-EXT.PYMNT-INTRFCE-DTE
                if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8)) && (pnd_Pay_Type_Req_Ind.notEquals(8))))                                    //Natural: IF ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 8 ) AND ( #PAY-TYPE-REQ-IND NE 8 )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Break_Flags_Pnd_Date_Break.setValue(true);                                                                                                        //Natural: MOVE TRUE TO #DATE-BREAK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Lob_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak))
        {
            if (condition(pnd_Input_Parm.equals("RACASH") && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().notEquals(readWork01Pymnt_Check_DteOld)))                       //Natural: IF #INPUT-PARM = 'RACASH' AND #RPT-EXT.PYMNT-CHECK-DTE NE OLD ( #RPT-EXT.PYMNT-CHECK-DTE )
            {
                pnd_Pymnt_Check_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                                               //Natural: ASSIGN #PYMNT-CHECK-DTE := #RPT-EXT.PYMNT-CHECK-DTE
                if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8)) && (pnd_Pay_Type_Req_Ind.notEquals(8))))                                    //Natural: IF ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 8 ) AND ( #PAY-TYPE-REQ-IND NE 8 )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Break_Flags_Pnd_Date_Break.setValue(true);                                                                                                        //Natural: MOVE TRUE TO #DATE-BREAK
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Lob_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak))
        {
            //*  NO EOD
            if (condition((! ((readWork01Cntrct_Type_CdeOld.equals("30") || readWork01Cntrct_Type_CdeOld.equals("31"))) || (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals(readWork01Cntrct_Type_CdeOld)  //Natural: IF NOT ( OLD ( #RPT-EXT.CNTRCT-TYPE-CDE ) = '30' OR = '31' ) OR ( #RPT-EXT.CNTRCT-TYPE-CDE = OLD ( #RPT-EXT.CNTRCT-TYPE-CDE ) AND #RPT-EXT.CNTRCT-LOB-CDE = OLD ( #RPT-EXT.CNTRCT-LOB-CDE ) )
                && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde().equals(readWork01Cntrct_Lob_CdeOld)))))
            {
                if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8)) && (pnd_Pay_Type_Req_Ind.notEquals(8))))                                    //Natural: IF ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 8 ) AND ( #PAY-TYPE-REQ-IND NE 8 )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Break_Flags_Pnd_Lob_Break.setValue(true);                                                                                                         //Natural: MOVE TRUE TO #LOB-BREAK
                    pnd_Old_Lob_Cde.setValue(readWork01Cntrct_Lob_CdeOld);                                                                                                //Natural: ASSIGN #OLD-LOB-CDE := OLD ( #RPT-EXT.CNTRCT-LOB-CDE )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Type_CdeIsBreak))
        {
            //* PRINT MDO SUB TOT ONCE
            if (condition(! (readWork01Cntrct_Type_CdeOld).equals("30") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals(readWork01Cntrct_Type_CdeOld)))            //Natural: IF NOT OLD ( #RPT-EXT.CNTRCT-TYPE-CDE ) = '30' OR #RPT-EXT.CNTRCT-TYPE-CDE = OLD ( #RPT-EXT.CNTRCT-TYPE-CDE )
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-TOTAL
                sub_Print_Sub_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-TOTAL-DETAIL
                sub_Print_Sub_Total_Detail();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                if (condition(pnd_Break_Flags_Pnd_Date_Break.getBoolean() || pnd_Break_Flags_Pnd_Lob_Break.getBoolean()))                                                 //Natural: IF #DATE-BREAK OR #LOB-BREAK
                {
                    pnd_Break_Flags.reset();                                                                                                                              //Natural: RESET #BREAK-FLAGS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().notEquals(readWork01Cntrct_Type_CdeOld)))                                                          //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE NE OLD ( #RPT-EXT.CNTRCT-TYPE-CDE )
            {
                if (condition(! (pnd_Cntrct_Type_Cde.getValue("*").equals("30") || pnd_Cntrct_Type_Cde.getValue("*").equals("31"))))                                      //Natural: IF NOT ( #CNTRCT-TYPE-CDE ( * ) = '30' OR = '31' )
                {
                    pnd_Sum_I.nadd(1);                                                                                                                                    //Natural: ASSIGN #SUM-I := #SUM-I + 1
                    pnd_Sum_I_Max.setValue(pnd_Sum_I);                                                                                                                    //Natural: ASSIGN #SUM-I-MAX := #SUM-I
                    pnd_Cntrct_Type_Cde.getValue(pnd_Sum_I).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde());                                                       //Natural: ASSIGN #CNTRCT-TYPE-CDE ( #SUM-I ) := #RPT-EXT.CNTRCT-TYPE-CDE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pymnt_Check_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                                               //Natural: ASSIGN #PYMNT-CHECK-DTE := #RPT-EXT.PYMNT-CHECK-DTE
                pnd_Pymnt_Intrfce_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte());                                                                           //Natural: ASSIGN #PYMNT-INTRFCE-DTE := #RPT-EXT.PYMNT-INTRFCE-DTE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-HEADER1-2
                sub_Determine_Header1_2();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    getReports().newPage(new ReportSpecification(0));                                                                                                     //Natural: NEWPAGE ( PRT1 )
                    if (condition(Global.isEscape())){return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=134 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(3, "LS=134 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
    private void CheckAtStartofData679() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                                                 //Natural: IF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND EQ 8
            {
                pnd_Int_Roll_Mode.setValue(true);                                                                                                                         //Natural: ASSIGN #INT-ROLL-MODE := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
