/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:56 PM
**        * FROM NATURAL PROGRAM : Fcpp805
************************************************************
**        * FILE NAME            : Fcpp805.java
**        * CLASS NAME           : Fcpp805
**        * INSTANCE NAME        : Fcpp805
************************************************************
************************************************************************
*
* PROGRAM   : FCPP805
*
* SYSTEM    : CPS
* TITLE     : INTERNAL ROLLOVER REPORT BY SOCIAL SECURITY NUMBER
* GENERATED :
*
* FUNCTION  : GENERATE A REPORT OF ALL INTERNAL ROLLOVER CONTRACTS
*           : SORTED BY SECURITY NUMBER.
*           :
* HISTORY   : 04/06/01 MARIA ACLAN
*           : ADDED CONTROL TOTALS FOR TPA/IPRO/P&I
*           : 05/02/03
*           : RESTOW. FCPA800 EXPANDED
* 05/06/08 : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp805 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpacntl pdaFcpacntl;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Index;
    private DbsField pnd_Cntrct_Amt;
    private DbsField pnd_Dvdnd_Amt;
    private DbsField pnd_Gross_Amt;
    private DbsField pnd_Settlmnt_Amt;
    private DbsField pnd_T_Cntrct_Amt;
    private DbsField pnd_T_Dvdnd_Amt;
    private DbsField pnd_T_Gross_Amt;
    private DbsField pnd_T_Cref_Amt;
    private DbsField pnd_T_Roll_Ct;
    private DbsField pnd_C_Cntrct_Amt;
    private DbsField pnd_C_Dvdnd_Amt;
    private DbsField pnd_C_Gross_Amt;
    private DbsField pnd_C_Cref_Amt;
    private DbsField pnd_C_Roll_Ct;
    private DbsField pnd_Total_T_Cntrct_Amt;
    private DbsField pnd_Total_T_Dvdnd_Amt;
    private DbsField pnd_Total_T_Gross_Amt;
    private DbsField pnd_Total_T_Cref_Amt;
    private DbsField pnd_Total_T_Roll_Ct;
    private DbsField pnd_Total_C_Cntrct_Amt;
    private DbsField pnd_Total_C_Dvdnd_Amt;
    private DbsField pnd_Total_C_Gross_Amt;
    private DbsField pnd_Total_C_Cref_Amt;
    private DbsField pnd_Total_C_Roll_Ct;
    private DbsField pnd_Total_Cntrct_Amt;
    private DbsField pnd_Total_Dvdnd_Amt;
    private DbsField pnd_Total_Gross_Amt;
    private DbsField pnd_Total_Cref_Amt;
    private DbsField pnd_Total_Roll_Ct;
    private DbsField pnd_Grp_Optn_Code;
    private DbsField pnd_Prt_Header_Name;
    private DbsField pnd_Ndx;
    private DbsField pnd_A;
    private DbsField pnd_Prt_Total_Name;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpacntl = new PdaFcpacntl(localVariables);

        // Local Variables
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Cntrct_Amt = localVariables.newFieldInRecord("pnd_Cntrct_Amt", "#CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Dvdnd_Amt", "#DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Gross_Amt = localVariables.newFieldInRecord("pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Settlmnt_Amt = localVariables.newFieldInRecord("pnd_Settlmnt_Amt", "#SETTLMNT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Cntrct_Amt = localVariables.newFieldInRecord("pnd_T_Cntrct_Amt", "#T-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_T_Dvdnd_Amt", "#T-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Gross_Amt = localVariables.newFieldInRecord("pnd_T_Gross_Amt", "#T-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Cref_Amt = localVariables.newFieldInRecord("pnd_T_Cref_Amt", "#T-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Roll_Ct = localVariables.newFieldInRecord("pnd_T_Roll_Ct", "#T-ROLL-CT", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Cntrct_Amt = localVariables.newFieldInRecord("pnd_C_Cntrct_Amt", "#C-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_C_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_C_Dvdnd_Amt", "#C-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_C_Gross_Amt = localVariables.newFieldInRecord("pnd_C_Gross_Amt", "#C-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_C_Cref_Amt = localVariables.newFieldInRecord("pnd_C_Cref_Amt", "#C-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_C_Roll_Ct = localVariables.newFieldInRecord("pnd_C_Roll_Ct", "#C-ROLL-CT", FieldType.PACKED_DECIMAL, 7);
        pnd_Total_T_Cntrct_Amt = localVariables.newFieldArrayInRecord("pnd_Total_T_Cntrct_Amt", "#TOTAL-T-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 5));
        pnd_Total_T_Dvdnd_Amt = localVariables.newFieldArrayInRecord("pnd_Total_T_Dvdnd_Amt", "#TOTAL-T-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Total_T_Gross_Amt = localVariables.newFieldArrayInRecord("pnd_Total_T_Gross_Amt", "#TOTAL-T-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Total_T_Cref_Amt = localVariables.newFieldArrayInRecord("pnd_Total_T_Cref_Amt", "#TOTAL-T-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Total_T_Roll_Ct = localVariables.newFieldArrayInRecord("pnd_Total_T_Roll_Ct", "#TOTAL-T-ROLL-CT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            5));
        pnd_Total_C_Cntrct_Amt = localVariables.newFieldArrayInRecord("pnd_Total_C_Cntrct_Amt", "#TOTAL-C-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 5));
        pnd_Total_C_Dvdnd_Amt = localVariables.newFieldArrayInRecord("pnd_Total_C_Dvdnd_Amt", "#TOTAL-C-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Total_C_Gross_Amt = localVariables.newFieldArrayInRecord("pnd_Total_C_Gross_Amt", "#TOTAL-C-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2, new 
            DbsArrayController(1, 5));
        pnd_Total_C_Cref_Amt = localVariables.newFieldArrayInRecord("pnd_Total_C_Cref_Amt", "#TOTAL-C-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 
            5));
        pnd_Total_C_Roll_Ct = localVariables.newFieldArrayInRecord("pnd_Total_C_Roll_Ct", "#TOTAL-C-ROLL-CT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            5));
        pnd_Total_Cntrct_Amt = localVariables.newFieldInRecord("pnd_Total_Cntrct_Amt", "#TOTAL-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Total_Dvdnd_Amt", "#TOTAL-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Gross_Amt = localVariables.newFieldInRecord("pnd_Total_Gross_Amt", "#TOTAL-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Cref_Amt = localVariables.newFieldInRecord("pnd_Total_Cref_Amt", "#TOTAL-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Roll_Ct = localVariables.newFieldInRecord("pnd_Total_Roll_Ct", "#TOTAL-ROLL-CT", FieldType.PACKED_DECIMAL, 7);
        pnd_Grp_Optn_Code = localVariables.newFieldInRecord("pnd_Grp_Optn_Code", "#GRP-OPTN-CODE", FieldType.STRING, 5);
        pnd_Prt_Header_Name = localVariables.newFieldInRecord("pnd_Prt_Header_Name", "#PRT-HEADER-NAME", FieldType.STRING, 40);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_Prt_Total_Name = localVariables.newFieldInRecord("pnd_Prt_Total_Name", "#PRT-TOTAL-NAME", FieldType.STRING, 40);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp805() throws Exception
    {
        super("Fcpp805");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP805", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 55
        //*                                                                                                                                                               //Natural: ON ERROR
        getWorkFiles().read(1, pdaFcpacntl.getCntl());                                                                                                                    //Natural: READ WORK FILE 1 ONCE CNTL
        //*  READ INTERNAL ROLLOVER FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 #GRP-OPTN-CODE WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(2, pnd_Grp_Optn_Code, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            getSort().writeSortInData(pnd_Grp_Optn_Code, pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),           //Natural: END-ALL
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(16), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(22), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(28), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(34), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(40), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(12), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(16), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(18), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(22), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(24), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(28), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(30), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(34), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(36), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(40), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(12), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(18), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(24), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(30), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(36), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde(), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(pnd_Grp_Optn_Code, pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                     //Natural: SORT BY #GRP-OPTN-CODE ANNT-SOC-SEC-NBR CNTRCT-PPCN-NBR USING INV-ACCT-COUNT INV-ACCT-CDE ( * ) INV-ACCT-UNIT-QTY ( * ) INV-ACCT-UNIT-VALUE ( * ) INV-ACCT-SETTL-AMT ( * ) INV-ACCT-CNTRCT-AMT ( * ) INV-ACCT-DVDND-AMT ( * ) CNTRCT-ROLL-DEST-CDE CNTRCT-MODE-CDE CNTRCT-HOLD-CDE
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pnd_Grp_Optn_Code, pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(2), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(8), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(14), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(20), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(26), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(32), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(38), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(4), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(10), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(16), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(22), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(28), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(34), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(40), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(6), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(12), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(18), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(24), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(30), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(36), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(2), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(8), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(14), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(20), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(26), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(32), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(38), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(4), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(10), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(16), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(22), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(28), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(34), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(40), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(6), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(12), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(18), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(24), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(30), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(36), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde())))
        {
            CheckAtStartofData572();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  PEND CODE ?????????
            //*  TERM CODE ?????????
            //*  CASH CODE ?????????
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF #GRP-OPTN-CODE
            //*  MLA 04/05/01
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *PROGRAM 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 47T #PRT-HEADER-NAME / 56T CNTL.CNTL-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 INV-ACCT-COUNT
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
            {
                pnd_Cntrct_Amt.reset();                                                                                                                                   //Natural: RESET #CNTRCT-AMT #DVDND-AMT #GROSS-AMT #SETTLMNT-AMT
                pnd_Dvdnd_Amt.reset();
                pnd_Gross_Amt.reset();
                pnd_Settlmnt_Amt.reset();
                short decideConditionsMet587 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ROLL-DEST-CDE;//Natural: VALUE ' '
                if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde().equals(" "))))
                {
                    decideConditionsMet587++;
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: VALUE 'IRAT', '03BT', 'QPLT'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde().equals("IRAT") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde().equals("03BT") 
                    || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde().equals("QPLT"))))
                {
                    decideConditionsMet587++;
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(pnd_Index).equals("T")))                                                        //Natural: IF INV-ACCT-CDE ( #INDEX ) = 'T'
                    {
                        pnd_Cntrct_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                //Natural: MOVE INV-ACCT-SETTL-AMT ( #INDEX ) TO #CNTRCT-AMT
                        pnd_Dvdnd_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                                 //Natural: MOVE INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT
                        pnd_Gross_Amt.compute(new ComputeParameters(false, pnd_Gross_Amt), pnd_Cntrct_Amt.add(pnd_Dvdnd_Amt));                                            //Natural: COMPUTE #GROSS-AMT = #CNTRCT-AMT + #DVDND-AMT
                        pnd_T_Cntrct_Amt.nadd(pnd_Cntrct_Amt);                                                                                                            //Natural: ADD #CNTRCT-AMT TO #T-CNTRCT-AMT
                        pnd_T_Dvdnd_Amt.nadd(pnd_Dvdnd_Amt);                                                                                                              //Natural: ADD #DVDND-AMT TO #T-DVDND-AMT
                        pnd_T_Gross_Amt.nadd(pnd_Gross_Amt);                                                                                                              //Natural: ADD #GROSS-AMT TO #T-GROSS-AMT
                        pnd_Total_Cntrct_Amt.nadd(pnd_Cntrct_Amt);                                                                                                        //Natural: ADD #CNTRCT-AMT TO #TOTAL-CNTRCT-AMT
                        pnd_Total_Dvdnd_Amt.nadd(pnd_Dvdnd_Amt);                                                                                                          //Natural: ADD #DVDND-AMT TO #TOTAL-DVDND-AMT
                        pnd_Total_Gross_Amt.nadd(pnd_Gross_Amt);                                                                                                          //Natural: ADD #GROSS-AMT TO #TOTAL-GROSS-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Settlmnt_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                              //Natural: MOVE INV-ACCT-SETTL-AMT ( #INDEX ) TO #SETTLMNT-AMT
                        pnd_T_Cref_Amt.nadd(pnd_Settlmnt_Amt);                                                                                                            //Natural: ADD #SETTLMNT-AMT TO #T-CREF-AMT
                        pnd_Total_Cref_Amt.nadd(pnd_Settlmnt_Amt);                                                                                                        //Natural: ADD #SETTLMNT-AMT TO #TOTAL-CREF-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_T_Roll_Ct.nadd(1);                                                                                                                                //Natural: ADD 1 TO #T-ROLL-CT
                    pnd_Total_Roll_Ct.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-ROLL-CT
                }                                                                                                                                                         //Natural: VALUE 'IRAC', '03BC', 'QPLC'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde().equals("IRAC") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde().equals("03BC") 
                    || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde().equals("QPLC"))))
                {
                    decideConditionsMet587++;
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(pnd_Index).equals("T") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(pnd_Index).equals("TG")  //Natural: IF INV-ACCT-CDE ( #INDEX ) = 'T' OR = 'TG' OR = 'G'
                        || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(pnd_Index).equals("G")))
                    {
                        pnd_Cntrct_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                //Natural: MOVE INV-ACCT-SETTL-AMT ( #INDEX ) TO #CNTRCT-AMT
                        pnd_Dvdnd_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                                 //Natural: MOVE INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT
                        pnd_Gross_Amt.compute(new ComputeParameters(false, pnd_Gross_Amt), pnd_Cntrct_Amt.add(pnd_Dvdnd_Amt));                                            //Natural: COMPUTE #GROSS-AMT = #CNTRCT-AMT + #DVDND-AMT
                        pnd_C_Cntrct_Amt.nadd(pnd_Cntrct_Amt);                                                                                                            //Natural: ADD #CNTRCT-AMT TO #C-CNTRCT-AMT
                        pnd_C_Dvdnd_Amt.nadd(pnd_Dvdnd_Amt);                                                                                                              //Natural: ADD #DVDND-AMT TO #C-DVDND-AMT
                        pnd_C_Gross_Amt.nadd(pnd_Gross_Amt);                                                                                                              //Natural: ADD #GROSS-AMT TO #C-GROSS-AMT
                        pnd_Total_Cntrct_Amt.nadd(pnd_Cntrct_Amt);                                                                                                        //Natural: ADD #CNTRCT-AMT TO #TOTAL-CNTRCT-AMT
                        pnd_Total_Dvdnd_Amt.nadd(pnd_Dvdnd_Amt);                                                                                                          //Natural: ADD #DVDND-AMT TO #TOTAL-DVDND-AMT
                        pnd_Total_Gross_Amt.nadd(pnd_Gross_Amt);                                                                                                          //Natural: ADD #GROSS-AMT TO #TOTAL-GROSS-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Settlmnt_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                              //Natural: MOVE INV-ACCT-SETTL-AMT ( #INDEX ) TO #SETTLMNT-AMT
                        pnd_C_Cref_Amt.nadd(pnd_Settlmnt_Amt);                                                                                                            //Natural: ADD #SETTLMNT-AMT TO #C-CREF-AMT
                        pnd_Total_Cref_Amt.nadd(pnd_Settlmnt_Amt);                                                                                                        //Natural: ADD #SETTLMNT-AMT TO #TOTAL-CREF-AMT
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_C_Roll_Ct.nadd(1);                                                                                                                                //Natural: ADD 1 TO #C-ROLL-CT
                    pnd_Total_Roll_Ct.nadd(1);                                                                                                                            //Natural: ADD 1 TO #TOTAL-ROLL-CT
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().display(1, new ReportMatrixColumnUnderline("-")," /Soc Sec No",                                                                              //Natural: DISPLAY ( 01 ) ( UC = - ) ' /Soc Sec No' ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) ' /Contract No' CNTRCT-PPCN-NBR ' /Contract Amt' #CNTRCT-AMT ( EM = ZZZ,ZZZ,ZZZ.99- HC = R ) ' /Dividend' #DVDND-AMT ( EM = ZZZ,ZZZ,ZZZ.99- HC = R ) 'Total/ Cntr + Dvdnd' #GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99- HC = R ) 'CREF or REA/Amount' #SETTLMNT-AMT ( EM = ZZZ,ZZZ,ZZZ.99- HC = R ) ' /Dest' CNTRCT-ROLL-DEST-CDE 'Mode/Code' CNTRCT-MODE-CDE ( EM = 999 )
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999")," /Contract No",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr()," /Contract Amt",
                		pnd_Cntrct_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right)," /Dividend",
                    
                		pnd_Dvdnd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Total/ Cntr + Dvdnd",
                    
                		pnd_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"CREF or REA/Amount",
                    
                		pnd_Settlmnt_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right)," /Dest",
                    
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde(),"Mode/Code",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde(), new ReportEditMask ("999"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      'Hold/Code'  ' ' /* ?????????
                //*      'Pend/Code'  ' ' /* ?????????
                //*      'Term/Code'  ' ' /* ?????????
                //*      'Cash/Code'  ' ' /* ?????????
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* *PERFORM PRINT-SUB-TOTALS            /* MLA 04/06/01
        //* *NEWPAGE (01)                      /* MLA 04/06/01
        //*  MLA 04/06/01
        if (condition(pnd_Total_T_Roll_Ct.getValue(5).equals(getZero()) && pnd_Total_C_Roll_Ct.getValue(5).equals(getZero())))                                            //Natural: IF #TOTAL-T-ROLL-CT ( 5 ) = 0 AND #TOTAL-C-ROLL-CT ( 5 ) = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 01 ) ///// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***   No Internal Rollover Contracts for this Period      ***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***   No Internal Rollover Contracts for this Period      ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
            //*  MLA   04/06/01
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            FOR02:                                                                                                                                                        //Natural: FOR #A 1 TO 5
            for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(5)); pnd_A.nadd(1))
            {
                short decideConditionsMet650 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #A;//Natural: VALUE 1
                if (condition((pnd_A.equals(1))))
                {
                    decideConditionsMet650++;
                    pnd_Prt_Total_Name.setValue("IPRO ROLLOVER");                                                                                                         //Natural: MOVE 'IPRO ROLLOVER' TO #PRT-TOTAL-NAME
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_A.equals(2))))
                {
                    decideConditionsMet650++;
                    pnd_Prt_Total_Name.setValue("P&I ROLLOVER");                                                                                                          //Natural: MOVE 'P&I ROLLOVER' TO #PRT-TOTAL-NAME
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_A.equals(3))))
                {
                    decideConditionsMet650++;
                    pnd_Prt_Total_Name.setValue("ROLLOVER OTHER");                                                                                                        //Natural: MOVE 'ROLLOVER OTHER' TO #PRT-TOTAL-NAME
                }                                                                                                                                                         //Natural: VALUE 4
                else if (condition((pnd_A.equals(4))))
                {
                    decideConditionsMet650++;
                    pnd_Prt_Total_Name.setValue("TPA ROLLOVER");                                                                                                          //Natural: MOVE 'TPA ROLLOVER' TO #PRT-TOTAL-NAME
                }                                                                                                                                                         //Natural: VALUE 5
                else if (condition((pnd_A.equals(5))))
                {
                    decideConditionsMet650++;
                    pnd_Prt_Total_Name.setValue("GRAND TOTAL");                                                                                                           //Natural: MOVE 'GRAND TOTAL' TO #PRT-TOTAL-NAME
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getReports().write(1, NEWLINE,NEWLINE,pnd_Prt_Total_Name,NEWLINE,new TabSetting(7),"TOTAL TIAA",new ReportTAsterisk(pnd_Cntrct_Amt),pnd_Total_T_Cntrct_Amt.getValue(pnd_A),  //Natural: WRITE ( 01 ) // #PRT-TOTAL-NAME / 7T 'TOTAL TIAA' T*#CNTRCT-AMT #TOTAL-T-CNTRCT-AMT ( #A ) ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#DVDND-AMT #TOTAL-T-DVDND-AMT ( #A ) ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#GROSS-AMT #TOTAL-T-GROSS-AMT ( #A ) ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#SETTLMNT-AMT #TOTAL-T-CREF-AMT ( #A ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 10X 'COUNT:' #TOTAL-T-ROLL-CT ( #A ) ( EM = Z,ZZZ,ZZ9 ) / 7T 'TOTAL CREF' T*#CNTRCT-AMT #TOTAL-C-CNTRCT-AMT ( #A ) ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#DVDND-AMT #TOTAL-C-DVDND-AMT ( #A ) ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#GROSS-AMT #TOTAL-C-GROSS-AMT ( #A ) ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#SETTLMNT-AMT #TOTAL-C-CREF-AMT ( #A ) ( EM = ZZZ,ZZZ,ZZZ.99- ) 10X 'COUNT:' #TOTAL-C-ROLL-CT ( #A ) ( EM = Z,ZZZ,ZZ9 )
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Dvdnd_Amt),pnd_Total_T_Dvdnd_Amt.getValue(pnd_A), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Gross_Amt),pnd_Total_T_Gross_Amt.getValue(pnd_A), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ReportTAsterisk(pnd_Settlmnt_Amt),pnd_Total_T_Cref_Amt.getValue(pnd_A), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(10),"COUNT:",pnd_Total_T_Roll_Ct.getValue(pnd_A), 
                    new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"TOTAL CREF",new ReportTAsterisk(pnd_Cntrct_Amt),pnd_Total_C_Cntrct_Amt.getValue(pnd_A), 
                    new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Dvdnd_Amt),pnd_Total_C_Dvdnd_Amt.getValue(pnd_A), new ReportEditMask 
                    ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Gross_Amt),pnd_Total_C_Gross_Amt.getValue(pnd_A), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
                    ReportTAsterisk(pnd_Settlmnt_Amt),pnd_Total_C_Cref_Amt.getValue(pnd_A), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(10),"COUNT:",pnd_Total_C_Roll_Ct.getValue(pnd_A), 
                    new ReportEditMask ("Z,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ** MLA 04/05/01   START OF TPA CHANGES  *********************
        //* *****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: HEADER-RTN
        //* **********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUB-TOTALS
        //* ** MLA 04/05/01   END OF TPA CHANGES  *********************
    }
    private void sub_Header_Rtn() throws Exception                                                                                                                        //Natural: HEADER-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************
        short decideConditionsMet681 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #GRP-OPTN-CODE;//Natural: VALUE 'IPRO'
        if (condition((pnd_Grp_Optn_Code.equals("IPRO"))))
        {
            decideConditionsMet681++;
            pnd_Prt_Header_Name.setValue("  IPRO ROLLOVER BY SOCIAL SECURITY  ");                                                                                         //Natural: ASSIGN #PRT-HEADER-NAME := '  IPRO ROLLOVER BY SOCIAL SECURITY  '
            pnd_Ndx.setValue(1);                                                                                                                                          //Natural: MOVE 1 TO #NDX
        }                                                                                                                                                                 //Natural: VALUE 'OTH'
        else if (condition((pnd_Grp_Optn_Code.equals("OTH"))))
        {
            decideConditionsMet681++;
            pnd_Prt_Header_Name.setValue("INTERNAL ROLLOVER BY SOCIAL SECURITY");                                                                                         //Natural: ASSIGN #PRT-HEADER-NAME := 'INTERNAL ROLLOVER BY SOCIAL SECURITY'
            //*  ORIGINALLY 2 - ROXAN 05/30/01
            pnd_Ndx.setValue(3);                                                                                                                                          //Natural: MOVE 3 TO #NDX
        }                                                                                                                                                                 //Natural: VALUE 'P&I'
        else if (condition((pnd_Grp_Optn_Code.equals("P&I"))))
        {
            decideConditionsMet681++;
            pnd_Prt_Header_Name.setValue("  P&I ROLLOVER BY SOCIAL SECURITY  ");                                                                                          //Natural: ASSIGN #PRT-HEADER-NAME := '  P&I ROLLOVER BY SOCIAL SECURITY  '
            //*  ORIGINALLY 3 - ROXAN 05/30/01
            pnd_Ndx.setValue(2);                                                                                                                                          //Natural: MOVE 2 TO #NDX
        }                                                                                                                                                                 //Natural: VALUE 'TPA'
        else if (condition((pnd_Grp_Optn_Code.equals("TPA"))))
        {
            decideConditionsMet681++;
            pnd_Prt_Header_Name.setValue("  TPA ROLLOVER BY SOCIAL SECURITY  ");                                                                                          //Natural: ASSIGN #PRT-HEADER-NAME := '  TPA ROLLOVER BY SOCIAL SECURITY  '
            pnd_Ndx.setValue(4);                                                                                                                                          //Natural: MOVE 4 TO #NDX
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Sub_Totals() throws Exception                                                                                                                  //Natural: PRINT-SUB-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************
        getReports().write(1, NEWLINE,NEWLINE,"TOTAL TIAA ",new ReportTAsterisk(pnd_Cntrct_Amt),pnd_T_Cntrct_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new              //Natural: WRITE ( 01 ) // 'TOTAL TIAA ' T*#CNTRCT-AMT #T-CNTRCT-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#DVDND-AMT #T-DVDND-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#GROSS-AMT #T-GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#SETTLMNT-AMT #T-CREF-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) 10X 'Count:' #T-ROLL-CT ( EM = Z,ZZZ,ZZ9 ) / 'TOTAL CREF ' T*#CNTRCT-AMT #C-CNTRCT-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#DVDND-AMT #C-DVDND-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#GROSS-AMT #C-GROSS-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) T*#SETTLMNT-AMT #C-CREF-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) 10X 'Count:' #C-ROLL-CT ( EM = Z,ZZZ,ZZ9 )
            ReportTAsterisk(pnd_Dvdnd_Amt),pnd_T_Dvdnd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Gross_Amt),pnd_T_Gross_Amt, new 
            ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Settlmnt_Amt),pnd_T_Cref_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(10),"Count:",pnd_T_Roll_Ct, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"TOTAL CREF ",new ReportTAsterisk(pnd_Cntrct_Amt),pnd_C_Cntrct_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new 
            ReportTAsterisk(pnd_Dvdnd_Amt),pnd_C_Dvdnd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Gross_Amt),pnd_C_Gross_Amt, new 
            ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Settlmnt_Amt),pnd_C_Cref_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),new ColumnSpacing(10),"Count:",pnd_C_Roll_Ct, 
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* *********************************
        //* * #NDX = 1   "IPRO"
        //* *        2   "OTH"
        //* *        3   "P&I"
        //* *        4   "TPA"
        //* *        5   "TOTAL"
        //* *********************************
        pnd_Total_T_Cntrct_Amt.getValue(pnd_Ndx).setValue(pnd_T_Cntrct_Amt);                                                                                              //Natural: MOVE #T-CNTRCT-AMT TO #TOTAL-T-CNTRCT-AMT ( #NDX )
        pnd_Total_T_Dvdnd_Amt.getValue(pnd_Ndx).setValue(pnd_T_Dvdnd_Amt);                                                                                                //Natural: MOVE #T-DVDND-AMT TO #TOTAL-T-DVDND-AMT ( #NDX )
        pnd_Total_T_Gross_Amt.getValue(pnd_Ndx).setValue(pnd_T_Gross_Amt);                                                                                                //Natural: MOVE #T-GROSS-AMT TO #TOTAL-T-GROSS-AMT ( #NDX )
        pnd_Total_T_Cref_Amt.getValue(pnd_Ndx).setValue(pnd_T_Cref_Amt);                                                                                                  //Natural: MOVE #T-CREF-AMT TO #TOTAL-T-CREF-AMT ( #NDX )
        pnd_Total_T_Roll_Ct.getValue(pnd_Ndx).setValue(pnd_T_Roll_Ct);                                                                                                    //Natural: MOVE #T-ROLL-CT TO #TOTAL-T-ROLL-CT ( #NDX )
        pnd_Total_C_Cntrct_Amt.getValue(pnd_Ndx).setValue(pnd_C_Cntrct_Amt);                                                                                              //Natural: MOVE #C-CNTRCT-AMT TO #TOTAL-C-CNTRCT-AMT ( #NDX )
        pnd_Total_C_Dvdnd_Amt.getValue(pnd_Ndx).setValue(pnd_C_Dvdnd_Amt);                                                                                                //Natural: MOVE #C-DVDND-AMT TO #TOTAL-C-DVDND-AMT ( #NDX )
        pnd_Total_C_Gross_Amt.getValue(pnd_Ndx).setValue(pnd_C_Gross_Amt);                                                                                                //Natural: MOVE #C-GROSS-AMT TO #TOTAL-C-GROSS-AMT ( #NDX )
        pnd_Total_C_Cref_Amt.getValue(pnd_Ndx).setValue(pnd_C_Cref_Amt);                                                                                                  //Natural: MOVE #C-CREF-AMT TO #TOTAL-C-CREF-AMT ( #NDX )
        pnd_Total_C_Roll_Ct.getValue(pnd_Ndx).setValue(pnd_C_Roll_Ct);                                                                                                    //Natural: MOVE #C-ROLL-CT TO #TOTAL-C-ROLL-CT ( #NDX )
        pnd_Total_T_Cntrct_Amt.getValue(5).nadd(pnd_T_Cntrct_Amt);                                                                                                        //Natural: ADD #T-CNTRCT-AMT TO #TOTAL-T-CNTRCT-AMT ( 5 )
        pnd_Total_T_Dvdnd_Amt.getValue(5).nadd(pnd_T_Dvdnd_Amt);                                                                                                          //Natural: ADD #T-DVDND-AMT TO #TOTAL-T-DVDND-AMT ( 5 )
        pnd_Total_T_Gross_Amt.getValue(5).nadd(pnd_T_Gross_Amt);                                                                                                          //Natural: ADD #T-GROSS-AMT TO #TOTAL-T-GROSS-AMT ( 5 )
        pnd_Total_T_Cref_Amt.getValue(5).nadd(pnd_T_Cref_Amt);                                                                                                            //Natural: ADD #T-CREF-AMT TO #TOTAL-T-CREF-AMT ( 5 )
        pnd_Total_T_Roll_Ct.getValue(5).nadd(pnd_T_Roll_Ct);                                                                                                              //Natural: ADD #T-ROLL-CT TO #TOTAL-T-ROLL-CT ( 5 )
        pnd_Total_C_Cntrct_Amt.getValue(5).nadd(pnd_C_Cntrct_Amt);                                                                                                        //Natural: ADD #C-CNTRCT-AMT TO #TOTAL-C-CNTRCT-AMT ( 5 )
        pnd_Total_C_Dvdnd_Amt.getValue(5).nadd(pnd_C_Dvdnd_Amt);                                                                                                          //Natural: ADD #C-DVDND-AMT TO #TOTAL-C-DVDND-AMT ( 5 )
        pnd_Total_C_Gross_Amt.getValue(5).nadd(pnd_C_Gross_Amt);                                                                                                          //Natural: ADD #C-GROSS-AMT TO #TOTAL-C-GROSS-AMT ( 5 )
        pnd_Total_C_Cref_Amt.getValue(5).nadd(pnd_C_Cref_Amt);                                                                                                            //Natural: ADD #C-CREF-AMT TO #TOTAL-C-CREF-AMT ( 5 )
        pnd_Total_C_Roll_Ct.getValue(5).nadd(pnd_C_Roll_Ct);                                                                                                              //Natural: ADD #C-ROLL-CT TO #TOTAL-C-ROLL-CT ( 5 )
        pnd_T_Cntrct_Amt.reset();                                                                                                                                         //Natural: RESET #T-CNTRCT-AMT #T-DVDND-AMT #T-GROSS-AMT #T-CREF-AMT #T-ROLL-CT #C-CNTRCT-AMT #C-DVDND-AMT #C-GROSS-AMT #C-CREF-AMT #C-ROLL-CT
        pnd_T_Dvdnd_Amt.reset();
        pnd_T_Gross_Amt.reset();
        pnd_T_Cref_Amt.reset();
        pnd_T_Roll_Ct.reset();
        pnd_C_Cntrct_Amt.reset();
        pnd_C_Dvdnd_Amt.reset();
        pnd_C_Gross_Amt.reset();
        pnd_C_Cref_Amt.reset();
        pnd_C_Roll_Ct.reset();
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_Grp_Optn_CodeIsBreak = pnd_Grp_Optn_Code.isBreak(endOfData);
        if (condition(pnd_Grp_Optn_CodeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-TOTALS
            sub_Print_Sub_Totals();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM HEADER-RTN
            sub_Header_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(47),pnd_Prt_Header_Name,NEWLINE,new 
            TabSetting(56),pdaFcpacntl.getCntl_Cntl_Check_Dte(), new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-")," /Soc Sec No",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999")," /Contract No",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr()," /Contract Amt",
        		pnd_Cntrct_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right)," /Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"Total/ Cntr + Dvdnd",
            
        		pnd_Gross_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"CREF or REA/Amount",
            
        		pnd_Settlmnt_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right)," /Dest",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Roll_Dest_Cde(),"Mode/Code",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde(), new ReportEditMask ("999"));
    }
    private void CheckAtStartofData572() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM HEADER-RTN
            sub_Header_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
