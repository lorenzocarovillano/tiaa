/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:40 PM
**        * FROM NATURAL PROGRAM : Fcpp985
************************************************************
**        * FILE NAME            : Fcpp985.java
**        * CLASS NAME           : Fcpp985
**        * INSTANCE NAME        : Fcpp985
************************************************************
***********************************************************************
* PROGRAM:  FCPP985
*
* TITLE:    CPS AP  XML STATEMENT GENERATION - DAILY CSR
* AUTHOR:   FRANCIS ENDAYA
* DESC:     THE PROGRAM READS CHECK FILE AND CREATE XML FILE TO BE SENT
*           OVER TO CCP FOR CHECK CREATION FOR AP CHECKS.
********************************
* HISTORY
* 03/XX/2017 F.ENDAYA NEW
*  08/12/2017  FENDAYA    PIN EXPANSION. STOW TO PICK UP NEW PDA's
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp985 extends BLNatBase
{
    // Data Areas
    private PdaFcpaacum pdaFcpaacum;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpa800d pdaFcpa800d;
    private PdaFcpa801b pdaFcpa801b;
    private PdaFcpa803c pdaFcpa803c;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa803a pdaFcpa803a;
    private PdaFcpa803h pdaFcpa803h;
    private PdaFcpa803l pdaFcpa803l;
    private LdaFcplbar1 ldaFcplbar1;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;
    private PdaFcpa800e pdaFcpa800e;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;

    private DbsGroup pnd_Key_Pnd_Key_Detail;
    private DbsField pnd_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Key_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Key_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Run_Type;
    private DbsField pnd_Ws_Pnd_File_Type;
    private DbsField pnd_Ws_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Monthly_Restart;
    private DbsField pnd_Pymnt_Check_Nbr_N10;

    private DbsGroup pnd_Pymnt_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N3;
    private DbsField pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Next_Check_Nbr;
    private DbsField pnd_Ws_Next_Check_Prefix;
    private DbsField pnd_Read_Cnt;
    private DbsField pnd_Chk_Key;

    private DbsGroup pnd_Chk_Key__R_Field_4;
    private DbsField pnd_Chk_Key_Pnd_Chk_Cntrct_Orgn_Cde;
    private DbsField pnd_Chk_Key_Pnd_Chk_Cntrct_Invrse_Dte;
    private DbsField pnd_Chk_Key_Pnd_Chk_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Chk_Key_Pnd_Chk_Cntrct_Cmpny_Code;
    private DbsField pnd_Chk_Key_Pnd_Chk_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Can_Tax_Key;

    private DbsGroup pnd_Can_Tax_Key__R_Field_5;
    private DbsField pnd_Can_Tax_Key_Pnd_Can_Cntrct_Orgn_Cde;
    private DbsField pnd_Can_Tax_Key_Pnd_Can_Cntrct_Invrse_Dte;
    private DbsField pnd_Can_Tax_Key_Pnd_Can_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Can_Tax_Key_Pnd_Can_Cntrct_Cmpny_Code;
    private DbsField pnd_Can_Tax_Key_Pnd_Can_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Found;
    private DbsField pnd_Inv_Acct_Can_Tax_Amt;
    private DbsField pnd_Total_Can_Tax_Amt;
    private DbsField pnd_Convert_To_Wire;
    private DbsField pnd_Xerox;
    private DbsField pnd_Batch_Cnt;
    private DbsField pnd_First_Record;
    private DbsField pnd_Last_Contract;
    private DbsField pnd_Last_Record;
    private DbsField pnd_Totals_Area;

    private DbsGroup pnd_Totals_Area__R_Field_6;
    private DbsField pnd_Totals_Area_Pnd_Total_Gross_Amt;
    private DbsField pnd_Totals_Area_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Totals_Area_Pnd_Total_Pymnt_Ded_Amt;
    private DbsField pnd_Totals_Area_Pnd_Total_Ded;
    private DbsField pnd_Totals_Area_Pnd_Ws_Interest_Amt;
    private DbsField pnd_Totals_Area_Pnd_Total_Payment_Amt;
    private DbsField pnd_Totals_Area_Pnd_Total_Net_Amt2;
    private DbsField pnd_Sve_Documentrequestid_Data;
    private DbsField pnd_Sve_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Sve_Wf_Pymnt_Addr_Grp;
    private DbsField pnd_Sve2_Wf_Pymnt_Addr_Grp;

    private DbsGroup pnd_Sve_Check_Sort_Fields;
    private DbsField pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Hold_Cde;
    private DbsField pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Company_Cde;
    private DbsField pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Total_Pages;
    private DbsField pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr_Alpha;

    private DbsGroup pnd_Sve_Check_Sort_Fields__R_Field_7;
    private DbsField pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr;
    private DbsField pnd_Sve_Check_Sort_Fields_Pnd_Sve_Filler;

    private DbsGroup pnd_Sve_Check_Fields;
    private DbsField pnd_Sve_Check_Fields_Pnd_Sve_Fund_On_Page;
    private DbsField pnd_Sve_Check_Fields_Pnd_Sve_Ded_Pymnt_Table;
    private DbsField pnd_Sve_Check_Fields_Pnd_Sve_Cntrct_Pnd__Ded;
    private DbsField pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Records;
    private DbsField pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Pnd;
    private DbsField pnd_Sve_Check_Fields_Pnd_Sve_Dpi_Ind;
    private DbsField pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind;
    private DbsField pnd_Sve_Check_Fields_Pnd_Sve_Filler2;
    private DbsField pnd_Save_First_Twenty;

    private DbsGroup pnd_Save_First_Twenty__R_Field_8;

    private DbsGroup pnd_Save_First_Twenty_Documentrequestid_Data;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Sys_Id;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Yyyy;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Mm;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Dd;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Hh;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Ii;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Ss;
    private DbsField pnd_Bar_Env_Intg_A;

    private DbsGroup pnd_Bar_Env_Intg_A__R_Field_9;
    private DbsField pnd_Bar_Env_Intg_A_Pnd_Bar_Env_Intg;
    private DbsField pnd_Pymnt;
    private DbsField pnd_Pymnt_Not_Found;

    private DataAccessProgramView vw_pymnt_View;
    private DbsField pymnt_View_Cntrct_Check_Crrncy_Cde;
    private DbsField pymnt_View_Cntrct_Crrncy_Cde;
    private DbsField pymnt_View_Cntrct_Orgn_Cde;
    private DbsField pymnt_View_Pymnt_Stats_Cde;
    private DbsField pymnt_View_Pymnt_Pay_Type_Req_Ind;
    private DbsField pymnt_View_Pymnt_Check_Dte;
    private DbsField pymnt_View_Pymnt_Check_Nbr;
    private DbsField pymnt_View_Pymnt_Nbr;
    private DbsField pymnt_View_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pymnt_View__R_Field_10;
    private DbsField pymnt_View_Pymnt_Prcss_Seq_Num;
    private DbsField pymnt_View_Pymnt_Instmt_Nbr;
    private DbsField pymnt_View_Pymnt_Check_Seq_Nbr;

    private DbsGroup pnd_Sk;
    private DbsField pnd_Sk_Pymnt_Check_Dte;
    private DbsField pnd_Sk_Cntrct_Orgn_Cde;
    private DbsField pnd_Sk_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Sk_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Sk_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Sk__R_Field_11;
    private DbsField pnd_Sk_Pnd_Sk_All;

    private DbsGroup pnd_Ck;
    private DbsField pnd_Ck_Pymnt_Check_Dte;
    private DbsField pnd_Ck_Cntrct_Orgn_Cde;
    private DbsField pnd_Ck_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ck_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ck_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ck__R_Field_12;
    private DbsField pnd_Ck_Pnd_Ck_All;
    private DbsField pnd_Save_Eft_Acct;
    private DbsField pnd_Check_Eft;
    private DbsField pnd_Check_Rollover;
    private DbsField pnd_Sve_Institutioninfoline_Data;
    private DbsField pnd_Check_Efthold;
    private DbsField pnd_Batch_Cnt_First;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_KeyOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaacum = new PdaFcpaacum(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        pdaFcpa801b = new PdaFcpa801b(localVariables);
        pdaFcpa803c = new PdaFcpa803c(localVariables);
        pdaFcpa110 = new PdaFcpa110(localVariables);
        pdaFcpa803 = new PdaFcpa803(localVariables);
        pdaFcpa803a = new PdaFcpa803a(localVariables);
        pdaFcpa803h = new PdaFcpa803h(localVariables);
        pdaFcpa803l = new PdaFcpa803l(localVariables);
        ldaFcplbar1 = new LdaFcplbar1();
        registerRecord(ldaFcplbar1);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);
        pdaFcpa800e = new PdaFcpa800e(localVariables);

        // Local Variables
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 31);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);

        pnd_Key_Pnd_Key_Detail = pnd_Key__R_Field_1.newGroupInGroup("pnd_Key_Pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Cntrct_Orgn_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Cntrct_Invrse_Dte = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Key_Cntrct_Cmbn_Nbr = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Key_Pymnt_Prcss_Seq_Num = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Run_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 8);
        pnd_Ws_Pnd_File_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_File_Type", "#FILE-TYPE", FieldType.STRING, 8);
        pnd_Ws_Cntrct_Hold_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Ws_Pymnt_Check_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pymnt_Check_Nbr_N10 = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10", "PYMNT-CHECK-NBR-N10", FieldType.PACKED_DECIMAL, 10);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Monthly_Restart = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Monthly_Restart", "#MONTHLY-RESTART", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Pymnt_Check_Nbr_N10", "#PYMNT-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Pymnt_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Pymnt_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Pymnt_Check_Nbr_N10);
        pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N3 = pnd_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N3", 
            "#PYMNT-CHECK-NBR-N3", FieldType.NUMERIC, 3);
        pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7 = pnd_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7", 
            "#PYMNT-CHECK-NBR-N7", FieldType.NUMERIC, 7);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_3", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Next_Check_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Nbr", "#WS-NEXT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Check_Prefix = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Prefix", "#WS-NEXT-CHECK-PREFIX", FieldType.NUMERIC, 3);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Chk_Key = localVariables.newFieldInRecord("pnd_Chk_Key", "#CHK-KEY", FieldType.STRING, 34);

        pnd_Chk_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Chk_Key__R_Field_4", "REDEFINE", pnd_Chk_Key);
        pnd_Chk_Key_Pnd_Chk_Cntrct_Orgn_Cde = pnd_Chk_Key__R_Field_4.newFieldInGroup("pnd_Chk_Key_Pnd_Chk_Cntrct_Orgn_Cde", "#CHK-CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Chk_Key_Pnd_Chk_Cntrct_Invrse_Dte = pnd_Chk_Key__R_Field_4.newFieldInGroup("pnd_Chk_Key_Pnd_Chk_Cntrct_Invrse_Dte", "#CHK-CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Chk_Key_Pnd_Chk_Cntrct_Cmbn_Nbr = pnd_Chk_Key__R_Field_4.newFieldInGroup("pnd_Chk_Key_Pnd_Chk_Cntrct_Cmbn_Nbr", "#CHK-CNTRCT-CMBN-NBR", FieldType.STRING, 
            14);
        pnd_Chk_Key_Pnd_Chk_Cntrct_Cmpny_Code = pnd_Chk_Key__R_Field_4.newFieldInGroup("pnd_Chk_Key_Pnd_Chk_Cntrct_Cmpny_Code", "#CHK-CNTRCT-CMPNY-CODE", 
            FieldType.STRING, 1);
        pnd_Chk_Key_Pnd_Chk_Pymnt_Prcss_Seq_Nbr = pnd_Chk_Key__R_Field_4.newFieldInGroup("pnd_Chk_Key_Pnd_Chk_Pymnt_Prcss_Seq_Nbr", "#CHK-PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Can_Tax_Key = localVariables.newFieldInRecord("pnd_Can_Tax_Key", "#CAN-TAX-KEY", FieldType.STRING, 34);

        pnd_Can_Tax_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Can_Tax_Key__R_Field_5", "REDEFINE", pnd_Can_Tax_Key);
        pnd_Can_Tax_Key_Pnd_Can_Cntrct_Orgn_Cde = pnd_Can_Tax_Key__R_Field_5.newFieldInGroup("pnd_Can_Tax_Key_Pnd_Can_Cntrct_Orgn_Cde", "#CAN-CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Can_Tax_Key_Pnd_Can_Cntrct_Invrse_Dte = pnd_Can_Tax_Key__R_Field_5.newFieldInGroup("pnd_Can_Tax_Key_Pnd_Can_Cntrct_Invrse_Dte", "#CAN-CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Can_Tax_Key_Pnd_Can_Cntrct_Cmbn_Nbr = pnd_Can_Tax_Key__R_Field_5.newFieldInGroup("pnd_Can_Tax_Key_Pnd_Can_Cntrct_Cmbn_Nbr", "#CAN-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Can_Tax_Key_Pnd_Can_Cntrct_Cmpny_Code = pnd_Can_Tax_Key__R_Field_5.newFieldInGroup("pnd_Can_Tax_Key_Pnd_Can_Cntrct_Cmpny_Code", "#CAN-CNTRCT-CMPNY-CODE", 
            FieldType.STRING, 1);
        pnd_Can_Tax_Key_Pnd_Can_Pymnt_Prcss_Seq_Nbr = pnd_Can_Tax_Key__R_Field_5.newFieldInGroup("pnd_Can_Tax_Key_Pnd_Can_Pymnt_Prcss_Seq_Nbr", "#CAN-PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.BOOLEAN, 1);
        pnd_Inv_Acct_Can_Tax_Amt = localVariables.newFieldArrayInRecord("pnd_Inv_Acct_Can_Tax_Amt", "#INV-ACCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Total_Can_Tax_Amt = localVariables.newFieldInRecord("pnd_Total_Can_Tax_Amt", "#TOTAL-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Convert_To_Wire = localVariables.newFieldInRecord("pnd_Convert_To_Wire", "#CONVERT-TO-WIRE", FieldType.NUMERIC, 9);
        pnd_Xerox = localVariables.newFieldInRecord("pnd_Xerox", "#XEROX", FieldType.NUMERIC, 9);
        pnd_Batch_Cnt = localVariables.newFieldInRecord("pnd_Batch_Cnt", "#BATCH-CNT", FieldType.NUMERIC, 7);
        pnd_First_Record = localVariables.newFieldInRecord("pnd_First_Record", "#FIRST-RECORD", FieldType.BOOLEAN, 1);
        pnd_Last_Contract = localVariables.newFieldInRecord("pnd_Last_Contract", "#LAST-CONTRACT", FieldType.BOOLEAN, 1);
        pnd_Last_Record = localVariables.newFieldInRecord("pnd_Last_Record", "#LAST-RECORD", FieldType.BOOLEAN, 1);
        pnd_Totals_Area = localVariables.newFieldInRecord("pnd_Totals_Area", "#TOTALS-AREA", FieldType.STRING, 95);

        pnd_Totals_Area__R_Field_6 = localVariables.newGroupInRecord("pnd_Totals_Area__R_Field_6", "REDEFINE", pnd_Totals_Area);
        pnd_Totals_Area_Pnd_Total_Gross_Amt = pnd_Totals_Area__R_Field_6.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Gross_Amt", "#TOTAL-GROSS-AMT", FieldType.NUMERIC, 
            17, 2);
        pnd_Totals_Area_Pnd_Cntrct_Ivc_Amt = pnd_Totals_Area__R_Field_6.newFieldInGroup("pnd_Totals_Area_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Totals_Area_Pnd_Total_Pymnt_Ded_Amt = pnd_Totals_Area__R_Field_6.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Pymnt_Ded_Amt", "#TOTAL-PYMNT-DED-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Totals_Area_Pnd_Total_Ded = pnd_Totals_Area__R_Field_6.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Ded", "#TOTAL-DED", FieldType.NUMERIC, 11, 
            2);
        pnd_Totals_Area_Pnd_Ws_Interest_Amt = pnd_Totals_Area__R_Field_6.newFieldInGroup("pnd_Totals_Area_Pnd_Ws_Interest_Amt", "#WS-INTEREST-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Totals_Area_Pnd_Total_Payment_Amt = pnd_Totals_Area__R_Field_6.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Payment_Amt", "#TOTAL-PAYMENT-AMT", 
            FieldType.NUMERIC, 17, 2);
        pnd_Totals_Area_Pnd_Total_Net_Amt2 = pnd_Totals_Area__R_Field_6.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Net_Amt2", "#TOTAL-NET-AMT2", FieldType.NUMERIC, 
            17, 2);
        pnd_Sve_Documentrequestid_Data = localVariables.newFieldInRecord("pnd_Sve_Documentrequestid_Data", "#SVE-DOCUMENTREQUESTID-DATA", FieldType.STRING, 
            40);
        pnd_Sve_Cntrct_Cmbn_Nbr = localVariables.newFieldInRecord("pnd_Sve_Cntrct_Cmbn_Nbr", "#SVE-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Sve_Wf_Pymnt_Addr_Grp = localVariables.newFieldArrayInRecord("pnd_Sve_Wf_Pymnt_Addr_Grp", "#SVE-WF-PYMNT-ADDR-GRP", FieldType.STRING, 250, 
            new DbsArrayController(1, 19));
        pnd_Sve2_Wf_Pymnt_Addr_Grp = localVariables.newFieldArrayInRecord("pnd_Sve2_Wf_Pymnt_Addr_Grp", "#SVE2-WF-PYMNT-ADDR-GRP", FieldType.STRING, 250, 
            new DbsArrayController(1, 19));

        pnd_Sve_Check_Sort_Fields = localVariables.newGroupInRecord("pnd_Sve_Check_Sort_Fields", "#SVE-CHECK-SORT-FIELDS");
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Hold_Cde = pnd_Sve_Check_Sort_Fields.newFieldInGroup("pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Hold_Cde", 
            "#SVE-CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Company_Cde = pnd_Sve_Check_Sort_Fields.newFieldInGroup("pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Company_Cde", 
            "#SVE-CNTRCT-COMPANY-CDE", FieldType.STRING, 1);
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Total_Pages = pnd_Sve_Check_Sort_Fields.newFieldInGroup("pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Total_Pages", 
            "#SVE-PYMNT-TOTAL-PAGES", FieldType.PACKED_DECIMAL, 3);
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cnr_Orgnl_Invrse_Dte = pnd_Sve_Check_Sort_Fields.newFieldInGroup("pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cnr_Orgnl_Invrse_Dte", 
            "#SVE-CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr_Alpha = pnd_Sve_Check_Sort_Fields.newFieldInGroup("pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr_Alpha", 
            "#SVE-PYMNT-NBR-ALPHA", FieldType.STRING, 10);

        pnd_Sve_Check_Sort_Fields__R_Field_7 = pnd_Sve_Check_Sort_Fields.newGroupInGroup("pnd_Sve_Check_Sort_Fields__R_Field_7", "REDEFINE", pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr_Alpha);
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr = pnd_Sve_Check_Sort_Fields__R_Field_7.newFieldInGroup("pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr", 
            "#SVE-PYMNT-NBR", FieldType.NUMERIC, 10);
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Filler = pnd_Sve_Check_Sort_Fields.newFieldInGroup("pnd_Sve_Check_Sort_Fields_Pnd_Sve_Filler", "#SVE-FILLER", 
            FieldType.STRING, 25);

        pnd_Sve_Check_Fields = localVariables.newGroupInRecord("pnd_Sve_Check_Fields", "#SVE-CHECK-FIELDS");
        pnd_Sve_Check_Fields_Pnd_Sve_Fund_On_Page = pnd_Sve_Check_Fields.newFieldArrayInGroup("pnd_Sve_Check_Fields_Pnd_Sve_Fund_On_Page", "#SVE-FUND-ON-PAGE", 
            FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 20));
        pnd_Sve_Check_Fields_Pnd_Sve_Ded_Pymnt_Table = pnd_Sve_Check_Fields.newFieldArrayInGroup("pnd_Sve_Check_Fields_Pnd_Sve_Ded_Pymnt_Table", "#SVE-DED-PYMNT-TABLE", 
            FieldType.STRING, 1, new DbsArrayController(1, 15));
        pnd_Sve_Check_Fields_Pnd_Sve_Cntrct_Pnd__Ded = pnd_Sve_Check_Fields.newFieldInGroup("pnd_Sve_Check_Fields_Pnd_Sve_Cntrct_Pnd__Ded", "#SVE-CNTRCT-#-DED", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Records = pnd_Sve_Check_Fields.newFieldInGroup("pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Records", "#SVE-PYMNT-RECORDS", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Pnd = pnd_Sve_Check_Fields.newFieldInGroup("pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Pnd", "#SVE-PYMNT-DED-#", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Sve_Check_Fields_Pnd_Sve_Dpi_Ind = pnd_Sve_Check_Fields.newFieldInGroup("pnd_Sve_Check_Fields_Pnd_Sve_Dpi_Ind", "#SVE-DPI-IND", FieldType.BOOLEAN, 
            1);
        pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind = pnd_Sve_Check_Fields.newFieldInGroup("pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind", "#SVE-PYMNT-DED-IND", 
            FieldType.BOOLEAN, 1);
        pnd_Sve_Check_Fields_Pnd_Sve_Filler2 = pnd_Sve_Check_Fields.newFieldInGroup("pnd_Sve_Check_Fields_Pnd_Sve_Filler2", "#SVE-FILLER2", FieldType.STRING, 
            7);
        pnd_Save_First_Twenty = localVariables.newFieldInRecord("pnd_Save_First_Twenty", "#SAVE-FIRST-TWENTY", FieldType.STRING, 20);

        pnd_Save_First_Twenty__R_Field_8 = localVariables.newGroupInRecord("pnd_Save_First_Twenty__R_Field_8", "REDEFINE", pnd_Save_First_Twenty);

        pnd_Save_First_Twenty_Documentrequestid_Data = pnd_Save_First_Twenty__R_Field_8.newGroupInGroup("pnd_Save_First_Twenty_Documentrequestid_Data", 
            "DOCUMENTREQUESTID-DATA");
        pnd_Save_First_Twenty_Documentrequestid_Sys_Id = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Sys_Id", 
            "DOCUMENTREQUESTID-SYS-ID", FieldType.STRING, 3);
        pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id", 
            "DOCUMENTREQUESTID-SUB-SYS-ID", FieldType.STRING, 3);
        pnd_Save_First_Twenty_Documentrequestid_Yyyy = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Yyyy", 
            "DOCUMENTREQUESTID-YYYY", FieldType.STRING, 4);
        pnd_Save_First_Twenty_Documentrequestid_Mm = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Mm", 
            "DOCUMENTREQUESTID-MM", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Dd = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Dd", 
            "DOCUMENTREQUESTID-DD", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Hh = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Hh", 
            "DOCUMENTREQUESTID-HH", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Ii = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Ii", 
            "DOCUMENTREQUESTID-II", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Ss = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Ss", 
            "DOCUMENTREQUESTID-SS", FieldType.STRING, 2);
        pnd_Bar_Env_Intg_A = localVariables.newFieldInRecord("pnd_Bar_Env_Intg_A", "#BAR-ENV-INTG-A", FieldType.STRING, 1);

        pnd_Bar_Env_Intg_A__R_Field_9 = localVariables.newGroupInRecord("pnd_Bar_Env_Intg_A__R_Field_9", "REDEFINE", pnd_Bar_Env_Intg_A);
        pnd_Bar_Env_Intg_A_Pnd_Bar_Env_Intg = pnd_Bar_Env_Intg_A__R_Field_9.newFieldInGroup("pnd_Bar_Env_Intg_A_Pnd_Bar_Env_Intg", "#BAR-ENV-INTG", FieldType.NUMERIC, 
            1);
        pnd_Pymnt = localVariables.newFieldInRecord("pnd_Pymnt", "#PYMNT", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Not_Found = localVariables.newFieldInRecord("pnd_Pymnt_Not_Found", "#PYMNT-NOT-FOUND", FieldType.BOOLEAN, 1);

        vw_pymnt_View = new DataAccessProgramView(new NameInfo("vw_pymnt_View", "PYMNT-VIEW"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        pymnt_View_Cntrct_Check_Crrncy_Cde = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        pymnt_View_Cntrct_Crrncy_Cde = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        pymnt_View_Cntrct_Orgn_Cde = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        pymnt_View_Pymnt_Stats_Cde = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        pymnt_View_Pymnt_Pay_Type_Req_Ind = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        pymnt_View_Pymnt_Check_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        pymnt_View_Pymnt_Check_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        pymnt_View_Pymnt_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        pymnt_View_Pymnt_Prcss_Seq_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        pymnt_View__R_Field_10 = vw_pymnt_View.getRecord().newGroupInGroup("pymnt_View__R_Field_10", "REDEFINE", pymnt_View_Pymnt_Prcss_Seq_Nbr);
        pymnt_View_Pymnt_Prcss_Seq_Num = pymnt_View__R_Field_10.newFieldInGroup("pymnt_View_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pymnt_View_Pymnt_Instmt_Nbr = pymnt_View__R_Field_10.newFieldInGroup("pymnt_View_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pymnt_View_Pymnt_Check_Seq_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SEQ_NBR");
        registerRecord(vw_pymnt_View);

        pnd_Sk = localVariables.newGroupInRecord("pnd_Sk", "#SK");
        pnd_Sk_Pymnt_Check_Dte = pnd_Sk.newFieldInGroup("pnd_Sk_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Sk_Cntrct_Orgn_Cde = pnd_Sk.newFieldInGroup("pnd_Sk_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Sk_Cntrct_Check_Crrncy_Cde = pnd_Sk.newFieldInGroup("pnd_Sk_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Sk_Pymnt_Pay_Type_Req_Ind = pnd_Sk.newFieldInGroup("pnd_Sk_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Sk_Pymnt_Prcss_Seq_Nbr = pnd_Sk.newFieldInGroup("pnd_Sk_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Sk__R_Field_11 = localVariables.newGroupInRecord("pnd_Sk__R_Field_11", "REDEFINE", pnd_Sk);
        pnd_Sk_Pnd_Sk_All = pnd_Sk__R_Field_11.newFieldInGroup("pnd_Sk_Pnd_Sk_All", "#SK-ALL", FieldType.STRING, 17);

        pnd_Ck = localVariables.newGroupInRecord("pnd_Ck", "#CK");
        pnd_Ck_Pymnt_Check_Dte = pnd_Ck.newFieldInGroup("pnd_Ck_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ck_Cntrct_Orgn_Cde = pnd_Ck.newFieldInGroup("pnd_Ck_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ck_Cntrct_Check_Crrncy_Cde = pnd_Ck.newFieldInGroup("pnd_Ck_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Ck_Pymnt_Pay_Type_Req_Ind = pnd_Ck.newFieldInGroup("pnd_Ck_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ck_Pymnt_Prcss_Seq_Nbr = pnd_Ck.newFieldInGroup("pnd_Ck_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Ck__R_Field_12 = localVariables.newGroupInRecord("pnd_Ck__R_Field_12", "REDEFINE", pnd_Ck);
        pnd_Ck_Pnd_Ck_All = pnd_Ck__R_Field_12.newFieldInGroup("pnd_Ck_Pnd_Ck_All", "#CK-ALL", FieldType.STRING, 17);
        pnd_Save_Eft_Acct = localVariables.newFieldInRecord("pnd_Save_Eft_Acct", "#SAVE-EFT-ACCT", FieldType.STRING, 21);
        pnd_Check_Eft = localVariables.newFieldInRecord("pnd_Check_Eft", "#CHECK-EFT", FieldType.BOOLEAN, 1);
        pnd_Check_Rollover = localVariables.newFieldInRecord("pnd_Check_Rollover", "#CHECK-ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Sve_Institutioninfoline_Data = localVariables.newFieldArrayInRecord("pnd_Sve_Institutioninfoline_Data", "#SVE-INSTITUTIONINFOLINE-DATA", FieldType.STRING, 
            70, new DbsArrayController(1, 8));
        pnd_Check_Efthold = localVariables.newFieldInRecord("pnd_Check_Efthold", "#CHECK-EFTHOLD", FieldType.BOOLEAN, 1);
        pnd_Batch_Cnt_First = localVariables.newFieldInRecord("pnd_Batch_Cnt_First", "#BATCH-CNT-FIRST", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Key_OLD", "Pnd_Key_OLD", FieldType.STRING, 31);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_pymnt_View.reset();
        internalLoopRecord.reset();

        ldaFcplbar1.initializeValues();
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();

        localVariables.reset();
        pnd_Ws_Cntrct_Hold_Cde.setInitialValue("0000");
        pnd_Ws_Pnd_Monthly_Restart.setInitialValue(false);
        pnd_First_Record.setInitialValue(true);
        pnd_Last_Contract.setInitialValue(false);
        pnd_Last_Record.setInitialValue(false);
        pnd_Batch_Cnt_First.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp985() throws Exception
    {
        super("Fcpp985");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 15 ) LS = 133 PS = 58 ZP = ON;//Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 21T #FCPACRPT.#TITLE 68T 'REPORT: RPT0' / 36T #RUN-TYPE //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*  ********************************** FCPP985 STARTS HERE
        //*  **********************************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARM
        sub_Process_Input_Parm();
        if (condition(Global.isEscape())) {return;}
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 ) #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).setValue(true);
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #CHECK-SORT-FIELDS PYMNT-ADDR-INFO #CHECK-FIELDS INV-INFO ( * )
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa801b.getPnd_Check_Fields(), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            CheckAtStartofData923();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            //*  RESTART (MONTHLY RUN)
            //*  END OF RESTART
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                   //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #KEY-DETAIL
            //*     PERFORM CALC-SIMPLEX-DUPLEX
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF CNTRCT-PPCN-NBR
            //*                                                                                                                                                           //Natural: AT BREAK OF #CHECK-SORT-FIELDS.CNTRCT-COMPANY-CDE
            //*  IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = 'G   '                                                                                                            //Natural: AT BREAK OF CNTRCT-ANNTY-INS-TYPE
            //*     ESCAPE TOP
            //*  END-IF
            pnd_Pymnt.setValue(false);                                                                                                                                    //Natural: ASSIGN #PYMNT := FALSE
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                               //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := TRUE
            //*  LEON 08-06-99
            //*  FE201408
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S")  //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR' OR = 'AS' OR = 'AP'
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AS") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AP")))
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Pymnt.setValue(true);                                                                                                                                 //Natural: ASSIGN #PYMNT := TRUE
                //*  FE201703 END
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
            //*  AT BREAK OF #SIMPLEX
            //*    #FCPA803.#FULL-XEROX                := TRUE
            //*  END-BREAK
            //*  IF CNTRCT-PYMNT-TYPE-IND = 'C' AND
            //*     CNTRCT-STTLMNT-TYPE-IND = 'C'
            //*    RESET #CANADIAN-TAX
            //*    PERFORM GET-CANADIAN-TAX
            //*    IF #FOUND
            //*      WRITE '*** Canadian:' '=' #CAN-TAX-CNTRCT-CMBN-NBR
            //*        '=' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR /
            //*        '=' INV-ACCT-COUNT /
            //*        '=' WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE /
            //*        '=' #CAN-TAX-INV-ACCT-CAN-TAX-AMT(1:2) /
            //*        '=' INV-ACCT-SETTL-AMT(1:2) /
            //*        '=' INV-ACCT-FDRL-TAX-AMT (1:2) /
            //*        '=' INV-ACCT-STATE-TAX-AMT(1:2) /
            //*        '=' INV-ACCT-LOCAL-TAX-AMT(1:2) /
            //*        '=' INV-ACCT-NET-PYMNT-AMT(1:2)
            //*    ELSE
            //*      WRITE '$$$ Canadian:' '=' #CAN-TAX-CNTRCT-CMBN-NBR
            //*        '=' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
            //*        '=' WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE /
            //*    END-IF
            //*  END-IF
            //*  WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
            //*  IF #GLOBAL-PAY
            //*    WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR :=
            //*      #FCPL876.PYMNT-CHECK-SCRTY-NBR
            //*  ELSE
            //*    WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR   := #WS.PYMNT-CHECK-NBR
            pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr().setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                        //Natural: ASSIGN #CHECK-SORT-FIELDS.PYMNT-NBR := #WS.PYMNT-CHECK-NBR-N10
            //*  END-IF
            //*  PERFORM ACCUM-CONTROL-TOTALS
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(false);                                                                                              //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := FALSE
            if (condition(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde().equals("0000") && pnd_Ws_Cntrct_Hold_Cde.notEquals("0000")))                             //Natural: IF #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE = '0000' AND #WS.CNTRCT-HOLD-CDE NE '0000'
            {
                ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                          //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().nadd(1);                                                                                                      //Natural: ADD 1 TO #RECORD-IN-PYMNT
            //*  IF ROTH-MONEY-SOURCE = 'ROTHP'
            //*    WRITE '*********** ROTH ***********'
            //*    #ROTH-IND := TRUE
            //*  ELSE
            //*    RESET #ROTH-IND
            //*  END-IF
            //*   DO NOT RELEASE GLOBAL WIRES FOR PRINTING
            //*  GLOBAL
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals("G")))                                                                                //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = 'G'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*                                              END            6/21    MB
                //*    CALLNAT 'FCPN803B'
                //*      USING
                //*      #CHECK-SORT-FIELDS
                //*      FCPA110                /* RA
                //*      #CHECK-FIELDS
                //*      WF-PYMNT-ADDR-GRP(*)
                //*      #FCPA803
                //*      #FCPA803A
                //*      #FCPA803H
                //*      #FCPA803L
                //*      #BARCODE-LDA
                //*      #CANADIAN-TAX
                                                                                                                                                                          //Natural: PERFORM GENERATE-XML-LINES
                sub_Generate_Xml_Lines();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*      6/21    MB
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
            sub_Write_Output_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Cntrct_Hold_Cde.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde());                                                                      //Natural: ASSIGN #WS.CNTRCT-HOLD-CDE := #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE
            readWork01Pnd_KeyOld.setValue(pnd_Key);                                                                                                                       //Natural: AT END OF DATA;//Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            //*    PERFORM PRINT-VOID-PAGE
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean() || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals("G   ")                    //Natural: IF #GLOBAL-PAY OR WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE EQ 'G   ' OR #SVE-CNTRCT-HOLD-CDE EQ 'G   ' OR #BATCH-CNT LT 1
                || pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Hold_Cde.equals("G   ") || pnd_Batch_Cnt.less(1)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Last_Record.setValue(true);                                                                                                                           //Natural: ASSIGN #LAST-RECORD := TRUE
                pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(false);                                                                                                //Natural: ASSIGN #NEW-PYMNT := FALSE
                pnd_Sve_Wf_Pymnt_Addr_Grp.getValue("*").setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*"));                                                       //Natural: ASSIGN #SVE-WF-PYMNT-ADDR-GRP ( * ) := WF-PYMNT-ADDR-GRP ( * )
                //*      #SVE-CHECK-SORT-FIELDS    := #CHECK-SORT-FIELDS
                pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Hold_Cde.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde());                                       //Natural: ASSIGN #SVE-CNTRCT-HOLD-CDE := #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE
                pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Company_Cde.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde());                                 //Natural: ASSIGN #SVE-CNTRCT-COMPANY-CDE := CNTRCT-COMPANY-CDE
                pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Total_Pages.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Total_Pages());                                   //Natural: ASSIGN #SVE-PYMNT-TOTAL-PAGES := PYMNT-TOTAL-PAGES
                pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cnr_Orgnl_Invrse_Dte.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte());                             //Natural: ASSIGN #SVE-CNR-ORGNL-INVRSE-DTE := #CHECK-SORT-FIELDS.CNR-ORGNL-INVRSE-DTE
                pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr_Alpha.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr_Alpha());                                       //Natural: ASSIGN #SVE-PYMNT-NBR-ALPHA := PYMNT-NBR-ALPHA
                //*      #SVE-CHECK-FIELDS-GRP     := #CHECK-FIELDS-GRP
                pnd_Sve_Check_Fields_Pnd_Sve_Fund_On_Page.getValue("*").setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue("*"));                       //Natural: ASSIGN #SVE-FUND-ON-PAGE ( * ) := #FUND-ON-PAGE ( * )
                pnd_Sve_Check_Fields_Pnd_Sve_Ded_Pymnt_Table.getValue("*").setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue("*"));                 //Natural: ASSIGN #SVE-DED-PYMNT-TABLE ( * ) := #DED-PYMNT-TABLE ( * )
                pnd_Sve_Check_Fields_Pnd_Sve_Cntrct_Pnd__Ded.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded());                                             //Natural: ASSIGN #SVE-CNTRCT-#-DED := #CNTRCT-#-DED
                pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Records.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Records());                                                 //Natural: ASSIGN #SVE-PYMNT-RECORDS := #PYMNT-RECORDS
                pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Pnd.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Pnd());                                                 //Natural: ASSIGN #SVE-PYMNT-DED-# := #PYMNT-DED-#
                pnd_Sve_Check_Fields_Pnd_Sve_Dpi_Ind.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind().getBoolean());                                                //Natural: ASSIGN #SVE-DPI-IND := #DPI-IND
                pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean());                                    //Natural: ASSIGN #SVE-PYMNT-DED-IND := #PYMNT-DED-IND
                pnd_Sve_Check_Sort_Fields_Pnd_Sve_Filler.reset();                                                                                                         //Natural: RESET #SVE-FILLER #SVE-FILLER2
                pnd_Sve_Check_Fields_Pnd_Sve_Filler2.reset();
                                                                                                                                                                          //Natural: PERFORM GENERATE-XML-LINES
                sub_Generate_Xml_Lines();
                if (condition(Global.isEscape())) {return;}
                //*      PERFORM UPDATE-CHECK-NO
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        DbsUtil.callnat(Fcpn965.class , getCurrentProcessState(), pnd_Batch_Cnt);                                                                                         //Natural: CALLNAT 'FCPN965' USING #BATCH-CNT
        if (condition(Global.isEscape())) return;
        //*  PERFORM PRINT-CONTROL-REPORT
        //*  ---------------------------------------START--------- /* JWO 09/2012
        //*  #XEROX := #FCPAACUM.#PYMNT-CNT (1) - #CONVERT-TO-WIRE
        //*  WRITE (15) //
        //*  'Number of checks converted to global wires:' #CONVERT-TO-WIRE /
        //*  'Number of checks for Xerox reconciliation: ' #XEROX
        //*  ---------------------------------------END----------- /* JWO 09/2012
        //*  IF #FCPA803.#CSR-RUN
        //*   ADD  1                           TO #FCPL876.PYMNT-CHECK-NBR
        //*   ADD  1                           TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*   ADD  1                           TO #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*   WRITE *PROGRAM *TIME 'Counters etc, at end of program:'
        //*     / 'Next check  number...........:' #FCPL876.PYMNT-CHECK-NBR
        //*     / 'Next EFT    number...........:' #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*     / 'Next payment sequence number.:' #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*   #FCPL876.#BAR-BARCODE(*)         := #BARCODE-LDA.#BAR-BARCODE(*)
        //*   WRITE WORK FILE 7 #FCPL876
        //*  END-IF
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OUTPUT-FILE
        //* **********************************
        //* ***********************************************************************
        //*  DEFINE SUBROUTINE UPDATE-CHECK-NO
        //* ***********************************************************************
        //*  MOVE #PYMNT-CHECK-NBR-N10         TO FCPA110.FCPA110-NEW-CHECK-NBR
        //*  MOVE #FCPL876.PYMNT-CHECK-SEQ-NBR TO FCPA110.FCPA110-NEW-SEQUENCE-NBR
        //*  MOVE 'UPDATE CHK SEQ' TO FCPA110.FCPA110-FUNCTION
        //* *CALLNAT 'FCPN110' FCPA110 /* RL FEB 27 DO NOT UPDATE IN 1400 USE 876
        //*  FCPA110.FCPA110-RETURN-CODE := '0000' /* FE201610
        //*  IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        //*   IGNORE
        //*  ELSE
        //*   PERFORM ERROR-DISPLAY-START
        //*   WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG(EM=X(61)) 77T '***'
        //*     / FCPA110.FCPA110-RETURN-CODE 77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*   TERMINATE 53
        //*  END-IF
        //*  END-SUBROUTINE
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //*  WHEN #GLOBAL-PAY
        //*    ADD  1                          TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*    #STMNT-TEXT-IND                 := 2
        //*  WHEN ANY
        //*      WRITE 'TEST' '=' #GLOBAL-PAY
        //*       '=' #FCPA803.#ZERO-CHECK
        //*       '=' #FCPA803.#STMNT
        //*  COMPUTE #BAR-LAST-PAGE = (PYMNT-TOTAL-PAGES + 1 ) / 2 * 2 - 1
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-STMNT-TYPE
        //* *************************************
        //* ************************************
        //*  DEFINE SUBROUTINE CALC-SIMPLEX-DUPLEX
        //* ************************************
        //*  IF  PYMNT-TOTAL-PAGES = 1
        //*    #SIMPLEX                               := TRUE
        //*  ELSE
        //*    #SIMPLEX                               := FALSE
        //*  END-IF
        //*  END-SUBROUTINE
        //* ********************************
        //*  DEFINE SUBROUTINE PRINT-VOID-PAGE
        //* ********************************
        //*  IF #FCPA803.#CSR-RUN
        //*    IGNORE
        //*  ELSE
        //*   CALLNAT 'FCPN803V' USING #FCPA803
        //*  END-IF
        //*  END-SUBROUTINE
        //*  **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START-OF-PROCESSING
        //*  PERFORM CALC-SIMPLEX-DUPLEX
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARM
        //*    PERFORM GET-NEXT-CHECK-FILE
        //*    IF #RUN-TYPE = 'HELD' OR = 'HOLD'
        //*      IGNORE
        //*    ELSE
        //*      PERFORM GET-HOLD-CONTROL-RECORD
        //*    END-IF
        //* ************************************
        //*  DEFINE SUBROUTINE GET-NEXT-CHECK-FILE
        //* ************************************
        //*  READ WORK FILE 6 ONCE #FCPL876
        //*  AT END OF FILE
        //*   PERFORM ERROR-DISPLAY-START
        //*   WRITE '***' 25T 'MISSING "NEXT CHECK" FILE'            77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*   TERMINATE 54
        //*  END-ENDFILE
        //*  #BARCODE-LDA.#BAR-BARCODE (*) := #FCPL876.#BAR-BARCODE(*)
        //*  #BARCODE-LDA.#BAR-ENVELOPES   := #FCPL876.#BAR-ENVELOPES
        //*  #BARCODE-LDA.#BAR-NEW-RUN     := #FCPL876.#BAR-NEW-RUN
        //*  WRITE *PROGRAM *TIME 'at start of program:'
        //*    / 'The program is going to use the following:'
        //*    / 'start assigned check   number:' #FCPL876.PYMNT-CHECK-NBR
        //*    / 'start assigned EFT     number:' #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*    / 'start sequence number........:' #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*  SUBTRACT  1                 FROM #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*  SUBTRACT  1                 FROM #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*  SUBTRACT  1                 FROM #FCPL876.PYMNT-CHECK-NBR
        //*  CLOSE WORK 6
        //*  END-SUBROUTINE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DAILY-RUN-INFO
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-CHECK-NO
        //* *********** REMOVED MAY 6 2006 RL CHECK VS STMNT NBR DIFF *************
        //* *#PYMNT-CHECK-NBR-N10         := FCPA110.START-CHECK-NO
        //* *#FCPL876.PYMNT-CHECK-SEQ-NBR := FCPA110.START-SEQ-NO
        //* *#FCPL876.PYMNT-CHECK-SEQ-NBR        := FCPA110.START-SEQ-NO
        //* ****************************************
        //*  DEFINE SUBROUTINE GET-HOLD-CONTROL-RECORD
        //* ****************************************
        //*  READ WORK FILE 4 ONCE #FCPAACUM
        //*  AT END OF FILE
        //*   PERFORM ERROR-DISPLAY-START
        //*   WRITE '***' 25T 'MISSING "HOLD CHECK" CONTROL FILE' 77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*   TERMINATE 55
        //*  END-ENDFILE
        //*  END-SUBROUTINE
        //* *************************************
        //*  DEFINE SUBROUTINE PRINT-CONTROL-REPORT
        //* *************************************
        //*  IF #RUN-TYPE = 'HELD' OR = 'HOLD'         /* DAILY RUN
        //*  #FCPL876.#BAR-NEW-RUN     := TRUE
        //*  RESET                        #FCPL876.#BAR-ENVELOPES
        //*  WRITE WORK FILE 3 #FCPAACUM
        //*  ELSE
        //*   #FCPL876.#BAR-NEW-RUN     := #BARCODE-LDA.#BAR-NEW-RUN
        //*   #FCPL876.#BAR-ENVELOPES   := #BARCODE-LDA.#BAR-ENVELOPES
        //*   #FCPACRPT.#NO-ABEND       := FALSE
        //*   #FCPACRPT.#PROGRAM        := *PROGRAM
        //*  CALLNAT 'FCPNCRPT' USING #FCPAACUM #FCPACRPT /* FE201610
        //*  END-IF
        //*  END-SUBROUTINE
        //* **********************************
        //*   DEFINE SUBROUTINE GET-CANADIAN-TAX
        //* **********************************
        //*  NEW ROUTINE TO MATCH CANADIAN TAX TO BE STORED ON PAYMENT FILE
        //* *
        //*  RESET #CHK-KEY #CAN-TAX-KEY
        //*   #INV-ACCT-CAN-TAX-AMT(*)
        //*   #TOTAL-CAN-TAX-AMT
        //* ** BUILD KEY
        //* * #CHK-CNTRCT-ORGN-CDE    := WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE
        //* * #CHK-CNTRCT-INVRSE-DTE  := WF-PYMNT-ADDR-GRP.CNTRCT-INVRSE-DTE
        //* * #CHK-CNTRCT-CMBN-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR
        //* * #CHK-CNTRCT-CMPNY-CODE := WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY(1)
        //* *#CHK-PYMNT-PRCSS-SEQ-NBR := WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NBR
        //*  WRITE '=' #UPDT-KEY
        //* *#FOUND := FALSE
        //* * REPEAT.
        //* *REPEAT UNTIL #FOUND
        //* *  READ WORK FILE 10 ONCE #CANADIAN-TAX
        //*   AT END OF FILE
        //*     RESET #CANADIAN-TAX
        //*     ESCAPE BOTTOM(REPEAT.)
        //*   END-ENDFILE
        //*   MOVE  #CAN-TAX-CNTRCT-ORGN-CDE    TO #CAN-CNTRCT-ORGN-CDE
        //*   MOVE  #CAN-TAX-CNTRCT-INVRSE-DTE  TO #CAN-CNTRCT-INVRSE-DTE
        //*   MOVE  #CAN-TAX-CNTRCT-CMBN-NBR    TO #CAN-CNTRCT-CMBN-NBR
        //*   MOVE  #CAN-TAX-CNTRCT-CMPNY-CDE    TO #CAN-CNTRCT-CMPNY-CODE
        //*   MOVE  #CAN-TAX-PYMNT-PRCSS-SEQ-NUM TO #CAN-PYMNT-PRCSS-SEQ-NBR
        //*   DECIDE FOR FIRST CONDITION
        //*    WHEN #CHK-KEY > #CAN-TAX-KEY
        //*      WRITE 'Greater than - ignore:' '=' #CHK-KEY '=' #CAN-TAX-KEY
        //*      IGNORE                          /* IGNORE AND READ NEXT TAX RECORD
        //*    WHEN #CHK-KEY < #CAN-TAX-KEY
        //*      WRITE 'Less than - escape:' '=' #CHK-KEY '=' #CAN-TAX-KEY
        //*      ESCAPE ROUTINE                  /* NO TAX RECORD ON FILE
        //*     WHEN #CHK-KEY = #CAN-TAX-KEY   /* MATCH FOUND SO GET CANADIAN TAX
        //*       CLOSE WORK FILE 10
        //*       #FOUND := TRUE
        //*     WHEN NONE
        //*       IGNORE
        //*   END-DECIDE
        //*  END-REPEAT
        //*  END-SUBROUTINE
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-XML-LINES
        //*      #SVE-CHECK-SORT-FIELDS    := #CHECK-SORT-FIELDS
        //*      #SVE-CHECK-FIELDS-GRP     := #CHECK-FIELDS-GRP
        //*  ************************************
        //*  ************************************ PARALLEL MODE 201703 START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-NUMBER-DB196
        //*  ************************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Write_Output_File() throws Exception                                                                                                                 //Natural: WRITE-OUTPUT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Bar_Env_Intg_A_Pnd_Bar_Env_Intg.setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter());                                                     //Natural: ASSIGN #BAR-ENV-INTG := #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER
        if (condition(DbsUtil.maskMatches(pnd_Bar_Env_Intg_A,"N")))                                                                                                       //Natural: IF #BAR-ENV-INTG-A EQ MASK ( N )
        {
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Bar_Env_Integrity_Counter().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter());                        //Natural: ASSIGN BAR-ENV-INTEGRITY-COUNTER := #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Bar_Env_Integrity_Counter().setValue(0);                                                                                     //Natural: ASSIGN BAR-ENV-INTEGRITY-COUNTER := 0
        }                                                                                                                                                                 //Natural: END-IF
        //*  IF #GLOBAL-PAY
        //*   WRITE WORK FILE 9 VARIABLE
        //*     PYMNT-ADDR-INFO
        //*     INV-INFO(1:INV-ACCT-COUNT)
        //*  ELSE
        getWorkFiles().write(9, true, pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa801b.getPnd_Check_Fields(),      //Natural: WRITE WORK FILE 9 VARIABLE #CHECK-SORT-FIELDS PYMNT-ADDR-INFO #CHECK-FIELDS INV-INFO ( 1:INV-ACCT-COUNT )
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        //*  END-IF
    }
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals("G")))                                                                                    //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = 'G'
        {
            //*  JWO 09/2012
            pnd_Convert_To_Wire.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #CONVERT-TO-WIRE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-STMNT-TYPE
        sub_Determine_Stmnt_Type();
        if (condition(Global.isEscape())) {return;}
        //*  CORRESPONDENCE ADDRESS
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean() && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Lines().getValue(2).notEquals(" ")))              //Natural: IF #FCPA803.#STMNT AND PYMNT-ADDR-LINES ( 2 ) NE ' '
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(2);                                                                                                         //Natural: ASSIGN #ADDR-IND := 2
            //*  PAYMENT ADDRESS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(1);                                                                                                         //Natural: ASSIGN #ADDR-IND := 1
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nadd(1);                                                                                                          //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*  RA
        short decideConditionsMet1398 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FCPA803.#ZERO-CHECK
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().getBoolean()))
        {
            decideConditionsMet1398++;
            pnd_Ws_Pymnt_Check_Nbr.setValue(9999999);                                                                                                                     //Natural: ASSIGN #WS.PYMNT-CHECK-NBR := 9999999
            pnd_Ws_Pymnt_Check_Nbr_N10.setValue(9999999999L);                                                                                                             //Natural: ASSIGN #WS.PYMNT-CHECK-NBR-N10 := 9999999999
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Text_Ind().setValue(1);                                                                                                   //Natural: ASSIGN #STMNT-TEXT-IND := 1
        }                                                                                                                                                                 //Natural: WHEN NOT #FCPA803.#STMNT
        else if (condition(! ((pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean()))))
        {
            decideConditionsMet1398++;
            //*    ADD  1                          TO #FCPL876.PYMNT-CHECK-NBR
            //*    ADD  1                          TO #PYMNT-CHECK-NBR-N10     /* RA
            //*    #WS.PYMNT-CHECK-NBR-N10         := #PYMNT-CHECK-NBR-N10    /* RA
            //*    #WS.PYMNT-CHECK-NBR             := #FCPL876.PYMNT-CHECK-NBR
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().getBoolean()))                                                                                          //Natural: IF #FCPA803.#CSR-RUN
            {
                if (condition(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde().equals("0000")))                                                                     //Natural: IF #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE = '0000'
                {
                    ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                           //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                            //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                //*      #FCPL876A.CNTRCT-ORGN-CDE     := WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE
                //*      #FCPL876A.PYMNT-CHECK-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
                //*      #FCPL876A.PYMNT-CHECK-NBR     := #FCPL876.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().setValue(false);                                                                                                     //Natural: MOVE FALSE TO #END-OF-PYMNT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().reset();                                                                                                          //Natural: RESET #RECORD-IN-PYMNT
        pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(true);                                                                                                         //Natural: ASSIGN #NEW-PYMNT := TRUE
    }
    private void sub_Determine_Stmnt_Type() throws Exception                                                                                                              //Natural: DETERMINE-STMNT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := TRUE
        short decideConditionsMet1430 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 3 OR WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 4 OR WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 9
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4) 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9)))
        {
            decideConditionsMet1430++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().setValue(true);                                                                                                    //Natural: ASSIGN #GLOBAL-PAY := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#STMNT := TRUE
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(3);                                                                                                    //Natural: ASSIGN #ACCUM-OCCUR := 3
        }                                                                                                                                                                 //Natural: WHEN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT = 0.00
        else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(new DbsDecimal("0.00"))))
        {
            decideConditionsMet1430++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#ZERO-CHECK := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#STMNT := TRUE
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(15);                                                                                                   //Natural: ASSIGN #ACCUM-OCCUR := 15
        }                                                                                                                                                                 //Natural: WHEN PYMNT-CHECK-AMT NE 0.00
        else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().notEquals(new DbsDecimal("0.00"))))
        {
            decideConditionsMet1430++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().setValue(false);                                                                                                   //Natural: ASSIGN #FCPA803.#ZERO-CHECK := FALSE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(false);                                                                                                        //Natural: ASSIGN #FCPA803.#STMNT := FALSE
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(1);                                                                                                    //Natural: ASSIGN #ACCUM-OCCUR := 1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Start_Of_Processing() throws Exception                                                                                                               //Natural: START-OF-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        //*  PERFORM PRINT-VOID-PAGE
        //*  IF NOT #FCPA803.#CSR-RUN
        //*  SUBTRACT 1                           FROM #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*  SUBTRACT 1                           FROM #FCPL876.PYMNT-CHECK-NBR
        //*  SUBTRACT 1                           FROM #PYMNT-CHECK-NBR-N10 /* RA
        //*  END-IF
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
        sub_Init_New_Pymnt();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                        //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
    }
    private void sub_Process_Input_Parm() throws Exception                                                                                                                //Natural: PROCESS-INPUT-PARM
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("FCPP985|sub_Process_Input_Parm");
        while(true)
        {
            try
            {
                //* ***********************************
                if (condition(Global.getSTACK().getDatacount().notEquals(1)))                                                                                             //Natural: IF *DATA NE 1
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(0, "***",new TabSetting(25),"INPUT PARAMETER WAS NOT SUPPLIED",new TabSetting(77),"***");                                          //Natural: WRITE '***' 25T 'INPUT PARAMETER WAS NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    DbsUtil.terminate(50);  if (true) return;                                                                                                             //Natural: TERMINATE 50
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_File_Type);                                                                                 //Natural: INPUT #FILE-TYPE
                short decideConditionsMet1471 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #FILE-TYPE;//Natural: VALUE 'CHECK'
                if (condition((pnd_Ws_Pnd_File_Type.equals("CHECK"))))
                {
                    decideConditionsMet1471++;
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().setValue(false);                                                                                              //Natural: ASSIGN #FCPA803.#CSR-RUN := FALSE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1 ) := #FCPACRPT.#TRUTH-TABLE ( 15 ) := TRUE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(15).setValue(true);
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("   ANNUITY PAYMENTS CHECK STATEMENTS");                                                             //Natural: ASSIGN #FCPACRPT.#TITLE := '   ANNUITY PAYMENTS CHECK STATEMENTS'
                    //*    PERFORM GET-MONTHLY-RESTART-INFO   /* RA
                    //*  RL
                    pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P22A1");                                                                                        //Natural: MOVE 'P22A1' TO FCPA110.FCPA110-SOURCE-CODE
                    //*  HERE WE GET SEQ NO ALSO
                    //*  1400 RL
                                                                                                                                                                          //Natural: PERFORM GET-START-CHECK-NO
                    sub_Get_Start_Check_No();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter().setValue(9);                                                                           //Natural: ASSIGN #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER := 9
                }                                                                                                                                                         //Natural: VALUE 'GLOBAL'
                else if (condition((pnd_Ws_Pnd_File_Type.equals("GLOBAL"))))
                {
                    decideConditionsMet1471++;
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().setValue(false);                                                                                              //Natural: ASSIGN #FCPA803.#CSR-RUN := FALSE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(3).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 3 ) := TRUE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue(" ANNUITY PAYMENTS GLOBAL PAY STATEMENTS");                                                          //Natural: ASSIGN #FCPACRPT.#TITLE := ' ANNUITY PAYMENTS GLOBAL PAY STATEMENTS'
                    ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter().setValue(9);                                                                           //Natural: ASSIGN #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER := 9
                }                                                                                                                                                         //Natural: VALUE 'CSR'
                else if (condition((pnd_Ws_Pnd_File_Type.equals("CSR"))))
                {
                    decideConditionsMet1471++;
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().setValue(true);                                                                                               //Natural: ASSIGN #FCPA803.#CSR-RUN := TRUE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1 ) := #FCPACRPT.#TRUTH-TABLE ( 15 ) := TRUE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(15).setValue(true);
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("ANNUITY PAYMENTS CANCEL & REDRAW CHECKS");                                                          //Natural: ASSIGN #FCPACRPT.#TITLE := 'ANNUITY PAYMENTS CANCEL & REDRAW CHECKS'
                                                                                                                                                                          //Natural: PERFORM GET-DAILY-RUN-INFO
                    sub_Get_Daily_Run_Info();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  RL
                    pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                        //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
                    //*  GET BANK DATA FOR CHK PRINTING-RL
                                                                                                                                                                          //Natural: PERFORM GET-START-CHECK-NO
                    sub_Get_Start_Check_No();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(25),"INVALID PARAMETER:",pnd_Ws_Pnd_File_Type,new TabSetting(77),"***");                                   //Natural: WRITE '***' 25T 'INVALID PARAMETER:' #FILE-TYPE 77T '***'
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(51);  if (true) return;                                                                                                             //Natural: TERMINATE 51
                }                                                                                                                                                         //Natural: END-DECIDE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Daily_Run_Info() throws Exception                                                                                                                //Natural: GET-DAILY-RUN-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        getWorkFiles().read(2, pnd_Ws_Pnd_Run_Type);                                                                                                                      //Natural: READ WORK FILE 2 ONCE #RUN-TYPE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING PARAMETER FILE",new TabSetting(77),"***");                                                            //Natural: WRITE '***' 25T 'MISSING PARAMETER FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Get_Start_Check_No() throws Exception                                                                                                                //Natural: GET-START-CHECK-NO
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //*  READ WORK FILE 2 ONCE #INPUT-PARM
        //*  AT END OF FILE
        //*   PERFORM ERROR-DISPLAY-START
        //*   WRITE '***' 25T 'MISSING PARAMETER FILE' 77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*   TERMINATE 53
        //*  END-ENDFILE
        //*  --------------------------------------------------
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //* *MOVE  'P22A1' TO FCPA110.FCPA110-SOURCE-CODE  /* RL
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        //*  RL
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            pdaFcpa110.getFcpa110_Fcpa110_Old_Check_Nbr().setValue(pdaFcpa110.getFcpa110_Start_Check_No());                                                               //Natural: ASSIGN FCPA110.FCPA110-OLD-CHECK-NBR := FCPA110.START-CHECK-NO
            pnd_Ws_Next_Check_Prefix.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                             //Natural: ASSIGN #WS-NEXT-CHECK-PREFIX := FCPA110.START-CHECK-PREFIX-N3
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //*                                             /* RA
        //*  #FCPL876.PYMNT-CHECK-NBR                  := #INPUT-PARM-CHECK-NBR
        //*  #FCPL876.PYMNT-CHECK-SEQ-NBR              := #INPUT-PARM-CHECK-SEQ-NBR
        //*  IF #FCPL876.PYMNT-CHECK-SEQ-NBR = 1
        //*   #BARCODE-LDA.#BAR-NEW-RUN               := TRUE
        //*  ELSE
        //*   #MONTHLY-RESTART                        := TRUE
        //*  END-IF                                            /* RA
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                      //Natural: RESET #BAR-ENV-ID-NUM
    }
    private void sub_Generate_Xml_Lines() throws Exception                                                                                                                //Natural: GENERATE-XML-LINES
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_First_Record.getBoolean()))                                                                                                                     //Natural: IF #FIRST-RECORD
        {
            setValueToSubstring("005",pnd_Save_First_Twenty,1,3);                                                                                                         //Natural: MOVE '005' TO SUBSTR ( #SAVE-FIRST-TWENTY,01,03 )
            pnd_Save_First_Twenty_Documentrequestid_Yyyy.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYY"));                                                     //Natural: MOVE EDITED *TIMX ( EM = YYYY ) TO DOCUMENTREQUESTID-YYYY
            pnd_Save_First_Twenty_Documentrequestid_Mm.setValueEdited(Global.getTIMX(),new ReportEditMask("MM"));                                                         //Natural: MOVE EDITED *TIMX ( EM = MM ) TO DOCUMENTREQUESTID-MM
            pnd_Save_First_Twenty_Documentrequestid_Dd.setValueEdited(Global.getTIMX(),new ReportEditMask("DD"));                                                         //Natural: MOVE EDITED *TIMX ( EM = DD ) TO DOCUMENTREQUESTID-DD
            pnd_Save_First_Twenty_Documentrequestid_Hh.setValueEdited(Global.getTIMX(),new ReportEditMask("HH"));                                                         //Natural: MOVE EDITED *TIMX ( EM = HH ) TO DOCUMENTREQUESTID-HH
            pnd_Save_First_Twenty_Documentrequestid_Ii.setValueEdited(Global.getTIMX(),new ReportEditMask("II"));                                                         //Natural: MOVE EDITED *TIMX ( EM = II ) TO DOCUMENTREQUESTID-II
            pnd_Save_First_Twenty_Documentrequestid_Ss.setValueEdited(Global.getTIMX(),new ReportEditMask("SS"));                                                         //Natural: MOVE EDITED *TIMX ( EM = SS ) TO DOCUMENTREQUESTID-SS
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        //*  PARALLEL MODE FE201703
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-NUMBER-DB196
        sub_Get_Check_Number_Db196();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  FCPA873
        //*  RA
        DbsUtil.callnat(Fcpn985.class , getCurrentProcessState(), pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa110.getFcpa110(), pdaFcpa801b.getPnd_Check_Fields(),     //Natural: CALLNAT 'FCPN985' USING #CHECK-SORT-FIELDS FCPA110 #CHECK-FIELDS WF-PYMNT-ADDR-GRP ( * ) #FCPA803 #FCPA803A #FCPA803H #FCPA803L #BARCODE-LDA #CANADIAN-TAX #FILE-TYPE #BATCH-CNT #SAVE-FIRST-TWENTY #FIRST-RECORD #LAST-CONTRACT #LAST-RECORD #TOTALS-AREA #SVE-DOCUMENTREQUESTID-DATA #SAVE-EFT-ACCT #CHECK-EFT #CHECK-ROLLOVER #SVE-INSTITUTIONINFOLINE-DATA ( * ) #CHECK-EFTHOLD
            pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*"), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa803a.getPnd_Fcpa803a(), pdaFcpa803h.getPnd_Fcpa803h(), 
            pdaFcpa803l.getPnd_Fcpa803l(), ldaFcplbar1.getPnd_Barcode_Lda(), pdaFcpa800e.getPnd_Canadian_Tax(), pnd_Ws_Pnd_File_Type, pnd_Batch_Cnt, pnd_Save_First_Twenty, 
            pnd_First_Record, pnd_Last_Contract, pnd_Last_Record, pnd_Totals_Area, pnd_Sve_Documentrequestid_Data, pnd_Save_Eft_Acct, pnd_Check_Eft, pnd_Check_Rollover, 
            pnd_Sve_Institutioninfoline_Data.getValue("*"), pnd_Check_Efthold);
        if (condition(Global.isEscape())) return;
        pnd_Sve_Wf_Pymnt_Addr_Grp.getValue("*").setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*"));                                                               //Natural: ASSIGN #SVE-WF-PYMNT-ADDR-GRP ( * ) := WF-PYMNT-ADDR-GRP ( * )
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Hold_Cde.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde());                                               //Natural: ASSIGN #SVE-CNTRCT-HOLD-CDE := #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cntrct_Company_Cde.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde());                                         //Natural: ASSIGN #SVE-CNTRCT-COMPANY-CDE := CNTRCT-COMPANY-CDE
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Total_Pages.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Total_Pages());                                           //Natural: ASSIGN #SVE-PYMNT-TOTAL-PAGES := PYMNT-TOTAL-PAGES
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Cnr_Orgnl_Invrse_Dte.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte());                                     //Natural: ASSIGN #SVE-CNR-ORGNL-INVRSE-DTE := #CHECK-SORT-FIELDS.CNR-ORGNL-INVRSE-DTE
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Pymnt_Nbr_Alpha.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr_Alpha());                                               //Natural: ASSIGN #SVE-PYMNT-NBR-ALPHA := PYMNT-NBR-ALPHA
        pnd_Sve_Check_Fields_Pnd_Sve_Fund_On_Page.getValue("*").setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue("*"));                               //Natural: ASSIGN #SVE-FUND-ON-PAGE ( * ) := #FUND-ON-PAGE ( * )
        pnd_Sve_Check_Fields_Pnd_Sve_Ded_Pymnt_Table.getValue("*").setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue("*"));                         //Natural: ASSIGN #SVE-DED-PYMNT-TABLE ( * ) := #DED-PYMNT-TABLE ( * )
        pnd_Sve_Check_Fields_Pnd_Sve_Cntrct_Pnd__Ded.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded());                                                     //Natural: ASSIGN #SVE-CNTRCT-#-DED := #CNTRCT-#-DED
        pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Records.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Records());                                                         //Natural: ASSIGN #SVE-PYMNT-RECORDS := #PYMNT-RECORDS
        pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Pnd.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Pnd());                                                         //Natural: ASSIGN #SVE-PYMNT-DED-# := #PYMNT-DED-#
        pnd_Sve_Check_Fields_Pnd_Sve_Dpi_Ind.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind().getBoolean());                                                        //Natural: ASSIGN #SVE-DPI-IND := #DPI-IND
        pnd_Sve_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind.setValue(pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean());                                            //Natural: ASSIGN #SVE-PYMNT-DED-IND := #PYMNT-DED-IND
        pnd_Sve_Check_Sort_Fields_Pnd_Sve_Filler.reset();                                                                                                                 //Natural: RESET #SVE-FILLER #SVE-FILLER2
        pnd_Sve_Check_Fields_Pnd_Sve_Filler2.reset();
        if (condition(pnd_First_Record.getBoolean()))                                                                                                                     //Natural: IF #FIRST-RECORD
        {
            pnd_First_Record.setValue(false);                                                                                                                             //Natural: ASSIGN #FIRST-RECORD := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GENERATE-XML-LINES
    }
    private void sub_Get_Check_Number_Db196() throws Exception                                                                                                            //Natural: GET-CHECK-NUMBER-DB196
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Sk.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                                                                     //Natural: MOVE BY NAME WF-PYMNT-ADDR-REC TO #SK
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals("G")))                                                                                    //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = 'G'
        {
            pnd_Sk_Pymnt_Pay_Type_Req_Ind.setValue(7);                                                                                                                    //Natural: MOVE 7 TO #SK.PYMNT-PAY-TYPE-REQ-IND
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Pymnt_Not_Found.setValue(true);                                                                                                                               //Natural: ASSIGN #PYMNT-NOT-FOUND := TRUE
        vw_pymnt_View.startDatabaseRead                                                                                                                                   //Natural: READ PYMNT-VIEW BY CHK-DTE-ORG-CURR-TYP-SEQ STARTING FROM #SK-ALL
        (
        "READ_PYMNTS",
        new Wc[] { new Wc("CHK_DTE_ORG_CURR_TYP_SEQ", ">=", pnd_Sk_Pnd_Sk_All.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CHK_DTE_ORG_CURR_TYP_SEQ", "ASC") }
        );
        READ_PYMNTS:
        while (condition(vw_pymnt_View.readNextRow("READ_PYMNTS")))
        {
            //*  WRITE / '********** From FCPP985 #SK-ALL  ' /* TESTING ONLY START
            //*      WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
            //*  / '=' #SK.PYMNT-CHECK-DTE(EM=YYYY/MM/DD)
            //*  / '=' #SK.CNTRCT-ORGN-CDE
            //*  / '=' #SK.CNTRCT-CHECK-CRRNCY-CDE
            //*  / '=' #SK.PYMNT-PAY-TYPE-REQ-IND
            //*  / '=' #SK.PYMNT-PRCSS-SEQ-NBR
            //*  /  '***** FCP-CONS-PYMNT *******************'
            //*  / '='    PYMNT-VIEW.PYMNT-CHECK-DTE(EM=YYYY/MM/DD)
            //*  / '='    PYMNT-VIEW.CNTRCT-ORGN-CDE
            //*  / '='    PYMNT-VIEW.CNTRCT-CHECK-CRRNCY-CDE
            //*  / '='    PYMNT-VIEW.PYMNT-PAY-TYPE-REQ-IND
            //*  / '='    PYMNT-VIEW.PYMNT-PRCSS-SEQ-NBR    /* TESTING END
            pnd_Ck.setValuesByName(vw_pymnt_View);                                                                                                                        //Natural: MOVE BY NAME PYMNT-VIEW TO #CK
            if (condition(pnd_Ck_Pnd_Ck_All.notEquals(pnd_Sk_Pnd_Sk_All)))                                                                                                //Natural: IF #CK-ALL NE #SK-ALL
            {
                //*      WRITE ' Will escape bottom ' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
                if (true) break READ_PYMNTS;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-PYMNTS. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pymnt_Not_Found.setValue(false);                                                                                                                          //Natural: ASSIGN #PYMNT-NOT-FOUND := FALSE
            //*  ADD 1 TO #WS.#CNT-WAITING-FOR-ET /* REC READ WITH UPD INTENT
            //*  IF PYMNT-VIEW.PYMNT-INSTMT-NBR EQ 1
            //*    ADD 1 TO #WS.#CNT-UPD-PYMNTS
            //*        COMPUTE FCP-CONS-PYMNU.PYMNT-CHECK-NBR =
            //*          WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR * -1
            if (condition(pnd_Pymnt.getBoolean() && ! (pnd_Pymnt_Not_Found.getBoolean())))                                                                                //Natural: IF #PYMNT AND NOT #PYMNT-NOT-FOUND
            {
                pnd_Ws_Pymnt_Check_Nbr_N10.setValue(pymnt_View_Pymnt_Nbr);                                                                                                //Natural: ASSIGN #WS.PYMNT-CHECK-NBR-N10 := #PYMNT-CHECK-NBR-N10 := PYMNT-VIEW.PYMNT-NBR
                pnd_Pymnt_Check_Nbr_N10.setValue(pymnt_View_Pymnt_Nbr);
                pnd_Ws_Pymnt_Check_Nbr.setValue(pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7);                                                                          //Natural: ASSIGN #WS.PYMNT-CHECK-NBR := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR := #PYMNT-CHECK-NBR-N7
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().setValue(pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7);
                //*       READ-PYMNTS.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: END-IF
            //*       WRITE ' WILL PROCESS THIS CNTR ='           /* TEST ONLY START
            //*         WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
            //*  /  '='   WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
            //*  /  '='   WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR /* TEST ONLY END
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET-CHECK-NUMBER-DB196
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_NbrIsBreak = pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().isBreak(endOfData);
        boolean pdaFcpa803c_getPnd_Check_Sort_Fields_Cntrct_Company_CdeIsBreak = pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde().isBreak(endOfData);
        boolean pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_TypeIsBreak = pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().isBreak(endOfData);
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        if (condition(pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_NbrIsBreak || pdaFcpa803c_getPnd_Check_Sort_Fields_Cntrct_Company_CdeIsBreak || pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_TypeIsBreak 
            || pnd_KeyIsBreak))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Ppcn_Break().setValue(true);                                                                                                    //Natural: ASSIGN #PPCN-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpa803c_getPnd_Check_Sort_Fields_Cntrct_Company_CdeIsBreak || pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_TypeIsBreak 
            || pnd_KeyIsBreak))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Company_Break().setValue(true);                                                                                                 //Natural: ASSIGN #COMPANY-BREAK := TRUE
            //*  SPIA PS-SELECT CHECK
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_TypeIsBreak || pnd_KeyIsBreak))
        {
            if (condition(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde().equals("L") && (pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S")  //Natural: IF #CHECK-SORT-FIELDS.CNTRCT-COMPANY-CDE = 'L' AND ( CNTRCT-ANNTY-INS-TYPE = 'S' OR CNTRCT-ANNTY-INS-TYPE = 'M' )
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M"))))
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Company_Break().setValue(true);                                                                                             //Natural: ASSIGN #COMPANY-BREAK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  FE201703 START
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_KeyIsBreak))
        {
            //*  NOT EOF
            if (condition(pnd_Key.notEquals(readWork01Pnd_KeyOld)))                                                                                                       //Natural: IF #KEY NE OLD ( #KEY )
            {
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().notEquals("G   ")))                                                                      //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE NE 'G   '
                {
                    if (condition(! (pnd_First_Record.getBoolean())))                                                                                                     //Natural: IF NOT #FIRST-RECORD
                    {
                        pnd_Last_Contract.setValue(true);                                                                                                                 //Natural: ASSIGN #LAST-CONTRACT := TRUE
                        pnd_Sve2_Wf_Pymnt_Addr_Grp.getValue("*").setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*"));                                              //Natural: ASSIGN #SVE2-WF-PYMNT-ADDR-GRP ( * ) := WF-PYMNT-ADDR-GRP ( * )
                        pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*").setValue(pnd_Sve_Wf_Pymnt_Addr_Grp.getValue("*"));                                               //Natural: ASSIGN WF-PYMNT-ADDR-GRP ( * ) := #SVE-WF-PYMNT-ADDR-GRP ( * )
                        //*      PERFORM GET-CHECK-NUMBER-DB196   /* PARALLEL MODE FE201703
                        if (condition(pnd_Batch_Cnt_First.getBoolean()))                                                                                                  //Natural: IF #BATCH-CNT-FIRST
                        {
                            pnd_Batch_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #BATCH-CNT
                            pnd_Batch_Cnt_First.setValue(false);                                                                                                          //Natural: ASSIGN #BATCH-CNT-FIRST := FALSE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  PROCESS TRAILER RECORDS
                        //*  FCPA873
                        //*  RA
                        DbsUtil.callnat(Fcpn985.class , getCurrentProcessState(), pnd_Sve_Check_Sort_Fields, pdaFcpa110.getFcpa110(), pnd_Sve_Check_Fields,               //Natural: CALLNAT 'FCPN985' USING #SVE-CHECK-SORT-FIELDS FCPA110 #SVE-CHECK-FIELDS WF-PYMNT-ADDR-GRP ( * ) #FCPA803 #FCPA803A #FCPA803H #FCPA803L #BARCODE-LDA #CANADIAN-TAX #FILE-TYPE #BATCH-CNT #SAVE-FIRST-TWENTY #FIRST-RECORD #LAST-CONTRACT #LAST-RECORD #TOTALS-AREA #SVE-DOCUMENTREQUESTID-DATA #SAVE-EFT-ACCT #CHECK-EFT #CHECK-ROLLOVER #SVE-INSTITUTIONINFOLINE-DATA ( * ) #CHECK-EFTHOLD
                            pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*"), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa803a.getPnd_Fcpa803a(), pdaFcpa803h.getPnd_Fcpa803h(), 
                            pdaFcpa803l.getPnd_Fcpa803l(), ldaFcplbar1.getPnd_Barcode_Lda(), pdaFcpa800e.getPnd_Canadian_Tax(), pnd_Ws_Pnd_File_Type, pnd_Batch_Cnt, 
                            pnd_Save_First_Twenty, pnd_First_Record, pnd_Last_Contract, pnd_Last_Record, pnd_Totals_Area, pnd_Sve_Documentrequestid_Data, 
                            pnd_Save_Eft_Acct, pnd_Check_Eft, pnd_Check_Rollover, pnd_Sve_Institutioninfoline_Data.getValue("*"), pnd_Check_Efthold);
                        if (condition(Global.isEscape())) return;
                        pnd_Last_Contract.setValue(false);                                                                                                                //Natural: ASSIGN #LAST-CONTRACT := FALSE
                        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().greater(getZero())))                                                             //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT GT 0
                        {
                            pnd_Batch_Cnt.nadd(1);                                                                                                                        //Natural: ADD 1 TO #BATCH-CNT
                        }                                                                                                                                                 //Natural: END-IF
                        pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*").setValue(pnd_Sve2_Wf_Pymnt_Addr_Grp.getValue("*"));                                              //Natural: ASSIGN WF-PYMNT-ADDR-GRP ( * ) := #SVE2-WF-PYMNT-ADDR-GRP ( * )
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                        sub_Init_New_Pymnt();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* RA
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(15, "LS=133 PS=58 ZP=ON");

        getReports().write(0, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(21),pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title(),new TabSetting(68),"REPORT: RPT0",NEWLINE,new 
            TabSetting(36),pnd_Ws_Pnd_Run_Type,NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData923() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pdaFcpa803.getPnd_Fcpa803().setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                              //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #FCPA803
            //*    IF NOT #MONTHLY-RESTART           /* RA
                                                                                                                                                                          //Natural: PERFORM START-OF-PROCESSING
            sub_Start_Of_Processing();
            if (condition(Global.isEscape())) {return;}
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                   //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #KEY-DETAIL
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals("G   ") || pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))                  //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE EQ 'G   ' OR #GLOBAL-PAY
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Batch_Cnt.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #BATCH-CNT
                pnd_Batch_Cnt_First.setValue(false);                                                                                                                      //Natural: ASSIGN #BATCH-CNT-FIRST := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
