/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:32 PM
**        * FROM NATURAL PROGRAM : Fcpp970
************************************************************
**        * FILE NAME            : Fcpp970.java
**        * CLASS NAME           : Fcpp970
**        * INSTANCE NAME        : Fcpp970
************************************************************
***********************************************************************
* PROGRAM:  FCPP970
*
* TITLE:    CPS DC XML STATEMENT GENERATION
* AUTHOR:   FRANCIS ENDAYA
* DESC:     THE PROGRAM READS CHECK FILE AND CREATE XML FILE TO BE SENT
*           OVER TO CCP FOR CHECK CREATION FOR DC CHECKS AND EFT.
************************************************************************
* HISTORY
************************************************************************
*
*  NOTES:
*  ******
*  THIS PROGRAM IS MODELLED AFTER "FCPP374"
* IA DEATH SETTLEMENTS ARE USING PROTOTYPE#4
* 2017/08/12 F.ENDAYA  PIN EXPANSION. FE201708
* ----------------------------------------------------------------------

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp970 extends BLNatBase
{
    // Data Areas
    private PdaFcpa110 pdaFcpa110;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;
    private LdaFcpl876b ldaFcpl876b;
    private LdaFcplbar1 ldaFcplbar1;
    private PdaFcpa873 pdaFcpa873;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa874a pdaFcpa874a;
    private PdaFcpa874h pdaFcpa874h;
    private PdaFcpa803l pdaFcpa803l;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Cntl_Idx;
    private DbsField pnd_Ws_Pnd_Bar_Name;
    private DbsField pnd_Ws_Pnd_Check_Ind;
    private DbsField pnd_Ws_Pnd_Statement_Ind;
    private DbsField pnd_Ws_Pnd_Bar_Sub;
    private DbsField pnd_Program;
    private DbsField pnd_Abend_Cde;
    private DbsField pnd_Prime_Counter;
    private DbsField pnd_C_Rcrd_10;
    private DbsField pnd_C_Rcrd_20;
    private DbsField pnd_C_Rcrd_30;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_Ws_First_Time_In;
    private DbsField pnd_Ws_Next_Check_Nbr;
    private DbsField pnd_Ws_Next_Check_Prefix;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_1;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Next_Eft_Nbr;
    private DbsField pnd_Ws_Next_Global_Nbr;
    private DbsField pnd_Ws_Current_Pymnt_Id;
    private DbsField pnd_Ws_Current_Pymnt_Seq_Nbr;
    private DbsField pnd_Ws_Current_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Current_Cntrct_Type_Cde;
    private DbsField pnd_Ws_One;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Save_S_D_M_Plex;
    private DbsField pnd_Ws_Format_Notification_Letter;
    private DbsField pnd_Ws_Bottom_Part;
    private DbsField pnd_Ws_Jde;
    private DbsField pnd_Ws_Format;
    private DbsField pnd_Ws_Forms;
    private DbsField pnd_Ws_Feed;
    private DbsField pnd_Ws_Max;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;

    private DbsGroup pnd_Ws_Work_Rec_Record;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes;

    private DbsGroup pnd_Ws_Work_Rec_Record__R_Field_2;

    private DbsGroup pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr;

    private DbsGroup pnd_Ws_Work_Rec_Record__R_Field_3;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Invrse_Dte;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_A;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_B;
    private DbsField pnd_Ws_Header_Record;

    private DbsGroup pnd_Ws_Header_Record__R_Field_4;

    private DbsGroup pnd_Ws_Header_Record_Pnd_Ws_Header_Level2;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Header_Record__R_Field_5;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Header_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Header_Record_Filler;
    private DbsField pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Insurance_Option;
    private DbsField pnd_Ws_Header_Record_Cntrct_Life_Contingency;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;

    private DbsGroup pnd_Ws_Header_Record__R_Field_6;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes;
    private DbsField pnd_Ws_Occurs;

    private DbsGroup pnd_Ws_Occurs__R_Field_7;

    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Side;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fed_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Occurs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Occurs_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Deductions;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Amt;

    private DbsGroup pnd_Ws_Occurs_Pnd_Pyhdr_Data;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte;

    private DbsGroup pnd_Ws_Occurs__R_Field_8;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler;

    private DbsGroup pnd_Ws_Name_N_Address_Record;
    private DbsField pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes;

    private DbsGroup pnd_Ws_Name_N_Address_Record__R_Field_9;

    private DbsGroup pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2;
    private DbsField pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_A;

    private DbsGroup pnd_Ws_Name_N_Address_Record_Nme_N_Addr;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Nme;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_B;
    private DbsField pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_C;
    private DbsField pnd_First_Page;
    private DbsField pnd_Last_Page;
    private DbsField pnd_Batch_Cnt;
    private DbsField pnd_First_Record;
    private DbsField pnd_Last_Contract;
    private DbsField pnd_Last_Record;
    private DbsField pnd_Save_First_Twenty;

    private DbsGroup pnd_Save_First_Twenty__R_Field_10;

    private DbsGroup pnd_Save_First_Twenty_Documentrequestid_Data;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Sys_Id;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Yyyy;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Mm;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Dd;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Hh;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Ii;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Ss;

    private DataAccessProgramView vw_fcp_Cons_Pymnu;
    private DbsField fcp_Cons_Pymnu_Pymnt_Nbr;
    private DbsField fcp_Cons_Pymnu_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnu_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnu_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnu_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnu_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde;
    private DbsField fcp_Cons_Pymnu_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind;
    private DbsField fcp_Cons_Pymnu_Cntrct_Crrncy_Cde;
    private DbsField fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup fcp_Cons_Pymnu__R_Field_11;
    private DbsField fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Pymnu_Pymnt_Instmt_Nbr;
    private DbsField fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr;
    private DbsField fcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Key;

    private DbsGroup pnd_Ws_Key__R_Field_12;

    private DbsGroup pnd_Ws_Key_Pnd_Ws_Key_Level2;
    private DbsField pnd_Ws_Key_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Key_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Key__R_Field_13;
    private DbsField pnd_Ws_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Invalid_Rec;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(localVariables);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);
        ldaFcpl876b = new LdaFcpl876b();
        registerRecord(ldaFcpl876b);
        ldaFcplbar1 = new LdaFcplbar1();
        registerRecord(ldaFcplbar1);
        pdaFcpa873 = new PdaFcpa873(localVariables);
        pdaFcpa803 = new PdaFcpa803(localVariables);
        pdaFcpa874a = new PdaFcpa874a(localVariables);
        pdaFcpa874h = new PdaFcpa874h(localVariables);
        pdaFcpa803l = new PdaFcpa803l(localVariables);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Cntl_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntl_Idx", "#CNTL-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Bar_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Bar_Name", "#BAR-NAME", FieldType.STRING, 38);
        pnd_Ws_Pnd_Check_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Check_Ind", "#CHECK-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Statement_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Statement_Ind", "#STATEMENT-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Bar_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Bar_Sub", "#BAR-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);
        pnd_Prime_Counter = localVariables.newFieldInRecord("pnd_Prime_Counter", "#PRIME-COUNTER", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Rcrd_10 = localVariables.newFieldInRecord("pnd_C_Rcrd_10", "#C-RCRD-10", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Rcrd_20 = localVariables.newFieldInRecord("pnd_C_Rcrd_20", "#C-RCRD-20", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Rcrd_30 = localVariables.newFieldInRecord("pnd_C_Rcrd_30", "#C-RCRD-30", FieldType.PACKED_DECIMAL, 7);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_Ws_First_Time_In = localVariables.newFieldInRecord("pnd_Ws_First_Time_In", "#WS-FIRST-TIME-IN", FieldType.STRING, 1);
        pnd_Ws_Next_Check_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Nbr", "#WS-NEXT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Check_Prefix = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Prefix", "#WS-NEXT-CHECK-PREFIX", FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_1", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Next_Eft_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Eft_Nbr", "#WS-NEXT-EFT-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Global_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Global_Nbr", "#WS-NEXT-GLOBAL-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Current_Pymnt_Id = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Id", "#WS-CURRENT-PYMNT-ID", FieldType.NUMERIC, 7);
        pnd_Ws_Current_Pymnt_Seq_Nbr = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Seq_Nbr", "#WS-CURRENT-PYMNT-SEQ-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Current_Pymnt_Prcss_Seq_Num = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Prcss_Seq_Num", "#WS-CURRENT-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Current_Cntrct_Type_Cde = localVariables.newFieldInRecord("pnd_Ws_Current_Cntrct_Type_Cde", "#WS-CURRENT-CNTRCT-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Ws_One = localVariables.newFieldInRecord("pnd_Ws_One", "#WS-ONE", FieldType.PACKED_DECIMAL, 1);
        pnd_Ws_Side_Printed = localVariables.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Cntr_Inv_Acct = localVariables.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Sim_Dup_Multiplex_Written = localVariables.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Save_S_D_M_Plex = localVariables.newFieldInRecord("pnd_Ws_Save_S_D_M_Plex", "#WS-SAVE-S-D-M-PLEX", FieldType.STRING, 1);
        pnd_Ws_Format_Notification_Letter = localVariables.newFieldInRecord("pnd_Ws_Format_Notification_Letter", "#WS-FORMAT-NOTIFICATION-LETTER", FieldType.STRING, 
            1);
        pnd_Ws_Bottom_Part = localVariables.newFieldInRecord("pnd_Ws_Bottom_Part", "#WS-BOTTOM-PART", FieldType.STRING, 1);
        pnd_Ws_Jde = localVariables.newFieldInRecord("pnd_Ws_Jde", "#WS-JDE", FieldType.STRING, 3);
        pnd_Ws_Format = localVariables.newFieldInRecord("pnd_Ws_Format", "#WS-FORMAT", FieldType.STRING, 8);
        pnd_Ws_Forms = localVariables.newFieldInRecord("pnd_Ws_Forms", "#WS-FORMS", FieldType.STRING, 6);
        pnd_Ws_Feed = localVariables.newFieldInRecord("pnd_Ws_Feed", "#WS-FEED", FieldType.STRING, 6);
        pnd_Ws_Max = localVariables.newFieldInRecord("pnd_Ws_Max", "#WS-MAX", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);

        pnd_Ws_Work_Rec_Record = localVariables.newGroupInRecord("pnd_Ws_Work_Rec_Record", "#WS-WORK-REC-RECORD");
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes = pnd_Ws_Work_Rec_Record.newFieldArrayInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes", "#WS-WORK-REC-BYTES", 
            FieldType.STRING, 1, new DbsArrayController(1, 500));

        pnd_Ws_Work_Rec_Record__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Work_Rec_Record__R_Field_2", "REDEFINE", pnd_Ws_Work_Rec_Record);

        pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key = pnd_Ws_Work_Rec_Record__R_Field_2.newGroupInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key", "#WS-WORK-REC-KEY");
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num", 
            "#WS-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code", 
            "#WS-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);

        pnd_Ws_Work_Rec_Record__R_Field_3 = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newGroupInGroup("pnd_Ws_Work_Rec_Record__R_Field_3", "REDEFINE", 
            pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Invrse_Dte = pnd_Ws_Work_Rec_Record__R_Field_3.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Invrse_Dte", "#WS-INVRSE-DTE", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_A = pnd_Ws_Work_Rec_Record__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_A", "#WS-FILLER-A", 
            FieldType.STRING, 217);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_B = pnd_Ws_Work_Rec_Record__R_Field_2.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_B", "#WS-FILLER-B", 
            FieldType.STRING, 242);
        pnd_Ws_Header_Record = localVariables.newFieldArrayInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD", FieldType.STRING, 1, new DbsArrayController(1, 
            500));

        pnd_Ws_Header_Record__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Header_Record__R_Field_4", "REDEFINE", pnd_Ws_Header_Record);

        pnd_Ws_Header_Record_Pnd_Ws_Header_Level2 = pnd_Ws_Header_Record__R_Field_4.newGroupInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Level2", "#WS-HEADER-LEVEL2");
        pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", 
            "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", 
            "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", 
            "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", 
            "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", 
            "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", 
            "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", 
            "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", 
            "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", 
            "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", 
            "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", 
            "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", 
            "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", 
            "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", 
            "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", 
            "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", 
            "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", 
            "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", 
            "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", 
            "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", 
            "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", 
            "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Ws_Header_Record__R_Field_5 = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newGroupInGroup("pnd_Ws_Header_Record__R_Field_5", "REDEFINE", pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record__R_Field_5.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record__R_Field_5.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", 
            "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", 
            "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", 
            "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", 
            "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", 
            "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time", 
            "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Ws_Header_Record_Pymnt_Intrfce_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Filler", "FILLER", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte", 
            "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type", 
            "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde", 
            "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Insurance_Option = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Insurance_Option", 
            "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Life_Contingency = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Life_Contingency", 
            "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", 
            "#WS-HEADER-FILLER", FieldType.STRING, 117);

        pnd_Ws_Header_Record__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Header_Record__R_Field_6", "REDEFINE", pnd_Ws_Header_Record);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes = pnd_Ws_Header_Record__R_Field_6.newFieldArrayInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes", "#WS-HEADER-BYTES", 
            FieldType.STRING, 1, new DbsArrayController(1, 500));
        pnd_Ws_Occurs = localVariables.newFieldArrayInRecord("pnd_Ws_Occurs", "#WS-OCCURS", FieldType.STRING, 1, new DbsArrayController(1, 40, 1, 500));

        pnd_Ws_Occurs__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_Occurs__R_Field_7", "REDEFINE", pnd_Ws_Occurs);

        pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2 = pnd_Ws_Occurs__R_Field_7.newGroupArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2", "#WS-OCCURS-LEVEL2", 
            new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Ws_Side = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Side", "#WS-SIDE", FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Ws_Occurs_Inv_Acct_Unit_Value = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9, 4);
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_State_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Local_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Occurs_Cntrct_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Option_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Occurs_Cntrct_Mode_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Cntr_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Occurs_Pymnt_Ded_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 10));

        pnd_Ws_Occurs_Pnd_Pyhdr_Data = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newGroupInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Data", "#PYHDR-DATA");
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte", "#PYHDR-PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);

        pnd_Ws_Occurs__R_Field_8 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs__R_Field_8", "REDEFINE", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4 = pnd_Ws_Occurs__R_Field_8.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4", "#PYHDR-PYMNT-SETTLMNT-DTE-A4", 
            FieldType.STRING, 4);
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde", "#PYHDR-ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde", "#PYHDR-ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler", "#WS-INV-ACCT-FILLER", 
            FieldType.STRING, 180);

        pnd_Ws_Name_N_Address_Record = localVariables.newGroupInRecord("pnd_Ws_Name_N_Address_Record", "#WS-NAME-N-ADDRESS-RECORD");
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes = pnd_Ws_Name_N_Address_Record.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes", 
            "#WS-NAME-N-ADDRESS-BYTES", FieldType.STRING, 1, new DbsArrayController(1, 2, 1, 500));

        pnd_Ws_Name_N_Address_Record__R_Field_9 = localVariables.newGroupInRecord("pnd_Ws_Name_N_Address_Record__R_Field_9", "REDEFINE", pnd_Ws_Name_N_Address_Record);

        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2 = pnd_Ws_Name_N_Address_Record__R_Field_9.newGroupArrayInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2", 
            "#WS-NAME-N-ADDR-LEVEL2", new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_A = pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_A", 
            "#WS-FILLER-A", FieldType.STRING, 111);

        pnd_Ws_Name_N_Address_Record_Nme_N_Addr = pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2.newGroupInGroup("pnd_Ws_Name_N_Address_Record_Nme_N_Addr", 
            "NME-N-ADDR");
        pnd_Ws_Name_N_Address_Record_Pymnt_Nme = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Nme", "PYMNT-NME", 
            FieldType.STRING, 38);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line1_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line1_Txt", 
            "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line2_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line2_Txt", 
            "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line3_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line3_Txt", 
            "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line4_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line4_Txt", 
            "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line5_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line5_Txt", 
            "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line6_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line6_Txt", 
            "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_B = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_B", 
            "#WS-FILLER-B", FieldType.STRING, 57);
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_C = pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_C", 
            "#WS-FILLER-C", FieldType.STRING, 84);
        pnd_First_Page = localVariables.newFieldInRecord("pnd_First_Page", "#FIRST-PAGE", FieldType.BOOLEAN, 1);
        pnd_Last_Page = localVariables.newFieldInRecord("pnd_Last_Page", "#LAST-PAGE", FieldType.BOOLEAN, 1);
        pnd_Batch_Cnt = localVariables.newFieldInRecord("pnd_Batch_Cnt", "#BATCH-CNT", FieldType.NUMERIC, 7);
        pnd_First_Record = localVariables.newFieldInRecord("pnd_First_Record", "#FIRST-RECORD", FieldType.BOOLEAN, 1);
        pnd_Last_Contract = localVariables.newFieldInRecord("pnd_Last_Contract", "#LAST-CONTRACT", FieldType.BOOLEAN, 1);
        pnd_Last_Record = localVariables.newFieldInRecord("pnd_Last_Record", "#LAST-RECORD", FieldType.BOOLEAN, 1);
        pnd_Save_First_Twenty = localVariables.newFieldInRecord("pnd_Save_First_Twenty", "#SAVE-FIRST-TWENTY", FieldType.STRING, 20);

        pnd_Save_First_Twenty__R_Field_10 = localVariables.newGroupInRecord("pnd_Save_First_Twenty__R_Field_10", "REDEFINE", pnd_Save_First_Twenty);

        pnd_Save_First_Twenty_Documentrequestid_Data = pnd_Save_First_Twenty__R_Field_10.newGroupInGroup("pnd_Save_First_Twenty_Documentrequestid_Data", 
            "DOCUMENTREQUESTID-DATA");
        pnd_Save_First_Twenty_Documentrequestid_Sys_Id = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Sys_Id", 
            "DOCUMENTREQUESTID-SYS-ID", FieldType.STRING, 3);
        pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id", 
            "DOCUMENTREQUESTID-SUB-SYS-ID", FieldType.STRING, 3);
        pnd_Save_First_Twenty_Documentrequestid_Yyyy = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Yyyy", 
            "DOCUMENTREQUESTID-YYYY", FieldType.STRING, 4);
        pnd_Save_First_Twenty_Documentrequestid_Mm = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Mm", 
            "DOCUMENTREQUESTID-MM", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Dd = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Dd", 
            "DOCUMENTREQUESTID-DD", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Hh = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Hh", 
            "DOCUMENTREQUESTID-HH", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Ii = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Ii", 
            "DOCUMENTREQUESTID-II", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Ss = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Ss", 
            "DOCUMENTREQUESTID-SS", FieldType.STRING, 2);

        vw_fcp_Cons_Pymnu = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnu", "FCP-CONS-PYMNU"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnu_Pymnt_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        fcp_Cons_Pymnu_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnu_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnu_Pymnt_Check_Dte = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_DTE");
        fcp_Cons_Pymnu_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnu_Cntrct_Invrse_Dte = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        fcp_Cons_Pymnu_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        fcp_Cons_Pymnu_Cntrct_Crrncy_Cde = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        fcp_Cons_Pymnu__R_Field_11 = vw_fcp_Cons_Pymnu.getRecord().newGroupInGroup("fcp_Cons_Pymnu__R_Field_11", "REDEFINE", fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num = fcp_Cons_Pymnu__R_Field_11.newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        fcp_Cons_Pymnu_Pymnt_Instmt_Nbr = fcp_Cons_Pymnu__R_Field_11.newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 
            2);
        fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SCRTY_NBR");
        fcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SEQ_NBR");
        registerRecord(vw_fcp_Cons_Pymnu);

        pnd_Ws_Key = localVariables.newFieldInRecord("pnd_Ws_Key", "#WS-KEY", FieldType.STRING, 17);

        pnd_Ws_Key__R_Field_12 = localVariables.newGroupInRecord("pnd_Ws_Key__R_Field_12", "REDEFINE", pnd_Ws_Key);

        pnd_Ws_Key_Pnd_Ws_Key_Level2 = pnd_Ws_Key__R_Field_12.newGroupInGroup("pnd_Ws_Key_Pnd_Ws_Key_Level2", "#WS-KEY-LEVEL2");
        pnd_Ws_Key_Pymnt_Check_Dte = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ws_Key_Cntrct_Orgn_Cde = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Key_Cntrct_Check_Crrncy_Cde = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);

        pnd_Ws_Key__R_Field_13 = pnd_Ws_Key_Pnd_Ws_Key_Level2.newGroupInGroup("pnd_Ws_Key__R_Field_13", "REDEFINE", pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Key_Pymnt_Prcss_Seq_Num = pnd_Ws_Key__R_Field_13.newFieldInGroup("pnd_Ws_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Ws_Key_Pymnt_Instmt_Nbr = pnd_Ws_Key__R_Field_13.newFieldInGroup("pnd_Ws_Key_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pnd_Invalid_Rec = localVariables.newFieldInRecord("pnd_Invalid_Rec", "#INVALID-REC", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnu.reset();

        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();
        ldaFcpl876b.initializeValues();
        ldaFcplbar1.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_2.setInitialValue("               IA-DEATH STATEMENT");
        pnd_Ws_Rec_1.setInitialValue(" ");
        pnd_Ws_First_Time_In.setInitialValue("Y");
        pnd_Ws_One.setInitialValue(1);
        pnd_Ws_Cntr_Inv_Acct.setInitialValue(0);
        pnd_Ws_Format_Notification_Letter.setInitialValue("N");
        pnd_Ws_Bottom_Part.setInitialValue(" ");
        pnd_Ws_Jde.setInitialValue(" ");
        pnd_Ws_Format.setInitialValue(" ");
        pnd_Ws_Forms.setInitialValue(" ");
        pnd_Ws_Max.setInitialValue(0);
        pnd_I.setInitialValue(0);
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_First_Record.setInitialValue(true);
        pnd_Last_Contract.setInitialValue(false);
        pnd_Last_Record.setInitialValue(false);
        pnd_Invalid_Rec.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp970() throws Exception
    {
        super("Fcpp970");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  INPUT #CNTL-IDX
        //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        //*  AD WORK FILE 6 ONCE #FCPL876
        //*  AT END OF FILE
        //*   PERFORM ERROR-DISPLAY-START
        //*   WRITE '***' 25T 'MISSING "NEXT CHECK" FILE'            77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*   TERMINATE 51
        //*  END-ENDFILE
        //* * * LEON BAR  *
        //*  #BARCODE-LDA.#BAR-BARCODE (*) := #FCPL876.#BAR-BARCODE(*)
        //*  #BARCODE-LDA.#BAR-ENVELOPES   := #FCPL876.#BAR-ENVELOPES
        //*  #BARCODE-LDA.#BAR-NEW-RUN     := #FCPL876.#BAR-NEW-RUN
        //*  #WS-NEXT-CHECK-NBR            := #FCPL876.PYMNT-CHECK-NBR
        //*  #WS-NEXT-CHECK-PREFIX         := FCPA110.START-CHECK-PREFIX-N3 /* RL
        //*  MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //*  MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //*  MOVE EDITED #WS-CHECK-NBR-N10 (EM=9999999999) TO
        //*   #WS-CHECK-NBR-A10
        //*  #WS-CURRENT-PYMNT-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*  #WS-NEXT-EFT-NBR          := #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*  #WS-NEXT-GLOBAL-NBR       := 1
        //*  WRITE *PROGRAM *TIME 'at start of program:'
        //*   / 'The program is going to use the following:'
        //*   / 'start assigned check   number:' #WS-NEXT-CHECK-NBR
        //*   / 'start assigned EFT     number:' #WS-NEXT-EFT-NBR
        //*   / 'start assigned Global  number:' #WS-NEXT-GLOBAL-NBR
        //*   / 'start sequence number........:' #WS-CURRENT-PYMNT-SEQ-NBR
        //*  ADJUST CURRENT-PYMNT-SEQ-NBR FOR THE FIRST TIME
        //*  SUBTRACT 1 FROM #WS-CURRENT-PYMNT-SEQ-NBR  /*
        //*  CLOSE WORK 6
        pnd_Prime_Counter.reset();                                                                                                                                        //Natural: RESET #PRIME-COUNTER
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #WS-WORK-REC-BYTES ( * )
        while (condition(getWorkFiles().read(1, pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"))))
        {
            pnd_Prime_Counter.nadd(1);                                                                                                                                    //Natural: AT END OF DATA;//Natural: ADD 1 TO #PRIME-COUNTER
            //*  PERFORM SHOW-REC
            //*  10=HEADER REC
            if (condition(pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(10)))                                                                                    //Natural: IF #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR EQ 10
            {
                if (condition(pnd_Ws_First_Time_In.equals("Y")))                                                                                                          //Natural: IF #WS-FIRST-TIME-IN = 'Y'
                {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                    sub_Initialize_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                          //Natural: ASSIGN #WS-HEADER-BYTES ( * ) := #WS-WORK-REC-BYTES ( * )
                    //*      PERFORM DETERMINE-PYMNT-ID
                    //*      PERFORM VOID-1ST-PAGE
                    pnd_Ws_First_Time_In.setValue("N");                                                                                                                   //Natural: ASSIGN #WS-FIRST-TIME-IN = 'N'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*      PERFORM FORMAT-DOCUMENT
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-NUMBER-DB196
                    sub_Get_Check_Number_Db196();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Invalid_Rec.setValue(false);                                                                                                                      //Natural: ASSIGN #INVALID-REC := FALSE
                                                                                                                                                                          //Natural: PERFORM GENERATE-XML-FILE
                    sub_Generate_Xml_File();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                    sub_Initialize_Fields();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*      PERFORM DETERMINE-PYMNT-ID
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ws_First_Time_In.equals("Y")))                                                                                                          //Natural: IF #WS-FIRST-TIME-IN = 'Y'
                {
                    pnd_Abend_Cde.setValue(43);                                                                                                                           //Natural: ASSIGN #ABEND-CDE := 43
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"REASON: The extract file must start with a record type 10", //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / 'REASON: The extract file must start with a record type 10' / 'however it starts with a record with' / '=' #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR
                        NEWLINE,"however it starts with a record with",NEWLINE,"=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                  //Natural: TERMINATE #ABEND-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-N-SEQ-NMBRS
            sub_Assign_Check_N_Seq_Nmbrs();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
            sub_Move_Small_Rec_To_Large_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            pnd_Last_Record.setValue(true);                                                                                                                               //Natural: ASSIGN #LAST-RECORD := TRUE
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-NUMBER-DB196
            sub_Get_Check_Number_Db196();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM GENERATE-XML-FILE
            sub_Generate_Xml_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        DbsUtil.callnat(Fcpn965.class , getCurrentProcessState(), pnd_Batch_Cnt);                                                                                         //Natural: CALLNAT 'FCPN965' USING #BATCH-CNT
        if (condition(Global.isEscape())) return;
        //*  10-05-1999
        if (condition(pnd_Prime_Counter.greater(getZero())))                                                                                                              //Natural: IF #PRIME-COUNTER > 0
        {
            //*  PERFORM FORMAT-DOCUMENT    /* PROCESS LAST-RECORD
            //* *
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"Counters etc, at end of program:",NEWLINE," number of records read......:",  //Natural: WRITE *PROGRAM *TIME 'Counters etc, at end of program:' / ' number of records read......:' #PRIME-COUNTER / '              records type 10:' #C-RCRD-10 / '              records type 20:' #C-RCRD-20 / '              records type 30:' #C-RCRD-30
                pnd_Prime_Counter,NEWLINE,"              records type 10:",pnd_C_Rcrd_10,NEWLINE,"              records type 20:",pnd_C_Rcrd_20,NEWLINE,
                "              records type 30:",pnd_C_Rcrd_30);
            if (Global.isEscape()) return;
            pnd_Ws_Current_Pymnt_Seq_Nbr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-CURRENT-PYMNT-SEQ-NBR
            //*  WRITE
            //*    'Next check  number...........:' #WS-NEXT-CHECK-NBR
            //*    / 'Next EFT    number...........:' #WS-NEXT-EFT-NBR
            //*    / 'Next Global number...........:' #WS-NEXT-GLOBAL-NBR
            //*    / 'Next payment sequence number.:' #WS-CURRENT-PYMNT-SEQ-NBR
            //*  #FCPL876.PYMNT-CHECK-SEQ-NBR   := #WS-CURRENT-PYMNT-SEQ-NBR
            //*  #FCPL876.PYMNT-CHECK-NBR       := #WS-NEXT-CHECK-NBR
            //*  #FCPL876.PYMNT-CHECK-SCRTY-NBR := #WS-NEXT-EFT-NBR
            //* ** LEON BAR * * * * * *
            //*  #FCPL876.#BAR-BARCODE(*)       := #BARCODE-LDA.#BAR-BARCODE (*)
            //*  #FCPL876.#BAR-ENVELOPES        := #BARCODE-LDA.#BAR-ENVELOPES
            //* * * * * * * * * * *  *
            //*   WRITE WORK FILE 7 #FCPL876
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION ****");                                                           //Natural: WRITE // '****** END OF PROGRAM EXECUTION ****'
            if (Global.isEscape()) return;
            //* ****** 10/05 LEON
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   WRITE WORK FILE 7 #FCPL876
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"***!! WORK FILE 1 CONTAIN NO RECORDS TO PROCESS !!**");                                           //Natural: WRITE // '***!! WORK FILE 1 CONTAIN NO RECORDS TO PROCESS !!**'
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION  **");                                                            //Natural: WRITE // '****** END OF PROGRAM EXECUTION  **'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* ********
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PYMNT-ID
        //*  ======================================================================
        //*  DETERMINE THE PAYMENT IDENTIFIER OF THE CURRENT PAYMENT
        //*  WHETHER  IT IS A CHECK, AN EFT OR A GLOBAL PAY
        //*  ----------------------------------------------------------------------
        //*    #FCPL876A.CNTRCT-ORGN-CDE     := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        //*    #FCPL876A.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
        //*    #WS-CURRENT-PYMNT-ID          := #WS-NEXT-CHECK-NBR
        //*    #FCPL876A.PYMNT-CHECK-NBR     := #WS-NEXT-CHECK-NBR
        //*    ADD 1                         TO #WS-NEXT-CHECK-NBR
        //* *  MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //* *  MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //* *  MOVE EDITED #WS-CHECK-NBR-N10 (EM=9999999999) TO
        //* *    #WS-CHECK-NBR-A10
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-CHECK-N-SEQ-NMBRS
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-SMALL-REC-TO-LARGE-REC
        //*    MOVE BY NAME #WS-HEADER-LEVEL2   TO EXTR
        //*    MOVE BY NAME #WS-OCCURS-LEVEL2(*) TO INV-ACCT(*)
        //*    MOVE BY NAME #WS-NAME-N-ADDRESS-RECORD TO EXTR
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DOCUMENT
        //*  ======================================================================
        //*  DEFINE SUBROUTINE FORMAT-STATEMENT
        //*  ======================================================================
        //*  1. SET UP THE XEROX "format" IN ACCORD WITH THE DOCUMNET
        //*     TO BE PRINTED.
        //*  ----------------------------------------------------------------------
        //*  ......SET UP THE XEROX "format" IN ACCORD WITH DOCUMNET & PROTOTYPE
        //*  RESET #WS-FEED
        //*  IF EXT.CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'           /* ROXAN
        //*   PERFORM SETUP-FONTS-PA-SELECT                   /*
        //*  ELSE                                              /*
        //*   DECIDE ON FIRST VALUE OF #WS-CURRENT-CNTRCT-TYPE-CDE
        //*     VALUE
        //*         'PP', /* MATURITY, SURVIVOR BENEFITS, PROTOTYPE #4
        //*         'L'   /*  LUMP SUM           ... PROTOTYPE#4
        //*       DECIDE ON FIRST VALUE OF #WS-BOTTOM-PART
        //*         VALUE 'C'             /* C=CHECK
        //* *        ASSIGN #WS-FORMAT = 'IADEA'     /* RL
        //*          ASSIGN #WS-FORMAT = 'IADEAX'    /* RL
        //* *        ASSIGN #WS-FORMS  = 'PRO2AA'    /* RL
        //*          ASSIGN #WS-FORMS  = 'MCMWIC'    /* RL
        //*          ASSIGN #WS-FEED   = 'CHECKP'    /* SECURITY PAPER
        //*          ASSIGN #WS-MAX    = 12
        //*          PERFORM FORMAT-XEROX-COMMANDS
        //* ******************* RL BEGIN POS-PAY/PAYEE MATCH *******************
        //* *        MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7 /* RL FRI FEB 10
        //*  ITE '=' #WS-NEXT-CHECK-NBR '=' #WS-CURRENT-PYMNT-ID
        //*          MOVE #WS-CURRENT-PYMNT-ID TO #WS-CHECK-NBR-N7 /* RL
        //*          MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //*          MOVE EDITED #WS-CHECK-NBR-N10 (EM=9999999999) TO
        //*            #WS-CHECK-NBR-A10
        //*        COMPRESS '12' #WS-CHECK-NBR-A10
        //* *COMPRESS '12' #WS-CHECK-NBR-A10 *PROGRAM ' 2950 #WS-NEXT-CHECK-NBR'
        //*            INTO #WS-REC-1 LEAVING NO SPACE
        //*          WRITE WORK FILE 8 #WS-REC-1
        //* ***************** RL END POS-PAY/PAYEE MATCH *********************
        //*        VALUE 'S'           /* S=SENT TO FORM
        //*          ASSIGN #WS-FORMAT = 'IADEAL'   /*  'INITA'  OLD LEON
        //*          ASSIGN #WS-FORMS  = 'PRO3A'
        //*          ASSIGN #WS-FEED   = 'BOND'     /* BOND PAPER
        //*          ASSIGN #WS-MAX    = 9
        //*          PERFORM FORMAT-XEROX-COMMANDS
        //*        NONE VALUE
        //*          IGNORE
        //*      END-DECIDE
        //*    VALUE
        //*        'X'      /*  LUMP SUM           ... PROTOTYPE#6 <<==========
        //*      DECIDE ON FIRST VALUE OF #WS-BOTTOM-PART
        //*        VALUE 'C'             /* C=CHECK
        //* *        ASSIGN #WS-FORMAT = 'MONTH1' /* RL
        //*          ASSIGN #WS-FORMAT = 'MONTHX' /* RL
        //* *        ASSIGN #WS-FORMS  = 'PRO2AA' /* RL
        //*          ASSIGN #WS-FORMS  = 'MCMWIC' /* RL
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //* * * * LEON BAR * *
        //* * * * * * * * * *
        //*  ======================================================================
        //*  ======================================================================
        //*  EFINE SUBROUTINE FORMAT-CHECK
        //*  ======================================================================
        //* ** * * LEON BAR * * * *
        //* *OVE TRUE        TO #CHECK-IND #FIRST-PAGE
        //* *OVE FALSE       TO #STATEMENT-IND
        //* * * * * * * * * * * * * * ---------------------------------------------
        //*  ----------------------------------------------------------------------
        //* *--> COMMENTED/INSERTED BY FRANK 07-26-94
        //* *ALLNAT 'FCPN386'
        //*  #WS-HEADER-BYTES(*)
        //*  #WS-OCCURS(*,*)
        //*  #WS-NAME-N-ADDRESS-BYTES(*,*)
        //*  FCPA110                 /* RL
        //*  #WS-CNTR-INV-ACCT
        //*  #WS-SIM-DUP-MULTIPLEX-WRITTEN
        //* *  *  LEON BAR * * * *
        //* *F #WS-SAVE-S-D-M-PLEX = '1' OR = '2'
        //*  #LAST-PAGE  := TRUE
        //* *ND-IF
        //*  ERFORM BARCODE-PROCESSING
        //* * * * * * * * * * * * * * *
        //* *F #WS-SAVE-S-D-M-PLEX = '1'                     /* SIMPLEX
        //*  COMPRESS '+3' INTO #WS-REC-1 LEAVING NO SPACE
        //*  PERFORM WRITE-WORK8               /* NH
        //*  LSE
        //*  IF #WS-FORMAT-NOTIFICATION-LETTER = 'Y'
        //*    IGNORE
        //*  ELSE
        //*    ASSIGN #WS-REC-1 = ' '
        //*    PERFORM WRITE-WORK8               /* NH
        //*    IF EXT.CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'   /* ROXAN
        //*      ASSIGN #WS-REC-1 =
        //*        ' $$XEROX FORMAT=INITB,FORMS=PASMUL,END;'  /* ???????? ROXAN
        //*    ELSE
        //*      DECIDE ON FIRST VALUE OF #WS-CURRENT-CNTRCT-TYPE-CDE
        //*        VALUE 'PP' /* MATURITY, SURVIVOR BENEFITS, PROTOTYPE #4
        //*          ASSIGN #WS-REC-1 =
        //*            ' $$XEROX FORMAT=INITB,FORMS=IAMULT,END;'    /*   LEON BAR
        //*        VALUE 'L'      /*  LUMP SUM           ... PROTOTYPE#6
        //*          ASSIGN #WS-REC-1 =
        //*            ' $$XEROX FORMAT=INITB,FORMS=IAMULT,END;'    /* 3 LEON BAR
        //*        NONE VALUE
        //*          IGNORE
        //*      END-DECIDE
        //*    END-IF
        //*    PERFORM WRITE-WORK8               /* NH
        //*    ASSIGN #WS-REC-1 = ' '
        //*    PERFORM WRITE-WORK8               /* NH
        //*  END-IF
        //* *ND-IF
        //* *ND-SUBROUTINE
        //*  ======================================================================
        //*  DEFINE SUBROUTINE SENT-TO-FORM
        //*  ======================================================================
        //* * LEON BAR * *
        //*  MOVE FALSE       TO #CHECK-IND
        //*  MOVE TRUE        TO #STATEMENT-IND #FIRST-PAGE
        //* *  * * * * * *
        //*  ----------------------------------------------------------------------
        //*  CALLNAT 'FCPN388'
        //*   #WS-HEADER-BYTES(*)
        //*   #WS-OCCURS(*,*)
        //*   #WS-NAME-N-ADDRESS-BYTES(*,*)
        //* * LEON BAR * *
        //*  IF #WS-SAVE-S-D-M-PLEX = '1' OR = '2'
        //*   #LAST-PAGE  := TRUE
        //*  END-IF
        //*  PERFORM BARCODE-PROCESSING
        //* * * * * * * * * * * * *
        //*  IF #WS-SAVE-S-D-M-PLEX = '1'                     /* SIMPLEX
        //*   COMPRESS '+3' INTO #WS-REC-1 LEAVING NO SPACE   /* RL ? COMMENT OUT ?
        //*   PERFORM WRITE-WORK8               /* NH
        //*  ELSE
        //*   ASSIGN #WS-REC-1 = ' '
        //*   PERFORM WRITE-WORK8               /* NH
        //*   IF EXT.CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M'   /* ROXAN
        //*     ASSIGN #WS-REC-1 =
        //*       ' $$XEROX FORMAT=INITB,FORMS=PASMUL,END;'  /* ???????? ROXAN
        //*   ELSE
        //*     DECIDE ON FIRST VALUE OF #WS-CURRENT-CNTRCT-TYPE-CDE
        //*       VALUE 'PP' /* MATURITY, SURVIVOR BENEFITS, PROTOTYPE #4
        //*         ASSIGN #WS-REC-1 =
        //*           ' $$XEROX FORMAT=INITB,FORMS=IAMULT,END;'     /*  LEON
        //*       VALUE 'L'      /*  LUMP SUM           ... PROTOTYPE#6
        //*         ASSIGN #WS-REC-1 =
        //*           ' $$XEROX FORMAT=INITB,FORMS=IAMULT,END;'    /* 3 LEON
        //*       NONE VALUE
        //*         IGNORE
        //*     END-DECIDE
        //*    WRITE WORK FILE 8 #WS-REC-1
        //*     PERFORM WRITE-WORK8               /* NH
        //*     ASSIGN #WS-REC-1 = ' '
        //*    WRITE WORK FILE 8 #WS-REC-1
        //*     PERFORM WRITE-WORK8               /* NH
        //*   END-IF
        //*  END-IF
        //*  END-SUBROUTINE
        //*  ======================================================================
        //*  DEFINE SUBROUTINE FORMAT-NOTIFICATION
        //*  ======================================================================
        //*  ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'Y'
        //*  ASSIGN #WS-REC-1 = ' '
        //*  PERFORM WRITE-WORK8               /* NH
        //*  ASSIGN #WS-REC-1 =
        //* *' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=LET' -     /* RL
        //* *',FORMS=PRO4A,FEED=CHECKP,END;'                  /* RL
        //*   ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=LETX' -
        //*   ',FORMS=MCMLET,FEED=CHECKP,END;'
        //* *PERFORM WRITE-WORK8               /* NH
        //*  ASSIGN #WS-REC-1 = ' '
        //*  PERFORM WRITE-WORK8               /* NH
        //*  ........ FORMAT THE "header" PORTION OF THE LETTER
        //*  CALLNAT 'FCPN378'
        //*   #WS-HEADER-BYTES(*)
        //*   #WS-OCCURS(*,*)
        //*   #WS-NAME-N-ADDRESS-BYTES(*,*)
        //*   #WS-CNTR-INV-ACCT
        //*   #WS-SIM-DUP-MULTIPLEX-WRITTEN
        //*  ........ FORMAT THE
        //*  CALLNAT 'FCPN370'
        //*   #WS-HEADER-BYTES(*)
        //*   #WS-OCCURS(*,*)
        //*   #WS-NAME-N-ADDRESS-BYTES(*,*)
        //*   #WS-CNTR-INV-ACCT
        //*   #WS-SIM-DUP-MULTIPLEX-WRITTEN
        //*   #WS-HEADER-RECORD.CNTRCT-ANNTY-INS-TYPE     /* TMM PASELECT 05/00
        //*   #WS-HEADER-RECORD.CNTRCT-ANNTY-TYPE-CDE     /* TMM PASELECT 05/00
        //*   #WS-HEADER-RECORD.CNTRCT-INSURANCE-OPTION   /* TMM PASELECT 05/00
        //*   #WS-HEADER-RECORD.CNTRCT-LIFE-CONTINGENCY   /* TMM PASELECT 05/00
        //*  END-SUBROUTINE
        //*  ======================================================================
        //*  DEFINE SUBROUTINE BODY-OF-STATEMENT
        //*  ======================================================================
        //*  CALLNAT 'FCPN372'                   /* FORMAT BODY
        //*   #WS-HEADER-BYTES(*)
        //*   #WS-OCCURS(*,*)
        //*   #WS-NAME-N-ADDRESS-BYTES(*,*)
        //*   #WS-CNTR-INV-ACCT
        //*   #WS-SIM-DUP-MULTIPLEX-WRITTEN
        //*   #WS-SIDE-PRINTED
        //*   #WS-HEADER-RECORD.CNTRCT-ANNTY-INS-TYPE     /* TMM PASELECT 05/00
        //*   #WS-HEADER-RECORD.CNTRCT-ANNTY-TYPE-CDE     /* TMM PASELECT 05/00
        //*   #WS-HEADER-RECORD.CNTRCT-INSURANCE-OPTION   /* TMM PASELECT 05/00
        //*   #WS-HEADER-RECORD.CNTRCT-LIFE-CONTINGENCY   /* TMM PASELECT 05/00
        //*  END-SUBROUTINE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SHOW-REC
        //*  DEFINE SUBROUTINE WRITE-WORK8
        //*  WRITE WORK FILE 8 #WS-REC-1
        //*  END-SUBROUTINE
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-LEON-RTN
        //* ** * * * * * * * * * * * * * * * *
        //* *
        //* *
        //* *  LEON BAR * * * * * * * * * * * *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BARCODE-PROCESSING
        //*  ----------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETUP-FONTS-PA-SELECT
        //* *    COMPRESS '12' #WS-CHECK-NBR-A10 *PROGRAM ' 6350'
        //*    WRITE WORK FILE 8 #WS-REC-1
        //* ***************** RL END POS-PAY/PAYEE MATCH *********************
        //* *------------
        //* *********************** RL BEGIN - PAYEE MATCH ************************
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //*  START-CHECK-PREFIX-N3
        //* ********************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-XML-FILE
        //*  *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-NUMBER-DB196
        //*  MOVE BY NAME #WS-HEADER-LEVEL2 TO #WS-KEY-LEVEL2
        //*  *****************************************
        //* *
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Determine_Pymnt_Id() throws Exception                                                                                                                //Natural: DETERMINE-PYMNT-ID
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                                      //Natural: ASSIGN #WS-HEADER-BYTES ( * ) := #WS-WORK-REC-BYTES ( * )
        pnd_Ws_Current_Pymnt_Seq_Nbr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-CURRENT-PYMNT-SEQ-NBR
        //*  CHECKS
        short decideConditionsMet1076 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1))))
        {
            decideConditionsMet1076++;
            if (condition(pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code.equals("0000")))                                                                               //Natural: IF #WS-WORK-REC-RECORD.#WS-CONTRACT-HOLD-CODE = '0000'
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                               //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                                //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
                //*  EFT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(2))))
        {
            decideConditionsMet1076++;
            pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Eft_Nbr);                                                                                                        //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-EFT-NBR
            //*  GLOBAL PAY
            pnd_Ws_Next_Eft_Nbr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WS-NEXT-EFT-NBR
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(3))))
        {
            decideConditionsMet1076++;
            pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Global_Nbr);                                                                                                     //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-GLOBAL-NBR
            pnd_Ws_Next_Global_Nbr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #WS-NEXT-GLOBAL-NBR
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Abend_Cde.setValue(44);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 44
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Invalid data for","=",pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind); //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Invalid data for' '=' #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  DETERMINE-PYMNT-ID
    }
    private void sub_Assign_Check_N_Seq_Nmbrs() throws Exception                                                                                                          //Natural: ASSIGN-CHECK-N-SEQ-NMBRS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  PERFORM GET-CHECK-NUMBER-DB196        /* 201703 DARK MODE
        //*  #WS-WORK-REC-RECORD.#WS-PYMNT-CHECK-NBR := #WS-CURRENT-PYMNT-ID
        //*  WS-WORK-REC-RECORD.#WS-PYMNT-CHECK-SEQ-NBR:= #WS-CURRENT-PYMNT-SEQ-NBR
        getWorkFiles().write(9, false, pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                                                                       //Natural: WRITE WORK FILE 9 #WS-WORK-REC-BYTES ( * )
    }
    private void sub_Move_Small_Rec_To_Large_Rec() throws Exception                                                                                                       //Natural: MOVE-SMALL-REC-TO-LARGE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  READ THE WORK FILE RECORD INTO THE WORKING STORAGE TABLES
        //*  ----------------------------------------------------------------------
        //*  HEADER REC
        short decideConditionsMet1110 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR;//Natural: VALUE 10
        if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(10))))
        {
            decideConditionsMet1110++;
            //*  OCCURS REC
            pnd_C_Rcrd_10.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-RCRD-10
            pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                                  //Natural: ASSIGN #WS-HEADER-BYTES ( * ) := #WS-WORK-REC-BYTES ( * )
            pnd_Ws_Current_Cntrct_Type_Cde.setValue(pnd_Ws_Header_Record_Cntrct_Type_Cde);                                                                                //Natural: ASSIGN #WS-CURRENT-CNTRCT-TYPE-CDE := #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
        }                                                                                                                                                                 //Natural: VALUE 20
        else if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(20))))
        {
            decideConditionsMet1110++;
            pnd_C_Rcrd_20.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-RCRD-20
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            pnd_Ws_Occurs.getValue(pnd_I,"*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                                                       //Natural: ASSIGN #WS-OCCURS ( #I,* ) := #WS-WORK-REC-BYTES ( * )
            //*  NAME/ADDR REC
            pnd_Ws_Cntr_Inv_Acct.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WS-CNTR-INV-ACCT
        }                                                                                                                                                                 //Natural: VALUE 30
        else if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(30))))
        {
            decideConditionsMet1110++;
            pnd_C_Rcrd_30.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-RCRD-30
            pnd_K.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #K
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue(pnd_K,"*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));            //Natural: ASSIGN #WS-NAME-N-ADDRESS-BYTES ( #K,* ) := #WS-WORK-REC-BYTES ( * )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Format_Document() throws Exception                                                                                                                   //Natural: FORMAT-DOCUMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  FORMAT THE DOCUMENT SEND TO THE ANNUITENT.
        //*  IF AN ADDRESS IN THE SECOND ADDRESS FIELDS, INDICATES THAT THE CHECK
        //*     IS MAILED TO ONE ADDRESS, AND A NOTIFICATION TO ANOTHER ADDRESS.
        //*     OTHERWISE, ONLY A CHECK IS PRINTED AND MAILED.
        //*  THE TEST IS USING ADDRESS FROM THE LDA FCPL220. THE LDA WAS NEVER
        //*  POPULATED WITH THE DATA. THEREFORE THE STATEMENT ADDRESS IS ALWAYS
        //*  BLANK. THIS IS PROBABLY A PRODUCTION PROBLEM. I COMMENTED OUT THE TEST
        //*  UNTIL THIS NEEDS TO BE RESOLVED.
        //*  ----------------------------------------------------------------------
        //*  .........  ONLY ONE ADDRESS - ONLY PRINT CHECK
        //*  CHECKS
        short decideConditionsMet1144 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1))))
        {
            decideConditionsMet1144++;
            //*  C=CHECK
            pnd_Ws_Bottom_Part.setValue("C");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'C'
            //*  EFT
            DbsUtil.terminateApplication("External Subroutine FORMAT_STATEMENT is missing from the collection!");                                                         //Natural: PERFORM FORMAT-STATEMENT
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(2))))
        {
            decideConditionsMet1144++;
            //*  .... FOR EFT PRODUCE A STATEMENT WITH "send to" BUT NO NOTIFICATION
            //*  S="SENT TO" FORM
            pnd_Ws_Bottom_Part.setValue("S");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
            //*  GLOBAL PAY
            DbsUtil.terminateApplication("External Subroutine FORMAT_STATEMENT is missing from the collection!");                                                         //Natural: PERFORM FORMAT-STATEMENT
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(3))))
        {
            decideConditionsMet1144++;
            //*  .... GLOBAL: PRODUCE A STATEMENT WITH "send to" BUT NO NOTIFICATION
            //*  S="SENT TO" FORM
            pnd_Ws_Bottom_Part.setValue("S");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
            DbsUtil.terminateApplication("External Subroutine FORMAT_STATEMENT is missing from the collection!");                                                         //Natural: PERFORM FORMAT-STATEMENT
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Abend_Cde.setValue(41);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 41
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Invalid data for","=",pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind); //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Invalid data for' '=' #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* * END-IF
        //*  FORMAT-DOCUMENT
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  INITIALIZE FIELDS FOR A NEW PAYMENT SEQUENCE NUMBER, AND NEW CHECK
        //*  ----------------------------------------------------------------------
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #J #K #L #WS-CNTR-INV-ACCT #WS-BOTTOM-PART #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* )
        pnd_J.reset();
        pnd_K.reset();
        pnd_L.reset();
        pnd_Ws_Cntr_Inv_Acct.reset();
        pnd_Ws_Bottom_Part.reset();
        pnd_Ws_Sim_Dup_Multiplex_Written.reset();
        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").reset();
        pnd_Ws_Occurs.getValue("*","*").reset();
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*").reset();
        pnd_Last_Page.setValue(false);                                                                                                                                    //Natural: ASSIGN #LAST-PAGE := FALSE
        pnd_First_Page.setValue(true);                                                                                                                                    //Natural: ASSIGN #FIRST-PAGE := TRUE
        pnd_Ws_Current_Pymnt_Prcss_Seq_Num.setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num);                                                                   //Natural: ASSIGN #WS-CURRENT-PYMNT-PRCSS-SEQ-NUM := #WS-PYMNT-PRCSS-SEQ-NUM
        pnd_Ws_Save_S_D_M_Plex.setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex);                                                                          //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := #WS-WORK-REC-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
    }
    private void sub_Show_Rec() throws Exception                                                                                                                          //Natural: SHOW-REC
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"=",pnd_Ws_First_Time_In,NEWLINE,"=",pnd_Ws_Current_Pymnt_Prcss_Seq_Num,NEWLINE,"=",pnd_Ws_Side_Printed,       //Natural: WRITE / '=' #WS-FIRST-TIME-IN / '=' #WS-CURRENT-PYMNT-PRCSS-SEQ-NUM / '=' #WS-SIDE-PRINTED / '=' #WS-SIM-DUP-MULTIPLEX-WRITTEN / '=' #WS-SAVE-S-D-M-PLEX / '=' #WS-MAX / '=' #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR / '=' #WS-WORK-REC-RECORD.#WS-RECORD-OCCUR-NMBR / '=' #WS-WORK-REC-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX / '=' #WS-WORK-REC-RECORD.#WS-PYMNT-PRCSS-SEQ-NUM / '=' #WS-WORK-REC-RECORD.#WS-CONTRACT-HOLD-CODE / '***************************************************'
            NEWLINE,"=",pnd_Ws_Sim_Dup_Multiplex_Written,NEWLINE,"=",pnd_Ws_Save_S_D_M_Plex,NEWLINE,"=",pnd_Ws_Max,NEWLINE,"=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr,
            NEWLINE,"=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr,NEWLINE,"=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex,NEWLINE,"=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num,
            NEWLINE,"=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code,NEWLINE,"***************************************************");
        if (Global.isEscape()) return;
    }
    private void sub_New_Leon_Rtn() throws Exception                                                                                                                      //Natural: NEW-LEON-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Rec_1.setValue(" ");                                                                                                                                       //Natural: ASSIGN #WS-REC-1 = ' '
        //*  PERFORM WRITE-WORK8
        //*  MATURITY, SURVIVOR BENEFITS, PROTOTYPE #4
        short decideConditionsMet1188 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-CURRENT-CNTRCT-TYPE-CDE;//Natural: VALUE 'PP'
        if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("PP"))))
        {
            decideConditionsMet1188++;
            //*   LUMP SUM           ... PROTOTYPE#6
            pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=INITB,FORMS=IAMULT,END;");                                                                                             //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=INITB,FORMS=IAMULT,END;'
        }                                                                                                                                                                 //Natural: VALUE 'L'
        else if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("L"))))
        {
            decideConditionsMet1188++;
            //*  3 LEON
            pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=INITB,FORMS=IAMULT,END;");                                                                                             //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=INITB,FORMS=IAMULT,END;'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  NH
        DbsUtil.terminateApplication("External Subroutine WRITE_WORK8 is missing from the collection!");                                                                  //Natural: PERFORM WRITE-WORK8
        pnd_Ws_Rec_1.setValue(" ");                                                                                                                                       //Natural: ASSIGN #WS-REC-1 = ' '
        //*  NH
        DbsUtil.terminateApplication("External Subroutine WRITE_WORK8 is missing from the collection!");                                                                  //Natural: PERFORM WRITE-WORK8
    }
    private void sub_Barcode_Processing() throws Exception                                                                                                                //Natural: BARCODE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *
        short decideConditionsMet1207 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STATEMENT-IND
        if (condition(pnd_Ws_Pnd_Statement_Ind.getBoolean()))
        {
            decideConditionsMet1207++;
            pnd_Ws_Pnd_Bar_Sub.setValue(2);                                                                                                                               //Natural: MOVE 2 TO #BAR-SUB
        }                                                                                                                                                                 //Natural: WHEN #CHECK-IND
        else if (condition(pnd_Ws_Pnd_Check_Ind.getBoolean()))
        {
            decideConditionsMet1207++;
            pnd_Ws_Pnd_Bar_Sub.setValue(1);                                                                                                                               //Natural: MOVE 1 TO #BAR-SUB
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_First_Page.getBoolean()))                                                                                                                       //Natural: IF #FIRST-PAGE
        {
            pnd_First_Page.setValue(false);                                                                                                                               //Natural: ASSIGN #FIRST-PAGE := FALSE
            //*  FIRST PAGE OF ENVELOPE
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Demand_Feed().setValue(true);                                                                                          //Natural: MOVE TRUE TO #BAR-DEMAND-FEED
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().nadd(1);                                                                                                  //Natural: ADD 1 TO #BAR-ENV-ID-NUM
        }                                                                                                                                                                 //Natural: END-IF
        //* ****** NEW
        if (condition(pnd_Last_Page.getBoolean()))                                                                                                                        //Natural: IF #LAST-PAGE
        {
            //*  LAST  PAGE OF ENVELOPE
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Set_Last_Page().setValue(true);                                                                                        //Natural: MOVE TRUE TO #BAR-SET-LAST-PAGE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  LAST  PAGE OF ENVELOPE
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Set_Last_Page().setValue(false);                                                                                       //Natural: MOVE FALSE TO #BAR-SET-LAST-PAGE
        }                                                                                                                                                                 //Natural: END-IF
        //* **********
        if (condition(pnd_Ws_Name_N_Address_Record_Pymnt_Nme.getValue(pnd_Ws_Pnd_Bar_Sub).getSubstring(1,3).equals("CR ")))                                               //Natural: IF SUBSTR ( #WS-NAME-N-ADDRESS-RECORD.PYMNT-NME ( #BAR-SUB ) ,1,3 ) = 'CR '
        {
            pnd_Ws_Pnd_Bar_Name.setValue(pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line1_Txt.getValue(pnd_Ws_Pnd_Bar_Sub));                                                 //Natural: MOVE #WS-NAME-N-ADDRESS-RECORD.PYMNT-ADDR-LINE1-TXT ( #BAR-SUB ) TO #BAR-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Bar_Name.setValue(pnd_Ws_Name_N_Address_Record_Pymnt_Nme.getValue(pnd_Ws_Pnd_Bar_Sub));                                                            //Natural: MOVE #WS-NAME-N-ADDRESS-RECORD.PYMNT-NME ( #BAR-SUB ) TO #BAR-NAME
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALLNAT 'FCPNBAR' #BARCODE-LDA
        //*  FOR  #BAR-SUB = 1 TO 17
        //*   IF #BAR-SUB = 1
        //*     MOVE '1'                  TO #WS-REC-1
        //*   ELSE
        //*     MOVE ' '                  TO #WS-REC-1
        //*   END-IF
        //*   MOVE '<'                  TO SUBSTR(#WS-REC-1,2,1)
        //*   MOVE #BARCODE-LDA.#BAR-BARCODE(#BAR-SUB) TO SUBSTR(#WS-REC-1,3,1)
        //* *****    WRITE '** LEON * BAR='  #BARCODE-LDA.#BAR-BARCODE(#BAR-SUB)
        //*   PERFORM WRITE-WORK8            /* NH
        //*  END-FOR
        //*  IF #BAR-SET-LAST-PAGE
        //* ******  06/14/99  LEON **
        //*   IF ((#WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DC') AND
        //*       (#WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND = 2))
        //*     IGNORE
        //*   ELSE
        //*     #FCPL876B.#ADDRESSEE         := #BAR-NAME
        //*     #FCPL876B.#BAR-ENV-ID        := #BARCODE-LDA.#BAR-ENV-ID
        //*     #FCPL876B.CNTRCT-ORGN-CDE    := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        //*     #FCPL876B.CNTRCT-PPCN-NBR    := #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
        //*     #FCPL876B.CNTRCT-PAYEE-CDE   := #WS-HEADER-RECORD.CNTRCT-PAYEE-CDE
        //*     #FCPL876B.CNTRCT-HOLD-CDE    := #WS-CONTRACT-HOLD-CODE
        //*     #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER :=
        //*       #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER
        //*     IF #CHECK-IND
        //*       #FCPL876B.PYMNT-CHECK-NBR  := #FCPL876A.PYMNT-CHECK-NBR
        //*     ELSE
        //*       RESET                         #FCPL876B.#PYMNT-CHECK-NBR
        //*     END-IF
        //*     #FCPL876B.#BAR-ENVELOPES     := #BARCODE-LDA.#BAR-ENVELOPES
        //*     #FCPL876B.#BAR-PAGES-IN-ENV  := #BARCODE-LDA.#BAR-PAGES-IN-ENV
        //*     #FCPL876B.#CHECK             := #CHECK-IND
        //*     WRITE WORK FILE 8 #FCPL876B
        //*   END-IF    /*   06/10/99
        //*  END-IF
        //*  CODE BELLOW IS A TEMP CODE
        //*  DISPLAY
        //*  'L/VL'       #KEY-REC-LVL-#
        //*  'O/CC'       #KEY-REC-OCCUR-#
        //* **** '/HOLD'      #KEY-CONTRACT-HOLD-CODE
        //*  '/HOLD'      #WS-CONTRACT-HOLD-CODE
        //*  'S/D'        #KEY-SIMPLEX-DUPLEX-MULTIPLEX
        //*  '/PPCN'      #REC-TYPE-10-DETAIL.CNTRCT-PPCN-NBR(AL=8)
        //*  '/CREF'      #REC-TYPE-10-DETAIL.CNTRCT-CREF-NBR(AL=8)
        //*  '/PAYEE'     #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE
        //*  'N/R'        #BARCODE-LDA.#BAR-NEW-RUN  (EM=F/T)
        //*  'C/H'        #CHECK-IND                 (EM=F/T)
        //*  'S/T'        #STATEMENT-IND             (EM=F/T)
        //*  'E/I'        #BARCODE-LDA.#BAR-BARCODE(1)
        //*  'B/1'        #BARCODE-LDA.#BAR-BARCODE(2)
        //*  'E/S'        #BARCODE-LDA.#BAR-BARCODE(3)
        //*  'B/2'        #BARCODE-LDA.#BAR-BARCODE(4)
        //*  'B/3'        #BARCODE-LDA.#BAR-BARCODE(5)
        //*  'B/4'        #BARCODE-LDA.#BAR-BARCODE(6)
        //*  'B/5'        #BARCODE-LDA.#BAR-BARCODE(7)
        //*  'P/C'        #BARCODE-LDA.#BAR-BARCODE(8)
        //*  'E/C'        #BARCODE-LDA.#BAR-BARCODE(9)
        //*  '/PIN'       #BARCODE-LDA.#BAR-ENV-ID
        //*  'S/I'        #BARCODE-LDA.#BAR-BARCODE(17)
        //*  '/ENV'       #BARCODE-LDA.#BAR-ENVELOPES
        //*  'P/E'        #BARCODE-LDA.#BAR-PAGES-IN-ENV
        //*  '/NAME'      #BAR-NAME
        //*  CODE ABOVE IS A TEMP CODE
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Barcode_Input().resetInitial();                                                                                                //Natural: RESET INITIAL #BARCODE-INPUT
    }
    private void sub_Setup_Fonts_Pa_Select() throws Exception                                                                                                             //Natural: SETUP-FONTS-PA-SELECT
    {
        if (BLNatReinput.isReinput()) return;

        //*   'PP', 'L'   /*             ... PROTOTYPE#4
        //*  C=CHECK
        short decideConditionsMet1308 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-BOTTOM-PART;//Natural: VALUE 'C'
        if (condition((pnd_Ws_Bottom_Part.equals("C"))))
        {
            decideConditionsMet1308++;
            //* *  ASSIGN #WS-FORMAT = 'PASDEA'    /* RL
            //*  RL
            pnd_Ws_Format.setValue("IADEAX");                                                                                                                             //Natural: ASSIGN #WS-FORMAT = 'IADEAX'
            //* *  ASSIGN #WS-FORMS  = 'PASCHK'    /* RL
            //*  RL
            pnd_Ws_Forms.setValue("MCMWIC");                                                                                                                              //Natural: ASSIGN #WS-FORMS = 'MCMWIC'
            //*  SECURITY PAPER
            pnd_Ws_Feed.setValue("CHECKP");                                                                                                                               //Natural: ASSIGN #WS-FEED = 'CHECKP'
            pnd_Ws_Max.setValue(12);                                                                                                                                      //Natural: ASSIGN #WS-MAX = 12
            DbsUtil.terminateApplication("External Subroutine FORMAT_XEROX_COMMANDS is missing from the collection!");                                                    //Natural: PERFORM FORMAT-XEROX-COMMANDS
            //* ******************* RL BEGIN POS-PAY/PAYEE MATCH *******************
            //* *  MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7 /* RL FEB 16 THUR
            //*  RL FEB 16 THUR
            pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Current_Pymnt_Id);                                                                                   //Natural: MOVE #WS-CURRENT-PYMNT-ID TO #WS-CHECK-NBR-N7
            pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                             //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
            pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                                   //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
            //*  S=SENT TO FORM
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "12", pnd_Ws_Check_Nbr_A10));                                                           //Natural: COMPRESS '12' #WS-CHECK-NBR-A10 INTO #WS-REC-1 LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Ws_Bottom_Part.equals("S"))))
        {
            decideConditionsMet1308++;
            //*   'INITA'  OLD LEON
            pnd_Ws_Format.setValue("PASDAL");                                                                                                                             //Natural: ASSIGN #WS-FORMAT = 'PASDAL'
            pnd_Ws_Forms.setValue("PASALT");                                                                                                                              //Natural: ASSIGN #WS-FORMS = 'PASALT'
            //*  BOND PAPER
            pnd_Ws_Feed.setValue("BOND");                                                                                                                                 //Natural: ASSIGN #WS-FEED = 'BOND'
            pnd_Ws_Max.setValue(9);                                                                                                                                       //Natural: ASSIGN #WS-MAX = 9
            DbsUtil.terminateApplication("External Subroutine FORMAT_XEROX_COMMANDS is missing from the collection!");                                                    //Natural: PERFORM FORMAT-XEROX-COMMANDS
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        pnd_Ws_Next_Check_Prefix.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                                 //Natural: ASSIGN #WS-NEXT-CHECK-PREFIX := FCPA110.START-CHECK-PREFIX-N3
        //*  MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //*  MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //*  MOVE EDITED #WS-CHECK-NBR-N10 (EM=9999999999) TO
        //*   #WS-CHECK-NBR-A10
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                      //Natural: RESET #BAR-ENV-ID-NUM
    }
    private void sub_Generate_Xml_File() throws Exception                                                                                                                 //Natural: GENERATE-XML-FILE
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_First_Record.getBoolean()))                                                                                                                     //Natural: IF #FIRST-RECORD
        {
            setValueToSubstring("005",pnd_Save_First_Twenty,1,3);                                                                                                         //Natural: MOVE '005' TO SUBSTR ( #SAVE-FIRST-TWENTY,01,03 )
            pnd_Save_First_Twenty_Documentrequestid_Yyyy.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYY"));                                                     //Natural: MOVE EDITED *TIMX ( EM = YYYY ) TO DOCUMENTREQUESTID-YYYY
            pnd_Save_First_Twenty_Documentrequestid_Mm.setValueEdited(Global.getTIMX(),new ReportEditMask("MM"));                                                         //Natural: MOVE EDITED *TIMX ( EM = MM ) TO DOCUMENTREQUESTID-MM
            pnd_Save_First_Twenty_Documentrequestid_Dd.setValueEdited(Global.getTIMX(),new ReportEditMask("DD"));                                                         //Natural: MOVE EDITED *TIMX ( EM = DD ) TO DOCUMENTREQUESTID-DD
            pnd_Save_First_Twenty_Documentrequestid_Hh.setValueEdited(Global.getTIMX(),new ReportEditMask("HH"));                                                         //Natural: MOVE EDITED *TIMX ( EM = HH ) TO DOCUMENTREQUESTID-HH
            pnd_Save_First_Twenty_Documentrequestid_Ii.setValueEdited(Global.getTIMX(),new ReportEditMask("II"));                                                         //Natural: MOVE EDITED *TIMX ( EM = II ) TO DOCUMENTREQUESTID-II
            pnd_Save_First_Twenty_Documentrequestid_Ss.setValueEdited(Global.getTIMX(),new ReportEditMask("SS"));                                                         //Natural: MOVE EDITED *TIMX ( EM = SS ) TO DOCUMENTREQUESTID-SS
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Batch_Cnt.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #BATCH-CNT
        //*  DEFECT 182415
        DbsUtil.callnat(Fcpn970.class , getCurrentProcessState(), pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*"), pnd_Ws_Occurs.getValue("*","*"),                //Natural: CALLNAT 'FCPN970' USING #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-HEADER-RECORD.CNTRCT-ANNTY-INS-TYPE #WS-HEADER-RECORD.CNTRCT-ANNTY-TYPE-CDE #WS-HEADER-RECORD.CNTRCT-INSURANCE-OPTION #WS-HEADER-RECORD.CNTRCT-LIFE-CONTINGENCY FCPA110 #BATCH-CNT #SAVE-FIRST-TWENTY #FIRST-RECORD #LAST-RECORD #INVALID-REC
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*"), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written, pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type, 
            pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde, pnd_Ws_Header_Record_Cntrct_Insurance_Option, pnd_Ws_Header_Record_Cntrct_Life_Contingency, pdaFcpa110.getFcpa110(), 
            pnd_Batch_Cnt, pnd_Save_First_Twenty, pnd_First_Record, pnd_Last_Record, pnd_Invalid_Rec);
        if (condition(Global.isEscape())) return;
        if (condition(! (pnd_Invalid_Rec.getBoolean())))                                                                                                                  //Natural: IF NOT #INVALID-REC
        {
            if (condition(pnd_First_Record.getBoolean()))                                                                                                                 //Natural: IF #FIRST-RECORD
            {
                pnd_First_Record.setValue(false);                                                                                                                         //Natural: ASSIGN #FIRST-RECORD := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Batch_Cnt.nsubtract(1);                                                                                                                                   //Natural: ASSIGN #BATCH-CNT := #BATCH-CNT - 1
            if (condition(pnd_Batch_Cnt.equals(getZero()) && ! (pnd_First_Record.getBoolean())))                                                                          //Natural: IF #BATCH-CNT = 0 AND NOT #FIRST-RECORD
            {
                pnd_First_Record.setValue(true);                                                                                                                          //Natural: ASSIGN #FIRST-RECORD := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  GENERATE-XML-FILE
    }
    private void sub_Get_Check_Number_Db196() throws Exception                                                                                                            //Natural: GET-CHECK-NUMBER-DB196
    {
        if (BLNatReinput.isReinput()) return;

        //*   #PYMNT-NOT-FOUND               := TRUE
        pnd_Ws_Check_Nbr_N10.reset();                                                                                                                                     //Natural: RESET #WS-CHECK-NBR-N10
        pnd_Ws_Key_Pymnt_Check_Dte.setValue(pnd_Ws_Header_Record_Pymnt_Check_Dte);                                                                                        //Natural: ASSIGN #WS-KEY.PYMNT-CHECK-DTE := #WS-HEADER-RECORD.PYMNT-CHECK-DTE
        pnd_Ws_Key_Cntrct_Orgn_Cde.setValue(pnd_Ws_Header_Record_Cntrct_Orgn_Cde);                                                                                        //Natural: ASSIGN #WS-KEY.CNTRCT-ORGN-CDE := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
        pnd_Ws_Key_Cntrct_Check_Crrncy_Cde.setValue(pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde);                                                                        //Natural: ASSIGN #WS-KEY.CNTRCT-CHECK-CRRNCY-CDE := #WS-HEADER-RECORD.CNTRCT-CHECK-CRRNCY-CDE
        pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind.setValue(pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind);                                                                          //Natural: ASSIGN #WS-KEY.PYMNT-PAY-TYPE-REQ-IND := #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND
        pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr.setValue(pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);                                                                                //Natural: ASSIGN #WS-KEY.PYMNT-PRCSS-SEQ-NBR := #WS-HEADER-RECORD.PYMNT-PRCSS-SEQ-NBR
        vw_fcp_Cons_Pymnu.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) FCP-CONS-PYMNU BY CHK-DTE-ORG-CURR-TYP-SEQ STARTING FROM #WS-KEY
        (
        "READ_PYMNTS",
        new Wc[] { new Wc("CHK_DTE_ORG_CURR_TYP_SEQ", ">=", pnd_Ws_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CHK_DTE_ORG_CURR_TYP_SEQ", "ASC") },
        1
        );
        READ_PYMNTS:
        while (condition(vw_fcp_Cons_Pymnu.readNextRow("READ_PYMNTS")))
        {
            //*  WRITE / '********** From FCPP970 #WS-KEY  ' /* TESTING ONLY START
            //*        #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
            //*  / '=' #WS-KEY.PYMNT-CHECK-DTE(EM=YYYY/MM/DD)
            //*  / '=' #WS-KEY.CNTRCT-ORGN-CDE
            //*  / '=' #WS-KEY.CNTRCT-CHECK-CRRNCY-CDE
            //*  / '=' #WS-KEY.PYMNT-PAY-TYPE-REQ-IND
            //*  / '=' #WS-KEY.PYMNT-PRCSS-SEQ-NUM
            //*  /  '***** FCP-CONS-PYMNT *******************'
            //*  / '='    FCP-CONS-PYMNU.PYMNT-CHECK-DTE(EM=YYYY/MM/DD)
            //*  / '='    FCP-CONS-PYMNU.CNTRCT-ORGN-CDE
            //*  / '='    FCP-CONS-PYMNU.CNTRCT-CHECK-CRRNCY-CDE
            //*  / '='    FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND
            //*  / '='    FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NUM    /* TESTING END
            if (condition(pnd_Ws_Key_Pymnt_Check_Dte.notEquals(fcp_Cons_Pymnu_Pymnt_Check_Dte) || pnd_Ws_Key_Cntrct_Orgn_Cde.notEquals(fcp_Cons_Pymnu_Cntrct_Orgn_Cde)    //Natural: IF #WS-KEY.PYMNT-CHECK-DTE NE FCP-CONS-PYMNU.PYMNT-CHECK-DTE OR #WS-KEY.CNTRCT-ORGN-CDE NE FCP-CONS-PYMNU.CNTRCT-ORGN-CDE OR #WS-KEY.CNTRCT-CHECK-CRRNCY-CDE NE FCP-CONS-PYMNU.CNTRCT-CHECK-CRRNCY-CDE OR #WS-KEY.PYMNT-PAY-TYPE-REQ-IND NE FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND OR #WS-KEY.PYMNT-PRCSS-SEQ-NUM NE FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NUM
                || pnd_Ws_Key_Cntrct_Check_Crrncy_Cde.notEquals(fcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde) || pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind.notEquals(fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind) 
                || pnd_Ws_Key_Pymnt_Prcss_Seq_Num.notEquals(fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num)))
            {
                if (true) break READ_PYMNTS;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-PYMNTS. )
            }                                                                                                                                                             //Natural: END-IF
            //*  .... GET THE RECORD TO MINIMIZE THE NUMBER OF HELD RECORDS
            //*  GET FCP-CONS-PYMNU *ISN(READ-PYMNTS.)
            //*  IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R' OR= ' '
            //*  (N10)
            if (condition(fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind.equals(1)))                                                                                               //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1
            {
                pnd_Ws_Check_Nbr_N10.setValue(fcp_Cons_Pymnu_Pymnt_Nbr);                                                                                                  //Natural: ASSIGN #WS-CHECK-NBR-N10 := FCP-CONS-PYMNU.PYMNT-NBR
                pnd_Ws_Header_Record_Pymnt_Check_Nbr.setValue(pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7);                                                                  //Natural: ASSIGN #WS-HEADER-RECORD.PYMNT-CHECK-NBR := #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR := #WS-CHECK-NBR-N7
                pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr.setValue(pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7);
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr);                                                                  //Natural: ASSIGN #WS-CHECK-NBR-N7 := FCP-CONS-PYMNU.PYMNT-CHECK-SCRTY-NBR
                pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr.setValue(pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7);                                                           //Natural: ASSIGN #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR := #WS-CHECK-NBR-N7
                pnd_Ws_Header_Record_Pymnt_Check_Nbr.setValue(fcp_Cons_Pymnu_Pymnt_Check_Nbr);                                                                            //Natural: MOVE FCP-CONS-PYMNU.PYMNT-CHECK-NBR TO #WS-HEADER-RECORD.PYMNT-CHECK-NBR
                //*  EFT AND GLOBAL
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr.setValue(fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr);                                                                    //Natural: MOVE FCP-CONS-PYMNU.PYMNT-CHECK-SCRTY-NBR TO #WS-HEADER-RECORD.PYMNT-CHECK-SCRTY-NBR
            //*    WRITE ' Will process this cntr=' #WS-HEADER-RECORD.CNTRCT-PPCN-NBR
            //*  '='   #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR   /* TEST ONLY START
            //*  '='   #WS-HEADER-RECORD.PYMNT-CHECK-NBR
            //*  '='   #WS-HEADER-RECORD.PYMNT-CHECK-SCRTY-NBR /* TEST ONLY END
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET-CHECK-NUMBER-DB196
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),"Page",getReports().getPageNumberDbs(0),  //Natural: WRITE NOTITLE *PROGRAM 41T #HEADER0-1 124T 'Page' *PAGE-NUMBER ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON");
    }
}
