/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:31 PM
**        * FROM NATURAL PROGRAM : Fcpp826
************************************************************
**        * FILE NAME            : Fcpp826.java
**        * CLASS NAME           : Fcpp826
**        * INSTANCE NAME        : Fcpp826
************************************************************
************************************************************************
* PROGRAM  : FCPP826
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS "AP" LEDGER INTERFACE REPORT
* CREATED  : 02/16/96
*
* 04/01/97 : RITA SALGADO
*          : GET LEDGER DESCRIPTION USING ISA NBR INSTEAD OF FUND CODE
* 05/22/00 : MCGEE
*          : STOW - FCPL199B
* 06/11/02 : ROXAN CARREON
*          : RE-STOWED. PA-SELECT AND SPIA NEW FUNDS IN FCPL199A
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp826 extends BLNatBase
{
    // Data Areas
    private PdaFcpa121 pdaFcpa121;
    private LdaFcpl199a ldaFcpl199a;
    private LdaFcpl199b ldaFcpl199b;
    private LdaFcpl826 ldaFcpl826;
    private LdaFcpl826a ldaFcpl826a;
    private LdaFcpl826b ldaFcpl826b;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Rec_Cnt;
    private DbsField pnd_Ws_Pnd_Cr_Total;
    private DbsField pnd_Ws_Pnd_Dr_Total;
    private DbsField pnd_Ws_Pnd_Cr_Amt;
    private DbsField pnd_Ws_Pnd_Dr_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Cr_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Dr_Amt;
    private DbsField pnd_Ws_Pnd_Cr_Cv;
    private DbsField pnd_Ws_Pnd_Dr_Cv;
    private DbsField pnd_Ws_Pnd_Fund_Sub;
    private DbsField pnd_Ws_Pnd_Isa_Sub;
    private DbsField pnd_Ws_Pnd_Newpage_Ind;

    private DbsGroup pnd_Ws_Pnd_Breaks;
    private DbsField pnd_Ws_Pnd_Ledger_Break;
    private DbsField pnd_Ws_Pnd_Fund_Break;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Check_Dte_Break;
    private DbsField pnd_Ws_Pnd_Orgn_Break;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Cr_AmtSum158;
    private DbsField readWork01Pnd_Cr_AmtSum;
    private DbsField readWork01Pnd_Dr_AmtSum158;
    private DbsField readWork01Pnd_Dr_AmtSum;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa121 = new PdaFcpa121(localVariables);
        ldaFcpl199a = new LdaFcpl199a();
        registerRecord(ldaFcpl199a);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);
        ldaFcpl826 = new LdaFcpl826();
        registerRecord(ldaFcpl826);
        ldaFcpl826a = new LdaFcpl826a();
        registerRecord(ldaFcpl826a);
        ldaFcpl826b = new LdaFcpl826b();
        registerRecord(ldaFcpl826b);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Rec_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Cr_Total = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cr_Total", "#CR-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Pnd_Dr_Total = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dr_Total", "#DR-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Pnd_Cr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cr_Amt", "#CR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dr_Amt", "#DR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Tot_Cr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Cr_Amt", "#TOT-CR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Tot_Dr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tot_Dr_Amt", "#TOT-DR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Cr_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cr_Cv", "#CR-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Dr_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dr_Cv", "#DR-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Fund_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fund_Sub", "#FUND-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Isa_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isa_Sub", "#ISA-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Newpage_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Newpage_Ind", "#NEWPAGE-IND", FieldType.BOOLEAN, 1);

        pnd_Ws_Pnd_Breaks = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Breaks", "#BREAKS");
        pnd_Ws_Pnd_Ledger_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Ledger_Break", "#LEDGER-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Fund_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Fund_Break", "#FUND-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Company_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Check_Dte_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Check_Dte_Break", "#CHECK-DTE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Orgn_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Orgn_Break", "#ORGN-BREAK", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Cr_AmtSum158 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM_158", "Pnd_Cr_Amt_SUM_158", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Cr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM", "Pnd_Cr_Amt_SUM", FieldType.PACKED_DECIMAL, 13, 2);
        readWork01Pnd_Dr_AmtSum158 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Dr_Amt_SUM_158", "Pnd_Dr_Amt_SUM_158", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Dr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Dr_Amt_SUM", "Pnd_Dr_Amt_SUM", FieldType.PACKED_DECIMAL, 13, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl199a.initializeValues();
        ldaFcpl199b.initializeValues();
        ldaFcpl826.initializeValues();
        ldaFcpl826a.initializeValues();
        ldaFcpl826b.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Ledger_Break.setInitialValue(true);
        pnd_Ws_Pnd_Fund_Break.setInitialValue(true);
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Ws_Pnd_Check_Dte_Break.setInitialValue(true);
        pnd_Ws_Pnd_Orgn_Break.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp826() throws Exception
    {
        super("Fcpp826");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 58T 'INTERFACE REPORT' 120T 'REPORT: RPT1' / 49T #ORGN-CODE-DESC ( #ORGN-IDX ) / 58T 'DUE ON' PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) / 58T 'COMPANY:' ACCT-COMPANY //
        //* * RS  60T  #ANNTY-DESC(#ANNTY-IDX)                 /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 59T 'CONTROL REPORT' 120T 'REPORT: RPT2' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #LGR-INTRFCE-RPT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl826.getPnd_Lgr_Intrfce_Rpt())))
        {
            CheckAtStartofData135();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            pnd_Ws_Pnd_Rec_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-CNT
            if (condition(ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_Ind().equals("C")))                                                                            //Natural: IF INV-ACCT-LEDGR-IND = 'C'
            {
                pnd_Ws_Pnd_Cr_Amt.setValue(ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_Amt());                                                                       //Natural: ASSIGN #CR-AMT := INV-ACCT-LEDGR-AMT
                pnd_Ws_Pnd_Cr_Cv.setValue("AD=D");                                                                                                                        //Natural: ASSIGN #CR-CV := ( AD = D )
                pnd_Ws_Pnd_Dr_Cv.setValue("AD=N");                                                                                                                        //Natural: ASSIGN #DR-CV := ( AD = N )
                pnd_Ws_Pnd_Dr_Amt.reset();                                                                                                                                //Natural: RESET #DR-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Dr_Amt.setValue(ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_Amt());                                                                       //Natural: ASSIGN #DR-AMT := INV-ACCT-LEDGR-AMT
                pnd_Ws_Pnd_Dr_Cv.setValue("AD=D");                                                                                                                        //Natural: ASSIGN #DR-CV := ( AD = D )
                pnd_Ws_Pnd_Cr_Cv.setValue("AD=N");                                                                                                                        //Natural: ASSIGN #CR-CV := ( AD = N )
                pnd_Ws_Pnd_Cr_Amt.reset();                                                                                                                                //Natural: RESET #CR-AMT
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //* * RS  AT BREAK OF #LGR-INTRFCE-RPT.CNTRCT-ANNTY-INS-TYPE                                                                                                  //Natural: AT BREAK OF #LGR-INTRFCE-RPT.INV-ACCT-LEDGR-NBR;//Natural: AT BREAK OF #LGR-INTRFCE-RPT.INV-ACCT-CDE;//Natural: AT BREAK OF ACCT-COMPANY
            //* * RS    #ANNTY-BREAK               := TRUE
            //* * RS  END-BREAK
                                                                                                                                                                          //Natural: AT BREAK OF #LGR-INTRFCE-RPT.PYMNT-CHECK-DTE;//Natural: AT BREAK OF #LGR-INTRFCE-RPT.CNTRCT-ORGN-CDE;//Natural: AT END OF DATA;//Natural: PERFORM TOTAL-RTN
            sub_Total_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
            sub_Init_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(0, "=",pnd_Ws_Pnd_Fund_Sub,NEWLINE,"=",ldaFcpl199a.getPnd_Fund_Lda_Pnd_Inv_Acct_Desc_8().getValue("*"),NEWLINE,"=",ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Cde()); //Natural: WRITE '=' #FUND-SUB / '=' #INV-ACCT-DESC-8 ( * ) / '=' #LGR-INTRFCE-RPT.INV-ACCT-CDE
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new TabSetting(15),"/ACCOUNT",                                     //Natural: DISPLAY ( 1 ) ( HC = L ) 15T '/ACCOUNT' #INV-ACCT-DESC-8 ( #FUND-SUB ) ( IS = ON ) 'LEDGER/NUMBER' #LGR-INTRFCE-RPT.INV-ACCT-LEDGR-NBR '/LEDGER DESCRIPTION' #FCPA121.INV-ACCT-LEDGR-DESC 'DEBIT/AMOUNT' #DR-AMT ( HC = R IC = $ CV = #DR-CV ) 'CREDIT/AMOUNT' #CR-AMT ( HC = R IC = $ CV = #CR-CV )
            		ldaFcpl199a.getPnd_Fund_Lda_Pnd_Inv_Acct_Desc_8().getValue(pnd_Ws_Pnd_Fund_Sub), new IdenticalSuppress(true),"LEDGER/NUMBER",
            		ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_Nbr(),"/LEDGER DESCRIPTION",
            		pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc(),"DEBIT/AMOUNT",
            		pnd_Ws_Pnd_Dr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new InsertionCharacter("$"), pnd_Ws_Pnd_Dr_Cv,"CREDIT/AMOUNT",
                
            		pnd_Ws_Pnd_Cr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new InsertionCharacter("$"), pnd_Ws_Pnd_Cr_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Pnd_Cr_AmtSum158.nadd(readWork01Pnd_Cr_AmtSum158,pnd_Ws_Pnd_Cr_Amt);                                                                                //Natural: END-WORK
            readWork01Pnd_Cr_AmtSum.nadd(readWork01Pnd_Cr_AmtSum,pnd_Ws_Pnd_Cr_Amt);
            readWork01Pnd_Dr_AmtSum158.nadd(readWork01Pnd_Dr_AmtSum158,pnd_Ws_Pnd_Dr_Amt);
            readWork01Pnd_Dr_AmtSum.nadd(readWork01Pnd_Dr_AmtSum,pnd_Ws_Pnd_Dr_Amt);
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM TOTAL-RTN
            sub_Total_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getReports().display(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(25),"RECORDS READ",                                    //Natural: DISPLAY ( 2 ) ( HC = R ) 25T 'RECORDS READ' #REC-CNT 'CREDIT TOTAL' #CR-TOTAL 'DEBIT TOTAL' #DR-TOTAL
        		pnd_Ws_Pnd_Rec_Cnt,"CREDIT TOTAL",
        		pnd_Ws_Pnd_Cr_Total,"DEBIT TOTAL",
        		pnd_Ws_Pnd_Dr_Total);
        if (Global.isEscape()) return;
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-RTN
        //* * RS  WHEN #ANNTY-BREAK
        //* * RS   #ANNTY-BREAK                  := FALSE
        //* * RS    EXAMINE #ANNTY(*) FOR #LGR-INTRFCE-RPT.CNTRCT-ANNTY-INS-TYPE
        //* * RS     GIVING INDEX IN #ANNTY-IDX
        //* * RS    IF #ANNTY-IDX = 0
        //* * RS      #ANNTY-IDX                  := #ANNTY-IDX-1
        //* * RS    END-IF
        //* * RS    #NEWPAGE-IND                  := TRUE
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-RTN
    }
    private void sub_Init_Rtn() throws Exception                                                                                                                          //Natural: INIT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        short decideConditionsMet199 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #LEDGER-BREAK
        if (condition(pnd_Ws_Pnd_Ledger_Break.getBoolean()))
        {
            decideConditionsMet199++;
            pnd_Ws_Pnd_Ledger_Break.setValue(false);                                                                                                                      //Natural: ASSIGN #LEDGER-BREAK := FALSE
            pnd_Ws_Pnd_Isa_Sub.setValue(ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Cde());                                                                                //Natural: ASSIGN #ISA-SUB := #LGR-INTRFCE-RPT.INV-ACCT-CDE
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Isa().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Isa_Sub.getInt() + 1));                        //Natural: ASSIGN #FCPA121.INV-ACCT-ISA := #ISA-LDA.#ISA-CDE ( #ISA-SUB )
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_Nbr());                                              //Natural: ASSIGN #FCPA121.INV-ACCT-LEDGR-NBR := #LGR-INTRFCE-RPT.INV-ACCT-LEDGR-NBR
            DbsUtil.callnat(Fcpn121.class , getCurrentProcessState(), pdaFcpa121.getPnd_Fcpa121());                                                                       //Natural: CALLNAT 'FCPN121' USING #FCPA121
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: WHEN #FUND-BREAK
        if (condition(pnd_Ws_Pnd_Fund_Break.getBoolean()))
        {
            decideConditionsMet199++;
            pnd_Ws_Pnd_Fund_Break.setValue(false);                                                                                                                        //Natural: ASSIGN #FUND-BREAK := FALSE
            pnd_Ws_Pnd_Fund_Sub.setValue(ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Cde());                                                                               //Natural: ASSIGN #FUND-SUB := #LGR-INTRFCE-RPT.INV-ACCT-CDE
        }                                                                                                                                                                 //Natural: WHEN #COMPANY-BREAK
        if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))
        {
            decideConditionsMet199++;
            pnd_Ws_Pnd_Company_Break.setValue(false);                                                                                                                     //Natural: ASSIGN #COMPANY-BREAK := FALSE
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN #CHECK-DTE-BREAK
        if (condition(pnd_Ws_Pnd_Check_Dte_Break.getBoolean()))
        {
            decideConditionsMet199++;
            pnd_Ws_Pnd_Check_Dte_Break.setValue(false);                                                                                                                   //Natural: ASSIGN #CHECK-DTE-BREAK := FALSE
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN #ORGN-BREAK
        if (condition(pnd_Ws_Pnd_Orgn_Break.getBoolean()))
        {
            decideConditionsMet199++;
            pnd_Ws_Pnd_Orgn_Break.setValue(false);                                                                                                                        //Natural: ASSIGN #ORGN-BREAK := FALSE
            DbsUtil.examine(new ExamineSource(ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Code().getValue("*")), new ExamineSearch(ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Cntrct_Orgn_Cde()),  //Natural: EXAMINE #ORGN-CODE ( * ) FOR #LGR-INTRFCE-RPT.CNTRCT-ORGN-CDE GIVING INDEX IN #ORGN-IDX
                new ExamineGivingIndex(ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Idx()));
            if (condition(ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Idx().equals(getZero())))                                                                                  //Natural: IF #ORGN-IDX = 0
            {
                ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Idx().setValue(ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Idx_1());                                                        //Natural: ASSIGN #ORGN-IDX := #ORGN-IDX-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet199 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Newpage_Ind.getBoolean()))                                                                                                               //Natural: IF #NEWPAGE-IND
        {
            pnd_Ws_Pnd_Newpage_Ind.setValue(false);                                                                                                                       //Natural: ASSIGN #NEWPAGE-IND := FALSE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Total_Rtn() throws Exception                                                                                                                         //Natural: TOTAL-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        short decideConditionsMet232 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #COMPANY-BREAK
        if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))
        {
            decideConditionsMet232++;
            getReports().write(1, NEWLINE,new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),"-------------------",new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),"-------------------",NEWLINE,new  //Natural: WRITE ( 1 ) / T*#DR-AMT '-------------------' T*#CR-AMT '-------------------' / T*#INV-ACCT-DESC-8 'TOTAL:' T*#DR-AMT 2X #TOT-DR-AMT T*#CR-AMT 2X #TOT-CR-AMT
                ReportTAsterisk(ldaFcpl199a.getPnd_Fund_Lda_Pnd_Inv_Acct_Desc_8()),"TOTAL:",new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(2),pnd_Ws_Pnd_Tot_Dr_Amt, 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new ColumnSpacing(2),pnd_Ws_Pnd_Tot_Cr_Amt, new ReportEditMask 
                ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet232 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_NbrIsBreak = ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_Nbr().isBreak(endOfData);
        boolean ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Inv_Acct_CdeIsBreak = ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Cde().isBreak(endOfData);
        boolean ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Acct_CompanyIsBreak = ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Acct_Company().isBreak(endOfData);
        boolean ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Pymnt_Check_DteIsBreak = ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Pymnt_Check_Dte().isBreak(endOfData);
        boolean ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Cntrct_Orgn_CdeIsBreak = ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Cntrct_Orgn_Cde().isBreak(endOfData);
        if (condition(ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_NbrIsBreak || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Inv_Acct_CdeIsBreak || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Acct_CompanyIsBreak 
            || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Pymnt_Check_DteIsBreak || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Ledger_Break.setValue(true);                                                                                                                       //Natural: ASSIGN #LEDGER-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Inv_Acct_CdeIsBreak || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Acct_CompanyIsBreak || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Pymnt_Check_DteIsBreak 
            || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Fund_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #FUND-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Acct_CompanyIsBreak || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Pymnt_Check_DteIsBreak || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Company_Break.setValue(true);                                                                                                                      //Natural: ASSIGN #COMPANY-BREAK := TRUE
            pnd_Ws_Pnd_Tot_Cr_Amt.setValue(readWork01Pnd_Cr_AmtSum158);                                                                                                   //Natural: ASSIGN #TOT-CR-AMT := SUM ( #CR-AMT )
            pnd_Ws_Pnd_Tot_Dr_Amt.setValue(readWork01Pnd_Dr_AmtSum158);                                                                                                   //Natural: ASSIGN #TOT-DR-AMT := SUM ( #DR-AMT )
            pnd_Ws_Pnd_Cr_Total.nadd(pnd_Ws_Pnd_Tot_Cr_Amt);                                                                                                              //Natural: ADD #TOT-CR-AMT TO #CR-TOTAL
            pnd_Ws_Pnd_Dr_Total.nadd(pnd_Ws_Pnd_Tot_Dr_Amt);                                                                                                              //Natural: ADD #TOT-DR-AMT TO #DR-TOTAL
            readWork01Pnd_Cr_AmtSum158.setDec(new DbsDecimal(0));                                                                                                         //Natural: END-BREAK
            readWork01Pnd_Dr_AmtSum158.setDec(new DbsDecimal(0));
        }
        if (condition(ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Pymnt_Check_DteIsBreak || ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Check_Dte_Break.setValue(true);                                                                                                                    //Natural: ASSIGN #CHECK-DTE-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl826_getPnd_Lgr_Intrfce_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Orgn_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #ORGN-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");
        Global.format(2, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(58),"INTERFACE REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,new TabSetting(49),ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Code_Desc(),NEWLINE,new 
            TabSetting(58),"DUE ON",ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Pymnt_Check_Dte(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(58),"COMPANY:",
            ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Acct_Company(),NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(59),"CONTROL REPORT",new TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new TabSetting(15),"/ACCOUNT",
        		ldaFcpl199a.getPnd_Fund_Lda_Pnd_Inv_Acct_Desc_8(), new IdenticalSuppress(true),"LEDGER/NUMBER",
        		ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Ledgr_Nbr(),"/LEDGER DESCRIPTION",
        		pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc(),"DEBIT/AMOUNT",
        		pnd_Ws_Pnd_Dr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new InsertionCharacter("$"), pnd_Ws_Pnd_Dr_Cv,"CREDIT/AMOUNT",
            
        		pnd_Ws_Pnd_Cr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new InsertionCharacter("$"), pnd_Ws_Pnd_Cr_Cv);
        getReports().setDisplayColumns(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(25),"RECORDS READ",
        		pnd_Ws_Pnd_Rec_Cnt,"CREDIT TOTAL",
        		pnd_Ws_Pnd_Cr_Total,"DEBIT TOTAL",
        		pnd_Ws_Pnd_Dr_Total);
    }
    private void CheckAtStartofData135() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
            sub_Init_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
