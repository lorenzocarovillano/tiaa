/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:43 PM
**        * FROM NATURAL PROGRAM : Fcpp835v
************************************************************
**        * FILE NAME            : Fcpp835v.java
**        * CLASS NAME           : Fcpp835v
**        * INSTANCE NAME        : Fcpp835v
************************************************************
************************************************************************
* PROGRAM  : FCPP835V
* SYSTEM   : CPS - LANDRUM
* FUNC     : EXTRACT CPS CANCELS STOPS FOR VOID PROCESSING.
* 8/8/2007-LANDRUM-EXPAND DATE FOR AP REC SELECTION TO TODAY - 6 MONTHS.
*
* 2/2008  - RONDEAU - ELIMINATE SELECTION OF RECORDS BASED ON DATE.
*           FIN-848   INSTEAD, USE DESCRIPTOR CNR-CS-STOP-CANCEL-STATUS
*                     TO IDENTIFY CANCELS REGARDLESS OF DATE.  ONLY
*                     PROCESS THOSE THAT BELONG IN THIS PROGRAM.
* 07/01/2015 FENDAYA  COR/NAS SUNSET. FE201506
* 04/2017    JJG      PIN EXPANSION CHANGES
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp835v extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaCpsa110 pdaCpsa110;
    private PdaFcpaaddr pdaFcpaaddr;
    private LdaFcpladdr ldaFcpladdr;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_S_Key;

    private DbsGroup pnd_S_Key__R_Field_1;
    private DbsField pnd_S_Key_Pnd_S_Key_Stats_Cde;
    private DbsField pnd_S_Key_Pnd_S_Key_Pay_Type_Req_Ind;
    private DbsField pnd_S_Key_Pnd_S_Key_Trans_Stats_Cde;
    private DbsField pnd_E_Key;
    private DbsField pnd_Date_D;
    private DbsField pnd_Date_Yyyymmdd;

    private DbsGroup pnd_Date_Yyyymmdd__R_Field_2;
    private DbsField pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_N;

    private DbsGroup pnd_Output_Delete_Rec;
    private DbsField pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_3;
    private DbsField pnd_Output_Delete_Rec_Pnd_Customer_Id_A8;
    private DbsField pnd_Output_Delete_Rec_Pnd_Record_Type;
    private DbsField pnd_Output_Delete_Rec_Pnd_Bank_Num;
    private DbsField pnd_Output_Delete_Rec_Pnd_Account_Num;
    private DbsField pnd_Output_Delete_Rec_Pnd_Run_Date;
    private DbsField pnd_Output_Delete_Rec_Pnd_Run_Time;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_4;
    private DbsField pnd_Output_Delete_Rec_Pnd_Run_Time_A6;
    private DbsField pnd_Output_Delete_Rec_Pnd_Input_Type;
    private DbsField pnd_Output_Delete_Rec_Pnd_Stop_Type;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_5;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N1;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_6;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N3;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N7;
    private DbsField pnd_Output_Delete_Rec_Pnd_Ending_Check_Num;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Amt;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_7;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Amt_N5;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Amt_N10;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_N;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_8;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A;
    private DbsField pnd_Output_Delete_Rec_Pnd_Payee_Name;
    private DbsField pnd_Output_Delete_Rec_Pnd_Reason;
    private DbsField pnd_Output_Delete_Rec_Pnd_Auto_Check_Paid_Inquiry;
    private DbsField pnd_Output_Delete_Rec_Pnd_Stop_Duration_Years;
    private DbsField pnd_Output_Delete_Rec_Pnd_User_Id;

    private DataAccessProgramView vw_pymnt;
    private DbsField pymnt_Cntrct_Ppcn_Nbr;

    private DbsGroup pymnt__R_Field_9;
    private DbsField pymnt_Cntrct_Ppcn_A8;
    private DbsField pymnt_Cntrct_Ppcn_A2;
    private DbsField pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField pymnt_Pymnt_Stats_Cde;
    private DbsField pymnt_Pymnt_Pay_Type_Req_Ind;
    private DbsField pymnt_Pymnt_Check_Dte;
    private DbsField pymnt_Pymnt_Source_Cde;
    private DbsField pymnt_Pymnt_Transmission_Stats_Cde;
    private DbsField pymnt_Cnr_Cs_Actvty_Cde;
    private DbsField pymnt_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField pymnt_Cntrct_Invrse_Dte;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pymnt_Cntrct_Payee_Cde;
    private DbsField pymnt_Cntrct_Orgn_Cde;
    private DbsField pymnt_Pymnt_Check_Nbr;
    private DbsField pymnt_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pymnt__R_Field_10;
    private DbsField pymnt_Pymnt_Prcss_Seq_Num;
    private DbsField pymnt_Pymnt_Instmt_Nbr;
    private DbsField pymnt_Pymnt_Check_Amt;

    private DbsGroup pymnt__R_Field_11;
    private DbsField pymnt_Pymnt_Amt;
    private DbsField pymnt_Cntrct_Cmbn_Nbr;
    private DbsField pymnt_Invrse_Process_Dte;
    private DbsField pymnt_Pymnt_Nbr;

    private DbsGroup pymnt__R_Field_12;
    private DbsField pymnt_Pnd_Pymnt_Nbr_N3;
    private DbsField pymnt_Pnd_Pymnt_Nbr_N7;
    private DbsField pymnt_Cnr_Cs_Stop_Cancel_Status;
    private DbsField xref_Key;

    private DbsGroup xref_Key__R_Field_13;
    private DbsField xref_Key_Ph_Unique_Id_Nbr;
    private DbsField xref_Key_Ph_Rcd_Type_Cde;
    private DbsField pnd_Addr_Key;

    private DbsGroup pnd_Addr_Key__R_Field_14;
    private DbsField pnd_Addr_Key_Pnd_Addr_Ppcn;
    private DbsField pnd_Addr_Key_Pnd_Addr_Cde;
    private DbsField pnd_Addr_Key_Pnd_Addr_Orgn;
    private DbsField pnd_Addr_Key_Pnd_Addr_Invdt;
    private DbsField pnd_Addr_Key_Pnd_Addr_Seq;
    private DbsField pnd_Cnt_Read;
    private DbsField pnd_Cnt_Stop;
    private DbsField pnd_Cnt_Cancel;
    private DbsField pnd_Cnt_Can_No_Redr;
    private DbsField pnd_Cnt_Stop_No_Redr;
    private DbsField pnd_Cnt_C_And_Cn;
    private DbsField pnd_Cnt_Other;
    private DbsField pnd_Prev_Check_Nbr;
    private DbsField pnd_Cmbne_Nbr;

    private DbsGroup pnd_Cmbne_Nbr__R_Field_15;
    private DbsField pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Acct_Total;
    private DbsField pnd_Acct_Count;
    private DbsField pnd_Ppy_Total_Amount;
    private DbsField pnd_Ppy_Total_Count;
    private DbsField pnd_Detail_Rec_Cnt;
    private DbsField pnd_New_Account;
    private DbsField pnd_Acctg_Date_D;
    private DbsField pnd_D;
    private DbsField pnd_Date_Mmddyyyy;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Rpt_Cnt;
    private DbsField pnd_Sd_Read;
    private DbsField pnd_Cancels_Read;
    private DbsField pnd_Bypassed_Origin_Cd;
    private DbsField pnd_Bypassed_Acct_Nbr;
    private DbsField pnd_Bypassed_Stats_Cd;
    private DbsField pnd_Bypassed_Activity_Cd;
    private DbsField pnd_Cancels_Processed;
    private DbsField pnd_Cancel_Isn;

    private DbsGroup pnd_Cancel_Isn__R_Field_16;
    private DbsField pnd_Cancel_Isn_Pnd_Cancel_Isn_A;
    private DbsField pnd_Control_Totals_Printed;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCpsa110 = new PdaCpsa110(localVariables);
        pdaFcpaaddr = new PdaFcpaaddr(localVariables);
        ldaFcpladdr = new LdaFcpladdr();
        registerRecord(ldaFcpladdr);
        registerRecord(ldaFcpladdr.getVw_fcp_Cons_Addr());
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables
        pnd_S_Key = localVariables.newFieldInRecord("pnd_S_Key", "#S-KEY", FieldType.STRING, 3);

        pnd_S_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_S_Key__R_Field_1", "REDEFINE", pnd_S_Key);
        pnd_S_Key_Pnd_S_Key_Stats_Cde = pnd_S_Key__R_Field_1.newFieldInGroup("pnd_S_Key_Pnd_S_Key_Stats_Cde", "#S-KEY-STATS-CDE", FieldType.STRING, 1);
        pnd_S_Key_Pnd_S_Key_Pay_Type_Req_Ind = pnd_S_Key__R_Field_1.newFieldInGroup("pnd_S_Key_Pnd_S_Key_Pay_Type_Req_Ind", "#S-KEY-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_S_Key_Pnd_S_Key_Trans_Stats_Cde = pnd_S_Key__R_Field_1.newFieldInGroup("pnd_S_Key_Pnd_S_Key_Trans_Stats_Cde", "#S-KEY-TRANS-STATS-CDE", FieldType.STRING, 
            1);
        pnd_E_Key = localVariables.newFieldInRecord("pnd_E_Key", "#E-KEY", FieldType.STRING, 3);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.DATE);
        pnd_Date_Yyyymmdd = localVariables.newFieldInRecord("pnd_Date_Yyyymmdd", "#DATE-YYYYMMDD", FieldType.STRING, 8);

        pnd_Date_Yyyymmdd__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Yyyymmdd__R_Field_2", "REDEFINE", pnd_Date_Yyyymmdd);
        pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_N = pnd_Date_Yyyymmdd__R_Field_2.newFieldInGroup("pnd_Date_Yyyymmdd_Pnd_Date_Yyyymmdd_N", "#DATE-YYYYMMDD-N", 
            FieldType.NUMERIC, 8);

        pnd_Output_Delete_Rec = localVariables.newGroupInRecord("pnd_Output_Delete_Rec", "#OUTPUT-DELETE-REC");
        pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D", "#CUSTOMER-ID-NUM-D", 
            FieldType.NUMERIC, 8);

        pnd_Output_Delete_Rec__R_Field_3 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_3", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D);
        pnd_Output_Delete_Rec_Pnd_Customer_Id_A8 = pnd_Output_Delete_Rec__R_Field_3.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Customer_Id_A8", "#CUSTOMER-ID-A8", 
            FieldType.STRING, 8);
        pnd_Output_Delete_Rec_Pnd_Record_Type = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Output_Delete_Rec_Pnd_Bank_Num = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Bank_Num", "#BANK-NUM", FieldType.NUMERIC, 
            3);
        pnd_Output_Delete_Rec_Pnd_Account_Num = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Account_Num", "#ACCOUNT-NUM", FieldType.NUMERIC, 
            13);
        pnd_Output_Delete_Rec_Pnd_Run_Date = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Run_Date", "#RUN-DATE", FieldType.NUMERIC, 
            8);
        pnd_Output_Delete_Rec_Pnd_Run_Time = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Run_Time", "#RUN-TIME", FieldType.NUMERIC, 
            6);

        pnd_Output_Delete_Rec__R_Field_4 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_4", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Run_Time);
        pnd_Output_Delete_Rec_Pnd_Run_Time_A6 = pnd_Output_Delete_Rec__R_Field_4.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Run_Time_A6", "#RUN-TIME-A6", 
            FieldType.STRING, 6);
        pnd_Output_Delete_Rec_Pnd_Input_Type = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Input_Type", "#INPUT-TYPE", FieldType.STRING, 
            2);
        pnd_Output_Delete_Rec_Pnd_Stop_Type = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Stop_Type", "#STOP-TYPE", FieldType.STRING, 
            1);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num", "#BEGINNING-CHECK-NUM", 
            FieldType.NUMERIC, 11);

        pnd_Output_Delete_Rec__R_Field_5 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_5", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N1 = pnd_Output_Delete_Rec__R_Field_5.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N1", 
            "#BEGINNING-CHECK-NUM-N1", FieldType.NUMERIC, 1);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10 = pnd_Output_Delete_Rec__R_Field_5.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10", 
            "#BEGINNING-CHECK-NUM-N10", FieldType.NUMERIC, 10);

        pnd_Output_Delete_Rec__R_Field_6 = pnd_Output_Delete_Rec__R_Field_5.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_6", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N3 = pnd_Output_Delete_Rec__R_Field_6.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N3", 
            "#BEGINNING-CHECK-NUM-N3", FieldType.NUMERIC, 3);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N7 = pnd_Output_Delete_Rec__R_Field_6.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N7", 
            "#BEGINNING-CHECK-NUM-N7", FieldType.NUMERIC, 7);
        pnd_Output_Delete_Rec_Pnd_Ending_Check_Num = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Ending_Check_Num", "#ENDING-CHECK-NUM", 
            FieldType.NUMERIC, 11);
        pnd_Output_Delete_Rec_Pnd_Check_Amt = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Amt", "#CHECK-AMT", FieldType.NUMERIC, 
            15, 2);

        pnd_Output_Delete_Rec__R_Field_7 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_7", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Check_Amt);
        pnd_Output_Delete_Rec_Pnd_Check_Amt_N5 = pnd_Output_Delete_Rec__R_Field_7.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Amt_N5", "#CHECK-AMT-N5", 
            FieldType.NUMERIC, 5);
        pnd_Output_Delete_Rec_Pnd_Check_Amt_N10 = pnd_Output_Delete_Rec__R_Field_7.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Amt_N10", "#CHECK-AMT-N10", 
            FieldType.NUMERIC, 10, 2);
        pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_N = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_N", "#CHECK-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Output_Delete_Rec__R_Field_8 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_8", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_N);
        pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A = pnd_Output_Delete_Rec__R_Field_8.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A", 
            "#CHECK-ISSUE-DATE-A", FieldType.STRING, 8);
        pnd_Output_Delete_Rec_Pnd_Payee_Name = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Payee_Name", "#PAYEE-NAME", FieldType.STRING, 
            10);
        pnd_Output_Delete_Rec_Pnd_Reason = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Reason", "#REASON", FieldType.STRING, 10);
        pnd_Output_Delete_Rec_Pnd_Auto_Check_Paid_Inquiry = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Auto_Check_Paid_Inquiry", 
            "#AUTO-CHECK-PAID-INQUIRY", FieldType.STRING, 1);
        pnd_Output_Delete_Rec_Pnd_Stop_Duration_Years = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Stop_Duration_Years", "#STOP-DURATION-YEARS", 
            FieldType.NUMERIC, 1);
        pnd_Output_Delete_Rec_Pnd_User_Id = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_User_Id", "#USER-ID", FieldType.NUMERIC, 
            6);

        vw_pymnt = new DataAccessProgramView(new NameInfo("vw_pymnt", "PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        pymnt_Cntrct_Ppcn_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");

        pymnt__R_Field_9 = vw_pymnt.getRecord().newGroupInGroup("pymnt__R_Field_9", "REDEFINE", pymnt_Cntrct_Ppcn_Nbr);
        pymnt_Cntrct_Ppcn_A8 = pymnt__R_Field_9.newFieldInGroup("pymnt_Cntrct_Ppcn_A8", "CNTRCT-PPCN-A8", FieldType.STRING, 8);
        pymnt_Cntrct_Ppcn_A2 = pymnt__R_Field_9.newFieldInGroup("pymnt_Cntrct_Ppcn_A2", "CNTRCT-PPCN-A2", FieldType.STRING, 2);
        pymnt_Cntrct_Unq_Id_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_UNQ_ID_NBR");
        pymnt_Pymnt_Stats_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        pymnt_Pymnt_Pay_Type_Req_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        pymnt_Pymnt_Check_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        pymnt_Pymnt_Source_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Source_Cde", "PYMNT-SOURCE-CDE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "PYMNT_SOURCE_CDE");
        pymnt_Pymnt_Transmission_Stats_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Transmission_Stats_Cde", "PYMNT-TRANSMISSION-STATS-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PYMNT_TRANSMISSION_STATS_CDE");
        pymnt_Cnr_Cs_Actvty_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNR_CS_ACTVTY_CDE");
        pymnt_Cnr_Cs_Pymnt_Acctg_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_PYMNT_ACCTG_DTE");
        pymnt_Cntrct_Invrse_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        pymnt_Cntrct_Cancel_Rdrw_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_IND");
        pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        pymnt_Cntrct_Payee_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        pymnt_Cntrct_Orgn_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        pymnt_Pymnt_Check_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_NBR");
        pymnt_Pymnt_Prcss_Seq_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_PRCSS_SEQ_NBR");

        pymnt__R_Field_10 = vw_pymnt.getRecord().newGroupInGroup("pymnt__R_Field_10", "REDEFINE", pymnt_Pymnt_Prcss_Seq_Nbr);
        pymnt_Pymnt_Prcss_Seq_Num = pymnt__R_Field_10.newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pymnt_Pymnt_Instmt_Nbr = pymnt__R_Field_10.newFieldInGroup("pymnt_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pymnt_Pymnt_Check_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_AMT");

        pymnt__R_Field_11 = vw_pymnt.getRecord().newGroupInGroup("pymnt__R_Field_11", "REDEFINE", pymnt_Pymnt_Check_Amt);
        pymnt_Pymnt_Amt = pymnt__R_Field_11.newFieldInGroup("pymnt_Pymnt_Amt", "PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pymnt_Cntrct_Cmbn_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        pymnt_Invrse_Process_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Invrse_Process_Dte", "INVRSE-PROCESS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "INVRSE_PROCESS_DTE");
        pymnt_Pymnt_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, "PYMNT_NBR");

        pymnt__R_Field_12 = vw_pymnt.getRecord().newGroupInGroup("pymnt__R_Field_12", "REDEFINE", pymnt_Pymnt_Nbr);
        pymnt_Pnd_Pymnt_Nbr_N3 = pymnt__R_Field_12.newFieldInGroup("pymnt_Pnd_Pymnt_Nbr_N3", "#PYMNT-NBR-N3", FieldType.NUMERIC, 3);
        pymnt_Pnd_Pymnt_Nbr_N7 = pymnt__R_Field_12.newFieldInGroup("pymnt_Pnd_Pymnt_Nbr_N7", "#PYMNT-NBR-N7", FieldType.NUMERIC, 7);
        pymnt_Cnr_Cs_Stop_Cancel_Status = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Cs_Stop_Cancel_Status", "CNR-CS-STOP-CANCEL-STATUS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNR_CS_STOP_CANCEL_STATUS");
        registerRecord(vw_pymnt);

        xref_Key = localVariables.newFieldInRecord("xref_Key", "XREF-KEY", FieldType.BINARY, 14);

        xref_Key__R_Field_13 = localVariables.newGroupInRecord("xref_Key__R_Field_13", "REDEFINE", xref_Key);
        xref_Key_Ph_Unique_Id_Nbr = xref_Key__R_Field_13.newFieldInGroup("xref_Key_Ph_Unique_Id_Nbr", "PH-UNIQUE-ID-NBR", FieldType.NUMERIC, 12);
        xref_Key_Ph_Rcd_Type_Cde = xref_Key__R_Field_13.newFieldInGroup("xref_Key_Ph_Rcd_Type_Cde", "PH-RCD-TYPE-CDE", FieldType.NUMERIC, 2);
        pnd_Addr_Key = localVariables.newFieldInRecord("pnd_Addr_Key", "#ADDR-KEY", FieldType.STRING, 31);

        pnd_Addr_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Addr_Key__R_Field_14", "REDEFINE", pnd_Addr_Key);
        pnd_Addr_Key_Pnd_Addr_Ppcn = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Ppcn", "#ADDR-PPCN", FieldType.STRING, 10);
        pnd_Addr_Key_Pnd_Addr_Cde = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Cde", "#ADDR-CDE", FieldType.STRING, 4);
        pnd_Addr_Key_Pnd_Addr_Orgn = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Orgn", "#ADDR-ORGN", FieldType.STRING, 2);
        pnd_Addr_Key_Pnd_Addr_Invdt = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Invdt", "#ADDR-INVDT", FieldType.NUMERIC, 8);
        pnd_Addr_Key_Pnd_Addr_Seq = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Seq", "#ADDR-SEQ", FieldType.NUMERIC, 7);
        pnd_Cnt_Read = localVariables.newFieldInRecord("pnd_Cnt_Read", "#CNT-READ", FieldType.NUMERIC, 7);
        pnd_Cnt_Stop = localVariables.newFieldInRecord("pnd_Cnt_Stop", "#CNT-STOP", FieldType.NUMERIC, 7);
        pnd_Cnt_Cancel = localVariables.newFieldInRecord("pnd_Cnt_Cancel", "#CNT-CANCEL", FieldType.NUMERIC, 7);
        pnd_Cnt_Can_No_Redr = localVariables.newFieldInRecord("pnd_Cnt_Can_No_Redr", "#CNT-CAN-NO-REDR", FieldType.NUMERIC, 7);
        pnd_Cnt_Stop_No_Redr = localVariables.newFieldInRecord("pnd_Cnt_Stop_No_Redr", "#CNT-STOP-NO-REDR", FieldType.NUMERIC, 7);
        pnd_Cnt_C_And_Cn = localVariables.newFieldInRecord("pnd_Cnt_C_And_Cn", "#CNT-C-AND-CN", FieldType.NUMERIC, 7);
        pnd_Cnt_Other = localVariables.newFieldInRecord("pnd_Cnt_Other", "#CNT-OTHER", FieldType.NUMERIC, 7);
        pnd_Prev_Check_Nbr = localVariables.newFieldInRecord("pnd_Prev_Check_Nbr", "#PREV-CHECK-NBR", FieldType.NUMERIC, 10);
        pnd_Cmbne_Nbr = localVariables.newFieldInRecord("pnd_Cmbne_Nbr", "#CMBNE-NBR", FieldType.STRING, 14);

        pnd_Cmbne_Nbr__R_Field_15 = localVariables.newGroupInRecord("pnd_Cmbne_Nbr__R_Field_15", "REDEFINE", pnd_Cmbne_Nbr);
        pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn = pnd_Cmbne_Nbr__R_Field_15.newFieldInGroup("pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn", "#CMBNE-PPCN", FieldType.STRING, 10);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Acct_Total = localVariables.newFieldInRecord("pnd_Acct_Total", "#ACCT-TOTAL", FieldType.NUMERIC, 12, 2);
        pnd_Acct_Count = localVariables.newFieldInRecord("pnd_Acct_Count", "#ACCT-COUNT", FieldType.NUMERIC, 5);
        pnd_Ppy_Total_Amount = localVariables.newFieldInRecord("pnd_Ppy_Total_Amount", "#PPY-TOTAL-AMOUNT", FieldType.NUMERIC, 12, 2);
        pnd_Ppy_Total_Count = localVariables.newFieldInRecord("pnd_Ppy_Total_Count", "#PPY-TOTAL-COUNT", FieldType.NUMERIC, 5);
        pnd_Detail_Rec_Cnt = localVariables.newFieldInRecord("pnd_Detail_Rec_Cnt", "#DETAIL-REC-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_New_Account = localVariables.newFieldInRecord("pnd_New_Account", "#NEW-ACCOUNT", FieldType.STRING, 1);
        pnd_Acctg_Date_D = localVariables.newFieldInRecord("pnd_Acctg_Date_D", "#ACCTG-DATE-D", FieldType.DATE);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);
        pnd_Date_Mmddyyyy = localVariables.newFieldInRecord("pnd_Date_Mmddyyyy", "#DATE-MMDDYYYY", FieldType.STRING, 10);
        pnd_Check_Date = localVariables.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.STRING, 8);
        pnd_Rpt_Cnt = localVariables.newFieldInRecord("pnd_Rpt_Cnt", "#RPT-CNT", FieldType.NUMERIC, 5);
        pnd_Sd_Read = localVariables.newFieldInRecord("pnd_Sd_Read", "#SD-READ", FieldType.NUMERIC, 8);
        pnd_Cancels_Read = localVariables.newFieldInRecord("pnd_Cancels_Read", "#CANCELS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypassed_Origin_Cd = localVariables.newFieldInRecord("pnd_Bypassed_Origin_Cd", "#BYPASSED-ORIGIN-CD", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypassed_Acct_Nbr = localVariables.newFieldInRecord("pnd_Bypassed_Acct_Nbr", "#BYPASSED-ACCT-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypassed_Stats_Cd = localVariables.newFieldInRecord("pnd_Bypassed_Stats_Cd", "#BYPASSED-STATS-CD", FieldType.PACKED_DECIMAL, 7);
        pnd_Bypassed_Activity_Cd = localVariables.newFieldInRecord("pnd_Bypassed_Activity_Cd", "#BYPASSED-ACTIVITY-CD", FieldType.PACKED_DECIMAL, 7);
        pnd_Cancels_Processed = localVariables.newFieldInRecord("pnd_Cancels_Processed", "#CANCELS-PROCESSED", FieldType.PACKED_DECIMAL, 7);
        pnd_Cancel_Isn = localVariables.newFieldInRecord("pnd_Cancel_Isn", "#CANCEL-ISN", FieldType.NUMERIC, 11);

        pnd_Cancel_Isn__R_Field_16 = localVariables.newGroupInRecord("pnd_Cancel_Isn__R_Field_16", "REDEFINE", pnd_Cancel_Isn);
        pnd_Cancel_Isn_Pnd_Cancel_Isn_A = pnd_Cancel_Isn__R_Field_16.newFieldInGroup("pnd_Cancel_Isn_Pnd_Cancel_Isn_A", "#CANCEL-ISN-A", FieldType.STRING, 
            11);
        pnd_Control_Totals_Printed = localVariables.newFieldInRecord("pnd_Control_Totals_Printed", "#CONTROL-TOTALS-PRINTED", FieldType.BOOLEAN, 1);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_pymnt.reset();

        ldaFcpladdr.initializeValues();

        localVariables.reset();
        pnd_S_Key.setInitialValue("D1");
        pnd_E_Key.setInitialValue("P1Y");
        pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D.setInitialValue(13241);
        pnd_Output_Delete_Rec_Pnd_Record_Type.setInitialValue("D");
        pnd_Output_Delete_Rec_Pnd_Bank_Num.setInitialValue(75);
        pnd_Output_Delete_Rec_Pnd_Account_Num.setInitialValue(-2147483648);
        pnd_Output_Delete_Rec_Pnd_Run_Date.setInitialValue(0);
        pnd_Output_Delete_Rec_Pnd_Run_Time.setInitialValue(0);
        pnd_Output_Delete_Rec_Pnd_Input_Type.setInitialValue("BS");
        pnd_Output_Delete_Rec_Pnd_Stop_Type.setInitialValue("E");
        pnd_Output_Delete_Rec_Pnd_Payee_Name.setInitialValue(" ");
        pnd_Output_Delete_Rec_Pnd_Reason.setInitialValue(" ");
        pnd_Output_Delete_Rec_Pnd_Auto_Check_Paid_Inquiry.setInitialValue("Y");
        pnd_Output_Delete_Rec_Pnd_Stop_Duration_Years.setInitialValue(2);
        pnd_Output_Delete_Rec_Pnd_User_Id.setInitialValue(48087);
        pnd_Cnt_Read.setInitialValue(0);
        pnd_Cnt_Stop.setInitialValue(0);
        pnd_Cnt_Cancel.setInitialValue(0);
        pnd_Cnt_Can_No_Redr.setInitialValue(0);
        pnd_Cnt_Stop_No_Redr.setInitialValue(0);
        pnd_Cnt_C_And_Cn.setInitialValue(0);
        pnd_Cnt_Other.setInitialValue(0);
        pnd_Header0_1.setInitialValue("          CPS Annuity Payments      ");
        pnd_Header0_2.setInitialValue(" CANCEL Checks extract for Wachovia VOIDs");
        pnd_New_Account.setInitialValue("N");
        pnd_Rpt_Cnt.setInitialValue(0);
        pnd_Sd_Read.setInitialValue(0);
        pnd_Control_Totals_Printed.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp835v() throws Exception
    {
        super("Fcpp835v");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP835V", onError);
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: FORMAT ( 1 ) LS = 133 PS = 55 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
            //*  FE201506
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
            sub_Open_Mq();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Acctg_Date_D.setValue(Global.getDATX());                                                                                                                      //Natural: MOVE *DATX TO #ACCTG-DATE-D
        getReports().write(0, "DERIVED ACCTG-DATE:",pnd_Acctg_Date_D, new ReportEditMask ("MM/DD/YYYY"));                                                                 //Natural: WRITE 'DERIVED ACCTG-DATE:' #ACCTG-DATE-D ( EM = MM/DD/YYYY )
        if (Global.isEscape()) return;
        pnd_Date_D.setValue(Global.getDATX());                                                                                                                            //Natural: COMPUTE #DATE-D = *DATX
        pnd_Date_Yyyymmdd.setValueEdited(pnd_Date_D,new ReportEditMask("YYYYMMDD"));                                                                                      //Natural: MOVE EDITED #DATE-D ( EM = YYYYMMDD ) TO #DATE-YYYYMMDD
        //* *COMPUTE #S-INV-DTE-N = 100000000 - #DATE-YYYYMMDD-N /* FIN-848
        //*  RL 8-8-2007
        pnd_Date_D.compute(new ComputeParameters(false, pnd_Date_D), Global.getDATX().subtract(180));                                                                     //Natural: COMPUTE #DATE-D = *DATX - 180
        pnd_Date_Yyyymmdd.setValueEdited(pnd_Date_D,new ReportEditMask("YYYYMMDD"));                                                                                      //Natural: MOVE EDITED #DATE-D ( EM = YYYYMMDD ) TO #DATE-YYYYMMDD
        //* *COMPUTE #E-INV-DTE-N = 100000000 - #DATE-YYYYMMDD-N /* FIN-848
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-ACCOUNT-HEADER
        sub_Display_Report_Account_Header();
        if (condition(Global.isEscape())) {return;}
        //* * CODE BELOW WAS COMMENTED OUT FOR FIN-848
        //* *PYMNT-196.
        //* *READ  PYMNT
        //* *    BY ORGN-STATUS-INVRSE-SEQ
        //* *    STARTING FROM #S-READ-KEY
        //* *  IF PYMNT.CNTRCT-ORGN-CDE NE 'AP' ESCAPE BOTTOM END-IF
        //* *  IF PYMNT-STATS-CDE NE 'P' ESCAPE BOTTOM END-IF
        //* *  ADD 1 TO #SD-READ
        //* *  IF PYMNT.CNTRCT-INVRSE-DTE GT #E-INV-DTE-N ESCAPE BOTTOM END-IF
        //* *  IF NOT (CNR-CS-ACTVTY-CDE ='C' OR= 'CN')
        //* *    ESCAPE TOP
        //* *  END-IF
        //* *  REJECT IF #PYMNT-NBR-N3 NE 220
        //* * COMMENT OUT NEXT LINE FOR PROD FIX TEST   /* RL 8-8-2007
        //* *   IF NOT (CNR-CS-PYMNT-ACCTG-DTE EQ #ACCTG-DATE-D)  ESCAPE TOP END-IF
        //* * CODE ABOVE WAS COMMENTED OUT FOR FIN-848
        //*  FIN-848
        //*  FIN-848
        vw_pymnt.startDatabaseRead                                                                                                                                        //Natural: READ PYMNT BY CNR-CS-STOP-CANCEL-STATUS FROM 'B'
        (
        "PYMNT_196",
        new Wc[] { new Wc("CNR_CS_STOP_CANCEL_STATUS", ">=", "B", WcType.BY) },
        new Oc[] { new Oc("CNR_CS_STOP_CANCEL_STATUS", "ASC") }
        );
        PYMNT_196:
        while (condition(vw_pymnt.readNextRow("PYMNT_196")))
        {
            //*  FIN-848
            if (condition(pymnt_Cnr_Cs_Stop_Cancel_Status.greater("B")))                                                                                                  //Natural: IF CNR-CS-STOP-CANCEL-STATUS > 'B'
            {
                //*  FIN-848
                if (true) break PYMNT_196;                                                                                                                                //Natural: ESCAPE BOTTOM ( PYMNT-196. ) IMMEDIATE
                //*  FIN-848
            }                                                                                                                                                             //Natural: END-IF
            //*  FIN-848
            pnd_Cancels_Read.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #CANCELS-READ
            //*  FIN-848
            if (condition(pymnt_Cntrct_Orgn_Cde.notEquals("AP")))                                                                                                         //Natural: IF PYMNT.CNTRCT-ORGN-CDE NOT = 'AP'
            {
                //*  FIN-848
                pnd_Bypassed_Origin_Cd.nadd(1);                                                                                                                           //Natural: ADD 1 TO #BYPASSED-ORIGIN-CD
                //*  FIN-848
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  FIN-848
            }                                                                                                                                                             //Natural: END-IF
            //*  FIN-848
            if (condition(pymnt_Pnd_Pymnt_Nbr_N3.notEquals(220)))                                                                                                         //Natural: IF #PYMNT-NBR-N3 NOT = 220
            {
                //*  FIN-848
                pnd_Bypassed_Acct_Nbr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #BYPASSED-ACCT-NBR
                //*  FIN-848
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  FIN-848
            }                                                                                                                                                             //Natural: END-IF
            //*  FIN-848
            if (condition(pymnt_Pymnt_Stats_Cde.notEquals("P")))                                                                                                          //Natural: IF PYMNT-STATS-CDE NOT = 'P'
            {
                //*  FIN-848
                pnd_Bypassed_Stats_Cd.nadd(1);                                                                                                                            //Natural: ADD 1 TO #BYPASSED-STATS-CD
                //*  FIN-848
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  FIN-848
            }                                                                                                                                                             //Natural: END-IF
            //*  JWO 2010-11
            if (condition(! (pymnt_Cnr_Cs_Actvty_Cde.equals("C") || pymnt_Cnr_Cs_Actvty_Cde.equals("CN") || pymnt_Cnr_Cs_Actvty_Cde.equals("CP") || pymnt_Cnr_Cs_Actvty_Cde.equals("RP")  //Natural: IF NOT ( CNR-CS-ACTVTY-CDE = 'C' OR = 'CN' OR = 'CP' OR = 'RP' OR = 'PR' )
                || pymnt_Cnr_Cs_Actvty_Cde.equals("PR"))))
            {
                //*  FIN-848
                pnd_Bypassed_Activity_Cd.nadd(1);                                                                                                                         //Natural: ADD 1 TO #BYPASSED-ACTIVITY-CD
                //*  FIN-848
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  FIN-848
            }                                                                                                                                                             //Natural: END-IF
            //*  FIN-848
            pnd_Cancels_Processed.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CANCELS-PROCESSED
            pnd_Sd_Read.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #SD-READ
            pnd_Cnt_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNT-READ
            //*  JWO 2010
            short decideConditionsMet649 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF PYMNT.CNR-CS-ACTVTY-CDE;//Natural: VALUE 'C'
            if (condition((pymnt_Cnr_Cs_Actvty_Cde.equals("C"))))
            {
                decideConditionsMet649++;
                pnd_Cnt_Cancel.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #CNT-CANCEL
            }                                                                                                                                                             //Natural: VALUE 'CN', 'CP', 'RP', 'PR'
            else if (condition((pymnt_Cnr_Cs_Actvty_Cde.equals("CN") || pymnt_Cnr_Cs_Actvty_Cde.equals("CP") || pymnt_Cnr_Cs_Actvty_Cde.equals("RP") || 
                pymnt_Cnr_Cs_Actvty_Cde.equals("PR"))))
            {
                decideConditionsMet649++;
                pnd_Cnt_Can_No_Redr.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-CAN-NO-REDR
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet649 > 0))
            {
                pnd_Cnt_C_And_Cn.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CNT-C-AND-CN
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N1.setValue(0);                                                                                                 //Natural: ASSIGN #BEGINNING-CHECK-NUM-N1 := 0
            pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10.setValue(pymnt_Pymnt_Nbr);                                                                                  //Natural: ASSIGN #BEGINNING-CHECK-NUM-N10 := PYMNT-NBR
            pnd_Output_Delete_Rec_Pnd_Ending_Check_Num.setValue(pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num);                                                           //Natural: ASSIGN #ENDING-CHECK-NUM := #BEGINNING-CHECK-NUM
            pnd_Output_Delete_Rec_Pnd_Check_Amt_N10.setValue(pymnt_Pymnt_Check_Amt);                                                                                      //Natural: ASSIGN #CHECK-AMT-N10 := PYMNT-CHECK-AMT
            pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A.setValueEdited(pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                            //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-ISSUE-DATE-A
            pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A);                                                            //Natural: MOVE EDITED #CHECK-ISSUE-DATE-A TO #D ( EM = YYYYMMDD )
            pnd_Date_Mmddyyyy.setValueEdited(pnd_D,new ReportEditMask("MM/DD/YYYY"));                                                                                     //Natural: MOVE EDITED #D ( EM = MM/DD/YYYY ) TO #DATE-MMDDYYYY
                                                                                                                                                                          //Natural: PERFORM GET-BANK-TABLE
            sub_Get_Bank_Table();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PYMNT_196"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PYMNT_196"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpaaddr.getAddr_Cntrct_Ppcn_Nbr().setValue(pymnt_Cntrct_Ppcn_Nbr);                                                                                        //Natural: ASSIGN ADDR.CNTRCT-PPCN-NBR := PYMNT.CNTRCT-PPCN-NBR
            pdaFcpaaddr.getAddr_Cntrct_Payee_Cde().setValue(pymnt_Cntrct_Payee_Cde);                                                                                      //Natural: ASSIGN ADDR.CNTRCT-PAYEE-CDE := PYMNT.CNTRCT-PAYEE-CDE
            pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().setValue(pymnt_Cntrct_Orgn_Cde);                                                                                        //Natural: ASSIGN ADDR.CNTRCT-ORGN-CDE := PYMNT.CNTRCT-ORGN-CDE
            pdaFcpaaddr.getAddr_Cntrct_Invrse_Dte().setValue(pymnt_Cntrct_Invrse_Dte);                                                                                    //Natural: ASSIGN ADDR.CNTRCT-INVRSE-DTE := PYMNT.CNTRCT-INVRSE-DTE
            pdaFcpaaddr.getAddr_Pymnt_Prcss_Seq_Nbr().setValue(pymnt_Pymnt_Prcss_Seq_Num);                                                                                //Natural: ASSIGN ADDR.PYMNT-PRCSS-SEQ-NBR := PYMNT.PYMNT-PRCSS-SEQ-NUM
            pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr().setValue(pymnt_Cntrct_Cmbn_Nbr);                                                                            //Natural: ASSIGN ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR := PYMNT.CNTRCT-CMBN-NBR
            DbsUtil.callnat(Fcpnaddr.class , getCurrentProcessState(), pdaFcpaaddr.getAddr_Work_Fields(), pdaFcpaaddr.getAddr());                                         //Natural: CALLNAT 'FCPNADDR' USING ADDR-WORK-FIELDS ADDR
            if (condition(Global.isEscape())) return;
            if (condition(pdaFcpaaddr.getAddr_Pymnt_Nme().getValue(1).getSubstring(1,3).equals("CR ") && pdaFcpaaddr.getAddr_Pymnt_Nme().getValue(2).notEquals(" ")))     //Natural: IF SUBSTRING ( ADDR.PYMNT-NME ( 1 ) ,1,3 ) EQ 'CR ' AND ADDR.PYMNT-NME ( 2 ) NE ' '
            {
                pnd_Output_Delete_Rec_Pnd_Payee_Name.setValue(pdaFcpaaddr.getAddr_Pymnt_Nme().getValue(2));                                                               //Natural: ASSIGN #OUTPUT-DELETE-REC.#PAYEE-NAME := ADDR.PYMNT-NME ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Delete_Rec_Pnd_Payee_Name.setValue(pdaFcpaaddr.getAddr_Pymnt_Nme().getValue(1));                                                               //Natural: ASSIGN #OUTPUT-DELETE-REC.#PAYEE-NAME := ADDR.PYMNT-NME ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Output_Delete_Rec_Pnd_Payee_Name.equals(" ") || pnd_Output_Delete_Rec_Pnd_Payee_Name.equals("CR    ")))                                     //Natural: IF #OUTPUT-DELETE-REC.#PAYEE-NAME = ' ' OR = 'CR    '
            {
                xref_Key_Ph_Unique_Id_Nbr.setValue(pymnt_Cntrct_Unq_Id_Nbr);                                                                                              //Natural: ASSIGN XREF-KEY.PH-UNIQUE-ID-NBR := PYMNT.CNTRCT-UNQ-ID-NBR
                //*    READ(1) COR-XREF-PH BY COR-SUPER-PIN-RCDTYPE  /* FE201506 START
                //*        STARTING FROM  XREF-KEY
                //*      MOVE PH-LAST-NME TO #OUTPUT-DELETE-REC.#PAYEE-NAME
                //*    END-READ
                pdaMdma101.getPnd_Mdma101().reset();                                                                                                                      //Natural: RESET #MDMA101
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pymnt_Cntrct_Unq_Id_Nbr);                                                                              //Natural: ASSIGN #MDMA101.#I-PIN-N12 := PYMNT.CNTRCT-UNQ-ID-NBR
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                              //Natural: CALLNAT 'MDMN101A' #MDMA101
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    DbsUtil.callnat(Mdmn101.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                               //Natural: CALLNAT 'MDMN101' #MDMA101
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                              //Natural: IF #MDMA101.#O-RETURN-CODE = '0000'
                {
                    pnd_Output_Delete_Rec_Pnd_Payee_Name.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                                           //Natural: ASSIGN #OUTPUT-DELETE-REC.#PAYEE-NAME := #MDMA101.#O-LAST-NAME
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Output_Delete_Rec);                                                                                                        //Natural: WRITE WORK FILE 1 #OUTPUT-DELETE-REC
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-DETAIL
            sub_Display_Report_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("PYMNT_196"))) break;
                else if (condition(Global.isEscapeBottomImmediate("PYMNT_196"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  FIN-848
            pnd_Cancel_Isn.setValue(vw_pymnt.getAstISN("PYMNT_196"));                                                                                                     //Natural: ASSIGN #CANCEL-ISN = *ISN ( PYMNT-196. )
            //*  FIN-848
            getWorkFiles().write(2, false, pnd_Cancel_Isn_Pnd_Cancel_Isn_A);                                                                                              //Natural: WRITE WORK FILE 2 #CANCEL-ISN-A
            pnd_Acct_Total.nadd(pymnt_Pymnt_Check_Amt);                                                                                                                   //Natural: COMPUTE #ACCT-TOTAL = #ACCT-TOTAL + PYMNT.PYMNT-CHECK-AMT
            pnd_Ppy_Total_Amount.nadd(pymnt_Pymnt_Check_Amt);                                                                                                             //Natural: COMPUTE #PPY-TOTAL-AMOUNT = #PPY-TOTAL-AMOUNT + PYMNT.PYMNT-CHECK-AMT
            pnd_Acct_Count.nadd(1);                                                                                                                                       //Natural: COMPUTE #ACCT-COUNT = #ACCT-COUNT + 1
            pnd_Ppy_Total_Count.nadd(1);                                                                                                                                  //Natural: COMPUTE #PPY-TOTAL-COUNT = #PPY-TOTAL-COUNT + 1
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-READ
        if (condition(vw_pymnt.getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-ACCOUNT-TRAILER
            sub_Display_Report_Account_Trailer();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CONTROL-TOTALS
            sub_Control_Totals();
            if (condition(Global.isEscape())) {return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"    CONTROL TOTALS    ",NEWLINE,"  COUNT         AMOUNT",new ColumnSpacing(7),NEWLINE,pnd_Ppy_Total_Count,  //Natural: WRITE ( 1 ) // '    CONTROL TOTALS    ' / '  COUNT         AMOUNT' 7X / #PPY-TOTAL-COUNT ( EM = ZZZ,ZZ9 ) 3X #PPY-TOTAL-AMOUNT ( EM = ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(3),pnd_Ppy_Total_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,Global.getPROGRAM(),"END OF RUN AT TIME:",Global.getTIME(),NEWLINE);                       //Natural: WRITE ( 1 ) /// *PROGRAM 'END OF RUN AT TIME:' *TIME /
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  FIN-848 ADDED THE IF STATEMENT BELOW.  OTHERWISE, NO CONTROL TOTALS
        //*          PRINTED IF DESCRIPTOR HAS NO "B" VALUES.
        //*  FIN-848
        if (condition(! (pnd_Control_Totals_Printed.getBoolean())))                                                                                                       //Natural: IF NOT #CONTROL-TOTALS-PRINTED
        {
            //*  FIN-848
                                                                                                                                                                          //Natural: PERFORM CONTROL-TOTALS
            sub_Control_Totals();
            if (condition(Global.isEscape())) {return;}
            //*  FIN-848
        }                                                                                                                                                                 //Natural: END-IF
        //*  ------------------------------ *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BANK-TABLE
        //*  -------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-ACCOUNT-HEADER
        //* *001T 'Bank Route:' CPSA110.BANK-ROUTING
        //* *001T 'Bank Route:' '00013241'
        //* *2X   'BANK NAME:'  CPSA110.BANK-ACCOUNT-NAME (AL=20)
        //* *2X   'Bank Account:' '2079950055011'
        //*  076T 'ALT'
        //*  076T 'IND'
        //*  ------------------------------------ *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-DETAIL
        //*  077T  #OUTPUT-DELETE-REC.#AL-INDICATOR
        //*  --------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-ACCOUNT-TRAILER
        //*  ----------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROL-TOTALS
        //*  ********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  ************************************************
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH'
        {
            //*  CLOSE MQ FE201504 END
            DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                  //Natural: FETCH RETURN 'MDMP0012'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                                                                                                                               //Natural: ON ERROR
    }
    private void sub_Get_Bank_Table() throws Exception                                                                                                                    //Natural: GET-BANK-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------ *
        pdaCpsa110.getCpsa110().reset();                                                                                                                                  //Natural: RESET CPSA110
        pdaCpsa110.getCpsa110_Cpsa110_Function().setValue("               ");                                                                                             //Natural: MOVE '               ' TO CPSA110.CPSA110-FUNCTION
        pdaCpsa110.getCpsa110_Cpsa110_Source_Code().setValue(pymnt_Pymnt_Source_Cde);                                                                                     //Natural: MOVE PYMNT.PYMNT-SOURCE-CDE TO CPSA110.CPSA110-SOURCE-CODE
        DbsUtil.callnat(Cpsn110.class , getCurrentProcessState(), pdaCpsa110.getCpsa110());                                                                               //Natural: CALLNAT 'CPSN110' CPSA110
        if (condition(Global.isEscape())) return;
        //* *IF  CPSA110.CPSA110-RETURN-CODE NE '00'
        pdaCpsa110.getCpsa110_Bank_Routing().setValue("031100225");                                                                                                       //Natural: MOVE '031100225' TO CPSA110.BANK-ROUTING
        pdaCpsa110.getCpsa110_Bank_Account_Name().setValue("Wachovia Bank N.A.");                                                                                         //Natural: MOVE 'Wachovia Bank N.A.' TO CPSA110.BANK-ACCOUNT-NAME
        //* *END-IF
        //*  GET-BANK-TABLE
    }
    private void sub_Display_Report_Account_Header() throws Exception                                                                                                     //Natural: DISPLAY-REPORT-ACCOUNT-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------- *
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        //*  'REPORT: CP1416DC'
        //*  <00013241>
        //* 2079950055011
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getINIT_USER(),new TabSetting(36),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(108),"PAGE:",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE 001T *INIT-USER 36T 'CONSOLIDATED PAYMENT SYSTEM' 108T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *PROGRAM 030T #HEADER0-1 108T *DATU / 030T #HEADER0-2 108T *TIMX ( EM = HH:IIAP ) // 001T 'Bank Route:' #OUTPUT-DELETE-REC.#CUSTOMER-ID-A8 2X 'Bank Name: Wachovia Bank N.A.' 2X 'Bank Account:'#OUTPUT-DELETE-REC.#ACCOUNT-NUM /
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(30),pnd_Header0_1,new TabSetting(108),Global.getDATU(),NEWLINE,new 
            TabSetting(30),pnd_Header0_2,new TabSetting(108),Global.getTIMX(), new ReportEditMask ("HH:IIAP"),NEWLINE,NEWLINE,new TabSetting(1),"Bank Route:",pnd_Output_Delete_Rec_Pnd_Customer_Id_A8,new 
            ColumnSpacing(2),"Bank Name: Wachovia Bank N.A.",new ColumnSpacing(2),"Bank Account:",pnd_Output_Delete_Rec_Pnd_Account_Num,NEWLINE);
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2 LINES
        //* (N13)
        //* (N10)
        //* (8.2)
        //* (A10)  MM/DD/YYYY
        //* (A8)
        //* (A2)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),"Account",new TabSetting(28),"Check",new TabSetting(43),"Check",new TabSetting(52),"Check",new      //Natural: WRITE ( 1 ) 013T 'Account' 028T 'Check' 043T 'Check' 052T 'Check' 063T 'PPCN' 071T 'Pay' 077T 'Payee'
            TabSetting(63),"PPCN",new TabSetting(71),"Pay",new TabSetting(77),"Payee");
        if (Global.isEscape()) return;
        //* (N13)
        //* (N10)
        //* (8.2)
        //* (A10)  MM/DD/YYYY
        //* (A8)
        //* (A2)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),"Number",new TabSetting(27),"Number",new TabSetting(42),"Amount",new TabSetting(52),"Date",new      //Natural: WRITE ( 1 ) 013T 'Number' 027T 'Number' 042T 'Amount' 052T 'Date' 062T 'Number' 071T 'Cde' 077T 'NAME'
            TabSetting(62),"Number",new TabSetting(71),"Cde",new TabSetting(77),"NAME");
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-HEADER
    }
    private void sub_Display_Report_Detail() throws Exception                                                                                                             //Natural: DISPLAY-REPORT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------ *
        pnd_Rpt_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #RPT-CNT
        //* (N05)
        //* (N13)
        //* N8.2
        //* (A10) MMDDYYYY
        //* (A8)
        //* (A2)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Rpt_Cnt, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(10),pnd_Output_Delete_Rec_Pnd_Account_Num,new  //Natural: WRITE ( 1 ) 003T #RPT-CNT ( EM = ZZ,ZZ9 ) 010T #OUTPUT-DELETE-REC.#ACCOUNT-NUM 025T #OUTPUT-DELETE-REC.#BEGINNING-CHECK-NUM-N10 036T #OUTPUT-DELETE-REC.#CHECK-AMT-N10 ( EM = ZZ,ZZZ,ZZZ.99 ) 050T #DATE-MMDDYYYY 061T PYMNT.CNTRCT-PPCN-A8 071T PYMNT.CNTRCT-PAYEE-CDE 077T #OUTPUT-DELETE-REC.#PAYEE-NAME
            TabSetting(25),pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10,new TabSetting(36),pnd_Output_Delete_Rec_Pnd_Check_Amt_N10, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Date_Mmddyyyy,new TabSetting(61),pymnt_Cntrct_Ppcn_A8,new TabSetting(71),pymnt_Cntrct_Payee_Cde,new 
            TabSetting(77),pnd_Output_Delete_Rec_Pnd_Payee_Name);
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-DETAIL
    }
    private void sub_Display_Report_Account_Trailer() throws Exception                                                                                                    //Natural: DISPLAY-REPORT-ACCOUNT-TRAILER
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        //*  N8.2
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(12),"Account Total Payments:",pnd_Acct_Total, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"));                    //Natural: WRITE ( 1 ) 012T 'Account Total Payments:' #ACCT-TOTAL ( EM = ZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-ACCOUNT-TRAILER
    }
    private void sub_Control_Totals() throws Exception                                                                                                                    //Natural: CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------- *
        //*  THE WRITE STATEMENT BELOW WAS COMMENTED OUT FOR FIN-848
        //* *WRITE
        //* *  'TOTAL SD READ COUNT------>:' #SD-READ (EM=Z,ZZZ,ZZ9) /
        //* *  'TOTAL AP PAYMENTS READ--->:' #CNT-READ (EM=Z,ZZZ,ZZ9) /
        //* *  'TOTAL CANCELS------------>:' #CNT-CANCEL (EM=Z,ZZZ,ZZ9) /
        //* *  'TOTAL CANCELS(NO REDRAW)->:' #CNT-CAN-NO-REDR (EM=Z,ZZZ,ZZ9) /
        //* *  'TOTAL CANCELS & CNRS----->:' #CNT-C-AND-CN (EM=Z,ZZZ,ZZ9) /
        //*  THE WRITE STATEMENT BELOW WAS ADDED FOR FIN-848
        getReports().write(0, " ",NEWLINE,"CANCEL STATUS READ------->:",pnd_Cancels_Read, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"  BYPASSED - ORIGIN CD--->:",pnd_Bypassed_Origin_Cd,  //Natural: WRITE ' ' / 'CANCEL STATUS READ------->:' #CANCELS-READ ( EM = Z,ZZZ,ZZ9 ) / '  BYPASSED - ORIGIN CD--->:' #BYPASSED-ORIGIN-CD ( EM = Z,ZZZ,ZZ9 ) / '  BYPASSED - ACCT NBR---->:' #BYPASSED-ACCT-NBR ( EM = Z,ZZZ,ZZ9 ) / '  BYPASSED - STATS CD---->:' #BYPASSED-STATS-CD ( EM = Z,ZZZ,ZZ9 ) / '  BYPASSED - ACTIVITY CD->:' #BYPASSED-ACTIVITY-CD ( EM = Z,ZZZ,ZZ9 ) / 'CANCELS PROCESSED-------->:' #CANCELS-PROCESSED ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"  BYPASSED - ACCT NBR---->:",pnd_Bypassed_Acct_Nbr, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"  BYPASSED - STATS CD---->:",pnd_Bypassed_Stats_Cd, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"  BYPASSED - ACTIVITY CD->:",pnd_Bypassed_Activity_Cd, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,"CANCELS PROCESSED-------->:",pnd_Cancels_Processed, 
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  FIN-848
        pnd_Control_Totals_Printed.setValue(true);                                                                                                                        //Natural: ASSIGN #CONTROL-TOTALS-PRINTED = TRUE
    }
    //*  FE201504 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"***",Global.getPROGRAM(),"  ERROR:",Global.getERROR_NR(),"LINE:",Global.getERROR_LINE(),NEWLINE,"***",new                  //Natural: WRITE // '***'*PROGRAM '  ERROR:' *ERROR-NR 'LINE:' *ERROR-LINE / '***' 10T 'ORIGIN    :' PYMNT.CNTRCT-ORGN-CDE / '***' 10T 'PPCN      :' PYMNT.CNTRCT-PPCN-NBR / '***' 10T 'SEQUENCE# :' PYMNT.PYMNT-PRCSS-SEQ-NUM / '***' 10T 'PYMNT AMT :' PYMNT.PYMNT-CHECK-AMT / '***  LAST RECORD READ:' #CNT-READ /
            TabSetting(10),"ORIGIN    :",pymnt_Cntrct_Orgn_Cde,NEWLINE,"***",new TabSetting(10),"PPCN      :",pymnt_Cntrct_Ppcn_Nbr,NEWLINE,"***",new TabSetting(10),"SEQUENCE# :",pymnt_Pymnt_Prcss_Seq_Num,NEWLINE,"***",new 
            TabSetting(10),"PYMNT AMT :",pymnt_Pymnt_Check_Amt,NEWLINE,"***  LAST RECORD READ:",pnd_Cnt_Read,NEWLINE);
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=55 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
