/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:10 PM
**        * FROM NATURAL PROGRAM : Fcpp810
************************************************************
**        * FILE NAME            : Fcpp810.java
**        * CLASS NAME           : Fcpp810
**        * INSTANCE NAME        : Fcpp810
************************************************************
************************************************************************
*
* PROGRAM   : FCPP810
* SYSTEM    : CPS
* TITLE     : ERROR REPORTS
* GENERATED :
* FUNCTION  : GENERATES:
*           : 1 - LEDGER ERROR REPORT
*           : 2 - MISCELLANEOUS ERROR REPORT
*           :
* HISTORY   :
* 08/20/97  : RITA SALGADO
*           : RE-STOWED FOR NEW FCPA800 (CHANGED FOR CHECK RE-START)
* 08/01/01  : A.REYHANIAN READ EXPANDED PAYMENT (INCLUDING TAX WITHHELD)
* 05/05/03  : R. CARREON  RESTOW FOR NEW FCPA800
* 04/28/08  : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp810 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa802 pdaFcpa802;
    private LdaFcpl801a ldaFcpl801a;
    private PdaFcpacntl pdaFcpacntl;
    private LdaFcpl810 ldaFcpl810;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Index;
    private DbsField pnd_T_Index;
    private DbsField pnd_Ph_Name;
    private DbsField pnd_Gross_Amt;
    private DbsField pnd_Ded_Amt;
    private DbsField pnd_Tax_Amt;
    private DbsField pnd_Net_Amt;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Gtn_Err_Count;
    private DbsField pnd_Ledger_Err_Count;
    private DbsField pnd_Gtn_Err_Gross;
    private DbsField pnd_Gtn_Err_Ded;
    private DbsField pnd_Gtn_Err_Tax;
    private DbsField pnd_Gtn_Err_Net;
    private DbsField pnd_Annty_Ins_Type;
    private DbsField pnd_Annty_Type;
    private DbsField pnd_Insurance_Option;
    private DbsField pnd_Life_Contingency;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa802 = new PdaFcpa802(localVariables);
        ldaFcpl801a = new LdaFcpl801a();
        registerRecord(ldaFcpl801a);
        pdaFcpacntl = new PdaFcpacntl(localVariables);
        ldaFcpl810 = new LdaFcpl810();
        registerRecord(ldaFcpl810);

        // Local Variables
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_T_Index = localVariables.newFieldInRecord("pnd_T_Index", "#T-INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Ph_Name = localVariables.newFieldInRecord("pnd_Ph_Name", "#PH-NAME", FieldType.STRING, 38);
        pnd_Gross_Amt = localVariables.newFieldInRecord("pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ded_Amt = localVariables.newFieldInRecord("pnd_Ded_Amt", "#DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Net_Amt = localVariables.newFieldInRecord("pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 25);
        pnd_Gtn_Err_Count = localVariables.newFieldInRecord("pnd_Gtn_Err_Count", "#GTN-ERR-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Ledger_Err_Count = localVariables.newFieldInRecord("pnd_Ledger_Err_Count", "#LEDGER-ERR-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Gtn_Err_Gross = localVariables.newFieldInRecord("pnd_Gtn_Err_Gross", "#GTN-ERR-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Gtn_Err_Ded = localVariables.newFieldInRecord("pnd_Gtn_Err_Ded", "#GTN-ERR-DED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Gtn_Err_Tax = localVariables.newFieldInRecord("pnd_Gtn_Err_Tax", "#GTN-ERR-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Gtn_Err_Net = localVariables.newFieldInRecord("pnd_Gtn_Err_Net", "#GTN-ERR-NET", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Annty_Ins_Type = localVariables.newFieldInRecord("pnd_Annty_Ins_Type", "#ANNTY-INS-TYPE", FieldType.STRING, 4);
        pnd_Annty_Type = localVariables.newFieldInRecord("pnd_Annty_Type", "#ANNTY-TYPE", FieldType.STRING, 4);
        pnd_Insurance_Option = localVariables.newFieldInRecord("pnd_Insurance_Option", "#INSURANCE-OPTION", FieldType.STRING, 4);
        pnd_Life_Contingency = localVariables.newFieldInRecord("pnd_Life_Contingency", "#LIFE-CONTINGENCY", FieldType.STRING, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl801a.initializeValues();
        ldaFcpl810.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp810() throws Exception
    {
        super("Fcpp810");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP810", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 55;//Natural: FORMAT ( 02 ) LS = 132 PS = 55
        //*                                                                                                                                                               //Natural: ON ERROR
        getWorkFiles().read(1, pdaFcpacntl.getCntl());                                                                                                                    //Natural: READ WORK FILE 1 ONCE CNTL
        //*  ABRM
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 PYMNT-ADDR-INFO WF-ERROR-MSG WF-PYMNT-TAX-REC INV-INFO ( * )
        while (condition(getWorkFiles().read(2, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), ldaFcpl801a.getWf_Error_Msg(), pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            //*    PYMNT-ADDR-INFO WF-ERROR-MSG                  INV-INFO(*)
            if (condition(!(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Ret_Code().equals("0003"))))                                                                              //Natural: ACCEPT IF GTN-RET-CODE = '0003'
            {
                continue;
            }
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 01 ) TITLE LEFT *PROGRAM '- 1' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 52T 'GROSS-TO-NET ERROR REPORT' / 56T CNTL.CNTL-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 02 ) TITLE LEFT *PROGRAM '- 2' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 55T 'LEDGER ERROR REPORT' / 56T CNTL.CNTL-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ph_Name.setValue(DbsUtil.compress(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name()));                      //Natural: COMPRESS PH-FIRST-NAME PH-LAST-NAME INTO #PH-NAME
            pnd_Gross_Amt.compute(new ComputeParameters(false, pnd_Gross_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #GROSS-AMT := INV-ACCT-SETTL-AMT ( 1:INV-ACCT-COUNT ) + 0
            pnd_Ded_Amt.compute(new ComputeParameters(false, pnd_Ded_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").add(getZero()));                 //Natural: ASSIGN #DED-AMT := PYMNT-DED-AMT ( * ) + 0
            pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #TAX-AMT := INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT ) + 0
            pnd_Tax_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));                  //Natural: ADD INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #TAX-AMT
            pnd_Tax_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));                  //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #TAX-AMT
            pnd_Net_Amt.compute(new ComputeParameters(false, pnd_Net_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #NET-AMT := INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT ) + 0
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 7
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(7)); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(pnd_Index).equals(0)))                                                              //Natural: IF GTN-RPT-CODE ( #INDEX ) = 00
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  CODE ONLY FOR REPORTS
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(pnd_Index).greaterOrEqual(1) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(pnd_Index).lessOrEqual(13))) //Natural: IF GTN-RPT-CODE ( #INDEX ) = 01 THRU 13
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                pnd_T_Index.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(pnd_Index));                                                                 //Natural: MOVE GTN-RPT-CODE ( #INDEX ) TO #T-INDEX
                pnd_Error_Msg.setValue(ldaFcpl810.getPnd_Gtn_Rpt_Msg_Pnd_Gtn_Rpt_Desc().getValue(pnd_T_Index));                                                           //Natural: MOVE #GTN-RPT-DESC ( #T-INDEX ) TO #ERROR-MSG
                if (condition(pnd_T_Index.equals(16)))                                                                                                                    //Natural: IF #T-INDEX = 16
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-LEDGER-ERROR-REPORT
                    sub_Print_Ledger_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-ERROR-REPORT
                    sub_Print_Error_Report();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ERROR-REPORT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-LEDGER-ERROR-REPORT
        //* ***********************************************************************
        if (condition(pnd_Gtn_Err_Count.greater(getZero())))                                                                                                              //Natural: IF #GTN-ERR-COUNT > 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new ReportTAsterisk(pnd_Gross_Amt),pnd_Gtn_Err_Gross, new ReportEditMask ("ZZZZ,ZZZ.99-"),new                   //Natural: WRITE ( 01 ) /// T*#GROSS-AMT #GTN-ERR-GROSS T*#DED-AMT #GTN-ERR-DED T*#TAX-AMT #GTN-ERR-TAX T*#NET-AMT #GTN-ERR-NET 10X 'COUNT:' #GTN-ERR-COUNT
                ReportTAsterisk(pnd_Ded_Amt),pnd_Gtn_Err_Ded, new ReportEditMask ("ZZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Tax_Amt),pnd_Gtn_Err_Tax, new 
                ReportEditMask ("ZZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Net_Amt),pnd_Gtn_Err_Net, new ReportEditMask ("ZZZZ,ZZZ.99-"),new ColumnSpacing(10),"COUNT:",pnd_Gtn_Err_Count, 
                new ReportEditMask ("ZZ,ZZ9"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 01 ) //// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***         No gross-to-net errors found                  ***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***         No gross-to-net errors found                  ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ledger_Err_Count.equals(getZero())))                                                                                                            //Natural: IF #LEDGER-ERR-COUNT = 0
        {
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 02 ) //// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***               No ledger errors found                  ***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***               No ledger errors found                  ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Error_Report() throws Exception                                                                                                                //Natural: PRINT-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, "/Combined #",                                                                                                                            //Natural: DISPLAY ( 01 ) '/Combined #' CNTRCT-CMBN-NBR '/Contract#' CNTRCT-PPCN-NBR 'Gross/Amount' #GROSS-AMT '/Deductions' #DED-AMT '/Taxes' #TAX-AMT 'Net/Amount' #NET-AMT 'Annuitant Name/Payee Name' #PH-NAME ( AL = 25 ) 'Error/Message' #ERROR-MSG
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Gross/Amount",
        		pnd_Gross_Amt,"/Deductions",
        		pnd_Ded_Amt,"/Taxes",
        		pnd_Tax_Amt,"Net/Amount",
        		pnd_Net_Amt,"Annuitant Name/Payee Name",
        		pnd_Ph_Name, new AlphanumericLength (25),"Error/Message",
        		pnd_Error_Msg);
        if (Global.isEscape()) return;
        pnd_Gtn_Err_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #GTN-ERR-COUNT
        pnd_Gtn_Err_Gross.nadd(pnd_Gross_Amt);                                                                                                                            //Natural: ADD #GROSS-AMT TO #GTN-ERR-GROSS
        pnd_Gtn_Err_Ded.nadd(pnd_Ded_Amt);                                                                                                                                //Natural: ADD #DED-AMT TO #GTN-ERR-DED
        pnd_Gtn_Err_Tax.nadd(pnd_Tax_Amt);                                                                                                                                //Natural: ADD #TAX-AMT TO #GTN-ERR-TAX
        pnd_Gtn_Err_Net.nadd(pnd_Net_Amt);                                                                                                                                //Natural: ADD #NET-AMT TO #GTN-ERR-NET
    }
    private void sub_Print_Ledger_Error_Report() throws Exception                                                                                                         //Natural: PRINT-LEDGER-ERROR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  TRANSLATE LEDGER CODES
        //* ***********************************************************************
        //*  PROGRAM   : FCPP807
        //*  SYSTEM    : CPS
        //*  TITLE     : TRANSLATE LEDGER CODES
        //*  GENERATED :
        //*  FUNCTION  : USED BY:
        //*            :     FCPP810
        //*            :
        //*  HISTORY   :
        //* ***********************************************************************
        pnd_Annty_Ins_Type.reset();                                                                                                                                       //Natural: RESET #ANNTY-INS-TYPE #ANNTY-TYPE #INSURANCE-OPTION #LIFE-CONTINGENCY
        pnd_Annty_Type.reset();
        pnd_Insurance_Option.reset();
        pnd_Life_Contingency.reset();
        short decideConditionsMet754 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-INS-TYPE;//Natural: VALUE 'A'
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("A"))))
        {
            decideConditionsMet754++;
            pnd_Annty_Ins_Type.setValue("Ann ");                                                                                                                          //Natural: MOVE 'Ann ' TO #ANNTY-INS-TYPE
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("I"))))
        {
            decideConditionsMet754++;
            pnd_Annty_Ins_Type.setValue("Ins ");                                                                                                                          //Natural: MOVE 'Ins ' TO #ANNTY-INS-TYPE
        }                                                                                                                                                                 //Natural: VALUE 'G'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("G"))))
        {
            decideConditionsMet754++;
            pnd_Annty_Ins_Type.setValue("Grp ");                                                                                                                          //Natural: MOVE 'Grp ' TO #ANNTY-INS-TYPE
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet764 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-TYPE-CDE;//Natural: VALUE 'M'
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M"))))
        {
            decideConditionsMet764++;
            pnd_Annty_Type.setValue("Mat ");                                                                                                                              //Natural: MOVE 'Mat ' TO #ANNTY-TYPE
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("D"))))
        {
            decideConditionsMet764++;
            pnd_Annty_Type.setValue("Dth ");                                                                                                                              //Natural: MOVE 'Dth ' TO #ANNTY-TYPE
        }                                                                                                                                                                 //Natural: VALUE 'P'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("P"))))
        {
            decideConditionsMet764++;
            pnd_Annty_Type.setValue("Par ");                                                                                                                              //Natural: MOVE 'Par ' TO #ANNTY-TYPE
        }                                                                                                                                                                 //Natural: VALUE 'N'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("N"))))
        {
            decideConditionsMet764++;
            pnd_Annty_Type.setValue("NPar");                                                                                                                              //Natural: MOVE 'NPar' TO #ANNTY-TYPE
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet776 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-INSURANCE-OPTION;//Natural: VALUE 'G'
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("G"))))
        {
            decideConditionsMet776++;
            pnd_Insurance_Option.setValue("Grp ");                                                                                                                        //Natural: MOVE 'Grp ' TO #INSURANCE-OPTION
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("C"))))
        {
            decideConditionsMet776++;
            pnd_Insurance_Option.setValue("Coll");                                                                                                                        //Natural: MOVE 'Coll' TO #INSURANCE-OPTION
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("I"))))
        {
            decideConditionsMet776++;
            pnd_Insurance_Option.setValue("Ind ");                                                                                                                        //Natural: MOVE 'Ind ' TO #INSURANCE-OPTION
        }                                                                                                                                                                 //Natural: VALUE 'P'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("P"))))
        {
            decideConditionsMet776++;
            pnd_Insurance_Option.setValue("PA  ");                                                                                                                        //Natural: MOVE 'PA  ' TO #INSURANCE-OPTION
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet788 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-LIFE-CONTINGENCY;//Natural: VALUE 'Y'
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency().equals("Y"))))
        {
            decideConditionsMet788++;
            pnd_Life_Contingency.setValue("Yes ");                                                                                                                        //Natural: MOVE 'Yes ' TO #LIFE-CONTINGENCY
        }                                                                                                                                                                 //Natural: VALUE 'N'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency().equals("N"))))
        {
            decideConditionsMet788++;
            pnd_Life_Contingency.setValue("No  ");                                                                                                                        //Natural: MOVE 'No  ' TO #LIFE-CONTINGENCY
        }                                                                                                                                                                 //Natural: NONE VALUES
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getReports().display(2, "Combined #/Contract#",                                                                                                                   //Natural: DISPLAY ( 02 ) 'Combined #/Contract#' CNTRCT-CMBN-NBR 'Ann/Type' #ANNTY-INS-TYPE 'Cntr/Type' #ANNTY-TYPE 'Life/Cont' #LIFE-CONTINGENCY 'Prod/Line' #INSURANCE-OPTION 'Annuitant/Payee Name' #PH-NAME 'Check Amt/Contr Net Amt' PYMNT-CHECK-AMT ( EM = ZZZZ,ZZZ.99- ) '/Error Message' WF-ERROR-MSG ( AL = 40 )
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Ann/Type",
        		pnd_Annty_Ins_Type,"Cntr/Type",
        		pnd_Annty_Type,"Life/Cont",
        		pnd_Life_Contingency,"Prod/Line",
        		pnd_Insurance_Option,"Annuitant/Payee Name",
        		pnd_Ph_Name,"Check Amt/Contr Net Amt",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("ZZZZ,ZZZ.99-"),"/Error Message",
        		ldaFcpl801a.getWf_Error_Msg(), new AlphanumericLength (40));
        if (Global.isEscape()) return;
        getReports().write(2, new ReportTAsterisk(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr()),pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),new                //Natural: WRITE ( 02 ) T*CNTRCT-CMBN-NBR CNTRCT-PPCN-NBR T*#PH-NAME PYMNT-NME ( 1 ) T*PYMNT-CHECK-AMT #NET-AMT
            ReportTAsterisk(pnd_Ph_Name),pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1),new ReportTAsterisk(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt()),pnd_Net_Amt, 
            new ReportEditMask ("ZZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        pnd_Ledger_Err_Count.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #LEDGER-ERR-COUNT
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),"- 1",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(52),"GROSS-TO-NET ERROR REPORT",NEWLINE,new 
            TabSetting(56),pdaFcpacntl.getCntl_Cntl_Check_Dte(), new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getPROGRAM(),"- 2",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(55),"LEDGER ERROR REPORT",NEWLINE,new 
            TabSetting(56),pdaFcpacntl.getCntl_Cntl_Check_Dte(), new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "/Combined #",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Gross/Amount",
        		pnd_Gross_Amt,"/Deductions",
        		pnd_Ded_Amt,"/Taxes",
        		pnd_Tax_Amt,"Net/Amount",
        		pnd_Net_Amt,"Annuitant Name/Payee Name",
        		pnd_Ph_Name, new AlphanumericLength (25),"Error/Message",
        		pnd_Error_Msg);
        getReports().setDisplayColumns(2, "Combined #/Contract#",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Ann/Type",
        		pnd_Annty_Ins_Type,"Cntr/Type",
        		pnd_Annty_Type,"Life/Cont",
        		pnd_Life_Contingency,"Prod/Line",
        		pnd_Insurance_Option,"Annuitant/Payee Name",
        		pnd_Ph_Name,"Check Amt/Contr Net Amt",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("ZZZZ,ZZZ.99-"),"/Error Message",
        		ldaFcpl801a.getWf_Error_Msg(), new AlphanumericLength (40));
    }
}
