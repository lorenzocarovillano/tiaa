/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:42 PM
**        * FROM NATURAL PROGRAM : Fcpp193
************************************************************
**        * FILE NAME            : Fcpp193.java
**        * CLASS NAME           : Fcpp193
**        * INSTANCE NAME        : Fcpp193
************************************************************
***********************************************************************
** PROGRAM:     FCPP193                                              **
** SYSTEM:      FULL CONSOLIDATED PAYMENT SYSTEM                     **
** DATE:        07-12-94                                             **
** AUTHOR:      ALTHEA A. YOUNG                                      **
** DESCRIPTION: THIS PROGRAM PRINTS THE EXTRACT AND REFORMAT REPORTS **
**              FOR SINGLE SUM AND IA DEATH BASED ON THE ORIGIN CODES**
**                                                                   **
** JOB STREAM:  CPS DAYTIME STREAM                                   **
**                                                                   **
** 07/22/96 LZ  - INCLUDED MDO IN THE REPORT                         **
** 05/18/98 AAY - BYPASS INTERNAL ROLLOVERS (CLASSIC, ROTH).         **
* 4/2017     : JJG - PIN EXPANSION RESTOW                            **
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp193 extends BLNatBase
{
    // Data Areas
    private LdaFcpl190a ldaFcpl190a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Rpt_Vars;

    private DbsGroup pnd_Rpt_Vars__R_Field_1;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Pymnt_Rqsts;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Gross_Amt;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Dvdnd_Amt;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Dpi_Amt;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Dci_Amt;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Process_Chg;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Fed_Ded;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_State_Ded;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Local_Ded;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Vlntry_Ded;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Net_Amt;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Ivc_Pymnts;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Ivc_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Pymnts;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Num_Checks;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Chk_Gross_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Chk_Net_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Num_Efts;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Eft_Gross_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Eft_Net_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Num_Globals;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Glob_Gross_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Glob_Net_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Num_Other;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Other_Gross_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Other_Net_Dollars;
    private DbsField pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus;
    private DbsField pnd_Mdo_Vars;

    private DbsGroup pnd_Mdo_Vars__R_Field_2;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Pymnt_Rqsts;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Gross_Amt;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Dvdnd_Amt;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Dpi_Amt;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Dci_Amt;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Process_Chg;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Fed_Ded;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_State_Ded;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Local_Ded;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Vlntry_Ded;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Net_Amt;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Ivc_Pymnts;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Ivc_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Ivc_Incl_Pymnts;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Ivc_Incl_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Num_Checks;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Chk_Gross_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Chk_Net_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Num_Efts;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Eft_Gross_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Eft_Net_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Num_Globals;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Glob_Gross_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Glob_Net_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Num_Other;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Other_Gross_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Other_Net_Dollars;
    private DbsField pnd_Mdo_Vars_Pnd_Mdo_Gross_Plus;
    private DbsField pnd_Total_Vars;

    private DbsGroup pnd_Total_Vars__R_Field_3;
    private DbsField pnd_Total_Vars_Pnd_Tot_Pymnt_Rqsts;
    private DbsField pnd_Total_Vars_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Total_Vars_Pnd_Tot_Dvdnd_Amt;
    private DbsField pnd_Total_Vars_Pnd_Tot_Dpi_Amt;
    private DbsField pnd_Total_Vars_Pnd_Tot_Dci_Amt;
    private DbsField pnd_Total_Vars_Pnd_Tot_Process_Chg;
    private DbsField pnd_Total_Vars_Pnd_Tot_Fed_Ded;
    private DbsField pnd_Total_Vars_Pnd_Tot_State_Ded;
    private DbsField pnd_Total_Vars_Pnd_Tot_Local_Ded;
    private DbsField pnd_Total_Vars_Pnd_Tot_Vlntry_Ded;
    private DbsField pnd_Total_Vars_Pnd_Tot_Net_Amt;
    private DbsField pnd_Total_Vars_Pnd_Tot_Ivc_Pymnts;
    private DbsField pnd_Total_Vars_Pnd_Tot_Ivc_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Ivc_Incl_Pymnts;
    private DbsField pnd_Total_Vars_Pnd_Tot_Ivc_Incl_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Num_Checks;
    private DbsField pnd_Total_Vars_Pnd_Tot_Chk_Gross_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Chk_Net_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Num_Efts;
    private DbsField pnd_Total_Vars_Pnd_Tot_Eft_Gross_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Eft_Net_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Num_Globals;
    private DbsField pnd_Total_Vars_Pnd_Tot_Glob_Gross_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Glob_Net_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Num_Other;
    private DbsField pnd_Total_Vars_Pnd_Tot_Other_Gross_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Other_Net_Dollars;
    private DbsField pnd_Total_Vars_Pnd_Tot_Gross_Plus;

    private DbsGroup pnd_Inv_Amts;
    private DbsField pnd_Inv_Amts_Pnd_Inv_Gross_Amt;
    private DbsField pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt;
    private DbsField pnd_Inv_Amts_Pnd_Inv_Dpi_Amt;
    private DbsField pnd_Inv_Amts_Pnd_Inv_Dci_Amt;
    private DbsField pnd_Inv_Amts_Pnd_Inv_Net_Amt;
    private DbsField pnd_Inv_Amts_Pnd_Inv_Ivc_Pymnts;
    private DbsField pnd_Inv_Amts_Pnd_Inv_Ivc_Incl_Pymnts;
    private DbsField pnd_I;
    private DbsField pnd_Report_Title;
    private DbsField pnd_Orgn_Title;
    private DbsField pnd_Old_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Old_Cntrct_Orgn_Cde;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld;
    private DbsField readWork01Cntrct_Orgn_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Rpt_Vars = localVariables.newFieldArrayInRecord("pnd_Rpt_Vars", "#RPT-VARS", FieldType.STRING, 1, new DbsArrayController(1, 260));

        pnd_Rpt_Vars__R_Field_1 = localVariables.newGroupInRecord("pnd_Rpt_Vars__R_Field_1", "REDEFINE", pnd_Rpt_Vars);
        pnd_Rpt_Vars_Pnd_Rpt_Pymnt_Rqsts = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Pymnt_Rqsts", "#RPT-PYMNT-RQSTS", FieldType.NUMERIC, 
            5);
        pnd_Rpt_Vars_Pnd_Rpt_Gross_Amt = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Gross_Amt", "#RPT-GROSS-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Dvdnd_Amt = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Dvdnd_Amt", "#RPT-DVDND-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Dpi_Amt = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Dpi_Amt", "#RPT-DPI-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Rpt_Vars_Pnd_Rpt_Dci_Amt = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Dci_Amt", "#RPT-DCI-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Rpt_Vars_Pnd_Rpt_Process_Chg = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Process_Chg", "#RPT-PROCESS-CHG", FieldType.NUMERIC, 
            11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Fed_Ded = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Fed_Ded", "#RPT-FED-DED", FieldType.NUMERIC, 11, 
            2);
        pnd_Rpt_Vars_Pnd_Rpt_State_Ded = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_State_Ded", "#RPT-STATE-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Local_Ded = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Local_Ded", "#RPT-LOCAL-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Vlntry_Ded = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Vlntry_Ded", "#RPT-VLNTRY-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Net_Amt = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Net_Amt", "#RPT-NET-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Rpt_Vars_Pnd_Rpt_Ivc_Pymnts = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Ivc_Pymnts", "#RPT-IVC-PYMNTS", FieldType.NUMERIC, 
            4);
        pnd_Rpt_Vars_Pnd_Rpt_Ivc_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Ivc_Dollars", "#RPT-IVC-DOLLARS", FieldType.NUMERIC, 
            11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Pymnts = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Pymnts", "#RPT-IVC-INCL-PYMNTS", 
            FieldType.NUMERIC, 4);
        pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Dollars", "#RPT-IVC-INCL-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Num_Checks = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Num_Checks", "#RPT-NUM-CHECKS", FieldType.NUMERIC, 
            4);
        pnd_Rpt_Vars_Pnd_Rpt_Chk_Gross_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Chk_Gross_Dollars", "#RPT-CHK-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Chk_Net_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Chk_Net_Dollars", "#RPT-CHK-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Num_Efts = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Num_Efts", "#RPT-NUM-EFTS", FieldType.NUMERIC, 4);
        pnd_Rpt_Vars_Pnd_Rpt_Eft_Gross_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Eft_Gross_Dollars", "#RPT-EFT-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Eft_Net_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Eft_Net_Dollars", "#RPT-EFT-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Num_Globals = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Num_Globals", "#RPT-NUM-GLOBALS", FieldType.NUMERIC, 
            4);
        pnd_Rpt_Vars_Pnd_Rpt_Glob_Gross_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Glob_Gross_Dollars", "#RPT-GLOB-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Glob_Net_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Glob_Net_Dollars", "#RPT-GLOB-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Num_Other = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Num_Other", "#RPT-NUM-OTHER", FieldType.NUMERIC, 
            4);
        pnd_Rpt_Vars_Pnd_Rpt_Other_Gross_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Other_Gross_Dollars", "#RPT-OTHER-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Other_Net_Dollars = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Other_Net_Dollars", "#RPT-OTHER-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus = pnd_Rpt_Vars__R_Field_1.newFieldInGroup("pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus", "#RPT-GROSS-PLUS", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdo_Vars = localVariables.newFieldArrayInRecord("pnd_Mdo_Vars", "#MDO-VARS", FieldType.STRING, 1, new DbsArrayController(1, 260));

        pnd_Mdo_Vars__R_Field_2 = localVariables.newGroupInRecord("pnd_Mdo_Vars__R_Field_2", "REDEFINE", pnd_Mdo_Vars);
        pnd_Mdo_Vars_Pnd_Mdo_Pymnt_Rqsts = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Pymnt_Rqsts", "#MDO-PYMNT-RQSTS", FieldType.NUMERIC, 
            5);
        pnd_Mdo_Vars_Pnd_Mdo_Gross_Amt = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Gross_Amt", "#MDO-GROSS-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Dvdnd_Amt = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Dvdnd_Amt", "#MDO-DVDND-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Dpi_Amt = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Dpi_Amt", "#MDO-DPI-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Mdo_Vars_Pnd_Mdo_Dci_Amt = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Dci_Amt", "#MDO-DCI-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Mdo_Vars_Pnd_Mdo_Process_Chg = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Process_Chg", "#MDO-PROCESS-CHG", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Fed_Ded = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Fed_Ded", "#MDO-FED-DED", FieldType.NUMERIC, 11, 
            2);
        pnd_Mdo_Vars_Pnd_Mdo_State_Ded = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_State_Ded", "#MDO-STATE-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Local_Ded = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Local_Ded", "#MDO-LOCAL-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Vlntry_Ded = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Vlntry_Ded", "#MDO-VLNTRY-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Net_Amt = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Net_Amt", "#MDO-NET-AMT", FieldType.NUMERIC, 11, 
            2);
        pnd_Mdo_Vars_Pnd_Mdo_Ivc_Pymnts = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Ivc_Pymnts", "#MDO-IVC-PYMNTS", FieldType.NUMERIC, 
            4);
        pnd_Mdo_Vars_Pnd_Mdo_Ivc_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Ivc_Dollars", "#MDO-IVC-DOLLARS", FieldType.NUMERIC, 
            11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Ivc_Incl_Pymnts = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Ivc_Incl_Pymnts", "#MDO-IVC-INCL-PYMNTS", 
            FieldType.NUMERIC, 4);
        pnd_Mdo_Vars_Pnd_Mdo_Ivc_Incl_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Ivc_Incl_Dollars", "#MDO-IVC-INCL-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Num_Checks = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Num_Checks", "#MDO-NUM-CHECKS", FieldType.NUMERIC, 
            4);
        pnd_Mdo_Vars_Pnd_Mdo_Chk_Gross_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Chk_Gross_Dollars", "#MDO-CHK-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Chk_Net_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Chk_Net_Dollars", "#MDO-CHK-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Num_Efts = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Num_Efts", "#MDO-NUM-EFTS", FieldType.NUMERIC, 4);
        pnd_Mdo_Vars_Pnd_Mdo_Eft_Gross_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Eft_Gross_Dollars", "#MDO-EFT-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Eft_Net_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Eft_Net_Dollars", "#MDO-EFT-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Num_Globals = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Num_Globals", "#MDO-NUM-GLOBALS", FieldType.NUMERIC, 
            4);
        pnd_Mdo_Vars_Pnd_Mdo_Glob_Gross_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Glob_Gross_Dollars", "#MDO-GLOB-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Glob_Net_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Glob_Net_Dollars", "#MDO-GLOB-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Num_Other = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Num_Other", "#MDO-NUM-OTHER", FieldType.NUMERIC, 
            4);
        pnd_Mdo_Vars_Pnd_Mdo_Other_Gross_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Other_Gross_Dollars", "#MDO-OTHER-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Other_Net_Dollars = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Other_Net_Dollars", "#MDO-OTHER-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Mdo_Vars_Pnd_Mdo_Gross_Plus = pnd_Mdo_Vars__R_Field_2.newFieldInGroup("pnd_Mdo_Vars_Pnd_Mdo_Gross_Plus", "#MDO-GROSS-PLUS", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars = localVariables.newFieldArrayInRecord("pnd_Total_Vars", "#TOTAL-VARS", FieldType.STRING, 1, new DbsArrayController(1, 260));

        pnd_Total_Vars__R_Field_3 = localVariables.newGroupInRecord("pnd_Total_Vars__R_Field_3", "REDEFINE", pnd_Total_Vars);
        pnd_Total_Vars_Pnd_Tot_Pymnt_Rqsts = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Pymnt_Rqsts", "#TOT-PYMNT-RQSTS", FieldType.NUMERIC, 
            5);
        pnd_Total_Vars_Pnd_Tot_Gross_Amt = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Dvdnd_Amt = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Dvdnd_Amt", "#TOT-DVDND-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Dpi_Amt = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Dpi_Amt", "#TOT-DPI-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Dci_Amt = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Dci_Amt", "#TOT-DCI-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Process_Chg = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Process_Chg", "#TOT-PROCESS-CHG", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Fed_Ded = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Fed_Ded", "#TOT-FED-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_State_Ded = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_State_Ded", "#TOT-STATE-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Local_Ded = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Local_Ded", "#TOT-LOCAL-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Vlntry_Ded = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Vlntry_Ded", "#TOT-VLNTRY-DED", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Net_Amt = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Net_Amt", "#TOT-NET-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Ivc_Pymnts = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Ivc_Pymnts", "#TOT-IVC-PYMNTS", FieldType.NUMERIC, 
            4);
        pnd_Total_Vars_Pnd_Tot_Ivc_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Ivc_Dollars", "#TOT-IVC-DOLLARS", FieldType.NUMERIC, 
            11, 2);
        pnd_Total_Vars_Pnd_Tot_Ivc_Incl_Pymnts = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Ivc_Incl_Pymnts", "#TOT-IVC-INCL-PYMNTS", 
            FieldType.NUMERIC, 4);
        pnd_Total_Vars_Pnd_Tot_Ivc_Incl_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Ivc_Incl_Dollars", "#TOT-IVC-INCL-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Num_Checks = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Num_Checks", "#TOT-NUM-CHECKS", FieldType.NUMERIC, 
            4);
        pnd_Total_Vars_Pnd_Tot_Chk_Gross_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Chk_Gross_Dollars", "#TOT-CHK-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Chk_Net_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Chk_Net_Dollars", "#TOT-CHK-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Num_Efts = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Num_Efts", "#TOT-NUM-EFTS", FieldType.NUMERIC, 
            4);
        pnd_Total_Vars_Pnd_Tot_Eft_Gross_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Eft_Gross_Dollars", "#TOT-EFT-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Eft_Net_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Eft_Net_Dollars", "#TOT-EFT-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Num_Globals = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Num_Globals", "#TOT-NUM-GLOBALS", FieldType.NUMERIC, 
            4);
        pnd_Total_Vars_Pnd_Tot_Glob_Gross_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Glob_Gross_Dollars", "#TOT-GLOB-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Glob_Net_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Glob_Net_Dollars", "#TOT-GLOB-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Num_Other = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Num_Other", "#TOT-NUM-OTHER", FieldType.NUMERIC, 
            4);
        pnd_Total_Vars_Pnd_Tot_Other_Gross_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Other_Gross_Dollars", "#TOT-OTHER-GROSS-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Other_Net_Dollars = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Other_Net_Dollars", "#TOT-OTHER-NET-DOLLARS", 
            FieldType.NUMERIC, 11, 2);
        pnd_Total_Vars_Pnd_Tot_Gross_Plus = pnd_Total_Vars__R_Field_3.newFieldInGroup("pnd_Total_Vars_Pnd_Tot_Gross_Plus", "#TOT-GROSS-PLUS", FieldType.NUMERIC, 
            11, 2);

        pnd_Inv_Amts = localVariables.newGroupInRecord("pnd_Inv_Amts", "#INV-AMTS");
        pnd_Inv_Amts_Pnd_Inv_Gross_Amt = pnd_Inv_Amts.newFieldInGroup("pnd_Inv_Amts_Pnd_Inv_Gross_Amt", "#INV-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt = pnd_Inv_Amts.newFieldInGroup("pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt", "#INV-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Inv_Amts_Pnd_Inv_Dpi_Amt = pnd_Inv_Amts.newFieldInGroup("pnd_Inv_Amts_Pnd_Inv_Dpi_Amt", "#INV-DPI-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Inv_Amts_Pnd_Inv_Dci_Amt = pnd_Inv_Amts.newFieldInGroup("pnd_Inv_Amts_Pnd_Inv_Dci_Amt", "#INV-DCI-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Inv_Amts_Pnd_Inv_Net_Amt = pnd_Inv_Amts.newFieldInGroup("pnd_Inv_Amts_Pnd_Inv_Net_Amt", "#INV-NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Inv_Amts_Pnd_Inv_Ivc_Pymnts = pnd_Inv_Amts.newFieldInGroup("pnd_Inv_Amts_Pnd_Inv_Ivc_Pymnts", "#INV-IVC-PYMNTS", FieldType.PACKED_DECIMAL, 
            4);
        pnd_Inv_Amts_Pnd_Inv_Ivc_Incl_Pymnts = pnd_Inv_Amts.newFieldInGroup("pnd_Inv_Amts_Pnd_Inv_Ivc_Incl_Pymnts", "#INV-IVC-INCL-PYMNTS", FieldType.PACKED_DECIMAL, 
            4);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 2);
        pnd_Report_Title = localVariables.newFieldInRecord("pnd_Report_Title", "#REPORT-TITLE", FieldType.STRING, 32);
        pnd_Orgn_Title = localVariables.newFieldInRecord("pnd_Orgn_Title", "#ORGN-TITLE", FieldType.STRING, 10);
        pnd_Old_Cntrct_Cancel_Rdrw_Actvty_Cde = localVariables.newFieldInRecord("pnd_Old_Cntrct_Cancel_Rdrw_Actvty_Cde", "#OLD-CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Old_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Old_Cntrct_Orgn_Cde", "#OLD-CNTRCT-ORGN-CDE", FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cntrct_Cancel_Rdrw_Actvty_Cde_OLD", "Cntrct_Cancel_Rdrw_Actvty_Cde_OLD", 
            FieldType.STRING, 2);
        readWork01Cntrct_Orgn_CdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Cntrct_Orgn_Cde_OLD", "Cntrct_Orgn_Cde_OLD", FieldType.STRING, 
            2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_I.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp193() throws Exception
    {
        super("Fcpp193");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        getReports().definePrinter(2, "'CMPRT01'");                                                                                                                       //Natural: DEFINE PRINTER ( 1 ) OUTPUT 'CMPRT01'
        pnd_Report_Title.setValue("CPS EXTRACT AND REFORMAT REPORT");                                                                                                     //Natural: FORMAT LS = 133 PS = 60;//Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: ASSIGN #REPORT-TITLE := 'CPS EXTRACT AND REFORMAT REPORT'
        getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),pnd_Report_Title,new TabSetting(122),Global.getDATX(),  //Natural: WRITE ( 1 ) NOTITLE *INIT-USER '-' *PROGRAM 53T #REPORT-TITLE 122T *DATX ( EM = LLL' 'DD','YY ) / 52T 'GROSS-TO-NET / PAYMENT TYPE TOTALS' 122T *TIMX ( EM = HH':'II' 'AP ) ///
            new ReportEditMask ("LLL' 'DD','YY"),NEWLINE,new TabSetting(52),"GROSS-TO-NET / PAYMENT TYPE TOTALS",new TabSetting(122),Global.getTIMX(), new 
            ReportEditMask ("HH':'II' 'AP"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        pnd_Rpt_Vars.getValue("*").moveAll("0");                                                                                                                          //Natural: MOVE ALL '0' TO #RPT-VARS ( * )
        pnd_Mdo_Vars.getValue("*").moveAll("0");                                                                                                                          //Natural: MOVE ALL '0' TO #MDO-VARS ( * )
        pnd_Total_Vars.getValue("*").moveAll("0");                                                                                                                        //Natural: MOVE ALL '0' TO #TOTAL-VARS ( * )
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK 3 #RPT-EXT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(3, ldaFcpl190a.getPnd_Rpt_Ext())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  05-18-98
            if (condition(!(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().notEquals(8))))                                                                           //Natural: ACCEPT IF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND NE 8
            {
                continue;
            }
            //*  SINGLE SUM & MDO
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS'
            {
                //*  MDO
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31")))                    //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = '30' OR = '31'
                {
                                                                                                                                                                          //Natural: PERFORM CALC-MDO-VARS
                    sub_Calc_Mdo_Vars();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  SINGLE SUM
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM CALC-RPT-VARS
                    sub_Calc_Rpt_Vars();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  DC/MS/IA/DS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM CALC-RPT-VARS
                sub_Calc_Rpt_Vars();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde());                                              //Natural: AT BREAK OF #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: AT BREAK OF #RPT-EXT.CNTRCT-ORGN-CDE;//Natural: END-WORK
            readWork01Cntrct_Orgn_CdeOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-RPT-VARS
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-MDO-VARS
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-GRAND-TOTALS
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-ORGN-TITLE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REPORT
        //* *************************************
    }
    private void sub_Calc_Rpt_Vars() throws Exception                                                                                                                     //Natural: CALC-RPT-VARS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Inv_Amts.reset();                                                                                                                                             //Natural: RESET #INV-AMTS
        pnd_Inv_Amts_Pnd_Inv_Gross_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                     //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) TO #INV-GROSS-AMT
        pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                     //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( 1:C-INV-ACCT ) TO #INV-DVDND-AMT
        pnd_Inv_Amts_Pnd_Inv_Dpi_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                         //Natural: ADD #RPT-EXT.INV-ACCT-DPI-AMT ( 1:C-INV-ACCT ) TO #INV-DPI-AMT
        pnd_Inv_Amts_Pnd_Inv_Dci_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dci_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                         //Natural: ADD #RPT-EXT.INV-ACCT-DCI-AMT ( 1:C-INV-ACCT ) TO #INV-DCI-AMT
        pnd_Inv_Amts_Pnd_Inv_Net_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                   //Natural: ADD #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( 1:C-INV-ACCT ) TO #INV-NET-AMT
        pnd_Rpt_Vars_Pnd_Rpt_Pymnt_Rqsts.nadd(1);                                                                                                                         //Natural: ADD 1 TO #RPT-PYMNT-RQSTS
        pnd_Rpt_Vars_Pnd_Rpt_Gross_Amt.nadd(pnd_Inv_Amts_Pnd_Inv_Gross_Amt);                                                                                              //Natural: ADD #INV-GROSS-AMT TO #RPT-GROSS-AMT
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("MS") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA"))) //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' OR = 'MS' OR = 'IA'
        {
            pnd_Rpt_Vars_Pnd_Rpt_Dvdnd_Amt.nadd(pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt);                                                                                          //Natural: ADD #INV-DVDND-AMT TO #RPT-DVDND-AMT
            pnd_Rpt_Vars_Pnd_Rpt_Dci_Amt.nadd(pnd_Inv_Amts_Pnd_Inv_Dci_Amt);                                                                                              //Natural: ADD #INV-DCI-AMT TO #RPT-DCI-AMT
            //*  ROXAN ADD #RPT-EXT.PYMNT-DED-AMT (1:C-INV-ACCT) TO #RPT-VLNTRY-DED
            pnd_Rpt_Vars_Pnd_Rpt_Vlntry_Ded.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue("*"));                                                               //Natural: ADD #RPT-EXT.PYMNT-DED-AMT ( * ) TO #RPT-VLNTRY-DED
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Rpt_Vars_Pnd_Rpt_Dpi_Amt.nadd(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt);                                                                                                  //Natural: ADD #INV-DPI-AMT TO #RPT-DPI-AMT
        pnd_Rpt_Vars_Pnd_Rpt_Process_Chg.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                     //Natural: ADD #RPT-EXT.INV-ACCT-EXP-AMT ( 1:C-INV-ACCT ) TO #RPT-PROCESS-CHG
        pnd_Rpt_Vars_Pnd_Rpt_Fed_Ded.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                    //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( 1:C-INV-ACCT ) TO #RPT-FED-DED
        pnd_Rpt_Vars_Pnd_Rpt_State_Ded.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                 //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( 1:C-INV-ACCT ) TO #RPT-STATE-DED
        pnd_Rpt_Vars_Pnd_Rpt_Local_Ded.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                 //Natural: ADD #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( 1:C-INV-ACCT ) TO #RPT-LOCAL-DED
        pnd_Rpt_Vars_Pnd_Rpt_Net_Amt.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                                  //Natural: ADD #INV-NET-AMT TO #RPT-NET-AMT
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #RPT-EXT.C-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); pnd_I.nadd(1))
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I).notEquals(getZero())))                                                            //Natural: IF #RPT-EXT.INV-ACCT-IVC-AMT ( #I ) NE 0
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I).equals(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I))))     //Natural: IF #RPT-EXT.INV-ACCT-IVC-AMT ( #I ) = #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
                {
                    pnd_Inv_Amts_Pnd_Inv_Ivc_Pymnts.nadd(1);                                                                                                              //Natural: ADD 1 TO #INV-IVC-PYMNTS
                    pnd_Rpt_Vars_Pnd_Rpt_Ivc_Dollars.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I));                                                 //Natural: ADD #RPT-EXT.INV-ACCT-IVC-AMT ( #I ) TO #RPT-IVC-DOLLARS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Inv_Amts_Pnd_Inv_Ivc_Incl_Pymnts.nadd(1);                                                                                                         //Natural: ADD 1 TO #INV-IVC-INCL-PYMNTS
                    pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Dollars.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I));                                            //Natural: ADD #RPT-EXT.INV-ACCT-IVC-AMT ( #I ) TO #RPT-IVC-INCL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Inv_Amts_Pnd_Inv_Ivc_Incl_Pymnts.greater(getZero())))                                                                                           //Natural: IF #INV-IVC-INCL-PYMNTS > 0
        {
            pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Pymnts.nadd(1);                                                                                                                 //Natural: ADD 1 TO #RPT-IVC-INCL-PYMNTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Inv_Amts_Pnd_Inv_Ivc_Pymnts.greater(getZero())))                                                                                            //Natural: IF #INV-IVC-PYMNTS > 0
            {
                pnd_Rpt_Vars_Pnd_Rpt_Ivc_Pymnts.nadd(1);                                                                                                                  //Natural: ADD 1 TO #RPT-IVC-PYMNTS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet412 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet412++;
            pnd_Rpt_Vars_Pnd_Rpt_Num_Checks.nadd(1);                                                                                                                      //Natural: ADD 1 TO #RPT-NUM-CHECKS
            pnd_Rpt_Vars_Pnd_Rpt_Chk_Gross_Dollars.compute(new ComputeParameters(false, pnd_Rpt_Vars_Pnd_Rpt_Chk_Gross_Dollars), pnd_Rpt_Vars_Pnd_Rpt_Chk_Gross_Dollars.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dci_Amt)); //Natural: ADD #INV-GROSS-AMT #INV-DVDND-AMT #INV-DPI-AMT #INV-DCI-AMT TO #RPT-CHK-GROSS-DOLLARS
            pnd_Rpt_Vars_Pnd_Rpt_Chk_Net_Dollars.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                      //Natural: ADD #INV-NET-AMT TO #RPT-CHK-NET-DOLLARS
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet412++;
            pnd_Rpt_Vars_Pnd_Rpt_Num_Efts.nadd(1);                                                                                                                        //Natural: ADD 1 TO #RPT-NUM-EFTS
            pnd_Rpt_Vars_Pnd_Rpt_Eft_Gross_Dollars.compute(new ComputeParameters(false, pnd_Rpt_Vars_Pnd_Rpt_Eft_Gross_Dollars), pnd_Rpt_Vars_Pnd_Rpt_Eft_Gross_Dollars.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dci_Amt)); //Natural: ADD #INV-GROSS-AMT #INV-DVDND-AMT #INV-DPI-AMT #INV-DCI-AMT TO #RPT-EFT-GROSS-DOLLARS
            pnd_Rpt_Vars_Pnd_Rpt_Eft_Net_Dollars.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                      //Natural: ADD #INV-NET-AMT TO #RPT-EFT-NET-DOLLARS
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(3))))
        {
            decideConditionsMet412++;
            pnd_Rpt_Vars_Pnd_Rpt_Num_Globals.nadd(1);                                                                                                                     //Natural: ADD 1 TO #RPT-NUM-GLOBALS
            pnd_Rpt_Vars_Pnd_Rpt_Glob_Gross_Dollars.compute(new ComputeParameters(false, pnd_Rpt_Vars_Pnd_Rpt_Glob_Gross_Dollars), pnd_Rpt_Vars_Pnd_Rpt_Glob_Gross_Dollars.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dci_Amt)); //Natural: ADD #INV-GROSS-AMT #INV-DVDND-AMT #INV-DPI-AMT #INV-DCI-AMT TO #RPT-GLOB-GROSS-DOLLARS
            pnd_Rpt_Vars_Pnd_Rpt_Glob_Net_Dollars.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                     //Natural: ADD #INV-NET-AMT TO #RPT-GLOB-NET-DOLLARS
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Rpt_Vars_Pnd_Rpt_Num_Other.nadd(1);                                                                                                                       //Natural: ADD 1 TO #RPT-NUM-OTHER
            pnd_Rpt_Vars_Pnd_Rpt_Other_Gross_Dollars.compute(new ComputeParameters(false, pnd_Rpt_Vars_Pnd_Rpt_Other_Gross_Dollars), pnd_Rpt_Vars_Pnd_Rpt_Other_Gross_Dollars.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dci_Amt)); //Natural: ADD #INV-GROSS-AMT #INV-DVDND-AMT #INV-DPI-AMT #INV-DCI-AMT TO #RPT-OTHER-GROSS-DOLLARS
            pnd_Rpt_Vars_Pnd_Rpt_Other_Net_Dollars.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                    //Natural: ADD #INV-NET-AMT TO #RPT-OTHER-NET-DOLLARS
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("MS") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA"))) //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' OR = 'MS' OR = 'IA'
        {
            pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus.compute(new ComputeParameters(false, pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus), pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dvdnd_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dci_Amt)); //Natural: ASSIGN #RPT-GROSS-PLUS := #RPT-GROSS-PLUS + #INV-GROSS-AMT + #INV-DVDND-AMT + #INV-DPI-AMT + #INV-DCI-AMT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus.compute(new ComputeParameters(false, pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus), pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt)); //Natural: ASSIGN #RPT-GROSS-PLUS := #RPT-GROSS-PLUS + #INV-GROSS-AMT + #INV-DPI-AMT
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calc_Mdo_Vars() throws Exception                                                                                                                     //Natural: CALC-MDO-VARS
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        pnd_Inv_Amts.reset();                                                                                                                                             //Natural: RESET #INV-AMTS
        pnd_Inv_Amts_Pnd_Inv_Gross_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                     //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) TO #INV-GROSS-AMT
        pnd_Inv_Amts_Pnd_Inv_Dpi_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                         //Natural: ADD #RPT-EXT.INV-ACCT-DPI-AMT ( 1:C-INV-ACCT ) TO #INV-DPI-AMT
        pnd_Inv_Amts_Pnd_Inv_Net_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                   //Natural: ADD #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( 1:C-INV-ACCT ) TO #INV-NET-AMT
        pnd_Mdo_Vars_Pnd_Mdo_Pymnt_Rqsts.nadd(1);                                                                                                                         //Natural: ADD 1 TO #MDO-PYMNT-RQSTS
        pnd_Mdo_Vars_Pnd_Mdo_Gross_Amt.nadd(pnd_Inv_Amts_Pnd_Inv_Gross_Amt);                                                                                              //Natural: ADD #INV-GROSS-AMT TO #MDO-GROSS-AMT
        pnd_Mdo_Vars_Pnd_Mdo_Dpi_Amt.nadd(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt);                                                                                                  //Natural: ADD #INV-DPI-AMT TO #MDO-DPI-AMT
        pnd_Mdo_Vars_Pnd_Mdo_Process_Chg.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                     //Natural: ADD #RPT-EXT.INV-ACCT-EXP-AMT ( 1:C-INV-ACCT ) TO #MDO-PROCESS-CHG
        pnd_Mdo_Vars_Pnd_Mdo_Fed_Ded.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                    //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( 1:C-INV-ACCT ) TO #MDO-FED-DED
        pnd_Mdo_Vars_Pnd_Mdo_State_Ded.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                 //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( 1:C-INV-ACCT ) TO #MDO-STATE-DED
        pnd_Mdo_Vars_Pnd_Mdo_Local_Ded.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct()));                 //Natural: ADD #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( 1:C-INV-ACCT ) TO #MDO-LOCAL-DED
        pnd_Mdo_Vars_Pnd_Mdo_Net_Amt.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                                  //Natural: ADD #INV-NET-AMT TO #MDO-NET-AMT
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #RPT-EXT.C-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); pnd_I.nadd(1))
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I).notEquals(getZero())))                                                            //Natural: IF #RPT-EXT.INV-ACCT-IVC-AMT ( #I ) NE 0
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I).equals(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I))))     //Natural: IF #RPT-EXT.INV-ACCT-IVC-AMT ( #I ) = #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
                {
                    pnd_Inv_Amts_Pnd_Inv_Ivc_Pymnts.nadd(1);                                                                                                              //Natural: ADD 1 TO #INV-IVC-PYMNTS
                    pnd_Mdo_Vars_Pnd_Mdo_Ivc_Dollars.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I));                                                 //Natural: ADD #RPT-EXT.INV-ACCT-IVC-AMT ( #I ) TO #MDO-IVC-DOLLARS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Inv_Amts_Pnd_Inv_Ivc_Incl_Pymnts.nadd(1);                                                                                                         //Natural: ADD 1 TO #INV-IVC-INCL-PYMNTS
                    pnd_Mdo_Vars_Pnd_Mdo_Ivc_Incl_Dollars.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I));                                            //Natural: ADD #RPT-EXT.INV-ACCT-IVC-AMT ( #I ) TO #MDO-IVC-INCL-DOLLARS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Inv_Amts_Pnd_Inv_Ivc_Incl_Pymnts.greater(getZero())))                                                                                           //Natural: IF #INV-IVC-INCL-PYMNTS > 0
        {
            pnd_Mdo_Vars_Pnd_Mdo_Ivc_Incl_Pymnts.nadd(1);                                                                                                                 //Natural: ADD 1 TO #MDO-IVC-INCL-PYMNTS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Inv_Amts_Pnd_Inv_Ivc_Pymnts.greater(getZero())))                                                                                            //Natural: IF #INV-IVC-PYMNTS > 0
            {
                pnd_Mdo_Vars_Pnd_Mdo_Ivc_Pymnts.nadd(1);                                                                                                                  //Natural: ADD 1 TO #MDO-IVC-PYMNTS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet469 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet469++;
            pnd_Mdo_Vars_Pnd_Mdo_Num_Checks.nadd(1);                                                                                                                      //Natural: ADD 1 TO #MDO-NUM-CHECKS
            pnd_Mdo_Vars_Pnd_Mdo_Chk_Gross_Dollars.compute(new ComputeParameters(false, pnd_Mdo_Vars_Pnd_Mdo_Chk_Gross_Dollars), pnd_Mdo_Vars_Pnd_Mdo_Chk_Gross_Dollars.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt)); //Natural: ADD #INV-GROSS-AMT #INV-DPI-AMT TO #MDO-CHK-GROSS-DOLLARS
            pnd_Mdo_Vars_Pnd_Mdo_Chk_Net_Dollars.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                      //Natural: ADD #INV-NET-AMT TO #MDO-CHK-NET-DOLLARS
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet469++;
            pnd_Mdo_Vars_Pnd_Mdo_Num_Efts.nadd(1);                                                                                                                        //Natural: ADD 1 TO #MDO-NUM-EFTS
            pnd_Mdo_Vars_Pnd_Mdo_Eft_Gross_Dollars.compute(new ComputeParameters(false, pnd_Mdo_Vars_Pnd_Mdo_Eft_Gross_Dollars), pnd_Mdo_Vars_Pnd_Mdo_Eft_Gross_Dollars.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt)); //Natural: ADD #INV-GROSS-AMT #INV-DPI-AMT TO #MDO-EFT-GROSS-DOLLARS
            pnd_Mdo_Vars_Pnd_Mdo_Eft_Net_Dollars.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                      //Natural: ADD #INV-NET-AMT TO #MDO-EFT-NET-DOLLARS
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(3))))
        {
            decideConditionsMet469++;
            pnd_Mdo_Vars_Pnd_Mdo_Num_Globals.nadd(1);                                                                                                                     //Natural: ADD 1 TO #MDO-NUM-GLOBALS
            pnd_Mdo_Vars_Pnd_Mdo_Glob_Gross_Dollars.compute(new ComputeParameters(false, pnd_Mdo_Vars_Pnd_Mdo_Glob_Gross_Dollars), pnd_Mdo_Vars_Pnd_Mdo_Glob_Gross_Dollars.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt)); //Natural: ADD #INV-GROSS-AMT #INV-DPI-AMT TO #MDO-GLOB-GROSS-DOLLARS
            pnd_Mdo_Vars_Pnd_Mdo_Glob_Net_Dollars.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                     //Natural: ADD #INV-NET-AMT TO #MDO-GLOB-NET-DOLLARS
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Mdo_Vars_Pnd_Mdo_Num_Other.nadd(1);                                                                                                                       //Natural: ADD 1 TO #MDO-NUM-OTHER
            pnd_Mdo_Vars_Pnd_Mdo_Other_Gross_Dollars.compute(new ComputeParameters(false, pnd_Mdo_Vars_Pnd_Mdo_Other_Gross_Dollars), pnd_Mdo_Vars_Pnd_Mdo_Other_Gross_Dollars.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt)); //Natural: ADD #INV-GROSS-AMT #INV-DPI-AMT TO #MDO-OTHER-GROSS-DOLLARS
            pnd_Mdo_Vars_Pnd_Mdo_Other_Net_Dollars.nadd(pnd_Inv_Amts_Pnd_Inv_Net_Amt);                                                                                    //Natural: ADD #INV-NET-AMT TO #MDO-OTHER-NET-DOLLARS
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Mdo_Vars_Pnd_Mdo_Gross_Plus.compute(new ComputeParameters(false, pnd_Mdo_Vars_Pnd_Mdo_Gross_Plus), pnd_Mdo_Vars_Pnd_Mdo_Gross_Plus.add(pnd_Inv_Amts_Pnd_Inv_Gross_Amt).add(pnd_Inv_Amts_Pnd_Inv_Dpi_Amt)); //Natural: ASSIGN #MDO-GROSS-PLUS := #MDO-GROSS-PLUS + #INV-GROSS-AMT + #INV-DPI-AMT
    }
    private void sub_Calc_Grand_Totals() throws Exception                                                                                                                 //Natural: CALC-GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Total_Vars_Pnd_Tot_Pymnt_Rqsts.nadd(pnd_Rpt_Vars_Pnd_Rpt_Pymnt_Rqsts);                                                                                        //Natural: ADD #RPT-PYMNT-RQSTS TO #TOT-PYMNT-RQSTS
        pnd_Total_Vars_Pnd_Tot_Dvdnd_Amt.nadd(pnd_Rpt_Vars_Pnd_Rpt_Dvdnd_Amt);                                                                                            //Natural: ADD #RPT-DVDND-AMT TO #TOT-DVDND-AMT
        pnd_Total_Vars_Pnd_Tot_Dci_Amt.nadd(pnd_Rpt_Vars_Pnd_Rpt_Dci_Amt);                                                                                                //Natural: ADD #RPT-DCI-AMT TO #TOT-DCI-AMT
        pnd_Total_Vars_Pnd_Tot_Vlntry_Ded.nadd(pnd_Rpt_Vars_Pnd_Rpt_Vlntry_Ded);                                                                                          //Natural: ADD #RPT-VLNTRY-DED TO #TOT-VLNTRY-DED
        pnd_Total_Vars_Pnd_Tot_Gross_Amt.nadd(pnd_Rpt_Vars_Pnd_Rpt_Gross_Amt);                                                                                            //Natural: ADD #RPT-GROSS-AMT TO #TOT-GROSS-AMT
        pnd_Total_Vars_Pnd_Tot_Dpi_Amt.nadd(pnd_Rpt_Vars_Pnd_Rpt_Dpi_Amt);                                                                                                //Natural: ADD #RPT-DPI-AMT TO #TOT-DPI-AMT
        pnd_Total_Vars_Pnd_Tot_Process_Chg.nadd(pnd_Rpt_Vars_Pnd_Rpt_Process_Chg);                                                                                        //Natural: ADD #RPT-PROCESS-CHG TO #TOT-PROCESS-CHG
        pnd_Total_Vars_Pnd_Tot_Fed_Ded.nadd(pnd_Rpt_Vars_Pnd_Rpt_Fed_Ded);                                                                                                //Natural: ADD #RPT-FED-DED TO #TOT-FED-DED
        pnd_Total_Vars_Pnd_Tot_State_Ded.nadd(pnd_Rpt_Vars_Pnd_Rpt_State_Ded);                                                                                            //Natural: ADD #RPT-STATE-DED TO #TOT-STATE-DED
        pnd_Total_Vars_Pnd_Tot_Local_Ded.nadd(pnd_Rpt_Vars_Pnd_Rpt_Local_Ded);                                                                                            //Natural: ADD #RPT-LOCAL-DED TO #TOT-LOCAL-DED
        pnd_Total_Vars_Pnd_Tot_Net_Amt.nadd(pnd_Rpt_Vars_Pnd_Rpt_Net_Amt);                                                                                                //Natural: ADD #RPT-NET-AMT TO #TOT-NET-AMT
        pnd_Total_Vars_Pnd_Tot_Ivc_Pymnts.nadd(pnd_Rpt_Vars_Pnd_Rpt_Ivc_Pymnts);                                                                                          //Natural: ADD #RPT-IVC-PYMNTS TO #TOT-IVC-PYMNTS
        pnd_Total_Vars_Pnd_Tot_Ivc_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Ivc_Dollars);                                                                                        //Natural: ADD #RPT-IVC-DOLLARS TO #TOT-IVC-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Ivc_Incl_Pymnts.nadd(pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Pymnts);                                                                                //Natural: ADD #RPT-IVC-INCL-PYMNTS TO #TOT-IVC-INCL-PYMNTS
        pnd_Total_Vars_Pnd_Tot_Ivc_Incl_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Dollars);                                                                              //Natural: ADD #RPT-IVC-INCL-DOLLARS TO #TOT-IVC-INCL-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Num_Checks.nadd(pnd_Rpt_Vars_Pnd_Rpt_Num_Checks);                                                                                          //Natural: ADD #RPT-NUM-CHECKS TO #TOT-NUM-CHECKS
        pnd_Total_Vars_Pnd_Tot_Chk_Gross_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Chk_Gross_Dollars);                                                                            //Natural: ADD #RPT-CHK-GROSS-DOLLARS TO #TOT-CHK-GROSS-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Chk_Net_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Chk_Net_Dollars);                                                                                //Natural: ADD #RPT-CHK-NET-DOLLARS TO #TOT-CHK-NET-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Num_Efts.nadd(pnd_Rpt_Vars_Pnd_Rpt_Num_Efts);                                                                                              //Natural: ADD #RPT-NUM-EFTS TO #TOT-NUM-EFTS
        pnd_Total_Vars_Pnd_Tot_Eft_Gross_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Eft_Gross_Dollars);                                                                            //Natural: ADD #RPT-EFT-GROSS-DOLLARS TO #TOT-EFT-GROSS-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Eft_Net_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Eft_Net_Dollars);                                                                                //Natural: ADD #RPT-EFT-NET-DOLLARS TO #TOT-EFT-NET-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Num_Globals.nadd(pnd_Rpt_Vars_Pnd_Rpt_Num_Globals);                                                                                        //Natural: ADD #RPT-NUM-GLOBALS TO #TOT-NUM-GLOBALS
        pnd_Total_Vars_Pnd_Tot_Glob_Gross_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Glob_Gross_Dollars);                                                                          //Natural: ADD #RPT-GLOB-GROSS-DOLLARS TO #TOT-GLOB-GROSS-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Glob_Net_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Glob_Net_Dollars);                                                                              //Natural: ADD #RPT-GLOB-NET-DOLLARS TO #TOT-GLOB-NET-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Num_Other.nadd(pnd_Rpt_Vars_Pnd_Rpt_Num_Other);                                                                                            //Natural: ADD #RPT-NUM-OTHER TO #TOT-NUM-OTHER
        pnd_Total_Vars_Pnd_Tot_Other_Gross_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Other_Gross_Dollars);                                                                        //Natural: ADD #RPT-OTHER-GROSS-DOLLARS TO #TOT-OTHER-GROSS-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Other_Net_Dollars.nadd(pnd_Rpt_Vars_Pnd_Rpt_Other_Net_Dollars);                                                                            //Natural: ADD #RPT-OTHER-NET-DOLLARS TO #TOT-OTHER-NET-DOLLARS
        pnd_Total_Vars_Pnd_Tot_Gross_Plus.nadd(pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus);                                                                                          //Natural: ADD #RPT-GROSS-PLUS TO #TOT-GROSS-PLUS
    }
    private void sub_Determine_Orgn_Title() throws Exception                                                                                                              //Natural: DETERMINE-ORGN-TITLE
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(pnd_Old_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ")))                                                                                                 //Natural: IF #OLD-CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' '
        {
            short decideConditionsMet523 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #OLD-CNTRCT-ORGN-CDE;//Natural: VALUE 'DC'
            if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("DC"))))
            {
                decideConditionsMet523++;
                pnd_Orgn_Title.setValue("IA DEATH");                                                                                                                      //Natural: ASSIGN #ORGN-TITLE := 'IA DEATH'
            }                                                                                                                                                             //Natural: VALUE 'IA'
            else if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("IA"))))
            {
                decideConditionsMet523++;
                pnd_Orgn_Title.setValue("IA");                                                                                                                            //Natural: ASSIGN #ORGN-TITLE := 'IA'
            }                                                                                                                                                             //Natural: VALUE 'MS'
            else if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("MS"))))
            {
                decideConditionsMet523++;
                pnd_Orgn_Title.setValue("MS");                                                                                                                            //Natural: ASSIGN #ORGN-TITLE := 'MS'
            }                                                                                                                                                             //Natural: VALUE 'DS'
            else if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("DS"))))
            {
                decideConditionsMet523++;
                pnd_Orgn_Title.setValue("DS");                                                                                                                            //Natural: ASSIGN #ORGN-TITLE := 'DS'
            }                                                                                                                                                             //Natural: VALUE 'SS'
            else if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("SS"))))
            {
                decideConditionsMet523++;
                pnd_Orgn_Title.setValue("SINGLE SUM");                                                                                                                    //Natural: ASSIGN #ORGN-TITLE := 'SINGLE SUM'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  REDRAW
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            short decideConditionsMet539 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #OLD-CNTRCT-ORGN-CDE;//Natural: VALUE 'DC'
            if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("DC"))))
            {
                decideConditionsMet539++;
                pnd_Orgn_Title.setValue("DC REDRAW");                                                                                                                     //Natural: ASSIGN #ORGN-TITLE := 'DC REDRAW'
            }                                                                                                                                                             //Natural: VALUE 'IA'
            else if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("IA"))))
            {
                decideConditionsMet539++;
                pnd_Orgn_Title.setValue("IA REDRAW");                                                                                                                     //Natural: ASSIGN #ORGN-TITLE := 'IA REDRAW'
            }                                                                                                                                                             //Natural: VALUE 'MS'
            else if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("MS"))))
            {
                decideConditionsMet539++;
                pnd_Orgn_Title.setValue("MS REDRAW");                                                                                                                     //Natural: ASSIGN #ORGN-TITLE := 'MS REDRAW'
            }                                                                                                                                                             //Natural: VALUE 'DS'
            else if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("DS"))))
            {
                decideConditionsMet539++;
                pnd_Orgn_Title.setValue("DS REDRAW");                                                                                                                     //Natural: ASSIGN #ORGN-TITLE := 'DS REDRAW'
            }                                                                                                                                                             //Natural: VALUE 'SS'
            else if (condition((pnd_Old_Cntrct_Orgn_Cde.equals("SS"))))
            {
                decideConditionsMet539++;
                pnd_Orgn_Title.setValue("SS REDRAW");                                                                                                                     //Natural: ASSIGN #ORGN-TITLE := 'SS REDRAW'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Control_Report() throws Exception                                                                                                              //Natural: WRITE-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(5),pnd_Orgn_Title,"-  NUMBER OF PAYMENT REQUESTS      ",new ColumnSpacing(10),pnd_Rpt_Vars_Pnd_Rpt_Pymnt_Rqsts,NEWLINE,NEWLINE,new  //Natural: WRITE ( 1 ) NOTITLE 5T #ORGN-TITLE '-  NUMBER OF PAYMENT REQUESTS      ' 10X #RPT-PYMNT-RQSTS // 16T '-  GROSS AMOUNT SETTLED            ' 2X #RPT-GROSS-PLUS ( EM = ZZZ,ZZZ,ZZ9.99- ) // '-' ( 132 ) // 5T 'TOTAL SETTLEMENT TOTALS'
            TabSetting(16),"-  GROSS AMOUNT SETTLED            ",new ColumnSpacing(2),pnd_Rpt_Vars_Pnd_Rpt_Gross_Plus, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,"-",new 
            RepeatItem(132),NEWLINE,NEWLINE,new TabSetting(5),"TOTAL SETTLEMENT TOTALS");
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(16),"-  GROSS AMOUNT (CONTRACTUAL)       ",pnd_Rpt_Vars_Pnd_Rpt_Gross_Amt,              //Natural: WRITE ( 1 ) NOTITLE //16T '-  GROSS AMOUNT (CONTRACTUAL)       ' #RPT-GROSS-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) //16T '-  DIVIDEND AMOUNT                  ' #RPT-DVDND-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  DPI AMOUNT                       ' #RPT-DPI-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  DCI AMOUNT                       ' #RPT-DCI-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  PROCESS CHARGE                   ' #RPT-PROCESS-CHG ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  FEDERAL TAX DEDUCTION $ AMOUNT   ' #RPT-FED-DED ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  STATE TAX DEDUCTION $ AMOUNT     ' #RPT-STATE-DED ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  LOCAL TAX DEDUCTION $ AMOUNT     ' #RPT-LOCAL-DED ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  VOLUNTARY DEDUCTION $ AMOUNT     ' #RPT-VLNTRY-DED ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  NET AMOUNT                       ' #RPT-NET-AMT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  NUMBER OF IVC PAYMENTS           ' 10X #RPT-IVC-PYMNTS ( EM = Z,ZZ9- ) / 16T '-  IVC PAYMENT DOLLAR AMOUNT        ' #RPT-IVC-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  NUMBER OF INCLUDED IVC PAYMENTS  ' 10X #RPT-IVC-INCL-PYMNTS ( EM = Z,ZZ9- ) / 16T '-  DOLLAR AMOUNT OF INCLUDED IVC    ' #RPT-IVC-INCL-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  NUMBER OF PAYMENT REQUESTS       ' 9X #RPT-PYMNT-RQSTS ( EM = ZZ,ZZ9- ) / 16T '-  NUMBER OF CHECKS                 ' 10X #RPT-NUM-CHECKS ( EM = Z,ZZ9- ) / 16T '-  CHECK GROSS DOLLAR AMOUNT        ' #RPT-CHK-GROSS-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  CHECK NET DOLLAR AMOUNT          ' #RPT-CHK-NET-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  NUMBER OF EFT"S                  ' 10X #RPT-NUM-EFTS ( EM = Z,ZZ9- ) / 16T '-  EFT GROSS DOLLAR AMOUNT          ' #RPT-EFT-GROSS-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  EFT NET DOLLAR AMOUNT            ' #RPT-EFT-NET-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  NUMBER OF GLOBALS                ' 10X #RPT-NUM-GLOBALS ( EM = Z,ZZ9- ) / 16T '-  GLOBAL GROSS DOLLAR AMOUNT       ' #RPT-GLOB-GROSS-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  GLOBAL NET DOLLAR AMOUNT         ' #RPT-GLOB-NET-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  NUMBER OF OTHER                  ' 10X #RPT-NUM-OTHER ( EM = Z,ZZ9- ) / 16T '-  OTHER GROSS DOLLAR AMOUNT        ' #RPT-OTHER-GROSS-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- ) / 16T '-  OTHER NET DOLLAR AMOUNT          ' #RPT-OTHER-NET-DOLLARS ( EM = ZZZ,ZZZ,ZZ9.99- )
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,new TabSetting(16),"-  DIVIDEND AMOUNT                  ",pnd_Rpt_Vars_Pnd_Rpt_Dvdnd_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  DPI AMOUNT                       ",pnd_Rpt_Vars_Pnd_Rpt_Dpi_Amt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  DCI AMOUNT                       ",pnd_Rpt_Vars_Pnd_Rpt_Dci_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(16),"-  PROCESS CHARGE                   ",pnd_Rpt_Vars_Pnd_Rpt_Process_Chg, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  FEDERAL TAX DEDUCTION $ AMOUNT   ",pnd_Rpt_Vars_Pnd_Rpt_Fed_Ded, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  STATE TAX DEDUCTION $ AMOUNT     ",pnd_Rpt_Vars_Pnd_Rpt_State_Ded, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  LOCAL TAX DEDUCTION $ AMOUNT     ",pnd_Rpt_Vars_Pnd_Rpt_Local_Ded, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  VOLUNTARY DEDUCTION $ AMOUNT     ",pnd_Rpt_Vars_Pnd_Rpt_Vlntry_Ded, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(16),"-  NET AMOUNT                       ",pnd_Rpt_Vars_Pnd_Rpt_Net_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  NUMBER OF IVC PAYMENTS           ",new 
            ColumnSpacing(10),pnd_Rpt_Vars_Pnd_Rpt_Ivc_Pymnts, new ReportEditMask ("Z,ZZ9-"),NEWLINE,new TabSetting(16),"-  IVC PAYMENT DOLLAR AMOUNT        ",pnd_Rpt_Vars_Pnd_Rpt_Ivc_Dollars, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  NUMBER OF INCLUDED IVC PAYMENTS  ",new ColumnSpacing(10),pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Pymnts, 
            new ReportEditMask ("Z,ZZ9-"),NEWLINE,new TabSetting(16),"-  DOLLAR AMOUNT OF INCLUDED IVC    ",pnd_Rpt_Vars_Pnd_Rpt_Ivc_Incl_Dollars, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  NUMBER OF PAYMENT REQUESTS       ",new ColumnSpacing(9),pnd_Rpt_Vars_Pnd_Rpt_Pymnt_Rqsts, 
            new ReportEditMask ("ZZ,ZZ9-"),NEWLINE,new TabSetting(16),"-  NUMBER OF CHECKS                 ",new ColumnSpacing(10),pnd_Rpt_Vars_Pnd_Rpt_Num_Checks, 
            new ReportEditMask ("Z,ZZ9-"),NEWLINE,new TabSetting(16),"-  CHECK GROSS DOLLAR AMOUNT        ",pnd_Rpt_Vars_Pnd_Rpt_Chk_Gross_Dollars, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  CHECK NET DOLLAR AMOUNT          ",pnd_Rpt_Vars_Pnd_Rpt_Chk_Net_Dollars, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  NUMBER OF EFT'S                  ",new ColumnSpacing(10),pnd_Rpt_Vars_Pnd_Rpt_Num_Efts, 
            new ReportEditMask ("Z,ZZ9-"),NEWLINE,new TabSetting(16),"-  EFT GROSS DOLLAR AMOUNT          ",pnd_Rpt_Vars_Pnd_Rpt_Eft_Gross_Dollars, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  EFT NET DOLLAR AMOUNT            ",pnd_Rpt_Vars_Pnd_Rpt_Eft_Net_Dollars, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  NUMBER OF GLOBALS                ",new ColumnSpacing(10),pnd_Rpt_Vars_Pnd_Rpt_Num_Globals, 
            new ReportEditMask ("Z,ZZ9-"),NEWLINE,new TabSetting(16),"-  GLOBAL GROSS DOLLAR AMOUNT       ",pnd_Rpt_Vars_Pnd_Rpt_Glob_Gross_Dollars, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  GLOBAL NET DOLLAR AMOUNT         ",pnd_Rpt_Vars_Pnd_Rpt_Glob_Net_Dollars, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  NUMBER OF OTHER                  ",new ColumnSpacing(10),pnd_Rpt_Vars_Pnd_Rpt_Num_Other, 
            new ReportEditMask ("Z,ZZ9-"),NEWLINE,new TabSetting(16),"-  OTHER GROSS DOLLAR AMOUNT        ",pnd_Rpt_Vars_Pnd_Rpt_Other_Gross_Dollars, new 
            ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(16),"-  OTHER NET DOLLAR AMOUNT          ",pnd_Rpt_Vars_Pnd_Rpt_Other_Net_Dollars, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().isBreak(endOfData);
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_CdeIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak))
        {
            if (condition(pnd_Rpt_Vars_Pnd_Rpt_Pymnt_Rqsts.greater(getZero())))                                                                                           //Natural: IF #RPT-PYMNT-RQSTS > 0
            {
                pnd_Old_Cntrct_Cancel_Rdrw_Actvty_Cde.setValue(readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld);                                                               //Natural: MOVE OLD ( #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE ) TO #OLD-CNTRCT-CANCEL-RDRW-ACTVTY-CDE
                pnd_Old_Cntrct_Orgn_Cde.setValue(readWork01Cntrct_Orgn_CdeOld);                                                                                           //Natural: MOVE OLD ( #RPT-EXT.CNTRCT-ORGN-CDE ) TO #OLD-CNTRCT-ORGN-CDE
                                                                                                                                                                          //Natural: PERFORM DETERMINE-ORGN-TITLE
                sub_Determine_Orgn_Title();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT
                sub_Write_Control_Report();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALC-GRAND-TOTALS
                sub_Calc_Grand_Totals();
                if (condition(Global.isEscape())) {return;}
                pnd_Rpt_Vars.getValue("*").moveAll("0");                                                                                                                  //Natural: MOVE ALL '0' TO #RPT-VARS ( * )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Mdo_Vars_Pnd_Mdo_Pymnt_Rqsts.greater(getZero())))                                                                                           //Natural: IF #MDO-PYMNT-RQSTS > 0
            {
                if (condition(readWork01Cntrct_Cancel_Rdrw_Actvty_CdeOld.equals(" ")))                                                                                    //Natural: IF OLD ( #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE ) = ' '
                {
                    pnd_Orgn_Title.setValue("MDO");                                                                                                                       //Natural: ASSIGN #ORGN-TITLE := 'MDO'
                    //*  REDRAW
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Orgn_Title.setValue("MDO REDRAW");                                                                                                                //Natural: ASSIGN #ORGN-TITLE := 'MDO REDRAW'
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rpt_Vars.getValue("*").setValue(pnd_Mdo_Vars.getValue("*"));                                                                                          //Natural: MOVE #MDO-VARS ( * ) TO #RPT-VARS ( * )
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT
                sub_Write_Control_Report();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALC-GRAND-TOTALS
                sub_Calc_Grand_Totals();
                if (condition(Global.isEscape())) {return;}
                pnd_Rpt_Vars.getValue("*").moveAll("0");                                                                                                                  //Natural: MOVE ALL '0' TO #RPT-VARS ( * )
                pnd_Mdo_Vars.getValue("*").moveAll("0");                                                                                                                  //Natural: MOVE ALL '0' TO #MDO-VARS ( * )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Cntrct_Orgn_CdeIsBreak))
        {
            //*  EOF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals(readWork01Cntrct_Orgn_CdeOld)))                                                             //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = OLD ( #RPT-EXT.CNTRCT-ORGN-CDE )
            {
                pnd_Orgn_Title.setValue("TOTALS");                                                                                                                        //Natural: ASSIGN #ORGN-TITLE := 'TOTALS'
                pnd_Rpt_Vars.getValue("*").setValue(pnd_Total_Vars.getValue("*"));                                                                                        //Natural: MOVE #TOTAL-VARS ( * ) TO #RPT-VARS ( * )
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REPORT
                sub_Write_Control_Report();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
        Global.format(1, "LS=133 PS=60");
    }
}
