/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:37 PM
**        * FROM NATURAL PROGRAM : Fcpp835
************************************************************
**        * FILE NAME            : Fcpp835.java
**        * CLASS NAME           : Fcpp835
**        * INSTANCE NAME        : Fcpp835
************************************************************
************************************************************************
*
* PROGRAM  : FCPP835
*
* SYSTEM   : CPS
* TITLE    : EXTRACT
*
* FUNCTION : THIS PROGRAM WILL EXTRACT UNPAID AP CSR RECORDS
*
* HISTORY
* MODIFICATIONS :
*  JAN 98   RCC  ADDED AN OCCURENCE IN FCPLCRPT FOR EFTS WITH NO
*                PYMNT-CHK-SAVE-IND.
*
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
* 09/11/00 : A. YOUNG - RE-COMPILED DUE TO FCPLPMNT.
*
*  01/19/2001- LEON G. POPULATE CNTRCT-ANNTY-INS-TYPE FIELD
* FOR FCPN199A CALL
*
** 04/19/2002 : MCGEE - REPLACE CALL TO FCPNADDR WITH FCPNADD2
**            : TO CORRECT ADDRESS MISMATCH PROBLEM - NEW ADDRESS
**            : SUBROUTINE MIMICS FCPN448 (ONLINE ADDRESS ROUTINE)
**
** 08/22/2002 : YOUNG / REYHANIAN - A TEMPORARY FIX WAS PUT IN AND
**            : LATER ON REMOVED TO BYPASS CANCEL/REDRAW PAYMENTS.
**    11/2002 : R CARREON  STOW - EGTRRA CHANGES IN LDA
**    05/2003 : R CARREON  STOW - DUE TO NEW FCPA800
*
*  05/15/2008 : AER - ROTH-MAJOR1 - CHANGED FCPA800 TO FCPA800D
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
*
* 20140823 F.ENDAYA  PH RECEIVABLE. ADD TWO NEW CODES AP AND AS AS VALID
* FE201408           CSR CODE.
*
* 04/24/2016 F.ENDAYA   - TEXAS CHILD SUPPORT PROJECT. ADD OUTPUT FILE
* FE201604          FOR ALL PARTICIPANTS WITH DEDUCTION CODE 900.
*
* 04/2017: JJG - PIN EXPANSION, #TEXAS-CHD-OUTPUT SIZE FROM 200 TO 205
* 01/15/2020 CTS       - CPS SUNSET (CHG694525) TAG: CTS-0115
*                        MODIFED TO PROCESS STOPS ONLY
*                        AND IT WILL NOT REISSUE THE CHECKS.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp835 extends BLNatBase
{
    // Data Areas
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpaacum pdaFcpaacum;
    private PdaFcpaaddr pdaFcpaaddr;
    private PdaFcpacntl pdaFcpacntl;
    private PdaFcpa199a pdaFcpa199a;
    private PdaFcpa800d pdaFcpa800d;
    private PdaFcpa801c pdaFcpa801c;
    private LdaFcplcrpt ldaFcplcrpt;
    private LdaFcplpmnt ldaFcplpmnt;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Process_Orgn_Cde;
    private DbsField pnd_Ws_Pnd_Pymnt_S_Start;
    private DbsField pnd_Ws_Pnd_Pymnt_S_End;
    private DbsField pnd_Ws_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Pnd_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Break_Ind;
    private DbsField pnd_Ws_Pnd_Det_Amts;
    private DbsField pnd_Ws_Pnd_Pymnt_Amt;
    private DbsField pnd_Ws_Pnd_Cnr_Orgnl_Invrse_Dte;

    private DbsGroup pnd_Texas_Chd_Output;

    private DbsGroup pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Payee_Cde;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Cde;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Amt;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Date;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Orgn_Cde;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Fil1;
    private DbsField pnd_Wk_Tot_Ded;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_PYMNTCntrct_Orgn_CdeOld;
    private DbsField rEAD_PYMNTCntrct_Ppcn_NbrOld;
    private DbsField rEAD_PYMNTPymnt_Prcss_Seq_NumOld;
    private DbsField rEAD_PYMNTPnd_Break_IndOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpaacum = new PdaFcpaacum(localVariables);
        pdaFcpaaddr = new PdaFcpaaddr(localVariables);
        pdaFcpacntl = new PdaFcpacntl(localVariables);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        pdaFcpa801c = new PdaFcpa801c(localVariables);
        ldaFcplcrpt = new LdaFcplcrpt();
        registerRecord(ldaFcplcrpt);
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Process_Orgn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Process_Orgn_Cde", "#PROCESS-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Pymnt_S_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_S_Start", "#PYMNT-S-START", FieldType.STRING, 4);
        pnd_Ws_Pnd_Pymnt_S_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_S_End", "#PYMNT-S-END", FieldType.STRING, 4);
        pnd_Ws_Pnd_Cntrct_Orgn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Cntrct_Ppcn_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Ws_Pnd_Pymnt_Prcss_Seq_Num = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Prcss_Seq_Num", "#PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Break_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Break_Ind", "#BREAK-IND", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Det_Amts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Amts", "#DET-AMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Pymnt_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Amt", "#PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Cnr_Orgnl_Invrse_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cnr_Orgnl_Invrse_Dte", "#CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);

        pnd_Texas_Chd_Output = localVariables.newGroupInRecord("pnd_Texas_Chd_Output", "#TEXAS-CHD-OUTPUT");

        pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name = pnd_Texas_Chd_Output.newGroupInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name", "#TXS-PH-NAME");
        pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name = pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name", 
            "#TXS-PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name = pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name", 
            "#TXS-PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name = pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name", 
            "#TXS-PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Texas_Chd_Output_Pnd_Txs_Annt_Soc_Sec_Nbr = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Annt_Soc_Sec_Nbr", "#TXS-ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Unq_Id_Nbr = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Unq_Id_Nbr", "#TXS-CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Ppcn_Nbr = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Ppcn_Nbr", "#TXS-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Payee_Cde = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Payee_Cde", "#TXS-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Cde = pnd_Texas_Chd_Output.newFieldArrayInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Cde", "#TXS-PYMNT-DED-CDE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 10));
        pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Amt = pnd_Texas_Chd_Output.newFieldArrayInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Amt", "#TXS-PYMNT-DED-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 10));
        pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Date = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Date", "#TXS-PYMNT-DATE", FieldType.STRING, 
            8);
        pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr", "#TXS-CR-DR", FieldType.STRING, 
            1);
        pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Orgn_Cde = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Orgn_Cde", "#TXS-CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Texas_Chd_Output_Pnd_Txs_Fil1 = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Fil1", "#TXS-FIL1", FieldType.STRING, 1);
        pnd_Wk_Tot_Ded = localVariables.newFieldInRecord("pnd_Wk_Tot_Ded", "#WK-TOT-DED", FieldType.PACKED_DECIMAL, 17, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_PYMNTCntrct_Orgn_CdeOld = internalLoopRecord.newFieldInRecord("READ_PYMNT_Cntrct_Orgn_Cde_OLD", "Cntrct_Orgn_Cde_OLD", FieldType.STRING, 
            2);
        rEAD_PYMNTCntrct_Ppcn_NbrOld = internalLoopRecord.newFieldInRecord("READ_PYMNT_Cntrct_Ppcn_Nbr_OLD", "Cntrct_Ppcn_Nbr_OLD", FieldType.STRING, 
            10);
        rEAD_PYMNTPymnt_Prcss_Seq_NumOld = internalLoopRecord.newFieldInRecord("READ_PYMNT_Pymnt_Prcss_Seq_Num_OLD", "Pymnt_Prcss_Seq_Num_OLD", FieldType.NUMERIC, 
            7);
        rEAD_PYMNTPnd_Break_IndOld = internalLoopRecord.newFieldInRecord("READ_PYMNT_Pnd_Break_Ind_OLD", "Pnd_Break_Ind_OLD", FieldType.PACKED_DECIMAL, 
            7);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcplcrpt.initializeValues();
        ldaFcplpmnt.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Process_Orgn_Cde.setInitialValue("AP");
        pnd_Ws_Pnd_Pymnt_S_Start.setInitialValue("H'0000C400'");
        pnd_Ws_Pnd_Pymnt_S_End.setInitialValue("H'0000C4FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp835() throws Exception
    {
        super("Fcpp835");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 15 ) PS = 58 LS = 132 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 15 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Consolidated Payment System' 119T 'PAGE :' *PAGE-NUMBER ( 15 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 38T 'Annuity Payments Cancel and Redraw extract control report' 119T 'REPORT: RPT15' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: ASSIGN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) := #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 ) := #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 ) := TRUE
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).setValue(true);
        pdaFcpaaddr.getAddr_Work_Fields_Addr_Abend_Ind().setValue(true);                                                                                                  //Natural: ASSIGN ADDR-ABEND-IND := CURRENT-ADDR := TRUE
        pdaFcpaaddr.getAddr_Work_Fields_Current_Addr().setValue(true);
        setValueToSubstring(pnd_Ws_Pnd_Process_Orgn_Cde,pnd_Ws_Pnd_Pymnt_S_Start,1,2);                                                                                    //Natural: MOVE #PROCESS-ORGN-CDE TO SUBSTRING ( #PYMNT-S-START,1,2 )
        setValueToSubstring(pnd_Ws_Pnd_Process_Orgn_Cde,pnd_Ws_Pnd_Pymnt_S_End,1,2);                                                                                      //Natural: MOVE #PROCESS-ORGN-CDE TO SUBSTRING ( #PYMNT-S-END,1,2 )
        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ = #PYMNT-S-START THRU #PYMNT-S-END
        (
        "READ_PYMNT",
        new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Pnd_Pymnt_S_Start, "And", WcType.BY) ,
        new Wc("ORGN_STATUS_INVRSE_SEQ", "<=", pnd_Ws_Pnd_Pymnt_S_End, WcType.BY) },
        new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
        );
        boolean endOfDataReadPymnt = true;
        boolean firstReadPymnt = true;
        READ_PYMNT:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ_PYMNT")))
        {
            CheckAtStartofData915();

            if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().greater(0)))
            {
                atBreakEventRead_Pymnt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadPymnt = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            //*  CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 1
                {
                    pnd_Ws_Pnd_Break_Ind.nadd(1);                                                                                                                         //Natural: ADD 1 TO #BREAK-IND
                }                                                                                                                                                         //Natural: END-IF
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #BREAK-IND
            //*  CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                                                                                                                                                                          //Natural: PERFORM GEN-OUTPUT
                sub_Gen_Output();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PYMNT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PYMNT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
            rEAD_PYMNTCntrct_Orgn_CdeOld.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                       //Natural: END-READ
            rEAD_PYMNTCntrct_Ppcn_NbrOld.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());
            rEAD_PYMNTPymnt_Prcss_Seq_NumOld.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());
            rEAD_PYMNTPnd_Break_IndOld.setValue(pnd_Ws_Pnd_Break_Ind);
        }
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().greater(0)))
        {
            atBreakEventRead_Pymnt(endOfDataReadPymnt);
        }
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-CNTRL-RECORD
        sub_Write_Cntrl_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //* *******************************
        //* ***************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-OUTPUT
        //*  VALUE 'C', 'S', 'CN', 'SN'
        //*  VALUE 'C', 'S', 'CN', 'SN', 'CP', 'SP', 'RP', 'PR'
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-STMNT-FIELDS
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDR-RECORD
        //* ********************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CNTRL-RECORD
        //* ***********************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* *************************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AMT-ERROR
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ***********************************************************************
        //*  COPYCODE   : FCPCACUM
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : "AP" - CONTROL ACCUMULATIONS.
        //* ***********************************************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-TOTALS
    }
    //* IB 091699
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Cnr_Orgnl_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Orgnl_Invrse_Dte());                                                                   //Natural: ASSIGN #CNR-ORGNL-INVRSE-DTE := FCP-CONS-PYMNT.CNR-ORGNL-INVRSE-DTE
        pnd_Ws_Pnd_Pymnt_Amt.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                                                                   //Natural: ASSIGN #PYMNT-AMT := FCP-CONS-PYMNT.PYMNT-CHECK-AMT
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info().reset();                                                                                                       //Natural: RESET PYMNT-ADDR-INFO #DET-AMTS
        pnd_Ws_Pnd_Det_Amts.reset();
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := TRUE
                                                                                                                                                                          //Natural: PERFORM GET-ADDR-RECORD
        sub_Get_Addr_Record();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Gen_Output() throws Exception                                                                                                                        //Natural: GEN-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info().setValuesByName(pdaFcpaaddr.getAddr());                                                                        //Natural: MOVE BY NAME ADDR TO PYMNT-ADDR-INFO
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                           //Natural: MOVE BY NAME FCP-CONS-PYMNT TO PYMNT-ADDR-INFO
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                   //Natural: ASSIGN INV-ACCT-COUNT := C*INV-ACCT
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                             //Natural: ASSIGN C-PYMNT-DED-GRP := C*PYMNT-DED-GRP
        //*  LEON 01/19/01
        //*  LEON 01/19/01
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).setValuesByName(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct().getValue(1, //Natural: MOVE BY NAME FCP-CONS-PYMNT.INV-ACCT ( 1:INV-ACCT-COUNT ) TO WF-PYMNT-ADDR-GRP.INV-INFO ( 1:INV-ACCT-COUNT )
            ":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Ins_Type().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Annty_Ins_Type());                                          //Natural: ASSIGN #FUND-PDA.#CNTRCT-ANNTY-INS-TYPE := FCP-CONS-PYMNT.CNTRCT-ANNTY-INS-TYPE
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO INV-ACCT-COUNT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Ws_Pnd_I).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_I));   //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #I ) := #FUND-PDA.#INV-ACCT-INPUT := FCP-CONS-PYMNT.INV-ACCT-CDE ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_I));
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Num());                            //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-N ( #I ) := #FUND-PDA.#INV-ACCT-NUM
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde());                           //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( #I ) := #FUND-PDA.#COMPANY-CDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).compute(new ComputeParameters(false,      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:INV-ACCT-COUNT ) := FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( 1:INV-ACCT-COUNT ) + FCP-CONS-PYMNT.INV-ACCT-DVDND-AMT ( 1:INV-ACCT-COUNT )
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())), ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue(1,
            ":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue(1, //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:INV-ACCT-COUNT ) := FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( 1:INV-ACCT-COUNT )
            ":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().setValue(pnd_Ws_Pnd_Pymnt_Amt);                                                                                //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT := #PYMNT-AMT
        short decideConditionsMet1059 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE ' ', 'R'
        if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
        {
            decideConditionsMet1059++;
                                                                                                                                                                          //Natural: PERFORM GEN-STMNT-FIELDS
            sub_Gen_Stmnt_Fields();
            if (condition(Global.isEscape())) {return;}
            getWorkFiles().write(1, true, pdaFcpa801c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1, //Natural: WRITE WORK FILE 1 VARIABLE #CHECK-SORT-FIELDS PYMNT-ADDR-INFO INV-INFO ( 1:INV-ACCT-COUNT )
                ":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                  //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(21);                                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 21
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                sub_Accum_Control_Totals();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(new DbsDecimal("0.00"))))                                                             //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT = 0.00
            {
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(15);                                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 15
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(1);                                                                                                //Natural: ASSIGN #ACCUM-OCCUR := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'C', 'S', 'CN', 'SN', 'CP', 'SP', 'RP', 'PR', 'AS', 'AP'
        else if (condition((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AS") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AP"))))
        {
            decideConditionsMet1059++;
            //*                                         /* FE201408
            getWorkFiles().write(2, true, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",                 //Natural: WRITE WORK FILE 2 VARIABLE PYMNT-ADDR-INFO INV-INFO ( 1:INV-ACCT-COUNT )
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
            //*  FE201604 START
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1,":",10).greaterOrEqual(900) && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1, //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( 1:10 ) = 900 THRU 999
                ":",10).lessOrEqual(999)))
            {
                pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Ph_Last_Name());                                                      //Natural: ASSIGN #TXS-PH-LAST-NAME := WF-PYMNT-ADDR-GRP.PH-LAST-NAME
                pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Ph_First_Name());                                                    //Natural: ASSIGN #TXS-PH-FIRST-NAME := WF-PYMNT-ADDR-GRP.PH-FIRST-NAME
                pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Ph_Middle_Name());                                                  //Natural: ASSIGN #TXS-PH-MIDDLE-NAME := WF-PYMNT-ADDR-GRP.PH-MIDDLE-NAME
                pnd_Texas_Chd_Output_Pnd_Txs_Annt_Soc_Sec_Nbr.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr());                                              //Natural: ASSIGN #TXS-ANNT-SOC-SEC-NBR := WF-PYMNT-ADDR-GRP.ANNT-SOC-SEC-NBR
                pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Unq_Id_Nbr.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr());                                            //Natural: ASSIGN #TXS-CNTRCT-UNQ-ID-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-UNQ-ID-NBR
                pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Ppcn_Nbr.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                                //Natural: ASSIGN #TXS-CNTRCT-PPCN-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
                pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Payee_Cde.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde());                                              //Natural: ASSIGN #TXS-CNTRCT-PAYEE-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-PAYEE-CDE
                pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Cde.getValue(1,":",10).setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1,":",                   //Natural: ASSIGN #TXS-PYMNT-DED-CDE ( 1:10 ) := WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( 1:10 )
                    10));
                pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Amt.getValue(1,":",10).setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1,":",                   //Natural: ASSIGN #TXS-PYMNT-DED-AMT ( 1:10 ) := WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( 1:10 )
                    10));
                pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Date.setValueEdited(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #TXS-PYMNT-DATE
                pnd_Wk_Tot_Ded.nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1,":",10));                                                                 //Natural: ADD WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( 1:10 ) TO #WK-TOT-DED
                if (condition(pnd_Wk_Tot_Ded.greater(getZero())))                                                                                                         //Natural: IF #WK-TOT-DED GT 0
                {
                    pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr.setValue("C");                                                                                                     //Natural: ASSIGN #TXS-CR-DR := 'C'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr.setValue("D");                                                                                                     //Natural: ASSIGN #TXS-CR-DR := 'D'
                }                                                                                                                                                         //Natural: END-IF
                pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Orgn_Cde.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde());                                                //Natural: ASSIGN #TXS-CNTRCT-ORGN-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE
                pnd_Texas_Chd_Output_Pnd_Txs_Fil1.setValue("X");                                                                                                          //Natural: ASSIGN #TXS-FIL1 := 'X'
                getWorkFiles().write(16, false, pnd_Texas_Chd_Output);                                                                                                    //Natural: WRITE WORK FILE 16 #TEXAS-CHD-OUTPUT
            }                                                                                                                                                             //Natural: END-IF
            //*  FE201604 END
            //*  JWO 2010-11
            //*  FE201408
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN")  //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'CN' OR = 'CP' OR = 'RP' OR = 'PR' OR = 'AP'
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AP")))
            {
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(19);                                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 19
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(20);                                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 20
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet1059 > 0))
        {
            pnd_Ws_Pnd_Det_Amts.nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue("*"));                                                               //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( * ) TO #DET-AMTS
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
            sub_Accum_Control_Totals();
            if (condition(Global.isEscape())) {return;}
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(false);                                                                                              //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := FALSE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"UNKNOWN CANCEL, STOP, REDRAW CODE:",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde(),new         //Natural: WRITE '***' 25T 'UNKNOWN CANCEL, STOP, REDRAW CODE:' FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Gen_Stmnt_Fields() throws Exception                                                                                                                  //Natural: GEN-STMNT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Ph_Last_Name().equals(" ") && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Ph_First_Name().equals(" ") && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Ph_Middle_Name().equals(" "))) //Natural: IF WF-PYMNT-ADDR-GRP.PH-LAST-NAME = ' ' AND WF-PYMNT-ADDR-GRP.PH-FIRST-NAME = ' ' AND WF-PYMNT-ADDR-GRP.PH-MIDDLE-NAME = ' '
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(" ")))                                                                                //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = ' '
            {
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().setValue(" AAA");                                                                                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE := ' AAA'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().setValue(" AAB");                                                                                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE := ' AAB'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1).equals(" ") && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Lines().getValue(1).equals(" "))) //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) = ' ' AND WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINES ( 1 ) = ' '
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(" ")))                                                                                //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = ' '
            {
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().setValue(" AAC");                                                                                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE := ' AAC'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().setValue(" AAD");                                                                                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE := ' AAD'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(" ")))                                                                                    //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = ' '
        {
            pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde().setValue("0000");                                                                                      //Natural: ASSIGN #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE := '0000'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());                                          //Natural: ASSIGN #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(1));                              //Natural: ASSIGN #CHECK-SORT-FIELDS.CNTRCT-COMPANY-CDE := WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( 1 )
        pdaFcpa801c.getPnd_Check_Sort_Fields_Cnr_Orgnl_Invrse_Dte().setValue(pnd_Ws_Pnd_Cnr_Orgnl_Invrse_Dte);                                                            //Natural: ASSIGN #CHECK-SORT-FIELDS.CNR-ORGNL-INVRSE-DTE := #CNR-ORGNL-INVRSE-DTE
        //*                                                 VF 8/99
    }
    private void sub_Get_Addr_Record() throws Exception                                                                                                                   //Natural: GET-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr());                                                      //Natural: ASSIGN ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR := FCP-CONS-PYMNT.CNTRCT-CMBN-NBR
        pdaFcpaaddr.getAddr_Cntrct_Payee_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                                //Natural: ASSIGN ADDR.CNTRCT-PAYEE-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                  //Natural: ASSIGN ADDR.CNTRCT-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        pdaFcpaaddr.getAddr_Cntrct_Invrse_Dte().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                              //Natural: ASSIGN ADDR.CNTRCT-INVRSE-DTE := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
        pdaFcpaaddr.getAddr_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                          //Natural: ASSIGN ADDR.PYMNT-PRCSS-SEQ-NBR := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
        DbsUtil.callnat(Fcpnadd2.class , getCurrentProcessState(), pdaFcpaaddr.getAddr_Work_Fields(), pdaFcpaaddr.getAddr());                                             //Natural: CALLNAT 'FCPNADD2' USING ADDR-WORK-FIELDS ADDR
        if (condition(Global.isEscape())) return;
    }
    private void sub_Write_Cntrl_Record() throws Exception                                                                                                                //Natural: WRITE-CNTRL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpacntl.getCntl_Cntl_Orgn_Cde().setValue("AP");                                                                                                               //Natural: ASSIGN CNTL.CNTL-ORGN-CDE := 'AP'
        pdaFcpacntl.getCntl_Cntl_Check_Dte().setValue(Global.getDATX());                                                                                                  //Natural: ASSIGN CNTL.CNTL-CHECK-DTE := *DATX
        pdaFcpacntl.getCntl_Cntl_Invrse_Dte().compute(new ComputeParameters(false, pdaFcpacntl.getCntl_Cntl_Invrse_Dte()), DbsField.subtract(100000000,                   //Natural: ASSIGN CNTL.CNTL-INVRSE-DTE := 100000000 - *DATN
            Global.getDATN()));
        pdaFcpacntl.getCntl_Cntl_Occur_Cnt().setValue(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur());                                                                       //Natural: ASSIGN CNTL.CNTL-OCCUR-CNT := #MAX-OCCUR
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO #MAX-OCCUR
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur())); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpacntl.getCntl_Cntl_Occur_Num().getValue(pnd_Ws_Pnd_I).setValue(pnd_Ws_Pnd_I);                                                                           //Natural: ASSIGN CNTL.CNTL-OCCUR-NUM ( #I ) := #I
            pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_I));                     //Natural: ASSIGN CNTL.CNTL-PYMNT-CNT ( #I ) := #FCPAACUM.#PYMNT-CNT ( #I )
            pdaFcpacntl.getCntl_Cntl_Rec_Cnt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN CNTL.CNTL-REC-CNT ( #I ) := #FCPAACUM.#REC-CNT ( #I )
            pdaFcpacntl.getCntl_Cntl_Gross_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_I));                     //Natural: ASSIGN CNTL.CNTL-GROSS-AMT ( #I ) := #FCPAACUM.#SETTL-AMT ( #I )
            pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I));                   //Natural: ASSIGN CNTL.CNTL-NET-AMT ( #I ) := #FCPAACUM.#NET-PYMNT-AMT ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getWorkFiles().write(15, false, pdaFcpacntl.getCntl());                                                                                                           //Natural: WRITE WORK FILE 15 CNTL
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1).setValue(true);                                                                                         //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1 ) := #FCPACRPT.#TRUTH-TABLE ( 15 ) := #FCPACRPT.#TRUTH-TABLE ( 19 ) := #FCPACRPT.#TRUTH-TABLE ( 20 ) := #FCPACRPT.#TRUTH-TABLE ( 21 ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(15).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(19).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(20).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(21).setValue(true);
        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO #MAX-OCCUR
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur())); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                             //Natural: IF #FCPACRPT.#TRUTH-TABLE ( #I )
            {
                getReports().display(15, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",                           //Natural: DISPLAY ( 15 ) ( HC = R ) 10T '/DESCRIPTION' #PYMNT-TYPE-DESC ( #I ) ( HC = L ) 'PYMNT/COUNT' #FCPAACUM.#PYMNT-CNT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 'RECORD/COUNT' #FCPAACUM.#REC-CNT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 'GROSS/AMOUNT' #FCPAACUM.#SETTL-AMT ( #I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ ) 'NET/AMOUNT' #FCPAACUM.#NET-PYMNT-AMT ( #I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ )
                		ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
                    "PYMNT/COUNT",
                		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"RECORD/COUNT",
                		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"GROSS/AMOUNT",
                		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),
                    "NET/AMOUNT",
                		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Amt_Error() throws Exception                                                                                                                         //Natural: AMT-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "***",new TabSetting(25),"PAYMENT AND ACTUAL FUND AMOUNTS DO NOT AGREE",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN    :",pnd_Ws_Pnd_Cntrct_Orgn_Cde,new  //Natural: WRITE '***' 25T 'PAYMENT AND ACTUAL FUND AMOUNTS DO NOT AGREE' 77T '***' / '***' 25T 'ORIGIN    :' #WS.#CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'PPCN      :' #WS.#CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'SEQUENCE# :' #WS.#PYMNT-PRCSS-SEQ-NUM 77T '***' / '***' 25T 'PYMNT AMT :' #PYMNT-AMT 77T '***' / '***' 25T 'NET AMTS  :' #DET-AMTS 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PPCN      :",pnd_Ws_Pnd_Cntrct_Ppcn_Nbr,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"SEQUENCE# :",pnd_Ws_Pnd_Pymnt_Prcss_Seq_Num,new 
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PYMNT AMT :",pnd_Ws_Pnd_Pymnt_Amt, new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new TabSetting(77),"***",NEWLINE,"***",new 
            TabSetting(25),"NET AMTS  :",pnd_Ws_Pnd_Det_Amts, new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new TabSetting(77),"***");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.terminate(50);  if (true) return;                                                                                                                         //Natural: TERMINATE 50
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Control_Totals() throws Exception                                                                                                              //Natural: ACCUM-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  #MAX-OCCUR
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().greaterOrEqual(1) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().lessOrEqual(50)))                  //Natural: IF #ACCUM-OCCUR = 1 THRU 50
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                     //Natural: ASSIGN #@@MAX-FUND := WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        short decideConditionsMet1209 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPAACUM.#NEW-PYMNT-IND
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                  //Natural: ADD 1 TO #FCPAACUM.#PYMNT-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 3 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(3).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Cntrct_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#CNTRCT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 4 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(4).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dvdnd_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DVDND-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 5 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(5).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dci_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DCI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DCI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 6 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(6).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dpi_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DPI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DPI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#NET-PYMNT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 8 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(8).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Fdrl_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#FDRL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 9 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(9).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_State_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#STATE-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 10 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(10).equals(true)))
        {
            decideConditionsMet1209++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Local_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#LOCAL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1209 > 0))
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                    //Natural: ADD 1 TO #FCPAACUM.#REC-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1209 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void atBreakEventRead_Pymnt() throws Exception {atBreakEventRead_Pymnt(false);}
    private void atBreakEventRead_Pymnt(boolean endOfData) throws Exception
    {
        boolean pnd_Ws_Pnd_Break_IndIsBreak = pnd_Ws_Pnd_Break_Ind.isBreak(endOfData);
        if (condition(pnd_Ws_Pnd_Break_IndIsBreak))
        {
            //*  CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                if (condition(pnd_Ws_Pnd_Pymnt_Amt.notEquals(pnd_Ws_Pnd_Det_Amts)))                                                                                       //Natural: IF #PYMNT-AMT NE #DET-AMTS
                {
                    pnd_Ws_Pnd_Cntrct_Orgn_Cde.setValue(rEAD_PYMNTCntrct_Orgn_CdeOld);                                                                                    //Natural: ASSIGN #WS.#CNTRCT-ORGN-CDE := OLD ( FCP-CONS-PYMNT.CNTRCT-ORGN-CDE )
                    pnd_Ws_Pnd_Cntrct_Ppcn_Nbr.setValue(rEAD_PYMNTCntrct_Ppcn_NbrOld);                                                                                    //Natural: ASSIGN #WS.#CNTRCT-PPCN-NBR := OLD ( FCP-CONS-PYMNT.CNTRCT-PPCN-NBR )
                    pnd_Ws_Pnd_Pymnt_Prcss_Seq_Num.setValue(rEAD_PYMNTPymnt_Prcss_Seq_NumOld);                                                                            //Natural: ASSIGN #WS.#PYMNT-PRCSS-SEQ-NUM := OLD ( FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM )
                                                                                                                                                                          //Natural: PERFORM AMT-ERROR
                    sub_Amt_Error();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  NOT EOF
                if (condition(pnd_Ws_Pnd_Break_Ind.notEquals(rEAD_PYMNTPnd_Break_IndOld)))                                                                                //Natural: IF #BREAK-IND NE OLD ( #BREAK-IND )
                {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                    sub_Init_New_Pymnt();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(15, "PS=58 LS=132 ZP=ON");

        getReports().write(15, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"Consolidated Payment System",new TabSetting(119),"PAGE :",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(38),"Annuity Payments Cancel and Redraw extract control report",new TabSetting(119),"REPORT: RPT15",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(15, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",
        		ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"PYMNT/COUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"RECORD/COUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"GROSS/AMOUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),"NET/AMOUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
    }
    private void CheckAtStartofData915() throws Exception
    {
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAtStartOfData()))
        {
            //*  CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                sub_Init_New_Pymnt();
                if (condition(Global.isEscape())) {return;}
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
