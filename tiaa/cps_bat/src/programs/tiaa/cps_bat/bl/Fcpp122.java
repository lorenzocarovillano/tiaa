/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:07 PM
**        * FROM NATURAL PROGRAM : Fcpp122
************************************************************
**        * FILE NAME            : Fcpp122.java
**        * CLASS NAME           : Fcpp122
**        * INSTANCE NAME        : Fcpp122
************************************************************
************************************************************************
* PROGRAM  : FCPP122
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS PAYMNET WARRANT EXTRACT
* CREATED  : 10/04/94
* FUNCTION : THIS PROGRAM READS CHECK FILE, ATTACHES INFORMATION FROM
*            THE FCP-CONS-LEDGER FILE AND CREATES AN EXTRACT FOR
*            WARRANT REPORT.
* UPDATES  : IF LEDGER RECORD IS NOT FOUND, REJECT THE DATA & TERMINATE
*            AT THE END OF THE PROGRAM
*
* 06/23/1998 - RIAD LOUTFI - ADDED CODE TO CAPTURE BOTH PAYMENT AND
*              SETTLEMENT TYPE.  TO IDENTIFY CLASSIC / ROTH IRAS.
*
* 03/12/99   - R. CARREON  - RESTOW DUE TO CHANGE IN FCPL378
* 06/03/99   - R. CARREON  - POPULATE NEW FIELD ##INV-OVR-PAY-DED-AMT
*                          - FOR OVER PAYMENT PER FUND IN FCPL123
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
* 09/11/00 : ALTHEA A. YOUNG
*          - RE-COMPILED DUE TO FCPL123.
*    11/02 : R. CARREON   STOW - EGTRRA CHANGES IN LDA
* 04/2017  : JJG PIN EXPANSION
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp122 extends BLNatBase
{
    // Data Areas
    private LdaFcpl378 ldaFcpl378;
    private PdaFcpaldgr pdaFcpaldgr;
    private LdaFcpl123 ldaFcpl123;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Output_Ind;
    private DbsField pnd_Ws_Pnd_Terminate_Ind;
    private DbsField pnd_Ws_Pnd_Break_Ind;

    private DbsGroup pnd_Ws_Pnd_Record_Counts;
    private DbsField pnd_Ws_Pnd_Records_All;
    private DbsField pnd_Ws_Pnd_Records_10;
    private DbsField pnd_Ws_Pnd_Records_20;
    private DbsField pnd_Ws_Pnd_Records_30;
    private DbsField pnd_Ws_Pnd_Records_Other;
    private DbsField pnd_Ws_Pnd_Ledger_Records_Read;
    private DbsField pnd_Ws_Pnd_Ledger_Read;
    private DbsField pnd_Ws_Pnd_Records_Written;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Sub;
    private DbsField pnd_Ws_Pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl378 = new LdaFcpl378();
        registerRecord(ldaFcpl378);
        localVariables = new DbsRecord();
        pdaFcpaldgr = new PdaFcpaldgr(localVariables);
        ldaFcpl123 = new LdaFcpl123();
        registerRecord(ldaFcpl123);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Output_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Output_Ind", "#OUTPUT-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Terminate_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate_Ind", "#TERMINATE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Break_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Break_Ind", "#BREAK-IND", FieldType.PACKED_DECIMAL, 7);

        pnd_Ws_Pnd_Record_Counts = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Record_Counts", "#RECORD-COUNTS");
        pnd_Ws_Pnd_Records_All = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_All", "#RECORDS-ALL", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 3));
        pnd_Ws_Pnd_Records_10 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_10", "#RECORDS-10", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 3));
        pnd_Ws_Pnd_Records_20 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_20", "#RECORDS-20", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 3));
        pnd_Ws_Pnd_Records_30 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_30", "#RECORDS-30", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 3));
        pnd_Ws_Pnd_Records_Other = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_Other", "#RECORDS-OTHER", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 3));
        pnd_Ws_Pnd_Ledger_Records_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ledger_Records_Read", "#LEDGER-RECORDS-READ", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Ledger_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ledger_Read", "#LEDGER-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Records_Written = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Records_Written", "#RECORDS-WRITTEN", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_I = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Sub = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_X = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_X", "#X", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl378.initializeValues();
        ldaFcpl123.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Terminate_Ind.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp122() throws Exception
    {
        super("Fcpp122");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT PS = 23 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'WARRANT REPORTING EXTRACT' 120T 'REPORT: RPT1' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pdaFcpaldgr.getLedger_Work_Fields_Ledger_Abend_Ind().setValue(true);                                                                                              //Natural: MOVE TRUE TO LEDGER-ABEND-IND
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-10-DETAIL
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_10_Detail())))
        {
            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            if (condition(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(10)))                                                                              //Natural: IF #KEY-REC-LVL-# = 10
            {
                pnd_Ws_Pnd_Break_Ind.nadd(1);                                                                                                                             //Natural: ADD 1 TO #BREAK-IND
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            short decideConditionsMet436 = 0;                                                                                                                             //Natural: AT BREAK OF #BREAK-IND;//Natural: DECIDE ON FIRST VALUE OF #KEY-REC-LVL-#;//Natural: VALUE 10
            if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(10))))
            {
                decideConditionsMet436++;
                short decideConditionsMet438 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE NE 'SS'
                if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde().notEquals("SS")))
                {
                    decideConditionsMet438++;
                    pnd_Ws_Pnd_Output_Ind.setValue(false);                                                                                                                //Natural: MOVE FALSE TO #OUTPUT-IND
                    //*  LEON 'CN SN' 08-05-99
                    //*  JWO 2010-11
                    pnd_Ws_Pnd_I.setValue(2);                                                                                                                             //Natural: MOVE 2 TO #I
                }                                                                                                                                                         //Natural: WHEN #REC-TYPE-10-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
                else if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") 
                    || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
                    || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
                    || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))
                {
                    decideConditionsMet438++;
                    pnd_Ws_Pnd_Output_Ind.setValue(false);                                                                                                                //Natural: MOVE FALSE TO #OUTPUT-IND
                    pnd_Ws_Pnd_I.setValue(3);                                                                                                                             //Natural: MOVE 3 TO #I
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Ws_Pnd_Output_Ind.setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #OUTPUT-IND
                    pnd_Ws_Pnd_I.setValue(1);                                                                                                                             //Natural: MOVE 1 TO #I
                    ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pymnt_Addr().reset();                                                                                           //Natural: RESET #PYMNT-ADDR PYMNT-INSTMT
                    ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Instmt().reset();
                    ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Key().setValuesByName(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10());                       //Natural: MOVE BY NAME #RECORD-TYPE-10 TO #WARRANT-KEY
                                                                                                                                                                          //Natural: PERFORM PROCESS-LEDGER-RECORD
                    sub_Process_Ledger_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PROCESS-10-RECORD
                    sub_Process_10_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Ws_Pnd_Records_10.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-10 ( #I )
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(20))))
            {
                decideConditionsMet436++;
                pnd_Ws_Pnd_Records_20.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-20 ( #I )
                if (condition(pnd_Ws_Pnd_Output_Ind.getBoolean()))                                                                                                        //Natural: IF #OUTPUT-IND
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-20-RECORD
                    sub_Process_20_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(30))))
            {
                decideConditionsMet436++;
                pnd_Ws_Pnd_Records_30.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-30 ( #I )
                if (condition(pnd_Ws_Pnd_Output_Ind.getBoolean()))                                                                                                        //Natural: IF #OUTPUT-IND
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-30-RECORD
                    sub_Process_30_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Records_Other.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                  //Natural: ADD 1 TO #RECORDS-OTHER ( #I )
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Records_10.getValue(1).nadd(pnd_Ws_Pnd_Records_10.getValue(2,":",3));                                                                                  //Natural: ADD #RECORDS-10 ( 2:3 ) TO #RECORDS-10 ( 1 )
        pnd_Ws_Pnd_Records_20.getValue(1).nadd(pnd_Ws_Pnd_Records_20.getValue(2,":",3));                                                                                  //Natural: ADD #RECORDS-20 ( 2:3 ) TO #RECORDS-20 ( 1 )
        pnd_Ws_Pnd_Records_30.getValue(1).nadd(pnd_Ws_Pnd_Records_30.getValue(2,":",3));                                                                                  //Natural: ADD #RECORDS-30 ( 2:3 ) TO #RECORDS-30 ( 1 )
        pnd_Ws_Pnd_Records_Other.getValue(1).nadd(pnd_Ws_Pnd_Records_Other.getValue(2,":",3));                                                                            //Natural: ADD #RECORDS-OTHER ( 2:3 ) TO #RECORDS-OTHER ( 1 )
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Records_All.getValue(pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_Ws_Pnd_Records_All.getValue(pnd_Ws_Pnd_I)), pnd_Ws_Pnd_Records_10.getValue(pnd_Ws_Pnd_I).add(pnd_Ws_Pnd_Records_20.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_30.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_Other.getValue(pnd_Ws_Pnd_I))); //Natural: ADD #RECORDS-10 ( #I ) #RECORDS-20 ( #I ) #RECORDS-30 ( #I ) #RECORDS-OTHER ( #I ) GIVING #RECORDS-ALL ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(14),"COUNTERS AT END OF PROGRAM:",NEWLINE,new TabSetting(17),"RECORDS READ:",NEWLINE,new TabSetting(17),"................ALL:",pnd_Ws_Pnd_Records_All.getValue(1),  //Natural: WRITE ( 1 ) 14T 'COUNTERS AT END OF PROGRAM:' / 17T 'RECORDS READ:' / 17T '................ALL:' #RECORDS-ALL ( 1 ) / 17T '............TYPE 10:' #RECORDS-10 ( 1 ) / 17T '............TYPE 20:' #RECORDS-20 ( 1 ) / 17T '............TYPE 30:' #RECORDS-30 ( 1 ) / 17T '..............OTHER:' #RECORDS-OTHER ( 1 ) // 17T 'RECORDS REJECTED:' / 17T '........NOT SINGLE SUM:' / 17T '................ALL:' #RECORDS-ALL ( 2 ) / 17T '............TYPE 10:' #RECORDS-10 ( 2 ) / 17T '............TYPE 20:' #RECORDS-20 ( 2 ) / 17T '............TYPE 30:' #RECORDS-30 ( 2 ) / 17T '..............OTHER:' #RECORDS-OTHER ( 2 ) / 17T '........CANCEL & STOP :' / 17T '................ALL:' #RECORDS-ALL ( 3 ) / 17T '............TYPE 10:' #RECORDS-10 ( 3 ) / 17T '............TYPE 20:' #RECORDS-20 ( 3 ) / 17T '............TYPE 30:' #RECORDS-30 ( 3 ) / 17T '..............OTHER:' #RECORDS-OTHER ( 3 ) // 17T 'LEDGER RECORDS READ:' #LEDGER-RECORDS-READ '(FOR ACCEPTED RECORDS FROM FCP-CONS-LEDGER FILE)' / 17T '   (ACTUAL LEDGERS):' #LEDGER-READ // 17T 'RECORDS WRITTEN....:' #RECORDS-WRITTEN //// 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************'
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"............TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"............TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"............TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..............OTHER:",pnd_Ws_Pnd_Records_Other.getValue(1), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"RECORDS REJECTED:",NEWLINE,new TabSetting(17),"........NOT SINGLE SUM:",NEWLINE,new TabSetting(17),"................ALL:",pnd_Ws_Pnd_Records_All.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"............TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"............TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"............TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..............OTHER:",pnd_Ws_Pnd_Records_Other.getValue(2), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........CANCEL & STOP :",NEWLINE,new TabSetting(17),"................ALL:",pnd_Ws_Pnd_Records_All.getValue(3), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"............TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"............TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"............TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(3), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..............OTHER:",pnd_Ws_Pnd_Records_Other.getValue(3), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"LEDGER RECORDS READ:",pnd_Ws_Pnd_Ledger_Records_Read, new ReportEditMask ("-Z,ZZZ,ZZ9"),"(FOR ACCEPTED RECORDS FROM FCP-CONS-LEDGER FILE)",NEWLINE,new 
            TabSetting(17),"   (ACTUAL LEDGERS):",pnd_Ws_Pnd_Ledger_Read, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"RECORDS WRITTEN....:",pnd_Ws_Pnd_Records_Written, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new 
            TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************");
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Terminate_Ind.getBoolean()))                                                                                                             //Natural: IF #TERMINATE-IND
        {
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-IF
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-LEDGER-RECORD
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-10-RECORD
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-20-RECORD
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-30-RECORD
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-OVR-PAY-DED
        //* ************************************
    }
    private void sub_Process_Ledger_Record() throws Exception                                                                                                             //Natural: PROCESS-LEDGER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        pdaFcpaldgr.getLedger_Cntrct_Orgn_Cde().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde());                                                         //Natural: MOVE #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE TO LEDGER.CNTRCT-ORGN-CDE
        pdaFcpaldgr.getLedger_Cntrct_Ppcn_Nbr().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Ppcn_Nbr());                                                         //Natural: MOVE #REC-TYPE-10-DETAIL.CNTRCT-PPCN-NBR TO LEDGER.CNTRCT-PPCN-NBR
        pdaFcpaldgr.getLedger_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Prcss_Seq_Nbr());                                                 //Natural: MOVE #REC-TYPE-10-DETAIL.PYMNT-PRCSS-SEQ-NBR TO LEDGER.PYMNT-PRCSS-SEQ-NBR
        pdaFcpaldgr.getLedger_Cntl_Check_Dte().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Dte());                                                          //Natural: MOVE #REC-TYPE-10-DETAIL.PYMNT-CHECK-DTE TO LEDGER.CNTL-CHECK-DTE
        pdaFcpaldgr.getLedger_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde());                             //Natural: MOVE #REC-TYPE-10-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE TO LEDGER.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        DbsUtil.callnat(Fcpnldgr.class , getCurrentProcessState(), pdaFcpaldgr.getLedger_Work_Fields(), pdaFcpaldgr.getLedger());                                         //Natural: CALLNAT 'FCPNLDGR' USING LEDGER-WORK-FIELDS LEDGER
        if (condition(Global.isEscape())) return;
        ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().setValue(2);                                                                                                  //Natural: MOVE 2 TO #RECORD-TYPE
        pnd_Ws_Pnd_Ledger_Records_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #LEDGER-RECORDS-READ
        pnd_Ws_Pnd_Ledger_Read.nadd(pdaFcpaldgr.getLedger_C_Inv_Ledgr());                                                                                                 //Natural: ADD C-INV-LEDGR TO #LEDGER-READ
        FOR02:                                                                                                                                                            //Natural: FOR #SUB = 1 TO C-INV-LEDGR
        for (pnd_Ws_Pnd_Sub.setValue(1); condition(pnd_Ws_Pnd_Sub.lessOrEqual(pdaFcpaldgr.getLedger_C_Inv_Ledgr())); pnd_Ws_Pnd_Sub.nadd(1))
        {
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Cde().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_Sub));                                 //Natural: MOVE LEDGER.INV-ACCT-CDE ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-CDE
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Nbr().getValue(pnd_Ws_Pnd_Sub));                     //Natural: MOVE LEDGER.INV-ACCT-LEDGR-NBR ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-NBR
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Amt().getValue(pnd_Ws_Pnd_Sub));                     //Natural: MOVE LEDGER.INV-ACCT-LEDGR-AMT ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-AMT
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Ind().getValue(pnd_Ws_Pnd_Sub));                     //Natural: MOVE LEDGER.INV-ACCT-LEDGR-IND ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-IND
            getWorkFiles().write(2, true, ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Grp1(), ldaFcpl123.getPnd_Warrant_Extract_Pnd_Ledger_Record());                   //Natural: WRITE WORK FILE 2 VARIABLE #WARRANT-GRP1 #LEDGER-RECORD
            pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                           //Natural: ADD 1 TO #RECORDS-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl().equals(getZero())))                                                                                       //Natural: IF C-INV-LEDGR-OVRFL = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Ledger_Read.nadd(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl());                                                                                       //Natural: ADD C-INV-LEDGR-OVRFL TO #LEDGER-READ
            FOR03:                                                                                                                                                        //Natural: FOR #SUB = 1 TO C-INV-LEDGR-OVRFL
            for (pnd_Ws_Pnd_Sub.setValue(1); condition(pnd_Ws_Pnd_Sub.lessOrEqual(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl())); pnd_Ws_Pnd_Sub.nadd(1))
            {
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Cde().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Cde_Ovrfl().getValue(pnd_Ws_Pnd_Sub));                       //Natural: MOVE LEDGER.INV-ACCT-CDE-OVRFL ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-CDE
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Nbr_Ovrfl().getValue(pnd_Ws_Pnd_Sub));           //Natural: MOVE LEDGER.INV-ACCT-LEDGR-NBR-OVRFL ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-NBR
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Amt_Ovrfl().getValue(pnd_Ws_Pnd_Sub));           //Natural: MOVE LEDGER.INV-ACCT-LEDGR-AMT-OVRFL ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-AMT
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Ind_Ovrfl().getValue(pnd_Ws_Pnd_Sub));           //Natural: MOVE LEDGER.INV-ACCT-LEDGR-IND-OVRFL ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-IND
                getWorkFiles().write(2, true, ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Grp1(), ldaFcpl123.getPnd_Warrant_Extract_Pnd_Ledger_Record());               //Natural: WRITE WORK FILE 2 VARIABLE #WARRANT-GRP1 #LEDGER-RECORD
                pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                       //Natural: ADD 1 TO #RECORDS-WRITTEN
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_10_Record() throws Exception                                                                                                                 //Natural: PROCESS-10-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().setValue(1);                                                                                                  //Natural: MOVE 1 TO #RECORD-TYPE
        ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pymnt_Addr().setValuesByName(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10());                                    //Natural: MOVE BY NAME #RECORD-TYPE-10 TO #PYMNT-ADDR
        //*  MOVE #REC-TYPE-10-DETAIL.CNTRCT-TYPE-CDE
        //*    TO #WARRANT-EXTRACT.CNTRCT-TYPE-CDE                    /* 09-11-2000
    }
    private void sub_Process_20_Record() throws Exception                                                                                                                 //Natural: PROCESS-20-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1());                              //Natural: MOVE #REC-TYPE-10-DET-1 TO #REC-TYPE-20-DET-1
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2());                              //Natural: MOVE #REC-TYPE-10-DET-2 TO #REC-TYPE-20-DET-2
        ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct().setValue(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd());                                                 //Natural: MOVE #KEY-REC-OCCUR-# TO C-INV-ACCT
        ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Info().getValue(ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct()).setValuesByName(ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct()); //Natural: MOVE BY NAME #REC-TYPE-20-DETAIL.#INV-ACCT TO #WARRANT-EXTRACT.#INV-INFO ( C-INV-ACCT )
    }
    private void sub_Process_30_Record() throws Exception                                                                                                                 //Natural: PROCESS-30-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_1().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1());                              //Natural: MOVE #REC-TYPE-10-DET-1 TO #REC-TYPE-30-DET-1
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_2().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2());                              //Natural: MOVE #REC-TYPE-10-DET-2 TO #REC-TYPE-30-DET-2
        ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Nme_And_Addr_Grp().getValue(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd()).setValuesByName(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp()); //Natural: MOVE BY NAME #REC-TYPE-30-DETAIL.#PYMNT-NME-AND-ADDR-GRP TO #WARRANT-EXTRACT.PYMNT-NME-AND-ADDR-GRP ( #KEY-REC-OCCUR-# )
        ldaFcpl123.getPnd_Warrant_Extract().setValuesByName(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2());                                              //Natural: MOVE BY NAME #REC-TYPE-30-DETAIL.#RECORD-TYPE-30-GRP2 TO #WARRANT-EXTRACT
    }
    private void sub_Compute_Ovr_Pay_Ded() throws Exception                                                                                                               //Natural: COMPUTE-OVR-PAY-DED
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #X 1 C-INV-ACCT
        for (pnd_Ws_Pnd_X.setValue(1); condition(pnd_Ws_Pnd_X.lessOrEqual(ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct())); pnd_Ws_Pnd_X.nadd(1))
        {
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt().getValue(pnd_Ws_Pnd_X).compute(new ComputeParameters(false, ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt().getValue(pnd_Ws_Pnd_X)),  //Natural: ASSIGN ##INV-ACCT-OVR-PAY-DED-AMT ( #X ) := #WARRANT-EXTRACT.INV-ACCT-SETTL-AMT ( #X ) + #WARRANT-EXTRACT.INV-ACCT-DVDND-AMT ( #X ) + #WARRANT-EXTRACT.INV-ACCT-DCI-AMT ( #X ) + #WARRANT-EXTRACT.INV-ACCT-DPI-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-NET-PYMNT-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-FDRL-TAX-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-STATE-TAX-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-LOCAL-TAX-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-EXP-AMT ( #X )
                ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_X).add(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_X)).add(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Pnd_X)).add(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Pnd_X)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Ws_Pnd_Break_IndIsBreak = pnd_Ws_Pnd_Break_Ind.isBreak(endOfData);
        if (condition(pnd_Ws_Pnd_Break_IndIsBreak))
        {
            if (condition(pnd_Ws_Pnd_Output_Ind.getBoolean()))                                                                                                            //Natural: IF #OUTPUT-IND
            {
                                                                                                                                                                          //Natural: PERFORM COMPUTE-OVR-PAY-DED
                sub_Compute_Ovr_Pay_Ded();
                if (condition(Global.isEscape())) {return;}
                getWorkFiles().write(2, true, ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Grp1(), ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Detail().getValue("*"), //Natural: WRITE WORK FILE 2 VARIABLE #WARRANT-GRP1 #WARRANT-DETAIL ( * ) #INV-DETAIL ( 1:C-INV-ACCT )
                    ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Detail().getValue(1,":",ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct()));
                pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                       //Natural: ADD 1 TO #RECORDS-WRITTEN
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(55),"WARRANT REPORTING EXTRACT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
}
