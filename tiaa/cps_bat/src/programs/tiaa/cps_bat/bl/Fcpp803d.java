/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:52 PM
**        * FROM NATURAL PROGRAM : Fcpp803d
************************************************************
**        * FILE NAME            : Fcpp803d.java
**        * CLASS NAME           : Fcpp803d
**        * INSTANCE NAME        : Fcpp803d
************************************************************
************************************************************************
* PROGRAM  : FCPP803D
* SYSTEM   : CPS
* TITLE    : IAR RESTRUCTURE
* FUNCTION : "AP","NZ","AL" ANNUITANT STMNTS/CHKS 1405
*          : "AP" ANNUITANT STMNTS/CHKS 2200 SERIES, 2205 PROC
*          : "CANCELS, STOPS,& REDRAWS.
* HISTORY  : LANDRUM - CLONE FROM FCPP803C FOR PAYEE MATCH PROJECT
*          : 5/15/08 - AER - ROTH-MAJOR1 - CHANGED FCPA800 TO FCPA800D
*          : 6/02/08 - AER - ROTH-MAJOR1 - CHANGED FCPN8032 TO FCPN8031
* 07/06/2009 :J.OSTEEN - MODIFIED TO PRINT ROTH MESSAGES ON CHECKS
* 12/2015    :J.OSTEEN - RE-COMPILED TO PICK PDA FCPA801B
*  4/2017  :JJG - RESTOW PIN EXPANSION
*
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES (INCLUDING CANCELS, STOPS & REDRAWS). THIS IS
* EXCLUSIVE TO THE 1400 STREAM AS OTHER CPS STREAMS (1500,1600,& 2200)
* HAVE BEEN ENGINEERED TO PROCESS THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp803d extends BLNatBase
{
    // Data Areas
    private PdaFcpaacum pdaFcpaacum;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpa800d pdaFcpa800d;
    private PdaFcpa801b pdaFcpa801b;
    private PdaFcpa803c pdaFcpa803c;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa803a pdaFcpa803a;
    private PdaFcpa803h pdaFcpa803h;
    private PdaFcpa803l pdaFcpa803l;
    private LdaFcplbar1 ldaFcplbar1;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;

    private DbsGroup pnd_Key_Pnd_Key_Detail;
    private DbsField pnd_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Key_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Key_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Run_Type;
    private DbsField pnd_Ws_Pnd_File_Type;
    private DbsField pnd_Ws_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Pymnt_Check_Nbr_N10;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Monthly_Restart;
    private DbsField pnd_Pymnt_Check_Nbr_N10;

    private DbsGroup pnd_Pymnt_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N3;
    private DbsField pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Next_Check_Nbr;
    private DbsField pnd_Ws_Next_Check_Prefix;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_KeyOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaacum = new PdaFcpaacum(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        pdaFcpa801b = new PdaFcpa801b(localVariables);
        pdaFcpa803c = new PdaFcpa803c(localVariables);
        pdaFcpa110 = new PdaFcpa110(localVariables);
        pdaFcpa803 = new PdaFcpa803(localVariables);
        pdaFcpa803a = new PdaFcpa803a(localVariables);
        pdaFcpa803h = new PdaFcpa803h(localVariables);
        pdaFcpa803l = new PdaFcpa803l(localVariables);
        ldaFcplbar1 = new LdaFcplbar1();
        registerRecord(ldaFcplbar1);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);

        // Local Variables
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 31);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);

        pnd_Key_Pnd_Key_Detail = pnd_Key__R_Field_1.newGroupInGroup("pnd_Key_Pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Cntrct_Orgn_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Cntrct_Invrse_Dte = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Key_Cntrct_Cmbn_Nbr = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Key_Pymnt_Prcss_Seq_Num = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Run_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 8);
        pnd_Ws_Pnd_File_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_File_Type", "#FILE-TYPE", FieldType.STRING, 8);
        pnd_Ws_Cntrct_Hold_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Ws_Pymnt_Check_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pymnt_Check_Nbr_N10 = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr_N10", "PYMNT-CHECK-NBR-N10", FieldType.PACKED_DECIMAL, 10);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Monthly_Restart = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Monthly_Restart", "#MONTHLY-RESTART", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Pymnt_Check_Nbr_N10", "#PYMNT-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Pymnt_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Pymnt_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Pymnt_Check_Nbr_N10);
        pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N3 = pnd_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N3", 
            "#PYMNT-CHECK-NBR-N3", FieldType.NUMERIC, 3);
        pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7 = pnd_Pymnt_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7", 
            "#PYMNT-CHECK-NBR-N7", FieldType.NUMERIC, 7);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_3", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Next_Check_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Nbr", "#WS-NEXT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Check_Prefix = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Prefix", "#WS-NEXT-CHECK-PREFIX", FieldType.NUMERIC, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Key_OLD", "Pnd_Key_OLD", FieldType.STRING, 31);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcplbar1.initializeValues();
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();

        localVariables.reset();
        pnd_Ws_Cntrct_Hold_Cde.setInitialValue("0000");
        pnd_Ws_Pnd_Monthly_Restart.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp803d() throws Exception
    {
        super("Fcpp803d");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 15 ) LS = 133 PS = 58 ZP = ON;//Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 21T #FCPACRPT.#TITLE 68T 'REPORT: RPT0' / 36T #RUN-TYPE //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARM
        sub_Process_Input_Parm();
        if (condition(Global.isEscape())) {return;}
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 ) #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).setValue(true);
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #CHECK-SORT-FIELDS PYMNT-ADDR-INFO #CHECK-FIELDS INV-INFO ( * )
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa801b.getPnd_Check_Fields(), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            CheckAtStartofData798();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            //*  RESTART (MONTHLY RUN)
            //*    IF #MONTHLY-RESTART
            //*  WRITE ' #MONTHLY-RESTART-IN' #MONTHLY-RESTART(EM=T/F)
            //*       IF #FCPL876.PYMNT-CHECK-SEQ-NBR >
            //*           WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SEQ-NBR
            //*         IF     #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER
            //*             NE BAR-ENV-INTEGRITY-COUNTER
            //*             OR #WS.CNTRCT-HOLD-CDE
            //*             NE #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE
            //*           ADD  1                          TO #BAR-ENV-ID-NUM
            //*          ADD  1                          TO #BARCODE-LDA.#BAR-ENVELOPES
            //*          PERFORM DETERMINE-STMNT-TYPE
            //*          #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER :=
            //*            BAR-ENV-INTEGRITY-COUNTER
            //*        END-IF
            //*        #WS.CNTRCT-HOLD-CDE        := #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE
            //*        PERFORM WRITE-OUTPUT-FILE
            //*        PERFORM ACCUM-CONTROL-TOTALS
            //*        #FCPAACUM.#NEW-PYMNT-IND           := FALSE
            //*        ESCAPE TOP
            //*      END-IF
            //*      MOVE FALSE                         TO #MONTHLY-RESTART
            //*      IF BAR-ENV-INTEGRITY-COUNTER = 0
            //*        #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER  := 9
            //*      ELSE
            //*        #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER  :=
            //*         BAR-ENV-INTEGRITY-COUNTER - 1
            //*      END-IF
            //*      PERFORM START-OF-PROCESSING
            //*    END-IF
            //*  END OF RESTART
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                   //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #KEY-DETAIL
                                                                                                                                                                          //Natural: PERFORM CALC-SIMPLEX-DUPLEX
            sub_Calc_Simplex_Duplex();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                             //Natural: AT BREAK OF CNTRCT-PPCN-NBR;//Natural: AT BREAK OF CNTRCT-COMPANY-CDE;//Natural: AT BREAK OF CNTRCT-ANNTY-INS-TYPE;//Natural: AT BREAK OF #KEY;//Natural: AT BREAK OF #SIMPLEX;//Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))                                                                                       //Natural: IF #GLOBAL-PAY
            {
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr());                                     //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR := #FCPL876.PYMNT-CHECK-SCRTY-NBR
                //* RA
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().setValue(pnd_Ws_Pymnt_Check_Nbr);                                                                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR := #WS.PYMNT-CHECK-NBR
                pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr().setValue(pnd_Ws_Pymnt_Check_Nbr_N10);                                                                    //Natural: ASSIGN #CHECK-SORT-FIELDS.PYMNT-NBR := #WS.PYMNT-CHECK-NBR-N10
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
            sub_Accum_Control_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(false);                                                                                              //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := FALSE
            if (condition(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde().equals("0000") && pnd_Ws_Cntrct_Hold_Cde.notEquals("0000")))                             //Natural: IF #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE = '0000' AND #WS.CNTRCT-HOLD-CDE NE '0000'
            {
                ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                          //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().nadd(1);                                                                                                      //Natural: ADD 1 TO #RECORD-IN-PYMNT
            DbsUtil.callnat(Fcpnroth.class , getCurrentProcessState(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte(),  //Natural: CALLNAT 'FCPNROTH' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR WF-PYMNT-ADDR-GRP.CNTRCT-INVRSE-DTE WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NBR #FCPA803
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr(), pdaFcpa803.getPnd_Fcpa803());
            if (condition(Global.isEscape())) return;
            //*  IF #ROTH-IND
            //*    WRITE '************* ROTH ROTH ROTH ****************'
            //*  END-IF
            //* *  CALLNAT 'FCPN803B' /* RL
            //* *CALLNAT 'FCPN8032'   /* RL
            //*  AER
            //*  RA /* RL
            DbsUtil.callnat(Fcpn8031.class , getCurrentProcessState(), pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa110.getFcpa110(), pdaFcpa801b.getPnd_Check_Fields(),  //Natural: CALLNAT 'FCPN8031' USING #CHECK-SORT-FIELDS FCPA110 #CHECK-FIELDS WF-PYMNT-ADDR-GRP ( * ) #FCPA803 #FCPA803A #FCPA803H #FCPA803L #BARCODE-LDA
                pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue("*"), pdaFcpa803.getPnd_Fcpa803(), pdaFcpa803a.getPnd_Fcpa803a(), pdaFcpa803h.getPnd_Fcpa803h(), 
                pdaFcpa803l.getPnd_Fcpa803l(), ldaFcplbar1.getPnd_Barcode_Lda());
            if (condition(Global.isEscape())) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
            sub_Write_Output_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Cntrct_Hold_Cde.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde());                                                                      //Natural: ASSIGN #WS.CNTRCT-HOLD-CDE := #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE
            readWork01Pnd_KeyOld.setValue(pnd_Key);                                                                                                                       //Natural: AT END OF DATA;//Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-VOID-PAGE
            sub_Print_Void_Page();
            if (condition(Global.isEscape())) {return;}
            //*  RA /* RL ? USE 876 IN 1400 NOT UPDT HERE
                                                                                                                                                                          //Natural: PERFORM UPDATE-CHECK-NO
            sub_Update_Check_No();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().getBoolean()))                                                                                              //Natural: IF #FCPA803.#CSR-RUN
        {
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().nadd(1);                                                                                                          //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-NBR
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().nadd(1);                                                                                                    //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nadd(1);                                                                                                      //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SEQ-NBR
            getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"Counters etc, at end of program:",NEWLINE,"Next check  number...........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr(), //Natural: WRITE *PROGRAM *TIME 'Counters etc, at end of program:' / 'Next check  number...........:' #FCPL876.PYMNT-CHECK-NBR / 'Next EFT    number...........:' #FCPL876.PYMNT-CHECK-SCRTY-NBR / 'Next payment sequence number.:' #FCPL876.PYMNT-CHECK-SEQ-NBR
                NEWLINE,"Next EFT    number...........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr(),NEWLINE,"Next payment sequence number.:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());
            if (Global.isEscape()) return;
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Barcode().getValue("*").setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue("*"));                           //Natural: ASSIGN #FCPL876.#BAR-BARCODE ( * ) := #BARCODE-LDA.#BAR-BARCODE ( * )
            getWorkFiles().write(7, false, ldaFcpl876.getPnd_Fcpl876());                                                                                                  //Natural: WRITE WORK FILE 7 #FCPL876
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OUTPUT-FILE
        //* **********************************
        //* *******************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CHECK-NO
        //* **********************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-STMNT-TYPE
        //* *************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-SIMPLEX-DUPLEX
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-VOID-PAGE
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: START-OF-PROCESSING
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARM
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEXT-CHECK-FILE
        //* *********** RL ADDED MAY 6 2006 CHECK VS STMNT NBR DIFF ***************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DAILY-RUN-INFO
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-CHECK-NO
        //* *********** REMOVED MAY 6 2006 RL CHECK VS STMNT NBR DIFF *************
        //* *#PYMNT-CHECK-NBR-N10         := FCPA110.START-CHECK-NO
        //* *#FCPL876.PYMNT-CHECK-SEQ-NBR := FCPA110.START-SEQ-NO
        //* *#FCPL876.PYMNT-CHECK-SEQ-NBR        := FCPA110.START-SEQ-NO
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-HOLD-CONTROL-RECORD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* ***********************************************************************
        //*  COPYCODE   : FCPCACUM
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : "AP" - CONTROL ACCUMULATIONS.
        //* ***********************************************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-TOTALS
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Write_Output_File() throws Exception                                                                                                                 //Natural: WRITE-OUTPUT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Bar_Env_Integrity_Counter().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter());                            //Natural: ASSIGN BAR-ENV-INTEGRITY-COUNTER := #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))                                                                                           //Natural: IF #GLOBAL-PAY
        {
            getWorkFiles().write(9, true, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",                 //Natural: WRITE WORK FILE 9 VARIABLE PYMNT-ADDR-INFO INV-INFO ( 1:INV-ACCT-COUNT )
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().write(9, true, pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa801b.getPnd_Check_Fields(),  //Natural: WRITE WORK FILE 9 VARIABLE #CHECK-SORT-FIELDS PYMNT-ADDR-INFO #CHECK-FIELDS INV-INFO ( 1:INV-ACCT-COUNT )
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Check_No() throws Exception                                                                                                                   //Natural: UPDATE-CHECK-NO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pdaFcpa110.getFcpa110_Fcpa110_New_Check_Nbr().setValue(pnd_Pymnt_Check_Nbr_N10);                                                                                  //Natural: MOVE #PYMNT-CHECK-NBR-N10 TO FCPA110.FCPA110-NEW-CHECK-NBR
        pdaFcpa110.getFcpa110_Fcpa110_New_Sequence_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                       //Natural: MOVE #FCPL876.PYMNT-CHECK-SEQ-NBR TO FCPA110.FCPA110-NEW-SEQUENCE-NBR
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("UPDATE CHK SEQ");                                                                                              //Natural: MOVE 'UPDATE CHK SEQ' TO FCPA110.FCPA110-FUNCTION
        //* *MOVE 'P22A1'          TO FCPA110.FCPA110-SOURCE-CODE /* RL
        //* *CALLNAT 'FCPN110' FCPA110 /* RL FEB 27 DO NOT UPDATE IN 1400 USE 876
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
                                                                                                                                                                          //Natural: PERFORM DETERMINE-STMNT-TYPE
        sub_Determine_Stmnt_Type();
        if (condition(Global.isEscape())) {return;}
        //*  CORRESPONDENCE ADDRESS
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean() && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Lines().getValue(2).notEquals(" ")))              //Natural: IF #FCPA803.#STMNT AND PYMNT-ADDR-LINES ( 2 ) NE ' '
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(2);                                                                                                         //Natural: ASSIGN #ADDR-IND := 2
            //*  PAYMENT ADDRESS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(1);                                                                                                         //Natural: ASSIGN #ADDR-IND := 1
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nadd(1);                                                                                                          //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SEQ-NBR
        short decideConditionsMet1004 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #GLOBAL-PAY
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))
        {
            decideConditionsMet1004++;
            //*  RA
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().nadd(1);                                                                                                    //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Text_Ind().setValue(2);                                                                                                   //Natural: ASSIGN #STMNT-TEXT-IND := 2
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#ZERO-CHECK
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().getBoolean()))
        {
            decideConditionsMet1004++;
            pnd_Ws_Pymnt_Check_Nbr.setValue(9999999);                                                                                                                     //Natural: ASSIGN #WS.PYMNT-CHECK-NBR := 9999999
            pnd_Ws_Pymnt_Check_Nbr_N10.setValue(9999999999L);                                                                                                             //Natural: ASSIGN #WS.PYMNT-CHECK-NBR-N10 := 9999999999
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Text_Ind().setValue(1);                                                                                                   //Natural: ASSIGN #STMNT-TEXT-IND := 1
        }                                                                                                                                                                 //Natural: WHEN NOT #FCPA803.#STMNT
        else if (condition(! ((pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean()))))
        {
            decideConditionsMet1004++;
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().nadd(1);                                                                                                          //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-NBR
            //*  RA
            //*  RA
            pnd_Pymnt_Check_Nbr_N10.nadd(1);                                                                                                                              //Natural: ADD 1 TO #PYMNT-CHECK-NBR-N10
            pnd_Ws_Pymnt_Check_Nbr_N10.setValue(pnd_Pymnt_Check_Nbr_N10);                                                                                                 //Natural: ASSIGN #WS.PYMNT-CHECK-NBR-N10 := #PYMNT-CHECK-NBR-N10
            pnd_Ws_Pymnt_Check_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                                 //Natural: ASSIGN #WS.PYMNT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().getBoolean()))                                                                                          //Natural: IF #FCPA803.#CSR-RUN
            {
                if (condition(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde().equals("0000")))                                                                     //Natural: IF #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE = '0000'
                {
                    ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                           //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                            //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
                }                                                                                                                                                         //Natural: END-IF
                ldaFcpl876a.getPnd_Fcpl876a_Cntrct_Orgn_Cde().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde());                                               //Natural: ASSIGN #FCPL876A.CNTRCT-ORGN-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE
                ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                              //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
                ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                      //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
                getWorkFiles().write(8, false, ldaFcpl876a.getPnd_Fcpl876a());                                                                                            //Natural: WRITE WORK FILE 8 #FCPL876A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().setValue(false);                                                                                                     //Natural: MOVE FALSE TO #END-OF-PYMNT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().reset();                                                                                                          //Natural: RESET #RECORD-IN-PYMNT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page().compute(new ComputeParameters(false, pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page()), (pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Total_Pages().add(1)).divide(2).multiply(2).subtract(1)); //Natural: COMPUTE #BAR-LAST-PAGE = ( PYMNT-TOTAL-PAGES + 1 ) / 2 * 2 - 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(true);                                                                                                         //Natural: ASSIGN #NEW-PYMNT := TRUE
    }
    private void sub_Determine_Stmnt_Type() throws Exception                                                                                                              //Natural: DETERMINE-STMNT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := TRUE
        //* TMM-2001/11
        //* TMM-2001/11
        //* TMM-2001/11
        short decideConditionsMet1044 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PYMNT-PAY-TYPE-REQ-IND = 3 OR PYMNT-PAY-TYPE-REQ-IND = 4 OR PYMNT-PAY-TYPE-REQ-IND = 9
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4) 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9)))
        {
            decideConditionsMet1044++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().setValue(true);                                                                                                    //Natural: ASSIGN #GLOBAL-PAY := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#STMNT := TRUE
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(3);                                                                                                    //Natural: ASSIGN #ACCUM-OCCUR := 3
        }                                                                                                                                                                 //Natural: WHEN PYMNT-CHECK-AMT = 0.00
        else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(new DbsDecimal("0.00"))))
        {
            decideConditionsMet1044++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#ZERO-CHECK := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#STMNT := TRUE
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(15);                                                                                                   //Natural: ASSIGN #ACCUM-OCCUR := 15
        }                                                                                                                                                                 //Natural: WHEN PYMNT-CHECK-AMT NE 0.00
        else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().notEquals(new DbsDecimal("0.00"))))
        {
            decideConditionsMet1044++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().setValue(false);                                                                                                   //Natural: ASSIGN #FCPA803.#ZERO-CHECK := FALSE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(false);                                                                                                        //Natural: ASSIGN #FCPA803.#STMNT := FALSE
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(1);                                                                                                    //Natural: ASSIGN #ACCUM-OCCUR := 1
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Calc_Simplex_Duplex() throws Exception                                                                                                               //Natural: CALC-SIMPLEX-DUPLEX
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        if (condition(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Total_Pages().equals(1)))                                                                                //Natural: IF PYMNT-TOTAL-PAGES = 1
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().setValue(true);                                                                                                       //Natural: ASSIGN #SIMPLEX := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().setValue(false);                                                                                                      //Natural: ASSIGN #SIMPLEX := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Void_Page() throws Exception                                                                                                                   //Natural: PRINT-VOID-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().getBoolean()))                                                                                              //Natural: IF #FCPA803.#CSR-RUN
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            DbsUtil.callnat(Fcpn803v.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803());                                                                      //Natural: CALLNAT 'FCPN803V' USING #FCPA803
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Start_Of_Processing() throws Exception                                                                                                               //Natural: START-OF-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
                                                                                                                                                                          //Natural: PERFORM PRINT-VOID-PAGE
        sub_Print_Void_Page();
        if (condition(Global.isEscape())) {return;}
        //*  IF NOT #FCPA803.#CSR-RUN
        //*  SUBTRACT 1                           FROM #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*  SUBTRACT 1                           FROM #FCPL876.PYMNT-CHECK-NBR
        //*  SUBTRACT 1                           FROM #PYMNT-CHECK-NBR-N10 /* RA
        //*  END-IF
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
        sub_Init_New_Pymnt();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALC-SIMPLEX-DUPLEX
        sub_Calc_Simplex_Duplex();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                        //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
    }
    private void sub_Process_Input_Parm() throws Exception                                                                                                                //Natural: PROCESS-INPUT-PARM
    {
        if (BLNatReinput.isReinput()) return;

        setLocalMethod("FCPP803D|sub_Process_Input_Parm");
        while(true)
        {
            try
            {
                //* ***********************************
                if (condition(Global.getSTACK().getDatacount().notEquals(1)))                                                                                             //Natural: IF *DATA NE 1
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    getReports().write(0, "***",new TabSetting(25),"INPUT PARAMETER WAS NOT SUPPLIED",new TabSetting(77),"***");                                          //Natural: WRITE '***' 25T 'INPUT PARAMETER WAS NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    DbsUtil.terminate(50);  if (true) return;                                                                                                             //Natural: TERMINATE 50
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_File_Type);                                                                                 //Natural: INPUT #FILE-TYPE
                short decideConditionsMet1099 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #FILE-TYPE;//Natural: VALUE 'CHECK'
                if (condition((pnd_Ws_Pnd_File_Type.equals("CHECK"))))
                {
                    decideConditionsMet1099++;
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().setValue(false);                                                                                              //Natural: ASSIGN #FCPA803.#CSR-RUN := FALSE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1 ) := #FCPACRPT.#TRUTH-TABLE ( 15 ) := TRUE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(15).setValue(true);
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("   ANNUITY PAYMENTS CHECK STATEMENTS");                                                             //Natural: ASSIGN #FCPACRPT.#TITLE := '   ANNUITY PAYMENTS CHECK STATEMENTS'
                    //*    PERFORM GET-MONTHLY-RESTART-INFO   /* RA
                    //*  RL
                    pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P22A1");                                                                                        //Natural: MOVE 'P22A1' TO FCPA110.FCPA110-SOURCE-CODE
                    //*  HERE WE GET SEQ NO ALSO
                    //*  1400 RL
                                                                                                                                                                          //Natural: PERFORM GET-START-CHECK-NO
                    sub_Get_Start_Check_No();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter().setValue(9);                                                                           //Natural: ASSIGN #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER := 9
                }                                                                                                                                                         //Natural: VALUE 'GLOBAL'
                else if (condition((pnd_Ws_Pnd_File_Type.equals("GLOBAL"))))
                {
                    decideConditionsMet1099++;
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().setValue(false);                                                                                              //Natural: ASSIGN #FCPA803.#CSR-RUN := FALSE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(3).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 3 ) := TRUE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue(" ANNUITY PAYMENTS GLOBAL PAY STATEMENTS");                                                          //Natural: ASSIGN #FCPACRPT.#TITLE := ' ANNUITY PAYMENTS GLOBAL PAY STATEMENTS'
                    ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter().setValue(9);                                                                           //Natural: ASSIGN #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER := 9
                }                                                                                                                                                         //Natural: VALUE 'CSR'
                else if (condition((pnd_Ws_Pnd_File_Type.equals("CSR"))))
                {
                    decideConditionsMet1099++;
                    pdaFcpa803.getPnd_Fcpa803_Pnd_Csr_Run().setValue(true);                                                                                               //Natural: ASSIGN #FCPA803.#CSR-RUN := TRUE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1 ) := #FCPACRPT.#TRUTH-TABLE ( 15 ) := TRUE
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(15).setValue(true);
                    pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("ANNUITY PAYMENTS CANCEL & REDRAW CHECKS");                                                          //Natural: ASSIGN #FCPACRPT.#TITLE := 'ANNUITY PAYMENTS CANCEL & REDRAW CHECKS'
                                                                                                                                                                          //Natural: PERFORM GET-DAILY-RUN-INFO
                    sub_Get_Daily_Run_Info();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  RL
                    pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                        //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
                    //*  GET BANK DATA FOR CHK PRINTING-RL
                                                                                                                                                                          //Natural: PERFORM GET-START-CHECK-NO
                    sub_Get_Start_Check_No();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  RL
                                                                                                                                                                          //Natural: PERFORM GET-NEXT-CHECK-FILE
                    sub_Get_Next_Check_File();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(pnd_Ws_Pnd_Run_Type.equals("HELD") || pnd_Ws_Pnd_Run_Type.equals("HOLD")))                                                              //Natural: IF #RUN-TYPE = 'HELD' OR = 'HOLD'
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM GET-HOLD-CONTROL-RECORD
                        sub_Get_Hold_Control_Record();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, "***",new TabSetting(25),"INVALID PARAMETER:",pnd_Ws_Pnd_File_Type,new TabSetting(77),"***");                                   //Natural: WRITE '***' 25T 'INVALID PARAMETER:' #FILE-TYPE 77T '***'
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(51);  if (true) return;                                                                                                             //Natural: TERMINATE 51
                }                                                                                                                                                         //Natural: END-DECIDE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Next_Check_File() throws Exception                                                                                                               //Natural: GET-NEXT-CHECK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getWorkFiles().read(6, ldaFcpl876.getPnd_Fcpl876());                                                                                                              //Natural: READ WORK FILE 6 ONCE #FCPL876
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'NEXT CHECK' FILE",new TabSetting(77),"***");                                                         //Natural: WRITE '***' 25T 'MISSING "NEXT CHECK" FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(54);  if (true) return;                                                                                                                     //Natural: TERMINATE 54
        }                                                                                                                                                                 //Natural: END-ENDFILE
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue("*").setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Barcode().getValue("*"));                               //Natural: ASSIGN #BARCODE-LDA.#BAR-BARCODE ( * ) := #FCPL876.#BAR-BARCODE ( * )
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes().setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes());                                                       //Natural: ASSIGN #BARCODE-LDA.#BAR-ENVELOPES := #FCPL876.#BAR-ENVELOPES
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().getBoolean());                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := #FCPL876.#BAR-NEW-RUN
        //* *S-NEXT-CHECK-PREFIX         := #FCPL876.PYMNT-CHECK-PREFIX /* RL
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"at start of program:",NEWLINE,"The program is going to use the following:",NEWLINE,                   //Natural: WRITE *PROGRAM *TIME 'at start of program:' / 'The program is going to use the following:' / 'start assigned check   number:' #FCPL876.PYMNT-CHECK-NBR / 'start assigned EFT     number:' #FCPL876.PYMNT-CHECK-SCRTY-NBR / 'start sequence number........:' #FCPL876.PYMNT-CHECK-SEQ-NBR
            "start assigned check   number:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr(),NEWLINE,"start assigned EFT     number:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr(),
            NEWLINE,"start sequence number........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());
        if (Global.isEscape()) return;
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().nsubtract(1);                                                                                                   //Natural: SUBTRACT 1 FROM #FCPL876.PYMNT-CHECK-SCRTY-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nsubtract(1);                                                                                                     //Natural: SUBTRACT 1 FROM #FCPL876.PYMNT-CHECK-SEQ-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().nsubtract(1);                                                                                                         //Natural: SUBTRACT 1 FROM #FCPL876.PYMNT-CHECK-NBR
        pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                           //Natural: ASSIGN #PYMNT-CHECK-NBR-N3 := FCPA110.START-CHECK-PREFIX-N3
        pnd_Pymnt_Check_Nbr_N10_Pnd_Pymnt_Check_Nbr_N7.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                             //Natural: ASSIGN #PYMNT-CHECK-NBR-N7 := #FCPL876.PYMNT-CHECK-NBR
    }
    private void sub_Get_Daily_Run_Info() throws Exception                                                                                                                //Natural: GET-DAILY-RUN-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        getWorkFiles().read(2, pnd_Ws_Pnd_Run_Type);                                                                                                                      //Natural: READ WORK FILE 2 ONCE #RUN-TYPE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING PARAMETER FILE",new TabSetting(77),"***");                                                            //Natural: WRITE '***' 25T 'MISSING PARAMETER FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Get_Start_Check_No() throws Exception                                                                                                                //Natural: GET-START-CHECK-NO
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //*  READ WORK FILE 2 ONCE #INPUT-PARM
        //*  AT END OF FILE
        //*   PERFORM ERROR-DISPLAY-START
        //*   WRITE '***' 25T 'MISSING PARAMETER FILE' 77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*   TERMINATE 53
        //*  END-ENDFILE
        //*  --------------------------------------------------
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //* *MOVE  'P22A1' TO FCPA110.FCPA110-SOURCE-CODE  /* RL
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        //*  RL
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            pdaFcpa110.getFcpa110_Fcpa110_Old_Check_Nbr().setValue(pdaFcpa110.getFcpa110_Start_Check_No());                                                               //Natural: ASSIGN FCPA110.FCPA110-OLD-CHECK-NBR := FCPA110.START-CHECK-NO
            pnd_Ws_Next_Check_Prefix.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                             //Natural: ASSIGN #WS-NEXT-CHECK-PREFIX := FCPA110.START-CHECK-PREFIX-N3
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //*                                             /* RA
        //*  #FCPL876.PYMNT-CHECK-NBR                  := #INPUT-PARM-CHECK-NBR
        //*  #FCPL876.PYMNT-CHECK-SEQ-NBR              := #INPUT-PARM-CHECK-SEQ-NBR
        //*  IF #FCPL876.PYMNT-CHECK-SEQ-NBR = 1
        //*   #BARCODE-LDA.#BAR-NEW-RUN               := TRUE
        //*  ELSE
        //*   #MONTHLY-RESTART                        := TRUE
        //*  END-IF                                            /* RA
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                      //Natural: RESET #BAR-ENV-ID-NUM
    }
    private void sub_Get_Hold_Control_Record() throws Exception                                                                                                           //Natural: GET-HOLD-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        getWorkFiles().read(4, pdaFcpaacum.getPnd_Fcpaacum());                                                                                                            //Natural: READ WORK FILE 4 ONCE #FCPAACUM
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'HOLD CHECK' CONTROL FILE",new TabSetting(77),"***");                                                 //Natural: WRITE '***' 25T 'MISSING "HOLD CHECK" CONTROL FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(55);  if (true) return;                                                                                                                     //Natural: TERMINATE 55
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  DAILY RUN
        if (condition(pnd_Ws_Pnd_Run_Type.equals("HELD") || pnd_Ws_Pnd_Run_Type.equals("HOLD")))                                                                          //Natural: IF #RUN-TYPE = 'HELD' OR = 'HOLD'
        {
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(true);                                                                                                   //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := TRUE
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().reset();                                                                                                        //Natural: RESET #FCPL876.#BAR-ENVELOPES
            getWorkFiles().write(3, false, pdaFcpaacum.getPnd_Fcpaacum());                                                                                                //Natural: WRITE WORK FILE 3 #FCPAACUM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().getBoolean());                                          //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := #BARCODE-LDA.#BAR-NEW-RUN
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes());                                                   //Natural: ASSIGN #FCPL876.#BAR-ENVELOPES := #BARCODE-LDA.#BAR-ENVELOPES
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                   //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
            DbsUtil.callnat(Fcpncrpt.class , getCurrentProcessState(), pdaFcpaacum.getPnd_Fcpaacum(), pdaFcpacrpt.getPnd_Fcpacrpt());                                     //Natural: CALLNAT 'FCPNCRPT' USING #FCPAACUM #FCPACRPT
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Control_Totals() throws Exception                                                                                                              //Natural: ACCUM-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  #MAX-OCCUR
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().greaterOrEqual(1) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().lessOrEqual(50)))                  //Natural: IF #ACCUM-OCCUR = 1 THRU 50
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                     //Natural: ASSIGN #@@MAX-FUND := WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        short decideConditionsMet1241 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPAACUM.#NEW-PYMNT-IND
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                  //Natural: ADD 1 TO #FCPAACUM.#PYMNT-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 3 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(3).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Cntrct_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#CNTRCT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 4 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(4).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dvdnd_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DVDND-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 5 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(5).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dci_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DCI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DCI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 6 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(6).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dpi_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DPI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DPI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#NET-PYMNT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 8 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(8).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Fdrl_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#FDRL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 9 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(9).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_State_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#STATE-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 10 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(10).equals(true)))
        {
            decideConditionsMet1241++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Local_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#LOCAL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1241 > 0))
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                    //Natural: ADD 1 TO #FCPAACUM.#REC-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1241 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_NbrIsBreak = pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().isBreak(endOfData);
        boolean pdaFcpa803c_getPnd_Check_Sort_Fields_Cntrct_Company_CdeIsBreak = pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde().isBreak(endOfData);
        boolean pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_TypeIsBreak = pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().isBreak(endOfData);
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        boolean pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak = pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().isBreak(endOfData);
        if (condition(pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_NbrIsBreak || pdaFcpa803c_getPnd_Check_Sort_Fields_Cntrct_Company_CdeIsBreak || pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_TypeIsBreak 
            || pnd_KeyIsBreak || pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Ppcn_Break().setValue(true);                                                                                                    //Natural: ASSIGN #PPCN-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpa803c_getPnd_Check_Sort_Fields_Cntrct_Company_CdeIsBreak || pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_TypeIsBreak 
            || pnd_KeyIsBreak || pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Company_Break().setValue(true);                                                                                                 //Natural: ASSIGN #COMPANY-BREAK := TRUE
            //*  SPIA PS-SELECT CHECK
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpa800d_getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_TypeIsBreak || pnd_KeyIsBreak || pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak))
        {
            if (condition(pdaFcpa803c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde().equals("L") && (pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S")  //Natural: IF CNTRCT-COMPANY-CDE = 'L' AND ( CNTRCT-ANNTY-INS-TYPE = 'S' OR CNTRCT-ANNTY-INS-TYPE = 'M' )
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M"))))
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Company_Break().setValue(true);                                                                                             //Natural: ASSIGN #COMPANY-BREAK := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_KeyIsBreak || pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak))
        {
            //*  NOT EOF
            if (condition(pnd_Key.notEquals(readWork01Pnd_KeyOld)))                                                                                                       //Natural: IF #KEY NE OLD ( #KEY )
            {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                sub_Init_New_Pymnt();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(15, "LS=133 PS=58 ZP=ON");

        getReports().write(0, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(21),pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title(),new TabSetting(68),"REPORT: RPT0",NEWLINE,new 
            TabSetting(36),pnd_Ws_Pnd_Run_Type,NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData798() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pdaFcpa803.getPnd_Fcpa803().setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                              //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #FCPA803
            //*    IF NOT #MONTHLY-RESTART           /* RA
                                                                                                                                                                          //Natural: PERFORM START-OF-PROCESSING
            sub_Start_Of_Processing();
            if (condition(Global.isEscape())) {return;}
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                   //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #KEY-DETAIL
            //*    END-IF                          /* RA
        }                                                                                                                                                                 //Natural: END-START
    }
}
