/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:35 PM
**        * FROM NATURAL PROGRAM : Fcpp832
************************************************************
**        * FILE NAME            : Fcpp832.java
**        * CLASS NAME           : Fcpp832
**        * INSTANCE NAME        : Fcpp832
************************************************************
************************************************************************
*
* PROGRAM  : FCPP832
*
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : NEGATIVE NET INCOME REPORT
* CREATED  : 05/01/96
*
* MODIFIED : 08/01/01 A.REYHANIAN   READ EXPANDED FILE
*          :                        (INCLUDING TAX ELECTION INFORMATION)
*          : 05/05/03 R.CARREON     STOW DUE TO FCPA800
*
*          : 05/07/08 : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHGS.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp832 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa802 pdaFcpa802;
    private LdaFcpl801a ldaFcpl801a;
    private PdaFcpacntl pdaFcpacntl;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Report_Vars;
    private DbsField pnd_Report_Vars_Pnd_Cntrct_Ppcn_Payee;
    private DbsField pnd_Report_Vars_Pnd_Payee_Name;
    private DbsField pnd_Report_Vars_Pnd_Gross_Amt;
    private DbsField pnd_Report_Vars_Pnd_Taxable_Income;

    private DbsGroup pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level;
    private DbsField pnd_Report_Vars_Pnd_Tax_Auth;
    private DbsField pnd_Report_Vars_Pnd_Tax_Calc;
    private DbsField pnd_Report_Vars_Pnd_Tax_Withheld;
    private DbsField pnd_Report_Vars_Pnd_Resp_Code;
    private DbsField pnd_Report_Vars_Pnd_Filing_Stat;
    private DbsField pnd_Report_Vars_Pnd_Allow_Count;
    private DbsField pnd_Report_Vars_Pnd_Tax_Fixed_Amt;
    private DbsField pnd_Report_Vars_Pnd_Tax_Fixed_Percent;

    private DbsGroup pnd_Amts;
    private DbsField pnd_Amts_Pnd_Fdrl_Tax_Amt_Withheld;
    private DbsField pnd_Amts_Pnd_State_Tax_Amt_Withheld;
    private DbsField pnd_Amts_Pnd_Local_Tax_Amt_Withheld;
    private DbsField pnd_Amts_Pnd_Ivc_Amt;
    private DbsField pnd_Amts_Pnd_Taxes_Calculated;
    private DbsField pnd_Negative_Net;
    private DbsField pnd_Input_Cnt;
    private DbsField pnd_Output_Cnt;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa802 = new PdaFcpa802(localVariables);
        ldaFcpl801a = new LdaFcpl801a();
        registerRecord(ldaFcpl801a);
        pdaFcpacntl = new PdaFcpacntl(localVariables);

        // Local Variables

        pnd_Report_Vars = localVariables.newGroupInRecord("pnd_Report_Vars", "#REPORT-VARS");
        pnd_Report_Vars_Pnd_Cntrct_Ppcn_Payee = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Cntrct_Ppcn_Payee", "#CNTRCT-PPCN-PAYEE", FieldType.STRING, 
            11);
        pnd_Report_Vars_Pnd_Payee_Name = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Payee_Name", "#PAYEE-NAME", FieldType.STRING, 26);
        pnd_Report_Vars_Pnd_Gross_Amt = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Report_Vars_Pnd_Taxable_Income = pnd_Report_Vars.newFieldInGroup("pnd_Report_Vars_Pnd_Taxable_Income", "#TAXABLE-INCOME", FieldType.PACKED_DECIMAL, 
            11, 2);

        pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level = pnd_Report_Vars.newGroupInGroup("pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level", "#REPORT-VARS-ON-EACH-LEVEL");
        pnd_Report_Vars_Pnd_Tax_Auth = pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.newFieldInGroup("pnd_Report_Vars_Pnd_Tax_Auth", "#TAX-AUTH", FieldType.STRING, 
            2);
        pnd_Report_Vars_Pnd_Tax_Calc = pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.newFieldInGroup("pnd_Report_Vars_Pnd_Tax_Calc", "#TAX-CALC", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Report_Vars_Pnd_Tax_Withheld = pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.newFieldInGroup("pnd_Report_Vars_Pnd_Tax_Withheld", "#TAX-WITHHELD", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Report_Vars_Pnd_Resp_Code = pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.newFieldInGroup("pnd_Report_Vars_Pnd_Resp_Code", "#RESP-CODE", FieldType.STRING, 
            1);
        pnd_Report_Vars_Pnd_Filing_Stat = pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.newFieldInGroup("pnd_Report_Vars_Pnd_Filing_Stat", "#FILING-STAT", 
            FieldType.STRING, 2);
        pnd_Report_Vars_Pnd_Allow_Count = pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.newFieldInGroup("pnd_Report_Vars_Pnd_Allow_Count", "#ALLOW-COUNT", 
            FieldType.NUMERIC, 2);
        pnd_Report_Vars_Pnd_Tax_Fixed_Amt = pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.newFieldInGroup("pnd_Report_Vars_Pnd_Tax_Fixed_Amt", "#TAX-FIXED-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Report_Vars_Pnd_Tax_Fixed_Percent = pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.newFieldInGroup("pnd_Report_Vars_Pnd_Tax_Fixed_Percent", 
            "#TAX-FIXED-PERCENT", FieldType.PACKED_DECIMAL, 7, 4);

        pnd_Amts = localVariables.newGroupInRecord("pnd_Amts", "#AMTS");
        pnd_Amts_Pnd_Fdrl_Tax_Amt_Withheld = pnd_Amts.newFieldInGroup("pnd_Amts_Pnd_Fdrl_Tax_Amt_Withheld", "#FDRL-TAX-AMT-WITHHELD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Amts_Pnd_State_Tax_Amt_Withheld = pnd_Amts.newFieldInGroup("pnd_Amts_Pnd_State_Tax_Amt_Withheld", "#STATE-TAX-AMT-WITHHELD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Amts_Pnd_Local_Tax_Amt_Withheld = pnd_Amts.newFieldInGroup("pnd_Amts_Pnd_Local_Tax_Amt_Withheld", "#LOCAL-TAX-AMT-WITHHELD", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Amts_Pnd_Ivc_Amt = pnd_Amts.newFieldInGroup("pnd_Amts_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Amts_Pnd_Taxes_Calculated = pnd_Amts.newFieldInGroup("pnd_Amts_Pnd_Taxes_Calculated", "#TAXES-CALCULATED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Negative_Net = localVariables.newFieldInRecord("pnd_Negative_Net", "#NEGATIVE-NET", FieldType.BOOLEAN, 1);
        pnd_Input_Cnt = localVariables.newFieldInRecord("pnd_Input_Cnt", "#INPUT-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_Output_Cnt = localVariables.newFieldInRecord("pnd_Output_Cnt", "#OUTPUT-CNT", FieldType.PACKED_DECIMAL, 6);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl801a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp832() throws Exception
    {
        super("Fcpp832");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 55
        getWorkFiles().read(1, pdaFcpacntl.getCntl());                                                                                                                    //Natural: READ WORK FILE 1 ONCE CNTL
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 'DATE:' *DATU 53T 'CONSOLIDATED PAYMENT SYSTEM' 119T *PROGRAM / 'TIME:' *TIMX ( EM = HH:IIAP ) 58T 'ANNUITY PAYMENTS' 119T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 50T 'NEGATIVE NET INCOME FOR' CNTL.CNTL-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) 110T 'ELECTION INFO' //
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*  AD WORK FILE 2 PYMNT-ADDR-INFO WF-ERROR-MSG INV-INFO(*)        /* ABRM
        //*  ABRM
        //*  ABRM
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 PYMNT-ADDR-INFO WF-ERROR-MSG WF-PYMNT-TAX-REC INV-INFO ( * )
        while (condition(getWorkFiles().read(2, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), ldaFcpl801a.getWf_Error_Msg(), pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            pnd_Input_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #INPUT-CNT
            pnd_Report_Vars.reset();                                                                                                                                      //Natural: RESET #REPORT-VARS #AMTS #NEGATIVE-NET
            pnd_Amts.reset();
            pnd_Negative_Net.reset();
            FOR01:                                                                                                                                                        //Natural: FOR #I = 1 TO INV-ACCT-COUNT
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_I.nadd(1))
            {
                pnd_Amts_Pnd_Fdrl_Tax_Amt_Withheld.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                                         //Natural: ADD INV-ACCT-FDRL-TAX-AMT ( #I ) TO #FDRL-TAX-AMT-WITHHELD
                pnd_Amts_Pnd_State_Tax_Amt_Withheld.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                                       //Natural: ADD INV-ACCT-STATE-TAX-AMT ( #I ) TO #STATE-TAX-AMT-WITHHELD
                pnd_Amts_Pnd_Local_Tax_Amt_Withheld.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                                       //Natural: ADD INV-ACCT-LOCAL-TAX-AMT ( #I ) TO #LOCAL-TAX-AMT-WITHHELD
                pnd_Report_Vars_Pnd_Gross_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_I));                                                 //Natural: ADD INV-ACCT-SETTL-AMT ( #I ) TO #GROSS-AMT
                pnd_Amts_Pnd_Ivc_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Amt().getValue(pnd_I));                                                            //Natural: ADD INV-ACCT-IVC-AMT ( #I ) TO #IVC-AMT
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Report_Vars_Pnd_Taxable_Income.compute(new ComputeParameters(false, pnd_Report_Vars_Pnd_Taxable_Income), pnd_Report_Vars_Pnd_Gross_Amt.subtract(pnd_Amts_Pnd_Ivc_Amt)); //Natural: ASSIGN #TAXABLE-INCOME := #GROSS-AMT - #IVC-AMT
            pnd_Amts_Pnd_Taxes_Calculated.compute(new ComputeParameters(false, pnd_Amts_Pnd_Taxes_Calculated), pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Fed_C_Tax_Withhold().add(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold()).add(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold())); //Natural: ASSIGN #TAXES-CALCULATED := WF-PYMNT-ADDR-GRP.TAX-FED-C-TAX-WITHHOLD + WF-PYMNT-ADDR-GRP.TAX-STA-C-TAX-WITHHOLD + WF-PYMNT-ADDR-GRP.TAX-LOC-C-TAX-WITHHOLD
            if (condition(pnd_Amts_Pnd_Taxes_Calculated.greater(pnd_Report_Vars_Pnd_Taxable_Income)))                                                                     //Natural: IF #TAXES-CALCULATED > #TAXABLE-INCOME
            {
                pnd_Negative_Net.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #NEGATIVE-NET
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Negative_Net.getBoolean()))                                                                                                                 //Natural: IF #NEGATIVE-NET
            {
                pnd_Output_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #OUTPUT-CNT
                pnd_Report_Vars_Pnd_Cntrct_Ppcn_Payee.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),         //Natural: COMPRESS WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR '-' WF-PYMNT-ADDR-GRP.CNTRCT-PAYEE-CDE INTO #CNTRCT-PPCN-PAYEE LEAVE NO
                    "-", pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde()));
                pnd_Report_Vars_Pnd_Payee_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name(),                   //Natural: COMPRESS WF-PYMNT-ADDR-GRP.PH-LAST-NAME ', ' WF-PYMNT-ADDR-GRP.PH-FIRST-NAME INTO #PAYEE-NAME LEAVE NO
                    ", ", pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name()));
                //*  DISPLAY FEDERAL LEVEL
                pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.reset();                                                                                                    //Natural: RESET #REPORT-VARS-ON-EACH-LEVEL
                pnd_Report_Vars_Pnd_Tax_Calc.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Fed_C_Tax_Withhold());                                                          //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-FED-C-TAX-WITHHOLD TO #TAX-CALC
                pnd_Report_Vars_Pnd_Tax_Withheld.setValue(pnd_Amts_Pnd_Fdrl_Tax_Amt_Withheld);                                                                            //Natural: MOVE #FDRL-TAX-AMT-WITHHELD TO #TAX-WITHHELD
                pnd_Report_Vars_Pnd_Resp_Code.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Fed_C_Resp_Code());                                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-FED-C-RESP-CODE TO #RESP-CODE
                pnd_Report_Vars_Pnd_Filing_Stat.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Fed_C_Filing_Stat());                                                        //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-FED-C-FILING-STAT TO #FILING-STAT
                pnd_Report_Vars_Pnd_Allow_Count.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Fed_C_Allow_Count());                                                        //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-FED-C-ALLOW-COUNT TO #ALLOW-COUNT
                pnd_Report_Vars_Pnd_Tax_Fixed_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Amount());                                                     //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-FED-C-FIXED-AMOUNT TO #TAX-FIXED-AMT
                pnd_Report_Vars_Pnd_Tax_Fixed_Percent.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Fed_C_Fixed_Percent());                                                //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-FED-C-FIXED-PERCENT TO #TAX-FIXED-PERCENT
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-1
                sub_Print_Report_1();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold().notEquals(getZero())))                                                             //Natural: IF WF-PYMNT-ADDR-GRP.TAX-STA-C-TAX-WITHHOLD NE 0
                {
                    pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.reset();                                                                                                //Natural: RESET #REPORT-VARS-ON-EACH-LEVEL
                    pnd_Report_Vars_Pnd_Tax_Auth.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde());                                                             //Natural: MOVE WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE TO #TAX-AUTH
                    pnd_Report_Vars_Pnd_Tax_Calc.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Tax_Withhold());                                                      //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-STA-C-TAX-WITHHOLD TO #TAX-CALC
                    pnd_Report_Vars_Pnd_Tax_Withheld.setValue(pnd_Amts_Pnd_State_Tax_Amt_Withheld);                                                                       //Natural: MOVE #STATE-TAX-AMT-WITHHELD TO #TAX-WITHHELD
                    pnd_Report_Vars_Pnd_Resp_Code.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Resp_Code());                                                        //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-STA-C-RESP-CODE TO #RESP-CODE
                    pnd_Report_Vars_Pnd_Filing_Stat.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Filing_Stat());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-STA-C-FILING-STAT TO #FILING-STAT
                    pnd_Report_Vars_Pnd_Allow_Count.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Allow_Count());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-STA-C-ALLOW-COUNT TO #ALLOW-COUNT
                    pnd_Report_Vars_Pnd_Tax_Fixed_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Amount());                                                 //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-STA-C-FIXED-AMOUNT TO #TAX-FIXED-AMT
                    pnd_Report_Vars_Pnd_Tax_Fixed_Percent.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Sta_C_Fixed_Percent());                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-STA-C-FIXED-PERCENT TO #TAX-FIXED-PERCENT
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-1
                    sub_Print_Report_1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold().notEquals(getZero())))                                                             //Natural: IF WF-PYMNT-ADDR-GRP.TAX-LOC-C-TAX-WITHHOLD NE 0
                {
                    pnd_Report_Vars_Pnd_Report_Vars_On_Each_Level.reset();                                                                                                //Natural: RESET #REPORT-VARS-ON-EACH-LEVEL
                    pnd_Report_Vars_Pnd_Tax_Auth.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Locality_Cde());                                                           //Natural: MOVE WF-PYMNT-ADDR-GRP.ANNT-LOCALITY-CDE TO #TAX-AUTH
                    pnd_Report_Vars_Pnd_Tax_Calc.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Loc_C_Tax_Withhold());                                                      //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-LOC-C-TAX-WITHHOLD TO #TAX-CALC
                    pnd_Report_Vars_Pnd_Tax_Withheld.setValue(pnd_Amts_Pnd_Local_Tax_Amt_Withheld);                                                                       //Natural: MOVE #LOCAL-TAX-AMT-WITHHELD TO #TAX-WITHHELD
                    pnd_Report_Vars_Pnd_Resp_Code.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Loc_C_Resp_Code());                                                        //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-LOC-C-RESP-CODE TO #RESP-CODE
                    pnd_Report_Vars_Pnd_Tax_Fixed_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Amount());                                                 //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-LOC-C-FIXED-AMOUNT TO #TAX-FIXED-AMT
                    pnd_Report_Vars_Pnd_Tax_Fixed_Percent.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Loc_C_Fixed_Percent());                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.TAX-LOC-C-FIXED-PERCENT TO #TAX-FIXED-PERCENT
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT-1
                    sub_Print_Report_1();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,"TOTAL # OF RECORDS READ FROM INPUT FILE  :",pnd_Input_Cnt,NEWLINE,"TOTAL # OF RECORDS WRITEN TO THE REPORT  :",                    //Natural: WRITE / 'TOTAL # OF RECORDS READ FROM INPUT FILE  :' #INPUT-CNT / 'TOTAL # OF RECORDS WRITEN TO THE REPORT  :' #OUTPUT-CNT
            pnd_Output_Cnt);
        if (Global.isEscape()) return;
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORT-1
    }
    private void sub_Print_Report_1() throws Exception                                                                                                                    //Natural: PRINT-REPORT-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        getReports().display(1, "CONTRACT #/PAYEE CODE",                                                                                                                  //Natural: DISPLAY ( 1 ) 'CONTRACT #/PAYEE CODE' #CNTRCT-PPCN-PAYEE 'SOCIAL/SECURITY NO' ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'PAYEE/NAME' #PAYEE-NAME ( AL = 15 ) 'GROSS/INCOME' #GROSS-AMT 'TAXABLE/INCOME' #TAXABLE-INCOME 'TAX/AUTH' #TAX-AUTH 'TAXES/CALCULATED' #TAX-CALC 'TAXES/WITHHELD' #TAX-WITHHELD 'RESP/CODE' #RESP-CODE 'MAR/STAT' #FILING-STAT 'NO/EX' #ALLOW-COUNT 'FIXED/AMOUNT' #TAX-FIXED-AMT 'FIXED/PERCENT' #TAX-FIXED-PERCENT
        		pnd_Report_Vars_Pnd_Cntrct_Ppcn_Payee,"SOCIAL/SECURITY NO",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"PAYEE/NAME",
        		pnd_Report_Vars_Pnd_Payee_Name, new AlphanumericLength (15),"GROSS/INCOME",
        		pnd_Report_Vars_Pnd_Gross_Amt,"TAXABLE/INCOME",
        		pnd_Report_Vars_Pnd_Taxable_Income,"TAX/AUTH",
        		pnd_Report_Vars_Pnd_Tax_Auth,"TAXES/CALCULATED",
        		pnd_Report_Vars_Pnd_Tax_Calc,"TAXES/WITHHELD",
        		pnd_Report_Vars_Pnd_Tax_Withheld,"RESP/CODE",
        		pnd_Report_Vars_Pnd_Resp_Code,"MAR/STAT",
        		pnd_Report_Vars_Pnd_Filing_Stat,"NO/EX",
        		pnd_Report_Vars_Pnd_Allow_Count,"FIXED/AMOUNT",
        		pnd_Report_Vars_Pnd_Tax_Fixed_Amt,"FIXED/PERCENT",
        		pnd_Report_Vars_Pnd_Tax_Fixed_Percent);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,"DATE:",Global.getDATU(),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(119),Global.getPROGRAM(),NEWLINE,"TIME:",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(58),"ANNUITY PAYMENTS",new 
            TabSetting(119),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(50),"NEGATIVE NET INCOME FOR",pdaFcpacntl.getCntl_Cntl_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),new TabSetting(110),"ELECTION INFO",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "CONTRACT #/PAYEE CODE",
        		pnd_Report_Vars_Pnd_Cntrct_Ppcn_Payee,"SOCIAL/SECURITY NO",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"PAYEE/NAME",
        		pnd_Report_Vars_Pnd_Payee_Name, new AlphanumericLength (15),"GROSS/INCOME",
        		pnd_Report_Vars_Pnd_Gross_Amt,"TAXABLE/INCOME",
        		pnd_Report_Vars_Pnd_Taxable_Income,"TAX/AUTH",
        		pnd_Report_Vars_Pnd_Tax_Auth,"TAXES/CALCULATED",
        		pnd_Report_Vars_Pnd_Tax_Calc,"TAXES/WITHHELD",
        		pnd_Report_Vars_Pnd_Tax_Withheld,"RESP/CODE",
        		pnd_Report_Vars_Pnd_Resp_Code,"MAR/STAT",
        		pnd_Report_Vars_Pnd_Filing_Stat,"NO/EX",
        		pnd_Report_Vars_Pnd_Allow_Count,"FIXED/AMOUNT",
        		pnd_Report_Vars_Pnd_Tax_Fixed_Amt,"FIXED/PERCENT",
        		pnd_Report_Vars_Pnd_Tax_Fixed_Percent);
    }
}
