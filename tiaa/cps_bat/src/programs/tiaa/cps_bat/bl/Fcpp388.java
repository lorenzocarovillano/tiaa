/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:36 PM
**        * FROM NATURAL PROGRAM : Fcpp388
************************************************************
**        * FILE NAME            : Fcpp388.java
**        * CLASS NAME           : Fcpp388
**        * INSTANCE NAME        : Fcpp388
************************************************************
************************************************************************
* OCT 95: PROGRAM WORKED ON BY LEON SILBERSTEIN FOR SWAT CHANGES
*         ALSO BY MIKE SUPONITSKY FOR ALTERNAT CARRIER WORK SHEET
*         THIS VERSION IS BASED ON A COPY FROM THE PROGRAM IN PROJFCP
*         AS OF 10/27/95 AT 12:09 PM.
************************************************************************
* PROGRAM  : FCPP388
* SYSTEM   : CPS
* TITLE    : M.I.T.
* GENERATED: NOVEMBER 30, 1994
* FUNCTION : THIS PROGRAM WILL CREATE AN M.I.T. DATASET TO UPDATE
*          : THE M.I.T. FILE.
* HISTORY  : MODIFIED FOR ALTERNATE CARRIER FACT SHEET.
*          : 10/17/95 M. SUPONITSKY
*          : 06/18/97 RITA SALGADO
*          : ADD LOGIC TO EXTRACT RECORDS FROM THE NZ WORK FILE TO FEED
*          : THE MIT UPDATE PROGRAM (FCPP128).
*
* 5/28/1998 - RIAD LOUTFI - ADDED RETIREMENT LOAN ("AL") PROCESSING.
* 4/27/1999 - LEON GURTOVNIK - FIX PRODUCTION PROBLEM WITH LAST "NZ"
*             RECORD.
* 10/13/99  - ROXAN CARREON - EW PROCESSING
* 12/07/99  -               - FCPAEXT
*
* 06/19/01  - JOAN NEUFELD - MADE CHANGE FOR MDO RECURRING PAYMENT
*                            FREQUENCY=D, DO NOT UPDATE MIT STATUS
*                            FOR MDO INITIAL PAYMENT MIT STATUS SHOULD
*                            BE UPDATED.
*                            FREQUENCY CODE VALUES ARE AS FOLLOWS:
*                            'M' -  STS
*                            '1' -  MONTHLY SWAT
*                            '2' -  QUARTERLY SWAT
*                            '3' -  SEMI-ANNUAL SWAT
*                            '4' -  ANNUAL SWAT
*                            '5' -  SEMI-MONTHLY SWAT
*                            'D' -  MDO
*                            'D' -  MDO
* 04/22/03  - ROXAN CARREON - STOW. FCPAEXT WAS EXPANDED
* 04/2017   - JJG - PIN EXPANSION CHANGES
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp388 extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl378 ldaFcpl378;
    private LdaFcpl128 ldaFcpl128;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Input_Record;
    private DbsField pnd_Ws_Pnd_I_Key;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd;
    private DbsField pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd;
    private DbsField pnd_Ws_Pnd_I_Rec_Det_1;
    private DbsField pnd_Ws_Pnd_I_Rec_Det_2;
    private DbsField pnd_Ws_Pnd_Accept_Ind;

    private DbsGroup pnd_Ws_Pnd_Record_Counts;
    private DbsField pnd_Ws_Pnd_Records_All;
    private DbsField pnd_Ws_Pnd_Records_10;
    private DbsField pnd_Ws_Pnd_Records_20;
    private DbsField pnd_Ws_Pnd_Records_30;
    private DbsField pnd_Ws_Pnd_Records_40;
    private DbsField pnd_Ws_Pnd_Records_45;
    private DbsField pnd_Ws_Pnd_Records_Other;
    private DbsField pnd_Ws_Pnd_Records_Written;
    private DbsField pnd_Ws_Pnd_Nz_Recs_Read;
    private DbsField pnd_Ws_Pnd_Nz_Recs_Written;
    private DbsField pnd_Ws_Pnd_Al_Recs_Read;
    private DbsField pnd_Ws_Pnd_Al_Recs_Written;
    private DbsField pnd_Ws_Pnd_Ew_Recs_Read;
    private DbsField pnd_Ws_Pnd_Ew_Recs_Written;
    private DbsField pnd_Ws_Pnd_Ss_Recs_Written;
    private DbsField pnd_Ws_Pnd_Discrepancy;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pymnt_Split_Reasn_Cde;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Swat_Frqncy_Cde;
    private DbsField pnd_Ws_Pymnt_Init_Rqst_Ind;
    private DbsField pnd_Ws_Pnd_Repeated_Swat_Sts_Pymnt;
    private DbsField pnd_Nz_Seq;

    private DbsGroup pnd_Nz_Seq__R_Field_3;
    private DbsField pnd_Nz_Seq_Pnd_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Nz_Seq_Pnd_Cntrct_Unq_Id_Nr;
    private DbsField pnd_Nz_Seq_Pnd_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Last_Cntrct_Orgn_Cde;
    private DbsField pnd_Records_Found;
    private DbsField pnd_Pnd_Rtb;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl378 = new LdaFcpl378();
        registerRecord(ldaFcpl378);
        ldaFcpl128 = new LdaFcpl128();
        registerRecord(ldaFcpl128);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Input_Record = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Record", "#INPUT-RECORD");
        pnd_Ws_Pnd_I_Key = pnd_Ws_Pnd_Input_Record.newFieldInGroup("pnd_Ws_Pnd_I_Key", "#I-KEY", FieldType.STRING, 41);

        pnd_Ws__R_Field_1 = pnd_Ws_Pnd_Input_Record.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_I_Key);
        pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd", "#I-KEY-REC-LVL-#", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd", "#I-KEY-REC-OCCUR-#", FieldType.NUMERIC, 
            2);
        pnd_Ws_Pnd_I_Rec_Det_1 = pnd_Ws_Pnd_Input_Record.newFieldInGroup("pnd_Ws_Pnd_I_Rec_Det_1", "#I-REC-DET-1", FieldType.STRING, 250);
        pnd_Ws_Pnd_I_Rec_Det_2 = pnd_Ws_Pnd_Input_Record.newFieldInGroup("pnd_Ws_Pnd_I_Rec_Det_2", "#I-REC-DET-2", FieldType.STRING, 209);
        pnd_Ws_Pnd_Accept_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accept_Ind", "#ACCEPT-IND", FieldType.BOOLEAN, 1);

        pnd_Ws_Pnd_Record_Counts = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Record_Counts", "#RECORD-COUNTS");
        pnd_Ws_Pnd_Records_All = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_All", "#RECORDS-ALL", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 9));
        pnd_Ws_Pnd_Records_10 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_10", "#RECORDS-10", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 9));
        pnd_Ws_Pnd_Records_20 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_20", "#RECORDS-20", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 9));
        pnd_Ws_Pnd_Records_30 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_30", "#RECORDS-30", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 9));
        pnd_Ws_Pnd_Records_40 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_40", "#RECORDS-40", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 9));
        pnd_Ws_Pnd_Records_45 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_45", "#RECORDS-45", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 9));
        pnd_Ws_Pnd_Records_Other = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_Other", "#RECORDS-OTHER", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 9));
        pnd_Ws_Pnd_Records_Written = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Records_Written", "#RECORDS-WRITTEN", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Nz_Recs_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Nz_Recs_Read", "#NZ-RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Nz_Recs_Written = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Nz_Recs_Written", "#NZ-RECS-WRITTEN", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Al_Recs_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Al_Recs_Read", "#AL-RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Al_Recs_Written = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Al_Recs_Written", "#AL-RECS-WRITTEN", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Ew_Recs_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ew_Recs_Read", "#EW-RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ew_Recs_Written = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ew_Recs_Written", "#EW-RECS-WRITTEN", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Ss_Recs_Written = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ss_Recs_Written", "#SS-RECS-WRITTEN", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Discrepancy = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Discrepancy", "#DISCREPANCY", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pymnt_Split_Reasn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pymnt_Split_Reasn_Cde);
        pnd_Ws_Swat_Frqncy_Cde = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Swat_Frqncy_Cde", "SWAT-FRQNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Pymnt_Init_Rqst_Ind = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pymnt_Init_Rqst_Ind", "PYMNT-INIT-RQST-IND", FieldType.STRING, 1);
        pnd_Ws_Pnd_Repeated_Swat_Sts_Pymnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Repeated_Swat_Sts_Pymnt", "#REPEATED-SWAT-STS-PYMNT", FieldType.BOOLEAN, 
            1);
        pnd_Nz_Seq = localVariables.newFieldInRecord("pnd_Nz_Seq", "#NZ-SEQ", FieldType.STRING, 36);

        pnd_Nz_Seq__R_Field_3 = localVariables.newGroupInRecord("pnd_Nz_Seq__R_Field_3", "REDEFINE", pnd_Nz_Seq);
        pnd_Nz_Seq_Pnd_Pymnt_Reqst_Log_Dte_Time = pnd_Nz_Seq__R_Field_3.newFieldInGroup("pnd_Nz_Seq_Pnd_Pymnt_Reqst_Log_Dte_Time", "#PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Nz_Seq_Pnd_Cntrct_Unq_Id_Nr = pnd_Nz_Seq__R_Field_3.newFieldInGroup("pnd_Nz_Seq_Pnd_Cntrct_Unq_Id_Nr", "#CNTRCT-UNQ-ID-NR", FieldType.NUMERIC, 
            12);
        pnd_Nz_Seq_Pnd_Pymnt_Prcss_Seq_Nbr = pnd_Nz_Seq__R_Field_3.newFieldInGroup("pnd_Nz_Seq_Pnd_Pymnt_Prcss_Seq_Nbr", "#PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        pnd_Last_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Last_Cntrct_Orgn_Cde", "#LAST-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Records_Found = localVariables.newFieldInRecord("pnd_Records_Found", "#RECORDS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Pnd_Rtb = localVariables.newFieldInRecord("pnd_Pnd_Rtb", "##RTB", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl378.initializeValues();
        ldaFcpl128.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp388() throws Exception
    {
        super("Fcpp388");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) LS = 133 PS = 60 ZP = ON;//Natural: FORMAT ( 01 ) LS = 133 PS = 60 ZP = ON;//Natural: FORMAT ( 02 ) LS = 133 PS = 60 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  #ACCEPT-IND INDICATES WHETHER THE RECORD SHOULD BE ACCEPTED FOR
        //*    THE UPDATE OF MIT, AND THUS RELEASED INTO THE SORT.
        //*    FOR ALTERNATE CARRIER FACT SHEET WE NEED TO PICK UP ADDITIONAL
        //*    DATA FROM RECORD TYPE 40, AND THUS THE INDICATION TO ACCEPT
        //*    THE RECORD IS SETUP WHEN WE ENCOUNTER RECORD 40. HOWEVER, IF
        //*    IT IS A REPEATED-SWAT OR REPEATED-STS PAYMENT,
        //*    THERE IS NO UPDATE OF MIT AND WE AVOID FROM
        //*    INDICATING TO SELECT THE RECORD.
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 08 RECORD #INPUT-RECORD
        while (condition(getWorkFiles().read(8, pnd_Ws_Pnd_Input_Record)))
        {
            pnd_Ws_Pnd_Accept_Ind.setValue(false);                                                                                                                        //Natural: ASSIGN #ACCEPT-IND := FALSE
            short decideConditionsMet805 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #I-KEY-REC-LVL-#;//Natural: VALUE 10
            if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(10))))
            {
                decideConditionsMet805++;
                ldaFcpl128.getPnd_Mit_Update_Record().reset();                                                                                                            //Natural: RESET #MIT-UPDATE-RECORD
                ldaFcpl378.getPnd_Pymnt_Ext_Key().setValue(pnd_Ws_Pnd_I_Key);                                                                                             //Natural: MOVE #I-KEY TO #PYMNT-EXT-KEY
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1().setValue(pnd_Ws_Pnd_I_Rec_Det_1);                                                            //Natural: MOVE #I-REC-DET-1 TO #REC-TYPE-10-DET-1
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2().setValue(pnd_Ws_Pnd_I_Rec_Det_2);                                                            //Natural: MOVE #I-REC-DET-2 TO #REC-TYPE-10-DET-2
                pnd_Ws_Pymnt_Split_Reasn_Cde.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Split_Reasn_Cde());                                                      //Natural: ASSIGN #WS.PYMNT-SPLIT-REASN-CDE := #REC-TYPE-10-DETAIL.PYMNT-SPLIT-REASN-CDE
                //* SWAT OR STS(STS='m' SWAT=ALL OTHR)
                if (condition(pnd_Ws_Swat_Frqncy_Cde.notEquals(" ") && pnd_Ws_Pymnt_Init_Rqst_Ind.notEquals("Y")))                                                        //Natural: IF #WS.SWAT-FRQNCY-CDE NE ' ' AND #WS.PYMNT-INIT-RQST-IND NE 'Y'
                {
                    pnd_Ws_Pnd_Repeated_Swat_Sts_Pymnt.setValue(true);                                                                                                    //Natural: ASSIGN #REPEATED-SWAT-STS-PYMNT := TRUE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Repeated_Swat_Sts_Pymnt.setValue(false);                                                                                                   //Natural: ASSIGN #REPEATED-SWAT-STS-PYMNT := FALSE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals(" ")))                                                       //Natural: IF #REC-TYPE-10-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE ' '
                {
                    pnd_Ws_Pnd_I.setValue(7);                                                                                                                             //Natural: ASSIGN #I := 7
                    //*  NON CNR PAYMENT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IF A NON-SWAT-STS
                    //*  REMOVED STS (='m')
                    if (condition(pnd_Ws_Swat_Frqncy_Cde.equals(" ")))                                                                                                    //Natural: IF #WS.SWAT-FRQNCY-CDE EQ ' '
                    {
                        //*  NON-SWAT-STS & ACFS
                        if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().getBoolean()))                                                                      //Natural: IF #REC-TYPE-10-DETAIL.#ACFS
                        {
                            pnd_Ws_Pnd_I.setValue(1);                                                                                                                     //Natural: ASSIGN #I := 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  NON-SWAT-STS & NOT-ACFS
                            pnd_Ws_Pnd_Accept_Ind.setValue(true);                                                                                                         //Natural: MOVE TRUE TO #ACCEPT-IND
                            pnd_Ws_Pnd_I.setValue(2);                                                                                                                     //Natural: ASSIGN #I := 2
                        }                                                                                                                                                 //Natural: END-IF
                        //*  IT IS A SWAT OR STS
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  UPDATE MIT FOR INITIAL SWAT ONLY
                        if (condition(pnd_Ws_Pymnt_Init_Rqst_Ind.equals("Y")))                                                                                            //Natural: IF #WS.PYMNT-INIT-RQST-IND EQ 'Y'
                        {
                            //*  INIT-SWAT-STS & ACFS
                            if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().getBoolean()))                                                                  //Natural: IF #REC-TYPE-10-DETAIL.#ACFS
                            {
                                pnd_Ws_Pnd_I.setValue(3);                                                                                                                 //Natural: ASSIGN #I := 3
                                //*  INIT-SWAT-STS & NOT-ACFS
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ws_Pnd_I.setValue(4);                                                                                                                 //Natural: ASSIGN #I := 4
                                pnd_Ws_Pnd_Accept_Ind.setValue(true);                                                                                                     //Natural: MOVE TRUE TO #ACCEPT-IND
                            }                                                                                                                                             //Natural: END-IF
                            //*  FOR REPEATED-SWAT-STS - COUNT FOR CONTROL
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  REPEATED-SWAT-STS & ACFS
                            if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().getBoolean()))                                                                  //Natural: IF #REC-TYPE-10-DETAIL.#ACFS
                            {
                                pnd_Ws_Pnd_I.setValue(5);                                                                                                                 //Natural: ASSIGN #I := 5
                                //*  REPEATED-SWAT-STS & NOT-ACFS
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ws_Pnd_I.setValue(6);                                                                                                                 //Natural: ASSIGN #I := 6
                            }                                                                                                                                             //Natural: END-IF
                            //*  INIT OR REPEATED SWAT-STS
                        }                                                                                                                                                 //Natural: END-IF
                        //*  SWAT-STS OR NON-SWAT-STS
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //* *&&
                pnd_Ws_Pnd_Records_10.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-10 ( #I )
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(20))))
            {
                decideConditionsMet805++;
                pnd_Ws_Pnd_Records_20.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-20 ( #I )
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(30))))
            {
                decideConditionsMet805++;
                pnd_Ws_Pnd_Records_30.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-30 ( #I )
            }                                                                                                                                                             //Natural: VALUE 40
            else if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(40))))
            {
                decideConditionsMet805++;
                pnd_Ws_Pnd_Records_40.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-40 ( #I )
                if (condition(pnd_Ws_Pnd_I_Key_Rec_Occur_Pnd.equals(1)))                                                                                                  //Natural: IF #I-KEY-REC-OCCUR-# = 01
                {
                    if (condition(pnd_Ws_Pnd_Repeated_Swat_Sts_Pymnt.getBoolean()))                                                                                       //Natural: IF #REPEATED-SWAT-STS-PYMNT
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1().setValue(pnd_Ws_Pnd_I_Rec_Det_1);                                                    //Natural: MOVE #I-REC-DET-1 TO #REC-TYPE-40-DET-1
                        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2().setValue(pnd_Ws_Pnd_I_Rec_Det_2);                                                    //Natural: MOVE #I-REC-DET-2 TO #REC-TYPE-40-DET-2
                        pnd_Ws_Pnd_Accept_Ind.setValue(true);                                                                                                             //Natural: MOVE TRUE TO #ACCEPT-IND
                        //*             MOVE  #REC-TYPE-40-DETAIL.#GOOD-LETTER
                        //*              TO   #MIT-UPDATE-RECORD.#GOOD-LETTER
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 45
            else if (condition((pnd_Ws_Pnd_I_Key_Rec_Lvl_Pnd.equals(45))))
            {
                decideConditionsMet805++;
                pnd_Ws_Pnd_Records_45.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-45 ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Records_Other.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                  //Natural: ADD 1 TO #RECORDS-OTHER ( #I )
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(!(pnd_Ws_Pnd_Accept_Ind.equals(true))))                                                                                                         //Natural: ACCEPT IF #ACCEPT-IND = TRUE
            {
                continue;
            }
            ldaFcpl128.getPnd_Mit_Update_Record().setValuesByName(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10());                                             //Natural: MOVE BY NAME #RECORD-TYPE-10 TO #MIT-UPDATE-RECORD
            pnd_Ws_Pnd_Ss_Recs_Written.nadd(1);                                                                                                                           //Natural: ADD 1 TO #SS-RECS-WRITTEN
            pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                           //Natural: ADD 1 TO #RECORDS-WRITTEN
            getWorkFiles().write(9, false, ldaFcpl128.getPnd_Mit_Update_Record());                                                                                        //Natural: WRITE WORK FILE 09 #MIT-UPDATE-RECORD
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM READ-NZ-EXTRACT
        sub_Read_Nz_Extract();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CREATE-HEADER
        sub_Create_Header();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Records_10.getValue(8).nadd(pnd_Ws_Pnd_Records_10.getValue(1,":",4));                                                                                  //Natural: ADD #RECORDS-10 ( 1:4 ) TO #RECORDS-10 ( 8 )
        pnd_Ws_Pnd_Records_20.getValue(8).nadd(pnd_Ws_Pnd_Records_20.getValue(1,":",4));                                                                                  //Natural: ADD #RECORDS-20 ( 1:4 ) TO #RECORDS-20 ( 8 )
        pnd_Ws_Pnd_Records_30.getValue(8).nadd(pnd_Ws_Pnd_Records_30.getValue(1,":",4));                                                                                  //Natural: ADD #RECORDS-30 ( 1:4 ) TO #RECORDS-30 ( 8 )
        pnd_Ws_Pnd_Records_40.getValue(8).nadd(pnd_Ws_Pnd_Records_40.getValue(1,":",4));                                                                                  //Natural: ADD #RECORDS-40 ( 1:4 ) TO #RECORDS-40 ( 8 )
        pnd_Ws_Pnd_Records_45.getValue(8).nadd(pnd_Ws_Pnd_Records_45.getValue(1,":",4));                                                                                  //Natural: ADD #RECORDS-45 ( 1:4 ) TO #RECORDS-45 ( 8 )
        pnd_Ws_Pnd_Records_Other.getValue(8).nadd(pnd_Ws_Pnd_Records_Other.getValue(1,":",4));                                                                            //Natural: ADD #RECORDS-OTHER ( 1:4 ) TO #RECORDS-OTHER ( 8 )
        pnd_Ws_Pnd_Records_10.getValue(9).nadd(pnd_Ws_Pnd_Records_10.getValue(5,":",7));                                                                                  //Natural: ADD #RECORDS-10 ( 5:7 ) TO #RECORDS-10 ( 9 )
        pnd_Ws_Pnd_Records_20.getValue(9).nadd(pnd_Ws_Pnd_Records_20.getValue(5,":",7));                                                                                  //Natural: ADD #RECORDS-20 ( 5:7 ) TO #RECORDS-20 ( 9 )
        pnd_Ws_Pnd_Records_30.getValue(9).nadd(pnd_Ws_Pnd_Records_30.getValue(5,":",7));                                                                                  //Natural: ADD #RECORDS-30 ( 5:7 ) TO #RECORDS-30 ( 9 )
        pnd_Ws_Pnd_Records_40.getValue(9).nadd(pnd_Ws_Pnd_Records_40.getValue(5,":",7));                                                                                  //Natural: ADD #RECORDS-40 ( 5:7 ) TO #RECORDS-40 ( 9 )
        pnd_Ws_Pnd_Records_45.getValue(9).nadd(pnd_Ws_Pnd_Records_45.getValue(5,":",7));                                                                                  //Natural: ADD #RECORDS-45 ( 5:7 ) TO #RECORDS-45 ( 9 )
        pnd_Ws_Pnd_Records_Other.getValue(9).nadd(pnd_Ws_Pnd_Records_Other.getValue(5,":",7));                                                                            //Natural: ADD #RECORDS-OTHER ( 5:7 ) TO #RECORDS-OTHER ( 9 )
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 9
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(9)); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Records_All.getValue(pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_Ws_Pnd_Records_All.getValue(pnd_Ws_Pnd_I)), pnd_Ws_Pnd_Records_10.getValue(pnd_Ws_Pnd_I).add(pnd_Ws_Pnd_Records_20.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_30.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_40.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_45.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_Other.getValue(pnd_Ws_Pnd_I))); //Natural: ADD #RECORDS-10 ( #I ) #RECORDS-20 ( #I ) #RECORDS-30 ( #I ) #RECORDS-40 ( #I ) #RECORDS-45 ( #I ) #RECORDS-OTHER ( #I ) GIVING #RECORDS-ALL ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Discrepancy.compute(new ComputeParameters(false, pnd_Ws_Pnd_Discrepancy), pnd_Ws_Pnd_Records_10.getValue(8).subtract(pnd_Ws_Pnd_Ss_Recs_Written));     //Natural: ASSIGN #DISCREPANCY := #RECORDS-10 ( 8 ) - #SS-RECS-WRITTEN
        getReports().write(1, new TabSetting(14),"COUNTERS AT END OF PROGRAM (PRIOR TO CONTROL RPT):",NEWLINE,new TabSetting(17),"RECORDS READ and ACCEPTED:",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 01 ) 14T 'COUNTERS AT END OF PROGRAM (PRIOR TO CONTROL RPT):' / 17T 'RECORDS READ and ACCEPTED:' // / 17T 'GROUP A: ACCEPTED - NOT-SWAT-STS-MDO, YES-ACFS:' / 17T '............ALL:' #RECORDS-ALL ( 1 ) / 17T '........TYPE 10:' #RECORDS-10 ( 1 ) / 17T '........TYPE 20:' #RECORDS-20 ( 1 ) / 17T '........TYPE 30:' #RECORDS-30 ( 1 ) / 17T '........TYPE 40:' #RECORDS-40 ( 1 ) / 17T '........TYPE 45:' #RECORDS-45 ( 1 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 1 ) / 17T 'GROUP B: ACCEPTED - NOT-SWAT-STS-MDO, NOT-ACFS:' / 17T '............ALL:' #RECORDS-ALL ( 2 ) / 17T '........TYPE 10:' #RECORDS-10 ( 2 ) / 17T '........TYPE 20:' #RECORDS-20 ( 2 ) / 17T '........TYPE 30:' #RECORDS-30 ( 2 ) / 17T '........TYPE 40:' #RECORDS-40 ( 2 ) / 17T '........TYPE 45:' #RECORDS-45 ( 2 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 2 ) / 17T 'GROUP C: ACCEPTED - INIT-SWAT-STS-MDO, YES-ACFS:' / 17T '............ALL:' #RECORDS-ALL ( 3 ) / 17T '........TYPE 10:' #RECORDS-10 ( 3 ) / 17T '........TYPE 20:' #RECORDS-20 ( 3 ) / 17T '........TYPE 30:' #RECORDS-30 ( 3 ) / 17T '........TYPE 40:' #RECORDS-40 ( 3 ) / 17T '........TYPE 45:' #RECORDS-45 ( 3 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 3 ) / 17T 'GROUP D: ACCEPTED - INIT-SWAT-STS-MDO, NOT-ACFS:' / 17T '............ALL:' #RECORDS-ALL ( 4 ) / 17T '........TYPE 10:' #RECORDS-10 ( 4 ) / 17T '........TYPE 20:' #RECORDS-20 ( 4 ) / 17T '........TYPE 30:' #RECORDS-30 ( 4 ) / 17T '........TYPE 40:' #RECORDS-40 ( 4 ) / 17T '........TYPE 45:' #RECORDS-45 ( 4 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 4 )
            TabSetting(17),"GROUP A: ACCEPTED - NOT-SWAT-STS-MDO, YES-ACFS:",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"GROUP B: ACCEPTED - NOT-SWAT-STS-MDO, NOT-ACFS:",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"GROUP C: ACCEPTED - INIT-SWAT-STS-MDO, YES-ACFS:",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(3), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(3), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(3), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"GROUP D: ACCEPTED - INIT-SWAT-STS-MDO, NOT-ACFS:",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(4), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(4), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(4), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(4), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(4), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(4), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(4), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, new TabSetting(14),"COUNTERS AT END OF PROGRAM (PRIOR TO CONTROL RPT):",NEWLINE,new TabSetting(17),"RECORDS READ and REJECTED:",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 01 ) 14T 'COUNTERS AT END OF PROGRAM (PRIOR TO CONTROL RPT):' / 17T 'RECORDS READ and REJECTED:' // / 17T 'GROUP E: REJECTED - REPEATED-SWAT-STS-MDO, YES-ACFS:' / 17T '............ALL:' #RECORDS-ALL ( 5 ) / 17T '........TYPE 10:' #RECORDS-10 ( 5 ) / 17T '........TYPE 20:' #RECORDS-20 ( 5 ) / 17T '........TYPE 30:' #RECORDS-30 ( 5 ) / 17T '........TYPE 40:' #RECORDS-40 ( 5 ) / 17T '........TYPE 45:' #RECORDS-45 ( 5 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 5 ) / 17T 'GROUP F: REJECTED - REPEATED-SWAT-STS-MDO, NOT-ACFS:' / 17T '............ALL:' #RECORDS-ALL ( 6 ) / 17T '........TYPE 10:' #RECORDS-10 ( 6 ) / 17T '........TYPE 20:' #RECORDS-20 ( 6 ) / 17T '........TYPE 30:' #RECORDS-30 ( 6 ) / 17T '........TYPE 40:' #RECORDS-40 ( 6 ) / 17T '........TYPE 45:' #RECORDS-45 ( 6 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 6 ) / 17T 'GROUP G: Rejected - Cancel & Redraw:' / 17T '............ALL:' #RECORDS-ALL ( 7 ) / 17T '........TYPE 10:' #RECORDS-10 ( 7 ) / 17T '........TYPE 20:' #RECORDS-20 ( 7 ) / 17T '........TYPE 30:' #RECORDS-30 ( 7 ) / 17T '........TYPE 40:' #RECORDS-40 ( 7 ) / 17T '........TYPE 45:' #RECORDS-45 ( 7 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 7 )
            TabSetting(17),"GROUP E: REJECTED - REPEATED-SWAT-STS-MDO, YES-ACFS:",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(5), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(5), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(5), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(5), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(5), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(5), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(5), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"GROUP F: REJECTED - REPEATED-SWAT-STS-MDO, NOT-ACFS:",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(6), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(6), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(6), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(6), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(6), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(6), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(6), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"GROUP G: Rejected - Cancel & Redraw:",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(7), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(7), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(7), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(7), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(7), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(7), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(7), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, new TabSetting(14),"COUNTERS AT END OF PROGRAM (PRIOR TO CONTROL RPT):",NEWLINE,new TabSetting(17),"RECORDS READ TOTALS:",NEWLINE,NEWLINE,NEWLINE,new  //Natural: WRITE ( 01 ) 14T 'COUNTERS AT END OF PROGRAM (PRIOR TO CONTROL RPT):' / 17T 'RECORDS READ TOTALS:'// / 17T '  ACCEPTED (Groups A thru D):' / 17T '............ALL:' #RECORDS-ALL ( 8 ) / 17T '........TYPE 10:' #RECORDS-10 ( 8 ) / 17T '........TYPE 20:' #RECORDS-20 ( 8 ) / 17T '........TYPE 30:' #RECORDS-30 ( 8 ) / 17T '........TYPE 40:' #RECORDS-40 ( 8 ) / 17T '........TYPE 45:' #RECORDS-45 ( 8 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 8 ) / 17T '  REJECTED (Groups E thru G):' / 17T '............ALL:' #RECORDS-ALL ( 9 ) / 17T '........TYPE 10:' #RECORDS-10 ( 9 ) / 17T '........TYPE 20:' #RECORDS-20 ( 9 ) / 17T '........TYPE 30:' #RECORDS-30 ( 9 ) / 17T '........TYPE 40:' #RECORDS-40 ( 9 ) / 17T '........TYPE 45:' #RECORDS-45 ( 9 ) / 17T '..........OTHER:' #RECORDS-OTHER ( 9 ) // 17T '   SSSS RECORDS:' #SS-RECS-WRITTEN // 17T 'DISCREPANCY....:' #DISCREPANCY // 17T 'Discrepancy is the difference' 'between type 10 records Accepted and Rejected' / 17T 'and it should be equal to zero!' // 17T '   ADAM RECORDS:' #NZ-RECS-WRITTEN // 17T '   LOAN RECORDS:' #AL-RECS-WRITTEN // 17T '     EW RECORDS:' #EW-RECS-WRITTEN // 17T '  TOTAL RECORDS:' #RECORDS-WRITTEN 'SSSS + ADAM + RETIREMENT LOAN + EW'
            TabSetting(17),"  ACCEPTED (Groups A thru D):",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(8), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(8), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(8), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(8), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(8), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(8), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(8), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"  REJECTED (Groups E thru G):",NEWLINE,new TabSetting(17),"............ALL:",pnd_Ws_Pnd_Records_All.getValue(9), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(9), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(9), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(9), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"........TYPE 40:",pnd_Ws_Pnd_Records_40.getValue(9), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),"........TYPE 45:",pnd_Ws_Pnd_Records_45.getValue(9), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........OTHER:",pnd_Ws_Pnd_Records_Other.getValue(9), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"   SSSS RECORDS:",pnd_Ws_Pnd_Ss_Recs_Written, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(17),"DISCREPANCY....:",pnd_Ws_Pnd_Discrepancy, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"Discrepancy is the difference","between type 10 records Accepted and Rejected",NEWLINE,new 
            TabSetting(17),"and it should be equal to zero!",NEWLINE,NEWLINE,new TabSetting(17),"   ADAM RECORDS:",pnd_Ws_Pnd_Nz_Recs_Written, new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"   LOAN RECORDS:",pnd_Ws_Pnd_Al_Recs_Written, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(17),"     EW RECORDS:",pnd_Ws_Pnd_Ew_Recs_Written, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"  TOTAL RECORDS:",pnd_Ws_Pnd_Records_Written, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),"SSSS + ADAM + RETIREMENT LOAN + EW");
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Discrepancy.notEquals(getZero())))                                                                                                       //Natural: IF #DISCREPANCY NE 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(17),"*",new RepeatItem(50),NEWLINE,new TabSetting(17),"*",new RepeatItem(50),NEWLINE,new //Natural: WRITE ( 01 ) /// / 17T '*' ( 50 ) / 17T '*' ( 50 ) / 17T '**' / 17T '**   WE HAVE A PROBLEM!!!!!!!!!!' / 17T '**' / 17T '*' ( 50 ) / 17T '*' ( 50 )
                TabSetting(17),"**",NEWLINE,new TabSetting(17),"**   WE HAVE A PROBLEM!!!!!!!!!!",NEWLINE,new TabSetting(17),"**",NEWLINE,new TabSetting(17),"*",new 
                RepeatItem(50),NEWLINE,new TabSetting(17),"*",new RepeatItem(50));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new              //Natural: WRITE ( 01 ) //// 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************'
            TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************");
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NZ-EXTRACT
        //* *--------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-NZ-RECORD
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-HEADER
        //* *------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 48T 'SSSS/ADAM/LOAN/EW BATCH CONTROL REPORT' 120T 'REPORT: RPT1' //
    }
    private void sub_Read_Nz_Extract() throws Exception                                                                                                                   //Natural: READ-NZ-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Records_Found.setValue(false);                                                                                                                                //Natural: ASSIGN #RECORDS-FOUND := FALSE
        //*  READ  WORK FILE 07  RECORD  NZ-PYMNT-ADDR               /* ROXAN
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 07 RECORD EXT ( * )
        while (condition(getWorkFiles().read(7, pdaFcpaext.getExt().getValue("*"))))
        {
            if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("NZ")))                                                                                              //Natural: IF EXT.CNTRCT-ORGN-CDE = 'NZ'
            {
                pnd_Ws_Pnd_Nz_Recs_Read.nadd(1);                                                                                                                          //Natural: ADD 1 TO #NZ-RECS-READ
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("AL")))                                                                                          //Natural: IF EXT.CNTRCT-ORGN-CDE = 'AL'
                {
                    pnd_Ws_Pnd_Al_Recs_Read.nadd(1);                                                                                                                      //Natural: ADD 1 TO #AL-RECS-READ
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW")))                                                                                      //Natural: IF EXT.CNTRCT-ORGN-CDE = 'EW'
                    {
                        pnd_Ws_Pnd_Ew_Recs_Read.nadd(1);                                                                                                                  //Natural: ADD 1 TO #EW-RECS-READ
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  FIRST NZ RECORD
            if (condition(pnd_Nz_Seq_Pnd_Pymnt_Reqst_Log_Dte_Time.equals(" ")))                                                                                           //Natural: IF #PYMNT-REQST-LOG-DTE-TIME = ' '
            {
                ldaFcpl128.getPnd_Mit_Update_Record().reset();                                                                                                            //Natural: RESET #MIT-UPDATE-RECORD
                pnd_Nz_Seq_Pnd_Pymnt_Reqst_Log_Dte_Time.setValue(pdaFcpaext.getExt_Pymnt_Reqst_Log_Dte_Time());                                                           //Natural: ASSIGN #PYMNT-REQST-LOG-DTE-TIME := EXT.PYMNT-REQST-LOG-DTE-TIME
                pnd_Nz_Seq_Pnd_Cntrct_Unq_Id_Nr.setValue(pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr());                                                                          //Natural: ASSIGN #CNTRCT-UNQ-ID-NR := EXT.CNTRCT-UNQ-ID-NBR
                pnd_Nz_Seq_Pnd_Pymnt_Prcss_Seq_Nbr.setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                                     //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := EXT.PYMNT-PRCSS-SEQ-NBR
                pnd_Last_Cntrct_Orgn_Cde.setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                                   //Natural: ASSIGN #LAST-CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
                pnd_Records_Found.setValue(true);                                                                                                                         //Natural: ASSIGN #RECORDS-FOUND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  WRITE            /* <====================== RS TEMPORARY
            //*    'log' NZ-EXT.PYMNT-REQST-LOG-DTE-TIME
            //*    'id'  NZ-EXT.CNTRCT-UNQ-ID-NBR
            //*    'seq' NZ-EXT.PYMNT-PRCSS-SEQ-NBR
            if (condition(pdaFcpaext.getExt_Pymnt_Reqst_Log_Dte_Time().notEquals(pnd_Nz_Seq_Pnd_Pymnt_Reqst_Log_Dte_Time) || pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr().notEquals(pnd_Nz_Seq_Pnd_Cntrct_Unq_Id_Nr)  //Natural: IF EXT.PYMNT-REQST-LOG-DTE-TIME NE #PYMNT-REQST-LOG-DTE-TIME OR EXT.CNTRCT-UNQ-ID-NBR NE #CNTRCT-UNQ-ID-NR OR EXT.CNTRCT-ORGN-CDE NE #LAST-CNTRCT-ORGN-CDE
                || pdaFcpaext.getExt_Cntrct_Orgn_Cde().notEquals(pnd_Last_Cntrct_Orgn_Cde)))
            {
                //*  OR NZ-EXT.PYMNT-PRCSS-SEQ-NBR       NE  #PYMNT-PRCSS-SEQ-NBR
                                                                                                                                                                          //Natural: PERFORM WRITE-NZ-RECORD
                sub_Write_Nz_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time().setValue(pdaFcpaext.getExt_Pymnt_Reqst_Log_Dte_Time());                                        //Natural: MOVE EXT.PYMNT-REQST-LOG-DTE-TIME TO #MIT-UPDATE-RECORD.PYMNT-REQST-LOG-DTE-TIME
            ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr().setValue(pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr());                                                      //Natural: MOVE EXT.CNTRCT-UNQ-ID-NBR TO #MIT-UPDATE-RECORD.CNTRCT-UNQ-ID-NBR
            ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr().setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                  //Natural: MOVE EXT.PYMNT-PRCSS-SEQ-NBR TO #MIT-UPDATE-RECORD.PYMNT-PRCSS-SEQ-NBR
            ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind().setValue(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind());                                            //Natural: MOVE EXT.PYMNT-PAY-TYPE-REQ-IND TO #MIT-UPDATE-RECORD.PYMNT-PAY-TYPE-REQ-IND
            ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Payee_Cde().setValue(pdaFcpaext.getExt_Cntrct_Payee_Cde());                                                        //Natural: MOVE EXT.CNTRCT-PAYEE-CDE TO #MIT-UPDATE-RECORD.CNTRCT-PAYEE-CDE
            ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Check_Dte().setValue(pdaFcpaext.getExt_Pymnt_Check_Dte());                                                          //Natural: MOVE EXT.PYMNT-CHECK-DTE TO #MIT-UPDATE-RECORD.PYMNT-CHECK-DTE
            if (condition(pdaFcpaext.getExt_Cntrct_Hold_Cde().notEquals(" ")))                                                                                            //Natural: IF EXT.CNTRCT-HOLD-CDE NE ' '
            {
                ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Hold_Cde().setValue(pdaFcpaext.getExt_Cntrct_Hold_Cde());                                                      //Natural: MOVE EXT.CNTRCT-HOLD-CDE TO #MIT-UPDATE-RECORD.CNTRCT-HOLD-CDE
            }                                                                                                                                                             //Natural: END-IF
            //*                                               /* ROXAN
            if (condition(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind().equals("N") && pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("X")))                              //Natural: IF EXT.CNTRCT-PYMNT-TYPE-IND = 'N' AND EXT.CNTRCT-STTLMNT-TYPE-IND = 'X'
            {
                pnd_Pnd_Rtb.setValue(true);                                                                                                                               //Natural: ASSIGN ##RTB := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*   IF EXT.CNTRCT-ORGN-CDE = 'NZ'
            //*      IF ##RTB = TRUE
            //*         #MIT-UPDATE-RECORD.#RTB           :=  TRUE
            //*      ELSE /*    ORIGIN-CODE = 'AL'
            //*         #MIT-UPDATE-RECORD.#PERIODIC-PAY  :=  TRUE
            //*      END-IF
            //*   ELSE
            //*      #MIT-UPDATE-RECORD.#RTB           :=  FALSE
            //*      #MIT-UPDATE-RECORD.#PERIODIC-PAY  :=  FALSE
            //*   END-IF
            //* *********************
            //* *WRITE            /* <====================== RS TEMPORARY
            //* *  'log'  #MIT-UPDATE-RECORD.PYMNT-REQST-LOG-DTE-TIME
            //* *  'id'   #MIT-UPDATE-RECORD.CNTRCT-UNQ-ID-NBR
            //* *  'seq'  #MIT-UPDATE-RECORD.PYMNT-PRCSS-SEQ-NBR
            //* *  '.... saved'
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*   NZ FILE NOT EMPTY
        if (condition(pnd_Records_Found.equals(true)))                                                                                                                    //Natural: IF #RECORDS-FOUND = TRUE
        {
            //*   LAST NZ RECORD
                                                                                                                                                                          //Natural: PERFORM WRITE-NZ-RECORD
            sub_Write_Nz_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Nz_Record() throws Exception                                                                                                                   //Natural: WRITE-NZ-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        //* *WRITE            /* <====================== RS TEMPORARY
        //* *  'log'  #MIT-UPDATE-RECORD.PYMNT-REQST-LOG-DTE-TIME
        //* *  'id'   #MIT-UPDATE-RECORD.CNTRCT-UNQ-ID-NBR
        //* *  'seq'  #MIT-UPDATE-RECORD.PYMNT-PRCSS-SEQ-NBR
        //* *  'hold' #MIT-UPDATE-RECORD.CNTRCT-HOLD-CDE
        //* *  'pp '  #PERIODIC-PAY
        //* *  'rtb'  #RTB
        //* ***IF NZ-EXT.CNTRCT-ORGN-CDE  =  'NZ'        /*  PREV  04/27/99 LEON
        //*   NEW   04/27/99 LEON
        if (condition(pnd_Last_Cntrct_Orgn_Cde.equals("NZ")))                                                                                                             //Natural: IF #LAST-CNTRCT-ORGN-CDE = 'NZ'
        {
            ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Orgn_Cde().setValue("NZ");                                                                                         //Natural: ASSIGN #MIT-UPDATE-RECORD.CNTRCT-ORGN-CDE := 'NZ'
            pnd_Ws_Pnd_Nz_Recs_Written.nadd(1);                                                                                                                           //Natural: ADD 1 TO #NZ-RECS-WRITTEN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Last_Cntrct_Orgn_Cde.equals("AL")))                                                                                                         //Natural: IF #LAST-CNTRCT-ORGN-CDE = 'AL'
            {
                ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Orgn_Cde().setValue("AL");                                                                                     //Natural: ASSIGN #MIT-UPDATE-RECORD.CNTRCT-ORGN-CDE := 'AL'
                pnd_Ws_Pnd_Al_Recs_Written.nadd(1);                                                                                                                       //Natural: ADD 1 TO #AL-RECS-WRITTEN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Last_Cntrct_Orgn_Cde.equals("EW")))                                                                                                     //Natural: IF #LAST-CNTRCT-ORGN-CDE = 'EW'
                {
                    ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Orgn_Cde().setValue("EW");                                                                                 //Natural: ASSIGN #MIT-UPDATE-RECORD.CNTRCT-ORGN-CDE := 'EW'
                    pnd_Ws_Pnd_Ew_Recs_Written.nadd(1);                                                                                                                   //Natural: ADD 1 TO #EW-RECS-WRITTEN
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(9, false, ldaFcpl128.getPnd_Mit_Update_Record());                                                                                            //Natural: WRITE WORK FILE 09 #MIT-UPDATE-RECORD
        pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                               //Natural: ADD 1 TO #RECORDS-WRITTEN
        ldaFcpl128.getPnd_Mit_Update_Record().reset();                                                                                                                    //Natural: RESET #MIT-UPDATE-RECORD
        pnd_Nz_Seq_Pnd_Pymnt_Reqst_Log_Dte_Time.setValue(pdaFcpaext.getExt_Pymnt_Reqst_Log_Dte_Time());                                                                   //Natural: ASSIGN #PYMNT-REQST-LOG-DTE-TIME := EXT.PYMNT-REQST-LOG-DTE-TIME
        pnd_Nz_Seq_Pnd_Cntrct_Unq_Id_Nr.setValue(pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr());                                                                                  //Natural: ASSIGN #CNTRCT-UNQ-ID-NR := EXT.CNTRCT-UNQ-ID-NBR
        pnd_Nz_Seq_Pnd_Pymnt_Prcss_Seq_Nbr.setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                                             //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := EXT.PYMNT-PRCSS-SEQ-NBR
        pnd_Last_Cntrct_Orgn_Cde.setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                                           //Natural: ASSIGN #LAST-CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
    }
    private void sub_Create_Header() throws Exception                                                                                                                     //Natural: CREATE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------
        ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Timestmp().setValue(Global.getTIMESTMP());                                                                                //Natural: MOVE *TIMESTMP TO #TIMESTMP
        ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Date_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("MM/DD/YYYY�HH:II:SS.T"));                                 //Natural: MOVE EDITED *TIMX ( EM = MM/DD/YYYY�HH:II:SS.T ) TO #DATE-TIME
        ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Rec_Cnt().setValue(pnd_Ws_Pnd_Records_Written);                                                                           //Natural: MOVE #RECORDS-WRITTEN TO #REC-CNT
        getWorkFiles().write(9, false, ldaFcpl128.getPnd_Mit_Header_Record());                                                                                            //Natural: WRITE WORK FILE 09 #MIT-HEADER-RECORD
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON");
        Global.format(1, "LS=133 PS=60 ZP=ON");
        Global.format(2, "LS=133 PS=60 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(48),"SSSS/ADAM/LOAN/EW BATCH CONTROL REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
}
