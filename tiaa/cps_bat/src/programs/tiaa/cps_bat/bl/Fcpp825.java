/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:29 PM
**        * FROM NATURAL PROGRAM : Fcpp825
************************************************************
**        * FILE NAME            : Fcpp825.java
**        * CLASS NAME           : Fcpp825
**        * INSTANCE NAME        : Fcpp825
************************************************************
************************************************************************
* PROGRAM  : FCPP825
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS "AP" LEDGER INTERFACE REPORT
* CREATED  : 02/16/96
* HISTORY  : 04/03/1997    RITA SALGADO
*          : - GET ISA CODE FROM FCPL199B INSTEAD OF USING FCPN801
*
* 05/22/00 : MCGEE
*          : STOW - FCPL199B
* 06/11/02 : CARREON
*          : STOW - FCPL199B
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp825 extends BLNatBase
{
    // Data Areas
    private PdaFcpa121 pdaFcpa121;
    private LdaFcpl801c ldaFcpl801c;
    private LdaFcpl199b ldaFcpl199b;
    private LdaFcpl826 ldaFcpl826;
    private LdaFcpl827 ldaFcpl827;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Key;

    private DbsGroup pnd_Ws__R_Field_1;

    private DbsGroup pnd_Ws_Pnd_Key_Detail;
    private DbsField pnd_Ws_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ws_Inv_Acct_Cde;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Inv_Acct_Cde_X;
    private DbsField pnd_Ws_Pnd_Rec_Cnt;
    private DbsField pnd_Ws_Pnd_Cr_Total;
    private DbsField pnd_Ws_Pnd_Dr_Total;
    private DbsField pnd_Ws_Pnd_Isa_Sub_A;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Isa_Sub;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa121 = new PdaFcpa121(localVariables);
        ldaFcpl801c = new LdaFcpl801c();
        registerRecord(ldaFcpl801c);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);
        ldaFcpl826 = new LdaFcpl826();
        registerRecord(ldaFcpl826);
        ldaFcpl827 = new LdaFcpl827();
        registerRecord(ldaFcpl827);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Key = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Key", "#KEY", FieldType.STRING, 3);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Key);

        pnd_Ws_Pnd_Key_Detail = pnd_Ws__R_Field_1.newGroupInGroup("pnd_Ws_Pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Ws_Cntrct_Annty_Ins_Type = pnd_Ws_Pnd_Key_Detail.newFieldInGroup("pnd_Ws_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 
            1);
        pnd_Ws_Inv_Acct_Cde = pnd_Ws_Pnd_Key_Detail.newFieldInGroup("pnd_Ws_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.NUMERIC, 2);

        pnd_Ws__R_Field_2 = pnd_Ws_Pnd_Key_Detail.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Inv_Acct_Cde);
        pnd_Ws_Inv_Acct_Cde_X = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Inv_Acct_Cde_X", "INV-ACCT-CDE-X", FieldType.STRING, 2);
        pnd_Ws_Pnd_Rec_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Cr_Total = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cr_Total", "#CR-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Pnd_Dr_Total = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dr_Total", "#DR-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Pnd_Isa_Sub_A = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isa_Sub_A", "#ISA-SUB-A", FieldType.STRING, 2);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Isa_Sub_A);
        pnd_Ws_Pnd_Isa_Sub = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Isa_Sub", "#ISA-SUB", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl801c.initializeValues();
        ldaFcpl199b.initializeValues();
        ldaFcpl826.initializeValues();
        ldaFcpl827.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp825() throws Exception
    {
        super("Fcpp825");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 59T 'CONTROL REPORT' 120T 'REPORT: RPT1' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*  CHECK FOR HEADER
                                                                                                                                                                          //Natural: PERFORM CHECK-HEADER
        sub_Check_Header();
        if (condition(Global.isEscape())) {return;}
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #LEDGER-EXT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl801c.getPnd_Ledger_Ext())))
        {
            CheckAtStartofData139();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //* *WRITE (1)
            //* *'='  #LEDGER-EXT.CNTRCT-ANNTY-INS-TYPE
            //* *'='  #LEDGER-EXT.INV-ISA-HASH
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            //*    MOVE BY NAME #LEDGER-EXT           TO #KEY-DETAIL
            pnd_Ws_Cntrct_Annty_Ins_Type.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Annty_Ins_Type());                                                                 //Natural: MOVE #LEDGER-EXT.CNTRCT-ANNTY-INS-TYPE TO #WS.CNTRCT-ANNTY-INS-TYPE
            pnd_Ws_Inv_Acct_Cde_X.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash());                                                                                 //Natural: MOVE #LEDGER-EXT.INV-ISA-HASH TO #WS.INV-ACCT-CDE-X
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
                                                                                                                                                                          //Natural: PERFORM LGR-INTRFCE-RPT
            sub_Lgr_Intrfce_Rpt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM LGR-ACCT-RPT
            sub_Lgr_Acct_Rpt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ACCUM CONTROLS
            pnd_Ws_Pnd_Rec_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-CNT
            if (condition(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind().equals("C")))                                                                                //Natural: IF #LEDGER-EXT.INV-ACCT-LEDGR-IND = 'C'
            {
                pnd_Ws_Pnd_Cr_Total.nadd(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                                                                             //Natural: ADD #LEDGER-EXT.INV-ACCT-LEDGR-AMT TO #CR-TOTAL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Dr_Total.nadd(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                                                                             //Natural: ADD #LEDGER-EXT.INV-ACCT-LEDGR-AMT TO #DR-TOTAL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //*  DISPLAY CONTROLS
        getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(25),"RECORDS READ",                                    //Natural: DISPLAY ( 1 ) ( HC = R ) 25T 'RECORDS READ' #REC-CNT 'CREDIT TOTAL' #CR-TOTAL 'DEBIT TOTAL' #DR-TOTAL
        		pnd_Ws_Pnd_Rec_Cnt,"CREDIT TOTAL",
        		pnd_Ws_Pnd_Cr_Total,"DEBIT TOTAL",
        		pnd_Ws_Pnd_Dr_Total);
        if (Global.isEscape()) return;
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LGR-ACCT-RPT
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LGR-INTRFCE-RPT
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-COMPANY
        //* *****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-HEADER
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Lgr_Acct_Rpt() throws Exception                                                                                                                      //Natural: LGR-ACCT-RPT
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        ldaFcpl827.getPnd_Lgr_Acct_Rpt().setValuesByName(ldaFcpl801c.getPnd_Ledger_Ext());                                                                                //Natural: MOVE BY NAME #LEDGER-EXT TO #LGR-ACCT-RPT
        ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Cde_X().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash());                                                           //Natural: MOVE #LEDGER-EXT.INV-ISA-HASH TO #LGR-ACCT-RPT.INV-ACCT-CDE-X
        if (condition(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Annty_Type_Cde().equals("M")))                                                                                 //Natural: IF #LEDGER-EXT.CNTRCT-ANNTY-TYPE-CDE = 'M'
        {
            ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Life_Contingency().setValue("A");                                                                                       //Natural: ASSIGN #LGR-ACCT-RPT.CNTRCT-LIFE-CONTINGENCY := 'A'
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(3, false, ldaFcpl827.getPnd_Lgr_Acct_Rpt());                                                                                                 //Natural: WRITE WORK FILE 3 #LGR-ACCT-RPT
    }
    private void sub_Lgr_Intrfce_Rpt() throws Exception                                                                                                                   //Natural: LGR-INTRFCE-RPT
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        ldaFcpl826.getPnd_Lgr_Intrfce_Rpt().setValuesByName(ldaFcpl801c.getPnd_Ledger_Ext());                                                                             //Natural: MOVE BY NAME #LEDGER-EXT TO #LGR-INTRFCE-RPT
        ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Inv_Acct_Cde_X().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash());                                                        //Natural: MOVE #LEDGER-EXT.INV-ISA-HASH TO #LGR-INTRFCE-RPT.INV-ACCT-CDE-X
        if (condition(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Annty_Ins_Type().equals("G")))                                                                                 //Natural: IF #LEDGER-EXT.CNTRCT-ANNTY-INS-TYPE = 'G'
        {
            ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Cntrct_Annty_Ins_Type().setValue("A");                                                                                      //Natural: ASSIGN #LGR-INTRFCE-RPT.CNTRCT-ANNTY-INS-TYPE := 'A'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Cntrct_Annty_Ins_Type().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Annty_Ins_Type());                                    //Natural: ASSIGN #LGR-INTRFCE-RPT.CNTRCT-ANNTY-INS-TYPE := #LEDGER-EXT.CNTRCT-ANNTY-INS-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().write(2, false, ldaFcpl826.getPnd_Lgr_Intrfce_Rpt());                                                                                              //Natural: WRITE WORK FILE 2 #LGR-INTRFCE-RPT
    }
    private void sub_Get_Company() throws Exception                                                                                                                       //Natural: GET-COMPANY
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        pnd_Ws_Pnd_Isa_Sub_A.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash());                                                                                      //Natural: MOVE #LEDGER-EXT.INV-ISA-HASH TO #ISA-SUB-A
        ldaFcpl826.getPnd_Lgr_Intrfce_Rpt_Acct_Company().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Isa_Sub.getInt() + 1));                    //Natural: ASSIGN #LGR-INTRFCE-RPT.ACCT-COMPANY := #ISA-LDA.#ISA-CDE ( #ISA-SUB )
        ldaFcpl827.getPnd_Lgr_Acct_Rpt_Acct_Company().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Isa_Sub.getInt() + 1));                       //Natural: ASSIGN #LGR-ACCT-RPT.ACCT-COMPANY := #ISA-LDA.#ISA-CDE ( #ISA-SUB )
        //*  WRITE (1)
        //*   / '==========================='
        //*   / '=' #LEDGER-EXT.INV-ISA-HASH  '='  #ISA-SUB-A
        //*   / '=' #LGR-INTRFCE-RPT.ACCT-COMPANY
        //*  / '=' #LGR-ACCT-RPT.ACCT-COMPANY
        //*   / '==========================='
    }
    private void sub_Check_Header() throws Exception                                                                                                                      //Natural: CHECK-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************
        getWorkFiles().read(1, ldaFcpl801c.getPnd_Ledger_Header());                                                                                                       //Natural: READ WORK FILE 1 ONCE RECORD #LEDGER-HEADER
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"INPUT FILE IS EMPTY",new TabSetting(77),"***",NEWLINE);                                                       //Natural: WRITE '***' 25T 'INPUT FILE IS EMPTY' 77T '***' /
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(ldaFcpl801c.getPnd_Ledger_Header_Pnd_Header_Text().notEquals("HD")))                                                                                //Natural: IF #HEADER-TEXT NE 'HD'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"HEADER RECORD IS NOT ON THE INPUT FILE",new TabSetting(77),"***",NEWLINE);                                    //Natural: WRITE '***' 25T 'HEADER RECORD IS NOT ON THE INPUT FILE' 77T '***' /
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Ws_Pnd_KeyIsBreak = pnd_Ws_Pnd_Key.isBreak(endOfData);
        if (condition(pnd_Ws_Pnd_KeyIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY
            sub_Get_Company();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(59),"CONTROL REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(25),"RECORDS READ",
        		pnd_Ws_Pnd_Rec_Cnt,"CREDIT TOTAL",
        		pnd_Ws_Pnd_Cr_Total,"DEBIT TOTAL",
        		pnd_Ws_Pnd_Dr_Total);
    }
    private void CheckAtStartofData139() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*    MOVE BY NAME #LEDGER-EXT           TO #KEY-DETAIL
            pnd_Ws_Cntrct_Annty_Ins_Type.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Annty_Ins_Type());                                                                 //Natural: MOVE #LEDGER-EXT.CNTRCT-ANNTY-INS-TYPE TO #WS.CNTRCT-ANNTY-INS-TYPE
            pnd_Ws_Inv_Acct_Cde_X.setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash());                                                                                 //Natural: MOVE #LEDGER-EXT.INV-ISA-HASH TO #WS.INV-ACCT-CDE-X
                                                                                                                                                                          //Natural: PERFORM GET-COMPANY
            sub_Get_Company();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
