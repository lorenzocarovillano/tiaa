/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:13 PM
**        * FROM NATURAL PROGRAM : Fcpp230a
************************************************************
**        * FILE NAME            : Fcpp230a.java
**        * CLASS NAME           : Fcpp230a
**        * INSTANCE NAME        : Fcpp230a
************************************************************
***********************************************************************
** PROGRAM:     FCPP230A                                             **
** SYSTEM:      CPS - CONSOLIDATED PAYMENT SYSTEM                    **
** DATE:        08-20-1999                                           **
** AUTHOR:      ALTHEA A. YOUNG                                      **
** DESCRIPTION: THIS PROGRAM CREATES LOW VALUE HEADER RECORDS FOR    **
**              EACH ORIGIN CODE.                                    **
**                                                                   **
***********************************************************************
** HISTORY:                                                          **
**                                                                   **
** 10/20/1999 : A. YOUNG - CREATE HEADER RECORD FOR 'VT' (PROLINE).  **
**                                                                   **
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp230a extends BLNatBase
{
    // Data Areas
    private LdaFcplorgn ldaFcplorgn;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Low_Val_Hdr_Rec;
    private DbsField pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_1;
    private DbsField pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Orgn_Cde;
    private DbsField pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_2;
    private DbsField pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_3;
    private DbsField pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_4;
    private DbsField pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_5;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplorgn = new LdaFcplorgn();
        registerRecord(ldaFcplorgn);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Low_Val_Hdr_Rec = localVariables.newGroupInRecord("pnd_Low_Val_Hdr_Rec", "#LOW-VAL-HDR-REC");
        pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_1 = pnd_Low_Val_Hdr_Rec.newFieldInGroup("pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_1", "#LOW-VAL-FILLER-1", 
            FieldType.STRING, 16);
        pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Orgn_Cde = pnd_Low_Val_Hdr_Rec.newFieldInGroup("pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Orgn_Cde", "#LOW-VAL-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_2 = pnd_Low_Val_Hdr_Rec.newFieldInGroup("pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_2", "#LOW-VAL-FILLER-2", 
            FieldType.STRING, 250);
        pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_3 = pnd_Low_Val_Hdr_Rec.newFieldInGroup("pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_3", "#LOW-VAL-FILLER-3", 
            FieldType.STRING, 250);
        pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_4 = pnd_Low_Val_Hdr_Rec.newFieldInGroup("pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_4", "#LOW-VAL-FILLER-4", 
            FieldType.STRING, 200);
        pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_5 = pnd_Low_Val_Hdr_Rec.newFieldArrayInGroup("pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_5", "#LOW-VAL-FILLER-5", 
            FieldType.STRING, 83, new DbsArrayController(1, 40));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplorgn.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp230a() throws Exception
    {
        super("Fcpp230a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 T-MAX
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcplorgn.getT_Max())); pnd_I.nadd(1))
        {
            if (condition(pnd_I.equals(1)))                                                                                                                               //Natural: IF #I = 1
            {
                pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_1.moveAll("H'00'");                                                                                                //Natural: MOVE ALL H'00' TO #LOW-VAL-FILLER-1
                pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_2.moveAll("H'00'");                                                                                                //Natural: MOVE ALL H'00' TO #LOW-VAL-FILLER-2
                pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_3.moveAll("H'00'");                                                                                                //Natural: MOVE ALL H'00' TO #LOW-VAL-FILLER-3
                pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_4.moveAll("H'00'");                                                                                                //Natural: MOVE ALL H'00' TO #LOW-VAL-FILLER-4
                pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Filler_5.getValue("*").moveAll("H'00'");                                                                                  //Natural: MOVE ALL H'00' TO #LOW-VAL-FILLER-5 ( * )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcplorgn.getT_Orgn_Cde().getValue(pnd_I).equals(" ")))                                                                                       //Natural: IF T-ORGN-CDE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*    IF NOT (T-ORGN-CDE (#I) = 'VT')
                pnd_Low_Val_Hdr_Rec_Pnd_Low_Val_Orgn_Cde.setValue(ldaFcplorgn.getT_Orgn_Cde().getValue(pnd_I));                                                           //Natural: ASSIGN #LOW-VAL-ORGN-CDE := T-ORGN-CDE ( #I )
                getWorkFiles().write(1, true, pnd_Low_Val_Hdr_Rec);                                                                                                       //Natural: WRITE WORK FILE 1 VARIABLE #LOW-VAL-HDR-REC
                //*    END-IF                                                 /* 10-20-1999
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //
}
