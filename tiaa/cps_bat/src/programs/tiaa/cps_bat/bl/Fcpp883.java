/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:15 PM
**        * FROM NATURAL PROGRAM : Fcpp883
************************************************************
**        * FILE NAME            : Fcpp883.java
**        * CLASS NAME           : Fcpp883
**        * INSTANCE NAME        : Fcpp883
************************************************************
************************************************************************
* PROGRAM  : FCPP883
* SYSTEM   : CPS
* TITLE    : AP LAYOUT CONVERSION BEFORE PROCESSING FCPP195
* FUNCTION : CONVERTS AP LAYOUT FCPA800 TO #OUTPUT IN FCPL190
* DATE     : 09/09/1997 - LIN ZHENG
*            FUND INFORMATION IS NOT FILLED IN. IT IS NOT NEEDED.
*
* HISTORY
* 5/15/08  : AER - ROTH-MAJOR1 - CHANGED FCPA800 TO FCPA800D
* 4/2017   : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp883 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800d pdaFcpa800d;
    private LdaFcpl190a ldaFcpl190a;

    // Local Variables
    public DbsRecord localVariables;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp883() throws Exception
    {
        super("Fcpp883");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 PYMNT-ADDR-INFO
        while (condition(getWorkFiles().read(1, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info())))
        {
            if (condition(!(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1))))                                                                              //Natural: ACCEPT IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 01
            {
                continue;
            }
            ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr().setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                              //Natural: MOVE BY NAME WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO TO #RPT-EXT.#PYMNT-ADDR
            ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct().reset();                                                                                                              //Natural: RESET #RPT-EXT.C-INV-ACCT
            getWorkFiles().write(2, true, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr());                                                                                   //Natural: WRITE WORK FILE 2 VARIABLE #RPT-EXT.#PYMNT-ADDR
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //
}
