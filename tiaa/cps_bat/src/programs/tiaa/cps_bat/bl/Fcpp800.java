/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:37 PM
**        * FROM NATURAL PROGRAM : Fcpp800
************************************************************
**        * FILE NAME            : Fcpp800.java
**        * CLASS NAME           : Fcpp800
**        * INSTANCE NAME        : Fcpp800
************************************************************
************************************************************************
* PROGRAM   : FCPP800  -  JOB: P2200CPM
* SYSTEM    : CPS
* TITLE     : IAR RESTRUCTURE GATEWAY TO GROSS TO NET
* GENERATED :
* FUNCTION  : THIS PROGRAM WILL:
*           : - TRANSLATE RECORDS RECEIVED FROM THE IA ADMIN SYSTEM
*           :   INTO THE PDA FORMAT REQUIRED BY THE GROSS TO NET
*           :   MODULE (FCPN140Z)
*           : - CREATE AN OUTPUT WORK FILE WITH INFORMATION FOR
*           :   SUBSEQUENT PROCESSING (LEDGERS, REPORTS, CHECKS, ETC)
* HISTORY   : 10/15/98    CHAN/BALAN
*           : - ALLOWING PASSAGE TO PAYMNT-DED-CODE 600 TO BECOME
*           :   008 FOR AMOUNTS THAT ARE TO BE PAID INTO MUTUAL FUNDS
*           : 06/03/96    RITA SALGADO
*           : - PULL OUT CODE FOR NON-CHECK PAYMENT CONVERSION TO CHECKS
*           :   ** EFT / GLOBAL PAY WITH HOLD CODE
*           :   ** EFT WITH INVALID EFT TRANSIT ID CHECK DIGIT
*           :   ** 0 NET PAYMENT WILL BE CONVERTED TO CHECK BY FCPP801
*           : - INTERNAL ROLLOVERS ARE CODED AS PAY TYPE 8 BY IA
*           : - CALCULATE ADDITIONAL (FLAT TAX) AGAINST GROSS SETTLEMENT
*           : - BYPASS CALCULATION OF TAX ON INTERNAL ROLLOVERS,
*           :   UNCLAIMED SUMS AND BLOCKED ACCOUNTS
*           : 06/19/96    RITA SALGADO
*           : - CORRECT RE-START ROUTINE
*           : - EXCLUDE DIVIDENDS FOR COLLEGE IN FCPN800B TOTALS
*           : 09/03/96    RITA SALGADO
*           : - ALLOW ERROR CODE FOR 'invalid combine' CONTRACT
*           : 11/25/96    RITA SALGADO
*           : - PRODUCE A VARIANCE REPORT WITH A MESSAGE EVEN IF
*           :   IAA & CPS CONTROLS BALANCE
*           : 11/10/97    RITA SALGADO
*           : - INCREASE FUND OCCURRENCES FROM 20 TO 40
*           : 08/10/98    RITA SALGADO
*           : - ADD CNTRCT-STTLMNT-TYPE-IND FOR ROTH & INTERNAL ROLLOVER
*           : 05/15/00    TOM MCGEE
*           : - ADDED FREE LOOK TO CNTRCT-AMT FIELDS (2700)
*           : - COMMENT FOR PA-SELECT DEDUCTIONS 009 PA-SELECT
*           : - COMMENT FOR PA-SELECT DEDUCTIONS 010 UNIVERSAL LIFE
*           : - OPEN DED 750 / 007 PERSONAL ANNUITY DEDUCTIONS
*           : 07/03/00    TOM MCGEE ADDED EURO SEE- LINES 3770
*
*           : 03/15/2001  LEON GURTOVNIK
*           : - CNANGE REPORT (01) TO SPLIT INTERNAL ROLLOVERS
*           :   OCCURANCE 4 AND 13 (PAYMENTS DUE) INTO
*           : TPA  ROLLOVER          '
*           : P&I  ROLLOVER          '
*           : IPRO ROLLOVER          '
*           : INTERNAL ROLLOVER OTHER'
*           : 08/01/01    A.REYHAN  ADDED TAX WITHHOLDING INFORMATION
*           : **
*           : 12-2001 - MCGEE
*           : IACH      PYMNT-PAY-TYPE-REQ-IND 3,4,9 CHECK,EFT,US/EFT
*           :           ADDED TOGETHER FOR GLOBAL PAYMENT
*           : **
*           : 04/03/02    A.REYHAN  TEMPORARY FIX FOR RESIDENCY CODE 06
*           : 03/24/03    A.REYHAN  ADD ERROR-CODE ONLY ONCE PER PAYMENT
*           : **
*           : 05/20/03    T.MCGEE  SELF REMITTER TPA TO CASH PEND CODE
*           : 07/01/03    T.MCGEE  CONTRACT EXPIRED  PEND CODE
*           : 02/12/06    R.WILKINS  NAME TO UPPER CASE
*           : 04/25/07    J.OSTEEN
*           :               VERIFY THAT A VALUE OF 'C' OR 'S'
*           :               IS CONTAINED IN PYMNT-CHK-SAV-IND IF
*           :               PYMNT-PAY-TYPE-REQ-IND IS A '2' FOR EFT
*           :               THIS WILL PREVENT DOWN STREAM JOBS FROM
*           :               ENDING ABNORMALLY WITH A RC 90 DURING THE
*           :               CONTROL PROCESS. MODIFIED LINES COMMENT WITH
*           :               MODIFIED LINES COMMENT WITH /*  JWO 04/25/07
*            04/22/2008   LCW - RESTOWED FOR ROTH 403B. ROTH-MAJOR1
*                             - ROTH FIELDS INCLUDED IN FCPA800.
*           : 02/17/2009  J.OSTEEN
*           :               MODIFY FOR NEW TIAA ACCESS FUND
*           : 07/28/2010  D.E.ANDER - MARKED DEA
*           :               MODIFY FOR NEW DATASET WITH CANADIAN INFO
* 8/26/2010   C. MASON    CHANGE TO INCLUDE CANADIAN TAX IN IA FEED FILE
* 10/12/10   C. MASON     RESET #CANADIAN-TAX DATA AREA
* 10/26/10   C. MASON     ADD ADDITIONAL FIELDS TO CANADIAN TAX PDA
* 05/12/2011: IA-ORGN-CDE (F.ALLIE) STOW ONLY
* 12/06/11   R.SACHARNY   ADD IA-ORGN-CDE AT THE END OF WORK FILE 5 RS0
* 01/12/12   R.SACHARNY   ADD NEW FILEDS FOR SUNY/CUNY (RS1)
* 07/02/12   M.BRISCOE    FOR GLOBAL WIRES, IF THERE IS A 'G' ANYWHERE
*                         IN THE HOLD CODE FIELD, OR IN THE HOLD CODE
*                         INDICATOR, THEN SET THE HOLD CODE FIELD TO
*                         JUST A 'G'
* 10/30/12   F.ENDAYA     SETTLEMENT DATE SHOULD ALWAYS BE VALID
*                         BUSINESS DAY.   FE 201210
* 12/13/2013 B.HOLLOWAY   ADD CANADIAN TAX TO U100-UNDO-TAX-CALC
*                         ROUTINE                     - TAG IS DBH1
* 06/03/2014 COGNIZANT    INC2321993 -CANADIAN CONTRACTS WITH PEND CODE
*                         K REJECTING FROM MONTHLY IA RUN- TAG IS CTS
* 05/01/2017 SAI K        RESTOW FOR PIN EXPANSION
* 10/18/2017 DHIRAJ       CHANGING SSN TO ZERO FOR 999999999 TAG DASDH
* 05/08/2020 SAURAV VIKRAM    RESET PYMNT-DED-GRP FOR S461 FILE WHEN
*    FOR NO DED I.E. GTN-CNTRCT-RPT-CODE(*) EQ 4 OR EQ 5  - SAURAV
* 01/15/2020 CTS          CPS SUNSET (CHG694525) TAG: CTS-0115
*                         STOP CHECKING FCP-CONS-CNTRL FOR
*                         #CNTL-CHECK-DTE.
*                         READ A NEW CONTROL FILE FOR #CNTL-CHECK-DTE.
* 09/17/2020 CTS          CPS SUNSET (CHG857760) TAG: CTS-0917
* OCTOBER/20 CTS          CPS SUNSET (CHG875628) TAG: CTS-1020
**********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp800 extends BLNatBase
{
    // Data Areas
    private PdaFcpa140e pdaFcpa140e;
    private LdaFcpl800 ldaFcpl800;
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa802 pdaFcpa802;
    private PdaFcpa140z pdaFcpa140z;
    private PdaFcpa146 pdaFcpa146;
    private PdaFcpa147 pdaFcpa147;
    private PdaFcpa800a pdaFcpa800a;
    private PdaFcpa800e pdaFcpa800e;
    private PdaCpwa115 pdaCpwa115;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup cntlgdg;
    private DbsField cntlgdg_Cntl_Orgn_Cde;
    private DbsField cntlgdg_Cntl_Check_Dte;
    private DbsField cntlgdg_Cntl_Invrse_Dte;
    private DbsField cntlgdg_Cntl_Occur_Cnt;
    private DbsField cntlgdg_Cntl_Type_Cde;

    private DbsGroup cntlgdg__R_Field_1;
    private DbsField cntlgdg_Cntl_Occur_Num;
    private DbsField cntlgdg_Cntl_Pymnt_Cnt;
    private DbsField cntlgdg_Cntl_Rec_Cnt;
    private DbsField cntlgdg_Cntl_Gross_Amt;
    private DbsField cntlgdg_Cntl_Net_Amt;
    private DbsField pnd_Disp_Count;
    private DbsField pnd_Read_Count;
    private DbsField pnd_Total;
    private DbsField pnd_Cntl_Check_Dte;
    private DbsField pnd_Combined_Contract;
    private DbsField pnd_Gtn_Error;
    private DbsField pnd_Negative_Ivc;
    private DbsField pnd_Max_Funds;
    private DbsField pnd_Index;
    private DbsField pnd_Index2;
    private DbsField pnd_T_Index;
    private DbsField pnd_T_Cntrct_Amt;
    private DbsField pnd_T_Dvdnd_Amt;
    private DbsField pnd_T_Reac_Cref_Amt;
    private DbsField pnd_Pymnt_Rpt_Index;
    private DbsField pnd_Cntrct_Rpt_Index;
    private DbsField pnd_Fund_Ct;
    private DbsField pnd_Max;
    private DbsField pnd_Date_X;

    private DbsGroup pnd_Date_X__R_Field_2;
    private DbsField pnd_Date_X_Pnd_Date_N;
    private DbsField pnd_Sv_Pda_Count;
    private DbsField pnd_Sv_Return_Code;
    private DbsField pnd_Sv_Rpt_Codes;

    private DbsGroup pnd_Sv_Rpt_Codes__R_Field_3;
    private DbsField pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code;
    private DbsField pnd_Do_Rpt_Code;
    private DbsField pnd_Sv_Prcss_Seq_Num;
    private DbsField pnd_Sv_Pay_Type_Ind;
    private DbsField pnd_Total_Taxes;
    private DbsField pnd_Total_Gross;
    private DbsField pnd_Total_Ivc;
    private DbsField pnd_Ivc_Sum;
    private DbsField pnd_Fund_Ratio;
    private DbsField pnd_Check_Amt;
    private DbsField pnd_New_Net_Amt;
    private DbsField pnd_Ph_Middle_Name;

    private DbsGroup pnd_Ph_Middle_Name__R_Field_4;
    private DbsField pnd_Ph_Middle_Name_Pnd_Ph_Middle_Init;
    private DbsField pnd_Leon_Indx_04_Or_13;
    private DbsField pnd_Leon_Index;

    private DbsGroup pnd_Leon_Calc_Report;
    private DbsField pnd_Leon_Calc_Report_Pnd_Rec_Count_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Cntrct_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Dvdnd_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Cref_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Gross_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Ded_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon;
    private DbsField pnd_Leon_Calc_Report_Pnd_Net_Amt_Leon;
    private DbsField pnd_I;
    private DbsField pnd_X;
    private DbsField pnd_Filler;
    private DbsField pnd_Wk_Check_Date;
    private DbsField pnd_Wk_Weekend_Day;
    private DbsField pnd_Wk_Yyyymmdd_N;

    private DbsGroup pnd_Wk_Yyyymmdd_N__R_Field_5;
    private DbsField pnd_Wk_Yyyymmdd_N_Pnd_Wk_Yyyymmdd_X;
    private DbsField pnd_Wk_Ind;

    private DbsGroup pymnt_Ded_Grp_S461;
    private DbsField pymnt_Ded_Grp_S461_Pymnt_Ded_Cde_S461;
    private DbsField pymnt_Ded_Grp_S461_Pymnt_Ded_Payee_Cde_S461;
    private DbsField pymnt_Ded_Grp_S461_Pymnt_Ded_Amt_S461;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa140e = new PdaFcpa140e(localVariables);
        ldaFcpl800 = new LdaFcpl800();
        registerRecord(ldaFcpl800);
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa802 = new PdaFcpa802(localVariables);
        pdaFcpa140z = new PdaFcpa140z(localVariables);
        pdaFcpa146 = new PdaFcpa146(localVariables);
        pdaFcpa147 = new PdaFcpa147(localVariables);
        pdaFcpa800a = new PdaFcpa800a(localVariables);
        pdaFcpa800e = new PdaFcpa800e(localVariables);
        pdaCpwa115 = new PdaCpwa115(localVariables);

        // Local Variables

        cntlgdg = localVariables.newGroupInRecord("cntlgdg", "CNTLGDG");
        cntlgdg_Cntl_Orgn_Cde = cntlgdg.newFieldInGroup("cntlgdg_Cntl_Orgn_Cde", "CNTL-ORGN-CDE", FieldType.STRING, 2);
        cntlgdg_Cntl_Check_Dte = cntlgdg.newFieldInGroup("cntlgdg_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE);
        cntlgdg_Cntl_Invrse_Dte = cntlgdg.newFieldInGroup("cntlgdg_Cntl_Invrse_Dte", "CNTL-INVRSE-DTE", FieldType.NUMERIC, 8);
        cntlgdg_Cntl_Occur_Cnt = cntlgdg.newFieldInGroup("cntlgdg_Cntl_Occur_Cnt", "CNTL-OCCUR-CNT", FieldType.PACKED_DECIMAL, 3);
        cntlgdg_Cntl_Type_Cde = cntlgdg.newFieldArrayInGroup("cntlgdg_Cntl_Type_Cde", "CNTL-TYPE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            50));

        cntlgdg__R_Field_1 = cntlgdg.newGroupInGroup("cntlgdg__R_Field_1", "REDEFINE", cntlgdg_Cntl_Type_Cde);
        cntlgdg_Cntl_Occur_Num = cntlgdg__R_Field_1.newFieldArrayInGroup("cntlgdg_Cntl_Occur_Num", "CNTL-OCCUR-NUM", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            50));
        cntlgdg_Cntl_Pymnt_Cnt = cntlgdg.newFieldArrayInGroup("cntlgdg_Cntl_Pymnt_Cnt", "CNTL-PYMNT-CNT", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            50));
        cntlgdg_Cntl_Rec_Cnt = cntlgdg.newFieldArrayInGroup("cntlgdg_Cntl_Rec_Cnt", "CNTL-REC-CNT", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            50));
        cntlgdg_Cntl_Gross_Amt = cntlgdg.newFieldArrayInGroup("cntlgdg_Cntl_Gross_Amt", "CNTL-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            50));
        cntlgdg_Cntl_Net_Amt = cntlgdg.newFieldArrayInGroup("cntlgdg_Cntl_Net_Amt", "CNTL-NET-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            50));
        pnd_Disp_Count = localVariables.newFieldInRecord("pnd_Disp_Count", "#DISP-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Read_Count = localVariables.newFieldInRecord("pnd_Read_Count", "#READ-COUNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cntl_Check_Dte = localVariables.newFieldInRecord("pnd_Cntl_Check_Dte", "#CNTL-CHECK-DTE", FieldType.DATE);
        pnd_Combined_Contract = localVariables.newFieldInRecord("pnd_Combined_Contract", "#COMBINED-CONTRACT", FieldType.BOOLEAN, 1);
        pnd_Gtn_Error = localVariables.newFieldInRecord("pnd_Gtn_Error", "#GTN-ERROR", FieldType.BOOLEAN, 1);
        pnd_Negative_Ivc = localVariables.newFieldInRecord("pnd_Negative_Ivc", "#NEGATIVE-IVC", FieldType.BOOLEAN, 1);
        pnd_Max_Funds = localVariables.newFieldInRecord("pnd_Max_Funds", "#MAX-FUNDS", FieldType.PACKED_DECIMAL, 2);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Index2 = localVariables.newFieldInRecord("pnd_Index2", "#INDEX2", FieldType.PACKED_DECIMAL, 2);
        pnd_T_Index = localVariables.newFieldInRecord("pnd_T_Index", "#T-INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_T_Cntrct_Amt = localVariables.newFieldInRecord("pnd_T_Cntrct_Amt", "#T-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_T_Dvdnd_Amt", "#T-DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_T_Reac_Cref_Amt = localVariables.newFieldInRecord("pnd_T_Reac_Cref_Amt", "#T-REAC-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pymnt_Rpt_Index = localVariables.newFieldInRecord("pnd_Pymnt_Rpt_Index", "#PYMNT-RPT-INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Cntrct_Rpt_Index = localVariables.newFieldInRecord("pnd_Cntrct_Rpt_Index", "#CNTRCT-RPT-INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Fund_Ct = localVariables.newFieldInRecord("pnd_Fund_Ct", "#FUND-CT", FieldType.PACKED_DECIMAL, 2);
        pnd_Max = localVariables.newFieldInRecord("pnd_Max", "#MAX", FieldType.PACKED_DECIMAL, 2);
        pnd_Date_X = localVariables.newFieldInRecord("pnd_Date_X", "#DATE-X", FieldType.STRING, 8);

        pnd_Date_X__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_X__R_Field_2", "REDEFINE", pnd_Date_X);
        pnd_Date_X_Pnd_Date_N = pnd_Date_X__R_Field_2.newFieldInGroup("pnd_Date_X_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);
        pnd_Sv_Pda_Count = localVariables.newFieldInRecord("pnd_Sv_Pda_Count", "#SV-PDA-COUNT", FieldType.NUMERIC, 2);
        pnd_Sv_Return_Code = localVariables.newFieldInRecord("pnd_Sv_Return_Code", "#SV-RETURN-CODE", FieldType.STRING, 4);
        pnd_Sv_Rpt_Codes = localVariables.newFieldInRecord("pnd_Sv_Rpt_Codes", "#SV-RPT-CODES", FieldType.STRING, 20);

        pnd_Sv_Rpt_Codes__R_Field_3 = localVariables.newGroupInRecord("pnd_Sv_Rpt_Codes__R_Field_3", "REDEFINE", pnd_Sv_Rpt_Codes);
        pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code = pnd_Sv_Rpt_Codes__R_Field_3.newFieldArrayInGroup("pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code", "#SV-RPT-CODE", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 10));
        pnd_Do_Rpt_Code = localVariables.newFieldInRecord("pnd_Do_Rpt_Code", "#DO-RPT-CODE", FieldType.NUMERIC, 2);
        pnd_Sv_Prcss_Seq_Num = localVariables.newFieldInRecord("pnd_Sv_Prcss_Seq_Num", "#SV-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Sv_Pay_Type_Ind = localVariables.newFieldInRecord("pnd_Sv_Pay_Type_Ind", "#SV-PAY-TYPE-IND", FieldType.NUMERIC, 1);
        pnd_Total_Taxes = localVariables.newFieldInRecord("pnd_Total_Taxes", "#TOTAL-TAXES", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Total_Gross = localVariables.newFieldInRecord("pnd_Total_Gross", "#TOTAL-GROSS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Ivc = localVariables.newFieldInRecord("pnd_Total_Ivc", "#TOTAL-IVC", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ivc_Sum = localVariables.newFieldInRecord("pnd_Ivc_Sum", "#IVC-SUM", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Fund_Ratio = localVariables.newFieldInRecord("pnd_Fund_Ratio", "#FUND-RATIO", FieldType.PACKED_DECIMAL, 8, 7);
        pnd_Check_Amt = localVariables.newFieldInRecord("pnd_Check_Amt", "#CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_New_Net_Amt = localVariables.newFieldInRecord("pnd_New_Net_Amt", "#NEW-NET-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ph_Middle_Name = localVariables.newFieldInRecord("pnd_Ph_Middle_Name", "#PH-MIDDLE-NAME", FieldType.STRING, 10);

        pnd_Ph_Middle_Name__R_Field_4 = localVariables.newGroupInRecord("pnd_Ph_Middle_Name__R_Field_4", "REDEFINE", pnd_Ph_Middle_Name);
        pnd_Ph_Middle_Name_Pnd_Ph_Middle_Init = pnd_Ph_Middle_Name__R_Field_4.newFieldInGroup("pnd_Ph_Middle_Name_Pnd_Ph_Middle_Init", "#PH-MIDDLE-INIT", 
            FieldType.STRING, 1);
        pnd_Leon_Indx_04_Or_13 = localVariables.newFieldInRecord("pnd_Leon_Indx_04_Or_13", "#LEON-INDX-04-OR-13", FieldType.NUMERIC, 2);
        pnd_Leon_Index = localVariables.newFieldInRecord("pnd_Leon_Index", "#LEON-INDEX", FieldType.NUMERIC, 1);

        pnd_Leon_Calc_Report = localVariables.newGroupArrayInRecord("pnd_Leon_Calc_Report", "#LEON-CALC-REPORT", new DbsArrayController(1, 4));
        pnd_Leon_Calc_Report_Pnd_Rec_Count_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Rec_Count_Leon", "#REC-COUNT-LEON", 
            FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Cntrct_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Cntrct_Amt_Leon", "#CNTRCT-AMT-LEON", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Dvdnd_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Dvdnd_Amt_Leon", "#DVDND-AMT-LEON", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Cref_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Cref_Amt_Leon", "#CREF-AMT-LEON", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Gross_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Gross_Amt_Leon", "#GROSS-AMT-LEON", 
            FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Ded_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Ded_Amt_Leon", "#DED-AMT-LEON", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon", "#TAX-AMT-LEON", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 2));
        pnd_Leon_Calc_Report_Pnd_Net_Amt_Leon = pnd_Leon_Calc_Report.newFieldArrayInGroup("pnd_Leon_Calc_Report_Pnd_Net_Amt_Leon", "#NET-AMT-LEON", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 2));
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 7);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_Filler = localVariables.newFieldArrayInRecord("pnd_Filler", "#FILLER", FieldType.STRING, 82, new DbsArrayController(1, 40));
        pnd_Wk_Check_Date = localVariables.newFieldInRecord("pnd_Wk_Check_Date", "#WK-CHECK-DATE", FieldType.DATE);
        pnd_Wk_Weekend_Day = localVariables.newFieldInRecord("pnd_Wk_Weekend_Day", "#WK-WEEKEND-DAY", FieldType.STRING, 8);
        pnd_Wk_Yyyymmdd_N = localVariables.newFieldInRecord("pnd_Wk_Yyyymmdd_N", "#WK-YYYYMMDD-N", FieldType.NUMERIC, 8);

        pnd_Wk_Yyyymmdd_N__R_Field_5 = localVariables.newGroupInRecord("pnd_Wk_Yyyymmdd_N__R_Field_5", "REDEFINE", pnd_Wk_Yyyymmdd_N);
        pnd_Wk_Yyyymmdd_N_Pnd_Wk_Yyyymmdd_X = pnd_Wk_Yyyymmdd_N__R_Field_5.newFieldInGroup("pnd_Wk_Yyyymmdd_N_Pnd_Wk_Yyyymmdd_X", "#WK-YYYYMMDD-X", FieldType.STRING, 
            8);
        pnd_Wk_Ind = localVariables.newFieldInRecord("pnd_Wk_Ind", "#WK-IND", FieldType.PACKED_DECIMAL, 3);

        pymnt_Ded_Grp_S461 = localVariables.newGroupInRecord("pymnt_Ded_Grp_S461", "PYMNT-DED-GRP-S461");
        pymnt_Ded_Grp_S461_Pymnt_Ded_Cde_S461 = pymnt_Ded_Grp_S461.newFieldArrayInGroup("pymnt_Ded_Grp_S461_Pymnt_Ded_Cde_S461", "PYMNT-DED-CDE-S461", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 10));
        pymnt_Ded_Grp_S461_Pymnt_Ded_Payee_Cde_S461 = pymnt_Ded_Grp_S461.newFieldArrayInGroup("pymnt_Ded_Grp_S461_Pymnt_Ded_Payee_Cde_S461", "PYMNT-DED-PAYEE-CDE-S461", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pymnt_Ded_Grp_S461_Pymnt_Ded_Amt_S461 = pymnt_Ded_Grp_S461.newFieldArrayInGroup("pymnt_Ded_Grp_S461_Pymnt_Ded_Amt_S461", "PYMNT-DED-AMT-S461", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 10));
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl800.initializeValues();

        localVariables.reset();
        pnd_Disp_Count.setInitialValue(500);
        pnd_Max_Funds.setInitialValue(40);
        pnd_Max.setInitialValue(40);
        pnd_Leon_Indx_04_Or_13.setInitialValue(0);
        pnd_Leon_Index.setInitialValue(0);
        pnd_Filler.getValue(1).setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp800() throws Exception
    {
        super("Fcpp800");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP800", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: ON ERROR
                                                                                                                                                                          //Natural: PERFORM A100-RESTART-ROUTINE
        sub_A100_Restart_Routine();
        if (condition(Global.isEscape())) {return;}
        //*  READ LAST RECORD OF CONTROL FILE TO MAKE SURE THE IAA INTERFACE
        //*    FILE IS GOOD
        //*  CTS-0115 >>
        //*  READ (1) FCP-CONS-CNTRL BY CNTL-ORG-CDE-INVRSE-DTE
        //*      STARTING FROM 'AP00000000'
        //*    IF FCP-CONS-CNTRL.CNTL-ORGN-CDE NE 'AP'
        //*     WRITE
        //*      // '*************************************************************'
        //*      /  '*************************************************************'
        //*      /  '*** A CPS control record is needed to process the checks  ***'
        //*      /  '***                                                       ***'
        //*      /  '***   "AP" control record in Adabas File 198 not found    ***'
        //*      /  '*************************************************************'
        //*      /  '*************************************************************'
        //*     TERMINATE 0091
        //*   END-IF
        //*   MOVE FCP-CONS-CNTRL.CNTL-CHECK-DTE TO #CNTL-CHECK-DTE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 7 CNTLGDG
        while (condition(getWorkFiles().read(7, cntlgdg)))
        {
            pnd_Cntl_Check_Dte.setValue(cntlgdg_Cntl_Check_Dte);                                                                                                          //Natural: MOVE CNTLGDG.CNTL-CHECK-DTE TO #CNTL-CHECK-DTE
            pnd_Cntl_Check_Dte.nadd(31);                                                                                                                                  //Natural: ADD 31 TO #CNTL-CHECK-DTE
            pnd_Date_X.setValueEdited(pnd_Cntl_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED #CNTL-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE-X
            setValueToSubstring("01",pnd_Date_X,7,2);                                                                                                                     //Natural: MOVE '01' TO SUBSTRING ( #DATE-X,7,2 )
            pnd_Cntl_Check_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_X);                                                                                 //Natural: MOVE EDITED #DATE-X TO #CNTL-CHECK-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  CTS-0115 <<
        //*  CONTROL FILE FRM IA INPUT
        getWorkFiles().read(4, ldaFcpl800.getIa_Cntl_Rec());                                                                                                              //Natural: READ WORK FILE 4 ONCE IA-CNTL-REC
        if (condition(ldaFcpl800.getIa_Cntl_Rec_Cntl_Rec_Id().notEquals("999999") || ldaFcpl800.getIa_Cntl_Rec_Cntl_Check_Dte().notEquals(pnd_Cntl_Check_Dte)))           //Natural: IF IA-CNTL-REC.CNTL-REC-ID NE '999999' OR IA-CNTL-REC.CNTL-CHECK-DTE NE #CNTL-CHECK-DTE
        {
            //*  CTS-0115
            //*  ABRM
            //* ABRM
            getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************",NEWLINE,"***",new  //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***' 60T '***' / '***' 60T '***' / '***    VERIFY THAT THE IAA INPUT CONTROL FILE IS CORRECT   ***' / '***' 60T '***' / '***    LAST MONTH"S CHECKS WERE DATED :' CNTLGDG.CNTL-CHECK-DTE ( EM = MM/DD/YYYY ) 60T '***' / '***    IAA CONTROL RECORD IS DATED    :' IA-CNTL-REC.CNTL-CHECK-DTE ( EM = MM/DD/YYYY ) 60T '***' / '***    IAA CONTROL RECORD EXPECTED    :' #CNTL-CHECK-DTE ( EM = MM/DD/YYYY ) 60T '***' / '***' 60T '***' / '***' 60T '***' / '**************************************************************' / '**************************************************************'
                TabSetting(60),"***",NEWLINE,"***",new TabSetting(60),"***",NEWLINE,"***    VERIFY THAT THE IAA INPUT CONTROL FILE IS CORRECT   ***",NEWLINE,"***",new 
                TabSetting(60),"***",NEWLINE,"***    LAST MONTH'S CHECKS WERE DATED :",cntlgdg_Cntl_Check_Dte, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(60),"***",NEWLINE,"***    IAA CONTROL RECORD IS DATED    :",ldaFcpl800.getIa_Cntl_Rec_Cntl_Check_Dte(), 
                new ReportEditMask ("MM/DD/YYYY"),new TabSetting(60),"***",NEWLINE,"***    IAA CONTROL RECORD EXPECTED    :",pnd_Cntl_Check_Dte, new ReportEditMask 
                ("MM/DD/YYYY"),new TabSetting(60),"***",NEWLINE,"***",new TabSetting(60),"***",NEWLINE,"***",new TabSetting(60),"***",NEWLINE,"**************************************************************",
                NEWLINE,"**************************************************************");
            if (Global.isEscape()) return;
            //*    FCP-CONS-CNTRL.CNTL-CHECK-DTE(EM=MM/DD/YYYY) 60T '***'
            DbsUtil.terminate(92);  if (true) return;                                                                                                                     //Natural: TERMINATE 0092
        }                                                                                                                                                                 //Natural: END-IF
        //*  SAVE IAA CONTROL TOTALS
                                                                                                                                                                          //Natural: PERFORM S100-SAVE-CONTROL-TOTALS
        sub_S100_Save_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  INPUT FRM IA - FCPA140E
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 2 GTN-PDA-E
        while (condition(getWorkFiles().read(2, pdaFcpa140e.getGtn_Pda_E())))
        {
            if (condition(pdaFcpa140e.getGtn_Pda_E_Cntrct_Hold_Cde().equals("00  ")))                                                                                     //Natural: IF GTN-PDA-E.CNTRCT-HOLD-CDE = '00  '
            {
                pdaFcpa140e.getGtn_Pda_E_Cntrct_Hold_Cde().reset();                                                                                                       //Natural: RESET GTN-PDA-E.CNTRCT-HOLD-CDE
            }                                                                                                                                                             //Natural: END-IF
            //*                                                     MB 7/2      START
            if (condition(DbsUtil.maskMatches(pdaFcpa140e.getGtn_Pda_E_Cntrct_Hold_Cde(),".'G'")))                                                                        //Natural: IF GTN-PDA-E.CNTRCT-HOLD-CDE = MASK ( .'G' )
            {
                pdaFcpa140e.getGtn_Pda_E_Cntrct_Hold_Cde().setValue("G");                                                                                                 //Natural: ASSIGN GTN-PDA-E.CNTRCT-HOLD-CDE := 'G'
            }                                                                                                                                                             //Natural: END-IF
            //*                                                     MB 7/2      END
            //*  CTS-1020 >>
            //*  IF GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND = 2        /*  JWO 4/25/07
            //*   JWO 4/25/07
            //*   JWO 4/25/07
            //*   JWO 4/25/07
            if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().notEquals(3) && pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().notEquals(4)                //Natural: IF GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND NE 3 AND GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND NE 4 AND GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND NE 9 AND GTN-PDA-E.PYMNT-CHK-SAV-IND NE 'C' AND GTN-PDA-E.PYMNT-CHK-SAV-IND NE 'S'
                && pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind().notEquals(9) && pdaFcpa140e.getGtn_Pda_E_Pymnt_Chk_Sav_Ind().notEquals("C") && pdaFcpa140e.getGtn_Pda_E_Pymnt_Chk_Sav_Ind().notEquals("S")))
            {
                pdaFcpa140e.getGtn_Pda_E_Pymnt_Chk_Sav_Ind().setValue("C");                                                                                               //Natural: ASSIGN GTN-PDA-E.PYMNT-CHK-SAV-IND := 'C'
                //*   JWO 4/25/07
            }                                                                                                                                                             //Natural: END-IF
            //*  DASDH  10/18/17
            if (condition(pdaFcpa140e.getGtn_Pda_E_Annt_Soc_Sec_Nbr().equals(999999999)))                                                                                 //Natural: IF GTN-PDA-E.ANNT-SOC-SEC-NBR = 999999999
            {
                //*  DASDH   10/18/17
                pdaFcpa140e.getGtn_Pda_E_Annt_Soc_Sec_Nbr().setValue(0);                                                                                                  //Natural: MOVE 0 TO GTN-PDA-E.ANNT-SOC-SEC-NBR
                //*  DASDH   10/18/17
            }                                                                                                                                                             //Natural: END-IF
            //*  500    TEMP
            pnd_Read_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #READ-COUNT
            //*  500
            pnd_Disp_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #DISP-COUNT
            //*  500
            if (condition(pnd_Disp_Count.greaterOrEqual(500)))                                                                                                            //Natural: IF #DISP-COUNT NOT < 500
            {
                //*  500
                pnd_Disp_Count.reset();                                                                                                                                   //Natural: RESET #DISP-COUNT
                //*  500
                //*  500
                //*  500
                //*  500
                getReports().display(0, pnd_Read_Count,pdaFcpa140e.getGtn_Pda_E_Cntrct_Ppcn_Nbr(),pdaFcpa140e.getGtn_Pda_E_Cntrct_Cmbn_Nbr(),Global.getTIME());           //Natural: DISPLAY #READ-COUNT GTN-PDA-E.CNTRCT-PPCN-NBR GTN-PDA-E.CNTRCT-CMBN-NBR *TIME
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  500
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Check_Dte().notEquals(pnd_Cntl_Check_Dte) && pnd_Read_Count.less(10)))                                           //Natural: IF GTN-PDA-E.PYMNT-CHECK-DTE NE #CNTL-CHECK-DTE AND #READ-COUNT < 10 THEN
            {
                getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************",NEWLINE,"***",new  //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***' 60T '***' / '***' 60T '***' / '***       VERIFY THAT THE IAA INPUT FILE IS CORRECT        ***' / '***' 60T '***' / '***         EXPECTING CHECKS DATED    :' #CNTL-CHECK-DTE ( EM = MM/DD/YYYY ) 60T '***' / '***    IAA INPUT RECORDS ARE DATED    :' GTN-PDA-E.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) 60T '***' / '***' 60T '***' / '***' 60T '***' / '**************************************************************' / '**************************************************************'
                    TabSetting(60),"***",NEWLINE,"***",new TabSetting(60),"***",NEWLINE,"***       VERIFY THAT THE IAA INPUT FILE IS CORRECT        ***",NEWLINE,"***",new 
                    TabSetting(60),"***",NEWLINE,"***         EXPECTING CHECKS DATED    :",pnd_Cntl_Check_Dte, new ReportEditMask ("MM/DD/YYYY"),new TabSetting(60),"***",NEWLINE,"***    IAA INPUT RECORDS ARE DATED    :",pdaFcpa140e.getGtn_Pda_E_Pymnt_Check_Dte(), 
                    new ReportEditMask ("MM/DD/YYYY"),new TabSetting(60),"***",NEWLINE,"***",new TabSetting(60),"***",NEWLINE,"***",new TabSetting(60),"***",
                    NEWLINE,"**************************************************************",NEWLINE,"**************************************************************");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(93);  if (true) return;                                                                                                                 //Natural: TERMINATE 0093
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa140e.getGtn_Pda_E_Pnd_Pnd_This_Pymnt().equals(1)))                                                                                       //Natural: IF GTN-PDA-E.##THIS-PYMNT = 01
            {
                                                                                                                                                                          //Natural: PERFORM A200-INITIALIZE-ROUTINE
                sub_A200_Initialize_Routine();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  INSTALL OF PREV REC
            pnd_Sv_Pda_Count.setValue(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Pda_Count());                                                                                      //Natural: MOVE GTN-PDA-Z.##PDA-COUNT TO #SV-PDA-COUNT
            //*  FROM FCPA140E TO FCPA140Z
            pdaFcpa140z.getGtn_Pda_Z().setValuesByName(pdaFcpa140e.getGtn_Pda_E());                                                                                       //Natural: MOVE BY NAME GTN-PDA-E TO GTN-PDA-Z
            //* *IF GTN-PDA-E.CNTRCT-LOB-CDE NE ' '
            //* *WRITE '=' GTN-PDA-E.CNTRCT-LOB-CDE 'Z-LOB:' GTN-PDA-Z.CNTRCT-LOB-CDE
            //* *'=' GTN-PDA-E.CNTRCT-PPCN-NBR
            //* *END-IF
            pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Pda_Count().setValue(pnd_Sv_Pda_Count);                                                                                      //Natural: MOVE #SV-PDA-COUNT TO GTN-PDA-Z.##PDA-COUNT
            pdaFcpa140z.getGtn_Pda_Z_Tax_Repeat_Adaread().setValue(pdaFcpa140e.getGtn_Pda_E_Pymnt_Payee_Tax_Ind());                                                       //Natural: MOVE GTN-PDA-E.PYMNT-PAYEE-TAX-IND TO GTN-PDA-Z.TAX-REPEAT-ADAREAD
            if (condition(pnd_Combined_Contract.getBoolean()))                                                                                                            //Natural: IF #COMBINED-CONTRACT
            {
                pdaFcpa140z.getGtn_Pda_Z_Pymnt_Cmbne_Ind().setValue("Y");                                                                                                 //Natural: MOVE 'Y' TO GTN-PDA-Z.PYMNT-CMBNE-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa140z.getGtn_Pda_Z_Pymnt_Cmbne_Ind().setValue(" ");                                                                                                 //Natural: MOVE ' ' TO GTN-PDA-Z.PYMNT-CMBNE-IND
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa140z.getGtn_Pda_Z_Cntrct_Dvdnd_Payee_Cde().equals(" ") || pdaFcpa140z.getGtn_Pda_Z_Cntrct_Dvdnd_Payee_Cde().equals("00000")))            //Natural: IF GTN-PDA-Z.CNTRCT-DVDND-PAYEE-CDE = ' ' OR = '00000'
            {
                pdaFcpa140z.getGtn_Pda_Z_Cntrct_Dvdnd_Payee_Ind().setValue(0);                                                                                            //Natural: MOVE 0 TO GTN-PDA-Z.CNTRCT-DVDND-PAYEE-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa140z.getGtn_Pda_Z_Cntrct_Dvdnd_Payee_Ind().setValue(2);                                                                                            //Natural: MOVE 2 TO GTN-PDA-Z.CNTRCT-DVDND-PAYEE-IND
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 10
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(10)); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).equals(getZero())))                                                            //Natural: IF GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX ) = 0
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  BLUE CROSS
                short decideConditionsMet1922 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX );//Natural: VALUE 200 : 299
                if (condition(((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).greaterOrEqual(200) && pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).lessOrEqual(299)))))
                {
                    decideConditionsMet1922++;
                    //*  LONG TERM CARE
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(1);                                                                             //Natural: MOVE 001 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 400
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).equals(400))))
                {
                    decideConditionsMet1922++;
                    //*  MAJOR MEDICAL
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(2);                                                                             //Natural: MOVE 002 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 300 : 399
                else if (condition(((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).greaterOrEqual(300) && pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).lessOrEqual(399)))))
                {
                    decideConditionsMet1922++;
                    //*  GROUP LIFE
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(3);                                                                             //Natural: MOVE 003 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 100 : 199
                else if (condition(((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).greaterOrEqual(100) && pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).lessOrEqual(199)))))
                {
                    decideConditionsMet1922++;
                    //*  OVERPAYMENT
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(4);                                                                             //Natural: MOVE 004 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 001 : 099
                else if (condition(((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).greaterOrEqual(1) && pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).lessOrEqual(99)))))
                {
                    decideConditionsMet1922++;
                    //*  NYSUT
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(5);                                                                             //Natural: MOVE 005 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 500
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).equals(500))))
                {
                    decideConditionsMet1922++;
                    //*  PERSONAL ANNUITY
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(6);                                                                             //Natural: MOVE 006 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 750
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).equals(750))))
                {
                    decideConditionsMet1922++;
                    //*  MUTUAL FUND
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(7);                                                                             //Natural: MOVE 007 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 600
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).equals(600))))
                {
                    decideConditionsMet1922++;
                    //*  PA-SELECT
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(8);                                                                             //Natural: MOVE 008 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 700
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).equals(700))))
                {
                    decideConditionsMet1922++;
                    //*  UNIVERSAL LIFE
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(9);                                                                             //Natural: MOVE 009 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 800
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).equals(800))))
                {
                    decideConditionsMet1922++;
                    //*  DENTAL  JWO 2009-09-10
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(10);                                                                            //Natural: MOVE 010 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE 710
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).equals(710))))
                {
                    decideConditionsMet1922++;
                    //*  KEEP ORIGINAL DED CODE
                    pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Cde().getValue(pnd_Index).setValue(11);                                                                            //Natural: MOVE 011 TO GTN-PDA-Z.PYMNT-DED-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ONE IVC AMT IS INPUT PER CONTRACT AND MUST BE PRORATED PER FUND
            pnd_Total_Gross.reset();                                                                                                                                      //Natural: RESET #TOTAL-GROSS #TOTAL-IVC #IVC-SUM
            pnd_Total_Ivc.reset();
            pnd_Ivc_Sum.reset();
            pnd_Total_Gross.nadd(pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Settl_Amt().getValue("*"));                                                                            //Natural: ADD GTN-PDA-Z.INV-ACCT-SETTL-AMT ( * ) TO #TOTAL-GROSS
            pnd_Total_Ivc.nadd(pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Ivc_Amt().getValue("*"));                                                                                //Natural: ADD GTN-PDA-Z.INV-ACCT-IVC-AMT ( * ) TO #TOTAL-IVC
            if (condition(pnd_Total_Ivc.less(getZero())))                                                                                                                 //Natural: IF #TOTAL-IVC < 0
            {
                pnd_Negative_Ivc.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #NEGATIVE-IVC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Negative_Ivc.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #NEGATIVE-IVC
            }                                                                                                                                                             //Natural: END-IF
            FOR02:                                                                                                                                                        //Natural: FOR #INDEX 1 #MAX
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pnd_Max)); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).equals(" ")))                                                                   //Natural: IF GTN-PDA-Z.INV-ACCT-CDE ( #INDEX ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1976 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF GTN-PDA-Z.INV-ACCT-CDE ( #INDEX );//Natural: VALUE '1 '
                if (condition((pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).equals("1 "))))
                {
                    decideConditionsMet1976++;
                    pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).setValue("G ");                                                                           //Natural: MOVE 'G ' TO GTN-PDA-Z.INV-ACCT-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE '1S'
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).equals("1S"))))
                {
                    decideConditionsMet1976++;
                    pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).setValue("T ");                                                                           //Natural: MOVE 'T ' TO GTN-PDA-Z.INV-ACCT-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE '1G'
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).equals("1G"))))
                {
                    decideConditionsMet1976++;
                    //*  JWO 02/17/2009
                    pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).setValue("TG");                                                                           //Natural: MOVE 'TG' TO GTN-PDA-Z.INV-ACCT-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: VALUE '11'
                else if (condition((pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).equals("11"))))
                {
                    decideConditionsMet1976++;
                    //*  JWO 02/17/2009
                    pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index).setValue("D ");                                                                           //Natural: MOVE 'D ' TO GTN-PDA-Z.INV-ACCT-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Cntrct_Amt().getValue(pnd_Index).reset();                                                                       //Natural: RESET GTN-PDA-Z.##INV-ACCT-CNTRCT-AMT ( #INDEX ) GTN-PDA-Z.##INV-ACCT-DVDND-AMT ( #INDEX )
                pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index).reset();
                //* TMM FREE LOOK
                if (condition(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_Index).equals("1S") || pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_Index).equals("1G")  //Natural: IF GTN-PDA-E.INV-ACCT-CDE ( #INDEX ) = '1S' OR = '1G' OR = '1 ' OR = '41'
                    || pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_Index).equals("1 ") || pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cde().getValue(pnd_Index).equals("41")))
                {
                    pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Cntrct_Amt().getValue(pnd_Index).setValue(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Cntrct_Amt().getValue(pnd_Index)); //Natural: MOVE GTN-PDA-E.INV-ACCT-CNTRCT-AMT ( #INDEX ) TO GTN-PDA-Z.##INV-ACCT-CNTRCT-AMT ( #INDEX )
                    pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index).setValue(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index)); //Natural: MOVE GTN-PDA-E.INV-ACCT-DVDND-AMT ( #INDEX ) TO GTN-PDA-Z.##INV-ACCT-DVDND-AMT ( #INDEX )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Unit_Value().getValue(pnd_Index).setValue(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Unit_Value().getValue(pnd_Index)); //Natural: MOVE GTN-PDA-E.INV-ACCT-UNIT-VALUE ( #INDEX ) TO GTN-PDA-Z.##INV-ACCT-UNIT-VALUE ( #INDEX )
                    pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Unit_Qty().getValue(pnd_Index).setValue(pdaFcpa140e.getGtn_Pda_E_Inv_Acct_Unit_Qty().getValue(pnd_Index));  //Natural: MOVE GTN-PDA-E.INV-ACCT-UNIT-QTY ( #INDEX ) TO GTN-PDA-Z.##INV-ACCT-UNIT-QTY ( #INDEX )
                }                                                                                                                                                         //Natural: END-IF
                //*  ONE IVC AMT IS INPUT PER CONTRACT AND MUST BE PRORATED PER FUND
                if (condition(pnd_Total_Ivc.notEquals(getZero())))                                                                                                        //Natural: IF #TOTAL-IVC NE 0
                {
                                                                                                                                                                          //Natural: PERFORM A300-ALLOCATE-IVC
                    sub_A300_Allocate_Ivc();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   LAST DAY SHOULD NOT BE SATURDAY OR SUNDAY      FE 201210 START
            pnd_Wk_Check_Date.compute(new ComputeParameters(false, pnd_Wk_Check_Date), pdaFcpa140e.getGtn_Pda_E_Pymnt_Check_Dte().subtract(1));                           //Natural: ASSIGN #WK-CHECK-DATE := GTN-PDA-E.PYMNT-CHECK-DTE - 1
            pnd_Wk_Weekend_Day.setValueEdited(pnd_Wk_Check_Date,new ReportEditMask("NNNNNNNN"));                                                                          //Natural: MOVE EDITED #WK-CHECK-DATE ( EM = NNNNNNNN ) TO #WK-WEEKEND-DAY
            FOR03:                                                                                                                                                        //Natural: FOR #WK-IND 1 TO 4
            for (pnd_Wk_Ind.setValue(1); condition(pnd_Wk_Ind.lessOrEqual(4)); pnd_Wk_Ind.nadd(1))
            {
                if (condition(pnd_Wk_Weekend_Day.equals("Saturday") || pnd_Wk_Weekend_Day.equals("Sunday")))                                                              //Natural: IF #WK-WEEKEND-DAY = 'Saturday' OR = 'Sunday'
                {
                    pnd_Wk_Check_Date.nsubtract(1);                                                                                                                       //Natural: ASSIGN #WK-CHECK-DATE := #WK-CHECK-DATE - 1
                    pnd_Wk_Weekend_Day.setValueEdited(pnd_Wk_Check_Date,new ReportEditMask("NNNNNNNN"));                                                                  //Natural: MOVE EDITED #WK-CHECK-DATE ( EM = NNNNNNNN ) TO #WK-WEEKEND-DAY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Wk_Yyyymmdd_N_Pnd_Wk_Yyyymmdd_X.setValueEdited(pnd_Wk_Check_Date,new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED #WK-CHECK-DATE ( EM = YYYYMMDD ) TO #WK-YYYYMMDD-X
                    pdaCpwa115.getCpwa115_For_Date().setValue(pnd_Wk_Yyyymmdd_N);                                                                                         //Natural: MOVE #WK-YYYYMMDD-N TO FOR-DATE
                    pdaCpwa115.getCpwa115_For_Time().setValue(1000);                                                                                                      //Natural: ASSIGN FOR-TIME := 0001000
                    DbsUtil.callnat(Cpwn116.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                   //Natural: CALLNAT 'CPWN116' USING CPWA115
                    if (condition(Global.isEscape())) return;
                    //*  HOLIDAY
                    if (condition(pdaCpwa115.getCpwa115_Business_Date_Ind().equals("N")))                                                                                 //Natural: IF CPWA115.BUSINESS-DATE-IND = 'N'
                    {
                        pnd_Wk_Check_Date.nsubtract(1);                                                                                                                   //Natural: ASSIGN #WK-CHECK-DATE := #WK-CHECK-DATE - 1
                        pnd_Wk_Weekend_Day.setValueEdited(pnd_Wk_Check_Date,new ReportEditMask("NNNNNNNN"));                                                              //Natural: MOVE EDITED #WK-CHECK-DATE ( EM = NNNNNNNN ) TO #WK-WEEKEND-DAY
                        //*  VALID BUSINESS DAY
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pdaCpwa115.getCpwa115_Error_Return_Code().equals(getZero())))                                                                       //Natural: IF ERROR-RETURN-CODE EQ 0
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, " CPWN116 error, this date will be accepted",pnd_Wk_Check_Date, new ReportEditMask ("YYYY'/'MM'/'DD"),                  //Natural: WRITE ' CPWN116 error, this date will be accepted' #WK-CHECK-DATE ( EM = YYYY'/'MM'/'DD ) ERROR-RETURN-CODE
                                pdaCpwa115.getCpwa115_Error_Return_Code());
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa140z.getGtn_Pda_Z_Pymnt_Settlmnt_Dte().setValue(pnd_Wk_Check_Date);                                                                                    //Natural: ASSIGN GTN-PDA-Z.PYMNT-SETTLMNT-DTE := #WK-CHECK-DATE
            pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Pymnt_Settlmnt_Eff_Dte().setValue(pnd_Wk_Check_Date);                                                                        //Natural: ASSIGN GTN-PDA-Z.##PYMNT-SETTLMNT-EFF-DTE := #WK-CHECK-DATE
            //*  GTN-PDA-Z.PYMNT-SETTLMNT-DTE       := GTN-PDA-E.PYMNT-CHECK-DTE - 1
            //*  GTN-PDA-Z.##PYMNT-SETTLMNT-EFF-DTE := GTN-PDA-E.PYMNT-CHECK-DTE - 1
            //*   2 LINES ABOVE WERE THE ORIGINAL CODES COMMENTED OUT FE 201210 END
            pnd_Gtn_Error.setValue(false);                                                                                                                                //Natural: MOVE FALSE TO #GTN-ERROR
            //*  CALLNAT 'FCPN140Z' USING GTN-PDA-Z       /* CTS-0115
            //*  CTS-0115
            DbsUtil.callnat(Fcpn140v.class , getCurrentProcessState(), pdaFcpa140z.getGtn_Pda_Z());                                                                       //Natural: CALLNAT 'FCPN140V' USING GTN-PDA-Z
            if (condition(Global.isEscape())) return;
            //*  TAX(ES) NOT TAKEN
            if (condition(pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code().notEquals("0000") && pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code().notEquals("7777")))                        //Natural: IF GTN-PDA-Z.GTN-RET-CODE NE '0000' AND GTN-PDA-Z.GTN-RET-CODE NE '7777'
            {
                                                                                                                                                                          //Natural: PERFORM Z100-DISPLAY-TAX-ERROR
                sub_Z100_Display_Tax_Error();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(94);  if (true) return;                                                                                                                 //Natural: TERMINATE 0094
                //*  WON't happen. Terminated.
                pnd_Gtn_Error.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO #GTN-ERROR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (DbsUtil.maskMatches(pdaFcpa140z.getGtn_Pda_Z_Tax_Sta_C_Fdrl_Tx_Pct(),"99999"))))                                                             //Natural: IF GTN-PDA-Z.TAX-STA-C-FDRL-TX-PCT NE MASK ( 99999 )
            {
                pdaFcpa140z.getGtn_Pda_Z_Tax_Sta_C_Fdrl_Tx_Pct().reset();                                                                                                 //Natural: RESET GTN-PDA-Z.TAX-STA-C-FDRL-TX-PCT
            }                                                                                                                                                             //Natural: END-IF
            //*  FROM FCPA140Z
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().setValuesByName(pdaFcpa140z.getGtn_Pda_Z());                                                              //Natural: MOVE BY NAME GTN-PDA-Z TO WF-PYMNT-ADDR-REC
            //*  TO   FCPA800   /* ABRM
            pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec().reset();                                                                                                    //Natural: RESET WF-PYMNT-TAX-REC
            //*  ABRM
            pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec().setValuesByName(pdaFcpa140z.getGtn_Pda_Z());                                                                //Natural: MOVE BY NAME GTN-PDA-Z TO WF-PYMNT-TAX-REC
            //*  RS0 FROM FCPA140Z TO FCPA800
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde().setValue(pdaFcpa140z.getGtn_Pda_Z_Ia_Orgn_Cde());                                                               //Natural: MOVE GTN-PDA-Z.IA-ORGN-CDE TO WF-PYMNT-ADDR-GRP.IA-ORGN-CDE
            //* RS1
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number().setValue(pdaFcpa140e.getGtn_Pda_E_Plan_Number());                                                               //Natural: MOVE GTN-PDA-E.PLAN-NUMBER TO WF-PYMNT-ADDR-GRP.PLAN-NUMBER
            //* RS1
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan().setValue(pdaFcpa140e.getGtn_Pda_E_Sub_Plan());                                                                     //Natural: MOVE GTN-PDA-E.SUB-PLAN TO WF-PYMNT-ADDR-GRP.SUB-PLAN
            //* RS1
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr().setValue(pdaFcpa140e.getGtn_Pda_E_Orig_Cntrct_Nbr());                                                       //Natural: MOVE GTN-PDA-E.ORIG-CNTRCT-NBR TO WF-PYMNT-ADDR-GRP.ORIG-CNTRCT-NBR
            //* RS1
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan().setValue(pdaFcpa140e.getGtn_Pda_E_Orig_Sub_Plan());                                                           //Natural: MOVE GTN-PDA-E.ORIG-SUB-PLAN TO WF-PYMNT-ADDR-GRP.ORIG-SUB-PLAN
            //*  PA-SELECT NET AMOUNT DEDUCTION ADJUSTMENTS WILL BE CARRIED
            //*  IN INV-ACCT-DCI-AMT - FOR 'AP' PAYMENTS
            if (condition((pdaFcpa140z.getGtn_Pda_Z_Pymnt_Ded_Amt().getValue("*").notEquals(getZero()) && (pdaFcpa140z.getGtn_Pda_Z_Cntrct_Annty_Ins_Type().equals("S")   //Natural: IF GTN-PDA-Z.PYMNT-DED-AMT ( * ) NE 0 AND ( GTN-PDA-Z.CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M' )
                || pdaFcpa140z.getGtn_Pda_Z_Cntrct_Annty_Ins_Type().equals("M")))))
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded().getValue(1,":",40).setValue(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Ded_Amt().getValue(1,        //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-NET-ADJ-DED ( 1 : 40 ) := GTN-PDA-Z.##INV-ACCT-DED-AMT ( 1 : 40 )
                    ":",40));
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa140e.getGtn_Pda_E_Current_Mode().equals("0")))                                                                                           //Natural: IF GTN-PDA-E.CURRENT-MODE = '0'
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Off_Mode_Contract().setValue(true);                                                                                       //Natural: MOVE TRUE TO WF-PYMNT-ADDR-GRP.OFF-MODE-CONTRACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Off_Mode_Contract().setValue(false);                                                                                      //Natural: MOVE FALSE TO WF-PYMNT-ADDR-GRP.OFF-MODE-CONTRACT
            }                                                                                                                                                             //Natural: END-IF
            //*  INVALID COMBINE
            short decideConditionsMet2084 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN GTN-PDA-E.ERROR-CODE-2 = '1'
            if (condition(pdaFcpa140e.getGtn_Pda_E_Error_Code_2().equals("1")))
            {
                decideConditionsMet2084++;
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().setValue("2");                                                                                        //Natural: MOVE '2' TO WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE
            }                                                                                                                                                             //Natural: WHEN GTN-PDA-E.PYMNT-SUSPEND-CDE = '0' OR = ' '
            else if (condition(pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals("0") || pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde().equals(" ")))
            {
                decideConditionsMet2084++;
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().reset();                                                                                              //Natural: RESET WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE
                //*  MISSING N/A
                if (condition(pdaFcpa140e.getGtn_Pda_E_Error_Code_1().equals("1")))                                                                                       //Natural: IF GTN-PDA-E.ERROR-CODE-1 = '1'
                {
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().setValue("1");                                                                                    //Natural: MOVE '1' TO WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().setValue(pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Cde());                                               //Natural: MOVE GTN-PDA-E.PYMNT-SUSPEND-CDE TO WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte().setValue(pdaFcpa140e.getGtn_Pda_E_Pymnt_Suspend_Dte());                                               //Natural: MOVE GTN-PDA-E.PYMNT-SUSPEND-DTE TO WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-DTE
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  TAXES ARE NOT TO BE WITHHELD FOR INTERNAL ROLLOVERS, BLOCKED
            //*  ACCOUNTS AND UNCLAIMED SUMS.
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("I")           //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 8 OR WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE = 'I' OR = 'K'
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("K")))
            {
                                                                                                                                                                          //Natural: PERFORM U100-UNDO-TAX-CALC
                sub_U100_Undo_Tax_Calc();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde().setValue("AP");                                                                                             //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE := 'AP'
            pnd_Date_X.setValueEdited(pdaFcpa140z.getGtn_Pda_Z_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED GTN-PDA-Z.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE-X
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte().compute(new ComputeParameters(false, pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte()),                //Natural: COMPUTE WF-PYMNT-ADDR-GRP.CNTRCT-INVRSE-DTE = 100000000 - #DATE-N
                DbsField.subtract(100000000,pnd_Date_X_Pnd_Date_N));
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num().setValue(pnd_Sv_Prcss_Seq_Num);                                                                         //Natural: MOVE #SV-PRCSS-SEQ-NUM TO WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NUM
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().setValue(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_This_Pymnt());                                                   //Natural: MOVE GTN-PDA-Z.##THIS-PYMNT TO WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR
            //*  NAME AND ADDRESS PORTION
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Rcrd_Typ().setValue("N");                                                                                                     //Natural: ASSIGN WF-PYMNT-ADDR-GRP.RCRD-TYP := 'N'
            //*  THIS ROUTINE WILL MOVE THE ZIP CODE TO THE LAST LINE OF ADDRESS
            FOR04:                                                                                                                                                        //Natural: FOR #INDEX 1 2
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(2)); pnd_Index.nadd(1))
            {
                if (condition(DbsUtil.maskMatches(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde().getValue(pnd_Index),"NNNNN....")))                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-ADDR-ZIP-CDE ( #INDEX ) = MASK ( NNNNN.... )
                {
                    //* CTS-0115
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde().getValue(pnd_Index).setValue("N");                                                                //Natural: MOVE 'N' TO WF-PYMNT-ADDR-GRP.PYMNT-FOREIGN-CDE ( #INDEX )
                    pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr1().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(pnd_Index));                      //Natural: ASSIGN FCPA147-ADDR1 := WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE1-TXT ( #INDEX )
                    pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr2().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(pnd_Index));                      //Natural: ASSIGN FCPA147-ADDR2 := WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE2-TXT ( #INDEX )
                    pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr3().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(pnd_Index));                      //Natural: ASSIGN FCPA147-ADDR3 := WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE3-TXT ( #INDEX )
                    pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr4().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(pnd_Index));                      //Natural: ASSIGN FCPA147-ADDR4 := WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE4-TXT ( #INDEX )
                    pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr5().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(pnd_Index));                      //Natural: ASSIGN FCPA147-ADDR5 := WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE5-TXT ( #INDEX )
                    pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr6().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(pnd_Index));                      //Natural: ASSIGN FCPA147-ADDR6 := WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE6-TXT ( #INDEX )
                    pdaFcpa147.getFcpa147_Parm_Fcpa147_Zip().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde().getValue(pnd_Index));                          //Natural: ASSIGN FCPA147-ZIP := WF-PYMNT-ADDR-GRP.PYMNT-ADDR-ZIP-CDE ( #INDEX )
                    DbsUtil.callnat(Fcpn147.class , getCurrentProcessState(), pdaFcpa147.getFcpa147_Parm());                                                              //Natural: CALLNAT 'FCPN147' USING FCPA147-PARM
                    if (condition(Global.isEscape())) return;
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(pnd_Index).setValue(pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr1());                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE1-TXT ( #INDEX ) := FCPA147-ADDR1
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(pnd_Index).setValue(pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr2());                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE2-TXT ( #INDEX ) := FCPA147-ADDR2
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(pnd_Index).setValue(pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr3());                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE3-TXT ( #INDEX ) := FCPA147-ADDR3
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(pnd_Index).setValue(pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr4());                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE4-TXT ( #INDEX ) := FCPA147-ADDR4
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(pnd_Index).setValue(pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr5());                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE5-TXT ( #INDEX ) := FCPA147-ADDR5
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(pnd_Index).setValue(pdaFcpa147.getFcpa147_Parm_Fcpa147_Addr6());                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINE6-TXT ( #INDEX ) := FCPA147-ADDR6
                    //* CTS-0115
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //* CTS-0115
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde().getValue(pnd_Index).setValue("Y");                                                                //Natural: MOVE 'Y' TO WF-PYMNT-ADDR-GRP.PYMNT-FOREIGN-CDE ( #INDEX )
                }                                                                                                                                                         //Natural: END-IF
                //*  WAITING
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PAYMENT PORTION
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Crrncy_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Check_Crrncy_Cde());                                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-CRRNCY-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-CHECK-CRRNCY-CDE
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Stats_Cde().setValue("W");                                                                                              //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-STATS-CDE := 'W'
            //*  EURO
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde().notEquals(pdaFcpa140e.getGtn_Pda_E_Global_Country().getSubstring(2, //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 3 AND WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE NE SUBSTR ( GTN-PDA-E.GLOBAL-COUNTRY,2,2 )
                2))))
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Check_Crrncy_Cde().setValue("3");                                                                                  //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-CHECK-CRRNCY-CDE := '3'
            }                                                                                                                                                             //Natural: END-IF
            //*  INTERNAL ROLLOVER
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                                            //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 8
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().reset();                                                                                                //Natural: RESET WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE
                //*  FOR TAX
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind().setValue("P");                                                                                    //Natural: MOVE 'P' TO WF-PYMNT-ADDR-GRP.CNTRCT-PYMNT-TYPE-IND
                //*  FOR TAX
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().setValue("R");                                                                                  //Natural: MOVE 'R' TO WF-PYMNT-ADDR-GRP.CNTRCT-STTLMNT-TYPE-IND
            }                                                                                                                                                             //Natural: END-IF
            //*  FUND ACCOUNT PORTION
            pnd_Pymnt_Rpt_Index.setValue(0);                                                                                                                              //Natural: ASSIGN #PYMNT-RPT-INDEX := 0
            pnd_Cntrct_Rpt_Index.setValue(7);                                                                                                                             //Natural: ASSIGN #CNTRCT-RPT-INDEX := 7
            FOR05:                                                                                                                                                        //Natural: FOR #INDEX 1 #MAX
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pnd_Max)); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("  ")))                                                     //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = '  '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Fund_Ct.setValue(pnd_Index);                                                                                                                          //Natural: ASSIGN #FUND-CT := #INDEX
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG")  //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' OR = 'TG' OR = 'G '
                    || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G ")))
                {
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Index).setValue(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Cntrct_Amt().getValue(pnd_Index)); //Natural: MOVE GTN-PDA-Z.##INV-ACCT-CNTRCT-AMT ( #INDEX ) TO WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( #INDEX )
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index).setValue(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index)); //Natural: MOVE GTN-PDA-Z.##INV-ACCT-DVDND-AMT ( #INDEX ) TO WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX )
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(pnd_Index).reset();                                                                      //Natural: RESET WF-PYMNT-ADDR-GRP.INV-ACCT-UNIT-QTY ( #INDEX ) WF-PYMNT-ADDR-GRP.INV-ACCT-UNIT-VALUE ( #INDEX )
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_Index).reset();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue(pnd_Index).setValue(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Unit_Qty().getValue(pnd_Index)); //Natural: MOVE GTN-PDA-Z.##INV-ACCT-UNIT-QTY ( #INDEX ) TO WF-PYMNT-ADDR-GRP.INV-ACCT-UNIT-QTY ( #INDEX )
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue(pnd_Index).setValue(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Unit_Value().getValue(pnd_Index)); //Natural: MOVE GTN-PDA-Z.##INV-ACCT-UNIT-VALUE ( #INDEX ) TO WF-PYMNT-ADDR-GRP.INV-ACCT-UNIT-VALUE ( #INDEX )
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Index).reset();                                                                    //Natural: RESET WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( #INDEX ) WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX )
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index).reset();
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index).greaterOrEqual(getZero())))                                    //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) GE 0
                {
                    //*  TAX(ES) NOT TAKEN
                    if (condition(pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code().equals("7777")))                                                                                //Natural: IF GTN-PDA-Z.GTN-RET-CODE = '7777'
                    {
                        pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code().setValue("0000");                                                                                         //Natural: MOVE '0000' TO GTN-PDA-Z.GTN-RET-CODE
                        pnd_Do_Rpt_Code.setValue(3);                                                                                                                      //Natural: ASSIGN #DO-RPT-CODE := 03
                                                                                                                                                                          //Natural: PERFORM E200-ERROR-CONTRACT
                        sub_E200_Error_Contract();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  POPULATE DEDUCTION COUNT FIELD
                FOR06:                                                                                                                                                    //Natural: FOR #INDEX2 1 10
                for (pnd_Index2.setValue(1); condition(pnd_Index2.lessOrEqual(10)); pnd_Index2.nadd(1))
                {
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index2).equals(0)))                                                        //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( #INDEX2 ) = 000
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp().setValue(pnd_Index2);                                                                               //Natural: ASSIGN C-PYMNT-DED-GRP := #INDEX2
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  IF THE NET AMOUNT IS NEGATIVE, RE-CALCULATE THE NET AMOUNT
                //*  WITHOUT APPLYING THE VOLUNTARY DEDUCTIONS
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index).nadd(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Inv_Acct_Ded_Amt().getValue(pnd_Index)); //Natural: ADD GTN-PDA-Z.##INV-ACCT-DED-AMT ( #INDEX ) TO WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX )
                //*  NET NEGATIVE
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index).less(getZero())))                                              //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) < 0
                {
                    pnd_Do_Rpt_Code.setValue(15);                                                                                                                         //Natural: ASSIGN #DO-RPT-CODE := 15
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
                    sub_E100_Error_Payment();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code().equals("7777")))                                                                                    //Natural: IF GTN-PDA-Z.GTN-RET-CODE = '7777'
                {
                    pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code().setValue("0000");                                                                                             //Natural: MOVE '0000' TO GTN-PDA-Z.GTN-RET-CODE
                    pnd_Do_Rpt_Code.setValue(5);                                                                                                                          //Natural: ASSIGN #DO-RPT-CODE := 05
                                                                                                                                                                          //Natural: PERFORM E200-ERROR-CONTRACT
                    sub_E200_Error_Contract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Do_Rpt_Code.setValue(4);                                                                                                                          //Natural: ASSIGN #DO-RPT-CODE := 04
                                                                                                                                                                          //Natural: PERFORM E200-ERROR-CONTRACT
                    sub_E200_Error_Contract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count().setValue(pnd_Fund_Ct);                                                                                       //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT := #FUND-CT
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Ivc_Ind().getValue(1,":",pnd_Fund_Ct).setValue(pdaFcpa140z.getGtn_Pda_Z_Pymnt_Settl_Ivc_Ind());                      //Natural: MOVE GTN-PDA-Z.PYMNT-SETTL-IVC-IND TO WF-PYMNT-ADDR-GRP.INV-ACCT-IVC-IND ( 1:#FUND-CT )
            if (condition(pdaFcpa140z.getGtn_Pda_Z_Cntrct_Ac_Lt_10yrs().equals(true)))                                                                                    //Natural: IF GTN-PDA-Z.CNTRCT-AC-LT-10YRS = TRUE THEN
            {
                setValueToSubstring("Y",pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Free_Bytes(),6,1);                                                                            //Natural: MOVE 'Y' TO SUBSTRING ( WF-PYMNT-ADDR-GRP.TAX-FREE-BYTES,6,1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                setValueToSubstring("N",pdaFcpa800.getWf_Pymnt_Addr_Grp_Tax_Free_Bytes(),6,1);                                                                            //Natural: MOVE 'N' TO SUBSTRING ( WF-PYMNT-ADDR-GRP.TAX-FREE-BYTES,6,1 )
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM D100-DETERMINE-REPORTS
            sub_D100_Determine_Reports();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM W100-WRITE-OUTPUT
            sub_W100_Write_Output();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM P100-PROCESS-CONTROLS
            sub_P100_Process_Controls();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM W200-WRITE-CONTROL-TOTALS
        sub_W200_Write_Control_Totals();
        if (condition(Global.isEscape())) {return;}
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A100-RESTART-ROUTINE
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A200-INITIALIZE-ROUTINE
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A300-ALLOCATE-IVC
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C100-COMPUTE-TOTALS
        //* *
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: D100-DETERMINE-REPORTS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: E100-ERROR-PAYMENT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: E200-ERROR-CONTRACT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: N100-IDENTIFY-TPA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: P100-PROCESS-CONTROLS
        //* ****************
        //*                                                   /* BEG LEON 03/15/01
        //* ***********************************************************************
        //*  ----------------------------------------------------ROXAN 05/07/-01
        //*  SPLIT T100-DECIDE-ROLLOVERS INTO
        //*        N100-IDENTIFY-TPA AND DECIDE ROLLOVER
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: S100-SAVE-CONTROL-TOTALS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: T100-DECIDE-ROLLOVERS
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: U100-UNDO-TAX-CALC
        //* ****************
        //*    + INV-ACCT-CAN-TAX-AMT(#INDEX)
        //*    INV-ACCT-CAN-TAX-AMT(#INDEX)
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: W100-WRITE-OUTPUT
        //*  IF PAYMENT TYPE IS EFT OR G.P AND, GROSS-TO-NET CALCULATION RESULTED
        //*  IN NET ZERO, THE PAYMENT TYPE WILL BE CONVERTED TO '1' BY FCPP801
        //*    ADD GTN-PDA-Z.GTN-FED-CANADIAN-AMT(*) TO
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: W200-WRITE-CONTROL-TOTALS
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: Z100-DISPLAY-TAX-ERROR
    }
    private void sub_A100_Restart_Routine() throws Exception                                                                                                              //Natural: A100-RESTART-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  FCPA800 DUMMIED OUT
        READWORK03:                                                                                                                                                       //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            //*  FCPA140E
            getWorkFiles().read(2, pdaFcpa140e.getGtn_Pda_E());                                                                                                           //Natural: READ WORK FILE 2 ONCE GTN-PDA-E
            //*  ##PDA-COUNT NEEDS THE LAST SEQ # FOR FCPN140Z TO CONTINUE CALCULATION
            pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Pda_Count().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR TO GTN-PDA-Z.##PDA-COUNT
            pnd_Sv_Prcss_Seq_Num.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num());                                                                         //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NUM TO #SV-PRCSS-SEQ-NUM
            pnd_Sv_Return_Code.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Ret_Code());                                                                                  //Natural: MOVE WF-PYMNT-ADDR-GRP.GTN-RET-CODE TO #SV-RETURN-CODE
            pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code.getValue("*").setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue("*"));                                        //Natural: MOVE WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( * ) TO #SV-RPT-CODE ( * )
            pnd_Sv_Pay_Type_Ind.setValue(pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind());                                                                              //Natural: MOVE GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND TO #SV-PAY-TYPE-IND
            if (condition(pdaFcpa140e.getGtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts().notEquals(1)))                                                                                 //Natural: IF GTN-PDA-E.##NBR-OF-PYMNTS NE 1
            {
                pnd_Combined_Contract.setValue(true);                                                                                                                     //Natural: MOVE TRUE TO #COMBINED-CONTRACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Combined_Contract.setValue(false);                                                                                                                    //Natural: MOVE FALSE TO #COMBINED-CONTRACT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa140e.getGtn_Pda_E_Pnd_Pnd_This_Pymnt().equals(1)))                                                                                       //Natural: IF GTN-PDA-E.##THIS-PYMNT = 01
            {
                pnd_Check_Amt.compute(new ComputeParameters(false, pnd_Check_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,                   //Natural: COMPUTE #CHECK-AMT = WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT ) + 0
                    ":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Check_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));            //Natural: COMPUTE #CHECK-AMT = #CHECK-AMT + WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT )
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM P100-PROCESS-CONTROLS
            sub_P100_Process_Controls();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK03_Exit:
        if (Global.isEscape()) return;
        //*  A100-RESTART-ROUTINE
    }
    private void sub_A200_Initialize_Routine() throws Exception                                                                                                           //Natural: A200-INITIALIZE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().reset();                                                                                                      //Natural: RESET WF-PYMNT-ADDR-REC
        pnd_Pymnt_Rpt_Index.reset();                                                                                                                                      //Natural: RESET #PYMNT-RPT-INDEX #CNTRCT-RPT-INDEX #CHECK-AMT GTN-PDA-Z.##PDA-COUNT
        pnd_Cntrct_Rpt_Index.reset();
        pnd_Check_Amt.reset();
        pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Pda_Count().reset();
        pnd_Sv_Return_Code.moveAll("0");                                                                                                                                  //Natural: MOVE ALL '0' TO #SV-RETURN-CODE
        pnd_Sv_Rpt_Codes.moveAll("0");                                                                                                                                    //Natural: MOVE ALL '0' TO #SV-RPT-CODES
        pnd_Sv_Prcss_Seq_Num.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #SV-PRCSS-SEQ-NUM
        pnd_Sv_Pay_Type_Ind.setValue(pdaFcpa140e.getGtn_Pda_E_Pymnt_Pay_Type_Req_Ind());                                                                                  //Natural: MOVE GTN-PDA-E.PYMNT-PAY-TYPE-REQ-IND TO #SV-PAY-TYPE-IND
        if (condition(pdaFcpa140e.getGtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts().notEquals(1)))                                                                                     //Natural: IF GTN-PDA-E.##NBR-OF-PYMNTS NE 1
        {
            pnd_Combined_Contract.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #COMBINED-CONTRACT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Combined_Contract.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #COMBINED-CONTRACT
        }                                                                                                                                                                 //Natural: END-IF
        //*  A200-INITIALIZE-ROUTINE
    }
    private void sub_A300_Allocate_Ivc() throws Exception                                                                                                                 //Natural: A300-ALLOCATE-IVC
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  LAST FUND
        if (condition(pnd_Index.equals(pnd_Max_Funds) || pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Cde().getValue(pnd_Index.getDec().add(1)).equals(" ")))                        //Natural: IF #INDEX = #MAX-FUNDS OR GTN-PDA-Z.INV-ACCT-CDE ( #INDEX +1 ) = ' '
        {
            pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Ivc_Amt().getValue(pnd_Index).compute(new ComputeParameters(false, pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Ivc_Amt().getValue(pnd_Index)),  //Natural: COMPUTE GTN-PDA-Z.INV-ACCT-IVC-AMT ( #INDEX ) = #TOTAL-IVC - #IVC-SUM
                pnd_Total_Ivc.subtract(pnd_Ivc_Sum));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Ivc_Amt().getValue(pnd_Index).compute(new ComputeParameters(false, pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Ivc_Amt().getValue(pnd_Index)),  //Natural: COMPUTE GTN-PDA-Z.INV-ACCT-IVC-AMT ( #INDEX ) = #TOTAL-IVC * ( GTN-PDA-Z.INV-ACCT-SETTL-AMT ( #INDEX ) / #TOTAL-GROSS )
                pnd_Total_Ivc.multiply((pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Settl_Amt().getValue(pnd_Index).divide(pnd_Total_Gross))));
            pnd_Ivc_Sum.nadd(pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Ivc_Amt().getValue(pnd_Index));                                                                            //Natural: ADD GTN-PDA-Z.INV-ACCT-IVC-AMT ( #INDEX ) TO #IVC-SUM
        }                                                                                                                                                                 //Natural: END-IF
        //*  A300-ALLOCATE-IVC
    }
    private void sub_C100_Compute_Totals() throws Exception                                                                                                               //Natural: C100-COMPUTE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        short decideConditionsMet2361 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE;//Natural: VALUE ' '
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals(" "))))
        {
            decideConditionsMet2361++;
            short decideConditionsMet2363 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet2363++;
                //*  CHECK
                pnd_T_Index.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #T-INDEX
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet2363++;
                //*  EFT
                //* TMM-2001/11
                pnd_T_Index.nadd(2);                                                                                                                                      //Natural: ADD 2 TO #T-INDEX
            }                                                                                                                                                             //Natural: VALUE 3,4,9
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4) 
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9))))
            {
                decideConditionsMet2363++;
                //*  GLOBAL PAY
                pnd_T_Index.nadd(3);                                                                                                                                      //Natural: ADD 3 TO #T-INDEX
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8))))
            {
                decideConditionsMet2363++;
                //*  INTERNAL ROLLOVER
                pnd_T_Index.nadd(4);                                                                                                                                      //Natural: ADD 4 TO #T-INDEX
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                //*  MISCELLANEOUS PENDS & INVALID PIN
                pnd_T_Index.nadd(9);                                                                                                                                      //Natural: ADD 9 TO #T-INDEX
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().setValue("M");                                                                                        //Natural: MOVE 'M' TO WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE '1'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("1"))))
        {
            decideConditionsMet2361++;
            //*  MISSING N/A
            pnd_T_Index.nadd(7);                                                                                                                                          //Natural: ADD 7 TO #T-INDEX
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("2"))))
        {
            decideConditionsMet2361++;
            //*  INVALID COMBINES
            pnd_T_Index.nadd(8);                                                                                                                                          //Natural: ADD 8 TO #T-INDEX
        }                                                                                                                                                                 //Natural: VALUE 'I'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("I"))))
        {
            decideConditionsMet2361++;
            //*  BLOCKED ACCOUNTS
            pnd_T_Index.nadd(5);                                                                                                                                          //Natural: ADD 5 TO #T-INDEX
        }                                                                                                                                                                 //Natural: VALUE 'K'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("K"))))
        {
            decideConditionsMet2361++;
            //*  UNCLAIMED SUMS
            pnd_T_Index.nadd(6);                                                                                                                                          //Natural: ADD 6 TO #T-INDEX
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            //*  MISCELLANEOUS PENDS & INVALID PIN
            pnd_T_Index.nadd(9);                                                                                                                                          //Natural: ADD 9 TO #T-INDEX
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(2,pnd_T_Index).nadd(1);                                                                                    //Natural: ADD 1 TO #REC-COUNT ( 2, #T-INDEX )
        if (condition(pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_This_Pymnt().equals(1)))                                                                                           //Natural: IF GTN-PDA-Z.##THIS-PYMNT = 1
        {
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pay_Count().getValue(2,pnd_T_Index).nadd(1);                                                                                //Natural: ADD 1 TO #PAY-COUNT ( 2, #T-INDEX )
        }                                                                                                                                                                 //Natural: END-IF
        //*   MOVED HERE FROM FOR LOOP --->>  ROXAN 05/07/01
        //* *                                              /* BEG LEON 03/15/01
        if (condition(pnd_T_Index.equals(13)))                                                                                                                            //Natural: IF #T-INDEX = 13
        {
            pnd_Leon_Indx_04_Or_13.setValue(2);                                                                                                                           //Natural: MOVE 2 TO #LEON-INDX-04-OR-13
                                                                                                                                                                          //Natural: PERFORM N100-IDENTIFY-TPA
            sub_N100_Identify_Tpa();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_T_Index.equals(4)))                                                                                                                         //Natural: IF #T-INDEX = 4
            {
                pnd_Leon_Indx_04_Or_13.setValue(1);                                                                                                                       //Natural: MOVE 1 TO #LEON-INDX-04-OR-13
                                                                                                                                                                          //Natural: PERFORM N100-IDENTIFY-TPA
                sub_N100_Identify_Tpa();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR07:                                                                                                                                                            //Natural: FOR #INDEX 1 WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
        {
            //* *                                              /* BEG LEON 03/15/01
            if (condition(pnd_T_Index.equals(13)))                                                                                                                        //Natural: IF #T-INDEX = 13
            {
                //*       MOVE 2 TO  #LEON-INDX-04-OR-13
                                                                                                                                                                          //Natural: PERFORM T100-DECIDE-ROLLOVERS
                sub_T100_Decide_Rollovers();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_T_Index.equals(4)))                                                                                                                     //Natural: IF #T-INDEX = 4
                {
                    //*       MOVE 1 TO  #LEON-INDX-04-OR-13
                                                                                                                                                                          //Natural: PERFORM T100-DECIDE-ROLLOVERS
                    sub_T100_Decide_Rollovers();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //* *                                               /* END LEON 03/15/01
            //*  FREE LOOK
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG")  //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' OR = 'TG' OR = 'G ' OR = '41'
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("41")))
            {
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Index));   //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( #INDEX ) TO #CNTRCT-AMT ( 2, #T-INDEX )
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT ( 2, #T-INDEX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #CREF-AMT ( 2, #T-INDEX )
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #GROSS-AMT ( 2, #T-INDEX )
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Index));        //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( #INDEX ) TO #TAX-AMT ( 2, #T-INDEX )
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_Index));       //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( #INDEX ) TO #TAX-AMT ( 2, #T-INDEX )
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Tax_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_Index));       //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( #INDEX ) TO #TAX-AMT ( 2, #T-INDEX )
            pdaFcpa800a.getPnd_Cps_Totals_Pnd_Net_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));       //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #NET-AMT ( 2, #T-INDEX )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(2,pnd_T_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*"));                          //Natural: ADD WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) TO #DED-AMT ( 2, #T-INDEX )
        //*  C100-COMPUTE-TOTALS
    }
    private void sub_D100_Determine_Reports() throws Exception                                                                                                            //Natural: D100-DETERMINE-REPORTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  TO MAINTAIN ERROR
        if (condition(pnd_Sv_Return_Code.equals("0000")))                                                                                                                 //Natural: IF #SV-RETURN-CODE = '0000'
        {
            //*  OF PREV RECORD
            pnd_Sv_Return_Code.setValue(pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code());                                                                                         //Natural: MOVE GTN-PDA-Z.GTN-RET-CODE TO #SV-RETURN-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  INTERNAL ROLLOVER
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                                                //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 8
        {
            pnd_Do_Rpt_Code.setValue(2);                                                                                                                                  //Natural: ASSIGN #DO-RPT-CODE := 02
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  IRS LIENS ?????
        //*  (M=MISCELLANEOUS)
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("?")))                                                                                   //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE = '?'
        {
            pnd_Do_Rpt_Code.setValue(6);                                                                                                                                  //Natural: ASSIGN #DO-RPT-CODE := 06
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  DVDND TO COLLEGE
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind().equals(2)))                                                                                //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-DVDND-PAYEE-IND = 2
        {
            pnd_Do_Rpt_Code.setValue(7);                                                                                                                                  //Natural: ASSIGN #DO-RPT-CODE := 07
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  DEDUCTION(S)
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").notEquals(getZero())))                                                                //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) NE 0
        {
            pnd_Do_Rpt_Code.setValue(8);                                                                                                                                  //Natural: ASSIGN #DO-RPT-CODE := 08
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  BLOCKED/UNCLAIMED
        short decideConditionsMet2475 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE;//Natural: VALUE 'I', 'K'
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("I") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("K"))))
        {
            decideConditionsMet2475++;
            pnd_Do_Rpt_Code.setValue(9);                                                                                                                                  //Natural: ASSIGN #DO-RPT-CODE := 09
            //*  MISSING N/A
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '1'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("1"))))
        {
            decideConditionsMet2475++;
            pnd_Do_Rpt_Code.setValue(10);                                                                                                                                 //Natural: ASSIGN #DO-RPT-CODE := 10
            //*  INVALID COMBINE
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("2"))))
        {
            decideConditionsMet2475++;
            pnd_Do_Rpt_Code.setValue(11);                                                                                                                                 //Natural: ASSIGN #DO-RPT-CODE := 11
            //*  BLANK/0/INVALID PIN
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE '3'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("3"))))
        {
            decideConditionsMet2475++;
            pnd_Do_Rpt_Code.setValue(12);                                                                                                                                 //Natural: ASSIGN #DO-RPT-CODE := 12
            //*  A MACHINE GEN. DEATH
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'M', 'A', 'C', 'E', 'G', 'P', 'Q', 'S','R'
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("M") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("A") 
            || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("C") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("E") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("G") 
            || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("P") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("Q") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("S") 
            || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("R"))))
        {
            decideConditionsMet2475++;
            pnd_Do_Rpt_Code.setValue(13);                                                                                                                                 //Natural: ASSIGN #DO-RPT-CODE := 13
            //*  C PEND. DEATH MANUAL
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CTS-0917 >>
        //* *IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND NE 8  /* INT ROLL
        //*   IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND     /* MULTIPLE PAY TYPE
        //*       NE #SV-PAY-TYPE-IND
        //*     #DO-RPT-CODE := 17
        //*     PERFORM E100-ERROR-PAYMENT
        //*   END-IF
        //* *END-IF
        //*  CTS-0917 <<
        if (condition(pnd_Negative_Ivc.getBoolean()))                                                                                                                     //Natural: IF #NEGATIVE-IVC
        {
            pnd_Do_Rpt_Code.setValue(18);                                                                                                                                 //Natural: ASSIGN #DO-RPT-CODE := 18
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  ERROR FROM CALL OF FCPN140Z
        if (condition(pnd_Gtn_Error.getBoolean()))                                                                                                                        //Natural: IF #GTN-ERROR
        {
            pnd_Do_Rpt_Code.setValue(14);                                                                                                                                 //Natural: ASSIGN #DO-RPT-CODE := 14
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
            sub_E100_Error_Payment();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  D100-DETERMINE-REPORTS
    }
    private void sub_E100_Error_Payment() throws Exception                                                                                                                //Natural: E100-ERROR-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        if (condition(pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code.getValue(1,":",7).equals(pnd_Do_Rpt_Code)))                                                                        //Natural: IF #SV-RPT-CODE ( 1:7 ) = #DO-RPT-CODE
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pymnt_Rpt_Index.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #PYMNT-RPT-INDEX
            pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code.getValue(pnd_Pymnt_Rpt_Index).setValue(pnd_Do_Rpt_Code);                                                                     //Natural: MOVE #DO-RPT-CODE TO #SV-RPT-CODE ( #PYMNT-RPT-INDEX )
            if (condition(pnd_Do_Rpt_Code.greaterOrEqual(9) && pnd_Do_Rpt_Code.lessOrEqual(13)))                                                                          //Natural: IF #DO-RPT-CODE = 09 THRU 13 THEN
            {
                pnd_Sv_Return_Code.setValue("0002");                                                                                                                      //Natural: MOVE '0002' TO #SV-RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Do_Rpt_Code.greaterOrEqual(14) && pnd_Do_Rpt_Code.lessOrEqual(18)))                                                                         //Natural: IF #DO-RPT-CODE = 14 THRU 18 THEN
            {
                pnd_Sv_Return_Code.setValue("0003");                                                                                                                      //Natural: MOVE '0003' TO #SV-RETURN-CODE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  E100-ERROR-PAYMENT
    }
    private void sub_E200_Error_Contract() throws Exception                                                                                                               //Natural: E200-ERROR-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        if (condition(pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code.getValue(8,":",10).equals(pnd_Do_Rpt_Code)))                                                                       //Natural: IF #SV-RPT-CODE ( 8:10 ) = #DO-RPT-CODE
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cntrct_Rpt_Index.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #CNTRCT-RPT-INDEX
            pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code.getValue(pnd_Cntrct_Rpt_Index).setValue(pnd_Do_Rpt_Code);                                                                    //Natural: MOVE #DO-RPT-CODE TO #SV-RPT-CODE ( #CNTRCT-RPT-INDEX )
        }                                                                                                                                                                 //Natural: END-IF
        //*  E200-ERROR-CONTRACT
    }
    private void sub_N100_Identify_Tpa() throws Exception                                                                                                                 //Natural: N100-IDENTIFY-TPA
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  TPA
        //*  P&I
        //*  IPRO
        //*  OTHER
        short decideConditionsMet2552 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE;//Natural: VALUE 28, 30
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(28) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(30))))
        {
            decideConditionsMet2552++;
            pnd_Leon_Index.setValue(1);                                                                                                                                   //Natural: MOVE 1 TO #LEON-INDEX
        }                                                                                                                                                                 //Natural: VALUE 22
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(22))))
        {
            decideConditionsMet2552++;
            pnd_Leon_Index.setValue(2);                                                                                                                                   //Natural: MOVE 2 TO #LEON-INDEX
        }                                                                                                                                                                 //Natural: VALUE 25, 27
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(25) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(27))))
        {
            decideConditionsMet2552++;
            pnd_Leon_Index.setValue(3);                                                                                                                                   //Natural: MOVE 3 TO #LEON-INDEX
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Leon_Index.setValue(4);                                                                                                                                   //Natural: MOVE 4 TO #LEON-INDEX
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Leon_Calc_Report_Pnd_Rec_Count_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(1);                                                                  //Natural: ADD 1 TO #REC-COUNT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        pnd_Leon_Calc_Report_Pnd_Ded_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*"));        //Natural: ADD WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) TO #DED-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        //*  END N100-IDENTIFY-TPA
    }
    private void sub_P100_Process_Controls() throws Exception                                                                                                             //Natural: P100-PROCESS-CONTROLS
    {
        if (BLNatReinput.isReinput()) return;

        FOR08:                                                                                                                                                            //Natural: FOR #INDEX 1 WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("  ")))                                                         //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = '  '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  FREE LOOK
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG")  //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' OR = 'TG' OR = 'G ' OR = '41'
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("41")))
            {
                pnd_T_Cntrct_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( #INDEX ) TO #T-CNTRCT-AMT
                pnd_T_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                                           //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #T-DVDND-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_T_Reac_Cref_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                       //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #T-REAC-CREF-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_T_Index.setValue(0);                                                                                                                                          //Natural: MOVE 0 TO #T-INDEX
                                                                                                                                                                          //Natural: PERFORM C100-COMPUTE-TOTALS
        sub_C100_Compute_Totals();
        if (condition(Global.isEscape())) {return;}
        //*  PAY THIS MONTH
        if (condition(! (pdaFcpa800.getWf_Pymnt_Addr_Grp_Off_Mode_Contract().getBoolean())))                                                                              //Natural: IF NOT WF-PYMNT-ADDR-GRP.OFF-MODE-CONTRACT
        {
            pnd_T_Index.setValue(9);                                                                                                                                      //Natural: MOVE 9 TO #T-INDEX
                                                                                                                                                                          //Natural: PERFORM C100-COMPUTE-TOTALS
            sub_C100_Compute_Totals();
            if (condition(Global.isEscape())) {return;}
            //*  DVDND TO COLL
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind().equals(2)))                                                                            //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-DVDND-PAYEE-IND = 2
            {
                pnd_Index.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                                                     //Natural: MOVE WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT TO #INDEX
                short decideConditionsMet2593 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
                if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1))))
                {
                    decideConditionsMet2593++;
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Chk_Coll_Dvdnd_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Index));          //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#INDEX ) TO #CHK-COLL-DVDND-AMT
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2))))
                {
                    decideConditionsMet2593++;
                    //* TMM-2001/11
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Eft_Coll_Dvdnd_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Index));          //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#INDEX ) TO #EFT-COLL-DVDND-AMT
                }                                                                                                                                                         //Natural: VALUE 3,4,9
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4) 
                    || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9))))
                {
                    decideConditionsMet2593++;
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gp_Coll_Dvdnd_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Index));           //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#INDEX ) TO #GP-COLL-DVDND-AMT
                }                                                                                                                                                         //Natural: VALUE 8
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8))))
                {
                    decideConditionsMet2593++;
                    pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ir_Coll_Dvdnd_Amt().nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1,":",pnd_Index));           //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#INDEX ) TO #IR-COLL-DVDND-AMT
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  P100-PROCESS-CONTROLS
    }
    private void sub_S100_Save_Control_Totals() throws Exception                                                                                                          //Natural: S100-SAVE-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Rec_Count().getValue(1,"*").setValue(ldaFcpl800.getIa_Cntl_Rec_C_Rec_Count().getValue("*"));                                    //Natural: MOVE IA-CNTL-REC.C-REC-COUNT ( * ) TO #CPS-TOTALS.#REC-COUNT ( 1,* )
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cntrct_Amt().getValue(1,"*").setValue(ldaFcpl800.getIa_Cntl_Rec_C_Cntrct_Amt().getValue("*"));                                  //Natural: MOVE IA-CNTL-REC.C-CNTRCT-AMT ( * ) TO #CPS-TOTALS.#CNTRCT-AMT ( 1,* )
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Dvdnd_Amt().getValue(1,"*").setValue(ldaFcpl800.getIa_Cntl_Rec_C_Dvdnd_Amt().getValue("*"));                                    //Natural: MOVE IA-CNTL-REC.C-DVDND-AMT ( * ) TO #CPS-TOTALS.#DVDND-AMT ( 1,* )
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Cref_Amt().getValue(1,"*").setValue(ldaFcpl800.getIa_Cntl_Rec_C_Cref_Amt().getValue("*"));                                      //Natural: MOVE IA-CNTL-REC.C-CREF-AMT ( * ) TO #CPS-TOTALS.#CREF-AMT ( 1,* )
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Gross_Amt().getValue(1,"*").setValue(ldaFcpl800.getIa_Cntl_Rec_C_Gross_Amt().getValue("*"));                                    //Natural: MOVE IA-CNTL-REC.C-GROSS-AMT ( * ) TO #CPS-TOTALS.#GROSS-AMT ( 1,* )
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Ded_Amt().getValue(1,"*").setValue(ldaFcpl800.getIa_Cntl_Rec_C_Ded_Amt().getValue("*"));                                        //Natural: MOVE IA-CNTL-REC.C-DED-AMT ( * ) TO #CPS-TOTALS.#DED-AMT ( 1,* )
        //*  S100-SAVE-CONTROL-TOTALS
    }
    //*  --------------ROXAN 05/07/01
    private void sub_T100_Decide_Rollovers() throws Exception                                                                                                             //Natural: T100-DECIDE-ROLLOVERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  FREE LOOK
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG")  //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' OR = 'TG' OR = 'G ' OR = '41'
            || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("41")))
        {
            pnd_Leon_Calc_Report_Pnd_Cntrct_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( #INDEX ) TO #CNTRCT-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
            pnd_Leon_Calc_Report_Pnd_Dvdnd_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Leon_Calc_Report_Pnd_Cref_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #CREF-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Leon_Calc_Report_Pnd_Gross_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #GROSS-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( #INDEX ) TO #TAX-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( #INDEX ) TO #TAX-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        pnd_Leon_Calc_Report_Pnd_Tax_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( #INDEX ) TO #TAX-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        pnd_Leon_Calc_Report_Pnd_Net_Amt_Leon.getValue(pnd_Leon_Index,pnd_Leon_Indx_04_Or_13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #NET-AMT-LEON ( #LEON-INDEX,#LEON-INDX-04-OR-13 )
        //*   TRANSFERED OUT OF FOR LOOP --->>  ROXAN 05/07/01
        //*  ADD   WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT (*)
        //*     TO #DED-AMT-LEON     (#LEON-INDEX,#LEON-INDX-04-OR-13)
        //*  END T100-DECIDE-ROLLOVERS
    }
    private void sub_U100_Undo_Tax_Calc() throws Exception                                                                                                                //Natural: U100-UNDO-TAX-CALC
    {
        if (BLNatReinput.isReinput()) return;

        FOR09:                                                                                                                                                            //Natural: FOR #INDEX 1 #MAX-FUNDS
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pnd_Max_Funds)); pnd_Index.nadd(1))
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals(" ")))                                                          //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  CTS
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index).compute(new ComputeParameters(false, pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)),  //Natural: COMPUTE WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) = WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) + WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( #INDEX ) + WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( #INDEX ) + WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( #INDEX ) + GTN-PDA-Z.INV-ACCT-CAN-TAX-AMT ( #INDEX )
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index).add(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Index)).add(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_Index)).add(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_Index)).add(pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Can_Tax_Amt().getValue(pnd_Index)));
            //*  CTS
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Index).reset();                                                                          //Natural: RESET WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( #INDEX ) WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( #INDEX ) WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( #INDEX ) GTN-PDA-Z.INV-ACCT-CAN-TAX-AMT ( #INDEX )
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_Index).reset();
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_Index).reset();
            pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Can_Tax_Amt().getValue(pnd_Index).reset();
            pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code().setValue("0000");                                                                                                     //Natural: MOVE '0000' TO GTN-PDA-Z.GTN-RET-CODE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CTS
        //*  CTS
        pdaFcpa140z.getGtn_Pda_Z_Gtn_Fed_Canadian_Amt().getValue("*").reset();                                                                                            //Natural: RESET GTN-PDA-Z.GTN-FED-CANADIAN-AMT ( * ) WF-PYMNT-TAX-GRP.GTN-FED-CANADIAN-AMT ( * )
        pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Canadian_Amt().getValue("*").reset();
        //*  U100-UNDO-TAX-CALC
    }
    private void sub_W100_Write_Output() throws Exception                                                                                                                 //Natural: W100-WRITE-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        if (condition(pdaFcpa140e.getGtn_Pda_E_Pnd_Pnd_This_Pymnt().equals(1)))                                                                                           //Natural: IF GTN-PDA-E.##THIS-PYMNT = 01
        {
            pnd_Check_Amt.compute(new ComputeParameters(false, pnd_Check_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",                   //Natural: COMPUTE #CHECK-AMT = WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#FUND-CT ) + 0
                pnd_Fund_Ct).add(getZero()));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Check_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pnd_Fund_Ct));                                                     //Natural: COMPUTE #CHECK-AMT = #CHECK-AMT + WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#FUND-CT )
        }                                                                                                                                                                 //Natural: END-IF
        //*  LAST RECORD IN A COMBINED CONTRACT WILL CONTAIN THE TOTAL
        //*  CHECK-AMOUNT
        if (condition(pdaFcpa140e.getGtn_Pda_E_Pnd_Pnd_This_Pymnt().equals(pdaFcpa140e.getGtn_Pda_E_Pnd_Pnd_Nbr_Of_Pymnts())))                                            //Natural: IF GTN-PDA-E.##THIS-PYMNT = GTN-PDA-E.##NBR-OF-PYMNTS
        {
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().setValue(pnd_Check_Amt);                                                                                    //Natural: MOVE #CHECK-AMT TO WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT
            //*  ZERO CHECK
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(getZero())))                                                                           //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT = 0
            {
                pnd_Do_Rpt_Code.setValue(1);                                                                                                                              //Natural: ASSIGN #DO-RPT-CODE := 01
                                                                                                                                                                          //Natural: PERFORM E100-ERROR-PAYMENT
                sub_E100_Error_Payment();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().reset();                                                                                                    //Natural: RESET WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue("*")), new ExamineTranslate(TranslateOption.Upper));                       //Natural: EXAMINE WF-PYMNT-ADDR-GRP.PYMNT-NME ( * ) TRANSLATE INTO UPPER CASE
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Ret_Code().setValue(pnd_Sv_Return_Code);                                                                                      //Natural: MOVE #SV-RETURN-CODE TO WF-PYMNT-ADDR-GRP.GTN-RET-CODE
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue("*").setValue(pnd_Sv_Rpt_Codes_Pnd_Sv_Rpt_Code.getValue("*"));                                            //Natural: MOVE #SV-RPT-CODE ( * ) TO WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( * )
        //*   CM IA-CAN-TAX                          /* FEED CANADIAN TAX TO IA
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt().reset();                                                                                                       //Natural: RESET WF-PYMNT-ADDR-GRP.CANADIAN-TAX-AMT
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt().compute(new ComputeParameters(false, pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt()), pdaFcpa140z.getGtn_Pda_Z_Gtn_Fed_Canadian_Amt().getValue(1).add(pdaFcpa140z.getGtn_Pda_Z_Gtn_Fed_Canadian_Amt().getValue(2))); //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CANADIAN-TAX-AMT := GTN-PDA-Z.GTN-FED-CANADIAN-AMT ( 1 ) + GTN-PDA-Z.GTN-FED-CANADIAN-AMT ( 2 )
        //* * S461 FILE WILL BE WRITTEN AFTER S462 FILE TO RESET PYMNT-DED-AMT
        //* *  FOR NO DED CONDITION FOR NET CHANGE LETTER - SAURAV
        //* *IF INV-ACCT-COUNT < 40
        //* *  #X := INV-ACCT-COUNT + 1
        //* *END-IF
        //* *IF #X > 0
        //* * WRITE  WORK FILE 3  VARIABLE PYMNT-ADDR-INFO  /* FOR CPS S461 OUTPUT
        //* *    WF-PYMNT-ADDR-GRP.INV-INFO (1:INV-ACCT-COUNT)
        //* *    #FILLER(#X:40)                                /* PAD WITH BLANKS
        //* *    WF-PYMNT-ADDR-GRP.CANADIAN-TAX-AMT
        //* *ELSE
        //* * WRITE  WORK FILE 3  VARIABLE PYMNT-ADDR-INFO  /* FOR CPS S461 OUTPUT
        //* *    WF-PYMNT-ADDR-GRP.INV-INFO (1:INV-ACCT-COUNT)
        //* *    WF-PYMNT-ADDR-GRP.CANADIAN-TAX-AMT
        //* *END-IF
        //* * CM END
        //*  PYMNT-ADDR-INFO IS FROM FCPA800
        //*  FOR TAX S462 OUTPUT
        //*  RS0 FROM FCPA800
        //*  RS1 FROM FCPA800
        //*  RS1
        //*  RS1
        //*  RS1
        getWorkFiles().write(5, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde(),  //Natural: WRITE WORK FILE 5 VARIABLE PYMNT-ADDR-INFO WF-PYMNT-TAX-GRP.WF-PYMNT-TAX-REC WF-PYMNT-ADDR-GRP.IA-ORGN-CDE WF-PYMNT-ADDR-GRP.PLAN-NUMBER WF-PYMNT-ADDR-GRP.SUB-PLAN WF-PYMNT-ADDR-GRP.ORIG-CNTRCT-NBR WF-PYMNT-ADDR-GRP.ORIG-SUB-PLAN WF-PYMNT-ADDR-GRP.INV-INFO ( 1:INV-ACCT-COUNT )
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        //*  PYMNT-DED-AMT = 0 FOR NO DEDUCTION CONDITION IN S461 /* SAURAV STARTS
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code().getValue("*").equals(4) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code().getValue("*").equals(5))) //Natural: IF WF-PYMNT-ADDR-GRP.GTN-CNTRCT-RPT-CODE ( * ) EQ 4 OR EQ 5
        {
            pymnt_Ded_Grp_S461.reset();                                                                                                                                   //Natural: RESET PYMNT-DED-GRP-S461
            pymnt_Ded_Grp_S461_Pymnt_Ded_Cde_S461.getValue("*").setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue("*"));                                  //Natural: ASSIGN PYMNT-DED-CDE-S461 ( * ) := WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( * )
            pymnt_Ded_Grp_S461_Pymnt_Ded_Payee_Cde_S461.getValue("*").setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde().getValue("*"));                      //Natural: ASSIGN PYMNT-DED-PAYEE-CDE-S461 ( * ) := WF-PYMNT-ADDR-GRP.PYMNT-DED-PAYEE-CDE ( * )
            pymnt_Ded_Grp_S461_Pymnt_Ded_Amt_S461.getValue("*").setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*"));                                  //Natural: ASSIGN PYMNT-DED-AMT-S461 ( * ) := WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * )
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Grp().reset();                                                                                                      //Natural: RESET WF-PYMNT-ADDR-GRP.PYMNT-DED-GRP
            //*  SAURAV ENDS
        }                                                                                                                                                                 //Natural: END-IF
        //*  S461 FILE WRITING SEQUENCE HAS BEEN CHANGED AND IT IS WRITTEN AFTER
        //*  S462 FILE NOW TO MAKE PYMNT-DED-GRP VALUE 0 FOR FILE S461 FOR NO DED
        //*  OR NO TAX/DED CONDITION WITHOUT IMPACTING VALUES IN S462 FILE- SAURAV
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count().less(40)))                                                                                         //Natural: IF INV-ACCT-COUNT < 40
        {
            pnd_X.compute(new ComputeParameters(false, pnd_X), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count().add(1));                                                  //Natural: ASSIGN #X := INV-ACCT-COUNT + 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_X.greater(getZero())))                                                                                                                          //Natural: IF #X > 0
        {
            //*  FOR CPS S461 OUTPUT
            //*  PAD WITH BLANKS
            getWorkFiles().write(3, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()),  //Natural: WRITE WORK FILE 3 VARIABLE PYMNT-ADDR-INFO WF-PYMNT-ADDR-GRP.INV-INFO ( 1:INV-ACCT-COUNT ) #FILLER ( #X:40 ) WF-PYMNT-ADDR-GRP.CANADIAN-TAX-AMT
                pnd_Filler.getValue(pnd_X,":",40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt());
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  FOR CPS S461 OUTPUT
            getWorkFiles().write(3, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()),  //Natural: WRITE WORK FILE 3 VARIABLE PYMNT-ADDR-INFO WF-PYMNT-ADDR-GRP.INV-INFO ( 1:INV-ACCT-COUNT ) WF-PYMNT-ADDR-GRP.CANADIAN-TAX-AMT
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt());
        }                                                                                                                                                                 //Natural: END-IF
        //*  /* SAURAV STARTS
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code().getValue("*").equals(4) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code().getValue("*").equals(5))) //Natural: IF WF-PYMNT-ADDR-GRP.GTN-CNTRCT-RPT-CODE ( * ) EQ 4 OR EQ 5
        {
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue("*").setValue(pymnt_Ded_Grp_S461_Pymnt_Ded_Cde_S461.getValue("*"));                                  //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( * ) := PYMNT-DED-CDE-S461 ( * )
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde().getValue("*").setValue(pymnt_Ded_Grp_S461_Pymnt_Ded_Payee_Cde_S461.getValue("*"));                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-DED-PAYEE-CDE ( * ) := PYMNT-DED-PAYEE-CDE-S461 ( * )
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").setValue(pymnt_Ded_Grp_S461_Pymnt_Ded_Amt_S461.getValue("*"));                                  //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) := PYMNT-DED-AMT-S461 ( * )
        }                                                                                                                                                                 //Natural: END-IF
        //*  /* SAURAV ENDS
        //*  DEA START
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind().equals("C") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().equals("C")))      //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-PYMNT-TYPE-IND = 'C' AND WF-PYMNT-ADDR-GRP.CNTRCT-STTLMNT-TYPE-IND = 'C'
        {
            //*  CLEAR DATA AREA
            pdaFcpa800e.getPnd_Canadian_Tax().reset();                                                                                                                    //Natural: RESET #CANADIAN-TAX
            //*  CTS-0115 >>
            //*  MOVE FCP-CONS-CNTRL.CNTL-ORGN-CDE TO #CAN-TAX-CNTRCT-ORGN-CDE
            pdaFcpa800e.getPnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Orgn_Cde().setValue(cntlgdg_Cntl_Orgn_Cde);                                                                //Natural: MOVE CNTLGDG.CNTL-ORGN-CDE TO #CAN-TAX-CNTRCT-ORGN-CDE
            //*  CTS-0115 <<
            pdaFcpa800e.getPnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Invrse_Dte().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte());                                //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-INVRSE-DTE TO #CAN-TAX-CNTRCT-INVRSE-DTE
            pdaFcpa800e.getPnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmbn_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr());                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR TO #CAN-TAX-CNTRCT-CMBN-NBR
            pdaFcpa800e.getPnd_Canadian_Tax_Pnd_Can_Tax_Cntrct_Cmpny_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(1));                      //Natural: MOVE WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( 1 ) TO #CAN-TAX-CNTRCT-CMPNY-CDE
            pdaFcpa800e.getPnd_Canadian_Tax_Pnd_Can_Tax_Pymnt_Prcss_Seq_Num().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr());                            //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NBR TO #CAN-TAX-PYMNT-PRCSS-SEQ-NUM
            pdaFcpa800e.getPnd_Canadian_Tax_Pnd_Can_Tax_Pay_Type_Req_Ind().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind());                            //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND TO #CAN-TAX-PAY-TYPE-REQ-IND
            FOR10:                                                                                                                                                        //Natural: FOR #I = 1 TO 40
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(40)); pnd_I.nadd(1))
            {
                //*  CTS
                if (condition(pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Can_Tax_Amt().getValue(pnd_I).equals(getZero())))                                                         //Natural: IF GTN-PDA-Z.INV-ACCT-CAN-TAX-AMT ( #I ) EQ 0
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //*  CTS
                pdaFcpa800e.getPnd_Canadian_Tax_Pnd_Can_Tax_Inv_Acct_Can_Tax_Amt().getValue(pnd_I).setValue(pdaFcpa140z.getGtn_Pda_Z_Inv_Acct_Can_Tax_Amt().getValue(pnd_I)); //Natural: MOVE GTN-PDA-Z.INV-ACCT-CAN-TAX-AMT ( #I ) TO #CAN-TAX-INV-ACCT-CAN-TAX-AMT ( #I )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  FOR CANADIAN TAX
            getWorkFiles().write(6, true, pdaFcpa800e.getPnd_Canadian_Tax());                                                                                             //Natural: WRITE WORK FILE 6 VARIABLE #CANADIAN-TAX
            //*    FCP-CONS-CNTRL.CNTL-ORGN-CDE
            //*    WF-PYMNT-ADDR-GRP.CNTRCT-INVRSE-DTE
            //*    WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR
            //*                      GTN-PDA-Z.GTN-TAX-COMPANY-CDE (1)
            //*    WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NUM
            //*    INV-ACCT-CAN-TAX-AMT(*)
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                            /* DEA END
        //*  W100-WRITE-OUTPUT
    }
    private void sub_W200_Write_Control_Totals() throws Exception                                                                                                         //Natural: W200-WRITE-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Program_Lit().setValue(" CPS CONTROL TOTALS ");                                                                                 //Natural: MOVE ' CPS CONTROL TOTALS ' TO #PROGRAM-LIT
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Title_Lit().getValue(1).setValue(" IAA INTERFACE FILE ");                                                                       //Natural: MOVE ' IAA INTERFACE FILE ' TO #TITLE-LIT ( 1 )
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Title_Lit().getValue(2).setValue(" CPS RECORD COUNTS  ");                                                                       //Natural: MOVE ' CPS RECORD COUNTS  ' TO #TITLE-LIT ( 2 )
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Title_Lit().getValue(3).setValue(" IAA - CPS VARIANCE ");                                                                       //Natural: MOVE ' IAA - CPS VARIANCE ' TO #TITLE-LIT ( 3 )
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Pymnt_Check_Dte().setValue(pdaFcpa140e.getGtn_Pda_E_Pymnt_Check_Dte());                                                         //Natural: MOVE GTN-PDA-E.PYMNT-CHECK-DTE TO #CPS-TOTALS.#PYMNT-CHECK-DTE
        //* ***CALLNAT 'FCPN800A' USING #CPS-TOTALS    /* ORIG
        //*  LEON 3/15
        DbsUtil.callnat(Fcpn800a.class , getCurrentProcessState(), pdaFcpa800a.getPnd_Cps_Totals(), pnd_Leon_Calc_Report.getValue("*"));                                  //Natural: CALLNAT 'FCPN800A' USING #CPS-TOTALS #LEON-CALC-REPORT ( * )
        if (condition(Global.isEscape())) return;
        pdaFcpa800a.getPnd_Cps_Totals_Pnd_Program_Lit().setValue(" CPS CONTROL TOTALS ");                                                                                 //Natural: MOVE ' CPS CONTROL TOTALS ' TO #PROGRAM-LIT
        DbsUtil.callnat(Fcpn800b.class , getCurrentProcessState(), pdaFcpa800a.getPnd_Cps_Totals());                                                                      //Natural: CALLNAT 'FCPN800B' USING #CPS-TOTALS
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa800a.getPnd_Cps_Totals_Pnd_Diff_In_Totals().getBoolean()))                                                                                   //Natural: IF #DIFF-IN-TOTALS
        {
            getReports().write(0, NEWLINE,NEWLINE,"***************************************************************",NEWLINE,"***************************************************************", //Natural: WRITE // '***************************************************************' / '***************************************************************' / '*** Variance(s) found between IAA control record totals and ***' / '***    control totals as processed by CPS                   ***' / '***                                                         ***' / '*** Please check control reports, make corrections,         ***' / '***    then re-submit                                       ***' / '***************************************************************' / '***************************************************************'
                NEWLINE,"*** Variance(s) found between IAA control record totals and ***",NEWLINE,"***    control totals as processed by CPS                   ***",
                NEWLINE,"***                                                         ***",NEWLINE,"*** Please check control reports, make corrections,         ***",
                NEWLINE,"***    then re-submit                                       ***",NEWLINE,"***************************************************************",
                NEWLINE,"***************************************************************");
            if (Global.isEscape()) return;
            DbsUtil.terminate(95);  if (true) return;                                                                                                                     //Natural: TERMINATE 0095
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  THE FOLLOWING LOGIC CAN BE USED TO CREATE NEW / CORRECT CONTROL RECORD
        //*  CAN BE USED FOR DEBUGGNING PURPOSES  ...  AR
        //*  MOVE EDITED '04/01/2003' TO
        //*  IA-CNTL-REC.CNTL-CHECK-DTE (EM=MM/DD/YYYY)
        //*  IA-CNTL-REC.C-REC-COUNT (*)  := #CPS-TOTALS.#REC-COUNT  (2,*)
        //*  IA-CNTL-REC.C-CNTRCT-AMT(*)  := #CPS-TOTALS.#CNTRCT-AMT (2,*)
        //*  IA-CNTL-REC.C-DVDND-AMT (*)  := #CPS-TOTALS.#DVDND-AMT  (2,*)
        //*  IA-CNTL-REC.C-CREF-AMT  (*)  := #CPS-TOTALS.#CREF-AMT   (2,*)
        //*  IA-CNTL-REC.C-GROSS-AMT (*)  := #CPS-TOTALS.#GROSS-AMT  (2,*)
        //*  IA-CNTL-REC.C-DED-AMT   (*)  := #CPS-TOTALS.#DED-AMT    (2,*)
        //*  WRITE WORK FILE 7 IA-CNTL-REC
        //*  W200-WRITE-CONTROL-TOTALS
    }
    private void sub_Z100_Display_Tax_Error() throws Exception                                                                                                            //Natural: Z100-DISPLAY-TAX-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        getReports().write(0, "ERROR:",pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Code(),pdaFcpa140z.getGtn_Pda_Z_Cntrct_Option_Cde(),pdaFcpa140z.getGtn_Pda_Z_Cntrct_Ac_Lt_10yrs(), //Natural: WRITE 'ERROR:' GTN-PDA-Z.GTN-RET-CODE GTN-PDA-Z.CNTRCT-OPTION-CDE GTN-PDA-Z.CNTRCT-AC-LT-10YRS GTN-PDA-Z.PYMNT-SPOUSE-PAY-STATS GTN-PDA-Z.CNTRCT-PYMNT-TYPE-IND GTN-PDA-Z.CNTRCT-STTLMNT-TYPE-IND
            pdaFcpa140z.getGtn_Pda_Z_Pymnt_Spouse_Pay_Stats(),pdaFcpa140z.getGtn_Pda_Z_Cntrct_Pymnt_Type_Ind(),pdaFcpa140z.getGtn_Pda_Z_Cntrct_Sttlmnt_Type_Ind());
        if (Global.isEscape()) return;
        getReports().write(0, "ERROR MESSAGE:",pdaFcpa140z.getGtn_Pda_Z_Gtn_Ret_Msg());                                                                                   //Natural: WRITE 'ERROR MESSAGE:' GTN-PDA-Z.GTN-RET-MSG
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Read_Count);                                                                                                                        //Natural: WRITE '=' #READ-COUNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Ppcn_Nbr());                                                                                            //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-PPCN-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Annt_Soc_Sec_Nbr());                                                                                           //Natural: WRITE '=' GTN-PDA-Z.ANNT-SOC-SEC-NBR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Annt_Ctznshp_Cde());                                                                                           //Natural: WRITE '=' GTN-PDA-Z.ANNT-CTZNSHP-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Annt_Rsdncy_Cde());                                                                                            //Natural: WRITE '=' GTN-PDA-Z.ANNT-RSDNCY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Annt_Locality_Cde());                                                                                          //Natural: WRITE '=' GTN-PDA-Z.ANNT-LOCALITY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Payee_Cde());                                                                                           //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-PAYEE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Option_Cde());                                                                                          //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-OPTION-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Ac_Lt_10yrs());                                                                                         //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-AC-LT-10YRS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Mode_Cde());                                                                                            //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-MODE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Pymnt_Spouse_Pay_Stats());                                                                                     //Natural: WRITE '=' GTN-PDA-Z.PYMNT-SPOUSE-PAY-STATS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Pymnt_Type_Ind());                                                                                      //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-PYMNT-TYPE-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Sttlmnt_Type_Ind());                                                                                    //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-STTLMNT-TYPE-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Lob_Cde());                                                                                             //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-LOB-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Sub_Lob_Cde());                                                                                         //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-SUB-LOB-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Ia_Lob_Cde());                                                                                          //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-IA-LOB-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Roll_Dest_Cde());                                                                                       //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-ROLL-DEST-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Annty_Ins_Type());                                                                                      //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-ANNTY-INS-TYPE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Cntrct_Annty_Type_Cde());                                                                                      //Natural: WRITE '=' GTN-PDA-Z.CNTRCT-ANNTY-TYPE-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Pda_Count());                                                                                          //Natural: WRITE '=' GTN-PDA-Z.##PDA-COUNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_This_Pymnt());                                                                                         //Natural: WRITE '=' GTN-PDA-Z.##THIS-PYMNT
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Pnd_Pnd_Nbr_Of_Pymnts());                                                                                      //Natural: WRITE '=' GTN-PDA-Z.##NBR-OF-PYMNTS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Annt_Ctznshp_Cde());                                                                                       //Natural: WRITE '=' GTN-PDA-Z.GTN-ANNT-CTZNSHP-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Annt_Rsdncy_Cde());                                                                                        //Natural: WRITE '=' GTN-PDA-Z.GTN-ANNT-RSDNCY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Annt_Locality_Cde());                                                                                      //Natural: WRITE '=' GTN-PDA-Z.GTN-ANNT-LOCALITY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Inv_Tin_Ind());                                                                                            //Natural: WRITE '=' GTN-PDA-Z.GTN-INV-TIN-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Pymnt_Type());                                                                                             //Natural: WRITE '=' GTN-PDA-Z.GTN-PYMNT-TYPE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Rollover_Elig_Ind());                                                                                      //Natural: WRITE '=' GTN-PDA-Z.GTN-ROLLOVER-ELIG-IND
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Elec_Cde());                                                                                               //Natural: WRITE '=' GTN-PDA-Z.GTN-ELEC-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Tax_Company_Cde().getValue(1));                                                                            //Natural: WRITE '=' GTN-PDA-Z.GTN-TAX-COMPANY-CDE ( 1 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Taxable_Amt().getValue(1));                                                                                //Natural: WRITE '=' GTN-PDA-Z.GTN-TAXABLE-AMT ( 1 )
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Cntrct_Money_Source());                                                                                    //Natural: WRITE '=' GTN-PDA-Z.GTN-CNTRCT-MONEY-SOURCE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Tax_Lob_Cde());                                                                                            //Natural: WRITE '=' GTN-PDA-Z.GTN-TAX-LOB-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Disability_Cde());                                                                                         //Natural: WRITE '=' GTN-PDA-Z.GTN-DISABILITY-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Hardship_Cde());                                                                                           //Natural: WRITE '=' GTN-PDA-Z.GTN-HARDSHIP-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Termination_Of_Empl_Cde());                                                                                //Natural: WRITE '=' GTN-PDA-Z.GTN-TERMINATION-OF-EMPL-CDE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Pymnt_Subject_To_Treaty_Rate());                                                                           //Natural: WRITE '=' GTN-PDA-Z.GTN-PYMNT-SUBJECT-TO-TREATY-RATE
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Settl_Options());                                                                                          //Natural: WRITE '=' GTN-PDA-Z.GTN-SETTL-OPTIONS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Term_Years());                                                                                             //Natural: WRITE '=' GTN-PDA-Z.GTN-TERM-YEARS
        if (Global.isEscape()) return;
        getReports().write(0, "=",pdaFcpa140z.getGtn_Pda_Z_Gtn_Pymnt_Type_Ind());                                                                                         //Natural: WRITE '=' GTN-PDA-Z.GTN-PYMNT-TYPE-IND
        if (Global.isEscape()) return;
        //*  Z100-DISPLAY-TAX-ERROR
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().setDisplayColumns(0, pnd_Read_Count,pdaFcpa140e.getGtn_Pda_E_Cntrct_Ppcn_Nbr(),pdaFcpa140e.getGtn_Pda_E_Cntrct_Cmbn_Nbr(),Global.getTIME());
    }
}
