/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:47 PM
**        * FROM NATURAL PROGRAM : Fcpp850
************************************************************
**        * FILE NAME            : Fcpp850.java
**        * CLASS NAME           : Fcpp850
**        * INSTANCE NAME        : Fcpp850
************************************************************
************************************************************************
* PROGRAM   : FCPP850
* SYSTEM    : CPS
* GENERATED : ON 03/2005 BY E. BOTTERI
* FUNCTION  : SPLIT DEDUCTIONS FILE FOR A SEPARATE HARVARD REPORT.  THE
*             NEW FILE WILL BE HANDLED BY FCPP851 (CLONE OF FCPP809).
* HISTORY   :
*
* 11/20/2018 J.OSTEEN - ADD CONTRACT RANGE W053 -- JWO1
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp850 extends BLNatBase
{
    // Data Areas
    private LdaFcpl804 ldaFcpl804;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cntr;
    private DbsField pnd_Cnt_Pers_Staff;
    private DbsField pnd_Cnt_Pers_Hourly;
    private DbsField pnd_Cnt_President;
    private DbsField pnd_Cnt_Mstr_Fclty_A;
    private DbsField pnd_Cnt_Mstr_Fclty_B;
    private DbsField pnd_Cnt_Mstr_Staff;
    private DbsField pnd_Cnt_Mstr_Hourly;
    private DbsField pnd_Cnt_Spec_Staff;
    private DbsField pnd_Cnt_Spec_Hourly;
    private DbsField pnd_Cnt_Brid_Staff;
    private DbsField pnd_Cnt_Brid_Hourly;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl804 = new LdaFcpl804();
        registerRecord(ldaFcpl804);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cntr = localVariables.newFieldInRecord("pnd_Cntr", "#CNTR", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Pers_Staff = localVariables.newFieldInRecord("pnd_Cnt_Pers_Staff", "#CNT-PERS-STAFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Pers_Hourly = localVariables.newFieldInRecord("pnd_Cnt_Pers_Hourly", "#CNT-PERS-HOURLY", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_President = localVariables.newFieldInRecord("pnd_Cnt_President", "#CNT-PRESIDENT", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Mstr_Fclty_A = localVariables.newFieldInRecord("pnd_Cnt_Mstr_Fclty_A", "#CNT-MSTR-FCLTY-A", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Mstr_Fclty_B = localVariables.newFieldInRecord("pnd_Cnt_Mstr_Fclty_B", "#CNT-MSTR-FCLTY-B", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Mstr_Staff = localVariables.newFieldInRecord("pnd_Cnt_Mstr_Staff", "#CNT-MSTR-STAFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Mstr_Hourly = localVariables.newFieldInRecord("pnd_Cnt_Mstr_Hourly", "#CNT-MSTR-HOURLY", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Spec_Staff = localVariables.newFieldInRecord("pnd_Cnt_Spec_Staff", "#CNT-SPEC-STAFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Spec_Hourly = localVariables.newFieldInRecord("pnd_Cnt_Spec_Hourly", "#CNT-SPEC-HOURLY", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Brid_Staff = localVariables.newFieldInRecord("pnd_Cnt_Brid_Staff", "#CNT-BRID-STAFF", FieldType.PACKED_DECIMAL, 7);
        pnd_Cnt_Brid_Hourly = localVariables.newFieldInRecord("pnd_Cnt_Brid_Hourly", "#CNT-BRID-HOURLY", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl804.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp850() throws Exception
    {
        super("Fcpp850");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP850", onError);
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  READ EXTRACTED DEDUCTIONS FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 DEDUCTION-REC
        while (condition(getWorkFiles().read(1, ldaFcpl804.getDeduction_Rec())))
        {
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF CNTRCT-PPCN-NBR
            short decideConditionsMet64 = 0;                                                                                                                              //Natural: VALUE 'W0764500' : 'W0764749'
            if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0764500' : 'W0764749"))))
            {
                decideConditionsMet64++;
                //*  JWO1 2018-11
                pnd_Cnt_Pers_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-PERS-STAFF
                //*    VALUE 'W0541000' : 'W0549999'
            }                                                                                                                                                             //Natural: VALUE 'W0530000' : 'W0549999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0530000' : 'W0549999"))))
            {
                decideConditionsMet64++;
                pnd_Cnt_Pers_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-PERS-STAFF
            }                                                                                                                                                             //Natural: VALUE 'W0764750' : 'W0764999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0764750' : 'W0764999"))))
            {
                decideConditionsMet64++;
                pnd_Cnt_Pers_Hourly.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-PERS-HOURLY
            }                                                                                                                                                             //Natural: VALUE 'W0765000' : 'W0765499'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0765000' : 'W0765499"))))
            {
                decideConditionsMet64++;
                pnd_Cnt_President.nadd(1);                                                                                                                                //Natural: ADD 1 TO #CNT-PRESIDENT
            }                                                                                                                                                             //Natural: VALUE 'W0765500' : 'W0767599'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0765500' : 'W0767599"))))
            {
                decideConditionsMet64++;
                pnd_Cnt_Mstr_Fclty_A.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CNT-MSTR-FCLTY-A
            }                                                                                                                                                             //Natural: VALUE 'W0767600' : 'W0769999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0767600' : 'W0769999"))))
            {
                decideConditionsMet64++;
                pnd_Cnt_Mstr_Fclty_B.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CNT-MSTR-FCLTY-B
            }                                                                                                                                                             //Natural: VALUE 'W0770000' : 'W0789999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0770000' : 'W0789999"))))
            {
                decideConditionsMet64++;
                pnd_Cnt_Mstr_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-MSTR-STAFF
            }                                                                                                                                                             //Natural: VALUE 'W0790000' : 'W0799999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0790000' : 'W0799999"))))
            {
                decideConditionsMet64++;
                //*  JWO 2009-09
                pnd_Cnt_Mstr_Hourly.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-MSTR-HOURLY
            }                                                                                                                                                             //Natural: VALUE 'W0550000' : 'W0554999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0550000' : 'W0554999"))))
            {
                decideConditionsMet64++;
                //*  JWO 2009-09
                pnd_Cnt_Spec_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-SPEC-STAFF
            }                                                                                                                                                             //Natural: VALUE 'W0555000' : 'W0557999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0555000' : 'W0557999"))))
            {
                decideConditionsMet64++;
                //*  JWO 2009-09
                pnd_Cnt_Brid_Staff.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNT-BRID-STAFF
            }                                                                                                                                                             //Natural: VALUE 'W0558000' : 'W0558999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0558000' : 'W0558999"))))
            {
                decideConditionsMet64++;
                //*  JWO 2009-09
                pnd_Cnt_Spec_Hourly.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-SPEC-HOURLY
            }                                                                                                                                                             //Natural: VALUE 'W0559000' : 'W0559999'
            else if (condition((ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().equals("W0559000' : 'W0559999"))))
            {
                decideConditionsMet64++;
                pnd_Cnt_Brid_Hourly.nadd(1);                                                                                                                              //Natural: ADD 1 TO #CNT-BRID-HOURLY
            }                                                                                                                                                             //Natural: ANY VALUE
            if (condition(decideConditionsMet64 > 0))
            {
                getWorkFiles().write(2, false, ldaFcpl804.getDeduction_Rec());                                                                                            //Natural: WRITE WORK FILE 2 DEDUCTION-REC
                pnd_Cntr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #CNTR
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  JWO 2009-09
        //*  JWO 2009-09
        //*  JWO 2009-09
        //*  JWO 2009-09
        getReports().write(0, "PERSONAL STAFF...:",pnd_Cnt_Pers_Staff,NEWLINE,"PERSONAL HOURLY..:",pnd_Cnt_Pers_Hourly,NEWLINE,"PRESIDENT/FELLOWS:",pnd_Cnt_President,    //Natural: WRITE 'PERSONAL STAFF...:' #CNT-PERS-STAFF / 'PERSONAL HOURLY..:' #CNT-PERS-HOURLY / 'PRESIDENT/FELLOWS:' #CNT-PRESIDENT / 'MASTER FCLTY A...:' #CNT-MSTR-FCLTY-A / 'MASTER FCLTY B...:' #CNT-MSTR-FCLTY-B / 'MASTER STAFF.....:' #CNT-MSTR-STAFF / 'MASTER HOURLY....:' #CNT-MSTR-HOURLY / 'SPECIAL STAFF....:' #CNT-SPEC-STAFF / 'BRIDGE STAFF.....:' #CNT-SPEC-HOURLY / 'SPECIAL HOURLY...:' #CNT-BRID-STAFF / 'BRIDGE HOURLY....:' #CNT-BRID-HOURLY // 'Total number of Harvard records written:' #CNTR
            NEWLINE,"MASTER FCLTY A...:",pnd_Cnt_Mstr_Fclty_A,NEWLINE,"MASTER FCLTY B...:",pnd_Cnt_Mstr_Fclty_B,NEWLINE,"MASTER STAFF.....:",pnd_Cnt_Mstr_Staff,
            NEWLINE,"MASTER HOURLY....:",pnd_Cnt_Mstr_Hourly,NEWLINE,"SPECIAL STAFF....:",pnd_Cnt_Spec_Staff,NEWLINE,"BRIDGE STAFF.....:",pnd_Cnt_Spec_Hourly,
            NEWLINE,"SPECIAL HOURLY...:",pnd_Cnt_Brid_Staff,NEWLINE,"BRIDGE HOURLY....:",pnd_Cnt_Brid_Hourly,NEWLINE,NEWLINE,"Total number of Harvard records written:",
            pnd_Cntr);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"***********************************************************",NEWLINE,"***********************************************************", //Natural: WRITE // '***********************************************************' / '***********************************************************' / '***'*PROGRAM '  ERROR:' *ERROR-NR 'LINE:' *ERROR-LINE / '***  LAST RECORD READ:' / '***         COMBINE #:' CNTRCT-CMBN-NBR / '***              PPCN:' CNTRCT-PPCN-NBR / '***********************************************************' / '***********************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  ERROR:",Global.getERROR_NR(),"LINE:",Global.getERROR_LINE(),NEWLINE,"***  LAST RECORD READ:",NEWLINE,"***         COMBINE #:",
            ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(),NEWLINE,"***********************************************************",
            NEWLINE,"***********************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
}
