/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:16 PM
**        * FROM NATURAL PROGRAM : Fcpp133
************************************************************
**        * FILE NAME            : Fcpp133.java
**        * CLASS NAME           : Fcpp133
**        * INSTANCE NAME        : Fcpp133
************************************************************
************************************************************************
* PROGRAM  : FCPP133
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CREATE FLAT FILE FOR THE DETAIL LEDGER REPORT
*            FOR MONTHLY AP PAYMENTS
* CREATED  : 12/11/2001
* FUNCTION : THIS PROGRAM WILL READ PIA.ANN.P2210CPM.LEDGER.S469
*            AND CREATES A REPORTING EXTRACT FOR CPS MONTHLY LEDGER
*            REPORT. ADDRESS AND ISA INFORMATION WILL BE ADDED TO THE
*            INPUT RECORDS AND WRITTEN TO AN OUTPUT FILE.
* HISTORY  : 06/11/02  ROXAN
*            RE-STOWED.  NEW ISA FOR SPIA AND PA-SELECT NEW FUNDS.
*          : 02/12/03 : ROXAN CARREON.  STOW FOR EGTRRA
*
* 05/12/08 : ROTH-MAJOR1 - SCAN FOR AER - MODIFIED PROGRAM TO DO A
*            CALLNAT TO FCPNPMNR INSTEAD OF FCPNPMNT.  THE NEW CALLNAT
*            HAS ROTH FIELDS THAT THE MONTHLY PROCESS NEEDS.
* 4/2017   : JJG - PIN EXPANSION RESTOW
*****************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp133 extends BLNatBase
{
    // Data Areas
    private LdaFcpl801c ldaFcpl801c;
    private LdaFcpl199b ldaFcpl199b;
    private PdaFcpa121 pdaFcpa121;
    private LdaFcpl121 ldaFcpl121;
    private PdaFcpaaddr pdaFcpaaddr;
    private PdaFcpapmnr pdaFcpapmnr;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Records_Read;
    private DbsField pnd_Ws_Pnd_Records_Written;
    private DbsField pnd_Ws_Pnd_Isa_Sub;
    private DbsField pnd_Ws_Pnd_Pymnt_Prcess_Seq_No;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Pnd_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Pnd_Temp_Date_Alpha;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Temp_Date;
    private DbsField pnd_St_Cntrct_Cmbn_Nb;

    private DbsGroup pnd_St_Cntrct_Cmbn_Nb__R_Field_3;
    private DbsField pnd_St_Cntrct_Cmbn_Nb_Pnd_St_Cntrct_Ppcn_Nbr;
    private DbsField pnd_St_Cntrct_Cmbn_Nb_Pnd_St_Cntrct_Payee_Cd;
    private DbsField pnd_Work_One;
    private DbsField pnd_Blank;
    private DbsField pnd_Long_Name;
    private DbsField pnd_Last_Name;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl801c = new LdaFcpl801c();
        registerRecord(ldaFcpl801c);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);
        localVariables = new DbsRecord();
        pdaFcpa121 = new PdaFcpa121(localVariables);
        ldaFcpl121 = new LdaFcpl121();
        registerRecord(ldaFcpl121);
        pdaFcpaaddr = new PdaFcpaaddr(localVariables);
        pdaFcpapmnr = new PdaFcpapmnr(localVariables);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Records_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Records_Read", "#RECORDS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Records_Written = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Records_Written", "#RECORDS-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Isa_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isa_Sub", "#ISA-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Pymnt_Prcess_Seq_No = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Prcess_Seq_No", "#PYMNT-PRCESS-SEQ-NO", FieldType.NUMERIC, 9);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Pymnt_Prcess_Seq_No);
        pnd_Ws_Pnd_Pymnt_Prcss_Seq_Num = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Prcss_Seq_Num", "#PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Ws_Pnd_Pymnt_Instmt_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Instmt_Nbr", "#PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Temp_Date_Alpha = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Temp_Date_Alpha", "#TEMP-DATE-ALPHA", FieldType.STRING, 8);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Temp_Date_Alpha);
        pnd_Ws_Pnd_Temp_Date = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 8);
        pnd_St_Cntrct_Cmbn_Nb = localVariables.newFieldInRecord("pnd_St_Cntrct_Cmbn_Nb", "#ST-CNTRCT-CMBN-NB", FieldType.STRING, 14);

        pnd_St_Cntrct_Cmbn_Nb__R_Field_3 = localVariables.newGroupInRecord("pnd_St_Cntrct_Cmbn_Nb__R_Field_3", "REDEFINE", pnd_St_Cntrct_Cmbn_Nb);
        pnd_St_Cntrct_Cmbn_Nb_Pnd_St_Cntrct_Ppcn_Nbr = pnd_St_Cntrct_Cmbn_Nb__R_Field_3.newFieldInGroup("pnd_St_Cntrct_Cmbn_Nb_Pnd_St_Cntrct_Ppcn_Nbr", 
            "#ST-CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_St_Cntrct_Cmbn_Nb_Pnd_St_Cntrct_Payee_Cd = pnd_St_Cntrct_Cmbn_Nb__R_Field_3.newFieldInGroup("pnd_St_Cntrct_Cmbn_Nb_Pnd_St_Cntrct_Payee_Cd", 
            "#ST-CNTRCT-PAYEE-CD", FieldType.STRING, 4);
        pnd_Work_One = localVariables.newFieldInRecord("pnd_Work_One", "#WORK-ONE", FieldType.STRING, 81);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.PACKED_DECIMAL, 3);
        pnd_Long_Name = localVariables.newFieldInRecord("pnd_Long_Name", "#LONG-NAME", FieldType.STRING, 50);
        pnd_Last_Name = localVariables.newFieldInRecord("pnd_Last_Name", "#LAST-NAME", FieldType.STRING, 50);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl801c.initializeValues();
        ldaFcpl199b.initializeValues();
        ldaFcpl121.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp133() throws Exception
    {
        super("Fcpp133");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        getWorkFiles().read(1, pnd_Work_One);                                                                                                                             //Natural: READ WORK FILE 01 ONCE #WORK-ONE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD #LEDGER-EXT
        while (condition(getWorkFiles().read(1, ldaFcpl801c.getPnd_Ledger_Ext())))
        {
            pnd_Ws_Pnd_Records_Read.nadd(1);                                                                                                                              //Natural: ADD 1 TO #RECORDS-READ
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Orgn_Cde());                                                 //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-ORGN-CDE := #LEDGER-EXT.CNTRCT-ORGN-CDE
            ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Acctg_Dte().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Acctg_Dte());                                                 //Natural: ASSIGN #LEDGER-EXTRACT.PYMNT-ACCTG-DTE := #LEDGER-EXT.PYMNT-ACCTG-DTE
            ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash());                                                       //Natural: ASSIGN #LEDGER-EXTRACT.INV-ACCT-CDE := #LEDGER-EXT.INV-ISA-HASH
            ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr());                                           //Natural: ASSIGN #LEDGER-EXTRACT.INV-ACCT-LEDGR-NBR := #LEDGER-EXT.INV-ACCT-LEDGR-NBR
            ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Check_Dte());                                                 //Natural: ASSIGN #LEDGER-EXTRACT.PYMNT-CHECK-DTE := #LEDGER-EXT.PYMNT-CHECK-DTE
            ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Settlmnt_Dte());                                           //Natural: ASSIGN #LEDGER-EXTRACT.PYMNT-SETTLMNT-DTE := #LEDGER-EXT.PYMNT-SETTLMNT-DTE
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Ppcn_Nbr());                                                 //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-PPCN-NBR := #LEDGER-EXT.CNTRCT-PPCN-NBR
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Payee_Cde().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Payee_Cde());                                               //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-PAYEE-CDE := #LEDGER-EXT.CNTRCT-PAYEE-CDE
            ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Cycle_Dte().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Cycle_Dte());                                                 //Natural: ASSIGN #LEDGER-EXTRACT.PYMNT-CYCLE-DTE := #LEDGER-EXT.PYMNT-CYCLE-DTE
            ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Prcss_Seq_Nbr());                                         //Natural: ASSIGN #LEDGER-EXTRACT.PYMNT-PRCSS-SEQ-NBR := #LEDGER-EXT.PYMNT-PRCSS-SEQ-NBR
            ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Intrfce_Dte());                                             //Natural: ASSIGN #LEDGER-EXTRACT.PYMNT-INTRFCE-DTE := #LEDGER-EXT.PYMNT-INTRFCE-DTE
            ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Amt().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                                           //Natural: ASSIGN #LEDGER-EXTRACT.INV-ACCT-LEDGR-AMT := #LEDGER-EXT.INV-ACCT-LEDGR-AMT
            ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Ind().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind());                                           //Natural: ASSIGN #LEDGER-EXTRACT.INV-ACCT-LEDGR-IND := #LEDGER-EXT.INV-ACCT-LEDGR-IND
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Annty_Ins_Type().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Annty_Ins_Type());                                     //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-ANNTY-INS-TYPE := #LEDGER-EXT.CNTRCT-ANNTY-INS-TYPE
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_Ind().reset();                                                                                            //Natural: RESET #LEDGER-EXTRACT.CNTRCT-CANCEL-RDRW-IND #LEDGER-EXTRACT.CNTRCT-CHECK-CRRNCY-CDE #LEDGER-EXTRACT.CNTRCT-CREF-NBR #LEDGER-EXTRACT.PYMNT-CORP-WPID #LEDGER-EXTRACT.CNTRCT-UNQ-ID-NBR
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Check_Crrncy_Cde().reset();
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cref_Nbr().reset();
            ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Corp_Wpid().reset();
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Unq_Id_Nbr().reset();
                                                                                                                                                                          //Natural: PERFORM GET-PYMNT-RECORD
            sub_Get_Pymnt_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM GET-NAME
            sub_Get_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM GET-ISA-CODE
            sub_Get_Isa_Code();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, false, ldaFcpl121.getPnd_Ledger_Extract());                                                                                           //Natural: WRITE WORK FILE 02 #LEDGER-EXTRACT
            pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                           //Natural: ADD 1 TO #RECORDS-WRITTEN
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().skip(1, 7);                                                                                                                                          //Natural: SKIP ( 1 ) 7 LINES
        getReports().write(1, new FieldAttributes ("AD=I"),new TabSetting(7),"Input Records          :", new FieldAttributes ("AD=I"),pnd_Ws_Pnd_Records_Read,            //Natural: WRITE ( 1 ) ( AD = I ) 7T 'Input Records          :' ( AD = I ) #RECORDS-READ /// 7T 'Output Records         :' ( AD = I ) #RECORDS-WRITTEN ///
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"Output Records         :", new FieldAttributes ("AD=I"),pnd_Ws_Pnd_Records_Written, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new  //Natural: WRITE ( 1 ) 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' /
            TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE);
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PYMNT-RECORD
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ISA-CODE
        //* *------------
    }
    private void sub_Get_Pymnt_Record() throws Exception                                                                                                                  //Natural: GET-PYMNT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pdaFcpapmnr.getPymnt().reset();                                                                                                                                   //Natural: RESET PYMNT
        pdaFcpapmnr.getPymnt_Cntrct_Ppcn_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Ppcn_Nbr());                                                                 //Natural: ASSIGN PYMNT.CNTRCT-PPCN-NBR := #LEDGER-EXT.CNTRCT-PPCN-NBR
        pdaFcpapmnr.getPymnt_Cntrct_Orgn_Cde().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Orgn_Cde());                                                                 //Natural: ASSIGN PYMNT.CNTRCT-ORGN-CDE := #LEDGER-EXT.CNTRCT-ORGN-CDE
        pdaFcpapmnr.getPymnt_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Prcss_Seq_Nbr());                                                         //Natural: ASSIGN PYMNT.PYMNT-PRCSS-SEQ-NBR := #LEDGER-EXT.PYMNT-PRCSS-SEQ-NBR
        pnd_Ws_Pnd_Temp_Date_Alpha.setValueEdited(ldaFcpl801c.getPnd_Ledger_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED #LEDGER-EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-ALPHA
        pdaFcpapmnr.getPymnt_Cntrct_Invrse_Dte().compute(new ComputeParameters(false, pdaFcpapmnr.getPymnt_Cntrct_Invrse_Dte()), DbsField.subtract(100000000,             //Natural: SUBTRACT #TEMP-DATE FROM 100000000 GIVING PYMNT.CNTRCT-INVRSE-DTE
            pnd_Ws_Pnd_Temp_Date));
        //*  AER
        DbsUtil.callnat(Fcpnpmnr.class , getCurrentProcessState(), pdaFcpapmnr.getPymnt_Work_Fields(), pdaFcpapmnr.getPymnt());                                           //Natural: CALLNAT 'FCPNPMNR' PYMNT-WORK-FIELDS PYMNT
        if (condition(Global.isEscape())) return;
        //*  (1180) GET-PYMNT-RECORD
    }
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------
        pdaFcpaaddr.getAddr().reset();                                                                                                                                    //Natural: RESET ADDR
        pdaFcpaaddr.getAddr_Cntrct_Ppcn_Nbr().setValue(ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Ppcn_Nbr());                                                                  //Natural: ASSIGN ADDR.CNTRCT-PPCN-NBR := #LEDGER-EXT.CNTRCT-PPCN-NBR
        pdaFcpaaddr.getAddr_Cntrct_Payee_Cde().setValue(pdaFcpapmnr.getPymnt_Cntrct_Payee_Cde());                                                                         //Natural: ASSIGN ADDR.CNTRCT-PAYEE-CDE := PYMNT.CNTRCT-PAYEE-CDE
        pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().setValue(pdaFcpapmnr.getPymnt_Cntrct_Orgn_Cde());                                                                           //Natural: ASSIGN ADDR.CNTRCT-ORGN-CDE := PYMNT.CNTRCT-ORGN-CDE
        pdaFcpaaddr.getAddr_Cntrct_Invrse_Dte().setValue(pdaFcpapmnr.getPymnt_Cntrct_Invrse_Dte());                                                                       //Natural: ASSIGN ADDR.CNTRCT-INVRSE-DTE := PYMNT.CNTRCT-INVRSE-DTE
        pdaFcpaaddr.getAddr_Pymnt_Prcss_Seq_Nbr().setValue(pdaFcpapmnr.getPymnt_Pymnt_Prcss_Seq_Num());                                                                   //Natural: ASSIGN ADDR.PYMNT-PRCSS-SEQ-NBR := PYMNT.PYMNT-PRCSS-SEQ-NUM
        pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr().setValue(pdaFcpapmnr.getPymnt_Cntrct_Cmbn_Nbr());                                                               //Natural: ASSIGN ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR := PYMNT.CNTRCT-CMBN-NBR
        DbsUtil.callnat(Fcpnaddr.class , getCurrentProcessState(), pdaFcpaaddr.getAddr_Work_Fields(), pdaFcpaaddr.getAddr());                                             //Natural: CALLNAT 'FCPNADDR' ADDR-WORK-FIELDS ADDR
        if (condition(Global.isEscape())) return;
        //*  IF LAST NAME IS MISSING
        //*  PICK UP NAME FROM STATEMENT LINE
        if (condition(pdaFcpaaddr.getAddr_Ph_Last_Name().equals(" ") && pdaFcpaaddr.getAddr_Pymnt_Nme().getValue(1).notEquals(" ")))                                      //Natural: IF ADDR.PH-LAST-NAME = ' ' AND ADDR.PYMNT-NME ( 1 ) NE ' ' THEN
        {
            pnd_Long_Name.setValue(pdaFcpaaddr.getAddr_Pymnt_Nme().getValue(1));                                                                                          //Natural: ASSIGN #LONG-NAME = ADDR.PYMNT-NME ( 1 )
            if (condition(pnd_Long_Name.getSubstring(1,3).equals("CR ")))                                                                                                 //Natural: IF SUBSTR ( #LONG-NAME,1,3 ) = 'CR ' THEN
            {
                pnd_Long_Name.setValue(pdaFcpaaddr.getAddr_Pymnt_Nme().getValue(1).getSubstring(4));                                                                      //Natural: ASSIGN #LONG-NAME = SUBSTR ( ADDR.PYMNT-NME ( 1 ) ,4 )
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Long_Name), new ExamineSearch(" "), new ExamineGivingPosition(pnd_Blank));                                              //Natural: EXAMINE #LONG-NAME FOR ' ' GIVING POSITION IN #BLANK
            if (condition(pnd_Blank.greaterOrEqual(1) && pnd_Blank.lessOrEqual(50)))                                                                                      //Natural: IF #BLANK = 1 THRU 50 THEN
            {
                pdaFcpaaddr.getAddr_Ph_First_Name().setValue(pnd_Long_Name.getSubstring(1,pnd_Blank.getInt()));                                                           //Natural: ASSIGN ADDR.PH-FIRST-NAME := SUBSTR ( #LONG-NAME,1,#BLANK )
                pnd_Last_Name.setValue(pnd_Long_Name.getSubstring(pnd_Blank.getInt()));                                                                                   //Natural: ASSIGN #LAST-NAME := SUBSTR ( #LONG-NAME ,#BLANK )
                pnd_Last_Name.setValue(pnd_Last_Name, MoveOption.LeftJustified);                                                                                          //Natural: MOVE LEFT #LAST-NAME TO #LAST-NAME
                DbsUtil.examine(new ExamineSource(pnd_Last_Name), new ExamineSearch(" "), new ExamineGivingPosition(pnd_Blank));                                          //Natural: EXAMINE #LAST-NAME FOR ' ' GIVING POSITION IN #BLANK
                if (condition(pnd_Blank.greaterOrEqual(1) && pnd_Blank.lessOrEqual(50)))                                                                                  //Natural: IF #BLANK = 1 THRU 50 THEN
                {
                    pdaFcpaaddr.getAddr_Ph_Middle_Name().setValue(pnd_Last_Name.getSubstring(1,pnd_Blank.getInt()));                                                      //Natural: ASSIGN ADDR.PH-MIDDLE-NAME := SUBSTR ( #LAST-NAME,1,#BLANK )
                    pdaFcpaaddr.getAddr_Ph_Last_Name().setValue(pnd_Last_Name.getSubstring(pnd_Blank.getInt()));                                                          //Natural: ASSIGN ADDR.PH-LAST-NAME := SUBSTR ( #LAST-NAME, #BLANK )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpaaddr.getAddr_Ph_Last_Name().setValue(pnd_Last_Name);                                                                                           //Natural: ASSIGN ADDR.PH-LAST-NAME := #LAST-NAME
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpaaddr.getAddr_Ph_Last_Name().setValue(pdaFcpaaddr.getAddr_Ph_Last_Name(), MoveOption.LeftJustified);                                                //Natural: MOVE LEFT ADDR.PH-LAST-NAME TO ADDR.PH-LAST-NAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpaaddr.getAddr_Ph_Last_Name().setValue(pnd_Long_Name);                                                                                               //Natural: ASSIGN ADDR.PH-LAST-NAME := #LONG-NAME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl121.getPnd_Ledger_Extract_Ph_Last_Name().setValue(pdaFcpaaddr.getAddr_Ph_Last_Name());                                                                     //Natural: ASSIGN #LEDGER-EXTRACT.PH-LAST-NAME := ADDR.PH-LAST-NAME
        ldaFcpl121.getPnd_Ledger_Extract_Ph_First_Name().setValue(pdaFcpaaddr.getAddr_Ph_First_Name());                                                                   //Natural: ASSIGN #LEDGER-EXTRACT.PH-FIRST-NAME := ADDR.PH-FIRST-NAME
        ldaFcpl121.getPnd_Ledger_Extract_Ph_Middle_Name().setValue(pdaFcpaaddr.getAddr_Ph_Middle_Name());                                                                 //Natural: ASSIGN #LEDGER-EXTRACT.PH-MIDDLE-NAME := ADDR.PH-MIDDLE-NAME
        //*  (1350) GET-NAME
    }
    private void sub_Get_Isa_Code() throws Exception                                                                                                                      //Natural: GET-ISA-CODE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        pnd_Ws_Pnd_Isa_Sub.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde_Num());                                                                                 //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-CDE-NUM TO #ISA-SUB
        ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Isa_Sub.getInt() + 1));                     //Natural: MOVE #ISA-LDA.#ISA-CDE ( #ISA-SUB ) TO #LEDGER-EXTRACT.INV-ACCT-ISA
        //*  (1790) GET-ISA-CODE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");
    }
}
