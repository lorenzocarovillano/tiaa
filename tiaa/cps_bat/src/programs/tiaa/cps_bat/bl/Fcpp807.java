/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:01 PM
**        * FROM NATURAL PROGRAM : Fcpp807
************************************************************
**        * FILE NAME            : Fcpp807.java
**        * CLASS NAME           : Fcpp807
**        * INSTANCE NAME        : Fcpp807
************************************************************
************************************************************************
*
* PROGRAM   : FCPP807
*
* SYSTEM    : CPS
* TITLE     : PENDED PAYMENTS REPORTS
* GENERATED :
*
* FUNCTION  : GENERATE REPORTS OF ALL CONTRACTS WHERE PAYMENTS ARE
*           : BEING SUSPENDED.
* HISTORY   :
* 02/20/97  : RITA SALGADO  INFLATION LINK BOND
* 06/20/00  : J HOFSTETTER  ADD 'PA Select' & 'Univ Life' TO DEDUCTIONS
* 06/12/02  : R. CARREON    INCLUDE PA-SELECT AND SPIA NEW FUNDS
* 05/20/03  : T. MCGEE      SELF REMITTER TPA TO CASH (PEND CODE)
* 07/01/03  : T. MCGEE      CONTRACT EXPIRED  (PEND CODE)
* 05/24/07  : LANDRUM       CSV FILE FOR NDM (EORK FILE 02)
* 05/06/08 : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 10/18/2010 J. BREMER - ADD VALUATION PERIOD AS PART OF SORT SEQUENCE -
*                                                JB01
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
*  THIS PROGRAM SHOULD BE CHANGED TO USE FCPL199A  RSS.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp807 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Pymnt_Suspend_Dte_E;
    private DbsField pnd_Pymnt_Check_Dte_E;
    private DbsField pnd_Pymnt_Check_Amt_E;
    private DbsField pnd_Sub_No;
    private DbsField pnd_Total_Wk2;
    private DbsField pnd_Index;
    private DbsField pnd_Na_Index;
    private DbsField pnd_Tax_Amt;
    private DbsField pnd_Net_Amt;
    private DbsField pnd_Check_Amt;
    private DbsField pnd_Suspend_Code;
    private DbsField pnd_Old_Suspend_Code;
    private DbsField pnd_Suspend_Lit;
    private DbsField cmbn_Cntrct_Break;
    private DbsField pend_Code_Break;
    private DbsField pnd_Name_Addr;

    private DbsGroup pnd_Accumulators;
    private DbsField pnd_Accumulators_Pnd_Ws_Amount_Acc;
    private DbsField pnd_Ws_Mon_Ann_Tot;
    private DbsField pnd_Ws_Grand_Tot;
    private DbsField pnd_Mon_Ann_Desc;
    private DbsField pnd_Ws_Ndx;
    private DbsField pnd_Ws_Period;
    private DbsField pnd_Ws_Valuat;

    private DbsGroup pnd_Ws_Description;
    private DbsField pnd_Ws_Description_F01;
    private DbsField pnd_Ws_Description_F02;
    private DbsField pnd_Ws_Description_F03;
    private DbsField pnd_Ws_Description_F04;
    private DbsField pnd_Ws_Description_F05;
    private DbsField pnd_Ws_Description_F06;
    private DbsField pnd_Ws_Description_F07;
    private DbsField pnd_Ws_Description_F08;
    private DbsField pnd_Ws_Description_F09;
    private DbsField pnd_Ws_Description_F10;
    private DbsField pnd_Ws_Description_F11;
    private DbsField pnd_Ws_Description_F12;
    private DbsField pnd_Ws_Description_F13;
    private DbsField pnd_Ws_Description_F14;
    private DbsField pnd_Ws_Description_F15;
    private DbsField pnd_Ws_Description_F16;
    private DbsField pnd_Ws_Description_F17;
    private DbsField pnd_Ws_Description_F18;
    private DbsField pnd_Ws_Description_F19;
    private DbsField pnd_Ws_Description_F20;
    private DbsField pnd_Ws_Description_F21;
    private DbsField pnd_Ws_Description_F22;
    private DbsField pnd_Ws_Description_F23;
    private DbsField pnd_Ws_Description_F24;
    private DbsField pnd_Ws_Description_F25;
    private DbsField pnd_Ws_Description_F26;
    private DbsField pnd_Ws_Description_F27;
    private DbsField pnd_Ws_Description_F28;
    private DbsField pnd_Ws_Description_F29;
    private DbsField pnd_Ws_Description_F30;
    private DbsField pnd_Ws_Description_F31;
    private DbsField pnd_Ws_Description_F32;
    private DbsField pnd_Ws_Description_F33;
    private DbsField pnd_Ws_Description_F34;
    private DbsField pnd_Ws_Description_F35;
    private DbsField pnd_Ws_Description_F36;
    private DbsField pnd_Ws_Description_F37;
    private DbsField pnd_Ws_Description_F38;
    private DbsField pnd_Ws_Description_F39;

    private DbsGroup pnd_Ws_Description__R_Field_1;
    private DbsField pnd_Ws_Description_Pnd_Ws_Desc_Table;
    private DbsField pnd_Annty_Ins_Type;
    private DbsField pnd_Annty_Type;
    private DbsField pnd_Insurance_Option;
    private DbsField pnd_Life_Contingency;
    private DbsField pnd_Print_Line;

    private DbsGroup pnd_Print_Line__R_Field_2;
    private DbsField pnd_Print_Line_P_Combine_Nbr;
    private DbsField pnd_Print_Line__Filler1;
    private DbsField pnd_Print_Line_P_Cntrct_Nbr;
    private DbsField pnd_Print_Line__Filler2;
    private DbsField pnd_Print_Line_P_Ss_Nbr;
    private DbsField pnd_Print_Line__Filler3;
    private DbsField pnd_Print_Line_P_Option;
    private DbsField pnd_Print_Line__Filler4;
    private DbsField pnd_Print_Line_P_Ann_Ins;
    private DbsField pnd_Print_Line__Filler5;
    private DbsField pnd_Print_Line_P_Cntrct_Type;
    private DbsField pnd_Print_Line__Filler6;
    private DbsField pnd_Print_Line_P_Prod_Line;
    private DbsField pnd_Print_Line__Filler7;
    private DbsField pnd_Print_Line_P_Life_Cont;
    private DbsField pnd_Print_Line__Filler8;
    private DbsField pnd_Print_Line_P_Fund;
    private DbsField pnd_Print_Line_P_Lit;
    private DbsField pnd_Print_Line_P_Amt;
    private DbsField pnd_Print_Line__Filler9;
    private DbsField pnd_Print_Line_P_Name_Addr;

    private DbsGroup pnd_Print_Line__R_Field_3;
    private DbsField pnd_Print_Line_P_Pend_Lit;
    private DbsField pnd_Print_Line_P_Pend_Date;
    private DbsField pnd_Print_Line__Filler10;
    private DbsField pnd_Print_Line_P_Net_Lit;
    private DbsField pnd_Print_Line_P_Net_Amt;
    private DbsField pnd_Print_Line_2;

    private DbsGroup pnd_Print_Line_2__R_Field_4;
    private DbsField pnd_Print_Line_2_P_Combine_Nbr_2;
    private DbsField pnd_Print_Line_2__Filler11;
    private DbsField pnd_Print_Line_2_P_Cntrct_Nbr_2;
    private DbsField pnd_Print_Line_2__Filler12;
    private DbsField pnd_Print_Line_2_P_Ss_Nbr_2;
    private DbsField pnd_Print_Line_2__Filler13;
    private DbsField pnd_Print_Line_2_P_Option_2;
    private DbsField pnd_Print_Line_2__Filler14;
    private DbsField pnd_Print_Line_2_P_Ann_Ins_2;
    private DbsField pnd_Print_Line_2__Filler15;
    private DbsField pnd_Print_Line_2_P_Cntrct_Type_2;
    private DbsField pnd_Print_Line_2__Filler16;
    private DbsField pnd_Print_Line_2_P_Prod_Line_2;
    private DbsField pnd_Print_Line_2__Filler17;
    private DbsField pnd_Print_Line_2_P_Life_Cont_2;
    private DbsField pnd_Print_Line_2__Filler18;
    private DbsField pnd_Print_Line_2_P_Fund_2;
    private DbsField pnd_Print_Line_2_P_Lit_2;
    private DbsField pnd_Print_Line_2_P_Amt_2;

    private DbsGroup pnd_Print_Line_2__R_Field_5;
    private DbsField pnd_Print_Line_2__Filler19;
    private DbsField pnd_Print_Line_2_P_Net_Lit_2;
    private DbsField pnd_Print_Line_2_P_Net_Amt_2;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pymnt_Suspend_CdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);

        // Local Variables
        pnd_Pymnt_Suspend_Dte_E = localVariables.newFieldInRecord("pnd_Pymnt_Suspend_Dte_E", "#PYMNT-SUSPEND-DTE-E", FieldType.STRING, 10);
        pnd_Pymnt_Check_Dte_E = localVariables.newFieldInRecord("pnd_Pymnt_Check_Dte_E", "#PYMNT-CHECK-DTE-E", FieldType.STRING, 10);
        pnd_Pymnt_Check_Amt_E = localVariables.newFieldInRecord("pnd_Pymnt_Check_Amt_E", "#PYMNT-CHECK-AMT-E", FieldType.STRING, 15);
        pnd_Sub_No = localVariables.newFieldInRecord("pnd_Sub_No", "#SUB-NO", FieldType.STRING, 4);
        pnd_Total_Wk2 = localVariables.newFieldInRecord("pnd_Total_Wk2", "#TOTAL-WK2", FieldType.NUMERIC, 15, 2);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Na_Index = localVariables.newFieldInRecord("pnd_Na_Index", "#NA-INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Net_Amt = localVariables.newFieldInRecord("pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Check_Amt = localVariables.newFieldInRecord("pnd_Check_Amt", "#CHECK-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Suspend_Code = localVariables.newFieldInRecord("pnd_Suspend_Code", "#SUSPEND-CODE", FieldType.STRING, 1);
        pnd_Old_Suspend_Code = localVariables.newFieldInRecord("pnd_Old_Suspend_Code", "#OLD-SUSPEND-CODE", FieldType.STRING, 1);
        pnd_Suspend_Lit = localVariables.newFieldInRecord("pnd_Suspend_Lit", "#SUSPEND-LIT", FieldType.STRING, 30);
        cmbn_Cntrct_Break = localVariables.newFieldInRecord("cmbn_Cntrct_Break", "CMBN-CNTRCT-BREAK", FieldType.BOOLEAN, 1);
        pend_Code_Break = localVariables.newFieldInRecord("pend_Code_Break", "PEND-CODE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Name_Addr = localVariables.newFieldArrayInRecord("pnd_Name_Addr", "#NAME-ADDR", FieldType.STRING, 20, new DbsArrayController(1, 10));

        pnd_Accumulators = localVariables.newGroupArrayInRecord("pnd_Accumulators", "#ACCUMULATORS", new DbsArrayController(1, 2));
        pnd_Accumulators_Pnd_Ws_Amount_Acc = pnd_Accumulators.newFieldArrayInGroup("pnd_Accumulators_Pnd_Ws_Amount_Acc", "#WS-AMOUNT-ACC", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 39));
        pnd_Ws_Mon_Ann_Tot = localVariables.newFieldInRecord("pnd_Ws_Mon_Ann_Tot", "#WS-MON-ANN-TOT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Grand_Tot = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot", "#WS-GRAND-TOT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Mon_Ann_Desc = localVariables.newFieldInRecord("pnd_Mon_Ann_Desc", "#MON-ANN-DESC", FieldType.STRING, 10);
        pnd_Ws_Ndx = localVariables.newFieldInRecord("pnd_Ws_Ndx", "#WS-NDX", FieldType.NUMERIC, 2);
        pnd_Ws_Period = localVariables.newFieldInRecord("pnd_Ws_Period", "#WS-PERIOD", FieldType.NUMERIC, 2);
        pnd_Ws_Valuat = localVariables.newFieldInRecord("pnd_Ws_Valuat", "#WS-VALUAT", FieldType.NUMERIC, 2);

        pnd_Ws_Description = localVariables.newGroupInRecord("pnd_Ws_Description", "#WS-DESCRIPTION");
        pnd_Ws_Description_F01 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F01", "F01", FieldType.STRING, 18);
        pnd_Ws_Description_F02 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F02", "F02", FieldType.STRING, 18);
        pnd_Ws_Description_F03 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F03", "F03", FieldType.STRING, 18);
        pnd_Ws_Description_F04 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F04", "F04", FieldType.STRING, 18);
        pnd_Ws_Description_F05 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F05", "F05", FieldType.STRING, 18);
        pnd_Ws_Description_F06 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F06", "F06", FieldType.STRING, 18);
        pnd_Ws_Description_F07 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F07", "F07", FieldType.STRING, 18);
        pnd_Ws_Description_F08 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F08", "F08", FieldType.STRING, 18);
        pnd_Ws_Description_F09 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F09", "F09", FieldType.STRING, 18);
        pnd_Ws_Description_F10 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F10", "F10", FieldType.STRING, 18);
        pnd_Ws_Description_F11 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F11", "F11", FieldType.STRING, 18);
        pnd_Ws_Description_F12 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F12", "F12", FieldType.STRING, 18);
        pnd_Ws_Description_F13 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F13", "F13", FieldType.STRING, 18);
        pnd_Ws_Description_F14 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F14", "F14", FieldType.STRING, 18);
        pnd_Ws_Description_F15 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F15", "F15", FieldType.STRING, 18);
        pnd_Ws_Description_F16 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F16", "F16", FieldType.STRING, 18);
        pnd_Ws_Description_F17 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F17", "F17", FieldType.STRING, 18);
        pnd_Ws_Description_F18 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F18", "F18", FieldType.STRING, 18);
        pnd_Ws_Description_F19 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F19", "F19", FieldType.STRING, 18);
        pnd_Ws_Description_F20 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F20", "F20", FieldType.STRING, 18);
        pnd_Ws_Description_F21 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F21", "F21", FieldType.STRING, 18);
        pnd_Ws_Description_F22 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F22", "F22", FieldType.STRING, 18);
        pnd_Ws_Description_F23 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F23", "F23", FieldType.STRING, 18);
        pnd_Ws_Description_F24 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F24", "F24", FieldType.STRING, 18);
        pnd_Ws_Description_F25 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F25", "F25", FieldType.STRING, 18);
        pnd_Ws_Description_F26 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F26", "F26", FieldType.STRING, 18);
        pnd_Ws_Description_F27 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F27", "F27", FieldType.STRING, 18);
        pnd_Ws_Description_F28 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F28", "F28", FieldType.STRING, 18);
        pnd_Ws_Description_F29 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F29", "F29", FieldType.STRING, 18);
        pnd_Ws_Description_F30 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F30", "F30", FieldType.STRING, 18);
        pnd_Ws_Description_F31 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F31", "F31", FieldType.STRING, 18);
        pnd_Ws_Description_F32 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F32", "F32", FieldType.STRING, 18);
        pnd_Ws_Description_F33 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F33", "F33", FieldType.STRING, 18);
        pnd_Ws_Description_F34 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F34", "F34", FieldType.STRING, 18);
        pnd_Ws_Description_F35 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F35", "F35", FieldType.STRING, 18);
        pnd_Ws_Description_F36 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F36", "F36", FieldType.STRING, 18);
        pnd_Ws_Description_F37 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F37", "F37", FieldType.STRING, 18);
        pnd_Ws_Description_F38 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F38", "F38", FieldType.STRING, 18);
        pnd_Ws_Description_F39 = pnd_Ws_Description.newFieldInGroup("pnd_Ws_Description_F39", "F39", FieldType.STRING, 18);

        pnd_Ws_Description__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Description__R_Field_1", "REDEFINE", pnd_Ws_Description);
        pnd_Ws_Description_Pnd_Ws_Desc_Table = pnd_Ws_Description__R_Field_1.newFieldArrayInGroup("pnd_Ws_Description_Pnd_Ws_Desc_Table", "#WS-DESC-TABLE", 
            FieldType.STRING, 18, new DbsArrayController(1, 39));
        pnd_Annty_Ins_Type = localVariables.newFieldInRecord("pnd_Annty_Ins_Type", "#ANNTY-INS-TYPE", FieldType.STRING, 4);
        pnd_Annty_Type = localVariables.newFieldInRecord("pnd_Annty_Type", "#ANNTY-TYPE", FieldType.STRING, 4);
        pnd_Insurance_Option = localVariables.newFieldInRecord("pnd_Insurance_Option", "#INSURANCE-OPTION", FieldType.STRING, 4);
        pnd_Life_Contingency = localVariables.newFieldInRecord("pnd_Life_Contingency", "#LIFE-CONTINGENCY", FieldType.STRING, 4);
        pnd_Print_Line = localVariables.newFieldInRecord("pnd_Print_Line", "#PRINT-LINE", FieldType.STRING, 131);

        pnd_Print_Line__R_Field_2 = localVariables.newGroupInRecord("pnd_Print_Line__R_Field_2", "REDEFINE", pnd_Print_Line);
        pnd_Print_Line_P_Combine_Nbr = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Combine_Nbr", "P-COMBINE-NBR", FieldType.STRING, 14);
        pnd_Print_Line__Filler1 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler1", "_FILLER1", FieldType.STRING, 1);
        pnd_Print_Line_P_Cntrct_Nbr = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Cntrct_Nbr", "P-CNTRCT-NBR", FieldType.STRING, 9);
        pnd_Print_Line__Filler2 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler2", "_FILLER2", FieldType.STRING, 1);
        pnd_Print_Line_P_Ss_Nbr = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Ss_Nbr", "P-SS-NBR", FieldType.STRING, 11);
        pnd_Print_Line__Filler3 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler3", "_FILLER3", FieldType.STRING, 2);
        pnd_Print_Line_P_Option = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Option", "P-OPTION", FieldType.STRING, 2);
        pnd_Print_Line__Filler4 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler4", "_FILLER4", FieldType.STRING, 2);
        pnd_Print_Line_P_Ann_Ins = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Ann_Ins", "P-ANN-INS", FieldType.STRING, 4);
        pnd_Print_Line__Filler5 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler5", "_FILLER5", FieldType.STRING, 2);
        pnd_Print_Line_P_Cntrct_Type = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Cntrct_Type", "P-CNTRCT-TYPE", FieldType.STRING, 4);
        pnd_Print_Line__Filler6 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler6", "_FILLER6", FieldType.STRING, 2);
        pnd_Print_Line_P_Prod_Line = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Prod_Line", "P-PROD-LINE", FieldType.STRING, 4);
        pnd_Print_Line__Filler7 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler7", "_FILLER7", FieldType.STRING, 2);
        pnd_Print_Line_P_Life_Cont = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Life_Cont", "P-LIFE-CONT", FieldType.STRING, 4);
        pnd_Print_Line__Filler8 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler8", "_FILLER8", FieldType.STRING, 2);
        pnd_Print_Line_P_Fund = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Fund", "P-FUND", FieldType.STRING, 5);
        pnd_Print_Line_P_Lit = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Lit", "P-LIT", FieldType.STRING, 14);
        pnd_Print_Line_P_Amt = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Amt", "P-AMT", FieldType.STRING, 13);
        pnd_Print_Line__Filler9 = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line__Filler9", "_FILLER9", FieldType.STRING, 10);
        pnd_Print_Line_P_Name_Addr = pnd_Print_Line__R_Field_2.newFieldInGroup("pnd_Print_Line_P_Name_Addr", "P-NAME-ADDR", FieldType.STRING, 20);

        pnd_Print_Line__R_Field_3 = localVariables.newGroupInRecord("pnd_Print_Line__R_Field_3", "REDEFINE", pnd_Print_Line);
        pnd_Print_Line_P_Pend_Lit = pnd_Print_Line__R_Field_3.newFieldInGroup("pnd_Print_Line_P_Pend_Lit", "P-PEND-LIT", FieldType.STRING, 11);
        pnd_Print_Line_P_Pend_Date = pnd_Print_Line__R_Field_3.newFieldInGroup("pnd_Print_Line_P_Pend_Date", "P-PEND-DATE", FieldType.STRING, 10);
        pnd_Print_Line__Filler10 = pnd_Print_Line__R_Field_3.newFieldInGroup("pnd_Print_Line__Filler10", "_FILLER10", FieldType.STRING, 57);
        pnd_Print_Line_P_Net_Lit = pnd_Print_Line__R_Field_3.newFieldInGroup("pnd_Print_Line_P_Net_Lit", "P-NET-LIT", FieldType.STRING, 17);
        pnd_Print_Line_P_Net_Amt = pnd_Print_Line__R_Field_3.newFieldInGroup("pnd_Print_Line_P_Net_Amt", "P-NET-AMT", FieldType.STRING, 13);
        pnd_Print_Line_2 = localVariables.newFieldInRecord("pnd_Print_Line_2", "#PRINT-LINE-2", FieldType.STRING, 131);

        pnd_Print_Line_2__R_Field_4 = localVariables.newGroupInRecord("pnd_Print_Line_2__R_Field_4", "REDEFINE", pnd_Print_Line_2);
        pnd_Print_Line_2_P_Combine_Nbr_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Combine_Nbr_2", "P-COMBINE-NBR-2", FieldType.STRING, 
            14);
        pnd_Print_Line_2__Filler11 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2__Filler11", "_FILLER11", FieldType.STRING, 2);
        pnd_Print_Line_2_P_Cntrct_Nbr_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Cntrct_Nbr_2", "P-CNTRCT-NBR-2", FieldType.STRING, 
            9);
        pnd_Print_Line_2__Filler12 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2__Filler12", "_FILLER12", FieldType.STRING, 2);
        pnd_Print_Line_2_P_Ss_Nbr_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Ss_Nbr_2", "P-SS-NBR-2", FieldType.STRING, 11);
        pnd_Print_Line_2__Filler13 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2__Filler13", "_FILLER13", FieldType.STRING, 2);
        pnd_Print_Line_2_P_Option_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Option_2", "P-OPTION-2", FieldType.STRING, 2);
        pnd_Print_Line_2__Filler14 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2__Filler14", "_FILLER14", FieldType.STRING, 3);
        pnd_Print_Line_2_P_Ann_Ins_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Ann_Ins_2", "P-ANN-INS-2", FieldType.STRING, 4);
        pnd_Print_Line_2__Filler15 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2__Filler15", "_FILLER15", FieldType.STRING, 2);
        pnd_Print_Line_2_P_Cntrct_Type_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Cntrct_Type_2", "P-CNTRCT-TYPE-2", FieldType.STRING, 
            4);
        pnd_Print_Line_2__Filler16 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2__Filler16", "_FILLER16", FieldType.STRING, 2);
        pnd_Print_Line_2_P_Prod_Line_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Prod_Line_2", "P-PROD-LINE-2", FieldType.STRING, 
            4);
        pnd_Print_Line_2__Filler17 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2__Filler17", "_FILLER17", FieldType.STRING, 2);
        pnd_Print_Line_2_P_Life_Cont_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Life_Cont_2", "P-LIFE-CONT-2", FieldType.STRING, 
            4);
        pnd_Print_Line_2__Filler18 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2__Filler18", "_FILLER18", FieldType.STRING, 4);
        pnd_Print_Line_2_P_Fund_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Fund_2", "P-FUND-2", FieldType.STRING, 5);
        pnd_Print_Line_2_P_Lit_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Lit_2", "P-LIT-2", FieldType.STRING, 14);
        pnd_Print_Line_2_P_Amt_2 = pnd_Print_Line_2__R_Field_4.newFieldInGroup("pnd_Print_Line_2_P_Amt_2", "P-AMT-2", FieldType.STRING, 13);

        pnd_Print_Line_2__R_Field_5 = localVariables.newGroupInRecord("pnd_Print_Line_2__R_Field_5", "REDEFINE", pnd_Print_Line_2);
        pnd_Print_Line_2__Filler19 = pnd_Print_Line_2__R_Field_5.newFieldInGroup("pnd_Print_Line_2__Filler19", "_FILLER19", FieldType.STRING, 86);
        pnd_Print_Line_2_P_Net_Lit_2 = pnd_Print_Line_2__R_Field_5.newFieldInGroup("pnd_Print_Line_2_P_Net_Lit_2", "P-NET-LIT-2", FieldType.STRING, 16);
        pnd_Print_Line_2_P_Net_Amt_2 = pnd_Print_Line_2__R_Field_5.newFieldInGroup("pnd_Print_Line_2_P_Net_Amt_2", "P-NET-AMT-2", FieldType.STRING, 13);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pymnt_Suspend_CdeOld = internalLoopRecord.newFieldInRecord("Sort01_Pymnt_Suspend_Cde_OLD", "Pymnt_Suspend_Cde_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();

        localVariables.reset();
        cmbn_Cntrct_Break.setInitialValue(true);
        pend_Code_Break.setInitialValue(true);
        pnd_Ws_Ndx.setInitialValue(0);
        pnd_Ws_Period.setInitialValue(0);
        pnd_Ws_Valuat.setInitialValue(0);
        pnd_Ws_Description_F01.setInitialValue("  TIAA            ");
        pnd_Ws_Description_F02.setInitialValue("  TIAA Insurance  ");
        pnd_Ws_Description_F03.setInitialValue("  TIAA P.A.       ");
        pnd_Ws_Description_F04.setInitialValue("  Real Estate     ");
        pnd_Ws_Description_F05.setInitialValue("TIAA Total        ");
        pnd_Ws_Description_F06.setInitialValue("  Stock           ");
        pnd_Ws_Description_F07.setInitialValue("  Bond            ");
        pnd_Ws_Description_F08.setInitialValue("  Money Market    ");
        pnd_Ws_Description_F09.setInitialValue("  Social Choice   ");
        pnd_Ws_Description_F10.setInitialValue("  Global          ");
        pnd_Ws_Description_F11.setInitialValue("  Growth          ");
        pnd_Ws_Description_F12.setInitialValue("  Equity Index    ");
        pnd_Ws_Description_F13.setInitialValue("  Infl Linked Bond");
        pnd_Ws_Description_F14.setInitialValue("CREF Total        ");
        pnd_Ws_Description_F15.setInitialValue("PA Select         ");
        pnd_Ws_Description_F16.setInitialValue("  Fixed Fund      ");
        pnd_Ws_Description_F17.setInitialValue("  Stock Index Acct");
        pnd_Ws_Description_F18.setInitialValue("  Growth Equity   ");
        pnd_Ws_Description_F19.setInitialValue("  Growth & Income ");
        pnd_Ws_Description_F20.setInitialValue("  Internatl Equity");
        pnd_Ws_Description_F21.setInitialValue("  Social Choice Eq");
        pnd_Ws_Description_F22.setInitialValue("  Large-Cap Value");
        pnd_Ws_Description_F23.setInitialValue("  Small-Cap Value");
        pnd_Ws_Description_F24.setInitialValue("  Real Estate Sec");
        pnd_Ws_Description_F25.setInitialValue("PA Select Total   ");
        pnd_Ws_Description_F26.setInitialValue("SPIA              ");
        pnd_Ws_Description_F27.setInitialValue("  Fixed Fund      ");
        pnd_Ws_Description_F28.setInitialValue("  Genl Ledger Acct");
        pnd_Ws_Description_F29.setInitialValue("  Stock Index Acct");
        pnd_Ws_Description_F30.setInitialValue("  Growth Equity   ");
        pnd_Ws_Description_F31.setInitialValue("  Growth & Income ");
        pnd_Ws_Description_F32.setInitialValue("  Internatl Equity");
        pnd_Ws_Description_F33.setInitialValue("  Social Choice Eq");
        pnd_Ws_Description_F34.setInitialValue("  Large-Cap Value");
        pnd_Ws_Description_F35.setInitialValue("  Small-Cap Value");
        pnd_Ws_Description_F36.setInitialValue("  Real Estate Sec");
        pnd_Ws_Description_F37.setInitialValue("SPIA Total        ");
        pnd_Ws_Description_F38.setInitialValue("                  ");
        pnd_Ws_Description_F39.setInitialValue("Grand Total       ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp807() throws Exception
    {
        super("Fcpp807");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP807", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 55;//Natural: FORMAT ( 02 ) LS = 132 PS = 55
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE2-HEADING
        sub_Write_Work_File2_Heading();
        if (condition(Global.isEscape())) {return;}
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  READ PAYMENT/NAME AND ADDRESS WORK FILE
        //*  SORTED BY PEND CODE, COMBINE #, CONTRACT #
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Ret_Code().notEquals("0002")))                                                                              //Natural: IF GTN-RET-CODE NE '0002'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),  //Natural: END-ALL
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde(), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte(), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(16), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(22), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(28), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(34), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(40), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(6), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(14), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(20), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(26), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(32), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(38), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(3), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(9), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(11), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(13), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(15), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(17), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(19), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(21), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(23), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(25), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(27), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(29), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(31), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(33), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(35), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(37), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(39), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(2), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(5), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(8), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(4), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(7), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(10), 
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name_1());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  ADDED 10/19/2010 JB01 - INC1080789
        getSort().sortData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr()); //Natural: SORT BY PYMNT-SUSPEND-CDE CNTRCT-CMBN-NBR CNTRCT-PPCN-NBR USING PYMNT-CHECK-DTE CNTRCT-UNQ-ID-NBR ANNT-SOC-SEC-NBR CNTRCT-OPTION-CDE CNTRCT-ANNTY-INS-TYPE CNTRCT-ANNTY-TYPE-CDE CNTRCT-INSURANCE-OPTION CNTRCT-LIFE-CONTINGENCY PYMNT-SUSPEND-DTE INV-ACCT-COUNT INV-ACCT-CDE-ALPHA ( * ) INV-ACCT-SETTL-AMT ( * ) INV-ACCT-CNTRCT-AMT ( * ) INV-ACCT-DVDND-AMT ( * ) INV-ACCT-FDRL-TAX-AMT ( * ) INV-ACCT-STATE-TAX-AMT ( * ) INV-ACCT-LOCAL-TAX-AMT ( * ) INV-ACCT-NET-PYMNT-AMT ( * ) INV-ACCT-VALUAT-PERIOD ( * ) PYMNT-DED-CDE ( * ) PYMNT-DED-AMT ( * ) PYMNT-CHECK-AMT PH-FIRST-NAME-1 PH-MIDDLE-NAME-1 PH-LAST-NAME PYMNT-NME ( 1 ) PYMNT-ADDR-LINE1-TXT ( 1 ) PYMNT-ADDR-LINE2-TXT ( 1 ) PYMNT-ADDR-LINE3-TXT ( 1 ) PYMNT-ADDR-LINE4-TXT ( 1 ) PYMNT-ADDR-LINE5-TXT ( 1 ) PYMNT-ADDR-LINE6-TXT ( 1 )
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(4), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(10), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(16), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(22), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(28), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(34), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(40), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(6), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(12), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(18), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(24), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(30), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(36), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(2), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(8), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(13), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(14), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(19), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(20), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(25), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(26), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(31), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(32), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(37), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(38), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(4), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(10), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(11), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(15), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(16), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(17), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(21), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(22), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(23), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(27), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(28), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(29), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(33), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(34), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(35), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(39), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(40), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(6), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(3), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(9), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(11), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(12), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(13), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(14), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(15), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(16), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(17), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(18), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(19), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(20), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(21), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(22), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(23), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(24), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(25), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(26), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(27), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(28), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(29), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(30), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(31), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(32), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(33), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(34), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(35), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(36), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(37), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(38), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(39), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(40), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(1), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(2), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(4), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(5), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(7), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(8), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(10), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(1), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(2), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(3), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(4), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(5), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(6), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(7), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(8), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(9), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(10), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name_1())))
        {
            CheckAtStartofData702();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  ----------------------------------
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '1' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 46T 'PENDING PAYMENTS FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) // 'Pend Code:' PYMNT-SUSPEND-CDE '   ' #SUSPEND-LIT / '=' ( 131 ) / '                                          Ann/ Cntr ' 'Prod  Life' / 'Combine Nbr   Contract#  Soc Sec No  Opt  Ins Type ' 'Type  Cont.' / '=' ( 131 ) //
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ----------------------------------
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 02 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '2' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 48T 'PENDING PAYMENTS FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) // 'Pend Code: 1  Missing Name and Address' / '=' ( 131 ) / '                                            Ann/ Cntr ' 'Prod  Life' / 'Combine Nbr     Contract#  Soc Sec No  Opt  Ins Type ' 'Type  Cont.' / '=' ( 131 ) //
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ----------------------------------
            //*  BEFORE BREAK PROCESSING
            //*  END-BEFORE
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF CNTRCT-CMBN-NBR
            //*                                                                                                                                                           //Natural: AT BREAK OF PYMNT-SUSPEND-CDE
            //*                                                                                                                                                           //Natural: AT END OF DATA
                                                                                                                                                                          //Natural: PERFORM BREAK-ROUTINE
            sub_Break_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-ROUTINE
            sub_Initialize_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Print_Line.reset();                                                                                                                                       //Natural: RESET #PRINT-LINE #PRINT-LINE-2
            pnd_Print_Line_2.reset();
            pnd_Print_Line_P_Combine_Nbr.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr());                                                                     //Natural: MOVE CNTRCT-CMBN-NBR TO P-COMBINE-NBR
            pnd_Print_Line_P_Cntrct_Nbr.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),new ReportEditMask("XXXXXXX-X"));                                //Natural: MOVE EDITED CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) TO P-CNTRCT-NBR
            pnd_Print_Line_P_Ss_Nbr.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(),new ReportEditMask("999-99-9999"));                                 //Natural: MOVE EDITED ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) TO P-SS-NBR
            pnd_Print_Line_P_Option.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde());                                                                        //Natural: MOVE CNTRCT-OPTION-CDE TO P-OPTION
            //*  TRANSLATE LEDGER FIELDS
            //* ***********************************************************************
            //*  PROGRAM   : FCPP807
            //*  SYSTEM    : CPS
            //*  TITLE     : TRANSLATE LEDGER CODES
            //*  GENERATED :
            //*  FUNCTION  : USED BY:
            //*            :     FCPP810
            //*            :
            //*  HISTORY   :
            //* ***********************************************************************
            pnd_Annty_Ins_Type.reset();                                                                                                                                   //Natural: RESET #ANNTY-INS-TYPE #ANNTY-TYPE #INSURANCE-OPTION #LIFE-CONTINGENCY
            pnd_Annty_Type.reset();
            pnd_Insurance_Option.reset();
            pnd_Life_Contingency.reset();
            short decideConditionsMet746 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-INS-TYPE;//Natural: VALUE 'A'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("A"))))
            {
                decideConditionsMet746++;
                pnd_Annty_Ins_Type.setValue("Ann ");                                                                                                                      //Natural: MOVE 'Ann ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("I"))))
            {
                decideConditionsMet746++;
                pnd_Annty_Ins_Type.setValue("Ins ");                                                                                                                      //Natural: MOVE 'Ins ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("G"))))
            {
                decideConditionsMet746++;
                pnd_Annty_Ins_Type.setValue("Grp ");                                                                                                                      //Natural: MOVE 'Grp ' TO #ANNTY-INS-TYPE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet756 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-TYPE-CDE;//Natural: VALUE 'M'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M"))))
            {
                decideConditionsMet756++;
                pnd_Annty_Type.setValue("Mat ");                                                                                                                          //Natural: MOVE 'Mat ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'D'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("D"))))
            {
                decideConditionsMet756++;
                pnd_Annty_Type.setValue("Dth ");                                                                                                                          //Natural: MOVE 'Dth ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("P"))))
            {
                decideConditionsMet756++;
                pnd_Annty_Type.setValue("Par ");                                                                                                                          //Natural: MOVE 'Par ' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("N"))))
            {
                decideConditionsMet756++;
                pnd_Annty_Type.setValue("NPar");                                                                                                                          //Natural: MOVE 'NPar' TO #ANNTY-TYPE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet768 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-INSURANCE-OPTION;//Natural: VALUE 'G'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("G"))))
            {
                decideConditionsMet768++;
                pnd_Insurance_Option.setValue("Grp ");                                                                                                                    //Natural: MOVE 'Grp ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("C"))))
            {
                decideConditionsMet768++;
                pnd_Insurance_Option.setValue("Coll");                                                                                                                    //Natural: MOVE 'Coll' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("I"))))
            {
                decideConditionsMet768++;
                pnd_Insurance_Option.setValue("Ind ");                                                                                                                    //Natural: MOVE 'Ind ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: VALUE 'P'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().equals("P"))))
            {
                decideConditionsMet768++;
                pnd_Insurance_Option.setValue("PA  ");                                                                                                                    //Natural: MOVE 'PA  ' TO #INSURANCE-OPTION
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet780 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-LIFE-CONTINGENCY;//Natural: VALUE 'Y'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency().equals("Y"))))
            {
                decideConditionsMet780++;
                pnd_Life_Contingency.setValue("Yes ");                                                                                                                    //Natural: MOVE 'Yes ' TO #LIFE-CONTINGENCY
            }                                                                                                                                                             //Natural: VALUE 'N'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency().equals("N"))))
            {
                decideConditionsMet780++;
                pnd_Life_Contingency.setValue("No  ");                                                                                                                    //Natural: MOVE 'No  ' TO #LIFE-CONTINGENCY
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Print_Line_P_Ann_Ins.setValue(pnd_Annty_Ins_Type);                                                                                                        //Natural: MOVE #ANNTY-INS-TYPE TO P-ANN-INS
            pnd_Print_Line_P_Cntrct_Type.setValue(pnd_Annty_Type);                                                                                                        //Natural: MOVE #ANNTY-TYPE TO P-CNTRCT-TYPE
            pnd_Print_Line_P_Prod_Line.setValue(pnd_Insurance_Option);                                                                                                    //Natural: MOVE #INSURANCE-OPTION TO P-PROD-LINE
            pnd_Print_Line_P_Life_Cont.setValue(pnd_Life_Contingency);                                                                                                    //Natural: MOVE #LIFE-CONTINGENCY TO P-LIFE-CONT
            pnd_Na_Index.setValue(1);                                                                                                                                     //Natural: MOVE 1 TO #NA-INDEX
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 INV-ACCT-COUNT
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals(" ")))                                                      //Natural: IF INV-ACCT-CDE-ALPHA ( #INDEX ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Print_Line_P_Fund.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index));                                                 //Natural: MOVE INV-ACCT-CDE-ALPHA ( #INDEX ) TO P-FUND
                pnd_Print_Line_P_Lit.setValue("Gross Amount");                                                                                                            //Natural: MOVE 'Gross Amount' TO P-LIT
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG")  //Natural: IF INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' OR = 'TG' OR = 'G '
                    || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G ")))
                {
                    pnd_Print_Line_P_Amt.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Index),new ReportEditMask("Z,ZZZ,ZZZ.99-"));   //Natural: MOVE EDITED INV-ACCT-CNTRCT-AMT ( #INDEX ) ( EM = Z,ZZZ,ZZZ.99- ) TO P-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-LINE
                    sub_Print_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index).notEquals(getZero())))                                         //Natural: IF INV-ACCT-DVDND-AMT ( #INDEX ) NE 0
                    {
                        pnd_Print_Line_P_Lit.setValue("Dividend");                                                                                                        //Natural: MOVE 'Dividend' TO P-LIT
                        pnd_Print_Line_P_Amt.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index),new ReportEditMask("Z,ZZZ,ZZZ.99-")); //Natural: MOVE EDITED INV-ACCT-DVDND-AMT ( #INDEX ) ( EM = Z,ZZZ,ZZZ.99- ) TO P-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-LINE
                        sub_Print_Line();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_Index).equals("M")))                                              //Natural: IF INV-ACCT-VALUAT-PERIOD ( #INDEX ) = 'M'
                    {
                        pnd_Print_Line_P_Fund.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Print_Line_P_Fund, "(M)"));                                    //Natural: COMPRESS P-FUND '(M)' INTO P-FUND LEAVING NO
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Print_Line_P_Fund.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Print_Line_P_Fund, "(A)"));                                    //Natural: COMPRESS P-FUND '(A)' INTO P-FUND LEAVING NO
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Print_Line_P_Amt.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index),new ReportEditMask("Z,ZZZ,ZZZ.99-"));    //Natural: MOVE EDITED INV-ACCT-SETTL-AMT ( #INDEX ) ( EM = Z,ZZZ,ZZZ.99- ) TO P-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-LINE
                    sub_Print_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR02:                                                                                                                                                        //Natural: FOR #INDEX 1 10
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(10)); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Index).equals(0)))                                                             //Natural: IF PYMNT-DED-AMT ( #INDEX ) = 000
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet825 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF PYMNT-DED-CDE ( #INDEX );//Natural: VALUE 001
                if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(1))))
                {
                    decideConditionsMet825++;
                    pnd_Print_Line_P_Lit.setValue("Blue Cross    ");                                                                                                      //Natural: MOVE 'Blue Cross    ' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 002
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(2))))
                {
                    decideConditionsMet825++;
                    pnd_Print_Line_P_Lit.setValue("Long Term Care");                                                                                                      //Natural: MOVE 'Long Term Care' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 003
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(3))))
                {
                    decideConditionsMet825++;
                    pnd_Print_Line_P_Lit.setValue("Major Medical ");                                                                                                      //Natural: MOVE 'Major Medical ' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 004
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(4))))
                {
                    decideConditionsMet825++;
                    pnd_Print_Line_P_Lit.setValue("Group Life    ");                                                                                                      //Natural: MOVE 'Group Life    ' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 005
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(5))))
                {
                    decideConditionsMet825++;
                    pnd_Print_Line_P_Lit.setValue("Overpayment   ");                                                                                                      //Natural: MOVE 'Overpayment   ' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 006
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(6))))
                {
                    decideConditionsMet825++;
                    pnd_Print_Line_P_Lit.setValue("NYSUT         ");                                                                                                      //Natural: MOVE 'NYSUT         ' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 007
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(7))))
                {
                    decideConditionsMet825++;
                    //*  JH 6/20/2000
                    pnd_Print_Line_P_Lit.setValue("Personal Annty");                                                                                                      //Natural: MOVE 'Personal Annty' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 008
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(8))))
                {
                    decideConditionsMet825++;
                    //*  JH 6/20/2000
                    pnd_Print_Line_P_Lit.setValue("Mutual Fund   ");                                                                                                      //Natural: MOVE 'Mutual Fund   ' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 009
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(9))))
                {
                    decideConditionsMet825++;
                    //*  JH 6/20/2000
                    pnd_Print_Line_P_Lit.setValue("PA Select     ");                                                                                                      //Natural: MOVE 'PA Select     ' TO P-LIT
                }                                                                                                                                                         //Natural: VALUE 010
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(10))))
                {
                    decideConditionsMet825++;
                    pnd_Print_Line_P_Lit.setValue("Universal Life");                                                                                                      //Natural: MOVE 'Universal Life' TO P-LIT
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    pnd_Print_Line_P_Lit.setValue("Unknown Ded   ");                                                                                                      //Natural: MOVE 'Unknown Ded   ' TO P-LIT
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Print_Line_P_Amt.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Index),new ReportEditMask("Z,ZZZ,ZZZ.99-"));             //Natural: MOVE EDITED PYMNT-DED-AMT ( #INDEX ) ( EM = Z,ZZZ,ZZZ.99- ) TO P-AMT
                                                                                                                                                                          //Natural: PERFORM PRINT-LINE
                sub_Print_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).notEquals(getZero()))) //Natural: IF INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT ) NE 0
            {
                pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",                    //Natural: ASSIGN #TAX-AMT := INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT ) + 0
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
                pnd_Print_Line_P_Amt.setValueEdited(pnd_Tax_Amt,new ReportEditMask("Z,ZZZ,ZZZ.99-"));                                                                     //Natural: MOVE EDITED #TAX-AMT ( EM = Z,ZZZ,ZZZ.99- ) TO P-AMT
                pnd_Print_Line_P_Lit.setValue("Federal Tax   ");                                                                                                          //Natural: MOVE 'Federal Tax   ' TO P-LIT
                                                                                                                                                                          //Natural: PERFORM PRINT-LINE
                sub_Print_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).notEquals(getZero()))) //Natural: IF INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT ) NE 0
            {
                pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",                   //Natural: ASSIGN #TAX-AMT := INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT ) + 0
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
                pnd_Print_Line_P_Amt.setValueEdited(pnd_Tax_Amt,new ReportEditMask("Z,ZZZ,ZZZ.99-"));                                                                     //Natural: MOVE EDITED #TAX-AMT ( EM = Z,ZZZ,ZZZ.99- ) TO P-AMT
                pnd_Print_Line_P_Lit.setValue("State Tax     ");                                                                                                          //Natural: MOVE 'State Tax     ' TO P-LIT
                                                                                                                                                                          //Natural: PERFORM PRINT-LINE
                sub_Print_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).notEquals(getZero()))) //Natural: IF INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT ) NE 0
            {
                pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",                   //Natural: ASSIGN #TAX-AMT := INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT ) + 0
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
                pnd_Print_Line_P_Amt.setValueEdited(pnd_Tax_Amt,new ReportEditMask("Z,ZZZ,ZZZ.99-"));                                                                     //Natural: MOVE EDITED #TAX-AMT ( EM = Z,ZZZ,ZZZ.99- ) TO P-AMT
                pnd_Print_Line_P_Lit.setValue("Local Tax     ");                                                                                                          //Natural: MOVE 'Local Tax     ' TO P-LIT
                                                                                                                                                                          //Natural: PERFORM PRINT-LINE
                sub_Print_Line();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("K")))                                                                               //Natural: IF PYMNT-SUSPEND-CDE = 'K'
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-WORK-FILE2
                sub_Write_Work_File2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Print_Line_P_Pend_Lit.setValue("Pend Date:");                                                                                                         //Natural: MOVE 'Pend Date:' TO P-PEND-LIT
                pnd_Print_Line_P_Pend_Date.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte(),new ReportEditMask("MM/DD/YYYY"));                          //Natural: MOVE EDITED PYMNT-SUSPEND-DTE ( EM = MM/DD/YYYY ) TO P-PEND-DATE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Net_Amt.compute(new ComputeParameters(false, pnd_Net_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #NET-AMT := INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT ) + 0
            //*  MISSING N/A
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("1")))                                                                               //Natural: IF PYMNT-SUSPEND-CDE = '1'
            {
                pnd_Print_Line_2.reset();                                                                                                                                 //Natural: RESET #PRINT-LINE-2
                pnd_Print_Line_2_P_Net_Lit_2.setValue("Net Payment     ");                                                                                                //Natural: MOVE 'Net Payment     ' TO P-NET-LIT-2
                pnd_Print_Line_2_P_Net_Amt_2.setValueEdited(pnd_Net_Amt,new ReportEditMask("Z,ZZZ,ZZZ.99-"));                                                             //Natural: MOVE EDITED #NET-AMT ( EM = Z,ZZZ,ZZZ.99- ) TO P-NET-AMT-2
                getReports().write(2, pnd_Print_Line_2);                                                                                                                  //Natural: WRITE ( 02 ) #PRINT-LINE-2
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().skip(2, 1);                                                                                                                                  //Natural: SKIP ( 02 ) 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Print_Line_P_Net_Lit.setValue("Net Payment     ");                                                                                                    //Natural: MOVE 'Net Payment     ' TO P-NET-LIT
                pnd_Print_Line_P_Net_Amt.setValueEdited(pnd_Net_Amt,new ReportEditMask("Z,ZZZ,ZZZ.99-"));                                                                 //Natural: MOVE EDITED #NET-AMT ( EM = Z,ZZZ,ZZZ.99- ) TO P-NET-AMT
                if (condition(pnd_Name_Addr.getValue(pnd_Na_Index).equals(" ")))                                                                                          //Natural: IF #NAME-ADDR ( #NA-INDEX ) = ' '
                {
                    getReports().write(1, pnd_Print_Line);                                                                                                                //Natural: WRITE ( 01 ) #PRINT-LINE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Print_Line_P_Name_Addr.setValue(pnd_Name_Addr.getValue(pnd_Na_Index));                                                                            //Natural: MOVE #NAME-ADDR ( #NA-INDEX ) TO P-NAME-ADDR
                    //*  PREPARATION FOR NEXT N/A LINE
                    pnd_Na_Index.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NA-INDEX
                    getReports().write(1, pnd_Print_Line);                                                                                                                //Natural: WRITE ( 01 ) #PRINT-LINE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Print_Line.reset();                                                                                                                                       //Natural: RESET #PRINT-LINE
            //*  ----------------------------------------------------------------
            FOR03:                                                                                                                                                        //Natural: FOR #INDEX = 1 TO INV-ACCT-COUNT
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_Index).equals("M")))                                                  //Natural: IF INV-ACCT-VALUAT-PERIOD ( #INDEX ) = 'M'
                {
                    //*   MONTHLY
                    pnd_Ws_Valuat.setValue(2);                                                                                                                            //Natural: MOVE 2 TO #WS-VALUAT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*   ANNUAL
                    pnd_Ws_Valuat.setValue(1);                                                                                                                            //Natural: MOVE 1 TO #WS-VALUAT
                }                                                                                                                                                         //Natural: END-IF
                //*  -----------------------------
                //* *  EXAMINE FULL VALUE OF #WS-ACCT-CODE(*)
                //*                        01 02 03 04 05 06 07 08 09 10 11 12....20 21
                //*                        -----------------------------------------------
                //*  (OLD) #WS-ACCT-VALUE  T  TG G  R  C  B  M  S  W  L  E  I
                //*  (NEW) INV-ACCT        T  C  M  S  B  W  L  E  R  I           TG G
                //* *  EXAMINE FULL VALUE OF #INV-ACCT(*)     /* JH 6/21/2000 FCPL199A
                //* *          FOR INV-ACCT-CDE-ALPHA(#INDEX)
                //* *          GIVING INDEX #WS-NDX
                //* *  IF #WS-NDX = 1 THRU 3
                //* *     DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-INS-TYPE
                //* *       VALUE 'I'
                //* *          MOVE 2 TO #WS-NDX
                //* *       VALUE 'P'
                //* *          MOVE 3 TO #WS-NDX
                //* *       NONE
                //* *          MOVE 1 TO #WS-NDX
                //* *     END-DECIDE
                //* *  END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG")  //Natural: IF INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' OR = 'TG' OR = 'G '
                    || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G ")))
                {
                    //*  TIAA INSURANCE
                    //*  TIAA P.A.
                    //*  PA SELECT, SPIA
                    short decideConditionsMet937 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ANNTY-INS-TYPE;//Natural: VALUE 'I'
                    if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("I"))))
                    {
                        decideConditionsMet937++;
                        pnd_Ws_Ndx.setValue(2);                                                                                                                           //Natural: MOVE 02 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'P'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("P"))))
                    {
                        decideConditionsMet937++;
                        pnd_Ws_Ndx.setValue(3);                                                                                                                           //Natural: MOVE 03 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'S','M'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M"))))
                    {
                        decideConditionsMet937++;
                        ignore();
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Ws_Ndx.setValue(1);                                                                                                                           //Natural: MOVE 01 TO #WS-NDX
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //*  TIAA OR CREF
                if (condition(! (pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M")))) //Natural: IF NOT ( CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M' )
                {
                    //*  REAL ESTATE
                    //*  STOCK
                    //*  BOND
                    //*  MONEY MARKET
                    //*  SOCIAL CHOICE
                    //*  GLOBAL
                    //*  GROWTH
                    //*  EQUITY INDEX
                    //*  INFLATION LINKED BOND
                    short decideConditionsMet960 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF INV-ACCT-CDE-ALPHA ( #INDEX );//Natural: VALUE 'R'
                    if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("R"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(4);                                                                                                                           //Natural: MOVE 04 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'C'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("C"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(6);                                                                                                                           //Natural: MOVE 06 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'B'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("B"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(7);                                                                                                                           //Natural: MOVE 07 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'M'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("M"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(8);                                                                                                                           //Natural: MOVE 08 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'S'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("S"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(9);                                                                                                                           //Natural: MOVE 09 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'W'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("W"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(10);                                                                                                                          //Natural: MOVE 10 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'L'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("L"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(11);                                                                                                                          //Natural: MOVE 11 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'E'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("E"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(12);                                                                                                                          //Natural: MOVE 12 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'I'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("I"))))
                    {
                        decideConditionsMet960++;
                        pnd_Ws_Ndx.setValue(13);                                                                                                                          //Natural: MOVE 13 TO #WS-NDX
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  PA SELECT OR SPIA
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("D") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M"))) //Natural: IF CNTRCT-ANNTY-TYPE-CDE = 'D' OR = 'M'
                    {
                        //*  PA SELECT
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S")))                                                               //Natural: IF CNTRCT-ANNTY-INS-TYPE = 'S'
                        {
                            //*  FIXED FUND
                            //*  STOCK INDEX ACCOUNT
                            //*  GROWTH EQUITY
                            //*  GROWTH & INCOME
                            //*  INTERNATIONAL EQUITY
                            //*  SOCIAL CHOICE EQUITY
                            //*  LARGE-CAP VALUE
                            //*  SMALL-CAP VALUE
                            //*  REAT ESTATE SECURITIES
                            short decideConditionsMet996 = 0;                                                                                                             //Natural: DECIDE ON FIRST VALUE OF INV-ACCT-CDE-ALPHA ( #INDEX );//Natural: VALUE 'T'
                            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(16);                                                                                                                  //Natural: MOVE 16 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'V'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("V"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(17);                                                                                                                  //Natural: MOVE 17 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'N'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("N"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(18);                                                                                                                  //Natural: MOVE 18 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'O'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("O"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(19);                                                                                                                  //Natural: MOVE 19 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'P'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("P"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(20);                                                                                                                  //Natural: MOVE 20 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'Q'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("Q"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(21);                                                                                                                  //Natural: MOVE 21 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'J'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("J"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(22);                                                                                                                  //Natural: MOVE 22 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'K'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("K"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(23);                                                                                                                  //Natural: MOVE 23 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'X'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("X"))))
                            {
                                decideConditionsMet996++;
                                pnd_Ws_Ndx.setValue(24);                                                                                                                  //Natural: MOVE 24 TO #WS-NDX
                            }                                                                                                                                             //Natural: NONE
                            else if (condition())
                            {
                                ignore();
                            }                                                                                                                                             //Natural: END-DECIDE
                        }                                                                                                                                                 //Natural: END-IF
                        //*  SPIA
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M")))                                                               //Natural: IF CNTRCT-ANNTY-INS-TYPE = 'M'
                        {
                            //*  FIXED FUND
                            //*  GENERAL LEDGER ACCOUNT
                            //*  STOCK INDEX ACCOUNT
                            //*  GROWTH EQUITY
                            //*  GROWTH & INCOME
                            //*  INTERNATIONAL EQUITY
                            //*  SOCIAL CHOICE EQUITY
                            //*  LARGE-CAP VALUE
                            //*  SMALL-CAP VALUE
                            //*  REAT ESTATE SECURITIES
                            short decideConditionsMet1031 = 0;                                                                                                            //Natural: DECIDE ON FIRST VALUE OF INV-ACCT-CDE-ALPHA ( #INDEX );//Natural: VALUE 'T'
                            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(27);                                                                                                                  //Natural: MOVE 27 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'U'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("U"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(28);                                                                                                                  //Natural: MOVE 28 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'V'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("V"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(29);                                                                                                                  //Natural: MOVE 29 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'N'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("N"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(30);                                                                                                                  //Natural: MOVE 30 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'O'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("O"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(31);                                                                                                                  //Natural: MOVE 31 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'P'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("P"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(32);                                                                                                                  //Natural: MOVE 32 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'Q'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("Q"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(33);                                                                                                                  //Natural: MOVE 33 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'J'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("J"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(34);                                                                                                                  //Natural: MOVE 34 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'K'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("K"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(35);                                                                                                                  //Natural: MOVE 35 TO #WS-NDX
                            }                                                                                                                                             //Natural: VALUE 'X'
                            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("X"))))
                            {
                                decideConditionsMet1031++;
                                pnd_Ws_Ndx.setValue(36);                                                                                                                  //Natural: MOVE 36 TO #WS-NDX
                            }                                                                                                                                             //Natural: NONE
                            else if (condition())
                            {
                                ignore();
                            }                                                                                                                                             //Natural: END-DECIDE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ---------------------------------------
                //* *  IF #WS-NDX = 5 MOVE 6 TO #WS-NDX END-IF
                short decideConditionsMet1060 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #WS-NDX;//Natural: VALUE 1,2,3
                if (condition((pnd_Ws_Ndx.equals(1) || pnd_Ws_Ndx.equals(2) || pnd_Ws_Ndx.equals(3))))
                {
                    decideConditionsMet1060++;
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(1,5).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                  //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( 1,5 )
                }                                                                                                                                                         //Natural: VALUE 4
                else if (condition((pnd_Ws_Ndx.equals(4))))
                {
                    decideConditionsMet1060++;
                    //*  TIAA TOTAL
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,5).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));      //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,5 )
                }                                                                                                                                                         //Natural: VALUE 6,7,8,9,10,11,12,13
                else if (condition((pnd_Ws_Ndx.equals(6) || pnd_Ws_Ndx.equals(7) || pnd_Ws_Ndx.equals(8) || pnd_Ws_Ndx.equals(9) || pnd_Ws_Ndx.equals(10) 
                    || pnd_Ws_Ndx.equals(11) || pnd_Ws_Ndx.equals(12) || pnd_Ws_Ndx.equals(13))))
                {
                    decideConditionsMet1060++;
                    //*  CREF TOTAL
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,14).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,14 )
                }                                                                                                                                                         //Natural: VALUE 16,17,18,19,20,21,22,23,24
                else if (condition((pnd_Ws_Ndx.equals(16) || pnd_Ws_Ndx.equals(17) || pnd_Ws_Ndx.equals(18) || pnd_Ws_Ndx.equals(19) || pnd_Ws_Ndx.equals(20) 
                    || pnd_Ws_Ndx.equals(21) || pnd_Ws_Ndx.equals(22) || pnd_Ws_Ndx.equals(23) || pnd_Ws_Ndx.equals(24))))
                {
                    decideConditionsMet1060++;
                    //*  PA SELECT TOTAL
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,25).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,25 )
                }                                                                                                                                                         //Natural: VALUE 27,28,29,30,31,32,33,34,35,36
                else if (condition((pnd_Ws_Ndx.equals(27) || pnd_Ws_Ndx.equals(28) || pnd_Ws_Ndx.equals(29) || pnd_Ws_Ndx.equals(30) || pnd_Ws_Ndx.equals(31) 
                    || pnd_Ws_Ndx.equals(32) || pnd_Ws_Ndx.equals(33) || pnd_Ws_Ndx.equals(34) || pnd_Ws_Ndx.equals(35) || pnd_Ws_Ndx.equals(36))))
                {
                    decideConditionsMet1060++;
                    //*  SPIA TOTAL
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,37).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,37 )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //* *  IF INV-ACCT-CDE-ALPHA(#INDEX) = 'T ' OR = 'TG' OR = 'G '
                if (condition(pnd_Ws_Ndx.equals(1) || pnd_Ws_Ndx.equals(2) || pnd_Ws_Ndx.equals(3)))                                                                      //Natural: IF #WS-NDX = 01 OR = 02 OR = 03
                {
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(1,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));         //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( 1,#WS-NDX )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ---------------------------------------
            FOR04:                                                                                                                                                        //Natural: FOR #INDEX #NA-INDEX 10
            for (pnd_Index.setValue(pnd_Na_Index); condition(pnd_Index.lessOrEqual(10)); pnd_Index.nadd(1))
            {
                if (condition(pnd_Name_Addr.getValue(pnd_Index).equals(" ")))                                                                                             //Natural: IF #NAME-ADDR ( #INDEX ) = ' '
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                pnd_Print_Line_P_Name_Addr.setValue(pnd_Name_Addr.getValue(pnd_Index));                                                                                   //Natural: MOVE #NAME-ADDR ( #INDEX ) TO P-NAME-ADDR
                getReports().write(1, pnd_Print_Line);                                                                                                                    //Natural: WRITE ( 01 ) #PRINT-LINE
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Print_Line.reset();                                                                                                                                   //Natural: RESET #PRINT-LINE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 01 ) 1
            sort01Pymnt_Suspend_CdeOld.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde());                                                                     //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM BREAK-ROUTINE
            sub_Break_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        getWorkFiles().write(2, true, ";", ";", ";", ";", ";", ";", ";");                                                                                                 //Natural: WRITE WORK FILE 2 VARIABLE ';' ';' ';' ';' ';' ';' ';'
        getWorkFiles().write(2, true, ";", ";", ";", ";", ";", ";", pnd_Total_Wk2, ";");                                                                                  //Natural: WRITE WORK FILE 2 VARIABLE ';' ';' ';' ';' ';' ';' #TOTAL-WK2 ';'
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-ROUTINE
        //*    DECIDE ON FIRST VALUE OF #SUSPEND-CODE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-ROUTINE
        //*     COMPUTE #TIAA-AMT =
        //*       #TOTAL-AMT(1) + #TOTAL-AMT(2) + #TOTAL-AMT(21) + #TOTAL-AMT(22)
        //*     COMPUTE #CREF-AMT = #TOTAL-AMT(3:20) + 0
        //* *        FOR #WS-NDX #WS-NDX  14
        //* *        FOR #WS-NDX = #WS-NDX TO 31
        //* *        FOR #WS-NDX #WS-NDX 14
        //* *        FOR #WS-NDX = #WS-NDX TO 31
        //* *          IF #WS-NDX = 15 OR = 23
        //* *        + #WS-AMOUNT-ACC(#WS-PERIOD,22)
        //* *        + #WS-AMOUNT-ACC(#WS-PERIOD,31)
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-LINE
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE2
        //* *************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-WORK-FILE2-HEADING
    }
    private void sub_Initialize_Routine() throws Exception                                                                                                                //Natural: INITIALIZE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1128 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CMBN-CNTRCT-BREAK
        if (condition(cmbn_Cntrct_Break.getBoolean()))
        {
            decideConditionsMet1128++;
            cmbn_Cntrct_Break.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO CMBN-CNTRCT-BREAK
            pnd_Check_Amt.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt());                                                                                    //Natural: MOVE PYMNT-CHECK-AMT TO #CHECK-AMT
            pnd_Na_Index.reset();                                                                                                                                         //Natural: RESET #NA-INDEX #NAME-ADDR ( * )
            pnd_Name_Addr.getValue("*").reset();
            //*  MISSING N/A
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().notEquals("1")))                                                                            //Natural: IF PYMNT-SUSPEND-CDE NE '1'
            {
                pnd_Name_Addr.getValue(1).setValue(DbsUtil.compress("ANNT:", pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name_1(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Middle_Name_1(),  //Natural: COMPRESS 'ANNT:' PH-FIRST-NAME-1 PH-MIDDLE-NAME-1 PH-LAST-NAME INTO #NAME-ADDR ( 1 )
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name()));
                pnd_Name_Addr.getValue(2).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1));                                                              //Natural: MOVE PYMNT-NME ( 1 ) TO #NAME-ADDR ( 2 )
                pnd_Na_Index.setValue(2);                                                                                                                                 //Natural: MOVE 2 TO #NA-INDEX
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(1).notEquals(" ")))                                                         //Natural: IF PYMNT-ADDR-LINE1-TXT ( 1 ) NE ' '
                {
                    pnd_Na_Index.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NA-INDEX
                    pnd_Name_Addr.getValue(pnd_Na_Index).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(1));                                    //Natural: MOVE PYMNT-ADDR-LINE1-TXT ( 1 ) TO #NAME-ADDR ( #NA-INDEX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(1).notEquals(" ")))                                                         //Natural: IF PYMNT-ADDR-LINE2-TXT ( 1 ) NE ' '
                {
                    pnd_Na_Index.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NA-INDEX
                    pnd_Name_Addr.getValue(pnd_Na_Index).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(1));                                    //Natural: MOVE PYMNT-ADDR-LINE2-TXT ( 1 ) TO #NAME-ADDR ( #NA-INDEX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(1).notEquals(" ")))                                                         //Natural: IF PYMNT-ADDR-LINE3-TXT ( 1 ) NE ' '
                {
                    pnd_Na_Index.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NA-INDEX
                    pnd_Name_Addr.getValue(pnd_Na_Index).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(1));                                    //Natural: MOVE PYMNT-ADDR-LINE3-TXT ( 1 ) TO #NAME-ADDR ( #NA-INDEX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(1).notEquals(" ")))                                                         //Natural: IF PYMNT-ADDR-LINE4-TXT ( 1 ) NE ' '
                {
                    pnd_Na_Index.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NA-INDEX
                    pnd_Name_Addr.getValue(pnd_Na_Index).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(1));                                    //Natural: MOVE PYMNT-ADDR-LINE4-TXT ( 1 ) TO #NAME-ADDR ( #NA-INDEX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(1).notEquals(" ")))                                                         //Natural: IF PYMNT-ADDR-LINE5-TXT ( 1 ) NE ' '
                {
                    pnd_Na_Index.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NA-INDEX
                    pnd_Name_Addr.getValue(pnd_Na_Index).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(1));                                    //Natural: MOVE PYMNT-ADDR-LINE5-TXT ( 1 ) TO #NAME-ADDR ( #NA-INDEX )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(1).notEquals(" ")))                                                         //Natural: IF PYMNT-ADDR-LINE6-TXT ( 1 ) NE ' '
                {
                    pnd_Na_Index.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #NA-INDEX
                    pnd_Name_Addr.getValue(pnd_Na_Index).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(1));                                    //Natural: MOVE PYMNT-ADDR-LINE6-TXT ( 1 ) TO #NAME-ADDR ( #NA-INDEX )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN PEND-CODE-BREAK
        if (condition(pend_Code_Break.getBoolean()))
        {
            decideConditionsMet1128++;
            pend_Code_Break.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO PEND-CODE-BREAK
            //*     RESET #TOTAL-AMT(*)
            //*    MOVE PYMNT-SUSPEND-CDE TO #SUSPEND-CODE
            short decideConditionsMet1167 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF PYMNT-SUSPEND-CDE;//Natural: VALUE 'A'
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("A"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Machine Generated Death       ");                                                                                               //Natural: MOVE 'Machine Generated Death       ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE 'C'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("C"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Pended Death Manual           ");                                                                                               //Natural: MOVE 'Pended Death Manual           ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE 'G'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("G"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Payee Change                  ");                                                                                               //Natural: MOVE 'Payee Change                  ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("I"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Blocked Account               ");                                                                                               //Natural: MOVE 'Blocked Account               ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE 'K'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("K"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Unclaimed Sums                ");                                                                                               //Natural: MOVE 'Unclaimed Sums                ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE 'M'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("M"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Miscellaneous                 ");                                                                                               //Natural: MOVE 'Miscellaneous                 ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("R"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Contract Expired              ");                                                                                               //Natural: MOVE 'Contract Expired              ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("S"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Self Remitter - TPA to Cash   ");                                                                                               //Natural: MOVE 'Self Remitter - TPA to Cash   ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE '1'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("1"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Missing Name and Address      ");                                                                                               //Natural: MOVE 'Missing Name and Address      ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("2"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Invalid Combine Number        ");                                                                                               //Natural: MOVE 'Invalid Combine Number        ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("3"))))
            {
                decideConditionsMet1167++;
                pnd_Suspend_Lit.setValue("Invalid PIN Number            ");                                                                                               //Natural: MOVE 'Invalid PIN Number            ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                pnd_Suspend_Lit.setValue("Unknown Suspend Code          ");                                                                                               //Natural: MOVE 'Unknown Suspend Code          ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1128 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  INITIALIZE-ROUTINE
    }
    private void sub_Break_Routine() throws Exception                                                                                                                     //Natural: BREAK-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1199 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN CMBN-CNTRCT-BREAK
        if (condition(cmbn_Cntrct_Break.getBoolean()))
        {
            decideConditionsMet1199++;
            //*  MISSING NAME&ADDRESS
            if (condition(pnd_Suspend_Code.equals("1")))                                                                                                                  //Natural: IF #SUSPEND-CODE = '1'
            {
                //*        PYMNT-SUSPEND-CDE NE '2'
                //*    IF #OLD-SUSPEND-CODE = '1'                       /* JB01
                getReports().skip(2, 5);                                                                                                                                  //Natural: SKIP ( 02 ) 5
                getReports().write(2, NEWLINE,"-",new RepeatItem(82),"Total Payment   ",pnd_Check_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"**********");              //Natural: WRITE ( 02 ) / '-' ( 82 ) 'Total Payment   ' #CHECK-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) '**********'
                if (Global.isEscape()) return;
                getReports().skip(2, 1);                                                                                                                                  //Natural: SKIP ( 02 ) 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, NEWLINE,"-",new RepeatItem(75),"Total Payment   ",pnd_Check_Amt, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"**********");              //Natural: WRITE ( 01 ) / '-' ( 75 ) 'Total Payment   ' #CHECK-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) '**********'
                if (Global.isEscape()) return;
                getReports().skip(1, 1);                                                                                                                                  //Natural: SKIP ( 01 ) 1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Check_Amt.reset();                                                                                                                                        //Natural: RESET #CHECK-AMT
        }                                                                                                                                                                 //Natural: WHEN PEND-CODE-BREAK
        if (condition(pend_Code_Break.getBoolean()))
        {
            decideConditionsMet1199++;
            FOR05:                                                                                                                                                        //Natural: FOR #WS-PERIOD 1 2
            for (pnd_Ws_Period.setValue(1); condition(pnd_Ws_Period.lessOrEqual(2)); pnd_Ws_Period.nadd(1))
            {
                if (condition(pnd_Ws_Period.equals(2)))                                                                                                                   //Natural: IF #WS-PERIOD = 2
                {
                    pnd_Mon_Ann_Desc.setValue("MONTHLY");                                                                                                                 //Natural: MOVE 'MONTHLY' TO #MON-ANN-DESC
                    //* *        MOVE 'Monthly Total     ' TO #WS-DESC-TABLE(16)
                    //* *        MOVE 'MONTHLY TOTAL     ' TO #WS-DESC-TABLE(32) /* RCC 6/12/02
                    //*  RCC
                    pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(38).setValue("MONTHLY TOTAL     ");                                                                     //Natural: MOVE 'MONTHLY TOTAL     ' TO #WS-DESC-TABLE ( 38 )
                    pnd_Ws_Ndx.setValue(4);                                                                                                                               //Natural: MOVE 4 TO #WS-NDX
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Mon_Ann_Desc.setValue("ANNUAL");                                                                                                                  //Natural: MOVE 'ANNUAL' TO #MON-ANN-DESC
                    //* *        MOVE 'Annual Total      ' TO #WS-DESC-TABLE(16)
                    //* *        MOVE 'ANNUAL TOTAL      ' TO #WS-DESC-TABLE(32) /* RCC 6/12/02
                    //*  RCC
                    pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(38).setValue("ANNUAL TOTAL      ");                                                                     //Natural: MOVE 'ANNUAL TOTAL      ' TO #WS-DESC-TABLE ( 38 )
                    pnd_Ws_Ndx.setValue(1);                                                                                                                               //Natural: MOVE 1 TO #WS-NDX
                }                                                                                                                                                         //Natural: END-IF
                //*       IF #SUSPEND-CODE = '1'        /* MISSING N/A
                if (condition(pnd_Old_Suspend_Code.equals("1")))                                                                                                          //Natural: IF #OLD-SUSPEND-CODE = '1'
                {
                    //*        IF  PYMNT-SUSPEND-CDE NE '2'
                    //*  JB01
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().setValue(pnd_Old_Suspend_Code);                                                                   //Natural: MOVE #OLD-SUSPEND-CODE TO PYMNT-SUSPEND-CDE
                    //*        END-IF
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 02 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  RCC 6/12/2002
                    getReports().write(2, NEWLINE,NEWLINE,new TabSetting(84),pnd_Mon_Ann_Desc,NEWLINE);                                                                   //Natural: WRITE ( 02 ) // 84T #MON-ANN-DESC /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR06:                                                                                                                                                //Natural: FOR #WS-NDX = #WS-NDX TO 37
                    for (pnd_Ws_Ndx.setValue(pnd_Ws_Ndx); condition(pnd_Ws_Ndx.lessOrEqual(37)); pnd_Ws_Ndx.nadd(1))
                    {
                        if (condition(pnd_Ws_Ndx.equals(15) || pnd_Ws_Ndx.equals(23)))                                                                                    //Natural: IF #WS-NDX = 15 OR = 23
                        {
                            getReports().write(2, new TabSetting(74),pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(pnd_Ws_Ndx));                                          //Natural: WRITE ( 02 ) 74T #WS-DESC-TABLE ( #WS-NDX )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(2, new TabSetting(74),pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(pnd_Ws_Ndx),pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,pnd_Ws_Ndx),  //Natural: WRITE ( 02 ) 74T #WS-DESC-TABLE ( #WS-NDX ) #WS-AMOUNT-ACC ( #WS-PERIOD,#WS-NDX ) ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  JH
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Mon_Ann_Tot.compute(new ComputeParameters(false, pnd_Ws_Mon_Ann_Tot), pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,               //Natural: ASSIGN #WS-MON-ANN-TOT := #WS-AMOUNT-ACC ( #WS-PERIOD,5 ) + #WS-AMOUNT-ACC ( #WS-PERIOD,14 ) + #WS-AMOUNT-ACC ( #WS-PERIOD,25 ) + #WS-AMOUNT-ACC ( #WS-PERIOD,37 )
                        5).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,14)).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,
                        25)).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,37)));
                    //* *        + #WS-AMOUNT-ACC(#WS-PERIOD,22)  /* RCC
                    //* *        + #WS-AMOUNT-ACC(#WS-PERIOD,31)  /**
                    //* *        WRITE (02) /// 74T #WS-DESC-TABLE(16)
                    //*  RCC
                    getReports().write(2, NEWLINE,new TabSetting(74),pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(32),pnd_Ws_Mon_Ann_Tot, new ReportEditMask             //Natural: WRITE ( 02 ) / 74T #WS-DESC-TABLE ( 32 ) #WS-MON-ANN-TOT ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                        ("Z,ZZZ,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Grand_Tot.nadd(pnd_Ws_Mon_Ann_Tot);                                                                                                            //Natural: ADD #WS-MON-ANN-TOT TO #WS-GRAND-TOT
                    pnd_Ws_Mon_Ann_Tot.reset();                                                                                                                           //Natural: RESET #WS-MON-ANN-TOT
                    if (condition(pnd_Ws_Period.equals(2)))                                                                                                               //Natural: IF #WS-PERIOD = 2
                    {
                        //* *           WRITE (02) /// 74T #WS-DESC-TABLE(15)
                        //*  RCC
                        getReports().write(2, NEWLINE,new TabSetting(74),pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(39),pnd_Ws_Grand_Tot, new ReportEditMask           //Natural: WRITE ( 02 ) / 74T #WS-DESC-TABLE ( 39 ) #WS-GRAND-TOT ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                            ("Z,ZZZ,ZZZ,ZZ9.99-"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  JB01
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().setValue(pnd_Suspend_Code);                                                                       //Natural: MOVE #SUSPEND-CODE TO PYMNT-SUSPEND-CDE
                    getReports().newPage(new ReportSpecification(2));                                                                                                     //Natural: NEWPAGE ( 02 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*        IF  PYMNT-SUSPEND-CDE NE '2'
                    //*  JB01
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().setValue(pnd_Old_Suspend_Code);                                                                   //Natural: MOVE #OLD-SUSPEND-CODE TO PYMNT-SUSPEND-CDE
                    //*        END-IF
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 01 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  RCC 06/12/02
                    getReports().write(1, NEWLINE,NEWLINE,new TabSetting(84),pnd_Mon_Ann_Desc,NEWLINE);                                                                   //Natural: WRITE ( 01 ) // 84T #MON-ANN-DESC /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR07:                                                                                                                                                //Natural: FOR #WS-NDX = #WS-NDX TO 37
                    for (pnd_Ws_Ndx.setValue(pnd_Ws_Ndx); condition(pnd_Ws_Ndx.lessOrEqual(37)); pnd_Ws_Ndx.nadd(1))
                    {
                        //*  RCC 06/12/02
                        if (condition(pnd_Ws_Ndx.equals(15) || pnd_Ws_Ndx.equals(26)))                                                                                    //Natural: IF #WS-NDX = 15 OR = 26
                        {
                            getReports().write(1, new TabSetting(74),pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(pnd_Ws_Ndx));                                          //Natural: WRITE ( 01 ) 74T #WS-DESC-TABLE ( #WS-NDX )
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        getReports().write(1, new TabSetting(74),pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(pnd_Ws_Ndx),pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,pnd_Ws_Ndx),  //Natural: WRITE ( 01 ) 74T #WS-DESC-TABLE ( #WS-NDX ) #WS-AMOUNT-ACC ( #WS-PERIOD,#WS-NDX ) ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  RCC 06/12/02
                        //*  RCC 06/12/02
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Mon_Ann_Tot.compute(new ComputeParameters(false, pnd_Ws_Mon_Ann_Tot), pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,               //Natural: ASSIGN #WS-MON-ANN-TOT := #WS-AMOUNT-ACC ( #WS-PERIOD,5 ) + #WS-AMOUNT-ACC ( #WS-PERIOD,14 ) + #WS-AMOUNT-ACC ( #WS-PERIOD,25 ) + #WS-AMOUNT-ACC ( #WS-PERIOD,37 )
                        5).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,14)).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,
                        25)).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Period,37)));
                    //* *        WRITE (01) /// 74T #WS-DESC-TABLE(16)
                    //* *        WRITE (01) /   74T #WS-DESC-TABLE(32)       /* RCC 06/12/02
                    getReports().write(1, NEWLINE,new TabSetting(74),pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(38),pnd_Ws_Mon_Ann_Tot, new ReportEditMask             //Natural: WRITE ( 01 ) / 74T #WS-DESC-TABLE ( 38 ) #WS-MON-ANN-TOT ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                        ("Z,ZZZ,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Grand_Tot.nadd(pnd_Ws_Mon_Ann_Tot);                                                                                                            //Natural: ADD #WS-MON-ANN-TOT TO #WS-GRAND-TOT
                    pnd_Ws_Mon_Ann_Tot.reset();                                                                                                                           //Natural: RESET #WS-MON-ANN-TOT
                    if (condition(pnd_Ws_Period.equals(2)))                                                                                                               //Natural: IF #WS-PERIOD = 2
                    {
                        //* *           WRITE (01) /// 74T #WS-DESC-TABLE(15)
                        //* *           WRITE (01) /// 74T #WS-DESC-TABLE(33)  /* RCC 06/12/02
                        getReports().write(1, NEWLINE,new TabSetting(74),pnd_Ws_Description_Pnd_Ws_Desc_Table.getValue(39),pnd_Ws_Grand_Tot, new ReportEditMask           //Natural: WRITE ( 01 ) / 74T #WS-DESC-TABLE ( 39 ) #WS-GRAND-TOT ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                            ("Z,ZZZ,ZZZ,ZZ9.99-"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    //*  JB01
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().setValue(pnd_Suspend_Code);                                                                       //Natural: MOVE #SUSPEND-CODE TO PYMNT-SUSPEND-CDE
                    getReports().newPage(new ReportSpecification(1));                                                                                                     //Natural: NEWPAGE ( 01 )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pnd_Mon_Ann_Desc.reset();                                                                                                                                     //Natural: RESET #MON-ANN-DESC #WS-GRAND-TOT #WS-AMOUNT-ACC ( *,* ) #WS-MON-ANN-TOT
            pnd_Ws_Grand_Tot.reset();
            pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue("*","*").reset();
            pnd_Ws_Mon_Ann_Tot.reset();
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1199 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  BREAK-ROUTINE
    }
    private void sub_Print_Line() throws Exception                                                                                                                        //Natural: PRINT-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  MISSING NAME&ADDRESS
        if (condition(pnd_Suspend_Code.equals("1")))                                                                                                                      //Natural: IF #SUSPEND-CODE = '1'
        {
            pnd_Print_Line_2_P_Combine_Nbr_2.setValue(pnd_Print_Line_P_Combine_Nbr);                                                                                      //Natural: MOVE P-COMBINE-NBR TO P-COMBINE-NBR-2
            pnd_Print_Line_2_P_Cntrct_Nbr_2.setValue(pnd_Print_Line_P_Cntrct_Nbr);                                                                                        //Natural: MOVE P-CNTRCT-NBR TO P-CNTRCT-NBR-2
            pnd_Print_Line_2_P_Ss_Nbr_2.setValue(pnd_Print_Line_P_Ss_Nbr);                                                                                                //Natural: MOVE P-SS-NBR TO P-SS-NBR-2
            pnd_Print_Line_2_P_Option_2.setValue(pnd_Print_Line_P_Option);                                                                                                //Natural: MOVE P-OPTION TO P-OPTION-2
            pnd_Print_Line_2_P_Ann_Ins_2.setValue(pnd_Print_Line_P_Ann_Ins);                                                                                              //Natural: MOVE P-ANN-INS TO P-ANN-INS-2
            pnd_Print_Line_2_P_Cntrct_Type_2.setValue(pnd_Print_Line_P_Cntrct_Type);                                                                                      //Natural: MOVE P-CNTRCT-TYPE TO P-CNTRCT-TYPE-2
            pnd_Print_Line_2_P_Prod_Line_2.setValue(pnd_Print_Line_P_Prod_Line);                                                                                          //Natural: MOVE P-PROD-LINE TO P-PROD-LINE-2
            pnd_Print_Line_2_P_Life_Cont_2.setValue(pnd_Print_Line_P_Life_Cont);                                                                                          //Natural: MOVE P-LIFE-CONT TO P-LIFE-CONT-2
            pnd_Print_Line_2_P_Fund_2.setValue(pnd_Print_Line_P_Fund);                                                                                                    //Natural: MOVE P-FUND TO P-FUND-2
            pnd_Print_Line_2_P_Lit_2.setValue(pnd_Print_Line_P_Lit);                                                                                                      //Natural: MOVE P-LIT TO P-LIT-2
            pnd_Print_Line_2_P_Amt_2.setValue(pnd_Print_Line_P_Amt);                                                                                                      //Natural: MOVE P-AMT TO P-AMT-2
            getReports().write(2, pnd_Print_Line_2);                                                                                                                      //Natural: WRITE ( 02 ) #PRINT-LINE-2
            if (Global.isEscape()) return;
            pnd_Print_Line.reset();                                                                                                                                       //Natural: RESET #PRINT-LINE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Name_Addr.getValue(pnd_Na_Index).notEquals(" ")))                                                                                               //Natural: IF #NAME-ADDR ( #NA-INDEX ) NE ' '
        {
            pnd_Print_Line_P_Name_Addr.setValue(pnd_Name_Addr.getValue(pnd_Na_Index));                                                                                    //Natural: MOVE #NAME-ADDR ( #NA-INDEX ) TO P-NAME-ADDR
            //*  PREPARATION FOR NEXT N/A LINE
            pnd_Na_Index.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #NA-INDEX
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, pnd_Print_Line);                                                                                                                            //Natural: WRITE ( 01 ) #PRINT-LINE
        if (Global.isEscape()) return;
        pnd_Print_Line.reset();                                                                                                                                           //Natural: RESET #PRINT-LINE
        //*  PRINT-LINE
    }
    private void sub_Write_Work_File2() throws Exception                                                                                                                  //Natural: WRITE-WORK-FILE2
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Sub_No.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr().getSubstring(11,4));                                                                        //Natural: MOVE SUBSTR ( CNTRCT-CMBN-NBR,11,4 ) TO #SUB-NO
        pnd_Pymnt_Suspend_Dte_E.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Dte(),new ReportEditMask("MM/DD/YYYY"));                                     //Natural: MOVE EDITED PYMNT-SUSPEND-DTE ( EM = MM/DD/YYYY ) TO #PYMNT-SUSPEND-DTE-E
        pnd_Pymnt_Check_Dte_E.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                         //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #PYMNT-CHECK-DTE-E
        pnd_Pymnt_Check_Amt_E.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(),new ReportEditMask("9999999.99"));                                         //Natural: MOVE EDITED PYMNT-CHECK-AMT ( EM = 9999999.99 ) TO #PYMNT-CHECK-AMT-E
        getWorkFiles().write(2, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr(), ";", pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), ";",                   //Natural: WRITE WORK FILE 2 VARIABLE CNTRCT-UNQ-ID-NBR ';' CNTRCT-PPCN-NBR ';' #SUB-NO ';' CNTRCT-OPTION-CDE ';' #PYMNT-CHECK-DTE-E ';' #PYMNT-CHECK-AMT-E ';'
            pnd_Sub_No, ";", pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde(), ";", pnd_Pymnt_Check_Dte_E, ";", pnd_Pymnt_Check_Amt_E, ";");
        pnd_Total_Wk2.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt());                                                                                            //Natural: ADD PYMNT-CHECK-AMT TO #TOTAL-WK2
    }
    private void sub_Write_Work_File2_Heading() throws Exception                                                                                                          //Natural: WRITE-WORK-FILE2-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************************
        getWorkFiles().write(2, true, "    Pin  ", ";", "Contract", ";", "Beneficiary", ";", "Option ", ";", "   Check", ";", "    Check");                               //Natural: WRITE WORK FILE 2 VARIABLE '    Pin  ' ';' 'Contract' ';' 'Beneficiary' ';' 'Option ' ';' '   Check' ';' '    Check'
        getWorkFiles().write(2, true, "  Number", ";", " Number ", ";", "   Code ", ";", "  Code ", ";", "    Date", ";", "   Amount ");                                  //Natural: WRITE WORK FILE 2 VARIABLE '  Number' ';' ' Number ' ';' '   Code ' ';' '  Code ' ';' '    Date' ';' '   Amount '
        getWorkFiles().write(2, true, ";", ";", ";");                                                                                                                     //Natural: WRITE WORK FILE 2 VARIABLE ';' ';' ';'
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pdaFcpa800_getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_NbrIsBreak = pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr().isBreak(endOfData);
        boolean pdaFcpa800_getWf_Pymnt_Addr_Grp_Pymnt_Suspend_CdeIsBreak = pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().isBreak(endOfData);
        if (condition(pdaFcpa800_getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_NbrIsBreak || pdaFcpa800_getWf_Pymnt_Addr_Grp_Pymnt_Suspend_CdeIsBreak))
        {
            cmbn_Cntrct_Break.setValue(true);                                                                                                                             //Natural: MOVE TRUE TO CMBN-CNTRCT-BREAK
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpa800_getWf_Pymnt_Addr_Grp_Pymnt_Suspend_CdeIsBreak))
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("2")))                                                                               //Natural: IF PYMNT-SUSPEND-CDE = '2'
            {
                pnd_Suspend_Lit.setValue("INVALID COMBINE NUMBER        ");                                                                                               //Natural: MOVE 'INVALID COMBINE NUMBER        ' TO #SUSPEND-LIT
            }                                                                                                                                                             //Natural: END-IF
            pend_Code_Break.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO PEND-CODE-BREAK
            pnd_Suspend_Code.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde());                                                                               //Natural: MOVE PYMNT-SUSPEND-CDE TO #SUSPEND-CODE
            pnd_Old_Suspend_Code.setValue(sort01Pymnt_Suspend_CdeOld);                                                                                                    //Natural: MOVE OLD ( PYMNT-SUSPEND-CDE ) TO #OLD-SUSPEND-CODE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"1",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(46),"PENDING PAYMENTS FOR",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE,"Pend Code:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde(),"   ",pnd_Suspend_Lit,
            NEWLINE,"=",NEWLINE,"                                          Ann/ Cntr ","Prod  Life",NEWLINE,"Combine Nbr   Contract#  Soc Sec No  Opt  Ins Type ",
            "Type  Cont.",NEWLINE,"=",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"2",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(48),"PENDING PAYMENTS FOR",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE,"Pend Code: 1  Missing Name and Address",NEWLINE,"=",NEWLINE,"                                            Ann/ Cntr ",
            "Prod  Life",NEWLINE,"Combine Nbr     Contract#  Soc Sec No  Opt  Ins Type ","Type  Cont.",NEWLINE,"=",NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData702() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-ROUTINE
            sub_Initialize_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
