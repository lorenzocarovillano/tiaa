/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:19 PM
**        * FROM NATURAL PROGRAM : Fcpp885
************************************************************
**        * FILE NAME            : Fcpp885.java
**        * CLASS NAME           : Fcpp885
**        * INSTANCE NAME        : Fcpp885
************************************************************
************************************************************************
* PROGRAM   : FCPP885
* SYSTEM    : CPS
* TITLE     : PAYMENT REGISTERS FOR TREASURY DIVISION
* GENERATED :
* FUNCTION  : GENERATE DC/SS/MDO/NZ PAYMENT REGISTERS FOR TREASURY DIV.
*           :
* HISTORY   :
* 08/31/99  : A. YOUNG - ADDED CSR CODE TRANSLATION TO DISPLAY.
* 05/18/98  : A. YOUNG - PROVIDE FOR THE PROCESSING OF ANNUITY LOAN (AL)
*           :            AND IRA (CLASSIC, ROTH) RECORDS.  SECOND REG.
*           :            PRINTED FOR INTERNAL ROLLOVERS ONLY.
* 12/14/99  : R. CARREON NEW LOCAL FOR NZ/AL
*  2/06/01  : T. TRAINOR INTERPRET 'CN' & 'SN' AS WELL AS C, S, AND R
*  2/16/01  : T. TRAINOR IF CSR ACT FLD IS NOT C, S, R, CN, OR SN
*                        LEAVE IT AS IS (V1.02)
*  4/07/03  : R. CARREON TPA EGTRRA.  INCLUDE LOB COUNT IN NZ ROLLOVER
*           :            REPORT.
* 06/6/2006 : LANDRUM PAYEE MATCH.
*           - INCREASED CHECK NBR FROM N7 TO N10. FORMAT FOR REPORTS
* 4/2017   : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp885 extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl190a ldaFcpl190a;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Vers_Id;

    private DbsGroup pnd_Common_Fields;
    private DbsField pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Common_Fields_Pnd_Pymnt_Check_Nbr;
    private DbsField pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde;
    private DbsField pnd_Common_Fields_Pnd_Pymnt_Nme;
    private DbsField pnd_Common_Fields_Pnd_Pymnt_Check_Dte;
    private DbsField pnd_Common_Fields_Pnd_Pymnt_Check_Amt;
    private DbsField pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Common_Fields_Pnd_Cntrct_Lob_Cde;
    private DbsField pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind;
    private DbsField pnd_Common_Fields_Pnd_Csr_Activity_Cde;
    private DbsField pnd_Index;
    private DbsField i;
    private DbsField pnd_Total_Lit;

    private DbsGroup totals;
    private DbsField totals_Pnd_T_Net;
    private DbsField totals_Pnd_T_Count;
    private DbsField totals_Pnd_Sub_Net;
    private DbsField totals_Pnd_Sub_Count;
    private DbsField pnd_Pymnt_Nbr;

    private DbsGroup pnd_Pymnt_Nbr__R_Field_1;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Type;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Num;

    private DbsGroup pnd_Pymnt_Nbr__R_Field_2;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7;
    private DbsField pnd_Check_Datex;

    private DbsGroup pnd_Check_Datex__R_Field_3;
    private DbsField pnd_Check_Datex_Pnd_Check_Date;
    private DbsField pnd_Input_Parm;

    private DbsGroup pnd_Input_Parm__R_Field_4;
    private DbsField pnd_Input_Parm_Pnd_Input_Orgn_Cde;
    private DbsField pnd_Input_Parm_Pnd_Input_Hdr;
    private DbsField pnd_System_Desc;
    private DbsField pnd_Start_Index;
    private DbsField pnd_Hdr_Desc;
    private DbsField pnd_Hdr_Dte;
    private DbsField pnd_Eom;
    private DbsField pnd_Int_Roll_Mode;
    private DbsField pnd_Sub_Print;
    private DbsField pnd_Cf_Txt;
    private DbsField pnd_Ws_Save_Ftre_Ind;
    private DbsField pnd_Ws_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Roll_Dest_Cde;
    private DbsField pnd_First_Pass;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Vers_Id = localVariables.newFieldInRecord("pnd_Vers_Id", "#VERS_ID", FieldType.STRING, 6);

        pnd_Common_Fields = localVariables.newGroupInRecord("pnd_Common_Fields", "#COMMON-FIELDS");
        pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Common_Fields_Pnd_Pymnt_Check_Nbr = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Pymnt_Check_Nbr", "#PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde", "#ANNT-RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Common_Fields_Pnd_Pymnt_Nme = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Pymnt_Nme", "#PYMNT-NME", FieldType.STRING, 38);
        pnd_Common_Fields_Pnd_Pymnt_Check_Dte = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Common_Fields_Pnd_Pymnt_Check_Amt = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Pymnt_Check_Amt", "#PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 
            3);
        pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind", "#PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Common_Fields_Pnd_Cntrct_Lob_Cde = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Cntrct_Lob_Cde", "#CNTRCT-LOB-CDE", FieldType.STRING, 
            4);
        pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind", "#PYMNT-FTRE-IND", FieldType.STRING, 
            1);
        pnd_Common_Fields_Pnd_Csr_Activity_Cde = pnd_Common_Fields.newFieldInGroup("pnd_Common_Fields_Pnd_Csr_Activity_Cde", "#CSR-ACTIVITY-CDE", FieldType.STRING, 
            9);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        i = localVariables.newFieldInRecord("i", "I", FieldType.PACKED_DECIMAL, 2);
        pnd_Total_Lit = localVariables.newFieldInRecord("pnd_Total_Lit", "#TOTAL-LIT", FieldType.STRING, 25);

        totals = localVariables.newGroupArrayInRecord("totals", "TOTALS", new DbsArrayController(1, 5));
        totals_Pnd_T_Net = totals.newFieldArrayInGroup("totals_Pnd_T_Net", "#T-NET", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 8));
        totals_Pnd_T_Count = totals.newFieldArrayInGroup("totals_Pnd_T_Count", "#T-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 8));
        totals_Pnd_Sub_Net = totals.newFieldArrayInGroup("totals_Pnd_Sub_Net", "#SUB-NET", FieldType.PACKED_DECIMAL, 11, 2, new DbsArrayController(1, 8));
        totals_Pnd_Sub_Count = totals.newFieldArrayInGroup("totals_Pnd_Sub_Count", "#SUB-COUNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            8));
        pnd_Pymnt_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Nbr", "#PYMNT-NBR", FieldType.STRING, 11);

        pnd_Pymnt_Nbr__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_Nbr__R_Field_1", "REDEFINE", pnd_Pymnt_Nbr);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Type = pnd_Pymnt_Nbr__R_Field_1.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Type", "#PYMNT-TYPE", FieldType.STRING, 1);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num = pnd_Pymnt_Nbr__R_Field_1.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Num", "#PYMNT-NUM", FieldType.NUMERIC, 10);

        pnd_Pymnt_Nbr__R_Field_2 = pnd_Pymnt_Nbr__R_Field_1.newGroupInGroup("pnd_Pymnt_Nbr__R_Field_2", "REDEFINE", pnd_Pymnt_Nbr_Pnd_Pymnt_Num);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3 = pnd_Pymnt_Nbr__R_Field_2.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3", "#PYMNT-NUM-N3", FieldType.NUMERIC, 
            3);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7 = pnd_Pymnt_Nbr__R_Field_2.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7", "#PYMNT-NUM-N7", FieldType.NUMERIC, 
            7);
        pnd_Check_Datex = localVariables.newFieldInRecord("pnd_Check_Datex", "#CHECK-DATEX", FieldType.STRING, 8);

        pnd_Check_Datex__R_Field_3 = localVariables.newGroupInRecord("pnd_Check_Datex__R_Field_3", "REDEFINE", pnd_Check_Datex);
        pnd_Check_Datex_Pnd_Check_Date = pnd_Check_Datex__R_Field_3.newFieldInGroup("pnd_Check_Datex_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 24);

        pnd_Input_Parm__R_Field_4 = localVariables.newGroupInRecord("pnd_Input_Parm__R_Field_4", "REDEFINE", pnd_Input_Parm);
        pnd_Input_Parm_Pnd_Input_Orgn_Cde = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Orgn_Cde", "#INPUT-ORGN-CDE", FieldType.STRING, 
            4);
        pnd_Input_Parm_Pnd_Input_Hdr = pnd_Input_Parm__R_Field_4.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Hdr", "#INPUT-HDR", FieldType.STRING, 20);
        pnd_System_Desc = localVariables.newFieldInRecord("pnd_System_Desc", "#SYSTEM-DESC", FieldType.STRING, 32);
        pnd_Start_Index = localVariables.newFieldInRecord("pnd_Start_Index", "#START-INDEX", FieldType.NUMERIC, 1);
        pnd_Hdr_Desc = localVariables.newFieldInRecord("pnd_Hdr_Desc", "#HDR-DESC", FieldType.STRING, 70);
        pnd_Hdr_Dte = localVariables.newFieldInRecord("pnd_Hdr_Dte", "#HDR-DTE", FieldType.STRING, 18);
        pnd_Eom = localVariables.newFieldInRecord("pnd_Eom", "#EOM", FieldType.BOOLEAN, 1);
        pnd_Int_Roll_Mode = localVariables.newFieldInRecord("pnd_Int_Roll_Mode", "#INT-ROLL-MODE", FieldType.BOOLEAN, 1);
        pnd_Sub_Print = localVariables.newFieldInRecord("pnd_Sub_Print", "#SUB-PRINT", FieldType.BOOLEAN, 1);
        pnd_Cf_Txt = localVariables.newFieldInRecord("pnd_Cf_Txt", "#CF-TXT", FieldType.STRING, 9);
        pnd_Ws_Save_Ftre_Ind = localVariables.newFieldInRecord("pnd_Ws_Save_Ftre_Ind", "#WS-SAVE-FTRE-IND", FieldType.STRING, 1);
        pnd_Ws_Pay_Type_Req_Ind = localVariables.newFieldInRecord("pnd_Ws_Pay_Type_Req_Ind", "#WS-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Roll_Dest_Cde = localVariables.newFieldInRecord("pnd_Ws_Roll_Dest_Cde", "#WS-ROLL-DEST-CDE", FieldType.STRING, 4);
        pnd_First_Pass = localVariables.newFieldInRecord("pnd_First_Pass", "#FIRST-PASS", FieldType.NUMERIC, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_Vers_Id.setInitialValue("V1.02 ");
        pnd_Ws_Save_Ftre_Ind.setInitialValue(" ");
        pnd_First_Pass.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp885() throws Exception
    {
        super("Fcpp885");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp885|Main");
        OnErrorManager.pushEvent("FCPP885", onError);
        setupReports();
        while(true)
        {
            try
            {
                //*  05-18-98                                                                                                                                             //Natural: FORMAT ( 01 ) LS = 133 PS = 60
                //*                                                                                                                                                       //Natural: FORMAT ( 02 ) LS = 133 PS = 60;//Natural: ON ERROR
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Parm);                                                                                       //Natural: INPUT #INPUT-PARM
                //*  05-18-98
                short decideConditionsMet829 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #INPUT-ORGN-CDE;//Natural: VALUE 'AL'
                if (condition((pnd_Input_Parm_Pnd_Input_Orgn_Cde.equals("AL"))))
                {
                    decideConditionsMet829++;
                    pnd_System_Desc.setValue("         ANNUITY LOANS          ");                                                                                         //Natural: ASSIGN #SYSTEM-DESC := '         ANNUITY LOANS          '
                }                                                                                                                                                         //Natural: VALUE 'NZ'
                else if (condition((pnd_Input_Parm_Pnd_Input_Orgn_Cde.equals("NZ"))))
                {
                    decideConditionsMet829++;
                    pnd_System_Desc.setValue("       NEW ANNUITIZATION        ");                                                                                         //Natural: ASSIGN #SYSTEM-DESC := '       NEW ANNUITIZATION        '
                }                                                                                                                                                         //Natural: VALUE 'DCSS'
                else if (condition((pnd_Input_Parm_Pnd_Input_Orgn_Cde.equals("DCSS"))))
                {
                    decideConditionsMet829++;
                    pnd_System_Desc.setValue("  IA DEATH / MDO / SINGLE SUM   ");                                                                                         //Natural: ASSIGN #SYSTEM-DESC := '  IA DEATH / MDO / SINGLE SUM   '
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  RL 5-26-2006
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
                sub_Get_Check_Formatting_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                if (condition(pnd_Input_Parm.getSubstring(5,12).equals("END-OF-MONTH")))                                                                                  //Natural: IF SUBSTR ( #INPUT-PARM,5,12 ) = 'END-OF-MONTH'
                {
                    pnd_Eom.setValue(true);                                                                                                                               //Natural: ASSIGN #EOM := TRUE
                }                                                                                                                                                         //Natural: END-IF
                //*  READ PAYMENT/NAME AND ADDRESS WORK FILE
                //*  05-18-98
                short decideConditionsMet850 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #INPUT-ORGN-CDE;//Natural: VALUE 'NZ  ', 'AL  '
                if (condition((pnd_Input_Parm_Pnd_Input_Orgn_Cde.equals("NZ  ") || pnd_Input_Parm_Pnd_Input_Orgn_Cde.equals("AL  "))))
                {
                    decideConditionsMet850++;
                    //*  A. YOUNG
                    READWORK01:                                                                                                                                           //Natural: READ WORK FILE 1 EXT ( * )
                    while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
                    {
                        CheckAtStartofData857();

                        //*  N10
                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num.reset();                                                                                                              //Natural: RESET #PYMNT-NUM
                        //*                                                                                                                                               //Natural: AT START OF DATA
                        //*                                                                                                                                               //Natural: AT END OF DATA
                        //*  12/14/99
                        //*  05-18-98
                        if (condition(pdaFcpaext.getExt_Pymnt_Ftre_Ind().equals(pnd_Ws_Save_Ftre_Ind)))                                                                   //Natural: IF EXT.PYMNT-FTRE-IND EQ #WS-SAVE-FTRE-IND
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  12/14/99
                            pnd_Ws_Save_Ftre_Ind.setValue(pdaFcpaext.getExt_Pymnt_Ftre_Ind());                                                                            //Natural: MOVE EXT.PYMNT-FTRE-IND TO #WS-SAVE-FTRE-IND #PYMNT-FTRE-IND
                            pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.setValue(pdaFcpaext.getExt_Pymnt_Ftre_Ind());
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTALS
                            sub_Print_Subtotals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        //*  05-18-98
                        if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8)) && (pnd_Ws_Pay_Type_Req_Ind.notEquals(8))))                                  //Natural: IF ( EXT.PYMNT-PAY-TYPE-REQ-IND = 8 ) AND ( #WS-PAY-TYPE-REQ-IND NE 8 )
                        {
                            if (condition(! (pnd_Sub_Print.getBoolean())))                                                                                                //Natural: IF NOT #SUB-PRINT
                            {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTALS
                                sub_Print_Subtotals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Sub_Print.setValue(false);                                                                                                            //Natural: ASSIGN #SUB-PRINT := FALSE
                            }                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                            sub_Print_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Cf_Txt.reset();                                                                                                                           //Natural: RESET #CF-TXT
                            //*  12/14/99
                            pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.setValue(pdaFcpaext.getExt_Pymnt_Ftre_Ind());                                                            //Natural: MOVE EXT.PYMNT-FTRE-IND TO #PYMNT-FTRE-IND #WS-SAVE-FTRE-IND
                            pnd_Ws_Save_Ftre_Ind.setValue(pdaFcpaext.getExt_Pymnt_Ftre_Ind());
                            pnd_Ws_Pay_Type_Req_Ind.setValue(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind());                                                                 //Natural: MOVE EXT.PYMNT-PAY-TYPE-REQ-IND TO #WS-PAY-TYPE-REQ-IND
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REPORT-HDR
                            sub_Determine_Report_Hdr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-NZ
                        sub_Process_Record_Nz();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRODUCE-REPORT
                        sub_Produce_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-WORK
                    READWORK01_Exit:
                    if (condition(getWorkFiles().getAtEndOfData()))
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTALS
                        sub_Print_Subtotals();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Cf_Txt.reset();                                                                                                                               //Natural: RESET #CF-TXT
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                        sub_Print_Totals();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-ENDDATA
                    if (Global.isEscape()) return;
                    getWorkFiles().close(1);                                                                                                                              //Natural: CLOSE WORK FILE 1
                }                                                                                                                                                         //Natural: VALUE 'DCSS'
                else if (condition((pnd_Input_Parm_Pnd_Input_Orgn_Cde.equals("DCSS"))))
                {
                    decideConditionsMet850++;
                    READWORK02:                                                                                                                                           //Natural: READ WORK FILE 1 #PYMNT-ADDR
                    while (condition(getWorkFiles().read(1, ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pymnt_Addr())))
                    {
                        CheckAtStartofData907();

                        //*                                                                                                                                               //Natural: AT START OF DATA
                        //*                                                                                                                                               //Natural: AT END OF DATA
                        //*  05-18-98
                        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().equals(pnd_Ws_Save_Ftre_Ind)))                                                          //Natural: IF #RPT-EXT.PYMNT-FTRE-IND EQ #WS-SAVE-FTRE-IND
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ws_Save_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());                                                                   //Natural: MOVE #RPT-EXT.PYMNT-FTRE-IND TO #WS-SAVE-FTRE-IND #PYMNT-FTRE-IND
                            pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTALS
                            sub_Print_Subtotals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        //*  05-18-98
                        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8)) && (pnd_Ws_Pay_Type_Req_Ind.notEquals(8))))                         //Natural: IF ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 8 ) AND ( #WS-PAY-TYPE-REQ-IND NE 8 )
                        {
                            if (condition(! (pnd_Sub_Print.getBoolean())))                                                                                                //Natural: IF NOT #SUB-PRINT
                            {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTALS
                                sub_Print_Subtotals();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                                if (condition(Map.getDoInput())) {return;}
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Sub_Print.setValue(false);                                                                                                            //Natural: ASSIGN #SUB-PRINT := FALSE
                            }                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                            sub_Print_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            pnd_Cf_Txt.reset();                                                                                                                           //Natural: RESET #CF-TXT
                            pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());                                                   //Natural: MOVE #RPT-EXT.PYMNT-FTRE-IND TO #PYMNT-FTRE-IND #WS-SAVE-FTRE-IND
                            pnd_Ws_Save_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());
                            pnd_Ws_Pay_Type_Req_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind());                                                        //Natural: MOVE #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND TO #WS-PAY-TYPE-REQ-IND
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REPORT-HDR
                            sub_Determine_Report_Hdr();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-DCSS
                        sub_Process_Record_Dcss();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRODUCE-REPORT
                        sub_Produce_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-WORK
                    READWORK02_Exit:
                    if (condition(getWorkFiles().getAtEndOfData()))
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUBTOTALS
                        sub_Print_Subtotals();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Cf_Txt.reset();                                                                                                                               //Natural: RESET #CF-TXT
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
                        sub_Print_Totals();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-ENDDATA
                    if (Global.isEscape()) return;
                    getWorkFiles().close(1);                                                                                                                              //Natural: CLOSE WORK FILE 1
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //* *------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-NZ
                //* *------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-DCSS
                //* *------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-REPORT-HDR
                //* *------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRODUCE-REPORT
                //* *----------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUBTOTALS
                //* *FOR #INDEX 1 5
                //* *----------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTALS
                //* *FOR #INDEX 1 5
                //* *------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CSR-CODE-TRANSLATION
                //* *------------
                //* *********************** RL BEGIN - PAYEE MATCH ************MAY 26,2006
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
                //*  RL PAYEE MATCH
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //*  RL PAYEE MATCH
                //* ************************** RL END-PAYEE MATCH *************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //*  1.02 2-16-01 TT
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM #VERS_ID 56T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T #SYSTEM-DESC / *TIMX ( EM = HH:II' 'AP ) 47T #HDR-DESC #CF-TXT //
                //*  1.02 2-16-01 TT
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 02 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM #VERS_ID 56T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T #SYSTEM-DESC / *TIMX ( EM = HH:II' 'AP ) 47T #HDR-DESC #CF-TXT //
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Process_Record_Nz() throws Exception                                                                                                                 //Natural: PROCESS-RECORD-NZ
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        //*  12/14/99
        if (condition((pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("NZ") && (((pdaFcpaext.getExt_Cntrct_Roll_Dest_Cde().equals("IRAT") || pdaFcpaext.getExt_Cntrct_Roll_Dest_Cde().equals("IRAC"))  //Natural: IF ( EXT.CNTRCT-ORGN-CDE = 'NZ' ) AND ( EXT.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = 'RTHT' OR = 'RTHC' )
            || pdaFcpaext.getExt_Cntrct_Roll_Dest_Cde().equals("RTHT")) || pdaFcpaext.getExt_Cntrct_Roll_Dest_Cde().equals("RTHC")))))
        {
            pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde.setValue("IRA");                                                                                                        //Natural: MOVE 'IRA' TO #CNTRCT-ORGN-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde.setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                          //Natural: MOVE EXT.CNTRCT-ORGN-CDE TO #CNTRCT-ORGN-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr.setValue(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr());                                                                              //Natural: MOVE EXT.CNTRCT-PPCN-NBR TO #CNTRCT-PPCN-NBR
        //* ********************* RL PAYEE MATCH BEGIN MAY 26 2006 ****************
        pnd_Check_Datex.setValueEdited(pdaFcpaext.getExt_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
        //*  (N10)
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num.reset();                                                                                                                              //Natural: RESET #PYMNT-NUM
        if (condition(! (pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(2) || pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8))))                                  //Natural: IF NOT ( EXT.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8 )
        {
            if (condition(pdaFcpaext.getExt_Pymnt_Check_Nbr().less(getZero())))                                                                                           //Natural: IF EXT.PYMNT-CHECK-NBR LT 0
            {
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                   //Natural: ASSIGN #PYMNT-NUM-N3 := FCPA110.START-CHECK-PREFIX-N3
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.compute(new ComputeParameters(false, pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7), pdaFcpaext.getExt_Pymnt_Check_Nbr().multiply(-1));   //Natural: COMPUTE #PYMNT-NUM-N7 = EXT.PYMNT-CHECK-NBR * -1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpaext.getExt_Pymnt_Check_Nbr().greater(getZero())))                                                                                    //Natural: IF EXT.PYMNT-CHECK-NBR GT 0
                {
                    pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                                         //Natural: ASSIGN #PYMNT-NUM-N7 := EXT.PYMNT-CHECK-NBR
                    if (condition(pnd_Check_Datex_Pnd_Check_Date.greater(20060430)))                                                                                      //Natural: IF #CHECK-DATE GT 20060430
                    {
                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                           //Natural: ASSIGN #PYMNT-NUM-N3 := FCPA110.START-CHECK-PREFIX-N3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.reset();                                                                                                           //Natural: RESET #PYMNT-NUM-N3
                    }                                                                                                                                                     //Natural: END-IF
                    //* *  ELSE
                    //* *    RESET #PYMNT-NUM  /* N10
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr());                                                                              //Natural: ASSIGN #PYMNT-NUM = EXT.PYMNT-CHECK-SCRTY-NBR
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************** RL MAY 26 2006  END **************************
        pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde.setValue(pdaFcpaext.getExt_Annt_Rsdncy_Cde());                                                                              //Natural: MOVE EXT.ANNT-RSDNCY-CDE TO #ANNT-RSDNCY-CDE
        pnd_Common_Fields_Pnd_Pymnt_Nme.setValue(pdaFcpaext.getExt_Pymnt_Nme().getValue(1));                                                                              //Natural: MOVE EXT.PYMNT-NME ( 1 ) TO #PYMNT-NME
        pnd_Common_Fields_Pnd_Pymnt_Check_Dte.setValue(pdaFcpaext.getExt_Pymnt_Check_Dte());                                                                              //Natural: MOVE EXT.PYMNT-CHECK-DTE TO #PYMNT-CHECK-DTE
        pnd_Common_Fields_Pnd_Pymnt_Check_Amt.setValue(pdaFcpaext.getExt_Pymnt_Check_Amt());                                                                              //Natural: MOVE EXT.PYMNT-CHECK-AMT TO #PYMNT-CHECK-AMT
        pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.setValue(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind());                                                                //Natural: MOVE EXT.PYMNT-PAY-TYPE-REQ-IND TO #PYMNT-PAY-TYPE-REQ-IND
        pnd_Ws_Roll_Dest_Cde.setValue(pdaFcpaext.getExt_Cntrct_Roll_Dest_Cde());                                                                                          //Natural: MOVE EXT.CNTRCT-ROLL-DEST-CDE TO #WS-ROLL-DEST-CDE
        pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.setValue(pdaFcpaext.getExt_Pymnt_Ftre_Ind());                                                                                //Natural: MOVE EXT.PYMNT-FTRE-IND TO #PYMNT-FTRE-IND
        //*  ROXANNE
        pnd_Common_Fields_Pnd_Cntrct_Lob_Cde.setValue(pdaFcpaext.getExt_Cntrct_Lob_Cde());                                                                                //Natural: MOVE EXT.CNTRCT-LOB-CDE TO #CNTRCT-LOB-CDE
        //* * 08-31-99
        //*   V1.01: 2-6-01 MOD BY TT - ALWAYS CALL CODE-TRANS ROUTINE
        //*  IF EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'R' OR = 'S' /*
        pnd_Common_Fields_Pnd_Csr_Activity_Cde.reset();                                                                                                                   //Natural: RESET #CSR-ACTIVITY-CDE
        pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue(pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde());                                                               //Natural: ASSIGN #CSR-ACTIVITY-CDE := EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
                                                                                                                                                                          //Natural: PERFORM CSR-CODE-TRANSLATION
        sub_Csr_Code_Translation();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  END-IF
    }
    private void sub_Process_Record_Dcss() throws Exception                                                                                                               //Natural: PROCESS-RECORD-DCSS
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());                                                                     //Natural: MOVE #RPT-EXT.CNTRCT-ORGN-CDE TO #CNTRCT-ORGN-CDE
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS")))                                                                                         //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS'
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31")))                        //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = '30' OR = '31'
            {
                pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde.setValue("MDO");                                                                                                    //Natural: MOVE 'MDO' TO #CNTRCT-ORGN-CDE
                //*  05-18-98
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Roll_Dest_Cde().equals("IRAT") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Roll_Dest_Cde().equals("IRAC")        //Natural: IF ( #RPT-EXT.CNTRCT-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC' OR = 'RTHT' OR = 'RTHC' )
                    || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Roll_Dest_Cde().equals("RTHT") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Roll_Dest_Cde().equals("RTHC")))
                {
                    pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde.setValue("IRA");                                                                                                //Natural: MOVE 'IRA' TO #CNTRCT-ORGN-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr());                                                                     //Natural: MOVE #RPT-EXT.CNTRCT-PPCN-NBR TO #CNTRCT-PPCN-NBR
        //* ********************* RL PAYEE MATCH BEGIN MAY 26 2006 ****************
        pnd_Check_Datex.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
        if (condition(! (ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2) || ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8))))                //Natural: IF NOT ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8 )
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().less(getZero())))                                                                                  //Natural: IF #RPT-EXT.PYMNT-CHECK-NBR LT 0
            {
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                   //Natural: ASSIGN #PYMNT-NUM-N3 := FCPA110.START-CHECK-PREFIX-N3
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.compute(new ComputeParameters(false, pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7), ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().multiply(-1)); //Natural: COMPUTE #PYMNT-NUM-N7 = #RPT-EXT.PYMNT-CHECK-NBR * -1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().greater(getZero())))                                                                           //Natural: IF #RPT-EXT.PYMNT-CHECK-NBR GT 0
                {
                    pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr());                                                                //Natural: ASSIGN #PYMNT-NUM-N7 := #RPT-EXT.PYMNT-CHECK-NBR
                    if (condition(pnd_Check_Datex_Pnd_Check_Date.greater(20060430)))                                                                                      //Natural: IF #CHECK-DATE GT 20060430
                    {
                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                           //Natural: ASSIGN #PYMNT-NUM-N3 := FCPA110.START-CHECK-PREFIX-N3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.reset();                                                                                                           //Natural: RESET #PYMNT-NUM-N3
                    }                                                                                                                                                     //Natural: END-IF
                    //* *  ELSE
                    //* *    RESET #PYMNT-NUM  /* N10
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr());                                                                     //Natural: ASSIGN #PYMNT-NUM = #RPT-EXT.PYMNT-CHECK-SCRTY-NBR
        }                                                                                                                                                                 //Natural: END-IF
        //* ******************** RL MAY 26 2006  END **************************
        //* *IF #RPT-EXT.PYMNT-CHECK-NBR NE 0
        //* *  MOVE #RPT-EXT.PYMNT-CHECK-NBR        TO #PYMNT-CHECK-NBR
        //* *ELSE
        //* *  MOVE #RPT-EXT.PYMNT-CHECK-SCRTY-NBR  TO #PYMNT-CHECK-NBR
        //* *END-IF
        pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde());                                                                     //Natural: MOVE #RPT-EXT.ANNT-RSDNCY-CDE TO #ANNT-RSDNCY-CDE
        pnd_Common_Fields_Pnd_Pymnt_Nme.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1));                                                                     //Natural: MOVE #RPT-EXT.PYMNT-NME ( 1 ) TO #PYMNT-NME
        pnd_Common_Fields_Pnd_Pymnt_Check_Dte.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                                     //Natural: MOVE #RPT-EXT.PYMNT-CHECK-DTE TO #PYMNT-CHECK-DTE
        pnd_Common_Fields_Pnd_Pymnt_Check_Amt.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                                     //Natural: MOVE #RPT-EXT.PYMNT-CHECK-AMT TO #PYMNT-CHECK-AMT
        pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind());                                                       //Natural: MOVE #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND TO #PYMNT-PAY-TYPE-REQ-IND
        //*  05-18-98
        pnd_Ws_Roll_Dest_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Roll_Dest_Cde());                                                                                 //Natural: MOVE #RPT-EXT.CNTRCT-ROLL-DEST-CDE TO #WS-ROLL-DEST-CDE
        pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());                                                                       //Natural: MOVE #RPT-EXT.PYMNT-FTRE-IND TO #PYMNT-FTRE-IND
        //*  ROXANNE
        pnd_Common_Fields_Pnd_Cntrct_Lob_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde());                                                                       //Natural: MOVE #RPT-EXT.CNTRCT-LOB-CDE TO #CNTRCT-LOB-CDE
        //* * 08-31-99
        //*   V1.01: 2-6-01 MOD BY TT - ALWAYS CALL CODE-TRANS ROUTINE
        //*  IF #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'R' OR = 'S'
        pnd_Common_Fields_Pnd_Csr_Activity_Cde.reset();                                                                                                                   //Natural: RESET #CSR-ACTIVITY-CDE
        pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde());                                                      //Natural: ASSIGN #CSR-ACTIVITY-CDE := #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
                                                                                                                                                                          //Natural: PERFORM CSR-CODE-TRANSLATION
        sub_Csr_Code_Translation();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  END-IF
    }
    private void sub_Determine_Report_Hdr() throws Exception                                                                                                              //Natural: DETERMINE-REPORT-HDR
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        //*  05-18-98
        if (condition(pnd_Ws_Pay_Type_Req_Ind.equals(8)))                                                                                                                 //Natural: IF #WS-PAY-TYPE-REQ-IND = 8
        {
            pnd_Int_Roll_Mode.setValue(true);                                                                                                                             //Natural: ASSIGN #INT-ROLL-MODE := TRUE
            pnd_Hdr_Desc.setValue(DbsUtil.compress(pnd_Input_Parm_Pnd_Input_Hdr, "INTERNAL ROLLOVER REGISTER"));                                                          //Natural: COMPRESS #INPUT-PARM.#INPUT-HDR 'INTERNAL ROLLOVER REGISTER' INTO #HDR-DESC
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hdr_Desc.setValue(DbsUtil.compress(pnd_Input_Parm_Pnd_Input_Hdr, "PAYMENT REGISTER"));                                                                    //Natural: COMPRESS #INPUT-PARM.#INPUT-HDR 'PAYMENT REGISTER' INTO #HDR-DESC
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Eom.getBoolean()))                                                                                                                              //Natural: IF #EOM
        {
            if (condition(pnd_Common_Fields_Pnd_Pymnt_Check_Dte.notEquals(getZero())))                                                                                    //Natural: IF #PYMNT-CHECK-DTE NE 0
            {
                pnd_Hdr_Dte.setValueEdited(pnd_Common_Fields_Pnd_Pymnt_Check_Dte,new ReportEditMask("LLLLLLLLL', 'YYYY"));                                                //Natural: MOVE EDITED #PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) TO #HDR-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Hdr_Dte.setValueEdited(Global.getDATX(),new ReportEditMask("LLLLLLLLL' 'DD', 'YYYY"));                                                                    //Natural: MOVE EDITED *DATX ( EM = LLLLLLLLL' 'DD', 'YYYY ) TO #HDR-DTE
            if (condition(pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.equals("F")))                                                                                              //Natural: IF #PYMNT-FTRE-IND = 'F'
            {
                pnd_Cf_Txt.setValue("(FUTURE)");                                                                                                                          //Natural: ASSIGN #CF-TXT := '(FUTURE)'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cf_Txt.setValue("(CURRENT)");                                                                                                                         //Natural: ASSIGN #CF-TXT := '(CURRENT)'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Hdr_Desc.setValue(DbsUtil.compress(pnd_Hdr_Desc, "for", pnd_Hdr_Dte));                                                                                        //Natural: COMPRESS #HDR-DESC 'for' #HDR-DTE INTO #HDR-DESC
    }
    private void sub_Produce_Report() throws Exception                                                                                                                    //Natural: PRODUCE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------
        short decideConditionsMet1157 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.equals(1))))
        {
            decideConditionsMet1157++;
            pnd_Index.setValue(1);                                                                                                                                        //Natural: MOVE 1 TO #INDEX
            pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("C");                                                                                                                   //Natural: MOVE 'C' TO #PYMNT-TYPE
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.equals(2))))
        {
            decideConditionsMet1157++;
            pnd_Index.setValue(2);                                                                                                                                        //Natural: MOVE 2 TO #INDEX
            pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("E");                                                                                                                   //Natural: MOVE 'E' TO #PYMNT-TYPE
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.equals(3))))
        {
            decideConditionsMet1157++;
            pnd_Index.setValue(3);                                                                                                                                        //Natural: MOVE 3 TO #INDEX
            //*  05-18-98
            pnd_Pymnt_Nbr_Pnd_Pymnt_Type.reset();                                                                                                                         //Natural: RESET #PYMNT-TYPE
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.equals(8))))
        {
            decideConditionsMet1157++;
            pnd_Index.setValue(4);                                                                                                                                        //Natural: MOVE 4 TO #INDEX
            pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("X");                                                                                                                   //Natural: MOVE 'X' TO #PYMNT-TYPE
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet1157 > 0))
        {
            //*                                                    /* ROXANNE 4/7/2003
            short decideConditionsMet1173 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PYMNT-PAY-TYPE-REQ-IND = 1 THRU 3
            if (condition(pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.greaterOrEqual(1) && pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.lessOrEqual(3)))
            {
                decideConditionsMet1173++;
                pnd_Start_Index.setValue(1);                                                                                                                              //Natural: ASSIGN #START-INDEX := I := 1
                i.setValue(1);
            }                                                                                                                                                             //Natural: WHEN #WS-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC'
            else if (condition(pnd_Ws_Roll_Dest_Cde.equals("IRAT") || pnd_Ws_Roll_Dest_Cde.equals("IRAC")))
            {
                decideConditionsMet1173++;
                i.setValue(2);                                                                                                                                            //Natural: ASSIGN I := 2
            }                                                                                                                                                             //Natural: WHEN #WS-ROLL-DEST-CDE = 'RTHT' OR = 'RTHC'
            else if (condition(pnd_Ws_Roll_Dest_Cde.equals("RTHT") || pnd_Ws_Roll_Dest_Cde.equals("RTHC")))
            {
                decideConditionsMet1173++;
                i.setValue(3);                                                                                                                                            //Natural: ASSIGN I := 3
            }                                                                                                                                                             //Natural: WHEN #CNTRCT-LOB-CDE = 'IRA'
            else if (condition(pnd_Common_Fields_Pnd_Cntrct_Lob_Cde.equals("IRA")))
            {
                decideConditionsMet1173++;
                i.setValue(4);                                                                                                                                            //Natural: ASSIGN I := 4
            }                                                                                                                                                             //Natural: WHEN #CNTRCT-LOB-CDE = 'SRA'
            else if (condition(pnd_Common_Fields_Pnd_Cntrct_Lob_Cde.equals("SRA")))
            {
                decideConditionsMet1173++;
                i.setValue(5);                                                                                                                                            //Natural: ASSIGN I := 5
            }                                                                                                                                                             //Natural: WHEN #CNTRCT-LOB-CDE = 'GRA'
            else if (condition(pnd_Common_Fields_Pnd_Cntrct_Lob_Cde.equals("GRA")))
            {
                decideConditionsMet1173++;
                i.setValue(6);                                                                                                                                            //Natural: ASSIGN I := 6
            }                                                                                                                                                             //Natural: WHEN #CNTRCT-LOB-CDE = 'GSRA'
            else if (condition(pnd_Common_Fields_Pnd_Cntrct_Lob_Cde.equals("GSRA")))
            {
                decideConditionsMet1173++;
                i.setValue(7);                                                                                                                                            //Natural: ASSIGN I := 7
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet1173 > 0))
            {
                if (condition(pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.greaterOrEqual(1) && pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.lessOrEqual(3)))             //Natural: IF #PYMNT-PAY-TYPE-REQ-IND = 1 THRU 3
                {
                    pnd_Start_Index.setValue(1);                                                                                                                          //Natural: ASSIGN #START-INDEX := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Start_Index.setValue(4);                                                                                                                          //Natural: ASSIGN #START-INDEX := 4
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                i.setValue(8);                                                                                                                                            //Natural: ASSIGN I := 8
            }                                                                                                                                                             //Natural: END-DECIDE
            //*                                                    /*
            //*   IF #PYMNT-PAY-TYPE-REQ-IND = 1 THRU 3                    /* 05-18-98
            //*     #START-INDEX := I := 1
            //*   ELSE
            //*     IF #WS-ROLL-DEST-CDE = 'IRAT' OR = 'IRAC'
            //*       I := 2
            //*     ELSE
            //*       IF #WS-ROLL-DEST-CDE = 'RTHT' OR = 'RTHC'
            //*         I := 3
            //*       ELSE
            //*         I := 4
            //*       END-IF
            //*     END-IF
            //*     #START-INDEX := 4
            //*   END-IF
            //* ********************* RL PAYEE MATCH BEGIN MAY 26 2006 ****************
            //* *  MOVE #PYMNT-CHECK-NBR TO #PYMNT-NUM
            pnd_Check_Datex.setValueEdited(pnd_Common_Fields_Pnd_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED #PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
            if (condition(! (pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.equals(2) || pnd_Common_Fields_Pnd_Pymnt_Pay_Type_Req_Ind.equals(8))))                          //Natural: IF NOT ( #PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8 )
            {
                if (condition(pnd_Common_Fields_Pnd_Pymnt_Check_Nbr.less(getZero())))                                                                                     //Natural: IF #PYMNT-CHECK-NBR LT 0
                {
                    pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                               //Natural: ASSIGN #PYMNT-NUM-N3 := FCPA110.START-CHECK-PREFIX-N3
                    pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.compute(new ComputeParameters(false, pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7), pnd_Common_Fields_Pnd_Pymnt_Check_Nbr.multiply(-1)); //Natural: COMPUTE #PYMNT-NUM-N7 = #PYMNT-CHECK-NBR * -1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Common_Fields_Pnd_Pymnt_Check_Nbr.greater(getZero())))                                                                              //Natural: IF #PYMNT-CHECK-NBR GT 0
                    {
                        pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N7.setValue(pnd_Common_Fields_Pnd_Pymnt_Check_Nbr);                                                                   //Natural: ASSIGN #PYMNT-NUM-N7 := #PYMNT-CHECK-NBR
                        if (condition(pnd_Check_Datex_Pnd_Check_Date.greater(20060430)))                                                                                  //Natural: IF #CHECK-DATE GT 20060430
                        {
                            pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                       //Natural: ASSIGN #PYMNT-NUM-N3 := FCPA110.START-CHECK-PREFIX-N3
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Pymnt_Nbr_Pnd_Pymnt_Num_N3.reset();                                                                                                       //Natural: RESET #PYMNT-NUM-N3
                        }                                                                                                                                                 //Natural: END-IF
                        //* *      ELSE
                        //* *        RESET #PYMNT-NUM  /* N10
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  OR EXT. ?
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr());                                                                 //Natural: ASSIGN #PYMNT-NUM = #RPT-EXT.PYMNT-CHECK-SCRTY-NBR
            }                                                                                                                                                             //Natural: END-IF
            //* **************** RL PAYEE MATCH END ********************
            totals_Pnd_T_Net.getValue(pnd_Index,i).nadd(pnd_Common_Fields_Pnd_Pymnt_Check_Amt);                                                                           //Natural: ADD #PYMNT-CHECK-AMT TO #T-NET ( #INDEX,I )
            totals_Pnd_T_Net.getValue(5,i).nadd(pnd_Common_Fields_Pnd_Pymnt_Check_Amt);                                                                                   //Natural: ADD #PYMNT-CHECK-AMT TO #T-NET ( 5,I )
            totals_Pnd_Sub_Net.getValue(pnd_Index,i).nadd(pnd_Common_Fields_Pnd_Pymnt_Check_Amt);                                                                         //Natural: ADD #PYMNT-CHECK-AMT TO #SUB-NET ( #INDEX,I )
            totals_Pnd_Sub_Net.getValue(5,i).nadd(pnd_Common_Fields_Pnd_Pymnt_Check_Amt);                                                                                 //Natural: ADD #PYMNT-CHECK-AMT TO #SUB-NET ( 5,I )
            totals_Pnd_T_Count.getValue(pnd_Index,i).nadd(1);                                                                                                             //Natural: ADD 1 TO #T-COUNT ( #INDEX,I )
            totals_Pnd_T_Count.getValue(5,i).nadd(1);                                                                                                                     //Natural: ADD 1 TO #T-COUNT ( 5,I )
            totals_Pnd_Sub_Count.getValue(pnd_Index,i).nadd(1);                                                                                                           //Natural: ADD 1 TO #SUB-COUNT ( #INDEX,I )
            totals_Pnd_Sub_Count.getValue(5,i).nadd(1);                                                                                                                   //Natural: ADD 1 TO #SUB-COUNT ( 5,I )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            //*  RL MAY 26, 2006
            //*  (AL=13)
            getReports().display(1, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                               //Natural: DISPLAY ( 01 ) ( UC = - ) 'Contract/Number' #CNTRCT-PPCN-NBR 'PAYMENT/NUMBER' #PYMNT-NBR 'Geo/Cde' #ANNT-RSDNCY-CDE ' /Payee' #PYMNT-NME 'Payment/Date' #PYMNT-CHECK-DTE ( EM = MM/DD/YY ) ' /Net Payment' #PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Origin/Code' #CNTRCT-ORGN-CDE 'CSR ACTIVITY/CODE' #CSR-ACTIVITY-CDE
            		pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr,"PAYMENT/NUMBER",
            		pnd_Pymnt_Nbr,"Geo/Cde",
            		pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde," /Payee",
            		pnd_Common_Fields_Pnd_Pymnt_Nme,"Payment/Date",
            		pnd_Common_Fields_Pnd_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YY")," /Net Payment",
            		pnd_Common_Fields_Pnd_Pymnt_Check_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Origin/Code",
            		pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde,"CSR ACTIVITY/CODE",
            		pnd_Common_Fields_Pnd_Csr_Activity_Cde);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  RL MAY 26, 2006
            //*  (AL=13)
            getReports().display(2, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                               //Natural: DISPLAY ( 02 ) ( UC = - ) 'Contract/Number' #CNTRCT-PPCN-NBR 'PAYMENT/NUMBER' #PYMNT-NBR 'Geo/Cde' #ANNT-RSDNCY-CDE ' /Payee' #PYMNT-NME 'Payment/Date' #PYMNT-CHECK-DTE ( EM = MM/DD/YY ) ' /Net Payment' #PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'CSR ACTIVITY/CODE' #CSR-ACTIVITY-CDE
            		pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr,"PAYMENT/NUMBER",
            		pnd_Pymnt_Nbr,"Geo/Cde",
            		pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde," /Payee",
            		pnd_Common_Fields_Pnd_Pymnt_Nme,"Payment/Date",
            		pnd_Common_Fields_Pnd_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YY")," /Net Payment",
            		pnd_Common_Fields_Pnd_Pymnt_Check_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CSR ACTIVITY/CODE",
            		pnd_Common_Fields_Pnd_Csr_Activity_Cde);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Subtotals() throws Exception                                                                                                                   //Natural: PRINT-SUBTOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        //*  05-18-98
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            if (condition(getReports().getAstLinesLeft(1).less(36)))                                                                                                      //Natural: NEWPAGE ( 01 ) IF LESS THAN 36 LINES LEFT
            {
                getReports().newPage(1);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,"SUBTOTALS     :");                                                                                             //Natural: WRITE ( 01 ) /// 'SUBTOTALS     :'
            if (Global.isEscape()) return;
            //*  05-18-98
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(getReports().getAstLinesLeft(2).less(36)))                                                                                                      //Natural: NEWPAGE ( 02 ) IF LESS THAN 36 LINES LEFT
            {
                getReports().newPage(2);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"SUBTOTALS     :");                                                                                             //Natural: WRITE ( 02 ) /// 'SUBTOTALS     :'
            if (Global.isEscape()) return;
            //*  05-18-98
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #INDEX #START-INDEX 5
        for (pnd_Index.setValue(pnd_Start_Index); condition(pnd_Index.lessOrEqual(5)); pnd_Index.nadd(1))
        {
            //*  CHECK
            short decideConditionsMet1273 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #INDEX;//Natural: VALUE 1
            if (condition((pnd_Index.equals(1))))
            {
                decideConditionsMet1273++;
                //*  EFT
                pnd_Total_Lit.setValue("     Check Payments     ");                                                                                                       //Natural: MOVE '     Check Payments     ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Index.equals(2))))
            {
                decideConditionsMet1273++;
                //*  GLOBAL
                pnd_Total_Lit.setValue("     EFT Payments       ");                                                                                                       //Natural: MOVE '     EFT Payments       ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Index.equals(3))))
            {
                decideConditionsMet1273++;
                //*  INTERNAL ROLLOVER
                pnd_Total_Lit.setValue("     Global Payments    ");                                                                                                       //Natural: MOVE '     Global Payments    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Index.equals(4))))
            {
                decideConditionsMet1273++;
                //*  05-18-98
                pnd_Total_Lit.setValue("     Internal Rollovers:");                                                                                                       //Natural: MOVE '     Internal Rollovers:' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Index.equals(5))))
            {
                decideConditionsMet1273++;
                //*  05-18-98
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    pnd_Total_Lit.setValue("     Payments           ");                                                                                                   //Natural: MOVE '     Payments           ' TO #TOTAL-LIT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Total_Lit.setValue("      Internal Rollovers");                                                                                                   //Natural: MOVE '      Internal Rollovers' TO #TOTAL-LIT
                    //*  05-18-98
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1273 > 0))
            {
                short decideConditionsMet1295 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #INDEX EQ 1 THRU 3 ) OR ( #INDEX EQ 5 )
                if (condition(((pnd_Index.greaterOrEqual(1) && pnd_Index.lessOrEqual(3)) || pnd_Index.equals(5))))
                {
                    decideConditionsMet1295++;
                    if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                    //Natural: IF NOT #INT-ROLL-MODE
                    {
                        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,pnd_Total_Lit);                                                                                     //Natural: WRITE ( 01 ) /// #TOTAL-LIT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, new ColumnSpacing(10),totals_Pnd_Sub_Net.getValue(pnd_Index,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new                  //Natural: WRITE ( 01 ) 10X #SUB-NET ( #INDEX,1 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,1 ) ( EM = Z,ZZZ,ZZ9 )
                            ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,1), new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,pnd_Total_Lit);                                                                                     //Natural: WRITE ( 02 ) /// #TOTAL-LIT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Index.equals(5)))                                                                                                               //Natural: IF #INDEX EQ 5
                        {
                            getReports().write(2, new ColumnSpacing(34),totals_Pnd_Sub_Net.getValue(pnd_Index,8), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new              //Natural: WRITE ( 02 ) 34X #SUB-NET ( #INDEX,8 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,8 ) ( EM = Z,ZZZ,ZZ9 )
                                ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,8), new ReportEditMask ("Z,ZZZ,ZZ9"));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(2, new ColumnSpacing(10),totals_Pnd_Sub_Net.getValue(pnd_Index,8), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new              //Natural: WRITE ( 02 ) 10X #SUB-NET ( #INDEX,8 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,8 ) ( EM = Z,ZZZ,ZZ9 )
                                ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,8), new ReportEditMask ("Z,ZZZ,ZZ9"));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #INDEX EQ 4
                else if (condition(pnd_Index.equals(4)))
                {
                    decideConditionsMet1295++;
                    if (condition(pnd_Int_Roll_Mode.getBoolean()))                                                                                                        //Natural: IF #INT-ROLL-MODE
                    {
                        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,pnd_Total_Lit);                                                                                     //Natural: WRITE ( 02 ) /// #TOTAL-LIT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(2, NEWLINE,NEWLINE,new ColumnSpacing(16),"Classic:",new ColumnSpacing(10),totals_Pnd_Sub_Net.getValue(pnd_Index,2),            //Natural: WRITE ( 02 ) //16X 'Classic:' 10X #SUB-NET ( #INDEX,2 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,2 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'Roth:' 10X #SUB-NET ( #INDEX,3 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,3 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'RA:  ' 10X #SUB-NET ( #INDEX,4 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,4 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'SRA: ' 10X #SUB-NET ( #INDEX,5 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,5 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'GRA: ' 10X #SUB-NET ( #INDEX,6 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,6 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'GSRA:' 10X #SUB-NET ( #INDEX,7 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #SUB-COUNT ( #INDEX,7 ) ( EM = Z,ZZZ,ZZ9 )
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,2), new ReportEditMask 
                            ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(19),"Roth:",new ColumnSpacing(10),totals_Pnd_Sub_Net.getValue(pnd_Index,3), new ReportEditMask 
                            ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,3), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
                            ColumnSpacing(19),"RA:  ",new ColumnSpacing(10),totals_Pnd_Sub_Net.getValue(pnd_Index,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
                            ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,4), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(19),"SRA: ",new 
                            ColumnSpacing(10),totals_Pnd_Sub_Net.getValue(pnd_Index,5), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,5), 
                            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(19),"GRA: ",new ColumnSpacing(10),totals_Pnd_Sub_Net.getValue(pnd_Index,6), 
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,6), new ReportEditMask 
                            ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(19),"GSRA:",new ColumnSpacing(10),totals_Pnd_Sub_Net.getValue(pnd_Index,7), new ReportEditMask 
                            ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_Sub_Count.getValue(pnd_Index,7), new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*                                                    /* ROXANNE 4/7/2003
                        totals_Pnd_Sub_Net.getValue(5,8).reset();                                                                                                         //Natural: RESET #SUB-NET ( 5,8 )
                        totals_Pnd_Sub_Count.getValue(5,8).reset();                                                                                                       //Natural: RESET #SUB-COUNT ( 5,8 )
                        totals_Pnd_Sub_Net.getValue(5,8).nadd(totals_Pnd_Sub_Net.getValue(pnd_Index,2,":",3));                                                            //Natural: ADD #SUB-NET ( #INDEX,2:3 ) TO #SUB-NET ( 5,8 )
                        totals_Pnd_Sub_Count.getValue(5,8).nadd(totals_Pnd_Sub_Count.getValue(pnd_Index,2,":",3));                                                        //Natural: ADD #SUB-COUNT ( #INDEX,2:3 ) TO #SUB-COUNT ( 5,8 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.equals("F")))                                                                                                  //Natural: IF #PYMNT-FTRE-IND = 'F'
        {
            pnd_Cf_Txt.setValue("(FUTURE)");                                                                                                                              //Natural: ASSIGN #CF-TXT := '(FUTURE)'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Cf_Txt.setValue("(CURRENT)");                                                                                                                             //Natural: ASSIGN #CF-TXT := '(CURRENT)'
        }                                                                                                                                                                 //Natural: END-IF
        //*  05-18-98
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  05-18-98
        if (condition(! (pnd_Sub_Print.getBoolean())))                                                                                                                    //Natural: IF NOT #SUB-PRINT
        {
            pnd_Sub_Print.setValue(true);                                                                                                                                 //Natural: ASSIGN #SUB-PRINT := TRUE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        //*  05-18-98
        if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                                //Natural: IF NOT #INT-ROLL-MODE
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,"SUMMARY TOTALS:");                                                                                             //Natural: WRITE ( 01 ) /// 'SUMMARY TOTALS:'
            if (Global.isEscape()) return;
            //*  05-18-98
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"SUMMARY TOTALS:");                                                                                             //Natural: WRITE ( 02 ) /// 'SUMMARY TOTALS:'
            if (Global.isEscape()) return;
            //*  05-18-98
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #INDEX #START-INDEX 5
        for (pnd_Index.setValue(pnd_Start_Index); condition(pnd_Index.lessOrEqual(5)); pnd_Index.nadd(1))
        {
            //*  CHECK
            short decideConditionsMet1364 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #INDEX;//Natural: VALUE 1
            if (condition((pnd_Index.equals(1))))
            {
                decideConditionsMet1364++;
                //*  EFT
                pnd_Total_Lit.setValue("Total Check Payments    ");                                                                                                       //Natural: MOVE 'Total Check Payments    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Index.equals(2))))
            {
                decideConditionsMet1364++;
                //*  GLOBAL
                pnd_Total_Lit.setValue("Total EFT Payments      ");                                                                                                       //Natural: MOVE 'Total EFT Payments      ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Index.equals(3))))
            {
                decideConditionsMet1364++;
                //*  INTERNAL ROLLOVER
                pnd_Total_Lit.setValue("Total Global Payments   ");                                                                                                       //Natural: MOVE 'Total Global Payments   ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Index.equals(4))))
            {
                decideConditionsMet1364++;
                pnd_Total_Lit.setValue("Total Internal Rollovers");                                                                                                       //Natural: MOVE 'Total Internal Rollovers' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Index.equals(5))))
            {
                decideConditionsMet1364++;
                //*  05-18-98
                if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                        //Natural: IF NOT #INT-ROLL-MODE
                {
                    pnd_Total_Lit.setValue("Total Payments          ");                                                                                                   //Natural: MOVE 'Total Payments          ' TO #TOTAL-LIT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Total_Lit.setValue("Total Internal Rollovers");                                                                                                   //Natural: MOVE 'Total Internal Rollovers' TO #TOTAL-LIT
                    //*  05-18-98
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1364 > 0))
            {
                short decideConditionsMet1385 = 0;                                                                                                                        //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( #INDEX EQ 1 THRU 3 ) OR ( #INDEX EQ 5 )
                if (condition(((pnd_Index.greaterOrEqual(1) && pnd_Index.lessOrEqual(3)) || pnd_Index.equals(5))))
                {
                    decideConditionsMet1385++;
                    if (condition(! (pnd_Int_Roll_Mode.getBoolean())))                                                                                                    //Natural: IF NOT #INT-ROLL-MODE
                    {
                        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,pnd_Total_Lit);                                                                                     //Natural: WRITE ( 01 ) /// #TOTAL-LIT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, new ColumnSpacing(10),totals_Pnd_T_Net.getValue(pnd_Index,1), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new                    //Natural: WRITE ( 01 ) 10X #T-NET ( #INDEX,1 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,1 ) ( EM = Z,ZZZ,ZZ9 )
                            ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,1), new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,pnd_Total_Lit);                                                                                     //Natural: WRITE ( 02 ) /// #TOTAL-LIT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Index.equals(5)))                                                                                                               //Natural: IF #INDEX EQ 5
                        {
                            getReports().write(2, new ColumnSpacing(34),totals_Pnd_T_Net.getValue(pnd_Index,8), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new                //Natural: WRITE ( 02 ) 34X #T-NET ( #INDEX,8 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,8 ) ( EM = Z,ZZZ,ZZ9 )
                                ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,8), new ReportEditMask ("Z,ZZZ,ZZ9"));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(2, new ColumnSpacing(10),totals_Pnd_T_Net.getValue(pnd_Index,8), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new                //Natural: WRITE ( 02 ) 10X #T-NET ( #INDEX,8 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,8 ) ( EM = Z,ZZZ,ZZ9 )
                                ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,8), new ReportEditMask ("Z,ZZZ,ZZ9"));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN #INDEX EQ 4
                else if (condition(pnd_Index.equals(4)))
                {
                    decideConditionsMet1385++;
                    if (condition(pnd_Int_Roll_Mode.getBoolean()))                                                                                                        //Natural: IF #INT-ROLL-MODE
                    {
                        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,pnd_Total_Lit);                                                                                     //Natural: WRITE ( 02 ) /// #TOTAL-LIT
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(2, NEWLINE,NEWLINE,new ColumnSpacing(16),"Classic:",new ColumnSpacing(10),totals_Pnd_T_Net.getValue(pnd_Index,2),              //Natural: WRITE ( 02 ) //16X 'Classic:' 10X #T-NET ( #INDEX,2 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,2 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'Roth:' 10X #T-NET ( #INDEX,3 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,3 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'RA:  ' 10X #T-NET ( #INDEX,4 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,4 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'SRA: ' 10X #T-NET ( #INDEX,5 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,5 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'GRA: ' 10X #T-NET ( #INDEX,6 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,6 ) ( EM = Z,ZZZ,ZZ9 ) / 19X 'GSRA:' 10X #T-NET ( #INDEX,7 ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 10X #T-COUNT ( #INDEX,7 ) ( EM = Z,ZZZ,ZZ9 )
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,2), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
                            ColumnSpacing(19),"Roth:",new ColumnSpacing(10),totals_Pnd_T_Net.getValue(pnd_Index,3), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
                            ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,3), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(19),"RA:  ",new 
                            ColumnSpacing(10),totals_Pnd_T_Net.getValue(pnd_Index,4), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,4), 
                            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(19),"SRA: ",new ColumnSpacing(10),totals_Pnd_T_Net.getValue(pnd_Index,5), 
                            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,5), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
                            ColumnSpacing(19),"GRA: ",new ColumnSpacing(10),totals_Pnd_T_Net.getValue(pnd_Index,6), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
                            ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,6), new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(19),"GSRA:",new 
                            ColumnSpacing(10),totals_Pnd_T_Net.getValue(pnd_Index,7), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),totals_Pnd_T_Count.getValue(pnd_Index,7), 
                            new ReportEditMask ("Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*                                                  /* ROXANNE 04/08/2003
                        totals_Pnd_T_Net.getValue(5,8).reset();                                                                                                           //Natural: RESET #T-NET ( 5,8 )
                        totals_Pnd_T_Count.getValue(5,8).reset();                                                                                                         //Natural: RESET #T-COUNT ( 5,8 )
                        totals_Pnd_T_Net.getValue(5,8).nadd(totals_Pnd_T_Net.getValue(pnd_Index,2,":",3));                                                                //Natural: ADD #T-NET ( #INDEX,2:3 ) TO #T-NET ( 5,8 )
                        totals_Pnd_T_Count.getValue(5,8).nadd(totals_Pnd_T_Count.getValue(pnd_Index,2,":",3));                                                            //Natural: ADD #T-COUNT ( #INDEX,2:3 ) TO #T-COUNT ( 5,8 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  08-31-99
    private void sub_Csr_Code_Translation() throws Exception                                                                                                              //Natural: CSR-CODE-TRANSLATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        //*  1.01 2-6-01 TT
        //*  JWO 2010-99
        //*  JWO 2010-99
        //*  JWO 2010-99
        //*  JWO 2010-99
        //*  1.02 2-16-01 TT
        short decideConditionsMet1432 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #CSR-ACTIVITY-CDE;//Natural: VALUE 'C'
        if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("C"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("CANCELLED");                                                                                                 //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'CANCELLED'
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("R"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("REDRAWN  ");                                                                                                 //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'REDRAWN  '
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("S"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("STOPPED  ");                                                                                                 //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'STOPPED  '
        }                                                                                                                                                                 //Natural: VALUE 'CN'
        else if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("CN"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("CANC NO R");                                                                                                 //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'CANC NO R'
        }                                                                                                                                                                 //Natural: VALUE 'CP'
        else if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("CP"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("CANCEL PEND");                                                                                               //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'CANCEL PEND'
        }                                                                                                                                                                 //Natural: VALUE 'SN'
        else if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("SN"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("STOP NO REDRAW  ");                                                                                          //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'STOP NO REDRAW  '
        }                                                                                                                                                                 //Natural: VALUE 'SP'
        else if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("SP"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("STOP PEND  ");                                                                                               //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'STOP PEND  '
        }                                                                                                                                                                 //Natural: VALUE 'RP'
        else if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("RP"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("REDRAW PEND  ");                                                                                             //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'REDRAW PEND  '
        }                                                                                                                                                                 //Natural: VALUE 'PR'
        else if (condition((pnd_Common_Fields_Pnd_Csr_Activity_Cde.equals("PR"))))
        {
            decideConditionsMet1432++;
            pnd_Common_Fields_Pnd_Csr_Activity_Cde.setValue("PENDED RETURN");                                                                                             //Natural: ASSIGN #CSR-ACTIVITY-CDE := 'PENDED RETURN'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *------------
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***              PPCN:' #CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***              PPCN:",
            pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************");
        getWorkFiles().close(1);                                                                                                                                          //Natural: CLOSE WORK FILE 1
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
        Global.format(2, "LS=133 PS=60");

        getReports().write(1, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),pnd_Vers_Id,new 
            TabSetting(56),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(56),pnd_System_Desc,NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(47),
            pnd_Hdr_Desc,pnd_Cf_Txt,NEWLINE,NEWLINE);
        getReports().write(2, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),pnd_Vers_Id,new 
            TabSetting(56),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(56),pnd_System_Desc,NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(47),
            pnd_Hdr_Desc,pnd_Cf_Txt,NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr,"PAYMENT/NUMBER",
        		pnd_Pymnt_Nbr,"Geo/Cde",
        		pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde," /Payee",
        		pnd_Common_Fields_Pnd_Pymnt_Nme,"Payment/Date",
        		pnd_Common_Fields_Pnd_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YY")," /Net Payment",
        		pnd_Common_Fields_Pnd_Pymnt_Check_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Origin/Code",
        		pnd_Common_Fields_Pnd_Cntrct_Orgn_Cde,"CSR ACTIVITY/CODE",
        		pnd_Common_Fields_Pnd_Csr_Activity_Cde);
        getReports().setDisplayColumns(2, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pnd_Common_Fields_Pnd_Cntrct_Ppcn_Nbr,"PAYMENT/NUMBER",
        		pnd_Pymnt_Nbr,"Geo/Cde",
        		pnd_Common_Fields_Pnd_Annt_Rsdncy_Cde," /Payee",
        		pnd_Common_Fields_Pnd_Pymnt_Nme,"Payment/Date",
        		pnd_Common_Fields_Pnd_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YY")," /Net Payment",
        		pnd_Common_Fields_Pnd_Pymnt_Check_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CSR ACTIVITY/CODE",
        		pnd_Common_Fields_Pnd_Csr_Activity_Cde);
    }
    private void CheckAtStartofData857() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*  12/14/99
            pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.setValue(pdaFcpaext.getExt_Pymnt_Ftre_Ind());                                                                            //Natural: MOVE EXT.PYMNT-FTRE-IND TO #PYMNT-FTRE-IND #WS-SAVE-FTRE-IND
            pnd_Ws_Save_Ftre_Ind.setValue(pdaFcpaext.getExt_Pymnt_Ftre_Ind());
            //*  12/14/99
            pnd_Ws_Pay_Type_Req_Ind.setValue(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind());                                                                                 //Natural: MOVE EXT.PYMNT-PAY-TYPE-REQ-IND TO #WS-PAY-TYPE-REQ-IND
            //*  12/14/99
            if (condition(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                                                          //Natural: IF EXT.PYMNT-PAY-TYPE-REQ-IND = 8
            {
                pnd_Int_Roll_Mode.setValue(true);                                                                                                                         //Natural: ASSIGN #INT-ROLL-MODE := TRUE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REPORT-HDR
            sub_Determine_Report_Hdr();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
    private void CheckAtStartofData907() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Common_Fields_Pnd_Pymnt_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());                                                                   //Natural: MOVE #RPT-EXT.PYMNT-FTRE-IND TO #PYMNT-FTRE-IND #WS-SAVE-FTRE-IND
            pnd_Ws_Save_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());
            //*  05-18-98
            pnd_Ws_Pay_Type_Req_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind());                                                                        //Natural: MOVE #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND TO #WS-PAY-TYPE-REQ-IND
            //*  05-18-98
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                                                 //Natural: IF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 8
            {
                pnd_Int_Roll_Mode.setValue(true);                                                                                                                         //Natural: ASSIGN #INT-ROLL-MODE := TRUE
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-REPORT-HDR
            sub_Determine_Report_Hdr();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
