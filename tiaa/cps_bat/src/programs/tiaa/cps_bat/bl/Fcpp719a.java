/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:34 PM
**        * FROM NATURAL PROGRAM : Fcpp719a
************************************************************
**        * FILE NAME            : Fcpp719a.java
**        * CLASS NAME           : Fcpp719a
**        * INSTANCE NAME        : Fcpp719a
************************************************************
************************************************************************
* PROGRAM  : FCPP719A
* SYSTEM   : CPS
* TITLE    : CONDITIONALLY END NATURAL SESSION AND BYPASS NEXT PROGRAMS
* WRITTEN  : FEB 23, 1994
* FUNCTION : THE PROGRAM READS A CONTROL CARD, TELLING IT WHETHER
*            THE PROGRAM FOLLOWING IT WILL RUN CHECKS OR EFT.
*            FCPP384 INSPECTS THE SPLIT CONTROL RECORD (WORKFILE) AND
*            DETERMINES IF THERE IS INPUT FOR THE REQUEST OR IS IT AN
*            EMPTY INPUT FILE.  IF THE INPUT IS EMPTY THE PROGRAM WILL
*            CLOSE THE NATURAL SESSION THUS BYPASSING THE EXECUTION OF
*            THE NEXT PROGRAM IN STREAM.
**--------------------------------------------------------------------**
* HISTORY
* ---------+------------------------------------------------------
* 02-11-00 : A. YOUNG - AVOID BY-PASSING UPDATE MODULE WHEN ONLY NON-
*          : PAYMENTS PROCESSED (I.E., CANCELS/STOPS WITH NO REDRAWS)
* ---------+------------------------------------------------------
* 06/27/00 : J HOFSTETTER - ADD PA-SELECT FIELDS TO #WS-WORK-RECORD
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp719a extends BLNatBase
{
    // Data Areas
    private LdaFcpl720a ldaFcpl720a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Work_Record;

    private DbsGroup pnd_Ws_Work_Record__R_Field_1;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Header_Level1;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Filler_A;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Filler_B;

    private DbsGroup pnd_Ws_Work_Record__R_Field_2;

    private DbsGroup pnd_Ws_Work_Record_Pnd_Ws_Header_Level2;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Work_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Work_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Work_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Work_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Work_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Work_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Work_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Work_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Work_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Work_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Work_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Work_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Work_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Work_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Work_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Work_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Work_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Work_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Work_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Work_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Work_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Work_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Work_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Work_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Work_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Work_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Work_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Work_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Work_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Work_Record__R_Field_3;
    private DbsField pnd_Ws_Work_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Work_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Work_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Work_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Work_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Work_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Work_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Work_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Work_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Work_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Work_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Work_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Work_Record_Filler;
    private DbsField pnd_Ws_Work_Record_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Ws_Work_Record_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ws_Work_Record_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ws_Work_Record_Cntrct_Insurance_Option;
    private DbsField pnd_Ws_Work_Record_Cntrct_Life_Contingency;
    private DbsField pnd_Ws_Work_Record_Pnd_Ws_Header_Filler;
    private DbsField pnd_Cs_No_Redraws;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Rrp_Request;
    private DbsField pnd_Ws_Pnd_Disp_Text;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl720a = new LdaFcpl720a();
        registerRecord(ldaFcpl720a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Ws_Work_Record = localVariables.newFieldArrayInRecord("pnd_Ws_Work_Record", "#WS-WORK-RECORD", FieldType.STRING, 1, new DbsArrayController(1, 
            500));

        pnd_Ws_Work_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Work_Record__R_Field_1", "REDEFINE", pnd_Ws_Work_Record);
        pnd_Ws_Work_Record_Pnd_Ws_Header_Level1 = pnd_Ws_Work_Record__R_Field_1.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Header_Level1", "#WS-HEADER-LEVEL1", 
            FieldType.STRING, 41);
        pnd_Ws_Work_Record_Pnd_Ws_Filler_A = pnd_Ws_Work_Record__R_Field_1.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Filler_A", "#WS-FILLER-A", FieldType.STRING, 
            217);
        pnd_Ws_Work_Record_Pnd_Ws_Filler_B = pnd_Ws_Work_Record__R_Field_1.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Filler_B", "#WS-FILLER-B", FieldType.STRING, 
            242);

        pnd_Ws_Work_Record__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Work_Record__R_Field_2", "REDEFINE", pnd_Ws_Work_Record);

        pnd_Ws_Work_Record_Pnd_Ws_Header_Level2 = pnd_Ws_Work_Record__R_Field_2.newGroupInGroup("pnd_Ws_Work_Record_Pnd_Ws_Header_Level2", "#WS-HEADER-LEVEL2");
        pnd_Ws_Work_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Work_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Work_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Work_Record_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Work_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Work_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Work_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Work_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Work_Record_Cntrct_Payee_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Work_Record_Cntrct_Invrse_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Work_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Crrncy_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Orgn_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Work_Record_Cntrct_Qlfied_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Sps_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Stats_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Annot_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Cmbne_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Ftre_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Payee_Na_Addr_Trggr", 
            "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Dvdnd_Payee_Ind", 
            "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Work_Record_Annt_Soc_Sec_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Work_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Work_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Rqst_Settl_Dte", 
            "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Ws_Work_Record_Cntrct_Rqst_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", 
            FieldType.DATE);
        pnd_Ws_Work_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Rqst_Settl_Tme", 
            "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pnd_Ws_Work_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Ws_Work_Record_Cntrct_Type_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Work_Record_Cntrct_Lob_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Work_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Work_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Work_Record_Cntrct_Cref_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Work_Record_Cntrct_Option_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Record_Cntrct_Mode_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Work_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Pymnt_Dest_Cde", 
            "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Work_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Work_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Dvdnd_Payee_Cde", 
            "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pnd_Ws_Work_Record_Cntrct_Hold_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Work_Record_Cntrct_Hold_Grp = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        pnd_Ws_Work_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Work_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Work_Record_Annt_Ctznshp_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Record_Annt_Rsdncy_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Work_Record_Pymnt_Split_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Work_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Split_Reasn_Cde", 
            "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pnd_Ws_Work_Record_Pymnt_Check_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Ws_Work_Record_Pymnt_Cycle_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Ws_Work_Record_Pymnt_Eft_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", 
            FieldType.DATE);
        pnd_Ws_Work_Record_Pymnt_Rqst_Pct = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Work_Record_Pymnt_Rqst_Amt = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Work_Record_Pymnt_Check_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);

        pnd_Ws_Work_Record__R_Field_3 = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newGroupInGroup("pnd_Ws_Work_Record__R_Field_3", "REDEFINE", pnd_Ws_Work_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Work_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Work_Record__R_Field_3.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Record_Pymnt_Instmt_Nbr = pnd_Ws_Work_Record__R_Field_3.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Check_Scrty_Nbr", 
            "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Work_Record_Pymnt_Check_Amt = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Work_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        pnd_Ws_Work_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Work_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Ec_Oprtr_Id_Nbr", 
            "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pnd_Ws_Work_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Work_Record_Cntrct_Hold_Tme = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Ws_Work_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Cancel_Rdrw_Ind", 
            "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pymnt_Acctg_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Ws_Work_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Reqst_Log_Dte_Time", 
            "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Ws_Work_Record_Pymnt_Intrfce_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Ws_Work_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Work_Record_Filler = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Filler", "FILLER", FieldType.STRING, 4);
        pnd_Ws_Work_Record_Cnr_Orgnl_Invrse_Dte = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Work_Record_Cntrct_Annty_Ins_Type = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Annty_Ins_Type", 
            "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Annty_Type_Cde = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Annty_Type_Cde", 
            "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Insurance_Option = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Insurance_Option", 
            "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Cntrct_Life_Contingency = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Cntrct_Life_Contingency", 
            "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 1);
        pnd_Ws_Work_Record_Pnd_Ws_Header_Filler = pnd_Ws_Work_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Work_Record_Pnd_Ws_Header_Filler", "#WS-HEADER-FILLER", 
            FieldType.STRING, 117);
        pnd_Cs_No_Redraws = localVariables.newFieldInRecord("pnd_Cs_No_Redraws", "#CS-NO-REDRAWS", FieldType.BOOLEAN, 1);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Rrp_Request = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rrp_Request", "#RRP-REQUEST", FieldType.STRING, 10);
        pnd_Ws_Pnd_Disp_Text = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Text", "#DISP-TEXT", FieldType.STRING, 60);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl720a.initializeValues();

        localVariables.reset();
        pnd_Cs_No_Redraws.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp719a() throws Exception
    {
        super("Fcpp719a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ---------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 80 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 32T 'PROCESS' #FCPL720A.#CNTRCT-ORGN-CDE #RRP-REQUEST 68T 'REPORT: RPT0' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        getWorkFiles().read(2, pnd_Ws_Pnd_Rrp_Request);                                                                                                                   //Natural: READ WORK FILE 2 ONCE #RRP-REQUEST
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING PARAMETER FILE",new TabSetting(77),"***");                                                            //Natural: WRITE '***' 25T 'MISSING PARAMETER FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  02-11-2000
        //*  02-11-2000
        getWorkFiles().read(8, pnd_Ws_Work_Record.getValue("*"));                                                                                                         //Natural: READ WORK 8 ONCE #WS-WORK-RECORD ( * )
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, "***",new TabSetting(25),"No records were processed for source code",ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde(),new              //Natural: WRITE '***' 25T 'No records were processed for source code' #FCPL720A.#CNTRCT-ORGN-CDE 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  02-11-2000
        if (condition(pnd_Ws_Work_Record_Pnd_Ws_Record_Level_Nmbr.notEquals(10)))                                                                                         //Natural: IF #WS-WORK-RECORD.#WS-RECORD-LEVEL-NMBR NE 10
        {
            getReports().write(0, Global.getPROGRAM(),"*** WARNING ***",NEWLINE,"REASON: The extract file must start with a record type 10",NEWLINE,"however it starts with:", //Natural: WRITE *PROGRAM '*** WARNING ***' / 'REASON: The extract file must start with a record type 10' / 'however it starts with:' / '=' #WS-WORK-RECORD.#WS-RECORD-LEVEL-NMBR
                NEWLINE,"=",pnd_Ws_Work_Record_Pnd_Ws_Record_Level_Nmbr);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ws_Work_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN") || pnd_Ws_Work_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN")))                //Natural: IF #WS-WORK-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN' OR = 'SN'
            {
                pnd_Cs_No_Redraws.setValue(true);                                                                                                                         //Natural: ASSIGN #CS-NO-REDRAWS := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ----------------------------
        //*  02-11-2000
        if (condition(pnd_Cs_No_Redraws.equals(false)))                                                                                                                   //Natural: IF #CS-NO-REDRAWS = FALSE
        {
            getWorkFiles().read(5, ldaFcpl720a.getPnd_Fcpl720a());                                                                                                        //Natural: READ WORK FILE 5 ONCE #FCPL720A
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"SPLIT CONTROL FILE IS EMPTY",new TabSetting(77),"***");                                                   //Natural: WRITE '***' 25T 'SPLIT CONTROL FILE IS EMPTY' 77T '***'
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(51);  if (true) return;                                                                                                                 //Natural: TERMINATE 51
            }                                                                                                                                                             //Natural: END-ENDFILE
            getReports().write(0, "Content of Split Control File:");                                                                                                      //Natural: WRITE 'Content of Split Control File:'
            if (Global.isEscape()) return;
            FOR01:                                                                                                                                                        //Natural: FOR #CNTL-IDX = 1 TO 4
            for (ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(1); condition(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().lessOrEqual(4)); ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().nadd(1))
            {
                getReports().write(0, NEWLINE,ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Text().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx()));                          //Natural: WRITE / #CNTL-TEXT ( #CNTL-IDX )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                FOR02:                                                                                                                                                    //Natural: FOR #REC-TYPE-IDX = 0 TO 3
                for (ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().setValue(0); condition(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().lessOrEqual(3)); 
                    ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().nadd(1))
                {
                    getReports().write(0, new TabSetting(17),ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Types_Text().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().getInt() + 1),ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().getInt() + 1,ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx()),  //Natural: WRITE 17T #REC-TYPES-TEXT ( #REC-TYPE-IDX ) #REC-TYPE-CNT ( #CNTL-IDX,#REC-TYPE-IDX ) ( EM = -Z,ZZZ,ZZ9 )
                        new ReportEditMask ("-Z,ZZZ,ZZ9"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            short decideConditionsMet193 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #RRP-REQUEST;//Natural: VALUE 'CHECK', 'CHECKS'
            if (condition((pnd_Ws_Pnd_Rrp_Request.equals("CHECK") || pnd_Ws_Pnd_Rrp_Request.equals("CHECKS"))))
            {
                decideConditionsMet193++;
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(3);                                                                                                   //Natural: ASSIGN #CNTL-IDX := 3
            }                                                                                                                                                             //Natural: VALUE 'EFT', 'EFTS'
            else if (condition((pnd_Ws_Pnd_Rrp_Request.equals("EFT") || pnd_Ws_Pnd_Rrp_Request.equals("EFTS"))))
            {
                decideConditionsMet193++;
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(4);                                                                                                   //Natural: ASSIGN #CNTL-IDX := 4
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"INVALID RUN REQUEST:",pnd_Ws_Pnd_Rrp_Request,new TabSetting(77),"***");                                   //Natural: WRITE '***' 25T 'INVALID RUN REQUEST:' #RRP-REQUEST 77T '***'
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(52);  if (true) return;                                                                                                                 //Natural: TERMINATE 52
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().getInt() + 1,0).equals(getZero())))          //Natural: IF #REC-TYPE-CNT ( #CNTL-IDX,0 ) = 0
            {
                pnd_Ws_Pnd_Disp_Text.setValue(DbsUtil.compress(pnd_Ws_Pnd_Rrp_Request, "RUN BYPASSED DUE TO EMPTY INPUT"));                                               //Natural: COMPRESS #RRP-REQUEST 'RUN BYPASSED DUE TO EMPTY INPUT' INTO #DISP-TEXT
                getReports().write(0, NEWLINE,NEWLINE,NEWLINE,pnd_Ws_Pnd_Disp_Text);                                                                                      //Natural: WRITE /// #DISP-TEXT
                if (Global.isEscape()) return;
                DbsUtil.terminate(0);  if (true) return;                                                                                                                  //Natural: TERMINATE 0
            }                                                                                                                                                             //Natural: END-IF
            //*  02-11-2000
        }                                                                                                                                                                 //Natural: END-IF
        //*  ----------------------------
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=80 ZP=ON");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(32),"PROCESS",ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde(),pnd_Ws_Pnd_Rrp_Request,new TabSetting(68),"REPORT: RPT0",NEWLINE,
            NEWLINE);
    }
}
