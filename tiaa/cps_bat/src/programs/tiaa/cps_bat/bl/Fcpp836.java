/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:46 PM
**        * FROM NATURAL PROGRAM : Fcpp836
************************************************************
**        * FILE NAME            : Fcpp836.java
**        * CLASS NAME           : Fcpp836
**        * INSTANCE NAME        : Fcpp836
************************************************************
*********************** RL PAYEE MATCH FEB 22, 2006 ********************
* PROGRAM  : FCPP836
* SYSTEM   : CPS
* TITLE    : CHECK UPDATE
* FUNCTION : THIS PROGRAM WILL UPDATE ADABAS PAYMENT FILE WITH
*            CHECK NUMBER AND CHECK SEQUENCE NUMBER.(AP CANCEL & REDRAW)
* HISTORY  :
*          : 01/25/1999  RITA SALGADO
*          : - CREATE ANNOTATION RECORD FOR EXPEDITED HOLDS (OV00;USPS)
*          :
*          : 08/05/99 - LEON GURTOVNIK
*          :  MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*          :  NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
* 02-16-00 : ALTHEA A. YOUNG
*          : REVISED TO PREVENT A DUPLICATE ANNOTATION RECORD FROM BEING
*          : CREATED FOR CANCELLED / STOPPED RECORDS WITH 'OV00' OR
*          : 'USPS' HOLD CODES.
*          :
* 01-02-01 : ROXAN CARREON
*          : PRIOR CHANGES FAILED IT's purpose of preventing duplicates.
*          : THIS REVISION WILL ALLOW UPDATES FOR EXISTING ANNOTATION
*          : RECORDS FOR PAYMENTS WITH OV00 AND USPS THUS PREVENTING
*          : DUPLICATES.
*          : DEFINE ANNOTATION KEY FOR RETRIEVAL OF ANNOT RECORD
*    11-02 : R CARREON   STOW - EGTRRA CHANGES IN LDA
*    05-03 : R CARREON   STOW - DUE TO NEW FCPA800
*          :
* R. LANDRUM    02/22/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
*
* 5/2/08   : NEEDS RESTOW TO PICK UP NAT 3 VERSION OF FCPCUSPS - AER
* 5/15/08  : ROTH-MAJOR1 - AER - CHANGED FCPA800 TO FCPA800D
* 04/20/10 -  CM - RESTOW FOR CHANGE IN FCPLPMNU
* 20140823 F.ENDAYA  PH RECEIVABLE. ADD TWO NEW CODES AP AND AS AS VALID
* FE201408           CSR CODE.
*  4/2017  : JJG - PIN EXPANSION, RESTOW PICK CHANGE TO FCPA800D
**********************************************************************
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp836 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800d pdaFcpa800d;
    private LdaFcplpmnu ldaFcplpmnu;
    private LdaFcplannu ldaFcplannu;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpaacum pdaFcpaacum;
    private PdaFcpacrpt pdaFcpacrpt;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_S__R_Field_1;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Prev_Hold_Cde;
    private DbsField pnd_Ws_Pnd_First_Check;
    private DbsField pnd_Ws_Pnd_Last_Check;
    private DbsField pnd_Ws_Pnd_Chk_Miss_Start;
    private DbsField pnd_Ws_Pnd_Chk_Miss_End;
    private DbsField pnd_Ws_Pnd_Missing_Checks;
    private DbsField pnd_Ws_Pnd_Missing_Checks_1;
    private DbsField pnd_Ws_Pnd_Rec_Updated;
    private DbsField pnd_Ws_Pnd_Update_Cnt;
    private DbsField pnd_Ws_Pnd_Et_Cnt;
    private DbsField pnd_Ws_Pnd_Pymnt;
    private DbsField pnd_Ws_Pnd_First_Check_Ind;
    private DbsField pnd_Ws_Pnd_Pymnt_Not_Found;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Annot_Key;

    private DbsGroup pnd_Ws_Annot_Key__R_Field_3;

    private DbsGroup pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        ldaFcplpmnu = new LdaFcplpmnu();
        registerRecord(ldaFcplpmnu);
        registerRecord(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());
        ldaFcplannu = new LdaFcplannu();
        registerRecord(ldaFcplannu);
        registerRecord(ldaFcplannu.getVw_fcp_Cons_Annu());
        pdaFcpa110 = new PdaFcpa110(localVariables);
        pdaFcpaacum = new PdaFcpaacum(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);

        // Local Variables

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_1", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_1.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 29);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Prev_Hold_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Hold_Cde", "#PREV-HOLD-CDE", FieldType.STRING, 4);
        pnd_Ws_Pnd_First_Check = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Check", "#FIRST-CHECK", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Last_Check = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Last_Check", "#LAST-CHECK", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Chk_Miss_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Chk_Miss_Start", "#CHK-MISS-START", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Chk_Miss_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Chk_Miss_End", "#CHK-MISS-END", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Missing_Checks = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Missing_Checks", "#MISSING-CHECKS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Missing_Checks_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Missing_Checks_1", "#MISSING-CHECKS-1", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Rec_Updated = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Updated", "#REC-UPDATED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Update_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Update_Cnt", "#UPDATE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Et_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Pymnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt", "#PYMNT", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_First_Check_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Check_Ind", "#FIRST-CHECK-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Pymnt_Not_Found = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Not_Found", "#PYMNT-NOT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Annot_Key = localVariables.newFieldInRecord("pnd_Ws_Annot_Key", "#WS-ANNOT-KEY", FieldType.STRING, 29);

        pnd_Ws_Annot_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Annot_Key__R_Field_3", "REDEFINE", pnd_Ws_Annot_Key);

        pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields = pnd_Ws_Annot_Key__R_Field_3.newGroupInGroup("pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields", "#WS-ANNOT-KEY-FIELDS");
        pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Annot_Key_Cntrct_Invrse_Dte = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Annot_Key_Cntrct_Orgn_Cde = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmnu.initializeValues();
        ldaFcplannu.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Et_Cnt.setInitialValue(150);
        pnd_Ws_Pnd_First_Check_Ind.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp836() throws Exception
    {
        super("Fcpp836");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 15 ) PS = 58 LS = 133 ZP = ON;//Natural: WRITE ( 2 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 39T 'ANNUITY PAYMENTS CANCEL AND REDRAW UPDATE CONTROL REPORT' 120T 'REPORT: RPT2' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: ASSIGN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) := #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 ) := #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 ) := TRUE
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).setValue(true);
        //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            pnd_Pymnt_S.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                              //Natural: MOVE BY NAME WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO TO #PYMNT-S
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 1
            {
                                                                                                                                                                          //Natural: PERFORM NEW-PYMNT
                sub_New_Pymnt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  LEON 'CN' 'SN'  08/06/99
            //*  JWO 2010-11
            //*  FE201408
            //*  FE201408  /* JWO 2010-11
            short decideConditionsMet972 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'CN' OR = 'CP' OR = 'RP' OR = 'PR' OR = 'AP'
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AP")))
            {
                decideConditionsMet972++;
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(19);                                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 19
            }                                                                                                                                                             //Natural: WHEN WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'S' OR = 'SN' OR = 'SP' OR = 'AS'
            else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AS")))
            {
                decideConditionsMet972++;
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(20);                                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 20
            }                                                                                                                                                             //Natural: WHEN WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
            else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))
            {
                decideConditionsMet972++;
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(new DbsDecimal("0.00"))))                                                         //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT = 0.00
                {
                    pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(15);                                                                                           //Natural: ASSIGN #ACCUM-OCCUR := 15
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(1);                                                                                            //Natural: ASSIGN #ACCUM-OCCUR := 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet972 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                sub_Accum_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(false);                                                                                          //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := FALSE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM UPDATE-PYMNT-FILE
            sub_Update_Pymnt_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Rec_Updated.nadd(pnd_Ws_Pnd_Update_Cnt);                                                                                                               //Natural: ADD #UPDATE-CNT TO #REC-UPDATED
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(2, "STARTING CHECK NUMBER.......  ",pnd_Ws_Pnd_First_Check,NEWLINE,"ENDING   CHECK NUMBER.......  ",pnd_Ws_Pnd_Last_Check,NEWLINE,"TOTAL MISSING CHECKS........",pnd_Ws_Pnd_Missing_Checks,  //Natural: WRITE ( 2 ) 'STARTING CHECK NUMBER.......  ' #FIRST-CHECK / 'ENDING   CHECK NUMBER.......  ' #LAST-CHECK / 'TOTAL MISSING CHECKS........' #MISSING-CHECKS / 'TOTAL CHECKS................' #FCPAACUM.#PYMNT-CNT ( 1 ) / 'TOTAL ZERO CHECKS...........' #FCPAACUM.#PYMNT-CNT ( 15 ) / 'RECORDS UPDATED.............' #REC-UPDATED
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"TOTAL CHECKS................",pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(1),NEWLINE,"TOTAL ZERO CHECKS...........",pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(15),NEWLINE,"RECORDS UPDATED.............",pnd_Ws_Pnd_Rec_Updated, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                       //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1).setValue(true);                                                                                         //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1 ) := #FCPACRPT.#TRUTH-TABLE ( 15 ) := #FCPACRPT.#TRUTH-TABLE ( 19 ) := #FCPACRPT.#TRUTH-TABLE ( 20 ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(15).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(19).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(20).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("   Annuity Payments CSR Batch Update");                                                                         //Natural: ASSIGN #FCPACRPT.#TITLE := '   Annuity Payments CSR Batch Update'
        DbsUtil.callnat(Fcpncrpt.class , getCurrentProcessState(), pdaFcpaacum.getPnd_Fcpaacum(), pdaFcpacrpt.getPnd_Fcpacrpt());                                         //Natural: CALLNAT 'FCPNCRPT' USING #FCPAACUM #FCPACRPT
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Ws_Pnd_Terminate.getBoolean()))                                                                                                                 //Natural: IF #TERMINATE
        {
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PYMNT-FILE
        //* **********************************
        //* ************************** RL END-PAYEE MATCH *************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-PYMNT
        //* **************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MISSING-CHECKS
        //* *******************************
        //* *********************** RL BEGIN - PAYEE MATCH ************FEB 22,2006
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //* ************************** RL END-PAYEE MATCH *************************
        //* ***********************************************************************
        //*  COPYCODE   : FCPCACUM
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : "AP" - CONTROL ACCUMULATIONS.
        //* ***********************************************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-TOTALS
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ANNOT-FOR-HOLD
    }
    private void sub_Update_Pymnt_File() throws Exception                                                                                                                 //Natural: UPDATE-PYMNT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Pymnt_Not_Found.setValue(true);                                                                                                                        //Natural: ASSIGN #PYMNT-NOT-FOUND := TRUE
        ldaFcplpmnu.getVw_fcp_Cons_Pymnu().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-PYMNU BY PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE
        (
        "READ02",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        READ02:
        while (condition(ldaFcplpmnu.getVw_fcp_Cons_Pymnu().readNextRow("READ02")))
        {
            if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Ppcn_Nbr().equals(pnd_Pymnt_S_Cntrct_Ppcn_Nbr) && ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Invrse_Dte().equals(pnd_Pymnt_S_Cntrct_Invrse_Dte)  //Natural: IF FCP-CONS-PYMNU.CNTRCT-PPCN-NBR = #PYMNT-S.CNTRCT-PPCN-NBR AND FCP-CONS-PYMNU.CNTRCT-INVRSE-DTE = #PYMNT-S.CNTRCT-INVRSE-DTE AND FCP-CONS-PYMNU.CNTRCT-ORGN-CDE = #PYMNT-S.CNTRCT-ORGN-CDE AND FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NBR = #PYMNT-S.PYMNT-PRCSS-SEQ-NBR
                && ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Orgn_Cde().equals(pnd_Pymnt_S_Cntrct_Orgn_Cde) && ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr().equals(pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Pymnt_Not_Found.setValue(false);                                                                                                                   //Natural: ASSIGN #PYMNT-NOT-FOUND := FALSE
            if (condition(pnd_Ws_Pnd_Pymnt.getBoolean()))                                                                                                                 //Natural: IF #PYMNT
            {
                //* *********************** RL BEGIN - PAYEE MATCH ************ MAY 10,2006
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                          //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1
                {
                    pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                    pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                     //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Nbr().setValue(pnd_Ws_Check_Nbr_N10);                                                                             //Natural: MOVE #WS-CHECK-NBR-N10 TO FCP-CONS-PYMNU.PYMNT-NBR
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr().compute(new ComputeParameters(false, ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr()),                //Natural: COMPUTE FCP-CONS-PYMNU.PYMNT-CHECK-NBR = WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR * -1
                        pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().multiply(-1));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                         //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-NBR := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
                }                                                                                                                                                         //Natural: END-IF
                ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr());                                 //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-SCRTY-NBR := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR
                ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Seq_Nbr());                                     //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-SEQ-NBR := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SEQ-NBR
                ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Amt().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt());                                             //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-AMT := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT
                //*  02-16-2000
                if (condition((ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind().equals(1) && (ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")        //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1 AND FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
                    || ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))))
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-ANNOT-FOR-HOLD
                    sub_Create_Annot_For_Hold();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Stats_Cde().setValue("P");                                                                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-STATS-CDE := 'P'
            pnd_Ws_Pnd_Update_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #UPDATE-CNT
            ldaFcplpmnu.getVw_fcp_Cons_Pymnu().updateDBRow("READ02");                                                                                                     //Natural: UPDATE
            if (condition(pnd_Ws_Pnd_Update_Cnt.greaterOrEqual(pnd_Ws_Pnd_Et_Cnt)))                                                                                       //Natural: IF #UPDATE-CNT GE #ET-CNT
            {
                pnd_Ws_Pnd_Rec_Updated.nadd(pnd_Ws_Pnd_Update_Cnt);                                                                                                       //Natural: ADD #UPDATE-CNT TO #REC-UPDATED
                pnd_Ws_Pnd_Update_Cnt.reset();                                                                                                                            //Natural: RESET #UPDATE-CNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Pymnt_Not_Found.getBoolean()))                                                                                                           //Natural: IF #PYMNT-NOT-FOUND
        {
            pnd_Ws_Pnd_Terminate.setValue(true);                                                                                                                          //Natural: ASSIGN #TERMINATE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"PAYMENT RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIAA PPCN#  :",pnd_Pymnt_S_Cntrct_Ppcn_Nbr,new  //Natural: WRITE '***' 25T 'PAYMENT RECORD IS NOT FOUND' 77T '***' / '***' 25T 'TIAA PPCN#  :' #PYMNT-S.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'INVERSE DATE:' #PYMNT-S.CNTRCT-INVRSE-DTE 77T '***' / '***' 25T 'ORIGIN      :' #PYMNT-S.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'SEQUENCE#   :' #PYMNT-S.PYMNT-PRCSS-SEQ-NBR 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"INVERSE DATE:",pnd_Pymnt_S_Cntrct_Invrse_Dte,new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"ORIGIN      :",pnd_Pymnt_S_Cntrct_Orgn_Cde,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"SEQUENCE#   :",pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr,new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_New_Pymnt() throws Exception                                                                                                                         //Natural: NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := TRUE
        //*  LEON 08-06-99
        //*  JWO 2010-11
        //*  FE201408
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S")  //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR' OR = 'AS' OR = 'AP'
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AS") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AP")))
        {
            pnd_Ws_Pnd_Pymnt.setValue(false);                                                                                                                             //Natural: ASSIGN #PYMNT := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Pymnt.setValue(true);                                                                                                                              //Natural: ASSIGN #PYMNT := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(pnd_Ws_Pnd_Pymnt.getBoolean() && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().greaterOrEqual(1) && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().lessOrEqual(9999998))) //Natural: IF #PYMNT AND WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR = 1 THRU 9999998
        {
            short decideConditionsMet1103 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FIRST-CHECK-IND
            if (condition(pnd_Ws_Pnd_First_Check_Ind.getBoolean()))
            {
                decideConditionsMet1103++;
                pnd_Ws_Pnd_First_Check_Ind.setValue(false);                                                                                                               //Natural: ASSIGN #FIRST-CHECK-IND := FALSE
                pnd_Ws_Pnd_First_Check.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                      //Natural: ASSIGN #FIRST-CHECK := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: WHEN #PREV-HOLD-CDE NE ' ' AND WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = ' '
            else if (condition(pnd_Ws_Pnd_Prev_Hold_Cde.notEquals(" ") && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(" ")))
            {
                decideConditionsMet1103++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ( #LAST-CHECK + 1 ) NE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
            else if (condition(pnd_Ws_Pnd_Last_Check.add(1).notEquals(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr())))
            {
                decideConditionsMet1103++;
                                                                                                                                                                          //Natural: PERFORM MISSING-CHECKS
                sub_Missing_Checks();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Ws_Pnd_Last_Check.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                           //Natural: ASSIGN #LAST-CHECK := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
            pnd_Ws_Pnd_Prev_Hold_Cde.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());                                                                        //Natural: ASSIGN #PREV-HOLD-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Missing_Checks() throws Exception                                                                                                                    //Natural: MISSING-CHECKS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Chk_Miss_Start.compute(new ComputeParameters(false, pnd_Ws_Pnd_Chk_Miss_Start), pnd_Ws_Pnd_Last_Check.add(1));                                         //Natural: ASSIGN #CHK-MISS-START := #LAST-CHECK + 1
        pnd_Ws_Pnd_Chk_Miss_End.compute(new ComputeParameters(false, pnd_Ws_Pnd_Chk_Miss_End), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().subtract(1));           //Natural: ASSIGN #CHK-MISS-END := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR - 1
        pnd_Ws_Pnd_Missing_Checks_1.compute(new ComputeParameters(false, pnd_Ws_Pnd_Missing_Checks_1), pnd_Ws_Pnd_Chk_Miss_End.subtract(pnd_Ws_Pnd_Last_Check));          //Natural: ASSIGN #MISSING-CHECKS-1 := #CHK-MISS-END - #LAST-CHECK
        pnd_Ws_Pnd_Missing_Checks.nadd(pnd_Ws_Pnd_Missing_Checks_1);                                                                                                      //Natural: ASSIGN #MISSING-CHECKS := #MISSING-CHECKS + #MISSING-CHECKS-1
        if (condition(pnd_Ws_Pnd_Missing_Checks_1.equals(1)))                                                                                                             //Natural: IF #MISSING-CHECKS-1 = 1
        {
            getReports().write(2, "CHECK ",pnd_Ws_Pnd_Chk_Miss_Start,new ColumnSpacing(18),"IS  MISSING",pnd_Ws_Pnd_Missing_Checks_1, new ReportEditMask                  //Natural: WRITE ( 2 ) 'CHECK ' #CHK-MISS-START 18X 'IS  MISSING' #MISSING-CHECKS-1 'CHECK'
                ("-Z,ZZZ,ZZ9"),"CHECK");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, "CHECKS",pnd_Ws_Pnd_Chk_Miss_Start,"THROUGH",pnd_Ws_Pnd_Chk_Miss_End,"ARE MISSING",pnd_Ws_Pnd_Missing_Checks_1, new                     //Natural: WRITE ( 2 ) 'CHECKS' #CHK-MISS-START 'THROUGH' #CHK-MISS-END 'ARE MISSING' #MISSING-CHECKS-1 'CHECKS'
                ReportEditMask ("-Z,ZZZ,ZZ9"),"CHECKS");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* RL
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
    }
    private void sub_Accum_Control_Totals() throws Exception                                                                                                              //Natural: ACCUM-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  #MAX-OCCUR
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().greaterOrEqual(1) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().lessOrEqual(50)))                  //Natural: IF #ACCUM-OCCUR = 1 THRU 50
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                     //Natural: ASSIGN #@@MAX-FUND := WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        short decideConditionsMet1159 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPAACUM.#NEW-PYMNT-IND
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                  //Natural: ADD 1 TO #FCPAACUM.#PYMNT-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 3 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(3).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Cntrct_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#CNTRCT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 4 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(4).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dvdnd_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DVDND-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 5 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(5).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dci_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DCI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DCI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 6 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(6).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dpi_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DPI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DPI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#NET-PYMNT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 8 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(8).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Fdrl_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#FDRL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 9 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(9).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_State_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#STATE-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 10 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(10).equals(true)))
        {
            decideConditionsMet1159++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Local_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#LOCAL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1159 > 0))
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                    //Natural: ADD 1 TO #FCPAACUM.#REC-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1159 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Create_Annot_For_Hold() throws Exception                                                                                                             //Natural: CREATE-ANNOT-FOR-HOLD
    {
        if (BLNatReinput.isReinput()) return;

        //*   CHECK FOR EXISTENCE OF ANNOTATION RECORD TO PREVENT DUPLICATE - RCC
        if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Annot_Ind().notEquals("Y")))                                                                                    //Natural: IF FCP-CONS-PYMNU.PYMNT-ANNOT-IND NE 'Y'
        {
            ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Annot_Ind().setValue("Y");                                                                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-ANNOT-IND := 'Y'
            ldaFcplannu.getVw_fcp_Cons_Annu().setValuesByName(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());                                                                        //Natural: MOVE BY NAME FCP-CONS-PYMNU TO FCP-CONS-ANNU
            ldaFcplannu.getFcp_Cons_Annu_Cntrct_Rcrd_Typ().setValue("4");                                                                                                 //Natural: ASSIGN FCP-CONS-ANNU.CNTRCT-RCRD-TYP := '4'
            if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")))                                                                                //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00'
            {
                ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "expedited mail: Tracking #"));            //Natural: COMPRESS ' Check sent' *DATU 'expedited mail: Tracking #' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))                                                                            //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'USPS'
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "USPS overnight"));                    //Natural: COMPRESS ' Check sent' *DATU 'USPS overnight' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplannu.getVw_fcp_Cons_Annu().insertDBRow();                                                                                                              //Natural: STORE FCP-CONS-ANNU
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   IF ANNOTATION RECORD EXIST - ROXAN
            pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.setValuesByName(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());                                                                 //Natural: MOVE BY NAME FCP-CONS-PYMNU TO #WS-ANNOT-KEY-FIELDS
            ldaFcplannu.getVw_fcp_Cons_Annu().startDatabaseFind                                                                                                           //Natural: FIND FCP-CONS-ANNU WITH PPCN-INV-ORGN-PRCSS-INST = #WS-ANNOT-KEY
            (
            "PND_PND_L0260",
            new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", "=", pnd_Ws_Annot_Key, WcType.WITH) }
            );
            PND_PND_L0260:
            while (condition(ldaFcplannu.getVw_fcp_Cons_Annu().readNextRow("PND_PND_L0260")))
            {
                ldaFcplannu.getVw_fcp_Cons_Annu().setIfNotFoundControlFlag(false);
                if (condition(ldaFcplannu.getFcp_Cons_Annu_Cntrct_Rcrd_Typ().notEquals("4")))                                                                             //Natural: IF FCP-CONS-ANNU.CNTRCT-RCRD-TYP NE '4'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   KEEP LINE1 IN LINE2 WHEN LINE2 IS EMPTY OTHERWISE OVERWRITE
                if (condition(ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt().equals(" ")))                                                                           //Natural: IF FCP-CONS-ANNU.PYMNT-RMRK-LINE2-TXT = ' '
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt().setValue(ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt());                                    //Natural: ASSIGN FCP-CONS-ANNU.PYMNT-RMRK-LINE2-TXT := FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")))                                                                            //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00'
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "expedited mail: Tracking #"));        //Natural: COMPRESS ' Check sent' *DATU 'expedited mail: Tracking #' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))                                                                        //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'USPS'
                    {
                        ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "USPS overnight"));                //Natural: COMPRESS ' Check sent' *DATU 'USPS overnight' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaFcplannu.getVw_fcp_Cons_Annu().updateDBRow("PND_PND_L0260");                                                                                           //Natural: UPDATE ( ##L0260. )
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(2, "PS=58 LS=132 ZP=ON");
        Global.format(15, "PS=58 LS=133 ZP=ON");

        getReports().write(2, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(39),"ANNUITY PAYMENTS CANCEL AND REDRAW UPDATE CONTROL REPORT",new 
            TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);
    }
}
