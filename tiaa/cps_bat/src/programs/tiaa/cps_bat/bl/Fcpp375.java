/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:08 PM
**        * FROM NATURAL PROGRAM : Fcpp375
************************************************************
**        * FILE NAME            : Fcpp375.java
**        * CLASS NAME           : Fcpp375
**        * INSTANCE NAME        : Fcpp375
************************************************************
**   THIS PROGRAM IS   NOT   CONSTRUCTABLE
**
**
**SAG GENERATOR: BATCH-TIAA                       VERSION: 3.2.2
**SAG TITLE: EXTRACT
**SAG SYSTEM: CPS
**SAG GDA: FCPG000
**SAG USER-DEFINED-LS(0): 133
**SAG USER-DEFINED-PS(0): 060
**SAG REPORT-HEADING(1): CONSOLIDATED PAYMENT SYSTEM
**SAG PRINT-FILE(1): 0
**SAG REPORT-HEADING(2): CANCEL/REDRAW IA  BATCH EXTRACT CONTROL REPORT
**SAG PRINT-FILE(2): 0
**SAG HEADING-LINE: 12000000
**SAG DESCS(1): THIS PROGRAM WILL EXTRACT RECORDS FOR A STATED
**SAG DESCS(2): PERIOD OF TIME.
**SAG PRIMARY-FILE: FCP-CONS-PYMNT
**SAG PRIMARY-KEY: CHK-DTE-ORG-CURR-TYP-SEQ
**SAG SECONDARY-FILE-1: FCP-CONS-ADDR
**SAG SECONDARY-KEY-1: PPCN-PAYEE-ORGN-INVRS-SEQ
**SAG SEC1-RELATED-TO-PRIME-VIA: CHK-DTE-ORG-CURR-TYP-SEQ
************************************************************************
* PROGRAM  : FCPP375
* SYSTEM   : CPS
* TITLE    : EXTRACT
* FUNCTION : THIS PROGRAM WILL EXTRACT RECORDS FOR A STATED
*            PERIOD OF TIME.
*
*
* HISTORY
*    03/99  - RESTOW  R. CARREON
*
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
* 09/11/00 : A. YOUNG    -  RE-COMPILED DUE TO FCPLPMNT.
*    11/02 : R. CARREON  -  STOW. EGTRRA CHANGES IN LDA
*    12/02 : R. CARREON  -  CALL TO FCPN115 CHANGED TO CPWN115
*            ALL REFERENCE TO FCPA115 WAS CHANGED TO CPWA115
*    05/08 : RESTOW FOR ROTH (AER) ROTH-MAJOR1
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
*  4/2017   : JJG PIN EXPANSION
* 01/15/2020: CTS  CPS SUNSET (CHG694525) TAG: CTS-0115
*             MODIFED TO PROCESS STOPS ONLY. IT WILL NOT REISSUE CHECKS.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp375 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private PdaCpwa115 pdaCpwa115;
    private LdaFcpladdr ldaFcpladdr;
    private LdaFcplcntu ldaFcplcntu;
    private LdaFcplpmnt ldaFcplpmnt;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Sec1_Key;

    private DbsGroup pnd_Sec1_Key__R_Field_1;

    private DbsGroup pnd_Sec1_Key_Pnd_Sec1_Key_Structure;
    private DbsField pnd_Sec1_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Sec1_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Sec1_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Sec1_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Sec1_Key_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Sec1_Start_Key;

    private DbsGroup pnd_Sec1_Start_Key__R_Field_2;
    private DbsField pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Prefix;

    private DbsGroup pnd_Sec1_Start_Key__R_Field_3;

    private DbsGroup pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Structure;
    private DbsField pnd_Sec1_Start_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Sec1_Start_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Sec1_Start_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Sec1_Start_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Sec1_Start_Key_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Sec1_End_Key;

    private DbsGroup pnd_Sec1_End_Key__R_Field_4;
    private DbsField pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Prefix;

    private DbsGroup pnd_Sec1_End_Key__R_Field_5;

    private DbsGroup pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Structure;
    private DbsField pnd_Sec1_End_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Sec1_End_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Sec1_End_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Sec1_End_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Sec1_End_Key_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Prime_Sec1_Related_Key;

    private DbsGroup pnd_Prime_Sec1_Related_Key__R_Field_6;

    private DbsGroup pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure;
    private DbsField pnd_Prime_Sec1_Related_Key_Pymnt_Check_Dte;
    private DbsField pnd_Prime_Sec1_Related_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Prime_Sec1_Related_Key_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Prime_Sec1_Related_Key_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Prime_Sec1_Related_Key_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Prime_Sec1_Related_Key__R_Field_7;
    private DbsField pnd_Prime_Sec1_Related_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Prime_Sec1_Related_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Payment_Date;
    private DbsField pnd_Inst_Tiaa_Amt;
    private DbsField pnd_Inst_Cref_Amt;
    private DbsField pnd_Counter;
    private DbsField pnd_Date_Numeric;

    private DbsGroup pnd_Date_Numeric__R_Field_8;
    private DbsField pnd_Date_Numeric_Pnd_Date_Alpha;
    private DbsField pnd_Key_For_Name_Address;

    private DbsGroup pnd_Control_Totals;
    private DbsField pnd_Control_Totals_Pnd_Type_Code;
    private DbsField pnd_Control_Totals_Pnd_Cnt;
    private DbsField pnd_Control_Totals_Pnd_Gross_Amt;
    private DbsField pnd_Control_Totals_Pnd_Net_Amt;

    private DbsGroup pnd_Control_Totals_Difference;
    private DbsField pnd_Control_Totals_Difference_Pnd_Type_Code_Diff;
    private DbsField pnd_Control_Totals_Difference_Pnd_Cnt_Diff;
    private DbsField pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff;
    private DbsField pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff;
    private DbsField pnd_Ws_Prime_Key;

    private DbsGroup pnd_Ws_Prime_Key__R_Field_9;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Prime_Key_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_In_Dex;
    private DbsField pnd_Ci2;
    private DbsField pnd_C;
    private DbsField pnd_D;
    private DbsField pnd_I;
    private DbsField pnd_K;
    private DbsField pnd_L;
    private DbsField pnd_Ws_Deductions_Table;
    private DbsField pnd_Ws_Cntr_Deductions;
    private DbsField pnd_Ws_Total_Lines_Of_Deductions;

    private DbsGroup pnd_Ws_Key;
    private DbsField pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Gross_A;
    private DbsField pnd_Ws_Gross_N;

    private DbsGroup pnd_Ws_Gross_N__R_Field_10;
    private DbsField pnd_Ws_Gross_N_Pnd_Ws_Int;
    private DbsField pnd_Ws_Gross_N_Pnd_Ws_Rem;
    private DbsField pnd_Ws_Type;
    private DbsField pnd_Work_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_11;
    private DbsField pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Seq_Nbr;
    private DbsField pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Installment_Nbr;
    private DbsField pnd_Ws_First_Time;
    private DbsField pnd_Ws_Inv_Acct_Filler;
    private DbsField pnd_Ws_Name_N_Address_Filler;
    private DbsField pnd_Ws_Combine_Nbr;

    private DbsGroup pnd_Ws_Combine_Nbr__R_Field_12;
    private DbsField pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_1;
    private DbsField pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_2;

    private DbsGroup pnd_Ws_Name_N_Address;
    private DbsField pnd_Ws_Name_N_Address_Rcrd_Typ;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Name_N_Address_Ph_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_Last_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_First_Name;
    private DbsField pnd_Ws_Name_N_Address_Ph_Middle_Name;
    private DbsField pnd_Ws_Name_N_Address_Pnd_Cntr_Pymnt_Nme_And_Addr_Grp;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Nme;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Postl_Data;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Dte;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Tme;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Eft_Transit_Id;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Deceased_Nme;
    private DbsField pnd_Ws_Name_N_Address_Cntrct_Hold_Tme;

    private DbsGroup pnd_Ws_Header_Record;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Header_Record__R_Field_13;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Header_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Header_Record_Pnd_Filler;
    private DbsField pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;

    private DbsGroup pnd_Ws_Occurs;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Side;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fed_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Occurs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Occurs_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Deductions;

    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Deductions;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Amt;
    private DbsField pnd_Cnr_Orgnl_Invrse_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        localVariables = new DbsRecord();
        pdaCpwa115 = new PdaCpwa115(localVariables);
        ldaFcpladdr = new LdaFcpladdr();
        registerRecord(ldaFcpladdr);
        registerRecord(ldaFcpladdr.getVw_fcp_Cons_Addr());
        ldaFcplcntu = new LdaFcplcntu();
        registerRecord(ldaFcplcntu);
        registerRecord(ldaFcplcntu.getVw_fcp_Cons_Cntrl());
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());

        // Local Variables
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Sec1_Key = localVariables.newFieldInRecord("pnd_Sec1_Key", "#SEC1-KEY", FieldType.STRING, 31);

        pnd_Sec1_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Sec1_Key__R_Field_1", "REDEFINE", pnd_Sec1_Key);

        pnd_Sec1_Key_Pnd_Sec1_Key_Structure = pnd_Sec1_Key__R_Field_1.newGroupInGroup("pnd_Sec1_Key_Pnd_Sec1_Key_Structure", "#SEC1-KEY-STRUCTURE");
        pnd_Sec1_Key_Cntrct_Ppcn_Nbr = pnd_Sec1_Key_Pnd_Sec1_Key_Structure.newFieldInGroup("pnd_Sec1_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Sec1_Key_Cntrct_Payee_Cde = pnd_Sec1_Key_Pnd_Sec1_Key_Structure.newFieldInGroup("pnd_Sec1_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Sec1_Key_Cntrct_Orgn_Cde = pnd_Sec1_Key_Pnd_Sec1_Key_Structure.newFieldInGroup("pnd_Sec1_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Sec1_Key_Cntrct_Invrse_Dte = pnd_Sec1_Key_Pnd_Sec1_Key_Structure.newFieldInGroup("pnd_Sec1_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Sec1_Key_Pymnt_Prcss_Seq_Nbr = pnd_Sec1_Key_Pnd_Sec1_Key_Structure.newFieldInGroup("pnd_Sec1_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Sec1_Start_Key = localVariables.newFieldInRecord("pnd_Sec1_Start_Key", "#SEC1-START-KEY", FieldType.STRING, 31);

        pnd_Sec1_Start_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Sec1_Start_Key__R_Field_2", "REDEFINE", pnd_Sec1_Start_Key);
        pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Prefix = pnd_Sec1_Start_Key__R_Field_2.newFieldInGroup("pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Prefix", "#SEC1-START-KEY-PREFIX", 
            FieldType.STRING, 17);

        pnd_Sec1_Start_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Sec1_Start_Key__R_Field_3", "REDEFINE", pnd_Sec1_Start_Key);

        pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Structure = pnd_Sec1_Start_Key__R_Field_3.newGroupInGroup("pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Structure", 
            "#SEC1-START-KEY-STRUCTURE");
        pnd_Sec1_Start_Key_Cntrct_Ppcn_Nbr = pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Structure.newFieldInGroup("pnd_Sec1_Start_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Sec1_Start_Key_Cntrct_Payee_Cde = pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Structure.newFieldInGroup("pnd_Sec1_Start_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Sec1_Start_Key_Cntrct_Orgn_Cde = pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Structure.newFieldInGroup("pnd_Sec1_Start_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Sec1_Start_Key_Cntrct_Invrse_Dte = pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Structure.newFieldInGroup("pnd_Sec1_Start_Key_Cntrct_Invrse_Dte", 
            "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Sec1_Start_Key_Pymnt_Prcss_Seq_Nbr = pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Structure.newFieldInGroup("pnd_Sec1_Start_Key_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 7);
        pnd_Sec1_End_Key = localVariables.newFieldInRecord("pnd_Sec1_End_Key", "#SEC1-END-KEY", FieldType.STRING, 31);

        pnd_Sec1_End_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Sec1_End_Key__R_Field_4", "REDEFINE", pnd_Sec1_End_Key);
        pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Prefix = pnd_Sec1_End_Key__R_Field_4.newFieldInGroup("pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Prefix", "#SEC1-END-KEY-PREFIX", 
            FieldType.STRING, 17);

        pnd_Sec1_End_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Sec1_End_Key__R_Field_5", "REDEFINE", pnd_Sec1_End_Key);

        pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Structure = pnd_Sec1_End_Key__R_Field_5.newGroupInGroup("pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Structure", "#SEC1-END-KEY-STRUCTURE");
        pnd_Sec1_End_Key_Cntrct_Ppcn_Nbr = pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Structure.newFieldInGroup("pnd_Sec1_End_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Sec1_End_Key_Cntrct_Payee_Cde = pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Structure.newFieldInGroup("pnd_Sec1_End_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Sec1_End_Key_Cntrct_Orgn_Cde = pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Structure.newFieldInGroup("pnd_Sec1_End_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Sec1_End_Key_Cntrct_Invrse_Dte = pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Structure.newFieldInGroup("pnd_Sec1_End_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Sec1_End_Key_Pymnt_Prcss_Seq_Nbr = pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Structure.newFieldInGroup("pnd_Sec1_End_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Prime_Sec1_Related_Key = localVariables.newFieldInRecord("pnd_Prime_Sec1_Related_Key", "#PRIME-SEC1-RELATED-KEY", FieldType.STRING, 17);

        pnd_Prime_Sec1_Related_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Prime_Sec1_Related_Key__R_Field_6", "REDEFINE", pnd_Prime_Sec1_Related_Key);

        pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure = pnd_Prime_Sec1_Related_Key__R_Field_6.newGroupInGroup("pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure", 
            "#PRIME-SEC1-RELATED-STRUCTURE");
        pnd_Prime_Sec1_Related_Key_Pymnt_Check_Dte = pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure.newFieldInGroup("pnd_Prime_Sec1_Related_Key_Pymnt_Check_Dte", 
            "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Prime_Sec1_Related_Key_Cntrct_Orgn_Cde = pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure.newFieldInGroup("pnd_Prime_Sec1_Related_Key_Cntrct_Orgn_Cde", 
            "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Prime_Sec1_Related_Key_Cntrct_Check_Crrncy_Cde = pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure.newFieldInGroup("pnd_Prime_Sec1_Related_Key_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Prime_Sec1_Related_Key_Pymnt_Pay_Type_Req_Ind = pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure.newFieldInGroup("pnd_Prime_Sec1_Related_Key_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Prime_Sec1_Related_Key_Pymnt_Prcss_Seq_Nbr = pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure.newFieldInGroup("pnd_Prime_Sec1_Related_Key_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Prime_Sec1_Related_Key__R_Field_7 = pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure.newGroupInGroup("pnd_Prime_Sec1_Related_Key__R_Field_7", 
            "REDEFINE", pnd_Prime_Sec1_Related_Key_Pymnt_Prcss_Seq_Nbr);
        pnd_Prime_Sec1_Related_Key_Pymnt_Prcss_Seq_Num = pnd_Prime_Sec1_Related_Key__R_Field_7.newFieldInGroup("pnd_Prime_Sec1_Related_Key_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Prime_Sec1_Related_Key_Pymnt_Instmt_Nbr = pnd_Prime_Sec1_Related_Key__R_Field_7.newFieldInGroup("pnd_Prime_Sec1_Related_Key_Pymnt_Instmt_Nbr", 
            "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Payment_Date = localVariables.newFieldInRecord("pnd_Payment_Date", "#PAYMENT-DATE", FieldType.STRING, 8);
        pnd_Inst_Tiaa_Amt = localVariables.newFieldInRecord("pnd_Inst_Tiaa_Amt", "#INST-TIAA-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Inst_Cref_Amt = localVariables.newFieldInRecord("pnd_Inst_Cref_Amt", "#INST-CREF-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Counter = localVariables.newFieldInRecord("pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 10);
        pnd_Date_Numeric = localVariables.newFieldInRecord("pnd_Date_Numeric", "#DATE-NUMERIC", FieldType.NUMERIC, 8);

        pnd_Date_Numeric__R_Field_8 = localVariables.newGroupInRecord("pnd_Date_Numeric__R_Field_8", "REDEFINE", pnd_Date_Numeric);
        pnd_Date_Numeric_Pnd_Date_Alpha = pnd_Date_Numeric__R_Field_8.newFieldInGroup("pnd_Date_Numeric_Pnd_Date_Alpha", "#DATE-ALPHA", FieldType.STRING, 
            8);
        pnd_Key_For_Name_Address = localVariables.newFieldInRecord("pnd_Key_For_Name_Address", "#KEY-FOR-NAME-ADDRESS", FieldType.STRING, 31);

        pnd_Control_Totals = localVariables.newGroupArrayInRecord("pnd_Control_Totals", "#CONTROL-TOTALS", new DbsArrayController(1, 50));
        pnd_Control_Totals_Pnd_Type_Code = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Type_Code", "#TYPE-CODE", FieldType.STRING, 2);
        pnd_Control_Totals_Pnd_Cnt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Control_Totals_Pnd_Gross_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Control_Totals_Pnd_Net_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 
            2);

        pnd_Control_Totals_Difference = localVariables.newGroupArrayInRecord("pnd_Control_Totals_Difference", "#CONTROL-TOTALS-DIFFERENCE", new DbsArrayController(1, 
            50));
        pnd_Control_Totals_Difference_Pnd_Type_Code_Diff = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Type_Code_Diff", 
            "#TYPE-CODE-DIFF", FieldType.STRING, 2);
        pnd_Control_Totals_Difference_Pnd_Cnt_Diff = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Cnt_Diff", "#CNT-DIFF", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff", 
            "#GROSS-AMT-DIFF", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff = pnd_Control_Totals_Difference.newFieldInGroup("pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff", 
            "#NET-AMT-DIFF", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Prime_Key = localVariables.newFieldInRecord("pnd_Ws_Prime_Key", "#WS-PRIME-KEY", FieldType.STRING, 11);

        pnd_Ws_Prime_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Ws_Prime_Key__R_Field_9", "REDEFINE", pnd_Ws_Prime_Key);
        pnd_Ws_Prime_Key_Cntrct_Orgn_Cde = pnd_Ws_Prime_Key__R_Field_9.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Prime_Key_Pymnt_Stats_Cde = pnd_Ws_Prime_Key__R_Field_9.newFieldInGroup("pnd_Ws_Prime_Key_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Prime_Key_Cntrct_Invrse_Dte = pnd_Ws_Prime_Key__R_Field_9.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_In_Dex = localVariables.newFieldInRecord("pnd_In_Dex", "#IN-DEX", FieldType.INTEGER, 2);
        pnd_Ci2 = localVariables.newFieldInRecord("pnd_Ci2", "#CI2", FieldType.INTEGER, 2);
        pnd_C = localVariables.newFieldInRecord("pnd_C", "#C", FieldType.INTEGER, 2);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);
        pnd_Ws_Deductions_Table = localVariables.newFieldArrayInRecord("pnd_Ws_Deductions_Table", "#WS-DEDUCTIONS-TABLE", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            40));
        pnd_Ws_Cntr_Deductions = localVariables.newFieldInRecord("pnd_Ws_Cntr_Deductions", "#WS-CNTR-DEDUCTIONS", FieldType.NUMERIC, 2);
        pnd_Ws_Total_Lines_Of_Deductions = localVariables.newFieldInRecord("pnd_Ws_Total_Lines_Of_Deductions", "#WS-TOTAL-LINES-OF-DEDUCTIONS", FieldType.NUMERIC, 
            3);

        pnd_Ws_Key = localVariables.newGroupInRecord("pnd_Ws_Key", "#WS-KEY");
        pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 
            2);
        pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 
            2);
        pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex", "#WS-SIMPLEX-DUPLEX-MULTIPLEX", 
            FieldType.STRING, 1);
        pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code", "#WS-SAVE-CONTRACT-HOLD-CODE", 
            FieldType.STRING, 4);
        pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Key.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Gross_A = localVariables.newFieldInRecord("pnd_Ws_Gross_A", "#WS-GROSS-A", FieldType.STRING, 14);
        pnd_Ws_Gross_N = localVariables.newFieldInRecord("pnd_Ws_Gross_N", "#WS-GROSS-N", FieldType.NUMERIC, 11, 2);

        pnd_Ws_Gross_N__R_Field_10 = localVariables.newGroupInRecord("pnd_Ws_Gross_N__R_Field_10", "REDEFINE", pnd_Ws_Gross_N);
        pnd_Ws_Gross_N_Pnd_Ws_Int = pnd_Ws_Gross_N__R_Field_10.newFieldInGroup("pnd_Ws_Gross_N_Pnd_Ws_Int", "#WS-INT", FieldType.NUMERIC, 9);
        pnd_Ws_Gross_N_Pnd_Ws_Rem = pnd_Ws_Gross_N__R_Field_10.newFieldInGroup("pnd_Ws_Gross_N_Pnd_Ws_Rem", "#WS-REM", FieldType.NUMERIC, 2);
        pnd_Ws_Type = localVariables.newFieldInRecord("pnd_Ws_Type", "#WS-TYPE", FieldType.STRING, 19);
        pnd_Work_Pymnt_Prcss_Seq_Nbr = localVariables.newFieldInRecord("pnd_Work_Pymnt_Prcss_Seq_Nbr", "#WORK-PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);

        pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_11 = localVariables.newGroupInRecord("pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_11", "REDEFINE", pnd_Work_Pymnt_Prcss_Seq_Nbr);
        pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Seq_Nbr = pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_11.newFieldInGroup("pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Seq_Nbr", 
            "#WORK-SEQ-NBR", FieldType.NUMERIC, 7);
        pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Installment_Nbr = pnd_Work_Pymnt_Prcss_Seq_Nbr__R_Field_11.newFieldInGroup("pnd_Work_Pymnt_Prcss_Seq_Nbr_Pnd_Work_Installment_Nbr", 
            "#WORK-INSTALLMENT-NBR", FieldType.NUMERIC, 2);
        pnd_Ws_First_Time = localVariables.newFieldInRecord("pnd_Ws_First_Time", "#WS-FIRST-TIME", FieldType.STRING, 1);
        pnd_Ws_Inv_Acct_Filler = localVariables.newFieldInRecord("pnd_Ws_Inv_Acct_Filler", "#WS-INV-ACCT-FILLER", FieldType.STRING, 188);
        pnd_Ws_Name_N_Address_Filler = localVariables.newFieldInRecord("pnd_Ws_Name_N_Address_Filler", "#WS-NAME-N-ADDRESS-FILLER", FieldType.STRING, 
            8);
        pnd_Ws_Combine_Nbr = localVariables.newFieldInRecord("pnd_Ws_Combine_Nbr", "#WS-COMBINE-NBR", FieldType.STRING, 14);

        pnd_Ws_Combine_Nbr__R_Field_12 = localVariables.newGroupInRecord("pnd_Ws_Combine_Nbr__R_Field_12", "REDEFINE", pnd_Ws_Combine_Nbr);
        pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_1 = pnd_Ws_Combine_Nbr__R_Field_12.newFieldInGroup("pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_1", "#WS-COMBINE-NBR-1", 
            FieldType.STRING, 10);
        pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_2 = pnd_Ws_Combine_Nbr__R_Field_12.newFieldInGroup("pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_2", "#WS-COMBINE-NBR-2", 
            FieldType.STRING, 4);

        pnd_Ws_Name_N_Address = localVariables.newGroupInRecord("pnd_Ws_Name_N_Address", "#WS-NAME-N-ADDRESS");
        pnd_Ws_Name_N_Address_Rcrd_Typ = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1);
        pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Name_N_Address_Cntrct_Ppcn_Nbr = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Name_N_Address_Cntrct_Payee_Cde = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Name_N_Address_Cntrct_Invrse_Dte = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7);

        pnd_Ws_Name_N_Address_Ph_Name = pnd_Ws_Name_N_Address.newGroupInGroup("pnd_Ws_Name_N_Address_Ph_Name", "PH-NAME");
        pnd_Ws_Name_N_Address_Ph_Last_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 
            16);
        pnd_Ws_Name_N_Address_Ph_First_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 
            10);
        pnd_Ws_Name_N_Address_Ph_Middle_Name = pnd_Ws_Name_N_Address_Ph_Name.newFieldInGroup("pnd_Ws_Name_N_Address_Ph_Middle_Name", "PH-MIDDLE-NAME", 
            FieldType.STRING, 12);
        pnd_Ws_Name_N_Address_Pnd_Cntr_Pymnt_Nme_And_Addr_Grp = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Pnd_Cntr_Pymnt_Nme_And_Addr_Grp", 
            "#CNTR-PYMNT-NME-AND-ADDR-GRP", FieldType.NUMERIC, 3);
        pnd_Ws_Name_N_Address_Pymnt_Nme = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde", "PYMNT-ADDR-ZIP-CDE", 
            FieldType.STRING, 9, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Postl_Data = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Postl_Data", "PYMNT-POSTL-DATA", 
            FieldType.STRING, 32, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind", "PYMNT-ADDR-TYPE-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Dte = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Dte", "PYMNT-ADDR-LAST-CHG-DTE", 
            FieldType.NUMERIC, 8, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Tme = pnd_Ws_Name_N_Address.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Tme", "PYMNT-ADDR-LAST-CHG-TME", 
            FieldType.NUMERIC, 7, new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Pymnt_Eft_Transit_Id = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", 
            FieldType.STRING, 21);
        pnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Name_N_Address_Pymnt_Deceased_Nme = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Deceased_Nme", "PYMNT-DECEASED-NME", 
            FieldType.STRING, 38);
        pnd_Ws_Name_N_Address_Cntrct_Hold_Tme = pnd_Ws_Name_N_Address.newFieldInGroup("pnd_Ws_Name_N_Address_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);

        pnd_Ws_Header_Record = localVariables.newGroupInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD");
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", FieldType.STRING, 
            1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", "PYMNT-PAYEE-NA-ADDR-TRGGR", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", "PYMNT-INST-REP-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", "CNTRCT-DVDND-PAYEE-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 
            1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", "CNTRCT-RQST-SETTL-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", "CNTRCT-DVDND-PAYEE-CDE", 
            FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 
            3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", "CNTRCT-DA-TIAA-1-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", "CNTRCT-DA-TIAA-2-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", "CNTRCT-DA-TIAA-3-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", "CNTRCT-DA-TIAA-4-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", "CNTRCT-DA-TIAA-5-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", "CNTRCT-DA-CREF-3-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", "CNTRCT-DA-CREF-4-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", "CNTRCT-DA-CREF-5-NBR", 
            FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", 
            FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);

        pnd_Ws_Header_Record__R_Field_13 = pnd_Ws_Header_Record.newGroupInGroup("pnd_Ws_Header_Record__R_Field_13", "REDEFINE", pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record__R_Field_13.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record__R_Field_13.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", "CNTRCT-EC-OPRTR-ID-NBR", 
            FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Ws_Header_Record_Pymnt_Intrfce_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pnd_Filler = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Filler", "#FILLER", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", "#WS-HEADER-FILLER", 
            FieldType.STRING, 121);

        pnd_Ws_Occurs = localVariables.newGroupInRecord("pnd_Ws_Occurs", "#WS-OCCURS");
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct = pnd_Ws_Occurs.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Occurs_Pnd_Ws_Side = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Side", "#WS-SIDE", FieldType.NUMERIC, 1, new DbsArrayController(1, 
            40));
        pnd_Ws_Occurs_Inv_Acct_Cde = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            40));
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Unit_Value = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 4, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1, 
            new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_State_Cde = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Local_Cde = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1, 
            new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, new 
            DbsArrayController(1, 40));
        pnd_Ws_Occurs_Cntrct_Payee_Cde = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, 
            new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Cntrct_Option_Cde = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Cntrct_Mode_Cde = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3, new 
            DbsArrayController(1, 40));
        pnd_Ws_Occurs_Pnd_Cntr_Deductions = pnd_Ws_Occurs.newFieldArrayInGroup("pnd_Ws_Occurs_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 40));

        pnd_Ws_Occurs_Pnd_Ws_Deductions = pnd_Ws_Occurs.newGroupArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Deductions", "#WS-DEDUCTIONS", new DbsArrayController(1, 
            40));
        pnd_Ws_Occurs_Pymnt_Ded_Cde = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Amt = pnd_Ws_Occurs_Pnd_Ws_Deductions.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 10));
        pnd_Cnr_Orgnl_Invrse_Dte = localVariables.newFieldInRecord("pnd_Cnr_Orgnl_Invrse_Dte", "#CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCdbatxa.initializeValues();
        ldaFcpladdr.initializeValues();
        ldaFcplcntu.initializeValues();
        ldaFcplpmnt.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_2.setInitialValue("  CANCEL/REDRAW IA  BATCH EXTRACT CONTROL REPORT");
        pnd_Payment_Date.setInitialValue(" ");
        pnd_Inst_Tiaa_Amt.setInitialValue(0);
        pnd_Inst_Cref_Amt.setInitialValue(0);
        pnd_Counter.setInitialValue(0);
        pnd_Key_For_Name_Address.setInitialValue(0);
        pnd_Control_Totals_Pnd_Type_Code.getValue(1).setInitialValue(" ");
        pnd_Control_Totals_Pnd_Cnt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Gross_Amt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Net_Amt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Difference_Pnd_Type_Code_Diff.getValue(1).setInitialValue(" ");
        pnd_Control_Totals_Difference_Pnd_Cnt_Diff.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Difference_Pnd_Gross_Amt_Diff.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Difference_Pnd_Net_Amt_Diff.getValue(1).setInitialValue(0);
        pnd_In_Dex.setInitialValue(1);
        pnd_Ci2.setInitialValue(1);
        pnd_C.setInitialValue(0);
        pnd_D.setInitialValue(0);
        pnd_I.setInitialValue(1);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_Ws_Deductions_Table.getValue(1).setInitialValue(0);
        pnd_Ws_Cntr_Deductions.setInitialValue(0);
        pnd_Ws_Total_Lines_Of_Deductions.setInitialValue(0);
        pnd_Ws_Gross_A.setInitialValue(" ");
        pnd_Ws_Gross_N.setInitialValue(0);
        pnd_Ws_Type.setInitialValue(" ");
        pnd_Ws_First_Time.setInitialValue("Y");
        pnd_Ws_Inv_Acct_Filler.setInitialValue(" ");
        pnd_Ws_Name_N_Address_Filler.setInitialValue(" ");
        pnd_Ws_Combine_Nbr.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp375() throws Exception
    {
        super("Fcpp375");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        //* *SAG END-EXIT
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-CNTRL BY CNTL-ORG-CDE-INVRSE-DTE = 'CI'
        (
        "READ01",
        new Wc[] { new Wc("CNTL_ORG_CDE_INVRSE_DTE", ">=", "CI", WcType.BY) },
        new Oc[] { new Oc("CNTL_ORG_CDE_INVRSE_DTE", "ASC") },
        1
        );
        READ01:
        while (condition(ldaFcplcntu.getVw_fcp_Cons_Cntrl().readNextRow("READ01")))
        {
            if (condition(ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("CI")))                                                                                    //Natural: IF FCP-CONS-CNTRL.CNTL-ORGN-CDE = 'CI'
            {
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"ERROR:  control record found for prior job");                            //Natural: WRITE *PROGRAM *TIME 'ERROR:  control record found for prior job'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Type_Cde().getValue("*").reset();                                                                                          //Natural: RESET FCP-CONS-CNTRL.CNTL-TYPE-CDE ( * ) FCP-CONS-CNTRL.CNTL-CNT ( * ) FCP-CONS-CNTRL.CNTL-GROSS-AMT ( * ) FCP-CONS-CNTRL.CNTL-NET-AMT ( * )
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue("*").reset();
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue("*").reset();
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue("*").reset();
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.setValue("IA");                                                                                                                  //Natural: ASSIGN #WS-PRIME-KEY.CNTRCT-ORGN-CDE := 'IA'
        pnd_Ws_Prime_Key_Pymnt_Stats_Cde.setValue("D");                                                                                                                   //Natural: ASSIGN #WS-PRIME-KEY.PYMNT-STATS-CDE := 'D'
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  PRIMARY FILE
        //* *READ FCP-CONS-PYMNT BY CHK-DTE-ORG-CURR-TYP-SEQ
        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ STARTING FROM #WS-PRIME-KEY
        (
        "READ_PRIME",
        new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
        );
        READ_PRIME:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ_PRIME")))
        {
            //*  CTS-0115 >>
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                     //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  #COUNTER := *COUNTER(READ-PRIME.)
            pnd_Counter.nadd(1);                                                                                                                                          //Natural: ASSIGN #COUNTER := #COUNTER + 1
            //*  CTS-0115 <<
            if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().equals(1)))                                                                                  //Natural: IF *COUNTER ( READ-PRIME. ) = 1
            {
                                                                                                                                                                          //Natural: PERFORM PRIME-WRITE-HEADINGS
                sub_Prime_Write_Headings();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sec1_Start_Key.moveAll("H'00'");                                                                                                                          //Natural: MOVE ALL H'00' TO #SEC1-START-KEY
            pnd_Sec1_End_Key.moveAll("H'FF'");                                                                                                                            //Natural: MOVE ALL H'FF' TO #SEC1-END-KEY
            pnd_Prime_Sec1_Related_Key_Pnd_Prime_Sec1_Related_Structure.setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                              //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #PRIME-SEC1-RELATED-STRUCTURE
            if (condition(pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.notEquals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde()) || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Stats_Cde().notEquals("D"))) //Natural: IF #WS-PRIME-KEY.CNTRCT-ORGN-CDE NE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE OR FCP-CONS-PYMNT.PYMNT-STATS-CDE NE 'D'
            {
                //* *IF #WS-PRIME-KEY.PYMNT-CHECK-DTE NE FCP-CONS-PYMNT.PYMNT-CHECK-DTE
                //* *    OR #WS-PRIME-KEY.CNTRCT-ORGN-CDE NE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
                //* *    OR FCP-CONS-PYMNT.CNTRCT-CHECK-CRRNCY-CDE NE '1'
                //* *    OR FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND NE 1
                if (true) break READ_PRIME;                                                                                                                               //Natural: ESCAPE BOTTOM ( READ-PRIME. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Sec1_Start_Key_Pnd_Sec1_Start_Key_Prefix.setValue(pnd_Prime_Sec1_Related_Key);                                                                            //Natural: ASSIGN #SEC1-START-KEY-PREFIX = #SEC1-END-KEY-PREFIX = #PRIME-SEC1-RELATED-KEY
            pnd_Sec1_End_Key_Pnd_Sec1_End_Key_Prefix.setValue(pnd_Prime_Sec1_Related_Key);
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                    //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
            {
                                                                                                                                                                          //Natural: PERFORM READ-NAME-N-ADDRESS
                sub_Read_Name_N_Address();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                    //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Stats_Cde().notEquals("D")))                                                                            //Natural: IF FCP-CONS-PYMNT.PYMNT-STATS-CDE NE 'D'
                {
                    getReports().write(0, ReportOption.NOTITLE,"J O B   T E R M I N A T E D");                                                                            //Natural: WRITE 'J O B   T E R M I N A T E D'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"WHERE: EXTRACT STEP      REASON: RECORD ALREADY PROCESSED");                                              //Natural: WRITE 'WHERE: EXTRACT STEP      REASON: RECORD ALREADY PROCESSED'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    getReports().write(0, ReportOption.NOTITLE,"=",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr(),"=",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde()); //Natural: WRITE '=' FCP-CONS-PYMNT.CNTRCT-PPCN-NBR '=' FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    DbsUtil.terminate(79);  if (true) return;                                                                                                             //Natural: TERMINATE 0079
                    if (true) break READ_PRIME;                                                                                                                           //Natural: ESCAPE BOTTOM ( READ-PRIME. ) IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_First_Time.equals("Y")))                                                                                                                 //Natural: IF #WS-FIRST-TIME = 'Y'
            {
                pnd_Ws_First_Time.setValue("N");                                                                                                                          //Natural: ASSIGN #WS-FIRST-TIME = 'N'
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                sub_Initialize_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num.notEquals(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num())))                                     //Natural: IF #WS-SAVE-PYMNT-PRCSS-SEQ-NUM NE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-CANCEL-STOP-OR-CHECKS
                sub_Write_Cancel_Stop_Or_Checks();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                sub_Initialize_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *ADD 1 TO #K
            //* *ADD C*PYMNT-DED-GRP TO #WS-DEDUCTIONS-TABLE(#K)
            //* *ADD #WS-DEDUCTIONS-TABLE(#K) TO
            //* *  #WS-TOTAL-LINES-OF-DEDUCTIONS
            FOR01:                                                                                                                                                        //Natural: FOR #L 1 TO C*INV-ACCT
            for (pnd_L.setValue(1); condition(pnd_L.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())); pnd_L.nadd(1))
            {
                pnd_K.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #K
                //*    #K := #L
                                                                                                                                                                          //Natural: PERFORM MOVE-INV-ACCT-FIELDS
                sub_Move_Inv_Acct_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PRINT-ON-WHICH-SIDE-OF-PAPER
                sub_Print_On_Which_Side_Of_Paper();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CANCEL-REDRAW-IA-CONTROL-TOTALS
            sub_Cancel_Redraw_Ia_Control_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("READ_PRIME"))) break;
                else if (condition(Global.isEscapeBottomImmediate("READ_PRIME"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  PRIMARY FILE.
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*                                        /* PROCESS LAST RECORD
        if (condition(pnd_Counter.greater(1)))                                                                                                                            //Natural: IF #COUNTER GT 1
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-CANCEL-STOP-OR-CHECKS
            sub_Write_Cancel_Stop_Or_Checks();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *ASSIGN #WS-RECORD-LEVEL-NMBR = 10
        //* *ASSIGN #WS-RECORD-OCCUR-NMBR = 1
        //* *WRITE WORK FILE 8
        //* *  #WS-KEY
        //* *  #WS-HEADER-RECORD
        //* *PERFORM SEPARATE-N-WRITE-OCCURS-RECORD
        //* *PERFORM SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
        //* *IF #COUNTER GT 0
                                                                                                                                                                          //Natural: PERFORM WRITE-CONTROL-REC
        sub_Write_Control_Rec();
        if (condition(Global.isEscape())) {return;}
        //* *END-IF
        //* *ASSIGN #WS-RECORD-LEVEL-NMBR = 10
        //* ***ASSIGN #WS-RECORD-OCCUR-NMBR = 1
        //* *WRITE WORK FILE 8
        //* *  #WS-KEY
        //* *  #WS-HEADER-RECORD
        //* *PERFORM SEPARATE-N-WRITE-OCCURS-RECORD
        //* *PERFORM SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
        //* *SAG DEFINE EXIT END-OF-PROGRAM
        //* *FORMAT LS=132 PS=55 FCP-CONS-PYMNTP=ON IS=OFF ES=OFF SG=OFF
        //* *SAG END-EXIT
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRIME-WRITE-HEADINGS
        //* ***********************************************************************
        //* *SAG DEFINE EXIT PRIME-WRITE-HEADINGS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ON-WHICH-SIDE-OF-PAPER
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-INV-ACCT-FIELDS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-OCCURS-RECORD
        //*     #WS-DEDUCTIONS(#C)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-OCCURS-RECORD-2
        //*     #WS-DEDUCTIONS(#C)
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SEPARATE-N-WRITE-NAME-N-ADDR-2
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-NAME-N-ADDRESS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CANCEL-REDRAW-IA-CONTROL-TOTALS
        //*  VALUE 'C', 'CN'
        //*  VALUE 'S', 'SN'
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CANCEL-STOP-OR-CHECKS
        //*  VALUE 'C', 'S', 'CN', 'SN'
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-CONTROL-REC
    }
    private void sub_Prime_Write_Headings() throws Exception                                                                                                              //Natural: PRIME-WRITE-HEADINGS
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, ReportOption.NOTITLE," ");                                                                                                                  //Natural: WRITE ( 0 ) ' '
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //*  PRIME-WRITE-HEADINGS
    }
    private void sub_Print_On_Which_Side_Of_Paper() throws Exception                                                                                                      //Natural: PRINT-ON-WHICH-SIDE-OF-PAPER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Deductions_Table.getValue(pnd_K).nadd(5);                                                                                                                  //Natural: ADD 5 TO #WS-DEDUCTIONS-TABLE ( #K )
        pnd_Ws_Total_Lines_Of_Deductions.nadd(pnd_Ws_Deductions_Table.getValue(pnd_K));                                                                                   //Natural: ADD #WS-DEDUCTIONS-TABLE ( #K ) TO #WS-TOTAL-LINES-OF-DEDUCTIONS
        short decideConditionsMet937 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #WS-TOTAL-LINES-OF-DEDUCTIONS;//Natural: VALUE 1 : 18
        if (condition(((pnd_Ws_Total_Lines_Of_Deductions.greaterOrEqual(1) && pnd_Ws_Total_Lines_Of_Deductions.lessOrEqual(18)))))
        {
            decideConditionsMet937++;
            pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_K).setValue(1);                                                                                                        //Natural: ASSIGN #WS-SIDE ( #K ) = 1
            //*  SIMPLEX
            pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex.setValue("1");                                                                                                     //Natural: ASSIGN #WS-SIMPLEX-DUPLEX-MULTIPLEX = '1'
        }                                                                                                                                                                 //Natural: VALUE 19 : 51
        else if (condition(((pnd_Ws_Total_Lines_Of_Deductions.greaterOrEqual(19) && pnd_Ws_Total_Lines_Of_Deductions.lessOrEqual(51)))))
        {
            decideConditionsMet937++;
            pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_K).setValue(2);                                                                                                        //Natural: ASSIGN #WS-SIDE ( #K ) = 2
            //*  DUPLEX
            pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex.setValue("2");                                                                                                     //Natural: ASSIGN #WS-SIMPLEX-DUPLEX-MULTIPLEX = '2'
        }                                                                                                                                                                 //Natural: VALUE 51 : 83
        else if (condition(((pnd_Ws_Total_Lines_Of_Deductions.greaterOrEqual(51) && pnd_Ws_Total_Lines_Of_Deductions.lessOrEqual(83)))))
        {
            decideConditionsMet937++;
            pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_K).setValue(3);                                                                                                        //Natural: ASSIGN #WS-SIDE ( #K ) = 3
            //*  MULTI-PLEX
            pnd_Ws_Key_Pnd_Ws_Simplex_Duplex_Multiplex.setValue("3");                                                                                                     //Natural: ASSIGN #WS-SIMPLEX-DUPLEX-MULTIPLEX = '3'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Move_Inv_Acct_Fields() throws Exception                                                                                                              //Natural: MOVE-INV-ACCT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_L));                                                //Natural: ASSIGN #WS-OCCURS.INV-ACCT-CDE ( #K ) = FCP-CONS-PYMNT.INV-ACCT-CDE ( #L )
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Unit_Qty().getValue(pnd_L));                                      //Natural: ASSIGN #WS-OCCURS.INV-ACCT-UNIT-QTY ( #K ) = FCP-CONS-PYMNT.INV-ACCT-UNIT-QTY ( #L )
        pnd_Ws_Occurs_Inv_Acct_Unit_Value.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Unit_Value().getValue(pnd_L));                                  //Natural: ASSIGN #WS-OCCURS.INV-ACCT-UNIT-VALUE ( #K ) = FCP-CONS-PYMNT.INV-ACCT-UNIT-VALUE ( #L )
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue(pnd_L));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-SETTL-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fed_Cde().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-FED-CDE ( #K ) = FCP-CONS-PYMNT.INV-ACCT-FED-CDE ( #L )
        pnd_Ws_Occurs_Inv_Acct_State_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Cde().getValue(pnd_L));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-STATE-CDE ( #K ) = FCP-CONS-PYMNT.INV-ACCT-STATE-CDE ( #L )
        pnd_Ws_Occurs_Inv_Acct_Local_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Cde().getValue(pnd_L));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-LOCAL-CDE ( #K ) = FCP-CONS-PYMNT.INV-ACCT-LOCAL-CDE ( #L )
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Ivc_Amt().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-IVC-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-IVC-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dci_Amt().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DCI-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-DCI-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dpi_Amt().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DPI-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-DPI-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Start_Accum_Amt().getValue(pnd_L));                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-START-ACCUM-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-START-ACCUM-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_End_Accum_Amt().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-END-ACCUM-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Dvdnd_Amt().getValue(pnd_L));                                    //Natural: ASSIGN #WS-OCCURS.INV-ACCT-DVDND-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-DVDND-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Ivc_Ind().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-IVC-IND ( #K ) = FCP-CONS-PYMNT.INV-ACCT-IVC-IND ( #L )
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Adj_Ivc_Amt().getValue(pnd_L));                                //Natural: ASSIGN #WS-OCCURS.INV-ACCT-ADJ-IVC-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-ADJ-IVC-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Valuat_Period().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-VALUAT-PERIOD ( #K ) = FCP-CONS-PYMNT.INV-ACCT-VALUAT-PERIOD ( #L )
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_L));                              //Natural: ASSIGN #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-FDRL-TAX-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_State_Tax_Amt().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-STATE-TAX-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Local_Tax_Amt().getValue(pnd_L));                            //Natural: ASSIGN #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-LOCAL-TAX-AMT ( #L )
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Exp_Amt().getValue(pnd_L));                                        //Natural: ASSIGN #WS-OCCURS.INV-ACCT-EXP-AMT ( #K ) = FCP-CONS-PYMNT.INV-ACCT-EXP-AMT ( #L )
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-PPCN-NBR ( #K ) = FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        pnd_Ws_Occurs_Cntrct_Payee_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                        //Natural: ASSIGN #WS-OCCURS.CNTRCT-PAYEE-CDE ( #K ) = FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind());                                              //Natural: ASSIGN #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #K ) = FCP-CONS-PYMNT.CNTRCT-PYMNT-TYPE-IND
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind());                                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #K ) = FCP-CONS-PYMNT.CNTRCT-STTLMNT-TYPE-IND
        pnd_Ws_Occurs_Cntrct_Option_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Option_Cde());                                                      //Natural: ASSIGN #WS-OCCURS.CNTRCT-OPTION-CDE ( #K ) = FCP-CONS-PYMNT.CNTRCT-OPTION-CDE
        pnd_Ws_Occurs_Cntrct_Mode_Cde.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Mode_Cde());                                                          //Natural: ASSIGN #WS-OCCURS.CNTRCT-MODE-CDE ( #K ) = FCP-CONS-PYMNT.CNTRCT-MODE-CDE
        //* **���
        //*  .....KEEP DEDUCTION GROUP ONLY ONCE FOR THE PAYMENT RECORD
        //*  THE FIRST INVS-ACCT FOR THIS RECORD
        if (condition(pnd_L.equals(1)))                                                                                                                                   //Natural: IF #L = 1
        {
            pnd_Ws_Occurs_Pnd_Cntr_Deductions.getValue(pnd_K).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                          //Natural: ASSIGN #CNTR-DEDUCTIONS ( #K ) := C*PYMNT-DED-GRP
            pnd_Ws_Occurs_Pymnt_Ded_Cde.getValue(pnd_K,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue("*"));                                        //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-CDE ( #K,* ) := FCP-CONS-PYMNT.PYMNT-DED-CDE ( * )
            pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde.getValue(pnd_K,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Payee_Cde().getValue("*"));                            //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-PAYEE-CDE ( #K,* ) := FCP-CONS-PYMNT.PYMNT-DED-PAYEE-CDE ( * )
            pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_K,"*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Amt().getValue("*"));                                        //Natural: ASSIGN #WS-OCCURS.PYMNT-DED-AMT ( #K,* ) := FCP-CONS-PYMNT.PYMNT-DED-AMT ( * )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Occurs_Pnd_Cntr_Deductions.getValue(pnd_K).reset();                                                                                                    //Natural: RESET #CNTR-DEDUCTIONS ( #K ) #WS-OCCURS.PYMNT-DED-CDE ( #K,* ) #WS-OCCURS.PYMNT-DED-PAYEE-CDE ( #K,* ) #WS-OCCURS.PYMNT-DED-AMT ( #K,* )
            pnd_Ws_Occurs_Pymnt_Ded_Cde.getValue(pnd_K,"*").reset();
            pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde.getValue(pnd_K,"*").reset();
            pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_K,"*").reset();
        }                                                                                                                                                                 //Natural: END-IF
        //* **���
    }
    private void sub_Separate_N_Write_Occurs_Record() throws Exception                                                                                                    //Natural: SEPARATE-N-WRITE-OCCURS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #C = 1 TO #K
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_K)); pnd_C.nadd(1))
        {
            //* *--> INSERTED 04-08-95 BY FRANK
            //* *IF #WS-OCCURS.INV-ACCT-CDE(#C) = '00' OR= '  '
            //* *  IGNORE
            //* *ELSE
            pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(20);                                                                                                             //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 20
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_C);                                                                                                          //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #C
            pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct.setValue(pnd_K);                                                                                                              //Natural: ASSIGN #CNTR-INV-ACCT = #K
            getWorkFiles().write(8, false, pnd_Ws_Key, pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct, pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_C),  //Natural: WRITE WORK FILE 8 #WS-KEY #CNTR-INV-ACCT #WS-SIDE ( #C ) #WS-OCCURS.INV-ACCT-CDE ( #C ) #WS-OCCURS.INV-ACCT-UNIT-QTY ( #C ) #WS-OCCURS.INV-ACCT-UNIT-VALUE ( #C ) #WS-OCCURS.INV-ACCT-SETTL-AMT ( #C ) #WS-OCCURS.INV-ACCT-FED-CDE ( #C ) #WS-OCCURS.INV-ACCT-STATE-CDE ( #C ) #WS-OCCURS.INV-ACCT-LOCAL-CDE ( #C ) #WS-OCCURS.INV-ACCT-IVC-AMT ( #C ) #WS-OCCURS.INV-ACCT-DCI-AMT ( #C ) #WS-OCCURS.INV-ACCT-DPI-AMT ( #C ) #WS-OCCURS.INV-ACCT-START-ACCUM-AMT ( #C ) #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #C ) #WS-OCCURS.INV-ACCT-DVDND-AMT ( #C ) #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #C ) #WS-OCCURS.INV-ACCT-IVC-IND ( #C ) #WS-OCCURS.INV-ACCT-ADJ-IVC-AMT ( #C ) #WS-OCCURS.INV-ACCT-VALUAT-PERIOD ( #C ) #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #C ) #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #C ) #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #C ) #WS-OCCURS.INV-ACCT-EXP-AMT ( #C ) #WS-OCCURS.CNTRCT-PPCN-NBR ( #C ) #WS-OCCURS.CNTRCT-PAYEE-CDE ( #C ) #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #C ) #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #C ) #WS-OCCURS.CNTRCT-OPTION-CDE ( #C ) #WS-OCCURS.CNTRCT-MODE-CDE ( #C ) #CNTR-DEDUCTIONS ( #C ) #WS-OCCURS.PYMNT-DED-CDE ( #C,* ) #WS-OCCURS.PYMNT-DED-PAYEE-CDE ( #C,* ) #WS-OCCURS.PYMNT-DED-AMT ( #C,* ) #WS-INV-ACCT-FILLER
                pnd_Ws_Occurs_Inv_Acct_Unit_Qty.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Unit_Value.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Fed_Cde.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_State_Cde.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Local_Cde.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Ivc_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Dci_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Ivc_Ind.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Valuat_Period.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Exp_Amt.getValue(pnd_C), pnd_Ws_Occurs_Cntrct_Ppcn_Nbr.getValue(pnd_C), 
                pnd_Ws_Occurs_Cntrct_Payee_Cde.getValue(pnd_C), pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind.getValue(pnd_C), pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind.getValue(pnd_C), 
                pnd_Ws_Occurs_Cntrct_Option_Cde.getValue(pnd_C), pnd_Ws_Occurs_Cntrct_Mode_Cde.getValue(pnd_C), pnd_Ws_Occurs_Pnd_Cntr_Deductions.getValue(pnd_C), 
                pnd_Ws_Occurs_Pymnt_Ded_Cde.getValue(pnd_C,"*"), pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde.getValue(pnd_C,"*"), pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_C,"*"), 
                pnd_Ws_Inv_Acct_Filler);
            //* *END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Separate_N_Write_Occurs_Record_2() throws Exception                                                                                                  //Natural: SEPARATE-N-WRITE-OCCURS-RECORD-2
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #C = 1 TO #K
        for (pnd_C.setValue(1); condition(pnd_C.lessOrEqual(pnd_K)); pnd_C.nadd(1))
        {
            //* *--> INSERTED 04-08-95 BY FRANK
            //* *IF #WS-OCCURS.INV-ACCT-CDE(#C) = '00' OR= '  '
            //* *  IGNORE
            //* *ELSE
            pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(20);                                                                                                             //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 20
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_C);                                                                                                          //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #C
            pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct.setValue(pnd_K);                                                                                                              //Natural: ASSIGN #CNTR-INV-ACCT = #K
            getWorkFiles().write(2, false, pnd_Ws_Key, pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct, pnd_Ws_Occurs_Pnd_Ws_Side.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Cde.getValue(pnd_C),  //Natural: WRITE WORK FILE 2 #WS-KEY #CNTR-INV-ACCT #WS-SIDE ( #C ) #WS-OCCURS.INV-ACCT-CDE ( #C ) #WS-OCCURS.INV-ACCT-UNIT-QTY ( #C ) #WS-OCCURS.INV-ACCT-UNIT-VALUE ( #C ) #WS-OCCURS.INV-ACCT-SETTL-AMT ( #C ) #WS-OCCURS.INV-ACCT-FED-CDE ( #C ) #WS-OCCURS.INV-ACCT-STATE-CDE ( #C ) #WS-OCCURS.INV-ACCT-LOCAL-CDE ( #C ) #WS-OCCURS.INV-ACCT-IVC-AMT ( #C ) #WS-OCCURS.INV-ACCT-DCI-AMT ( #C ) #WS-OCCURS.INV-ACCT-DPI-AMT ( #C ) #WS-OCCURS.INV-ACCT-START-ACCUM-AMT ( #C ) #WS-OCCURS.INV-ACCT-END-ACCUM-AMT ( #C ) #WS-OCCURS.INV-ACCT-DVDND-AMT ( #C ) #WS-OCCURS.INV-ACCT-NET-PYMNT-AMT ( #C ) #WS-OCCURS.INV-ACCT-IVC-IND ( #C ) #WS-OCCURS.INV-ACCT-ADJ-IVC-AMT ( #C ) #WS-OCCURS.INV-ACCT-VALUAT-PERIOD ( #C ) #WS-OCCURS.INV-ACCT-FDRL-TAX-AMT ( #C ) #WS-OCCURS.INV-ACCT-STATE-TAX-AMT ( #C ) #WS-OCCURS.INV-ACCT-LOCAL-TAX-AMT ( #C ) #WS-OCCURS.INV-ACCT-EXP-AMT ( #C ) #WS-OCCURS.CNTRCT-PPCN-NBR ( #C ) #WS-OCCURS.CNTRCT-PAYEE-CDE ( #C ) #WS-OCCURS.CNTRCT-PYMNT-TYPE-IND ( #C ) #WS-OCCURS.CNTRCT-STTLMNT-TYPE-IND ( #C ) #WS-OCCURS.CNTRCT-OPTION-CDE ( #C ) #WS-OCCURS.CNTRCT-MODE-CDE ( #C ) #CNTR-DEDUCTIONS ( #C ) #WS-OCCURS.PYMNT-DED-CDE ( #C,* ) #WS-OCCURS.PYMNT-DED-PAYEE-CDE ( #C,* ) #WS-OCCURS.PYMNT-DED-AMT ( #C,* ) #WS-INV-ACCT-FILLER
                pnd_Ws_Occurs_Inv_Acct_Unit_Qty.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Unit_Value.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Settl_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Fed_Cde.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_State_Cde.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Local_Cde.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Ivc_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Dci_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Dpi_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Ivc_Ind.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Valuat_Period.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt.getValue(pnd_C), 
                pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt.getValue(pnd_C), pnd_Ws_Occurs_Inv_Acct_Exp_Amt.getValue(pnd_C), pnd_Ws_Occurs_Cntrct_Ppcn_Nbr.getValue(pnd_C), 
                pnd_Ws_Occurs_Cntrct_Payee_Cde.getValue(pnd_C), pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind.getValue(pnd_C), pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind.getValue(pnd_C), 
                pnd_Ws_Occurs_Cntrct_Option_Cde.getValue(pnd_C), pnd_Ws_Occurs_Cntrct_Mode_Cde.getValue(pnd_C), pnd_Ws_Occurs_Pnd_Cntr_Deductions.getValue(pnd_C), 
                pnd_Ws_Occurs_Pymnt_Ded_Cde.getValue(pnd_C,"*"), pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde.getValue(pnd_C,"*"), pnd_Ws_Occurs_Pymnt_Ded_Amt.getValue(pnd_C,"*"), 
                pnd_Ws_Inv_Acct_Filler);
            //* *END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Separate_N_Write_Name_N_Addr_Record() throws Exception                                                                                               //Natural: SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #D = 1 TO #CNTR-PYMNT-NME-AND-ADDR-GRP
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(pnd_Ws_Name_N_Address_Pnd_Cntr_Pymnt_Nme_And_Addr_Grp)); pnd_D.nadd(1))
        {
            if (condition(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(pnd_D).notEquals(" ")))                                                                                //Natural: IF #WS-NAME-N-ADDRESS.PYMNT-NME ( #D ) NE ' '
            {
                pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(30);                                                                                                         //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 30
                pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_D);                                                                                                      //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #D
                if (condition(pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde.equals("IA")))                                                                                        //Natural: IF #WS-NAME-N-ADDRESS.CNTRCT-ORGN-CDE = 'IA'
                {
                    pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr.setValue(pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num);                                                         //Natural: ASSIGN #WS-NAME-N-ADDRESS.PYMNT-PRCSS-SEQ-NBR = #WS-HEADER-RECORD.PYMNT-PRCSS-SEQ-NUM
                }                                                                                                                                                         //Natural: END-IF
                getWorkFiles().write(8, false, pnd_Ws_Key, pnd_Ws_Name_N_Address_Rcrd_Typ, pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde, pnd_Ws_Name_N_Address_Cntrct_Ppcn_Nbr,  //Natural: WRITE WORK FILE 8 #WS-KEY #WS-NAME-N-ADDRESS.RCRD-TYP #WS-NAME-N-ADDRESS.CNTRCT-ORGN-CDE #WS-NAME-N-ADDRESS.CNTRCT-PPCN-NBR #WS-NAME-N-ADDRESS.CNTRCT-PAYEE-CDE #WS-NAME-N-ADDRESS.CNTRCT-INVRSE-DTE #WS-NAME-N-ADDRESS.PYMNT-PRCSS-SEQ-NBR #WS-NAME-N-ADDRESS.PH-NAME #WS-NAME-N-ADDRESS.PYMNT-NME ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-ZIP-CDE ( #D ) #WS-NAME-N-ADDRESS.PYMNT-POSTL-DATA ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-TYPE-IND ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LAST-CHG-DTE ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LAST-CHG-TME ( #D ) #WS-NAME-N-ADDRESS.PYMNT-EFT-TRANSIT-ID #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR #WS-NAME-N-ADDRESS.PYMNT-CHK-SAV-IND #WS-NAME-N-ADDRESS.PYMNT-DECEASED-NME #WS-NAME-N-ADDRESS.CNTRCT-HOLD-TME #WS-NAME-N-ADDRESS-FILLER
                    pnd_Ws_Name_N_Address_Cntrct_Payee_Cde, pnd_Ws_Name_N_Address_Cntrct_Invrse_Dte, pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr, pnd_Ws_Name_N_Address_Ph_Name, 
                    pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt.getValue(pnd_D), 
                    pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt.getValue(pnd_D), 
                    pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Postl_Data.getValue(pnd_D), 
                    pnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Dte.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Tme.getValue(pnd_D), 
                    pnd_Ws_Name_N_Address_Pymnt_Eft_Transit_Id, pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr, pnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind, pnd_Ws_Name_N_Address_Pymnt_Deceased_Nme, 
                    pnd_Ws_Name_N_Address_Cntrct_Hold_Tme, pnd_Ws_Name_N_Address_Filler);
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Separate_N_Write_Name_N_Addr_2() throws Exception                                                                                                    //Natural: SEPARATE-N-WRITE-NAME-N-ADDR-2
    {
        if (BLNatReinput.isReinput()) return;

        FOR05:                                                                                                                                                            //Natural: FOR #D = 1 TO #CNTR-PYMNT-NME-AND-ADDR-GRP
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(pnd_Ws_Name_N_Address_Pnd_Cntr_Pymnt_Nme_And_Addr_Grp)); pnd_D.nadd(1))
        {
            if (condition(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(pnd_D).notEquals(" ")))                                                                                //Natural: IF #WS-NAME-N-ADDRESS.PYMNT-NME ( #D ) NE ' '
            {
                pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(30);                                                                                                         //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 30
                pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(pnd_D);                                                                                                      //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = #D
                if (condition(pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde.equals("IA")))                                                                                        //Natural: IF #WS-NAME-N-ADDRESS.CNTRCT-ORGN-CDE = 'IA'
                {
                    pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr.setValue(pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num);                                                         //Natural: ASSIGN #WS-NAME-N-ADDRESS.PYMNT-PRCSS-SEQ-NBR = #WS-HEADER-RECORD.PYMNT-PRCSS-SEQ-NUM
                }                                                                                                                                                         //Natural: END-IF
                getWorkFiles().write(2, false, pnd_Ws_Key, pnd_Ws_Name_N_Address_Rcrd_Typ, pnd_Ws_Name_N_Address_Cntrct_Orgn_Cde, pnd_Ws_Name_N_Address_Cntrct_Ppcn_Nbr,  //Natural: WRITE WORK FILE 2 #WS-KEY #WS-NAME-N-ADDRESS.RCRD-TYP #WS-NAME-N-ADDRESS.CNTRCT-ORGN-CDE #WS-NAME-N-ADDRESS.CNTRCT-PPCN-NBR #WS-NAME-N-ADDRESS.CNTRCT-PAYEE-CDE #WS-NAME-N-ADDRESS.CNTRCT-INVRSE-DTE #WS-NAME-N-ADDRESS.PYMNT-PRCSS-SEQ-NBR #WS-NAME-N-ADDRESS.PH-NAME #WS-NAME-N-ADDRESS.PYMNT-NME ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-ZIP-CDE ( #D ) #WS-NAME-N-ADDRESS.PYMNT-POSTL-DATA ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-TYPE-IND ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LAST-CHG-DTE ( #D ) #WS-NAME-N-ADDRESS.PYMNT-ADDR-LAST-CHG-TME ( #D ) #WS-NAME-N-ADDRESS.PYMNT-EFT-TRANSIT-ID #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR #WS-NAME-N-ADDRESS.PYMNT-CHK-SAV-IND #WS-NAME-N-ADDRESS.PYMNT-DECEASED-NME #WS-NAME-N-ADDRESS.CNTRCT-HOLD-TME #WS-NAME-N-ADDRESS-FILLER
                    pnd_Ws_Name_N_Address_Cntrct_Payee_Cde, pnd_Ws_Name_N_Address_Cntrct_Invrse_Dte, pnd_Ws_Name_N_Address_Pymnt_Prcss_Seq_Nbr, pnd_Ws_Name_N_Address_Ph_Name, 
                    pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt.getValue(pnd_D), 
                    pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt.getValue(pnd_D), 
                    pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Zip_Cde.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Postl_Data.getValue(pnd_D), 
                    pnd_Ws_Name_N_Address_Pymnt_Addr_Type_Ind.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Dte.getValue(pnd_D), pnd_Ws_Name_N_Address_Pymnt_Addr_Last_Chg_Tme.getValue(pnd_D), 
                    pnd_Ws_Name_N_Address_Pymnt_Eft_Transit_Id, pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr, pnd_Ws_Name_N_Address_Pymnt_Chk_Sav_Ind, pnd_Ws_Name_N_Address_Pymnt_Deceased_Nme, 
                    pnd_Ws_Name_N_Address_Cntrct_Hold_Tme, pnd_Ws_Name_N_Address_Filler);
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Occurs.reset();                                                                                                                                            //Natural: RESET #WS-OCCURS #K #WS-DEDUCTIONS-TABLE ( * ) #WS-TOTAL-LINES-OF-DEDUCTIONS
        pnd_K.reset();
        pnd_Ws_Deductions_Table.getValue("*").reset();
        pnd_Ws_Total_Lines_Of_Deductions.reset();
        pnd_Ws_Key_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                         //Natural: ASSIGN #WS-SAVE-PYMNT-PRCSS-SEQ-NUM = FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
        pnd_Ws_Name_N_Address.setValuesByName(ldaFcpladdr.getVw_fcp_Cons_Addr());                                                                                         //Natural: MOVE BY NAME FCP-CONS-ADDR TO #WS-NAME-N-ADDRESS
        pnd_Ws_Name_N_Address_Pnd_Cntr_Pymnt_Nme_And_Addr_Grp.setValue(ldaFcpladdr.getFcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp());                                  //Natural: ASSIGN #CNTR-PYMNT-NME-AND-ADDR-GRP = C*PYMNT-NME-AND-ADDR-GRP
        pnd_Ws_Header_Record.setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                         //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #WS-HEADER-RECORD
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Hold_Cde().equals(" ")))                                                                                       //Natural: IF FCP-CONS-PYMNT.CNTRCT-HOLD-CDE = ' '
        {
            pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code.setValue("0000");                                                                                                   //Natural: ASSIGN #WS-SAVE-CONTRACT-HOLD-CODE = '0000'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Key_Pnd_Ws_Save_Contract_Hold_Code.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Hold_Cde());                                                          //Natural: ASSIGN #WS-SAVE-CONTRACT-HOLD-CODE = FCP-CONS-PYMNT.CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Key_Pnd_Ws_Cntrct_Cmbn_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr());                                                                      //Natural: ASSIGN #WS-CNTRCT-CMBN-NBR = FCP-CONS-PYMNT.CNTRCT-CMBN-NBR
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                        //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
        {
            //*  IB 9/16/99
            pnd_Cnr_Orgnl_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Orgnl_Invrse_Dte());                                                                      //Natural: MOVE FCP-CONS-PYMNT.CNR-ORGNL-INVRSE-DTE TO #CNR-ORGNL-INVRSE-DTE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Read_Name_N_Address() throws Exception                                                                                                               //Natural: READ-NAME-N-ADDRESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *SAG DEFINE EXIT BUILD-SEC1-KEY
        //*  THIS EXIT IS USED TO POPULATE SEC1 KEY FROM OTHER THAN PRIME FILE
        //*  FIELDS
        pnd_Ws_Combine_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr());                                                                                     //Natural: ASSIGN #WS-COMBINE-NBR = FCP-CONS-PYMNT.CNTRCT-CMBN-NBR
        pnd_Sec1_Start_Key_Cntrct_Ppcn_Nbr.setValue(pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_1);                                                                             //Natural: ASSIGN #SEC1-START-KEY.CNTRCT-PPCN-NBR = #WS-COMBINE-NBR-1
        pnd_Sec1_Start_Key_Cntrct_Payee_Cde.setValue(pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_2);                                                                            //Natural: ASSIGN #SEC1-START-KEY.CNTRCT-PAYEE-CDE = #WS-COMBINE-NBR-2
        pnd_Sec1_Start_Key_Cntrct_Orgn_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                     //Natural: ASSIGN #SEC1-START-KEY.CNTRCT-ORGN-CDE = FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        pnd_Sec1_Start_Key_Cntrct_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                                 //Natural: ASSIGN #SEC1-START-KEY.CNTRCT-INVRSE-DTE = FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
        pnd_Sec1_Start_Key_Pymnt_Prcss_Seq_Nbr.setValue(0);                                                                                                               //Natural: ASSIGN #SEC1-START-KEY.PYMNT-PRCSS-SEQ-NBR = 0
        pnd_Sec1_End_Key.moveAll("9");                                                                                                                                    //Natural: MOVE ALL '9' TO #SEC1-END-KEY
        //* *SAG END-EXIT
        //*  SECONDARY FILE 1.
        ldaFcpladdr.getVw_fcp_Cons_Addr().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #SEC1-START-KEY
        (
        "READ_SEC1",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Sec1_Start_Key, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        READ_SEC1:
        while (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().readNextRow("READ_SEC1")))
        {
            //*  IF THE MAXIMUM VALUE OF SECONDARY-FILE-1's key is reached,
            //*  DO END OF DATA PROCESSING.
            pnd_Sec1_Key_Pnd_Sec1_Key_Structure.setValuesByName(ldaFcpladdr.getVw_fcp_Cons_Addr());                                                                       //Natural: MOVE BY NAME FCP-CONS-ADDR TO #SEC1-KEY-STRUCTURE
            if (condition(pnd_Sec1_Key.greater(pnd_Sec1_End_Key)))                                                                                                        //Natural: IF #SEC1-KEY GT #SEC1-END-KEY THEN
            {
                if (true) break READ_SEC1;                                                                                                                                //Natural: ESCAPE BOTTOM ( READ-SEC1. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            //* *SAG DEFINE EXIT AFTER-SEC1-READ
            if (condition((ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr().equals(pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_1)) && (ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde().equals(pnd_Ws_Combine_Nbr_Pnd_Ws_Combine_Nbr_2))  //Natural: IF ( FCP-CONS-ADDR.CNTRCT-PPCN-NBR = #WS-COMBINE-NBR-1 ) AND ( FCP-CONS-ADDR.CNTRCT-PAYEE-CDE = #WS-COMBINE-NBR-2 ) AND ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE = FCP-CONS-PYMNT.CNTRCT-ORGN-CDE )
                && (ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde().equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde()))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, ReportOption.NOTITLE,"PYMNT-PAYEE-CDE:",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde(),"ADDR-PAYEE-CDE:",ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde(), //Natural: WRITE 'PYMNT-PAYEE-CDE:' FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE 'ADDR-PAYEE-CDE:' FCP-CONS-ADDR.CNTRCT-PAYEE-CDE 'PYMNT-PPCN:' FCP-CONS-PYMNT.CNTRCT-PPCN-NBR 'ADDR-PPCN:' FCP-CONS-ADDR.CNTRCT-PPCN-NBR
                    "PYMNT-PPCN:",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr(),"ADDR-PPCN:",ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_SEC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_SEC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Nme().getValue(1).reset();                                                                                             //Natural: RESET FCP-CONS-ADDR.PYMNT-NME ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE2-TXT ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE3-TXT ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE4-TXT ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE5-TXT ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE6-TXT ( 1 )
                ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line1_Txt().getValue(1).reset();
                ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line2_Txt().getValue(1).reset();
                ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line3_Txt().getValue(1).reset();
                ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line4_Txt().getValue(1).reset();
                ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line5_Txt().getValue(1).reset();
                ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line6_Txt().getValue(1).reset();
                getReports().write(0, ReportOption.NOTITLE,"PLEASE PULL THESE CHECKS - NO ADDRESS RECORD");                                                               //Natural: WRITE 'PLEASE PULL THESE CHECKS - NO ADDRESS RECORD'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_SEC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_SEC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    WRITE 'J O B   T E R M I N A T E D:   NO ADDRESS RECORD'
                //*    TERMINATE 0077
                //*    ESCAPE BOTTOM(READ-SEC1.) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde().notEquals(ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde())))                                   //Natural: IF FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE NE FCP-CONS-ADDR.CNTRCT-PAYEE-CDE
            {
                getReports().write(0, ReportOption.NOTITLE,"PYMNT.PAYEE.CDE:",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde(),"ADDR.PAYEE.CDE:",ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Payee_Cde(), //Natural: WRITE 'PYMNT.PAYEE.CDE:' FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE 'ADDR.PAYEE.CDE:' FCP-CONS-ADDR.CNTRCT-PAYEE-CDE 'PYMNT.PPCN:' FCP-CONS-PYMNT.CNTRCT-PPCN-NBR 'ADDR.PPCN:' FCP-CONS-ADDR.CNTRCT-PPCN-NBR
                    "PYMNT.PPCN:",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr(),"ADDR.PPCN:",ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Ppcn_Nbr());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_SEC1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_SEC1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *SAG END-EXIT
            //*  SECONDARY FILE 1
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Cancel_Redraw_Ia_Control_Totals() throws Exception                                                                                                   //Natural: CANCEL-REDRAW-IA-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  'CN'  JWO 2010-11
        //*  'CN'  JWO 2010-11
        short decideConditionsMet1116 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'CN', 'CP', 'RP', 'PR'
        if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
        {
            decideConditionsMet1116++;
            pnd_In_Dex.setValue(1);                                                                                                                                       //Natural: ASSIGN #IN-DEX := 1
        }                                                                                                                                                                 //Natural: VALUE 'S', 'SN', 'SP'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
        {
            decideConditionsMet1116++;
            pnd_In_Dex.setValue(2);                                                                                                                                       //Natural: ASSIGN #IN-DEX := 2
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
        {
            decideConditionsMet1116++;
            pnd_In_Dex.setValue(3);                                                                                                                                       //Natural: ASSIGN #IN-DEX := 3
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet1116 > 0))
        {
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Type_Cde().getValue(pnd_In_Dex).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde());                                 //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-TYPE-CDE ( #IN-DEX ) := FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
            ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(pnd_In_Dex).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue("*"));                   //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( * ) TO FCP-CONS-CNTRL.CNTL-GROSS-AMT ( #IN-DEX )
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                    //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
            {
                ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(pnd_In_Dex).nadd(1);                                                                                    //Natural: ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT ( #IN-DEX )
                ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(pnd_In_Dex).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                  //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO FCP-CONS-CNTRL.CNTL-NET-AMT ( #IN-DEX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                                         //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
        {
            //*  CHECKS
            //*  EFT
            //*  GLOBALS /* TMM-2001-11
            short decideConditionsMet1138 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet1138++;
                pnd_Ci2.setValue(48);                                                                                                                                     //Natural: ASSIGN #CI2 := 48
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet1138++;
                pnd_Ci2.setValue(49);                                                                                                                                     //Natural: ASSIGN #CI2 := 49
            }                                                                                                                                                             //Natural: VALUE 3,4,9
            else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(3) || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(4) 
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind().equals(9))))
            {
                decideConditionsMet1138++;
                pnd_Ci2.setValue(50);                                                                                                                                     //Natural: ASSIGN #CI2 := 50
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1138 > 0))
            {
                ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Type_Cde().getValue(pnd_Ci2).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde());                                //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-TYPE-CDE ( #CI2 ) := FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
                ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(pnd_Ci2).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue("*"));                  //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( * ) TO FCP-CONS-CNTRL.CNTL-GROSS-AMT ( #CI2 )
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
                {
                    ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(pnd_Ci2).nadd(1);                                                                                   //Natural: ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT ( #CI2 )
                    ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(pnd_Ci2).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                 //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO FCP-CONS-CNTRL.CNTL-NET-AMT ( #CI2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  LEON 08-04-99
        //*  JWO 2010-11
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S")  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))
        {
            //*  CANCELS       /* 'CN' LEON 08-04-99
            //*  JWO 2010-11
            //*  STOPS         /* 'SN' LEON 08-04-99
            //*  STOPS                 /* JWO 2010-11
            short decideConditionsMet1165 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'CN'
            if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN"))))
            {
                decideConditionsMet1165++;
                pnd_Ci2.setValue(46);                                                                                                                                     //Natural: ASSIGN #CI2 := 46
            }                                                                                                                                                             //Natural: VALUE 'CP', 'RP', 'PR'
            else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
                || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
            {
                decideConditionsMet1165++;
                pnd_Ci2.setValue(46);                                                                                                                                     //Natural: ASSIGN #CI2 := 46
            }                                                                                                                                                             //Natural: VALUE 'S', 'SN'
            else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN"))))
            {
                decideConditionsMet1165++;
                pnd_Ci2.setValue(47);                                                                                                                                     //Natural: ASSIGN #CI2 := 47
            }                                                                                                                                                             //Natural: VALUE 'SP'
            else if (condition((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
            {
                decideConditionsMet1165++;
                pnd_Ci2.setValue(47);                                                                                                                                     //Natural: ASSIGN #CI2 := 47
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1165 > 0))
            {
                ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Type_Cde().getValue(pnd_Ci2).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Type_Cde());                                //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-TYPE-CDE ( #CI2 ) := FCP-CONS-PYMNT.CNTRCT-TYPE-CDE
                ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue(pnd_Ci2).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue("*"));                  //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( * ) TO FCP-CONS-CNTRL.CNTL-GROSS-AMT ( #CI2 )
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 01
                {
                    ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue(pnd_Ci2).nadd(1);                                                                                   //Natural: ADD 1 TO FCP-CONS-CNTRL.CNTL-CNT ( #CI2 )
                    ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue(pnd_Ci2).nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                 //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO FCP-CONS-CNTRL.CNTL-NET-AMT ( #CI2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Write_Cancel_Stop_Or_Checks() throws Exception                                                                                                       //Natural: WRITE-CANCEL-STOP-OR-CHECKS
    {
        if (BLNatReinput.isReinput()) return;

        //*  IB 9/16/99
        pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte.setValue(pnd_Cnr_Orgnl_Invrse_Dte);                                                                                     //Natural: MOVE #CNR-ORGNL-INVRSE-DTE TO #WS-HEADER-RECORD.CNR-ORGNL-INVRSE-DTE
        //*  JWO 2010-11
        short decideConditionsMet1191 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'S', 'CN', 'SN', 'CP', 'SP', 'RP', 'PR'
        if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S") 
            || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP") 
            || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR"))))
        {
            decideConditionsMet1191++;
            pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(10);                                                                                                             //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 10
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(1);                                                                                                              //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = 1
            getWorkFiles().write(2, false, pnd_Ws_Key, pnd_Ws_Header_Record);                                                                                             //Natural: WRITE WORK FILE 2 #WS-KEY #WS-HEADER-RECORD
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-OCCURS-RECORD-2
            sub_Separate_N_Write_Occurs_Record_2();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-NAME-N-ADDR-2
            sub_Separate_N_Write_Name_N_Addr_2();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R"))))
        {
            decideConditionsMet1191++;
            pnd_Ws_Key_Pnd_Ws_Record_Level_Nmbr.setValue(10);                                                                                                             //Natural: ASSIGN #WS-RECORD-LEVEL-NMBR = 10
            pnd_Ws_Key_Pnd_Ws_Record_Occur_Nmbr.setValue(1);                                                                                                              //Natural: ASSIGN #WS-RECORD-OCCUR-NMBR = 1
            getWorkFiles().write(8, false, pnd_Ws_Key, pnd_Ws_Header_Record);                                                                                             //Natural: WRITE WORK FILE 8 #WS-KEY #WS-HEADER-RECORD
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-OCCURS-RECORD
            sub_Separate_N_Write_Occurs_Record();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM SEPARATE-N-WRITE-NAME-N-ADDR-RECORD
            sub_Separate_N_Write_Name_N_Addr_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            getReports().write(0, ReportOption.NOTITLE,"UNEXPECTED VALUE FOR","#WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE IS:",pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde); //Natural: WRITE 'UNEXPECTED VALUE FOR' '#WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE IS:' #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Write_Control_Rec() throws Exception                                                                                                                 //Natural: WRITE-CONTROL-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  CALLNAT 'FCPN115' USING FCPA115   - CHANGED TO CPWN115
        DbsUtil.callnat(Cpwn115.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                               //Natural: CALLNAT 'CPWN115' USING CPWA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa115.getCpwa115_Error_Code().notEquals(getZero())))                                                                                           //Natural: IF CPWA115.ERROR-CODE NE 0
        {
            getReports().write(0, ReportOption.NOTITLE,"ERROR: FCPP375 detects invalid RETURN-CODE of",pdaCpwa115.getCpwa115_Error_Code(),"from",pdaCpwa115.getCpwa115_Error_Program()); //Natural: WRITE 'ERROR: FCPP375 detects invalid RETURN-CODE of' CPWA115.ERROR-CODE 'from' CPWA115.ERROR-PROGRAM
            if (Global.isEscape()) return;
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().setValue("CI");                                                                                                     //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-ORGN-CDE := 'CI'
        pnd_Date_Numeric.setValue(pdaCpwa115.getCpwa115_Interface_Date());                                                                                                //Natural: ASSIGN #DATE-NUMERIC := CPWA115.INTERFACE-DATE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cycle_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Numeric_Pnd_Date_Alpha);                                    //Natural: MOVE EDITED #DATE-ALPHA TO FCP-CONS-CNTRL.CNTL-CYCLE-DTE ( EM = YYYYMMDD )
        pnd_Date_Numeric.setValue(pdaCpwa115.getCpwa115_Payment_Date());                                                                                                  //Natural: ASSIGN #DATE-NUMERIC := CPWA115.PAYMENT-DATE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Numeric_Pnd_Date_Alpha);                                    //Natural: MOVE EDITED #DATE-ALPHA TO FCP-CONS-CNTRL.CNTL-CHECK-DTE ( EM = YYYYMMDD )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte().compute(new ComputeParameters(false, ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte()), (DbsField.subtract(100000000, //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-INVRSE-DTE := ( 100000000 - CPWA115.PAYMENT-DATE )
            pdaCpwa115.getCpwa115_Payment_Date())));
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().insertDBRow();                                                                                                                 //Natural: STORE FCP-CONS-CNTRL
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(0),  //Natural: WRITE NOTITLE *PROGRAM 41T #HEADER0-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
