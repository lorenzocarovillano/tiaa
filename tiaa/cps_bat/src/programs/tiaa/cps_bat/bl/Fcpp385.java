/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:31 PM
**        * FROM NATURAL PROGRAM : Fcpp385
************************************************************
**        * FILE NAME            : Fcpp385.java
**        * CLASS NAME           : Fcpp385
**        * INSTANCE NAME        : Fcpp385
************************************************************
************************************************************************
* PROGRAM  : FCPP385
* SYSTEM   : CPS
* TITLE    : UPDATE DATABASE WITH CHECK NUMBERS
* AUTHOR   : LEON SILBERSTEIN
* GENERATED: JUL 29,93 AT 04:58 PM
* FUNCTION : THIS PROGRAM WILL UPDATE ADABAS PAYMENT FILE WITH
*            CHECK NUMBER AND CHECK SEQUENCE NUMBER.
*            ALSO, IT LISTS THE "Missing Checks" I.E., CHECK NUMBERS
*            WHICH HAVE NOT BEEN ISSUED TO CHECKS
* HISTORY  :
*          : 01/25/1999  RITA SALGADO
*          : - CREATE ANNOTATION RECORD FOR EXPEDITED HOLDS (OV00;USPS)
*          :
*          : 08/05/99 - LEON GURTOVNIK
*          :  MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*          :  NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
* 02-16-00 : ALTHEA A. YOUNG
*          : REVISED TO PREVENT A DUPLICATE ANNOTATION RECORD FROM BEING
*          : CREATED FOR CANCELLED / STOPPED RECORDS WITH 'OV00' OR
*          : 'USPS' HOLD CODES.
*
* 01-02-01 : ROXAN CARREON
*          : PRIOR CHANGES FAILED IT's purpose of preventing duplicates.
*          : REVISED TO ALLOW UPDATES FOR EXISTING ANNOTATION RECORDS
*          : FOR PAYMENTS WITH OV00 AND USPS
*          : DEFINE ANNOTAION KEY FOR RETRIEVAL
*  T MCGEE           /* TMM 02/2001 - GLOBAL PAY
*    11-02 : R CARREON  STOW - EGTRRA CHANGES IN LDA
* R. LANDRUM    02/22/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
*
* 5/2/08 - NEEDS RESTOW TO PICK UP NAT 3 VERSION OF FCPCUSPS - AER
* 04/20/10 -  CM - RESTOW FOR CHANGE IN FCPLPMNU
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp385 extends BLNatBase
{
    // Data Areas
    private LdaFcplcntr ldaFcplcntr;
    private LdaFcplpmnu ldaFcplpmnu;
    private LdaFcplannu ldaFcplannu;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_1;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Program;
    private DbsField pnd_Abend_Code;
    private DbsField pnd_Ci1;
    private DbsField pnd_Ci2;
    private DbsField pnd_Cntl_Isn;

    private DbsGroup pnd_Ct_Cntrct_Typ;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Type_Code;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Cnt;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt;
    private DbsField pnd_Ct_Cntrct_Typ_Pnd_Net_Amt;

    private DbsGroup pnd_Ct_Pay_Typ;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Pay_Code;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Cnt;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Gross_Amt;
    private DbsField pnd_Ct_Pay_Typ_Pnd_Net_Amt;
    private DbsField pnd_Ws_Gross_Amt;
    private DbsField pnd_Ws_Prime_Key;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;

    private DbsGroup pnd_C_Recs_Read;
    private DbsField pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_10;
    private DbsField pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_20;
    private DbsField pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_30;
    private DbsField pnd_C_Recs_Read_Pnd_C_Recs_Read_Other;
    private DbsField pnd_C_Recs_Read_Pnd_C_Recs_Read_All;
    private DbsField pnd_C_Updated;
    private DbsField pnd_C_Not_Updated;
    private DbsField pnd_C_Missing_Checks;
    private DbsField pnd_C_Waiting_For_Et;
    private DbsField pnd_Ws_Check_Nbr;
    private DbsField pnd_Old_Chk_Nbr;
    private DbsField pnd_Prev_Hold_Cde;
    private DbsField pnd_First;
    private DbsField pnd_Pymnt_Record_Updated;
    private DbsField pnd_Ws_Key;

    private DbsGroup pnd_Ws_Key__R_Field_2;

    private DbsGroup pnd_Ws_Key_Pnd_Ws_Key_Level2;
    private DbsField pnd_Ws_Key_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Key_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Key__R_Field_3;
    private DbsField pnd_Ws_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record;

    private DbsGroup pnd_Ws_Header_Record__R_Field_4;

    private DbsGroup pnd_Ws_Header_Record_Pnd_Ws_Header_Level2;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Header_Record__R_Field_5;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Header_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;
    private DbsField pnd_Ws_Occurs;

    private DbsGroup pnd_Ws_Occurs__R_Field_6;

    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Side;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fed_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Occurs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Occurs_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Deductions;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Amt;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler;
    private DbsField pnd_Ws_Work_Rec;

    private DbsGroup pnd_Ws_Work_Rec__R_Field_7;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Filler_A;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Filler_B;
    private DbsField pnd_Ws_Annot_Key;

    private DbsGroup pnd_Ws_Annot_Key__R_Field_8;

    private DbsGroup pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplcntr = new LdaFcplcntr();
        registerRecord(ldaFcplcntr);
        registerRecord(ldaFcplcntr.getVw_fcp_Cons_Cntrl());
        ldaFcplpmnu = new LdaFcplpmnu();
        registerRecord(ldaFcplpmnu);
        registerRecord(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());
        ldaFcplannu = new LdaFcplannu();
        registerRecord(ldaFcplannu);
        registerRecord(ldaFcplannu.getVw_fcp_Cons_Annu());
        localVariables = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_1", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Abend_Code = localVariables.newFieldInRecord("pnd_Abend_Code", "#ABEND-CODE", FieldType.NUMERIC, 2);
        pnd_Ci1 = localVariables.newFieldInRecord("pnd_Ci1", "#CI1", FieldType.INTEGER, 2);
        pnd_Ci2 = localVariables.newFieldInRecord("pnd_Ci2", "#CI2", FieldType.INTEGER, 2);
        pnd_Cntl_Isn = localVariables.newFieldInRecord("pnd_Cntl_Isn", "#CNTL-ISN", FieldType.NUMERIC, 10);

        pnd_Ct_Cntrct_Typ = localVariables.newGroupArrayInRecord("pnd_Ct_Cntrct_Typ", "#CT-CNTRCT-TYP", new DbsArrayController(1, 50));
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Type_Code", "#TYPE-CODE", FieldType.STRING, 2);
        pnd_Ct_Cntrct_Typ_Pnd_Cnt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ct_Cntrct_Typ_Pnd_Net_Amt = pnd_Ct_Cntrct_Typ.newFieldInGroup("pnd_Ct_Cntrct_Typ_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Ct_Pay_Typ = localVariables.newGroupArrayInRecord("pnd_Ct_Pay_Typ", "#CT-PAY-TYP", new DbsArrayController(1, 50));
        pnd_Ct_Pay_Typ_Pnd_Pay_Code = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Pay_Code", "#PAY-CODE", FieldType.STRING, 1);
        pnd_Ct_Pay_Typ_Pnd_Cnt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ct_Pay_Typ_Pnd_Gross_Amt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ct_Pay_Typ_Pnd_Net_Amt = pnd_Ct_Pay_Typ.newFieldInGroup("pnd_Ct_Pay_Typ_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Gross_Amt = localVariables.newFieldInRecord("pnd_Ws_Gross_Amt", "#WS-GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Prime_Key = localVariables.newFieldInRecord("pnd_Ws_Prime_Key", "#WS-PRIME-KEY", FieldType.STRING, 6);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);

        pnd_C_Recs_Read = localVariables.newGroupInRecord("pnd_C_Recs_Read", "#C-RECS-READ");
        pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_10 = pnd_C_Recs_Read.newFieldInGroup("pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_10", "#C-RECS-READ-LEVEL-10", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_20 = pnd_C_Recs_Read.newFieldInGroup("pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_20", "#C-RECS-READ-LEVEL-20", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_30 = pnd_C_Recs_Read.newFieldInGroup("pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_30", "#C-RECS-READ-LEVEL-30", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_C_Recs_Read_Pnd_C_Recs_Read_Other = pnd_C_Recs_Read.newFieldInGroup("pnd_C_Recs_Read_Pnd_C_Recs_Read_Other", "#C-RECS-READ-OTHER", FieldType.PACKED_DECIMAL, 
            7);
        pnd_C_Recs_Read_Pnd_C_Recs_Read_All = pnd_C_Recs_Read.newFieldInGroup("pnd_C_Recs_Read_Pnd_C_Recs_Read_All", "#C-RECS-READ-ALL", FieldType.PACKED_DECIMAL, 
            7);
        pnd_C_Updated = localVariables.newFieldInRecord("pnd_C_Updated", "#C-UPDATED", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Not_Updated = localVariables.newFieldInRecord("pnd_C_Not_Updated", "#C-NOT-UPDATED", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Missing_Checks = localVariables.newFieldInRecord("pnd_C_Missing_Checks", "#C-MISSING-CHECKS", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Waiting_For_Et = localVariables.newFieldInRecord("pnd_C_Waiting_For_Et", "#C-WAITING-FOR-ET", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Check_Nbr = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr", "#WS-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Old_Chk_Nbr = localVariables.newFieldInRecord("pnd_Old_Chk_Nbr", "#OLD-CHK-NBR", FieldType.NUMERIC, 7);
        pnd_Prev_Hold_Cde = localVariables.newFieldInRecord("pnd_Prev_Hold_Cde", "#PREV-HOLD-CDE", FieldType.STRING, 4);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Record_Updated = localVariables.newFieldInRecord("pnd_Pymnt_Record_Updated", "#PYMNT-RECORD-UPDATED", FieldType.BOOLEAN, 1);
        pnd_Ws_Key = localVariables.newFieldInRecord("pnd_Ws_Key", "#WS-KEY", FieldType.STRING, 17);

        pnd_Ws_Key__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Key__R_Field_2", "REDEFINE", pnd_Ws_Key);

        pnd_Ws_Key_Pnd_Ws_Key_Level2 = pnd_Ws_Key__R_Field_2.newGroupInGroup("pnd_Ws_Key_Pnd_Ws_Key_Level2", "#WS-KEY-LEVEL2");
        pnd_Ws_Key_Pymnt_Check_Dte = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ws_Key_Cntrct_Orgn_Cde = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Key_Cntrct_Check_Crrncy_Cde = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);

        pnd_Ws_Key__R_Field_3 = pnd_Ws_Key_Pnd_Ws_Key_Level2.newGroupInGroup("pnd_Ws_Key__R_Field_3", "REDEFINE", pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Key_Pymnt_Prcss_Seq_Num = pnd_Ws_Key__R_Field_3.newFieldInGroup("pnd_Ws_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Ws_Key_Pymnt_Instmt_Nbr = pnd_Ws_Key__R_Field_3.newFieldInGroup("pnd_Ws_Key_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record = localVariables.newFieldArrayInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD", FieldType.STRING, 1, new DbsArrayController(1, 
            500));

        pnd_Ws_Header_Record__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Header_Record__R_Field_4", "REDEFINE", pnd_Ws_Header_Record);

        pnd_Ws_Header_Record_Pnd_Ws_Header_Level2 = pnd_Ws_Header_Record__R_Field_4.newGroupInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Level2", "#WS-HEADER-LEVEL2");
        pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", 
            "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", 
            "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", 
            "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", 
            "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", 
            "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", 
            "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", 
            "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", 
            "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", 
            "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", 
            "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", 
            "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", 
            "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", 
            "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", 
            "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", 
            "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", 
            "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", 
            "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", 
            "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", 
            "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", 
            "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", 
            "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Ws_Header_Record__R_Field_5 = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newGroupInGroup("pnd_Ws_Header_Record__R_Field_5", "REDEFINE", pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record__R_Field_5.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record__R_Field_5.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", 
            "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", 
            "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", 
            "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", 
            "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", 
            "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time", 
            "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Ws_Header_Record_Pymnt_Intrfce_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", 
            "#WS-HEADER-FILLER", FieldType.STRING, 133);
        pnd_Ws_Occurs = localVariables.newFieldArrayInRecord("pnd_Ws_Occurs", "#WS-OCCURS", FieldType.STRING, 1, new DbsArrayController(1, 500));

        pnd_Ws_Occurs__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Occurs__R_Field_6", "REDEFINE", pnd_Ws_Occurs);

        pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2 = pnd_Ws_Occurs__R_Field_6.newGroupInGroup("pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2", "#WS-OCCURS-LEVEL2");
        pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Ws_Side = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Side", "#WS-SIDE", FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Ws_Occurs_Inv_Acct_Unit_Value = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_State_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Local_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Occurs_Cntrct_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Option_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Occurs_Cntrct_Mode_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Cntr_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Occurs_Pymnt_Ded_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler", "#WS-INV-ACCT-FILLER", 
            FieldType.STRING, 188);
        pnd_Ws_Work_Rec = localVariables.newFieldArrayInRecord("pnd_Ws_Work_Rec", "#WS-WORK-REC", FieldType.STRING, 1, new DbsArrayController(1, 500));

        pnd_Ws_Work_Rec__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_Work_Rec__R_Field_7", "REDEFINE", pnd_Ws_Work_Rec);
        pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num", "#WS-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code", "#WS-CONTRACT-HOLD-CODE", 
            FieldType.STRING, 4);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Work_Rec_Pnd_Ws_Filler_A = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Filler_A", "#WS-FILLER-A", FieldType.STRING, 
            217);
        pnd_Ws_Work_Rec_Pnd_Ws_Filler_B = pnd_Ws_Work_Rec__R_Field_7.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Filler_B", "#WS-FILLER-B", FieldType.STRING, 
            242);
        pnd_Ws_Annot_Key = localVariables.newFieldInRecord("pnd_Ws_Annot_Key", "#WS-ANNOT-KEY", FieldType.STRING, 29);

        pnd_Ws_Annot_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Ws_Annot_Key__R_Field_8", "REDEFINE", pnd_Ws_Annot_Key);

        pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields = pnd_Ws_Annot_Key__R_Field_8.newGroupInGroup("pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields", "#WS-ANNOT-KEY-FIELDS");
        pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Annot_Key_Cntrct_Invrse_Dte = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Annot_Key_Cntrct_Orgn_Cde = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplcntr.initializeValues();
        ldaFcplpmnu.initializeValues();
        ldaFcplannu.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_2.setInitialValue("MONTHLY SETTLEMENT BATCH UPDATE CONTROL REPORT");
        pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_10.setInitialValue(0);
        pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_20.setInitialValue(0);
        pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_30.setInitialValue(0);
        pnd_C_Recs_Read_Pnd_C_Recs_Read_Other.setInitialValue(0);
        pnd_C_Recs_Read_Pnd_C_Recs_Read_All.setInitialValue(0);
        pnd_C_Updated.setInitialValue(0);
        pnd_C_Not_Updated.setInitialValue(0);
        pnd_First.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp385() throws Exception
    {
        super("Fcpp385");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(1).setValue("L");                                                                                                        //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 1 ) := 'L'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(2).setValue("PP");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 2 ) := 'PP'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(3).setValue("PP");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 3 ) := 'PP'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(14).setValue("L");                                                                                                       //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 14 ) := 'L'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(15).setValue("PP");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 15 ) := 'PP'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(16).setValue("PP");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 16 ) := 'PP'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(27).setValue("10");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 27 ) := '10'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(28).setValue("20");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 28 ) := '20'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(29).setValue("30");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 29 ) := '30'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(30).setValue("31");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 30 ) := '31'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(31).setValue("40");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 31 ) := '40'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(32).setValue("50");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 32 ) := '50'
        pnd_Ct_Cntrct_Typ_Pnd_Type_Code.getValue(50).setValue("CA");                                                                                                      //Natural: ASSIGN #CT-CNTRCT-TYP.#TYPE-CODE ( 50 ) := 'CA'
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* **************** RL PAYEE MATCH FEB22, 2006 BEGIN *******************
        //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        //* ***************** RL PAYEE MATCH FEB22, 2006 END ********************
        //*  ........ GET THE CONTROL RECORD OFF THE DATABASE
        ldaFcplcntr.getVw_fcp_Cons_Cntrl().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-CNTRL BY CNTL-ORG-CDE-INVRSE-DTE = 'CM'
        (
        "READ01",
        new Wc[] { new Wc("CNTL_ORG_CDE_INVRSE_DTE", ">=", "CM", WcType.BY) },
        new Oc[] { new Oc("CNTL_ORG_CDE_INVRSE_DTE", "ASC") },
        1
        );
        READ01:
        while (condition(ldaFcplcntr.getVw_fcp_Cons_Cntrl().readNextRow("READ01")))
        {
            if (condition(ldaFcplcntr.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().equals("CM")))                                                                                    //Natural: IF FCP-CONS-CNTRL.CNTL-ORGN-CDE = 'CM'
            {
                pnd_Cntl_Isn.setValue(ldaFcplcntr.getVw_fcp_Cons_Cntrl().getAstISN("Read01"));                                                                            //Natural: ASSIGN #CNTL-ISN := *ISN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cntl_Isn.reset();                                                                                                                                     //Natural: RESET #CNTL-ISN
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ........ MAKE SURE CONTROL RECORD WAS FOUND
        if (condition(pnd_Cntl_Isn.equals(getZero())))                                                                                                                    //Natural: IF #CNTL-ISN = 0
        {
            getReports().write(0, Global.getPROGRAM(),"J O B   T E R M I N A T E D:   NO CONTROL RECORD");                                                                //Natural: WRITE *PROGRAM 'J O B   T E R M I N A T E D:   NO CONTROL RECORD'
            if (Global.isEscape()) return;
            DbsUtil.terminate(77);  if (true) return;                                                                                                                     //Natural: TERMINATE 0077
        }                                                                                                                                                                 //Natural: END-IF
        //*  .... READ THE "checks" FILE AND:
        //*  ........ 1. ACCUMULATE CONTROLS
        //*  ........ 2. LIST "missing checks"
        //*  ........ 3. UPDATE THE DATABASE WITH THE ACTUAL CHECK NUMBERS
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 8 #WS-WORK-REC ( * )
        while (condition(getWorkFiles().read(8, pnd_Ws_Work_Rec.getValue("*"))))
        {
            short decideConditionsMet805 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WS-WORK-REC.#WS-RECORD-LEVEL-NMBR;//Natural: VALUE 10
            if (condition((pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(10))))
            {
                decideConditionsMet805++;
                pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_10.nadd(1);                                                                                                         //Natural: ADD 1 TO #C-RECS-READ-LEVEL-10
                pnd_Ws_Header_Record.getValue("*").setValue(pnd_Ws_Work_Rec.getValue("*"));                                                                               //Natural: MOVE #WS-WORK-REC ( * ) TO #WS-HEADER-RECORD ( * )
                                                                                                                                                                          //Natural: PERFORM ACCUM-FOR-CONTROL
                sub_Accum_For_Control();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ...... ADD FOR CONTROL TOTALS BY PAY TYPE
                pnd_Ct_Pay_Typ_Pnd_Cnt.getValue(pnd_Ci2).nadd(1);                                                                                                         //Natural: ADD 1 TO #CT-PAY-TYP.#CNT ( #CI2 )
                pnd_Ct_Pay_Typ_Pnd_Net_Amt.getValue(pnd_Ci2).nadd(pnd_Ws_Header_Record_Pymnt_Check_Amt);                                                                  //Natural: ADD #WS-HEADER-RECORD.PYMNT-CHECK-AMT TO #CT-PAY-TYP.#NET-AMT ( #CI2 )
                //*  ....... CHECK FOR MISSING CHECKS
                pnd_Ws_Key_Pnd_Ws_Key_Level2.setValuesByName(pnd_Ws_Header_Record_Pnd_Ws_Header_Level2);                                                                  //Natural: MOVE BY NAME #WS-HEADER-LEVEL2 TO #WS-KEY-LEVEL2
                //* *--> INSERTED BY 03-29-95 BY FRANK
                if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))          //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
                {
                    //*  CHECK
                    if (condition(pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1)))                                                                                 //Natural: IF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND = 1
                    {
                        if (condition(pnd_First.getBoolean()))                                                                                                            //Natural: IF #FIRST
                        {
                            pnd_First.reset();                                                                                                                            //Natural: RESET #FIRST
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Old_Chk_Nbr.add(1).notEquals(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr)))                                                 //Natural: IF #OLD-CHK-NBR + 1 NE #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
                            {
                                                                                                                                                                          //Natural: PERFORM MISSING-CHECKS
                                sub_Missing_Checks();
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Old_Chk_Nbr.setValue(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr);                                                                            //Natural: ASSIGN #OLD-CHK-NBR = #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  ......... UPDATE THE DATABASE WITH THE CHECK NUMBERS
                pnd_Ws_Key_Pymnt_Instmt_Nbr.setValue(1);                                                                                                                  //Natural: ASSIGN #WS-KEY.PYMNT-INSTMT-NBR = 01
                                                                                                                                                                          //Natural: PERFORM READ-ADABAS
                sub_Read_Adabas();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(20))))
            {
                decideConditionsMet805++;
                pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_20.nadd(1);                                                                                                         //Natural: ADD 1 TO #C-RECS-READ-LEVEL-20
                pnd_Ws_Occurs.getValue("*").setValue(pnd_Ws_Work_Rec.getValue("*"));                                                                                      //Natural: MOVE #WS-WORK-REC ( * ) TO #WS-OCCURS ( * )
                //*  ...... ADD THE CURRENT RECORD NUMBERS TO THE CONTROL TOTALS
                pnd_Ct_Cntrct_Typ_Pnd_Gross_Amt.getValue(pnd_Ci1).nadd(pnd_Ws_Occurs_Inv_Acct_Settl_Amt);                                                                 //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT TO #CT-CNTRCT-TYP.#GROSS-AMT ( #CI1 )
                //*  ...... ADD FOR CONTROL TOTALS BY PAY TYPE
                pnd_Ct_Pay_Typ_Pnd_Gross_Amt.getValue(pnd_Ci2).nadd(pnd_Ws_Occurs_Inv_Acct_Settl_Amt);                                                                    //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT TO #CT-PAY-TYP.#GROSS-AMT ( #CI2 )
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(30))))
            {
                decideConditionsMet805++;
                pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_30.nadd(1);                                                                                                         //Natural: ADD 1 TO #C-RECS-READ-LEVEL-30
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_C_Recs_Read_Pnd_C_Recs_Read_Other.nadd(1);                                                                                                            //Natural: ADD 1 TO #C-RECS-READ-OTHER
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  E.T. ALL REMAINING TRANSACTIONS
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        pnd_C_Recs_Read_Pnd_C_Recs_Read_All.compute(new ComputeParameters(false, pnd_C_Recs_Read_Pnd_C_Recs_Read_All), DbsField.add(getZero(),pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_10).add(pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_20).add(pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_30).add(pnd_C_Recs_Read_Pnd_C_Recs_Read_Other)); //Natural: ASSIGN #C-RECS-READ-ALL := 0 + #C-RECS-READ-LEVEL-10 + #C-RECS-READ-LEVEL-20 + #C-RECS-READ-LEVEL-30 + #C-RECS-READ-OTHER
        getReports().write(0, Global.getPROGRAM(),"Counters at end of program (prior to control rpt):",NEWLINE,"TOTAL MISSING CHECKS........ ",pnd_C_Missing_Checks,      //Natural: WRITE *PROGRAM 'Counters at end of program (prior to control rpt):' / 'TOTAL MISSING CHECKS........ ' #C-MISSING-CHECKS / 'number of updated database records:' #C-UPDATED / 'Number of checks not found on the database:' #C-NOT-UPDATED / 'counts of input records by record type:' / '................ all:' #C-RECS-READ-ALL / '......  10:' #C-RECS-READ-LEVEL-10 / '......  20:' #C-RECS-READ-LEVEL-20 / '......  30:' #C-RECS-READ-LEVEL-30 / '.... other:' #C-RECS-READ-OTHER
            NEWLINE,"number of updated database records:",pnd_C_Updated,NEWLINE,"Number of checks not found on the database:",pnd_C_Not_Updated,NEWLINE,
            "counts of input records by record type:",NEWLINE,"................ all:",pnd_C_Recs_Read_Pnd_C_Recs_Read_All,NEWLINE,"......  10:",pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_10,
            NEWLINE,"......  20:",pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_20,NEWLINE,"......  30:",pnd_C_Recs_Read_Pnd_C_Recs_Read_Level_30,NEWLINE,".... other:",
            pnd_C_Recs_Read_Pnd_C_Recs_Read_Other);
        if (Global.isEscape()) return;
        //*  MATCH MY COUNTS TO CONTROL RECORDS COUNT
        //*  INVOKE CONTROL REPORT VIA THIS PROGRAM
        //*  ISN OF CONTROL RECORD USED
        //*  ACCUMS TO BE COMPARED TO CONTROL RECORD ON DB
        //*  ACCUMS TO BE COMPARED TO CONTROL RECORD ON DB
        DbsUtil.callnat(Fcpn376.class , getCurrentProcessState(), pnd_Program, pnd_Cntl_Isn, pnd_Ct_Cntrct_Typ.getValue("*"), pnd_Ct_Pay_Typ.getValue("*"));              //Natural: CALLNAT 'FCPN376' #PROGRAM #CNTL-ISN #CT-CNTRCT-TYP ( * ) #CT-PAY-TYP ( * )
        if (condition(Global.isEscape())) return;
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"END OF  PROGRAM RUN");                                                                                //Natural: WRITE *PROGRAM *TIME 'END OF  PROGRAM RUN'
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*            **********    SUBROUTINES    ********
        //* ***********************************************************************
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MISSING-CHECKS
        //*  ======================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ADABAS
        //*  ======================================================================
        //* ************************** RL END-PAYEE MATCH *************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-FOR-CONTROL
        //*  VALUE 'C', 'CN'
        //*  VALUE 'S', 'SN'
        //* *********************** RL BEGIN - PAYEE MATCH ************FEB 22,2006
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //*  RL PAYEE MATCH
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  RL PAYEE MATCH
        //* ************************** RL END-PAYEE MATCH *************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ANNOT-FOR-HOLD
    }
    private void sub_Missing_Checks() throws Exception                                                                                                                    //Natural: MISSING-CHECKS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        pnd_Ws_Check_Nbr.setValue(pnd_Old_Chk_Nbr);                                                                                                                       //Natural: ASSIGN #WS-CHECK-NBR = #OLD-CHK-NBR
        pnd_Ws_Check_Nbr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #WS-CHECK-NBR
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Ws_Check_Nbr.equals(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr))) {break;}                                                                 //Natural: UNTIL #WS-CHECK-NBR = #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
            getReports().write(2, ReportOption.NOTITLE,"MISSING CHECK:",pnd_Ws_Check_Nbr, new ReportEditMask ("9999999"));                                                //Natural: WRITE ( 2 ) 'MISSING CHECK:' #WS-CHECK-NBR ( EM = 9999999 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Check_Nbr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #WS-CHECK-NBR
            pnd_C_Missing_Checks.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #C-MISSING-CHECKS
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Read_Adabas() throws Exception                                                                                                                       //Natural: READ-ADABAS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pymnt_Record_Updated.setValue(false);                                                                                                                         //Natural: ASSIGN #PYMNT-RECORD-UPDATED := FALSE
        ldaFcplpmnu.getVw_fcp_Cons_Pymnu().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNU BY CHK-DTE-ORG-CURR-TYP-SEQ STARTING FROM #WS-KEY
        (
        "READ_PYMNTS",
        new Wc[] { new Wc("CHK_DTE_ORG_CURR_TYP_SEQ", ">=", pnd_Ws_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CHK_DTE_ORG_CURR_TYP_SEQ", "ASC") }
        );
        READ_PYMNTS:
        while (condition(ldaFcplpmnu.getVw_fcp_Cons_Pymnu().readNextRow("READ_PYMNTS")))
        {
            if (condition(pnd_Ws_Key_Pymnt_Check_Dte.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Dte()) || pnd_Ws_Key_Cntrct_Orgn_Cde.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Orgn_Cde())  //Natural: IF #WS-KEY.PYMNT-CHECK-DTE NE FCP-CONS-PYMNU.PYMNT-CHECK-DTE OR #WS-KEY.CNTRCT-ORGN-CDE NE FCP-CONS-PYMNU.CNTRCT-ORGN-CDE OR #WS-KEY.CNTRCT-CHECK-CRRNCY-CDE NE FCP-CONS-PYMNU.CNTRCT-CHECK-CRRNCY-CDE OR #WS-KEY.PYMNT-PAY-TYPE-REQ-IND NE FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND OR #WS-KEY.PYMNT-PRCSS-SEQ-NUM NE FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NUM
                || pnd_Ws_Key_Cntrct_Check_Crrncy_Cde.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde()) || pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind()) 
                || pnd_Ws_Key_Pymnt_Prcss_Seq_Num.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num())))
            {
                if (true) break READ_PYMNTS;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-PYMNTS. )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pymnt_Record_Updated.setValue(true);                                                                                                                      //Natural: ASSIGN #PYMNT-RECORD-UPDATED := TRUE
            if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ")))              //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R' OR = ' '
            {
                //*  CHECKS
                if (condition(pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1)))                                                                                     //Natural: IF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND = 1
                {
                    //* *********************** RL BEGIN - PAYEE MATCH ************ MAY 10,2006
                    pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr);                                                       //Natural: MOVE #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                    pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                     //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Nbr().setValue(pnd_Ws_Check_Nbr_N10);                                                                             //Natural: MOVE #WS-CHECK-NBR-N10 TO FCP-CONS-PYMNU.PYMNT-NBR
                    pnd_Ws_Check_Nbr_N10.reset();                                                                                                                         //Natural: RESET #WS-CHECK-NBR-N10
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr().compute(new ComputeParameters(false, ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr()),                //Natural: COMPUTE FCP-CONS-PYMNU.PYMNT-CHECK-NBR := #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR * -1
                        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr.multiply(-1));
                    //* *    FCP-CONS-PYMNU.PYMNT-CHECK-NBR :=
                    //* *      #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
                    //*  EFT AND GLOBAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr().setValue(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr);                                          //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-SCRTY-NBR := #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
                }                                                                                                                                                         //Natural: END-IF
                ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr);                                            //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-SEQ-NBR := #WS-HEADER-RECORD.#WS-PYMNT-CHECK-SEQ-NBR
                //*  02-16-2000
                if (condition((ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind().equals(1) && (ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")        //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1 AND FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
                    || ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))))
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-ANNOT-FOR-HOLD
                    sub_Create_Annot_For_Hold();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PYMNTS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PYMNTS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Stats_Cde().setValue("P");                                                                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-STATS-CDE := 'P'
            pnd_C_Updated.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-UPDATED
            pnd_C_Waiting_For_Et.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #C-WAITING-FOR-ET
            ldaFcplpmnu.getVw_fcp_Cons_Pymnu().updateDBRow("READ_PYMNTS");                                                                                                //Natural: UPDATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Pymnt_Record_Updated.getBoolean()))                                                                                                             //Natural: IF #PYMNT-RECORD-UPDATED
        {
            if (condition(pnd_C_Waiting_For_Et.greaterOrEqual(150)))                                                                                                      //Natural: IF #C-WAITING-FOR-ET GE 150
            {
                pnd_C_Waiting_For_Et.reset();                                                                                                                             //Natural: RESET #C-WAITING-FOR-ET
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, Global.getPROGRAM(),"RECORD NOT UPDATED TO ADABAS FILE:",NEWLINE,"=",pnd_Ws_Key);                                                       //Natural: WRITE *PROGRAM 'RECORD NOT UPDATED TO ADABAS FILE:' / '=' #WS-KEY
            if (Global.isEscape()) return;
            pnd_C_Not_Updated.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #C-NOT-UPDATED
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_For_Control() throws Exception                                                                                                                 //Natural: ACCUM-FOR-CONTROL
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  DETERMINE INDECIES OF CONTROL COUNTERS
        //*  #CI1 = CONTROLS BY CONTRACT TYPE
        //*  #CI2 = CONTROLS BY PAYMENT TYPE (CHECK, EFT, GLOBAL PAY)
        //*  ---------------------------------------------------------------------
        //*  ............. DETERMINE INDEX BY CONTRACT TYPE
        short decideConditionsMet967 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE ' '
        if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" "))))
        {
            decideConditionsMet967++;
            if (condition(pnd_Ws_Header_Record_Cntrct_Orgn_Cde.equals("DC")))                                                                                             //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DC'
            {
                if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("L")))                                                                                          //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L'
                {
                    pnd_Ci1.setValue(1);                                                                                                                                  //Natural: ASSIGN #CI1 := 1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("PP")))                                                                                     //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'PP'
                    {
                        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.notEquals("F")))                                                                                //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND NE 'F'
                        {
                            pnd_Ci1.setValue(2);                                                                                                                          //Natural: ASSIGN #CI1 := 2
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ci1.setValue(3);                                                                                                                          //Natural: ASSIGN #CI1 := 3
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  JWO 2010-11
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'C', 'CN', 'CP', 'RP', 'PR'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN") 
            || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR"))))
        {
            decideConditionsMet967++;
            if (condition(((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("L") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("PP")) && pnd_Ws_Header_Record_Cntrct_Orgn_Cde.equals("DC")))) //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L' OR = 'PP' AND #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DC'
            {
                pnd_Ci1.setValue(12);                                                                                                                                     //Natural: ASSIGN #CI1 := 12
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((((((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("10") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("20")) || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("30"))  //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '10' OR = '20' OR = '30' OR = '31' OR = '40' OR = '50' AND #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'MS'
                || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("31")) || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("40")) || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("50")) 
                && pnd_Ws_Header_Record_Cntrct_Orgn_Cde.equals("MS"))))
            {
                pnd_Ci1.setValue(25);                                                                                                                                     //Natural: ASSIGN #CI1 := 25
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R"))))
        {
            decideConditionsMet967++;
            if (condition(pnd_Ws_Header_Record_Cntrct_Orgn_Cde.equals("DC")))                                                                                             //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DC'
            {
                if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("L")))                                                                                          //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L'
                {
                    pnd_Ci1.setValue(14);                                                                                                                                 //Natural: ASSIGN #CI1 := 14
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("PP")))                                                                                     //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'PP'
                    {
                        if (condition(pnd_Ws_Header_Record_Pymnt_Ftre_Ind.notEquals("F")))                                                                                //Natural: IF #WS-HEADER-RECORD.PYMNT-FTRE-IND NE 'F'
                        {
                            pnd_Ci1.setValue(15);                                                                                                                         //Natural: ASSIGN #CI1 := 15
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ci1.setValue(16);                                                                                                                         //Natural: ASSIGN #CI1 := 16
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Header_Record_Cntrct_Orgn_Cde.equals("MS")))                                                                                             //Natural: IF #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'MS'
            {
                if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("10")))                                                                                         //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '10'
                {
                    pnd_Ci1.setValue(27);                                                                                                                                 //Natural: ASSIGN #CI1 := 27
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("20")))                                                                                     //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '20'
                    {
                        pnd_Ci1.setValue(28);                                                                                                                             //Natural: ASSIGN #CI1 := 28
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("30")))                                                                                 //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '30'
                        {
                            pnd_Ci1.setValue(29);                                                                                                                         //Natural: ASSIGN #CI1 := 29
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("31")))                                                                             //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '31'
                            {
                                pnd_Ci1.setValue(30);                                                                                                                     //Natural: ASSIGN #CI1 := 30
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("40")))                                                                         //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '40'
                                {
                                    pnd_Ci1.setValue(31);                                                                                                                 //Natural: ASSIGN #CI1 := 31
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    if (condition(pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("50")))                                                                     //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '50'
                                    {
                                        pnd_Ci1.setValue(32);                                                                                                             //Natural: ASSIGN #CI1 := 32
                                    }                                                                                                                                     //Natural: END-IF
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  JWO 2010-11
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'S', 'SN', 'SP'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN") 
            || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP"))))
        {
            decideConditionsMet967++;
            if (condition(((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("L") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("PP")) && pnd_Ws_Header_Record_Cntrct_Orgn_Cde.equals("DC")))) //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = 'L' OR = 'PP' AND #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'DC'
            {
                pnd_Ci1.setValue(13);                                                                                                                                     //Natural: ASSIGN #CI1 := 13
            }                                                                                                                                                             //Natural: END-IF
            if (condition(((((((pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("10") || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("20")) || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("30"))  //Natural: IF #WS-HEADER-RECORD.CNTRCT-TYPE-CDE = '10' OR = '20' OR = '30' OR = '31' OR = '40' OR = '50' AND #WS-HEADER-RECORD.CNTRCT-ORGN-CDE = 'MS'
                || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("31")) || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("40")) || pnd_Ws_Header_Record_Cntrct_Type_Cde.equals("50")) 
                && pnd_Ws_Header_Record_Cntrct_Orgn_Cde.equals("MS"))))
            {
                pnd_Ci1.setValue(26);                                                                                                                                     //Natural: ASSIGN #CI1 := 26
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet967 > 0))
        {
            //*  ...... ADD FOR CONTROL TOTALS BY SETTLEMENT TYPE
            pnd_Ct_Cntrct_Typ_Pnd_Cnt.getValue(pnd_Ci1).nadd(1);                                                                                                          //Natural: ADD 1 TO #CT-CNTRCT-TYP.#CNT ( #CI1 )
            pnd_Ct_Cntrct_Typ_Pnd_Net_Amt.getValue(pnd_Ci1).nadd(pnd_Ws_Header_Record_Pymnt_Check_Amt);                                                                   //Natural: ADD #WS-HEADER-RECORD.PYMNT-CHECK-AMT TO #CT-CNTRCT-TYP.#NET-AMT ( #CI1 )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet1047 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1))))
        {
            decideConditionsMet1047++;
            //*  CHECKS AND REDRAWN CHECKS
            if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))              //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
            {
                pnd_Ci2.setValue(48);                                                                                                                                     //Natural: ASSIGN #CI2 := 48
                if (condition(pnd_Prev_Hold_Cde.notEquals("0000") && pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code.equals("0000")))                                 //Natural: IF #PREV-HOLD-CDE NE '0000' AND #WS-HEADER-RECORD.#WS-SAVE-CONTRACT-HOLD-CODE = '0000'
                {
                    pnd_First.setValue(true);                                                                                                                             //Natural: ASSIGN #FIRST := TRUE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Prev_Hold_Cde.setValue(pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code);                                                                          //Natural: ASSIGN #PREV-HOLD-CDE := #WS-HEADER-RECORD.#WS-SAVE-CONTRACT-HOLD-CODE
                //*  EFT
                //*  TMM 02/2001 - GLOBAL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(2))))
        {
            decideConditionsMet1047++;
            pnd_Ci2.setValue(49);                                                                                                                                         //Natural: ASSIGN #CI2 := 49
        }                                                                                                                                                                 //Natural: VALUE 3,4,9
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(3) || pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(4) || pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(9))))
        {
            decideConditionsMet1047++;
            pnd_Ci2.setValue(50);                                                                                                                                         //Natural: ASSIGN #CI2 := 50
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  CANCELS         /* LEON 'CN' 08-05-99
        //*  CANCELS         /* JWO 2010-11
        //*  STOPS           /* LEON 'SN' 08-05-99
        //*  STOPS    /* JWO 2010-11
        short decideConditionsMet1071 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'CN'
        if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN"))))
        {
            decideConditionsMet1071++;
            pnd_Ci2.setValue(46);                                                                                                                                         //Natural: ASSIGN #CI2 := 46
        }                                                                                                                                                                 //Natural: VALUE 'CP', 'RP', 'PR'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP") 
            || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR"))))
        {
            decideConditionsMet1071++;
            pnd_Ci2.setValue(46);                                                                                                                                         //Natural: ASSIGN #CI2 := 46
        }                                                                                                                                                                 //Natural: VALUE 'S', 'SN'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN"))))
        {
            decideConditionsMet1071++;
            pnd_Ci2.setValue(47);                                                                                                                                         //Natural: ASSIGN #CI2 := 47
        }                                                                                                                                                                 //Natural: VALUE 'SP'
        else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP"))))
        {
            decideConditionsMet1071++;
            pnd_Ci2.setValue(47);                                                                                                                                         //Natural: ASSIGN #CI2 := 47
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* RL
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Create_Annot_For_Hold() throws Exception                                                                                                             //Natural: CREATE-ANNOT-FOR-HOLD
    {
        if (BLNatReinput.isReinput()) return;

        //*   CHECK FOR EXISTENCE OF ANNOTATION RECORD TO PREVENT DUPLICATE - RCC
        if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Annot_Ind().notEquals("Y")))                                                                                    //Natural: IF FCP-CONS-PYMNU.PYMNT-ANNOT-IND NE 'Y'
        {
            ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Annot_Ind().setValue("Y");                                                                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-ANNOT-IND := 'Y'
            ldaFcplannu.getVw_fcp_Cons_Annu().setValuesByName(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());                                                                        //Natural: MOVE BY NAME FCP-CONS-PYMNU TO FCP-CONS-ANNU
            ldaFcplannu.getFcp_Cons_Annu_Cntrct_Rcrd_Typ().setValue("4");                                                                                                 //Natural: ASSIGN FCP-CONS-ANNU.CNTRCT-RCRD-TYP := '4'
            if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")))                                                                                //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00'
            {
                ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "expedited mail: Tracking #"));            //Natural: COMPRESS ' Check sent' *DATU 'expedited mail: Tracking #' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))                                                                            //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'USPS'
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "USPS overnight"));                    //Natural: COMPRESS ' Check sent' *DATU 'USPS overnight' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplannu.getVw_fcp_Cons_Annu().insertDBRow();                                                                                                              //Natural: STORE FCP-CONS-ANNU
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   IF ANNOTATION RECORD EXIST - ROXAN
            pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.setValuesByName(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());                                                                 //Natural: MOVE BY NAME FCP-CONS-PYMNU TO #WS-ANNOT-KEY-FIELDS
            ldaFcplannu.getVw_fcp_Cons_Annu().startDatabaseFind                                                                                                           //Natural: FIND FCP-CONS-ANNU WITH PPCN-INV-ORGN-PRCSS-INST = #WS-ANNOT-KEY
            (
            "PND_PND_L0260",
            new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", "=", pnd_Ws_Annot_Key, WcType.WITH) }
            );
            PND_PND_L0260:
            while (condition(ldaFcplannu.getVw_fcp_Cons_Annu().readNextRow("PND_PND_L0260")))
            {
                ldaFcplannu.getVw_fcp_Cons_Annu().setIfNotFoundControlFlag(false);
                if (condition(ldaFcplannu.getFcp_Cons_Annu_Cntrct_Rcrd_Typ().notEquals("4")))                                                                             //Natural: IF FCP-CONS-ANNU.CNTRCT-RCRD-TYP NE '4'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   KEEP LINE1 IN LINE2 WHEN LINE2 IS EMPTY OTHERWISE OVERWRITE
                if (condition(ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt().equals(" ")))                                                                           //Natural: IF FCP-CONS-ANNU.PYMNT-RMRK-LINE2-TXT = ' '
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt().setValue(ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt());                                    //Natural: ASSIGN FCP-CONS-ANNU.PYMNT-RMRK-LINE2-TXT := FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")))                                                                            //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00'
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "expedited mail: Tracking #"));        //Natural: COMPRESS ' Check sent' *DATU 'expedited mail: Tracking #' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))                                                                        //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'USPS'
                    {
                        ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "USPS overnight"));                //Natural: COMPRESS ' Check sent' *DATU 'USPS overnight' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaFcplannu.getVw_fcp_Cons_Annu().updateDBRow("PND_PND_L0260");                                                                                           //Natural: UPDATE ( ##L0260. )
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),"Page",getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 41T #HEADER0-1 124T 'Page' *PAGE-NUMBER ( 2 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
