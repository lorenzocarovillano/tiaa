/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:49 PM
**        * FROM NATURAL PROGRAM : Fcpp851
************************************************************
**        * FILE NAME            : Fcpp851.java
**        * CLASS NAME           : Fcpp851
**        * INSTANCE NAME        : Fcpp851
************************************************************
************************************************************************
* PROGRAM   : FCPP851 - CLONE OF FCPP809
* SYSTEM    : CPS
* TITLE     : DEDUCTIONS REPORTS FOR HARVARD
* GENERATED : ON 03/10/2005 BY E. BOTTERI
* FUNCTION  : A WORK FILE OF VOLUNTARY DEDUCTION RECORDS WERE CREATED
*           : FROM FCPP804 AND THEM DIVIDED BY FCPP850 TO SELECT ONLY
*           : HARVARD RECORDS.  FROM THIS EXTRACTED/SELECTED FILE
*           : GENERATE A REPORT OF ALL VOLUNTARY DEDUCTIONS THAT
*           : REDUCED THE ANNUITANT's net payment:
*           :   1 - BY DEDUCTION CODE WITHIN PAYEE CODE
*           :   2 - BY PAYEE CODE WITHIN TIAA
*           :   3 - BY PAYEE CODE WITHIN CREF
*           :   8 - BY PAYEE CODE WITHIN LIFE
*           :   4 - GRAND TOTAL BY PAYEE CODE
*           :   5 - DEDUCTION CODE NOT 001 THRU 010
*           :   6 - CANADA TAXES FOR TIAA CONTRACTS
*           :   7 - CANADA TAXES FOR CREF CONTRACTS
*           : CREATES A LONG TERM CARD DEDUCTION FILE FOR INS SERVICES
*           :
*           : SORT : DED'n code, ded'N PAYEE CODE, PPCN
*           :
* HISTORY   :
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp851 extends BLNatBase
{
    // Data Areas
    private LdaFcpl804 ldaFcpl804;
    private LdaFcpl809 ldaFcpl809;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField payee_Break;
    private DbsField deduction_Break;
    private DbsField deduction_Lit;
    private DbsField pnd_Index;
    private DbsField pnd_Ph_Name;

    private DbsGroup pnd_Ph_Name__R_Field_1;
    private DbsField pnd_Ph_Name__Filler1;
    private DbsField pnd_Ph_Name_Pnd_Ph_First_Name;
    private DbsField pnd_Payee_Cde;
    private DbsField pnd_Ded_Zeros;

    private DbsGroup pnd_Ded_Amounts;
    private DbsField pnd_Ded_Amounts_Pnd_Ded_Amt;
    private DbsField pnd_Ded_Amounts_Pnd_Tot_Ded_Amt;
    private DbsField pnd_Tiaa_Ded_Amt;
    private DbsField pnd_Cref_Ded_Amt;
    private DbsField pnd_Life_Ded_Amt;
    private DbsField pnd_Tcl_Ded_Amt;
    private DbsField pnd_Tiaa_Canada_Tax;
    private DbsField pnd_Cref_Canada_Tax;
    private DbsField pnd_Ltc_Rec_Cnt;
    private DbsField pnd_Ltc_Ded_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl804 = new LdaFcpl804();
        registerRecord(ldaFcpl804);
        ldaFcpl809 = new LdaFcpl809();
        registerRecord(ldaFcpl809);

        // Local Variables
        localVariables = new DbsRecord();
        payee_Break = localVariables.newFieldInRecord("payee_Break", "PAYEE-BREAK", FieldType.BOOLEAN, 1);
        deduction_Break = localVariables.newFieldInRecord("deduction_Break", "DEDUCTION-BREAK", FieldType.BOOLEAN, 1);
        deduction_Lit = localVariables.newFieldInRecord("deduction_Lit", "DEDUCTION-LIT", FieldType.STRING, 20);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 7);
        pnd_Ph_Name = localVariables.newFieldInRecord("pnd_Ph_Name", "#PH-NAME", FieldType.STRING, 39);

        pnd_Ph_Name__R_Field_1 = localVariables.newGroupInRecord("pnd_Ph_Name__R_Field_1", "REDEFINE", pnd_Ph_Name);
        pnd_Ph_Name__Filler1 = pnd_Ph_Name__R_Field_1.newFieldInGroup("pnd_Ph_Name__Filler1", "_FILLER1", FieldType.STRING, 16);
        pnd_Ph_Name_Pnd_Ph_First_Name = pnd_Ph_Name__R_Field_1.newFieldInGroup("pnd_Ph_Name_Pnd_Ph_First_Name", "#PH-FIRST-NAME", FieldType.STRING, 23);
        pnd_Payee_Cde = localVariables.newFieldInRecord("pnd_Payee_Cde", "#PAYEE-CDE", FieldType.STRING, 8);
        pnd_Ded_Zeros = localVariables.newFieldInRecord("pnd_Ded_Zeros", "#DED-ZEROS", FieldType.PACKED_DECIMAL, 12);

        pnd_Ded_Amounts = localVariables.newGroupArrayInRecord("pnd_Ded_Amounts", "#DED-AMOUNTS", new DbsArrayController(1, 4));
        pnd_Ded_Amounts_Pnd_Ded_Amt = pnd_Ded_Amounts.newFieldArrayInGroup("pnd_Ded_Amounts_Pnd_Ded_Amt", "#DED-AMT", FieldType.PACKED_DECIMAL, 11, 2, 
            new DbsArrayController(1, 12));
        pnd_Ded_Amounts_Pnd_Tot_Ded_Amt = pnd_Ded_Amounts.newFieldArrayInGroup("pnd_Ded_Amounts_Pnd_Tot_Ded_Amt", "#TOT-DED-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 12));
        pnd_Tiaa_Ded_Amt = localVariables.newFieldInRecord("pnd_Tiaa_Ded_Amt", "#TIAA-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cref_Ded_Amt = localVariables.newFieldInRecord("pnd_Cref_Ded_Amt", "#CREF-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Life_Ded_Amt = localVariables.newFieldInRecord("pnd_Life_Ded_Amt", "#LIFE-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tcl_Ded_Amt = localVariables.newFieldInRecord("pnd_Tcl_Ded_Amt", "#TCL-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tiaa_Canada_Tax = localVariables.newFieldInRecord("pnd_Tiaa_Canada_Tax", "#TIAA-CANADA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cref_Canada_Tax = localVariables.newFieldInRecord("pnd_Cref_Canada_Tax", "#CREF-CANADA-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ltc_Rec_Cnt = localVariables.newFieldInRecord("pnd_Ltc_Rec_Cnt", "#LTC-REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ltc_Ded_Amt = localVariables.newFieldInRecord("pnd_Ltc_Ded_Amt", "#LTC-DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl804.initializeValues();
        ldaFcpl809.initializeValues();

        localVariables.reset();
        payee_Break.setInitialValue(true);
        deduction_Break.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp851() throws Exception
    {
        super("Fcpp851");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP851", onError);
        setupReports();
        //*  ------------------------------------------------
        //*  DEDUCTION LIST
        //*  --                                                                                                                                                           //Natural: FORMAT ( 01 ) LS = 132 PS = 55
        //*  TIAA DEDUCTIONS SUMMARY
        //*  CREF DEDUCTIONS SUMMARY                                                                                                                                      //Natural: FORMAT ( 02 ) LS = 132 PS = 55
        //*  LIFE DEDUCTIONS SUMMARY                                                                                                                                      //Natural: FORMAT ( 03 ) LS = 132 PS = 55
        //*  TOTAL DEDUCTIONS SUMMARY                                                                                                                                     //Natural: FORMAT ( 08 ) LS = 132 PS = 55
        //*  --                                                                                                                                                           //Natural: FORMAT ( 04 ) LS = 132 PS = 55
        //*  DEDUCTIONS WITH CODE NOT 001 THRU 010
        //*  --                                                                                                                                                           //Natural: FORMAT ( 05 ) LS = 132 PS = 55
        //*  TIAA DEDUCTIONS WITH CANADIAN TAXES
        //*  CREF DEDUCTIONS WITH CANADIAN TAXES                                                                                                                          //Natural: FORMAT ( 06 ) LS = 132 PS = 55
        //*  ------------------------------------------------                                                                                                             //Natural: FORMAT ( 07 ) LS = 132 PS = 55
        //*  ------------------------------------------------                                                                                                             //Natural: ON ERROR
        //*  READ EXTRACTED DEDUCTIONS FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 DEDUCTION-REC
        while (condition(getWorkFiles().read(1, ldaFcpl804.getDeduction_Rec())))
        {
            if (condition(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().greaterOrEqual(1) && ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().lessOrEqual(11)))                  //Natural: IF PYMNT-DED-CDE = 001 THRU 011
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().display(5, "/Combine #",                                                                                                                     //Natural: DISPLAY ( 05 ) '/Combine #' CNTRCT-CMBN-NBR 'Contract#/Payee stat' CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 'Deduction/Code' PYMNT-DED-CDE 'Deduction/ Payee Code' PYMNT-DED-PAYEE-CDE 'Deduction/Amount' PYMNT-DED-AMT ( EM = ZZZ,ZZZ,ZZZ.99- )
                		ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),"Contract#/Payee stat",
                		ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"Deduction/Code",
                		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(),"Deduction/ Payee Code",
                		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(),"Deduction/Amount",
                		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(), ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(), ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(),  //Natural: END-ALL
                ldaFcpl804.getDeduction_Rec_Cntrct_Company(), ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde(), ldaFcpl804.getDeduction_Rec_Ph_First_Name(), 
                ldaFcpl804.getDeduction_Rec_Ph_Middle_Name(), ldaFcpl804.getDeduction_Rec_Ph_Last_Name(), ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(), 
                ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde(), ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr(), 
                ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde(), ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde(), ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Seq());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  ------------------------------------------------
        //*  07/03/96
        getSort().sortData(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(), ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(), ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr()); //Natural: SORT BY PYMNT-DED-CDE PYMNT-DED-PAYEE-CDE CNTRCT-PPCN-NBR USING CNTRCT-COMPANY CNTRCT-PAYEE-CDE PH-FIRST-NAME PH-MIDDLE-NAME PH-LAST-NAME CNTRCT-CMBN-NBR CNTRCT-MODE-CDE PYMNT-DED-AMT ANNT-SOC-SEC-NBR PYMNT-SUSPEND-CDE CNTRCT-HOLD-CDE PYMNT-DED-PAYEE-SEQ
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(), ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(), ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), 
            ldaFcpl804.getDeduction_Rec_Cntrct_Company(), ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde(), ldaFcpl804.getDeduction_Rec_Ph_First_Name(), ldaFcpl804.getDeduction_Rec_Ph_Middle_Name(), 
            ldaFcpl804.getDeduction_Rec_Ph_Last_Name(), ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(), ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde(), ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), 
            ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr(), ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde(), ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde(), 
            ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Seq())))
        {
            CheckAtStartofData154();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 01 ) TITLE LEFT *INIT-USER '-' *PROGRAM '1' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 45T 'ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS' / *TIMX ( EM = HH:II' 'AP ) 47T 'DEDUCTIONS REPORT FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) / '-' ( 131 ) / 'DEDUCTION CODE      : ' PYMNT-DED-CDE ( EM = 999 ) 5X DEDUCTION-LIT / 'DEDUCTION PAYEE CODE: ' PYMNT-DED-PAYEE-CDE / '-' ( 131 ) /
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ------------------------------------------------------------------
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 02 ) TITLE LEFT *INIT-USER '-' *PROGRAM '2' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 45T 'ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS' / *TIMX ( EM = HH:II' 'AP ) 41T 'TIAA SUMMARY DEDUCTION REPORT FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  -------------
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 03 ) TITLE LEFT *INIT-USER '-' *PROGRAM '3' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 45T 'ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS' / *TIMX ( EM = HH:II' 'AP ) 41T 'CREF SUMMARY DEDUCTION REPORT FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  -------------
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 08 ) TITLE LEFT *INIT-USER '-' *PROGRAM '8' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 08 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 45T 'ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS' / *TIMX ( EM = HH:II' 'AP ) 41T 'Life SUMMARY DEDUCTION REPORT FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  -------------
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 04 ) TITLE LEFT *INIT-USER '-' *PROGRAM '4' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 45T 'ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS' / *TIMX ( EM = HH:II' 'AP ) 38T 'GRAND TOTAL SUMMARY DEDUCTION REPORT FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) /
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ------------------------------------------------------------------
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 06 ) TITLE LEFT *INIT-USER '-' *PROGRAM '6' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 45T 'ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS' / *TIMX ( EM = HH:II' 'AP ) 38T 'TIAA CONTRACTS WITH CANADIAN TAXES FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) / '-' ( 131 ) / 'DEDUCTION CODE      : ' PYMNT-DED-CDE ( EM = 999 ) 5X DEDUCTION-LIT / 'DEDUCTION PAYEE CODE: ' PYMNT-DED-PAYEE-CDE / '-' ( 131 ) /
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  -------------
            if (condition(Global.isEscape()))                                                                                                                             //Natural: WRITE ( 07 ) TITLE LEFT *INIT-USER '-' *PROGRAM '7' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 45T 'ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS' / *TIMX ( EM = HH:II' 'AP ) 38T 'CREF CONTRACTS WITH CANADIAN TAXES FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) / '-' ( 131 ) / 'DEDUCTION CODE      : ' PYMNT-DED-CDE ( EM = 999 ) 5X DEDUCTION-LIT / 'DEDUCTION PAYEE CODE: ' PYMNT-DED-PAYEE-CDE / '-' ( 131 ) /
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ---------------------------------------------------
            //*  BEFORE BREAK PROCESSING
            //*  END-BEFORE
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF PYMNT-DED-CDE
            //*                                                                                                                                                           //Natural: AT BREAK OF PYMNT-DED-PAYEE-CDE
            //*                                                                                                                                                           //Natural: AT END OF DATA
                                                                                                                                                                          //Natural: PERFORM BREAK-ROUTINE
            sub_Break_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-ROUTINE
            sub_Initialize_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ph_Name.setValue(ldaFcpl804.getDeduction_Rec_Ph_Last_Name());                                                                                             //Natural: MOVE PH-LAST-NAME TO #PH-NAME
            pnd_Ph_Name_Pnd_Ph_First_Name.setValue(DbsUtil.compress(ldaFcpl804.getDeduction_Rec_Ph_First_Name(), ldaFcpl804.getDeduction_Rec_Ph_Middle_Name()));          //Natural: COMPRESS PH-FIRST-NAME PH-MIDDLE-NAME INTO #PH-FIRST-NAME
            getReports().display(1, new ReportMatrixColumnUnderline("-"),"Ded/Cde",                                                                                       //Natural: DISPLAY ( 01 ) ( UC = - ) 'Ded/Cde' PYMNT-DED-CDE ( EM = 999 ) 'Ded/Payee' PYMNT-DED-PAYEE-CDE 'Contract#' CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 'Payee/Stat' CNTRCT-PAYEE-CDE '/Annuitant Name' #PH-NAME '/Combine #' CNTRCT-CMBN-NBR 'Mode/Code' CNTRCT-MODE-CDE '/Deductions' PYMNT-DED-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) '/Soc Sec No.' ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'Pend/Code' PYMNT-SUSPEND-CDE 'Hold/Code' CNTRCT-HOLD-CDE
            		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(), new ReportEditMask ("999"),"Ded/Payee",
            		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(),"Contract#",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"Payee/Stat",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde(),"/Annuitant Name",
            		pnd_Ph_Name,"/Combine #",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),"Mode/Code",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde(),"/Deductions",
            		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"/Soc Sec No.",
            		ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"Pend/Code",
            		ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde(),"Hold/Code",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde());
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaFcpl804.getDeduction_Rec_Cntrct_Company().equals("T")))                                                                                      //Natural: IF CNTRCT-COMPANY = 'T'
            {
                //*  TIAA
                pnd_Tiaa_Ded_Amt.nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                                                                                       //Natural: ADD PYMNT-DED-AMT TO #TIAA-DED-AMT
                pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde()).nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                    //Natural: ADD PYMNT-DED-AMT TO #DED-AMT ( 1,PYMNT-DED-CDE )
                pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde()).nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                //Natural: ADD PYMNT-DED-AMT TO #TOT-DED-AMT ( 1,PYMNT-DED-CDE )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl804.getDeduction_Rec_Cntrct_Company().equals("C")))                                                                                  //Natural: IF CNTRCT-COMPANY = 'C'
                {
                    //*  CREF
                    pnd_Cref_Ded_Amt.nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                                                                                   //Natural: ADD PYMNT-DED-AMT TO #CREF-DED-AMT
                    pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde()).nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                //Natural: ADD PYMNT-DED-AMT TO #DED-AMT ( 2,PYMNT-DED-CDE )
                    pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde()).nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());            //Natural: ADD PYMNT-DED-AMT TO #TOT-DED-AMT ( 2,PYMNT-DED-CDE )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  LIFE
                    pnd_Life_Ded_Amt.nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                                                                                   //Natural: ADD PYMNT-DED-AMT TO #LIFE-DED-AMT
                    pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde()).nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                //Natural: ADD PYMNT-DED-AMT TO #DED-AMT ( 3,PYMNT-DED-CDE )
                    pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde()).nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());            //Natural: ADD PYMNT-DED-AMT TO #TOT-DED-AMT ( 3,PYMNT-DED-CDE )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  TOTAL
            pnd_Tcl_Ded_Amt.nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                                                                                            //Natural: ADD PYMNT-DED-AMT TO #TCL-DED-AMT
            pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde()).nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                        //Natural: ADD PYMNT-DED-AMT TO #DED-AMT ( 4,PYMNT-DED-CDE )
            pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde()).nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                    //Natural: ADD PYMNT-DED-AMT TO #TOT-DED-AMT ( 4,PYMNT-DED-CDE )
            //*  CANADIAN TAXES ARE CODED AS NYSUT DEDUCTIONS WITH A DEDUCTION
            //*  PAYEE CODE OF 'Y1650'
            if (condition(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(6) && ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde().equals("Y1650")))                    //Natural: IF PYMNT-DED-CDE = 006 AND PYMNT-DED-PAYEE-CDE = 'Y1650'
            {
                                                                                                                                                                          //Natural: PERFORM CANADIAN-TAXES
                sub_Canadian_Taxes();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  /* LZ
            //*   ACCEPT IF PYMNT-DED-CDE = 002
            //*  ROXAN INCLUDE UL
            if (condition(!(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(2) || ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(10))))                            //Natural: ACCEPT IF PYMNT-DED-CDE = 002 OR = 10
            {
                continue;
            }
            //*  RESET #LTC-DED
            //*  MOVE EDITED PYMNT-CHECK-DTE (EM=YYYYMMDD) TO #PYMNT-CHECK-DTE
            //*  MOVE PH-LAST-NAME        TO #PYMNT-NAME-A9
            //*  MOVE PH-FIRST-INIT       TO SUBSTRING(#PYMNT-NAME-A9,8,1)
            //*  MOVE PH-MIDDLE-INIT      TO SUBSTRING(#PYMNT-NAME-A9,9,1)
            //*  MOVE ANNT-SOC-SEC-NBR    TO #ANNT-SOC-SEC-NBR
            //*  MOVE PYMNT-DED-PAYEE-SEQ TO #PYMNT-DED-SEQ-NBR
            //*  MOVE CNTRCT-PPCN-NBR     TO #CNTRCT-PPCN-NBR-A8
            //*  MOVE CNTRCT-PAYEE-CDE    TO #CNTRCT-PAYEE-CDE-A2
            //*  MOVE PYMNT-DED-AMT       TO #PYMNT-DED-AMT
            //*  WRITE WORK FILE 2 #LTC-DED
            //*  ADD 1                    TO #LTC-REC-CNT
            //*  ADD PYMNT-DED-AMT        TO #LTC-DED-AMT
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        if (condition(getSort().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM BREAK-ROUTINE
            sub_Break_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        endSort();
        //*  ---------------------------------
        //*  WRITE
        //*   'TOTAL # OF LONG TERM CARE DEDUCTION RECORDS:' #LTC-REC-CNT
        //*   'TOTAL AMOUNT OF LONG TERM CARE DEDUCTION:   ' #LTC-DED-AMT
        //*  ------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-ROUTINE
        //*  ------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BREAK-ROUTINE
        //*  ------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CANADIAN-TAXES
        //*  ------------------------------------------------------------------
        //*  TIAA
        if (condition(pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,"*").greater(getZero())))                                                                                //Natural: IF #TOT-DED-AMT ( 1,* ) > 0
        {
            pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,12).compute(new ComputeParameters(false, pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,12)), pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1, //Natural: COMPUTE #TOT-DED-AMT ( 1,12 ) = #TOT-DED-AMT ( 1,1:11 ) + 0
                1,":",11).add(getZero()));
            //*  JWO 2009-09
            //*  (LB)
            //*  PA SELECT
            //*  DENTAL
            //*  UNIVERSAL LIFE
            //*  TOTAL
            getReports().write(2, NEWLINE,NEWLINE,"Total   ",pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,1), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,2),  //Natural: WRITE ( 02 ) // 'Total   ' #TOT-DED-AMT ( 1,1 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 1,2 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 1,3 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 1,4 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 1,5 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 1,6 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 1,7 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 1,8 ) ( EM = Z ( 8 ) .99- ) / 10T #TOT-DED-AMT ( 1,9 ) ( EM = Z ( 8 ) .99- ) 23T #TOT-DED-AMT ( 1,11 ) ( EM = Z ( 8 ) .99- ) 36T #TOT-DED-AMT ( 1,10 ) ( EM = Z ( 8 ) .99- ) 116T #TOT-DED-AMT ( 1,12 ) ( EM = Z ( 9 ) .99- )
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,3), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,4), 
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,5), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,6), 
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,7), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,8), 
                new ReportEditMask ("ZZZZZZZZ.99-"),NEWLINE,new TabSetting(10),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,9), new ReportEditMask ("ZZZZZZZZ.99-"),new 
                TabSetting(23),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,11), new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(36),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,10), 
                new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(116),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,12), new ReportEditMask ("ZZZZZZZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"**********************************************************",NEWLINE,new  //Natural: WRITE ( 02 ) ///// / 45T '**********************************************************' / 45T '**********************************************************' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '***       No TIAA Deductions for this Period           ***' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '**********************************************************' / 45T '**********************************************************'
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***       No TIAA Deductions for this Period           ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"**********************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------------
        //*  CREF
        if (condition(pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,"*").greater(getZero())))                                                                                //Natural: IF #TOT-DED-AMT ( 2,* ) > 0
        {
            pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,12).compute(new ComputeParameters(false, pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,12)), pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2, //Natural: COMPUTE #TOT-DED-AMT ( 2,12 ) = #TOT-DED-AMT ( 2,1:11 ) + 0
                1,":",11).add(getZero()));
            //*  JWO 2009-09
            //*  (LB)
            //*  PA SELECT
            //*  DENTAL
            //*  UNIVERSAL LIFE
            //*  TOTAL
            getReports().write(3, NEWLINE,NEWLINE,"Total   ",pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,1), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,2),  //Natural: WRITE ( 03 ) // 'Total   ' #TOT-DED-AMT ( 2,1 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 2,2 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 2,3 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 2,4 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 2,5 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 2,6 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 2,7 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 2,8 ) ( EM = Z ( 8 ) .99- ) / 10T #TOT-DED-AMT ( 2,9 ) ( EM = Z ( 8 ) .99- ) 23T #TOT-DED-AMT ( 1,11 ) ( EM = Z ( 8 ) .99- ) 36T #TOT-DED-AMT ( 2,10 ) ( EM = Z ( 8 ) .99- ) 116T #TOT-DED-AMT ( 2,12 ) ( EM = Z ( 9 ) .99- )
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,3), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,4), 
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,5), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,6), 
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,7), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,8), 
                new ReportEditMask ("ZZZZZZZZ.99-"),NEWLINE,new TabSetting(10),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,9), new ReportEditMask ("ZZZZZZZZ.99-"),new 
                TabSetting(23),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,11), new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(36),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,10), 
                new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(116),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(2,12), new ReportEditMask ("ZZZZZZZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(3, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"**********************************************************",NEWLINE,new  //Natural: WRITE ( 03 ) ///// / 45T '**********************************************************' / 45T '**********************************************************' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '***       No CREF Deductions for this Period           ***' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '**********************************************************' / 45T '**********************************************************'
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***       No CREF Deductions for this Period           ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"**********************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------------
        //*  LIFE
        if (condition(pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,"*").greater(getZero())))                                                                                //Natural: IF #TOT-DED-AMT ( 3,* ) > 0
        {
            pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,12).compute(new ComputeParameters(false, pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,12)), pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3, //Natural: COMPUTE #TOT-DED-AMT ( 3,12 ) = #TOT-DED-AMT ( 3,1:11 ) + 0
                1,":",11).add(getZero()));
            //*  JWO 2009-09
            //*  (LB)
            //*  PA SELECT
            //*  DENTAL
            //*  UNIVERSAL LIFE
            //*  TOTAL
            getReports().write(8, NEWLINE,NEWLINE,"Total   ",pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,1), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,2),  //Natural: WRITE ( 08 ) // 'Total   ' #TOT-DED-AMT ( 3,1 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 3,2 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 3,3 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 3,4 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 3,5 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 3,6 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 3,7 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 3,8 ) ( EM = Z ( 8 ) .99- ) / 10T #TOT-DED-AMT ( 3,9 ) ( EM = Z ( 8 ) .99- ) 23T #TOT-DED-AMT ( 1,11 ) ( EM = Z ( 8 ) .99- ) 36T #TOT-DED-AMT ( 3,10 ) ( EM = Z ( 8 ) .99- ) 116T #TOT-DED-AMT ( 3,12 ) ( EM = Z ( 9 ) .99- )
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,3), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,4), 
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,5), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,6), 
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,7), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,8), 
                new ReportEditMask ("ZZZZZZZZ.99-"),NEWLINE,new TabSetting(10),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,9), new ReportEditMask ("ZZZZZZZZ.99-"),new 
                TabSetting(23),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,11), new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(36),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,10), 
                new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(116),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(3,12), new ReportEditMask ("ZZZZZZZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(8, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"**********************************************************",NEWLINE,new  //Natural: WRITE ( 08 ) ///// / 45T '**********************************************************' / 45T '**********************************************************' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '***       No Life Deductions for this Period           ***' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '**********************************************************' / 45T '**********************************************************'
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***       No Life Deductions for this Period           ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"**********************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------------
        //*  TOTAL
        if (condition(pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,"*").greater(getZero())))                                                                                //Natural: IF #TOT-DED-AMT ( 4,* ) > 0
        {
            pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,12).compute(new ComputeParameters(false, pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,12)), pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4, //Natural: COMPUTE #TOT-DED-AMT ( 4,12 ) = #TOT-DED-AMT ( 4,1:11 ) + 0
                1,":",11).add(getZero()));
            //*  JWO 2009-09
            //*  (LB)
            //*  PA SELECT
            //*  DENTAL
            //*  UNIVERSAL LIFE
            //*  TOTAL
            getReports().write(4, NEWLINE,NEWLINE,"Total   ",pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,1), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,2),  //Natural: WRITE ( 04 ) // 'Total   ' #TOT-DED-AMT ( 4,1 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 4,2 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 4,3 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 4,4 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 4,5 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 4,6 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 4,7 ) ( EM = Z ( 8 ) .99- ) #TOT-DED-AMT ( 4,8 ) ( EM = Z ( 8 ) .99- ) / 10T #TOT-DED-AMT ( 4,9 ) ( EM = Z ( 8 ) .99- ) 23T #TOT-DED-AMT ( 1,11 ) ( EM = Z ( 8 ) .99- ) 36T #TOT-DED-AMT ( 4,10 ) ( EM = Z ( 8 ) .99- ) 116T #TOT-DED-AMT ( 4,12 ) ( EM = Z ( 9 ) .99- )
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,3), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,4), 
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,5), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,6), 
                new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,7), new ReportEditMask ("ZZZZZZZZ.99-"),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,8), 
                new ReportEditMask ("ZZZZZZZZ.99-"),NEWLINE,new TabSetting(10),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,9), new ReportEditMask ("ZZZZZZZZ.99-"),new 
                TabSetting(23),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(1,11), new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(36),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,10), 
                new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(116),pnd_Ded_Amounts_Pnd_Tot_Ded_Amt.getValue(4,12), new ReportEditMask ("ZZZZZZZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(4, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"**********************************************************",NEWLINE,new  //Natural: WRITE ( 04 ) ///// / 45T '**********************************************************' / 45T '**********************************************************' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '***       No Deductions for this Period                ***' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '**********************************************************' / 45T '**********************************************************'
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***       No Deductions for this Period                ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"**********************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------------------------------------------------------------
        if (condition(pnd_Tiaa_Canada_Tax.equals(getZero())))                                                                                                             //Natural: IF #TIAA-CANADA-TAX = 0
        {
            getReports().write(6, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"**********************************************************",NEWLINE,new  //Natural: WRITE ( 06 ) ///// / 45T '**********************************************************' / 45T '**********************************************************' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '***         No TIAA Canada Tax for this Period         ***' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '**********************************************************' / 45T '**********************************************************'
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***         No TIAA Canada Tax for this Period         ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"**********************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(6, NEWLINE,NEWLINE,new TabSetting(55),"    TIAA  Total",new ReportTAsterisk(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt()),pnd_Tiaa_Canada_Tax,  //Natural: WRITE ( 06 ) // 55T '    TIAA  Total' T*PYMNT-DED-AMT #TIAA-CANADA-TAX ( EM = ZZZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZZZ,ZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  -------------
        if (condition(pnd_Cref_Canada_Tax.equals(getZero())))                                                                                                             //Natural: IF #CREF-CANADA-TAX = 0
        {
            getReports().write(7, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"**********************************************************",NEWLINE,new  //Natural: WRITE ( 07 ) ///// / 45T '**********************************************************' / 45T '**********************************************************' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '***         No CREF Canada Tax for this Period         ***' / 45T '***                                                    ***' / 45T '***                                                    ***' / 45T '**********************************************************' / 45T '**********************************************************'
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***         No CREF Canada Tax for this Period         ***",NEWLINE,new 
                TabSetting(45),"***                                                    ***",NEWLINE,new TabSetting(45),"***                                                    ***",NEWLINE,new 
                TabSetting(45),"**********************************************************",NEWLINE,new TabSetting(45),"**********************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(7, NEWLINE,NEWLINE,new TabSetting(55),"    CREF  Total",new ReportTAsterisk(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt()),pnd_Cref_Canada_Tax,  //Natural: WRITE ( 07 ) // 55T '    CREF  Total' T*PYMNT-DED-AMT #CREF-CANADA-TAX ( EM = ZZZZZ,ZZZ.99- )
                new ReportEditMask ("ZZZZZ,ZZZ.99-"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Initialize_Routine() throws Exception                                                                                                                //Natural: INITIALIZE-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------
        short decideConditionsMet311 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN DEDUCTION-BREAK
        if (condition(deduction_Break.getBoolean()))
        {
            decideConditionsMet311++;
            deduction_Break.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO DEDUCTION-BREAK
            short decideConditionsMet314 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF PYMNT-DED-CDE;//Natural: VALUE 001
            if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(1))))
            {
                decideConditionsMet314++;
                deduction_Lit.setValue("Blue Cross          ");                                                                                                           //Natural: MOVE 'Blue Cross          ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 002
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(2))))
            {
                decideConditionsMet314++;
                deduction_Lit.setValue("Long Term Care      ");                                                                                                           //Natural: MOVE 'Long Term Care      ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 003
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(3))))
            {
                decideConditionsMet314++;
                deduction_Lit.setValue("Major Medical       ");                                                                                                           //Natural: MOVE 'Major Medical       ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 004
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(4))))
            {
                decideConditionsMet314++;
                deduction_Lit.setValue("Group Life          ");                                                                                                           //Natural: MOVE 'Group Life          ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 005
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(5))))
            {
                decideConditionsMet314++;
                deduction_Lit.setValue("Overpayment Recovery");                                                                                                           //Natural: MOVE 'Overpayment Recovery' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 006
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(6))))
            {
                decideConditionsMet314++;
                deduction_Lit.setValue("NYSUT               ");                                                                                                           //Natural: MOVE 'NYSUT               ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 007
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(7))))
            {
                decideConditionsMet314++;
                //*  (LB)
                deduction_Lit.setValue("Personal Annuity    ");                                                                                                           //Natural: MOVE 'Personal Annuity    ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 008
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(8))))
            {
                decideConditionsMet314++;
                //*  (LB)
                deduction_Lit.setValue("Mutual Funds        ");                                                                                                           //Natural: MOVE 'Mutual Funds        ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 009
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(9))))
            {
                decideConditionsMet314++;
                //*  JH
                //*  (LB)
                deduction_Lit.setValue("PA Select           ");                                                                                                           //Natural: MOVE 'PA Select           ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 010
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(10))))
            {
                decideConditionsMet314++;
                //*  JH
                //*  (LB)
                deduction_Lit.setValue("Universal Life      ");                                                                                                           //Natural: MOVE 'Universal Life      ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: VALUE 011
            else if (condition((ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().equals(11))))
            {
                decideConditionsMet314++;
                //*  JWO
                deduction_Lit.setValue("Dental              ");                                                                                                           //Natural: MOVE 'Dental              ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                deduction_Lit.setValue("Unknown Deduction   ");                                                                                                           //Natural: MOVE 'Unknown Deduction   ' TO DEDUCTION-LIT
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN PAYEE-BREAK
        if (condition(payee_Break.getBoolean()))
        {
            decideConditionsMet311++;
            payee_Break.setValue(false);                                                                                                                                  //Natural: MOVE FALSE TO PAYEE-BREAK
            pnd_Payee_Cde.setValue(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde());                                                                                    //Natural: MOVE PYMNT-DED-PAYEE-CDE TO #PAYEE-CDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet311 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Break_Routine() throws Exception                                                                                                                     //Natural: BREAK-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------
        short decideConditionsMet357 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN DEDUCTION-BREAK
        if (condition(deduction_Break.getBoolean()))
        {
            decideConditionsMet357++;
            getReports().write(1, NEWLINE,new TabSetting(55),"    TIAA  Total",new ReportTAsterisk(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt()),pnd_Tiaa_Ded_Amt,         //Natural: WRITE ( 01 ) / 55T '    TIAA  Total' T*PYMNT-DED-AMT #TIAA-DED-AMT ( EM = ZZZZZ,ZZZ.99- ) / 55T '    CREF  Total' T*PYMNT-DED-AMT #CREF-DED-AMT ( EM = ZZZZZ,ZZZ.99- ) / 55T '    Life  Total' T*PYMNT-DED-AMT #LIFE-DED-AMT ( EM = ZZZZZ,ZZZ.99- ) / 55T 'Deduction Total' T*PYMNT-DED-AMT #TCL-DED-AMT ( EM = ZZZZZ,ZZZ.99- ) //
                new ReportEditMask ("ZZZZZ,ZZZ.99-"),NEWLINE,new TabSetting(55),"    CREF  Total",new ReportTAsterisk(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt()),pnd_Cref_Ded_Amt, 
                new ReportEditMask ("ZZZZZ,ZZZ.99-"),NEWLINE,new TabSetting(55),"    Life  Total",new ReportTAsterisk(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt()),pnd_Life_Ded_Amt, 
                new ReportEditMask ("ZZZZZ,ZZZ.99-"),NEWLINE,new TabSetting(55),"Deduction Total",new ReportTAsterisk(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt()),pnd_Tcl_Ded_Amt, 
                new ReportEditMask ("ZZZZZ,ZZZ.99-"),NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            pnd_Tiaa_Ded_Amt.reset();                                                                                                                                     //Natural: RESET #TIAA-DED-AMT #CREF-DED-AMT #TCL-DED-AMT
            pnd_Cref_Ded_Amt.reset();
            pnd_Tcl_Ded_Amt.reset();
        }                                                                                                                                                                 //Natural: WHEN PAYEE-BREAK
        if (condition(payee_Break.getBoolean()))
        {
            decideConditionsMet357++;
            //*  TIAA
            if (condition(pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,"*").greater(getZero())))                                                                                //Natural: IF #DED-AMT ( 1,* ) > 0
            {
                pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,12).compute(new ComputeParameters(false, pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,12)), pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1, //Natural: COMPUTE #DED-AMT ( 1,12 ) = #DED-AMT ( 1,1:11 ) + 0
                    1,":",11).add(getZero()));
                //*  JWO 2009-09
                getReports().display(2, "Pay Cde",                                                                                                                        //Natural: DISPLAY ( 02 ) 'Pay Cde' #PAYEE-CDE ( HC = L ) 'Blue Cross/  PA Select' #DED-AMT ( 1,1 ) ( EM = Z ( 8 ) .99- ) 'Lng Trm Care/   Dental' #DED-AMT ( 1,2 ) ( EM = Z ( 8 ) .99- ) ' Major Med/   Univ Life' #DED-AMT ( 1,3 ) ( EM = Z ( 8 ) .99- ) 'Group Life' #DED-AMT ( 1,4 ) ( EM = Z ( 8 ) .99- ) 'Ovrpay Recov' #DED-AMT ( 1,5 ) ( EM = Z ( 8 ) .99- ) '     NYSUT' #DED-AMT ( 1,6 ) ( EM = Z ( 8 ) .99- ) 'Personl Ann' #DED-AMT ( 1,7 ) ( EM = Z ( 8 ) .99- ) 'Mutual Fund' #DED-AMT ( 1,8 ) ( EM = Z ( 8 ) .99- ) '/         TOTAL' #DED-ZEROS ( EM = Z ( 12 ) )
                		pnd_Payee_Cde, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Blue Cross/  PA Select",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,1), new ReportEditMask ("ZZZZZZZZ.99-"),"Lng Trm Care/   Dental",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,2), new ReportEditMask ("ZZZZZZZZ.99-")," Major Med/   Univ Life",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,3), new ReportEditMask ("ZZZZZZZZ.99-"),"Group Life",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,4), new ReportEditMask ("ZZZZZZZZ.99-"),"Ovrpay Recov",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,5), new ReportEditMask ("ZZZZZZZZ.99-"),"     NYSUT",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,6), new ReportEditMask ("ZZZZZZZZ.99-"),"Personl Ann",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,7), new ReportEditMask ("ZZZZZZZZ.99-"),"Mutual Fund",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,8), new ReportEditMask ("ZZZZZZZZ.99-"),"/         TOTAL",
                		pnd_Ded_Zeros, new ReportEditMask ("ZZZZZZZZZZZZ"));
                if (Global.isEscape()) return;
                //*  PA SELECT
                //*  DENTAL
                //*  UNIVERSAL LIFE
                //*  TOTAL
                getReports().write(2, new TabSetting(10),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,9), new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(23),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,11),  //Natural: WRITE ( 02 ) 10T #DED-AMT ( 1,9 ) ( EM = Z ( 8 ) .99- ) 23T #DED-AMT ( 1,11 ) ( EM = Z ( 8 ) .99- ) 36T #DED-AMT ( 1,10 ) ( EM = Z ( 8 ) .99- ) 116T #DED-AMT ( 1,12 ) ( EM = Z ( 9 ) .99- ) /
                    new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(36),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,10), new ReportEditMask ("ZZZZZZZZ.99-"),new 
                    TabSetting(116),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(1,12), new ReportEditMask ("ZZZZZZZZZ.99-"),NEWLINE);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  CREF
            if (condition(pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,"*").greater(getZero())))                                                                                //Natural: IF #DED-AMT ( 2,* ) > 0
            {
                pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,12).compute(new ComputeParameters(false, pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,12)), pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2, //Natural: COMPUTE #DED-AMT ( 2,12 ) = #DED-AMT ( 2,1:11 ) + 0
                    1,":",11).add(getZero()));
                //*  JWO 2009-09
                getReports().display(3, "Pay Cde",                                                                                                                        //Natural: DISPLAY ( 03 ) 'Pay Cde' #PAYEE-CDE ( HC = L ) 'Blue Cross/  PA Select' #DED-AMT ( 2,1 ) ( EM = Z ( 8 ) .99- ) 'Lng Trm Care/   Dental' #DED-AMT ( 2,2 ) ( EM = Z ( 8 ) .99- ) ' Major Med/   Univ Life' #DED-AMT ( 2,3 ) ( EM = Z ( 8 ) .99- ) 'Group Life' #DED-AMT ( 2,4 ) ( EM = Z ( 8 ) .99- ) 'Ovrpay Recov' #DED-AMT ( 2,5 ) ( EM = Z ( 8 ) .99- ) '     NYSUT' #DED-AMT ( 2,6 ) ( EM = Z ( 8 ) .99- ) 'Personl Ann' #DED-AMT ( 2,7 ) ( EM = Z ( 8 ) .99- ) 'Mutual Fund' #DED-AMT ( 2,8 ) ( EM = Z ( 8 ) .99- ) '/         TOTAL' #DED-ZEROS ( EM = Z ( 12 ) )
                		pnd_Payee_Cde, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Blue Cross/  PA Select",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,1), new ReportEditMask ("ZZZZZZZZ.99-"),"Lng Trm Care/   Dental",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,2), new ReportEditMask ("ZZZZZZZZ.99-")," Major Med/   Univ Life",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,3), new ReportEditMask ("ZZZZZZZZ.99-"),"Group Life",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,4), new ReportEditMask ("ZZZZZZZZ.99-"),"Ovrpay Recov",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,5), new ReportEditMask ("ZZZZZZZZ.99-"),"     NYSUT",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,6), new ReportEditMask ("ZZZZZZZZ.99-"),"Personl Ann",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,7), new ReportEditMask ("ZZZZZZZZ.99-"),"Mutual Fund",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,8), new ReportEditMask ("ZZZZZZZZ.99-"),"/         TOTAL",
                		pnd_Ded_Zeros, new ReportEditMask ("ZZZZZZZZZZZZ"));
                if (Global.isEscape()) return;
                //*  PA SELECT
                //*  DENTAL
                //*  UNIVERSAL LIFE
                //*  TOTAL
                getReports().write(3, new TabSetting(10),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,9), new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(23),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,11),  //Natural: WRITE ( 03 ) 10T #DED-AMT ( 2,9 ) ( EM = Z ( 8 ) .99- ) 23T #DED-AMT ( 2,11 ) ( EM = Z ( 8 ) .99- ) 36T #DED-AMT ( 2,10 ) ( EM = Z ( 8 ) .99- ) 116T #DED-AMT ( 2,12 ) ( EM = Z ( 9 ) .99- ) /
                    new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(36),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,10), new ReportEditMask ("ZZZZZZZZ.99-"),new 
                    TabSetting(116),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(2,12), new ReportEditMask ("ZZZZZZZZZ.99-"),NEWLINE);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  LIFE
            if (condition(pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,"*").greater(getZero())))                                                                                //Natural: IF #DED-AMT ( 3,* ) > 0
            {
                pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,12).compute(new ComputeParameters(false, pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,12)), pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3, //Natural: COMPUTE #DED-AMT ( 3,12 ) = #DED-AMT ( 3,1:11 ) + 0
                    1,":",11).add(getZero()));
                //*  JWO 2009-09
                getReports().display(8, "Pay Cde",                                                                                                                        //Natural: DISPLAY ( 08 ) 'Pay Cde' #PAYEE-CDE ( HC = L ) 'Blue Cross/  PA Select' #DED-AMT ( 3,1 ) ( EM = Z ( 8 ) .99- ) 'Lng Trm Care/   Dental' #DED-AMT ( 3,2 ) ( EM = Z ( 8 ) .99- ) ' Major Med/   Univ Life' #DED-AMT ( 3,3 ) ( EM = Z ( 8 ) .99- ) 'Group Life' #DED-AMT ( 3,4 ) ( EM = Z ( 8 ) .99- ) 'Ovrpay Recov' #DED-AMT ( 3,5 ) ( EM = Z ( 8 ) .99- ) '     NYSUT' #DED-AMT ( 3,6 ) ( EM = Z ( 8 ) .99- ) 'Personl Ann' #DED-AMT ( 3,7 ) ( EM = Z ( 8 ) .99- ) 'Mutual Fund' #DED-AMT ( 3,8 ) ( EM = Z ( 8 ) .99- ) '/         TOTAL' #DED-ZEROS ( EM = Z ( 12 ) )
                		pnd_Payee_Cde, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Blue Cross/  PA Select",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,1), new ReportEditMask ("ZZZZZZZZ.99-"),"Lng Trm Care/   Dental",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,2), new ReportEditMask ("ZZZZZZZZ.99-")," Major Med/   Univ Life",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,3), new ReportEditMask ("ZZZZZZZZ.99-"),"Group Life",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,4), new ReportEditMask ("ZZZZZZZZ.99-"),"Ovrpay Recov",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,5), new ReportEditMask ("ZZZZZZZZ.99-"),"     NYSUT",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,6), new ReportEditMask ("ZZZZZZZZ.99-"),"Personl Ann",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,7), new ReportEditMask ("ZZZZZZZZ.99-"),"Mutual Fund",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,8), new ReportEditMask ("ZZZZZZZZ.99-"),"/         TOTAL",
                		pnd_Ded_Zeros, new ReportEditMask ("ZZZZZZZZZZZZ"));
                if (Global.isEscape()) return;
                //*  PA SELECT
                //*  DENTAL
                //*  UNIVERSAL LIFE
                //*  TOTAL
                getReports().write(8, new TabSetting(10),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,9), new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(23),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,11),  //Natural: WRITE ( 08 ) 10T #DED-AMT ( 3,9 ) ( EM = Z ( 8 ) .99- ) 23T #DED-AMT ( 3,11 ) ( EM = Z ( 8 ) .99- ) 36T #DED-AMT ( 3,10 ) ( EM = Z ( 8 ) .99- ) 116T #DED-AMT ( 3,12 ) ( EM = Z ( 9 ) .99- ) /
                    new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(36),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,10), new ReportEditMask ("ZZZZZZZZ.99-"),new 
                    TabSetting(116),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(3,12), new ReportEditMask ("ZZZZZZZZZ.99-"),NEWLINE);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            //*  TOTAL
            if (condition(pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,"*").greater(getZero())))                                                                                //Natural: IF #DED-AMT ( 4,* ) > 0
            {
                pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,12).compute(new ComputeParameters(false, pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,12)), pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4, //Natural: COMPUTE #DED-AMT ( 4,12 ) = #DED-AMT ( 4,1:11 ) + 0
                    1,":",11).add(getZero()));
                //*  JWO 2009-09
                getReports().display(4, "Pay Cde",                                                                                                                        //Natural: DISPLAY ( 04 ) 'Pay Cde' #PAYEE-CDE ( HC = L ) 'Blue Cross/  PA Select' #DED-AMT ( 4,1 ) ( EM = Z ( 8 ) .99- ) 'Lng Trm Care/   Dental' #DED-AMT ( 4,2 ) ( EM = Z ( 8 ) .99- ) ' Major Med/   Univ Life' #DED-AMT ( 4,3 ) ( EM = Z ( 8 ) .99- ) 'Group Life' #DED-AMT ( 4,4 ) ( EM = Z ( 8 ) .99- ) 'Ovrpay Recov' #DED-AMT ( 4,5 ) ( EM = Z ( 8 ) .99- ) '     NYSUT' #DED-AMT ( 4,6 ) ( EM = Z ( 8 ) .99- ) 'Personl Ann' #DED-AMT ( 4,7 ) ( EM = Z ( 8 ) .99- ) 'Mutual Fund' #DED-AMT ( 4,8 ) ( EM = Z ( 8 ) .99- ) '/         TOTAL' #DED-ZEROS ( EM = Z ( 12 ) )
                		pnd_Payee_Cde, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Blue Cross/  PA Select",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,1), new ReportEditMask ("ZZZZZZZZ.99-"),"Lng Trm Care/   Dental",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,2), new ReportEditMask ("ZZZZZZZZ.99-")," Major Med/   Univ Life",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,3), new ReportEditMask ("ZZZZZZZZ.99-"),"Group Life",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,4), new ReportEditMask ("ZZZZZZZZ.99-"),"Ovrpay Recov",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,5), new ReportEditMask ("ZZZZZZZZ.99-"),"     NYSUT",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,6), new ReportEditMask ("ZZZZZZZZ.99-"),"Personl Ann",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,7), new ReportEditMask ("ZZZZZZZZ.99-"),"Mutual Fund",
                		pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,8), new ReportEditMask ("ZZZZZZZZ.99-"),"/         TOTAL",
                		pnd_Ded_Zeros, new ReportEditMask ("ZZZZZZZZZZZZ"));
                if (Global.isEscape()) return;
                //*  PA SELECT
                //*  DENTAL
                //*  UNIVERSAL LIFE
                //*  TOTAL
                getReports().write(4, new TabSetting(10),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,9), new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(23),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,11),  //Natural: WRITE ( 04 ) 10T #DED-AMT ( 4,9 ) ( EM = Z ( 8 ) .99- ) 23T #DED-AMT ( 4,11 ) ( EM = Z ( 8 ) .99- ) 36T #DED-AMT ( 4,10 ) ( EM = Z ( 8 ) .99- ) 116T #DED-AMT ( 4,12 ) ( EM = Z ( 9 ) .99- ) /
                    new ReportEditMask ("ZZZZZZZZ.99-"),new TabSetting(36),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,10), new ReportEditMask ("ZZZZZZZZ.99-"),new 
                    TabSetting(116),pnd_Ded_Amounts_Pnd_Ded_Amt.getValue(4,12), new ReportEditMask ("ZZZZZZZZZ.99-"),NEWLINE);
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ded_Amounts_Pnd_Ded_Amt.getValue("*","*").reset();                                                                                                        //Natural: RESET #DED-AMT ( *,* )
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet357 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Canadian_Taxes() throws Exception                                                                                                                    //Natural: CANADIAN-TAXES
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------
        if (condition(ldaFcpl804.getDeduction_Rec_Cntrct_Company().equals("T")))                                                                                          //Natural: IF CNTRCT-COMPANY = 'T'
        {
            getReports().display(6, new ReportMatrixColumnUnderline("-"),"Contract#",                                                                                     //Natural: DISPLAY ( 06 ) ( UC = - ) 'Contract#' CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 'Payee/Stat' CNTRCT-PAYEE-CDE '/Annuitant Name' #PH-NAME '/Combine #' CNTRCT-CMBN-NBR 'Mode/Code' CNTRCT-MODE-CDE '/Deductions' PYMNT-DED-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) '/Soc Sec No.' ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'Pend/Code' PYMNT-SUSPEND-CDE 'Hold/Code' CNTRCT-HOLD-CDE
            		ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"Payee/Stat",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde(),"/Annuitant Name",
            		pnd_Ph_Name,"/Combine #",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),"Mode/Code",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde(),"/Deductions",
            		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"/Soc Sec No.",
            		ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"Pend/Code",
            		ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde(),"Hold/Code",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde());
            if (Global.isEscape()) return;
            pnd_Tiaa_Canada_Tax.nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                                                                                        //Natural: ADD PYMNT-DED-AMT TO #TIAA-CANADA-TAX
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().display(7, new ReportMatrixColumnUnderline("-"),"Contract#",                                                                                     //Natural: DISPLAY ( 07 ) ( UC = - ) 'Contract#' CNTRCT-PPCN-NBR ( EM = XXXXXXX-X ) 'Payee/Stat' CNTRCT-PAYEE-CDE '/Annuitant Name' #PH-NAME '/Combine #' CNTRCT-CMBN-NBR 'Mode/Code' CNTRCT-MODE-CDE '/Deductions' PYMNT-DED-AMT ( EM = ZZZ,ZZZ,ZZZ.99- ) '/Soc Sec No.' ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'Pend/Code' PYMNT-SUSPEND-CDE 'Hold/Code' CNTRCT-HOLD-CDE
            		ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"Payee/Stat",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde(),"/Annuitant Name",
            		pnd_Ph_Name,"/Combine #",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),"Mode/Code",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde(),"/Deductions",
            		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"/Soc Sec No.",
            		ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"Pend/Code",
            		ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde(),"Hold/Code",
            		ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde());
            if (Global.isEscape()) return;
            pnd_Cref_Canada_Tax.nadd(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt());                                                                                        //Natural: ADD PYMNT-DED-AMT TO #CREF-CANADA-TAX
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"***********************************************************",NEWLINE,"***********************************************************", //Natural: WRITE // '***********************************************************' / '***********************************************************' / '***'*PROGRAM '  ERROR:' *ERROR-NR 'LINE:' *ERROR-LINE / '***  LAST RECORD READ:' / '***         COMBINE #:' CNTRCT-CMBN-NBR / '***              PPCN:' CNTRCT-PPCN-NBR / '***********************************************************' / '***********************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  ERROR:",Global.getERROR_NR(),"LINE:",Global.getERROR_LINE(),NEWLINE,"***  LAST RECORD READ:",NEWLINE,"***         COMBINE #:",
            ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(),NEWLINE,"***********************************************************",
            NEWLINE,"***********************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl804_getDeduction_Rec_Pymnt_Ded_CdeIsBreak = ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().isBreak(endOfData);
        boolean ldaFcpl804_getDeduction_Rec_Pymnt_Ded_Payee_CdeIsBreak = ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde().isBreak(endOfData);
        if (condition(ldaFcpl804_getDeduction_Rec_Pymnt_Ded_CdeIsBreak || ldaFcpl804_getDeduction_Rec_Pymnt_Ded_Payee_CdeIsBreak))
        {
            deduction_Break.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO DEDUCTION-BREAK
            //*  JH 6/6/00 ??
            payee_Break.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO PAYEE-BREAK
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl804_getDeduction_Rec_Pymnt_Ded_Payee_CdeIsBreak))
        {
            payee_Break.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO PAYEE-BREAK
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
        Global.format(2, "LS=132 PS=55");
        Global.format(3, "LS=132 PS=55");
        Global.format(8, "LS=132 PS=55");
        Global.format(4, "LS=132 PS=55");
        Global.format(5, "LS=132 PS=55");
        Global.format(6, "LS=132 PS=55");
        Global.format(7, "LS=132 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"1",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(45),"ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(47),"DEDUCTIONS REPORT FOR",ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,"-",new RepeatItem(131),NEWLINE,"DEDUCTION CODE      : ",ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(), 
            new ReportEditMask ("999"),new ColumnSpacing(5),deduction_Lit,NEWLINE,"DEDUCTION PAYEE CODE: ",ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(),NEWLINE,"-",new 
            RepeatItem(131),NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"2",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(45),"ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(41),"TIAA SUMMARY DEDUCTION REPORT FOR",ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"3",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(45),"ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(41),"CREF SUMMARY DEDUCTION REPORT FOR",ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
        getReports().write(8, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"8",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(8), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(45),"ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(41),"Life SUMMARY DEDUCTION REPORT FOR",ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"4",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(45),"ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(38),"GRAND TOTAL SUMMARY DEDUCTION REPORT FOR",ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"6",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(45),"ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(38),"TIAA CONTRACTS WITH CANADIAN TAXES FOR",ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,"-",new RepeatItem(131),NEWLINE,"DEDUCTION CODE      : ",ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(), 
            new ReportEditMask ("999"),new ColumnSpacing(5),deduction_Lit,NEWLINE,"DEDUCTION PAYEE CODE: ",ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(),NEWLINE,"-",new 
            RepeatItem(131),NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"7",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(45),"ANNUITY PAYMENTS - HARVARD DEFINED BENEFITS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(38),"CREF CONTRACTS WITH CANADIAN TAXES FOR",ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,"-",new RepeatItem(131),NEWLINE,"DEDUCTION CODE      : ",ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(), 
            new ReportEditMask ("999"),new ColumnSpacing(5),deduction_Lit,NEWLINE,"DEDUCTION PAYEE CODE: ",ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(),NEWLINE,"-",new 
            RepeatItem(131),NEWLINE);

        getReports().setDisplayColumns(5, "/Combine #",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),"Contract#/Payee stat",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"Deduction/Code",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(),"Deduction/ Payee Code",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(),"Deduction/Amount",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"));
        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-"),"Ded/Cde",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde(), new ReportEditMask ("999"),"Ded/Payee",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(),"Contract#",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"Payee/Stat",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde(),"/Annuitant Name",
        		pnd_Ph_Name,"/Combine #",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),"Mode/Code",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde(),"/Deductions",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"/Soc Sec No.",
        		ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"Pend/Code",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde(),"Hold/Code",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde());
        getReports().setDisplayColumns(2, "Pay Cde",
        		pnd_Payee_Cde, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Blue Cross/  PA Select",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Lng Trm Care/   Dental",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-")," Major Med/   Univ Life",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Group Life",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Ovrpay Recov",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"     NYSUT",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Personl Ann",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Mutual Fund",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"/         TOTAL",
        		pnd_Ded_Zeros, new ReportEditMask ("ZZZZZZZZZZZZ"));
        getReports().setDisplayColumns(3, "Pay Cde",
        		pnd_Payee_Cde, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Blue Cross/  PA Select",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Lng Trm Care/   Dental",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-")," Major Med/   Univ Life",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Group Life",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Ovrpay Recov",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"     NYSUT",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Personl Ann",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Mutual Fund",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"/         TOTAL",
        		pnd_Ded_Zeros, new ReportEditMask ("ZZZZZZZZZZZZ"));
        getReports().setDisplayColumns(8, "Pay Cde",
        		pnd_Payee_Cde, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Blue Cross/  PA Select",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Lng Trm Care/   Dental",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-")," Major Med/   Univ Life",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Group Life",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Ovrpay Recov",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"     NYSUT",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Personl Ann",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Mutual Fund",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"/         TOTAL",
        		pnd_Ded_Zeros, new ReportEditMask ("ZZZZZZZZZZZZ"));
        getReports().setDisplayColumns(4, "Pay Cde",
        		pnd_Payee_Cde, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Blue Cross/  PA Select",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Lng Trm Care/   Dental",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-")," Major Med/   Univ Life",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Group Life",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Ovrpay Recov",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"     NYSUT",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Personl Ann",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"Mutual Fund",
        		pnd_Ded_Amounts_Pnd_Ded_Amt, new ReportEditMask ("ZZZZZZZZ.99-"),"/         TOTAL",
        		pnd_Ded_Zeros, new ReportEditMask ("ZZZZZZZZZZZZ"));
        getReports().setDisplayColumns(6, new ReportMatrixColumnUnderline("-"),"Contract#",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"Payee/Stat",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde(),"/Annuitant Name",
        		pnd_Ph_Name,"/Combine #",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),"Mode/Code",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde(),"/Deductions",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"/Soc Sec No.",
        		ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"Pend/Code",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde(),"Hold/Code",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde());
        getReports().setDisplayColumns(7, new ReportMatrixColumnUnderline("-"),"Contract#",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr(), new ReportEditMask ("XXXXXXX-X"),"Payee/Stat",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde(),"/Annuitant Name",
        		pnd_Ph_Name,"/Combine #",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr(),"Mode/Code",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde(),"/Deductions",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt(), new ReportEditMask ("ZZZ,ZZZ,ZZZ.99-"),"/Soc Sec No.",
        		ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"Pend/Code",
        		ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde(),"Hold/Code",
        		ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde());
    }
    private void CheckAtStartofData154() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-ROUTINE
            sub_Initialize_Routine();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
