/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:25:25 PM
**        * FROM NATURAL PROGRAM : Cpvp660
************************************************************
**        * FILE NAME            : Cpvp660.java
**        * CLASS NAME           : Cpvp660
**        * INSTANCE NAME        : Cpvp660
************************************************************
************************************************************************
* PROGRAM  : CPVP660
* SYSTEM   : CWFSCR
* TITLE    : EXTRACT FOR FEED TO OMNIPRO OF GENERIC WARRANTS PAYMENTS
* WRITTEN  : 07/01/00
* BY       : JOHN OSTEEN
* FUNCTION : READS WARRANTS READY TO BE PAID AND FEEDS THEM TO OMNIPRO
*          | AND UPDATES THE FCP-CONS-LEDGER FILE.
*          | - FOR GENERIC WARRANTS!!!
*
* 09/2013  J.OSTEEN    MODIFIED TO ADD IA ORIGIN CODE AND IA OPTION
*                      CODE TO THE LEDGER RECORD FEED TO DI-HUB
*                      TAGGED WITH JWO1
* 04/2015  F.ENDAYA    COR/NAS SUNSET. FE201504
* 05/2016  F.ENDAYA    TEXAS CS FIX.   FE201606
* 04/2017  J.OSTREA    PIN EXPANSION, REPLACE MDMA100 AND MDMN100A
*                      WITH MDMA101 AND MDMN101A FOR 12-PIN VERSION
*********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpvp660 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaEfsa1205 pdaEfsa1205;
    private LdaCpvl660 ldaCpvl660;
    private LdaEfsl3117 ldaEfsl3117;
    private PdaMdma101 pdaMdma101;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_ref;

    private DbsGroup ref_Rt_Record;
    private DbsField ref_Rt_A_I_Ind;
    private DbsField ref_Rt_Table_Id;
    private DbsField ref_Rt_Short_Key;
    private DbsField ref_Rt_Long_Key;
    private DbsField pnd_Kdo_Key;
    private DbsField pnd_I;

    private DataAccessProgramView vw_iaa;
    private DbsField iaa_Cntrct_Ppcn_Nbr;
    private DbsField iaa_Cntrct_Optn_Cde;
    private DbsField iaa_Cntrct_Orgn_Cde;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;

    private DbsGroup pnd_Texas_Chd_Output;

    private DbsGroup pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Payee_Cde;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Cde;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Amt;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Date;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Orgn_Cde;
    private DbsField pnd_Texas_Chd_Output_Pnd_Txs_Fil1;
    private DbsField pnd_Wk_Tot_Ded;
    private DbsField pls_Trace;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaEfsa1205 = new PdaEfsa1205(localVariables);
        ldaCpvl660 = new LdaCpvl660();
        registerRecord(ldaCpvl660);
        ldaEfsl3117 = new LdaEfsl3117();
        registerRecord(ldaEfsl3117);
        registerRecord(ldaEfsl3117.getVw_cwf_Efm_Wks_Tran_View());
        pdaMdma101 = new PdaMdma101(localVariables);

        // Local Variables

        vw_ref = new DataAccessProgramView(new NameInfo("vw_ref", "REF"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        ref_Rt_Record = vw_ref.getRecord().newGroupInGroup("REF_RT_RECORD", "RT-RECORD");
        ref_Rt_A_I_Ind = ref_Rt_Record.newFieldInGroup("ref_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ref_Rt_Table_Id = ref_Rt_Record.newFieldInGroup("ref_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ref_Rt_Short_Key = ref_Rt_Record.newFieldInGroup("ref_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        ref_Rt_Long_Key = ref_Rt_Record.newFieldInGroup("ref_Rt_Long_Key", "RT-LONG-KEY", FieldType.STRING, 40, RepeatingFieldStrategy.None, "RT_LONG_KEY");
        registerRecord(vw_ref);

        pnd_Kdo_Key = localVariables.newFieldInRecord("pnd_Kdo_Key", "#KDO-KEY", FieldType.STRING, 30);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);

        vw_iaa = new DataAccessProgramView(new NameInfo("vw_iaa", "IAA"), "IAA_CNTRCT", "IA_CONTRACT_PART");
        iaa_Cntrct_Ppcn_Nbr = vw_iaa.getRecord().newFieldInGroup("iaa_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        iaa_Cntrct_Optn_Cde = vw_iaa.getRecord().newFieldInGroup("iaa_Cntrct_Optn_Cde", "CNTRCT-OPTN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTN_CDE");
        iaa_Cntrct_Orgn_Cde = vw_iaa.getRecord().newFieldInGroup("iaa_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        registerRecord(vw_iaa);

        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);

        pnd_Texas_Chd_Output = localVariables.newGroupInRecord("pnd_Texas_Chd_Output", "#TEXAS-CHD-OUTPUT");

        pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name = pnd_Texas_Chd_Output.newGroupInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name", "#TXS-PH-NAME");
        pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name = pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name", 
            "#TXS-PH-LAST-NAME", FieldType.STRING, 16);
        pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name = pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name", 
            "#TXS-PH-FIRST-NAME", FieldType.STRING, 10);
        pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name = pnd_Texas_Chd_Output_Pnd_Txs_Ph_Name.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name", 
            "#TXS-PH-MIDDLE-NAME", FieldType.STRING, 12);
        pnd_Texas_Chd_Output_Pnd_Txs_Annt_Soc_Sec_Nbr = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Annt_Soc_Sec_Nbr", "#TXS-ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Unq_Id_Nbr = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Unq_Id_Nbr", "#TXS-CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Ppcn_Nbr = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Ppcn_Nbr", "#TXS-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Payee_Cde = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Payee_Cde", "#TXS-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Cde = pnd_Texas_Chd_Output.newFieldArrayInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Cde", "#TXS-PYMNT-DED-CDE", 
            FieldType.NUMERIC, 3, new DbsArrayController(1, 10));
        pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Amt = pnd_Texas_Chd_Output.newFieldArrayInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Amt", "#TXS-PYMNT-DED-AMT", 
            FieldType.NUMERIC, 9, 2, new DbsArrayController(1, 10));
        pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Date = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Date", "#TXS-PYMNT-DATE", FieldType.STRING, 
            8);
        pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr", "#TXS-CR-DR", FieldType.STRING, 
            1);
        pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Orgn_Cde = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Orgn_Cde", "#TXS-CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Texas_Chd_Output_Pnd_Txs_Fil1 = pnd_Texas_Chd_Output.newFieldInGroup("pnd_Texas_Chd_Output_Pnd_Txs_Fil1", "#TXS-FIL1", FieldType.STRING, 1);
        pnd_Wk_Tot_Ded = localVariables.newFieldInRecord("pnd_Wk_Tot_Ded", "#WK-TOT-DED", FieldType.PACKED_DECIMAL, 17, 2);
        pls_Trace = WsIndependent.getInstance().newFieldInRecord("pls_Trace", "+TRACE", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_ref.reset();
        vw_iaa.reset();

        ldaCpvl660.initializeValues();
        ldaEfsl3117.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cpvp660() throws Exception
    {
        super("Cpvp660");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ------------------------------------------------------------------
        //*  FE201504                                                                                                                                                     //Natural: FORMAT LS = 132 PS = 60
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        //* *MOVE TRUE TO +TRACE
        vw_ref.startDatabaseRead                                                                                                                                          //Natural: READ REF BY RT-SUPER1 STARTING FROM 'ACGWBR'
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", "ACGWBR", WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        READ01:
        while (condition(vw_ref.readNextRow("READ01")))
        {
            if (condition(ref_Rt_Table_Id.notEquals("CGWBR") || ref_Rt_A_I_Ind.notEquals("A")))                                                                           //Natural: IF REF.RT-TABLE-ID NE 'CGWBR' OR RT-A-I-IND NE 'A'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Rqst_Log_Dte_Tme().setValue(ref_Rt_Short_Key);                                                                               //Natural: ASSIGN #RQST-LOG-DTE-TME := RT-SHORT-KEY
            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Msg_Entry_Dte_Tme().setValue(ref_Rt_Long_Key);                                                                               //Natural: ASSIGN #MSG-ENTRY-DTE-TME := RT-LONG-KEY
            DbsUtil.callnat(Efsn1205.class , getCurrentProcessState(), pdaEfsa1205.getPnd_Appc_Parm().getValue("*"));                                                     //Natural: CALLNAT 'EFSN1205' #APPC-PARM ( * )
            if (condition(Global.isEscape())) return;
            getWorkFiles().write(1, false, pdaEfsa1205.getPnd_Appc_Parm_Pnd_Pdqa652());                                                                                   //Natural: WRITE WORK FILE 1 #PDQA652
            if (condition(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_Lfslash_N().equals("Y")))                                                                              //Natural: IF #PAYMENT-L/N = 'Y'
            {
                //* *  ESCAPE TOP
                //* *END-IF
                if (condition(getReports().getAstLinesLeft(0).less(2)))                                                                                                   //Natural: NEWPAGE IF LESS THAN 2 LINES LEFT
                {
                    getReports().newPage(0);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(0, "KDO-MIT-WORK-RQST",pdaEfsa1205.getPnd_Appc_Parm_Pnd_Rqst_Log_Dte_Tme(),NEWLINE,"DOC-ENTRY-DTE-TME",pdaEfsa1205.getPnd_Appc_Parm_Pnd_Msg_Entry_Dte_Tme(), //Natural: WRITE 'KDO-MIT-WORK-RQST' #RQST-LOG-DTE-TME / 'DOC-ENTRY-DTE-TME' #MSG-ENTRY-DTE-TME //
                    NEWLINE,NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaCpvl660.getGw_Ledger_Pnd_Post_Date().setValueEdited(Global.getDATX(),new ReportEditMask("YYMM"));                                                      //Natural: MOVE EDITED *DATX ( EM = YYMM ) TO #POST-DATE
                ldaCpvl660.getGw_Ledger_Pnd_Journal_Date().setValueEdited(Global.getDATX(),new ReportEditMask("MMDDYY"));                                                 //Natural: MOVE EDITED *DATX ( EM = MMDDYY ) TO #JOURNAL-DATE
                if (condition(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Residence().notEquals(" ")))                                                                            //Natural: IF #PH-RESIDENCE NE ' '
                {
                    ldaCpvl660.getGw_Ledger_Pnd_Residency().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Residence());                                                    //Natural: MOVE #PH-RESIDENCE TO #RESIDENCY
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaCpvl660.getGw_Ledger_Pnd_Residency().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Residency_Ss());                                                    //Natural: MOVE #RESIDENCY-SS TO #RESIDENCY
                }                                                                                                                                                         //Natural: END-IF
                //*  JWO1 09/2013
                vw_iaa.startDatabaseFind                                                                                                                                  //Natural: FIND IAA WITH CNTRCT-PPCN-NBR = #TIAA-CONT
                (
                "FIND01",
                new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tiaa_Cont(), WcType.WITH) }
                );
                FIND01:
                while (condition(vw_iaa.readNextRow("FIND01", true)))
                {
                    vw_iaa.setIfNotFoundControlFlag(false);
                    //*  JWO1 09/2013
                    if (condition(vw_iaa.getAstCOUNTER().equals(0)))                                                                                                      //Natural: IF NO RECORDS FOUND
                    {
                        //*  JWO1 09/2013
                        //*  JWO1 09/2013
                        //*  JWO1 09/2013
                        ldaCpvl660.getGw_Ledger_Pnd_Ia_Orgn_Cde().reset();                                                                                                //Natural: RESET #IA-ORGN-CDE #OPTION-CDE
                        ldaCpvl660.getGw_Ledger_Pnd_Option_Cde().reset();
                        //*  JWO1 09/2013
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        //*  JWO1 09/2013
                        //*  JWO1 09/2013
                        //*  JWO1 09/2013
                    }                                                                                                                                                     //Natural: END-NOREC
                    ldaCpvl660.getGw_Ledger_Pnd_Ia_Orgn_Cde().setValue(iaa_Cntrct_Orgn_Cde);                                                                              //Natural: ASSIGN #IA-ORGN-CDE := CNTRCT-ORGN-CDE
                    ldaCpvl660.getGw_Ledger_Pnd_Option_Cde().setValue(iaa_Cntrct_Optn_Cde);                                                                               //Natural: ASSIGN #OPTION-CDE := CNTRCT-OPTN-CDE
                    //*  JWO1 09/2013
                }                                                                                                                                                         //Natural: END-FIND
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cnt().greater(getZero())))                                                                         //Natural: IF #LEDGER1-CNT > 0
                {
                    FOR01:                                                                                                                                                //Natural: FOR #I 1 TO #LEDGER1-CNT
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cnt())); pnd_I.nadd(1))
                    {
                        ldaCpvl660.getGw_Ledger_Pnd_Company().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Company().getValue(pnd_I));                               //Natural: MOVE #LEDGER1-COMPANY ( #I ) TO #COMPANY
                        ldaCpvl660.getGw_Ledger_Pnd_Amount().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Amount().getValue(pnd_I));                                 //Natural: MOVE #LEDGER1-AMOUNT ( #I ) TO #AMOUNT
                        ldaCpvl660.getGw_Ledger_Pnd_Dccr_Ind().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Ind().getValue(pnd_I));                                  //Natural: MOVE #LEDGER1-IND ( #I ) TO #DCCR-IND
                        ldaCpvl660.getGw_Ledger_Pnd_Gl_Company().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Company().getValue(pnd_I));                            //Natural: MOVE #LEDGER1-COMPANY ( #I ) TO #GL-COMPANY
                        ldaCpvl660.getGw_Ledger_Pnd_Gl_Account().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Acct().getValue(pnd_I));                               //Natural: MOVE #LEDGER1-ACCT ( #I ) TO #GL-ACCOUNT
                        ldaCpvl660.getGw_Ledger_Pnd_Eff_Yr().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Eff_Date().getValue(pnd_I).getSubstring(3,                 //Natural: MOVE SUBSTRING ( #LEDGER1-EFF-DATE ( #I ) ,3,2 ) TO #EFF-YR
                            2));
                        ldaCpvl660.getGw_Ledger_Pnd_Eff_Mm().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Eff_Date().getValue(pnd_I).getSubstring(5,                 //Natural: MOVE SUBSTRING ( #LEDGER1-EFF-DATE ( #I ) ,5,2 ) TO #EFF-MM
                            2));
                        ldaCpvl660.getGw_Ledger_Pnd_Eff_Dd().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Eff_Date().getValue(pnd_I).getSubstring(7,                 //Natural: MOVE SUBSTRING ( #LEDGER1-EFF-DATE ( #I ) ,7,2 ) TO #EFF-DD
                            2));
                        ldaCpvl660.getGw_Ledger_Pnd_Policy_Number().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cont().getValue(pnd_I));                            //Natural: MOVE #LEDGER1-CONT ( #I ) TO #POLICY-NUMBER
                        getReports().display(0, "Contract",                                                                                                               //Natural: DISPLAY 'Contract' #LEDGER1-CONT ( #I ) 'GL/Company' #LEDGER1-COMPANY ( #I ) 'GL/Account' #LEDGER1-ACCT ( #I ) 'GL/Amount' #LEDGER1-AMOUNT ( #I ) 'DR/CR' #LEDGER1-IND ( #I ) 'GL/Date' #LEDGER1-EFF-DATE ( #I )
                        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cont().getValue(pnd_I),"GL/Company",
                        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Company().getValue(pnd_I),"GL/Account",
                        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Acct().getValue(pnd_I),"GL/Amount",
                        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Amount().getValue(pnd_I),"DR/CR",
                        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Ind().getValue(pnd_I),"GL/Date",
                        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Eff_Date().getValue(pnd_I));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getWorkFiles().write(2, false, ldaCpvl660.getGw_Ledger());                                                                                        //Natural: WRITE WORK FILE 2 GW-LEDGER
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Cnt().greater(getZero())))                                                                         //Natural: IF #LEDGER2-CNT > 0
                {
                    FOR02:                                                                                                                                                //Natural: FOR #I 1 TO #LEDGER2-CNT
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Cnt())); pnd_I.nadd(1))
                    {
                        ldaCpvl660.getGw_Ledger_Pnd_Company().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Company().getValue(pnd_I));                               //Natural: MOVE #LEDGER2-COMPANY ( #I ) TO #COMPANY
                        ldaCpvl660.getGw_Ledger_Pnd_Amount().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Amount().getValue(pnd_I));                                 //Natural: MOVE #LEDGER2-AMOUNT ( #I ) TO #AMOUNT
                        ldaCpvl660.getGw_Ledger_Pnd_Dccr_Ind().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Ind().getValue(pnd_I));                                  //Natural: MOVE #LEDGER2-IND ( #I ) TO #DCCR-IND
                        ldaCpvl660.getGw_Ledger_Pnd_Gl_Company().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Company().getValue(pnd_I));                            //Natural: MOVE #LEDGER2-COMPANY ( #I ) TO #GL-COMPANY
                        ldaCpvl660.getGw_Ledger_Pnd_Gl_Account().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Acct().getValue(pnd_I));                               //Natural: MOVE #LEDGER2-ACCT ( #I ) TO #GL-ACCOUNT
                        ldaCpvl660.getGw_Ledger_Pnd_Eff_Yr().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Eff_Date().getValue(pnd_I).getSubstring(3,                 //Natural: MOVE SUBSTRING ( #LEDGER2-EFF-DATE ( #I ) ,3,2 ) TO #EFF-YR
                            2));
                        ldaCpvl660.getGw_Ledger_Pnd_Eff_Mm().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Eff_Date().getValue(pnd_I).getSubstring(5,                 //Natural: MOVE SUBSTRING ( #LEDGER2-EFF-DATE ( #I ) ,5,2 ) TO #EFF-MM
                            2));
                        ldaCpvl660.getGw_Ledger_Pnd_Eff_Dd().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Eff_Date().getValue(pnd_I).getSubstring(7,                 //Natural: MOVE SUBSTRING ( #LEDGER2-EFF-DATE ( #I ) ,7,2 ) TO #EFF-DD
                            2));
                        ldaCpvl660.getGw_Ledger_Pnd_Policy_Number().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Cont().getValue(pnd_I));                            //Natural: MOVE #LEDGER2-CONT ( #I ) TO #POLICY-NUMBER
                        getReports().display(0, pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Cont().getValue(pnd_I),pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Company().getValue(pnd_I), //Natural: DISPLAY #LEDGER2-CONT ( #I ) #LEDGER2-COMPANY ( #I ) #LEDGER2-ACCT ( #I ) #LEDGER2-AMOUNT ( #I ) #LEDGER2-IND ( #I ) #LEDGER2-EFF-DATE ( #I )
                            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Acct().getValue(pnd_I),pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Amount().getValue(pnd_I),
                            pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Ind().getValue(pnd_I),pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger2_Eff_Date().getValue(pnd_I));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getWorkFiles().write(2, false, ldaCpvl660.getGw_Ledger());                                                                                        //Natural: WRITE WORK FILE 2 GW-LEDGER
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Payment_Pfslash_N().equals("P") && pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr().getValue("*").greaterOrEqual("900")  //Natural: IF #PAYMENT-P/N = 'P' AND #LEDGER1-COST-CTR ( * ) = '900' THRU '999'
                && pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr().getValue("*").lessOrEqual("999")))
            {
                DbsUtil.examine(new ExamineSource(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr().getValue("*")), new ExamineSearch("948"), new ExamineGivingIndex(pnd_I)); //Natural: EXAMINE #LEDGER1-COST-CTR ( * ) FOR '948' GIVING INDEX #I
                //*  FE201606 START
                pdaMdma101.getPnd_Mdma101().reset();                                                                                                                      //Natural: RESET #MDMA101
                pdaMdma101.getPnd_Mdma101_Pnd_I_Pin_N12().setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Pin());                                                               //Natural: ASSIGN #MDMA101.#I-PIN-N12 := #PIN
                DbsUtil.callnat(Mdmn101a.class , getCurrentProcessState(), pdaMdma101.getPnd_Mdma101());                                                                  //Natural: CALLNAT 'MDMN101A' #MDMA101
                if (condition(Global.isEscape())) return;
                if (condition(pdaMdma101.getPnd_Mdma101_Pnd_O_Return_Code().equals("0000")))                                                                              //Natural: IF #MDMA101.#O-RETURN-CODE = '0000'
                {
                    pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Last_Name());                                                      //Natural: ASSIGN #TXS-PH-LAST-NAME := #MDMA101.#O-LAST-NAME
                    pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_First_Name());                                                    //Natural: ASSIGN #TXS-PH-FIRST-NAME := #MDMA101.#O-FIRST-NAME
                    pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name.setValue(pdaMdma101.getPnd_Mdma101_Pnd_O_Middle_Name());                                                  //Natural: ASSIGN #TXS-PH-MIDDLE-NAME := #MDMA101.#O-MIDDLE-NAME
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Texas_Chd_Output_Pnd_Txs_Ph_Last_Name.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Last_Name());                                                  //Natural: ASSIGN #TXS-PH-LAST-NAME := #PH-LAST-NAME
                    pnd_Texas_Chd_Output_Pnd_Txs_Ph_First_Name.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_First_Name());                                                //Natural: ASSIGN #TXS-PH-FIRST-NAME := #PH-FIRST-NAME
                    pnd_Texas_Chd_Output_Pnd_Txs_Ph_Middle_Name.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Middle_Name());                                              //Natural: ASSIGN #TXS-PH-MIDDLE-NAME := #PH-MIDDLE-NAME
                    //*  FE201606 END
                }                                                                                                                                                         //Natural: END-IF
                pnd_Texas_Chd_Output_Pnd_Txs_Annt_Soc_Sec_Nbr.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ph_Ssn());                                                        //Natural: ASSIGN #TXS-ANNT-SOC-SEC-NBR := #PH-SSN
                pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Unq_Id_Nbr.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Pin());                                                          //Natural: ASSIGN #TXS-CNTRCT-UNQ-ID-NBR := #PIN
                pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Ppcn_Nbr.setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Tiaa_Cont());                                                      //Natural: ASSIGN #TXS-CNTRCT-PPCN-NBR := #TIAA-CONT
                pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Payee_Cde.setValue("01");                                                                                             //Natural: ASSIGN #TXS-CNTRCT-PAYEE-CDE := '01'
                pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Cde.getValue(1).setValueEdited(new ReportEditMask("999"),pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cost_Ctr().getValue(pnd_I)); //Natural: MOVE EDITED #LEDGER1-COST-CTR ( #I ) TO #TXS-PYMNT-DED-CDE ( 1 ) ( EM = 999 )
                //*    #TXS-PYMNT-DED-CDE(1)      := #LEDGER1-COST-CTR (#I)
                pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Ded_Amt.getValue(1).setValue(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Amount().getValue(pnd_I));                       //Natural: ASSIGN #TXS-PYMNT-DED-AMT ( 1 ) := #LEDGER1-AMOUNT ( #I )
                pnd_Texas_Chd_Output_Pnd_Txs_Pymnt_Date.setValueEdited(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Effective_Date(),new ReportEditMask("99999999"));                 //Natural: MOVE EDITED #EFFECTIVE-DATE ( EM = 99999999 ) TO #TXS-PYMNT-DATE
                pnd_Wk_Tot_Ded.nadd(pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Amount().getValue("*"));                                                                     //Natural: ADD #LEDGER1-AMOUNT ( * ) TO #WK-TOT-DED
                if (condition(pnd_Wk_Tot_Ded.greater(getZero())))                                                                                                         //Natural: IF #WK-TOT-DED GT 0
                {
                    pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr.setValue("C");                                                                                                     //Natural: ASSIGN #TXS-CR-DR := 'C'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Texas_Chd_Output_Pnd_Txs_Cr_Dr.setValue("D");                                                                                                     //Natural: ASSIGN #TXS-CR-DR := 'D'
                    //*  FE201606
                }                                                                                                                                                         //Natural: END-IF
                pnd_Texas_Chd_Output_Pnd_Txs_Cntrct_Orgn_Cde.setValue("VT");                                                                                              //Natural: ASSIGN #TXS-CNTRCT-ORGN-CDE := 'VT'
                pnd_Texas_Chd_Output_Pnd_Txs_Fil1.setValue("X");                                                                                                          //Natural: ASSIGN #TXS-FIL1 := 'X'
                getWorkFiles().write(3, false, pnd_Texas_Chd_Output);                                                                                                     //Natural: WRITE WORK FILE 3 #TEXAS-CHD-OUTPUT
            }                                                                                                                                                             //Natural: END-IF
            vw_ref.deleteDBRow("READ01");                                                                                                                                 //Natural: DELETE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  ************************************************
        //*  CLOSE MQ FE201504 END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
    }
    //*  FE201504 START
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=60");

        getReports().setDisplayColumns(0, "Contract",
        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Cont(),"GL/Company",
        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Company(),"GL/Account",
        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Acct(),"GL/Amount",
        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Amount(),"DR/CR",
        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Ind(),"GL/Date",
        		pdaEfsa1205.getPnd_Appc_Parm_Pnd_Ledger1_Eff_Date());
    }
}
