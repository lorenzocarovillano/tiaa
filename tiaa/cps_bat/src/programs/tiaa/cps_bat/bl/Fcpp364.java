/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:51 PM
**        * FROM NATURAL PROGRAM : Fcpp364
************************************************************
**        * FILE NAME            : Fcpp364.java
**        * CLASS NAME           : Fcpp364
**        * INSTANCE NAME        : Fcpp364
************************************************************
************************************************************************
* PROGRAM  : FCPP364
* SYSTEM   : CPS
* TITLE    : ELECTRONIC WARRANTS STATEMENT
* GENERATED: JAN 1, 2000  AT 10:20 AM
* FUNCTION : THIS PROGRAM WILL BUILD ELECTRONIC WARRANTS STATEMENT.
*
* HISTORY  : 01/01/2000   LEON GURTOVNIK
*            CREATE  THIS PROGRAM FROM FCPP380 (SS) TO PROCESS (EW)
*              VB RECORD AS I/O
*
*          : 05/03/1994   RITA SALGADO
*          : ADD SUSPENSE NOOTIFICATION FOR SUSPENSE REPURCHASE
*
*          : 05/26/98     R. CARREON
*          : ADD PROCESSING OF IRA
*
*          : 03/12/99     R. CARREON
*              RESTOW - CHANGE IN FCPL378
*          : 03/22/00     R. CARREON
*              INCLUDE PROCESSING FOR PAYEE-CDE 01-99
*
*          : 03/18/02     T. MCGEE
*              REPAIR THREE PAGE XEROX COMMAND "DUP"
*          :    11/02     R. CARREON  STOW - EGTRRA CHANGES IN LDA
*          : 04/17/03     R. CARREON  STOW - TPA EGTRRA
*          :                          FCPG364 WAS EXPANDED
*          : 01/05/2006   R. LANDRUM
*              POS-PAY, PAYEE MATCH. ADD STOP POINT FOR CHECK NUMBER ON
*              STATEMENT BODY & CHECK FACE, 10 DIGIT MICR LINE.
*
*************************** NOTE !!! **********************************
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR THE PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTETERNALLY.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp364 extends BLNatBase
{
    // Data Areas
    private GdaFcpg364 gdaFcpg364;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpanzcn pdaFcpanzcn;
    private PdaFcpassc1 pdaFcpassc1;
    private PdaFcpassc2 pdaFcpassc2;
    private LdaFcplssc1 ldaFcplssc1;
    private LdaFcpl378 ldaFcpl378;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;
    private LdaFcpl876b ldaFcpl876b;
    private LdaFcplbar1 ldaFcplbar1;
    private PdaFcpa199a pdaFcpa199a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Plan_S;
    private DbsField pnd_Plan_S_Cntrct_Rcrd_Typ;
    private DbsField pnd_Plan_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Plan_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Plan_S_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Plan_S_Cntl_Check_Dte;

    private DbsGroup pnd_Plan_S__R_Field_1;
    private DbsField pnd_Plan_S_Pnd_Plan_Superde;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Next_Check_Nbr;
    private DbsField pnd_Ws_Next_Check_Prefix;
    private DbsField pnd_Ws_Next_Eft_Nbr;
    private DbsField pnd_Ws_Next_Global_Nbr;
    private DbsField pnd_Ws_Next_Int_Rollover_Nbr;
    private DbsField pnd_Ws_Current_Pymnt_Id;
    private DbsField pnd_Ws_Current_Pymnt_Seq_Nbr;
    private DbsField pnd_Abend_Cde;
    private DbsField pnd_Ws_First_Xerox;

    private DbsGroup pnd_Input_Parm;
    private DbsField pnd_Input_Parm_Pnd_Parm_Check_Nbr;
    private DbsField pnd_Input_Parm_Pnd_Ws_Filler;
    private DbsField pnd_Input_Parm_Pnd_Parm_Check_Seq_Nbr;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Save_S_D_M_Plex;
    private DbsField pnd_Ws_Format_Notification_Letter;
    private DbsField pnd_Ws_Printed_Institution_Amt;
    private DbsField pnd_Ws_Bottom_Part;
    private DbsField pnd_Ws_Jde;
    private DbsField pnd_Ws_Format;
    private DbsField pnd_Ws_Forms;
    private DbsField pnd_Ws_Feed;
    private DbsField pnd_Ws_Max;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ws_Rec_1;
    private DbsField pnd_Ws_Pnd_Run_Type;
    private DbsField pnd_Ws_Pnd_Bar_Name;
    private DbsField pnd_Ws_Pnd_Check_Ind;
    private DbsField pnd_Ws_Pnd_Statement_Ind;
    private DbsField pnd_Ws_Pnd_Break_If_Acfs;
    private DbsField pnd_Ws_Pnd_Ws_Acfs;
    private DbsField pnd_Ws_Pnd_Bar_Sub;
    private DbsField pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Work_Rec;

    private DbsGroup pnd_Ws_Work_Rec__R_Field_3;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Key;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2;
    private DbsField pnd_Ws_Header;
    private DbsField pnd_Ws_Occurs;
    private DbsField pnd_Ws_Name_N_Address;

    private DbsGroup pnd_Ws_Name_N_Address__R_Field_4;

    private DbsGroup pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2;
    private DbsField pnd_Ws_Name_N_Address__Filler1;

    private DbsGroup pnd_Ws_Name_N_Address_Nme_N_Addr;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Nme;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Ws_Name_N_Address__Filler2;
    private DbsField pnd_Ws_Name_N_Address__Filler3;
    private DbsField pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr;
    private DbsField pnd_Ws_Name_N_Address__Filler4;
    private DbsField pnd_First_Page;
    private DbsField pnd_Last_Page;
    private DbsField pnd_Ws_Ira_Int;
    private DbsField pnd_A;
    private DbsField pnd_Z;
    private DbsField pnd_Y;
    private DbsField pnd_Tbl_Side;
    private DbsField pnd_Form;
    private DbsField pnd_Stmnt_Lines;
    private DbsField pnd_Ws_Company;
    private DbsField pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaFcpg364 = GdaFcpg364.getInstance(getCallnatLevel());
        registerRecord(gdaFcpg364);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpanzcn = new PdaFcpanzcn(localVariables);
        pdaFcpassc1 = new PdaFcpassc1(localVariables);
        pdaFcpassc2 = new PdaFcpassc2(localVariables);
        ldaFcplssc1 = new LdaFcplssc1();
        registerRecord(ldaFcplssc1);
        ldaFcpl378 = new LdaFcpl378();
        registerRecord(ldaFcpl378);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);
        ldaFcpl876b = new LdaFcpl876b();
        registerRecord(ldaFcpl876b);
        ldaFcplbar1 = new LdaFcplbar1();
        registerRecord(ldaFcplbar1);
        pdaFcpa199a = new PdaFcpa199a(localVariables);

        // Local Variables

        pnd_Plan_S = localVariables.newGroupInRecord("pnd_Plan_S", "#PLAN-S");
        pnd_Plan_S_Cntrct_Rcrd_Typ = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        pnd_Plan_S_Cntrct_Orgn_Cde = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Plan_S_Cntrct_Ppcn_Nbr = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Plan_S_Pymnt_Prcss_Seq_Nbr = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pnd_Plan_S_Cntl_Check_Dte = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE);

        pnd_Plan_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Plan_S__R_Field_1", "REDEFINE", pnd_Plan_S);
        pnd_Plan_S_Pnd_Plan_Superde = pnd_Plan_S__R_Field_1.newFieldInGroup("pnd_Plan_S_Pnd_Plan_Superde", "#PLAN-SUPERDE", FieldType.STRING, 26);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Next_Check_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Nbr", "#WS-NEXT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Check_Prefix = localVariables.newFieldInRecord("pnd_Ws_Next_Check_Prefix", "#WS-NEXT-CHECK-PREFIX", FieldType.NUMERIC, 3);
        pnd_Ws_Next_Eft_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Eft_Nbr", "#WS-NEXT-EFT-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Global_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Global_Nbr", "#WS-NEXT-GLOBAL-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Int_Rollover_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Int_Rollover_Nbr", "#WS-NEXT-INT-ROLLOVER-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Current_Pymnt_Id = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Id", "#WS-CURRENT-PYMNT-ID", FieldType.NUMERIC, 7);
        pnd_Ws_Current_Pymnt_Seq_Nbr = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Seq_Nbr", "#WS-CURRENT-PYMNT-SEQ-NBR", FieldType.NUMERIC, 
            7);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);
        pnd_Ws_First_Xerox = localVariables.newFieldInRecord("pnd_Ws_First_Xerox", "#WS-FIRST-XEROX", FieldType.STRING, 1);

        pnd_Input_Parm = localVariables.newGroupInRecord("pnd_Input_Parm", "#INPUT-PARM");
        pnd_Input_Parm_Pnd_Parm_Check_Nbr = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Parm_Check_Nbr", "#PARM-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Input_Parm_Pnd_Ws_Filler = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Ws_Filler", "#WS-FILLER", FieldType.STRING, 1);
        pnd_Input_Parm_Pnd_Parm_Check_Seq_Nbr = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Parm_Check_Seq_Nbr", "#PARM-CHECK-SEQ-NBR", FieldType.NUMERIC, 
            7);
        pnd_Ws_Side_Printed = localVariables.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Cntr_Inv_Acct = localVariables.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Sim_Dup_Multiplex_Written = localVariables.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Save_S_D_M_Plex = localVariables.newFieldInRecord("pnd_Ws_Save_S_D_M_Plex", "#WS-SAVE-S-D-M-PLEX", FieldType.STRING, 1);
        pnd_Ws_Format_Notification_Letter = localVariables.newFieldInRecord("pnd_Ws_Format_Notification_Letter", "#WS-FORMAT-NOTIFICATION-LETTER", FieldType.STRING, 
            1);
        pnd_Ws_Printed_Institution_Amt = localVariables.newFieldInRecord("pnd_Ws_Printed_Institution_Amt", "#WS-PRINTED-INSTITUTION-AMT", FieldType.STRING, 
            1);
        pnd_Ws_Bottom_Part = localVariables.newFieldInRecord("pnd_Ws_Bottom_Part", "#WS-BOTTOM-PART", FieldType.STRING, 1);
        pnd_Ws_Jde = localVariables.newFieldInRecord("pnd_Ws_Jde", "#WS-JDE", FieldType.STRING, 3);
        pnd_Ws_Format = localVariables.newFieldInRecord("pnd_Ws_Format", "#WS-FORMAT", FieldType.STRING, 6);
        pnd_Ws_Forms = localVariables.newFieldInRecord("pnd_Ws_Forms", "#WS-FORMS", FieldType.STRING, 6);
        pnd_Ws_Feed = localVariables.newFieldInRecord("pnd_Ws_Feed", "#WS-FEED", FieldType.STRING, 6);
        pnd_Ws_Max = localVariables.newFieldInRecord("pnd_Ws_Max", "#WS-MAX", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ws_Rec_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_Ws_Pnd_Run_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 8);
        pnd_Ws_Pnd_Bar_Name = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Bar_Name", "#BAR-NAME", FieldType.STRING, 38);
        pnd_Ws_Pnd_Check_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Check_Ind", "#CHECK-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Statement_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Statement_Ind", "#STATEMENT-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Break_If_Acfs = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Break_If_Acfs", "#BREAK-IF-ACFS", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ws_Acfs = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Acfs", "#WS-ACFS", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Bar_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Bar_Sub", "#BAR-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt", "#WS-INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Ws_Work_Rec = localVariables.newFieldArrayInRecord("pnd_Ws_Work_Rec", "#WS-WORK-REC", FieldType.STRING, 250, new DbsArrayController(1, 2));

        pnd_Ws_Work_Rec__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Work_Rec__R_Field_3", "REDEFINE", pnd_Ws_Work_Rec);
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Key = pnd_Ws_Work_Rec__R_Field_3.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Key", "#WS-WORK-REC-KEY", FieldType.STRING, 
            41);
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1 = pnd_Ws_Work_Rec__R_Field_3.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1", "#WS-WORK-REC-DET-1", 
            FieldType.STRING, 250);
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2 = pnd_Ws_Work_Rec__R_Field_3.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2", "#WS-WORK-REC-DET-2", 
            FieldType.STRING, 209);
        pnd_Ws_Header = localVariables.newFieldArrayInRecord("pnd_Ws_Header", "#WS-HEADER", FieldType.STRING, 250, new DbsArrayController(1, 2));
        pnd_Ws_Occurs = localVariables.newFieldArrayInRecord("pnd_Ws_Occurs", "#WS-OCCURS", FieldType.STRING, 250, new DbsArrayController(1, 80));
        pnd_Ws_Name_N_Address = localVariables.newFieldArrayInRecord("pnd_Ws_Name_N_Address", "#WS-NAME-N-ADDRESS", FieldType.STRING, 250, new DbsArrayController(1, 
            4));

        pnd_Ws_Name_N_Address__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Name_N_Address__R_Field_4", "REDEFINE", pnd_Ws_Name_N_Address);

        pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2 = pnd_Ws_Name_N_Address__R_Field_4.newGroupArrayInGroup("pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2", 
            "#WS-NAME-N-ADDR-LEVEL2", new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address__Filler1 = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address__Filler1", "_FILLER1", 
            FieldType.STRING, 111);

        pnd_Ws_Name_N_Address_Nme_N_Addr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newGroupInGroup("pnd_Ws_Name_N_Address_Nme_N_Addr", "NME-N-ADDR");
        pnd_Ws_Name_N_Address_Pymnt_Nme = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", 
            FieldType.STRING, 35);
        pnd_Ws_Name_N_Address__Filler2 = pnd_Ws_Name_N_Address_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address__Filler2", "_FILLER2", FieldType.STRING, 
            57);
        pnd_Ws_Name_N_Address__Filler3 = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address__Filler3", "_FILLER3", 
            FieldType.STRING, 9);
        pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr", 
            "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21);
        pnd_Ws_Name_N_Address__Filler4 = pnd_Ws_Name_N_Address_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address__Filler4", "_FILLER4", 
            FieldType.STRING, 54);
        pnd_First_Page = localVariables.newFieldInRecord("pnd_First_Page", "#FIRST-PAGE", FieldType.BOOLEAN, 1);
        pnd_Last_Page = localVariables.newFieldInRecord("pnd_Last_Page", "#LAST-PAGE", FieldType.BOOLEAN, 1);
        pnd_Ws_Ira_Int = localVariables.newFieldInRecord("pnd_Ws_Ira_Int", "#WS-IRA-INT", FieldType.STRING, 1);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_Z = localVariables.newFieldInRecord("pnd_Z", "#Z", FieldType.PACKED_DECIMAL, 7);
        pnd_Y = localVariables.newFieldInRecord("pnd_Y", "#Y", FieldType.INTEGER, 2);
        pnd_Tbl_Side = localVariables.newFieldArrayInRecord("pnd_Tbl_Side", "#TBL-SIDE", FieldType.STRING, 1, new DbsArrayController(1, 40));
        pnd_Form = localVariables.newFieldInRecord("pnd_Form", "#FORM", FieldType.STRING, 1);
        pnd_Stmnt_Lines = localVariables.newFieldInRecord("pnd_Stmnt_Lines", "#STMNT-LINES", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Company = localVariables.newFieldInRecord("pnd_Ws_Company", "#WS-COMPANY", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.INTEGER, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplssc1.initializeValues();
        ldaFcpl378.initializeValues();
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();
        ldaFcpl876b.initializeValues();
        ldaFcplbar1.initializeValues();

        localVariables.reset();
        pnd_Plan_S_Cntrct_Rcrd_Typ.setInitialValue("6");
        pnd_Ws_First_Xerox.setInitialValue("Y");
        pnd_Ws_Cntr_Inv_Acct.setInitialValue(0);
        pnd_Ws_Format_Notification_Letter.setInitialValue("N");
        pnd_Ws_Printed_Institution_Amt.setInitialValue("N");
        pnd_Ws_Bottom_Part.setInitialValue(" ");
        pnd_Ws_Jde.setInitialValue(" ");
        pnd_Ws_Format.setInitialValue(" ");
        pnd_Ws_Forms.setInitialValue(" ");
        pnd_Ws_Feed.setInitialValue(" ");
        pnd_Ws_Max.setInitialValue(0);
        pnd_I.setInitialValue(0);
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
        pnd_Ws_Pnd_Ws_Rec_1.setInitialValue(" ");
        pnd_Ws_Pnd_Break_If_Acfs.setInitialValue(true);
        pnd_A.setInitialValue(0);
        pnd_Z.setInitialValue(0);
        pnd_Y.setInitialValue(0);
        pnd_Form.setInitialValue(" ");
        pnd_Stmnt_Lines.setInitialValue(0);
        pnd_Ws_Company.setInitialValue(" ");
        pnd_X.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp364() throws Exception
    {
        super("Fcpp364");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 58 LS = 132 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        getWorkFiles().read(2, pnd_Ws_Pnd_Run_Type);                                                                                                                      //Natural: READ WORK FILE 2 ONCE #RUN-TYPE
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'RUN TYPE' PARAMETER FILE",new TabSetting(77),"***");                                                 //Natural: WRITE '***' 25T 'MISSING "RUN TYPE" PARAMETER FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(pnd_Ws_Pnd_Run_Type.equals("HELD") || pnd_Ws_Pnd_Run_Type.equals("HOLD")))                                                                          //Natural: IF #RUN-TYPE = 'HELD' OR = 'HOLD'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().read(4, pdaFcpassc2.getPnd_Fcpassc2());                                                                                                        //Natural: READ WORK FILE 4 ONCE #FCPASSC2
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"MISSING 'HOLD CHECK' CONTROL FILE",new TabSetting(77),"***");                                             //Natural: WRITE '***' 25T 'MISSING "HOLD CHECK" CONTROL FILE' 77T '***'
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(50);  if (true) return;                                                                                                                 //Natural: TERMINATE 50
            }                                                                                                                                                             //Natural: END-ENDFILE
        }                                                                                                                                                                 //Natural: END-IF
        getWorkFiles().read(6, ldaFcpl876.getPnd_Fcpl876());                                                                                                              //Natural: READ WORK FILE 6 ONCE #FCPL876
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'NEXT CHECK' FILE",new TabSetting(77),"***");                                                         //Natural: WRITE '***' 25T 'MISSING "NEXT CHECK" FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-ENDFILE
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue("*").setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Barcode().getValue("*"));                               //Natural: ASSIGN #BARCODE-LDA.#BAR-BARCODE ( * ) := #FCPL876.#BAR-BARCODE ( * )
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes().setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes());                                                       //Natural: ASSIGN #BARCODE-LDA.#BAR-ENVELOPES := #FCPL876.#BAR-ENVELOPES
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().getBoolean());                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := #FCPL876.#BAR-NEW-RUN
        pnd_Ws_Current_Pymnt_Seq_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                                           //Natural: ASSIGN #WS-CURRENT-PYMNT-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
        pnd_Ws_Next_Check_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                                      //Natural: ASSIGN #WS-NEXT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
        //* *S-NEXT-CHECK-PREFIX         := #FCPL876.PYMNT-CHECK-PREFIX
        pnd_Ws_Next_Eft_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr());                                                                                  //Natural: ASSIGN #WS-NEXT-EFT-NBR := #FCPL876.PYMNT-CHECK-SCRTY-NBR
        pnd_Ws_Next_Global_Nbr.setValue(1);                                                                                                                               //Natural: ASSIGN #WS-NEXT-GLOBAL-NBR := 1
        pnd_Ws_Next_Int_Rollover_Nbr.setValue(1);                                                                                                                         //Natural: ASSIGN #WS-NEXT-INT-ROLLOVER-NBR := 1
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPASSC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 6 )
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
        //*  RL
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMAT-DATA
        sub_Get_Check_Format_Data();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"at start of program:",NEWLINE,"The program is going to use the following:",NEWLINE,                   //Natural: WRITE *PROGRAM *TIME 'at start of program:' / 'The program is going to use the following:' / 'start assigned check   number          :' #WS-NEXT-CHECK-NBR / 'start assigned EFT     number          :' #WS-NEXT-EFT-NBR / 'start assigned Global  number          :' #WS-NEXT-GLOBAL-NBR / 'start assigned Internal Rollover number:' #WS-NEXT-INT-ROLLOVER-NBR / 'start sequence number..................:' #WS-CURRENT-PYMNT-SEQ-NBR
            "start assigned check   number          :",pnd_Ws_Next_Check_Nbr,NEWLINE,"start assigned EFT     number          :",pnd_Ws_Next_Eft_Nbr,NEWLINE,
            "start assigned Global  number          :",pnd_Ws_Next_Global_Nbr,NEWLINE,"start assigned Internal Rollover number:",pnd_Ws_Next_Int_Rollover_Nbr,
            NEWLINE,"start sequence number..................:",pnd_Ws_Current_Pymnt_Seq_Nbr);
        if (Global.isEscape()) return;
        //* *--------------------------------------------------------------**
        //*  ADJUST CURRENT-PYMNT-SEQ-NBR FOR THE FIRST TIME
        pnd_Ws_Current_Pymnt_Seq_Nbr.nsubtract(1);                                                                                                                        //Natural: SUBTRACT 1 FROM #WS-CURRENT-PYMNT-SEQ-NBR
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 EXT ( * )
        while (condition(getWorkFiles().read(1, gdaFcpg364.getExt().getValue("*"))))
        {
            //*            /* MOVE 'SS' FOR NOW & MOVE 'EW' BEFORE WRITE OUT GDG FILE
            gdaFcpg364.getExt_Cntrct_Orgn_Cde().setValue("SS");                                                                                                           //Natural: MOVE 'SS' TO EXT.CNTRCT-ORGN-CDE
            pnd_Ws_Ira_Int.setValue(" ");                                                                                                                                 //Natural: ASSIGN #WS-IRA-INT := ' '
            pnd_Ws_Company.setValue(" ");                                                                                                                                 //Natural: ASSIGN #WS-COMPANY := ' '
            pnd_Stmnt_Lines.setValue(0);                                                                                                                                  //Natural: ASSIGN #STMNT-LINES := 0
                                                                                                                                                                          //Natural: PERFORM PRINT-ON-WHICH-SIDE-OF-PAPER
            sub_Print_On_Which_Side_Of_Paper();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM RESET-10-20-30-40-45
            sub_Reset_10_20_30_40_45();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
            sub_Initialize_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  10
            pnd_A.setValue(1);                                                                                                                                            //Natural: MOVE 1 TO #A
                                                                                                                                                                          //Natural: PERFORM MOVE-NEW-TO-OLD-KEY
            sub_Move_New_To_Old_Key();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MOVE-NEW-TO-OLD-REC10
            sub_Move_New_To_Old_Rec10();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
            sub_Move_Small_Rec_To_Large_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  20
            pnd_A.setValue(1);                                                                                                                                            //Natural: MOVE 1 TO #A
            REPEAT01:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_A.greater(gdaFcpg364.getExt_C_Inv_Acct()))) {break;}                                                                                    //Natural: UNTIL #A GT EXT.C-INV-ACCT
                                                                                                                                                                          //Natural: PERFORM MOVE-NEW-TO-OLD-KEY
                sub_Move_New_To_Old_Key();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM MOVE-NEW-TO-OLD-REC20
                sub_Move_New_To_Old_Rec20();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
                sub_Move_Small_Rec_To_Large_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_A.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #A
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  30  NAME AND ADDRESSS
            pnd_Z.setValue(1);                                                                                                                                            //Natural: MOVE 1 TO #Z
            REPEAT02:                                                                                                                                                     //Natural: REPEAT
            while (condition(whileTrue))
            {
                if (condition(pnd_Z.greater(gdaFcpg364.getExt_C_Pymnt_Nme_And_Addr()))) {break;}                                                                          //Natural: UNTIL #Z GT EXT.C-PYMNT-NME-AND-ADDR
                                                                                                                                                                          //Natural: PERFORM MOVE-NEW-TO-OLD-KEY
                sub_Move_New_To_Old_Key();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM MOVE-NEW-TO-OLD-REC30
                sub_Move_New_To_Old_Rec30();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
                sub_Move_Small_Rec_To_Large_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Z.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #Z
            }                                                                                                                                                             //Natural: END-REPEAT
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  40 PROCESS PLAN RECORD
            //*  ROXAN 3.22.00
            if (condition(((((gdaFcpg364.getExt_Cntrct_Lob_Cde().equals("RAD ") || gdaFcpg364.getExt_Cntrct_Lob_Cde().equals("GRAD")) || gdaFcpg364.getExt_Cntrct_Lob_Cde().equals("SRAD"))  //Natural: IF ( EXT.CNTRCT-LOB-CDE EQ 'RAD ' OR EQ 'GRAD' OR EQ 'SRAD' OR EQ 'GSRD' ) AND ( SUBSTR ( EXT.CNTRCT-PAYEE-CDE,1,3 ) = 'ALT' OR SUBSTR ( EXT.PYMNT-NME ( 1 ) ,1,3 ) = 'CR ' )
                || gdaFcpg364.getExt_Cntrct_Lob_Cde().equals("GSRD")) && (gdaFcpg364.getExt_Cntrct_Payee_Cde().getSubstring(1,3).equals("ALT") || gdaFcpg364.getExt_Pymnt_Nme().getValue(1).getSubstring(1,
                3).equals("CR ")))))
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-40-45
                sub_Process_Record_Type_40_45();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1.setValue(ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_1());                                             //Natural: MOVE #REC-TYPE-40-DET-1 TO #WS-WORK-REC-DET-1
                pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2.setValue(ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Rec_Type_40_Det_2());                                             //Natural: MOVE #REC-TYPE-40-DET-2 TO #WS-WORK-REC-DET-2
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
                sub_Move_Small_Rec_To_Large_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Break_If_Acfs.getBoolean()))                                                                                                         //Natural: IF #BREAK-IF-ACFS
            {
                //*  BYPASS INTERNAL
                if (condition(pnd_Ws_Ira_Int.equals(" ")))                                                                                                                //Natural: IF #WS-IRA-INT = ' '
                {
                                                                                                                                                                          //Natural: PERFORM FORMAT-DOCUMENT
                    sub_Format_Document();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  MOVE EW BACK
            gdaFcpg364.getExt_Cntrct_Orgn_Cde().setValue("EW");                                                                                                           //Natural: MOVE 'EW' TO EXT.CNTRCT-ORGN-CDE
            getWorkFiles().write(9, true, gdaFcpg364.getExt().getValue("*"));                                                                                             //Natural: WRITE WORK FILE 09 VARIABLE EXT ( * )
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *--------------------------------------------------------------
        pnd_Ws_Current_Pymnt_Seq_Nbr.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-CURRENT-PYMNT-SEQ-NBR
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"Counters etc, at end of program:",NEWLINE,"Next check  number...........:",pnd_Ws_Next_Check_Nbr,     //Natural: WRITE *PROGRAM *TIME 'Counters etc, at end of program:' / 'Next check  number...........:' #WS-NEXT-CHECK-NBR / 'Next EFT    number..............:' #WS-NEXT-EFT-NBR / 'Next Global number..............:' #WS-NEXT-GLOBAL-NBR / 'Next Internal Rollover number...:' #WS-NEXT-INT-ROLLOVER-NBR / 'Next payment sequence number....:' #WS-CURRENT-PYMNT-SEQ-NBR
            NEWLINE,"Next EFT    number..............:",pnd_Ws_Next_Eft_Nbr,NEWLINE,"Next Global number..............:",pnd_Ws_Next_Global_Nbr,NEWLINE,"Next Internal Rollover number...:",
            pnd_Ws_Next_Int_Rollover_Nbr,NEWLINE,"Next payment sequence number....:",pnd_Ws_Current_Pymnt_Seq_Nbr);
        if (Global.isEscape()) return;
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Current_Pymnt_Seq_Nbr);                                                                           //Natural: ASSIGN #FCPL876.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().setValue(pnd_Ws_Next_Check_Nbr);                                                                                      //Natural: ASSIGN #FCPL876.PYMNT-CHECK-NBR := #WS-NEXT-CHECK-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().setValue(pnd_Ws_Next_Eft_Nbr);                                                                                  //Natural: ASSIGN #FCPL876.PYMNT-CHECK-SCRTY-NBR := #WS-NEXT-EFT-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Last_Rollover_Nbr().setValue(pnd_Ws_Next_Int_Rollover_Nbr);                                                                       //Natural: ASSIGN #FCPL876.PYMNT-LAST-ROLLOVER-NBR := #WS-NEXT-INT-ROLLOVER-NBR
        ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Barcode().getValue("*").setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue("*"));                               //Natural: ASSIGN #FCPL876.#BAR-BARCODE ( * ) := #BARCODE-LDA.#BAR-BARCODE ( * )
        ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes());                                                       //Natural: ASSIGN #FCPL876.#BAR-ENVELOPES := #BARCODE-LDA.#BAR-ENVELOPES
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        getWorkFiles().write(7, false, ldaFcpl876.getPnd_Fcpl876());                                                                                                      //Natural: WRITE WORK FILE 07 #FCPL876
        getReports().write(0, NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION ****");                                                                                    //Natural: WRITE // '****** END OF PROGRAM EXECUTION ****'
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-SMALL-REC-TO-LARGE-REC
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PYMNT-ID
        //* *
        //*  DETERMINE THE PAYMENT IDENTIFIER OF THE CURRENT PAYMENT
        //*  WHETHER  IT IS A CHECK, AN EFT OR A GLOBAL PAY
        //* **** #FCPL876A.CNTRCT-ORGN-CDE   := #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATEMENT
        //* * COMPRESS '12' #WS-CHECK-NBR-A10 *PROGRAM ' 2525 #WS-CURRENT-PYMNT-ID'
        //*      ' $$XEROX FORMAT=DABAC,FORMS=BLANK,FEED=BOND,END;'
        //*      ' $$XEROX FORMAT=IAPERB,FORMS=IAMULT,FEED=BOND,END;'
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DOCUMENT
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-XEROX-COMMANDS
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-CHECK
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SENT-TO-FORM
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-NOTIFICATION
        //* * COMPRESS '12' #WS-CHECK-NBR-A10 *PROGRAM ' 3945 #WS-CURRENT-PYMNT-ID'
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BODY-OF-STATEMENT
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BARCODE-PROCESSING
        //* *#FCPL876B.CNTRCT-ORGN-CDE    := #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-CHECK-SEQ-NUMBERS
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* * * * * WILL USE THIS FOR REAL SS STATEMENTS IN FUTURE * * * * * *
        //* *#FCPACRPT.#TITLE          := '   ANNUITANT STATEMENT CONTROL REPORT'
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-DATA
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-DATA
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BLANK-PAGE
        //* *FOR #I 1 17
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-NEW-TO-OLD-KEY
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-NEW-TO-OLD-REC10
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-NEW-TO-OLD-REC20
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-NEW-TO-OLD-REC30
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-40-45
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PLAN-RECORD
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ON-WHICH-SIDE-OF-PAPER
        //* *---------------------------------------------------------**
        //* *---------------------------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-10-20-30-40-45
        //* *---------------------------------------------------------**
        //* *********************** RL BEGIN - PAYEE MATCH ************************
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMAT-DATA
        //* ************************** RL END-PAYEE MATCH *************************
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T 'PROCESS ELECTRONIC WARRANTS CHECKS' 68T 'REPORT: RPT0' / 36T #RUN-TYPE //
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Move_Small_Rec_To_Large_Rec() throws Exception                                                                                                       //Natural: MOVE-SMALL-REC-TO-LARGE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  HEADER REC
        short decideConditionsMet1451 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #KEY-REC-LVL-#;//Natural: VALUE 10
        if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(10))))
        {
            decideConditionsMet1451++;
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PYMNT-ID
            sub_Determine_Pymnt_Id();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-SEQ-NUMBERS
            sub_Assign_Check_Seq_Numbers();
            if (condition(Global.isEscape())) {return;}
            gdaFcpg364.getExt_Pymnt_Check_Nbr().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Nbr());                                                         //Natural: MOVE #REC-TYPE-10-DETAIL.PYMNT-CHECK-NBR TO EXT.PYMNT-CHECK-NBR
            gdaFcpg364.getExt_Pymnt_Check_Seq_Nbr().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr());                                                 //Natural: MOVE #REC-TYPE-10-DETAIL.PYMNT-CHECK-SEQ-NBR TO EXT.PYMNT-CHECK-SEQ-NBR
            gdaFcpg364.getExt_Pymnt_Check_Scrty_Nbr().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr());                                             //Natural: MOVE #REC-TYPE-10-DETAIL.PYMNT-CHECK-SCRTY-NBR TO EXT.PYMNT-CHECK-SCRTY-NBR
            pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1());                                                 //Natural: MOVE #REC-TYPE-10-DET-1 TO #WS-WORK-REC-DET-1
            pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2());                                                 //Natural: MOVE #REC-TYPE-10-DET-2 TO #WS-WORK-REC-DET-2
            pnd_Ws_Header.getValue("*").setValue(pnd_Ws_Work_Rec.getValue("*"));                                                                                          //Natural: MOVE #WS-WORK-REC ( * ) TO #WS-HEADER ( * )
            //* * SET CONTROL RECORD PYMNT INDEXES **
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
            sub_Set_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 20
        else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(20))))
        {
            decideConditionsMet1451++;
            if (condition(pnd_I.greater(getZero())))                                                                                                                      //Natural: IF #I > 0
            {
                pnd_I.compute(new ComputeParameters(false, pnd_I), pnd_J.add(1));                                                                                         //Natural: COMPUTE #I = #J + 1
                pnd_J.nadd(2);                                                                                                                                            //Natural: COMPUTE #J = #J + 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_I.setValue(1);                                                                                                                                        //Natural: ASSIGN #I := 1
                pnd_J.setValue(2);                                                                                                                                        //Natural: ASSIGN #J := 2
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-SEQ-NUMBERS
            sub_Assign_Check_Seq_Numbers();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Occurs.getValue(pnd_I).setValue(pnd_Ws_Work_Rec.getValue(1));                                                                                          //Natural: MOVE #WS-WORK-REC ( 1 ) TO #WS-OCCURS ( #I )
            pnd_Ws_Occurs.getValue(pnd_J).setValue(pnd_Ws_Work_Rec.getValue(2));                                                                                          //Natural: MOVE #WS-WORK-REC ( 2 ) TO #WS-OCCURS ( #J )
            pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt.nadd(ldaFcpl378.getPnd_Rec_Type_20_Detail_Inv_Acct_Dpi_Amt());                                                                 //Natural: ADD #REC-TYPE-20-DETAIL.INV-ACCT-DPI-AMT TO #WS-INV-ACCT-DPI-AMT
            pnd_Ws_Cntr_Inv_Acct.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WS-CNTR-INV-ACCT
            //*  NAME/ADDR REC
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
            sub_Accum_Control_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 30
        else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(30))))
        {
            decideConditionsMet1451++;
            if (condition(pnd_K.greater(getZero())))                                                                                                                      //Natural: IF #K > 0
            {
                pnd_K.compute(new ComputeParameters(false, pnd_K), pnd_L.add(1));                                                                                         //Natural: COMPUTE #K = #L + 1
                pnd_L.nadd(2);                                                                                                                                            //Natural: COMPUTE #L = #L + 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_K.setValue(1);                                                                                                                                        //Natural: ASSIGN #K = 1
                pnd_L.setValue(2);                                                                                                                                        //Natural: ASSIGN #L = 2
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-SEQ-NUMBERS
            sub_Assign_Check_Seq_Numbers();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Name_N_Address.getValue(pnd_K).setValue(pnd_Ws_Work_Rec.getValue(1));                                                                                  //Natural: MOVE #WS-WORK-REC ( 1 ) TO #WS-NAME-N-ADDRESS ( #K )
            pnd_Ws_Name_N_Address.getValue(pnd_L).setValue(pnd_Ws_Work_Rec.getValue(2));                                                                                  //Natural: MOVE #WS-WORK-REC ( 2 ) TO #WS-NAME-N-ADDRESS ( #L )
        }                                                                                                                                                                 //Natural: VALUE 40
        else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(40))))
        {
            decideConditionsMet1451++;
            if (condition(pnd_Ws_Ira_Int.equals("Y")))                                                                                                                    //Natural: IF #WS-IRA-INT = 'Y'
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Break_If_Acfs.getBoolean()))                                                                                                         //Natural: IF #BREAK-IF-ACFS
            {
                                                                                                                                                                          //Natural: PERFORM FORMAT-DOCUMENT
                sub_Format_Document();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Pnd_Break_If_Acfs.setValue(false);                                                                                                                 //Natural: MOVE FALSE TO #BREAK-IF-ACFS
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean() || (! (ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean())  //Natural: IF #GOOD-LETTER OR NOT #GOOD-LETTER AND #KEY-REC-OCCUR-# = 01
                && ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().equals(1)))))
            {
                ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp().setValuesByName(pnd_Ws_Name_N_Address_Nme_N_Addr.getValue(1));                          //Natural: MOVE BY NAME #WS-NAME-N-ADDRESS.NME-N-ADDR ( 1 ) TO #REC-TYPE-30-DETAIL.#PYMNT-NME-AND-ADDR-GRP
                ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Nme().setValue(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(2));                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( 2 ) TO #REC-TYPE-30-DETAIL.PYMNT-NME
                ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Eft_Acct_Nbr().setValue(pnd_Ws_Name_N_Address_Pymnt_Eft_Acct_Nbr.getValue(1));                                 //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-EFT-ACCT-NBR ( 1 ) TO #REC-TYPE-30-DETAIL.PYMNT-EFT-ACCT-NBR
                DbsUtil.callnat(Fcpnacfs.class , getCurrentProcessState(), ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10(),  //Natural: CALLNAT 'FCPNACFS' USING #PYMNT-EXT-KEY #RECORD-TYPE-10 #RECORD-TYPE-30 #RECORD-TYPE-40 #WS-INV-ACCT-DPI-AMT
                    ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30(), ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Record_Type_40(), pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt);
                if (condition(Global.isEscape())) return;
                pnd_Ws_Pnd_Ws_Acfs.setValue(true);                                                                                                                        //Natural: MOVE TRUE TO #WS-ACFS
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
                sub_Barcode_Processing();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Pnd_Ws_Acfs.setValue(false);                                                                                                                       //Natural: MOVE FALSE TO #WS-ACFS
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Pymnt_Id() throws Exception                                                                                                                //Natural: DETERMINE-PYMNT-ID
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Ira_Int.setValue(" ");                                                                                                                                     //Natural: ASSIGN #WS-IRA-INT := ' '
        if (condition(((! ((((ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ANNT") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT1"))  //Natural: IF NOT ( ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ANNT' OR = 'ALT1' OR = 'ALT2' ) OR SUBSTR ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE,1,2 ) = MASK ( 99 ) ) AND #REC-TYPE-10-DETAIL.CNTRCT-TYPE-CDE = 'R' AND #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND = 6
            || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT2")) || DbsUtil.maskMatches(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().getSubstring(1,2),"99"))) 
            && ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Type_Cde().equals("R")) && ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(6))))
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr().reset();                                                                                            //Natural: RESET #KEY-PYMNT-CHECK-NBR #KEY-PYMNT-CHECK-SEQ-NBR #REC-TYPE-10-DETAIL.PYMNT-CHECK-NBR #REC-TYPE-10-DETAIL.PYMNT-CHECK-SCRTY-NBR #REC-TYPE-10-DETAIL.PYMNT-CHECK-SEQ-NBR #WS-CURRENT-PYMNT-ID
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr().reset();
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Nbr().reset();
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr().reset();
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr().reset();
            pnd_Ws_Current_Pymnt_Id.reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Current_Pymnt_Seq_Nbr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-CURRENT-PYMNT-SEQ-NBR
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Current_Pymnt_Seq_Nbr);                                                            //Natural: ASSIGN #REC-TYPE-10-DETAIL.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
            //*  CHECKS
            short decideConditionsMet1535 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet1535++;
                if (condition(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code().equals("0000") || ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code().equals("    "))) //Natural: IF #KEY-CONTRACT-HOLD-CODE = '0000' OR = '    '
                {
                    ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                           //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                            //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
                    //*  BACK AS EW FOR REPORT LEON
                }                                                                                                                                                         //Natural: END-IF
                ldaFcpl876a.getPnd_Fcpl876a_Cntrct_Orgn_Cde().setValue("EW");                                                                                             //Natural: ASSIGN #FCPL876A.CNTRCT-ORGN-CDE := 'EW'
                ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Current_Pymnt_Seq_Nbr);                                                                 //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
                pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Check_Nbr);                                                                                                  //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-CHECK-NBR
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Nbr().setValue(pnd_Ws_Next_Check_Nbr);                                                                   //Natural: ASSIGN #REC-TYPE-10-DETAIL.PYMNT-CHECK-NBR := #WS-NEXT-CHECK-NBR
                ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().setValue(pnd_Ws_Next_Check_Nbr);                                                                            //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-NBR := #WS-NEXT-CHECK-NBR
                //*  RL FEB13
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Next_Check_Nbr);                                                                                 //Natural: MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
                //*  RL ?
                pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                //*  RL ?
                //*  RL ?
                pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                               //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
                pnd_Ws_Next_Check_Nbr.nadd(1);                                                                                                                            //Natural: ADD 1 TO #WS-NEXT-CHECK-NBR
                //*  PRETEND IS SS LEON
                //*  EFT
                getWorkFiles().write(8, false, ldaFcpl876a.getPnd_Fcpl876a());                                                                                            //Natural: WRITE WORK FILE 8 #FCPL876A
                ldaFcpl876a.getPnd_Fcpl876a_Cntrct_Orgn_Cde().setValue("SS");                                                                                             //Natural: ASSIGN #FCPL876A.CNTRCT-ORGN-CDE := 'SS'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet1535++;
                pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Eft_Nbr);                                                                                                    //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-EFT-NBR
                //*  GLOBAL PAY
                pnd_Ws_Next_Eft_Nbr.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-NEXT-EFT-NBR
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr().setValue(pnd_Ws_Current_Pymnt_Id);                                                           //Natural: ASSIGN #REC-TYPE-10-DETAIL.PYMNT-CHECK-SCRTY-NBR := #WS-CURRENT-PYMNT-ID
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(3))))
            {
                decideConditionsMet1535++;
                pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Global_Nbr);                                                                                                 //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-GLOBAL-NBR
                //*  INTERNAL ROLLOVER
                pnd_Ws_Next_Global_Nbr.nadd(1);                                                                                                                           //Natural: ADD 1 TO #WS-NEXT-GLOBAL-NBR
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(8))))
            {
                decideConditionsMet1535++;
                pnd_Ws_Ira_Int.setValue("Y");                                                                                                                             //Natural: ASSIGN #WS-IRA-INT := 'Y'
                pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Int_Rollover_Nbr);                                                                                           //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-INT-ROLLOVER-NBR
                pnd_Ws_Next_Int_Rollover_Nbr.nadd(1);                                                                                                                     //Natural: ADD 1 TO #WS-NEXT-INT-ROLLOVER-NBR
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Check_Scrty_Nbr().setValue(pnd_Ws_Current_Pymnt_Id);                                                           //Natural: ASSIGN #REC-TYPE-10-DETAIL.PYMNT-CHECK-SCRTY-NBR := #WS-CURRENT-PYMNT-ID
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                getReports().write(0, "***",new TabSetting(25),"- INVALID PAYMENT METHOD:",ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind(),new              //Natural: WRITE '***' 25T '- INVALID PAYMENT METHOD:' #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND 77T '***'
                    TabSetting(77),"***");
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(53);  if (true) return;                                                                                                                 //Natural: TERMINATE 53
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Statement() throws Exception                                                                                                                  //Natural: FORMAT-STATEMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  C=CHECK
        if (condition(pnd_Ws_Bottom_Part.equals("C")))                                                                                                                    //Natural: IF #WS-BOTTOM-PART = 'C'
        {
            //* *ASSIGN #WS-FORMAT = 'SSDA1'
            //*  RL PAYEE MATCH FORMAT
            pnd_Ws_Format.setValue("SSDA1X");                                                                                                                             //Natural: ASSIGN #WS-FORMAT = 'SSDA1X'
            //* *ASSIGN #WS-FORMS  = 'SSWC'
            //*  RL PAYEE MATCH FORMS
            pnd_Ws_Forms.setValue("MCMWIC");                                                                                                                              //Natural: ASSIGN #WS-FORMS = 'MCMWIC'
            pnd_Ws_Feed.setValue("CHECKP");                                                                                                                               //Natural: ASSIGN #WS-FEED = 'CHECKP'
            pnd_Ws_Max.setValue(13);                                                                                                                                      //Natural: ASSIGN #WS-MAX = 13
                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
            sub_Format_Xerox_Commands();
            if (condition(Global.isEscape())) {return;}
            //* ******************* RL BEGIN POS-PAY/PAYEE MATCH **********************
            //* *MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
            //*  RL FEB 16 THURS
            pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Current_Pymnt_Id);                                                                                   //Natural: MOVE #WS-CURRENT-PYMNT-ID TO #WS-CHECK-NBR-N7
            pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                             //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
            pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                                   //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
            pnd_Ws_Pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "12", pnd_Ws_Check_Nbr_A10));                                                    //Natural: COMPRESS '12' #WS-CHECK-NBR-A10 INTO #WS-REC-1 LEAVING NO SPACE
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            //* ***************** RL END POS-PAY/PAYEE MATCH *********************
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  S=SENT TO FORM
            if (condition(pnd_Ws_Bottom_Part.equals("S")))                                                                                                                //Natural: IF #WS-BOTTOM-PART = 'S'
            {
                pnd_Ws_Format.setValue("SSDA2");                                                                                                                          //Natural: ASSIGN #WS-FORMAT = 'SSDA2'
                pnd_Ws_Forms.setValue("SSWOC");                                                                                                                           //Natural: ASSIGN #WS-FORMS = 'SSWOC'
                pnd_Ws_Feed.setValue("BOND");                                                                                                                             //Natural: ASSIGN #WS-FEED = 'BOND'
                pnd_Ws_Max.setValue(10);                                                                                                                                  //Natural: ASSIGN #WS-MAX = 10
                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
                sub_Format_Xerox_Commands();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*   BACK AS EW
        setValueToSubstring("EW",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'EW' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
        //*  FORMAT HEADER
        DbsUtil.callnat(Fcpn236.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN236' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        //*  PRETEND IS SS
        setValueToSubstring("SS",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'SS' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
        pnd_Ws_Side_Printed.setValue("1");                                                                                                                                //Natural: ASSIGN #WS-SIDE-PRINTED = '1'
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
        sub_Body_Of_Statement();
        if (condition(Global.isEscape())) {return;}
        //*  TRAILER
        DbsUtil.callnat(Fcpn280.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN280' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT
            pnd_Ws_Cntr_Inv_Acct);
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Ws_Bottom_Part.equals("C")))                                                                                                                    //Natural: IF #WS-BOTTOM-PART = 'C'
        {
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
            sub_Format_Check();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ws_Bottom_Part.equals("S")))                                                                                                                //Natural: IF #WS-BOTTOM-PART = 'S'
            {
                                                                                                                                                                          //Natural: PERFORM SENT-TO-FORM
                sub_Sent_To_Form();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("2") || pnd_Ws_Save_S_D_M_Plex.equals("3")))                                                                          //Natural: IF #WS-SAVE-S-D-M-PLEX = '2' OR = '3'
        {
            pnd_Ws_Side_Printed.setValue("2");                                                                                                                            //Natural: ASSIGN #WS-SIDE-PRINTED = '2'
            //*  FORMAT BODY
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
            sub_Body_Of_Statement();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* * MULTI-PLEX NEED TO BE TESTED
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("3")))                                                                                                                //Natural: IF #WS-SAVE-S-D-M-PLEX = '3'
        {
            pnd_Ws_Side_Printed.setValue("3");                                                                                                                            //Natural: ASSIGN #WS-SIDE-PRINTED = '3'
            if (condition(pnd_Ws_Side_Printed.equals("3")))                                                                                                               //Natural: IF #WS-SIDE-PRINTED = '3'
            {
                pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                        //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                      //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=IAMONB,FORMS=IAMULT,FEED=BOND,END;");                                                                       //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=IAMONB,FORMS=IAMULT,FEED=BOND,END;'
                getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                      //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                        //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                      //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Last_Page.setValue(true);                                                                                                                                 //Natural: ASSIGN #LAST-PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
            sub_Body_Of_Statement();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
            sub_Barcode_Processing();
            if (condition(Global.isEscape())) {return;}
            pnd_Last_Page.setValue(false);                                                                                                                                //Natural: ASSIGN #LAST-PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Document() throws Exception                                                                                                                   //Natural: FORMAT-DOCUMENT
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(2)))                                                                           //Natural: IF #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND = 2
        {
            //*  S=SENT TO FORM
            pnd_Ws_Bottom_Part.setValue("S");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
            sub_Format_Statement();
            if (condition(Global.isEscape())) {return;}
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ROXAN
        if (condition((ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ANNT") || (DbsUtil.maskMatches(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().getSubstring(1,2),"99")  //Natural: IF #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ANNT' OR ( SUBSTR ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE,1,2 ) = MASK ( 99 ) AND SUBSTR ( #WS-NAME-N-ADDRESS.PYMNT-NME ( 1 ) ,1,3 ) NE 'CR ' )
            && !pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(1).getSubstring(1,3).equals("CR ")))))
        {
            //* * AND SUBSTR(#REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE,3,1) NE 'P')     /*
            if (condition(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(2).equals(" ") && pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt.getValue(2).equals(" ")                   //Natural: IF #WS-NAME-N-ADDRESS.PYMNT-NME ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE2-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE3-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE4-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE5-TXT ( 2 ) = ' ' AND #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE6-TXT ( 2 ) = ' '
                && pnd_Ws_Name_N_Address_Pymnt_Addr_Line2_Txt.getValue(2).equals(" ") && pnd_Ws_Name_N_Address_Pymnt_Addr_Line3_Txt.getValue(2).equals(" ") 
                && pnd_Ws_Name_N_Address_Pymnt_Addr_Line4_Txt.getValue(2).equals(" ") && pnd_Ws_Name_N_Address_Pymnt_Addr_Line5_Txt.getValue(2).equals(" ") 
                && pnd_Ws_Name_N_Address_Pymnt_Addr_Line6_Txt.getValue(2).equals(" ")))
            {
                //*  C=CHECK
                pnd_Ws_Bottom_Part.setValue("C");                                                                                                                         //Natural: ASSIGN #WS-BOTTOM-PART = 'C'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
                sub_Format_Statement();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  S=SENT TO FORM
                pnd_Ws_Bottom_Part.setValue("S");                                                                                                                         //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
                sub_Format_Statement();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-NOTIFICATION
                sub_Format_Notification();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
                sub_Format_Check();
                if (condition(Global.isEscape())) {return;}
                pnd_Ws_Format_Notification_Letter.setValue("N");                                                                                                          //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ROXAN 3.22.00
        if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT1") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT2")    //Natural: IF ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ALT1' OR = 'ALT2' ) OR SUBSTR ( EXT.PYMNT-NME ( 1 ) ,1,3 ) = 'CR '
            || gdaFcpg364.getExt_Pymnt_Nme().getValue(1).getSubstring(1,3).equals("CR ")))
        {
            //*  S=SENT TO FORM
            pnd_Ws_Bottom_Part.setValue("S");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
            sub_Format_Statement();
            if (condition(Global.isEscape())) {return;}
            //*  C=CHECK
            pnd_Ws_Bottom_Part.setValue("C");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'C'
                                                                                                                                                                          //Natural: PERFORM FORMAT-NOTIFICATION
            sub_Format_Notification();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
            sub_Format_Check();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Format_Notification_Letter.setValue("N");                                                                                                              //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'
        }                                                                                                                                                                 //Natural: END-IF
        //*   ROUTINE TO PRINT SUSPENSE NOTIFICATION                  /* RCS
        //*   (ADDRESS MOVED TO ALIGN WITH WINDOW ENVELOPE            /* RCS
        //*  RCS
        //*  RCS
        if (condition(! (ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ANNT") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT1") //Natural: IF NOT ( ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ANNT' OR = 'ALT1' OR = 'ALT2' ) OR SUBSTR ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE,1,2 ) = MASK ( 99 ) )
            || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT2") || DbsUtil.maskMatches(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().getSubstring(1,
            2),"99"))))
        {
            //*  RCS
            if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Type_Cde().equals("R")))                                                                            //Natural: IF #REC-TYPE-10-DETAIL.CNTRCT-TYPE-CDE = 'R'
            {
                //*  RCS
                if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(6)))                                                                   //Natural: IF #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND = 6
                {
                    //*   LEON
                    if (true) return;                                                                                                                                     //Natural: ESCAPE ROUTINE
                    //* ******  IN THIS CASE DO NOT PRINT STATEMENT BUT WRITE OUT TO GDG
                    //* ******  FILE SO RECORD WILL BE PROCCESED FOR REGISTERS  /*LEON
                    //* ******  CODE IS LEFT HERE FOR FUTURE USE                /*LEON
                    //* *(   ASSIGN #WS-BOTTOM-PART = 'S'                         /* RCS
                    //* *(   MOVE  #WS-NAME-N-ADDRESS(1) TO #WS-NAME-N-ADDRESS(3) /* RCS
                    //* *(   MOVE  #WS-NAME-N-ADDRESS(2) TO #WS-NAME-N-ADDRESS(4) /* RCS
                    //* *(   RESET NME-N-ADDR(1)                                  /* RCS
                    //* ***  PERFORM FORMAT-STATEMENT                             /* RCS
                    //* *(   PERFORM FORMAT-NOTIFICATION                          /* RCS
                    //* *(   ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'          /* RCS
                    //* *(   ASSIGN #I = 2
                    //* *(   CALLNAT 'FCPN253'                                    /* RCS
                    //* *(     #WS-HEADER(*)                                      /* RCS
                    //* *(     #WS-OCCURS(*)                                      /* RCS
                    //* *(     #WS-NAME-N-ADDRESS(*)                              /* RCS
                    //* *(     #I                                                 /* RCS
                    //* **** PERFORM SENT-TO-FORM                                 /* RCS
                    //*  RCS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM FORMAT-NOTIFICATION
                    sub_Format_Notification();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
                    sub_Format_Check();
                    if (condition(Global.isEscape())) {return;}
                    pnd_Ws_Format_Notification_Letter.setValue("N");                                                                                                      //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "ERROR: ","=",ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde()," HAS ","=",ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind()); //Natural: WRITE 'ERROR: ' '=' #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE ' HAS ' '=' #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND
                if (Global.isEscape()) return;
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Xerox_Commands() throws Exception                                                                                                             //Natural: FORMAT-XEROX-COMMANDS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Ws_Sim_Dup_Multiplex_Written.notEquals(pnd_Ws_Save_S_D_M_Plex)))                                                                                //Natural: IF #WS-SIM-DUP-MULTIPLEX-WRITTEN NE #WS-SAVE-S-D-M-PLEX
        {
            pnd_Ws_Sim_Dup_Multiplex_Written.setValue(pnd_Ws_Save_S_D_M_Plex);                                                                                            //Natural: ASSIGN #WS-SIM-DUP-MULTIPLEX-WRITTEN := #WS-SAVE-S-D-M-PLEX
            short decideConditionsMet1748 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #WS-SAVE-S-D-M-PLEX;//Natural: VALUE '1'
            if (condition((pnd_Ws_Save_S_D_M_Plex.equals("1"))))
            {
                decideConditionsMet1748++;
                pnd_Ws_Jde.setValue("SIM");                                                                                                                               //Natural: ASSIGN #WS-JDE = 'SIM'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Ws_Save_S_D_M_Plex.equals("2"))))
            {
                decideConditionsMet1748++;
                pnd_Ws_Jde.setValue("DUP");                                                                                                                               //Natural: ASSIGN #WS-JDE = 'DUP'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                //*  MCGEE 03/18/02
                pnd_Ws_Jde.setValue("DUP");                                                                                                                               //Natural: ASSIGN #WS-JDE = 'DUP'
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " $$XEROX JDE=CHK", pnd_Ws_Jde, ",JDL=CHECK,FORMAT=", pnd_Ws_Format,             //Natural: COMPRESS ' $$XEROX JDE=CHK' #WS-JDE ',JDL=CHECK,FORMAT=' #WS-FORMAT ',FORMS=' #WS-FORMS ',FEED=' #WS-FEED ',END;' INTO #WS-REC-1 LEAVING NO SPACE
                ",FORMS=", pnd_Ws_Forms, ",FEED=", pnd_Ws_Feed, ",END;"));
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Check() throws Exception                                                                                                                      //Natural: FORMAT-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Check_Ind.setValue(true);                                                                                                                              //Natural: MOVE TRUE TO #CHECK-IND #FIRST-PAGE
        pnd_First_Page.setValue(true);
        pnd_Ws_Pnd_Statement_Ind.setValue(false);                                                                                                                         //Natural: MOVE FALSE TO #STATEMENT-IND
        //*   BACK AS EW
        setValueToSubstring("EW",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'EW' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
        //*  RL CHECK&SEQ NBR & OTHER BANK SPECIFIC DATA FOR CHECK PRINT
        DbsUtil.callnat(Fcpn380.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN380' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) FCPA110 #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pdaFcpa110.getFcpa110(), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        //*  PRETEND IS SS
        setValueToSubstring("SS",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'SS' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
        pnd_Ws_Pnd_Ws_Rec_1.setValue("+3");                                                                                                                               //Natural: MOVE '+3' TO #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        //*  JFT - TO DETERMINE IF LAST PAGE - CONSISTENT WITH FCPP378 WHERE
        //*        PAGE OR SIDE IS CALCULATED
        if (condition(((pnd_Ws_Cntr_Inv_Acct.multiply(5)).add(2)).lessOrEqual(51)))                                                                                       //Natural: IF ( ( #WS-CNTR-INV-ACCT * 5 ) + 2 ) LE 51
        {
            pnd_Last_Page.setValue(true);                                                                                                                                 //Natural: ASSIGN #LAST-PAGE := TRUE
            //* *ELSE
            //* *  #LAST-PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Save_S_D_M_Plex.notEquals("1") && pnd_Ws_Format_Notification_Letter.notEquals("Y")))                                                         //Natural: IF #WS-SAVE-S-D-M-PLEX NE '1' AND #WS-FORMAT-NOTIFICATION-LETTER NE 'Y'
        {
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=DABAC,FORMS=BLANK,END;");                                                                                       //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=DABAC,FORMS=BLANK,END;'
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  RL FCPN260 STOP ADDR '17' & A '+7'
    private void sub_Sent_To_Form() throws Exception                                                                                                                      //Natural: SENT-TO-FORM
    {
        if (BLNatReinput.isReinput()) return;

        //*  /* RL #WS-BOTTOM-PART = 'S'
        pnd_Ws_Pnd_Check_Ind.setValue(false);                                                                                                                             //Natural: MOVE FALSE TO #CHECK-IND
        pnd_Ws_Pnd_Statement_Ind.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #STATEMENT-IND #FIRST-PAGE
        pnd_First_Page.setValue(true);
        DbsUtil.callnat(Fcpn260a.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"));        //Natural: CALLNAT 'FCPN260A' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * )
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Ws_Rec_1.setValue("+3");                                                                                                                               //Natural: MOVE '+3' TO #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        //*  JFT - TO DETERMINE IF LAST PAGE - CONSISTENT WITH FCPP378 WHERE
        //*        PAGE OR SIDE IS CALCULATED
        if (condition(((pnd_Ws_Cntr_Inv_Acct.multiply(5)).add(2)).lessOrEqual(83)))                                                                                       //Natural: IF ( ( #WS-CNTR-INV-ACCT * 5 ) + 2 ) LE 83
        {
            pnd_Last_Page.setValue(true);                                                                                                                                 //Natural: ASSIGN #LAST-PAGE := TRUE
            //* *ELSE
            //* ** #LAST-PAGE := FALSE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BARCODE-PROCESSING
        sub_Barcode_Processing();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Save_S_D_M_Plex.notEquals("1")))                                                                                                             //Natural: IF #WS-SAVE-S-D-M-PLEX NE '1'
        {
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=DABAC,FORMS=BLANK,END;");                                                                                       //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=DABAC,FORMS=BLANK,END;'
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Notification() throws Exception                                                                                                               //Natural: FORMAT-NOTIFICATION
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Format_Notification_Letter.setValue("Y");                                                                                                                  //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'Y'
        if (condition(pnd_Ws_First_Xerox.equals("Y")))                                                                                                                    //Natural: IF #WS-FIRST-XEROX = 'Y'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                            //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
        //* *ASSIGN #WS-REC-1 = ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSLET,' - RL
        //* *  'FORMS=SSLET,FEED=CHECKP,END;'                                  /*RL
        //* RL
        //* RL
        pnd_Ws_Pnd_Ws_Rec_1.setValue(" $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSLETX,FORMS=MCMLET,FEED=CHECKP,END;");                                                        //Natural: ASSIGN #WS-REC-1 = ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSLETX,FORMS=MCMLET,FEED=CHECKP,END;'
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Pnd_Ws_Rec_1.reset();                                                                                                                                      //Natural: RESET #WS-REC-1
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        //* ***************** RL BEGIN POS-PAY/PAYEE MATCH *********************
        //* *MOVE #WS-NEXT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //*  RL FEB 16 THURS
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Current_Pymnt_Id);                                                                                       //Natural: MOVE #WS-CURRENT-PYMNT-ID TO #WS-CHECK-NBR-N7
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        pnd_Ws_Check_Nbr_A10.setValueEdited(pnd_Ws_Check_Nbr_N10,new ReportEditMask("9999999999"));                                                                       //Natural: MOVE EDITED #WS-CHECK-NBR-N10 ( EM = 9999999999 ) TO #WS-CHECK-NBR-A10
        pnd_Ws_Pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "12", pnd_Ws_Check_Nbr_A10));                                                        //Natural: COMPRESS '12' #WS-CHECK-NBR-A10 INTO #WS-REC-1 LEAVING NO SPACE
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        //* ***************** RL END POS-PAY/PAYEE MATCH *********************
        if (condition(pnd_Ws_First_Xerox.equals("Y")))                                                                                                                    //Natural: IF #WS-FIRST-XEROX = 'Y'
        {
            pnd_Ws_First_Xerox.setValue("N");                                                                                                                             //Natural: ASSIGN #WS-FIRST-XEROX = 'N'
            //*  IGNORE
            //* *ELSE                           /* RL ?
            //* *  ASSIGN #WS-REC-1 = ' '       /* RL ?
            //* *  WRITE WORK FILE 8 #WS-REC-1  /* RL ?
        }                                                                                                                                                                 //Natural: END-IF
        //*   BACK AS EW
        setValueToSubstring("EW",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'EW' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
        //*  FORMAT HEADER
        DbsUtil.callnat(Fcpn236.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN236' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        DbsUtil.callnat(Fcpn230.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN230' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        //*  PRETEND IS SS
        setValueToSubstring("SS",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'SS' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
    }
    private void sub_Body_Of_Statement() throws Exception                                                                                                                 //Natural: BODY-OF-STATEMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  BACK AS EW
        setValueToSubstring("EW",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'EW' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
        //*  FORMAT BODY
        DbsUtil.callnat(Fcpn246.class , getCurrentProcessState(), pnd_Ws_Header.getValue("*"), pnd_Ws_Occurs.getValue("*"), pnd_Ws_Name_N_Address.getValue("*"),          //Natural: CALLNAT 'FCPN246' #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-SIDE-PRINTED #WS-PRINTED-INSTITUTION-AMT
            pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written, pnd_Ws_Side_Printed, pnd_Ws_Printed_Institution_Amt);
        if (condition(Global.isEscape())) return;
        //* *#WS-CHECK-NBR-A10 /* RL
        //*  PRETEND IS SS
        setValueToSubstring("SS",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'SS' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
    }
    private void sub_Barcode_Processing() throws Exception                                                                                                                //Natural: BARCODE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        short decideConditionsMet1889 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #STATEMENT-IND
        if (condition(pnd_Ws_Pnd_Statement_Ind.getBoolean()))
        {
            decideConditionsMet1889++;
            pnd_Ws_Pnd_Bar_Sub.setValue(2);                                                                                                                               //Natural: MOVE 2 TO #BAR-SUB
        }                                                                                                                                                                 //Natural: WHEN #CHECK-IND
        else if (condition(pnd_Ws_Pnd_Check_Ind.getBoolean()))
        {
            decideConditionsMet1889++;
            pnd_Ws_Pnd_Bar_Sub.setValue(1);                                                                                                                               //Natural: MOVE 1 TO #BAR-SUB
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Ws_Acfs.getBoolean()))                                                                                                                   //Natural: IF #WS-ACFS
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_First_Page.getBoolean()))                                                                                                                   //Natural: IF #FIRST-PAGE
            {
                pnd_First_Page.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-PAGE := FALSE
                //*  FIRST PAGE OF ENVELOPE
                ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Demand_Feed().setValue(true);                                                                                      //Natural: MOVE TRUE TO #BAR-DEMAND-FEED
                ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().nadd(1);                                                                                              //Natural: ADD 1 TO #BAR-ENV-ID-NUM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  NOT ACFS
        //*  STATEMENT
        //*  CHECK
        //*  PRINTING ACFS
        //*  BAD ACFS AND FIRST ACFS
        //*  LAST ACFS
        if (condition((((! (ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().getBoolean()) || pnd_Ws_Pnd_Statement_Ind.getBoolean()) && pnd_Last_Page.getBoolean())        //Natural: IF ( ( NOT #ACFS OR #STATEMENT-IND ) AND #LAST-PAGE ) OR #CHECK-IND AND #WS-ACFS AND ( NOT #GOOD-LETTER OR #KEY-REC-OCCUR-# = #PLAN-DATA-TOP )
            || ((pnd_Ws_Pnd_Check_Ind.getBoolean() && pnd_Ws_Pnd_Ws_Acfs.getBoolean()) && (! (ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean()) 
            || ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().equals(ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top()))))))
        {
            //*  LAST  PAGE OF ENVELOPE
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Set_Last_Page().setValue(true);                                                                                        //Natural: MOVE TRUE TO #BAR-SET-LAST-PAGE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(pnd_Ws_Pnd_Bar_Sub).getSubstring(1,3).equals("CR ")))                                                      //Natural: IF SUBSTR ( #WS-NAME-N-ADDRESS.PYMNT-NME ( #BAR-SUB ) ,1,3 ) = 'CR '
        {
            pnd_Ws_Pnd_Bar_Name.setValue(pnd_Ws_Name_N_Address_Pymnt_Addr_Line1_Txt.getValue(pnd_Ws_Pnd_Bar_Sub));                                                        //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-ADDR-LINE1-TXT ( #BAR-SUB ) TO #BAR-NAME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Bar_Name.setValue(pnd_Ws_Name_N_Address_Pymnt_Nme.getValue(pnd_Ws_Pnd_Bar_Sub));                                                                   //Natural: MOVE #WS-NAME-N-ADDRESS.PYMNT-NME ( #BAR-SUB ) TO #BAR-NAME
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpnbar.class , getCurrentProcessState(), ldaFcplbar1.getPnd_Barcode_Lda());                                                                      //Natural: CALLNAT 'FCPNBAR' #BARCODE-LDA
        if (condition(Global.isEscape())) return;
        FOR01:                                                                                                                                                            //Natural: FOR #BAR-SUB = 1 TO 17
        for (pnd_Ws_Pnd_Bar_Sub.setValue(1); condition(pnd_Ws_Pnd_Bar_Sub.lessOrEqual(17)); pnd_Ws_Pnd_Bar_Sub.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Bar_Sub.equals(1)))                                                                                                                  //Natural: IF #BAR-SUB = 1
            {
                pnd_Ws_Pnd_Ws_Rec_1.setValue("1");                                                                                                                        //Natural: MOVE '1' TO #WS-REC-1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Ws_Rec_1.setValue(" ");                                                                                                                        //Natural: MOVE ' ' TO #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Ws_Acfs.getBoolean()))                                                                                                               //Natural: IF #WS-ACFS
            {
                setValueToSubstring("2",pnd_Ws_Pnd_Ws_Rec_1,2,1);                                                                                                         //Natural: MOVE '2' TO SUBSTR ( #WS-REC-1,2,1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                setValueToSubstring("<",pnd_Ws_Pnd_Ws_Rec_1,2,1);                                                                                                         //Natural: MOVE '<' TO SUBSTR ( #WS-REC-1,2,1 )
            }                                                                                                                                                             //Natural: END-IF
            setValueToSubstring(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue(pnd_Ws_Pnd_Bar_Sub),pnd_Ws_Pnd_Ws_Rec_1,3,1);                                   //Natural: MOVE #BARCODE-LDA.#BAR-BARCODE ( #BAR-SUB ) TO SUBSTR ( #WS-REC-1,3,1 )
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  FORCE EW BACK FOR REPORT LEON
        if (condition(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Set_Last_Page().getBoolean()))                                                                               //Natural: IF #BAR-SET-LAST-PAGE
        {
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee().setValue(pnd_Ws_Pnd_Bar_Name);                                                                                    //Natural: ASSIGN #FCPL876B.#ADDRESSEE := #BAR-NAME
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id());                                                       //Natural: ASSIGN #FCPL876B.#BAR-ENV-ID := #BARCODE-LDA.#BAR-ENV-ID
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde().setValue("EW");                                                                                                 //Natural: ASSIGN #FCPL876B.CNTRCT-ORGN-CDE := 'EW'
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Ppcn_Nbr());                                               //Natural: ASSIGN #FCPL876B.CNTRCT-PPCN-NBR := #REC-TYPE-10-DETAIL.CNTRCT-PPCN-NBR
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde());                                             //Natural: ASSIGN #FCPL876B.CNTRCT-PAYEE-CDE := #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde().setValue(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code());                                         //Natural: ASSIGN #FCPL876B.CNTRCT-HOLD-CDE := #KEY-CONTRACT-HOLD-CODE
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Integrity_Counter());                         //Natural: ASSIGN #FCPL876B.#BAR-ENV-INTEGRITY-COUNTER := #BARCODE-LDA.#BAR-ENV-INTEGRITY-COUNTER
            if (condition(pnd_Ws_Pnd_Check_Ind.getBoolean()))                                                                                                             //Natural: IF #CHECK-IND
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr().setValue(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr());                                                    //Natural: ASSIGN #FCPL876B.PYMNT-CHECK-NBR := #FCPL876A.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876b.getPnd_Fcpl876b_Pnd_Pymnt_Check_Nbr().reset();                                                                                                //Natural: RESET #FCPL876B.#PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes());                                                 //Natural: ASSIGN #FCPL876B.#BAR-ENVELOPES := #BARCODE-LDA.#BAR-ENVELOPES
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Pages_In_Env());                                           //Natural: ASSIGN #FCPL876B.#BAR-PAGES-IN-ENV := #BARCODE-LDA.#BAR-PAGES-IN-ENV
            ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().setValue(pnd_Ws_Pnd_Check_Ind.getBoolean());                                                                          //Natural: ASSIGN #FCPL876B.#CHECK := #CHECK-IND
            //*  PRETEND IS SS LEON
            getWorkFiles().write(8, false, ldaFcpl876b.getPnd_Fcpl876b());                                                                                                //Natural: WRITE WORK FILE 8 #FCPL876B
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde().setValue("SS");                                                                                                 //Natural: ASSIGN #FCPL876B.CNTRCT-ORGN-CDE := 'SS'
        }                                                                                                                                                                 //Natural: END-IF
        //*  CODE BELLOW IS A TEMP CODE
        //*  DISPLAY
        //*  'L/VL'       #KEY-REC-LVL-#
        //*  'O/CC'       #KEY-REC-OCCUR-#
        //*  '/HOLD'      #KEY-CONTRACT-HOLD-CODE
        //*  'S/D'        #KEY-SIMPLEX-DUPLEX-MULTIPLEX
        //*  '/PPCN'      #REC-TYPE-10-DETAIL.CNTRCT-PPCN-NBR(AL=8)
        //*  '/CREF'      #REC-TYPE-10-DETAIL.CNTRCT-CREF-NBR(AL=8)
        //*  '/PAYEE'     #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE
        //*  'N/R'        #BARCODE-LDA.#BAR-NEW-RUN  (EM=F/T)
        //*  'C/H'        #CHECK-IND                 (EM=F/T)
        //*  'S/T'        #STATEMENT-IND             (EM=F/T)
        //*  'E/I'        #BARCODE-LDA.#BAR-BARCODE(1)
        //*  'B/1'        #BARCODE-LDA.#BAR-BARCODE(2)
        //*  'E/S'        #BARCODE-LDA.#BAR-BARCODE(3)
        //*  'B/2'        #BARCODE-LDA.#BAR-BARCODE(4)
        //*  'B/3'        #BARCODE-LDA.#BAR-BARCODE(5)
        //*  'B/4'        #BARCODE-LDA.#BAR-BARCODE(6)
        //*  'B/5'        #BARCODE-LDA.#BAR-BARCODE(7)
        //*  'P/C'        #BARCODE-LDA.#BAR-BARCODE(8)
        //*  'E/C'        #BARCODE-LDA.#BAR-BARCODE(9)
        //*  '/PIN'       #BARCODE-LDA.#BAR-ENV-ID
        //*  'S/I'        #BARCODE-LDA.#BAR-BARCODE(17)
        //*  '/ENV'       #BARCODE-LDA.#BAR-ENVELOPES
        //*  'P/E'        #BARCODE-LDA.#BAR-PAGES-IN-ENV
        //*  '/NAME'      #BAR-NAME
        //*  CODE ABOVE IS A TEMP CODE
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Barcode_Input().resetInitial();                                                                                                //Natural: RESET INITIAL #BARCODE-INPUT
    }
    private void sub_Assign_Check_Seq_Numbers() throws Exception                                                                                                          //Natural: ASSIGN-CHECK-SEQ-NUMBERS
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Ws_Current_Pymnt_Id.equals(getZero())))                                                                                                         //Natural: IF #WS-CURRENT-PYMNT-ID = 0
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr().reset();                                                                                            //Natural: RESET #KEY-PYMNT-CHECK-NBR #KEY-PYMNT-CHECK-SEQ-NBR
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr().reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr().setValue(pnd_Ws_Current_Pymnt_Id);                                                                  //Natural: MOVE #WS-CURRENT-PYMNT-ID TO #KEY-PYMNT-CHECK-NBR
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Current_Pymnt_Seq_Nbr);                                                         //Natural: MOVE #WS-CURRENT-PYMNT-SEQ-NBR TO #KEY-PYMNT-CHECK-SEQ-NBR
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Key.setValue(ldaFcpl378.getPnd_Pymnt_Ext_Key());                                                                                  //Natural: MOVE #PYMNT-EXT-KEY TO #WS-WORK-REC-KEY
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //*  BACK AS EW
        setValueToSubstring("EW",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'EW' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 )
        if (condition(pnd_Ws_Pnd_Run_Type.equals("HELD") || pnd_Ws_Pnd_Run_Type.equals("HOLD")))                                                                          //Natural: IF #RUN-TYPE = 'HELD' OR = 'HOLD'
        {
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(true);                                                                                                   //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := TRUE
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().reset();                                                                                                        //Natural: RESET #FCPL876.#BAR-ENVELOPES
            getWorkFiles().write(3, false, pdaFcpassc2.getPnd_Fcpassc2());                                                                                                //Natural: WRITE WORK FILE 3 #FCPASSC2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().getBoolean());                                          //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := #BARCODE-LDA.#BAR-NEW-RUN
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes());                                                   //Natural: ASSIGN #FCPL876.#BAR-ENVELOPES := #BARCODE-LDA.#BAR-ENVELOPES
            //*  ROXAN
            if (condition(pdaFcpassc2.getPnd_Fcpassc2_Pnd_Pymnt_Cnt().getValue(43).greater(getZero())))                                                                   //Natural: IF #PYMNT-CNT ( 43 ) > 0
            {
                pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(43).setValue(true);                                                                                //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 43 ) := TRUE
            }                                                                                                                                                             //Natural: END-IF
            //*  BACK AS 'EW'
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(45).setValue(true);                                                                                    //Natural: MOVE TRUE TO #FCPACRPT.#TRUTH-TABLE ( 45 ) #FCPACRPT.#TRUTH-TABLE ( 48:50 )
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(48,":",50).setValue(true);
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("   ELECTRONIC WARRANTS CONTROL REPORT");                                                                    //Natural: ASSIGN #FCPACRPT.#TITLE := '   ELECTRONIC WARRANTS CONTROL REPORT'
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                   //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
            gdaFcpg364.getExt_Cntrct_Orgn_Cde().setValue("EW");                                                                                                           //Natural: ASSIGN EXT.CNTRCT-ORGN-CDE := 'EW'
            //* ***CALLNAT 'FCPNSSC2'     /* WILL BE USED FOR FUTURE SS PROCESS LEON
            DbsUtil.callnat(Fcpncnt2.class , getCurrentProcessState(), pdaFcpassc2.getPnd_Fcpassc2(), pdaFcpacrpt.getPnd_Fcpacrpt(), "EW");                               //Natural: CALLNAT 'FCPNCNT2' USING #FCPASSC2 #FCPACRPT 'EW'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*   PRETEND IS SS
        setValueToSubstring("SS",pnd_Ws_Header.getValue(1),66,2);                                                                                                         //Natural: MOVE 'SS' TO SUBSTR ( #WS-HEADER ( 1 ) ,66,2 ) EXT.CNTRCT-ORGN-CDE
        gdaFcpg364.getExt_Cntrct_Orgn_Cde().setValue("SS");
    }
    private void sub_Accum_Control_Data() throws Exception                                                                                                                //Natural: ACCUM-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcplssc1.getPnd_Fcplssc1_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplssc1.getPnd_Fcplssc1_Pnd_Max_Fund()).setValuesByName(ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct()); //Natural: MOVE BY NAME #INV-ACCT TO #CNTRL-INV-ACCT ( 1:#MAX-FUND )
        //* ***CALLNAT 'FCPNSSC1'          /* WILL USE FOR FUTURE SS PROCESS LEON
        DbsUtil.callnat(Fcpnnzc1.class , getCurrentProcessState(), pdaFcpassc1.getPnd_Fcpassc1(), pdaFcpassc2.getPnd_Fcpassc2(), ldaFcplssc1.getPnd_Fcplssc1_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNNZC1' USING #FCPASSC1 #FCPASSC2 #FCPLSSC1.#MAX-FUND #FCPLSSC1.#CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ldaFcplssc1.getPnd_Fcplssc1_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplssc1.getPnd_Fcplssc1_Pnd_Max_Fund()));
        if (condition(Global.isEscape())) return;
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                                  //Natural: ASSIGN #NEW-PYMNT-IND := FALSE
    }
    private void sub_Set_Control_Data() throws Exception                                                                                                                  //Natural: SET-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpanzcn.getPnd_Fcpanzcn().setValuesByName(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10());                                                         //Natural: MOVE BY NAME #RECORD-TYPE-10 TO #FCPANZCN
        //*  LEON
        DbsUtil.callnat(Fcpnnzcn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpanzcn.getPnd_Fcpanzcn(), pdaFcpassc1.getPnd_Fcpassc1());          //Natural: CALLNAT 'FCPNNZCN' #FCPACRPT #FCPANZCN #FCPASSC1
        if (condition(Global.isEscape())) return;
        //* **       WILL BE USED FOR FUTURE SS PROCESS
        //* *****MOVE  BY NAME  #RECORD-TYPE-10  TO  #FCPASSCN
        //* *****CALLNAT  'FCPNSSCN'  #FCPACRPT #FCPASSCN #FCPASSC1
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #J #K #L #WS-CNTR-INV-ACCT #WS-BOTTOM-PART #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-HEADER ( * ) #WS-OCCURS ( * ) #WS-NAME-N-ADDRESS ( * ) #WS-INV-ACCT-DPI-AMT #LAST-PAGE
        pnd_J.reset();
        pnd_K.reset();
        pnd_L.reset();
        pnd_Ws_Cntr_Inv_Acct.reset();
        pnd_Ws_Bottom_Part.reset();
        pnd_Ws_Sim_Dup_Multiplex_Written.reset();
        pnd_Ws_Header.getValue("*").reset();
        pnd_Ws_Occurs.getValue("*").reset();
        pnd_Ws_Name_N_Address.getValue("*").reset();
        pnd_Ws_Pnd_Ws_Inv_Acct_Dpi_Amt.reset();
        pnd_Last_Page.reset();
        pnd_Ws_Printed_Institution_Amt.setValue("N");                                                                                                                     //Natural: ASSIGN #WS-PRINTED-INSTITUTION-AMT = 'N'
        //* ***ASSIGN #WS-SAVE-S-D-M-PLEX = #KEY-SIMPLEX-DUPLEX-MULTIPLEX  /* ORIG
        //* *
        //* * IN FUTURE WHEN WE WILL USE THIS PGM OLSO FOR OTHER ORIGIN
        //* * THE EXTRACT (MAY BE) MUST BE CHANGE TO CALCULATE CORRECT NUMBER
        //* * OF PAGES (INCLUDE IN LINE COUNTER SUBTOTALS) LIKE IS DOING NOW
        //* * FOR OTHERS (NZ,AL). BUT FOR NOW I CAN't use this field and
        //* * PGM HAVE TO DO IT OWN CALCULATION.
        //* *
        //* ****ASSIGN #WS-SAVE-S-D-M-PLEX  = EXT.PYMNT-TOTAL-PAGES        /* LEON
        pnd_Ws_Pnd_Break_If_Acfs.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #BREAK-IF-ACFS #FIRST-PAGE
        pnd_First_Page.setValue(true);
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #NEW-PYMNT-IND := TRUE
    }
    private void sub_Blank_Page() throws Exception                                                                                                                        //Natural: BLANK-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *OMPRESS ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSDA1,FORMS=SSWC,'  /*RL
        //* RL
        pnd_Ws_Pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSDA1X,FORMS=MCMWIC,", "FEED=BOND,END;"));     //Natural: COMPRESS ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=SSDA1X,FORMS=MCMWIC,' 'FEED=BOND,END;' INTO #WS-REC-1 LEAVING NO SPACE
        //*  RL NBR STOPS IN NEW FORMAT
        getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                              //Natural: WRITE WORK FILE 8 #WS-REC-1
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 24
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(24)); pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Ws_Rec_1.setValue("11");                                                                                                                           //Natural: ASSIGN #WS-REC-1 := '11'
            if (condition(pnd_I.equals(12)))                                                                                                                              //Natural: IF #I = 12
            {
                pnd_Ws_Pnd_Ws_Rec_1.setValue("11THIS PAGE IS LEFT BLANK");                                                                                                //Natural: ASSIGN #WS-REC-1 := '11THIS PAGE IS LEFT BLANK'
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(8, false, pnd_Ws_Pnd_Ws_Rec_1);                                                                                                          //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Move_New_To_Old_Key() throws Exception                                                                                                               //Natural: MOVE-NEW-TO-OLD-KEY
    {
        if (BLNatReinput.isReinput()) return;

        //* *      KEY PORTION
        //*                    - PYMNT-TOTAL-PAGES WAS CALCULATED W/O
        //*                      THE TOTAL LINES IN PROGRAM FCPP873  ROXAN
        //*                      SHOULD USE THE S-D-M-PLEX FROM PRINT-ON-WHICH...
        //*  IN FUTURE EXTRACT PGM SHOULD CALCULATE THIS FIELD BECUSE  LEON
        //*  WE PLANING TO USE THIS PGM FOR OTHER ORIGIN's.
        //* **MOVE EXT.PYMNT-TOTAL-PAGES       TO  #KEY-SIMPLEX-DUPLEX-MULTIPLEX
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num().setValue(gdaFcpg364.getExt_Pymnt_Prcss_Seq_Num());                                                              //Natural: MOVE EXT.PYMNT-PRCSS-SEQ-NUM TO #KEY-SEQ-NUM
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num().setValue(gdaFcpg364.getExt_Pymnt_Instmt_Nbr());                                                                //Natural: MOVE EXT.PYMNT-INSTMT-NBR TO #KEY-INST-NUM
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code().setValue(gdaFcpg364.getExt_Cntrct_Hold_Cde());                                                       //Natural: MOVE EXT.CNTRCT-HOLD-CDE TO #KEY-CONTRACT-HOLD-CODE
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr().setValue(gdaFcpg364.getExt_Pymnt_Check_Nbr());                                                          //Natural: MOVE EXT.PYMNT-CHECK-NBR TO #KEY-PYMNT-CHECK-NBR
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Seq_Nbr().setValue(gdaFcpg364.getExt_Pymnt_Check_Seq_Nbr());                                                  //Natural: MOVE EXT.PYMNT-CHECK-SEQ-NBR TO #KEY-PYMNT-CHECK-SEQ-NBR
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr().setValue(gdaFcpg364.getExt_Cntrct_Cmbn_Nbr());                                                          //Natural: MOVE EXT.CNTRCT-CMBN-NBR TO #KEY-CNTRCT-CMBN-NBR
    }
    private void sub_Move_New_To_Old_Rec10() throws Exception                                                                                                             //Natural: MOVE-NEW-TO-OLD-REC10
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(10);                                                                                               //Natural: ASSIGN #KEY-REC-LVL-# := 10
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(pnd_A);                                                                                          //Natural: ASSIGN #KEY-REC-OCCUR-# := #A
        ldaFcpl378.getPnd_Rec_Type_10_Detail().setValuesByName(gdaFcpg364.getExt_Extr());                                                                                 //Natural: MOVE BY NAME EXTR TO #REC-TYPE-10-DETAIL
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1());                                                     //Natural: MOVE #REC-TYPE-10-DET-1 TO #WS-WORK-REC-DET-1
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2());                                                     //Natural: MOVE #REC-TYPE-10-DET-2 TO #WS-WORK-REC-DET-2
        pnd_Ws_Header.getValue("*").setValue(pnd_Ws_Work_Rec.getValue("*"));                                                                                              //Natural: MOVE #WS-WORK-REC ( * ) TO #WS-HEADER ( * )
    }
    private void sub_Move_New_To_Old_Rec20() throws Exception                                                                                                             //Natural: MOVE-NEW-TO-OLD-REC20
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(20);                                                                                               //Natural: ASSIGN #KEY-REC-LVL-# := 20
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(pnd_A);                                                                                          //Natural: ASSIGN #KEY-REC-OCCUR-# := #A
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Side().setValue(pnd_Tbl_Side.getValue(pnd_A));                                                                           //Natural: MOVE #TBL-SIDE ( #A ) TO #REC-TYPE-20-DETAIL.#SIDE
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct_Top().setValue(gdaFcpg364.getExt_C_Inv_Acct());                                                                 //Natural: MOVE EXT.C-INV-ACCT TO #REC-TYPE-20-DETAIL.#INV-ACCT-TOP
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct().setValuesByName(gdaFcpg364.getExt_Inv_Acct().getValue(pnd_A));                                                //Natural: MOVE BY NAME EXT.INV-ACCT ( #A ) TO #REC-TYPE-20-DETAIL.#INV-ACCT
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top().setValue(gdaFcpg364.getExt_C_Pymnt_Ded_Grp());                                                       //Natural: MOVE EXT.C-PYMNT-DED-GRP TO #REC-TYPE-20-DETAIL.#PYMNT-DED-GRP-TOP
        FOR03:                                                                                                                                                            //Natural: FOR #Y 1 TO C-PYMNT-DED-GRP
        for (pnd_Y.setValue(1); condition(pnd_Y.lessOrEqual(gdaFcpg364.getExt_C_Pymnt_Ded_Grp())); pnd_Y.nadd(1))
        {
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pymnt_Ded_Cde().getValue(pnd_Y).setValue(gdaFcpg364.getExt_Pymnt_Ded_Cde().getValue(pnd_Y));                             //Natural: MOVE EXT.PYMNT-DED-CDE ( #Y ) TO #REC-TYPE-20-DETAIL.PYMNT-DED-CDE ( #Y )
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pymnt_Ded_Payee_Cde().getValue(pnd_Y).setValue(gdaFcpg364.getExt_Pymnt_Ded_Payee_Cde().getValue(pnd_Y));                 //Natural: MOVE EXT.PYMNT-DED-PAYEE-CDE ( #Y ) TO #REC-TYPE-20-DETAIL.PYMNT-DED-PAYEE-CDE ( #Y )
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pymnt_Ded_Amt().getValue(pnd_Y).setValue(gdaFcpg364.getExt_Pymnt_Ded_Amt().getValue(pnd_Y));                             //Natural: MOVE EXT.PYMNT-DED-AMT ( #Y ) TO #REC-TYPE-20-DETAIL.PYMNT-DED-AMT ( #Y )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1().setValuesByName(gdaFcpg364.getExt_Extr());                                                         //Natural: MOVE BY NAME EXTR TO #RECORD-TYPE-20-GRP1
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1.setValue(ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1());                                                     //Natural: MOVE #REC-TYPE-20-DET-1 TO #WS-WORK-REC-DET-1
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2.setValue(ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2());                                                     //Natural: MOVE #REC-TYPE-20-DET-2 TO #WS-WORK-REC-DET-2
    }
    private void sub_Move_New_To_Old_Rec30() throws Exception                                                                                                             //Natural: MOVE-NEW-TO-OLD-REC30
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(30);                                                                                               //Natural: ASSIGN #KEY-REC-LVL-# := 30
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(pnd_Z);                                                                                          //Natural: ASSIGN #KEY-REC-OCCUR-# := #Z
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Rcrd_Typ().setValue(gdaFcpg364.getExt_Rcrd_Typ());                                                                           //Natural: MOVE EXT.RCRD-TYP TO #REC-TYPE-30-DETAIL.RCRD-TYP
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Cntrct_Orgn_Cde().setValue(gdaFcpg364.getExt_Cntrct_Orgn_Cde());                                                             //Natural: MOVE EXT.CNTRCT-ORGN-CDE TO #REC-TYPE-30-DETAIL.CNTRCT-ORGN-CDE
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Cntrct_Ppcn_Nbr().setValue(gdaFcpg364.getExt_Cntrct_Ppcn_Nbr());                                                             //Natural: MOVE EXT.CNTRCT-PPCN-NBR TO #REC-TYPE-30-DETAIL.CNTRCT-PPCN-NBR
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Cntrct_Payee_Cde().setValue(gdaFcpg364.getExt_Cntrct_Payee_Cde());                                                           //Natural: MOVE EXT.CNTRCT-PAYEE-CDE TO #REC-TYPE-30-DETAIL.CNTRCT-PAYEE-CDE
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Cntrct_Invrse_Dte().setValue(gdaFcpg364.getExt_Cntrct_Invrse_Dte());                                                         //Natural: MOVE EXT.CNTRCT-INVRSE-DTE TO #REC-TYPE-30-DETAIL.CNTRCT-INVRSE-DTE
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pymnt_Prcss_Seq_Nbr().setValue(gdaFcpg364.getExt_Pymnt_Prcss_Seq_Num());                                                     //Natural: MOVE EXT.PYMNT-PRCSS-SEQ-NUM TO #REC-TYPE-30-DETAIL.PYMNT-PRCSS-SEQ-NBR
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Ph_Last_Name().setValue(gdaFcpg364.getExt_Ph_Last_Name());                                                                   //Natural: MOVE EXT.PH-LAST-NAME TO #REC-TYPE-30-DETAIL.PH-LAST-NAME
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Ph_First_Name().setValue(gdaFcpg364.getExt_Ph_First_Name());                                                                 //Natural: MOVE EXT.PH-FIRST-NAME TO #REC-TYPE-30-DETAIL.PH-FIRST-NAME
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Ph_Middle_Name().setValue(gdaFcpg364.getExt_Ph_Middle_Name());                                                               //Natural: MOVE EXT.PH-MIDDLE-NAME TO #REC-TYPE-30-DETAIL.PH-MIDDLE-NAME
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp().setValuesByName(gdaFcpg364.getExt_Pymnt_Nme_And_Addr_Grp().getValue(pnd_Z));                    //Natural: MOVE BY NAME EXT.PYMNT-NME-AND-ADDR-GRP ( #Z ) TO #PYMNT-NME-AND-ADDR-GRP
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2().setValuesByName(gdaFcpg364.getExt_Extr());                                                         //Natural: MOVE BY NAME EXTR TO #RECORD-TYPE-30-GRP2
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_1.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_1());                                                     //Natural: MOVE #REC-TYPE-30-DET-1 TO #WS-WORK-REC-DET-1
        pnd_Ws_Work_Rec_Pnd_Ws_Work_Rec_Det_2.setValue(ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Rec_Type_30_Det_2());                                                     //Natural: MOVE #REC-TYPE-30-DET-2 TO #WS-WORK-REC-DET-2
    }
    private void sub_Process_Record_Type_40_45() throws Exception                                                                                                         //Natural: PROCESS-RECORD-TYPE-40-45
    {
        if (BLNatReinput.isReinput()) return;

                                                                                                                                                                          //Natural: PERFORM GET-PLAN-RECORD
        sub_Get_Plan_Record();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Record_Type_40().reset();                                                                                                //Natural: RESET #RECORD-TYPE-40
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(40);                                                                                               //Natural: MOVE 40 TO #KEY-REC-LVL-#
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1().setValuesByName(gdaFcpg364.getVw_fcp_Cons_Plan());                                                 //Natural: MOVE BY NAME FCP-CONS-PLAN TO #RECORD-TYPE-40-GRP1
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pymnt_Check_Dte().setValue(gdaFcpg364.getExt_Pymnt_Check_Dte());                                                             //Natural: MOVE EXT.PYMNT-CHECK-DTE TO #REC-TYPE-40-DETAIL.PYMNT-CHECK-DTE
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display().setValue(true);                                                                                      //Natural: MOVE TRUE TO #PLAN-TYPE-DISPLAY
        if (condition(gdaFcpg364.getFcp_Cons_Plan_Cntrct_Good_Contract().getBoolean() && ! (gdaFcpg364.getFcp_Cons_Plan_Cntrct_Invalid_Cond_Ind().getBoolean())           //Natural: IF FCP-CONS-PLAN.CNTRCT-GOOD-CONTRACT AND NOT FCP-CONS-PLAN.CNTRCT-INVALID-COND-IND AND NOT FCP-CONS-PLAN.CNTRCT-MULTI-PAYEE
            && ! (gdaFcpg364.getFcp_Cons_Plan_Cntrct_Multi_Payee().getBoolean())))
        {
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().setValue(true);                                                                                        //Natural: MOVE TRUE TO #GOOD-LETTER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().setValue(false);                                                                                       //Natural: MOVE FALSE TO #GOOD-LETTER
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gdaFcpg364.getFcp_Cons_Plan_Count_Castplan_Data().equals(getZero())))                                                                               //Natural: IF C*PLAN-DATA = 0
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(1);                                                                                          //Natural: MOVE 1 TO #KEY-REC-OCCUR-#
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top().setValue(1);                                                                                         //Natural: MOVE 1 TO #PLAN-DATA-TOP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top().setValue(gdaFcpg364.getFcp_Cons_Plan_Count_Castplan_Data());                                         //Natural: MOVE C*PLAN-DATA TO #PLAN-DATA-TOP
            if (condition(! (ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean()) && gdaFcpg364.getFcp_Cons_Plan_Count_Castplan_Data().greater(1)         //Natural: IF NOT #GOOD-LETTER AND C*PLAN-DATA > 1 AND FCP-CONS-PLAN.PLAN-TYPE ( 2:#PLAN-DATA-TOP ) NE FCP-CONS-PLAN.PLAN-TYPE ( 1 )
                && gdaFcpg364.getFcp_Cons_Plan_Plan_Type().getValue(2,":",ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top()).notEquals(gdaFcpg364.getFcp_Cons_Plan_Plan_Type().getValue(1))))
            {
                ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display().setValue(false);                                                                             //Natural: MOVE FALSE TO #PLAN-TYPE-DISPLAY
            }                                                                                                                                                             //Natural: END-IF
            FOR04:                                                                                                                                                        //Natural: FOR #I = 1 TO C*PLAN-DATA
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(gdaFcpg364.getFcp_Cons_Plan_Count_Castplan_Data())); pnd_I.nadd(1))
            {
                ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(pnd_I);                                                                                  //Natural: MOVE #I TO #KEY-REC-OCCUR-#
                ldaFcpl378.getPnd_Rec_Type_40_Detail_Plan_Data().setValuesByName(gdaFcpg364.getFcp_Cons_Plan_Plan_Data().getValue(pnd_I));                                //Natural: MOVE BY NAME FCP-CONS-PLAN.PLAN-DATA ( #I ) TO #REC-TYPE-40-DETAIL.PLAN-DATA
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(gdaFcpg364.getFcp_Cons_Plan_Cntrct_Invalid_Cond_Ind().getBoolean()))                                                                                //Natural: IF FCP-CONS-PLAN.CNTRCT-INVALID-COND-IND
        {
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Record_Type_45().reset();                                                                                            //Natural: RESET #RECORD-TYPE-45
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(45);                                                                                           //Natural: MOVE 45 TO #KEY-REC-LVL-#
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(1);                                                                                          //Natural: MOVE 1 TO #KEY-REC-OCCUR-#
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1().setValuesByName(gdaFcpg364.getVw_fcp_Cons_Plan());                                             //Natural: MOVE BY NAME FCP-CONS-PLAN TO #RECORD-TYPE-45-GRP1
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Invalid_Reasons_Top().setValue(gdaFcpg364.getFcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons());                      //Natural: MOVE C*CNTRCT-INVALID-REASONS TO #REC-TYPE-45-DETAIL.#INVALID-REASONS-TOP
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Pymnt_Check_Dte().setValue(gdaFcpg364.getExt_Pymnt_Check_Dte());                                                         //Natural: MOVE EXT.PYMNT-CHECK-DTE TO #REC-TYPE-45-DETAIL.PYMNT-CHECK-DTE
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Invalid_Reasons().getValue(1,":",8).setValue(gdaFcpg364.getFcp_Cons_Plan_Cntrct_Invalid_Reasons().getValue(1,            //Natural: MOVE FCP-CONS-PLAN.CNTRCT-INVALID-REASONS ( 1:8 ) TO #REC-TYPE-45-DETAIL.INVALID-REASONS ( 1:8 )
                ":",8));
            if (condition(gdaFcpg364.getFcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons().greater(8)))                                                                     //Natural: IF C*CNTRCT-INVALID-REASONS > 8
            {
                ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(2);                                                                                      //Natural: MOVE 2 TO #KEY-REC-OCCUR-#
                ldaFcpl378.getPnd_Rec_Type_45_Detail_Invalid_Reasons().getValue("*").reset();                                                                             //Natural: RESET #REC-TYPE-45-DETAIL.INVALID-REASONS ( * )
                ldaFcpl378.getPnd_Rec_Type_45_Detail_Invalid_Reasons().getValue(1,":",2).setValue(gdaFcpg364.getFcp_Cons_Plan_Cntrct_Invalid_Reasons().getValue(9,        //Natural: MOVE FCP-CONS-PLAN.CNTRCT-INVALID-REASONS ( 9:10 ) TO #REC-TYPE-45-DETAIL.INVALID-REASONS ( 1:2 )
                    ":",10));
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRETEND IS SS
        pnd_Plan_S_Cntrct_Orgn_Cde.setValue("SS");                                                                                                                        //Natural: MOVE 'SS' TO #PLAN-S.CNTRCT-ORGN-CDE
    }
    private void sub_Get_Plan_Record() throws Exception                                                                                                                   //Natural: GET-PLAN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****MOVE EXT.CNTRCT-ORGN-CDE     TO #PLAN-S.CNTRCT-ORGN-CDE
        //*  BACK AS EW
        pnd_Plan_S_Cntrct_Orgn_Cde.setValue("EW");                                                                                                                        //Natural: MOVE 'EW' TO #PLAN-S.CNTRCT-ORGN-CDE
        pnd_Plan_S_Cntrct_Ppcn_Nbr.setValue(gdaFcpg364.getExt_Cntrct_Ppcn_Nbr());                                                                                         //Natural: MOVE EXT.CNTRCT-PPCN-NBR TO #PLAN-S.CNTRCT-PPCN-NBR
        pnd_Plan_S_Pymnt_Prcss_Seq_Nbr.setValue(gdaFcpg364.getExt_Pymnt_Prcss_Seq_Nbr());                                                                                 //Natural: MOVE EXT.PYMNT-PRCSS-SEQ-NBR TO #PLAN-S.PYMNT-PRCSS-SEQ-NBR
        pnd_Plan_S_Cntl_Check_Dte.setValue(gdaFcpg364.getExt_Pymnt_Check_Dte());                                                                                          //Natural: MOVE EXT.PYMNT-CHECK-DTE TO #PLAN-S.CNTL-CHECK-DTE
        gdaFcpg364.getVw_fcp_Cons_Plan().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) FCP-CONS-PLAN BY RCRD-ORGN-PPCN-PRCSS-CHKDT = #PLAN-SUPERDE THRU #PLAN-SUPERDE
        (
        "READ_PLAN",
        new Wc[] { new Wc("RCRD_ORGN_PPCN_PRCSS_CHKDT", ">=", pnd_Plan_S_Pnd_Plan_Superde.getBinary(), "And", WcType.BY) ,
        new Wc("RCRD_ORGN_PPCN_PRCSS_CHKDT", "<=", pnd_Plan_S_Pnd_Plan_Superde.getBinary(), WcType.BY) },
        new Oc[] { new Oc("RCRD_ORGN_PPCN_PRCSS_CHKDT", "ASC") },
        1
        );
        READ_PLAN:
        while (condition(gdaFcpg364.getVw_fcp_Cons_Plan().readNextRow("READ_PLAN")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(gdaFcpg364.getVw_fcp_Cons_Plan().getAstCOUNTER().equals(getZero())))                                                                                //Natural: IF *COUNTER ( READ-PLAN. ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"PLAN RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN    :",pnd_Plan_S_Cntrct_Orgn_Cde,new  //Natural: WRITE '***' 25T 'PLAN RECORD IS NOT FOUND' 77T '***' / '***' 25T 'ORIGIN    :' #PLAN-S.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'PPCN      :' #PLAN-S.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'SEQUENCE# :' #PLAN-S.PYMNT-PRCSS-SEQ-NBR 77T '***' / '***' 25T 'CHECK DATE:' #PLAN-S.CNTL-CHECK-DTE ( EM = MM/DD/YYYY ) 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PPCN      :",pnd_Plan_S_Cntrct_Ppcn_Nbr,new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"SEQUENCE# :",pnd_Plan_S_Pymnt_Prcss_Seq_Nbr,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"CHECK DATE:",pnd_Plan_S_Cntl_Check_Dte, 
                new ReportEditMask ("MM/DD/YYYY"),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  DEFAULT
    private void sub_Print_On_Which_Side_Of_Paper() throws Exception                                                                                                      //Natural: PRINT-ON-WHICH-SIDE-OF-PAPER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Save_S_D_M_Plex.setValue("1");                                                                                                                             //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := '1'
        FOR05:                                                                                                                                                            //Natural: FOR #X 1 TO EXT.C-INV-ACCT
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(gdaFcpg364.getExt_C_Inv_Acct())); pnd_X.nadd(1))
        {
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(gdaFcpg364.getExt_Inv_Acct_Cde().getValue(pnd_X));                                                  //Natural: ASSIGN #INV-ACCT-INPUT := EXT.INV-ACCT-CDE ( #X )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            if (condition(pnd_X.equals(1)))                                                                                                                               //Natural: IF #X = 1
            {
                pnd_Ws_Company.setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde());                                                                                   //Natural: ASSIGN #WS-COMPANY := #COMPANY-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ws_Company.notEquals(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde())))                                                                   //Natural: IF #WS-COMPANY NE #COMPANY-CDE
                {
                    pnd_Stmnt_Lines.nadd(2);                                                                                                                              //Natural: ADD 2 TO #STMNT-LINES
                    pnd_Ws_Company.setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde());                                                                               //Natural: ASSIGN #WS-COMPANY := #COMPANY-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  LINES PER STATEMENT: 5 LINES PER EACH FUND
            pnd_Stmnt_Lines.nadd(5);                                                                                                                                      //Natural: ADD 5 TO #STMNT-LINES
            if (condition(pnd_X.equals(gdaFcpg364.getExt_C_Inv_Acct())))                                                                                                  //Natural: IF #X = EXT.C-INV-ACCT
            {
                pnd_Stmnt_Lines.nadd(2);                                                                                                                                  //Natural: ADD 2 TO #STMNT-LINES
            }                                                                                                                                                             //Natural: END-IF
            //*  JFT - FORMAT STATEMENT
            //*  ROXAN 3.22.00
            if (condition(gdaFcpg364.getExt_Pymnt_Pay_Type_Req_Ind().equals(2) || gdaFcpg364.getExt_Cntrct_Payee_Cde().equals("ALT1") || gdaFcpg364.getExt_Cntrct_Payee_Cde().equals("ALT2")  //Natural: IF EXT.PYMNT-PAY-TYPE-REQ-IND = 2 OR EXT.CNTRCT-PAYEE-CDE = 'ALT1' OR = 'ALT2' OR ( SUBSTR ( EXT.PYMNT-NME ( 1 ) ,1,2 ) = 'CR' )
                || gdaFcpg364.getExt_Pymnt_Nme().getValue(1).getSubstring(1,2).equals("CR")))
            {
                pnd_Form.setValue("S");                                                                                                                                   //Natural: ASSIGN #FORM := 'S'
            }                                                                                                                                                             //Natural: END-IF
            //*  ROXAN 3.22.00
            if (condition(((gdaFcpg364.getExt_Pymnt_Pay_Type_Req_Ind().equals(6) && ! (((gdaFcpg364.getExt_Cntrct_Payee_Cde().equals("ALT1") || gdaFcpg364.getExt_Cntrct_Payee_Cde().equals("ALT2"))  //Natural: IF EXT.PYMNT-PAY-TYPE-REQ-IND = 6 AND NOT ( ( EXT.CNTRCT-PAYEE-CDE = 'ALT1' OR = 'ALT2' ) OR SUBSTR ( EXT.PYMNT-NME ( 1 ) ,1,2 ) = 'CR' ) AND EXT.CNTRCT-TYPE-CDE = 'R'
                || gdaFcpg364.getExt_Pymnt_Nme().getValue(1).getSubstring(1,2).equals("CR")))) && gdaFcpg364.getExt_Cntrct_Type_Cde().equals("R"))))
            {
                pnd_Form.setValue("S");                                                                                                                                   //Natural: ASSIGN #FORM := 'S'
            }                                                                                                                                                             //Natural: END-IF
            //* *IF EXT.CNTRCT-PAYEE-CDE = 'ANNT' AND #FORM = ' '     /* ROXAN 3.28.00
            //*  ROXAN 3.28.00              /* 3.28.00
            if (condition(((gdaFcpg364.getExt_Cntrct_Payee_Cde().equals("ANNT") || (DbsUtil.maskMatches(gdaFcpg364.getExt_Cntrct_Payee_Cde().getSubstring(1,2),"99")      //Natural: IF ( EXT.CNTRCT-PAYEE-CDE = 'ANNT' OR ( SUBSTR ( EXT.CNTRCT-PAYEE-CDE,1,2 ) = MASK ( 99 ) AND SUBSTR ( EXT.CNTRCT-PAYEE-CDE,3,1 ) NE 'P' ) ) AND #FORM = ' '
                && !gdaFcpg364.getExt_Cntrct_Payee_Cde().getSubstring(3,1).equals("P"))) && pnd_Form.equals(" "))))
            {
                if (condition(gdaFcpg364.getExt_Pymnt_Nme().getValue(2).equals(" ") && gdaFcpg364.getExt_Pymnt_Addr_Line1_Txt().getValue(2).equals(" ")                   //Natural: IF EXT.PYMNT-NME ( 2 ) = ' ' AND EXT.PYMNT-ADDR-LINE1-TXT ( 2 ) = ' ' AND EXT.PYMNT-ADDR-LINE2-TXT ( 2 ) = ' ' AND EXT.PYMNT-ADDR-LINE3-TXT ( 2 ) = ' ' AND EXT.PYMNT-ADDR-LINE4-TXT ( 2 ) = ' ' AND EXT.PYMNT-ADDR-LINE5-TXT ( 2 ) = ' ' AND EXT.PYMNT-ADDR-LINE6-TXT ( 2 ) = ' '
                    && gdaFcpg364.getExt_Pymnt_Addr_Line2_Txt().getValue(2).equals(" ") && gdaFcpg364.getExt_Pymnt_Addr_Line3_Txt().getValue(2).equals(" ") 
                    && gdaFcpg364.getExt_Pymnt_Addr_Line4_Txt().getValue(2).equals(" ") && gdaFcpg364.getExt_Pymnt_Addr_Line5_Txt().getValue(2).equals(" ") 
                    && gdaFcpg364.getExt_Pymnt_Addr_Line6_Txt().getValue(2).equals(" ")))
                {
                    pnd_Form.setValue("C");                                                                                                                               //Natural: ASSIGN #FORM := 'C'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Form.setValue("S");                                                                                                                               //Natural: ASSIGN #FORM := 'S'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Form.equals("C")))                                                                                                                          //Natural: IF #FORM = 'C'
            {
                short decideConditionsMet2267 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #STMNT-LINES;//Natural: VALUE 1 : 18
                if (condition(((pnd_Stmnt_Lines.greaterOrEqual(1) && pnd_Stmnt_Lines.lessOrEqual(18)))))
                {
                    decideConditionsMet2267++;
                    //*  ROXAN
                    pnd_Tbl_Side.getValue(pnd_X).setValue("1");                                                                                                           //Natural: MOVE '1' TO #TBL-SIDE ( #X )
                    pnd_Ws_Save_S_D_M_Plex.setValue("1");                                                                                                                 //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := '1'
                }                                                                                                                                                         //Natural: VALUE 19 : 51
                else if (condition(((pnd_Stmnt_Lines.greaterOrEqual(19) && pnd_Stmnt_Lines.lessOrEqual(51)))))
                {
                    decideConditionsMet2267++;
                    //*  ROXAN
                    pnd_Tbl_Side.getValue(pnd_X).setValue("2");                                                                                                           //Natural: MOVE '2' TO #TBL-SIDE ( #X )
                    pnd_Ws_Save_S_D_M_Plex.setValue("2");                                                                                                                 //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := '2'
                }                                                                                                                                                         //Natural: VALUE 52 : 116
                else if (condition(((pnd_Stmnt_Lines.greaterOrEqual(52) && pnd_Stmnt_Lines.lessOrEqual(116)))))
                {
                    decideConditionsMet2267++;
                    //*  ROXAN
                    pnd_Tbl_Side.getValue(pnd_X).setValue("3");                                                                                                           //Natural: MOVE '3' TO #TBL-SIDE ( #X )
                    pnd_Ws_Save_S_D_M_Plex.setValue("3");                                                                                                                 //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := '3'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Form.equals("S")))                                                                                                                          //Natural: IF #FORM = 'S'
            {
                short decideConditionsMet2286 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #STMNT-LINES;//Natural: VALUE 1 : 18
                if (condition(((pnd_Stmnt_Lines.greaterOrEqual(1) && pnd_Stmnt_Lines.lessOrEqual(18)))))
                {
                    decideConditionsMet2286++;
                    //*  ROXAN
                    pnd_Tbl_Side.getValue(pnd_X).setValue("1");                                                                                                           //Natural: MOVE '1' TO #TBL-SIDE ( #X )
                    pnd_Ws_Save_S_D_M_Plex.setValue("1");                                                                                                                 //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := '1'
                }                                                                                                                                                         //Natural: VALUE 19 : 83
                else if (condition(((pnd_Stmnt_Lines.greaterOrEqual(19) && pnd_Stmnt_Lines.lessOrEqual(83)))))
                {
                    decideConditionsMet2286++;
                    //*  ROXAN
                    pnd_Tbl_Side.getValue(pnd_X).setValue("2");                                                                                                           //Natural: MOVE '2' TO #TBL-SIDE ( #X )
                    pnd_Ws_Save_S_D_M_Plex.setValue("2");                                                                                                                 //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := '2'
                }                                                                                                                                                         //Natural: VALUE 84 : 148
                else if (condition(((pnd_Stmnt_Lines.greaterOrEqual(84) && pnd_Stmnt_Lines.lessOrEqual(148)))))
                {
                    decideConditionsMet2286++;
                    //*  ROXAN
                    pnd_Tbl_Side.getValue(pnd_X).setValue("3");                                                                                                           //Natural: MOVE '3' TO #TBL-SIDE ( #X )
                    pnd_Ws_Save_S_D_M_Plex.setValue("3");                                                                                                                 //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := '3'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex().setValue(pnd_Ws_Save_S_D_M_Plex);                                                              //Natural: ASSIGN #KEY-SIMPLEX-DUPLEX-MULTIPLEX := #WS-SAVE-S-D-M-PLEX
    }
    private void sub_Reset_10_20_30_40_45() throws Exception                                                                                                              //Natural: RESET-10-20-30-40-45
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl378.getPnd_Rec_Type_10_Detail().reset();                                                                                                                   //Natural: RESET #REC-TYPE-10-DETAIL #REC-TYPE-20-DETAIL #REC-TYPE-30-DETAIL #REC-TYPE-40-DETAIL #REC-TYPE-45-DETAIL #REC-TYPE-20-DETAIL.PYMNT-DED-CDE ( * ) #REC-TYPE-20-DETAIL.PYMNT-DED-AMT ( * )
        ldaFcpl378.getPnd_Rec_Type_20_Detail().reset();
        ldaFcpl378.getPnd_Rec_Type_30_Detail().reset();
        ldaFcpl378.getPnd_Rec_Type_40_Detail().reset();
        ldaFcpl378.getPnd_Rec_Type_45_Detail().reset();
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pymnt_Ded_Cde().getValue("*").reset();
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pymnt_Ded_Amt().getValue("*").reset();
    }
    //*  RL
    private void sub_Get_Check_Format_Data() throws Exception                                                                                                             //Natural: GET-CHECK-FORMAT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                      //Natural: RESET #BAR-ENV-ID-NUM
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");

        getReports().write(0, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(24),"PROCESS ELECTRONIC WARRANTS CHECKS",new TabSetting(68),"REPORT: RPT0",NEWLINE,new 
            TabSetting(36),pnd_Ws_Pnd_Run_Type,NEWLINE,NEWLINE);
    }
}
