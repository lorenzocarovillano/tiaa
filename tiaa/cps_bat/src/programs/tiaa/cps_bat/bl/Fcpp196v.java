/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:04 PM
**        * FROM NATURAL PROGRAM : Fcpp196v
************************************************************
**        * FILE NAME            : Fcpp196v.java
**        * CLASS NAME           : Fcpp196v
**        * INSTANCE NAME        : Fcpp196v
************************************************************
************************************************************************
* PROGRAM  : FCPP196V
* SYSTEM   : CONSOLIDATED PAYMENT SYSTEM
* TITLE    : PREP CANCEL RECS FOR WACHOVIA VOID PROCES
* WRITTEN  : LANDRUM - FEB 26, 2007
*
* WACHOVIA ACCOUNTS 2079950055011 2079950060725
* OIA    DELETES CPWP232 - PCP1676D
* RACASH DELETES FCPP252 - P1446CPD HEADER DOES NOT HAVE ACCT NBR
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp196v extends BLNatBase
{
    // Data Areas
    private PdaCpsa110 pdaCpsa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Input_Delete_Rec;

    private DbsGroup pnd_Input_Delete_Rec__R_Field_1;
    private DbsField pnd_Input_Delete_Rec_Pnd_Rec_Type_A1;
    private DbsField pnd_Input_Delete_Rec_Pnd_Rec_Data_A79;

    private DbsGroup pnd_Input_Delete_Rec__R_Field_2;
    private DbsField pnd_Input_Delete_Rec_Pnd_Pos_2_4;
    private DbsField pnd_Input_Delete_Rec_Pnd_Current_Date;
    private DbsField pnd_Input_Delete_Rec_Pnd_From_Date;
    private DbsField pnd_Input_Delete_Rec_Pnd_To_Date;
    private DbsField pnd_Input_Delete_Rec_Filler;

    private DbsGroup pnd_Input_Delete_Rec__R_Field_3;
    private DbsField pnd_Input_Delete_Rec_Pnd_Bank_Account_Literal;
    private DbsField pnd_Input_Delete_Rec_Pnd_Hdr_Rec_Bank_Account;

    private DbsGroup pnd_Input_Delete_Rec__R_Field_4;
    private DbsField pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_A13;

    private DbsGroup pnd_Input_Delete_Rec__R_Field_5;
    private DbsField pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_N13;
    private DbsField pnd_Input_Delete_Rec_Pnd_Hdr_Rec_Bank_Routing;

    private DbsGroup pnd_Input_Delete_Rec__R_Field_6;
    private DbsField pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr;

    private DbsGroup pnd_Input_Delete_Rec__R_Field_7;
    private DbsField pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N3;
    private DbsField pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N7;
    private DbsField pnd_Input_Delete_Rec_Pnd_Check_Issue_Date;
    private DbsField pnd_Input_Delete_Rec_Pnd_Filler_Zeroes;
    private DbsField pnd_Input_Delete_Rec_Pnd_Check_Amount;
    private DbsField pnd_Input_Delete_Rec_Pnd_Accounting_Date;
    private DbsField pnd_Input_Delete_Rec_Pnd_Detail_Name;

    private DbsGroup pnd_Output_Delete_Rec;
    private DbsField pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D;
    private DbsField pnd_Output_Delete_Rec_Pnd_Record_Type;
    private DbsField pnd_Output_Delete_Rec_Pnd_Bank_Num;
    private DbsField pnd_Output_Delete_Rec_Pnd_Account_Num;
    private DbsField pnd_Output_Delete_Rec_Pnd_Run_Date;
    private DbsField pnd_Output_Delete_Rec_Pnd_Run_Time;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_8;
    private DbsField pnd_Output_Delete_Rec_Pnd_Run_Time_A6;
    private DbsField pnd_Output_Delete_Rec_Pnd_Input_Type;
    private DbsField pnd_Output_Delete_Rec_Pnd_Stop_Type;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_9;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N1;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_10;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N3;
    private DbsField pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N7;
    private DbsField pnd_Output_Delete_Rec_Pnd_Ending_Check_Num;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Amt;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_11;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Amt_N5;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Amt_N10;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_N;

    private DbsGroup pnd_Output_Delete_Rec__R_Field_12;
    private DbsField pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A;
    private DbsField pnd_Output_Delete_Rec_Pnd_Payee_Name;
    private DbsField pnd_Output_Delete_Rec_Pnd_Reason;
    private DbsField pnd_Output_Delete_Rec_Pnd_Auto_Check_Paid_Inquiry;
    private DbsField pnd_Output_Delete_Rec_Pnd_Stop_Duration_Years;
    private DbsField pnd_Output_Delete_Rec_Pnd_User_Id;
    private DbsField pnd_Wacho_Racash_Bank_Acct_N13;
    private DbsField pnd_Ba_Nbr_1;
    private DbsField pnd_Acct_Total_1;
    private DbsField pnd_Acct_Count_1;
    private DbsField pnd_Ba_Nbr_2;
    private DbsField pnd_Acct_Total_2;
    private DbsField pnd_Acct_Count_2;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Amt;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnt_Bank_Account;
    private DbsField fcp_Cons_Pymnt_Bank_Routing;
    private DbsField fcp_Cons_Pymnt_Pymnt_Source_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup fcp_Cons_Pymnt__R_Field_13;
    private DbsField fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Pymnt_Pymnt_Instmt_Nbr;

    private DataAccessProgramView vw_fcp_Cons_Addr;
    private DbsField fcp_Cons_Addr_Rcrd_Typ;
    private DbsField fcp_Cons_Addr_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Addr_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Addr_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Addr_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr;
    private DbsField fcp_Cons_Addr_Ph_Last_Name;
    private DbsField fcp_Cons_Addr_Ph_First_Name;
    private DbsField fcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp;

    private DbsGroup fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp;
    private DbsField fcp_Cons_Addr_Pymnt_Nme;
    private DbsField fcp_Cons_Addr_Pymnt_Addr_Line1_Txt;
    private DbsField fcp_Cons_Addr_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Addr_Key;

    private DbsGroup pnd_Addr_Key__R_Field_14;
    private DbsField pnd_Addr_Key_Pnd_Addr_Ppcn;
    private DbsField pnd_Addr_Key_Pnd_Addr_Cde;
    private DbsField pnd_Addr_Key_Pnd_Addr_Orgn;
    private DbsField pnd_Addr_Key_Pnd_Addr_Invdt;
    private DbsField pnd_Addr_Key_Pnd_Addr_Seq;
    private DbsField pnd_Cmbne_Nbr;

    private DbsGroup pnd_Cmbne_Nbr__R_Field_15;
    private DbsField pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Total_Write_Cnt;
    private DbsField pnd_Total_Amount;
    private DbsField pnd_D;
    private DbsField pnd_Date_Mmddyyyy;
    private DbsField pnd_Time;

    private DbsGroup pnd_Time__R_Field_16;
    private DbsField pnd_Time_Pnd_Time_N6;
    private DbsField pnd_Rpt_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpsa110 = new PdaCpsa110(localVariables);

        // Local Variables
        pnd_Input_Delete_Rec = localVariables.newFieldInRecord("pnd_Input_Delete_Rec", "#INPUT-DELETE-REC", FieldType.STRING, 80);

        pnd_Input_Delete_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Delete_Rec__R_Field_1", "REDEFINE", pnd_Input_Delete_Rec);
        pnd_Input_Delete_Rec_Pnd_Rec_Type_A1 = pnd_Input_Delete_Rec__R_Field_1.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Rec_Type_A1", "#REC-TYPE-A1", 
            FieldType.STRING, 1);
        pnd_Input_Delete_Rec_Pnd_Rec_Data_A79 = pnd_Input_Delete_Rec__R_Field_1.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Rec_Data_A79", "#REC-DATA-A79", 
            FieldType.STRING, 79);

        pnd_Input_Delete_Rec__R_Field_2 = pnd_Input_Delete_Rec__R_Field_1.newGroupInGroup("pnd_Input_Delete_Rec__R_Field_2", "REDEFINE", pnd_Input_Delete_Rec_Pnd_Rec_Data_A79);
        pnd_Input_Delete_Rec_Pnd_Pos_2_4 = pnd_Input_Delete_Rec__R_Field_2.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Pos_2_4", "#POS-2-4", FieldType.STRING, 
            3);
        pnd_Input_Delete_Rec_Pnd_Current_Date = pnd_Input_Delete_Rec__R_Field_2.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Current_Date", "#CURRENT-DATE", 
            FieldType.STRING, 8);
        pnd_Input_Delete_Rec_Pnd_From_Date = pnd_Input_Delete_Rec__R_Field_2.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_From_Date", "#FROM-DATE", FieldType.STRING, 
            6);
        pnd_Input_Delete_Rec_Pnd_To_Date = pnd_Input_Delete_Rec__R_Field_2.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_To_Date", "#TO-DATE", FieldType.STRING, 
            6);
        pnd_Input_Delete_Rec_Filler = pnd_Input_Delete_Rec__R_Field_2.newFieldInGroup("pnd_Input_Delete_Rec_Filler", "FILLER", FieldType.STRING, 56);

        pnd_Input_Delete_Rec__R_Field_3 = pnd_Input_Delete_Rec__R_Field_2.newGroupInGroup("pnd_Input_Delete_Rec__R_Field_3", "REDEFINE", pnd_Input_Delete_Rec_Filler);
        pnd_Input_Delete_Rec_Pnd_Bank_Account_Literal = pnd_Input_Delete_Rec__R_Field_3.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Bank_Account_Literal", 
            "#BANK-ACCOUNT-LITERAL", FieldType.STRING, 13);
        pnd_Input_Delete_Rec_Pnd_Hdr_Rec_Bank_Account = pnd_Input_Delete_Rec__R_Field_3.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Hdr_Rec_Bank_Account", 
            "#HDR-REC-BANK-ACCOUNT", FieldType.STRING, 21);

        pnd_Input_Delete_Rec__R_Field_4 = pnd_Input_Delete_Rec__R_Field_3.newGroupInGroup("pnd_Input_Delete_Rec__R_Field_4", "REDEFINE", pnd_Input_Delete_Rec_Pnd_Hdr_Rec_Bank_Account);
        pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_A13 = pnd_Input_Delete_Rec__R_Field_4.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_A13", "#HDR-BANK-ACCT-A13", 
            FieldType.STRING, 13);

        pnd_Input_Delete_Rec__R_Field_5 = pnd_Input_Delete_Rec__R_Field_4.newGroupInGroup("pnd_Input_Delete_Rec__R_Field_5", "REDEFINE", pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_A13);
        pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_N13 = pnd_Input_Delete_Rec__R_Field_5.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_N13", "#HDR-BANK-ACCT-N13", 
            FieldType.NUMERIC, 13);
        pnd_Input_Delete_Rec_Pnd_Hdr_Rec_Bank_Routing = pnd_Input_Delete_Rec__R_Field_3.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Hdr_Rec_Bank_Routing", 
            "#HDR-REC-BANK-ROUTING", FieldType.STRING, 9);

        pnd_Input_Delete_Rec__R_Field_6 = pnd_Input_Delete_Rec__R_Field_1.newGroupInGroup("pnd_Input_Delete_Rec__R_Field_6", "REDEFINE", pnd_Input_Delete_Rec_Pnd_Rec_Data_A79);
        pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr = pnd_Input_Delete_Rec__R_Field_6.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr", "#PYMNT-NBR", FieldType.NUMERIC, 
            10);

        pnd_Input_Delete_Rec__R_Field_7 = pnd_Input_Delete_Rec__R_Field_6.newGroupInGroup("pnd_Input_Delete_Rec__R_Field_7", "REDEFINE", pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr);
        pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N3 = pnd_Input_Delete_Rec__R_Field_7.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N3", "#PYMNT-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N7 = pnd_Input_Delete_Rec__R_Field_7.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N7", "#PYMNT-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Input_Delete_Rec_Pnd_Check_Issue_Date = pnd_Input_Delete_Rec__R_Field_6.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Check_Issue_Date", "#CHECK-ISSUE-DATE", 
            FieldType.STRING, 6);
        pnd_Input_Delete_Rec_Pnd_Filler_Zeroes = pnd_Input_Delete_Rec__R_Field_6.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Filler_Zeroes", "#FILLER-ZEROES", 
            FieldType.NUMERIC, 14);
        pnd_Input_Delete_Rec_Pnd_Check_Amount = pnd_Input_Delete_Rec__R_Field_6.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Check_Amount", "#CHECK-AMOUNT", 
            FieldType.NUMERIC, 9, 2);
        pnd_Input_Delete_Rec_Pnd_Accounting_Date = pnd_Input_Delete_Rec__R_Field_6.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Accounting_Date", "#ACCOUNTING-DATE", 
            FieldType.STRING, 6);
        pnd_Input_Delete_Rec_Pnd_Detail_Name = pnd_Input_Delete_Rec__R_Field_6.newFieldInGroup("pnd_Input_Delete_Rec_Pnd_Detail_Name", "#DETAIL-NAME", 
            FieldType.STRING, 34);

        pnd_Output_Delete_Rec = localVariables.newGroupInRecord("pnd_Output_Delete_Rec", "#OUTPUT-DELETE-REC");
        pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D", "#CUSTOMER-ID-NUM-D", 
            FieldType.NUMERIC, 8);
        pnd_Output_Delete_Rec_Pnd_Record_Type = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Output_Delete_Rec_Pnd_Bank_Num = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Bank_Num", "#BANK-NUM", FieldType.NUMERIC, 
            3);
        pnd_Output_Delete_Rec_Pnd_Account_Num = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Account_Num", "#ACCOUNT-NUM", FieldType.NUMERIC, 
            13);
        pnd_Output_Delete_Rec_Pnd_Run_Date = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Run_Date", "#RUN-DATE", FieldType.NUMERIC, 
            8);
        pnd_Output_Delete_Rec_Pnd_Run_Time = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Run_Time", "#RUN-TIME", FieldType.NUMERIC, 
            6);

        pnd_Output_Delete_Rec__R_Field_8 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_8", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Run_Time);
        pnd_Output_Delete_Rec_Pnd_Run_Time_A6 = pnd_Output_Delete_Rec__R_Field_8.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Run_Time_A6", "#RUN-TIME-A6", 
            FieldType.STRING, 6);
        pnd_Output_Delete_Rec_Pnd_Input_Type = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Input_Type", "#INPUT-TYPE", FieldType.STRING, 
            2);
        pnd_Output_Delete_Rec_Pnd_Stop_Type = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Stop_Type", "#STOP-TYPE", FieldType.STRING, 
            1);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num", "#BEGINNING-CHECK-NUM", 
            FieldType.NUMERIC, 11);

        pnd_Output_Delete_Rec__R_Field_9 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_9", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N1 = pnd_Output_Delete_Rec__R_Field_9.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N1", 
            "#BEGINNING-CHECK-NUM-N1", FieldType.NUMERIC, 1);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10 = pnd_Output_Delete_Rec__R_Field_9.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10", 
            "#BEGINNING-CHECK-NUM-N10", FieldType.NUMERIC, 10);

        pnd_Output_Delete_Rec__R_Field_10 = pnd_Output_Delete_Rec__R_Field_9.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_10", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N3 = pnd_Output_Delete_Rec__R_Field_10.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N3", 
            "#BEGINNING-CHECK-NUM-N3", FieldType.NUMERIC, 3);
        pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N7 = pnd_Output_Delete_Rec__R_Field_10.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N7", 
            "#BEGINNING-CHECK-NUM-N7", FieldType.NUMERIC, 7);
        pnd_Output_Delete_Rec_Pnd_Ending_Check_Num = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Ending_Check_Num", "#ENDING-CHECK-NUM", 
            FieldType.NUMERIC, 11);
        pnd_Output_Delete_Rec_Pnd_Check_Amt = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Amt", "#CHECK-AMT", FieldType.NUMERIC, 
            15, 2);

        pnd_Output_Delete_Rec__R_Field_11 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_11", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Check_Amt);
        pnd_Output_Delete_Rec_Pnd_Check_Amt_N5 = pnd_Output_Delete_Rec__R_Field_11.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Amt_N5", "#CHECK-AMT-N5", 
            FieldType.NUMERIC, 5);
        pnd_Output_Delete_Rec_Pnd_Check_Amt_N10 = pnd_Output_Delete_Rec__R_Field_11.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Amt_N10", "#CHECK-AMT-N10", 
            FieldType.NUMERIC, 10, 2);
        pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_N = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_N", "#CHECK-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Output_Delete_Rec__R_Field_12 = pnd_Output_Delete_Rec.newGroupInGroup("pnd_Output_Delete_Rec__R_Field_12", "REDEFINE", pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_N);
        pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A = pnd_Output_Delete_Rec__R_Field_12.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A", 
            "#CHECK-ISSUE-DATE-A", FieldType.STRING, 8);
        pnd_Output_Delete_Rec_Pnd_Payee_Name = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Payee_Name", "#PAYEE-NAME", FieldType.STRING, 
            10);
        pnd_Output_Delete_Rec_Pnd_Reason = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Reason", "#REASON", FieldType.STRING, 10);
        pnd_Output_Delete_Rec_Pnd_Auto_Check_Paid_Inquiry = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Auto_Check_Paid_Inquiry", 
            "#AUTO-CHECK-PAID-INQUIRY", FieldType.STRING, 1);
        pnd_Output_Delete_Rec_Pnd_Stop_Duration_Years = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_Stop_Duration_Years", "#STOP-DURATION-YEARS", 
            FieldType.NUMERIC, 1);
        pnd_Output_Delete_Rec_Pnd_User_Id = pnd_Output_Delete_Rec.newFieldInGroup("pnd_Output_Delete_Rec_Pnd_User_Id", "#USER-ID", FieldType.NUMERIC, 
            6);
        pnd_Wacho_Racash_Bank_Acct_N13 = localVariables.newFieldInRecord("pnd_Wacho_Racash_Bank_Acct_N13", "#WACHO-RACASH-BANK-ACCT-N13", FieldType.NUMERIC, 
            13);
        pnd_Ba_Nbr_1 = localVariables.newFieldInRecord("pnd_Ba_Nbr_1", "#BA-NBR-1", FieldType.NUMERIC, 13);
        pnd_Acct_Total_1 = localVariables.newFieldInRecord("pnd_Acct_Total_1", "#ACCT-TOTAL-1", FieldType.NUMERIC, 12, 2);
        pnd_Acct_Count_1 = localVariables.newFieldInRecord("pnd_Acct_Count_1", "#ACCT-COUNT-1", FieldType.NUMERIC, 5);
        pnd_Ba_Nbr_2 = localVariables.newFieldInRecord("pnd_Ba_Nbr_2", "#BA-NBR-2", FieldType.NUMERIC, 13);
        pnd_Acct_Total_2 = localVariables.newFieldInRecord("pnd_Acct_Total_2", "#ACCT-TOTAL-2", FieldType.NUMERIC, 12, 2);
        pnd_Acct_Count_2 = localVariables.newFieldInRecord("pnd_Acct_Count_2", "#ACCT-COUNT-2", FieldType.NUMERIC, 5);

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnt_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "CNTRCT_CMBN_NBR");
        fcp_Cons_Pymnt_Pymnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        fcp_Cons_Pymnt_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnt_Cntrct_Invrse_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Pymnt_Pymnt_Check_Amt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        fcp_Cons_Pymnt_Pymnt_Check_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_DTE");
        fcp_Cons_Pymnt_Bank_Account = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21, 
            RepeatingFieldStrategy.None, "BANK_ACCOUNT");
        fcp_Cons_Pymnt_Bank_Routing = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9, 
            RepeatingFieldStrategy.None, "BANK_ROUTING");
        fcp_Cons_Pymnt_Pymnt_Source_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Source_Cde", "PYMNT-SOURCE-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "PYMNT_SOURCE_CDE");
        fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        fcp_Cons_Pymnt__R_Field_13 = vw_fcp_Cons_Pymnt.getRecord().newGroupInGroup("fcp_Cons_Pymnt__R_Field_13", "REDEFINE", fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num = fcp_Cons_Pymnt__R_Field_13.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        fcp_Cons_Pymnt_Pymnt_Instmt_Nbr = fcp_Cons_Pymnt__R_Field_13.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 
            2);
        registerRecord(vw_fcp_Cons_Pymnt);

        vw_fcp_Cons_Addr = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Addr", "FCP-CONS-ADDR"), "FCP_CONS_ADDR", "FCP_CONS_LEDGR", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ADDR"));
        fcp_Cons_Addr_Rcrd_Typ = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYP");
        fcp_Cons_Addr_Cntrct_Orgn_Cde = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Addr_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Addr_Cntrct_Payee_Cde = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Addr_Cntrct_Invrse_Dte = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        fcp_Cons_Addr_Ph_Last_Name = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16, 
            RepeatingFieldStrategy.None, "PH_LAST_NAME");
        fcp_Cons_Addr_Ph_First_Name = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "PH_FIRST_NAME");
        fcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp", 
            "C*PYMNT-NME-AND-ADDR-GRP", RepeatingFieldStrategy.CAsteriskVariable, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");

        fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp = vw_fcp_Cons_Addr.getRecord().newGroupInGroup("fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_Pymnt_Nme = fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_NME", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_Pymnt_Addr_Line1_Txt = fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE1_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_Pymnt_Addr_Line2_Txt = fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE2_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        registerRecord(vw_fcp_Cons_Addr);

        pnd_Addr_Key = localVariables.newFieldInRecord("pnd_Addr_Key", "#ADDR-KEY", FieldType.STRING, 31);

        pnd_Addr_Key__R_Field_14 = localVariables.newGroupInRecord("pnd_Addr_Key__R_Field_14", "REDEFINE", pnd_Addr_Key);
        pnd_Addr_Key_Pnd_Addr_Ppcn = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Ppcn", "#ADDR-PPCN", FieldType.STRING, 10);
        pnd_Addr_Key_Pnd_Addr_Cde = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Cde", "#ADDR-CDE", FieldType.STRING, 4);
        pnd_Addr_Key_Pnd_Addr_Orgn = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Orgn", "#ADDR-ORGN", FieldType.STRING, 2);
        pnd_Addr_Key_Pnd_Addr_Invdt = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Invdt", "#ADDR-INVDT", FieldType.NUMERIC, 8);
        pnd_Addr_Key_Pnd_Addr_Seq = pnd_Addr_Key__R_Field_14.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Seq", "#ADDR-SEQ", FieldType.NUMERIC, 7);
        pnd_Cmbne_Nbr = localVariables.newFieldInRecord("pnd_Cmbne_Nbr", "#CMBNE-NBR", FieldType.STRING, 14);

        pnd_Cmbne_Nbr__R_Field_15 = localVariables.newGroupInRecord("pnd_Cmbne_Nbr__R_Field_15", "REDEFINE", pnd_Cmbne_Nbr);
        pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn = pnd_Cmbne_Nbr__R_Field_15.newFieldInGroup("pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn", "#CMBNE-PPCN", FieldType.STRING, 10);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Total_Write_Cnt = localVariables.newFieldInRecord("pnd_Total_Write_Cnt", "#TOTAL-WRITE-CNT", FieldType.NUMERIC, 7);
        pnd_Total_Amount = localVariables.newFieldInRecord("pnd_Total_Amount", "#TOTAL-AMOUNT", FieldType.NUMERIC, 12, 2);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);
        pnd_Date_Mmddyyyy = localVariables.newFieldInRecord("pnd_Date_Mmddyyyy", "#DATE-MMDDYYYY", FieldType.STRING, 10);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.NUMERIC, 7);

        pnd_Time__R_Field_16 = localVariables.newGroupInRecord("pnd_Time__R_Field_16", "REDEFINE", pnd_Time);
        pnd_Time_Pnd_Time_N6 = pnd_Time__R_Field_16.newFieldInGroup("pnd_Time_Pnd_Time_N6", "#TIME-N6", FieldType.NUMERIC, 6);
        pnd_Rpt_Cnt = localVariables.newFieldInRecord("pnd_Rpt_Cnt", "#RPT-CNT", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();
        vw_fcp_Cons_Addr.reset();

        localVariables.reset();
        pnd_Output_Delete_Rec_Pnd_Customer_Id_Num_D.setInitialValue(13241);
        pnd_Output_Delete_Rec_Pnd_Record_Type.setInitialValue("D");
        pnd_Output_Delete_Rec_Pnd_Bank_Num.setInitialValue(75);
        pnd_Output_Delete_Rec_Pnd_Input_Type.setInitialValue("BS");
        pnd_Output_Delete_Rec_Pnd_Stop_Type.setInitialValue("E");
        pnd_Output_Delete_Rec_Pnd_Payee_Name.setInitialValue(" ");
        pnd_Output_Delete_Rec_Pnd_Reason.setInitialValue(" ");
        pnd_Output_Delete_Rec_Pnd_Auto_Check_Paid_Inquiry.setInitialValue("Y");
        pnd_Output_Delete_Rec_Pnd_Stop_Duration_Years.setInitialValue(2);
        pnd_Output_Delete_Rec_Pnd_User_Id.setInitialValue(48087);
        pnd_Wacho_Racash_Bank_Acct_N13.setInitialValue(-2147483648);
        pnd_Ba_Nbr_1.setInitialValue(-2147483648);
        pnd_Ba_Nbr_2.setInitialValue(-2147483648);
        pnd_Header0_1.setInitialValue("  Wachovia Deleted Checks Daily Report    ");
        pnd_Header0_2.setInitialValue("for Accounts 2079950055011 & 2079950060725");
        pnd_Rpt_Cnt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp196v() throws Exception
    {
        super("Fcpp196v");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 55 ZP = ON IS = OFF ES = OFF SG = OFF
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ****************************************************
        //* * WACHOVIA ACCOUNTS 2079950055011 & 2079950060725 **
        //* ****************************************************
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-ACCOUNT-HEADER
        sub_Display_Report_Account_Header();
        if (condition(Global.isEscape())) {return;}
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT-DELETE-REC
        while (condition(getWorkFiles().read(1, pnd_Input_Delete_Rec)))
        {
            if (condition(! (pnd_Input_Delete_Rec_Pnd_Rec_Type_A1.equals("1") || pnd_Input_Delete_Rec_Pnd_Rec_Type_A1.equals("2"))))                                      //Natural: IF NOT ( #INPUT-DELETE-REC.#REC-TYPE-A1 = '1' OR = '2' )
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Delete_Rec_Pnd_Rec_Type_A1.equals("1")))                                                                                              //Natural: IF #INPUT-DELETE-REC.#REC-TYPE-A1 = '1'
            {
                if (condition(pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_A13.greater(" ")))                                                                                   //Natural: IF #HDR-BANK-ACCT-A13 GT ' '
                {
                    pnd_Output_Delete_Rec_Pnd_Account_Num.setValue(pnd_Input_Delete_Rec_Pnd_Hdr_Bank_Acct_N13);                                                           //Natural: ASSIGN #OUTPUT-DELETE-REC.#ACCOUNT-NUM := #HDR-BANK-ACCT-N13
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Delete_Rec_Pnd_Account_Num.setValue(pnd_Wacho_Racash_Bank_Acct_N13);                                                                       //Natural: ASSIGN #OUTPUT-DELETE-REC.#ACCOUNT-NUM := #WACHO-RACASH-BANK-ACCT-N13
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Delete_Rec_Pnd_Run_Date.setValue(Global.getDATN());                                                                                                //Natural: MOVE *DATN TO #RUN-DATE
            pnd_Time.setValue(Global.getTIMN());                                                                                                                          //Natural: MOVE *TIMN TO #TIME
            pnd_Output_Delete_Rec_Pnd_Run_Time.setValue(pnd_Time_Pnd_Time_N6);                                                                                            //Natural: MOVE #TIME-N6 TO #RUN-TIME
            pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N1.setValue(0);                                                                                                 //Natural: ASSIGN #BEGINNING-CHECK-NUM-N1 := 0
            pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10.setValue(pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr);                                                               //Natural: ASSIGN #BEGINNING-CHECK-NUM-N10 := #PYMNT-NBR
            pnd_Output_Delete_Rec_Pnd_Ending_Check_Num.setValue(pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num);                                                           //Natural: ASSIGN #ENDING-CHECK-NUM := #BEGINNING-CHECK-NUM
                                                                                                                                                                          //Natural: PERFORM READ-196-FCP-CON-PYMNT
            sub_Read_196_Fcp_Con_Pymnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Output_Delete_Rec_Pnd_Check_Amt_N10.setValue(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                             //Natural: ASSIGN #CHECK-AMT-N10 := PYMNT-CHECK-AMT
            if (condition(pnd_Output_Delete_Rec_Pnd_Account_Num.equals(pnd_Ba_Nbr_1)))                                                                                    //Natural: IF #OUTPUT-DELETE-REC.#ACCOUNT-NUM = #BA-NBR-1
            {
                pnd_Acct_Total_1.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                                    //Natural: COMPUTE #ACCT-TOTAL-1 = #ACCT-TOTAL-1 + PYMNT-CHECK-AMT
                pnd_Acct_Count_1.nadd(1);                                                                                                                                 //Natural: COMPUTE #ACCT-COUNT-1 = #ACCT-COUNT-1 + 1
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Output_Delete_Rec_Pnd_Account_Num.equals(pnd_Ba_Nbr_2)))                                                                                    //Natural: IF #OUTPUT-DELETE-REC.#ACCOUNT-NUM = #BA-NBR-2
            {
                pnd_Acct_Total_2.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                                    //Natural: COMPUTE #ACCT-TOTAL-2 = #ACCT-TOTAL-2 + PYMNT-CHECK-AMT
                pnd_Acct_Count_2.nadd(1);                                                                                                                                 //Natural: COMPUTE #ACCT-COUNT-2 = #ACCT-COUNT-2 + 1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A.setValueEdited(fcp_Cons_Pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                   //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-ISSUE-DATE-A
            pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Output_Delete_Rec_Pnd_Check_Issue_Date_A);                                                            //Natural: MOVE EDITED #CHECK-ISSUE-DATE-A TO #D ( EM = YYYYMMDD )
            pnd_Date_Mmddyyyy.setValueEdited(pnd_D,new ReportEditMask("MM/DD/YYYY"));                                                                                     //Natural: MOVE EDITED #D ( EM = MM/DD/YYYY ) TO #DATE-MMDDYYYY
            if (condition(fcp_Cons_Pymnt_Cntrct_Orgn_Cde.equals("IA") || fcp_Cons_Pymnt_Cntrct_Orgn_Cde.equals("AP")))                                                    //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'IA' OR = 'AP'
            {
                pnd_Cmbne_Nbr.setValue(fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr);                                                                                                   //Natural: ASSIGN #CMBNE-NBR := FCP-CONS-PYMNT.CNTRCT-CMBN-NBR
                pnd_Addr_Key_Pnd_Addr_Ppcn.setValue(pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn);                                                                                        //Natural: ASSIGN #ADDR-PPCN := #CMBNE-PPCN
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Addr_Key_Pnd_Addr_Ppcn.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                                      //Natural: ASSIGN #ADDR-PPCN := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Addr_Key_Pnd_Addr_Ppcn.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                                          //Natural: ASSIGN #ADDR-PPCN := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
            pnd_Addr_Key_Pnd_Addr_Cde.setValue(fcp_Cons_Pymnt_Cntrct_Payee_Cde);                                                                                          //Natural: ASSIGN #ADDR-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
            pnd_Addr_Key_Pnd_Addr_Orgn.setValue(fcp_Cons_Pymnt_Cntrct_Orgn_Cde);                                                                                          //Natural: ASSIGN #ADDR-ORGN := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
            pnd_Addr_Key_Pnd_Addr_Invdt.setValue(fcp_Cons_Pymnt_Cntrct_Invrse_Dte);                                                                                       //Natural: ASSIGN #ADDR-INVDT := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
            pnd_Addr_Key_Pnd_Addr_Seq.setValue(fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num);                                                                                       //Natural: ASSIGN #ADDR-SEQ := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
            vw_fcp_Cons_Addr.startDatabaseRead                                                                                                                            //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #ADDR-KEY
            (
            "READ_ADR",
            new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Addr_Key, WcType.BY) },
            new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
            1
            );
            READ_ADR:
            while (condition(vw_fcp_Cons_Addr.readNextRow("READ_ADR")))
            {
                if (condition((((((fcp_Cons_Addr_Cntrct_Orgn_Cde.equals("IA") || fcp_Cons_Addr_Cntrct_Orgn_Cde.equals("AP")) && pnd_Addr_Key_Pnd_Addr_Ppcn.equals(pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn))  //Natural: IF ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE = 'IA' OR = 'AP' AND #ADDR-PPCN = #CMBNE-PPCN AND #ADDR-CDE = FCP-CONS-ADDR.CNTRCT-PAYEE-CDE AND #ADDR-ORGN = FCP-CONS-ADDR.CNTRCT-ORGN-CDE ) OR ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE NE 'IA' AND FCP-CONS-ADDR.CNTRCT-ORGN-CDE NE 'AP' AND #ADDR-ORGN = FCP-CONS-ADDR.CNTRCT-ORGN-CDE AND #ADDR-INVDT = FCP-CONS-ADDR.CNTRCT-INVRSE-DTE AND #ADDR-SEQ = FCP-CONS-ADDR.PYMNT-PRCSS-SEQ-NBR )
                    && pnd_Addr_Key_Pnd_Addr_Cde.equals(fcp_Cons_Addr_Cntrct_Payee_Cde)) && pnd_Addr_Key_Pnd_Addr_Orgn.equals(fcp_Cons_Addr_Cntrct_Orgn_Cde)) 
                    || ((((fcp_Cons_Addr_Cntrct_Orgn_Cde.notEquals("IA") && fcp_Cons_Addr_Cntrct_Orgn_Cde.notEquals("AP")) && pnd_Addr_Key_Pnd_Addr_Orgn.equals(fcp_Cons_Addr_Cntrct_Orgn_Cde)) 
                    && pnd_Addr_Key_Pnd_Addr_Invdt.equals(fcp_Cons_Addr_Cntrct_Invrse_Dte)) && pnd_Addr_Key_Pnd_Addr_Seq.equals(fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr)))))
                {
                    if (condition(fcp_Cons_Addr_Pymnt_Nme.getValue(1).getSubstring(1,3).equals("CR ")))                                                                   //Natural: IF SUBSTRING ( PYMNT-NME ( 1 ) ,1,3 ) EQ 'CR '
                    {
                        //*          AND #OUTPUT-VOID-REC.#PAYEE-NME NE ' '
                        pnd_Output_Delete_Rec_Pnd_Payee_Name.setValue(fcp_Cons_Addr_Pymnt_Nme.getValue(2));                                                               //Natural: ASSIGN #OUTPUT-DELETE-REC.#PAYEE-NAME := PYMNT-NME ( 2 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Output_Delete_Rec_Pnd_Payee_Name.setValue(fcp_Cons_Addr_Pymnt_Nme.getValue(1));                                                               //Natural: ASSIGN #OUTPUT-DELETE-REC.#PAYEE-NAME := PYMNT-NME ( 1 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    fcp_Cons_Addr_Pymnt_Nme.getValue(1).setValue("***** UNKNOWN *****");                                                                                  //Natural: ASSIGN FCP-CONS-ADDR.PYMNT-NME ( 1 ) := '***** UNKNOWN *****'
                    fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(1).reset();                                                                                               //Natural: RESET FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( 2 )
                    fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(2).reset();
                }                                                                                                                                                         //Natural: END-IF
                //*  (READ-ADR.)
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, false, pnd_Output_Delete_Rec);                                                                                                        //Natural: WRITE WORK FILE 2 #OUTPUT-DELETE-REC
            pnd_Total_Write_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #TOTAL-WRITE-CNT
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-DETAIL
            sub_Display_Report_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-ACCOUNT-TRAILER
        sub_Display_Report_Account_Trailer();
        if (condition(Global.isEscape())) {return;}
        pnd_Total_Amount.compute(new ComputeParameters(false, pnd_Total_Amount), pnd_Acct_Total_1.add(pnd_Acct_Total_2));                                                 //Natural: COMPUTE #TOTAL-AMOUNT = #ACCT-TOTAL-1 + #ACCT-TOTAL-2
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"    CONTROL TOTALS    ",NEWLINE,"  COUNT         AMOUNT",new ColumnSpacing(7),NEWLINE,pnd_Total_Write_Cnt,  //Natural: WRITE ( 1 ) // '    CONTROL TOTALS    ' / '  COUNT         AMOUNT' 7X / #TOTAL-WRITE-CNT ( EM = ZZZ,ZZ9 ) 3X #TOTAL-AMOUNT ( EM = ZZZ,ZZZ,ZZZ.99 ) // 'Account 2079950055011 Record Count:' #ACCT-COUNT-1 ( EM = ZZZ,ZZ9 ) 'Total Amount:' #ACCT-TOTAL-1 ( EM = ZZZ,ZZZ,ZZZ.99 ) / 'Account 2079950060725 Record Count:' #ACCT-COUNT-2 ( EM = ZZZ,ZZ9 ) 'Total Amount:' #ACCT-TOTAL-2 ( EM = ZZZ,ZZZ,ZZZ.99 ) /
            new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(3),pnd_Total_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,"Account 2079950055011 Record Count:",pnd_Acct_Count_1, 
            new ReportEditMask ("ZZ,ZZ9"),"Total Amount:",pnd_Acct_Total_1, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE,"Account 2079950060725 Record Count:",pnd_Acct_Count_2, 
            new ReportEditMask ("ZZ,ZZ9"),"Total Amount:",pnd_Acct_Total_2, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,Global.getPROGRAM(),"END OF RUN AT TIME:",Global.getTIME(),NEWLINE);                           //Natural: WRITE ( 1 ) /// *PROGRAM 'END OF RUN AT TIME:' *TIME /
        if (Global.isEscape()) return;
        //*  -------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-196-FCP-CON-PYMNT
        //*  ------------------------------ *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BANK-TABLE
        //*  -------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-ACCOUNT-HEADER
        //* *2X   'Bank Name: Wachovia Bank N.A.'
        //*  ------------------------------------ *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-DETAIL
        //*  --------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-ACCOUNT-TRAILER
    }
    private void sub_Read_196_Fcp_Con_Pymnt() throws Exception                                                                                                            //Natural: READ-196-FCP-CON-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------- *
        if (condition(pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N3.equals(getZero()) && pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N7.greater(getZero())))                               //Natural: IF #PYMNT-NBR-N3 EQ 0 AND #PYMNT-NBR-N7 GT 0
        {
            vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) FCP-CONS-PYMNT WITH PYMNT-CHECK-NBR = #PYMNT-NBR-N7
            (
            "FIND_PYMNT_N7",
            new Wc[] { new Wc("PYMNT_CHECK_NBR", "=", pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N7, WcType.WITH) },
            1
            );
            FIND_PYMNT_N7:
            while (condition(vw_fcp_Cons_Pymnt.readNextRow("FIND_PYMNT_N7", true)))
            {
                vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
                if (condition(vw_fcp_Cons_Pymnt.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO PYMNT REC (196) FOUND","=",pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr_N7);                                                          //Natural: WRITE 'NO PYMNT REC (196) FOUND' '=' #PYMNT-NBR-N7
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FIND_PYMNT_N7"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FIND_PYMNT_N7"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    fcp_Cons_Pymnt_Cntrct_Payee_Cde.reset();                                                                                                              //Natural: RESET FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*  (FIND-PYMNT-N7.)
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  N10
            vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) FCP-CONS-PYMNT WITH PYMNT-NBR = #PYMNT-NBR
            (
            "FIND_PYMNT",
            new Wc[] { new Wc("PYMNT_NBR", "=", pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr, WcType.WITH) },
            1
            );
            FIND_PYMNT:
            while (condition(vw_fcp_Cons_Pymnt.readNextRow("FIND_PYMNT", true)))
            {
                vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
                if (condition(vw_fcp_Cons_Pymnt.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO PYMNT REC (196) FOUND","=",pnd_Input_Delete_Rec_Pnd_Pymnt_Nbr);                                                             //Natural: WRITE 'NO PYMNT REC (196) FOUND' '=' #PYMNT-NBR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FIND_PYMNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FIND_PYMNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    fcp_Cons_Pymnt_Cntrct_Payee_Cde.reset();                                                                                                              //Natural: RESET FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*  (FIND-PYMNT.)
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ-196-FCP-CON-PYMNT
    }
    private void sub_Get_Bank_Table() throws Exception                                                                                                                    //Natural: GET-BANK-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------ *
        pdaCpsa110.getCpsa110().reset();                                                                                                                                  //Natural: RESET CPSA110
        pdaCpsa110.getCpsa110_Cpsa110_Function().setValue("               ");                                                                                             //Natural: MOVE '               ' TO CPSA110.CPSA110-FUNCTION
        pdaCpsa110.getCpsa110_Cpsa110_Source_Code().setValue(fcp_Cons_Pymnt_Pymnt_Source_Cde);                                                                            //Natural: MOVE FCP-CONS-PYMNT.PYMNT-SOURCE-CDE TO CPSA110.CPSA110-SOURCE-CODE
        DbsUtil.callnat(Cpsn110.class , getCurrentProcessState(), pdaCpsa110.getCpsa110());                                                                               //Natural: CALLNAT 'CPSN110' CPSA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpsa110.getCpsa110_Cpsa110_Return_Code().notEquals("00")))                                                                                       //Natural: IF CPSA110.CPSA110-RETURN-CODE NE '00'
        {
            pdaCpsa110.getCpsa110_Bank_Routing().setValue("031100225");                                                                                                   //Natural: MOVE '031100225' TO CPSA110.BANK-ROUTING
            pdaCpsa110.getCpsa110_Bank_Account_Name().setValue("Wachovia Bank N.A.");                                                                                     //Natural: MOVE 'Wachovia Bank N.A.' TO CPSA110.BANK-ACCOUNT-NAME
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-BANK-TABLE
    }
    private void sub_Display_Report_Account_Header() throws Exception                                                                                                     //Natural: DISPLAY-REPORT-ACCOUNT-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------- *
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getINIT_USER(),new TabSetting(33),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(100),"PAGE:",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE 001T *INIT-USER 33T 'CONSOLIDATED PAYMENT SYSTEM' 100T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *PROGRAM 027T #HEADER0-1 100T *DATU / 026T #HEADER0-2 100T *TIMX ( EM = HH:IIAP ) // 001T 'Bank Route:' CPSA110.BANK-ROUTING 2X 'Bank Name:' CPSA110.BANK-ACCOUNT-NAME ( AL = 20 )
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(27),pnd_Header0_1,new TabSetting(100),Global.getDATU(),NEWLINE,new 
            TabSetting(26),pnd_Header0_2,new TabSetting(100),Global.getTIMX(), new ReportEditMask ("HH:IIAP"),NEWLINE,NEWLINE,new TabSetting(1),"Bank Route:",pdaCpsa110.getCpsa110_Bank_Routing(),new 
            ColumnSpacing(2),"Bank Name:",pdaCpsa110.getCpsa110_Bank_Account_Name(), new AlphanumericLength (20));
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2 LINES
        //* (N13)
        //* (N10)
        //* (8.2)
        //* (A10)  MM/DD/YYYY
        //* (A8)
        //* (A2)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),"Account",new TabSetting(28),"Check",new TabSetting(43),"Check",new TabSetting(52),"Check",new      //Natural: WRITE ( 1 ) 013T 'Account' 028T 'Check' 043T 'Check' 052T 'Check' 063T 'PPCN' 073T 'Pay' 082T 'Payee'
            TabSetting(63),"PPCN",new TabSetting(73),"Pay",new TabSetting(82),"Payee");
        if (Global.isEscape()) return;
        //* (N13)
        //* (N10)
        //* (8.2)
        //* (A10)  MM/DD/YYYY
        //* (A8)
        //* (A2)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),"Number",new TabSetting(27),"Number",new TabSetting(42),"Amount",new TabSetting(52),"Date",new      //Natural: WRITE ( 1 ) 013T 'Number' 027T 'Number' 042T 'Amount' 052T 'Date' 062T 'Number' 073T 'Cde' 082T 'Name'
            TabSetting(62),"Number",new TabSetting(73),"Cde",new TabSetting(82),"Name");
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-HEADER
    }
    private void sub_Display_Report_Detail() throws Exception                                                                                                             //Natural: DISPLAY-REPORT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------ *
        pnd_Rpt_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #RPT-CNT
        //* (N05)
        //* (N13)
        //*  N8.2
        //* (A10) MMDDYYYY
        //* (A8)
        //* (A2)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Rpt_Cnt, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(10),pnd_Output_Delete_Rec_Pnd_Account_Num,new  //Natural: WRITE ( 1 ) 003T #RPT-CNT ( EM = ZZ,ZZ9 ) 010T #OUTPUT-DELETE-REC.#ACCOUNT-NUM 025T #OUTPUT-DELETE-REC.#BEGINNING-CHECK-NUM-N10 036T #OUTPUT-DELETE-REC.#CHECK-AMT-N10 ( EM = ZZ,ZZZ,ZZZ.99 ) 050T #DATE-MMDDYYYY 061T FCP-CONS-PYMNT.CNTRCT-PPCN-NBR 073T FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE 082T #OUTPUT-DELETE-REC.#PAYEE-NAME
            TabSetting(25),pnd_Output_Delete_Rec_Pnd_Beginning_Check_Num_N10,new TabSetting(36),pnd_Output_Delete_Rec_Pnd_Check_Amt_N10, new ReportEditMask 
            ("ZZ,ZZZ,ZZZ.99"),new TabSetting(50),pnd_Date_Mmddyyyy,new TabSetting(61),fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr,new TabSetting(73),fcp_Cons_Pymnt_Cntrct_Payee_Cde,new 
            TabSetting(82),pnd_Output_Delete_Rec_Pnd_Payee_Name);
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-DETAIL
    }
    private void sub_Display_Report_Account_Trailer() throws Exception                                                                                                    //Natural: DISPLAY-REPORT-ACCOUNT-TRAILER
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        //*  N8.2
        //*  N8.2
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(12),"Account 2079950055011 Total Payments:",pnd_Acct_Total_1, new ReportEditMask        //Natural: WRITE ( 1 ) / / 012T 'Account 2079950055011 Total Payments:' #ACCT-TOTAL-1 ( EM = ZZ,ZZZ,ZZZ.99 ) / 012T 'Account 2079950060725 Total Payments:' #ACCT-TOTAL-2 ( EM = ZZ,ZZZ,ZZZ.99 )
            ("ZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(12),"Account 2079950060725 Total Payments:",pnd_Acct_Total_2, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-ACCOUNT-TRAILER
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=55 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
