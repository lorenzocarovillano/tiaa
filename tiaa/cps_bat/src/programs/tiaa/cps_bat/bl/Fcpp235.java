/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:30 PM
**        * FROM NATURAL PROGRAM : Fcpp235
************************************************************
**        * FILE NAME            : Fcpp235.java
**        * CLASS NAME           : Fcpp235
**        * INSTANCE NAME        : Fcpp235
************************************************************
************************************************************************
* PROGRAM  : FCPP235
* SYSTEM   : CPS
* AUTHOR   : LEON SILBERSTEIN
* DATE     : MAR 1995
* FUNCTION : THIS PROGRAM RESETS ON THE PAYMENT FILE ON THE CPS
*            DATABASE, ALL THE STOP PAYMENTS, THAT HAVE BEEN EXTRACTED
*            BUT NOT YET RESET.
*
** RL MAY 23,2006 PAYEE MATCH - PROCESS 7 & 10 DIGIT CHKS - 1400 & 2200
*
* *********************************************************************
* PROGRAM DESCRIPTION:
* *********************************************************************
* THIS PROGRAM RESETS ON THE PAYMENT FILE ON THE CPS
* DATABASE, ALL THE STOP PAYMENTS, THAT HAVE BEEN EXTRACTED IN
* PROGRAM FCPP233, (AND MARKED AS "I").
*
* ----------------------------------------------------------------------
* PROGRAMMED ABENDS:
* ----------------------------------------------------------------------
*
*  CONDITION CODE 33:
*      NO RECORD FOUND ON THE CONTROL FILE (WORK FILE 2)!
*
*  CONDITION CODE 34:
*      THE NUMBER OR THE AMOUNT OF THE EXTRACTED STOPS IN PROG FCPP234
*      DOES NOT MATCH THE NUMBER OR AMOUNT RESET BY THIS PROGRAM!
* ----------------------------------------------------------------------
* RESTART:
*            RESTART PROCEDURE FOR THIS PROGRAM IS RERUN.
*
* ----------------------------------------------------------------------
*
* HISTORY:
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp235 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Stats_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr;

    private DbsGroup fcp_Cons_Pymnt__R_Field_1;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr_N3;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr_N7;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Amt;
    private DbsField fcp_Cons_Pymnt_Pymnt_Acctg_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Intrfce_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Rqust_Term_Id;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind;
    private DbsField pnd_Force_Et_Count;
    private DbsField pnd_Program;
    private DbsField pnd_Abend_Cde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Cnt_Recs_Marked_As_Y;
    private DbsField pnd_Ws_Pnd_Cnt_Recs_Marked_As_I;
    private DbsField pnd_Ws_Pnd_Cnt_Recs_Marked_As_Other;
    private DbsField pnd_Ws_Pnd_Amt_Recs_Marked_As_Y;
    private DbsField pnd_Ws_Pnd_Amt_Recs_Marked_As_I;
    private DbsField pnd_Ws_Pnd_Amt_Recs_Marked_As_Other;
    private DbsField pnd_Ws_Pnd_Et_Cnt;

    private DbsGroup cntlrec;
    private DbsField cntlrec_Cntlrec_Full;

    private DbsGroup cntlrec__R_Field_2;
    private DbsField cntlrec_Create_Date;
    private DbsField cntlrec_Create_Time;
    private DbsField cntlrec_Pnd_Ws_Number_Of_Stopped_Payments;
    private DbsField cntlrec_Pnd_Ws_Stop_Amount;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnt_Pymnt_Stats_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_STATS_CDE");
        fcp_Cons_Pymnt_Pymnt_Check_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_DTE");
        fcp_Cons_Pymnt_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnt_Pymnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");

        fcp_Cons_Pymnt__R_Field_1 = vw_fcp_Cons_Pymnt.getRecord().newGroupInGroup("fcp_Cons_Pymnt__R_Field_1", "REDEFINE", fcp_Cons_Pymnt_Pymnt_Nbr);
        fcp_Cons_Pymnt_Pymnt_Nbr_N3 = fcp_Cons_Pymnt__R_Field_1.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr_N3", "PYMNT-NBR-N3", FieldType.NUMERIC, 3);
        fcp_Cons_Pymnt_Pymnt_Nbr_N7 = fcp_Cons_Pymnt__R_Field_1.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr_N7", "PYMNT-NBR-N7", FieldType.NUMERIC, 7);
        fcp_Cons_Pymnt_Pymnt_Check_Amt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        fcp_Cons_Pymnt_Pymnt_Acctg_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_ACCTG_DTE");
        fcp_Cons_Pymnt_Pymnt_Intrfce_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_INTRFCE_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_RQUST_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "CNR_CS_RQUST_TME");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_USER_ID");
        fcp_Cons_Pymnt_Cnr_Cs_Rqust_Term_Id = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Rqust_Term_Id", "CNR-CS-RQUST-TERM-ID", 
            FieldType.STRING, 8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_TERM_ID");
        fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNR_CS_ACTVTY_CDE");
        fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte", "CNR-CS-PYMNT-INTRFCE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_INTRFCE_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_ACCTG_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Check_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_CHECK_DTE");
        fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNR_CS_NEW_STOP_IND");
        registerRecord(vw_fcp_Cons_Pymnt);

        pnd_Force_Et_Count = localVariables.newFieldInRecord("pnd_Force_Et_Count", "#FORCE-ET-COUNT", FieldType.NUMERIC, 5);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Cnt_Recs_Marked_As_Y = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cnt_Recs_Marked_As_Y", "#CNT-RECS-MARKED-AS-Y", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Ws_Pnd_Cnt_Recs_Marked_As_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cnt_Recs_Marked_As_I", "#CNT-RECS-MARKED-AS-I", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Ws_Pnd_Cnt_Recs_Marked_As_Other = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cnt_Recs_Marked_As_Other", "#CNT-RECS-MARKED-AS-OTHER", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Ws_Pnd_Amt_Recs_Marked_As_Y = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Recs_Marked_As_Y", "#AMT-RECS-MARKED-AS-Y", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Amt_Recs_Marked_As_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Recs_Marked_As_I", "#AMT-RECS-MARKED-AS-I", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Amt_Recs_Marked_As_Other = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt_Recs_Marked_As_Other", "#AMT-RECS-MARKED-AS-OTHER", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Et_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 5);

        cntlrec = localVariables.newGroupInRecord("cntlrec", "CNTLREC");
        cntlrec_Cntlrec_Full = cntlrec.newFieldInGroup("cntlrec_Cntlrec_Full", "CNTLREC-FULL", FieldType.STRING, 80);

        cntlrec__R_Field_2 = localVariables.newGroupInRecord("cntlrec__R_Field_2", "REDEFINE", cntlrec);
        cntlrec_Create_Date = cntlrec__R_Field_2.newFieldInGroup("cntlrec_Create_Date", "CREATE-DATE", FieldType.STRING, 8);
        cntlrec_Create_Time = cntlrec__R_Field_2.newFieldInGroup("cntlrec_Create_Time", "CREATE-TIME", FieldType.STRING, 10);
        cntlrec_Pnd_Ws_Number_Of_Stopped_Payments = cntlrec__R_Field_2.newFieldInGroup("cntlrec_Pnd_Ws_Number_Of_Stopped_Payments", "#WS-NUMBER-OF-STOPPED-PAYMENTS", 
            FieldType.NUMERIC, 7);
        cntlrec_Pnd_Ws_Stop_Amount = cntlrec__R_Field_2.newFieldInGroup("cntlrec_Pnd_Ws_Stop_Amount", "#WS-STOP-AMOUNT", FieldType.NUMERIC, 15, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();

        localVariables.reset();
        pnd_Force_Et_Count.setInitialValue(100);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp235() throws Exception
    {
        super("Fcpp235");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  .............. ENVIRONMENTAL INITIALIZATIONS
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //*  SELECT ALL THE STOPS MARKED AS "I" AND RESET THEM
        //*  RL MAY 15, 2006
        vw_fcp_Cons_Pymnt.startDatabaseRead                                                                                                                               //Natural: READ FCP-CONS-PYMNT BY CHECK-NBR-STOP-IND
        (
        "READ01",
        new Oc[] { new Oc("CHECK_NBR_STOP_IND", "ASC") }
        );
        READ01:
        while (condition(vw_fcp_Cons_Pymnt.readNextRow("READ01")))
        {
            //* ************************ RL END MAY 15 2006 **************************
            short decideConditionsMet108 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN FCP-CONS-PYMNT.PYMNT-NBR-N7 GT 0 AND FCP-CONS-PYMNT.PYMNT-NBR-N3 = 170 OR = 220
            if (condition((fcp_Cons_Pymnt_Pymnt_Nbr_N7.greater(getZero()) && (fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(170) || fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(220)))))
            {
                decideConditionsMet108++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN FCP-CONS-PYMNT.PYMNT-CHECK-NBR GT 0 AND FCP-CONS-PYMNT.PYMNT-NBR-N7 EQ 0
            else if (condition(fcp_Cons_Pymnt_Pymnt_Check_Nbr.greater(getZero()) && fcp_Cons_Pymnt_Pymnt_Nbr_N7.equals(getZero())))
            {
                decideConditionsMet108++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN FCP-CONS-PYMNT.PYMNT-NBR-N7 GT 0 AND FCP-CONS-PYMNT.PYMNT-NBR-N3 GT 0 AND NOT ( FCP-CONS-PYMNT.PYMNT-NBR-N3 = 170 OR = 220 )
            else if (condition(((fcp_Cons_Pymnt_Pymnt_Nbr_N7.greater(getZero()) && fcp_Cons_Pymnt_Pymnt_Nbr_N3.greater(getZero())) && ! ((fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(170) 
                || fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(220))))))
            {
                decideConditionsMet108++;
                //*  N7
                //*  N10
                getReports().write(0, "*** 10 DIGIT FROM ANOTHER SETTTLEMNT SYSTEM  ***",NEWLINE,"=",fcp_Cons_Pymnt_Pymnt_Check_Nbr,NEWLINE,"=",fcp_Cons_Pymnt_Pymnt_Nbr, //Natural: WRITE '*** 10 DIGIT FROM ANOTHER SETTTLEMNT SYSTEM  ***' / '=' FCP-CONS-PYMNT.PYMNT-CHECK-NBR / '=' FCP-CONS-PYMNT.PYMNT-NBR /
                    NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet108 > 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                //*  N7
                //*  N10
                getReports().write(0, "*** CHECK NUMBER MISSING OR INVALID ***",NEWLINE,"=",fcp_Cons_Pymnt_Pymnt_Check_Nbr,NEWLINE,"=",fcp_Cons_Pymnt_Pymnt_Nbr,          //Natural: WRITE '*** CHECK NUMBER MISSING OR INVALID ***' / '=' FCP-CONS-PYMNT.PYMNT-CHECK-NBR / '=' FCP-CONS-PYMNT.PYMNT-NBR /
                    NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-DECIDE
            //* ************************ RL END MAY 15 2006 **************************
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.CNR-CS-NEW-STOP-IND
            short decideConditionsMet129 = 0;                                                                                                                             //Natural: VALUE 'Y'
            if (condition((fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.equals("Y"))))
            {
                decideConditionsMet129++;
                pnd_Ws_Pnd_Cnt_Recs_Marked_As_Y.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-Y
                pnd_Ws_Pnd_Amt_Recs_Marked_As_Y.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                     //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO #AMT-RECS-MARKED-AS-Y
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.equals("I"))))
            {
                decideConditionsMet129++;
                pnd_Ws_Pnd_Cnt_Recs_Marked_As_I.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-I
                pnd_Ws_Pnd_Amt_Recs_Marked_As_I.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                     //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO #AMT-RECS-MARKED-AS-I
                //*  ..... RESET THE STOP-IND FOR RECORDS WITH INTERMIDIATE VALUE "I"
                fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.reset();                                                                                                               //Natural: RESET FCP-CONS-PYMNT.CNR-CS-NEW-STOP-IND
                vw_fcp_Cons_Pymnt.updateDBRow("READ01");                                                                                                                  //Natural: UPDATE
                pnd_Ws_Pnd_Et_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ET-CNT
                if (condition(pnd_Ws_Pnd_Et_Cnt.greaterOrEqual(pnd_Force_Et_Count)))                                                                                      //Natural: IF #ET-CNT GE #FORCE-ET-COUNT
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Ws_Pnd_Et_Cnt.reset();                                                                                                                            //Natural: RESET #ET-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Ws_Pnd_Cnt_Recs_Marked_As_Other.nadd(1);                                                                                                              //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-OTHER
                pnd_Ws_Pnd_Amt_Recs_Marked_As_Other.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                 //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO #AMT-RECS-MARKED-AS-OTHER
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ......... VCOMMIT ALL THE UPDATE PEDNING RECORDS
        //* ******************* MAY 19 2006 FOR 2200 N10 CHECKS *******************
        //*  RL MAY 19, 2006
        vw_fcp_Cons_Pymnt.startDatabaseRead                                                                                                                               //Natural: READ FCP-CONS-PYMNT BY PYMNT-NBR-STOP-IND
        (
        "READ02",
        new Oc[] { new Oc("PYMNT_NBR_STOP_IND", "ASC") }
        );
        READ02:
        while (condition(vw_fcp_Cons_Pymnt.readNextRow("READ02")))
        {
            //*  RL MAY 19, 2006
            if (condition(!(fcp_Cons_Pymnt_Pymnt_Nbr_N3.equals(220))))                                                                                                    //Natural: ACCEPT IF PYMNT-NBR-N3 = 220
            {
                continue;
            }
            //* ************************ RL END MAY 19 2006 **************************
            //*                                                                                                                                                           //Natural: DECIDE ON FIRST VALUE OF FCP-CONS-PYMNT.CNR-CS-NEW-STOP-IND
            short decideConditionsMet164 = 0;                                                                                                                             //Natural: VALUE 'Y'
            if (condition((fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.equals("Y"))))
            {
                decideConditionsMet164++;
                pnd_Ws_Pnd_Cnt_Recs_Marked_As_Y.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-Y
                pnd_Ws_Pnd_Amt_Recs_Marked_As_Y.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                     //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO #AMT-RECS-MARKED-AS-Y
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.equals("I"))))
            {
                decideConditionsMet164++;
                pnd_Ws_Pnd_Cnt_Recs_Marked_As_I.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-I
                pnd_Ws_Pnd_Amt_Recs_Marked_As_I.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                     //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO #AMT-RECS-MARKED-AS-I
                //*  ..... RESET THE STOP-IND FOR RECORDS WITH INTERMIDIATE VALUE "I"
                fcp_Cons_Pymnt_Cnr_Cs_New_Stop_Ind.reset();                                                                                                               //Natural: RESET FCP-CONS-PYMNT.CNR-CS-NEW-STOP-IND
                vw_fcp_Cons_Pymnt.updateDBRow("READ02");                                                                                                                  //Natural: UPDATE
                pnd_Ws_Pnd_Et_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #ET-CNT
                if (condition(pnd_Ws_Pnd_Et_Cnt.greaterOrEqual(pnd_Force_Et_Count)))                                                                                      //Natural: IF #ET-CNT GE #FORCE-ET-COUNT
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Ws_Pnd_Et_Cnt.reset();                                                                                                                            //Natural: RESET #ET-CNT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                pnd_Ws_Pnd_Cnt_Recs_Marked_As_Other.nadd(1);                                                                                                              //Natural: ADD 1 TO #CNT-RECS-MARKED-AS-OTHER
                pnd_Ws_Pnd_Amt_Recs_Marked_As_Other.nadd(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                 //Natural: ADD FCP-CONS-PYMNT.PYMNT-CHECK-AMT TO #AMT-RECS-MARKED-AS-OTHER
            }                                                                                                                                                             //Natural: END-DECIDE
            //* *
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  ......... VCOMMIT ALL THE UPDATE PEDNING RECORDS
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  END OF PROGRAM
        //*  ..... REPORT STATISTICS ON SYSOUT
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, Global.getPROGRAM(),"END OF RUN at time:",Global.getDATU(),"-",Global.getTIME(),NEWLINE,NEWLINE,"The records that have been 'downloaded' to the Stop-File,",NEWLINE,"(marked as 'I' in STOP-IND) are reset now:",NEWLINE,NEWLINE,NEWLINE,"Number of records..............:",pnd_Ws_Pnd_Cnt_Recs_Marked_As_I," $Amount:",pnd_Ws_Pnd_Amt_Recs_Marked_As_I,  //Natural: WRITE *PROGRAM 'END OF RUN at time:' *DATU '-' *TIME // 'The records that have been "downloaded" to the Stop-File,' / '(marked as "I" in STOP-IND) are reset now:' /// 'Number of records..............:' #CNT-RECS-MARKED-AS-I ' $Amount:' #AMT-RECS-MARKED-AS-I ( EM = Z,ZZZ,ZZZ,ZZZ,ZZZ.99 ) /// 'During the run the program also gathered the following stats,' / 'for general informative purpose:' /// 'Records read with STOP-IND "Y":' #CNT-RECS-MARKED-AS-Y ' $Amount:' #AMT-RECS-MARKED-AS-Y ( EM = Z,ZZZ,ZZZ,ZZZ,ZZZ.99 ) / 'Records read with STOP-IND any:' #CNT-RECS-MARKED-AS-OTHER ' $Amount:' #AMT-RECS-MARKED-AS-OTHER ( EM = Z,ZZZ,ZZZ,ZZZ,ZZZ.99 )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,NEWLINE,"During the run the program also gathered the following stats,",NEWLINE,"for general informative purpose:",NEWLINE,NEWLINE,NEWLINE,"Records read with STOP-IND 'Y':",pnd_Ws_Pnd_Cnt_Recs_Marked_As_Y," $Amount:",pnd_Ws_Pnd_Amt_Recs_Marked_As_Y, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZZ.99"),NEWLINE,"Records read with STOP-IND any:",pnd_Ws_Pnd_Cnt_Recs_Marked_As_Other," $Amount:",pnd_Ws_Pnd_Amt_Recs_Marked_As_Other, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  .. VERIFY AGAINST THE CONTROLS PASSED FROM FCPP234 (EXTRACT STOPS)
        getWorkFiles().read(2, cntlrec);                                                                                                                                  //Natural: READ WORK FILE 2 ONCE CNTLREC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Abend_Cde.setValue(33);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 33
            getReports().write(0, NEWLINE,NEWLINE,Global.getPROGRAM(),Global.getDATU(),"-",Global.getTIME(),NEWLINE,"No record found on the Control FIle (Work File 2)!", //Natural: WRITE // *PROGRAM *DATU '-' *TIME / 'No record found on the Control FIle (Work File 2)!' / 'The program terminates with Condition Code:' #ABEND-CDE
                NEWLINE,"The program terminates with Condition Code:",pnd_Abend_Cde);
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //*  ..... SHOW THE CONTROL DATA
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,"The Control Record created by program FCPP234 indicates:",NEWLINE,NEWLINE,"CREATE-DATE:",cntlrec_Create_Date,"CREATE-TIME:",cntlrec_Create_Time,NEWLINE,NEWLINE,"Number of records..............:",cntlrec_Pnd_Ws_Number_Of_Stopped_Payments,  //Natural: WRITE /// 'The Control Record created by program FCPP234 indicates:' / / 'CREATE-DATE:' CNTLREC.CREATE-DATE 'CREATE-TIME:' CNTLREC.CREATE-TIME // 'Number of records..............:' CNTLREC.#WS-NUMBER-OF-STOPPED-PAYMENTS ( NL = 5 ) ' $Amount:' CNTLREC.#WS-STOP-AMOUNT ( EM = Z,ZZZ,ZZZ.99 )
            new NumericLength (5)," $Amount:",cntlrec_Pnd_Ws_Stop_Amount, new ReportEditMask ("Z,ZZZ,ZZZ.99"));
        if (Global.isEscape()) return;
        //*  ... MATCH AGAINST THE CONTROLS FROM FCPP234
        if (condition(pnd_Ws_Pnd_Cnt_Recs_Marked_As_I.notEquals(cntlrec_Pnd_Ws_Number_Of_Stopped_Payments) || pnd_Ws_Pnd_Amt_Recs_Marked_As_I.notEquals(cntlrec_Pnd_Ws_Stop_Amount))) //Natural: IF #CNT-RECS-MARKED-AS-I NE CNTLREC.#WS-NUMBER-OF-STOPPED-PAYMENTS OR #AMT-RECS-MARKED-AS-I NE CNTLREC.#WS-STOP-AMOUNT
        {
            pnd_Abend_Cde.setValue(34);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 34
            getReports().write(0, NEWLINE,NEWLINE,Global.getPROGRAM(),Global.getDATU(),"-",Global.getTIME(),NEWLINE,"The number or the Amount of the extracted Stops in prog FCPP234", //Natural: WRITE // *PROGRAM *DATU '-' *TIME / 'The number or the Amount of the extracted Stops in prog FCPP234' / 'Does not match the number or amount reset by this program!' / 'This may be a valid condition if FCPP235 was restart.' / 'The program terminates with Condition Code:' #ABEND-CDE
                NEWLINE,"Does not match the number or amount reset by this program!",NEWLINE,"This may be a valid condition if FCPP235 was restart.",NEWLINE,
                "The program terminates with Condition Code:",pnd_Abend_Cde);
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-IF
    }

    //
}
