/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:25:23 PM
**        * FROM NATURAL PROGRAM : Cpvp135
************************************************************
**        * FILE NAME            : Cpvp135.java
**        * CLASS NAME           : Cpvp135
**        * INSTANCE NAME        : Cpvp135
************************************************************
************************************************************************
** PROGRAM    :  CPVP135                                              **
** APPLICATION:  CONSOLIDATED PAYMENT SYSTEM                          **
**            :  OPEN INVESTMENT ARCHITECTURE                         **
**            :                                                       **
** DATE       :  01/30/2002                                           **
**            :                                                       **
** DESCRIPTION:  END-OF-CYCLE AND END-OF-DAY PROCESSING.              **
**            :                                                       **
** AUTHOR     :  ALTHEA A. YOUNG                                      **
**            :                                                       **
** NOTES      :  ERROR CODE 84 (OUT-OF-BALANCE)                       **
**            :  - IF RECORDS WERE DELETED FROM FCP-CONS-PYMNT,       **
**            :    DECREASE THE CHECK, EFT, OR OTHER TOTALS ON THE    **
**            :    'RECEIVE' RECORD ACCORDINGLY BY THE APPROPRIATE    **
**            :    AMOUNTS.                                           **
**            :  - IF A RECORD WAS NOT UPDATED IN PCP1610D, INCREASE  **
**            :    THE CHECK, EFT, OR OTHER TOTALS ON THE 'RECEIVE'   **
**            :    RECORD ACCORDINGLY BY THE APPROPRIATE AMOUNTS.     **
**            :                                                       **
** HISTORY    :                                                       **
**            :                                                       **
** 01/30/2002 :  A. YOUNG     - ADAPTED FROM CPSP135.                 **
**            :               - REPLACED ORIGIN CODE PROCESSING WITH  **
**            :                 SUB-PROGRAM 'CPVNORGN'.               **
**            :               - ADDED #WS AREA FOR PARM TRANSLATION.  **
**            :               - 'Other' PROCESSING.                   **
**            :               - REPLACED HARD-CODED PROGRAM NAME WITH **
**            :                 *PROGRAM.                             **
** 10/15/2002 :  A. YOUNG     - CANCELS & STOPS ADDED TO OTHER TOTALS,**
**            :                 REGULAR & REDRAWN TO CHECK TOTALS.    **
**            :                                                       **
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpvp135 extends BLNatBase
{
    // Data Areas
    private PdaCpoaorgn pdaCpoaorgn;
    private PdaCpoatotl pdaCpoatotl;
    private PdaCpoa100 pdaCpoa100;
    private PdaCpoa101 pdaCpoa101;
    private LdaCpolpmnt ldaCpolpmnt;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Rt_Desc1;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Rt_Eoc;
    private DbsField pnd_Ws__Filler1;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Rt_Eod;
    private DbsField pnd_Ws__Filler2;
    private DbsField pnd_Orig_Status;

    private DbsGroup pnd_Orig_Status__R_Field_3;
    private DbsField pnd_Orig_Status_Pnd_Orig_Cde;
    private DbsField pnd_Orig_Status_Pnd_Stat_Cde;
    private DbsField pnd_End_Of_Day;
    private DbsField pnd_End_Of_Cycle;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpoaorgn = new PdaCpoaorgn(localVariables);
        pdaCpoatotl = new PdaCpoatotl(localVariables);
        pdaCpoa100 = new PdaCpoa100(localVariables);
        pdaCpoa101 = new PdaCpoa101(localVariables);
        ldaCpolpmnt = new LdaCpolpmnt();
        registerRecord(ldaCpolpmnt);
        registerRecord(ldaCpolpmnt.getVw_pymnt());

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Rt_Desc1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rt_Desc1", "#RT-DESC1", FieldType.STRING, 75);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Rt_Desc1);
        pnd_Ws_Pnd_Rt_Eoc = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Rt_Eoc", "#RT-EOC", FieldType.STRING, 12);
        pnd_Ws__Filler1 = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws__Filler1", "_FILLER1", FieldType.STRING, 63);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Rt_Desc1);
        pnd_Ws_Pnd_Rt_Eod = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Rt_Eod", "#RT-EOD", FieldType.STRING, 10);
        pnd_Ws__Filler2 = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws__Filler2", "_FILLER2", FieldType.STRING, 65);
        pnd_Orig_Status = localVariables.newFieldInRecord("pnd_Orig_Status", "#ORIG-STATUS", FieldType.STRING, 3);

        pnd_Orig_Status__R_Field_3 = localVariables.newGroupInRecord("pnd_Orig_Status__R_Field_3", "REDEFINE", pnd_Orig_Status);
        pnd_Orig_Status_Pnd_Orig_Cde = pnd_Orig_Status__R_Field_3.newFieldInGroup("pnd_Orig_Status_Pnd_Orig_Cde", "#ORIG-CDE", FieldType.STRING, 2);
        pnd_Orig_Status_Pnd_Stat_Cde = pnd_Orig_Status__R_Field_3.newFieldInGroup("pnd_Orig_Status_Pnd_Stat_Cde", "#STAT-CDE", FieldType.STRING, 1);
        pnd_End_Of_Day = localVariables.newFieldInRecord("pnd_End_Of_Day", "#END-OF-DAY", FieldType.STRING, 10);
        pnd_End_Of_Cycle = localVariables.newFieldInRecord("pnd_End_Of_Cycle", "#END-OF-CYCLE", FieldType.STRING, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCpolpmnt.initializeValues();

        localVariables.reset();
        pnd_Orig_Status.setInitialValue("  D");
        pnd_End_Of_Day.setInitialValue("END OF DAY");
        pnd_End_Of_Cycle.setInitialValue("END OF CYCLE");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cpvp135() throws Exception
    {
        super("Cpvp135");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ---------------------------------------                             /*
        //*                                                                                                                                                               //Natural: FORMAT PS = 58 LS = 132 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* *--------------------------------------------------------------------**
        //*                            M A I N L I N E                            *
        //* *--------------------------------------------------------------------**
        //*  01-30-2002
        //*  01-30-2002
        DbsUtil.callnat(Cpvnorgn.class , getCurrentProcessState(), pdaCpoaorgn.getCpoaorgn());                                                                            //Natural: CALLNAT 'CPVNORGN' CPOAORGN
        if (condition(Global.isEscape())) return;
        pdaCpoatotl.getCpoatotl_Pnd_Origin_Code().getValue("*").setValue(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Cde_2().getValue("*"));                                         //Natural: ASSIGN CPOATOTL.#ORIGIN-CODE ( * ) := CPOAORGN.#ORGN-CDE-2 ( * )
        //*  01-30-2002
                                                                                                                                                                          //Natural: PERFORM CALL-CPOA100-RTN
        sub_Call_Cpoa100_Rtn();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Rt_Desc1.setValue(pdaCpoa100.getCpoa100_Rt_Desc1());                                                                                                   //Natural: ASSIGN #WS.#RT-DESC1 := CPOA100.RT-DESC1
        //*  END-OF-CYCLE     /* 01-30-2002
        //*  01-30-2002
        if (condition(pnd_Ws_Pnd_Rt_Eoc.equals(pnd_End_Of_Cycle)))                                                                                                        //Natural: IF #WS.#RT-EOC EQ #END-OF-CYCLE
        {
            pdaCpoa101.getCpoa101_Function().setValue(pnd_Ws_Pnd_Rt_Eoc);                                                                                                 //Natural: ASSIGN CPOA101.FUNCTION := #WS.#RT-EOC
            //*  01-30-2002
                                                                                                                                                                          //Natural: PERFORM CALL-CPON101-RTN
            sub_Call_Cpon101_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  END-OF-DAY       /* 01-30-2002
            //*   REMOVE AFTER TEST
            //*  01-30-2002
            if (condition(pnd_Ws_Pnd_Rt_Eod.equals(pnd_End_Of_Day)))                                                                                                      //Natural: IF #WS.#RT-EOD EQ #END-OF-DAY
            {
                pnd_Orig_Status_Pnd_Stat_Cde.setValue("Z");                                                                                                               //Natural: ASSIGN #STAT-CDE := 'Z'
                pdaCpoa101.getCpoa101_Function().setValue(pnd_Ws_Pnd_Rt_Eod);                                                                                             //Natural: ASSIGN CPOA101.FUNCTION := #WS.#RT-EOD
                                                                                                                                                                          //Natural: PERFORM END-OF-DAY-RTN
                sub_End_Of_Day_Rtn();
                if (condition(Global.isEscape())) {return;}
                //*  01-30-2002
                                                                                                                                                                          //Natural: PERFORM CALL-CPON101-RTN
                sub_Call_Cpon101_Rtn();
                if (condition(Global.isEscape())) {return;}
                //*  ERROR PROCESSING /* 01-30-2002
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape())) {return;}
                //* *  IF  *INIT-USER  = 'PCP1635D'                           /* 01-30-2002
                //*  01-30-2002
                if (condition(Global.getINIT_USER().equals("PCP1670D")))                                                                                                  //Natural: IF *INIT-USER = 'PCP1670D'
                {
                    getReports().write(0, "***",new TabSetting(5),"ERROR ON REFERENCE TABLE 'CPARM' - ","'END OF CYCLE'  PARAMETER NOT FOUND",new TabSetting(77),         //Natural: WRITE '***' 5T 'ERROR ON REFERENCE TABLE "CPARM" - ' '"END OF CYCLE"  PARAMETER NOT FOUND' 77T '***'
                        "***");
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  01-30-2002
                    if (condition(Global.getINIT_USER().equals("PCP1699D")))                                                                                              //Natural: IF *INIT-USER = 'PCP1699D'
                    {
                        getReports().write(0, "***",new TabSetting(5),"ERROR ON REFERENCE TABLE 'CPARM' - ","'END OF DAY'  PARAMETER NOT FOUND",new TabSetting(77),       //Natural: WRITE '***' 5T 'ERROR ON REFERENCE TABLE "CPARM" - ' '"END OF DAY"  PARAMETER NOT FOUND' 77T '***'
                            "***");
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, "***",new TabSetting(5),"PROGRAM",Global.getPROGRAM(),"RUNNING IN WRONG JOB",new TabSetting(77),"***");                     //Natural: WRITE '***' 5T 'PROGRAM' *PROGRAM 'RUNNING IN WRONG JOB' 77T '***'
                        if (Global.isEscape()) return;
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape())) {return;}
                DbsUtil.terminate(50);  if (true) return;                                                                                                                 //Natural: TERMINATE 50
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CPOA100-RTN
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-OF-DAY-RTN
        //* ***********************************************************************
        //*  ------------------------     READ ADABAS PYMNT FILE 196
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALL-CPON101-RTN
        //* ***********************************************************************
        //* ***********************************************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Call_Cpoa100_Rtn() throws Exception                                                                                                                  //Natural: CALL-CPOA100-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pdaCpoa100.getCpoa100_Cpoa100_Table_Id().setValue("CPARM");                                                                                                       //Natural: ASSIGN CPOA100-TABLE-ID := 'CPARM'
        pdaCpoa100.getCpoa100_Cpoa100_A_I_Ind().setValue("A");                                                                                                            //Natural: ASSIGN CPOA100-A-I-IND := 'A'
        pdaCpoa100.getCpoa100_Cpoa100_Short_Key().setValue(Global.getINIT_USER());                                                                                        //Natural: ASSIGN CPOA100-SHORT-KEY := *INIT-USER
        DbsUtil.callnat(Cpon100.class , getCurrentProcessState(), pdaCpoa100.getCpoa100().getValue("*"));                                                                 //Natural: CALLNAT 'CPON100' USING CPOA100 ( * )
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpoa100.getCpoa100_Cpoa100_Return_Code().notEquals("00")))                                                                                       //Natural: IF CPOA100.CPOA100-RETURN-CODE NE '00'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(5),"ERROR ON REFERENCE TABLE 'CPARM' -","END OF DAY, END OF CYCLE PARAMETER;",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE '***' 5T 'ERROR ON REFERENCE TABLE "CPARM" -' 'END OF DAY, END OF CYCLE PARAMETER;' 77T '***' / '***' 5T 'CALLING PROGRAM CPON100              ' 77T '***' / '***' 5T '          OR                         ' 77T '***' / '***' 5T 'PROGRAM CPOP135 EXECUTED IN WRONG JOB' 77T '***'
                TabSetting(5),"CALLING PROGRAM CPON100              ",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(5),"          OR                         ",new 
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(5),"PROGRAM CPOP135 EXECUTED IN WRONG JOB",new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(99);  if (true) return;                                                                                                                     //Natural: TERMINATE 99
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALL-CPOA100-RTN
    }
    //*  01-30-2002
    private void sub_End_Of_Day_Rtn() throws Exception                                                                                                                    //Natural: END-OF-DAY-RTN
    {
        if (BLNatReinput.isReinput()) return;

        FOR01:                                                                                                                                                            //Natural: FOR #O FROM 1 TO CPOAORGN.#ORGN-USED
        for (pdaCpoaorgn.getCpoaorgn_Pnd_O().setValue(1); condition(pdaCpoaorgn.getCpoaorgn_Pnd_O().lessOrEqual(pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used())); 
            pdaCpoaorgn.getCpoaorgn_Pnd_O().nadd(1))
        {
            //*  01-30-2002
            if (condition(pdaCpoatotl.getCpoatotl_Pnd_Origin_Code().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).equals(" ")))                                               //Natural: IF CPOATOTL.#ORIGIN-CODE ( #O ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  01-30-2002
                //*  01-30-2002
            }                                                                                                                                                             //Natural: END-IF
            pnd_Orig_Status_Pnd_Orig_Cde.setValue(pdaCpoatotl.getCpoatotl_Pnd_Origin_Code().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()));                                   //Natural: ASSIGN #ORIG-CDE := CPOATOTL.#ORIGIN-CODE ( #O )
            ldaCpolpmnt.getVw_pymnt().startDatabaseRead                                                                                                                   //Natural: READ PYMNT BY ORGN-STATUS-INVRSE-SEQ STARTING FROM #ORIG-STATUS
            (
            "R2",
            new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Orig_Status, WcType.BY) },
            new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
            );
            R2:
            while (condition(ldaCpolpmnt.getVw_pymnt().readNextRow("R2")))
            {
                //* *
                if (condition(ldaCpolpmnt.getPymnt_Cntrct_Orgn_Cde().notEquals(pnd_Orig_Status_Pnd_Orig_Cde) || ldaCpolpmnt.getPymnt_Pymnt_Stats_Cde().notEquals(pnd_Orig_Status_Pnd_Stat_Cde))) //Natural: IF PYMNT.CNTRCT-ORGN-CDE NOT = #ORIG-CDE OR PYMNT.PYMNT-STATS-CDE NOT = #STAT-CDE
                {
                    if (true) break R2;                                                                                                                                   //Natural: ESCAPE BOTTOM ( R2. )
                }                                                                                                                                                         //Natural: END-IF
                //*  01-06-03
                if (condition(ldaCpolpmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C ") || ldaCpolpmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S ")      //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C ' OR = 'S ' OR = 'CN' OR = 'SN'
                    || ldaCpolpmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || ldaCpolpmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN")))
                {
                    pdaCpoatotl.getCpoatotl_Pnd_Cs_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(1);                                                               //Natural: ADD 1 TO CPOATOTL.#CS-CNT ( #O )
                    pdaCpoatotl.getCpoatotl_Pnd_Cs_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(ldaCpolpmnt.getPymnt_Cntrct_Net_Pymnt_Amt());                     //Natural: ADD PYMNT.CNTRCT-NET-PYMNT-AMT TO CPOATOTL.#CS-AMT ( #O )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  01-06-03
                    if (condition(ldaCpolpmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R ")))                                                                     //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R '
                    {
                        pdaCpoatotl.getCpoatotl_Pnd_Redraw_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(1);                                                       //Natural: ADD 1 TO CPOATOTL.#REDRAW-CNT ( #O )
                        pdaCpoatotl.getCpoatotl_Pnd_Redraw_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(ldaCpolpmnt.getPymnt_Cntrct_Net_Pymnt_Amt());             //Natural: ADD PYMNT.CNTRCT-NET-PYMNT-AMT TO CPOATOTL.#REDRAW-AMT ( #O )
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet641 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF PYMNT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
                    if (condition((ldaCpolpmnt.getPymnt_Pymnt_Pay_Type_Req_Ind().equals(1))))
                    {
                        decideConditionsMet641++;
                        if (condition(ldaCpolpmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || ldaCpolpmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))) //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
                        {
                            pdaCpoatotl.getCpoatotl_Pnd_Check_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(1);                                                    //Natural: ADD 1 TO CPOATOTL.#CHECK-CNT ( #O )
                            pdaCpoatotl.getCpoatotl_Pnd_Check_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(ldaCpolpmnt.getPymnt_Cntrct_Net_Pymnt_Amt());          //Natural: ADD PYMNT.CNTRCT-NET-PYMNT-AMT TO CPOATOTL.#CHECK-AMT ( #O )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaCpoatotl.getCpoatotl_Pnd_Other_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(1);                                                    //Natural: ADD 1 TO CPOATOTL.#OTHER-CNT ( #O )
                            pdaCpoatotl.getCpoatotl_Pnd_Other_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(ldaCpolpmnt.getPymnt_Cntrct_Net_Pymnt_Amt());          //Natural: ADD PYMNT.CNTRCT-NET-PYMNT-AMT TO CPOATOTL.#OTHER-AMT ( #O )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 2
                    else if (condition((ldaCpolpmnt.getPymnt_Pymnt_Pay_Type_Req_Ind().equals(2))))
                    {
                        decideConditionsMet641++;
                        pdaCpoatotl.getCpoatotl_Pnd_Eft_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(1);                                                          //Natural: ADD 1 TO CPOATOTL.#EFT-CNT ( #O )
                        pdaCpoatotl.getCpoatotl_Pnd_Eft_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(ldaCpolpmnt.getPymnt_Cntrct_Net_Pymnt_Amt());                //Natural: ADD PYMNT.CNTRCT-NET-PYMNT-AMT TO CPOATOTL.#EFT-AMT ( #O )
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        pdaCpoatotl.getCpoatotl_Pnd_Other_Cnt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(1);                                                        //Natural: ADD 1 TO CPOATOTL.#OTHER-CNT ( #O )
                        pdaCpoatotl.getCpoatotl_Pnd_Other_Amt().getValue(pdaCpoaorgn.getCpoaorgn_Pnd_O()).nadd(ldaCpolpmnt.getPymnt_Cntrct_Net_Pymnt_Amt());              //Natural: ADD PYMNT.CNTRCT-NET-PYMNT-AMT TO CPOATOTL.#OTHER-AMT ( #O )
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //*  R2.
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  R1.
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaCpoa101.getCpoa101_Cntrct_Orgn_Cde().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Origin_Code().getValue(1,    //Natural: ASSIGN CPOA101.CNTRCT-ORGN-CDE ( 1:#ORGN-USED ) := CPOATOTL.#ORIGIN-CODE ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Check_Cnt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Check_Cnt().getValue(1,            //Natural: ASSIGN CPOA101.CHECK-CNT ( 1:#ORGN-USED ) := CPOATOTL.#CHECK-CNT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Check_Amt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Check_Amt().getValue(1,            //Natural: ASSIGN CPOA101.CHECK-AMT ( 1:#ORGN-USED ) := CPOATOTL.#CHECK-AMT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Eft_Cnt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Eft_Cnt().getValue(1,                //Natural: ASSIGN CPOA101.EFT-CNT ( 1:#ORGN-USED ) := CPOATOTL.#EFT-CNT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Eft_Amt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Eft_Amt().getValue(1,                //Natural: ASSIGN CPOA101.EFT-AMT ( 1:#ORGN-USED ) := CPOATOTL.#EFT-AMT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Other_Cnt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Other_Cnt().getValue(1,            //Natural: ASSIGN CPOA101.OTHER-CNT ( 1:#ORGN-USED ) := CPOATOTL.#OTHER-CNT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Other_Amt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Other_Amt().getValue(1,            //Natural: ASSIGN CPOA101.OTHER-AMT ( 1:#ORGN-USED ) := CPOATOTL.#OTHER-AMT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Cs_Cnt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Cs_Cnt().getValue(1,                  //Natural: ASSIGN CPOA101.CS-CNT ( 1:#ORGN-USED ) := CPOATOTL.#CS-CNT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Cs_Amt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Cs_Amt().getValue(1,                  //Natural: ASSIGN CPOA101.CS-AMT ( 1:#ORGN-USED ) := CPOATOTL.#CS-AMT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Redraw_Cnt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Redraw_Cnt().getValue(1,          //Natural: ASSIGN CPOA101.REDRAW-CNT ( 1:#ORGN-USED ) := CPOATOTL.#REDRAW-CNT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        pdaCpoa101.getCpoa101_Redraw_Amt().getValue(1,":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()).setValue(pdaCpoatotl.getCpoatotl_Pnd_Redraw_Amt().getValue(1,          //Natural: ASSIGN CPOA101.REDRAW-AMT ( 1:#ORGN-USED ) := CPOATOTL.#REDRAW-AMT ( 1:#ORGN-USED )
            ":",pdaCpoaorgn.getCpoaorgn_Pnd_Orgn_Used()));
        //*  END-OF-DAY-RTN
    }
    private void sub_Call_Cpon101_Rtn() throws Exception                                                                                                                  //Natural: CALL-CPON101-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pdaCpoa101.getCpoa101_Procedure().setValue(Global.getINIT_USER());                                                                                                //Natural: ASSIGN CPOA101.PROCEDURE := *INIT-USER
        pdaCpoa101.getCpoa101_Module().setValue(Global.getPROGRAM());                                                                                                     //Natural: ASSIGN CPOA101.MODULE := *PROGRAM
        DbsUtil.callnat(Cpvn101.class , getCurrentProcessState(), pdaCpoa101.getCpoa101());                                                                               //Natural: CALLNAT 'CPVN101' CPOA101
        if (condition(Global.isEscape())) return;
        //*  CALL-CPON101-RTN
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
    }
}
