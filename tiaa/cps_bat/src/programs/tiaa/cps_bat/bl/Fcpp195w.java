/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:01 PM
**        * FROM NATURAL PROGRAM : Fcpp195w
************************************************************
**        * FILE NAME            : Fcpp195w.java
**        * CLASS NAME           : Fcpp195w
**        * INSTANCE NAME        : Fcpp195w
************************************************************
************************************************************************
* PROGRAM  : FCPP195W
* SYSTEM   : CPS
* TITLE    : POSITIVE PAY FOR ON-LINE PAYMENTS.
* AUTHOR   : LANDRUM.      JAN 2006
* FUNCTION : THIS PROGRAM IS FOR POSITIVE PAY FOR WACHOVIA.
*
* HISTORY:              CLONE OF FCPP195
* LANDRUM 5-2-2006  RESOLVE PROD ABEND NAT1301 ON #READ-CNT (P3 TO N9)
* LANDRUM 5-3-2006  PPY FILE-USE PAYEE-NAME(2) FOR CR PYMNTS
* LANDRUM 5-12-2006 FCP-CONS-PYMNT.CNTRCT-PPCN-NBR TO PPY FILE
* 01/2016 SAURAV VIKRAM  - RESOLVED INCONSISTENCY OF REPORT   - VIKRAM
*                          GENERATION  REF-PRB72470
* 5/2017   : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp195w extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private LdaFcpl190a ldaFcpl190a;
    private LdaFcpl300 ldaFcpl300;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Total;
    private DbsField pnd_Diff_Total;
    private DbsField pnd_Count;
    private DbsField pnd_Rec_Cnt;
    private DbsField pnd_Diff_Count;
    private DbsField pnd_Ws_Chk_Dte;
    private DbsField pnd_Check_Date;
    private DbsField i;
    private DbsField pnd_Date_Exists;
    private DbsField pnd_First;
    private DbsField pnd_Data_Present;
    private DbsField pnd_Prime_Counter;

    private DbsGroup pnd_Output_Record;
    private DbsField pnd_Output_Record_Pnd_Acct_Nbr;
    private DbsField pnd_Output_Record_Pnd_Pymnt_Check_Nbr;

    private DbsGroup pnd_Output_Record__R_Field_1;
    private DbsField pnd_Output_Record_Pnd_Pymnt_Check_Nbr_N3;
    private DbsField pnd_Output_Record_Pnd_Pymnt_Check_Nbr_N7;
    private DbsField pnd_Output_Record_Pnd_Pymnt_Check_Amt;
    private DbsField pnd_Output_Record_Pnd_Pymnt_Check_Dte;
    private DbsField pnd_Output_Record_Pnd_Void_Ind;
    private DbsField pnd_Output_Record_Pnd_Additional_Data;

    private DbsGroup pnd_Output_Record__R_Field_2;
    private DbsField pnd_Output_Record_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Output_Record_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Output_Record_Pnd_Al_Indicator;
    private DbsField pnd_Output_Record_Pnd_Filler1;
    private DbsField pnd_Output_Record_Pnd_Filo1;
    private DbsField pnd_Output_Record_Pnd_Payee_Nme;
    private DbsField pnd_Output_Record_Pnd_Fil02;

    private DbsGroup pnd_Header_Record;
    private DbsField pnd_Header_Record_Pnd_Recon_Hdr;
    private DbsField pnd_Header_Record_Pnd_Bank_No;
    private DbsField pnd_Header_Record_Pnd_Acct_Nbr;
    private DbsField pnd_Header_Record_Pnd_Issue_Amount;
    private DbsField pnd_Header_Record_Pnd_Issue_Count;
    private DbsField pnd_Header_Record_Fil03;

    private DbsGroup pnd_Control_Record;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date;

    private DbsGroup pnd_Control_Record__R_Field_3;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2;
    private DbsField pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Check;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Check;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Eft;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft;
    private DbsField pnd_Control_Record_Pnd_Cntl_Cnt_Int_Roll;
    private DbsField pnd_Control_Record_Pnd_Cntl_Net_Amt_Int_Roll;

    private DbsGroup pnd_Amt_R;
    private DbsField pnd_Amt_R_Pnd_Issue_Amount_W;

    private DbsGroup pnd_Amt_R__R_Field_4;
    private DbsField pnd_Amt_R_Pnd_Issue_Amount_2_W;
    private DbsField pnd_Amt_R_Pnd_Issue_Count_W;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        ldaFcpl300 = new LdaFcpl300();
        registerRecord(ldaFcpl300);
        localVariables = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnt_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Pymnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        registerRecord(vw_fcp_Cons_Pymnt);

        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Total = localVariables.newFieldInRecord("pnd_Total", "#TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Diff_Total = localVariables.newFieldInRecord("pnd_Diff_Total", "#DIFF-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Rec_Cnt = localVariables.newFieldInRecord("pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Diff_Count = localVariables.newFieldInRecord("pnd_Diff_Count", "#DIFF-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Chk_Dte = localVariables.newFieldInRecord("pnd_Ws_Chk_Dte", "#WS-CHK-DTE", FieldType.STRING, 8);
        pnd_Check_Date = localVariables.newFieldArrayInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.STRING, 8, new DbsArrayController(1, 99));
        i = localVariables.newFieldInRecord("i", "I", FieldType.INTEGER, 2);
        pnd_Date_Exists = localVariables.newFieldInRecord("pnd_Date_Exists", "#DATE-EXISTS", FieldType.INTEGER, 2);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Data_Present = localVariables.newFieldInRecord("pnd_Data_Present", "#DATA-PRESENT", FieldType.BOOLEAN, 1);
        pnd_Prime_Counter = localVariables.newFieldInRecord("pnd_Prime_Counter", "#PRIME-COUNTER", FieldType.PACKED_DECIMAL, 7);

        pnd_Output_Record = localVariables.newGroupInRecord("pnd_Output_Record", "#OUTPUT-RECORD");
        pnd_Output_Record_Pnd_Acct_Nbr = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Acct_Nbr", "#ACCT-NBR", FieldType.NUMERIC, 13);
        pnd_Output_Record_Pnd_Pymnt_Check_Nbr = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Pymnt_Check_Nbr", "#PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            10);

        pnd_Output_Record__R_Field_1 = pnd_Output_Record.newGroupInGroup("pnd_Output_Record__R_Field_1", "REDEFINE", pnd_Output_Record_Pnd_Pymnt_Check_Nbr);
        pnd_Output_Record_Pnd_Pymnt_Check_Nbr_N3 = pnd_Output_Record__R_Field_1.newFieldInGroup("pnd_Output_Record_Pnd_Pymnt_Check_Nbr_N3", "#PYMNT-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Output_Record_Pnd_Pymnt_Check_Nbr_N7 = pnd_Output_Record__R_Field_1.newFieldInGroup("pnd_Output_Record_Pnd_Pymnt_Check_Nbr_N7", "#PYMNT-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Output_Record_Pnd_Pymnt_Check_Amt = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Pymnt_Check_Amt", "#PYMNT-CHECK-AMT", FieldType.NUMERIC, 
            10, 2);
        pnd_Output_Record_Pnd_Pymnt_Check_Dte = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.STRING, 
            8);
        pnd_Output_Record_Pnd_Void_Ind = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Void_Ind", "#VOID-IND", FieldType.STRING, 1);
        pnd_Output_Record_Pnd_Additional_Data = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Additional_Data", "#ADDITIONAL-DATA", FieldType.STRING, 
            30);

        pnd_Output_Record__R_Field_2 = pnd_Output_Record.newGroupInGroup("pnd_Output_Record__R_Field_2", "REDEFINE", pnd_Output_Record_Pnd_Additional_Data);
        pnd_Output_Record_Pnd_Cntrct_Ppcn_Nbr = pnd_Output_Record__R_Field_2.newFieldInGroup("pnd_Output_Record_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 8);
        pnd_Output_Record_Pnd_Cntrct_Payee_Cde = pnd_Output_Record__R_Field_2.newFieldInGroup("pnd_Output_Record_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Output_Record_Pnd_Al_Indicator = pnd_Output_Record__R_Field_2.newFieldInGroup("pnd_Output_Record_Pnd_Al_Indicator", "#AL-INDICATOR", FieldType.STRING, 
            1);
        pnd_Output_Record_Pnd_Filler1 = pnd_Output_Record__R_Field_2.newFieldInGroup("pnd_Output_Record_Pnd_Filler1", "#FILLER1", FieldType.STRING, 19);
        pnd_Output_Record_Pnd_Filo1 = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Filo1", "#FILO1", FieldType.STRING, 8);
        pnd_Output_Record_Pnd_Payee_Nme = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Payee_Nme", "#PAYEE-NME", FieldType.STRING, 50);
        pnd_Output_Record_Pnd_Fil02 = pnd_Output_Record.newFieldInGroup("pnd_Output_Record_Pnd_Fil02", "#FIL02", FieldType.STRING, 20);

        pnd_Header_Record = localVariables.newGroupInRecord("pnd_Header_Record", "#HEADER-RECORD");
        pnd_Header_Record_Pnd_Recon_Hdr = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Recon_Hdr", "#RECON-HDR", FieldType.STRING, 20);
        pnd_Header_Record_Pnd_Bank_No = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Bank_No", "#BANK-NO", FieldType.NUMERIC, 4);
        pnd_Header_Record_Pnd_Acct_Nbr = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Acct_Nbr", "#ACCT-NBR", FieldType.NUMERIC, 13);
        pnd_Header_Record_Pnd_Issue_Amount = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Issue_Amount", "#ISSUE-AMOUNT", FieldType.NUMERIC, 
            12, 2);
        pnd_Header_Record_Pnd_Issue_Count = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Issue_Count", "#ISSUE-COUNT", FieldType.NUMERIC, 
            5);
        pnd_Header_Record_Fil03 = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Fil03", "FIL03", FieldType.STRING, 96);

        pnd_Control_Record = localVariables.newGroupInRecord("pnd_Control_Record", "#CONTROL-RECORD");
        pnd_Control_Record_Pnd_Cntl_Payment_Date = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date", "#CNTL-PAYMENT-DATE", 
            FieldType.STRING, 8);

        pnd_Control_Record__R_Field_3 = pnd_Control_Record.newGroupInGroup("pnd_Control_Record__R_Field_3", "REDEFINE", pnd_Control_Record_Pnd_Cntl_Payment_Date);
        pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2 = pnd_Control_Record__R_Field_3.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date_1_2", "#CNTL-PAYMENT-DATE-1-2", 
            FieldType.STRING, 2);
        pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8 = pnd_Control_Record__R_Field_3.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Payment_Date_3_8", "#CNTL-PAYMENT-DATE-3-8", 
            FieldType.STRING, 6);
        pnd_Control_Record_Pnd_Cntl_Cnt_Check = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Check", "#CNTL-CNT-CHECK", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Check = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Check", "#CNTL-NET-AMT-CHECK", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Control_Record_Pnd_Cntl_Cnt_Eft = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Eft", "#CNTL-CNT-EFT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Eft", "#CNTL-NET-AMT-EFT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Control_Record_Pnd_Cntl_Cnt_Int_Roll = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Cnt_Int_Roll", "#CNTL-CNT-INT-ROLL", 
            FieldType.PACKED_DECIMAL, 9);
        pnd_Control_Record_Pnd_Cntl_Net_Amt_Int_Roll = pnd_Control_Record.newFieldInGroup("pnd_Control_Record_Pnd_Cntl_Net_Amt_Int_Roll", "#CNTL-NET-AMT-INT-ROLL", 
            FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Amt_R = localVariables.newGroupInRecord("pnd_Amt_R", "#AMT-R");
        pnd_Amt_R_Pnd_Issue_Amount_W = pnd_Amt_R.newFieldInGroup("pnd_Amt_R_Pnd_Issue_Amount_W", "#ISSUE-AMOUNT-W", FieldType.NUMERIC, 12);

        pnd_Amt_R__R_Field_4 = pnd_Amt_R.newGroupInGroup("pnd_Amt_R__R_Field_4", "REDEFINE", pnd_Amt_R_Pnd_Issue_Amount_W);
        pnd_Amt_R_Pnd_Issue_Amount_2_W = pnd_Amt_R__R_Field_4.newFieldInGroup("pnd_Amt_R_Pnd_Issue_Amount_2_W", "#ISSUE-AMOUNT-2-W", FieldType.NUMERIC, 
            12, 2);
        pnd_Amt_R_Pnd_Issue_Count_W = pnd_Amt_R.newFieldInGroup("pnd_Amt_R_Pnd_Issue_Count_W", "#ISSUE-COUNT-W", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();

        ldaCdbatxa.initializeValues();
        ldaFcpl190a.initializeValues();
        ldaFcpl300.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("               Reconciliation Report");
        pnd_Header0_2.setInitialValue("                Daily Positive Pay");
        pnd_Ws_Chk_Dte.setInitialValue(" ");
        pnd_Check_Date.getValue(1).setInitialValue(" ");
        i.setInitialValue(1);
        pnd_Date_Exists.setInitialValue(0);
        pnd_First.setInitialValue(true);
        pnd_Data_Present.setInitialValue(false);
        pnd_Output_Record_Pnd_Acct_Nbr.setInitialValue(-2147483648);
        pnd_Header_Record_Pnd_Recon_Hdr.setInitialValue("RECONCILIATIONHEADER");
        pnd_Header_Record_Pnd_Bank_No.setInitialValue(75);
        pnd_Header_Record_Pnd_Acct_Nbr.setInitialValue(-2147483648);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp195w() throws Exception
    {
        super("Fcpp195w");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 55 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //*  RL FEB 16,2006 GET CHK-NBR PREFIX
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        //*  PRIMARY FILE
        getWorkFiles().read(13, pnd_Control_Record);                                                                                                                      //Natural: READ WORK 13 ONCE #CONTROL-RECORD
        //* *
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, "CONTROL RECORD 'CMWKF13' IS MISSING, NOTIFY 'MIS'");                                                                                   //Natural: WRITE ( 00 ) 'CONTROL RECORD "CMWKF13" IS MISSING, NOTIFY "MIS"'
            if (Global.isEscape()) return;
            DbsUtil.terminate(40);  if (true) return;                                                                                                                     //Natural: TERMINATE 40
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Amt_R_Pnd_Issue_Amount_2_W.setValue(pnd_Control_Record_Pnd_Cntl_Net_Amt_Check);                                                                               //Natural: ASSIGN #AMT-R.#ISSUE-AMOUNT-2-W := #CONTROL-RECORD.#CNTL-NET-AMT-CHECK
        pnd_Amt_R_Pnd_Issue_Count_W.setValue(pnd_Control_Record_Pnd_Cntl_Cnt_Check);                                                                                      //Natural: ASSIGN #AMT-R.#ISSUE-COUNT-W := #CONTROL-RECORD.#CNTL-CNT-CHECK
        READWORK01:                                                                                                                                                       //Natural: READ WORK 6 #RPT-EXT
        while (condition(getWorkFiles().read(6, ldaFcpl190a.getPnd_Rpt_Ext())))
        {
            if (condition(!(((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))  //Natural: ACCEPT IF ( #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R' ) AND #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 1
                && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1)))))
            {
                continue;
            }
            if (condition(pnd_First.getBoolean()))                                                                                                                        //Natural: IF #FIRST
            {
                pnd_Check_Date.getValue(i).setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("MM'/'DD'/'YY"));                               //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = MM'/'DD'/'YY ) TO #CHECK-DATE ( I )
                pnd_First.setValue(false);                                                                                                                                //Natural: ASSIGN #FIRST := FALSE
            }                                                                                                                                                             //Natural: END-IF
            //*  ADDED CHECK DATE PROCESSING TO PROVIDE FOR MULTIPLE CHECK DATES
            pnd_Ws_Chk_Dte.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("MM'/'DD'/'YY"));                                               //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = MM'/'DD'/'YY ) TO #WS-CHK-DTE
            if (condition(pnd_Ws_Chk_Dte.notEquals(pnd_Check_Date.getValue(i))))                                                                                          //Natural: IF #WS-CHK-DTE NE #CHECK-DATE ( I )
            {
                DbsUtil.examine(new ExamineSource(pnd_Check_Date.getValue("*"),true), new ExamineSearch(pnd_Ws_Chk_Dte), new ExamineGivingNumber(pnd_Date_Exists));       //Natural: EXAMINE FULL #CHECK-DATE ( * ) FOR #WS-CHK-DTE GIVING NUMBER #DATE-EXISTS
                if (condition(pnd_Date_Exists.equals(getZero())))                                                                                                         //Natural: IF #DATE-EXISTS = 0
                {
                    i.nadd(1);                                                                                                                                            //Natural: ADD 1 TO I
                    pnd_Check_Date.getValue(i).setValue(pnd_Ws_Chk_Dte);                                                                                                  //Natural: ASSIGN #CHECK-DATE ( I ) := #WS-CHK-DTE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Date_Exists.reset();                                                                                                                              //Natural: RESET #DATE-EXISTS
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Rec_Cnt.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #REC-CNT
            if (condition(pnd_Rec_Cnt.equals(1)))                                                                                                                         //Natural: IF #REC-CNT = 1
            {
                pnd_Header_Record_Pnd_Issue_Amount.setValue(pnd_Amt_R_Pnd_Issue_Amount_2_W);                                                                              //Natural: ASSIGN #HEADER-RECORD.#ISSUE-AMOUNT := #AMT-R.#ISSUE-AMOUNT-2-W
                pnd_Header_Record_Pnd_Issue_Count.setValue(pnd_Amt_R_Pnd_Issue_Count_W);                                                                                  //Natural: ASSIGN #HEADER-RECORD.#ISSUE-COUNT := #AMT-R.#ISSUE-COUNT-W
                //* *      #HEADER-RECORD.#ACCT-NBR     := #RPT-EXT.PYMNT-EFT-ACCT-NBR
                getWorkFiles().write(7, false, pnd_Header_Record);                                                                                                        //Natural: WRITE WORK FILE 7 #HEADER-RECORD
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Record_Pnd_Pymnt_Check_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr());                                                                 //Natural: ASSIGN #OUTPUT-RECORD.#PYMNT-CHECK-NBR := #RPT-EXT.PYMNT-CHECK-NBR
            pnd_Output_Record_Pnd_Pymnt_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                             //Natural: ASSIGN #PYMNT-CHECK-NBR-N3 := FCPA110.START-CHECK-PREFIX-N3
            //*  COMPUTE #OUTPUT-RECORD.#PYMNT-CHECK-AMT =
            //*    (#RPT-EXT.PYMNT-CHECK-AMT * 100)
            pnd_Output_Record_Pnd_Pymnt_Check_Amt.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                                 //Natural: ASSIGN #OUTPUT-RECORD.#PYMNT-CHECK-AMT := #RPT-EXT.PYMNT-CHECK-AMT
            //*  RL 5-4-06
            pnd_Output_Record_Pnd_Additional_Data.moveAll(" ");                                                                                                           //Natural: MOVE ALL ' ' TO #OUTPUT-RECORD.#ADDITIONAL-DATA
            //* ******************** RL 5-3-2006 BEGIN ********************************
            vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) FCP-CONS-PYMNT WITH PYMNT-NBR = #PYMNT-CHECK-NBR
            (
            "FIND01",
            new Wc[] { new Wc("PYMNT_NBR", "=", pnd_Output_Record_Pnd_Pymnt_Check_Nbr, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(vw_fcp_Cons_Pymnt.readNextRow("FIND01", true)))
            {
                vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
                if (condition(vw_fcp_Cons_Pymnt.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORDS FOUND
                {
                    fcp_Cons_Pymnt_Cntrct_Payee_Cde.reset();                                                                                                              //Natural: RESET FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition((fcp_Cons_Pymnt_Cntrct_Payee_Cde.equals("ALT1") || fcp_Cons_Pymnt_Cntrct_Payee_Cde.equals("ALT2")) || (fcp_Cons_Pymnt_Cntrct_Payee_Cde.notEquals("ALT1")  //Natural: IF ( FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE EQ 'ALT1' OR FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE EQ 'ALT2' ) OR ( FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE NE 'ALT1' AND FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE NE 'ALT2' AND FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE NE 'ANNT' AND SUBSTR ( #RPT-EXT.PYMNT-NME ( 1 ) ,1,3 ) EQ 'CR ' )
                && fcp_Cons_Pymnt_Cntrct_Payee_Cde.notEquals("ALT2") && fcp_Cons_Pymnt_Cntrct_Payee_Cde.notEquals("ANNT") && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1).getSubstring(1,
                3).equals("CR "))))
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(1).getSubstring(1,3).equals("CR ")))                                             //Natural: IF SUBSTRING ( PYMNT-ADDR-LINE1-TXT ( 1 ) ,1,3 ) EQ 'CR '
                {
                    pnd_Output_Record_Pnd_Payee_Nme.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(2));                                              //Natural: ASSIGN #OUTPUT-RECORD.#PAYEE-NME := PYMNT-ADDR-LINE1-TXT ( 2 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Record_Pnd_Payee_Nme.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(1));                                              //Natural: ASSIGN #OUTPUT-RECORD.#PAYEE-NME := PYMNT-ADDR-LINE1-TXT ( 1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1).getSubstring(1,3).equals("CR ")))                                                        //Natural: IF SUBSTRING ( #RPT-EXT.PYMNT-NME ( 1 ) ,1,3 ) EQ 'CR '
                {
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(1).getSubstring(1,3).equals("CR ")))                                         //Natural: IF SUBSTRING ( PYMNT-ADDR-LINE1-TXT ( 1 ) ,1,3 ) EQ 'CR '
                    {
                        pnd_Output_Record_Pnd_Payee_Nme.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(2));                                          //Natural: ASSIGN #OUTPUT-RECORD.#PAYEE-NME := PYMNT-ADDR-LINE1-TXT ( 2 )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Output_Record_Pnd_Payee_Nme.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(1));                                          //Natural: ASSIGN #OUTPUT-RECORD.#PAYEE-NME := PYMNT-ADDR-LINE1-TXT ( 1 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Record_Pnd_Payee_Nme.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1));                                                         //Natural: ASSIGN #OUTPUT-RECORD.#PAYEE-NME := #RPT-EXT.PYMNT-NME ( 1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Record_Pnd_Cntrct_Payee_Cde.setValue(fcp_Cons_Pymnt_Cntrct_Payee_Cde);                                                                             //Natural: ASSIGN #OUTPUT-RECORD.#CNTRCT-PAYEE-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
            //*  RL MAY 16 2006
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AL")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'AL'
            {
                pnd_Output_Record_Pnd_Al_Indicator.setValue("2");                                                                                                         //Natural: ASSIGN #AL-INDICATOR := '2'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Record_Pnd_Al_Indicator.setValue(" ");                                                                                                         //Natural: ASSIGN #AL-INDICATOR := ' '
            }                                                                                                                                                             //Natural: END-IF
            //*  (A10) RL MAY 12 2006
            //*  (A8)  RL MAY 12 2006
            pnd_Output_Record_Pnd_Cntrct_Ppcn_Nbr.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                               //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-PPCN-NBR TO #OUTPUT-RECORD.#CNTRCT-PPCN-NBR
            //* ********************* RL 5-3-2006 END *********************************
            //* *
            //* *  #OUTPUT-RECORD.#ACCT-NBR  :=  #RPT-EXT.PYMNT-EFT-ACCT-NBR
            pnd_Output_Record_Pnd_Void_Ind.setValue(" ");                                                                                                                 //Natural: MOVE ' ' TO #OUTPUT-RECORD.#VOID-IND
            pnd_Output_Record_Pnd_Pymnt_Check_Dte.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                            //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #OUTPUT-RECORD.#PYMNT-CHECK-DTE
            pnd_Count.nadd(1);                                                                                                                                            //Natural: COMPUTE #COUNT = #COUNT + 1
            pnd_Total.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                                                                 //Natural: COMPUTE #TOTAL = #TOTAL + #RPT-EXT.PYMNT-CHECK-AMT
            //* VIKRAM
            getWorkFiles().write(7, false, pnd_Output_Record);                                                                                                            //Natural: WRITE WORK FILE 7 #OUTPUT-RECORD
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            pnd_Diff_Count.compute(new ComputeParameters(false, pnd_Diff_Count), pnd_Header_Record_Pnd_Issue_Count.subtract(pnd_Count));                                  //Natural: ASSIGN #DIFF-COUNT := #HEADER-RECORD.#ISSUE-COUNT - #COUNT
            pnd_Diff_Total.compute(new ComputeParameters(false, pnd_Diff_Total), pnd_Header_Record_Pnd_Issue_Amount.subtract(pnd_Total));                                 //Natural: ASSIGN #DIFF-TOTAL := #HEADER-RECORD.#ISSUE-AMOUNT - #TOTAL
            pnd_Data_Present.setValue(true);                                                                                                                              //Natural: ASSIGN #DATA-PRESENT := TRUE
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"CHECK DATE(S): ",pnd_Check_Date.getValue("*"),NEWLINE,NEWLINE,"    EXTRACT TOTALS    ",new                //Natural: WRITE ( 1 ) / 'CHECK DATE(S): ' #CHECK-DATE ( * ) //'    EXTRACT TOTALS    ' 7X '    CONTROL TOTALS    ' 7X '      DIFFERENCES     ' / '  COUNT         AMOUNT' 7X '  COUNT         AMOUNT' 7X '  COUNT         AMOUNT' 7X / #COUNT ( EM = ZZZ,ZZ9 ) 3X #TOTAL ( EM = ZZZ,ZZZ,ZZZ.99 ) 7X #HEADER-RECORD.#ISSUE-COUNT ( EM = ZZZ,ZZ9 ) #HEADER-RECORD.#ISSUE-AMOUNT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 7X #DIFF-COUNT ( EM = ZZZ,ZZ9 ) 3X #DIFF-TOTAL ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) /// '*' ( 34 ) ' END OF REPORT ' '*' ( 34 )
                ColumnSpacing(7),"    CONTROL TOTALS    ",new ColumnSpacing(7),"      DIFFERENCES     ",NEWLINE,"  COUNT         AMOUNT",new ColumnSpacing(7),"  COUNT         AMOUNT",new 
                ColumnSpacing(7),"  COUNT         AMOUNT",new ColumnSpacing(7),NEWLINE,pnd_Count, new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(3),pnd_Total, 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(7),pnd_Header_Record_Pnd_Issue_Count, new ReportEditMask ("ZZ,ZZ9"),pnd_Header_Record_Pnd_Issue_Amount, 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Diff_Count, new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(3),pnd_Diff_Total, 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,NEWLINE,"*",new RepeatItem(34)," END OF REPORT ","*",new RepeatItem(34));
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Diff_Count.notEquals(getZero()) || pnd_Diff_Total.notEquals(getZero())))                                                                    //Natural: IF #DIFF-COUNT NE 0 OR #DIFF-TOTAL NE 0
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 1 )
                if (condition(Global.isEscape())){return;}
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"(ERROR MESSAGE)",NEWLINE,"FCPP195  CHECK CONTROL TOTALS   DO NOT BALANCE  ",new               //Natural: WRITE ( 1 ) // '(ERROR MESSAGE)' / 'FCPP195  CHECK CONTROL TOTALS   DO NOT BALANCE  ' 1X 'FILE    CHECK CONTROL TOTALS' // '*-----------------------------*' / '* CPS SYSTEM HAS BEEN ABORTED *' / '*-----------------------------*' / 'CNTL-REC-AMT' #HEADER-RECORD.#ISSUE-AMOUNT 'TOTAL' #TOTAL 'CNTL-REC-CNT' #HEADER-RECORD.#ISSUE-COUNT 'COUNT' #COUNT
                    ColumnSpacing(1),"FILE    CHECK CONTROL TOTALS",NEWLINE,NEWLINE,"*-----------------------------*",NEWLINE,"* CPS SYSTEM HAS BEEN ABORTED *",
                    NEWLINE,"*-----------------------------*",NEWLINE,"CNTL-REC-AMT",pnd_Header_Record_Pnd_Issue_Amount,"TOTAL",pnd_Total,"CNTL-REC-CNT",
                    pnd_Header_Record_Pnd_Issue_Count,"COUNT",pnd_Count);
                if (condition(Global.isEscape())) return;
                //*      TERMINATE 50                                           /* 05-18-98
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  VIKRAM START
        if (condition(! (pnd_Data_Present.getBoolean())))                                                                                                                 //Natural: IF NOT #DATA-PRESENT
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"CHECK DATE(S): ",pnd_Check_Date.getValue("*"),NEWLINE,NEWLINE,"    EXTRACT TOTALS    ",new                //Natural: WRITE ( 1 ) / 'CHECK DATE(S): ' #CHECK-DATE ( * ) //'    EXTRACT TOTALS    ' 7X '    CONTROL TOTALS    ' 7X '      DIFFERENCES     ' / '  COUNT         AMOUNT' 7X '  COUNT         AMOUNT' 7X '  COUNT         AMOUNT' 7X / #COUNT ( EM = ZZZ,ZZ9 ) 3X #TOTAL ( EM = ZZZ,ZZZ,ZZZ.99 ) 7X #HEADER-RECORD.#ISSUE-COUNT ( EM = ZZZ,ZZ9 ) #HEADER-RECORD.#ISSUE-AMOUNT ( EM = Z,ZZZ,ZZZ,ZZ9.99 ) 7X #DIFF-COUNT ( EM = ZZZ,ZZ9 ) 3X #DIFF-TOTAL ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) /// '*' ( 34 ) ' END OF REPORT ' '*' ( 34 )
                ColumnSpacing(7),"    CONTROL TOTALS    ",new ColumnSpacing(7),"      DIFFERENCES     ",NEWLINE,"  COUNT         AMOUNT",new ColumnSpacing(7),"  COUNT         AMOUNT",new 
                ColumnSpacing(7),"  COUNT         AMOUNT",new ColumnSpacing(7),NEWLINE,pnd_Count, new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(3),pnd_Total, 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"),new ColumnSpacing(7),pnd_Header_Record_Pnd_Issue_Count, new ReportEditMask ("ZZ,ZZ9"),pnd_Header_Record_Pnd_Issue_Amount, 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new ColumnSpacing(7),pnd_Diff_Count, new ReportEditMask ("ZZZ,ZZ9"),new ColumnSpacing(3),pnd_Diff_Total, 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,NEWLINE,"*",new RepeatItem(34)," END OF REPORT ","*",new RepeatItem(34));
            if (Global.isEscape()) return;
            //*  VIKRAM END
        }                                                                                                                                                                 //Natural: END-IF
        //* **************** RL BEGIN - PAYEE MATCH FEB 16 ******************
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************** RL END-PAYEE MATCH *************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(15),pnd_Header0_1,new TabSetting(71),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 15T #HEADER0-1 71T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YY ) 15T #HEADER0-2 71T *TIMX ( EM = HH':'II' 'AP ) /
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YY"),new 
                        TabSetting(15),pnd_Header0_2,new TabSetting(71),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=55 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
