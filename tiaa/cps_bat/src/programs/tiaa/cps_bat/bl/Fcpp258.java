/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:44 PM
**        * FROM NATURAL PROGRAM : Fcpp258
************************************************************
**        * FILE NAME            : Fcpp258.java
**        * CLASS NAME           : Fcpp258
**        * INSTANCE NAME        : Fcpp258
************************************************************
* FCPP258 - UNLOAD READY-TO-TRANSMIT VOID INFO
*
* FIND ALL RECORDS WITH CNR-CS-STOP-CANCEL-STATUS = 'C' (READY TO
* TRANSMIT) AND WRITE A WORK FILE WITH INFORMATION FOR AN EVENTUAL
* MATCH PROCESS AGAINST THE UNDUPLICATED WACHO.STOPS.CANCELS FILE
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp258 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Amt;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Stop_Select_Dt;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Cancel_Select_Dt;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt;
    private DbsField pnd_Work_Rec;

    private DbsGroup pnd_Work_Rec__R_Field_1;
    private DbsField pnd_Work_Rec_Pnd_Work_Flag;
    private DbsField pnd_Work_Rec_Pnd_Work_Isn;
    private DbsField pnd_Work_Rec_Pnd_Work_Ppcn_Nbr;
    private DbsField pnd_Work_Rec_Pnd_Work_Check_Nbr;
    private DbsField pnd_Work_Rec_Pnd_Work_Check_Amt;
    private DbsField pnd_Work_Rec_Pnd_Work_Check_Issue_Dt;
    private DbsField pnd_Recs_Read;
    private DbsField pnd_Other_Check_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnt_Pymnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        fcp_Cons_Pymnt_Pymnt_Check_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_DTE");
        fcp_Cons_Pymnt_Pymnt_Check_Amt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status", "CNR-CS-STOP-CANCEL-STATUS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNR_CS_STOP_CANCEL_STATUS");
        fcp_Cons_Pymnt_Cnr_Cs_Stop_Select_Dt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Stop_Select_Dt", "CNR-CS-STOP-SELECT-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNR_CS_STOP_SELECT_DT");
        fcp_Cons_Pymnt_Cnr_Cs_Cancel_Select_Dt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Cancel_Select_Dt", "CNR-CS-CANCEL-SELECT-DT", 
            FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, "CNR_CS_CANCEL_SELECT_DT");
        fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt", "CNR-CS-TRANSMIT-DT", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNR_CS_TRANSMIT_DT");
        registerRecord(vw_fcp_Cons_Pymnt);

        pnd_Work_Rec = localVariables.newFieldInRecord("pnd_Work_Rec", "#WORK-REC", FieldType.STRING, 80);

        pnd_Work_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Work_Rec__R_Field_1", "REDEFINE", pnd_Work_Rec);
        pnd_Work_Rec_Pnd_Work_Flag = pnd_Work_Rec__R_Field_1.newFieldInGroup("pnd_Work_Rec_Pnd_Work_Flag", "#WORK-FLAG", FieldType.STRING, 1);
        pnd_Work_Rec_Pnd_Work_Isn = pnd_Work_Rec__R_Field_1.newFieldInGroup("pnd_Work_Rec_Pnd_Work_Isn", "#WORK-ISN", FieldType.NUMERIC, 11);
        pnd_Work_Rec_Pnd_Work_Ppcn_Nbr = pnd_Work_Rec__R_Field_1.newFieldInGroup("pnd_Work_Rec_Pnd_Work_Ppcn_Nbr", "#WORK-PPCN-NBR", FieldType.STRING, 
            8);
        pnd_Work_Rec_Pnd_Work_Check_Nbr = pnd_Work_Rec__R_Field_1.newFieldInGroup("pnd_Work_Rec_Pnd_Work_Check_Nbr", "#WORK-CHECK-NBR", FieldType.NUMERIC, 
            11);
        pnd_Work_Rec_Pnd_Work_Check_Amt = pnd_Work_Rec__R_Field_1.newFieldInGroup("pnd_Work_Rec_Pnd_Work_Check_Amt", "#WORK-CHECK-AMT", FieldType.NUMERIC, 
            15, 2);
        pnd_Work_Rec_Pnd_Work_Check_Issue_Dt = pnd_Work_Rec__R_Field_1.newFieldInGroup("pnd_Work_Rec_Pnd_Work_Check_Issue_Dt", "#WORK-CHECK-ISSUE-DT", 
            FieldType.STRING, 8);
        pnd_Recs_Read = localVariables.newFieldInRecord("pnd_Recs_Read", "#RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Other_Check_Nbr = localVariables.newFieldInRecord("pnd_Other_Check_Nbr", "#OTHER-CHECK-NBR", FieldType.NUMERIC, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp258() throws Exception
    {
        super("Fcpp258");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 55 LS = 80;//Natural: FORMAT ( 1 ) PS = 55 LS = 80
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE TITLE LEFT *DATU 6X 'VOID RECONCILIATION PROCESS - UNLOAD SELECTED VOIDS' 6X *PROGRAM / *TIME ( EM = X ( 8 ) ) 6X '---------------------------------------------------' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU 6X 'VOID RECONCILIATION PROCESS - UNLOAD SELECTED VOIDS' 6X *PROGRAM / *TIME ( EM = X ( 8 ) ) 6X '---------------------------------------------------' 6X 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = Z9 ) // 'STOP' 68X 'OTHER'/ 'STAT       *ISN       PIN' 5X 'CHECK NBR    CHECK DT     CHECK AMT' 5X 'CHECK NBR' / '----       ----       ---' 5X '---------    --------     ---------' 5X '---------' //
        vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                               //Natural: FIND FCP-CONS-PYMNT WITH CNR-CS-STOP-CANCEL-STATUS = 'C'
        (
        "FD1",
        new Wc[] { new Wc("CNR_CS_STOP_CANCEL_STATUS", "=", "C", WcType.WITH) }
        );
        FD1:
        while (condition(vw_fcp_Cons_Pymnt.readNextRow("FD1")))
        {
            vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
            pnd_Recs_Read.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #RECS-READ
            pnd_Work_Rec_Pnd_Work_Flag.setValue(fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status);                                                                                //Natural: ASSIGN #WORK-FLAG = CNR-CS-STOP-CANCEL-STATUS
            pnd_Work_Rec_Pnd_Work_Isn.setValue(vw_fcp_Cons_Pymnt.getAstISN("FD1"));                                                                                       //Natural: ASSIGN #WORK-ISN = *ISN ( FD1. )
            pnd_Work_Rec_Pnd_Work_Ppcn_Nbr.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                                      //Natural: ASSIGN #WORK-PPCN-NBR = CNTRCT-PPCN-NBR
            pnd_Work_Rec_Pnd_Work_Check_Amt.setValue(fcp_Cons_Pymnt_Pymnt_Check_Amt);                                                                                     //Natural: ASSIGN #WORK-CHECK-AMT = PYMNT-CHECK-AMT
            pnd_Work_Rec_Pnd_Work_Check_Issue_Dt.setValueEdited(fcp_Cons_Pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                           //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #WORK-CHECK-ISSUE-DT
            if (condition(fcp_Cons_Pymnt_Pymnt_Nbr.notEquals(getZero())))                                                                                                 //Natural: IF PYMNT-NBR NOT = 0
            {
                pnd_Work_Rec_Pnd_Work_Check_Nbr.setValue(fcp_Cons_Pymnt_Pymnt_Nbr);                                                                                       //Natural: ASSIGN #WORK-CHECK-NBR = PYMNT-NBR
                pnd_Other_Check_Nbr.setValue(fcp_Cons_Pymnt_Pymnt_Check_Nbr);                                                                                             //Natural: ASSIGN #OTHER-CHECK-NBR = PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Rec_Pnd_Work_Check_Nbr.setValue(fcp_Cons_Pymnt_Pymnt_Check_Nbr);                                                                                 //Natural: ASSIGN #WORK-CHECK-NBR = PYMNT-CHECK-NBR
                pnd_Other_Check_Nbr.setValue(fcp_Cons_Pymnt_Pymnt_Nbr);                                                                                                   //Natural: ASSIGN #OTHER-CHECK-NBR = PYMNT-NBR
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, pnd_Work_Rec);                                                                                                                 //Natural: WRITE WORK FILE 1 #WORK-REC
            getReports().write(1, new ColumnSpacing(2),pnd_Work_Rec_Pnd_Work_Flag,pnd_Work_Rec_Pnd_Work_Isn,pnd_Work_Rec_Pnd_Work_Ppcn_Nbr,pnd_Work_Rec_Pnd_Work_Check_Nbr,new  //Natural: WRITE ( 1 ) 2X #WORK-FLAG #WORK-ISN #WORK-PPCN-NBR #WORK-CHECK-NBR 4X #WORK-CHECK-ISSUE-DT #WORK-CHECK-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 2X #OTHER-CHECK-NBR
                ColumnSpacing(4),pnd_Work_Rec_Pnd_Work_Check_Issue_Dt,pnd_Work_Rec_Pnd_Work_Check_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(2),
                pnd_Other_Check_Nbr);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  (FD1.)
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,new ColumnSpacing(23),"***********************************",NEWLINE,new ColumnSpacing(23),"*   CONTROL TOTALS FOR FCPP258    *",NEWLINE,new  //Natural: WRITE // 23X '***********************************' / 23X '*   CONTROL TOTALS FOR FCPP258    *' / 23X '***********************************'
            ColumnSpacing(23),"***********************************");
        if (Global.isEscape()) return;
        getReports().write(0, NEWLINE,NEWLINE,new ColumnSpacing(23),"  STATUS C RECS READ...",pnd_Recs_Read, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new                 //Natural: WRITE // 23X '  STATUS C RECS READ...' #RECS-READ ( EM = Z,ZZZ,ZZ9 ) / 23X '  WORK RECS WRITTEN....' #RECS-READ ( EM = Z,ZZZ,ZZ9 )
            ColumnSpacing(23),"  WORK RECS WRITTEN....",pnd_Recs_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=80");
        Global.format(1, "PS=55 LS=80");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(6),"VOID RECONCILIATION PROCESS - UNLOAD SELECTED VOIDS",new 
            ColumnSpacing(6),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(6),"---------------------------------------------------",
            NEWLINE,NEWLINE);
        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(6),"VOID RECONCILIATION PROCESS - UNLOAD SELECTED VOIDS",new 
            ColumnSpacing(6),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(6),"---------------------------------------------------",new 
            ColumnSpacing(6),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("Z9"),NEWLINE,NEWLINE,"STOP",new ColumnSpacing(68),"OTHER",NEWLINE,"STAT       *ISN       PIN",new 
            ColumnSpacing(5),"CHECK NBR    CHECK DT     CHECK AMT",new ColumnSpacing(5),"CHECK NBR",NEWLINE,"----       ----       ---",new ColumnSpacing(5),"---------    --------     ---------",new 
            ColumnSpacing(5),"---------",NEWLINE,NEWLINE);
    }
}
