/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:39 PM
**        * FROM NATURAL PROGRAM : Fcpp398
************************************************************
**        * FILE NAME            : Fcpp398.java
**        * CLASS NAME           : Fcpp398
**        * INSTANCE NAME        : Fcpp398
************************************************************
************************************************************************
* PROGRAM  : FCPP398
* SYSTEM   : CPS
*
* FUNCTION : THIS PROGRAM WILL PROPOGATE SIMPLEX/DUPLEX INDICATIOR FROM
*            RECORD TYPE 10 TO ALL RECORDS.
* HISTORY
*
*
*  03/12/99 :  R. CARREON
*  03/12/99 :  R. CARREON
*             ADDITION OF PYMNT-SPOUSE-PAY-STATS IN FCPL378
*
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*     11/02 :  R. CARREON   STOW - EGTRRA CHANGES IN  LDA
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp398 extends BLNatBase
{
    // Data Areas
    private LdaFcpl378 ldaFcpl378;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Ws_Funds_Per_Pymnt;
    private DbsField pnd_Ws_Pnd_Ws_Type;
    private DbsField pnd_Ws_Pnd_Ws_Csr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl378 = new LdaFcpl378();
        registerRecord(ldaFcpl378);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Ws_Funds_Per_Pymnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Funds_Per_Pymnt", "#WS-FUNDS-PER-PYMNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Ws_Type = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Type", "#WS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Ws_Csr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Csr", "#WS-CSR", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl378.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp398() throws Exception
    {
        super("Fcpp398");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-20-DETAIL
        while (condition(getWorkFiles().read(1, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_20_Detail())))
        {
            if (condition(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(10)))                                                                              //Natural: IF #KEY-REC-LVL-# = 10
            {
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_1().setValue(ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_1());                      //Natural: MOVE #REC-TYPE-20-DET-1 TO #REC-TYPE-10-DET-1
                ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Rec_Type_10_Det_2().setValue(ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Rec_Type_20_Det_2());                      //Natural: MOVE #REC-TYPE-20-DET-2 TO #REC-TYPE-10-DET-2
                pnd_Ws_Pnd_Ws_Type.setValue(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex());                                                          //Natural: MOVE #KEY-SIMPLEX-DUPLEX-MULTIPLEX TO #WS-TYPE
                pnd_Ws_Pnd_Ws_Csr.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde());                                                         //Natural: MOVE #REC-TYPE-10-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE TO #WS-CSR
                pnd_Ws_Pnd_Ws_Funds_Per_Pymnt.setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Funds_Per_Pymnt());                                                       //Natural: MOVE #REC-TYPE-10-DETAIL.#FUNDS-PER-PYMNT TO #WS-FUNDS-PER-PYMNT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex().setValue(pnd_Ws_Pnd_Ws_Type);                                                          //Natural: MOVE #WS-TYPE TO #KEY-SIMPLEX-DUPLEX-MULTIPLEX
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(20)))                                                                              //Natural: IF #KEY-REC-LVL-# = 20
            {
                ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct_Top().setValue(pnd_Ws_Pnd_Ws_Funds_Per_Pymnt);                                                          //Natural: MOVE #WS-FUNDS-PER-PYMNT TO #INV-ACCT-TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  LEON CN SN 08-05-99
            //*  JWO 2010-11
            if (condition(pnd_Ws_Pnd_Ws_Csr.equals("C") || pnd_Ws_Pnd_Ws_Csr.equals("S") || pnd_Ws_Pnd_Ws_Csr.equals("CN") || pnd_Ws_Pnd_Ws_Csr.equals("SN")              //Natural: IF #WS-CSR = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
                || pnd_Ws_Pnd_Ws_Csr.equals("CP") || pnd_Ws_Pnd_Ws_Csr.equals("SP") || pnd_Ws_Pnd_Ws_Csr.equals("RP") || pnd_Ws_Pnd_Ws_Csr.equals("PR")))
            {
                getWorkFiles().write(2, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_20_Detail());                                                //Natural: WRITE WORK FILE 2 #PYMNT-EXT-KEY #REC-TYPE-20-DETAIL
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(3, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_20_Detail());                                                //Natural: WRITE WORK FILE 3 #PYMNT-EXT-KEY #REC-TYPE-20-DETAIL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //
}
