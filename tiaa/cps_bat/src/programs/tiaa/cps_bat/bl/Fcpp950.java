/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:21 PM
**        * FROM NATURAL PROGRAM : Fcpp950
************************************************************
**        * FILE NAME            : Fcpp950.java
**        * CLASS NAME           : Fcpp950
**        * INSTANCE NAME        : Fcpp950
************************************************************
* 01/15/2020  : CTS CPS SUNSET (CHG694525) TAG: CTS-0115
* 08/20/2020  : CTS CPS SUNSET (CHG842595) TAG: CTS-0820
* 12/10/2020  : CTS CPS SUNSET (CHG921452) TAG: CTS-1220

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp950 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input;
    private DbsField pnd_Input_Pnd_I_Chk_Dte;
    private DbsField pnd_Input_Pnd_I_For_Dte;
    private DbsField pnd_Input_Pnd_I_Name;
    private DbsField pnd_Input_Pnd_I_Ssn;
    private DbsField pnd_Input_Pnd_I_Pin;
    private DbsField pnd_Input_Pnd_I_Stat;
    private DbsField pnd_Input_Pnd_I_Pyee_Cde;
    private DbsField pnd_Input_Pnd_I_Cntrct_Mde;
    private DbsField pnd_Input_Pnd_I_Dod;
    private DbsField pnd_Input_Pnd_I_Contact;
    private DbsField pnd_Input_Pnd_I_User_Area;
    private DbsField pnd_Input_Pnd_I_User_Id;
    private DbsField pnd_Input_Pnd_I_Nfp_Ffp;
    private DbsField pnd_Input_Pnd_I_Origin_Code;
    private DbsField pnd_Input_Pnd_I_Ck_Contract;

    private DbsGroup pnd_Input__R_Field_1;
    private DbsField pnd_Input_Pnd_I_Contract;
    private DbsField pnd_Input_Pnd_I_Filler;
    private DbsField pnd_Input_Pnd_I_Ck_Payee_Code;
    private DbsField pnd_Input_Pnd_I_Ck_Record_Status;
    private DbsField pnd_Input_Pnd_I_Ck_Form_Id;

    private DbsGroup pnd_Input__R_Field_2;
    private DbsField pnd_Input_Pnd_I_Ck_Form_Id12;
    private DbsField pnd_Input_Pnd_I_Ck_Form_Id34;
    private DbsField pnd_Input_Pnd_I_Ck_Global_Payment;
    private DbsField pnd_Input_Pnd_I_Ck_Check_Date;
    private DbsField pnd_Input_Pnd_I_Ck_Net_Amount;
    private DbsField pnd_Input_Pnd_I_Ck_Check_No;
    private DbsField pnd_Input_Pnd_I_Ck_Special_Handling;
    private DbsField pnd_Input_Pnd_I_Annotation_Ind;
    private DbsField pnd_Input_Pnd_I_Rec_Fnd;
    private DbsField status_Text;
    private DbsField payment_Type;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Rpt;
    private DbsField pnd_Prev_Contract;
    private DbsField pnd_Prev_Pyee_Cde;

    private DataAccessProgramView vw_payment;
    private DbsField payment_Pymnt_Annot_Ind;
    private DbsField payment_Pymnt_Cmbne_Ind;
    private DbsField payment_Cntrct_Ppcn_Nbr;
    private DbsField payment_Cntrct_Payee_Cde;
    private DbsField payment_Cntrct_Orgn_Cde;
    private DbsField payment_Cntrct_Cmbn_Nbr;
    private DbsField payment_Pymnt_Pay_Type_Req_Ind;
    private DbsField payment_Pymnt_Stats_Cde;
    private DbsField payment_Cntrct_Hold_Cde;
    private DbsField payment_Pymnt_Check_Dte;
    private DbsField payment_Pymnt_Check_Amt;
    private DbsField payment_Pymnt_Check_Scrty_Nbr;
    private DbsField payment_Pymnt_Nbr;
    private DbsField payment_Notused1;
    private DbsField payment_Cntrct_Cancel_Rdrw_Actvty_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Input = localVariables.newGroupInRecord("pnd_Input", "#INPUT");
        pnd_Input_Pnd_I_Chk_Dte = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Chk_Dte", "#I-CHK-DTE", FieldType.STRING, 8);
        pnd_Input_Pnd_I_For_Dte = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_For_Dte", "#I-FOR-DTE", FieldType.STRING, 8);
        pnd_Input_Pnd_I_Name = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Name", "#I-NAME", FieldType.STRING, 38);
        pnd_Input_Pnd_I_Ssn = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ssn", "#I-SSN", FieldType.STRING, 9);
        pnd_Input_Pnd_I_Pin = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Pin", "#I-PIN", FieldType.NUMERIC, 12);
        pnd_Input_Pnd_I_Stat = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Stat", "#I-STAT", FieldType.STRING, 2);
        pnd_Input_Pnd_I_Pyee_Cde = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Pyee_Cde", "#I-PYEE-CDE", FieldType.STRING, 2);
        pnd_Input_Pnd_I_Cntrct_Mde = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Cntrct_Mde", "#I-CNTRCT-MDE", FieldType.NUMERIC, 3);
        pnd_Input_Pnd_I_Dod = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Dod", "#I-DOD", FieldType.STRING, 8);
        pnd_Input_Pnd_I_Contact = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Contact", "#I-CONTACT", FieldType.STRING, 35);
        pnd_Input_Pnd_I_User_Area = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_User_Area", "#I-USER-AREA", FieldType.STRING, 6);
        pnd_Input_Pnd_I_User_Id = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_User_Id", "#I-USER-ID", FieldType.STRING, 8);
        pnd_Input_Pnd_I_Nfp_Ffp = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Nfp_Ffp", "#I-NFP-FFP", FieldType.STRING, 1);
        pnd_Input_Pnd_I_Origin_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Origin_Code", "#I-ORIGIN-CODE", FieldType.STRING, 2);
        pnd_Input_Pnd_I_Ck_Contract = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Contract", "#I-CK-CONTRACT", FieldType.STRING, 10);

        pnd_Input__R_Field_1 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_1", "REDEFINE", pnd_Input_Pnd_I_Ck_Contract);
        pnd_Input_Pnd_I_Contract = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_I_Contract", "#I-CONTRACT", FieldType.STRING, 8);
        pnd_Input_Pnd_I_Filler = pnd_Input__R_Field_1.newFieldInGroup("pnd_Input_Pnd_I_Filler", "#I-FILLER", FieldType.STRING, 2);
        pnd_Input_Pnd_I_Ck_Payee_Code = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Payee_Code", "#I-CK-PAYEE-CODE", FieldType.STRING, 2);
        pnd_Input_Pnd_I_Ck_Record_Status = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Record_Status", "#I-CK-RECORD-STATUS", FieldType.STRING, 1);
        pnd_Input_Pnd_I_Ck_Form_Id = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Form_Id", "#I-CK-FORM-ID", FieldType.STRING, 4);

        pnd_Input__R_Field_2 = pnd_Input.newGroupInGroup("pnd_Input__R_Field_2", "REDEFINE", pnd_Input_Pnd_I_Ck_Form_Id);
        pnd_Input_Pnd_I_Ck_Form_Id12 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_I_Ck_Form_Id12", "#I-CK-FORM-ID12", FieldType.STRING, 2);
        pnd_Input_Pnd_I_Ck_Form_Id34 = pnd_Input__R_Field_2.newFieldInGroup("pnd_Input_Pnd_I_Ck_Form_Id34", "#I-CK-FORM-ID34", FieldType.STRING, 2);
        pnd_Input_Pnd_I_Ck_Global_Payment = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Global_Payment", "#I-CK-GLOBAL-PAYMENT", FieldType.STRING, 1);
        pnd_Input_Pnd_I_Ck_Check_Date = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Check_Date", "#I-CK-CHECK-DATE", FieldType.STRING, 8);
        pnd_Input_Pnd_I_Ck_Net_Amount = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Net_Amount", "#I-CK-NET-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Input_Pnd_I_Ck_Check_No = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Check_No", "#I-CK-CHECK-NO", FieldType.NUMERIC, 11);
        pnd_Input_Pnd_I_Ck_Special_Handling = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Ck_Special_Handling", "#I-CK-SPECIAL-HANDLING", FieldType.STRING, 
            4);
        pnd_Input_Pnd_I_Annotation_Ind = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Annotation_Ind", "#I-ANNOTATION-IND", FieldType.STRING, 1);
        pnd_Input_Pnd_I_Rec_Fnd = pnd_Input.newFieldInGroup("pnd_Input_Pnd_I_Rec_Fnd", "#I-REC-FND", FieldType.STRING, 1);
        status_Text = localVariables.newFieldInRecord("status_Text", "STATUS-TEXT", FieldType.STRING, 16);
        payment_Type = localVariables.newFieldInRecord("payment_Type", "PAYMENT-TYPE", FieldType.STRING, 8);
        pnd_Check_Date = localVariables.newFieldInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.STRING, 8);
        pnd_Rpt = localVariables.newFieldInRecord("pnd_Rpt", "#RPT", FieldType.STRING, 3);
        pnd_Prev_Contract = localVariables.newFieldInRecord("pnd_Prev_Contract", "#PREV-CONTRACT", FieldType.STRING, 8);
        pnd_Prev_Pyee_Cde = localVariables.newFieldInRecord("pnd_Prev_Pyee_Cde", "#PREV-PYEE-CDE", FieldType.STRING, 2);

        vw_payment = new DataAccessProgramView(new NameInfo("vw_payment", "PAYMENT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        payment_Pymnt_Annot_Ind = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_ANNOT_IND");
        payment_Pymnt_Cmbne_Ind = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CMBNE_IND");
        payment_Cntrct_Ppcn_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        payment_Cntrct_Payee_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        payment_Cntrct_Orgn_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        payment_Cntrct_Cmbn_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        payment_Pymnt_Pay_Type_Req_Ind = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        payment_Pymnt_Stats_Cde = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        payment_Cntrct_Hold_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_CDE");
        payment_Pymnt_Check_Dte = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        payment_Pymnt_Check_Amt = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        payment_Pymnt_Check_Scrty_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SCRTY_NBR");
        payment_Pymnt_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        payment_Notused1 = vw_payment.getRecord().newFieldInGroup("payment_Notused1", "NOTUSED1", FieldType.STRING, 8, RepeatingFieldStrategy.None, "NOTUSED1");
        payment_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_payment.reset();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp950() throws Exception
    {
        super("Fcpp950");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        READWORK01:                                                                                                                                                       //Natural: FORMAT ( 1 ) PS = 60 LS = 250;//Natural: READ WORK 1 RECORD #INPUT
        while (condition(getWorkFiles().read(1, pnd_Input)))
        {
            if (condition(pnd_Input_Pnd_I_Nfp_Ffp.equals("Y")))                                                                                                           //Natural: IF #I-NFP-FFP = 'Y'
            {
                pnd_Rpt.setValue("NFP");                                                                                                                                  //Natural: ASSIGN #RPT := 'NFP'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Rpt.setValue("FFP");                                                                                                                                  //Natural: ASSIGN #RPT := 'FFP'
            }                                                                                                                                                             //Natural: END-IF
            //*  CTS-1220 >>
            if (condition(pnd_Prev_Contract.notEquals(pnd_Input_Pnd_I_Contract)))                                                                                         //Natural: IF #PREV-CONTRACT NOT EQUAL #I-CONTRACT
            {
                                                                                                                                                                          //Natural: PERFORM READ-PAYMENT
                sub_Read_Payment();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Contract.setValue(pnd_Input_Pnd_I_Contract);                                                                                                     //Natural: MOVE #I-CONTRACT TO #PREV-CONTRACT
                pnd_Prev_Pyee_Cde.setValue(pnd_Input_Pnd_I_Pyee_Cde);                                                                                                     //Natural: MOVE #I-PYEE-CDE TO #PREV-PYEE-CDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Prev_Pyee_Cde.notEquals(pnd_Input_Pnd_I_Pyee_Cde)))                                                                                     //Natural: IF #PREV-PYEE-CDE NOT EQUAL #I-PYEE-CDE
                {
                                                                                                                                                                          //Natural: PERFORM READ-PAYMENT
                    sub_Read_Payment();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Prev_Contract.setValue(pnd_Input_Pnd_I_Contract);                                                                                                 //Natural: MOVE #I-CONTRACT TO #PREV-CONTRACT
                    pnd_Prev_Pyee_Cde.setValue(pnd_Input_Pnd_I_Pyee_Cde);                                                                                                 //Natural: MOVE #I-PYEE-CDE TO #PREV-PYEE-CDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  CTS-1220 <<
            //*  CTS-0115 >>
            if (condition(pnd_Input_Pnd_I_Rec_Fnd.equals("Y")))                                                                                                           //Natural: IF #I-REC-FND EQ 'Y'
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT
                sub_Process_Input();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  ELSE                        /* CTS-1220
                //*  CTS-0115 <<
                //*    PERFORM READ-PAYMENT      /* CTS-1220
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PAYMENT
        //*  CTS-0115 >>
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT
        //*  CTS-0115 <<
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PAYMENT
    }
    private void sub_Read_Payment() throws Exception                                                                                                                      //Natural: READ-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        status_Text.reset();                                                                                                                                              //Natural: RESET STATUS-TEXT PAYMENT-TYPE
        payment_Type.reset();
        vw_payment.startDatabaseRead                                                                                                                                      //Natural: READ PAYMENT BY PPCN-INV-ORGN-PRCSS-INST STARTING FROM #I-CONTRACT
        (
        "READ02",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Input_Pnd_I_Contract, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") }
        );
        READ02:
        while (condition(vw_payment.readNextRow("READ02")))
        {
            if (condition(payment_Cntrct_Ppcn_Nbr.notEquals(pnd_Input_Pnd_I_Contract)))                                                                                   //Natural: IF CNTRCT-PPCN-NBR NE #I-CONTRACT
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Check_Date.setValueEdited(payment_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                        //Natural: MOVE EDITED PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATE
            if (condition(pnd_Check_Date.lessOrEqual(pnd_Input_Pnd_I_Dod)))                                                                                               //Natural: IF #CHECK-DATE <= #I-DOD
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet113 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE PYMNT-STATS-CDE;//Natural: VALUE 'C'
            if (condition((payment_Pymnt_Stats_Cde.equals("C"))))
            {
                decideConditionsMet113++;
                status_Text.setValue("CASHED");                                                                                                                           //Natural: ASSIGN STATUS-TEXT := 'CASHED'
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR")))                                                                                        //Natural: IF CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'PR'
                {
                    status_Text.setValue("PEND RETR");                                                                                                                    //Natural: ASSIGN STATUS-TEXT := 'PEND RETR'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: VALUE 'P', 'W'
            else if (condition((payment_Pymnt_Stats_Cde.equals("P") || payment_Pymnt_Stats_Cde.equals("W"))))
            {
                decideConditionsMet113++;
                status_Text.setValue("PROCESSED");                                                                                                                        //Natural: ASSIGN STATUS-TEXT := 'PROCESSED'
                if (condition(payment_Notused1.equals("EFT REJ")))                                                                                                        //Natural: IF PAYMENT.NOTUSED1 = 'EFT REJ'
                {
                    status_Text.setValue("REJECTED");                                                                                                                     //Natural: ASSIGN STATUS-TEXT := 'REJECTED'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Notused1.equals("EFT REV")))                                                                                                        //Natural: IF PAYMENT.NOTUSED1 = 'EFT REV'
                {
                    status_Text.setValue("REVERSED");                                                                                                                     //Natural: ASSIGN STATUS-TEXT := 'REVERSED'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN'
                {
                    status_Text.setValue("CANC NO RD");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'CANC NO RD'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SN'
                {
                    status_Text.setValue("STOP NO RD");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'STOP NO RD'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CP'
                {
                    status_Text.setValue("CANC PEND");                                                                                                                    //Natural: ASSIGN STATUS-TEXT := 'CANC PEND'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SP'
                {
                    status_Text.setValue("STOP PEND");                                                                                                                    //Natural: ASSIGN STATUS-TEXT := 'STOP PEND'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'RP'
                {
                    status_Text.setValue("RDRW PEND");                                                                                                                    //Natural: ASSIGN STATUS-TEXT := 'RDRW PEND'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'PR'
                {
                    status_Text.setValue("PEND RETR");                                                                                                                    //Natural: ASSIGN STATUS-TEXT := 'PEND RETR'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C")))                                                                                         //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C'
                {
                    status_Text.setValue("CANCELLED");                                                                                                                    //Natural: ASSIGN STATUS-TEXT := 'CANCELLED'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S")))                                                                                         //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'S'
                {
                    status_Text.setValue("STOPPED");                                                                                                                      //Natural: ASSIGN STATUS-TEXT := 'STOPPED'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                         //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
                {
                    status_Text.setValue("REDRAWN");                                                                                                                      //Natural: ASSIGN STATUS-TEXT := 'REDRAWN'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("MA")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'MA'
                {
                    status_Text.setValue("MAN ST NR");                                                                                                                    //Natural: ASSIGN STATUS-TEXT := 'MAN ST NR'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("MB")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'MB'
                {
                    status_Text.setValue("MAN ST RE");                                                                                                                    //Natural: ASSIGN STATUS-TEXT := 'MAN ST RE'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("MC")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'MC'
                {
                    status_Text.setValue("MAN CAN NR");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'MAN CAN NR'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("MD")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'MD'
                {
                    status_Text.setValue("MAN CAN RE");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'MAN CAN RE'
                    //*  VALUES 'D','W'(WAITING),'X'(CANCELLED)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                status_Text.setValue("WAIT PRCSD");                                                                                                                       //Natural: ASSIGN STATUS-TEXT := 'WAIT PRCSD'
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C") || payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN")))                                   //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'CN'
                {
                    status_Text.setValue("WAIT CANCL");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'WAIT CANCL'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S") || payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN")))                                   //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'S' OR = 'SN'
                {
                    status_Text.setValue("WAIT STOPD");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'WAIT STOPD'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CP'
                {
                    status_Text.setValue("WAIT CP");                                                                                                                      //Natural: ASSIGN STATUS-TEXT := 'WAIT CP'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SP'
                {
                    status_Text.setValue("WAIT SP");                                                                                                                      //Natural: ASSIGN STATUS-TEXT := 'WAIT SP'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'RP'
                {
                    status_Text.setValue("WAIT RP");                                                                                                                      //Natural: ASSIGN STATUS-TEXT := 'WAIT RP'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'PR'
                {
                    status_Text.setValue("WAIT PR");                                                                                                                      //Natural: ASSIGN STATUS-TEXT := 'WAIT PR'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                         //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
                {
                    status_Text.setValue("WAIT RDRWN");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'WAIT RDRWN'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("AS")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'AS'
                {
                    status_Text.setValue("WAIT AR ST");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'WAIT AR ST'
                }                                                                                                                                                         //Natural: END-IF
                if (condition(payment_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("AP")))                                                                                        //Natural: IF PAYMENT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'AP'
                {
                    status_Text.setValue("WAIT AR PN");                                                                                                                   //Natural: ASSIGN STATUS-TEXT := 'WAIT AR PN'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet197 = 0;                                                                                                                             //Natural: DECIDE ON FIRST PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((payment_Pymnt_Pay_Type_Req_Ind.equals(1))))
            {
                decideConditionsMet197++;
                payment_Type.setValue("CHECK");                                                                                                                           //Natural: MOVE 'CHECK' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((payment_Pymnt_Pay_Type_Req_Ind.equals(2))))
            {
                decideConditionsMet197++;
                payment_Type.setValue("EFT");                                                                                                                             //Natural: MOVE 'EFT' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((payment_Pymnt_Pay_Type_Req_Ind.equals(3))))
            {
                decideConditionsMet197++;
                payment_Type.setValue("GLB CHK");                                                                                                                         //Natural: MOVE 'GLB CHK' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((payment_Pymnt_Pay_Type_Req_Ind.equals(4))))
            {
                decideConditionsMet197++;
                payment_Type.setValue("GLB USD");                                                                                                                         //Natural: MOVE 'GLB USD' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((payment_Pymnt_Pay_Type_Req_Ind.equals(5))))
            {
                decideConditionsMet197++;
                payment_Type.setValue("ADVICE");                                                                                                                          //Natural: MOVE 'ADVICE' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((payment_Pymnt_Pay_Type_Req_Ind.equals(7))))
            {
                decideConditionsMet197++;
                payment_Type.setValue("GLB WIRE");                                                                                                                        //Natural: MOVE 'GLB WIRE' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((payment_Pymnt_Pay_Type_Req_Ind.equals(8))))
            {
                decideConditionsMet197++;
                payment_Type.setValue("INT ROLL");                                                                                                                        //Natural: MOVE 'INT ROLL' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((payment_Pymnt_Pay_Type_Req_Ind.equals(9))))
            {
                decideConditionsMet197++;
                payment_Type.setValue("GLB EFT");                                                                                                                         //Natural: MOVE 'GLB EFT' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(payment_Cntrct_Ppcn_Nbr.notEquals(payment_Cntrct_Cmbn_Nbr.getSubstring(1,8))))                                                                  //Natural: IF CNTRCT-PPCN-NBR NE SUBSTRING ( CNTRCT-CMBN-NBR,1,8 )
            {
                payment_Pymnt_Check_Amt.reset();                                                                                                                          //Natural: RESET PYMNT-CHECK-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(payment_Pymnt_Cmbne_Ind.notEquals("Y")))                                                                                                        //Natural: IF PYMNT-CMBNE-IND NE 'Y'
            {
                payment_Cntrct_Cmbn_Nbr.reset();                                                                                                                          //Natural: RESET CNTRCT-CMBN-NBR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(payment_Pymnt_Pay_Type_Req_Ind.equals(2)))                                                                                                      //Natural: IF PYMNT-PAY-TYPE-REQ-IND = 2
            {
                payment_Pymnt_Nbr.setValue(payment_Pymnt_Check_Scrty_Nbr);                                                                                                //Natural: MOVE PYMNT-CHECK-SCRTY-NBR TO PYMNT-NBR
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PRINT-PAYMENT
            sub_Print_Payment();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Process_Input() throws Exception                                                                                                                     //Natural: PROCESS-INPUT
    {
        if (BLNatReinput.isReinput()) return;

        status_Text.reset();                                                                                                                                              //Natural: RESET STATUS-TEXT PAYMENT-TYPE CNTRCT-CMBN-NBR
        payment_Type.reset();
        payment_Cntrct_Cmbn_Nbr.reset();
        if (condition(pnd_Input_Pnd_I_Ck_Check_Date.lessOrEqual(pnd_Input_Pnd_I_Dod)))                                                                                    //Natural: IF #I-CK-CHECK-DATE <= #I-DOD
        {
            //*  ESCAPE BOTTOM                      /*CTS-0820
            //* CTS-0820
            Global.setEscapeCode(EscapeType.Top); if (true) return;                                                                                                       //Natural: ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-IF
        payment_Pymnt_Check_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Pnd_I_Ck_Check_Date);                                                             //Natural: MOVE EDITED #I-CK-CHECK-DATE TO PYMNT-CHECK-DTE ( EM = YYYYMMDD )
        payment_Cntrct_Orgn_Cde.setValue(pnd_Input_Pnd_I_Origin_Code);                                                                                                    //Natural: MOVE #I-ORIGIN-CODE TO CNTRCT-ORGN-CDE
        payment_Cntrct_Ppcn_Nbr.setValue(pnd_Input_Pnd_I_Contract);                                                                                                       //Natural: MOVE #I-CONTRACT TO CNTRCT-PPCN-NBR
        payment_Cntrct_Payee_Cde.setValue(pnd_Input_Pnd_I_Ck_Payee_Code);                                                                                                 //Natural: MOVE #I-CK-PAYEE-CODE TO CNTRCT-PAYEE-CDE
        payment_Pymnt_Check_Amt.setValue(pnd_Input_Pnd_I_Ck_Net_Amount);                                                                                                  //Natural: MOVE #I-CK-NET-AMOUNT TO PYMNT-CHECK-AMT
        payment_Pymnt_Nbr.setValue(pnd_Input_Pnd_I_Ck_Check_No);                                                                                                          //Natural: MOVE #I-CK-CHECK-NO TO PYMNT-NBR
        payment_Pymnt_Annot_Ind.setValue(pnd_Input_Pnd_I_Annotation_Ind);                                                                                                 //Natural: MOVE #I-ANNOTATION-IND TO PYMNT-ANNOT-IND
        payment_Cntrct_Hold_Cde.setValue(pnd_Input_Pnd_I_Ck_Special_Handling);                                                                                            //Natural: MOVE #I-CK-SPECIAL-HANDLING TO CNTRCT-HOLD-CDE
        short decideConditionsMet244 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST #I-CK-RECORD-STATUS;//Natural: VALUE 'O'
        if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("O"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("OUTSTANDING");                                                                                                                          //Natural: MOVE 'OUTSTANDING' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("C"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("CLEARED");                                                                                                                              //Natural: MOVE 'CLEARED' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'W'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("W"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("WAITING TO POST");                                                                                                                      //Natural: MOVE 'WAITING TO POST' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'V'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("V"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("VOID");                                                                                                                                 //Natural: MOVE 'VOID' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("S"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("STOPPED");                                                                                                                              //Natural: MOVE 'STOPPED' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("R"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("REDEPOSITED");                                                                                                                          //Natural: MOVE 'REDEPOSITED' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'E'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("E"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("ESCHEATMENT");                                                                                                                          //Natural: MOVE 'ESCHEATMENT' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'F'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("F"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("FORFEITURE");                                                                                                                           //Natural: MOVE 'FORFEITURE' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'N'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("N"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("NET TO TRUSTEE");                                                                                                                       //Natural: MOVE 'NET TO TRUSTEE' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'X'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("X"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("RETURN TO ACCT");                                                                                                                       //Natural: MOVE 'RETURN TO ACCT' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: VALUE 'J'
        else if (condition((pnd_Input_Pnd_I_Ck_Record_Status.equals("J"))))
        {
            decideConditionsMet244++;
            status_Text.setValue("REJECTED EFT");                                                                                                                         //Natural: MOVE 'REJECTED EFT' TO STATUS-TEXT
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        short decideConditionsMet270 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I-CK-FORM-ID12 = 'CK'
        if (condition(pnd_Input_Pnd_I_Ck_Form_Id12.equals("CK")))
        {
            decideConditionsMet270++;
            payment_Type.setValue("CHECK");                                                                                                                               //Natural: MOVE 'CHECK' TO PAYMENT-TYPE
        }                                                                                                                                                                 //Natural: WHEN #I-CK-FORM-ID12 = 'EF'
        else if (condition(pnd_Input_Pnd_I_Ck_Form_Id12.equals("EF")))
        {
            decideConditionsMet270++;
            payment_Type.setValue("EFT");                                                                                                                                 //Natural: MOVE 'EFT' TO PAYMENT-TYPE
        }                                                                                                                                                                 //Natural: WHEN #I-CK-FORM-ID12 = 'AV'
        else if (condition(pnd_Input_Pnd_I_Ck_Form_Id12.equals("AV")))
        {
            decideConditionsMet270++;
            payment_Type.setValue("ADVICE");                                                                                                                              //Natural: MOVE 'ADVICE' TO PAYMENT-TYPE
        }                                                                                                                                                                 //Natural: WHEN #I-CK-FORM-ID = 'IDST'
        else if (condition(pnd_Input_Pnd_I_Ck_Form_Id.equals("IDST")))
        {
            decideConditionsMet270++;
            payment_Type.setValue("INT ROLL");                                                                                                                            //Natural: MOVE 'INT ROLL' TO PAYMENT-TYPE
        }                                                                                                                                                                 //Natural: WHEN #I-CK-FORM-ID = 'GLBL'
        else if (condition(pnd_Input_Pnd_I_Ck_Form_Id.equals("GLBL")))
        {
            decideConditionsMet270++;
            if (condition(pnd_Input_Pnd_I_Ck_Global_Payment.equals("C")))                                                                                                 //Natural: IF #I-CK-GLOBAL-PAYMENT = 'C'
            {
                payment_Type.setValue("GLB CHK");                                                                                                                         //Natural: MOVE 'GLB CHK' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                payment_Type.setValue("GLB EFT");                                                                                                                         //Natural: MOVE 'GLB EFT' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #I-CK-FORM-ID = 'WIRE'
        else if (condition(pnd_Input_Pnd_I_Ck_Form_Id.equals("WIRE")))
        {
            decideConditionsMet270++;
            if (condition(pnd_Input_Pnd_I_Origin_Code.equals("AP")))                                                                                                      //Natural: IF #I-ORIGIN-CODE = 'AP'
            {
                payment_Type.setValue("MAN GLBL");                                                                                                                        //Natural: MOVE 'MAN GLBL' TO PAYMENT-TYPE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM PRINT-PAYMENT
        sub_Print_Payment();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Print_Payment() throws Exception                                                                                                                     //Natural: PRINT-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(1, new ReportZeroPrint (false),"SSN",                                                                                                        //Natural: DISPLAY ( 1 ) ( ZP = OFF ) 'SSN' #I-SSN ( ZP = ON ) 'Report/Date' #I-FOR-DTE 'Name' #I-NAME 'PIN' #I-PIN 'Contract' #I-CONTRACT 'Payee/Code' #I-PYEE-CDE 'Mode/Code' #I-CNTRCT-MDE 'DOD' #I-DOD 'ContactName' #I-CONTACT 'PAYMENT/DATE' PYMNT-CHECK-DTE 'SYS/SRC' CNTRCT-ORGN-CDE 'CPS/CONTRACT' CNTRCT-PPCN-NBR 'PAYEE/CODE' CNTRCT-PAYEE-CDE 'PAYMENT/AMOUNT' PYMNT-CHECK-AMT 'PAYMENT/NUMBER' PYMNT-NBR 'COMB/CTR' CNTRCT-CMBN-NBR 'ANNOTATED' PYMNT-ANNOT-IND / 'HOLD/CODE' CNTRCT-HOLD-CDE 'STATUS' STATUS-TEXT 'PAYMENT/TYPE' PAYMENT-TYPE 'Report/Type' #RPT
        		pnd_Input_Pnd_I_Ssn, new ReportZeroPrint (true),"Report/Date",
        		pnd_Input_Pnd_I_For_Dte,"Name",
        		pnd_Input_Pnd_I_Name,"PIN",
        		pnd_Input_Pnd_I_Pin,"Contract",
        		pnd_Input_Pnd_I_Contract,"Payee/Code",
        		pnd_Input_Pnd_I_Pyee_Cde,"Mode/Code",
        		pnd_Input_Pnd_I_Cntrct_Mde,"DOD",
        		pnd_Input_Pnd_I_Dod,"ContactName",
        		pnd_Input_Pnd_I_Contact,"PAYMENT/DATE",
        		payment_Pymnt_Check_Dte,"SYS/SRC",
        		payment_Cntrct_Orgn_Cde,"CPS/CONTRACT",
        		payment_Cntrct_Ppcn_Nbr,"PAYEE/CODE",
        		payment_Cntrct_Payee_Cde,"PAYMENT/AMOUNT",
        		payment_Pymnt_Check_Amt,"PAYMENT/NUMBER",
        		payment_Pymnt_Nbr,"COMB/CTR",
        		payment_Cntrct_Cmbn_Nbr,"ANNOTATED",
        		payment_Pymnt_Annot_Ind,NEWLINE,"HOLD/CODE",
        		payment_Cntrct_Hold_Cde,"STATUS",
        		status_Text,"PAYMENT/TYPE",
        		payment_Type,"Report/Type",
        		pnd_Rpt);
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=250");

        getReports().setDisplayColumns(1, new ReportZeroPrint (false),"SSN",
        		pnd_Input_Pnd_I_Ssn, new ReportZeroPrint (true),"Report/Date",
        		pnd_Input_Pnd_I_For_Dte,"Name",
        		pnd_Input_Pnd_I_Name,"PIN",
        		pnd_Input_Pnd_I_Pin,"Contract",
        		pnd_Input_Pnd_I_Contract,"Payee/Code",
        		pnd_Input_Pnd_I_Pyee_Cde,"Mode/Code",
        		pnd_Input_Pnd_I_Cntrct_Mde,"DOD",
        		pnd_Input_Pnd_I_Dod,"ContactName",
        		pnd_Input_Pnd_I_Contact,"PAYMENT/DATE",
        		payment_Pymnt_Check_Dte,"SYS/SRC",
        		payment_Cntrct_Orgn_Cde,"CPS/CONTRACT",
        		payment_Cntrct_Ppcn_Nbr,"PAYEE/CODE",
        		payment_Cntrct_Payee_Cde,"PAYMENT/AMOUNT",
        		payment_Pymnt_Check_Amt,"PAYMENT/NUMBER",
        		payment_Pymnt_Nbr,"COMB/CTR",
        		payment_Cntrct_Cmbn_Nbr,"ANNOTATED",
        		payment_Pymnt_Annot_Ind,NEWLINE,"HOLD/CODE",
        		payment_Cntrct_Hold_Cde,"STATUS",
        		status_Text,"PAYMENT/TYPE",
        		payment_Type,"Report/Type",
        		pnd_Rpt);
    }
}
