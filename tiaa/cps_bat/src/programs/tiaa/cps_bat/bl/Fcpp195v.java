/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:58 PM
**        * FROM NATURAL PROGRAM : Fcpp195v
************************************************************
**        * FILE NAME            : Fcpp195v.java
**        * CLASS NAME           : Fcpp195v
**        * INSTANCE NAME        : Fcpp195v
************************************************************
************************************************************************
* PROGRAM  : FCPP195V
* SYSTEM   : CONSOLIDATED PAYMENT SYSTEM
* TITLE    : CREATE DELETE/VOID RECORDS FOR WACHOVIA IN POS-PAY FORMAT.
* WRITTEN  : LANDRUM - AUG 17, 2006
*
* LANDRUM 4-10-2007 REMOVE ANY DUPLICATES.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp195v extends BLNatBase
{
    // Data Areas
    private PdaCpsa110 pdaCpsa110;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Input_Stop_Rec;
    private DbsField pnd_Input_Stop_Rec_Pnd_Customer_Id_Num_D;
    private DbsField pnd_Input_Stop_Rec_Pnd_Record_Type;
    private DbsField pnd_Input_Stop_Rec_Pnd_Bank_Num;
    private DbsField pnd_Input_Stop_Rec_Pnd_Account_Num;
    private DbsField pnd_Input_Stop_Rec_Pnd_Run_Date;
    private DbsField pnd_Input_Stop_Rec_Pnd_Run_Time;
    private DbsField pnd_Input_Stop_Rec_Pnd_Input_Type;
    private DbsField pnd_Input_Stop_Rec_Pnd_Stop_Type;
    private DbsField pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num;

    private DbsGroup pnd_Input_Stop_Rec__R_Field_1;
    private DbsField pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N1;
    private DbsField pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10;

    private DbsGroup pnd_Input_Stop_Rec__R_Field_2;
    private DbsField pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N3;
    private DbsField pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N7;
    private DbsField pnd_Input_Stop_Rec_Pnd_Ending_Check_Num;
    private DbsField pnd_Input_Stop_Rec_Pnd_Check_Amt;

    private DbsGroup pnd_Input_Stop_Rec__R_Field_3;
    private DbsField pnd_Input_Stop_Rec_Pnd_Check_Amt_N5;
    private DbsField pnd_Input_Stop_Rec_Pnd_Check_Amt_N10;
    private DbsField pnd_Input_Stop_Rec_Pnd_Check_Issue_Date_N;

    private DbsGroup pnd_Input_Stop_Rec__R_Field_4;
    private DbsField pnd_Input_Stop_Rec_Pnd_Check_Issue_Date_A;
    private DbsField pnd_Input_Stop_Rec_Pnd_Payee_Name;
    private DbsField pnd_Input_Stop_Rec_Pnd_Reason;
    private DbsField pnd_Input_Stop_Rec_Pnd_Auto_Check_Paid_Inquiry;
    private DbsField pnd_Input_Stop_Rec_Pnd_Stop_Duration_Years;
    private DbsField pnd_Input_Stop_Rec_Pnd_User_Id;

    private DbsGroup pnd_Trailer_Rec_T;
    private DbsField pnd_Trailer_Rec_T_Pnd_Customer_Id_Num_T;
    private DbsField pnd_Trailer_Rec_T_Pnd_Record_Type;
    private DbsField pnd_Trailer_Rec_T_Pnd_Filler_01;

    private DbsGroup pnd_Trailer_Rec_T__R_Field_5;
    private DbsField pnd_Trailer_Rec_T_Pnd_Filler_01_A;
    private DbsField pnd_Trailer_Rec_T_Pnd_Filler_01_B;
    private DbsField pnd_Trailer_Rec_T_Pnd_Total_Stops;
    private DbsField pnd_Trailer_Rec_T_Pnd_Filler_02;

    private DbsGroup pnd_Output_Void_Rec;
    private DbsField pnd_Output_Void_Rec_Pnd_Acct_Nbr;
    private DbsField pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr;

    private DbsGroup pnd_Output_Void_Rec__R_Field_6;
    private DbsField pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr_N3;
    private DbsField pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr_N7;
    private DbsField pnd_Output_Void_Rec_Pnd_Pymnt_Check_Amt;
    private DbsField pnd_Output_Void_Rec_Pnd_Pymnt_Check_Dte;
    private DbsField pnd_Output_Void_Rec_Pnd_Void_Ind;
    private DbsField pnd_Output_Void_Rec_Pnd_Additional_Data;

    private DbsGroup pnd_Output_Void_Rec__R_Field_7;
    private DbsField pnd_Output_Void_Rec_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Output_Void_Rec_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Output_Void_Rec_Pnd_Al_Indicator;
    private DbsField pnd_Output_Void_Rec_Pnd_Filler1;
    private DbsField pnd_Output_Void_Rec_Pnd_Filo1;
    private DbsField pnd_Output_Void_Rec_Pnd_Payee_Nme;
    private DbsField pnd_Output_Void_Rec_Pnd_Fil02;

    private DbsGroup pnd_Header_Record;
    private DbsField pnd_Header_Record_Pnd_Recon_Hdr;
    private DbsField pnd_Header_Record_Pnd_Bank_No;
    private DbsField pnd_Header_Record_Pnd_Acct_Nbr;
    private DbsField pnd_Header_Record_Pnd_Acct_Total;
    private DbsField pnd_Header_Record_Pnd_Acct_Count;
    private DbsField pnd_Header_Record_Fil03;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Pymnt_Bank_Account;
    private DbsField fcp_Cons_Pymnt_Bank_Routing;
    private DbsField fcp_Cons_Pymnt_Pymnt_Acctg_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Source_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup fcp_Cons_Pymnt__R_Field_8;
    private DbsField fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Pymnt_Pymnt_Instmt_Nbr;

    private DataAccessProgramView vw_fcp_Cons_Addr;
    private DbsField fcp_Cons_Addr_Rcrd_Typ;
    private DbsField fcp_Cons_Addr_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Addr_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Addr_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Addr_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr;
    private DbsField fcp_Cons_Addr_Ph_Last_Name;
    private DbsField fcp_Cons_Addr_Ph_First_Name;
    private DbsField fcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp;

    private DbsGroup fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp;
    private DbsField fcp_Cons_Addr_Pymnt_Nme;
    private DbsField fcp_Cons_Addr_Pymnt_Addr_Line1_Txt;
    private DbsField fcp_Cons_Addr_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Addr_Key;

    private DbsGroup pnd_Addr_Key__R_Field_9;
    private DbsField pnd_Addr_Key_Pnd_Addr_Ppcn;
    private DbsField pnd_Addr_Key_Pnd_Addr_Cde;
    private DbsField pnd_Addr_Key_Pnd_Addr_Orgn;
    private DbsField pnd_Addr_Key_Pnd_Addr_Invdt;
    private DbsField pnd_Addr_Key_Pnd_Addr_Seq;
    private DbsField pnd_Prev_Check_Nbr;
    private DbsField pnd_Cmbne_Nbr;

    private DbsGroup pnd_Cmbne_Nbr__R_Field_10;
    private DbsField pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Ppy_Total_Amount;
    private DbsField pnd_Ppy_Total_Count;
    private DbsField pnd_Detail_Rec_Cnt;
    private DbsField pnd_New_Account;
    private DbsField pnd_D;
    private DbsField pnd_Date_Mmddyyyy;
    private DbsField pnd_Rpt_Cnt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpsa110 = new PdaCpsa110(localVariables);

        // Local Variables

        pnd_Input_Stop_Rec = localVariables.newGroupInRecord("pnd_Input_Stop_Rec", "#INPUT-STOP-REC");
        pnd_Input_Stop_Rec_Pnd_Customer_Id_Num_D = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Customer_Id_Num_D", "#CUSTOMER-ID-NUM-D", 
            FieldType.NUMERIC, 8);
        pnd_Input_Stop_Rec_Pnd_Record_Type = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 
            1);
        pnd_Input_Stop_Rec_Pnd_Bank_Num = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Bank_Num", "#BANK-NUM", FieldType.NUMERIC, 3);
        pnd_Input_Stop_Rec_Pnd_Account_Num = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Account_Num", "#ACCOUNT-NUM", FieldType.NUMERIC, 
            13);
        pnd_Input_Stop_Rec_Pnd_Run_Date = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Run_Date", "#RUN-DATE", FieldType.NUMERIC, 8);
        pnd_Input_Stop_Rec_Pnd_Run_Time = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Run_Time", "#RUN-TIME", FieldType.NUMERIC, 6);
        pnd_Input_Stop_Rec_Pnd_Input_Type = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Input_Type", "#INPUT-TYPE", FieldType.STRING, 2);
        pnd_Input_Stop_Rec_Pnd_Stop_Type = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Stop_Type", "#STOP-TYPE", FieldType.STRING, 1);
        pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num", "#BEGINNING-CHECK-NUM", 
            FieldType.NUMERIC, 11);

        pnd_Input_Stop_Rec__R_Field_1 = pnd_Input_Stop_Rec.newGroupInGroup("pnd_Input_Stop_Rec__R_Field_1", "REDEFINE", pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num);
        pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N1 = pnd_Input_Stop_Rec__R_Field_1.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N1", 
            "#BEGINNING-CHECK-NUM-N1", FieldType.NUMERIC, 1);
        pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10 = pnd_Input_Stop_Rec__R_Field_1.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10", 
            "#BEGINNING-CHECK-NUM-N10", FieldType.NUMERIC, 10);

        pnd_Input_Stop_Rec__R_Field_2 = pnd_Input_Stop_Rec__R_Field_1.newGroupInGroup("pnd_Input_Stop_Rec__R_Field_2", "REDEFINE", pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10);
        pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N3 = pnd_Input_Stop_Rec__R_Field_2.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N3", 
            "#BEGINNING-CHECK-NUM-N3", FieldType.NUMERIC, 3);
        pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N7 = pnd_Input_Stop_Rec__R_Field_2.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N7", 
            "#BEGINNING-CHECK-NUM-N7", FieldType.NUMERIC, 7);
        pnd_Input_Stop_Rec_Pnd_Ending_Check_Num = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Ending_Check_Num", "#ENDING-CHECK-NUM", FieldType.NUMERIC, 
            11);
        pnd_Input_Stop_Rec_Pnd_Check_Amt = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Check_Amt", "#CHECK-AMT", FieldType.NUMERIC, 15, 
            2);

        pnd_Input_Stop_Rec__R_Field_3 = pnd_Input_Stop_Rec.newGroupInGroup("pnd_Input_Stop_Rec__R_Field_3", "REDEFINE", pnd_Input_Stop_Rec_Pnd_Check_Amt);
        pnd_Input_Stop_Rec_Pnd_Check_Amt_N5 = pnd_Input_Stop_Rec__R_Field_3.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Check_Amt_N5", "#CHECK-AMT-N5", FieldType.NUMERIC, 
            5);
        pnd_Input_Stop_Rec_Pnd_Check_Amt_N10 = pnd_Input_Stop_Rec__R_Field_3.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Check_Amt_N10", "#CHECK-AMT-N10", 
            FieldType.NUMERIC, 10, 2);
        pnd_Input_Stop_Rec_Pnd_Check_Issue_Date_N = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Check_Issue_Date_N", "#CHECK-ISSUE-DATE-N", 
            FieldType.NUMERIC, 8);

        pnd_Input_Stop_Rec__R_Field_4 = pnd_Input_Stop_Rec.newGroupInGroup("pnd_Input_Stop_Rec__R_Field_4", "REDEFINE", pnd_Input_Stop_Rec_Pnd_Check_Issue_Date_N);
        pnd_Input_Stop_Rec_Pnd_Check_Issue_Date_A = pnd_Input_Stop_Rec__R_Field_4.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Check_Issue_Date_A", "#CHECK-ISSUE-DATE-A", 
            FieldType.STRING, 8);
        pnd_Input_Stop_Rec_Pnd_Payee_Name = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Payee_Name", "#PAYEE-NAME", FieldType.STRING, 10);
        pnd_Input_Stop_Rec_Pnd_Reason = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Reason", "#REASON", FieldType.STRING, 10);
        pnd_Input_Stop_Rec_Pnd_Auto_Check_Paid_Inquiry = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Auto_Check_Paid_Inquiry", "#AUTO-CHECK-PAID-INQUIRY", 
            FieldType.STRING, 1);
        pnd_Input_Stop_Rec_Pnd_Stop_Duration_Years = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_Stop_Duration_Years", "#STOP-DURATION-YEARS", 
            FieldType.NUMERIC, 1);
        pnd_Input_Stop_Rec_Pnd_User_Id = pnd_Input_Stop_Rec.newFieldInGroup("pnd_Input_Stop_Rec_Pnd_User_Id", "#USER-ID", FieldType.NUMERIC, 6);

        pnd_Trailer_Rec_T = localVariables.newGroupInRecord("pnd_Trailer_Rec_T", "#TRAILER-REC-T");
        pnd_Trailer_Rec_T_Pnd_Customer_Id_Num_T = pnd_Trailer_Rec_T.newFieldInGroup("pnd_Trailer_Rec_T_Pnd_Customer_Id_Num_T", "#CUSTOMER-ID-NUM-T", FieldType.NUMERIC, 
            8);
        pnd_Trailer_Rec_T_Pnd_Record_Type = pnd_Trailer_Rec_T.newFieldInGroup("pnd_Trailer_Rec_T_Pnd_Record_Type", "#RECORD-TYPE", FieldType.STRING, 1);
        pnd_Trailer_Rec_T_Pnd_Filler_01 = pnd_Trailer_Rec_T.newFieldInGroup("pnd_Trailer_Rec_T_Pnd_Filler_01", "#FILLER-01", FieldType.STRING, 30);

        pnd_Trailer_Rec_T__R_Field_5 = pnd_Trailer_Rec_T.newGroupInGroup("pnd_Trailer_Rec_T__R_Field_5", "REDEFINE", pnd_Trailer_Rec_T_Pnd_Filler_01);
        pnd_Trailer_Rec_T_Pnd_Filler_01_A = pnd_Trailer_Rec_T__R_Field_5.newFieldInGroup("pnd_Trailer_Rec_T_Pnd_Filler_01_A", "#FILLER-01-A", FieldType.NUMERIC, 
            15);
        pnd_Trailer_Rec_T_Pnd_Filler_01_B = pnd_Trailer_Rec_T__R_Field_5.newFieldInGroup("pnd_Trailer_Rec_T_Pnd_Filler_01_B", "#FILLER-01-B", FieldType.NUMERIC, 
            15);
        pnd_Trailer_Rec_T_Pnd_Total_Stops = pnd_Trailer_Rec_T.newFieldInGroup("pnd_Trailer_Rec_T_Pnd_Total_Stops", "#TOTAL-STOPS", FieldType.NUMERIC, 
            7);
        pnd_Trailer_Rec_T_Pnd_Filler_02 = pnd_Trailer_Rec_T.newFieldInGroup("pnd_Trailer_Rec_T_Pnd_Filler_02", "#FILLER-02", FieldType.STRING, 69);

        pnd_Output_Void_Rec = localVariables.newGroupInRecord("pnd_Output_Void_Rec", "#OUTPUT-VOID-REC");
        pnd_Output_Void_Rec_Pnd_Acct_Nbr = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Acct_Nbr", "#ACCT-NBR", FieldType.NUMERIC, 13);
        pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr", "#PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            10);

        pnd_Output_Void_Rec__R_Field_6 = pnd_Output_Void_Rec.newGroupInGroup("pnd_Output_Void_Rec__R_Field_6", "REDEFINE", pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr);
        pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr_N3 = pnd_Output_Void_Rec__R_Field_6.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr_N3", "#PYMNT-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr_N7 = pnd_Output_Void_Rec__R_Field_6.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr_N7", "#PYMNT-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Output_Void_Rec_Pnd_Pymnt_Check_Amt = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Pymnt_Check_Amt", "#PYMNT-CHECK-AMT", FieldType.NUMERIC, 
            10, 2);
        pnd_Output_Void_Rec_Pnd_Pymnt_Check_Dte = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Pymnt_Check_Dte", "#PYMNT-CHECK-DTE", FieldType.STRING, 
            8);
        pnd_Output_Void_Rec_Pnd_Void_Ind = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Void_Ind", "#VOID-IND", FieldType.STRING, 1);
        pnd_Output_Void_Rec_Pnd_Additional_Data = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Additional_Data", "#ADDITIONAL-DATA", FieldType.STRING, 
            30);

        pnd_Output_Void_Rec__R_Field_7 = pnd_Output_Void_Rec.newGroupInGroup("pnd_Output_Void_Rec__R_Field_7", "REDEFINE", pnd_Output_Void_Rec_Pnd_Additional_Data);
        pnd_Output_Void_Rec_Pnd_Cntrct_Ppcn_Nbr = pnd_Output_Void_Rec__R_Field_7.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", 
            FieldType.STRING, 8);
        pnd_Output_Void_Rec_Pnd_Cntrct_Payee_Cde = pnd_Output_Void_Rec__R_Field_7.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 2);
        pnd_Output_Void_Rec_Pnd_Al_Indicator = pnd_Output_Void_Rec__R_Field_7.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Al_Indicator", "#AL-INDICATOR", 
            FieldType.STRING, 1);
        pnd_Output_Void_Rec_Pnd_Filler1 = pnd_Output_Void_Rec__R_Field_7.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Filler1", "#FILLER1", FieldType.STRING, 
            19);
        pnd_Output_Void_Rec_Pnd_Filo1 = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Filo1", "#FILO1", FieldType.STRING, 8);
        pnd_Output_Void_Rec_Pnd_Payee_Nme = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Payee_Nme", "#PAYEE-NME", FieldType.STRING, 50);
        pnd_Output_Void_Rec_Pnd_Fil02 = pnd_Output_Void_Rec.newFieldInGroup("pnd_Output_Void_Rec_Pnd_Fil02", "#FIL02", FieldType.STRING, 20);

        pnd_Header_Record = localVariables.newGroupInRecord("pnd_Header_Record", "#HEADER-RECORD");
        pnd_Header_Record_Pnd_Recon_Hdr = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Recon_Hdr", "#RECON-HDR", FieldType.STRING, 20);
        pnd_Header_Record_Pnd_Bank_No = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Bank_No", "#BANK-NO", FieldType.NUMERIC, 4);
        pnd_Header_Record_Pnd_Acct_Nbr = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Acct_Nbr", "#ACCT-NBR", FieldType.NUMERIC, 13);
        pnd_Header_Record_Pnd_Acct_Total = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Acct_Total", "#ACCT-TOTAL", FieldType.NUMERIC, 12, 
            2);
        pnd_Header_Record_Pnd_Acct_Count = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Pnd_Acct_Count", "#ACCT-COUNT", FieldType.NUMERIC, 5);
        pnd_Header_Record_Fil03 = pnd_Header_Record.newFieldInGroup("pnd_Header_Record_Fil03", "FIL03", FieldType.STRING, 96);

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnt_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "CNTRCT_CMBN_NBR");
        fcp_Cons_Pymnt_Pymnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        fcp_Cons_Pymnt_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnt_Cntrct_Invrse_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Pymnt_Bank_Account = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21, 
            RepeatingFieldStrategy.None, "BANK_ACCOUNT");
        fcp_Cons_Pymnt_Bank_Routing = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9, 
            RepeatingFieldStrategy.None, "BANK_ROUTING");
        fcp_Cons_Pymnt_Pymnt_Acctg_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_ACCTG_DTE");
        fcp_Cons_Pymnt_Pymnt_Source_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Source_Cde", "PYMNT-SOURCE-CDE", FieldType.STRING, 
            5, RepeatingFieldStrategy.None, "PYMNT_SOURCE_CDE");
        fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        fcp_Cons_Pymnt__R_Field_8 = vw_fcp_Cons_Pymnt.getRecord().newGroupInGroup("fcp_Cons_Pymnt__R_Field_8", "REDEFINE", fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num = fcp_Cons_Pymnt__R_Field_8.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        fcp_Cons_Pymnt_Pymnt_Instmt_Nbr = fcp_Cons_Pymnt__R_Field_8.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 
            2);
        registerRecord(vw_fcp_Cons_Pymnt);

        vw_fcp_Cons_Addr = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Addr", "FCP-CONS-ADDR"), "FCP_CONS_ADDR", "FCP_CONS_LEDGR", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ADDR"));
        fcp_Cons_Addr_Rcrd_Typ = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYP");
        fcp_Cons_Addr_Cntrct_Orgn_Cde = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Addr_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Addr_Cntrct_Payee_Cde = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Addr_Cntrct_Invrse_Dte = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        fcp_Cons_Addr_Ph_Last_Name = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16, 
            RepeatingFieldStrategy.None, "PH_LAST_NAME");
        fcp_Cons_Addr_Ph_First_Name = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "PH_FIRST_NAME");
        fcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp = vw_fcp_Cons_Addr.getRecord().newFieldInGroup("fcp_Cons_Addr_Count_Castpymnt_Nme_And_Addr_Grp", 
            "C*PYMNT-NME-AND-ADDR-GRP", RepeatingFieldStrategy.CAsteriskVariable, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");

        fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp = vw_fcp_Cons_Addr.getRecord().newGroupInGroup("fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_Pymnt_Nme = fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 
            38, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_NME", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_Pymnt_Addr_Line1_Txt = fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE1_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        fcp_Cons_Addr_Pymnt_Addr_Line2_Txt = fcp_Cons_Addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("fcp_Cons_Addr_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", 
            FieldType.STRING, 35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE2_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        registerRecord(vw_fcp_Cons_Addr);

        pnd_Addr_Key = localVariables.newFieldInRecord("pnd_Addr_Key", "#ADDR-KEY", FieldType.STRING, 31);

        pnd_Addr_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_Addr_Key__R_Field_9", "REDEFINE", pnd_Addr_Key);
        pnd_Addr_Key_Pnd_Addr_Ppcn = pnd_Addr_Key__R_Field_9.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Ppcn", "#ADDR-PPCN", FieldType.STRING, 10);
        pnd_Addr_Key_Pnd_Addr_Cde = pnd_Addr_Key__R_Field_9.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Cde", "#ADDR-CDE", FieldType.STRING, 4);
        pnd_Addr_Key_Pnd_Addr_Orgn = pnd_Addr_Key__R_Field_9.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Orgn", "#ADDR-ORGN", FieldType.STRING, 2);
        pnd_Addr_Key_Pnd_Addr_Invdt = pnd_Addr_Key__R_Field_9.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Invdt", "#ADDR-INVDT", FieldType.NUMERIC, 8);
        pnd_Addr_Key_Pnd_Addr_Seq = pnd_Addr_Key__R_Field_9.newFieldInGroup("pnd_Addr_Key_Pnd_Addr_Seq", "#ADDR-SEQ", FieldType.NUMERIC, 7);
        pnd_Prev_Check_Nbr = localVariables.newFieldInRecord("pnd_Prev_Check_Nbr", "#PREV-CHECK-NBR", FieldType.NUMERIC, 10);
        pnd_Cmbne_Nbr = localVariables.newFieldInRecord("pnd_Cmbne_Nbr", "#CMBNE-NBR", FieldType.STRING, 14);

        pnd_Cmbne_Nbr__R_Field_10 = localVariables.newGroupInRecord("pnd_Cmbne_Nbr__R_Field_10", "REDEFINE", pnd_Cmbne_Nbr);
        pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn = pnd_Cmbne_Nbr__R_Field_10.newFieldInGroup("pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn", "#CMBNE-PPCN", FieldType.STRING, 10);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Ppy_Total_Amount = localVariables.newFieldInRecord("pnd_Ppy_Total_Amount", "#PPY-TOTAL-AMOUNT", FieldType.NUMERIC, 12, 2);
        pnd_Ppy_Total_Count = localVariables.newFieldInRecord("pnd_Ppy_Total_Count", "#PPY-TOTAL-COUNT", FieldType.NUMERIC, 5);
        pnd_Detail_Rec_Cnt = localVariables.newFieldInRecord("pnd_Detail_Rec_Cnt", "#DETAIL-REC-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_New_Account = localVariables.newFieldInRecord("pnd_New_Account", "#NEW-ACCOUNT", FieldType.STRING, 1);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);
        pnd_Date_Mmddyyyy = localVariables.newFieldInRecord("pnd_Date_Mmddyyyy", "#DATE-MMDDYYYY", FieldType.STRING, 10);
        pnd_Rpt_Cnt = localVariables.newFieldInRecord("pnd_Rpt_Cnt", "#RPT-CNT", FieldType.NUMERIC, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();
        vw_fcp_Cons_Addr.reset();

        localVariables.reset();
        pnd_Output_Void_Rec_Pnd_Acct_Nbr.setInitialValue(-2147483648);
        pnd_Header_Record_Pnd_Recon_Hdr.setInitialValue("RECONCILIATIONHEADER");
        pnd_Header_Record_Pnd_Bank_No.setInitialValue(75);
        pnd_Header_Record_Pnd_Acct_Nbr.setInitialValue(-2147483648);
        pnd_Header0_1.setInitialValue("          Voided Checks Daily Report");
        pnd_Header0_2.setInitialValue("      Sorted by Account and Check Number");
        pnd_New_Account.setInitialValue("N");
        pnd_Rpt_Cnt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp195v() throws Exception
    {
        super("Fcpp195v");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 55 ZP = ON IS = OFF ES = OFF SG = OFF
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 #INPUT-STOP-REC
        while (condition(getWorkFiles().read(1, pnd_Input_Stop_Rec)))
        {
            if (condition(pnd_Input_Stop_Rec_Pnd_Record_Type.notEquals("D")))                                                                                             //Natural: IF #INPUT-STOP-REC.#RECORD-TYPE NE 'D'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10.equals(pnd_Prev_Check_Nbr)))                                                                     //Natural: IF #BEGINNING-CHECK-NUM-N10 EQ #PREV-CHECK-NBR
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Prev_Check_Nbr.setValue(pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10);                                                                                  //Natural: MOVE #BEGINNING-CHECK-NUM-N10 TO #PREV-CHECK-NBR
                                                                                                                                                                          //Natural: PERFORM READ-196-FCP-CON-PYMNT
            sub_Read_196_Fcp_Con_Pymnt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Detail_Rec_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #DETAIL-REC-CNT
            if (condition(pnd_Detail_Rec_Cnt.equals(1)))                                                                                                                  //Natural: IF #DETAIL-REC-CNT = 1
            {
                pnd_Header_Record_Pnd_Acct_Nbr.setValue(pnd_Input_Stop_Rec_Pnd_Account_Num);                                                                              //Natural: MOVE #INPUT-STOP-REC.#ACCOUNT-NUM TO #HEADER-RECORD.#ACCT-NBR
                                                                                                                                                                          //Natural: PERFORM GET-BANK-TABLE
                sub_Get_Bank_Table();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-ACCOUNT-HEADER
                sub_Display_Report_Account_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Input_Stop_Rec_Pnd_Account_Num.notEquals(pnd_Header_Record_Pnd_Acct_Nbr)))                                                                  //Natural: IF #INPUT-STOP-REC.#ACCOUNT-NUM NE #HEADER-RECORD.#ACCT-NBR
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-POS-PAY-ACCOUNT-HEADER
                sub_Write_Pos_Pay_Account_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-ACCOUNT-TRAILER
                sub_Display_Report_Account_Trailer();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_New_Account.setValue("Y");                                                                                                                            //Natural: MOVE 'Y' TO #NEW-ACCOUNT
                pnd_Header_Record_Pnd_Acct_Nbr.setValue(pnd_Input_Stop_Rec_Pnd_Account_Num);                                                                              //Natural: MOVE #INPUT-STOP-REC.#ACCOUNT-NUM TO #HEADER-RECORD.#ACCT-NBR
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-ACCOUNT-HEADER
                sub_Display_Report_Account_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Header_Record_Pnd_Acct_Total.reset();                                                                                                                 //Natural: RESET #ACCT-TOTAL #ACCT-COUNT
                pnd_Header_Record_Pnd_Acct_Count.reset();
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Void_Rec_Pnd_Acct_Nbr.setValue(pnd_Input_Stop_Rec_Pnd_Account_Num);                                                                                //Natural: MOVE #INPUT-STOP-REC.#ACCOUNT-NUM TO #OUTPUT-VOID-REC.#ACCT-NBR
            pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr.setValue(pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10);                                                             //Natural: ASSIGN #OUTPUT-VOID-REC.#PYMNT-CHECK-NBR := #BEGINNING-CHECK-NUM-N10
            pnd_Output_Void_Rec_Pnd_Pymnt_Check_Amt.setValue(pnd_Input_Stop_Rec_Pnd_Check_Amt_N10);                                                                       //Natural: ASSIGN #OUTPUT-VOID-REC.#PYMNT-CHECK-AMT := #CHECK-AMT-N10
            pnd_Header_Record_Pnd_Acct_Total.nadd(pnd_Input_Stop_Rec_Pnd_Check_Amt_N10);                                                                                  //Natural: COMPUTE #ACCT-TOTAL = #ACCT-TOTAL + #CHECK-AMT-N10
            pnd_Ppy_Total_Amount.nadd(pnd_Input_Stop_Rec_Pnd_Check_Amt_N10);                                                                                              //Natural: COMPUTE #PPY-TOTAL-AMOUNT = #PPY-TOTAL-AMOUNT + #CHECK-AMT-N10
            pnd_Header_Record_Pnd_Acct_Count.nadd(1);                                                                                                                     //Natural: COMPUTE #ACCT-COUNT = #ACCT-COUNT + 1
            pnd_Ppy_Total_Count.nadd(1);                                                                                                                                  //Natural: COMPUTE #PPY-TOTAL-COUNT = #PPY-TOTAL-COUNT + 1
            pnd_Output_Void_Rec_Pnd_Additional_Data.moveAll(" ");                                                                                                         //Natural: MOVE ALL ' ' TO #OUTPUT-VOID-REC.#ADDITIONAL-DATA
            if (condition(fcp_Cons_Pymnt_Cntrct_Orgn_Cde.equals("AP") && pnd_Input_Stop_Rec_Pnd_Payee_Name.notEquals(" ")))                                               //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'AP' AND #INPUT-STOP-REC.#PAYEE-NAME NE ' '
            {
                pnd_Output_Void_Rec_Pnd_Payee_Nme.setValue(pnd_Input_Stop_Rec_Pnd_Payee_Name);                                                                            //Natural: ASSIGN #OUTPUT-VOID-REC.#PAYEE-NME := #INPUT-STOP-REC.#PAYEE-NAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(fcp_Cons_Pymnt_Cntrct_Orgn_Cde.equals("IA") || fcp_Cons_Pymnt_Cntrct_Orgn_Cde.equals("AP")))                                                //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'IA' OR = 'AP'
                {
                    pnd_Cmbne_Nbr.setValue(fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr);                                                                                               //Natural: ASSIGN #CMBNE-NBR := FCP-CONS-PYMNT.CNTRCT-CMBN-NBR
                    pnd_Addr_Key_Pnd_Addr_Ppcn.setValue(pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn);                                                                                    //Natural: ASSIGN #ADDR-PPCN := #CMBNE-PPCN
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Addr_Key_Pnd_Addr_Ppcn.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                                  //Natural: ASSIGN #ADDR-PPCN := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
                }                                                                                                                                                         //Natural: END-IF
                pnd_Addr_Key_Pnd_Addr_Ppcn.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                                      //Natural: ASSIGN #ADDR-PPCN := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
                pnd_Addr_Key_Pnd_Addr_Cde.setValue(fcp_Cons_Pymnt_Cntrct_Payee_Cde);                                                                                      //Natural: ASSIGN #ADDR-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
                pnd_Addr_Key_Pnd_Addr_Orgn.setValue(fcp_Cons_Pymnt_Cntrct_Orgn_Cde);                                                                                      //Natural: ASSIGN #ADDR-ORGN := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
                pnd_Addr_Key_Pnd_Addr_Invdt.setValue(fcp_Cons_Pymnt_Cntrct_Invrse_Dte);                                                                                   //Natural: ASSIGN #ADDR-INVDT := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
                pnd_Addr_Key_Pnd_Addr_Seq.setValue(fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num);                                                                                   //Natural: ASSIGN #ADDR-SEQ := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
                vw_fcp_Cons_Addr.startDatabaseRead                                                                                                                        //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #ADDR-KEY
                (
                "READ_ADR",
                new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Addr_Key, WcType.BY) },
                new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
                1
                );
                READ_ADR:
                while (condition(vw_fcp_Cons_Addr.readNextRow("READ_ADR")))
                {
                    if (condition((((((fcp_Cons_Addr_Cntrct_Orgn_Cde.equals("IA") || fcp_Cons_Addr_Cntrct_Orgn_Cde.equals("AP")) && pnd_Addr_Key_Pnd_Addr_Ppcn.equals(pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn))  //Natural: IF ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE = 'IA' OR = 'AP' AND #ADDR-PPCN = #CMBNE-PPCN AND #ADDR-CDE = FCP-CONS-ADDR.CNTRCT-PAYEE-CDE AND #ADDR-ORGN = FCP-CONS-ADDR.CNTRCT-ORGN-CDE ) OR ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE NE 'IA' AND FCP-CONS-ADDR.CNTRCT-ORGN-CDE NE 'AP' AND #ADDR-ORGN = FCP-CONS-ADDR.CNTRCT-ORGN-CDE AND #ADDR-INVDT = FCP-CONS-ADDR.CNTRCT-INVRSE-DTE AND #ADDR-SEQ = FCP-CONS-ADDR.PYMNT-PRCSS-SEQ-NBR )
                        && pnd_Addr_Key_Pnd_Addr_Cde.equals(fcp_Cons_Addr_Cntrct_Payee_Cde)) && pnd_Addr_Key_Pnd_Addr_Orgn.equals(fcp_Cons_Addr_Cntrct_Orgn_Cde)) 
                        || ((((fcp_Cons_Addr_Cntrct_Orgn_Cde.notEquals("IA") && fcp_Cons_Addr_Cntrct_Orgn_Cde.notEquals("AP")) && pnd_Addr_Key_Pnd_Addr_Orgn.equals(fcp_Cons_Addr_Cntrct_Orgn_Cde)) 
                        && pnd_Addr_Key_Pnd_Addr_Invdt.equals(fcp_Cons_Addr_Cntrct_Invrse_Dte)) && pnd_Addr_Key_Pnd_Addr_Seq.equals(fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr)))))
                    {
                        if (condition(fcp_Cons_Addr_Pymnt_Nme.getValue(1).getSubstring(1,3).equals("CR ")))                                                               //Natural: IF SUBSTRING ( PYMNT-NME ( 1 ) ,1,3 ) EQ 'CR '
                        {
                            //*          AND #OUTPUT-VOID-REC.#PAYEE-NME NE ' '
                            pnd_Output_Void_Rec_Pnd_Payee_Nme.setValue(fcp_Cons_Addr_Pymnt_Nme.getValue(2));                                                              //Natural: ASSIGN #OUTPUT-VOID-REC.#PAYEE-NME := PYMNT-NME ( 2 )
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Output_Void_Rec_Pnd_Payee_Nme.setValue(fcp_Cons_Addr_Pymnt_Nme.getValue(1));                                                              //Natural: ASSIGN #OUTPUT-VOID-REC.#PAYEE-NME := PYMNT-NME ( 1 )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        fcp_Cons_Addr_Pymnt_Nme.getValue(1).setValue("***** UNKNOWN *****");                                                                              //Natural: ASSIGN FCP-CONS-ADDR.PYMNT-NME ( 1 ) := '***** UNKNOWN *****'
                        fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(1).reset();                                                                                           //Natural: RESET FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( 2 )
                        fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(2).reset();
                    }                                                                                                                                                     //Natural: END-IF
                    //*  (READ-ADR.)
                }                                                                                                                                                         //Natural: END-READ
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  (1810)
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Void_Rec_Pnd_Cntrct_Payee_Cde.setValue(fcp_Cons_Pymnt_Cntrct_Payee_Cde);                                                                           //Natural: ASSIGN #OUTPUT-VOID-REC.#CNTRCT-PAYEE-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
            if (condition(fcp_Cons_Pymnt_Cntrct_Orgn_Cde.equals("AL")))                                                                                                   //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'AL'
            {
                pnd_Output_Void_Rec_Pnd_Al_Indicator.setValue("2");                                                                                                       //Natural: ASSIGN #AL-INDICATOR := '2'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Void_Rec_Pnd_Al_Indicator.setValue(" ");                                                                                                       //Natural: ASSIGN #AL-INDICATOR := ' '
            }                                                                                                                                                             //Natural: END-IF
            pnd_Output_Void_Rec_Pnd_Cntrct_Ppcn_Nbr.setValue(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr);                                                                             //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-PPCN-NBR TO #OUTPUT-VOID-REC.#CNTRCT-PPCN-NBR
            pnd_Output_Void_Rec_Pnd_Void_Ind.setValue("V");                                                                                                               //Natural: MOVE 'V' TO #OUTPUT-VOID-REC.#VOID-IND
            //*  MOVE #CHECK-ISSUE-DATE-A TO #OUTPUT-VOID-REC.#PYMNT-CHECK-DTE
            pnd_Output_Void_Rec_Pnd_Pymnt_Check_Dte.setValueEdited(fcp_Cons_Pymnt_Pymnt_Acctg_Dte,new ReportEditMask("YYYYMMDD"));                                        //Natural: MOVE EDITED FCP-CONS-PYMNT.PYMNT-ACCTG-DTE ( EM = YYYYMMDD ) TO #OUTPUT-VOID-REC.#PYMNT-CHECK-DTE
            pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Input_Stop_Rec_Pnd_Check_Issue_Date_A);                                                               //Natural: MOVE EDITED #CHECK-ISSUE-DATE-A TO #D ( EM = YYYYMMDD )
            pnd_Date_Mmddyyyy.setValueEdited(pnd_D,new ReportEditMask("MM/DD/YYYY"));                                                                                     //Natural: MOVE EDITED #D ( EM = MM/DD/YYYY ) TO #DATE-MMDDYYYY
            if (condition(pnd_New_Account.equals("Y")))                                                                                                                   //Natural: IF #NEW-ACCOUNT EQ 'Y'
            {
                getWorkFiles().write(3, false, pnd_Output_Void_Rec);                                                                                                      //Natural: WRITE WORK FILE 3 #OUTPUT-VOID-REC
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(2, false, pnd_Output_Void_Rec);                                                                                                      //Natural: WRITE WORK FILE 2 #OUTPUT-VOID-REC
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-DETAIL
            sub_Display_Report_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* * SORT STEP AFTER THIS PGM EXECUTES TO PUT HEADERS IN FROM OF DETAIL
            //* * REPORT STEP AFTER SORT
            //* *
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-POS-PAY-ACCOUNT-HEADER
            sub_Write_Pos_Pay_Account_Header();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DISPLAY-REPORT-ACCOUNT-TRAILER
            sub_Display_Report_Account_Trailer();
            if (condition(Global.isEscape())) {return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"    CONTROL TOTALS    ",NEWLINE,"  COUNT         AMOUNT",new ColumnSpacing(7),NEWLINE,pnd_Ppy_Total_Count,  //Natural: WRITE ( 1 ) // '    CONTROL TOTALS    ' / '  COUNT         AMOUNT' 7X / #PPY-TOTAL-COUNT ( EM = ZZZ,ZZ9 ) 3X #PPY-TOTAL-AMOUNT ( EM = ZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("ZZ,ZZ9"),new ColumnSpacing(3),pnd_Ppy_Total_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape())) return;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,Global.getPROGRAM(),"END OF RUN AT TIME:",Global.getTIME(),NEWLINE);                       //Natural: WRITE ( 1 ) /// *PROGRAM 'END OF RUN AT TIME:' *TIME /
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  -------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-196-FCP-CON-PYMNT
        //*  ------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-197-FCP-CONS-ADDR
        //*  ------------------------------ *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-BANK-TABLE
        //*  -------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-ACCOUNT-HEADER
        //* *001T 'Bank Route:' FCP-CONS-PYMNT.BANK-ROUTING
        //* *2X   'Bank Name: Wachovia Bank N.A.'
        //*  -------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-POS-PAY-ACCOUNT-HEADER
        //*  ------------------------------------ *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-DETAIL
        //*  --------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-REPORT-ACCOUNT-TRAILER
    }
    private void sub_Read_196_Fcp_Con_Pymnt() throws Exception                                                                                                            //Natural: READ-196-FCP-CON-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------- *
        if (condition(pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N3.equals(getZero()) && pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N7.greater(getZero())))               //Natural: IF #BEGINNING-CHECK-NUM-N3 EQ 0 AND #BEGINNING-CHECK-NUM-N7 GT 0
        {
            vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) FCP-CONS-PYMNT WITH PYMNT-CHECK-NBR = #BEGINNING-CHECK-NUM-N7
            (
            "FIND_PYMNT_N7",
            new Wc[] { new Wc("PYMNT_CHECK_NBR", "=", pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N7, WcType.WITH) },
            1
            );
            FIND_PYMNT_N7:
            while (condition(vw_fcp_Cons_Pymnt.readNextRow("FIND_PYMNT_N7", true)))
            {
                vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
                if (condition(vw_fcp_Cons_Pymnt.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO PYMNT REC (196) FOUND","=",pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N7);                                                  //Natural: WRITE 'NO PYMNT REC (196) FOUND' '=' #BEGINNING-CHECK-NUM-N7
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FIND_PYMNT_N7"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FIND_PYMNT_N7"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    fcp_Cons_Pymnt_Cntrct_Payee_Cde.reset();                                                                                                              //Natural: RESET FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*  (FIND-PYMNT-N7.)
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                           //Natural: FIND ( 1 ) FCP-CONS-PYMNT WITH PYMNT-NBR = #BEGINNING-CHECK-NUM-N10
            (
            "FIND_PYMNT",
            new Wc[] { new Wc("PYMNT_NBR", "=", pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10, WcType.WITH) },
            1
            );
            FIND_PYMNT:
            while (condition(vw_fcp_Cons_Pymnt.readNextRow("FIND_PYMNT", true)))
            {
                vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
                if (condition(vw_fcp_Cons_Pymnt.getAstCOUNTER().equals(0)))                                                                                               //Natural: IF NO RECORDS FOUND
                {
                    getReports().write(0, "NO PYMNT REC (196) FOUND","=",pnd_Input_Stop_Rec_Pnd_Beginning_Check_Num_N10);                                                 //Natural: WRITE 'NO PYMNT REC (196) FOUND' '=' #BEGINNING-CHECK-NUM-N10
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("FIND_PYMNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("FIND_PYMNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    fcp_Cons_Pymnt_Cntrct_Payee_Cde.reset();                                                                                                              //Natural: RESET FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                //*  (FIND-PYMNT.)
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ-196-FCP-CON-PYMNT
    }
    private void sub_Read_197_Fcp_Cons_Addr() throws Exception                                                                                                            //Natural: READ-197-FCP-CONS-ADDR
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------- *
        vw_fcp_Cons_Addr.startDatabaseRead                                                                                                                                //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #ADDR-KEY
        (
        "READ_ADDR",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Addr_Key, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        READ_ADDR:
        while (condition(vw_fcp_Cons_Addr.readNextRow("READ_ADDR")))
        {
            if (condition((((((fcp_Cons_Addr_Cntrct_Orgn_Cde.equals("IA") || fcp_Cons_Addr_Cntrct_Orgn_Cde.equals("AP")) && pnd_Addr_Key_Pnd_Addr_Ppcn.equals(pnd_Cmbne_Nbr_Pnd_Cmbne_Ppcn))  //Natural: IF ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE = 'IA' OR = 'AP' AND #ADDR-PPCN = #CMBNE-PPCN AND #ADDR-CDE = FCP-CONS-ADDR.CNTRCT-PAYEE-CDE AND #ADDR-ORGN = FCP-CONS-ADDR.CNTRCT-ORGN-CDE ) OR ( FCP-CONS-ADDR.CNTRCT-ORGN-CDE NE 'IA' AND FCP-CONS-ADDR.CNTRCT-ORGN-CDE NE 'AP' AND #ADDR-ORGN = FCP-CONS-ADDR.CNTRCT-ORGN-CDE AND #ADDR-INVDT = FCP-CONS-ADDR.CNTRCT-INVRSE-DTE AND #ADDR-SEQ = FCP-CONS-ADDR.PYMNT-PRCSS-SEQ-NBR )
                && pnd_Addr_Key_Pnd_Addr_Cde.equals(fcp_Cons_Addr_Cntrct_Payee_Cde)) && pnd_Addr_Key_Pnd_Addr_Orgn.equals(fcp_Cons_Addr_Cntrct_Orgn_Cde)) 
                || ((((fcp_Cons_Addr_Cntrct_Orgn_Cde.notEquals("IA") && fcp_Cons_Addr_Cntrct_Orgn_Cde.notEquals("AP")) && pnd_Addr_Key_Pnd_Addr_Orgn.equals(fcp_Cons_Addr_Cntrct_Orgn_Cde)) 
                && pnd_Addr_Key_Pnd_Addr_Invdt.equals(fcp_Cons_Addr_Cntrct_Invrse_Dte)) && pnd_Addr_Key_Pnd_Addr_Seq.equals(fcp_Cons_Addr_Pymnt_Prcss_Seq_Nbr)))))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                fcp_Cons_Addr_Pymnt_Nme.getValue(1).setValue("***** UNKNOWN *****");                                                                                      //Natural: ASSIGN FCP-CONS-ADDR.PYMNT-NME ( 1 ) := '***** UNKNOWN *****'
                fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(1).reset();                                                                                                   //Natural: RESET FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( 1 ) FCP-CONS-ADDR.PYMNT-ADDR-LINE1-TXT ( 2 )
                fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(2).reset();
            }                                                                                                                                                             //Natural: END-IF
            //*  (READ-ADDR.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition((fcp_Cons_Pymnt_Cntrct_Payee_Cde.equals("ALT1") || fcp_Cons_Pymnt_Cntrct_Payee_Cde.equals("ALT2")) || (fcp_Cons_Pymnt_Cntrct_Payee_Cde.notEquals("ALT1")  //Natural: IF ( FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE EQ 'ALT1' OR FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE EQ 'ALT2' ) OR ( FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE NE 'ALT1' AND FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE NE 'ALT2' AND FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE NE 'ANNT' AND SUBSTR ( PYMNT-NME ( 1 ) ,1,3 ) EQ 'CR ' )
            && fcp_Cons_Pymnt_Cntrct_Payee_Cde.notEquals("ALT2") && fcp_Cons_Pymnt_Cntrct_Payee_Cde.notEquals("ANNT") && fcp_Cons_Addr_Pymnt_Nme.getValue(1).getSubstring(1,
            3).equals("CR "))))
        {
            if (condition(fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(1).getSubstring(1,3).equals("CR ")))                                                                //Natural: IF SUBSTRING ( PYMNT-ADDR-LINE1-TXT ( 1 ) ,1,3 ) EQ 'CR '
            {
                pnd_Output_Void_Rec_Pnd_Payee_Nme.setValue(fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(2));                                                               //Natural: ASSIGN #OUTPUT-VOID-REC.#PAYEE-NME := PYMNT-ADDR-LINE1-TXT ( 2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Void_Rec_Pnd_Payee_Nme.setValue(fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(1));                                                               //Natural: ASSIGN #OUTPUT-VOID-REC.#PAYEE-NME := PYMNT-ADDR-LINE1-TXT ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(fcp_Cons_Addr_Pymnt_Nme.getValue(1).getSubstring(1,3).equals("CR ")))                                                                           //Natural: IF SUBSTRING ( PYMNT-NME ( 1 ) ,1,3 ) EQ 'CR '
            {
                if (condition(fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(1).getSubstring(1,3).equals("CR ")))                                                            //Natural: IF SUBSTRING ( PYMNT-ADDR-LINE1-TXT ( 1 ) ,1,3 ) EQ 'CR '
                {
                    pnd_Output_Void_Rec_Pnd_Payee_Nme.setValue(fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(2));                                                           //Natural: ASSIGN #OUTPUT-VOID-REC.#PAYEE-NME := PYMNT-ADDR-LINE1-TXT ( 2 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Output_Void_Rec_Pnd_Payee_Nme.setValue(fcp_Cons_Addr_Pymnt_Addr_Line1_Txt.getValue(1));                                                           //Natural: ASSIGN #OUTPUT-VOID-REC.#PAYEE-NME := PYMNT-ADDR-LINE1-TXT ( 1 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Output_Void_Rec_Pnd_Payee_Nme.setValue(fcp_Cons_Addr_Pymnt_Nme.getValue(1));                                                                          //Natural: ASSIGN #OUTPUT-VOID-REC.#PAYEE-NME := PYMNT-NME ( 1 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  READ-197-FCP-CONS-ADDR
    }
    private void sub_Get_Bank_Table() throws Exception                                                                                                                    //Natural: GET-BANK-TABLE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------ *
        pdaCpsa110.getCpsa110().reset();                                                                                                                                  //Natural: RESET CPSA110
        pdaCpsa110.getCpsa110_Cpsa110_Function().setValue("               ");                                                                                             //Natural: MOVE '               ' TO CPSA110.CPSA110-FUNCTION
        pdaCpsa110.getCpsa110_Cpsa110_Source_Code().setValue(fcp_Cons_Pymnt_Pymnt_Source_Cde);                                                                            //Natural: MOVE FCP-CONS-PYMNT.PYMNT-SOURCE-CDE TO CPSA110.CPSA110-SOURCE-CODE
        DbsUtil.callnat(Cpsn110.class , getCurrentProcessState(), pdaCpsa110.getCpsa110());                                                                               //Natural: CALLNAT 'CPSN110' CPSA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpsa110.getCpsa110_Cpsa110_Return_Code().notEquals("00")))                                                                                       //Natural: IF CPSA110.CPSA110-RETURN-CODE NE '00'
        {
            pdaCpsa110.getCpsa110_Bank_Routing().setValue("031100225");                                                                                                   //Natural: MOVE '031100225' TO CPSA110.BANK-ROUTING
            pdaCpsa110.getCpsa110_Bank_Account_Name().setValue("Wachovia Bank N.A.");                                                                                     //Natural: MOVE 'Wachovia Bank N.A.' TO CPSA110.BANK-ACCOUNT-NAME
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-BANK-TABLE
    }
    private void sub_Display_Report_Account_Header() throws Exception                                                                                                     //Natural: DISPLAY-REPORT-ACCOUNT-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------- *
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(1),Global.getINIT_USER(),new TabSetting(40),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(108),"PAGE:",getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE 001T *INIT-USER 40T 'CONSOLIDATED PAYMENT SYSTEM' 108T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / 001T *PROGRAM 030T #HEADER0-1 108T *DATU / 030T #HEADER0-2 108T *TIMX ( EM = HH:IIAP ) // 001T 'Bank Route:' CPSA110.BANK-ROUTING 2X 'Bank Name:' CPSA110.BANK-ACCOUNT-NAME ( AL = 20 ) 2X 'Bank Account:' #HEADER-RECORD.#ACCT-NBR /
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(1),Global.getPROGRAM(),new TabSetting(30),pnd_Header0_1,new TabSetting(108),Global.getDATU(),NEWLINE,new 
            TabSetting(30),pnd_Header0_2,new TabSetting(108),Global.getTIMX(), new ReportEditMask ("HH:IIAP"),NEWLINE,NEWLINE,new TabSetting(1),"Bank Route:",pdaCpsa110.getCpsa110_Bank_Routing(),new 
            ColumnSpacing(2),"Bank Name:",pdaCpsa110.getCpsa110_Bank_Account_Name(), new AlphanumericLength (20),new ColumnSpacing(2),"Bank Account:",pnd_Header_Record_Pnd_Acct_Nbr,
            NEWLINE);
        if (Global.isEscape()) return;
        getReports().skip(1, 2);                                                                                                                                          //Natural: SKIP ( 1 ) 2 LINES
        //* (N13)
        //* (N10)
        //* (8.2)
        //* (A10)  MM/DD/YYYY
        //* (A8)
        //* (A2)
        //* (A1)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),"Account",new TabSetting(28),"Check",new TabSetting(43),"Check",new TabSetting(52),"Check",new      //Natural: WRITE ( 1 ) 013T 'Account' 028T 'Check' 043T 'Check' 052T 'Check' 063T 'PPCN' 071T 'Pay' 076T 'ALT' 087T 'Payee'
            TabSetting(63),"PPCN",new TabSetting(71),"Pay",new TabSetting(76),"ALT",new TabSetting(87),"Payee");
        if (Global.isEscape()) return;
        //* (N13)
        //* (N10)
        //* (8.2)
        //* (A10)  MM/DD/YYYY
        //* (A8)
        //* (A2)
        //* (A1)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(13),"Number",new TabSetting(27),"Number",new TabSetting(42),"Amount",new TabSetting(52),"Date",new      //Natural: WRITE ( 1 ) 013T 'Number' 027T 'Number' 042T 'Amount' 052T 'Date' 062T 'Number' 071T 'Cde' 076T 'Ind' 088T 'Name'
            TabSetting(62),"Number",new TabSetting(71),"Cde",new TabSetting(76),"Ind",new TabSetting(88),"Name");
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-HEADER
    }
    private void sub_Write_Pos_Pay_Account_Header() throws Exception                                                                                                      //Natural: WRITE-POS-PAY-ACCOUNT-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------- *
        if (condition(pnd_New_Account.equals("Y")))                                                                                                                       //Natural: IF #NEW-ACCOUNT EQ 'Y'
        {
            pnd_Header_Record_Pnd_Acct_Nbr.setValue(pnd_Input_Stop_Rec_Pnd_Account_Num);                                                                                  //Natural: MOVE #INPUT-STOP-REC.#ACCOUNT-NUM TO #HEADER-RECORD.#ACCT-NBR
            getWorkFiles().write(3, false, pnd_Header_Record);                                                                                                            //Natural: WRITE WORK FILE 3 #HEADER-RECORD
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getWorkFiles().write(2, false, pnd_Header_Record);                                                                                                            //Natural: WRITE WORK FILE 2 #HEADER-RECORD
        }                                                                                                                                                                 //Natural: END-IF
        //*  WRITE-POS-PAY-ACCOUNT-HEADER
    }
    private void sub_Display_Report_Detail() throws Exception                                                                                                             //Natural: DISPLAY-REPORT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------ *
        pnd_Rpt_Cnt.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #RPT-CNT
        //* (N05)
        //* (N13)
        //* (N10)
        //*  N8.2
        //* (A10) MMDDYYYY
        //* (A8)
        //* (A2)
        //* (A1)
        //* (A50)
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(3),pnd_Rpt_Cnt, new ReportEditMask ("ZZ,ZZ9"),new TabSetting(10),pnd_Output_Void_Rec_Pnd_Acct_Nbr,new   //Natural: WRITE ( 1 ) 003T #RPT-CNT ( EM = ZZ,ZZ9 ) 010T #OUTPUT-VOID-REC.#ACCT-NBR 025T #OUTPUT-VOID-REC.#PYMNT-CHECK-NBR 036T #OUTPUT-VOID-REC.#PYMNT-CHECK-AMT ( EM = ZZ,ZZZ,ZZZ.99 ) 050T #DATE-MMDDYYYY 061T #OUTPUT-VOID-REC.#CNTRCT-PPCN-NBR 071T #OUTPUT-VOID-REC.#CNTRCT-PAYEE-CDE 077T #OUTPUT-VOID-REC.#AL-INDICATOR 082T #OUTPUT-VOID-REC.#PAYEE-NME
            TabSetting(25),pnd_Output_Void_Rec_Pnd_Pymnt_Check_Nbr,new TabSetting(36),pnd_Output_Void_Rec_Pnd_Pymnt_Check_Amt, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"),new 
            TabSetting(50),pnd_Date_Mmddyyyy,new TabSetting(61),pnd_Output_Void_Rec_Pnd_Cntrct_Ppcn_Nbr,new TabSetting(71),pnd_Output_Void_Rec_Pnd_Cntrct_Payee_Cde,new 
            TabSetting(77),pnd_Output_Void_Rec_Pnd_Al_Indicator,new TabSetting(82),pnd_Output_Void_Rec_Pnd_Payee_Nme);
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-DETAIL
    }
    private void sub_Display_Report_Account_Trailer() throws Exception                                                                                                    //Natural: DISPLAY-REPORT-ACCOUNT-TRAILER
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------- *
        //*  N8.2
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(12),"Account Total Payments:",pnd_Header_Record_Pnd_Acct_Total, new ReportEditMask ("ZZ,ZZZ,ZZZ.99"));  //Natural: WRITE ( 1 ) 012T 'Account Total Payments:' #ACCT-TOTAL ( EM = ZZ,ZZZ,ZZZ.99 )
        if (Global.isEscape()) return;
        //*  DISPLAY-REPORT-ACCOUNT-TRAILER
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=55 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
