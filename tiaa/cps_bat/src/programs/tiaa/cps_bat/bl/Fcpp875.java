/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:06 PM
**        * FROM NATURAL PROGRAM : Fcpp875
************************************************************
**        * FILE NAME            : Fcpp875.java
**        * CLASS NAME           : Fcpp875
**        * INSTANCE NAME        : Fcpp875
************************************************************
********************* RL PAYEE MATCH FEB 22, 2006 **********************
* PROGRAM  : FCPP875
* SYSTEM   : CPS
* TITLE    : CHECK UPDATE
* FUNCTION : THIS PROGRAM WILL UPDATE ADABAS PAYMENT FILE WITH
*            CHECK NUMBER AND CHECK SEQUENCE NUMBER.
* HISTORY  :
*          : 01/25/1999  RITA SALGADO
*          : - CREATE ANNOTATION RECORD FOR EXPEDITED HOLDS (OV00;USPS)
* 12/01/99 : ROXAN
*            FCPNCNT2 REPLACED FCPNNZC2 FOR CONTROL PROGRAM
* 12/07/99 : ROXAN
*            FCPAEXT  REPLACED FCPA371/FCPA801C/FCPA873
*
* 02-16-00 : ALTHEA A. YOUNG
*          : REVISED TO PREVENT A DUPLICATE ANNOTATION RECORD FROM BEING
*          : CREATED FOR CANCELLED / STOPPED RECORDS WITH 'OV00' OR
*          : 'USPS' HOLD CODES.
*
* 01-11-01 : ROXAN CARREON
*          : PRIOR CHANGES FAILED IT's purpose of preventing duplicates.
*          : THIS REVISION WILL ALLOW UPDATES FOR EXISTING ANNOTATION
*          : RECORDS FOR PAYMENTS WITH OV00 AND USPS THUS PREVENTING
*          : DUPLICATES.
*          : DEFINE ANNOTAITON KEY FOR RETRIEVAL
*    11-02 : R CARREN  STOW - EGTRRA CHANGES IN LDA
* 04/18/02 : R CARREN  STOW - TPA EGTRRA CHANGES DONE IN FCPAEXT
*          :
* R. LANDRUM    02/22/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
*
* 5/2/08 - NEEDS RESTOW TO PICK UP NAT 3 VERSION OF FCPCUSPS - AER
* 04/20/10 -  CM - RESTOW FOR CHANGE IN FCPLPMNU
* 04/2017  - JJG - PIN EXPANSION, RESTOW FOR CHANGE IN FCPAEXT
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp875 extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private LdaFcplpmnu ldaFcplpmnu;
    private LdaFcplannu ldaFcplannu;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpanzcn pdaFcpanzcn;
    private PdaFcpanzc1 pdaFcpanzc1;
    private PdaFcpanzc2 pdaFcpanzc2;
    private LdaFcplnzc2 ldaFcplnzc2;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_1;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_First_Chk;

    private DbsGroup pnd_First_Chk__R_Field_2;
    private DbsField pnd_First_Chk_Pnd_First_Chk_N3;
    private DbsField pnd_First_Chk_Pnd_First_Chk_N7;
    private DbsField pnd_Last_Chk;

    private DbsGroup pnd_Last_Chk__R_Field_3;
    private DbsField pnd_Last_Chk_Pnd_Last_Chk_N3;
    private DbsField pnd_Last_Chk_Pnd_Last_Chk_N7;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Pymnt_S__R_Field_4;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Prev_Hold_Cde;
    private DbsField pnd_Ws_Pnd_First_Check;
    private DbsField pnd_Ws_Pnd_Last_Check;
    private DbsField pnd_Ws_Pnd_Chk_Miss_Start;
    private DbsField pnd_Ws_Pnd_Chk_Miss_End;
    private DbsField pnd_Ws_Pnd_Missing_Checks;
    private DbsField pnd_Ws_Pnd_Missing_Checks_1;
    private DbsField pnd_Ws_Pnd_Rec_Updated;
    private DbsField pnd_Ws_Pnd_Update_Cnt;
    private DbsField pnd_Ws_Pnd_Et_Cnt;
    private DbsField pnd_Ws_Pnd_Pymnt;
    private DbsField pnd_Ws_Pnd_First_Check_Ind;
    private DbsField pnd_Ws_Pnd_Pymnt_Not_Found;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Annot_Key;

    private DbsGroup pnd_Ws_Annot_Key__R_Field_5;

    private DbsGroup pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Pymnt_SuperdeOld;
    private DbsField readWork02Pnd_Pymnt_SuperdeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcplpmnu = new LdaFcplpmnu();
        registerRecord(ldaFcplpmnu);
        registerRecord(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());
        ldaFcplannu = new LdaFcplannu();
        registerRecord(ldaFcplannu);
        registerRecord(ldaFcplannu.getVw_fcp_Cons_Annu());
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpanzcn = new PdaFcpanzcn(localVariables);
        pdaFcpanzc1 = new PdaFcpanzc1(localVariables);
        pdaFcpanzc2 = new PdaFcpanzc2(localVariables);
        ldaFcplnzc2 = new LdaFcplnzc2();
        registerRecord(ldaFcplnzc2);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_1", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_First_Chk = localVariables.newFieldInRecord("pnd_First_Chk", "#FIRST-CHK", FieldType.NUMERIC, 10);

        pnd_First_Chk__R_Field_2 = localVariables.newGroupInRecord("pnd_First_Chk__R_Field_2", "REDEFINE", pnd_First_Chk);
        pnd_First_Chk_Pnd_First_Chk_N3 = pnd_First_Chk__R_Field_2.newFieldInGroup("pnd_First_Chk_Pnd_First_Chk_N3", "#FIRST-CHK-N3", FieldType.NUMERIC, 
            3);
        pnd_First_Chk_Pnd_First_Chk_N7 = pnd_First_Chk__R_Field_2.newFieldInGroup("pnd_First_Chk_Pnd_First_Chk_N7", "#FIRST-CHK-N7", FieldType.NUMERIC, 
            7);
        pnd_Last_Chk = localVariables.newFieldInRecord("pnd_Last_Chk", "#LAST-CHK", FieldType.NUMERIC, 10);

        pnd_Last_Chk__R_Field_3 = localVariables.newGroupInRecord("pnd_Last_Chk__R_Field_3", "REDEFINE", pnd_Last_Chk);
        pnd_Last_Chk_Pnd_Last_Chk_N3 = pnd_Last_Chk__R_Field_3.newFieldInGroup("pnd_Last_Chk_Pnd_Last_Chk_N3", "#LAST-CHK-N3", FieldType.NUMERIC, 3);
        pnd_Last_Chk_Pnd_Last_Chk_N7 = pnd_Last_Chk__R_Field_3.newFieldInGroup("pnd_Last_Chk_Pnd_Last_Chk_N7", "#LAST-CHK-N7", FieldType.NUMERIC, 7);

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Num = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);

        pnd_Pymnt_S__R_Field_4 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_4", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_4.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 27);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Prev_Hold_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Hold_Cde", "#PREV-HOLD-CDE", FieldType.STRING, 4);
        pnd_Ws_Pnd_First_Check = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Check", "#FIRST-CHECK", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Last_Check = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Last_Check", "#LAST-CHECK", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Chk_Miss_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Chk_Miss_Start", "#CHK-MISS-START", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Chk_Miss_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Chk_Miss_End", "#CHK-MISS-END", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Missing_Checks = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Missing_Checks", "#MISSING-CHECKS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Missing_Checks_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Missing_Checks_1", "#MISSING-CHECKS-1", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Rec_Updated = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Updated", "#REC-UPDATED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Update_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Update_Cnt", "#UPDATE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Et_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Pymnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt", "#PYMNT", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_First_Check_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Check_Ind", "#FIRST-CHECK-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Pymnt_Not_Found = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Not_Found", "#PYMNT-NOT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Annot_Key = localVariables.newFieldInRecord("pnd_Ws_Annot_Key", "#WS-ANNOT-KEY", FieldType.STRING, 29);

        pnd_Ws_Annot_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Annot_Key__R_Field_5", "REDEFINE", pnd_Ws_Annot_Key);

        pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields = pnd_Ws_Annot_Key__R_Field_5.newGroupInGroup("pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields", "#WS-ANNOT-KEY-FIELDS");
        pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Annot_Key_Cntrct_Invrse_Dte = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Annot_Key_Cntrct_Orgn_Cde = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Pymnt_SuperdeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Pymnt_Superde_OLD", "Pnd_Pymnt_Superde_OLD", FieldType.STRING, 
            27);
        readWork02Pnd_Pymnt_SuperdeOld = internalLoopRecord.newFieldInRecord("ReadWork02_Pnd_Pymnt_Superde_OLD", "Pnd_Pymnt_Superde_OLD", FieldType.STRING, 
            27);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcplpmnu.initializeValues();
        ldaFcplannu.initializeValues();
        ldaFcplnzc2.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Et_Cnt.setInitialValue(150);
        pnd_Ws_Pnd_First_Check_Ind.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp875() throws Exception
    {
        super("Fcpp875");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 2 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'NEW ANNUITIZATION UPDATE CONTROL REPORT' 120T 'REPORT: RPT2' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPANZC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 6 )
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
        //* **************** RL PAYEE MATCH FEB22, 2006 BEGIN *******************
        //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        //* ***************** RL PAYEE MATCH FEB22, 2006 END ********************
        pnd_Ws_Pnd_Pymnt.setValue(true);                                                                                                                                  //Natural: ASSIGN #PYMNT := TRUE
        //*  PAYMENT FILE
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 EXT ( * )
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            CheckAtStartofData1038();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            //*     MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #PYMNT-S               /*
            pnd_Pymnt_S.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                                        //Natural: MOVE BY NAME EXTR TO #PYMNT-S
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
                                                                                                                                                                          //Natural: AT BREAK OF #PYMNT-SUPERDE;//Natural: PERFORM ACCUM-CONTROL-DATA
            sub_Accum_Control_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WRITE WORK FILE 3 VARIABLE PYMNT-ADDR-INFO INV-INFO(1:C-INV-ACCT)/*
            getWorkFiles().write(3, false, pdaFcpaext.getExt().getValue("*"));                                                                                            //Natural: WRITE WORK FILE 3 EXT ( * )
            readWork01Pnd_Pymnt_SuperdeOld.setValue(pnd_Pymnt_S_Pnd_Pymnt_Superde);                                                                                       //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Pymnt.setValue(false);                                                                                                                                 //Natural: ASSIGN #PYMNT := FALSE
        //* READ WORK FILE 2 PYMNT-ADDR-INFO INV-INFO(*) /* CANCEL/STOP FILE  /*
        //*  CANCEL/STOP FILE             /*
        boolean endOfDataReadwork02 = true;                                                                                                                               //Natural: READ WORK FILE 2 EXT ( * )
        boolean firstReadwork02 = true;
        READWORK02:
        while (condition(getWorkFiles().read(2, pdaFcpaext.getExt().getValue("*"))))
        {
            CheckAtStartofData1064();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork02();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork02 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            //*   MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #PYMNT-S                 /*
            pnd_Pymnt_S.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                                        //Natural: MOVE BY NAME EXTR TO #PYMNT-S
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
                                                                                                                                                                          //Natural: AT BREAK OF #PYMNT-SUPERDE;//Natural: PERFORM ACCUM-CONTROL-DATA
            sub_Accum_Control_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork02Pnd_Pymnt_SuperdeOld.setValue(pnd_Pymnt_S_Pnd_Pymnt_Superde);                                                                                       //Natural: END-WORK
        }
        READWORK02_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork02(endOfDataReadwork02);
        }
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Rec_Updated.nadd(pnd_Ws_Pnd_Update_Cnt);                                                                                                               //Natural: ADD #UPDATE-CNT TO #REC-UPDATED
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //*  RL
        pnd_First_Chk_Pnd_First_Chk_N7.setValue(pnd_Ws_Pnd_First_Check);                                                                                                  //Natural: MOVE #FIRST-CHECK TO #FIRST-CHK-N7
        //*  RL
        pnd_First_Chk_Pnd_First_Chk_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                           //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #FIRST-CHK-N3
        //*  RL
        pnd_Last_Chk_Pnd_Last_Chk_N7.setValue(pnd_Ws_Pnd_Last_Check);                                                                                                     //Natural: MOVE #LAST-CHECK TO #LAST-CHK-N7
        //*  RL
        pnd_Last_Chk_Pnd_Last_Chk_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                             //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #LAST-CHK-N3
        //*  RL
        //*  RL
        getReports().write(2, "STARTING CHECK NUMBER.......  ",pnd_First_Chk,NEWLINE,"ENDING   CHECK NUMBER.......  ",pnd_Last_Chk,NEWLINE,"TOTAL MISSING CHECKS........",pnd_Ws_Pnd_Missing_Checks,  //Natural: WRITE ( 2 ) 'STARTING CHECK NUMBER.......  ' #FIRST-CHK / 'ENDING   CHECK NUMBER.......  ' #LAST-CHK / 'TOTAL MISSING CHECKS........' #MISSING-CHECKS / 'TOTAL ZERO CHECKS...........' #FCPANZC2.#PYMNT-CNT ( 47 ) / 'TOTAL CHECKS................' #FCPANZC2.#PYMNT-CNT ( 48 ) / 'TOTAL EFT...................' #FCPANZC2.#PYMNT-CNT ( 49 ) / 'TOTAL GLOBAL PAY............' #FCPANZC2.#PYMNT-CNT ( 50 ) / 'RECORDS UPDATED.............' #REC-UPDATED
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"TOTAL ZERO CHECKS...........",pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue(47),NEWLINE,"TOTAL CHECKS................",pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue(48),NEWLINE,"TOTAL EFT...................",pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue(49),NEWLINE,"TOTAL GLOBAL PAY............",pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue(50),NEWLINE,"RECORDS UPDATED.............",pnd_Ws_Pnd_Rec_Updated, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* *'STARTING CHECK NUMBER.......  ' #FIRST-CHECK             /
        //* *'ENDING   CHECK NUMBER.......  ' #LAST-CHECK              /
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                       //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue("*").setValue(true);                                                                                       //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( * ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("     New Annuitization Batch Update");                                                                          //Natural: ASSIGN #FCPACRPT.#TITLE := '     New Annuitization Batch Update'
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(43).setValue(false);                                                                                       //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 43 ) := FALSE
        //*  CALLNAT 'FCPNNZC2'                        /*  12/01/99  ROXAN
        //*   12/01/99  ROXAN
        DbsUtil.callnat(Fcpncnt2.class , getCurrentProcessState(), pdaFcpanzc2.getPnd_Fcpanzc2(), pdaFcpacrpt.getPnd_Fcpacrpt(), "NZ");                                   //Natural: CALLNAT 'FCPNCNT2' USING #FCPANZC2 #FCPACRPT 'NZ'
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Ws_Pnd_Terminate.getBoolean()))                                                                                                                 //Natural: IF #TERMINATE
        {
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PYMNT-FILE
        //* ************************** RL END-PAYEE MATCH *************************
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-PYMNT
        //* **************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-DATA
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-DATA
        //* ***********************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MISSING-CHECKS
        //* *******************************
        //* *********************** RL BEGIN - PAYEE MATCH ************FEB 22,2006
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //* ************************** RL END-PAYEE MATCH *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ANNOT-FOR-HOLD
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Update_Pymnt_File() throws Exception                                                                                                                 //Natural: UPDATE-PYMNT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        pnd_Ws_Pnd_Pymnt_Not_Found.setValue(true);                                                                                                                        //Natural: MOVE TRUE TO #PYMNT-NOT-FOUND
        ldaFcplpmnu.getVw_fcp_Cons_Pymnu().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNU BY PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE
        (
        "READ03",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") }
        );
        READ03:
        while (condition(ldaFcplpmnu.getVw_fcp_Cons_Pymnu().readNextRow("READ03")))
        {
            if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Ppcn_Nbr().equals(pnd_Pymnt_S_Cntrct_Ppcn_Nbr) && ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Invrse_Dte().equals(pnd_Pymnt_S_Cntrct_Invrse_Dte)  //Natural: IF FCP-CONS-PYMNU.CNTRCT-PPCN-NBR = #PYMNT-S.CNTRCT-PPCN-NBR AND FCP-CONS-PYMNU.CNTRCT-INVRSE-DTE = #PYMNT-S.CNTRCT-INVRSE-DTE AND FCP-CONS-PYMNU.CNTRCT-ORGN-CDE = #PYMNT-S.CNTRCT-ORGN-CDE AND FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NUM = #PYMNT-S.PYMNT-PRCSS-SEQ-NUM
                && ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Orgn_Cde().equals(pnd_Pymnt_S_Cntrct_Orgn_Cde) && ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num().equals(pnd_Pymnt_S_Pymnt_Prcss_Seq_Num)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Pymnt_Not_Found.setValue(false);                                                                                                                   //Natural: MOVE FALSE TO #PYMNT-NOT-FOUND
            if (condition(pnd_Ws_Pnd_Pymnt.getBoolean()))                                                                                                                 //Natural: IF #PYMNT
            {
                //* *********************** RL BEGIN - PAYEE MATCH ************ MAY 10,2006
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                          //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1
                {
                    pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                               //Natural: MOVE EXT.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                    pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                     //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Nbr().setValue(pnd_Ws_Check_Nbr_N10);                                                                             //Natural: MOVE #WS-CHECK-NBR-N10 TO FCP-CONS-PYMNU.PYMNT-NBR
                    pnd_Ws_Check_Nbr_N10.reset();                                                                                                                         //Natural: RESET #WS-CHECK-NBR-N10
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr().compute(new ComputeParameters(false, ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr()),                //Natural: COMPUTE FCP-CONS-PYMNU.PYMNT-CHECK-NBR = EXT.PYMNT-CHECK-NBR * -1
                        pdaFcpaext.getExt_Pymnt_Check_Nbr().multiply(-1));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr().setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                        //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-NBR := EXT.PYMNT-CHECK-NBR
                }                                                                                                                                                         //Natural: END-IF
                ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr().setValue(pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr());                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-SCRTY-NBR := EXT.PYMNT-CHECK-SCRTY-NBR
                ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr().setValue(pdaFcpaext.getExt_Pymnt_Check_Seq_Nbr());                                                    //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-SEQ-NBR := EXT.PYMNT-CHECK-SEQ-NBR
                ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Amt().setValue(pdaFcpaext.getExt_Pymnt_Check_Amt());                                                            //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-AMT := EXT.PYMNT-CHECK-AMT
                //*  02-16-2000
                if (condition((ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind().equals(1) && (ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")        //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1 AND FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
                    || ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))))
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-ANNOT-FOR-HOLD
                    sub_Create_Annot_For_Hold();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                //*  RIAD
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Stats_Cde().setValue("P");                                                                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-STATS-CDE := 'P'
            pnd_Ws_Pnd_Update_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #UPDATE-CNT
            ldaFcplpmnu.getVw_fcp_Cons_Pymnu().updateDBRow("READ03");                                                                                                     //Natural: UPDATE
            if (condition(pnd_Ws_Pnd_Update_Cnt.greaterOrEqual(pnd_Ws_Pnd_Et_Cnt)))                                                                                       //Natural: IF #UPDATE-CNT GE #ET-CNT
            {
                pnd_Ws_Pnd_Rec_Updated.nadd(pnd_Ws_Pnd_Update_Cnt);                                                                                                       //Natural: ADD #UPDATE-CNT TO #REC-UPDATED
                pnd_Ws_Pnd_Update_Cnt.reset();                                                                                                                            //Natural: RESET #UPDATE-CNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Pymnt_Not_Found.getBoolean()))                                                                                                           //Natural: IF #PYMNT-NOT-FOUND
        {
            pnd_Ws_Pnd_Terminate.setValue(true);                                                                                                                          //Natural: ASSIGN #TERMINATE := TRUE
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"PAYMENT RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"TIAA PPCN#  :",pnd_Pymnt_S_Cntrct_Ppcn_Nbr,new  //Natural: WRITE '***' 25T 'PAYMENT RECORD IS NOT FOUND' 77T '***' / '***' 25T 'TIAA PPCN#  :' #PYMNT-S.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'INVERSE DATE:' #PYMNT-S.CNTRCT-INVRSE-DTE 77T '***' / '***' 25T 'ORIGIN      :' #PYMNT-S.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'SEQUENCE#   :' EXT.PYMNT-PRCSS-SEQ-NBR 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"INVERSE DATE:",pnd_Pymnt_S_Cntrct_Invrse_Dte,new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"ORIGIN      :",pnd_Pymnt_S_Cntrct_Orgn_Cde,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"SEQUENCE#   :",pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr(),new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_New_Pymnt() throws Exception                                                                                                                         //Natural: NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := TRUE
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
        sub_Set_Control_Data();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Ws_Pnd_Pymnt.getBoolean() && pdaFcpaext.getExt_Pymnt_Check_Nbr().greaterOrEqual(1) && pdaFcpaext.getExt_Pymnt_Check_Nbr().lessOrEqual(9999998))) //Natural: IF #PYMNT AND EXT.PYMNT-CHECK-NBR = 1 THRU 9999998
        {
            short decideConditionsMet1211 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FIRST-CHECK-IND
            if (condition(pnd_Ws_Pnd_First_Check_Ind.getBoolean()))
            {
                decideConditionsMet1211++;
                pnd_Ws_Pnd_First_Check_Ind.setValue(false);                                                                                                               //Natural: ASSIGN #FIRST-CHECK-IND := FALSE
                pnd_Ws_Pnd_First_Check.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                                                     //Natural: ASSIGN #FIRST-CHECK := EXT.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: WHEN #PREV-HOLD-CDE NE '0000' AND EXT.CNTRCT-HOLD-CDE = '0000'
            else if (condition(pnd_Ws_Pnd_Prev_Hold_Cde.notEquals("0000") && pdaFcpaext.getExt_Cntrct_Hold_Cde().equals("0000")))
            {
                decideConditionsMet1211++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ( #LAST-CHECK + 1 ) NE EXT.PYMNT-CHECK-NBR
            else if (condition(pnd_Ws_Pnd_Last_Check.add(1).notEquals(pdaFcpaext.getExt_Pymnt_Check_Nbr())))
            {
                decideConditionsMet1211++;
                                                                                                                                                                          //Natural: PERFORM MISSING-CHECKS
                sub_Missing_Checks();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Ws_Pnd_Last_Check.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                                                          //Natural: ASSIGN #LAST-CHECK := EXT.PYMNT-CHECK-NBR
            pnd_Ws_Pnd_Prev_Hold_Cde.setValue(pdaFcpaext.getExt_Cntrct_Hold_Cde());                                                                                       //Natural: ASSIGN #PREV-HOLD-CDE := EXT.CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM UPDATE-PYMNT-FILE
        sub_Update_Pymnt_File();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Set_Control_Data() throws Exception                                                                                                                  //Natural: SET-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO      TO #FCPANZCN            /*
        pdaFcpanzcn.getPnd_Fcpanzcn().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                          //Natural: MOVE BY NAME EXTR TO #FCPANZCN
        DbsUtil.callnat(Fcpnnzcn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpanzcn.getPnd_Fcpanzcn(), pdaFcpanzc1.getPnd_Fcpanzc1());          //Natural: CALLNAT 'FCPNNZCN' USING #FCPACRPT #FCPANZCN #FCPANZC1
        if (condition(Global.isEscape())) return;
    }
    private void sub_Accum_Control_Data() throws Exception                                                                                                                //Natural: ACCUM-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().setValue(pdaFcpaext.getExt_C_Inv_Acct());                                                                              //Natural: ASSIGN #MAX-FUND := C-INV-ACCT
        if (condition(ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().equals(getZero())))                                                                                      //Natural: IF #MAX-FUND = 0
        {
            ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().setValue(1);                                                                                                       //Natural: ASSIGN #MAX-FUND := 1
        }                                                                                                                                                                 //Natural: END-IF
        //* MOVE BY NAME INV-INFO(1:#MAX-FUND) TO #CNTRL-INV-ACCT(1:#MAX-FUND)  /*
        ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()).setValuesByName(pdaFcpaext.getExt_Inv_Acct().getValue(1, //Natural: MOVE BY NAME EXT.INV-ACCT ( 1:#MAX-FUND ) TO #CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
        DbsUtil.callnat(Fcpnnzc1.class , getCurrentProcessState(), pdaFcpanzc1.getPnd_Fcpanzc1(), pdaFcpanzc2.getPnd_Fcpanzc2(), ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNNZC1' USING #FCPANZC1 #FCPANZC2 #FCPLNZC2.#MAX-FUND #FCPLNZC2.#CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
        if (condition(Global.isEscape())) return;
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                                  //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := FALSE
    }
    private void sub_Missing_Checks() throws Exception                                                                                                                    //Natural: MISSING-CHECKS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Chk_Miss_Start.compute(new ComputeParameters(false, pnd_Ws_Pnd_Chk_Miss_Start), pnd_Ws_Pnd_Last_Check.add(1));                                         //Natural: ASSIGN #CHK-MISS-START := #LAST-CHECK + 1
        pnd_Ws_Pnd_Chk_Miss_End.compute(new ComputeParameters(false, pnd_Ws_Pnd_Chk_Miss_End), pdaFcpaext.getExt_Pymnt_Check_Nbr().subtract(1));                          //Natural: ASSIGN #CHK-MISS-END := EXT.PYMNT-CHECK-NBR - 1
        pnd_Ws_Pnd_Missing_Checks_1.compute(new ComputeParameters(false, pnd_Ws_Pnd_Missing_Checks_1), pnd_Ws_Pnd_Chk_Miss_End.subtract(pnd_Ws_Pnd_Last_Check));          //Natural: ASSIGN #MISSING-CHECKS-1 := #CHK-MISS-END - #LAST-CHECK
        pnd_Ws_Pnd_Missing_Checks.nadd(pnd_Ws_Pnd_Missing_Checks_1);                                                                                                      //Natural: ASSIGN #MISSING-CHECKS := #MISSING-CHECKS + #MISSING-CHECKS-1
        if (condition(pnd_Ws_Pnd_Missing_Checks_1.equals(1)))                                                                                                             //Natural: IF #MISSING-CHECKS-1 = 1
        {
            getReports().write(2, "CHECK ",pnd_Ws_Pnd_Chk_Miss_Start,new ColumnSpacing(18),"IS  MISSING",pnd_Ws_Pnd_Missing_Checks_1, new ReportEditMask                  //Natural: WRITE ( 2 ) 'CHECK ' #CHK-MISS-START 18X 'IS  MISSING' #MISSING-CHECKS-1 'CHECK'
                ("-Z,ZZZ,ZZ9"),"CHECK");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, "CHECKS",pnd_Ws_Pnd_Chk_Miss_Start,"THROUGH",pnd_Ws_Pnd_Chk_Miss_End,"ARE MISSING",pnd_Ws_Pnd_Missing_Checks_1, new                     //Natural: WRITE ( 2 ) 'CHECKS' #CHK-MISS-START 'THROUGH' #CHK-MISS-END 'ARE MISSING' #MISSING-CHECKS-1 'CHECKS'
                ReportEditMask ("-Z,ZZZ,ZZ9"),"CHECKS");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* RL
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
    }
    private void sub_Create_Annot_For_Hold() throws Exception                                                                                                             //Natural: CREATE-ANNOT-FOR-HOLD
    {
        if (BLNatReinput.isReinput()) return;

        //*   CHECK FOR EXISTENCE OF ANNOTATION RECORD TO PREVENT DUPLICATE - RCC
        if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Annot_Ind().notEquals("Y")))                                                                                    //Natural: IF FCP-CONS-PYMNU.PYMNT-ANNOT-IND NE 'Y'
        {
            ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Annot_Ind().setValue("Y");                                                                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-ANNOT-IND := 'Y'
            ldaFcplannu.getVw_fcp_Cons_Annu().setValuesByName(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());                                                                        //Natural: MOVE BY NAME FCP-CONS-PYMNU TO FCP-CONS-ANNU
            ldaFcplannu.getFcp_Cons_Annu_Cntrct_Rcrd_Typ().setValue("4");                                                                                                 //Natural: ASSIGN FCP-CONS-ANNU.CNTRCT-RCRD-TYP := '4'
            if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")))                                                                                //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00'
            {
                ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "expedited mail: Tracking #"));            //Natural: COMPRESS ' Check sent' *DATU 'expedited mail: Tracking #' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))                                                                            //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'USPS'
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "USPS overnight"));                    //Natural: COMPRESS ' Check sent' *DATU 'USPS overnight' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplannu.getVw_fcp_Cons_Annu().insertDBRow();                                                                                                              //Natural: STORE FCP-CONS-ANNU
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   IF ANNOTATION RECORD EXIST - ROXAN
            pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.setValuesByName(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());                                                                 //Natural: MOVE BY NAME FCP-CONS-PYMNU TO #WS-ANNOT-KEY-FIELDS
            ldaFcplannu.getVw_fcp_Cons_Annu().startDatabaseFind                                                                                                           //Natural: FIND FCP-CONS-ANNU WITH PPCN-INV-ORGN-PRCSS-INST = #WS-ANNOT-KEY
            (
            "PND_PND_L0260",
            new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", "=", pnd_Ws_Annot_Key, WcType.WITH) }
            );
            PND_PND_L0260:
            while (condition(ldaFcplannu.getVw_fcp_Cons_Annu().readNextRow("PND_PND_L0260")))
            {
                ldaFcplannu.getVw_fcp_Cons_Annu().setIfNotFoundControlFlag(false);
                if (condition(ldaFcplannu.getFcp_Cons_Annu_Cntrct_Rcrd_Typ().notEquals("4")))                                                                             //Natural: IF FCP-CONS-ANNU.CNTRCT-RCRD-TYP NE '4'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   KEEP LINE1 IN LINE2 WHEN LINE2 IS EMPTY OTHERWISE OVERWRITE
                if (condition(ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt().equals(" ")))                                                                           //Natural: IF FCP-CONS-ANNU.PYMNT-RMRK-LINE2-TXT = ' '
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt().setValue(ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt());                                    //Natural: ASSIGN FCP-CONS-ANNU.PYMNT-RMRK-LINE2-TXT := FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")))                                                                            //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00'
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "expedited mail: Tracking #"));        //Natural: COMPRESS ' Check sent' *DATU 'expedited mail: Tracking #' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))                                                                        //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'USPS'
                    {
                        ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "USPS overnight"));                //Natural: COMPRESS ' Check sent' *DATU 'USPS overnight' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaFcplannu.getVw_fcp_Cons_Annu().updateDBRow("PND_PND_L0260");                                                                                           //Natural: UPDATE ( ##L0260. )
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Pymnt_S_Pnd_Pymnt_SuperdeIsBreak = pnd_Pymnt_S_Pnd_Pymnt_Superde.isBreak(endOfData);
        if (condition(pnd_Pymnt_S_Pnd_Pymnt_SuperdeIsBreak))
        {
            //*  NOT EOF
            if (condition(pnd_Pymnt_S_Pnd_Pymnt_Superde.notEquals(readWork01Pnd_Pymnt_SuperdeOld)))                                                                       //Natural: IF #PYMNT-SUPERDE NE OLD ( #PYMNT-SUPERDE )
            {
                                                                                                                                                                          //Natural: PERFORM NEW-PYMNT
                sub_New_Pymnt();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }

    private void atBreakEventReadwork02() throws Exception {atBreakEventReadwork02(false);}
    private void atBreakEventReadwork02(boolean endOfData) throws Exception
    {
        boolean pnd_Pymnt_S_Pnd_Pymnt_SuperdeIsBreak = pnd_Pymnt_S_Pnd_Pymnt_Superde.isBreak(endOfData);
        if (condition(pnd_Pymnt_S_Pnd_Pymnt_SuperdeIsBreak))
        {
            //*  NOT EOF
            if (condition(pnd_Pymnt_S_Pnd_Pymnt_Superde.notEquals(readWork01Pnd_Pymnt_SuperdeOld)))                                                                       //Natural: IF #PYMNT-SUPERDE NE OLD ( #PYMNT-SUPERDE )
            {
                                                                                                                                                                          //Natural: PERFORM NEW-PYMNT
                sub_New_Pymnt();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(2, "PS=58 LS=132 ZP=ON");

        getReports().write(2, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(47),"NEW ANNUITIZATION UPDATE CONTROL REPORT",new TabSetting(120),
            "REPORT: RPT2",NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData1038() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*     MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #PYMNT-S               /*
            pnd_Pymnt_S.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                                        //Natural: MOVE BY NAME EXTR TO #PYMNT-S
                                                                                                                                                                          //Natural: PERFORM NEW-PYMNT
            sub_New_Pymnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
    private void CheckAtStartofData1064() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*   MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #PYMNT-S                 /*
            pnd_Pymnt_S.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                                        //Natural: MOVE BY NAME EXTR TO #PYMNT-S
                                                                                                                                                                          //Natural: PERFORM NEW-PYMNT
            sub_New_Pymnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
