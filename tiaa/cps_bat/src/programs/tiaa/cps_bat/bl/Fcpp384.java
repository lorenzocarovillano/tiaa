/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:29 PM
**        * FROM NATURAL PROGRAM : Fcpp384
************************************************************
**        * FILE NAME            : Fcpp384.java
**        * CLASS NAME           : Fcpp384
**        * INSTANCE NAME        : Fcpp384
************************************************************
************************************************************************
* PROGRAM  : FCPP384
* SYSTEM   : CPS
* TITLE    : CONDITIONALLY END NATURAL SESSION AND BYPASS NEXT PROGRAMS
* WRITTEN  : FEB 23, 1994
* FUNCTION : THE PROGRAM READS A CONTROL CARD, TELLING IT WHETHER
*            THE PROGRAM FOLLOWING IT WILL RUN CHECKS OR EFT.
*            FCPP384 INSPECTS THE SPLIT CONTROL RECORD (WORKFILE) AND
*            DETERMINES IF THERE IS INPUT FOR THE REQUEST OR IS IT AN
*            EMPTY INPUT FILE.  IF THE INPUT IS EMPTY THE PROGRAM WILL
*            CLOSE THE NATURAL SESSION THUS BYPASSING THE EXECUTION OF
*            THE NEXT PROGRAM IN STREAM.
* ----------------------------------------------------------------------
* WORK FILES:
*
*  WF54 = SPLIT CONTROL RECORD
* ----------------------------------------------------------------------
* HISTORY
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp384 extends BLNatBase
{
    // Data Areas
    private LdaFcpl720a ldaFcpl720a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField run_Rqust_Parameters;

    private DbsGroup run_Rqust_Parameters__R_Field_1;
    private DbsField run_Rqust_Parameters_Pnd_Rrp_Request;
    private DbsField run_Rqust_Parameters_Pnd_Run_Type;
    private DbsField pnd_Abend_Cde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Disp_Text;
    private DbsField pnd_Ws_Pnd_Program;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl720a = new LdaFcpl720a();
        registerRecord(ldaFcpl720a);

        // Local Variables
        localVariables = new DbsRecord();
        run_Rqust_Parameters = localVariables.newFieldInRecord("run_Rqust_Parameters", "RUN-RQUST-PARAMETERS", FieldType.STRING, 80);

        run_Rqust_Parameters__R_Field_1 = localVariables.newGroupInRecord("run_Rqust_Parameters__R_Field_1", "REDEFINE", run_Rqust_Parameters);
        run_Rqust_Parameters_Pnd_Rrp_Request = run_Rqust_Parameters__R_Field_1.newFieldInGroup("run_Rqust_Parameters_Pnd_Rrp_Request", "#RRP-REQUEST", 
            FieldType.STRING, 10);
        run_Rqust_Parameters_Pnd_Run_Type = run_Rqust_Parameters__R_Field_1.newFieldInGroup("run_Rqust_Parameters_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 
            8);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Disp_Text = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Text", "#DISP-TEXT", FieldType.STRING, 60);
        pnd_Ws_Pnd_Program = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl720a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp384() throws Exception
    {
        super("Fcpp384");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 80 ZP = ON;//Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 32T 'PROCESS' #FCPL720A.#CNTRCT-ORGN-CDE #RRP-REQUEST 68T 'REPORT: RPT0' / 36T #RUN-TYPE
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 2 RUN-RQUST-PARAMETERS
        while (condition(getWorkFiles().read(2, run_Rqust_Parameters)))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getWorkFiles().read(5, ldaFcpl720a.getPnd_Fcpl720a());                                                                                                            //Natural: READ WORK FILE 5 ONCE #FCPL720A
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Abend_Cde.setValue(42);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 42
            getReports().write(0, Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"Split control file is empty");                                        //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / 'Split control file is empty'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
            sub_Terminate_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDFILE
        getReports().write(0, "Content of Split Control File:");                                                                                                          //Natural: WRITE 'Content of Split Control File:'
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #CNTL-IDX = 1 TO 4
        for (ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(1); condition(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().lessOrEqual(4)); ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().nadd(1))
        {
            getReports().write(0, NEWLINE,ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Text().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx()));                              //Natural: WRITE / #CNTL-TEXT ( #CNTL-IDX )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR02:                                                                                                                                                        //Natural: FOR #REC-TYPE-IDX = 0 TO 3
            for (ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().setValue(0); condition(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().lessOrEqual(3)); 
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().nadd(1))
            {
                getReports().write(0, new TabSetting(17),ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Types_Text().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().getInt() + 1),ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().getInt() + 1,ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx()),  //Natural: WRITE 17T #REC-TYPES-TEXT ( #REC-TYPE-IDX ) #REC-TYPE-CNT ( #CNTL-IDX,#REC-TYPE-IDX ) ( EM = -Z,ZZZ,ZZ9 )
                    new ReportEditMask ("-Z,ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        short decideConditionsMet63 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #RRP-REQUEST;//Natural: VALUE 'CHECK', 'CHECKS'
        if (condition((run_Rqust_Parameters_Pnd_Rrp_Request.equals("CHECK") || run_Rqust_Parameters_Pnd_Rrp_Request.equals("CHECKS"))))
        {
            decideConditionsMet63++;
            if (condition(run_Rqust_Parameters_Pnd_Run_Type.equals("HOLD") || run_Rqust_Parameters_Pnd_Run_Type.equals("HELD")))                                          //Natural: IF #RUN-TYPE = 'HOLD' OR = 'HELD'
            {
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(1);                                                                                                   //Natural: ASSIGN #CNTL-IDX := 1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(2);                                                                                                   //Natural: ASSIGN #CNTL-IDX := 2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'EFT', 'EFTS'
        else if (condition((run_Rqust_Parameters_Pnd_Rrp_Request.equals("EFT") || run_Rqust_Parameters_Pnd_Rrp_Request.equals("EFTS"))))
        {
            decideConditionsMet63++;
            ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(4);                                                                                                       //Natural: ASSIGN #CNTL-IDX := 4
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Abend_Cde.setValue(41);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 41
            getReports().write(0, Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Invalid Run Request:",run_Rqust_Parameters_Pnd_Rrp_Request);        //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Invalid Run Request:' #RRP-REQUEST
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
            sub_Terminate_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().getInt() + 1,0).equals(getZero())))              //Natural: IF #REC-TYPE-CNT ( #CNTL-IDX,0 ) = 0
        {
            pnd_Ws_Pnd_Disp_Text.setValue(DbsUtil.compress(run_Rqust_Parameters_Pnd_Run_Type, run_Rqust_Parameters_Pnd_Rrp_Request, "run bypassed due to empty input"));  //Natural: COMPRESS #RUN-TYPE #RRP-REQUEST 'run bypassed due to empty input' INTO #DISP-TEXT
            getReports().write(0, NEWLINE,NEWLINE,NEWLINE,pnd_Ws_Pnd_Disp_Text);                                                                                          //Natural: WRITE /// #DISP-TEXT
            if (Global.isEscape()) return;
            pnd_Abend_Cde.setValue(0);                                                                                                                                    //Natural: ASSIGN #ABEND-CDE := 0
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
            sub_Terminate_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet84 = 0;                                                                                                                                  //Natural: DECIDE ON FIRST VALUE OF #FCPL720A.#CNTRCT-ORGN-CDE;//Natural: VALUE 'DC'
        if (condition((ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet84++;
            pnd_Ws_Pnd_Program.setValue("FCPP374");                                                                                                                       //Natural: ASSIGN #PROGRAM := 'FCPP374'
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde().equals("MS"))))
        {
            decideConditionsMet84++;
            pnd_Ws_Pnd_Program.setValue("FCPP387");                                                                                                                       //Natural: ASSIGN #PROGRAM := 'FCPP387'
        }                                                                                                                                                                 //Natural: ANY VALUE
        if (condition(decideConditionsMet84 > 0))
        {
            Global.getSTACK().pushData(StackOption.TOP, ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx());                                                                      //Natural: FETCH RETURN #PROGRAM #CNTL-IDX
            DbsUtil.invokeMain(DbsUtil.getBlType(pnd_Ws_Pnd_Program), getCurrentProcessState());
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Abend_Cde.setValue(43);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 43
            getReports().write(0, Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Invalid Origin Code:",ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde()); //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Invalid Origin Code:' #FCPL720A.#CNTRCT-ORGN-CDE
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM TERMINATE-PROCESSING
            sub_Terminate_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TERMINATE-PROCESSING
    }
    private void sub_Terminate_Processing() throws Exception                                                                                                              //Natural: TERMINATE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        getWorkFiles().close(8);                                                                                                                                          //Natural: CLOSE WORK FILE 8
        getWorkFiles().close(9);                                                                                                                                          //Natural: CLOSE WORK FILE 9
        DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                              //Natural: TERMINATE #ABEND-CDE
        getWorkFiles().write(8, false, "SAME AS 'WRITE WORK FILE 9' STATEMENT BELLOW");                                                                                   //Natural: WRITE WORK FILE 8 'SAME AS "WRITE WORK FILE 9" STATEMENT BELLOW'
        getWorkFiles().write(9, false, "THIS STATEMENT WILL NEVER BE EXECUTED DUE TO THE", "TERMINATE STATEMENT ABOVE. THIS STATEMENT IS NEEDED BECAUSE THE WORK",        //Natural: WRITE WORK FILE 9 'THIS STATEMENT WILL NEVER BE EXECUTED DUE TO THE' 'TERMINATE STATEMENT ABOVE. THIS STATEMENT IS NEEDED BECAUSE THE WORK' 'FILE WILL NOT BE CLOSED PROPRLY IF WORK FILE IS NOT REFERENCE IN' 'THIS PROGRAM'
            "FILE WILL NOT BE CLOSED PROPRLY IF WORK FILE IS NOT REFERENCE IN", "THIS PROGRAM");
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=80 ZP=ON");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(32),"PROCESS",ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde(),run_Rqust_Parameters_Pnd_Rrp_Request,new TabSetting(68),"REPORT: RPT0",NEWLINE,new 
            TabSetting(36),run_Rqust_Parameters_Pnd_Run_Type);
    }
}
