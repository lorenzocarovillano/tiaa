/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:43 PM
**        * FROM NATURAL PROGRAM : Fcpp986
************************************************************
**        * FILE NAME            : Fcpp986.java
**        * CLASS NAME           : Fcpp986
**        * INSTANCE NAME        : Fcpp986
************************************************************
*********************** RL PAYEE MATCH FEB 22, 2006 ********************
* PROGRAM  : FCPP986 (CLONE OF FCPP836)
* SYSTEM   : CPS
* TITLE    : CHECK UPDATE
* FUNCTION : THIS PROGRAM IS TO PRODUCE RECONCILIATION REPORT FOR AP
*    DAILY DCS TO CCP ELIMINATION PROJECT.
* HISTORY  :
*  03/XX/2016  FENDAYA  NEW
**********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp986 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800d pdaFcpa800d;
    private LdaFcplpmnu ldaFcplpmnu;
    private LdaFcplannu ldaFcplannu;
    private PdaFcpa110 pdaFcpa110;
    private PdaFcpaacum pdaFcpaacum;
    private PdaFcpacrpt pdaFcpacrpt;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_S__R_Field_1;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Prev_Hold_Cde;
    private DbsField pnd_Ws_Pnd_First_Check;
    private DbsField pnd_Ws_Pnd_Last_Check;
    private DbsField pnd_Ws_Pnd_Chk_Miss_Start;
    private DbsField pnd_Ws_Pnd_Chk_Miss_End;
    private DbsField pnd_Ws_Pnd_Missing_Checks;
    private DbsField pnd_Ws_Pnd_Missing_Checks_1;
    private DbsField pnd_Ws_Pnd_Rec_Updated;
    private DbsField pnd_Ws_Pnd_Update_Cnt;
    private DbsField pnd_Ws_Pnd_Et_Cnt;
    private DbsField pnd_Ws_Pnd_Pymnt;
    private DbsField pnd_Ws_Pnd_First_Check_Ind;
    private DbsField pnd_Ws_Pnd_Pymnt_Not_Found;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Annot_Key;

    private DbsGroup pnd_Ws_Annot_Key__R_Field_3;

    private DbsGroup pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        ldaFcplpmnu = new LdaFcplpmnu();
        registerRecord(ldaFcplpmnu);
        registerRecord(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());
        ldaFcplannu = new LdaFcplannu();
        registerRecord(ldaFcplannu);
        registerRecord(ldaFcplannu.getVw_fcp_Cons_Annu());
        pdaFcpa110 = new PdaFcpa110(localVariables);
        pdaFcpaacum = new PdaFcpaacum(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);

        // Local Variables

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_1", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_1.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 29);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Prev_Hold_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Hold_Cde", "#PREV-HOLD-CDE", FieldType.STRING, 4);
        pnd_Ws_Pnd_First_Check = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Check", "#FIRST-CHECK", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Last_Check = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Last_Check", "#LAST-CHECK", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Chk_Miss_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Chk_Miss_Start", "#CHK-MISS-START", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Chk_Miss_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Chk_Miss_End", "#CHK-MISS-END", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Missing_Checks = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Missing_Checks", "#MISSING-CHECKS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Missing_Checks_1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Missing_Checks_1", "#MISSING-CHECKS-1", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Rec_Updated = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Updated", "#REC-UPDATED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Update_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Update_Cnt", "#UPDATE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Et_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Pymnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt", "#PYMNT", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_First_Check_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Check_Ind", "#FIRST-CHECK-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Pymnt_Not_Found = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Not_Found", "#PYMNT-NOT-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Annot_Key = localVariables.newFieldInRecord("pnd_Ws_Annot_Key", "#WS-ANNOT-KEY", FieldType.STRING, 29);

        pnd_Ws_Annot_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Annot_Key__R_Field_3", "REDEFINE", pnd_Ws_Annot_Key);

        pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields = pnd_Ws_Annot_Key__R_Field_3.newGroupInGroup("pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields", "#WS-ANNOT-KEY-FIELDS");
        pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Annot_Key_Cntrct_Invrse_Dte = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Annot_Key_Cntrct_Orgn_Cde = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmnu.initializeValues();
        ldaFcplannu.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Et_Cnt.setInitialValue(150);
        pnd_Ws_Pnd_First_Check_Ind.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp986() throws Exception
    {
        super("Fcpp986");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 15 ) PS = 58 LS = 133 ZP = ON;//Natural: WRITE ( 2 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 39T 'ANNUITY PAYMENTS Cancels and Redraws DCS ELIMINATION REPORT' 120T 'REPORT: RPT2' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: ASSIGN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) := #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 ) := #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 ) := TRUE
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).setValue(true);
        //*  PERFORM GET-CHECK-FORMATTING-DATA /* RL PAYEE MATCH
        //*  WF-PYMNT-ADDR-REC
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 PYMNT-ADDR-INFO INV-INFO ( * )
        while (condition(getWorkFiles().read(1, pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            //*       #CHECK-SORT-FIELDS
            //*       PYMNT-ADDR-INFO
            //*       #CHECK-FIELDS
            //*       INV-INFO(1:INV-ACCT-COUNT)
            pnd_Pymnt_S.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                              //Natural: MOVE BY NAME WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO TO #PYMNT-S
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-INSTMT-NBR = 1
            {
                                                                                                                                                                          //Natural: PERFORM NEW-PYMNT
                sub_New_Pymnt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  LEON 'CN' 'SN'  08/06/99
            //*  JWO 2010-11
            //*  FE201408
            //*  FE201408  /* JWO 2010-11
            short decideConditionsMet926 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'CN' OR = 'CP' OR = 'RP' OR = 'PR' OR = 'AP'
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AP")))
            {
                decideConditionsMet926++;
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(19);                                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 19
            }                                                                                                                                                             //Natural: WHEN WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'S' OR = 'SN' OR = 'SP' OR = 'AS'
            else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
                || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AS")))
            {
                decideConditionsMet926++;
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(20);                                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 20
            }                                                                                                                                                             //Natural: WHEN WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
            else if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))
            {
                decideConditionsMet926++;
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(new DbsDecimal("0.00"))))                                                         //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT = 0.00
                {
                    pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(15);                                                                                           //Natural: ASSIGN #ACCUM-OCCUR := 15
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(1);                                                                                            //Natural: ASSIGN #ACCUM-OCCUR := 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet926 > 0))
            {
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                sub_Accum_Control_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(false);                                                                                          //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := FALSE
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  PERFORM UPDATE-PYMNT-FILE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Rec_Updated.nadd(pnd_Ws_Pnd_Update_Cnt);                                                                                                               //Natural: ADD #UPDATE-CNT TO #REC-UPDATED
        //*  END TRANSACTION
        getReports().write(2, "STARTING CHECK NUMBER.......  ",pnd_Ws_Pnd_First_Check,NEWLINE,"ENDING   CHECK NUMBER.......  ",pnd_Ws_Pnd_Last_Check,NEWLINE,"TOTAL MISSING CHECKS........",pnd_Ws_Pnd_Missing_Checks,  //Natural: WRITE ( 2 ) 'STARTING CHECK NUMBER.......  ' #FIRST-CHECK / 'ENDING   CHECK NUMBER.......  ' #LAST-CHECK / 'TOTAL MISSING CHECKS........' #MISSING-CHECKS / 'TOTAL CHECKS................' #FCPAACUM.#PYMNT-CNT ( 1 ) / 'TOTAL ZERO CHECKS...........' #FCPAACUM.#PYMNT-CNT ( 15 ) / 'RECORDS UPDATED.............' #REC-UPDATED
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"TOTAL CHECKS................",pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(1),NEWLINE,"TOTAL ZERO CHECKS...........",pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(15),NEWLINE,"RECORDS UPDATED.............",pnd_Ws_Pnd_Rec_Updated, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                       //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1).setValue(true);                                                                                         //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1 ) := #FCPACRPT.#TRUTH-TABLE ( 15 ) := #FCPACRPT.#TRUTH-TABLE ( 19 ) := #FCPACRPT.#TRUTH-TABLE ( 20 ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(15).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(19).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(20).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("   Annuity Payments CSR Batch Update");                                                                         //Natural: ASSIGN #FCPACRPT.#TITLE := '   Annuity Payments CSR Batch Update'
        DbsUtil.callnat(Fcpncrpt.class , getCurrentProcessState(), pdaFcpaacum.getPnd_Fcpaacum(), pdaFcpacrpt.getPnd_Fcpacrpt());                                         //Natural: CALLNAT 'FCPNCRPT' USING #FCPAACUM #FCPACRPT
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Ws_Pnd_Terminate.getBoolean()))                                                                                                                 //Natural: IF #TERMINATE
        {
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-IF
        //* **********************************
        //*  DEFINE SUBROUTINE UPDATE-PYMNT-FILE
        //* **********************************
        //*  #PYMNT-NOT-FOUND               := TRUE
        //*  READ (1)  FCP-CONS-PYMNU BY PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE
        //*   IF      FCP-CONS-PYMNU.CNTRCT-PPCN-NBR   = #PYMNT-S.CNTRCT-PPCN-NBR
        //*       AND FCP-CONS-PYMNU.CNTRCT-INVRSE-DTE = #PYMNT-S.CNTRCT-INVRSE-DTE
        //*       AND FCP-CONS-PYMNU.CNTRCT-ORGN-CDE   = #PYMNT-S.CNTRCT-ORGN-CDE
        //*       AND FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NBR
        //*       =   #PYMNT-S.PYMNT-PRCSS-SEQ-NBR
        //*     IGNORE
        //*   ELSE
        //*      ESCAPE BOTTOM
        //*  END-IF
        //*  #PYMNT-NOT-FOUND                       := FALSE
        //*  IF #PYMNT
        //* *********************** RL BEGIN - PAYEE MATCH ************ MAY 10,2006
        //*    IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND =  1
        //*      MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
        //*      MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        //*      MOVE #WS-CHECK-NBR-N10 TO FCP-CONS-PYMNU.PYMNT-NBR
        //*      COMPUTE FCP-CONS-PYMNU.PYMNT-CHECK-NBR =
        //*        WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR * -1
        //*    ELSE
        //*      FCP-CONS-PYMNU.PYMNT-CHECK-NBR       :=
        //*        WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
        //*    END-IF
        //* ************************** RL END-PAYEE MATCH *************************
        //*    FCP-CONS-PYMNU.PYMNT-CHECK-SCRTY-NBR :=
        //*      WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR
        //*    FCP-CONS-PYMNU.PYMNT-CHECK-SEQ-NBR   :=
        //*      WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SEQ-NBR
        //*    FCP-CONS-PYMNU.PYMNT-CHECK-AMT       :=
        //*      WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT
        //*    IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND =  1   AND    /* 02-16-2000
        //*        FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
        //*      PERFORM CREATE-ANNOT-FOR-HOLD
        //*    END-IF
        //*  END-IF
        //*  FCP-CONS-PYMNU.PYMNT-STATS-CDE         := 'P'
        //*  ADD  1                                 TO #UPDATE-CNT
        //*  UPDATE
        //*  IF #UPDATE-CNT GE #ET-CNT
        //*    ADD   #UPDATE-CNT                    TO #REC-UPDATED
        //*    RESET #UPDATE-CNT
        //*    END TRANSACTION
        //*  END-IF
        //*  END-READ
        //*  IF #PYMNT-NOT-FOUND
        //*  #TERMINATE                  := TRUE
        //*  PERFORM ERROR-DISPLAY-START
        //*  WRITE
        //*    '***' 25T 'PAYMENT RECORD IS NOT FOUND'                77T '***' /
        //*    '***' 25T 'TIAA PPCN#  :' #PYMNT-S.CNTRCT-PPCN-NBR     77T '***' /
        //*     '***' 25T 'INVERSE DATE:' #PYMNT-S.CNTRCT-INVRSE-DTE   77T '***' /
        //*     '***' 25T 'ORIGIN      :' #PYMNT-S.CNTRCT-ORGN-CDE     77T '***' /
        //*     '***' 25T 'SEQUENCE#   :' #PYMNT-S.PYMNT-PRCSS-SEQ-NBR 77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*  END-IF
        //*  END-SUBROUTINE
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-PYMNT
        //* **************************
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MISSING-CHECKS
        //* *******************************
        //* *******************************************
        //*  DEFINE SUBROUTINE GET-CHECK-FORMATTING-DATA
        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        //*  MOVE 'NEXT'  TO FCPA110.FCPA110-FUNCTION
        //*  MOVE  'P14A1' TO FCPA110.FCPA110-SOURCE-CODE  /* RL
        //*  CALLNAT 'FCPN110' FCPA110
        //*  IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        //*   PERFORM ERROR-DISPLAY-START
        //*   WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG(EM=X(61)) 77T '***'
        //*     / FCPA110.FCPA110-RETURN-CODE 77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*   TERMINATE 53
        //*  END-IF
        //*  MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3          /*RL
        //*  END-SUBROUTINE
        //* ************************** RL END-PAYEE MATCH *************************
        //* ***********************************************************************
        //*  COPYCODE   : FCPCACUM
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : "AP" - CONTROL ACCUMULATIONS.
        //* ***********************************************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-TOTALS
    }
    private void sub_New_Pymnt() throws Exception                                                                                                                         //Natural: NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := TRUE
        //*  LEON 08-06-99
        //*  JWO 2010-11
        //*  FE201408
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S")  //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR' OR = 'AS' OR = 'AP'
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") 
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AS") || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AP")))
        {
            pnd_Ws_Pnd_Pymnt.setValue(false);                                                                                                                             //Natural: ASSIGN #PYMNT := FALSE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Pymnt.setValue(true);                                                                                                                              //Natural: ASSIGN #PYMNT := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(pnd_Ws_Pnd_Pymnt.getBoolean() && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().greaterOrEqual(1) && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().lessOrEqual(9999998))) //Natural: IF #PYMNT AND WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR = 1 THRU 9999998
        {
            short decideConditionsMet1075 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FIRST-CHECK-IND
            if (condition(pnd_Ws_Pnd_First_Check_Ind.getBoolean()))
            {
                decideConditionsMet1075++;
                pnd_Ws_Pnd_First_Check_Ind.setValue(false);                                                                                                               //Natural: ASSIGN #FIRST-CHECK-IND := FALSE
                pnd_Ws_Pnd_First_Check.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                      //Natural: ASSIGN #FIRST-CHECK := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: WHEN #PREV-HOLD-CDE NE ' ' AND WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = ' '
            else if (condition(pnd_Ws_Pnd_Prev_Hold_Cde.notEquals(" ") && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(" ")))
            {
                decideConditionsMet1075++;
                ignore();
            }                                                                                                                                                             //Natural: WHEN ( #LAST-CHECK + 1 ) NE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
            else if (condition(pnd_Ws_Pnd_Last_Check.add(1).notEquals(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr())))
            {
                decideConditionsMet1075++;
                                                                                                                                                                          //Natural: PERFORM MISSING-CHECKS
                sub_Missing_Checks();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Ws_Pnd_Last_Check.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                           //Natural: ASSIGN #LAST-CHECK := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR
            pnd_Ws_Pnd_Prev_Hold_Cde.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());                                                                        //Natural: ASSIGN #PREV-HOLD-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Missing_Checks() throws Exception                                                                                                                    //Natural: MISSING-CHECKS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Chk_Miss_Start.compute(new ComputeParameters(false, pnd_Ws_Pnd_Chk_Miss_Start), pnd_Ws_Pnd_Last_Check.add(1));                                         //Natural: ASSIGN #CHK-MISS-START := #LAST-CHECK + 1
        pnd_Ws_Pnd_Chk_Miss_End.compute(new ComputeParameters(false, pnd_Ws_Pnd_Chk_Miss_End), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr().subtract(1));           //Natural: ASSIGN #CHK-MISS-END := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR - 1
        pnd_Ws_Pnd_Missing_Checks_1.compute(new ComputeParameters(false, pnd_Ws_Pnd_Missing_Checks_1), pnd_Ws_Pnd_Chk_Miss_End.subtract(pnd_Ws_Pnd_Last_Check));          //Natural: ASSIGN #MISSING-CHECKS-1 := #CHK-MISS-END - #LAST-CHECK
        pnd_Ws_Pnd_Missing_Checks.nadd(pnd_Ws_Pnd_Missing_Checks_1);                                                                                                      //Natural: ASSIGN #MISSING-CHECKS := #MISSING-CHECKS + #MISSING-CHECKS-1
        if (condition(pnd_Ws_Pnd_Missing_Checks_1.equals(1)))                                                                                                             //Natural: IF #MISSING-CHECKS-1 = 1
        {
            getReports().write(2, "CHECK ",pnd_Ws_Pnd_Chk_Miss_Start,new ColumnSpacing(18),"IS  MISSING",pnd_Ws_Pnd_Missing_Checks_1, new ReportEditMask                  //Natural: WRITE ( 2 ) 'CHECK ' #CHK-MISS-START 18X 'IS  MISSING' #MISSING-CHECKS-1 'CHECK'
                ("-Z,ZZZ,ZZ9"),"CHECK");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(2, "CHECKS",pnd_Ws_Pnd_Chk_Miss_Start,"THROUGH",pnd_Ws_Pnd_Chk_Miss_End,"ARE MISSING",pnd_Ws_Pnd_Missing_Checks_1, new                     //Natural: WRITE ( 2 ) 'CHECKS' #CHK-MISS-START 'THROUGH' #CHK-MISS-END 'ARE MISSING' #MISSING-CHECKS-1 'CHECKS'
                ReportEditMask ("-Z,ZZZ,ZZ9"),"CHECKS");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Control_Totals() throws Exception                                                                                                              //Natural: ACCUM-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  #MAX-OCCUR
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().greaterOrEqual(1) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().lessOrEqual(50)))                  //Natural: IF #ACCUM-OCCUR = 1 THRU 50
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund().setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                     //Natural: ASSIGN #@@MAX-FUND := WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        short decideConditionsMet1110 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPAACUM.#NEW-PYMNT-IND
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                  //Natural: ADD 1 TO #FCPAACUM.#PYMNT-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 3 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(3).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Cntrct_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#CNTRCT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 4 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(4).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dvdnd_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DVDND-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 5 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(5).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dci_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DCI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DCI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 6 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(6).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dpi_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DPI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DPI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#NET-PYMNT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 8 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(8).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Fdrl_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#FDRL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 9 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(9).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_State_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#STATE-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 10 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(10).equals(true)))
        {
            decideConditionsMet1110++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Local_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#LOCAL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1110 > 0))
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                    //Natural: ADD 1 TO #FCPAACUM.#REC-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet1110 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(2, "PS=58 LS=132 ZP=ON");
        Global.format(15, "PS=58 LS=133 ZP=ON");

        getReports().write(2, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(39),"ANNUITY PAYMENTS Cancels and Redraws DCS ELIMINATION REPORT",new 
            TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);
    }
}
