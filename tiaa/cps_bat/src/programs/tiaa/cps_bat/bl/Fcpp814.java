/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:21 PM
**        * FROM NATURAL PROGRAM : Fcpp814
************************************************************
**        * FILE NAME            : Fcpp814.java
**        * CLASS NAME           : Fcpp814
**        * INSTANCE NAME        : Fcpp814
************************************************************
**********************************************************************
*
* PROGRAM   : FCPP814
*
* SYSTEM    : CPS
* TITLE     : GA PAYMENT REGISTER RPT WITH ADDRESSES (WAS UPENN ONLY).
* GENERATED : 03/03/08
*
* FUNCTION  : GENERATE REPORT "GROUP ANNUITY PMT RGSTR with Addresses"
*           :
* HISTORY
* 03/03/08  : LCWILLIAMS - CLONED FROM FCPP813.
*
* 05/07/08 : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 04/24/2012 :R.SACHARNY - REPORT RANGE CHANGES (RS1)
* 04/09/2013 :B.HOLLOWAY - EXPAND THIS PROGRAM TO INCLUDE ALL GROUP
*                          ANNUITY CONTRACTS (W0250000 THROUGH W0899999)
*                          AND TO WRITE SUBTOTALS BASED ON THE SAME
*                          RANGES AS THE REGULAR GROUP ANNUITY
*                          PAYMENT REGISTER REPORT (CP2320M9/FCPP813).
*                          CHANGE TAG IS DBH1.
* 04/01/2014 F.ENDAYA      CREF REA PROJECT. RESTOW THE MODULE TO
*                          PICK UP LATEST VERSION OF NECA4000.
* 11/20/2014 F.ENDAYA      GROUP ANNUITY PAYMENT REGISTER PROJECT.
* FE201411                 REFORMAT NAME AND ADDRESS IN CP2320MG REPORT.
* 05/09/2017 SAI K         RECOMPILE DUE TO CHANGE IN FCPA800
* 11/20/2018 J.OSTEEN - ADD CONTRACT RANGE W053 -- JWO1
**********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp814 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa803c pdaFcpa803c;
    private PdaNeca4000 pdaNeca4000;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Name_Full;
    private DbsField pnd_State_Cde_A;
    private DbsField pnd_State_Cde_N;
    private DbsField pnd_Rec_Count;

    private DbsGroup pnd_Dtl_Amts;
    private DbsField pnd_Dtl_Amts_Pnd_Contract_Amt;
    private DbsField pnd_Dtl_Amts_Pnd_Dvdnd_Amt;
    private DbsField pnd_Dtl_Amts_Pnd_Fed_Amt;
    private DbsField pnd_Dtl_Amts_Pnd_Tax_Amt;
    private DbsField pnd_Dtl_Amts_Pnd_Ded_Amt;
    private DbsField pnd_Dtl_Amts_Pnd_Net_Amt;
    private DbsField pnd_Pymnt_Nbr;

    private DbsGroup pnd_Pymnt_Nbr__R_Field_1;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Type;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Num;
    private DbsField pnd_Old_Cntrct_Ppcn_A4;
    private DbsField pnd_Old_Cntrct_Ppcn_A5;
    private DbsField pnd_Old_Cntrct_Ppcn_A8;

    private DbsGroup pnd_Ga_Sub_Totals;
    private DbsField pnd_Ga_Sub_Totals_Pnd_Sub_Count;
    private DbsField pnd_Ga_Sub_Totals_Pnd_Sub_Cntrct;
    private DbsField pnd_Ga_Sub_Totals_Pnd_Sub_Dvdnd;
    private DbsField pnd_Ga_Sub_Totals_Pnd_Sub_Fed;
    private DbsField pnd_Ga_Sub_Totals_Pnd_Sub_Tax;
    private DbsField pnd_Ga_Sub_Totals_Pnd_Sub_Ded;
    private DbsField pnd_Ga_Sub_Totals_Pnd_Sub_Net;
    private DbsField pnd_Tot_Count;
    private DbsField pnd_Tot_Cntrct;
    private DbsField pnd_Tot_Dvdnd;
    private DbsField pnd_Tot_Fed;
    private DbsField pnd_Tot_Tax;
    private DbsField pnd_Tot_Ded;
    private DbsField pnd_Tot_Net;
    private DbsField pnd_Index;
    private DbsField pnd_T_Amount;
    private DbsField pnd_I_Amount;
    private DbsField pnd_Pa_Amount;
    private DbsField pnd_R_Amount;
    private DbsField pnd_C_Amount;
    private DbsField pnd_B_Amount;
    private DbsField pnd_M_Amount;
    private DbsField pnd_S_Amount;
    private DbsField pnd_W_Amount;
    private DbsField pnd_L_Amount;
    private DbsField pnd_E_Amount;
    private DbsField pnd_Tiaa_Amount;
    private DbsField pnd_Cref_Amount;
    private DbsField pnd_Grand_Amount;
    private DbsField pnd_Pa_Select_Fixed;
    private DbsField pnd_Pa_Select_Stock_Index;
    private DbsField pnd_Pa_Select_Growth_Equity;
    private DbsField pnd_Pa_Select_Growth_Income;
    private DbsField pnd_Pa_Select_Intrntl_Equity;
    private DbsField pnd_Pa_Select_Social_Ch_Equity;
    private DbsField pnd_Pa_Select_Lcv;
    private DbsField pnd_Pa_Select_Scv;
    private DbsField pnd_Pa_Select_Real;
    private DbsField pnd_Spia_Select_Fixed;
    private DbsField pnd_Spia_Select_Free_Look;
    private DbsField pnd_Spia_Select_Stock_Index;
    private DbsField pnd_Spia_Select_Growth_Equity;
    private DbsField pnd_Spia_Select_Growth_Income;
    private DbsField pnd_Spia_Select_Intrntl_Equity;
    private DbsField pnd_Spia_Select_Social_Ch_Equity;
    private DbsField pnd_Spia_Lcv;
    private DbsField pnd_Spia_Scv;
    private DbsField pnd_Spia_Real;
    private DbsField pnd_Pa_Select_Total;
    private DbsField pnd_Spia_Select_Total;
    private DbsField pnd_Sdm_Ind;
    private DbsField pnd_Mm_Ind;
    private DbsField pnd_Print_Sub_Totals_1;
    private DbsField pnd_Print_Sub_Totals_2;
    private DbsField pnd_First_One_In_Range;
    private DbsField pnd_D_Check_Dte;
    private DbsField pnd_One_Line_Addr;

    private DbsGroup pnd_Name;
    private DbsField pnd_Name_Pnd_Name1;

    private DbsGroup pnd_Name__R_Field_2;
    private DbsField pnd_Name_Pnd_Name_Arr;
    private DbsField pnd_Name_Pnd_Name_Last;
    private DbsField pnd_Name_Pnd_Name_3rd;
    private DbsField pnd_Name_Pnd_Name_2nd;
    private DbsField pnd_Name_Pnd_Name_First;
    private DbsField pnd_Name_Pnd_X;
    private DbsField pnd_Name_Pnd_I;
    private DbsField pnd_Name_Pnd_N;
    private DbsField pnd_I2;
    private DbsField pnd_J;
    private DbsField pnd_Name_Tab;

    private DbsGroup pnd_Name_Tab__R_Field_3;
    private DbsField pnd_Name_Tab_Pnd_N_A;
    private DbsField pnd_N_Out;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa803c = new PdaFcpa803c(localVariables);
        pdaNeca4000 = new PdaNeca4000(localVariables);

        // Local Variables
        pnd_Name_Full = localVariables.newFieldInRecord("pnd_Name_Full", "#NAME-FULL", FieldType.STRING, 38);
        pnd_State_Cde_A = localVariables.newFieldInRecord("pnd_State_Cde_A", "#STATE-CDE-A", FieldType.STRING, 3);
        pnd_State_Cde_N = localVariables.newFieldInRecord("pnd_State_Cde_N", "#STATE-CDE-N", FieldType.STRING, 2);
        pnd_Rec_Count = localVariables.newFieldInRecord("pnd_Rec_Count", "#REC-COUNT", FieldType.PACKED_DECIMAL, 4);

        pnd_Dtl_Amts = localVariables.newGroupInRecord("pnd_Dtl_Amts", "#DTL-AMTS");
        pnd_Dtl_Amts_Pnd_Contract_Amt = pnd_Dtl_Amts.newFieldInGroup("pnd_Dtl_Amts_Pnd_Contract_Amt", "#CONTRACT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dtl_Amts_Pnd_Dvdnd_Amt = pnd_Dtl_Amts.newFieldInGroup("pnd_Dtl_Amts_Pnd_Dvdnd_Amt", "#DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dtl_Amts_Pnd_Fed_Amt = pnd_Dtl_Amts.newFieldInGroup("pnd_Dtl_Amts_Pnd_Fed_Amt", "#FED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dtl_Amts_Pnd_Tax_Amt = pnd_Dtl_Amts.newFieldInGroup("pnd_Dtl_Amts_Pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dtl_Amts_Pnd_Ded_Amt = pnd_Dtl_Amts.newFieldInGroup("pnd_Dtl_Amts_Pnd_Ded_Amt", "#DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dtl_Amts_Pnd_Net_Amt = pnd_Dtl_Amts.newFieldInGroup("pnd_Dtl_Amts_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pymnt_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Nbr", "#PYMNT-NBR", FieldType.STRING, 11);

        pnd_Pymnt_Nbr__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_Nbr__R_Field_1", "REDEFINE", pnd_Pymnt_Nbr);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Type = pnd_Pymnt_Nbr__R_Field_1.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Type", "#PYMNT-TYPE", FieldType.STRING, 1);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num = pnd_Pymnt_Nbr__R_Field_1.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Num", "#PYMNT-NUM", FieldType.NUMERIC, 10);
        pnd_Old_Cntrct_Ppcn_A4 = localVariables.newFieldInRecord("pnd_Old_Cntrct_Ppcn_A4", "#OLD-CNTRCT-PPCN-A4", FieldType.STRING, 4);
        pnd_Old_Cntrct_Ppcn_A5 = localVariables.newFieldInRecord("pnd_Old_Cntrct_Ppcn_A5", "#OLD-CNTRCT-PPCN-A5", FieldType.STRING, 5);
        pnd_Old_Cntrct_Ppcn_A8 = localVariables.newFieldInRecord("pnd_Old_Cntrct_Ppcn_A8", "#OLD-CNTRCT-PPCN-A8", FieldType.STRING, 8);

        pnd_Ga_Sub_Totals = localVariables.newGroupInRecord("pnd_Ga_Sub_Totals", "#GA-SUB-TOTALS");
        pnd_Ga_Sub_Totals_Pnd_Sub_Count = pnd_Ga_Sub_Totals.newFieldInGroup("pnd_Ga_Sub_Totals_Pnd_Sub_Count", "#SUB-COUNT", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ga_Sub_Totals_Pnd_Sub_Cntrct = pnd_Ga_Sub_Totals.newFieldInGroup("pnd_Ga_Sub_Totals_Pnd_Sub_Cntrct", "#SUB-CNTRCT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ga_Sub_Totals_Pnd_Sub_Dvdnd = pnd_Ga_Sub_Totals.newFieldInGroup("pnd_Ga_Sub_Totals_Pnd_Sub_Dvdnd", "#SUB-DVDND", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ga_Sub_Totals_Pnd_Sub_Fed = pnd_Ga_Sub_Totals.newFieldInGroup("pnd_Ga_Sub_Totals_Pnd_Sub_Fed", "#SUB-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ga_Sub_Totals_Pnd_Sub_Tax = pnd_Ga_Sub_Totals.newFieldInGroup("pnd_Ga_Sub_Totals_Pnd_Sub_Tax", "#SUB-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ga_Sub_Totals_Pnd_Sub_Ded = pnd_Ga_Sub_Totals.newFieldInGroup("pnd_Ga_Sub_Totals_Pnd_Sub_Ded", "#SUB-DED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ga_Sub_Totals_Pnd_Sub_Net = pnd_Ga_Sub_Totals.newFieldInGroup("pnd_Ga_Sub_Totals_Pnd_Sub_Net", "#SUB-NET", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Count = localVariables.newFieldInRecord("pnd_Tot_Count", "#TOT-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Tot_Cntrct = localVariables.newFieldInRecord("pnd_Tot_Cntrct", "#TOT-CNTRCT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Dvdnd = localVariables.newFieldInRecord("pnd_Tot_Dvdnd", "#TOT-DVDND", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Fed = localVariables.newFieldInRecord("pnd_Tot_Fed", "#TOT-FED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Tax = localVariables.newFieldInRecord("pnd_Tot_Tax", "#TOT-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Ded = localVariables.newFieldInRecord("pnd_Tot_Ded", "#TOT-DED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tot_Net = localVariables.newFieldInRecord("pnd_Tot_Net", "#TOT-NET", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_T_Amount = localVariables.newFieldInRecord("pnd_T_Amount", "#T-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_I_Amount = localVariables.newFieldInRecord("pnd_I_Amount", "#I-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pa_Amount = localVariables.newFieldInRecord("pnd_Pa_Amount", "#PA-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_R_Amount = localVariables.newFieldInRecord("pnd_R_Amount", "#R-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_C_Amount = localVariables.newFieldInRecord("pnd_C_Amount", "#C-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_B_Amount = localVariables.newFieldInRecord("pnd_B_Amount", "#B-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_M_Amount = localVariables.newFieldInRecord("pnd_M_Amount", "#M-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_S_Amount = localVariables.newFieldInRecord("pnd_S_Amount", "#S-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_W_Amount = localVariables.newFieldInRecord("pnd_W_Amount", "#W-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_L_Amount = localVariables.newFieldInRecord("pnd_L_Amount", "#L-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_E_Amount = localVariables.newFieldInRecord("pnd_E_Amount", "#E-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tiaa_Amount = localVariables.newFieldInRecord("pnd_Tiaa_Amount", "#TIAA-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cref_Amount = localVariables.newFieldInRecord("pnd_Cref_Amount", "#CREF-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Grand_Amount = localVariables.newFieldInRecord("pnd_Grand_Amount", "#GRAND-AMOUNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pa_Select_Fixed = localVariables.newFieldInRecord("pnd_Pa_Select_Fixed", "#PA-SELECT-FIXED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pa_Select_Stock_Index = localVariables.newFieldInRecord("pnd_Pa_Select_Stock_Index", "#PA-SELECT-STOCK-INDEX", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Pa_Select_Growth_Equity = localVariables.newFieldInRecord("pnd_Pa_Select_Growth_Equity", "#PA-SELECT-GROWTH-EQUITY", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Pa_Select_Growth_Income = localVariables.newFieldInRecord("pnd_Pa_Select_Growth_Income", "#PA-SELECT-GROWTH-INCOME", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Pa_Select_Intrntl_Equity = localVariables.newFieldInRecord("pnd_Pa_Select_Intrntl_Equity", "#PA-SELECT-INTRNTL-EQUITY", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Pa_Select_Social_Ch_Equity = localVariables.newFieldInRecord("pnd_Pa_Select_Social_Ch_Equity", "#PA-SELECT-SOCIAL-CH-EQUITY", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Pa_Select_Lcv = localVariables.newFieldInRecord("pnd_Pa_Select_Lcv", "#PA-SELECT-LCV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pa_Select_Scv = localVariables.newFieldInRecord("pnd_Pa_Select_Scv", "#PA-SELECT-SCV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pa_Select_Real = localVariables.newFieldInRecord("pnd_Pa_Select_Real", "#PA-SELECT-REAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Spia_Select_Fixed = localVariables.newFieldInRecord("pnd_Spia_Select_Fixed", "#SPIA-SELECT-FIXED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Spia_Select_Free_Look = localVariables.newFieldInRecord("pnd_Spia_Select_Free_Look", "#SPIA-SELECT-FREE-LOOK", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Spia_Select_Stock_Index = localVariables.newFieldInRecord("pnd_Spia_Select_Stock_Index", "#SPIA-SELECT-STOCK-INDEX", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Spia_Select_Growth_Equity = localVariables.newFieldInRecord("pnd_Spia_Select_Growth_Equity", "#SPIA-SELECT-GROWTH-EQUITY", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Spia_Select_Growth_Income = localVariables.newFieldInRecord("pnd_Spia_Select_Growth_Income", "#SPIA-SELECT-GROWTH-INCOME", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Spia_Select_Intrntl_Equity = localVariables.newFieldInRecord("pnd_Spia_Select_Intrntl_Equity", "#SPIA-SELECT-INTRNTL-EQUITY", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Spia_Select_Social_Ch_Equity = localVariables.newFieldInRecord("pnd_Spia_Select_Social_Ch_Equity", "#SPIA-SELECT-SOCIAL-CH-EQUITY", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Spia_Lcv = localVariables.newFieldInRecord("pnd_Spia_Lcv", "#SPIA-LCV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Spia_Scv = localVariables.newFieldInRecord("pnd_Spia_Scv", "#SPIA-SCV", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Spia_Real = localVariables.newFieldInRecord("pnd_Spia_Real", "#SPIA-REAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Pa_Select_Total = localVariables.newFieldInRecord("pnd_Pa_Select_Total", "#PA-SELECT-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Spia_Select_Total = localVariables.newFieldInRecord("pnd_Spia_Select_Total", "#SPIA-SELECT-TOTAL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Sdm_Ind = localVariables.newFieldInRecord("pnd_Sdm_Ind", "#SDM-IND", FieldType.BOOLEAN, 1);
        pnd_Mm_Ind = localVariables.newFieldInRecord("pnd_Mm_Ind", "#MM-IND", FieldType.BOOLEAN, 1);
        pnd_Print_Sub_Totals_1 = localVariables.newFieldInRecord("pnd_Print_Sub_Totals_1", "#PRINT-SUB-TOTALS-1", FieldType.BOOLEAN, 1);
        pnd_Print_Sub_Totals_2 = localVariables.newFieldArrayInRecord("pnd_Print_Sub_Totals_2", "#PRINT-SUB-TOTALS-2", FieldType.BOOLEAN, 1, new DbsArrayController(1, 
            11));
        pnd_First_One_In_Range = localVariables.newFieldArrayInRecord("pnd_First_One_In_Range", "#FIRST-ONE-IN-RANGE", FieldType.BOOLEAN, 1, new DbsArrayController(1, 
            11));
        pnd_D_Check_Dte = localVariables.newFieldInRecord("pnd_D_Check_Dte", "#D-CHECK-DTE", FieldType.DATE);
        pnd_One_Line_Addr = localVariables.newFieldInRecord("pnd_One_Line_Addr", "#ONE-LINE-ADDR", FieldType.STRING, 210);

        pnd_Name = localVariables.newGroupInRecord("pnd_Name", "#NAME");
        pnd_Name_Pnd_Name1 = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Name1", "#NAME1", FieldType.STRING, 38);

        pnd_Name__R_Field_2 = pnd_Name.newGroupInGroup("pnd_Name__R_Field_2", "REDEFINE", pnd_Name_Pnd_Name1);
        pnd_Name_Pnd_Name_Arr = pnd_Name__R_Field_2.newFieldArrayInGroup("pnd_Name_Pnd_Name_Arr", "#NAME-ARR", FieldType.STRING, 1, new DbsArrayController(1, 
            38));
        pnd_Name_Pnd_Name_Last = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Name_Last", "#NAME-LAST", FieldType.STRING, 15);
        pnd_Name_Pnd_Name_3rd = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Name_3rd", "#NAME-3RD", FieldType.STRING, 15);
        pnd_Name_Pnd_Name_2nd = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Name_2nd", "#NAME-2ND", FieldType.STRING, 15);
        pnd_Name_Pnd_Name_First = pnd_Name.newFieldInGroup("pnd_Name_Pnd_Name_First", "#NAME-FIRST", FieldType.STRING, 15);
        pnd_Name_Pnd_X = pnd_Name.newFieldInGroup("pnd_Name_Pnd_X", "#X", FieldType.NUMERIC, 3);
        pnd_Name_Pnd_I = pnd_Name.newFieldInGroup("pnd_Name_Pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_Name_Pnd_N = pnd_Name.newFieldInGroup("pnd_Name_Pnd_N", "#N", FieldType.NUMERIC, 3);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Name_Tab = localVariables.newFieldInRecord("pnd_Name_Tab", "#NAME-TAB", FieldType.STRING, 38);

        pnd_Name_Tab__R_Field_3 = localVariables.newGroupInRecord("pnd_Name_Tab__R_Field_3", "REDEFINE", pnd_Name_Tab);
        pnd_Name_Tab_Pnd_N_A = pnd_Name_Tab__R_Field_3.newFieldArrayInGroup("pnd_Name_Tab_Pnd_N_A", "#N-A", FieldType.STRING, 1, new DbsArrayController(1, 
            38));
        pnd_N_Out = localVariables.newFieldInRecord("pnd_N_Out", "#N-OUT", FieldType.STRING, 38);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Sdm_Ind.setInitialValue(false);
        pnd_Mm_Ind.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp814() throws Exception
    {
        super("Fcpp814");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP814", onError);
        setupReports();
        //*  FE201411                                                                                                                                                     //Natural: FORMAT ( 00 ) LS = 140 PS = 57
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 211 PS = 57
        //*                                                                                                                                                               //Natural: ON ERROR
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 40T 'GROUP ANNUITY PAYMENT REGISTER FOR' #D-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        pnd_First_One_In_Range.getValue("*").setValue(true);                                                                                                              //Natural: MOVE TRUE TO #FIRST-ONE-IN-RANGE ( * )
        //*                    READ PMNT / NAAD WORK FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CHECK-SORT-FIELDS.PYMNT-NBR WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            //*  DBH1 - START
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0250000") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0899999"))) //Natural: IF CNTRCT-PPCN-NBR EQ 'W0250000' THRU 'W0899999'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().less("W0899999")))                                                                        //Natural: IF CNTRCT-PPCN-NBR LT 'W0899999'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM IMMEDIATE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  DBH1 - END
            pnd_D_Check_Dte.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte());                                                                                  //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE TO #D-CHECK-DTE
            pnd_Sdm_Ind.setValue(false);                                                                                                                                  //Natural: MOVE FALSE TO #SDM-IND #MM-IND
            pnd_Mm_Ind.setValue(false);
            pnd_Dtl_Amts.reset();                                                                                                                                         //Natural: RESET #DTL-AMTS
            pnd_Dtl_Amts_Pnd_Contract_Amt.compute(new ComputeParameters(false, pnd_Dtl_Amts_Pnd_Contract_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1, //Natural: ASSIGN #CONTRACT-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:INV-ACCT-COUNT ) + 0
                ":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
            pnd_Dtl_Amts_Pnd_Fed_Amt.compute(new ComputeParameters(false, pnd_Dtl_Amts_Pnd_Fed_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,  //Natural: ASSIGN #FED-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT ) + 0
                ":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
            pnd_Dtl_Amts_Pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Dtl_Amts_Pnd_Tax_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1, //Natural: ASSIGN #TAX-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT ) + 0
                ":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
            pnd_Dtl_Amts_Pnd_Tax_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));     //Natural: ASSIGN #TAX-AMT := #TAX-AMT + WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT )
            pnd_Dtl_Amts_Pnd_Ded_Amt.compute(new ComputeParameters(false, pnd_Dtl_Amts_Pnd_Ded_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").add(getZero())); //Natural: ASSIGN #DED-AMT := WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) + 0
            pnd_Dtl_Amts_Pnd_Net_Amt.compute(new ComputeParameters(false, pnd_Dtl_Amts_Pnd_Net_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1, //Natural: ASSIGN #NET-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT ) + 0
                ":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
            //*  PA-SELECT / SPIA
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") && (pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("D")    //Natural: IF ( ( WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'S' ) AND ( WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE = 'D' OR = 'M' ) )
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M")))))
            {
                pnd_Sdm_Ind.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #SDM-IND
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M"))) //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'M' AND WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE = 'M'
                {
                    pnd_Mm_Ind.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #MM-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 INV-ACCT-COUNT
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") && (pnd_Sdm_Ind.getBoolean() || pnd_Mm_Ind.getBoolean()))) //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' AND ( #SDM-IND OR #MM-IND )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG")  //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T' OR = 'TG' OR = 'G '
                        || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G ")))
                    {
                        pnd_Dtl_Amts_Pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                        //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Dtl_Amts_Pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                        //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #DVDND-AMT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet1661 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX );//Natural: VALUE 'T', 'TG', 'G '
                if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("TG") 
                    || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("G "))))
                {
                    decideConditionsMet1661++;
                    short decideConditionsMet1663 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'I'
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("I")))
                    {
                        decideConditionsMet1663++;
                        pnd_I_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                  //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #I-AMOUNT
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'P'
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("P")))
                    {
                        decideConditionsMet1663++;
                        pnd_Pa_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                 //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-AMOUNT
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' AND #SDM-IND
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") && pnd_Sdm_Ind.getBoolean()))
                    {
                        decideConditionsMet1663++;
                        pnd_Pa_Select_Fixed.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                           //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-FIXED
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #INDEX ) = 'T ' AND #MM-IND
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("T ") && pnd_Mm_Ind.getBoolean()))
                    {
                        decideConditionsMet1663++;
                        pnd_Spia_Select_Fixed.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-SELECT-FIXED
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pnd_T_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                  //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #T-AMOUNT
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("R"))))
                {
                    decideConditionsMet1661++;
                    pnd_R_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #R-AMOUNT
                }                                                                                                                                                         //Natural: VALUE 'C'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("C"))))
                {
                    decideConditionsMet1661++;
                    pnd_C_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #C-AMOUNT
                }                                                                                                                                                         //Natural: VALUE 'B'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("B"))))
                {
                    decideConditionsMet1661++;
                    pnd_B_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #B-AMOUNT
                }                                                                                                                                                         //Natural: VALUE 'M'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("M"))))
                {
                    decideConditionsMet1661++;
                    pnd_M_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #M-AMOUNT
                }                                                                                                                                                         //Natural: VALUE 'S'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("S"))))
                {
                    decideConditionsMet1661++;
                    pnd_S_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #S-AMOUNT
                }                                                                                                                                                         //Natural: VALUE 'W'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("W"))))
                {
                    decideConditionsMet1661++;
                    pnd_W_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #W-AMOUNT
                }                                                                                                                                                         //Natural: VALUE 'L'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("L"))))
                {
                    decideConditionsMet1661++;
                    pnd_L_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #L-AMOUNT
                }                                                                                                                                                         //Natural: VALUE 'E'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("E"))))
                {
                    decideConditionsMet1661++;
                    //*  NO GENERAL LEDGER FOR PA SELECT
                    pnd_E_Amount.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #E-AMOUNT
                }                                                                                                                                                         //Natural: VALUE 'U'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("U"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Select_Free_Look.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-SELECT-FREE-LOOK
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'V'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("V"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Sdm_Ind.getBoolean()))                                                                                                              //Natural: IF #SDM-IND
                    {
                        pnd_Pa_Select_Stock_Index.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-STOCK-INDEX
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Select_Stock_Index.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                   //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-SELECT-STOCK-INDEX
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'N'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("N"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Sdm_Ind.getBoolean()))                                                                                                              //Natural: IF #SDM-IND
                    {
                        pnd_Pa_Select_Growth_Equity.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                   //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-GROWTH-EQUITY
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Select_Growth_Equity.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                 //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-SELECT-GROWTH-EQUITY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'O'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("O"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Sdm_Ind.getBoolean()))                                                                                                              //Natural: IF #SDM-IND
                    {
                        pnd_Pa_Select_Growth_Income.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                   //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-GROWTH-INCOME
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Select_Growth_Income.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                 //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-SELECT-GROWTH-INCOME
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'P'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("P"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Sdm_Ind.getBoolean()))                                                                                                              //Natural: IF #SDM-IND
                    {
                        pnd_Pa_Select_Intrntl_Equity.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                  //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-INTRNTL-EQUITY
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Select_Intrntl_Equity.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-SELECT-INTRNTL-EQUITY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'Q'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("Q"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Sdm_Ind.getBoolean()))                                                                                                              //Natural: IF #SDM-IND
                    {
                        pnd_Pa_Select_Social_Ch_Equity.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-SOCIAL-CH-EQUITY
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Select_Social_Ch_Equity.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                              //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-SELECT-SOCIAL-CH-EQUITY
                    }                                                                                                                                                     //Natural: END-IF
                    //*                  PA-SELECT / SPIA NEW FUNDS 'J, K & X'
                }                                                                                                                                                         //Natural: VALUE 'J'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("J"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Sdm_Ind.getBoolean()))                                                                                                              //Natural: IF #SDM-IND
                    {
                        pnd_Pa_Select_Lcv.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                             //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-LCV
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Lcv.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                  //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-LCV
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'K'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("K"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Sdm_Ind.getBoolean()))                                                                                                              //Natural: IF #SDM-IND
                    {
                        pnd_Pa_Select_Scv.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                             //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-SCV
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Scv.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                  //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-SCV
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'X'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Index).equals("X"))))
                {
                    decideConditionsMet1661++;
                    if (condition(pnd_Sdm_Ind.getBoolean()))                                                                                                              //Natural: IF #SDM-IND
                    {
                        pnd_Pa_Select_Real.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                            //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #PA-SELECT-REAL
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Mm_Ind.getBoolean()))                                                                                                               //Natural: IF #MM-IND
                    {
                        pnd_Spia_Real.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                                                 //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #SPIA-REAL
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE VALUES
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CHECK
            short decideConditionsMet1759 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet1759++;
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("C");                                                                                                               //Natural: MOVE 'C' TO #PYMNT-TYPE
                //*  EFT
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());                                                                   //Natural: MOVE #CHECK-SORT-FIELDS.PYMNT-NBR TO #PYMNT-NUM
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet1759++;
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("E");                                                                                                               //Natural: MOVE 'E' TO #PYMNT-TYPE
                //*  GLOBAL PAY
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr());                                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR TO #PYMNT-NUM
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3))))
            {
                decideConditionsMet1759++;
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("G");                                                                                                               //Natural: MOVE 'G' TO #PYMNT-TYPE
                //*  ROLLOVER
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                  //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #PYMNT-NUM
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8))))
            {
                decideConditionsMet1759++;
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("I");                                                                                                               //Natural: MOVE 'I' TO #PYMNT-TYPE
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr());                                                                  //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR TO #PYMNT-NUM
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            short decideConditionsMet1779 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-PPCN-NBR = 'W0250000' THRU 'W0279999'
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0250000") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0279999")))
            {
                decideConditionsMet1779++;
                pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR = 'W0280000' THRU 'W0299999'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0280000") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0299999")))
            {
                decideConditionsMet1779++;
                if (condition(pnd_Old_Cntrct_Ppcn_A4.less("W028")))                                                                                                       //Natural: IF #OLD-CNTRCT-PPCN-A4 LT 'W028'
                {
                    pnd_Print_Sub_Totals_1.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                //*  JWO1
                pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
                //*    WHEN SUBSTRING(CNTRCT-PPCN-NBR,1,4) = 'W054'
            }                                                                                                                                                             //Natural: WHEN SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W054' OR = 'W053'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W054") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,
                4).equals("W053")))
            {
                decideConditionsMet1779++;
                pnd_Old_Cntrct_Ppcn_A4.setValue("W054");                                                                                                                  //Natural: MOVE 'W054' TO #OLD-CNTRCT-PPCN-A4
                //*      MOVE SUBSTRING(CNTRCT-PPCN-NBR,1,4) TO #OLD-CNTRCT-PPCN-A4
                pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR = 'W0560000' THRU 'W0560249'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0560000") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0560249")))
            {
                decideConditionsMet1779++;
                if (condition(pnd_Old_Cntrct_Ppcn_A4.less("W056")))                                                                                                       //Natural: IF #OLD-CNTRCT-PPCN-A4 LT 'W056'
                {
                    pnd_Print_Sub_Totals_1.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR = 'W0560250' THRU 'W0589999'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0560250") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0589999")))
            {
                decideConditionsMet1779++;
                if (condition(pnd_Old_Cntrct_Ppcn_A8.less("W0560250")))                                                                                                   //Natural: IF #OLD-CNTRCT-PPCN-A8 LT 'W0560250'
                {
                    pnd_Print_Sub_Totals_1.setValue(true);                                                                                                                //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR = 'W0620000' THRU 'W0620999'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0620000") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0620999")))
            {
                decideConditionsMet1779++;
                if (condition(!pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5).equals(pnd_Old_Cntrct_Ppcn_A5)))                                       //Natural: IF SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) NE #OLD-CNTRCT-PPCN-A5
                {
                    if (condition(pnd_Old_Cntrct_Ppcn_A5.notEquals(" ")))                                                                                                 //Natural: IF #OLD-CNTRCT-PPCN-A5 NE ' '
                    {
                        pnd_Print_Sub_Totals_1.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                    pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                    pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN CNTRCT-PPCN-NBR = 'W0621000' THRU 'W0629999'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().greaterOrEqual("W0621000") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().lessOrEqual("W0629999")))
            {
                decideConditionsMet1779++;
                if (condition(!pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5).equals(pnd_Old_Cntrct_Ppcn_A5)))                                       //Natural: IF SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) NE #OLD-CNTRCT-PPCN-A5
                {
                    if (condition(pnd_Old_Cntrct_Ppcn_A5.notEquals(" ")))                                                                                                 //Natural: IF #OLD-CNTRCT-PPCN-A5 NE ' '
                    {
                        pnd_Print_Sub_Totals_1.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                    pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                    pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W069'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W069")))
            {
                decideConditionsMet1779++;
                if (condition(!pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals(pnd_Old_Cntrct_Ppcn_A4)))                                       //Natural: IF SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) NE #OLD-CNTRCT-PPCN-A4
                {
                    if (condition(pnd_Old_Cntrct_Ppcn_A4.notEquals(" ")))                                                                                                 //Natural: IF #OLD-CNTRCT-PPCN-A4 NE ' '
                    {
                        pnd_Print_Sub_Totals_1.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                    pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                    pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) GT 'W070' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) LT 'W074'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).compareTo("W070") > 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).compareTo("W074") 
                < 0))
            {
                decideConditionsMet1779++;
                pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
            }                                                                                                                                                             //Natural: WHEN SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) GE 'W074' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) LE 'W075'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).compareTo("W074") >= 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).compareTo("W075") 
                <= 0))
            {
                decideConditionsMet1779++;
                pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
            }                                                                                                                                                             //Natural: WHEN SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) GT 'W067889' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) LT 'W068147'
            else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W067889") > 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W068147") 
                < 0))
            {
                decideConditionsMet1779++;
                pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                     //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                if (condition(!pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals(pnd_Old_Cntrct_Ppcn_A4)))                                       //Natural: IF SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) NE #OLD-CNTRCT-PPCN-A4
                {
                    if (condition(pnd_Old_Cntrct_Ppcn_A4.notEquals(" ")))                                                                                                 //Natural: IF #OLD-CNTRCT-PPCN-A4 NE ' '
                    {
                        pnd_Print_Sub_Totals_1.setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Old_Cntrct_Ppcn_A4.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) TO #OLD-CNTRCT-PPCN-A4
                    pnd_Old_Cntrct_Ppcn_A5.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,5));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,5 ) TO #OLD-CNTRCT-PPCN-A5
                    pnd_Old_Cntrct_Ppcn_A8.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8));                                                 //Natural: MOVE SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) TO #OLD-CNTRCT-PPCN-A8
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  JWO1
            if (condition((! (pnd_Print_Sub_Totals_1.getBoolean()) && (pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W054")                 //Natural: IF NOT #PRINT-SUB-TOTALS-1 AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W054' OR = 'W053'
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W053")))))
            {
                //*      SUBSTRING(CNTRCT-PPCN-NBR,1,4) = 'W054'
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PRINT-SUB-TOTALS-2
                sub_Determine_Print_Sub_Totals_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Print_Sub_Totals_1.getBoolean()) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).compareTo("W074")                //Natural: IF NOT #PRINT-SUB-TOTALS-1 AND ( SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) GE 'W074' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) LE 'W075' )
                >= 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).compareTo("W075") <= 0))
            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PRINT-SUB-TOTALS-2
                sub_Determine_Print_Sub_Totals_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Print_Sub_Totals_1.getBoolean()) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).compareTo("W063")                //Natural: IF NOT #PRINT-SUB-TOTALS-1 AND ( SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) GE 'W063' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) LE 'W068' )
                >= 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).compareTo("W068") <= 0))
            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PRINT-SUB-TOTALS-2
                sub_Determine_Print_Sub_Totals_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Print_Sub_Totals_1.getBoolean()) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W061")))                 //Natural: IF NOT #PRINT-SUB-TOTALS-1 AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) EQ 'W061'
            {
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PRINT-SUB-TOTALS-2
                sub_Determine_Print_Sub_Totals_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Print_Sub_Totals_1.getBoolean() || pnd_Print_Sub_Totals_2.getValue("*").getBoolean()))                                                      //Natural: IF #PRINT-SUB-TOTALS-1 OR #PRINT-SUB-TOTALS-2 ( * )
            {
                getReports().write(1, NEWLINE,"Sub Totals",new TabSetting(33),pnd_Ga_Sub_Totals_Pnd_Sub_Cntrct, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new                  //Natural: WRITE ( 01 ) / 'Sub Totals' 33T #SUB-CNTRCT ( EM = Z,ZZZ,ZZ9.99- ) 1X #SUB-FED ( EM = ZZZ,ZZ9.99- ) 1X #SUB-TAX ( EM = ZZZ,ZZ9.99- ) 1X #SUB-DED ( EM = ZZ,ZZ9.99- ) 1X #SUB-NET ( EM = Z,ZZZ,ZZ9.99- ) 1X 'Count:' #SUB-COUNT ( EM = ZZZ,ZZ9 )
                    ColumnSpacing(1),pnd_Ga_Sub_Totals_Pnd_Sub_Fed, new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Ga_Sub_Totals_Pnd_Sub_Tax, 
                    new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Ga_Sub_Totals_Pnd_Sub_Ded, new ReportEditMask ("ZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Ga_Sub_Totals_Pnd_Sub_Net, 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),"Count:",pnd_Ga_Sub_Totals_Pnd_Sub_Count, new ReportEditMask ("ZZZ,ZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  07/29/96
                pnd_Ga_Sub_Totals.reset();                                                                                                                                //Natural: RESET #GA-SUB-TOTALS #PRINT-SUB-TOTALS-1 #PRINT-SUB-TOTALS-2 ( * )
                pnd_Print_Sub_Totals_1.reset();
                pnd_Print_Sub_Totals_2.getValue("*").reset();
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-STATE-CDE
            sub_Get_State_Cde();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM RE-ARRANGE-NAME
            sub_Re_Arrange_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                               /* FE201411 START
            //*  COMPRESS     PYMNT-ADDR-LINE1-TXT (2) '-' PYMNT-ADDR-LINE2-TXT (2)
            //*    '-' PYMNT-ADDR-LINE3-TXT (2) '-' PYMNT-ADDR-LINE4-TXT (2)
            //*    '-' PYMNT-ADDR-LINE5-TXT (2) '-' PYMNT-ADDR-LINE6-TXT (2)
            //*    INTO #ONE-LINE-ADDR  /*  LEAVING NO
            if (condition(getReports().getAstLineCount(1).greaterOrEqual(56)))                                                                                            //Natural: IF *LINE-COUNT ( 01 ) GE 56
            {
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  FE201411 END
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                               //Natural: DISPLAY ( 01 ) ( UC = - ) 'Contract/Number' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR 'Payee/Code' WF-PYMNT-ADDR-GRP.CNTRCT-PAYEE-CDE 'SSN' ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 'Geo/Cde' STT-ABBR-STATE-CDE ( 1 ) 'Contract/Amount' #CONTRACT-AMT ( EM = ZZZ,ZZ9.99- ) 'Federal/Tax' #FED-AMT ( EM = ZZZ,ZZ9.99- ) 'State/Local' #TAX-AMT ( EM = ZZZ,ZZ9.99- ) ' /Deductns' #DED-AMT ( EM = ZZ,ZZ9.99- ) ' /Net Payment' #NET-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Pay/Type' #PYMNT-TYPE 'Payee/Name & Address' #NAME-FULL ( AL = 36 )
            		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Payee/Code",
            		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde(),"SSN",
            		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"Geo/Cde",
            		pdaNeca4000.getNeca4000_Stt_Abbr_State_Cde().getValue(1),"Contract/Amount",
            		pnd_Dtl_Amts_Pnd_Contract_Amt, new ReportEditMask ("ZZZ,ZZ9.99-"),"Federal/Tax",
            		pnd_Dtl_Amts_Pnd_Fed_Amt, new ReportEditMask ("ZZZ,ZZ9.99-"),"State/Local",
            		pnd_Dtl_Amts_Pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
            		pnd_Dtl_Amts_Pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
            		pnd_Dtl_Amts_Pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Pay/Type",
            		pnd_Pymnt_Nbr_Pnd_Pymnt_Type,"Payee/Name & Address",
            		pnd_Name_Full, new AlphanumericLength (36));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  WRITE (01)                  #ONE-LINE-ADDR /* FE201411 START
            pnd_One_Line_Addr.reset();                                                                                                                                    //Natural: RESET #ONE-LINE-ADDR
            //*          #TWO-LINE-ADDR
            //*  #2ND-ADDR := FALSE
            setValueToSubstring(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(2),pnd_One_Line_Addr,1,35);                                               //Natural: MOVE PYMNT-ADDR-LINE1-TXT ( 2 ) TO SUBSTR ( #ONE-LINE-ADDR,001,35 )
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(2).notEquals(" ")))                                                             //Natural: IF PYMNT-ADDR-LINE2-TXT ( 2 ) NE ' '
            {
                setValueToSubstring(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(2),pnd_One_Line_Addr,36,35);                                          //Natural: MOVE PYMNT-ADDR-LINE2-TXT ( 2 ) TO SUBSTR ( #ONE-LINE-ADDR,036,35 )
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(2).notEquals(" ")))                                                         //Natural: IF PYMNT-ADDR-LINE3-TXT ( 2 ) NE ' '
                {
                    setValueToSubstring(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(2),pnd_One_Line_Addr,71,35);                                      //Natural: MOVE PYMNT-ADDR-LINE3-TXT ( 2 ) TO SUBSTR ( #ONE-LINE-ADDR,071,35 )
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(2).notEquals(" ")))                                                     //Natural: IF PYMNT-ADDR-LINE4-TXT ( 2 ) NE ' '
                    {
                        //*          #2ND-ADDR := TRUE
                        setValueToSubstring(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(2),pnd_One_Line_Addr,106,35);                                 //Natural: MOVE PYMNT-ADDR-LINE4-TXT ( 2 ) TO SUBSTR ( #ONE-LINE-ADDR,106,35 )
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(2).notEquals(" ")))                                                 //Natural: IF PYMNT-ADDR-LINE5-TXT ( 2 ) NE ' '
                        {
                            setValueToSubstring(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(2),pnd_One_Line_Addr,141,35);                             //Natural: MOVE PYMNT-ADDR-LINE5-TXT ( 2 ) TO SUBSTR ( #ONE-LINE-ADDR,141,35 )
                            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(2).notEquals(" ")))                                             //Natural: IF PYMNT-ADDR-LINE6-TXT ( 2 ) NE ' '
                            {
                                setValueToSubstring(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(2),pnd_One_Line_Addr,176,35);                         //Natural: MOVE PYMNT-ADDR-LINE6-TXT ( 2 ) TO SUBSTR ( #ONE-LINE-ADDR,176,35 )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(1, pnd_One_Line_Addr);                                                                                                                     //Natural: WRITE ( 01 ) #ONE-LINE-ADDR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  IF #2ND-ADDR
            //*    WRITE (01)                #TWO-LINE-ADDR
            //*  END-IF                                     /* FE201411 END
            pnd_Ga_Sub_Totals_Pnd_Sub_Cntrct.nadd(pnd_Dtl_Amts_Pnd_Contract_Amt);                                                                                         //Natural: ADD #CONTRACT-AMT TO #SUB-CNTRCT
            pnd_Ga_Sub_Totals_Pnd_Sub_Dvdnd.nadd(pnd_Dtl_Amts_Pnd_Dvdnd_Amt);                                                                                             //Natural: ADD #DVDND-AMT TO #SUB-DVDND
            pnd_Ga_Sub_Totals_Pnd_Sub_Fed.nadd(pnd_Dtl_Amts_Pnd_Fed_Amt);                                                                                                 //Natural: ADD #FED-AMT TO #SUB-FED
            pnd_Ga_Sub_Totals_Pnd_Sub_Tax.nadd(pnd_Dtl_Amts_Pnd_Tax_Amt);                                                                                                 //Natural: ADD #TAX-AMT TO #SUB-TAX
            pnd_Ga_Sub_Totals_Pnd_Sub_Ded.nadd(pnd_Dtl_Amts_Pnd_Ded_Amt);                                                                                                 //Natural: ADD #DED-AMT TO #SUB-DED
            pnd_Ga_Sub_Totals_Pnd_Sub_Net.nadd(pnd_Dtl_Amts_Pnd_Net_Amt);                                                                                                 //Natural: ADD #NET-AMT TO #SUB-NET
            pnd_Ga_Sub_Totals_Pnd_Sub_Count.nadd(1);                                                                                                                      //Natural: ADD 1 TO #SUB-COUNT
            pnd_Tot_Cntrct.nadd(pnd_Dtl_Amts_Pnd_Contract_Amt);                                                                                                           //Natural: ADD #CONTRACT-AMT TO #TOT-CNTRCT
            pnd_Tot_Dvdnd.nadd(pnd_Dtl_Amts_Pnd_Dvdnd_Amt);                                                                                                               //Natural: ADD #DVDND-AMT TO #TOT-DVDND
            pnd_Tot_Fed.nadd(pnd_Dtl_Amts_Pnd_Fed_Amt);                                                                                                                   //Natural: ADD #FED-AMT TO #TOT-FED
            pnd_Tot_Tax.nadd(pnd_Dtl_Amts_Pnd_Tax_Amt);                                                                                                                   //Natural: ADD #TAX-AMT TO #TOT-TAX
            pnd_Tot_Ded.nadd(pnd_Dtl_Amts_Pnd_Ded_Amt);                                                                                                                   //Natural: ADD #DED-AMT TO #TOT-DED
            pnd_Tot_Net.nadd(pnd_Dtl_Amts_Pnd_Net_Amt);                                                                                                                   //Natural: ADD #NET-AMT TO #TOT-NET
            pnd_Tot_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #TOT-COUNT
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,"Sub Totals",new TabSetting(33),pnd_Ga_Sub_Totals_Pnd_Sub_Cntrct, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Ga_Sub_Totals_Pnd_Sub_Fed,  //Natural: WRITE ( 01 ) / 'Sub Totals' 33T #SUB-CNTRCT ( EM = Z,ZZZ,ZZ9.99- ) 1X #SUB-FED ( EM = ZZZ,ZZ9.99- ) 1X #SUB-TAX ( EM = ZZZ,ZZ9.99- ) 1X #SUB-DED ( EM = ZZ,ZZ9.99- ) 1X #SUB-NET ( EM = Z,ZZZ,ZZ9.99- ) 1X 'Count:' #SUB-COUNT ( EM = ZZZ,ZZ9 ) //// 'Grand Totals' 33T #TOT-CNTRCT ( EM = Z,ZZZ,ZZ9.99- ) 1X #TOT-FED ( EM = ZZZ,ZZ9.99- ) 1X #TOT-TAX ( EM = ZZZ,ZZ9.99- ) 1X #TOT-DED ( EM = ZZZ,ZZ9.99- ) 1X #TOT-NET ( EM = Z,ZZZ,ZZ9.99- ) 1X 'Count:' #TOT-COUNT ( EM = ZZZ,ZZ9 )
            new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Ga_Sub_Totals_Pnd_Sub_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Ga_Sub_Totals_Pnd_Sub_Ded, 
            new ReportEditMask ("ZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Ga_Sub_Totals_Pnd_Sub_Net, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),"Count:",pnd_Ga_Sub_Totals_Pnd_Sub_Count, 
            new ReportEditMask ("ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,"Grand Totals",new TabSetting(33),pnd_Tot_Cntrct, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new 
            ColumnSpacing(1),pnd_Tot_Fed, new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Tot_Tax, new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Tot_Ded, 
            new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),pnd_Tot_Net, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),"Count:",pnd_Tot_Count, 
            new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        pnd_Tiaa_Amount.compute(new ComputeParameters(false, pnd_Tiaa_Amount), pnd_T_Amount.add(pnd_I_Amount).add(pnd_Pa_Amount).add(pnd_R_Amount));                      //Natural: ASSIGN #TIAA-AMOUNT := #T-AMOUNT + #I-AMOUNT + #PA-AMOUNT + #R-AMOUNT
        pnd_Cref_Amount.compute(new ComputeParameters(false, pnd_Cref_Amount), pnd_C_Amount.add(pnd_B_Amount).add(pnd_M_Amount).add(pnd_S_Amount).add(pnd_W_Amount).add(pnd_L_Amount).add(pnd_E_Amount)); //Natural: ASSIGN #CREF-AMOUNT := #C-AMOUNT + #B-AMOUNT + #M-AMOUNT + #S-AMOUNT + #W-AMOUNT + #L-AMOUNT + #E-AMOUNT
        pnd_Pa_Select_Total.compute(new ComputeParameters(false, pnd_Pa_Select_Total), pnd_Pa_Select_Fixed.add(pnd_Pa_Select_Stock_Index).add(pnd_Pa_Select_Growth_Equity).add(pnd_Pa_Select_Growth_Income).add(pnd_Pa_Select_Intrntl_Equity).add(pnd_Pa_Select_Social_Ch_Equity).add(pnd_Pa_Select_Lcv).add(pnd_Pa_Select_Scv).add(pnd_Pa_Select_Real)); //Natural: ASSIGN #PA-SELECT-TOTAL := #PA-SELECT-FIXED + #PA-SELECT-STOCK-INDEX + #PA-SELECT-GROWTH-EQUITY + #PA-SELECT-GROWTH-INCOME + #PA-SELECT-INTRNTL-EQUITY + #PA-SELECT-SOCIAL-CH-EQUITY + #PA-SELECT-LCV + #PA-SELECT-SCV + #PA-SELECT-REAL
        pnd_Spia_Select_Total.compute(new ComputeParameters(false, pnd_Spia_Select_Total), pnd_Spia_Select_Fixed.add(pnd_Spia_Select_Free_Look).add(pnd_Spia_Select_Stock_Index).add(pnd_Spia_Select_Growth_Equity).add(pnd_Spia_Select_Growth_Income).add(pnd_Spia_Select_Intrntl_Equity).add(pnd_Spia_Select_Social_Ch_Equity).add(pnd_Spia_Lcv).add(pnd_Spia_Scv).add(pnd_Spia_Real)); //Natural: ASSIGN #SPIA-SELECT-TOTAL := #SPIA-SELECT-FIXED + #SPIA-SELECT-FREE-LOOK + #SPIA-SELECT-STOCK-INDEX + #SPIA-SELECT-GROWTH-EQUITY + #SPIA-SELECT-GROWTH-INCOME + #SPIA-SELECT-INTRNTL-EQUITY + #SPIA-SELECT-SOCIAL-CH-EQUITY + #SPIA-LCV + #SPIA-SCV + #SPIA-REAL
        pnd_Grand_Amount.compute(new ComputeParameters(false, pnd_Grand_Amount), pnd_Tiaa_Amount.add(pnd_Cref_Amount).add(pnd_Pa_Select_Total).add(pnd_Spia_Select_Total)); //Natural: ASSIGN #GRAND-AMOUNT := #TIAA-AMOUNT + #CREF-AMOUNT + #PA-SELECT-TOTAL + #SPIA-SELECT-TOTAL
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),"  TIAA                 ",pnd_T_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new  //Natural: WRITE ( 01 ) //// / 48T '  TIAA                 ' #T-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  TIAA Insurance       ' #I-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  TIAA P.A.            ' #PA-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Real Estate          ' #R-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T 'TIAA Total             ' #TIAA-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) // 48T '  Stock                ' #C-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Bond                 ' #B-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Money Market         ' #M-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Social Choice        ' #S-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Global               ' #W-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Growth               ' #L-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Equity Index         ' #E-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T 'CREF Total             ' #CREF-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T 'PA Select              ' / 48T '  Fixed Fund           ' #PA-SELECT-FIXED ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Stock Index Account  ' #PA-SELECT-STOCK-INDEX ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Growth Equity        ' #PA-SELECT-GROWTH-EQUITY ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Growth & Income      ' #PA-SELECT-GROWTH-INCOME ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  International Equity ' #PA-SELECT-INTRNTL-EQUITY ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Social Choice Equity ' #PA-SELECT-SOCIAL-CH-EQUITY ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Large Cap Value      ' #PA-SELECT-LCV ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Small Cap Value      ' #PA-SELECT-SCV ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Real Estate Security ' #PA-SELECT-REAL ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T 'PA Select Total        ' #PA-SELECT-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T 'SPIA                   ' / 48T '  Fixed Fund           ' #SPIA-SELECT-FIXED ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  General Leger Account' #SPIA-SELECT-FREE-LOOK ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Stock Index Account  ' #SPIA-SELECT-STOCK-INDEX ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Growth Equity        ' #SPIA-SELECT-GROWTH-EQUITY ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Growth & Income      ' #SPIA-SELECT-GROWTH-INCOME ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  International Equity ' #SPIA-SELECT-INTRNTL-EQUITY ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Social Choice Equity ' #SPIA-SELECT-SOCIAL-CH-EQUITY ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Large Cap Value      ' #SPIA-LCV ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Small Cap Value      ' #SPIA-SCV ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T '  Real Estate Security ' #SPIA-REAL ( EM = ZZZ,ZZZ,ZZ9.99- ) / 48T 'SPIA Total             ' #SPIA-SELECT-TOTAL ( EM = ZZZ,ZZZ,ZZ9.99- ) // 48T 'Grand Total       ' #GRAND-AMOUNT ( EM = ZZZ,ZZZ,ZZ9.99- )
            TabSetting(48),"  TIAA Insurance       ",pnd_I_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  TIAA P.A.            ",pnd_Pa_Amount, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Real Estate          ",pnd_R_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"TIAA Total             ",pnd_Tiaa_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,new TabSetting(48),"  Stock                ",pnd_C_Amount, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Bond                 ",pnd_B_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"  Money Market         ",pnd_M_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Social Choice        ",pnd_S_Amount, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Global               ",pnd_W_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"  Growth               ",pnd_L_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Equity Index         ",pnd_E_Amount, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"CREF Total             ",pnd_Cref_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"PA Select              ",NEWLINE,new TabSetting(48),"  Fixed Fund           ",pnd_Pa_Select_Fixed, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"  Stock Index Account  ",pnd_Pa_Select_Stock_Index, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Growth Equity        ",pnd_Pa_Select_Growth_Equity, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Growth & Income      ",pnd_Pa_Select_Growth_Income, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  International Equity ",pnd_Pa_Select_Intrntl_Equity, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"  Social Choice Equity ",pnd_Pa_Select_Social_Ch_Equity, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Large Cap Value      ",pnd_Pa_Select_Lcv, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Small Cap Value      ",pnd_Pa_Select_Scv, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"  Real Estate Security ",pnd_Pa_Select_Real, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"PA Select Total        ",pnd_Pa_Select_Total, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"SPIA                   ",NEWLINE,new TabSetting(48),"  Fixed Fund           ",pnd_Spia_Select_Fixed, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  General Leger Account",pnd_Spia_Select_Free_Look, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"  Stock Index Account  ",pnd_Spia_Select_Stock_Index, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Growth Equity        ",pnd_Spia_Select_Growth_Equity, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Growth & Income      ",pnd_Spia_Select_Growth_Income, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  International Equity ",pnd_Spia_Select_Intrntl_Equity, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"  Social Choice Equity ",pnd_Spia_Select_Social_Ch_Equity, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Large Cap Value      ",pnd_Spia_Lcv, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"  Small Cap Value      ",pnd_Spia_Scv, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new 
            TabSetting(48),"  Real Estate Security ",pnd_Spia_Real, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,new TabSetting(48),"SPIA Total             ",pnd_Spia_Select_Total, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),NEWLINE,NEWLINE,new TabSetting(48),"Grand Total       ",pnd_Grand_Amount, new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        //*                    PA-SELECT - NEW FUNDS
        //*                       SPIA - NEW FUNDS
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PRINT-SUB-TOTALS-2
        //*  WHEN #FIRST-ONE-IN-RANGE(8) AND
        //*      SUBSTRING(CNTRCT-PPCN-NBR,1,8) GE 'W0540000' AND
        //*      SUBSTRING(CNTRCT-PPCN-NBR,1,8) GE 'W0530000' AND
        //*      SUBSTRING(CNTRCT-PPCN-NBR,1,8) LE 'W0540999'
        //*    MOVE TRUE TO #PRINT-SUB-TOTALS-2(8)
        //*    RESET #FIRST-ONE-IN-RANGE(8)
        //*      SUBSTRING(CNTRCT-PPCN-NBR,1,8) GE 'W0541000' AND
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-STATE-CDE
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RE-ARRANGE-NAME
        //* ****************
        //*                             2ND NAME
        //*                             3RD NAME
        //*                             4TH NAME
    }
    private void sub_Determine_Print_Sub_Totals_2() throws Exception                                                                                                      //Natural: DETERMINE-PRINT-SUB-TOTALS-2
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        short decideConditionsMet2010 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FIRST-ONE-IN-RANGE ( 1 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W063' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) GE 'W063041'
        if (condition(pnd_First_One_In_Range.getValue(1).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W063") 
            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W063041") >= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(1).setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 1 )
            pnd_First_One_In_Range.getValue(1).reset();                                                                                                                   //Natural: RESET #FIRST-ONE-IN-RANGE ( 1 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 2 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W064' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) GE 'W064110'
        else if (condition(pnd_First_One_In_Range.getValue(2).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W064") 
            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W064110") >= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(2).setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 2 )
            pnd_First_One_In_Range.getValue(2).reset();                                                                                                                   //Natural: RESET #FIRST-ONE-IN-RANGE ( 2 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 3 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W065' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) GE 'W065025'
        else if (condition(pnd_First_One_In_Range.getValue(3).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W065") 
            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W065025") >= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(3).setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 3 )
            pnd_First_One_In_Range.getValue(3).reset();                                                                                                                   //Natural: RESET #FIRST-ONE-IN-RANGE ( 3 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 4 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W066' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) GE 'W066073'
        else if (condition(pnd_First_One_In_Range.getValue(4).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W066") 
            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W066073") >= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(4).setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 4 )
            pnd_First_One_In_Range.getValue(4).reset();                                                                                                                   //Natural: RESET #FIRST-ONE-IN-RANGE ( 4 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 5 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W067' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) GE 'W067062'
        else if (condition(pnd_First_One_In_Range.getValue(5).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W067") 
            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W067062") >= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(5).setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 5 )
            pnd_First_One_In_Range.getValue(5).reset();                                                                                                                   //Natural: RESET #FIRST-ONE-IN-RANGE ( 5 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 6 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) GE 'W067990' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) LT 'W068147'
        else if (condition(pnd_First_One_In_Range.getValue(6).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W067990") 
            >= 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W068147") < 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(6).setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 6 )
            pnd_First_One_In_Range.getValue(6).reset();                                                                                                                   //Natural: RESET #FIRST-ONE-IN-RANGE ( 6 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 7 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,4 ) = 'W068' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,7 ) GE 'W068147'
        else if (condition(pnd_First_One_In_Range.getValue(7).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,4).equals("W068") 
            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,7).compareTo("W068147") >= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(7).setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 7 )
            //*  JWO1
            pnd_First_One_In_Range.getValue(7).reset();                                                                                                                   //Natural: RESET #FIRST-ONE-IN-RANGE ( 7 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 9 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) GE 'W0530000' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) LE 'W0549999'
        else if (condition(pnd_First_One_In_Range.getValue(9).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8).compareTo("W0530000") 
            >= 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8).compareTo("W0549999") <= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(9).setValue(true);                                                                                                            //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 9 )
            pnd_First_One_In_Range.getValue(9).reset();                                                                                                                   //Natural: RESET #FIRST-ONE-IN-RANGE ( 9 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 10 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) GE 'W0740000' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) LE 'W0740999'
        else if (condition(pnd_First_One_In_Range.getValue(10).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8).compareTo("W0740000") 
            >= 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8).compareTo("W0740999") <= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(10).setValue(true);                                                                                                           //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 10 )
            pnd_First_One_In_Range.getValue(10).reset();                                                                                                                  //Natural: RESET #FIRST-ONE-IN-RANGE ( 10 )
        }                                                                                                                                                                 //Natural: WHEN #FIRST-ONE-IN-RANGE ( 11 ) AND SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) GE 'W0741000' AND SUBSTRING ( CNTRCT-PPCN-NBR,1,8 ) LE 'W0759999'
        else if (condition(pnd_First_One_In_Range.getValue(11).equals(true) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8).compareTo("W0741000") 
            >= 0 && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().getSubstring(1,8).compareTo("W0759999") <= 0))
        {
            decideConditionsMet2010++;
            pnd_Print_Sub_Totals_2.getValue(11).setValue(true);                                                                                                           //Natural: MOVE TRUE TO #PRINT-SUB-TOTALS-2 ( 11 )
            pnd_First_One_In_Range.getValue(11).reset();                                                                                                                  //Natural: RESET #FIRST-ONE-IN-RANGE ( 11 )
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_State_Cde() throws Exception                                                                                                                     //Natural: GET-STATE-CDE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        pnd_State_Cde_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde()));                                //Natural: COMPRESS '0' ANNT-RSDNCY-CDE INTO #STATE-CDE-A LEAVING NO
        pdaNeca4000.getNeca4000().reset();                                                                                                                                //Natural: RESET NECA4000
        pdaNeca4000.getNeca4000_Request_Ind().setValue(" ");                                                                                                              //Natural: ASSIGN NECA4000.REQUEST-IND := ' '
        pdaNeca4000.getNeca4000_Function_Cde().setValue("STT");                                                                                                           //Natural: ASSIGN NECA4000.FUNCTION-CDE := 'STT'
        pdaNeca4000.getNeca4000_Inpt_Key_Option_Cde().setValue("01");                                                                                                     //Natural: ASSIGN NECA4000.INPT-KEY-OPTION-CDE := '01'
        pdaNeca4000.getNeca4000_Stt_Key_Country_Cde().setValue("US");                                                                                                     //Natural: ASSIGN NECA4000.STT-KEY-COUNTRY-CDE := 'US'
        pdaNeca4000.getNeca4000_Stt_Key_State_Cde().setValue(pnd_State_Cde_A);                                                                                            //Natural: ASSIGN NECA4000.STT-KEY-STATE-CDE := #STATE-CDE-A
        DbsUtil.callnat(Necn4000.class , getCurrentProcessState(), pdaNeca4000.getNeca4000());                                                                            //Natural: CALLNAT 'NECN4000' NECA4000
        if (condition(Global.isEscape())) return;
    }
    private void sub_Re_Arrange_Name() throws Exception                                                                                                                   //Natural: RE-ARRANGE-NAME
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Name_Pnd_Name1.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1));                                                                             //Natural: ASSIGN #NAME1 := PYMNT-NME ( 1 )
        DbsUtil.examine(new ExamineSource(pnd_Name_Pnd_Name1), new ExamineSearch("JR"), new ExamineReplace("  ", true));                                                  //Natural: EXAMINE #NAME1 FOR 'JR' WITH DELIMITER ' ' REPLACE FIRST WITH '  '
        DbsUtil.examine(new ExamineSource(pnd_Name_Pnd_Name1), new ExamineSearch("SR"), new ExamineReplace("  ", true));                                                  //Natural: EXAMINE #NAME1 FOR 'SR' WITH DELIMITER ' ' REPLACE FIRST WITH '  '
        DbsUtil.examine(new ExamineSource(pnd_Name_Pnd_Name1), new ExamineSearch("CR"), new ExamineReplace("  ", true), new ExamineGivingPosition(pnd_Name_Pnd_X));       //Natural: EXAMINE #NAME1 FOR 'CR' WITH DELIMITER ' ' REPLACE FIRST WITH '  ' GIVING POSITION #X
        pnd_Name_Pnd_Name1.setValue(pnd_Name_Pnd_Name1, MoveOption.LeftJustified);                                                                                        //Natural: MOVE LEFT #NAME1 TO #NAME1
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 30
        for (pnd_Name_Pnd_I.setValue(1); condition(pnd_Name_Pnd_I.lessOrEqual(30)); pnd_Name_Pnd_I.nadd(1))
        {
            if (condition(pnd_Name_Pnd_Name_Arr.getValue(pnd_Name_Pnd_I).equals(" ")))                                                                                    //Natural: IF #NAME-ARR ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Name_Pnd_Name_First.setValue(pnd_Name_Pnd_Name1.getSubstring(1,pnd_Name_Pnd_I.getInt()));                                                                     //Natural: MOVE SUBSTR ( #NAME1,1,#I ) TO #NAME-FIRST
        FOR03:                                                                                                                                                            //Natural: FOR #N 1 #I
        for (pnd_Name_Pnd_N.setValue(1); condition(pnd_Name_Pnd_N.lessOrEqual(pnd_Name_Pnd_I)); pnd_Name_Pnd_N.nadd(1))
        {
            setValueToSubstring("=",pnd_Name_Pnd_Name1,pnd_Name_Pnd_N.getInt(),1);                                                                                        //Natural: MOVE '=' TO SUBSTR ( #NAME1,#N,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR04:                                                                                                                                                            //Natural: FOR #I 1 38
        for (pnd_Name_Pnd_I.setValue(1); condition(pnd_Name_Pnd_I.lessOrEqual(38)); pnd_Name_Pnd_I.nadd(1))
        {
            if (condition(pnd_Name_Pnd_Name_Arr.getValue(pnd_Name_Pnd_I).equals("=")))                                                                                    //Natural: IF #NAME-ARR ( #I ) = '='
            {
                pnd_Name_Pnd_N.setValue(pnd_Name_Pnd_I);                                                                                                                  //Natural: ASSIGN #N := #I
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Name_Pnd_Name_Arr.getValue(pnd_Name_Pnd_I).equals(" ")))                                                                                    //Natural: IF #NAME-ARR ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Name_Pnd_N.nadd(1);                                                                                                                                           //Natural: ASSIGN #N := #N + 1
        pnd_Name_Pnd_I.nsubtract(pnd_Name_Pnd_N);                                                                                                                         //Natural: ASSIGN #I := #I - #N
        pnd_Name_Pnd_Name_2nd.setValue(pnd_Name_Pnd_Name1.getSubstring(pnd_Name_Pnd_N.getInt(),pnd_Name_Pnd_I.getInt()));                                                 //Natural: MOVE SUBSTR ( #NAME1,#N,#I ) TO #NAME-2ND
        pnd_Name_Pnd_N.compute(new ComputeParameters(false, pnd_Name_Pnd_N), pnd_Name_Pnd_I.add(pnd_Name_Pnd_N));                                                         //Natural: ASSIGN #N := #I + #N
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 #N
        for (pnd_Name_Pnd_I.setValue(1); condition(pnd_Name_Pnd_I.lessOrEqual(pnd_Name_Pnd_N)); pnd_Name_Pnd_I.nadd(1))
        {
            setValueToSubstring("=",pnd_Name_Pnd_Name1,pnd_Name_Pnd_I.getInt(),1);                                                                                        //Natural: MOVE '=' TO SUBSTR ( #NAME1,#I,1 )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #I 1 38
        for (pnd_Name_Pnd_I.setValue(1); condition(pnd_Name_Pnd_I.lessOrEqual(38)); pnd_Name_Pnd_I.nadd(1))
        {
            if (condition(pnd_Name_Pnd_Name_Arr.getValue(pnd_Name_Pnd_I).equals("=")))                                                                                    //Natural: IF #NAME-ARR ( #I ) = '='
            {
                pnd_Name_Pnd_N.setValue(pnd_Name_Pnd_I);                                                                                                                  //Natural: ASSIGN #N := #I
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Name_Pnd_Name_Arr.getValue(pnd_Name_Pnd_I).equals(" ")))                                                                                    //Natural: IF #NAME-ARR ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Name_Pnd_N.nadd(1);                                                                                                                                           //Natural: ASSIGN #N := #N + 1
        pnd_Name_Pnd_I.nsubtract(pnd_Name_Pnd_N);                                                                                                                         //Natural: ASSIGN #I := #I - #N
        if (condition(pnd_Name_Pnd_I.notEquals(getZero())))                                                                                                               //Natural: IF #I NE 0
        {
            pnd_Name_Pnd_Name_3rd.setValue(pnd_Name_Pnd_Name1.getSubstring(pnd_Name_Pnd_N.getInt(),pnd_Name_Pnd_I.getInt()));                                             //Natural: MOVE SUBSTR ( #NAME1,#N,#I ) TO #NAME-3RD
            pnd_Name_Pnd_N.compute(new ComputeParameters(false, pnd_Name_Pnd_N), pnd_Name_Pnd_I.add(pnd_Name_Pnd_N));                                                     //Natural: ASSIGN #N := #I + #N
            FOR07:                                                                                                                                                        //Natural: FOR #I 1 #N
            for (pnd_Name_Pnd_I.setValue(1); condition(pnd_Name_Pnd_I.lessOrEqual(pnd_Name_Pnd_N)); pnd_Name_Pnd_I.nadd(1))
            {
                setValueToSubstring("=",pnd_Name_Pnd_Name1,pnd_Name_Pnd_I.getInt(),1);                                                                                    //Natural: MOVE '=' TO SUBSTR ( #NAME1,#I,1 )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR08:                                                                                                                                                            //Natural: FOR #I 1 38
        for (pnd_Name_Pnd_I.setValue(1); condition(pnd_Name_Pnd_I.lessOrEqual(38)); pnd_Name_Pnd_I.nadd(1))
        {
            if (condition(pnd_Name_Pnd_Name_Arr.getValue(pnd_Name_Pnd_I).equals("=")))                                                                                    //Natural: IF #NAME-ARR ( #I ) = '='
            {
                pnd_Name_Pnd_N.setValue(pnd_Name_Pnd_I);                                                                                                                  //Natural: ASSIGN #N := #I
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Name_Pnd_Name_Arr.getValue(pnd_Name_Pnd_I).equals(" ")))                                                                                    //Natural: IF #NAME-ARR ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Name_Pnd_N.nadd(1);                                                                                                                                           //Natural: ASSIGN #N := #N + 1
        pnd_Name_Pnd_I.nsubtract(pnd_Name_Pnd_N);                                                                                                                         //Natural: ASSIGN #I := #I - #N
        if (condition(pnd_Name_Pnd_I.notEquals(getZero())))                                                                                                               //Natural: IF #I NE 0
        {
            pnd_Name_Pnd_Name_Last.setValue(pnd_Name_Pnd_Name1.getSubstring(pnd_Name_Pnd_N.getInt(),pnd_Name_Pnd_I.getInt()));                                            //Natural: MOVE SUBSTR ( #NAME1,#N,#I ) TO #NAME-LAST
            pnd_Name_Pnd_N.compute(new ComputeParameters(false, pnd_Name_Pnd_N), pnd_Name_Pnd_I.add(pnd_Name_Pnd_N));                                                     //Natural: ASSIGN #N := #I + #N
            FOR09:                                                                                                                                                        //Natural: FOR #I 1 #N
            for (pnd_Name_Pnd_I.setValue(1); condition(pnd_Name_Pnd_I.lessOrEqual(pnd_Name_Pnd_N)); pnd_Name_Pnd_I.nadd(1))
            {
                setValueToSubstring("=",pnd_Name_Pnd_Name1,pnd_Name_Pnd_I.getInt(),1);                                                                                    //Natural: MOVE '=' TO SUBSTR ( #NAME1,#I,1 )
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2125 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #NAME-LAST = ' ' AND #NAME-3RD GT ' '
        if (condition(pnd_Name_Pnd_Name_Last.equals(" ") && pnd_Name_Pnd_Name_3rd.greater(" ")))
        {
            decideConditionsMet2125++;
            pnd_Name_Pnd_Name_Last.setValue(pnd_Name_Pnd_Name_3rd);                                                                                                       //Natural: ASSIGN #NAME-LAST := #NAME-3RD
            pnd_Name_Pnd_Name_3rd.setValue(" ");                                                                                                                          //Natural: ASSIGN #NAME-3RD := ' '
        }                                                                                                                                                                 //Natural: WHEN #NAME-LAST = ' ' AND #NAME-3RD = ' '
        else if (condition(pnd_Name_Pnd_Name_Last.equals(" ") && pnd_Name_Pnd_Name_3rd.equals(" ")))
        {
            decideConditionsMet2125++;
            pnd_Name_Pnd_Name_Last.setValue(pnd_Name_Pnd_Name_2nd);                                                                                                       //Natural: ASSIGN #NAME-LAST := #NAME-2ND
            pnd_Name_Pnd_Name_2nd.setValue(" ");                                                                                                                          //Natural: ASSIGN #NAME-2ND := ' '
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*                                   FE201411 START
        //*  COMPRESS #NAME-LAST '*' #NAME-FIRST '*'
        //*   #NAME-3RD  '*' #NAME-2ND   '*'   INTO #NAME-FULL  LEAVING NO
        //*  FE201411
        pnd_Name_Full.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Name_Pnd_Name_First, "*", pnd_Name_Pnd_Name_2nd, "*", pnd_Name_Pnd_Name_3rd,           //Natural: COMPRESS #NAME-FIRST '*' #NAME-2ND '*' #NAME-3RD '*' #NAME-LAST '*' INTO #NAME-FULL LEAVING NO
            "*", pnd_Name_Pnd_Name_Last, "*"));
        DbsUtil.examine(new ExamineSource(pnd_Name_Full), new ExamineSearch("*"), new ExamineReplace(" "));                                                               //Natural: EXAMINE #NAME-FULL FOR '*' REPLACE WITH ' '
        pnd_Name_Tab.setValue(pnd_Name_Full);                                                                                                                             //Natural: ASSIGN #NAME-TAB := #NAME-FULL
        pnd_N_Out.reset();                                                                                                                                                //Natural: RESET #N-OUT #J
        pnd_J.reset();
        FOR10:                                                                                                                                                            //Natural: FOR #I2 1 38
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(38)); pnd_I2.nadd(1))
        {
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
            if (condition(pnd_I2.less(38)))                                                                                                                               //Natural: IF #I2 LT 38
            {
                if (condition(pnd_Name_Tab_Pnd_N_A.getValue(pnd_I2).equals(" ") && pnd_Name_Tab_Pnd_N_A.getValue(pnd_I2.getDec().add(1)).equals(" ")))                    //Natural: IF #N-A ( #I2 ) = ' ' AND #N-A ( #I2+1 ) = ' '
                {
                    pnd_I2.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #I2
                    if (condition(pnd_I2.less(38)))                                                                                                                       //Natural: IF #I2 LT 38
                    {
                        if (condition(pnd_Name_Tab_Pnd_N_A.getValue(pnd_I2).equals(" ") && pnd_Name_Tab_Pnd_N_A.getValue(pnd_I2.getDec().add(1)).equals(" ")))            //Natural: IF #N-A ( #I2 ) = ' ' AND #N-A ( #I2+1 ) = ' '
                        {
                            pnd_I2.nadd(1);                                                                                                                               //Natural: ADD 1 TO #I2
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                setValueToSubstring(pnd_Name_Tab_Pnd_N_A.getValue(pnd_I2),pnd_N_Out,pnd_J.getInt(),1);                                                                    //Natural: MOVE #N-A ( #I2 ) TO SUBSTR ( #N-OUT,#J,1 )
            }                                                                                                                                                             //Natural: END-IF
            setValueToSubstring(pnd_Name_Tab_Pnd_N_A.getValue(pnd_I2),pnd_N_Out,pnd_J.getInt(),1);                                                                        //Natural: MOVE #N-A ( #I2 ) TO SUBSTR ( #N-OUT,#J,1 )
            //*  FE201411 END
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Name_Full.setValue(pnd_N_Out);                                                                                                                                //Natural: ASSIGN #NAME-FULL := #N-OUT
        pnd_Name_Pnd_Name_Last.reset();                                                                                                                                   //Natural: RESET #NAME-LAST #NAME-3RD #NAME-2ND #NAME-FIRST
        pnd_Name_Pnd_Name_3rd.reset();
        pnd_Name_Pnd_Name_2nd.reset();
        pnd_Name_Pnd_Name_First.reset();
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=140 PS=57");
        Global.format(1, "LS=211 PS=57");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(40),"GROUP ANNUITY PAYMENT REGISTER FOR",pnd_D_Check_Dte, 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Payee/Code",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde(),"SSN",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr(), new ReportEditMask ("999-99-9999"),"Geo/Cde",
        		pdaNeca4000.getNeca4000_Stt_Abbr_State_Cde(),"Contract/Amount",
        		pnd_Dtl_Amts_Pnd_Contract_Amt, new ReportEditMask ("ZZZ,ZZ9.99-"),"Federal/Tax",
        		pnd_Dtl_Amts_Pnd_Fed_Amt, new ReportEditMask ("ZZZ,ZZ9.99-"),"State/Local",
        		pnd_Dtl_Amts_Pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Dtl_Amts_Pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Dtl_Amts_Pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Pay/Type",
        		pnd_Pymnt_Nbr_Pnd_Pymnt_Type,"Payee/Name & Address",
        		pnd_Name_Full, new AlphanumericLength (36));
    }
}
