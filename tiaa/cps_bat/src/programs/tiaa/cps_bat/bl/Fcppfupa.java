/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:45 PM
**        * FROM NATURAL PROGRAM : Fcppfupa
************************************************************
**        * FILE NAME            : Fcppfupa.java
**        * CLASS NAME           : Fcppfupa
**        * INSTANCE NAME        : Fcppfupa
************************************************************
************************************************************************
* PROGRAM   : FCPPFUPA
*
* SYSTEM    : CPS
* TITLE     : LEDGER REVERSAL MODULE
* GENERATED : 08/13/2020  CTS CPS SUNSET (CHG830649)
*
* FUNCTION  : THIS MODULE READS CURRENT LEDGER PAYMENT FILE AND STORE
*           : FUTURE PAYMENTS IN GDS.
*
* HISTORY   :
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcppfupa extends BLNatBase
{
    // Data Areas
    private LdaFcpl121 ldaFcpl121;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Datx;
    private DbsField pnd_Input_Date;
    private DbsField pnd_Interface_Date;

    private DbsGroup pnd_Cntl_File;
    private DbsField pnd_Cntl_File_Pnd_Prev_Run_Date;
    private DbsField pnd_Cntl_File_Filler;
    private DbsField pnd_Curr_Run_Date;
    private DbsField pnd_Ws_Prev_Run_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl121 = new LdaFcpl121();
        registerRecord(ldaFcpl121);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Cntrct_Ppcn_Nbr = localVariables.newFieldInRecord("pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 70);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Input_Date = localVariables.newFieldInRecord("pnd_Input_Date", "#INPUT-DATE", FieldType.DATE);
        pnd_Interface_Date = localVariables.newFieldInRecord("pnd_Interface_Date", "#INTERFACE-DATE", FieldType.DATE);

        pnd_Cntl_File = localVariables.newGroupInRecord("pnd_Cntl_File", "#CNTL-FILE");
        pnd_Cntl_File_Pnd_Prev_Run_Date = pnd_Cntl_File.newFieldInGroup("pnd_Cntl_File_Pnd_Prev_Run_Date", "#PREV-RUN-DATE", FieldType.STRING, 8);
        pnd_Cntl_File_Filler = pnd_Cntl_File.newFieldInGroup("pnd_Cntl_File_Filler", "FILLER", FieldType.STRING, 72);
        pnd_Curr_Run_Date = localVariables.newFieldInRecord("pnd_Curr_Run_Date", "#CURR-RUN-DATE", FieldType.STRING, 8);
        pnd_Ws_Prev_Run_Date = localVariables.newFieldInRecord("pnd_Ws_Prev_Run_Date", "#WS-PREV-RUN-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl121.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcppfupa() throws Exception
    {
        super("Fcppfupa");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcppfupa|Main");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Date, new FieldAttributes ("AD='_'"), new ReportEditMask ("YYYYMMDD"));                      //Natural: INPUT #INPUT-DATE ( AD = '_' EM = YYYYMMDD )
                if (condition(pnd_Input_Date.equals(getZero())))                                                                                                          //Natural: IF #INPUT-DATE = 0
                {
                    getReports().write(1, "***",new TabSetting(25),"INPUT INTERFACE DATE WAS NOT SUPPLIED",new TabSetting(77),"***");                                     //Natural: WRITE ( 1 ) '***' 25T 'INPUT INTERFACE DATE WAS NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
                pnd_Datx.setValue(Global.getDATX());                                                                                                                      //Natural: ASSIGN #DATX := *DATX
                //*  IF *TIMN GE 0000000 AND *TIMN LE 0800000
                //*  #DATX := *DATX - 1
                //*  END-IF
                pnd_Curr_Run_Date.setValueEdited(pnd_Datx,new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #CURR-RUN-DATE
                getReports().write(0, "#CURR-RUN-DATE: ",pnd_Curr_Run_Date);                                                                                              //Natural: WRITE '#CURR-RUN-DATE: ' #CURR-RUN-DATE
                if (Global.isEscape()) return;
                READWORK01:                                                                                                                                               //Natural: READ WORK 04 RECORD #CNTL-FILE
                while (condition(getWorkFiles().read(4, pnd_Cntl_File)))
                {
                    getReports().write(0, "#PREV-RUN-DATE: ",pnd_Cntl_File_Pnd_Prev_Run_Date);                                                                            //Natural: WRITE '#PREV-RUN-DATE: ' #PREV-RUN-DATE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Prev_Run_Date.setValue(pnd_Cntl_File_Pnd_Prev_Run_Date);                                                                                       //Natural: MOVE #PREV-RUN-DATE TO #WS-PREV-RUN-DATE
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                READWORK02:                                                                                                                                               //Natural: READ WORK 01 RECORD #LEDGER-EXTRACT
                while (condition(getWorkFiles().read(1, ldaFcpl121.getPnd_Ledger_Extract())))
                {
                    if (condition(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde().equals("NZ")))                                                                       //Natural: IF CNTRCT-ORGN-CDE = 'NZ'
                    {
                        if (condition(pnd_Curr_Run_Date.equals(pnd_Ws_Prev_Run_Date)))                                                                                    //Natural: IF #CURR-RUN-DATE = #WS-PREV-RUN-DATE
                        {
                                                                                                                                                                          //Natural: PERFORM ERROR-JOB-RUN
                            sub_Error_Job_Run();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition((Global.getTIMN().greaterOrEqual(0) && Global.getTIMN().lessOrEqual(2000000)) && (pnd_Curr_Run_Date.greater(pnd_Ws_Prev_Run_Date)))) //Natural: IF ( *TIMN GE 0000000 AND *TIMN LE 2000000 ) AND ( #CURR-RUN-DATE GT #WS-PREV-RUN-DATE )
                        {
                            pnd_Datx.compute(new ComputeParameters(false, pnd_Datx), Global.getDATX().subtract(1));                                                       //Natural: ASSIGN #DATX := *DATX - 1
                            pnd_Curr_Run_Date.setValueEdited(pnd_Datx,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #CURR-RUN-DATE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition((pnd_Curr_Run_Date.equals(pnd_Ws_Prev_Run_Date))))                                                                                  //Natural: IF ( #CURR-RUN-DATE EQ #WS-PREV-RUN-DATE )
                        {
                                                                                                                                                                          //Natural: PERFORM ERROR-JOB-RUN
                            sub_Error_Job_Run();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde().equals("NZ")))                                                                       //Natural: IF CNTRCT-ORGN-CDE = 'NZ'
                    {
                        pnd_Interface_Date.setValue(pnd_Input_Date);                                                                                                      //Natural: ASSIGN #INTERFACE-DATE := #INPUT-DATE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Interface_Date.setValue(pnd_Datx);                                                                                                            //Natural: ASSIGN #INTERFACE-DATE := #DATX
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte().greater(pnd_Interface_Date)))                                                      //Natural: IF PYMNT-INTRFCE-DTE > #INTERFACE-DATE
                    {
                        getReports().write(0, ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde(),"-",ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr(),                  //Natural: WRITE CNTRCT-ORGN-CDE '-' CNTRCT-PPCN-NBR '-' PYMNT-INTRFCE-DTE ' | INTERFACE DATE:' #INTERFACE-DATE ' ==> WRITING FUTURE FILE..'
                            "-",ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte()," | INTERFACE DATE:",pnd_Interface_Date," ==> WRITING FUTURE FILE..");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getWorkFiles().write(2, false, ldaFcpl121.getPnd_Ledger_Extract());                                                                               //Natural: WRITE WORK FILE 02 #LEDGER-EXTRACT
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde(),"-",ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr(),                  //Natural: WRITE CNTRCT-ORGN-CDE '-' CNTRCT-PPCN-NBR '-' PYMNT-INTRFCE-DTE ' | INTERFACE DATE:' #INTERFACE-DATE ' ==> WRITING CURRENT FILE..'
                            "-",ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte()," | INTERFACE DATE:",pnd_Interface_Date," ==> WRITING CURRENT FILE..");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getWorkFiles().write(3, false, ldaFcpl121.getPnd_Ledger_Extract());                                                                               //Natural: WRITE WORK FILE 03 #LEDGER-EXTRACT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK02_Exit:
                if (Global.isEscape()) return;
                pnd_Cntl_File_Pnd_Prev_Run_Date.setValue(pnd_Curr_Run_Date);                                                                                              //Natural: MOVE #CURR-RUN-DATE TO #PREV-RUN-DATE
                getWorkFiles().write(5, false, pnd_Cntl_File);                                                                                                            //Natural: WRITE WORK FILE 5 #CNTL-FILE
                //* ****************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-JOB-RUN
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Error_Job_Run() throws Exception                                                                                                                     //Natural: ERROR-JOB-RUN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        getReports().write(0, "************      ERROR   ******************");                                                                                            //Natural: WRITE '************      ERROR   ******************'
        if (Global.isEscape()) return;
        getReports().write(0, "PREVIOUS RUN DATE IS SAME AS CURRENT DATE   ");                                                                                            //Natural: WRITE 'PREVIOUS RUN DATE IS SAME AS CURRENT DATE   '
        if (Global.isEscape()) return;
        getReports().write(0, "THIS STEP CAN NOT RUN TWICE IN THE SAME DATE");                                                                                            //Natural: WRITE 'THIS STEP CAN NOT RUN TWICE IN THE SAME DATE'
        if (Global.isEscape()) return;
        getReports().write(0, "PLEASE RUN FROM NEXT STEP OR CONTACT SUPPORT");                                                                                            //Natural: WRITE 'PLEASE RUN FROM NEXT STEP OR CONTACT SUPPORT'
        if (Global.isEscape()) return;
        getReports().write(0, "********************************************");                                                                                            //Natural: WRITE '********************************************'
        if (Global.isEscape()) return;
        pnd_Cntl_File_Pnd_Prev_Run_Date.setValue(pnd_Ws_Prev_Run_Date);                                                                                                   //Natural: MOVE #WS-PREV-RUN-DATE TO #PREV-RUN-DATE
        getWorkFiles().write(5, false, pnd_Cntl_File);                                                                                                                    //Natural: WRITE WORK FILE 5 #CNTL-FILE
        DbsUtil.terminate(92);  if (true) return;                                                                                                                         //Natural: TERMINATE 0092
        //*  ERROR-JOB-RUN
    }

    //
}
