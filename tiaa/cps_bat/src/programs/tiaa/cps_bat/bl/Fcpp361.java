/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:47 PM
**        * FROM NATURAL PROGRAM : Fcpp361
************************************************************
**        * FILE NAME            : Fcpp361.java
**        * CLASS NAME           : Fcpp361
**        * INSTANCE NAME        : Fcpp361
************************************************************
************************************************************************
* PROGRAM  : FCPP520
* SYSTEM   : CPS
* TITLE    : E.W. SUSPENSE REPURCHASE
* CREATED  : OCT 19, 1999 - CLONED FROM FCPP290
* FUNCTION : THIS PROGRAM WILL EXTRACT FROM A WORK FILE ALL RECORDS
*            WITH PAYMENT-TYPE-REQUEST-INDICATOR = 6 (SUSPENSE) AND
*            CNTRCT-TYPE-CDE = R (REPURCHASE).
* 12/07/99 : USE FCPAEXT FOR EW SXTRACT
* 06/11/02 : RE-STOWED. PA-SELECT AND SPIA NEW FUNDS IN FCPL199A
* 04/22/03 : RE-STOWED. FCPAEXT WAS EXPANDED
* 4/2017   : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp361 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl199a ldaFcpl199a;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl200 ldaFcpl200;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Prime_Counter;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_15;
    private DbsField pnd_Header0_2;
    private DbsField pnd_First;
    private DbsField pnd_Header_Date;
    private DbsField pnd_W_Fld;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Ws_Fund_Name;
    private DbsField pnd_Ws_Tot_Net_Pymnt;
    private DbsField pnd_Ws_Diff_Total_Amount;
    private DbsField pnd_Ws_Diff_No_Of_Transactions;

    private DbsGroup pnd_Suspense_Repurchase_Record;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Payee_Cde;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Annuitant_Full_Name;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Pymnt_Settlmnt_Dte_Mdy;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Reason_Cde;

    private DbsGroup pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt_X;

    private DbsGroup pnd_Suspense_Repurchase_Record__R_Field_1;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde;
    private DbsField pnd_Suspense_Repurchase_Record_Pnd_Op_Net_Pymnt_Amt;

    private DbsGroup pnd_Suspense_Repurchase_Record_Trailer;
    private DbsField pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Trailer_Tag;
    private DbsField pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Run_Date_Mdy;
    private DbsField pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Item_Count;
    private DbsField pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Total_Pymnt_Amt;
    private DbsField pnd_Suspense_Repurchase_Record_Trailer_Pnd_Filler;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl199a = new LdaFcpl199a();
        registerRecord(ldaFcpl199a);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl200 = new LdaFcpl200();
        registerRecord(ldaFcpl200);
        registerRecord(ldaFcpl200.getVw_fcp_Cons_Cntrl_View());

        // Local Variables
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Prime_Counter = localVariables.newFieldInRecord("pnd_Prime_Counter", "#PRIME-COUNTER", FieldType.PACKED_DECIMAL, 7);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_15 = localVariables.newFieldInRecord("pnd_Header0_15", "#HEADER0-15", FieldType.STRING, 64);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 64);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Header_Date = localVariables.newFieldInRecord("pnd_Header_Date", "#HEADER-DATE", FieldType.STRING, 10);
        pnd_W_Fld = localVariables.newFieldInRecord("pnd_W_Fld", "#W-FLD", FieldType.STRING, 64);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Ws_Fund_Name = localVariables.newFieldArrayInRecord("pnd_Ws_Fund_Name", "#WS-FUND-NAME", FieldType.STRING, 15, new DbsArrayController(1, 40));
        pnd_Ws_Tot_Net_Pymnt = localVariables.newFieldInRecord("pnd_Ws_Tot_Net_Pymnt", "#WS-TOT-NET-PYMNT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Diff_Total_Amount = localVariables.newFieldInRecord("pnd_Ws_Diff_Total_Amount", "#WS-DIFF-TOTAL-AMOUNT", FieldType.NUMERIC, 10, 2);
        pnd_Ws_Diff_No_Of_Transactions = localVariables.newFieldInRecord("pnd_Ws_Diff_No_Of_Transactions", "#WS-DIFF-NO-OF-TRANSACTIONS", FieldType.NUMERIC, 
            8);

        pnd_Suspense_Repurchase_Record = localVariables.newGroupInRecord("pnd_Suspense_Repurchase_Record", "#SUSPENSE-REPURCHASE-RECORD");
        pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Payee_Cde = pnd_Suspense_Repurchase_Record.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Payee_Cde", 
            "#OP-CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Ppcn_Nbr = pnd_Suspense_Repurchase_Record.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Ppcn_Nbr", 
            "#OP-CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Suspense_Repurchase_Record_Pnd_Op_Annt_Soc_Sec_Nbr = pnd_Suspense_Repurchase_Record.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Annt_Soc_Sec_Nbr", 
            "#OP-ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9);
        pnd_Suspense_Repurchase_Record_Pnd_Op_Annuitant_Full_Name = pnd_Suspense_Repurchase_Record.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Annuitant_Full_Name", 
            "#OP-ANNUITANT-FULL-NAME", FieldType.STRING, 20);
        pnd_Suspense_Repurchase_Record_Pnd_Op_Pymnt_Settlmnt_Dte_Mdy = pnd_Suspense_Repurchase_Record.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Pymnt_Settlmnt_Dte_Mdy", 
            "#OP-PYMNT-SETTLMNT-DTE-MDY", FieldType.STRING, 8);
        pnd_Suspense_Repurchase_Record_Pnd_Op_Reason_Cde = pnd_Suspense_Repurchase_Record.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Reason_Cde", 
            "#OP-REASON-CDE", FieldType.STRING, 1);

        pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt = pnd_Suspense_Repurchase_Record.newGroupArrayInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt", 
            "#OP-FUND-CDE-AMT", new DbsArrayController(1, 20));
        pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt_X = pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt_X", 
            "#OP-FUND-CDE-AMT-X", FieldType.STRING, 11);

        pnd_Suspense_Repurchase_Record__R_Field_1 = pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt.newGroupInGroup("pnd_Suspense_Repurchase_Record__R_Field_1", 
            "REDEFINE", pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde_Amt_X);
        pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde = pnd_Suspense_Repurchase_Record__R_Field_1.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde", 
            "#OP-FUND-CDE", FieldType.STRING, 2);
        pnd_Suspense_Repurchase_Record_Pnd_Op_Net_Pymnt_Amt = pnd_Suspense_Repurchase_Record__R_Field_1.newFieldInGroup("pnd_Suspense_Repurchase_Record_Pnd_Op_Net_Pymnt_Amt", 
            "#OP-NET-PYMNT-AMT", FieldType.NUMERIC, 9, 2);

        pnd_Suspense_Repurchase_Record_Trailer = localVariables.newGroupInRecord("pnd_Suspense_Repurchase_Record_Trailer", "#SUSPENSE-REPURCHASE-RECORD-TRAILER");
        pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Trailer_Tag = pnd_Suspense_Repurchase_Record_Trailer.newFieldInGroup("pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Trailer_Tag", 
            "#OP-TRAILER-TAG", FieldType.STRING, 1);
        pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Run_Date_Mdy = pnd_Suspense_Repurchase_Record_Trailer.newFieldInGroup("pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Run_Date_Mdy", 
            "#OP-RUN-DATE-MDY", FieldType.STRING, 8);
        pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Item_Count = pnd_Suspense_Repurchase_Record_Trailer.newFieldInGroup("pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Item_Count", 
            "#OP-ITEM-COUNT", FieldType.NUMERIC, 9);
        pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Total_Pymnt_Amt = pnd_Suspense_Repurchase_Record_Trailer.newFieldInGroup("pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Total_Pymnt_Amt", 
            "#OP-TOTAL-PYMNT-AMT", FieldType.NUMERIC, 11, 2);
        pnd_Suspense_Repurchase_Record_Trailer_Pnd_Filler = pnd_Suspense_Repurchase_Record_Trailer.newFieldInGroup("pnd_Suspense_Repurchase_Record_Trailer_Pnd_Filler", 
            "#FILLER", FieldType.STRING, 241);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCdbatxa.initializeValues();
        ldaFcpl199a.initializeValues();
        ldaFcpl200.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_15.setInitialValue("ELECTRONIC WARRANTS SUSPENSE REPURCHASE REPORT");
        pnd_First.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp361() throws Exception
    {
        super("Fcpp361");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp361|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                //*                                                                                                                                                       //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 01 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM = *PROGRAM
                //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
                pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                              //Natural: ASSIGN #CUR-LANG = *LANGUAGE
                pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                       //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Header0_2);                                                                                        //Natural: INPUT #HEADER0-2
                pnd_Header_Date.setValueEdited(Global.getDATX(),new ReportEditMask("MM/DD/YYYY"));                                                                        //Natural: MOVE EDITED *DATX ( EM = MM/DD/YYYY ) TO #HEADER-DATE
                pnd_Header0_2.setValue(DbsUtil.compress(pnd_Header0_2, pnd_Header_Date));                                                                                 //Natural: COMPRESS #HEADER0-2 #HEADER-DATE INTO #HEADER0-2
                DbsUtil.examine(new ExamineSource(pnd_Header0_2,true), new ExamineSearch(" "), new ExamineGivingLength(pnd_I));                                           //Natural: EXAMINE FULL #HEADER0-2 FOR ' ' GIVING LENGTH #I
                pnd_I.compute(new ComputeParameters(false, pnd_I), (DbsField.subtract(64,pnd_I)).divide(2));                                                              //Natural: COMPUTE #I = ( 64 - #I ) / 2
                pnd_W_Fld.moveAll("H'00'");                                                                                                                               //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #I
                pnd_Header0_2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header0_2));                                                        //Natural: COMPRESS #W-FLD #HEADER0-2 INTO #HEADER0-2 LEAVING NO SPACE
                DbsUtil.examine(new ExamineSource(pnd_Header0_2,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                        //Natural: EXAMINE FULL #HEADER0-2 FOR FULL H'00' REPLACE ' '
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( 01 )
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH'
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA = 'INFP9000'
                //* ***********************
                //*   MAIN PROGRAM LOGIC  *
                //* ***********************
                //* *
                //*  READ  WORK FILE 01  PYMNT-ADDR-INFO  INV-INFO (*)    /*  ROXAN
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 01 EXT ( * )
                while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
                {
                    //* *
                    if (condition(!(pdaFcpaext.getExt_Cntrct_Type_Cde().equals("R") && pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(6))))                            //Natural: ACCEPT IF CNTRCT-TYPE-CDE = 'R' AND PYMNT-PAY-TYPE-REQ-IND = 6
                    {
                        continue;
                    }
                    pnd_Suspense_Repurchase_Record.reset();                                                                                                               //Natural: RESET #SUSPENSE-REPURCHASE-RECORD #OP-NET-PYMNT-AMT ( * )
                    pnd_Suspense_Repurchase_Record_Pnd_Op_Net_Pymnt_Amt.getValue("*").reset();
                    pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Payee_Cde.setValue(pdaFcpaext.getExt_Cntrct_Payee_Cde());                                                //Natural: ASSIGN #OP-CNTRCT-PAYEE-CDE = CNTRCT-PAYEE-CDE
                    pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Ppcn_Nbr.setValue(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr());                                                  //Natural: ASSIGN #OP-CNTRCT-PPCN-NBR = CNTRCT-PPCN-NBR
                    pnd_Suspense_Repurchase_Record_Pnd_Op_Annt_Soc_Sec_Nbr.setValue(pdaFcpaext.getExt_Annt_Soc_Sec_Nbr());                                                //Natural: ASSIGN #OP-ANNT-SOC-SEC-NBR = ANNT-SOC-SEC-NBR
                    pnd_Suspense_Repurchase_Record_Pnd_Op_Annuitant_Full_Name.setValue(DbsUtil.compress(pdaFcpaext.getExt_Ph_Last_Name(), pdaFcpaext.getExt_Ph_First_Name(),  //Natural: COMPRESS PH-LAST-NAME PH-FIRST-NAME PH-MIDDLE-NAME INTO #OP-ANNUITANT-FULL-NAME
                        pdaFcpaext.getExt_Ph_Middle_Name()));
                    pnd_Suspense_Repurchase_Record_Pnd_Op_Pymnt_Settlmnt_Dte_Mdy.setValueEdited(pdaFcpaext.getExt_Pymnt_Settlmnt_Dte(),new ReportEditMask("MMDDYYYY"));   //Natural: MOVE EDITED PYMNT-SETTLMNT-DTE ( EM = MMDDYYYY ) TO #OP-PYMNT-SETTLMNT-DTE-MDY
                    pnd_Suspense_Repurchase_Record_Pnd_Op_Reason_Cde.setValue("1");                                                                                       //Natural: ASSIGN #OP-REASON-CDE = '1'
                    //* *
                    pnd_Ws_Fund_Name.getValue("*").reset();                                                                                                               //Natural: RESET #WS-FUND-NAME ( * )
                    FOR01:                                                                                                                                                //Natural: FOR #I FROM 1 TO C-INV-ACCT
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaFcpaext.getExt_C_Inv_Acct())); pnd_I.nadd(1))
                    {
                        if (condition(pdaFcpaext.getExt_Inv_Acct_Cde().getValue(pnd_I).equals(" ")))                                                                      //Natural: IF INV-ACCT-CDE ( #I ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        //*  RCC
                        if (condition(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I).greater(getZero())))                                                     //Natural: IF INV-ACCT-NET-PYMNT-AMT ( #I ) > 0
                        {
                            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpaext.getExt_Inv_Acct_Cde().getValue(pnd_I));                                  //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := INV-ACCT-CDE ( #I )
                            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
                            if (condition(Global.isEscape())) return;
                            pdaFcpaext.getExt_Inv_Acct_Cde().getValue(pnd_I).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha());                                  //Natural: ASSIGN INV-ACCT-CDE ( #I ) := #INV-ACCT-ALPHA
                            DbsUtil.examine(new ExamineSource(ldaFcpl199a.getPnd_Fund_Lda_Pnd_Inv_Acct().getValue("*"),true), new ExamineSearch(pdaFcpaext.getExt_Inv_Acct_Cde().getValue(pnd_I),  //Natural: EXAMINE FULL #INV-ACCT ( * ) FOR FULL INV-ACCT-CDE ( #I ) GIVING INDEX #J
                                true), new ExamineGivingIndex(pnd_J));
                            //*        ASSIGN #WS-FUND-NAME(#I) = #INV-ACCT-DESC-8 (#J)     /* RCC
                            pnd_Ws_Fund_Name.getValue(pnd_I).setValue(ldaFcpl199a.getPnd_Fund_Lda_Pnd_Inv_Acct_Desc_8().getValue(pnd_J));                                 //Natural: ASSIGN #WS-FUND-NAME ( #I ) = #FUND-LDA.#INV-ACCT-DESC-8 ( #J )
                            pnd_Suspense_Repurchase_Record_Pnd_Op_Fund_Cde.getValue(pnd_I).setValue(pdaFcpaext.getExt_Inv_Acct_Cde().getValue(pnd_I));                    //Natural: ASSIGN #OP-FUND-CDE ( #I ) = INV-ACCT-CDE ( #I )
                            pnd_Suspense_Repurchase_Record_Pnd_Op_Net_Pymnt_Amt.getValue(pnd_I).setValue(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));     //Natural: ASSIGN #OP-NET-PYMNT-AMT ( #I ) = INV-ACCT-NET-PYMNT-AMT ( #I )
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *
                    pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Item_Count.nadd(1);                                                                                     //Natural: ADD 1 TO #OP-ITEM-COUNT
                    pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Total_Pymnt_Amt.nadd(pdaFcpaext.getExt_Pymnt_Check_Amt());                                              //Natural: ADD PYMNT-CHECK-AMT TO #OP-TOTAL-PYMNT-AMT
                    getReports().skip(0, 2);                                                                                                                              //Natural: SKIP 2
                    getReports().display(1, new ReportEmptyLineSuppression(true),new ColumnSpacing(2),"INST",                                                             //Natural: DISPLAY ( 01 ) ( ES = ON ) 2X 'INST' #OP-CNTRCT-PAYEE-CDE 4X 'NAME' #OP-ANNUITANT-FULL-NAME 4X 'PPCN'#OP-CNTRCT-PPCN-NBR 3X 'SOC SEC #' #OP-ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) 5X 'SETTLEMENT/EFFECTIVE/DATE' #OP-PYMNT-SETTLMNT-DTE-MDY ( EM = XX/XX/XXXX ) 5X 'INVESTMENT/ACCOUNT' #WS-FUND-NAME ( * ) 6X 'NET PAYMENT' #OP-NET-PYMNT-AMT ( * ) ( EM = ZZZ,ZZZ,ZZ9.99- ZP = OFF )
                    		pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Payee_Cde,new ColumnSpacing(4),"NAME",
                    		pnd_Suspense_Repurchase_Record_Pnd_Op_Annuitant_Full_Name,new ColumnSpacing(4),"PPCN",
                    		pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Ppcn_Nbr,new ColumnSpacing(3),"SOC SEC #",
                    		pnd_Suspense_Repurchase_Record_Pnd_Op_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(5),"SETTLEMENT/EFFECTIVE/DATE",
                        
                    		pnd_Suspense_Repurchase_Record_Pnd_Op_Pymnt_Settlmnt_Dte_Mdy, new ReportEditMask ("XX/XX/XXXX"),new ColumnSpacing(5),"INVESTMENT/ACCOUNT",
                        
                    		pnd_Ws_Fund_Name.getValue("*"),new ColumnSpacing(6),"NET PAYMENT",
                    		pnd_Suspense_Repurchase_Record_Pnd_Op_Net_Pymnt_Amt.getValue("*"), new ReportEditMask ("Z,ZZZ,ZZ9.99-"), new ReportZeroPrint (false));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *
                    //*      NZ-EXT.INV-ACCT-NET-PYMNT-AMT(1:C-INV-ACCT) + 0    /*  ROXAN
                    pnd_Ws_Tot_Net_Pymnt.compute(new ComputeParameters(false, pnd_Ws_Tot_Net_Pymnt), pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(1,               //Natural: COMPUTE #WS-TOT-NET-PYMNT = EXT.INV-ACCT-NET-PYMNT-AMT ( 1:C-INV-ACCT ) + 0
                        ":",pdaFcpaext.getExt_C_Inv_Acct()).add(getZero()));
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,new ColumnSpacing(74),"TOTAL",new ColumnSpacing(16),pnd_Ws_Tot_Net_Pymnt, new ReportEditMask       //Natural: WRITE ( 01 ) / 74X 'TOTAL' 16X #WS-TOT-NET-PYMNT ( EM = ZZZ,ZZZ,ZZ9.99- )
                        ("ZZZ,ZZZ,ZZ9.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //*  IDENTIFIES TRAILER RECORD
                pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Trailer_Tag.setValue("*");                                                                                  //Natural: MOVE '*' TO #OP-TRAILER-TAG
                pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Run_Date_Mdy.setValueEdited(Global.getDATX(),new ReportEditMask("MMDDYYYY"));                               //Natural: MOVE EDITED *DATX ( EM = MMDDYYYY ) TO #OP-RUN-DATE-MDY
                ldaFcpl200.getVw_fcp_Cons_Cntrl_View().startDatabaseRead                                                                                                  //Natural: READ ( 1 ) FCP-CONS-CNTRL-VIEW BY CNTL-ORG-CDE-INVRSE-DTE
                (
                "READ02",
                new Oc[] { new Oc("CNTL_ORG_CDE_INVRSE_DTE", "ASC") },
                1
                );
                READ02:
                while (condition(ldaFcpl200.getVw_fcp_Cons_Cntrl_View().readNextRow("READ02")))
                {
                    if (condition(ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Type_Cde().getValue(5).equals("SP")))                                                            //Natural: IF CNTL-TYPE-CDE ( 5 ) = 'SP'
                    {
                        pnd_Ws_Diff_No_Of_Transactions.compute(new ComputeParameters(false, pnd_Ws_Diff_No_Of_Transactions), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(5).subtract(pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Item_Count)); //Natural: SUBTRACT #OP-ITEM-COUNT FROM FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 5 ) GIVING #WS-DIFF-NO-OF-TRANSACTIONS
                        pnd_Ws_Diff_Total_Amount.compute(new ComputeParameters(false, pnd_Ws_Diff_Total_Amount), ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(5).subtract(pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Total_Pymnt_Amt)); //Natural: SUBTRACT #OP-TOTAL-PYMNT-AMT FROM FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 5 ) GIVING #WS-DIFF-TOTAL-AMOUNT
                        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"CPS TOTAL AMOUNT: ",new ColumnSpacing(11),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Net_Amt().getValue(5),new  //Natural: WRITE ( 01 ) // 'CPS TOTAL AMOUNT: ' 11X FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT ( 5 ) 8X 'NUMBER OF TRANSACTIONS: ' 3X FCP-CONS-CNTRL-VIEW.CNTL-CNT ( 5 ) / 'FCPP361 TOTAL AMOUNT: ' 7X #OP-TOTAL-PYMNT-AMT 8X 'NUMBER OF TRANSACTIONS: ' #OP-ITEM-COUNT / 'DIFFERENCE TOTAL AMOUNT: ' 5X #WS-DIFF-TOTAL-AMOUNT 8X 'NUMBER OF TRANSACTIONS:  ' #WS-DIFF-NO-OF-TRANSACTIONS
                            ColumnSpacing(8),"NUMBER OF TRANSACTIONS: ",new ColumnSpacing(3),ldaFcpl200.getFcp_Cons_Cntrl_View_Cntl_Cnt().getValue(5),NEWLINE,"FCPP361 TOTAL AMOUNT: ",new 
                            ColumnSpacing(7),pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Total_Pymnt_Amt,new ColumnSpacing(8),"NUMBER OF TRANSACTIONS: ",pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Item_Count,NEWLINE,"DIFFERENCE TOTAL AMOUNT: ",new 
                            ColumnSpacing(5),pnd_Ws_Diff_Total_Amount,new ColumnSpacing(8),"NUMBER OF TRANSACTIONS:  ",pnd_Ws_Diff_No_Of_Transactions);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Ws_Diff_No_Of_Transactions.notEquals(getZero()) && pnd_Ws_Diff_Total_Amount.notEquals(getZero())))                              //Natural: IF #WS-DIFF-NO-OF-TRANSACTIONS NOT = 0 AND #WS-DIFF-TOTAL-AMOUNT NOT = 0
                        {
                            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"(ERROR MESSAGE)",NEWLINE,"FCPP361 SUSPENSE CONTROL TOTALS   DO NOT BALANCE  ",new //Natural: WRITE ( 01 ) // '(ERROR MESSAGE)' / 'FCPP361 SUSPENSE CONTROL TOTALS   DO NOT BALANCE  ' 1X 'TO CPS SUSPENSE CONTROL TOTALS' // '*-----------------------------*' / '* CPS SYSTEM HAS BEEN ABORTED *' / '*-----------------------------*'
                                ColumnSpacing(1),"TO CPS SUSPENSE CONTROL TOTALS",NEWLINE,NEWLINE,"*-----------------------------*",NEWLINE,"* CPS SYSTEM HAS BEEN ABORTED *",
                                NEWLINE,"*-----------------------------*");
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            DbsUtil.terminate(88);  if (true) return;                                                                                                     //Natural: TERMINATE 0088
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                if (condition(pnd_Suspense_Repurchase_Record_Trailer_Pnd_Op_Item_Count.equals(getZero())))                                                                //Natural: IF #OP-ITEM-COUNT = 0
                {
                    getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(20),"********       NO REPURCHASE TRANSACTION        *********");   //Natural: WRITE ( 01 ) /// 20T '********       NO REPURCHASE TRANSACTION        *********'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* *
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(0),  //Natural: WRITE ( 01 ) NOTITLE *PROGRAM 41T #HEADER0-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 43T #HEADER0-15 / 34T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
                        TabSetting(43),pnd_Header0_15,NEWLINE,new TabSetting(34),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask 
                        ("HH':'II' 'AP"));
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().setDisplayColumns(1, new ReportEmptyLineSuppression(true),new ColumnSpacing(2),"INST",
        		pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Payee_Cde,new ColumnSpacing(4),"NAME",
        		pnd_Suspense_Repurchase_Record_Pnd_Op_Annuitant_Full_Name,new ColumnSpacing(4),"PPCN",
        		pnd_Suspense_Repurchase_Record_Pnd_Op_Cntrct_Ppcn_Nbr,new ColumnSpacing(3),"SOC SEC #",
        		pnd_Suspense_Repurchase_Record_Pnd_Op_Annt_Soc_Sec_Nbr, new ReportEditMask ("999-99-9999"),new ColumnSpacing(5),"SETTLEMENT/EFFECTIVE/DATE",
        		pnd_Suspense_Repurchase_Record_Pnd_Op_Pymnt_Settlmnt_Dte_Mdy, new ReportEditMask ("XX/XX/XXXX"),new ColumnSpacing(5),"INVESTMENT/ACCOUNT",
        		pnd_Ws_Fund_Name,new ColumnSpacing(6),"NET PAYMENT",
        		pnd_Suspense_Repurchase_Record_Pnd_Op_Net_Pymnt_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"), new ReportZeroPrint (false));
    }
}
