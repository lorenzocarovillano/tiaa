/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 10:22:25 PM
**        * FROM NATURAL PROGRAM : Rmwb0010
************************************************************
**        * FILE NAME            : Rmwb0010.java
**        * CLASS NAME           : Rmwb0010
**        * INSTANCE NAME        : Rmwb0010
************************************************************
* ----------------------------------------------------------------------
* PROGRAM  : RMWB0010
* SYSTEM   : FCP-CONS-PYMNT DOWNLOAD
* ----------------------------------------------------------------------
********************************
** HISTORY
*  11/21/13 F.ENDAYA   - TRANSFER RMW CPS PAYMENT AND FUNDS RECORDS
*                      - EXTRACT TO THIS PROGRAM. FE201311
*  09/21/2015 FENDAYA  - INCLUDE AP AND NZ FUND INFORMATION IN THE
*                        EXTRACT FILE.            FE201509
*
*  03/2017    J.OSTEEN - PIN EXPANSION TO 12 BYTES            /* JWO
*
* ----------------------------------------------------------------------
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Rmwb0010 extends BLNatBase
{
    // Data Areas
    private PdaErla1000 pdaErla1000;
    private PdaInta040 pdaInta040;
    private PdaMiha0100 pdaMiha0100;
    private PdaCdaobj pdaCdaobj;
    private LdaCwflwr ldaCwflwr;
    private LdaCwflwrb ldaCwflwrb;
    private LdaCwflwra ldaCwflwra;
    private LdaCwflact ldaCwflact;
    private LdaCwflacta ldaCwflacta;
    private LdaCwflcntr ldaCwflcntr;
    private LdaCwflcoup ldaCwflcoup;
    private PdaCwfa1400 pdaCwfa1400;
    private PdaCwfa1401 pdaCwfa1401;
    private PdaCwfpda_P pdaCwfpda_P;
    private PdaCwfpda_D pdaCwfpda_D;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup constants;
    private DbsField constants_Pnd_Rmwb0010;

    private DataAccessProgramView vw_payment;
    private DbsField payment_Cntrct_Ppcn_Nbr;
    private DbsField payment_Cntrct_Payee_Cde;
    private DbsField payment_Cntrct_Invrse_Dte;
    private DbsField payment_Cntrct_Orgn_Cde;
    private DbsField payment_Cntrct_Pymnt_Type_Ind;
    private DbsField payment_Cntrct_Sttlmnt_Type_Ind;
    private DbsField payment_Pymnt_Stats_Cde;
    private DbsField payment_Pymnt_Cmbne_Ind;
    private DbsField payment_Pymnt_Pay_Type_Req_Ind;
    private DbsField payment_Cntrct_Unq_Id_Nbr;
    private DbsField payment_Cntrct_Type_Cde;
    private DbsField payment_Cntrct_Lob_Cde;
    private DbsField payment_Cntrct_Sub_Lob_Cde;
    private DbsField payment_Cntrct_Ia_Lob_Cde;
    private DbsField payment_Cntrct_Mode_Cde;
    private DbsField payment_Cntrct_Pymnt_Dest_Cde;
    private DbsField payment_Cntrct_Roll_Dest_Cde;
    private DbsField payment_Annt_Ctznshp_Cde;
    private DbsField payment_Annt_Rsdncy_Cde;
    private DbsField payment_Pymnt_Split_Cde;
    private DbsField payment_Pymnt_Split_Reasn_Cde;
    private DbsField payment_Pymnt_Check_Dte;
    private DbsField payment_Pymnt_Prcss_Seq_Nbr;
    private DbsField payment_Pymnt_Settlmnt_Dte;
    private DbsField payment_Cntrct_Cancel_Rdrw_Ind;
    private DbsField payment_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField payment_Cntrct_Cancel_Rdrw_Amt;
    private DbsField payment_Cnr_Cs_Rqust_Dte;
    private DbsField payment_Ppcn_Inv_Orgn_Prcss_Inst;
    private DbsField payment_Pymnt_Instlmnt_Nbr;
    private DbsField payment_Pymnt_Reqst_Nbr;
    private DbsField payment_Pymnt_Acctg_Dte;
    private DbsField payment_Cntrct_Settl_Amt;
    private DbsField payment_Nbr_Of_Funds;
    private DbsField payment_Count_Castinv_Acct;

    private DbsGroup payment_Inv_Acct;
    private DbsField payment_Inv_Acct_Cde;
    private DbsField payment_Inv_Acct_Unit_Qty;
    private DbsField payment_Inv_Acct_Unit_Value;
    private DbsField payment_Inv_Acct_Settl_Amt;
    private DbsField payment_Inv_Acct_Fed_Cde;
    private DbsField payment_Inv_Acct_State_Cde;
    private DbsField payment_Inv_Acct_Local_Cde;
    private DbsField payment_Inv_Acct_Ivc_Amt;
    private DbsField payment_Inv_Acct_Dci_Amt;
    private DbsField payment_Inv_Acct_Dpi_Amt;
    private DbsField payment_Inv_Acct_Start_Accum_Amt;
    private DbsField payment_Inv_Acct_End_Accum_Amt;
    private DbsField payment_Inv_Acct_Dvdnd_Amt;
    private DbsField payment_Inv_Acct_Net_Pymnt_Amt;
    private DbsField payment_Inv_Acct_Ivc_Ind;
    private DbsField payment_Inv_Acct_Adj_Ivc_Amt;
    private DbsField payment_Inv_Acct_Valuat_Period;
    private DbsField payment_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField payment_Inv_Acct_State_Tax_Amt;
    private DbsField payment_Inv_Acct_Local_Tax_Amt;
    private DbsField payment_Inv_Acct_Exp_Amt;
    private DbsField payment_Cntrct_Cmbn_Nbr;
    private DbsField payment_Cntrct_Option_Cde;

    private DbsGroup payment_Out;
    private DbsField payment_Out_Payment_Out_Data;

    private DbsGroup payment_Out__R_Field_1;
    private DbsField payment_Out_Cntrct_Ppcn_Nbr;
    private DbsField payment_Out_Cntrct_Payee_Cde;
    private DbsField payment_Out_Cntrct_Invrse_Dte;
    private DbsField payment_Out_Cntrct_Orgn_Cde;
    private DbsField payment_Out_Cntrct_Pymnt_Type_Ind;
    private DbsField payment_Out_Cntrct_Sttlmnt_Type_Ind;
    private DbsField payment_Out_Pymnt_Stats_Cde;
    private DbsField payment_Out_Pymnt_Cmbne_Ind;
    private DbsField payment_Out_Pymnt_Pay_Type_Req_Ind;
    private DbsField payment_Out_Cntrct_Unq_Id_Nbr;
    private DbsField payment_Out_Cntrct_Type_Cde;
    private DbsField payment_Out_Cntrct_Lob_Cde;
    private DbsField payment_Out_Cntrct_Sub_Lob_Cde;
    private DbsField payment_Out_Cntrct_Ia_Lob_Cde;
    private DbsField payment_Out_Cntrct_Mode_Cde;
    private DbsField payment_Out_Cntrct_Pymnt_Dest_Cde;
    private DbsField payment_Out_Cntrct_Roll_Dest_Cde;
    private DbsField payment_Out_Annt_Ctznshp_Cde;
    private DbsField payment_Out_Annt_Rsdncy_Cde;
    private DbsField payment_Out_Pymnt_Split_Cde;
    private DbsField payment_Out_Pymnt_Split_Reasn_Cde;
    private DbsField payment_Out_Pymnt_Check_Dte;
    private DbsField payment_Out_Pymnt_Settlmnt_Dte;
    private DbsField payment_Out_Cntrct_Cancel_Rdrw_Ind;
    private DbsField payment_Out_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField payment_Out_Cntrct_Cancel_Rdrw_Amt;
    private DbsField payment_Out_Cnr_Cs_Rqust_Dte;
    private DbsField payment_Out_Ppcn_Inv_Orgn_Prcss_Inst;
    private DbsField payment_Out_Pymnt_Instlmnt_Nbr;
    private DbsField payment_Out_Pymnt_Reqst_Nbr;
    private DbsField payment_Out_Pymnt_Acctg_Dte;
    private DbsField payment_Out_Cntrct_Settl_Amt;
    private DbsField payment_Out_Cntrct_Option_Cde;
    private DbsField payment_Out_Filler1;

    private DbsGroup payment_Out__R_Field_2;
    private DbsField payment_Out_Header_Ind_Out;
    private DbsField payment_Out_Header_Date_Out;
    private DbsField payment_Out_Header_Filler_Out;

    private DbsGroup payment_Out__R_Field_3;
    private DbsField payment_Out_Trailer_Ind_Out;
    private DbsField payment_Out_Trailer_Date_Out;
    private DbsField payment_Out_Trailer_Count_Out;
    private DbsField payment_Out_Trailer_Filler_Out;

    private DataAccessProgramView vw_payment_2;
    private DbsField payment_2_Cntrct_Orgn_Cde;
    private DbsField payment_2_Cntrct_Lob_Cde;
    private DbsField payment_2_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField payment_2_Cntrct_Settl_Amt;
    private DbsField payment_2_Pymnt_Instlmnt_Nbr;
    private DbsField payment_2_Pymnt_Reqst_Nbr;

    private DbsGroup payment_Out_2;
    private DbsField payment_Out_2_Payment_Out_Data_2;

    private DbsGroup payment_Out_2__R_Field_4;
    private DbsField payment_Out_2_Cntrct_Orgn_Cde;
    private DbsField payment_Out_2_Cntrct_Lob_Cde;
    private DbsField payment_Out_2_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField payment_Out_2_Cntrct_Settl_Amt;
    private DbsField payment_Out_2_Pymnt_Instlmnt_Nbr;
    private DbsField payment_Out_2_Pymnt_Reqst_Nbr;

    private DbsGroup payment_Out_2__R_Field_5;
    private DbsField payment_Out_2_Header_Ind_Out_2;
    private DbsField payment_Out_2_Header_Date_Out_2;
    private DbsField payment_Out_2_Header_Filler_Out_2;

    private DbsGroup payment_Out_2__R_Field_6;
    private DbsField payment_Out_2_Trailer_Ind_Out_2;
    private DbsField payment_Out_2_Trailer_Date_Out_2;
    private DbsField payment_Out_2_Trailer_Count_Out_2;
    private DbsField payment_Out_2_Trailer_Filler_Out_2;
    private DbsField pnd_I;
    private DbsField pnd_Write_Count;
    private DbsField pnd_Write_Count_2;
    private DbsField pnd_Current_Cntrct_Invrse_Dte;
    private DbsField pnd_Prior_Cntrct_Invrse_Dte;
    private DbsField pnd_Invrse_Found;
    private DbsField pnd_Isn;
    private DbsField pnd_Save_Cntrct_Invrse_Dte;
    private DbsField pnd_Save_Current_Date;
    private DbsField pnd_Error_Msg;

    private DbsGroup pnd_Usr0020n_Parm;

    private DbsGroup pnd_Usr0020n_Parm_Pnd_Parm_Area;
    private DbsField pnd_Usr0020n_Parm_Pnd_Error_Type;
    private DbsField pnd_Usr0020n_Parm_Pnd_Error_Application;
    private DbsField pnd_Usr0020n_Parm_Pnd_Error_Nr;
    private DbsField pnd_Usr0020n_Parm_Pnd_Error_Language_Code;
    private DbsField pnd_Usr0020n_Parm_Pnd_Error_Response;

    private DbsGroup pnd_Usr0020n_Parm_Pnd_Return_Area;
    private DbsField pnd_Usr0020n_Parm_Pnd_Short_Text_Flag;
    private DbsField pnd_Usr0020n_Parm_Pnd_Long_Text_Flag;
    private DbsField pnd_Usr0020n_Parm_Pnd_Short_Text;
    private DbsField pnd_Usr0020n_Parm_Pnd_Long_Text;

    private DataAccessProgramView vw_cwf_Corp_Rpt_Start_Tbl;
    private DbsField cwf_Corp_Rpt_Start_Tbl_Tbl_Scrty_Level_Ind;
    private DbsField cwf_Corp_Rpt_Start_Tbl_Tbl_Table_Nme;
    private DbsField cwf_Corp_Rpt_Start_Tbl_Tbl_Key_Field;

    private DbsGroup cwf_Corp_Rpt_Start_Tbl__R_Field_7;
    private DbsField cwf_Corp_Rpt_Start_Tbl_Dte_Key;
    private DbsField cwf_Corp_Rpt_Start_Tbl_Dte_Key_Fill;
    private DbsField cwf_Corp_Rpt_Start_Tbl_Tbl_Data_Field;

    private DbsGroup cwf_Corp_Rpt_Start_Tbl__R_Field_8;
    private DbsField cwf_Corp_Rpt_Start_Tbl_Invrse_Dte;
    private DbsField cwf_Corp_Rpt_Start_Tbl_Rest_Of_Data;
    private DbsField pnd_St_Tbl_Prime_Key;

    private DbsGroup pnd_St_Tbl_Prime_Key__R_Field_9;
    private DbsField pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Scrty_Level_Ind;
    private DbsField pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Nme;
    private DbsField pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Key;

    private DataAccessProgramView vw_cps_Fund;
    private DbsField cps_Fund_Pymnt_Reqst_Nbr;
    private DbsField cps_Fund_Pymnt_Instlmnt_Nbr;
    private DbsField cps_Fund_Inv_Acct_Seq_Nbr;
    private DbsField cps_Fund_Inv_Acct_Ticker;
    private DbsField cps_Fund_Inv_Acct_Dvdnd_Amt;
    private DbsField cps_Fund_Inv_Acct_Unit_Value;
    private DbsField cps_Fund_Inv_Acct_Unit_Qty;
    private DbsField cps_Fund_Inv_Acct_Settl_Amt;
    private DbsField cps_Fund_Inv_Acct_Exp_Amt;
    private DbsField cps_Fund_Inv_Acct_Ivc_Amt;
    private DbsField cps_Fund_Inv_Acct_Start_Accum_Amt;
    private DbsField cps_Fund_Inv_Acct_End_Accum_Amt;
    private DbsField cps_Fund_Inv_Acct_Ivc_Ind;
    private DbsField cps_Fund_Inv_Acct_Adj_Ivc_Amt;
    private DbsField cps_Fund_Inv_Acct_Valuat_Period;
    private DbsField cps_Fund_Inv_Acct_Fed_Cde;
    private DbsField cps_Fund_Inv_Acct_State_Cde;
    private DbsField cps_Fund_Inv_Acct_Local_Cde;
    private DbsField cps_Fund_Inv_Acct_Cde;

    private DbsGroup cps_Fund_Out;
    private DbsField cps_Fund_Out_Cps_Out_Data;

    private DbsGroup cps_Fund_Out__R_Field_10;
    private DbsField cps_Fund_Out_Pymnt_Reqst_Nbr;
    private DbsField cps_Fund_Out_Pymnt_Instlmnt_Nbr;
    private DbsField cps_Fund_Out_Inv_Acct_Seq_Nbr;
    private DbsField cps_Fund_Out_Inv_Acct_Ticker;
    private DbsField cps_Fund_Out_Inv_Acct_Dvdnd_Amt;
    private DbsField cps_Fund_Out_Inv_Acct_Unit_Value;
    private DbsField cps_Fund_Out_Inv_Acct_Unit_Qty;
    private DbsField cps_Fund_Out_Inv_Acct_Settl_Amt;
    private DbsField cps_Fund_Out_Inv_Acct_Exp_Amt;
    private DbsField cps_Fund_Out_Inv_Acct_Ivc_Amt;
    private DbsField cps_Fund_Out_Inv_Acct_Start_Accum_Amt;
    private DbsField cps_Fund_Out_Inv_Acct_End_Accum_Amt;
    private DbsField cps_Fund_Out_Inv_Acct_Ivc_Ind;
    private DbsField cps_Fund_Out_Inv_Acct_Adj_Ivc_Amt;
    private DbsField cps_Fund_Out_Inv_Acct_Valuat_Period;
    private DbsField cps_Fund_Out_Inv_Acct_Fed_Cde;
    private DbsField cps_Fund_Out_Inv_Acct_State_Cde;
    private DbsField cps_Fund_Out_Inv_Acct_Local_Cde;
    private DbsField cps_Fund_Out_Inv_Acct_Cde;

    private DbsGroup cps_Fund_Out__R_Field_11;
    private DbsField cps_Fund_Out_Header_Ind_Out;
    private DbsField cps_Fund_Out_Header_Date_Out;
    private DbsField cps_Fund_Out_Header_Filler_Out;

    private DbsGroup cps_Fund_Out__R_Field_12;
    private DbsField cps_Fund_Out_Trailer_Ind_Out;
    private DbsField cps_Fund_Out_Trailer_Date_Out;
    private DbsField cps_Fund_Out_Trailer_Count_Out;
    private DbsField cps_Fund_Out_Trailer_Filler_Out;

    private DbsGroup cps_Fund_Out3;
    private DbsField cps_Fund_Out3_Cps_Out_Data3;
    private DbsField pnd_Read_Count;
    private DbsField pnd_Write_Cnt3;
    private DbsField pnd_Rec_Type;
    private DbsField pnd_Pymnt_Reqst_Instmt_Seq;

    private DbsGroup pnd_Pymnt_Reqst_Instmt_Seq__R_Field_13;
    private DbsField pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr;
    private DbsField pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr;
    private DbsField pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Inv_Acct_Seq_Nbr;
    private DbsField legacy_Eft_Key;

    private DbsGroup legacy_Eft_Key__R_Field_14;
    private DbsField legacy_Eft_Key_Key_Contract;
    private DbsField legacy_Eft_Key_Key_Payee;
    private DbsField legacy_Eft_Key_Key_Origin;
    private DbsField legacy_Eft_Key_Key_Inverse_Date;
    private DbsField legacy_Eft_Key_Key_Process_Seq;

    private DbsGroup legacy_Eft_Key__R_Field_15;
    private DbsField legacy_Eft_Key_Pnd_Seq_A;
    private DbsField legacy_Eft_Key_Pnd_Seq_Fil;
    private DbsField pnd_I2;
    private DbsField pnd_Prior_Date_Incr;
    private DbsField pnd_Fund_Cde;
    private DbsField pnd_Tckr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaErla1000 = new PdaErla1000(localVariables);
        pdaInta040 = new PdaInta040(localVariables);
        pdaMiha0100 = new PdaMiha0100(localVariables);
        pdaCdaobj = new PdaCdaobj(localVariables);
        ldaCwflwr = new LdaCwflwr();
        registerRecord(ldaCwflwr);
        registerRecord(ldaCwflwr.getVw_cwf_Who_Wr());
        ldaCwflwrb = new LdaCwflwrb();
        registerRecord(ldaCwflwrb);
        ldaCwflwra = new LdaCwflwra();
        registerRecord(ldaCwflwra);
        ldaCwflact = new LdaCwflact();
        registerRecord(ldaCwflact);
        registerRecord(ldaCwflact.getVw_cwf_Who_Act());
        ldaCwflacta = new LdaCwflacta();
        registerRecord(ldaCwflacta);
        ldaCwflcntr = new LdaCwflcntr();
        registerRecord(ldaCwflcntr);
        ldaCwflcoup = new LdaCwflcoup();
        registerRecord(ldaCwflcoup);
        pdaCwfa1400 = new PdaCwfa1400(localVariables);
        pdaCwfa1401 = new PdaCwfa1401(localVariables);
        pdaCwfpda_P = new PdaCwfpda_P(localVariables);
        pdaCwfpda_D = new PdaCwfpda_D(localVariables);

        // Local Variables

        constants = localVariables.newGroupInRecord("constants", "CONSTANTS");
        constants_Pnd_Rmwb0010 = constants.newFieldInGroup("constants_Pnd_Rmwb0010", "#RMWB0010", FieldType.STRING, 8);

        vw_payment = new DataAccessProgramView(new NameInfo("vw_payment", "PAYMENT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));
        payment_Cntrct_Ppcn_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        payment_Cntrct_Payee_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        payment_Cntrct_Invrse_Dte = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        payment_Cntrct_Orgn_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        payment_Cntrct_Pymnt_Type_Ind = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_TYPE_IND");
        payment_Cntrct_Sttlmnt_Type_Ind = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_STTLMNT_TYPE_IND");
        payment_Pymnt_Stats_Cde = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        payment_Pymnt_Cmbne_Ind = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CMBNE_IND");
        payment_Pymnt_Pay_Type_Req_Ind = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        payment_Cntrct_Unq_Id_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_UNQ_ID_NBR");
        payment_Cntrct_Type_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        payment_Cntrct_Lob_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_LOB_CDE");
        payment_Cntrct_Sub_Lob_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_SUB_LOB_CDE");
        payment_Cntrct_Ia_Lob_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_IA_LOB_CDE");
        payment_Cntrct_Mode_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_CDE");
        payment_Cntrct_Pymnt_Dest_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_DEST_CDE");
        payment_Cntrct_Roll_Dest_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_ROLL_DEST_CDE");
        payment_Annt_Ctznshp_Cde = vw_payment.getRecord().newFieldInGroup("payment_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ANNT_CTZNSHP_CDE");
        payment_Annt_Rsdncy_Cde = vw_payment.getRecord().newFieldInGroup("payment_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ANNT_RSDNCY_CDE");
        payment_Pymnt_Split_Cde = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "PYMNT_SPLIT_CDE");
        payment_Pymnt_Split_Reasn_Cde = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 
            6, RepeatingFieldStrategy.None, "PYMNT_SPLIT_REASN_CDE");
        payment_Pymnt_Check_Dte = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        payment_Pymnt_Prcss_Seq_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        payment_Pymnt_Settlmnt_Dte = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_SETTLMNT_DTE");
        payment_Cntrct_Cancel_Rdrw_Ind = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_IND");
        payment_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        payment_Cntrct_Cancel_Rdrw_Amt = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Cancel_Rdrw_Amt", "CNTRCT-CANCEL-RDRW-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_AMT");
        payment_Cnr_Cs_Rqust_Dte = vw_payment.getRecord().newFieldInGroup("payment_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNR_CS_RQUST_DTE");
        payment_Ppcn_Inv_Orgn_Prcss_Inst = vw_payment.getRecord().newFieldInGroup("payment_Ppcn_Inv_Orgn_Prcss_Inst", "PPCN-INV-ORGN-PRCSS-INST", FieldType.STRING, 
            29, RepeatingFieldStrategy.None, "PPCN_INV_ORGN_PRCSS_INST");
        payment_Ppcn_Inv_Orgn_Prcss_Inst.setSuperDescriptor(true);
        payment_Pymnt_Instlmnt_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 2, 
            RepeatingFieldStrategy.None, "PYMNT_INSTLMNT_NBR");
        payment_Pymnt_Reqst_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        payment_Pymnt_Acctg_Dte = vw_payment.getRecord().newFieldInGroup("payment_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_ACCTG_DTE");
        payment_Cntrct_Settl_Amt = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Settl_Amt", "CNTRCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 13, 
            2, RepeatingFieldStrategy.None, "CNTRCT_SETTL_AMT");
        payment_Nbr_Of_Funds = vw_payment.getRecord().newFieldInGroup("payment_Nbr_Of_Funds", "NBR-OF-FUNDS", FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, 
            "NBR_OF_FUNDS");
        payment_Count_Castinv_Acct = vw_payment.getRecord().newFieldInGroup("payment_Count_Castinv_Acct", "C*INV-ACCT", RepeatingFieldStrategy.CAsteriskVariable, 
            "FCP_CONS_PYMT_INV_ACCT");

        payment_Inv_Acct = vw_payment.getRecord().newGroupArrayInGroup("payment_Inv_Acct", "INV-ACCT", new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Cde = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_CDE", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Unit_Qty = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9, 3, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_UNIT_QTY", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Unit_Value = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_UNIT_VALUE", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Settl_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 11, 
            2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_SETTL_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Fed_Cde = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_FED_CDE", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_State_Cde = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_STATE_CDE", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Local_Cde = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_LOCAL_CDE", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Ivc_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_IVC_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Dci_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DCI_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Dpi_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DPI_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Start_Accum_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_START_ACCUM_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_End_Accum_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_END_ACCUM_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Dvdnd_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DVDND_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Net_Pymnt_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_NET_PYMNT_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Ivc_Ind = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_IVC_IND", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Adj_Ivc_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_ADJ_IVC_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Valuat_Period = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 
            1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_VALUAT_PERIOD", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Valuat_Period.setDdmHeader("RE-/VALUATION/PERIOD");
        payment_Inv_Acct_Fdrl_Tax_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FDRL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_State_Tax_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STATE_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Local_Tax_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOCAL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Inv_Acct_Exp_Amt = payment_Inv_Acct.newFieldInGroup("payment_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_EXP_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payment_Cntrct_Cmbn_Nbr = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        payment_Cntrct_Option_Cde = vw_payment.getRecord().newFieldInGroup("payment_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTION_CDE");
        registerRecord(vw_payment);

        payment_Out = localVariables.newGroupInRecord("payment_Out", "PAYMENT-OUT");
        payment_Out_Payment_Out_Data = payment_Out.newFieldInGroup("payment_Out_Payment_Out_Data", "PAYMENT-OUT-DATA", FieldType.STRING, 205);

        payment_Out__R_Field_1 = payment_Out.newGroupInGroup("payment_Out__R_Field_1", "REDEFINE", payment_Out_Payment_Out_Data);
        payment_Out_Cntrct_Ppcn_Nbr = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        payment_Out_Cntrct_Payee_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        payment_Out_Cntrct_Invrse_Dte = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        payment_Out_Cntrct_Orgn_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        payment_Out_Cntrct_Pymnt_Type_Ind = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 
            1);
        payment_Out_Cntrct_Sttlmnt_Type_Ind = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        payment_Out_Pymnt_Stats_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1);
        payment_Out_Pymnt_Cmbne_Ind = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 1);
        payment_Out_Pymnt_Pay_Type_Req_Ind = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        payment_Out_Cntrct_Unq_Id_Nbr = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        payment_Out_Cntrct_Type_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        payment_Out_Cntrct_Lob_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        payment_Out_Cntrct_Sub_Lob_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 
            4);
        payment_Out_Cntrct_Ia_Lob_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 
            2);
        payment_Out_Cntrct_Mode_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3);
        payment_Out_Cntrct_Pymnt_Dest_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 
            4);
        payment_Out_Cntrct_Roll_Dest_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 
            4);
        payment_Out_Annt_Ctznshp_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2);
        payment_Out_Annt_Rsdncy_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        payment_Out_Pymnt_Split_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", FieldType.STRING, 2);
        payment_Out_Pymnt_Split_Reasn_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 
            6);
        payment_Out_Pymnt_Check_Dte = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.STRING, 8);
        payment_Out_Pymnt_Settlmnt_Dte = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.STRING, 
            8);
        payment_Out_Cntrct_Cancel_Rdrw_Ind = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 
            1);
        payment_Out_Cntrct_Cancel_Rdrw_Actvty_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        payment_Out_Cntrct_Cancel_Rdrw_Amt = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Cancel_Rdrw_Amt", "CNTRCT-CANCEL-RDRW-AMT", FieldType.NUMERIC, 
            11, 2);
        payment_Out_Cnr_Cs_Rqust_Dte = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.STRING, 8);
        payment_Out_Ppcn_Inv_Orgn_Prcss_Inst = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Ppcn_Inv_Orgn_Prcss_Inst", "PPCN-INV-ORGN-PRCSS-INST", 
            FieldType.STRING, 29);
        payment_Out_Pymnt_Instlmnt_Nbr = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 
            2);
        payment_Out_Pymnt_Reqst_Nbr = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35);
        payment_Out_Pymnt_Acctg_Dte = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.STRING, 8);
        payment_Out_Cntrct_Settl_Amt = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Settl_Amt", "CNTRCT-SETTL-AMT", FieldType.NUMERIC, 13, 
            2);
        payment_Out_Cntrct_Option_Cde = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        payment_Out_Filler1 = payment_Out__R_Field_1.newFieldInGroup("payment_Out_Filler1", "FILLER1", FieldType.STRING, 2);

        payment_Out__R_Field_2 = payment_Out.newGroupInGroup("payment_Out__R_Field_2", "REDEFINE", payment_Out_Payment_Out_Data);
        payment_Out_Header_Ind_Out = payment_Out__R_Field_2.newFieldInGroup("payment_Out_Header_Ind_Out", "HEADER-IND-OUT", FieldType.STRING, 7);
        payment_Out_Header_Date_Out = payment_Out__R_Field_2.newFieldInGroup("payment_Out_Header_Date_Out", "HEADER-DATE-OUT", FieldType.NUMERIC, 8);
        payment_Out_Header_Filler_Out = payment_Out__R_Field_2.newFieldInGroup("payment_Out_Header_Filler_Out", "HEADER-FILLER-OUT", FieldType.STRING, 
            185);

        payment_Out__R_Field_3 = payment_Out.newGroupInGroup("payment_Out__R_Field_3", "REDEFINE", payment_Out_Payment_Out_Data);
        payment_Out_Trailer_Ind_Out = payment_Out__R_Field_3.newFieldInGroup("payment_Out_Trailer_Ind_Out", "TRAILER-IND-OUT", FieldType.STRING, 7);
        payment_Out_Trailer_Date_Out = payment_Out__R_Field_3.newFieldInGroup("payment_Out_Trailer_Date_Out", "TRAILER-DATE-OUT", FieldType.NUMERIC, 8);
        payment_Out_Trailer_Count_Out = payment_Out__R_Field_3.newFieldInGroup("payment_Out_Trailer_Count_Out", "TRAILER-COUNT-OUT", FieldType.NUMERIC, 
            10);
        payment_Out_Trailer_Filler_Out = payment_Out__R_Field_3.newFieldInGroup("payment_Out_Trailer_Filler_Out", "TRAILER-FILLER-OUT", FieldType.STRING, 
            175);

        vw_payment_2 = new DataAccessProgramView(new NameInfo("vw_payment_2", "PAYMENT-2"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        payment_2_Cntrct_Orgn_Cde = vw_payment_2.getRecord().newFieldInGroup("payment_2_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        payment_2_Cntrct_Lob_Cde = vw_payment_2.getRecord().newFieldInGroup("payment_2_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_LOB_CDE");
        payment_2_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_payment_2.getRecord().newFieldInGroup("payment_2_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        payment_2_Cntrct_Settl_Amt = vw_payment_2.getRecord().newFieldInGroup("payment_2_Cntrct_Settl_Amt", "CNTRCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_SETTL_AMT");
        payment_2_Pymnt_Instlmnt_Nbr = vw_payment_2.getRecord().newFieldInGroup("payment_2_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PYMNT_INSTLMNT_NBR");
        payment_2_Pymnt_Reqst_Nbr = vw_payment_2.getRecord().newFieldInGroup("payment_2_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        registerRecord(vw_payment_2);

        payment_Out_2 = localVariables.newGroupInRecord("payment_Out_2", "PAYMENT-OUT-2");
        payment_Out_2_Payment_Out_Data_2 = payment_Out_2.newFieldInGroup("payment_Out_2_Payment_Out_Data_2", "PAYMENT-OUT-DATA-2", FieldType.STRING, 58);

        payment_Out_2__R_Field_4 = payment_Out_2.newGroupInGroup("payment_Out_2__R_Field_4", "REDEFINE", payment_Out_2_Payment_Out_Data_2);
        payment_Out_2_Cntrct_Orgn_Cde = payment_Out_2__R_Field_4.newFieldInGroup("payment_Out_2_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        payment_Out_2_Cntrct_Lob_Cde = payment_Out_2__R_Field_4.newFieldInGroup("payment_Out_2_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4);
        payment_Out_2_Cntrct_Cancel_Rdrw_Actvty_Cde = payment_Out_2__R_Field_4.newFieldInGroup("payment_Out_2_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        payment_Out_2_Cntrct_Settl_Amt = payment_Out_2__R_Field_4.newFieldInGroup("payment_Out_2_Cntrct_Settl_Amt", "CNTRCT-SETTL-AMT", FieldType.NUMERIC, 
            13, 2);
        payment_Out_2_Pymnt_Instlmnt_Nbr = payment_Out_2__R_Field_4.newFieldInGroup("payment_Out_2_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 
            2);
        payment_Out_2_Pymnt_Reqst_Nbr = payment_Out_2__R_Field_4.newFieldInGroup("payment_Out_2_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 
            35);

        payment_Out_2__R_Field_5 = payment_Out_2.newGroupInGroup("payment_Out_2__R_Field_5", "REDEFINE", payment_Out_2_Payment_Out_Data_2);
        payment_Out_2_Header_Ind_Out_2 = payment_Out_2__R_Field_5.newFieldInGroup("payment_Out_2_Header_Ind_Out_2", "HEADER-IND-OUT-2", FieldType.STRING, 
            7);
        payment_Out_2_Header_Date_Out_2 = payment_Out_2__R_Field_5.newFieldInGroup("payment_Out_2_Header_Date_Out_2", "HEADER-DATE-OUT-2", FieldType.NUMERIC, 
            8);
        payment_Out_2_Header_Filler_Out_2 = payment_Out_2__R_Field_5.newFieldInGroup("payment_Out_2_Header_Filler_Out_2", "HEADER-FILLER-OUT-2", FieldType.STRING, 
            43);

        payment_Out_2__R_Field_6 = payment_Out_2.newGroupInGroup("payment_Out_2__R_Field_6", "REDEFINE", payment_Out_2_Payment_Out_Data_2);
        payment_Out_2_Trailer_Ind_Out_2 = payment_Out_2__R_Field_6.newFieldInGroup("payment_Out_2_Trailer_Ind_Out_2", "TRAILER-IND-OUT-2", FieldType.STRING, 
            7);
        payment_Out_2_Trailer_Date_Out_2 = payment_Out_2__R_Field_6.newFieldInGroup("payment_Out_2_Trailer_Date_Out_2", "TRAILER-DATE-OUT-2", FieldType.NUMERIC, 
            8);
        payment_Out_2_Trailer_Count_Out_2 = payment_Out_2__R_Field_6.newFieldInGroup("payment_Out_2_Trailer_Count_Out_2", "TRAILER-COUNT-OUT-2", FieldType.NUMERIC, 
            10);
        payment_Out_2_Trailer_Filler_Out_2 = payment_Out_2__R_Field_6.newFieldInGroup("payment_Out_2_Trailer_Filler_Out_2", "TRAILER-FILLER-OUT-2", FieldType.STRING, 
            33);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Write_Count = localVariables.newFieldInRecord("pnd_Write_Count", "#WRITE-COUNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Write_Count_2 = localVariables.newFieldInRecord("pnd_Write_Count_2", "#WRITE-COUNT-2", FieldType.PACKED_DECIMAL, 12);
        pnd_Current_Cntrct_Invrse_Dte = localVariables.newFieldInRecord("pnd_Current_Cntrct_Invrse_Dte", "#CURRENT-CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Prior_Cntrct_Invrse_Dte = localVariables.newFieldInRecord("pnd_Prior_Cntrct_Invrse_Dte", "#PRIOR-CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Invrse_Found = localVariables.newFieldInRecord("pnd_Invrse_Found", "#INVRSE-FOUND", FieldType.BOOLEAN, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_Save_Cntrct_Invrse_Dte = localVariables.newFieldInRecord("pnd_Save_Cntrct_Invrse_Dte", "#SAVE-CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Save_Current_Date = localVariables.newFieldInRecord("pnd_Save_Current_Date", "#SAVE-CURRENT-DATE", FieldType.NUMERIC, 8);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 72);

        pnd_Usr0020n_Parm = localVariables.newGroupInRecord("pnd_Usr0020n_Parm", "#USR0020N-PARM");

        pnd_Usr0020n_Parm_Pnd_Parm_Area = pnd_Usr0020n_Parm.newGroupInGroup("pnd_Usr0020n_Parm_Pnd_Parm_Area", "#PARM-AREA");
        pnd_Usr0020n_Parm_Pnd_Error_Type = pnd_Usr0020n_Parm_Pnd_Parm_Area.newFieldInGroup("pnd_Usr0020n_Parm_Pnd_Error_Type", "#ERROR-TYPE", FieldType.STRING, 
            1);
        pnd_Usr0020n_Parm_Pnd_Error_Application = pnd_Usr0020n_Parm_Pnd_Parm_Area.newFieldInGroup("pnd_Usr0020n_Parm_Pnd_Error_Application", "#ERROR-APPLICATION", 
            FieldType.STRING, 8);
        pnd_Usr0020n_Parm_Pnd_Error_Nr = pnd_Usr0020n_Parm_Pnd_Parm_Area.newFieldInGroup("pnd_Usr0020n_Parm_Pnd_Error_Nr", "#ERROR-NR", FieldType.NUMERIC, 
            4);
        pnd_Usr0020n_Parm_Pnd_Error_Language_Code = pnd_Usr0020n_Parm_Pnd_Parm_Area.newFieldInGroup("pnd_Usr0020n_Parm_Pnd_Error_Language_Code", "#ERROR-LANGUAGE-CODE", 
            FieldType.STRING, 1);
        pnd_Usr0020n_Parm_Pnd_Error_Response = pnd_Usr0020n_Parm_Pnd_Parm_Area.newFieldInGroup("pnd_Usr0020n_Parm_Pnd_Error_Response", "#ERROR-RESPONSE", 
            FieldType.NUMERIC, 4);

        pnd_Usr0020n_Parm_Pnd_Return_Area = pnd_Usr0020n_Parm.newGroupInGroup("pnd_Usr0020n_Parm_Pnd_Return_Area", "#RETURN-AREA");
        pnd_Usr0020n_Parm_Pnd_Short_Text_Flag = pnd_Usr0020n_Parm_Pnd_Return_Area.newFieldInGroup("pnd_Usr0020n_Parm_Pnd_Short_Text_Flag", "#SHORT-TEXT-FLAG", 
            FieldType.BOOLEAN, 1);
        pnd_Usr0020n_Parm_Pnd_Long_Text_Flag = pnd_Usr0020n_Parm_Pnd_Return_Area.newFieldInGroup("pnd_Usr0020n_Parm_Pnd_Long_Text_Flag", "#LONG-TEXT-FLAG", 
            FieldType.BOOLEAN, 1);
        pnd_Usr0020n_Parm_Pnd_Short_Text = pnd_Usr0020n_Parm_Pnd_Return_Area.newFieldInGroup("pnd_Usr0020n_Parm_Pnd_Short_Text", "#SHORT-TEXT", FieldType.STRING, 
            65);
        pnd_Usr0020n_Parm_Pnd_Long_Text = pnd_Usr0020n_Parm_Pnd_Return_Area.newFieldArrayInGroup("pnd_Usr0020n_Parm_Pnd_Long_Text", "#LONG-TEXT", FieldType.STRING, 
            78, new DbsArrayController(1, 20));

        vw_cwf_Corp_Rpt_Start_Tbl = new DataAccessProgramView(new NameInfo("vw_cwf_Corp_Rpt_Start_Tbl", "CWF-CORP-RPT-START-TBL"), "CWF_CORP_RPT_START_TBL", 
            "CWF_DCMNT_TABLE");
        cwf_Corp_Rpt_Start_Tbl_Tbl_Scrty_Level_Ind = vw_cwf_Corp_Rpt_Start_Tbl.getRecord().newFieldInGroup("cwf_Corp_Rpt_Start_Tbl_Tbl_Scrty_Level_Ind", 
            "TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2, RepeatingFieldStrategy.None, "TBL_SCRTY_LEVEL_IND");
        cwf_Corp_Rpt_Start_Tbl_Tbl_Scrty_Level_Ind.setDdmHeader("SECURITY/LEVEL");
        cwf_Corp_Rpt_Start_Tbl_Tbl_Table_Nme = vw_cwf_Corp_Rpt_Start_Tbl.getRecord().newFieldInGroup("cwf_Corp_Rpt_Start_Tbl_Tbl_Table_Nme", "TBL-TABLE-NME", 
            FieldType.STRING, 20, RepeatingFieldStrategy.None, "TBL_TABLE_NME");
        cwf_Corp_Rpt_Start_Tbl_Tbl_Table_Nme.setDdmHeader("TABLE NAME");
        cwf_Corp_Rpt_Start_Tbl_Tbl_Key_Field = vw_cwf_Corp_Rpt_Start_Tbl.getRecord().newFieldInGroup("cwf_Corp_Rpt_Start_Tbl_Tbl_Key_Field", "TBL-KEY-FIELD", 
            FieldType.STRING, 30, RepeatingFieldStrategy.None, "TBL_KEY_FIELD");

        cwf_Corp_Rpt_Start_Tbl__R_Field_7 = vw_cwf_Corp_Rpt_Start_Tbl.getRecord().newGroupInGroup("cwf_Corp_Rpt_Start_Tbl__R_Field_7", "REDEFINE", cwf_Corp_Rpt_Start_Tbl_Tbl_Key_Field);
        cwf_Corp_Rpt_Start_Tbl_Dte_Key = cwf_Corp_Rpt_Start_Tbl__R_Field_7.newFieldInGroup("cwf_Corp_Rpt_Start_Tbl_Dte_Key", "DTE-KEY", FieldType.STRING, 
            8);
        cwf_Corp_Rpt_Start_Tbl_Dte_Key_Fill = cwf_Corp_Rpt_Start_Tbl__R_Field_7.newFieldInGroup("cwf_Corp_Rpt_Start_Tbl_Dte_Key_Fill", "DTE-KEY-FILL", 
            FieldType.STRING, 22);
        cwf_Corp_Rpt_Start_Tbl_Tbl_Data_Field = vw_cwf_Corp_Rpt_Start_Tbl.getRecord().newFieldInGroup("cwf_Corp_Rpt_Start_Tbl_Tbl_Data_Field", "TBL-DATA-FIELD", 
            FieldType.STRING, 253, RepeatingFieldStrategy.None, "TBL_DATA_FIELD");

        cwf_Corp_Rpt_Start_Tbl__R_Field_8 = vw_cwf_Corp_Rpt_Start_Tbl.getRecord().newGroupInGroup("cwf_Corp_Rpt_Start_Tbl__R_Field_8", "REDEFINE", cwf_Corp_Rpt_Start_Tbl_Tbl_Data_Field);
        cwf_Corp_Rpt_Start_Tbl_Invrse_Dte = cwf_Corp_Rpt_Start_Tbl__R_Field_8.newFieldInGroup("cwf_Corp_Rpt_Start_Tbl_Invrse_Dte", "INVRSE-DTE", FieldType.NUMERIC, 
            8);
        cwf_Corp_Rpt_Start_Tbl_Rest_Of_Data = cwf_Corp_Rpt_Start_Tbl__R_Field_8.newFieldInGroup("cwf_Corp_Rpt_Start_Tbl_Rest_Of_Data", "REST-OF-DATA", 
            FieldType.STRING, 245);
        registerRecord(vw_cwf_Corp_Rpt_Start_Tbl);

        pnd_St_Tbl_Prime_Key = localVariables.newFieldInRecord("pnd_St_Tbl_Prime_Key", "#ST-TBL-PRIME-KEY", FieldType.STRING, 30);

        pnd_St_Tbl_Prime_Key__R_Field_9 = localVariables.newGroupInRecord("pnd_St_Tbl_Prime_Key__R_Field_9", "REDEFINE", pnd_St_Tbl_Prime_Key);
        pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Scrty_Level_Ind = pnd_St_Tbl_Prime_Key__R_Field_9.newFieldInGroup("pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Scrty_Level_Ind", 
            "#ST-TBL-SCRTY-LEVEL-IND", FieldType.STRING, 2);
        pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Nme = pnd_St_Tbl_Prime_Key__R_Field_9.newFieldInGroup("pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Nme", "#ST-TBL-NME", FieldType.STRING, 
            20);
        pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Key = pnd_St_Tbl_Prime_Key__R_Field_9.newFieldInGroup("pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Key", "#ST-TBL-KEY", FieldType.STRING, 
            8);

        vw_cps_Fund = new DataAccessProgramView(new NameInfo("vw_cps_Fund", "CPS-FUND"), "CPS_FUND", "CPS_FUND");
        cps_Fund_Pymnt_Reqst_Nbr = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        cps_Fund_Pymnt_Instlmnt_Nbr = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PYMNT_INSTLMNT_NBR");
        cps_Fund_Inv_Acct_Seq_Nbr = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Seq_Nbr", "INV-ACCT-SEQ-NBR", FieldType.NUMERIC, 5, RepeatingFieldStrategy.None, 
            "INV_ACCT_SEQ_NBR");
        cps_Fund_Inv_Acct_Ticker = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Ticker", "INV-ACCT-TICKER", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "INV_ACCT_TICKER");
        cps_Fund_Inv_Acct_Dvdnd_Amt = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "INV_ACCT_DVDND_AMT");
        cps_Fund_Inv_Acct_Unit_Value = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 4, RepeatingFieldStrategy.None, "INV_ACCT_UNIT_VALUE");
        cps_Fund_Inv_Acct_Unit_Qty = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3, RepeatingFieldStrategy.None, "INV_ACCT_UNIT_QTY");
        cps_Fund_Inv_Acct_Settl_Amt = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "INV_ACCT_SETTL_AMT");
        cps_Fund_Inv_Acct_Exp_Amt = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "INV_ACCT_EXP_AMT");
        cps_Fund_Inv_Acct_Ivc_Amt = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "INV_ACCT_IVC_AMT");
        cps_Fund_Inv_Acct_Start_Accum_Amt = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "INV_ACCT_START_ACCUM_AMT");
        cps_Fund_Inv_Acct_End_Accum_Amt = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "INV_ACCT_END_ACCUM_AMT");
        cps_Fund_Inv_Acct_Ivc_Ind = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INV_ACCT_IVC_IND");
        cps_Fund_Inv_Acct_Adj_Ivc_Amt = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "INV_ACCT_ADJ_IVC_AMT");
        cps_Fund_Inv_Acct_Valuat_Period = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "INV_ACCT_VALUAT_PERIOD");
        cps_Fund_Inv_Acct_Fed_Cde = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "INV_ACCT_FED_CDE");
        cps_Fund_Inv_Acct_State_Cde = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "INV_ACCT_STATE_CDE");
        cps_Fund_Inv_Acct_Local_Cde = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "INV_ACCT_LOCAL_CDE");
        cps_Fund_Inv_Acct_Cde = vw_cps_Fund.getRecord().newFieldInGroup("cps_Fund_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "INV_ACCT_CDE");
        registerRecord(vw_cps_Fund);

        cps_Fund_Out = localVariables.newGroupInRecord("cps_Fund_Out", "CPS-FUND-OUT");
        cps_Fund_Out_Cps_Out_Data = cps_Fund_Out.newFieldInGroup("cps_Fund_Out_Cps_Out_Data", "CPS-OUT-DATA", FieldType.STRING, 154);

        cps_Fund_Out__R_Field_10 = cps_Fund_Out.newGroupInGroup("cps_Fund_Out__R_Field_10", "REDEFINE", cps_Fund_Out_Cps_Out_Data);
        cps_Fund_Out_Pymnt_Reqst_Nbr = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35);
        cps_Fund_Out_Pymnt_Instlmnt_Nbr = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 
            2);
        cps_Fund_Out_Inv_Acct_Seq_Nbr = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Seq_Nbr", "INV-ACCT-SEQ-NBR", FieldType.STRING, 
            5);
        cps_Fund_Out_Inv_Acct_Ticker = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Ticker", "INV-ACCT-TICKER", FieldType.STRING, 10);
        cps_Fund_Out_Inv_Acct_Dvdnd_Amt = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.NUMERIC, 
            11, 2);
        cps_Fund_Out_Inv_Acct_Unit_Value = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.NUMERIC, 
            9, 4);
        cps_Fund_Out_Inv_Acct_Unit_Qty = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.NUMERIC, 
            9, 3);
        cps_Fund_Out_Inv_Acct_Settl_Amt = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.NUMERIC, 
            11, 2);
        cps_Fund_Out_Inv_Acct_Exp_Amt = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.NUMERIC, 
            11, 2);
        cps_Fund_Out_Inv_Acct_Ivc_Amt = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.NUMERIC, 
            11, 2);
        cps_Fund_Out_Inv_Acct_Start_Accum_Amt = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.NUMERIC, 11, 2);
        cps_Fund_Out_Inv_Acct_End_Accum_Amt = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.NUMERIC, 11, 2);
        cps_Fund_Out_Inv_Acct_Ivc_Ind = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        cps_Fund_Out_Inv_Acct_Adj_Ivc_Amt = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.NUMERIC, 
            11, 2);
        cps_Fund_Out_Inv_Acct_Valuat_Period = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        cps_Fund_Out_Inv_Acct_Fed_Cde = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        cps_Fund_Out_Inv_Acct_State_Cde = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 
            1);
        cps_Fund_Out_Inv_Acct_Local_Cde = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 
            1);
        cps_Fund_Out_Inv_Acct_Cde = cps_Fund_Out__R_Field_10.newFieldInGroup("cps_Fund_Out_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2);

        cps_Fund_Out__R_Field_11 = cps_Fund_Out.newGroupInGroup("cps_Fund_Out__R_Field_11", "REDEFINE", cps_Fund_Out_Cps_Out_Data);
        cps_Fund_Out_Header_Ind_Out = cps_Fund_Out__R_Field_11.newFieldInGroup("cps_Fund_Out_Header_Ind_Out", "HEADER-IND-OUT", FieldType.STRING, 7);
        cps_Fund_Out_Header_Date_Out = cps_Fund_Out__R_Field_11.newFieldInGroup("cps_Fund_Out_Header_Date_Out", "HEADER-DATE-OUT", FieldType.NUMERIC, 
            8);
        cps_Fund_Out_Header_Filler_Out = cps_Fund_Out__R_Field_11.newFieldInGroup("cps_Fund_Out_Header_Filler_Out", "HEADER-FILLER-OUT", FieldType.STRING, 
            139);

        cps_Fund_Out__R_Field_12 = cps_Fund_Out.newGroupInGroup("cps_Fund_Out__R_Field_12", "REDEFINE", cps_Fund_Out_Cps_Out_Data);
        cps_Fund_Out_Trailer_Ind_Out = cps_Fund_Out__R_Field_12.newFieldInGroup("cps_Fund_Out_Trailer_Ind_Out", "TRAILER-IND-OUT", FieldType.STRING, 7);
        cps_Fund_Out_Trailer_Date_Out = cps_Fund_Out__R_Field_12.newFieldInGroup("cps_Fund_Out_Trailer_Date_Out", "TRAILER-DATE-OUT", FieldType.NUMERIC, 
            8);
        cps_Fund_Out_Trailer_Count_Out = cps_Fund_Out__R_Field_12.newFieldInGroup("cps_Fund_Out_Trailer_Count_Out", "TRAILER-COUNT-OUT", FieldType.NUMERIC, 
            10);
        cps_Fund_Out_Trailer_Filler_Out = cps_Fund_Out__R_Field_12.newFieldInGroup("cps_Fund_Out_Trailer_Filler_Out", "TRAILER-FILLER-OUT", FieldType.STRING, 
            129);

        cps_Fund_Out3 = localVariables.newGroupInRecord("cps_Fund_Out3", "CPS-FUND-OUT3");
        cps_Fund_Out3_Cps_Out_Data3 = cps_Fund_Out3.newFieldInGroup("cps_Fund_Out3_Cps_Out_Data3", "CPS-OUT-DATA3", FieldType.STRING, 154);
        pnd_Read_Count = localVariables.newFieldInRecord("pnd_Read_Count", "#READ-COUNT", FieldType.PACKED_DECIMAL, 12);
        pnd_Write_Cnt3 = localVariables.newFieldInRecord("pnd_Write_Cnt3", "#WRITE-CNT3", FieldType.PACKED_DECIMAL, 12);
        pnd_Rec_Type = localVariables.newFieldInRecord("pnd_Rec_Type", "#REC-TYPE", FieldType.STRING, 7);
        pnd_Pymnt_Reqst_Instmt_Seq = localVariables.newFieldInRecord("pnd_Pymnt_Reqst_Instmt_Seq", "#PYMNT-REQST-INSTMT-SEQ", FieldType.STRING, 42);

        pnd_Pymnt_Reqst_Instmt_Seq__R_Field_13 = localVariables.newGroupInRecord("pnd_Pymnt_Reqst_Instmt_Seq__R_Field_13", "REDEFINE", pnd_Pymnt_Reqst_Instmt_Seq);
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr = pnd_Pymnt_Reqst_Instmt_Seq__R_Field_13.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr", 
            "#PYMNT-REQST-NBR", FieldType.STRING, 35);
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr = pnd_Pymnt_Reqst_Instmt_Seq__R_Field_13.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr", 
            "#PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 2);
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Inv_Acct_Seq_Nbr = pnd_Pymnt_Reqst_Instmt_Seq__R_Field_13.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Inv_Acct_Seq_Nbr", 
            "#INV-ACCT-SEQ-NBR", FieldType.NUMERIC, 5);
        legacy_Eft_Key = localVariables.newFieldInRecord("legacy_Eft_Key", "LEGACY-EFT-KEY", FieldType.STRING, 33);

        legacy_Eft_Key__R_Field_14 = localVariables.newGroupInRecord("legacy_Eft_Key__R_Field_14", "REDEFINE", legacy_Eft_Key);
        legacy_Eft_Key_Key_Contract = legacy_Eft_Key__R_Field_14.newFieldInGroup("legacy_Eft_Key_Key_Contract", "KEY-CONTRACT", FieldType.STRING, 10);
        legacy_Eft_Key_Key_Payee = legacy_Eft_Key__R_Field_14.newFieldInGroup("legacy_Eft_Key_Key_Payee", "KEY-PAYEE", FieldType.STRING, 4);
        legacy_Eft_Key_Key_Origin = legacy_Eft_Key__R_Field_14.newFieldInGroup("legacy_Eft_Key_Key_Origin", "KEY-ORIGIN", FieldType.STRING, 2);
        legacy_Eft_Key_Key_Inverse_Date = legacy_Eft_Key__R_Field_14.newFieldInGroup("legacy_Eft_Key_Key_Inverse_Date", "KEY-INVERSE-DATE", FieldType.NUMERIC, 
            8);
        legacy_Eft_Key_Key_Process_Seq = legacy_Eft_Key__R_Field_14.newFieldInGroup("legacy_Eft_Key_Key_Process_Seq", "KEY-PROCESS-SEQ", FieldType.NUMERIC, 
            9);

        legacy_Eft_Key__R_Field_15 = legacy_Eft_Key__R_Field_14.newGroupInGroup("legacy_Eft_Key__R_Field_15", "REDEFINE", legacy_Eft_Key_Key_Process_Seq);
        legacy_Eft_Key_Pnd_Seq_A = legacy_Eft_Key__R_Field_15.newFieldInGroup("legacy_Eft_Key_Pnd_Seq_A", "#SEQ-A", FieldType.STRING, 7);
        legacy_Eft_Key_Pnd_Seq_Fil = legacy_Eft_Key__R_Field_15.newFieldInGroup("legacy_Eft_Key_Pnd_Seq_Fil", "#SEQ-FIL", FieldType.STRING, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 9);
        pnd_Prior_Date_Incr = localVariables.newFieldInRecord("pnd_Prior_Date_Incr", "#PRIOR-DATE-INCR", FieldType.NUMERIC, 8);
        pnd_Fund_Cde = localVariables.newFieldInRecord("pnd_Fund_Cde", "#FUND-CDE", FieldType.STRING, 1);
        pnd_Tckr = localVariables.newFieldInRecord("pnd_Tckr", "#TCKR", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_payment.reset();
        vw_payment_2.reset();
        vw_cwf_Corp_Rpt_Start_Tbl.reset();
        vw_cps_Fund.reset();

        ldaCwflwr.initializeValues();
        ldaCwflwrb.initializeValues();
        ldaCwflwra.initializeValues();
        ldaCwflact.initializeValues();
        ldaCwflacta.initializeValues();
        ldaCwflcntr.initializeValues();
        ldaCwflcoup.initializeValues();

        localVariables.reset();
        constants_Pnd_Rmwb0010.setInitialValue("RMWB0010");
        pnd_Current_Cntrct_Invrse_Dte.setInitialValue(0);
        pnd_Prior_Cntrct_Invrse_Dte.setInitialValue(0);
        pnd_Invrse_Found.setInitialValue(false);
        pnd_St_Tbl_Prime_Key.setInitialValue("A CWF-RPRT-RUN-TBL");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Rmwb0010() throws Exception
    {
        super("Rmwb0010");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Rmwb0010|Main");
        while(true)
        {
            try
            {
                //* ***********************************************************************
                //*  MAIN ROUTINE
                //* ***********************************************************************
                //*  FE201509
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Prior_Date_Incr);                                                                                  //Natural: INPUT #PRIOR-DATE-INCR
                                                                                                                                                                          //Natural: PERFORM GET-INVERSE-DATES
                sub_Get_Inverse_Dates();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  FE201509
                pnd_Prior_Cntrct_Invrse_Dte.nadd(pnd_Prior_Date_Incr);                                                                                                    //Natural: ADD #PRIOR-DATE-INCR TO #PRIOR-CNTRCT-INVRSE-DTE
                getReports().write(2, "CURRENT:",pnd_Current_Cntrct_Invrse_Dte);                                                                                          //Natural: WRITE ( 2 ) 'CURRENT:' #CURRENT-CNTRCT-INVRSE-DTE
                if (Global.isEscape()) return;
                getReports().write(2, "PRIOR:  ",pnd_Prior_Cntrct_Invrse_Dte);                                                                                            //Natural: WRITE ( 2 ) 'PRIOR:  ' #PRIOR-CNTRCT-INVRSE-DTE
                if (Global.isEscape()) return;
                payment_Out_Payment_Out_Data.reset();                                                                                                                     //Natural: RESET PAYMENT-OUT-DATA
                payment_Out_Header_Ind_Out.setValue("HEADER ");                                                                                                           //Natural: ASSIGN PAYMENT-OUT.HEADER-IND-OUT := 'HEADER '
                payment_Out_Header_Date_Out.setValue(pnd_Save_Current_Date);                                                                                              //Natural: ASSIGN PAYMENT-OUT.HEADER-DATE-OUT := #SAVE-CURRENT-DATE
                getWorkFiles().write(1, false, payment_Out);                                                                                                              //Natural: WRITE WORK FILE 1 PAYMENT-OUT
                //*  FE201311 START
                cps_Fund_Out.reset();                                                                                                                                     //Natural: RESET CPS-FUND-OUT
                cps_Fund_Out_Header_Ind_Out.setValue("HEADER ");                                                                                                          //Natural: ASSIGN CPS-FUND-OUT.HEADER-IND-OUT := 'HEADER '
                cps_Fund_Out_Header_Date_Out.compute(new ComputeParameters(false, cps_Fund_Out_Header_Date_Out), Global.getDATN().subtract(1));                           //Natural: ASSIGN CPS-FUND-OUT.HEADER-DATE-OUT := *DATN - 1
                //*  FE201311 END
                getWorkFiles().write(2, false, cps_Fund_Out);                                                                                                             //Natural: WRITE WORK FILE 2 CPS-FUND-OUT
                vw_payment.startDatabaseRead                                                                                                                              //Natural: READ PAYMENT BY CNTRCT-INVRSE-DTE STARTING FROM #CURRENT-CNTRCT-INVRSE-DTE
                (
                "RD1",
                new Wc[] { new Wc("CNTRCT_INVRSE_DTE", ">=", pnd_Current_Cntrct_Invrse_Dte, WcType.BY) },
                new Oc[] { new Oc("CNTRCT_INVRSE_DTE", "ASC") }
                );
                RD1:
                while (condition(vw_payment.readNextRow("RD1")))
                {
                    if (condition(payment_Cntrct_Invrse_Dte.greaterOrEqual(pnd_Prior_Cntrct_Invrse_Dte)))                                                                 //Natural: AT END OF DATA;//Natural: IF PAYMENT.CNTRCT-INVRSE-DTE >= #PRIOR-CNTRCT-INVRSE-DTE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Write_Count.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WRITE-COUNT
                    payment_Out_Payment_Out_Data.reset();                                                                                                                 //Natural: RESET PAYMENT-OUT-DATA
                    payment_Out.setValuesByName(vw_payment);                                                                                                              //Natural: MOVE BY NAME PAYMENT TO PAYMENT-OUT
                                                                                                                                                                          //Natural: PERFORM DATE-EDIT-ROUTINE
                    sub_Date_Edit_Routine();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  FE201509 START
                    if (condition(payment_Cntrct_Orgn_Cde.equals("AP") || payment_Cntrct_Orgn_Cde.equals("NZ")))                                                          //Natural: IF PAYMENT.CNTRCT-ORGN-CDE = 'AP' OR = 'NZ'
                    {
                        legacy_Eft_Key_Key_Contract.setValue(payment_Cntrct_Cmbn_Nbr);                                                                                    //Natural: ASSIGN KEY-CONTRACT := PAYMENT.CNTRCT-CMBN-NBR
                        legacy_Eft_Key_Key_Payee.setValue(payment_Cntrct_Payee_Cde);                                                                                      //Natural: ASSIGN KEY-PAYEE := PAYMENT.CNTRCT-PAYEE-CDE
                        legacy_Eft_Key_Key_Origin.setValue(payment_Cntrct_Orgn_Cde);                                                                                      //Natural: ASSIGN KEY-ORIGIN := PAYMENT.CNTRCT-ORGN-CDE
                        legacy_Eft_Key_Key_Inverse_Date.setValue(payment_Cntrct_Invrse_Dte);                                                                              //Natural: ASSIGN KEY-INVERSE-DATE := PAYMENT.CNTRCT-INVRSE-DTE
                        legacy_Eft_Key_Key_Process_Seq.setValue(payment_Pymnt_Prcss_Seq_Nbr);                                                                             //Natural: ASSIGN KEY-PROCESS-SEQ := PAYMENT.PYMNT-PRCSS-SEQ-NBR
                        payment_Out_Pymnt_Reqst_Nbr.setValue(legacy_Eft_Key);                                                                                             //Natural: ASSIGN PAYMENT-OUT.PYMNT-REQST-NBR := LEGACY-EFT-KEY
                        //*  FE201509 END
                    }                                                                                                                                                     //Natural: END-IF
                    getWorkFiles().write(1, false, payment_Out);                                                                                                          //Natural: WRITE WORK FILE 1 PAYMENT-OUT
                    //*  FE201311 START
                    pnd_Pymnt_Reqst_Instmt_Seq.reset();                                                                                                                   //Natural: RESET #PYMNT-REQST-INSTMT-SEQ
                    //*  POPULATE CPS-FUND KEY
                    if (condition(payment_Pymnt_Reqst_Nbr.notEquals(" ")))                                                                                                //Natural: IF PAYMENT.PYMNT-REQST-NBR NE ' '
                    {
                        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr.setValue(payment_Pymnt_Reqst_Nbr);                                                                 //Natural: ASSIGN #PYMNT-REQST-NBR := PAYMENT.PYMNT-REQST-NBR
                        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr.setValue(payment_Pymnt_Instlmnt_Nbr);                                                           //Natural: ASSIGN #PYMNT-INSTLMNT-NBR := PAYMENT.PYMNT-INSTLMNT-NBR
                        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Inv_Acct_Seq_Nbr.setValue(1);                                                                                      //Natural: ASSIGN #INV-ACCT-SEQ-NBR := 1
                        vw_cps_Fund.startDatabaseRead                                                                                                                     //Natural: READ CPS-FUND BY PYMNT-REQST-INSTMT-SEQ STARTING FROM #PYMNT-REQST-INSTMT-SEQ
                        (
                        "RD_FUND1",
                        new Wc[] { new Wc("PYMNT_REQST_INSTMT_SEQ", ">=", pnd_Pymnt_Reqst_Instmt_Seq, WcType.BY) },
                        new Oc[] { new Oc("PYMNT_REQST_INSTMT_SEQ", "ASC") }
                        );
                        RD_FUND1:
                        while (condition(vw_cps_Fund.readNextRow("RD_FUND1")))
                        {
                            if (condition((cps_Fund_Pymnt_Reqst_Nbr.notEquals(pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr) || cps_Fund_Pymnt_Instlmnt_Nbr.notEquals(pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr))  //Natural: IF ( CPS-FUND.PYMNT-REQST-NBR NE #PYMNT-REQST-NBR OR CPS-FUND.PYMNT-INSTLMNT-NBR NE #PYMNT-INSTLMNT-NBR ) OR ( PAYMENT.NBR-OF-FUNDS EQ 0 )
                                || (payment_Nbr_Of_Funds.equals(getZero()))))
                            {
                                if (true) break RD_FUND1;                                                                                                                 //Natural: ESCAPE BOTTOM ( RD-FUND1. )
                            }                                                                                                                                             //Natural: END-IF
                            pnd_Read_Count.nadd(1);                                                                                                                       //Natural: ADD 1 TO #READ-COUNT
                            cps_Fund_Out.reset();                                                                                                                         //Natural: RESET CPS-FUND-OUT
                            cps_Fund_Out.setValuesByName(vw_cps_Fund);                                                                                                    //Natural: MOVE BY NAME CPS-FUND TO CPS-FUND-OUT
                            getWorkFiles().write(2, false, cps_Fund_Out);                                                                                                 //Natural: WRITE WORK FILE 2 CPS-FUND-OUT
                            //*  FE201311 END
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  FE201509 START
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(payment_Cntrct_Orgn_Cde.equals("AP") || payment_Cntrct_Orgn_Cde.equals("NZ")))                                                      //Natural: IF PAYMENT.CNTRCT-ORGN-CDE = 'AP' OR EQ 'NZ'
                        {
                            FOR01:                                                                                                                                        //Natural: FOR #I2 1 TO 40
                            for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(40)); pnd_I2.nadd(1))
                            {
                                if (condition(payment_Inv_Acct_Cde.getValue(pnd_I2).equals(" ")))                                                                         //Natural: IF PAYMENT.INV-ACCT-CDE ( #I2 ) = ' '
                                {
                                    if (condition(true)) break;                                                                                                           //Natural: ESCAPE BOTTOM
                                }                                                                                                                                         //Natural: END-IF
                                pnd_Read_Count.nadd(1);                                                                                                                   //Natural: ADD 1 TO #READ-COUNT
                                cps_Fund_Out.reset();                                                                                                                     //Natural: RESET CPS-FUND-OUT
                                cps_Fund_Out_Inv_Acct_Seq_Nbr.reset();                                                                                                    //Natural: RESET CPS-FUND-OUT.INV-ACCT-SEQ-NBR
                                pnd_Fund_Cde.setValue(payment_Inv_Acct_Cde.getValue(pnd_I2));                                                                             //Natural: ASSIGN #FUND-CDE := PAYMENT.INV-ACCT-CDE ( #I2 )
                                DbsUtil.callnat(Iaan021x.class , getCurrentProcessState(), pnd_Fund_Cde, pnd_Tckr);                                                       //Natural: CALLNAT 'IAAN021X' USING #FUND-CDE #TCKR
                                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                                if (condition(pnd_Tckr.equals(" ")))                                                                                                      //Natural: IF #TCKR = ' '
                                {
                                    cps_Fund_Out_Inv_Acct_Ticker.reset();                                                                                                 //Natural: RESET CPS-FUND-OUT.INV-ACCT-TICKER
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    cps_Fund_Out_Inv_Acct_Ticker.setValue(pnd_Tckr);                                                                                      //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-TICKER := #TCKR
                                }                                                                                                                                         //Natural: END-IF
                                cps_Fund_Out_Pymnt_Reqst_Nbr.setValue(legacy_Eft_Key);                                                                                    //Natural: ASSIGN CPS-FUND-OUT.PYMNT-REQST-NBR := LEGACY-EFT-KEY
                                cps_Fund_Out_Pymnt_Instlmnt_Nbr.setValue(payment_Pymnt_Instlmnt_Nbr);                                                                     //Natural: ASSIGN CPS-FUND-OUT.PYMNT-INSTLMNT-NBR := PAYMENT.PYMNT-INSTLMNT-NBR
                                cps_Fund_Out_Inv_Acct_Dvdnd_Amt.setValue(payment_Inv_Acct_Dvdnd_Amt.getValue(pnd_I2));                                                    //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-DVDND-AMT := PAYMENT.INV-ACCT-DVDND-AMT ( #I2 )
                                cps_Fund_Out_Inv_Acct_Unit_Value.setValue(payment_Inv_Acct_Unit_Value.getValue(pnd_I2));                                                  //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-UNIT-VALUE := PAYMENT.INV-ACCT-UNIT-VALUE ( #I2 )
                                cps_Fund_Out_Inv_Acct_Unit_Qty.setValue(payment_Inv_Acct_Unit_Qty.getValue(pnd_I2));                                                      //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-UNIT-QTY := PAYMENT.INV-ACCT-UNIT-QTY ( #I2 )
                                cps_Fund_Out_Inv_Acct_Settl_Amt.setValue(payment_Inv_Acct_Settl_Amt.getValue(pnd_I2));                                                    //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-SETTL-AMT := PAYMENT.INV-ACCT-SETTL-AMT ( #I2 )
                                cps_Fund_Out_Inv_Acct_Exp_Amt.setValue(payment_Inv_Acct_Exp_Amt.getValue(pnd_I2));                                                        //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-EXP-AMT := PAYMENT.INV-ACCT-EXP-AMT ( #I2 )
                                cps_Fund_Out_Inv_Acct_Ivc_Amt.setValue(payment_Inv_Acct_Ivc_Amt.getValue(pnd_I2));                                                        //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-IVC-AMT := PAYMENT.INV-ACCT-IVC-AMT ( #I2 )
                                cps_Fund_Out_Inv_Acct_Start_Accum_Amt.setValue(payment_Inv_Acct_Start_Accum_Amt.getValue(pnd_I2));                                        //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-START-ACCUM-AMT := PAYMENT.INV-ACCT-START-ACCUM-AMT ( #I2 )
                                cps_Fund_Out_Inv_Acct_End_Accum_Amt.setValue(payment_Inv_Acct_End_Accum_Amt.getValue(pnd_I2));                                            //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-END-ACCUM-AMT := PAYMENT.INV-ACCT-END-ACCUM-AMT ( #I2 )
                                cps_Fund_Out_Inv_Acct_Ivc_Ind.setValue(payment_Inv_Acct_Ivc_Ind.getValue(pnd_I2));                                                        //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-IVC-IND := PAYMENT.INV-ACCT-IVC-IND ( #I2 )
                                cps_Fund_Out_Inv_Acct_Adj_Ivc_Amt.setValue(payment_Inv_Acct_Adj_Ivc_Amt.getValue(pnd_I2));                                                //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-ADJ-IVC-AMT := PAYMENT.INV-ACCT-ADJ-IVC-AMT ( #I2 )
                                cps_Fund_Out_Inv_Acct_Valuat_Period.setValue(payment_Inv_Acct_Valuat_Period.getValue(pnd_I2));                                            //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-VALUAT-PERIOD := PAYMENT.INV-ACCT-VALUAT-PERIOD ( #I2 )
                                cps_Fund_Out_Inv_Acct_Fed_Cde.setValue(payment_Inv_Acct_Fed_Cde.getValue(pnd_I2));                                                        //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-FED-CDE := PAYMENT.INV-ACCT-FED-CDE ( #I2 )
                                cps_Fund_Out_Inv_Acct_State_Cde.setValue(payment_Inv_Acct_State_Cde.getValue(pnd_I2));                                                    //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-STATE-CDE := PAYMENT.INV-ACCT-STATE-CDE ( #I2 )
                                cps_Fund_Out_Inv_Acct_Local_Cde.setValue(payment_Inv_Acct_Local_Cde.getValue(pnd_I2));                                                    //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-LOCAL-CDE := PAYMENT.INV-ACCT-LOCAL-CDE ( #I2 )
                                cps_Fund_Out_Inv_Acct_Cde.setValue(payment_Inv_Acct_Cde.getValue(pnd_I2));                                                                //Natural: ASSIGN CPS-FUND-OUT.INV-ACCT-CDE := PAYMENT.INV-ACCT-CDE ( #I2 )
                                getWorkFiles().write(2, false, cps_Fund_Out);                                                                                             //Natural: WRITE WORK FILE 2 CPS-FUND-OUT
                            }                                                                                                                                             //Natural: END-FOR
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("RD1"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        //*  FE201509 END
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (condition(vw_payment.getAtEndOfData()))
                {
                }                                                                                                                                                         //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //*  FE201509
                if (condition(pnd_Write_Count.greater(getZero()) && pnd_Prior_Date_Incr.equals(getZero())))                                                               //Natural: IF #WRITE-COUNT > 0 AND #PRIOR-DATE-INCR = 0
                {
                                                                                                                                                                          //Natural: PERFORM UPDATE-INVERSE-DATE
                    sub_Update_Inverse_Date();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                payment_Out_Payment_Out_Data.reset();                                                                                                                     //Natural: RESET PAYMENT-OUT-DATA
                payment_Out_Trailer_Ind_Out.setValue("TRAILER");                                                                                                          //Natural: ASSIGN PAYMENT-OUT.TRAILER-IND-OUT := 'TRAILER'
                payment_Out_Trailer_Date_Out.setValue(pnd_Save_Current_Date);                                                                                             //Natural: ASSIGN PAYMENT-OUT.TRAILER-DATE-OUT := #SAVE-CURRENT-DATE
                payment_Out_Trailer_Count_Out.setValue(pnd_Write_Count);                                                                                                  //Natural: ASSIGN PAYMENT-OUT.TRAILER-COUNT-OUT := #WRITE-COUNT
                getWorkFiles().write(1, false, payment_Out);                                                                                                              //Natural: WRITE WORK FILE 1 PAYMENT-OUT
                getReports().write(2, "WRITE COUNT PAYMENT(Insert)",new TabSetting(35),pnd_Write_Count, new ReportEditMask ("ZZ,ZZZ,Z99"));                               //Natural: WRITE ( 2 ) 'WRITE COUNT PAYMENT(Insert)' 35T #WRITE-COUNT ( EM = ZZ,ZZZ,Z99 )
                if (Global.isEscape()) return;
                getReports().write(2, "WRITE COUNT CPS-FUND(Insert)",new TabSetting(35),pnd_Read_Count, new ReportEditMask ("ZZ,ZZZ,Z99"));                               //Natural: WRITE ( 2 ) 'WRITE COUNT CPS-FUND(Insert)' 35T #READ-COUNT ( EM = ZZ,ZZZ,Z99 )
                if (Global.isEscape()) return;
                cps_Fund_Out.reset();                                                                                                                                     //Natural: RESET CPS-FUND-OUT
                RW3:                                                                                                                                                      //Natural: READ WORK FILE 3 CPS-FUND-OUT3
                while (condition(getWorkFiles().read(3, cps_Fund_Out3)))
                {
                    cps_Fund_Out_Cps_Out_Data.setValue(cps_Fund_Out3_Cps_Out_Data3);                                                                                      //Natural: AT END OF DATA;//Natural: MOVE CPS-OUT-DATA3 TO CPS-OUT-DATA
                    pnd_Rec_Type.setValue(cps_Fund_Out_Cps_Out_Data.getSubstring(1,7));                                                                                   //Natural: MOVE SUBSTR ( CPS-OUT-DATA,1,7 ) TO #REC-TYPE
                    if (condition(pnd_Rec_Type.equals("HEADER ")))                                                                                                        //Natural: IF #REC-TYPE EQ 'HEADER '
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Rec_Type.equals("TRAILER")))                                                                                                        //Natural: IF #REC-TYPE EQ 'TRAILER'
                    {
                        pnd_Write_Cnt3.setValue(cps_Fund_Out_Trailer_Count_Out);                                                                                          //Natural: MOVE CPS-FUND-OUT.TRAILER-COUNT-OUT TO #WRITE-CNT3
                        getReports().write(2, "WRITE COUNT CPS-FUND(Update)",new TabSetting(35),pnd_Write_Cnt3, new ReportEditMask ("ZZ,ZZZ,Z99"));                       //Natural: WRITE ( 2 ) 'WRITE COUNT CPS-FUND(Update)' 35T #WRITE-CNT3 ( EM = ZZ,ZZZ,Z99 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RW3"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RW3"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Read_Count.nadd(1);                                                                                                                               //Natural: ADD 1 TO #READ-COUNT
                    getWorkFiles().write(2, false, cps_Fund_Out);                                                                                                         //Natural: WRITE WORK FILE 2 CPS-FUND-OUT
                }                                                                                                                                                         //Natural: END-WORK
                RW3_Exit:
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                }                                                                                                                                                         //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
                if (Global.isEscape()) return;
                cps_Fund_Out_Trailer_Ind_Out.setValue("TRAILER");                                                                                                         //Natural: ASSIGN CPS-FUND-OUT.TRAILER-IND-OUT := 'TRAILER'
                cps_Fund_Out_Trailer_Date_Out.compute(new ComputeParameters(false, cps_Fund_Out_Trailer_Date_Out), Global.getDATN().subtract(1));                         //Natural: ASSIGN CPS-FUND-OUT.TRAILER-DATE-OUT := *DATN - 1
                cps_Fund_Out_Trailer_Count_Out.setValue(pnd_Read_Count);                                                                                                  //Natural: ASSIGN CPS-FUND-OUT.TRAILER-COUNT-OUT := #READ-COUNT
                getWorkFiles().write(2, false, cps_Fund_Out);                                                                                                             //Natural: WRITE WORK FILE 2 CPS-FUND-OUT
                getReports().write(2, "WRITE COUNT CPS-FUND(TOTAL)",new TabSetting(35),pnd_Read_Count, new ReportEditMask ("ZZ,ZZZ,Z99"));                                //Natural: WRITE ( 2 ) 'WRITE COUNT CPS-FUND(TOTAL)' 35T #READ-COUNT ( EM = ZZ,ZZZ,Z99 )
                if (Global.isEscape()) return;
                //*  SET PAYMENT-OUT-DATA-2  /* FE201311 COMMENT OUT UPDATED RECORDS START
                //*  HEADER-IND-OUT-2  := 'HEADER ' /* WILL BE EXTRACTED IN PCP1050
                //*  HEADER-DATE-OUT-2 := #SAVE-CURRENT-DATE
                //*  WRITE WORK FILE 2 PAYMENT-OUT-2
                //*  READ PAYMENT-2 IN PHYSICAL SEQUENCE
                //*    AT END OF DATA
                //*      ESCAPE BOTTOM
                //*   END-ENDDATA
                //*   ADD 1 TO #WRITE-COUNT-2
                //*   RESET PAYMENT-OUT-DATA-2
                //*   MOVE BY NAME PAYMENT-2 TO PAYMENT-OUT-2
                //*   WRITE WORK FILE 2 PAYMENT-OUT-2
                //*  END-READ
                //*  RESET PAYMENT-OUT-DATA-2
                //*  TRAILER-IND-OUT-2   := 'TRAILER'
                //*  TRAILER-DATE-OUT-2  := #SAVE-CURRENT-DATE
                //*  TRAILER-COUNT-OUT-2 := #WRITE-COUNT-2
                //*  WRITE WORK FILE 2 PAYMENT-OUT-2
                //* *                                    /* FE201311 COMMENT OUT END
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-INVERSE-DATES
                //*  SET CURRENT DATE TO CURRENT DAY - 1 DAY SINCE JOB RUNS AFTER MIDNIGHT.
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-INVERSE-DATE
                //* *************
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DATE-EDIT-ROUTINE
                //* *************
                //*  ------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-ROUTINE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Inverse_Dates() throws Exception                                                                                                                 //Natural: GET-INVERSE-DATES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Invrse_Found.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #INVRSE-FOUND #CURRENT-CNTRCT-INVRSE-DTE #PRIOR-CNTRCT-INVRSE-DTE
        pnd_Current_Cntrct_Invrse_Dte.resetInitial();
        pnd_Prior_Cntrct_Invrse_Dte.resetInitial();
        pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Key.setValue(constants_Pnd_Rmwb0010);                                                                                             //Natural: MOVE #RMWB0010 TO #ST-TBL-KEY
        pnd_Current_Cntrct_Invrse_Dte.compute(new ComputeParameters(false, pnd_Current_Cntrct_Invrse_Dte), (DbsField.subtract(100000000,Global.getDATN())).add(1));       //Natural: ASSIGN #CURRENT-CNTRCT-INVRSE-DTE := ( 100000000 - *DATN ) + 1
        pnd_Save_Current_Date.setValue(pnd_Current_Cntrct_Invrse_Dte);                                                                                                    //Natural: ASSIGN #SAVE-CURRENT-DATE := #CURRENT-CNTRCT-INVRSE-DTE
        vw_cwf_Corp_Rpt_Start_Tbl.startDatabaseRead                                                                                                                       //Natural: READ CWF-CORP-RPT-START-TBL BY TBL-PRIME-KEY STARTING FROM #ST-TBL-PRIME-KEY
        (
        "READ_TBL",
        new Wc[] { new Wc("TBL_PRIME_KEY", ">=", pnd_St_Tbl_Prime_Key, WcType.BY) },
        new Oc[] { new Oc("TBL_PRIME_KEY", "ASC") }
        );
        READ_TBL:
        while (condition(vw_cwf_Corp_Rpt_Start_Tbl.readNextRow("READ_TBL")))
        {
            if (condition(cwf_Corp_Rpt_Start_Tbl_Tbl_Table_Nme.notEquals(pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Nme) || cwf_Corp_Rpt_Start_Tbl_Dte_Key.notEquals(pnd_St_Tbl_Prime_Key_Pnd_St_Tbl_Key))) //Natural: IF CWF-CORP-RPT-START-TBL.TBL-TABLE-NME NE #ST-TBL-NME OR CWF-CORP-RPT-START-TBL.DTE-KEY NE #ST-TBL-KEY
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn.setValue(vw_cwf_Corp_Rpt_Start_Tbl.getAstISN("READ_TBL"));                                                                                            //Natural: ASSIGN #ISN := *ISN ( READ-TBL. )
            pnd_Invrse_Found.setValue(true);                                                                                                                              //Natural: ASSIGN #INVRSE-FOUND := TRUE
            pnd_Prior_Cntrct_Invrse_Dte.setValue(cwf_Corp_Rpt_Start_Tbl_Invrse_Dte);                                                                                      //Natural: ASSIGN #PRIOR-CNTRCT-INVRSE-DTE := CWF-CORP-RPT-START-TBL.INVRSE-DTE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(! (pnd_Invrse_Found.getBoolean())))                                                                                                                 //Natural: IF NOT #INVRSE-FOUND
        {
            pnd_Error_Msg.setValue("No Inverse Date on CWF-CORP-RPT-START-TBL  Record Key 'RMWB0010'");                                                                   //Natural: ASSIGN #ERROR-MSG := 'No Inverse Date on CWF-CORP-RPT-START-TBL  Record Key "RMWB0010"'
                                                                                                                                                                          //Natural: PERFORM ERROR-ROUTINE
            sub_Error_Routine();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(35);  if (true) return;                                                                                                                     //Natural: TERMINATE 35
        }                                                                                                                                                                 //Natural: END-IF
        //* *************
        //*  GET-INVERSE-DATES
    }
    private void sub_Update_Inverse_Date() throws Exception                                                                                                               //Natural: UPDATE-INVERSE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        GET_TBL:                                                                                                                                                          //Natural: GET CWF-CORP-RPT-START-TBL #ISN
        vw_cwf_Corp_Rpt_Start_Tbl.readByID(pnd_Isn.getLong(), "GET_TBL");
        cwf_Corp_Rpt_Start_Tbl_Invrse_Dte.setValue(pnd_Save_Cntrct_Invrse_Dte);                                                                                           //Natural: ASSIGN CWF-CORP-RPT-START-TBL.INVRSE-DTE := #SAVE-CNTRCT-INVRSE-DTE
        vw_cwf_Corp_Rpt_Start_Tbl.updateDBRow("GET_TBL");                                                                                                                 //Natural: UPDATE ( GET-TBL. )
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* *************
        //*  UPDATE-INVERSE-DATE
    }
    private void sub_Date_Edit_Routine() throws Exception                                                                                                                 //Natural: DATE-EDIT-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        payment_Out_Pymnt_Check_Dte.setValueEdited(payment_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED PAYMENT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO PAYMENT-OUT.PYMNT-CHECK-DTE
        payment_Out_Pymnt_Settlmnt_Dte.setValueEdited(payment_Pymnt_Settlmnt_Dte,new ReportEditMask("YYYYMMDD"));                                                         //Natural: MOVE EDITED PAYMENT.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO PAYMENT-OUT.PYMNT-SETTLMNT-DTE
        payment_Out_Pymnt_Acctg_Dte.setValueEdited(payment_Pymnt_Acctg_Dte,new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED PAYMENT.PYMNT-ACCTG-DTE ( EM = YYYYMMDD ) TO PAYMENT-OUT.PYMNT-ACCTG-DTE
        payment_Out_Cnr_Cs_Rqust_Dte.setValueEdited(payment_Cnr_Cs_Rqust_Dte,new ReportEditMask("YYYYMMDD"));                                                             //Natural: MOVE EDITED PAYMENT.CNR-CS-RQUST-DTE ( EM = YYYYMMDD ) TO PAYMENT-OUT.CNR-CS-RQUST-DTE
        //*  THIS DATE WILL BE SENT TO THE REFERENCE TABLE TO BE
        //*  USED AS THE PRIOR INVERSE DATE ON THE NEXT RUN.
        pnd_Save_Cntrct_Invrse_Dte.setValue(payment_Cntrct_Invrse_Dte);                                                                                                   //Natural: MOVE PAYMENT.CNTRCT-INVRSE-DTE TO #SAVE-CNTRCT-INVRSE-DTE
        //* *************
        //*  DATE-EDIT-ROUTINE
    }
    private void sub_Error_Routine() throws Exception                                                                                                                     //Natural: ERROR-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------
        pdaErla1000.getErla1000().reset();                                                                                                                                //Natural: RESET ERLA1000
        pdaErla1000.getErla1000_Err_Pgm_Name().setValue(Global.getPROGRAM());                                                                                             //Natural: ASSIGN ERLA1000.ERR-PGM-NAME = *PROGRAM
        pdaErla1000.getErla1000_Err_Pgm_Level().setValue(Global.getLEVEL());                                                                                              //Natural: ASSIGN ERLA1000.ERR-PGM-LEVEL = *LEVEL
        pdaErla1000.getErla1000_Err_Nbr().setValue(Global.getERROR_NR());                                                                                                 //Natural: ASSIGN ERLA1000.ERR-NBR = *ERROR-NR
        pdaErla1000.getErla1000_Err_Line_Nbr().setValue(Global.getERROR_LINE());                                                                                          //Natural: ASSIGN ERLA1000.ERR-LINE-NBR = *ERROR-LINE
        pdaErla1000.getErla1000_Err_Status_Cde().setValue("U");                                                                                                           //Natural: ASSIGN ERLA1000.ERR-STATUS-CDE = 'U'
        pdaErla1000.getErla1000_Err_Type_Cde().setValue("N");                                                                                                             //Natural: ASSIGN ERLA1000.ERR-TYPE-CDE = 'N'
        pdaErla1000.getErla1000_Err_Notes().setValue(pdaInta040.getMsg_Info_Sub_Pnd_Pnd_Msg_Txt());                                                                       //Natural: ASSIGN ERLA1000.ERR-NOTES = ##MSG-TXT
        pdaInta040.getMsg_Info_Sub_Pnd_Pnd_Msg_Pgm_Nme().setValue(Global.getPROGRAM());                                                                                   //Natural: ASSIGN MSG-INFO-SUB.##MSG-PGM-NME := *PROGRAM
        DbsUtil.callnat(Erln1000.class , getCurrentProcessState(), pdaErla1000.getErla1000());                                                                            //Natural: CALLNAT 'ERLN1000' ERLA1000
        if (condition(Global.isEscape())) return;
        getReports().write(0, pnd_Error_Msg);                                                                                                                             //Natural: WRITE #ERROR-MSG
        if (Global.isEscape()) return;
        pnd_Usr0020n_Parm.reset();                                                                                                                                        //Natural: RESET #USR0020N-PARM
    }

    //
}
