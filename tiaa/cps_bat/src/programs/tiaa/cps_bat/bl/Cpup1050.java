/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:24:20 PM
**        * FROM NATURAL PROGRAM : Cpup1050
************************************************************
**        * FILE NAME            : Cpup1050.java
**        * CLASS NAME           : Cpup1050
**        * INSTANCE NAME        : Cpup1050
************************************************************
**--------------------------------------------------------------------**
** PROGRAM    :  CPUO1050                                             **
** APPLICATION:  CONSOLIDATED PAYMENT SYSTEM                          **
**            :  INVESTMENT SOLUTIONS - RETIREE INCOME WEB (DETAIL)   **
** DATE       :  2011/05/12                                           **
** DESCRIPTION:  GET 2 YEARS WORTH OF INFORMATION FOR THE MONTHLY RUN **
**            :  AND PREVIOUS DAY  DATA ON A DAILY RUN                **
** AUTHOR     :  R.CARREON                                            **
**            :                                                       **
**            :                                                       **
** HISTORY    :                                                       **
*  07/05/13 CTS -CHANGE THE DEFINITION OF THE INPUT PARM IN           **
*                ORDER TO ACCOMODATE A TO/FROM DATE RANGE IN ADDITION **
*                TO THE EXISTING ALLOWABLE PARM VALUES.  ACCEPTABLE   **
*                PARM VALUES AND THE JOBS THAT USE THEM ARE:          **
*                'DAILY' - USED IN PCP1050D DAILY RUN                 **
*                'YYYYMMDD' - USED IN PCP1050D FOR A SPECIFIC DATE    **
*                'WEEKLY' - USED IN PCP1050W WEEKLY RUN               **
*                'YYYYMMDDYYYYMMDD' - USED IN PCP1050R FOR A SPECIFIC **
*                                     FROM/THRU PERIOD                **
*                CHANGE TAG IS CTS1                                   **
*  04/02/2014 CTS - PRB63004 - REMOVED WEEKLY RUN LOGIC FOR PCP1050W  **
*                CHANGE TAG IS --->  CTS2                             **
*  12/2014       SET START TO 3 DAYS BEFORE DAILY RUN DATE INSTEAD    **
*                OF JUST 1 DAY BEFORE -- TAG JWO                      **
*  03/2017 J. OSTEEN - PIN EXPANSION TO 12 BYTES  --  TAG JWO1        **
*  06/2017 SAI K     - PIN EXPANSION                                  **
*  01/15/2020  CTS - CPS SUNSET (CHG694525) TAG: CTS-0115             **
*                                                                     **
**--------------------------------------------------------------------**

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpup1050 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_pay;
    private DbsField pay_Cntrct_Orgn_Cde;
    private DbsField pay_Pymnt_Stats_Cde;
    private DbsField pay_Cntrct_Invrse_Dte;
    private DbsField pay_Cntrct_Ppcn_Nbr;
    private DbsField pay_Pymnt_Prcss_Seq_Nbr;
    private DbsField pay_Pymnt_Rprnt_Pndng_Ind;
    private DbsField pay_Invrse_Process_Dte;
    private DbsField pay_Cntrct_Unq_Id_Nbr;
    private DbsField pay_Annt_Soc_Sec_Nbr;
    private DbsField pay_Pymnt_Reqst_Nbr;
    private DbsField pay_Pymnt_Instlmnt_Nbr;
    private DbsField pay_Cntrct_Cmbn_Nbr;
    private DbsField pay_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_W_Date_X;
    private DbsField pnd_W_Datx;

    private DbsGroup pnd_W_Datx__R_Field_1;
    private DbsField pnd_W_Datx_Pnd_W_Datn;
    private DbsField pnd_Date;
    private DbsField pnd_Date_Start;

    private DbsGroup pnd_Date_Start__R_Field_2;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Ccyy;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Mm;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Dd;

    private DbsGroup pnd_Date_Start__R_Field_3;
    private DbsField pnd_Date_Start_Pnd_Date_Start_A;
    private DbsField pnd_Date_Start_D;
    private DbsField pnd_Date_End;

    private DbsGroup pnd_Date_End__R_Field_4;
    private DbsField pnd_Date_End_Pnd_Date_End_Ccyy;
    private DbsField pnd_Date_End_Pnd_Date_End_Mm;
    private DbsField pnd_Date_End_Pnd_Date_End_Dd;

    private DbsGroup pnd_Date_End__R_Field_5;
    private DbsField pnd_Date_End_Pnd_Date_End_A;
    private DbsField pnd_Start_Invrse_Date;
    private DbsField pnd_End_Invrse_Date;
    private DbsField pnd_Rec_Process;
    private DbsField pnd_Orgn_Status_Invrse_Seq;

    private DbsGroup pnd_Orgn_Status_Invrse_Seq__R_Field_6;
    private DbsField pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde;
    private DbsField pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde;
    private DbsField pnd_Orgn_Status_Invrse_Seq_Cntrct_Invrse_Dte;

    private DbsGroup pnd_Orgn_Table;
    private DbsField pnd_Orgn_Table_Pnd_Orgn1;
    private DbsField pnd_Orgn_Table_Pnd_Orgn2;
    private DbsField pnd_Orgn_Table_Pnd_Orgn3;
    private DbsField pnd_Orgn_Table_Pnd_Orgn4;
    private DbsField pnd_Orgn_Table_Pnd_Orgn5;
    private DbsField pnd_Orgn_Table_Pnd_Orgn6;
    private DbsField pnd_Orgn_Table_Pnd_Orgn7;
    private DbsField pnd_Orgn_Table_Pnd_Orgn8;
    private DbsField pnd_Orgn_Table_Pnd_Orgn9;
    private DbsField pnd_Orgn_Table_Pnd_Orgn10;
    private DbsField pnd_Orgn_Table_Pnd_Orgn11;
    private DbsField pnd_Orgn_Table_Pnd_Orgn12;
    private DbsField pnd_Orgn_Table_Pnd_Orgn13;
    private DbsField pnd_Orgn_Table_Pnd_Orgn14;
    private DbsField pnd_Orgn_Table_Pnd_Orgn15;

    private DbsGroup pnd_Orgn_Table__R_Field_7;
    private DbsField pnd_Orgn_Table_Pnd_Orgn_Array;
    private DbsField pnd_Stat_Array;
    private DbsField pnd_R;
    private DbsField pnd_A;
    private DbsField pnd_New_Parm;

    private DbsGroup pnd_New_Parm__R_Field_8;
    private DbsField pnd_New_Parm_Pnd_Parm;

    private DbsGroup pnd_New_Parm__R_Field_9;
    private DbsField pnd_New_Parm_Pnd_Start_Date;
    private DbsField pnd_New_Parm_Pnd_End_Date;
    private DbsField quo;

    private DbsGroup quo__R_Field_10;
    private DbsField quo_Quo1;
    private DbsField quo_Rem;
    private DbsField pnd_Pin;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_pay = new DataAccessProgramView(new NameInfo("vw_pay", "PAY"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        pay_Cntrct_Orgn_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        pay_Pymnt_Stats_Cde = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        pay_Cntrct_Invrse_Dte = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        pay_Cntrct_Ppcn_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        pay_Pymnt_Prcss_Seq_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_PRCSS_SEQ_NBR");
        pay_Pymnt_Rprnt_Pndng_Ind = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Rprnt_Pndng_Ind", "PYMNT-RPRNT-PNDNG-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_RPRNT_PNDNG_IND");
        pay_Invrse_Process_Dte = vw_pay.getRecord().newFieldInGroup("pay_Invrse_Process_Dte", "INVRSE-PROCESS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "INVRSE_PROCESS_DTE");
        pay_Cntrct_Unq_Id_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_UNQ_ID_NBR");
        pay_Annt_Soc_Sec_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "ANNT_SOC_SEC_NBR");
        pay_Pymnt_Reqst_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        pay_Pymnt_Instlmnt_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "PYMNT_INSTLMNT_NBR");
        pay_Cntrct_Cmbn_Nbr = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        pay_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_pay.getRecord().newFieldInGroup("pay_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        registerRecord(vw_pay);

        pnd_W_Date_X = localVariables.newFieldInRecord("pnd_W_Date_X", "#W-DATE-X", FieldType.DATE);
        pnd_W_Datx = localVariables.newFieldInRecord("pnd_W_Datx", "#W-DATX", FieldType.STRING, 8);

        pnd_W_Datx__R_Field_1 = localVariables.newGroupInRecord("pnd_W_Datx__R_Field_1", "REDEFINE", pnd_W_Datx);
        pnd_W_Datx_Pnd_W_Datn = pnd_W_Datx__R_Field_1.newFieldInGroup("pnd_W_Datx_Pnd_W_Datn", "#W-DATN", FieldType.NUMERIC, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);
        pnd_Date_Start = localVariables.newFieldInRecord("pnd_Date_Start", "#DATE-START", FieldType.NUMERIC, 8);

        pnd_Date_Start__R_Field_2 = localVariables.newGroupInRecord("pnd_Date_Start__R_Field_2", "REDEFINE", pnd_Date_Start);
        pnd_Date_Start_Pnd_Date_Strt_Ccyy = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Ccyy", "#DATE-STRT-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Date_Start_Pnd_Date_Strt_Mm = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Mm", "#DATE-STRT-MM", FieldType.NUMERIC, 
            2);
        pnd_Date_Start_Pnd_Date_Strt_Dd = pnd_Date_Start__R_Field_2.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Dd", "#DATE-STRT-DD", FieldType.NUMERIC, 
            2);

        pnd_Date_Start__R_Field_3 = localVariables.newGroupInRecord("pnd_Date_Start__R_Field_3", "REDEFINE", pnd_Date_Start);
        pnd_Date_Start_Pnd_Date_Start_A = pnd_Date_Start__R_Field_3.newFieldInGroup("pnd_Date_Start_Pnd_Date_Start_A", "#DATE-START-A", FieldType.STRING, 
            8);
        pnd_Date_Start_D = localVariables.newFieldInRecord("pnd_Date_Start_D", "#DATE-START-D", FieldType.DATE);
        pnd_Date_End = localVariables.newFieldInRecord("pnd_Date_End", "#DATE-END", FieldType.NUMERIC, 8);

        pnd_Date_End__R_Field_4 = localVariables.newGroupInRecord("pnd_Date_End__R_Field_4", "REDEFINE", pnd_Date_End);
        pnd_Date_End_Pnd_Date_End_Ccyy = pnd_Date_End__R_Field_4.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Ccyy", "#DATE-END-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Date_End_Pnd_Date_End_Mm = pnd_Date_End__R_Field_4.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Mm", "#DATE-END-MM", FieldType.NUMERIC, 2);
        pnd_Date_End_Pnd_Date_End_Dd = pnd_Date_End__R_Field_4.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Dd", "#DATE-END-DD", FieldType.NUMERIC, 2);

        pnd_Date_End__R_Field_5 = localVariables.newGroupInRecord("pnd_Date_End__R_Field_5", "REDEFINE", pnd_Date_End);
        pnd_Date_End_Pnd_Date_End_A = pnd_Date_End__R_Field_5.newFieldInGroup("pnd_Date_End_Pnd_Date_End_A", "#DATE-END-A", FieldType.STRING, 8);
        pnd_Start_Invrse_Date = localVariables.newFieldInRecord("pnd_Start_Invrse_Date", "#START-INVRSE-DATE", FieldType.NUMERIC, 8);
        pnd_End_Invrse_Date = localVariables.newFieldInRecord("pnd_End_Invrse_Date", "#END-INVRSE-DATE", FieldType.NUMERIC, 8);
        pnd_Rec_Process = localVariables.newFieldInRecord("pnd_Rec_Process", "#REC-PROCESS", FieldType.NUMERIC, 8);
        pnd_Orgn_Status_Invrse_Seq = localVariables.newFieldInRecord("pnd_Orgn_Status_Invrse_Seq", "#ORGN-STATUS-INVRSE-SEQ", FieldType.STRING, 11);

        pnd_Orgn_Status_Invrse_Seq__R_Field_6 = localVariables.newGroupInRecord("pnd_Orgn_Status_Invrse_Seq__R_Field_6", "REDEFINE", pnd_Orgn_Status_Invrse_Seq);
        pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde = pnd_Orgn_Status_Invrse_Seq__R_Field_6.newFieldInGroup("pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde", 
            "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde = pnd_Orgn_Status_Invrse_Seq__R_Field_6.newFieldInGroup("pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde", 
            "PYMNT-STATS-CDE", FieldType.STRING, 1);
        pnd_Orgn_Status_Invrse_Seq_Cntrct_Invrse_Dte = pnd_Orgn_Status_Invrse_Seq__R_Field_6.newFieldInGroup("pnd_Orgn_Status_Invrse_Seq_Cntrct_Invrse_Dte", 
            "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);

        pnd_Orgn_Table = localVariables.newGroupInRecord("pnd_Orgn_Table", "#ORGN-TABLE");
        pnd_Orgn_Table_Pnd_Orgn1 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn1", "#ORGN1", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn2 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn2", "#ORGN2", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn3 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn3", "#ORGN3", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn4 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn4", "#ORGN4", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn5 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn5", "#ORGN5", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn6 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn6", "#ORGN6", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn7 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn7", "#ORGN7", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn8 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn8", "#ORGN8", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn9 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn9", "#ORGN9", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn10 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn10", "#ORGN10", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn11 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn11", "#ORGN11", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn12 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn12", "#ORGN12", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn13 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn13", "#ORGN13", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn14 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn14", "#ORGN14", FieldType.STRING, 2);
        pnd_Orgn_Table_Pnd_Orgn15 = pnd_Orgn_Table.newFieldInGroup("pnd_Orgn_Table_Pnd_Orgn15", "#ORGN15", FieldType.STRING, 2);

        pnd_Orgn_Table__R_Field_7 = localVariables.newGroupInRecord("pnd_Orgn_Table__R_Field_7", "REDEFINE", pnd_Orgn_Table);
        pnd_Orgn_Table_Pnd_Orgn_Array = pnd_Orgn_Table__R_Field_7.newFieldArrayInGroup("pnd_Orgn_Table_Pnd_Orgn_Array", "#ORGN-ARRAY", FieldType.STRING, 
            2, new DbsArrayController(1, 15));
        pnd_Stat_Array = localVariables.newFieldArrayInRecord("pnd_Stat_Array", "#STAT-ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.NUMERIC, 2);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 2);
        pnd_New_Parm = localVariables.newFieldInRecord("pnd_New_Parm", "#NEW-PARM", FieldType.STRING, 16);

        pnd_New_Parm__R_Field_8 = localVariables.newGroupInRecord("pnd_New_Parm__R_Field_8", "REDEFINE", pnd_New_Parm);
        pnd_New_Parm_Pnd_Parm = pnd_New_Parm__R_Field_8.newFieldInGroup("pnd_New_Parm_Pnd_Parm", "#PARM", FieldType.STRING, 10);

        pnd_New_Parm__R_Field_9 = localVariables.newGroupInRecord("pnd_New_Parm__R_Field_9", "REDEFINE", pnd_New_Parm);
        pnd_New_Parm_Pnd_Start_Date = pnd_New_Parm__R_Field_9.newFieldInGroup("pnd_New_Parm_Pnd_Start_Date", "#START-DATE", FieldType.NUMERIC, 8);
        pnd_New_Parm_Pnd_End_Date = pnd_New_Parm__R_Field_9.newFieldInGroup("pnd_New_Parm_Pnd_End_Date", "#END-DATE", FieldType.NUMERIC, 8);
        quo = localVariables.newFieldInRecord("quo", "QUO", FieldType.NUMERIC, 6, 2);

        quo__R_Field_10 = localVariables.newGroupInRecord("quo__R_Field_10", "REDEFINE", quo);
        quo_Quo1 = quo__R_Field_10.newFieldInGroup("quo_Quo1", "QUO1", FieldType.NUMERIC, 4);
        quo_Rem = quo__R_Field_10.newFieldInGroup("quo_Rem", "REM", FieldType.NUMERIC, 2);
        pnd_Pin = localVariables.newFieldInRecord("pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_pay.reset();

        localVariables.reset();
        pnd_Orgn_Table_Pnd_Orgn1.setInitialValue("AP");
        pnd_Orgn_Table_Pnd_Orgn2.setInitialValue("OP");
        pnd_Orgn_Table_Pnd_Orgn3.setInitialValue("IR");
        pnd_Orgn_Table_Pnd_Orgn4.setInitialValue("AD");
        pnd_Orgn_Table_Pnd_Orgn5.setInitialValue("SS");
        pnd_Orgn_Table_Pnd_Orgn6.setInitialValue("SI");
        pnd_Orgn_Table_Pnd_Orgn7.setInitialValue("AL");
        pnd_Orgn_Table_Pnd_Orgn8.setInitialValue("DC");
        pnd_Orgn_Table_Pnd_Orgn9.setInitialValue("VT");
        pnd_Orgn_Table_Pnd_Orgn10.setInitialValue("MS");
        pnd_Orgn_Table_Pnd_Orgn11.setInitialValue("DS");
        pnd_Orgn_Table_Pnd_Orgn12.setInitialValue("IA");
        pnd_Orgn_Table_Pnd_Orgn13.setInitialValue("NZ");
        pnd_Orgn_Table_Pnd_Orgn14.setInitialValue("EW");
        pnd_Orgn_Table_Pnd_Orgn15.setInitialValue("NV");
        pnd_Stat_Array.getValue(1).setInitialValue("D");
        pnd_Stat_Array.getValue(2).setInitialValue("C");
        pnd_Stat_Array.getValue(3).setInitialValue("P");
        pnd_Stat_Array.getValue(4).setInitialValue("W");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cpup1050() throws Exception
    {
        super("Cpup1050");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Cpup1050|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  CTS1 STARTS
                //* **********************************************************************
                //*  INPUT #PARM
                //*  MOVE *DATN TO #DATE-START
                //*   #DATE-END
                //*  QUO := #DATE-END-CCYY / 4
                //*  #START-INVRSE-DATE   := 100000000 - #DATE-START
                //*                                         TEST ONLY  -  RCC
                //* *IF *LIBRARY-ID = SCAN 'PROJ'
                //*   IF #PARM = 'MONTHLY'
                //*     #W-DATN := 20110601
                //*   END-IF
                //*   IF #PARM = 'DAILY'
                //*     #W-DATN := 20110402
                //*   END-IF
                //*   MOVE #W-DATN TO #DATE-START
                //*     #DATE-END
                //* *END-IF
                //*  IF #DATE-END-DD = 1
                //*   PERFORM GET-START-END-DATE
                //*   #DATE-STRT-MM     := #DATE-END-MM
                //*   #DATE-STRT-DD     := #DATE-END-DD
                //*  END-IF
                //*  #DATE-END-CCYY       := #DATE-END-CCYY - 2
                //*  IF #PARM = 'DAILY'
                //*   #DATE-START-D := *DATX
                //*   MOVE EDITED #DATE-START-D (EM=YYYYMMDD) TO #DATE-START-A
                //*   #DATE-END := #DATE-START
                //*  END-IF
                //*  IF #PARM = MASK(YYYYMMDD)
                //*   #DATE-START   := #PARM-N
                //*   #DATE-END := #DATE-START
                //*  END-IF
                //* * #DATE-START := 20121126
                //* * #DATE-END := 20121122
                //* ***********************************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_New_Parm);                                                                                         //Natural: INPUT #NEW-PARM
                //*  USED IN PCP1050D
                short decideConditionsMet163 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #PARM = 'DAILY'
                if (condition(pnd_New_Parm_Pnd_Parm.equals("DAILY")))
                {
                    decideConditionsMet163++;
                    pnd_Date_Start_D.setValue(Global.getDATX());                                                                                                          //Natural: ASSIGN #DATE-START-D := *DATX
                    pnd_Date_Start_Pnd_Date_Start_A.setValueEdited(pnd_Date_Start_D,new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #DATE-START-D ( EM = YYYYMMDD ) TO #DATE-START-A
                    pnd_Date_End.setValue(pnd_Date_Start);                                                                                                                //Natural: ASSIGN #DATE-END := #DATE-START
                    //*  WHEN #PARM = 'WEEKLY'
                    //*    #DATE-START := #DATE-END := *DATN
                    //*    QUO := #DATE-END-CCYY / 4
                    //*    IF #DATE-END-DD = 1
                    //*      PERFORM GET-START-END-DATE
                    //*      #DATE-STRT-MM     := #DATE-END-MM
                    //*      #DATE-STRT-DD     := #DATE-END-DD
                    //*    END-IF
                    //*    #DATE-END-CCYY       := #DATE-END-CCYY - 2
                }                                                                                                                                                         //Natural: WHEN #PARM = ' '
                else if (condition(pnd_New_Parm_Pnd_Parm.equals(" ")))
                {
                    decideConditionsMet163++;
                    getReports().write(0, Global.getPROGRAM(),"ABENDING due to missing parm value",NEWLINE);                                                              //Natural: WRITE *PROGRAM 'ABENDING due to missing parm value' /
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    //*  FROM DATE OR FROM/THRU DATES ARE USED IN PCP1050R
                    //*  SINGLE OR FROM DATE
                    if (condition(DbsUtil.maskMatches(pnd_New_Parm_Pnd_Start_Date,"YYYYMMDD")))                                                                           //Natural: IF #START-DATE = MASK ( YYYYMMDD )
                    {
                        pnd_Date_Start.setValue(pnd_New_Parm_Pnd_Start_Date);                                                                                             //Natural: ASSIGN #DATE-START := #START-DATE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, Global.getPROGRAM(),"ABENDING due to invalid input date parm",NEWLINE,"Start date is required in YYYYMMDD format");         //Natural: WRITE *PROGRAM 'ABENDING due to invalid input date parm' / 'Start date is required in YYYYMMDD format'
                        if (Global.isEscape()) return;
                        DbsUtil.terminate(99);  if (true) return;                                                                                                         //Natural: TERMINATE 99
                    }                                                                                                                                                     //Natural: END-IF
                    //*  SINGLE DATE ONLY
                    if (condition(pnd_New_Parm_Pnd_End_Date.equals(getZero())))                                                                                           //Natural: IF #END-DATE = 0
                    {
                        pnd_Date_End.setValue(pnd_New_Parm_Pnd_Start_Date);                                                                                               //Natural: ASSIGN #DATE-END := #START-DATE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  THRU DATE ONLY
                        if (condition(DbsUtil.maskMatches(pnd_New_Parm_Pnd_End_Date,"YYYYMMDD")))                                                                         //Natural: IF #END-DATE = MASK ( YYYYMMDD )
                        {
                            pnd_Date_End.setValue(pnd_New_Parm_Pnd_End_Date);                                                                                             //Natural: ASSIGN #DATE-END := #END-DATE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            getReports().write(0, Global.getPROGRAM(),"ABENDING due to invalid input date parm",NEWLINE,"End date is required in YYYYMMDD format");       //Natural: WRITE *PROGRAM 'ABENDING due to invalid input date parm' / 'End date is required in YYYYMMDD format'
                            if (Global.isEscape()) return;
                            DbsUtil.terminate(99);  if (true) return;                                                                                                     //Natural: TERMINATE 99
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  CTS1 ENDS
                                                                                                                                                                          //Natural: PERFORM FUTURE-DATED
                sub_Future_Dated();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Start_Invrse_Date.compute(new ComputeParameters(false, pnd_Start_Invrse_Date), DbsField.subtract(100000000,pnd_Date_Start));                          //Natural: ASSIGN #START-INVRSE-DATE := 100000000 - #DATE-START
                pnd_End_Invrse_Date.compute(new ComputeParameters(false, pnd_End_Invrse_Date), DbsField.subtract(100000000,pnd_Date_End));                                //Natural: ASSIGN #END-INVRSE-DATE := 100000000 - #DATE-END
                pnd_Orgn_Status_Invrse_Seq_Cntrct_Invrse_Dte.setValue(pnd_Start_Invrse_Date);                                                                             //Natural: ASSIGN #ORGN-STATUS-INVRSE-SEQ.CNTRCT-INVRSE-DTE := #START-INVRSE-DATE
                FOR01:                                                                                                                                                    //Natural: FOR #R 1 15
                for (pnd_R.setValue(1); condition(pnd_R.lessOrEqual(15)); pnd_R.nadd(1))
                {
                    if (condition(pnd_Orgn_Table_Pnd_Orgn_Array.getValue(pnd_R).equals(" ")))                                                                             //Natural: IF #ORGN-ARRAY ( #R ) = ' '
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Orgn_Status_Invrse_Seq_Cntrct_Orgn_Cde.setValue(pnd_Orgn_Table_Pnd_Orgn_Array.getValue(pnd_R));                                                   //Natural: MOVE #ORGN-ARRAY ( #R ) TO #ORGN-STATUS-INVRSE-SEQ.CNTRCT-ORGN-CDE
                    FOR02:                                                                                                                                                //Natural: FOR #A 1 10
                    for (pnd_A.setValue(1); condition(pnd_A.lessOrEqual(10)); pnd_A.nadd(1))
                    {
                        if (condition(pnd_Stat_Array.getValue(pnd_A).equals(" ")))                                                                                        //Natural: IF #STAT-ARRAY ( #A ) = ' '
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde.setValue(pnd_Stat_Array.getValue(pnd_A));                                                              //Natural: MOVE #STAT-ARRAY ( #A ) TO #ORGN-STATUS-INVRSE-SEQ.PYMNT-STATS-CDE
                        getReports().display(0, pnd_Orgn_Table_Pnd_Orgn_Array.getValue(pnd_R),pnd_Stat_Array.getValue(pnd_A));                                            //Natural: DISPLAY #ORGN-ARRAY ( #R ) #STAT-ARRAY ( #A )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //* *DISPLAY #START-INVRSE-DATE #END-INVRSE-DATE
                        //* *'INV/DATE' PAY.CNTRCT-INVRSE-DTE
                        //* *PAY.CNTRCT-PPCN-NBR PAY.CNTRCT-ORGN-CDE
                        //* *'1'
                        vw_pay.startDatabaseRead                                                                                                                          //Natural: READ PAY BY ORGN-STATUS-INVRSE-SEQ STARTING FROM #ORGN-STATUS-INVRSE-SEQ
                        (
                        "PAY1",
                        new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Orgn_Status_Invrse_Seq, WcType.BY) },
                        new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
                        );
                        PAY1:
                        while (condition(vw_pay.readNextRow("PAY1")))
                        {
                            if (condition(pay_Cntrct_Invrse_Dte.greater(pnd_End_Invrse_Date) || pay_Pymnt_Stats_Cde.notEquals(pnd_Orgn_Status_Invrse_Seq_Pymnt_Stats_Cde))) //Natural: IF PAY.CNTRCT-INVRSE-DTE > #END-INVRSE-DATE OR PAY.PYMNT-STATS-CDE NE #ORGN-STATUS-INVRSE-SEQ.PYMNT-STATS-CDE
                            {
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: END-IF
                            if (condition(pay_Cntrct_Ppcn_Nbr.equals(" ")))                                                                                               //Natural: IF CNTRCT-PPCN-NBR = ' '
                            {
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                            }                                                                                                                                             //Natural: END-IF
                            //*  CTS-0115 >>
                            if (condition(pay_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                 //Natural: IF PAY.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
                            {
                                if (condition(true)) continue;                                                                                                            //Natural: ESCAPE TOP
                                //*  CTS-0115 <<
                                //*  JWO1
                            }                                                                                                                                             //Natural: END-IF
                            //* *WRITE #START-INVRSE-DATE #END-INVRSE-DATE
                            //* *PAY.CNTRCT-INVRSE-DTE
                            //* *PAY.CNTRCT-PPCN-NBR PAY.CNTRCT-ORGN-CDE
                            //* *'2'
                            pnd_Date.compute(new ComputeParameters(false, pnd_Date), DbsField.subtract(100000000,pay_Cntrct_Invrse_Dte));                                 //Natural: ASSIGN #DATE := 100000000 - PAY.CNTRCT-INVRSE-DTE
                            pnd_Pin.setValue(pay_Cntrct_Unq_Id_Nbr);                                                                                                      //Natural: ASSIGN #PIN := PAY.CNTRCT-UNQ-ID-NBR
                            //*  JWO1
                            getWorkFiles().write(1, false, pay_Cntrct_Ppcn_Nbr, pay_Annt_Soc_Sec_Nbr, pnd_Pin, pay_Cntrct_Orgn_Cde, pnd_Date_Start, pnd_Date_End);        //Natural: WRITE WORK FILE 1 PAY.CNTRCT-PPCN-NBR PAY.ANNT-SOC-SEC-NBR #PIN PAY.CNTRCT-ORGN-CDE #DATE-START #DATE-END
                            //*        PAY.CNTRCT-UNQ-ID-NBR
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  --------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FUTURE-DATED
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Future_Dated() throws Exception                                                                                                                      //Natural: FUTURE-DATED
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(pnd_Date_Start.equals(pnd_Date_End)))                                                                                                               //Natural: IF #DATE-START = #DATE-END
        {
            pnd_Date_Start_D.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date_Start_Pnd_Date_Start_A);                                                              //Natural: MOVE EDITED #DATE-START-A TO #DATE-START-D ( EM = YYYYMMDD )
            pnd_Date_Start_D.nadd(60);                                                                                                                                    //Natural: ASSIGN #DATE-START-D := #DATE-START-D + 60
            //*  JWO 11/2014
            pnd_Date_Start_Pnd_Date_Start_A.setValueEdited(pnd_Date_Start_D,new ReportEditMask("YYYYMMDD"));                                                              //Natural: MOVE EDITED #DATE-START-D ( EM = YYYYMMDD ) TO #DATE-START-A
            pnd_Date_Start_D.nsubtract(63);                                                                                                                               //Natural: ASSIGN #DATE-START-D := #DATE-START-D - 63
            pnd_Date_End_Pnd_Date_End_A.setValueEdited(pnd_Date_Start_D,new ReportEditMask("YYYYMMDD"));                                                                  //Natural: MOVE EDITED #DATE-START-D ( EM = YYYYMMDD ) TO #DATE-END-A
            getReports().write(0, "$$$","=",pnd_Date_Start_Pnd_Date_Start_A,"=",pnd_Date_End_Pnd_Date_End_A);                                                             //Natural: WRITE '$$$' '=' #DATE-START-A '=' #DATE-END-A
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        getReports().setDisplayColumns(0, pnd_Orgn_Table_Pnd_Orgn_Array,pnd_Stat_Array);
    }
}
