/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:54 PM
**        * FROM NATURAL PROGRAM : Fcpp804
************************************************************
**        * FILE NAME            : Fcpp804.java
**        * CLASS NAME           : Fcpp804
**        * INSTANCE NAME        : Fcpp804
************************************************************
************************************************************************
* PROGRAM   : FCPP804
* SYSTEM    : CPS
* TITLE     : REPORT FILE EXTRACT
* GENERATED :
* FUNCTION  : CREATE  SEPARATE FILES FOR THESE REPORTS NEEDED TO BE
*           : GENERATED OFF OF THE PAYMENT NAME AND ADDRESS WORK FILE
*           : BEFORE THE CHECK PRINTING PROCESS:
*           : - INTERNAL ROLLOVERS
*           : - DEDUCTIONS TAKEN
*           : - HOLDS
*           : PRODUCE A SUMMARY REPORT OF GROSS AMOUNTS BY PAY TYPE
*           :
* HISTORY   : 07/15/1996    RITA SALGADO
*           : - PRODUCE A SUMMARY REPORT OF GROSS AMOUNTS BY
*           :   PAY TYPE.
*           : 08/30/96      RITA SALGADO
*           : RE-STOWED TO ACCEPT NEW FCPL199A FOR INFLATION LINK BOND
*           : 05/06/97      RITA SALGADO
*           : ADD PERSONAL ANNUITY TOTAL LINE TO SUMMARY REPORT
*           : 05/18/00      TOM MCGEE
*           : ADD PA-SELECT AND SPIA BREAK OUT
*           : REMOVE OLD PA-SELECT CODE (ACCUMED UNDER INDEX FIVE)
*           : 06/12/02      ROXAN CARREON
*           : ADD PA-SELECT AND SPIA NEW FUNDS BREAK OUT
*           : 07/10/02      PROD ERROR.  CHANGE FREE LOOK TO GENERAL
*           :               ACCOUNT.
*           :               CHANG LOGIC TO INCLUDE FIXED FUND
*           : 05/02.03      RESTOW. FCPA800 WAS EXPANDED
*            05/05/2008   LCW - RESTOWED FOR ROTH 403B. ROTH-MAJOR1
*                             - ROTH FIELDS INCLUDED IN FCPA800.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp804 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private LdaFcpl804 ldaFcpl804;
    private LdaFcpl199a ldaFcpl199a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Index;
    private DbsField pnd_I_Pay_Type;
    private DbsField pnd_I_Fund_Seq;
    private DbsField pnd_I_Fund;
    private DbsField pnd_Int_Roll_Ct;
    private DbsField pnd_Ded_Rec_Ct;
    private DbsField pnd_Rec_Ct;
    private DbsField pnd_Pymnt_Ded_Pay_Cde;

    private DbsGroup pnd_Pymnt_Ded_Pay_Cde__R_Field_1;
    private DbsField pnd_Pymnt_Ded_Pay_Cde__Filler1;
    private DbsField pnd_Pymnt_Ded_Pay_Cde_Pnd_Pymnt_Ded_Pay_Seq;

    private DbsGroup pnd_Ws_Valuation;

    private DbsGroup pnd_Ws_Valuation_Pnd_T_Pay_Type;

    private DbsGroup pnd_Ws_Valuation_Pnd_T_Fund_Seq;
    private DbsField pnd_Ws_Valuation_Pnd_Cntrct_Amt;
    private DbsField pnd_Ws_Valuation_Pnd_Dvdnd_Amt;
    private DbsField pnd_Ws_Valuation_Pnd_Gross_Amt;
    private DbsField pnd_Fund_Name;
    private DbsField pnd_Tot_Cntrct_Amt;
    private DbsField pnd_Tot_Dvdnd_Amt;
    private DbsField pnd_Tot_Gross_Amt;

    private DbsGroup pnd_Rcc_Var;
    private DbsField pnd_Rcc_Var_Pnd_Pay_Type_Cntrct_Amt;
    private DbsField pnd_Rcc_Var_Pnd_Pay_Type_Dvdnd_Amt;
    private DbsField pnd_Rcc_Var_Pnd_Pay_Type_Gross_Amt;
    private DbsField pnd_Rcc_Var_Pnd_Ws_Pay_Type_Desc;
    private DbsField pnd_Rcc_Var_Pnd_Ws_Valuat;
    private DbsField pnd_Rcc_Var_Pnd_Ws_Valuat_Desc;
    private DbsField pnd_Rcc_Var_Pnd_Ws_Period;
    private DbsField pa_Select_Hdr;
    private DbsField spia_Hdr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        ldaFcpl804 = new LdaFcpl804();
        registerRecord(ldaFcpl804);
        ldaFcpl199a = new LdaFcpl199a();
        registerRecord(ldaFcpl199a);

        // Local Variables
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_I_Pay_Type = localVariables.newFieldInRecord("pnd_I_Pay_Type", "#I-PAY-TYPE", FieldType.PACKED_DECIMAL, 2);
        pnd_I_Fund_Seq = localVariables.newFieldInRecord("pnd_I_Fund_Seq", "#I-FUND-SEQ", FieldType.PACKED_DECIMAL, 2);
        pnd_I_Fund = localVariables.newFieldInRecord("pnd_I_Fund", "#I-FUND", FieldType.PACKED_DECIMAL, 2);
        pnd_Int_Roll_Ct = localVariables.newFieldInRecord("pnd_Int_Roll_Ct", "#INT-ROLL-CT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ded_Rec_Ct = localVariables.newFieldInRecord("pnd_Ded_Rec_Ct", "#DED-REC-CT", FieldType.PACKED_DECIMAL, 7);
        pnd_Rec_Ct = localVariables.newFieldInRecord("pnd_Rec_Ct", "#REC-CT", FieldType.PACKED_DECIMAL, 9);
        pnd_Pymnt_Ded_Pay_Cde = localVariables.newFieldInRecord("pnd_Pymnt_Ded_Pay_Cde", "#PYMNT-DED-PAY-CDE", FieldType.STRING, 8);

        pnd_Pymnt_Ded_Pay_Cde__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_Ded_Pay_Cde__R_Field_1", "REDEFINE", pnd_Pymnt_Ded_Pay_Cde);
        pnd_Pymnt_Ded_Pay_Cde__Filler1 = pnd_Pymnt_Ded_Pay_Cde__R_Field_1.newFieldInGroup("pnd_Pymnt_Ded_Pay_Cde__Filler1", "_FILLER1", FieldType.STRING, 
            5);
        pnd_Pymnt_Ded_Pay_Cde_Pnd_Pymnt_Ded_Pay_Seq = pnd_Pymnt_Ded_Pay_Cde__R_Field_1.newFieldInGroup("pnd_Pymnt_Ded_Pay_Cde_Pnd_Pymnt_Ded_Pay_Seq", 
            "#PYMNT-DED-PAY-SEQ", FieldType.STRING, 3);

        pnd_Ws_Valuation = localVariables.newGroupArrayInRecord("pnd_Ws_Valuation", "#WS-VALUATION", new DbsArrayController(1, 2));

        pnd_Ws_Valuation_Pnd_T_Pay_Type = pnd_Ws_Valuation.newGroupArrayInGroup("pnd_Ws_Valuation_Pnd_T_Pay_Type", "#T-PAY-TYPE", new DbsArrayController(1, 
            4));

        pnd_Ws_Valuation_Pnd_T_Fund_Seq = pnd_Ws_Valuation_Pnd_T_Pay_Type.newGroupArrayInGroup("pnd_Ws_Valuation_Pnd_T_Fund_Seq", "#T-FUND-SEQ", new DbsArrayController(1, 
            57));
        pnd_Ws_Valuation_Pnd_Cntrct_Amt = pnd_Ws_Valuation_Pnd_T_Fund_Seq.newFieldInGroup("pnd_Ws_Valuation_Pnd_Cntrct_Amt", "#CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Valuation_Pnd_Dvdnd_Amt = pnd_Ws_Valuation_Pnd_T_Fund_Seq.newFieldInGroup("pnd_Ws_Valuation_Pnd_Dvdnd_Amt", "#DVDND-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Ws_Valuation_Pnd_Gross_Amt = pnd_Ws_Valuation_Pnd_T_Fund_Seq.newFieldInGroup("pnd_Ws_Valuation_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Fund_Name = localVariables.newFieldInRecord("pnd_Fund_Name", "#FUND-NAME", FieldType.STRING, 15);
        pnd_Tot_Cntrct_Amt = localVariables.newFieldInRecord("pnd_Tot_Cntrct_Amt", "#TOT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Tot_Dvdnd_Amt", "#TOT-DVDND-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Tot_Gross_Amt = localVariables.newFieldInRecord("pnd_Tot_Gross_Amt", "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Rcc_Var = localVariables.newGroupInRecord("pnd_Rcc_Var", "#RCC-VAR");
        pnd_Rcc_Var_Pnd_Pay_Type_Cntrct_Amt = pnd_Rcc_Var.newFieldInGroup("pnd_Rcc_Var_Pnd_Pay_Type_Cntrct_Amt", "#PAY-TYPE-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Rcc_Var_Pnd_Pay_Type_Dvdnd_Amt = pnd_Rcc_Var.newFieldInGroup("pnd_Rcc_Var_Pnd_Pay_Type_Dvdnd_Amt", "#PAY-TYPE-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Rcc_Var_Pnd_Pay_Type_Gross_Amt = pnd_Rcc_Var.newFieldInGroup("pnd_Rcc_Var_Pnd_Pay_Type_Gross_Amt", "#PAY-TYPE-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            13, 2);
        pnd_Rcc_Var_Pnd_Ws_Pay_Type_Desc = pnd_Rcc_Var.newFieldInGroup("pnd_Rcc_Var_Pnd_Ws_Pay_Type_Desc", "#WS-PAY-TYPE-DESC", FieldType.STRING, 12);
        pnd_Rcc_Var_Pnd_Ws_Valuat = pnd_Rcc_Var.newFieldInGroup("pnd_Rcc_Var_Pnd_Ws_Valuat", "#WS-VALUAT", FieldType.NUMERIC, 1);
        pnd_Rcc_Var_Pnd_Ws_Valuat_Desc = pnd_Rcc_Var.newFieldInGroup("pnd_Rcc_Var_Pnd_Ws_Valuat_Desc", "#WS-VALUAT-DESC", FieldType.STRING, 10);
        pnd_Rcc_Var_Pnd_Ws_Period = pnd_Rcc_Var.newFieldInGroup("pnd_Rcc_Var_Pnd_Ws_Period", "#WS-PERIOD", FieldType.NUMERIC, 1);
        pa_Select_Hdr = localVariables.newFieldInRecord("pa_Select_Hdr", "PA-SELECT-HDR", FieldType.BOOLEAN, 1);
        spia_Hdr = localVariables.newFieldInRecord("spia_Hdr", "SPIA-HDR", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl804.initializeValues();
        ldaFcpl199a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp804() throws Exception
    {
        super("Fcpp804");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP804", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) LS = 132 PS = 62;//Natural: FORMAT ( 01 ) LS = 132 PS = 62
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  READ PAYMENT/NAME AND ADDRESS WORK FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            pnd_Rec_Ct.nadd(1);                                                                                                                                           //Natural: ADD 1 TO #REC-CT
            //*  INTERNAL ROLLOVER
            short decideConditionsMet593 = 0;                                                                                                                             //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( * ) = 02
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue("*").equals(2)))
            {
                decideConditionsMet593++;
                //*  DEDUCTIONS TAKEN
                                                                                                                                                                          //Natural: PERFORM CREATE-INTROLL-RECORDS
                sub_Create_Introll_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) NE 0
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").notEquals(getZero())))
            {
                decideConditionsMet593++;
                                                                                                                                                                          //Natural: PERFORM CREATE-DED-RECORDS
                sub_Create_Ded_Records();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    WHEN WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'I'
                //*      ESCAPE TOP
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet593 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  CHECK
            short decideConditionsMet606 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet606++;
                //*  EFT
                pnd_I_Pay_Type.setValue(1);                                                                                                                               //Natural: MOVE 1 TO #I-PAY-TYPE
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet606++;
                //*  GLOBAL PAY
                pnd_I_Pay_Type.setValue(2);                                                                                                                               //Natural: MOVE 2 TO #I-PAY-TYPE
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3))))
            {
                decideConditionsMet606++;
                //*  INTERNAL ROLLOVER
                pnd_I_Pay_Type.setValue(3);                                                                                                                               //Natural: MOVE 3 TO #I-PAY-TYPE
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8))))
            {
                decideConditionsMet606++;
                pnd_I_Pay_Type.setValue(4);                                                                                                                               //Natural: MOVE 4 TO #I-PAY-TYPE
            }                                                                                                                                                             //Natural: NONE VALUES
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-DECIDE
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 TO WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
            {
                pnd_I_Fund.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_Index));                                                                //Natural: MOVE WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-N ( #INDEX ) TO #I-FUND
                pnd_I_Fund_Seq.setValue(ldaFcpl199a.getPnd_Fund_Lda_Pnd_Inv_Acct_Seq().getValue(pnd_I_Fund));                                                             //Natural: MOVE #FUND-LDA.#INV-ACCT-SEQ ( #I-FUND ) TO #I-FUND-SEQ
                //* * PA SELECT FIXED SPIA FIXED
                //* * WAS CONVERTED TO FUND 'T' SEQ (01) FCPP800
                //*  THIS CONDIITON CAN BE SIMPLIFIED LIKE CHECKING FOR
                //*   CNTRCT-ANNTY-INS-TYPE  =  'S' OR = 'M' /*  RCC
                if (condition(((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("D"))  //Natural: IF ( ( WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'S' AND WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE = 'D' ) OR ( WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'S' AND WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE = 'M' ) OR ( WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'M' AND WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE = 'M' ) )
                    || (pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M")) 
                    || (pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M")))))
                {
                    //*    COMMENT OUT NEXT TWO LINES; REPLACED BY LINE 1260 - ROXAN 09/20/00
                    //*    TO MAKE LINES 1560 AND 1570 TRUE
                    //*     AND WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-N(#INDEX) = 01 /* FIXED
                    //*       MOVE 40 TO #I-FUND-SEQ
                    //*  RCC 07/10/02  INCLUDE FIXED FUND
                    pnd_I_Fund_Seq.nadd(32);                                                                                                                              //Natural: ADD 32 TO #I-FUND-SEQ
                }                                                                                                                                                         //Natural: END-IF
                //*  TIAA INSURNCE
                //*  PERSONAL ANNTY
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("I") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("P"))) //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'I' OR = 'P'
                {
                    pnd_I_Fund_Seq.setValue(4);                                                                                                                           //Natural: MOVE 4 TO #I-FUND-SEQ
                }                                                                                                                                                         //Natural: END-IF
                //*  RCC 07/10/02
                if (condition(pnd_I_Fund_Seq.greater(4)))                                                                                                                 //Natural: IF #I-FUND-SEQ > 4
                {
                    //*  REMOVED THESE
                    pnd_I_Fund_Seq.nadd(1);                                                                                                                               //Natural: ADD 1 TO #I-FUND-SEQ
                    //*  LINES
                }                                                                                                                                                         //Natural: END-IF
                //*   PA SELECT         02/03/99  V. FULTON
                //*  THIS CONDIITON CAN BE SIMPLIFIED LIKE CHECKING FOR
                //*   CNTRCT-ANNTY-INS-TYPE  =  'S' ONLY
                if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("D"))  //Natural: IF ( WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'S' AND WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE = 'D' ) OR ( WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'S' AND WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE = 'M' )
                    || (pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M"))))
                {
                    //*       COMPUTE #I-FUND-SEQ = #I-FUND-SEQ - 10
                    //*  RCC 07/10/02
                    pnd_I_Fund_Seq.nsubtract(14);                                                                                                                         //Natural: COMPUTE #I-FUND-SEQ = #I-FUND-SEQ - 14
                    //* *         ACCUMULATE PA-SELECT IN THE 30+ INDEX RANGE
                    //* *         ACCUMULATE SPIA IN THE 43+ INDEX RANGE
                }                                                                                                                                                         //Natural: END-IF
                //*  #I-FUND-SEQ : 1=T; 2=TG; 3=G; 4=INSURANCE; 5=PA; 6=R; 7=C; .....
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_Index).equals("M")))                                                  //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-VALUAT-PERIOD ( #INDEX ) = 'M'
                {
                    pnd_Rcc_Var_Pnd_Ws_Period.setValue(2);                                                                                                                //Natural: MOVE 2 TO #WS-PERIOD
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rcc_Var_Pnd_Ws_Period.setValue(1);                                                                                                                //Natural: MOVE 1 TO #WS-PERIOD
                }                                                                                                                                                         //Natural: END-IF
                //*  TIAA
                //*  RCC 07/10/02
                //*                                                                                                                                                       //Natural: DECIDE ON FIRST #I-FUND-SEQ
                short decideConditionsMet671 = 0;                                                                                                                         //Natural: VALUE 1,2,3,4,31,32,45,46
                if (condition((pnd_I_Fund_Seq.equals(1) || pnd_I_Fund_Seq.equals(2) || pnd_I_Fund_Seq.equals(3) || pnd_I_Fund_Seq.equals(4) || pnd_I_Fund_Seq.equals(31) 
                    || pnd_I_Fund_Seq.equals(32) || pnd_I_Fund_Seq.equals(45) || pnd_I_Fund_Seq.equals(46))))
                {
                    decideConditionsMet671++;
                    pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(1,pnd_I_Pay_Type,pnd_I_Fund_Seq).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( #INDEX ) TO #CNTRCT-AMT ( 1,#I-PAY-TYPE, #I-FUND-SEQ )
                    pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(1,pnd_I_Pay_Type,pnd_I_Fund_Seq).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT ( 1,#I-PAY-TYPE, #I-FUND-SEQ )
                    pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(1,pnd_I_Pay_Type,pnd_I_Fund_Seq).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #GROSS-AMT ( 1,#I-PAY-TYPE, #I-FUND-SEQ )
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    //*  VARIABLE
                    pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ )
                    pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ )
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 46T 'GROSS PAYMENT AMOUNT BY PAYMENT TYPE' / 56T WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        FOR02:                                                                                                                                                            //Natural: FOR #I-PAY-TYPE 1 3
        for (pnd_I_Pay_Type.setValue(1); condition(pnd_I_Pay_Type.lessOrEqual(3)); pnd_I_Pay_Type.nadd(1))
        {
            FOR03:                                                                                                                                                        //Natural: FOR #WS-PERIOD 1 2
            for (pnd_Rcc_Var_Pnd_Ws_Period.setValue(1); condition(pnd_Rcc_Var_Pnd_Ws_Period.lessOrEqual(2)); pnd_Rcc_Var_Pnd_Ws_Period.nadd(1))
            {
                pa_Select_Hdr.setValue(false);                                                                                                                            //Natural: ASSIGN PA-SELECT-HDR := FALSE
                spia_Hdr.setValue(false);                                                                                                                                 //Natural: ASSIGN SPIA-HDR := FALSE
                if (condition(pnd_Rcc_Var_Pnd_Ws_Period.equals(2)))                                                                                                       //Natural: IF #WS-PERIOD = 2
                {
                    pnd_Rcc_Var_Pnd_Ws_Valuat_Desc.setValue("MONTHLY");                                                                                                   //Natural: MOVE 'MONTHLY' TO #WS-VALUAT-DESC
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Rcc_Var_Pnd_Ws_Valuat_Desc.setValue("ANNUAL");                                                                                                    //Natural: MOVE 'ANNUAL' TO #WS-VALUAT-DESC
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet699 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #I-PAY-TYPE;//Natural: VALUE 1
                if (condition((pnd_I_Pay_Type.equals(1))))
                {
                    decideConditionsMet699++;
                    if (condition(pnd_Rcc_Var_Pnd_Ws_Period.equals(1)))                                                                                                   //Natural: IF #WS-PERIOD = 1
                    {
                        getReports().write(1, NEWLINE,new ColumnSpacing(5),"CHECKS:    ",NEWLINE,new ColumnSpacing(5),"=======");                                         //Natural: WRITE ( 01 ) / 5X 'CHECKS:    ' / 5X '======='
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Rcc_Var_Pnd_Ws_Pay_Type_Desc.setValue("CHECKS ");                                                                                             //Natural: MOVE 'CHECKS ' TO #WS-PAY-TYPE-DESC
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, NEWLINE,new ColumnSpacing(5),"           ",pnd_Rcc_Var_Pnd_Ws_Valuat_Desc,NEWLINE);                                             //Natural: WRITE ( 01 ) / 5X '           ' #WS-VALUAT-DESC /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_I_Pay_Type.equals(2))))
                {
                    decideConditionsMet699++;
                    if (condition(pnd_Rcc_Var_Pnd_Ws_Period.equals(1)))                                                                                                   //Natural: IF #WS-PERIOD = 1
                    {
                        getReports().newPage(new ReportSpecification(1));                                                                                                 //Natural: NEWPAGE ( 01 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, NEWLINE,NEWLINE,new ColumnSpacing(5),"EFTs:      ",NEWLINE,new ColumnSpacing(5),"=====");                                   //Natural: WRITE ( 01 ) // 5X 'EFTs:      ' / 5X '====='
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Rcc_Var_Pnd_Ws_Pay_Type_Desc.setValue("EFTs ");                                                                                               //Natural: MOVE 'EFTs ' TO #WS-PAY-TYPE-DESC
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, NEWLINE,new ColumnSpacing(5),"           ",pnd_Rcc_Var_Pnd_Ws_Valuat_Desc,NEWLINE);                                             //Natural: WRITE ( 01 ) / 5X '           ' #WS-VALUAT-DESC /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_I_Pay_Type.equals(3))))
                {
                    decideConditionsMet699++;
                    if (condition(pnd_Rcc_Var_Pnd_Ws_Period.equals(1)))                                                                                                   //Natural: IF #WS-PERIOD = 1
                    {
                        getReports().newPage(new ReportSpecification(1));                                                                                                 //Natural: NEWPAGE ( 01 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getReports().write(1, NEWLINE,NEWLINE,new ColumnSpacing(5),"GLOBAL PAY:",NEWLINE,new ColumnSpacing(5),"===========");                             //Natural: WRITE ( 01 ) // 5X 'GLOBAL PAY:' / 5X '==========='
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Rcc_Var_Pnd_Ws_Pay_Type_Desc.setValue("GLOBAL PAY ");                                                                                         //Natural: MOVE 'GLOBAL PAY ' TO #WS-PAY-TYPE-DESC
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, NEWLINE,new ColumnSpacing(5),"           ",pnd_Rcc_Var_Pnd_Ws_Valuat_Desc,NEWLINE);                                             //Natural: WRITE ( 01 ) / 5X '           ' #WS-VALUAT-DESC /
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                FOR04:                                                                                                                                                    //Natural: FOR #I-FUND-SEQ 1 24
                for (pnd_I_Fund_Seq.setValue(1); condition(pnd_I_Fund_Seq.lessOrEqual(24)); pnd_I_Fund_Seq.nadd(1))
                {
                    //*  TIAA
                    if (condition(pnd_I_Fund_Seq.equals(1) || pnd_I_Fund_Seq.equals(2)))                                                                                  //Natural: IF #I-FUND-SEQ = 1 OR = 2
                    {
                        pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,3).nadd(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period, //Natural: ADD #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) TO #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 3 )
                            pnd_I_Pay_Type,pnd_I_Fund_Seq));
                        pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,3).nadd(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period, //Natural: ADD #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) TO #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 3 )
                            pnd_I_Pay_Type,pnd_I_Fund_Seq));
                        pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,3).nadd(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period, //Natural: ADD #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) TO #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 3 )
                            pnd_I_Pay_Type,pnd_I_Fund_Seq));
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    short decideConditionsMet733 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #I-FUND-SEQ;//Natural: VALUE 3
                    if (condition((pnd_I_Fund_Seq.equals(3))))
                    {
                        decideConditionsMet733++;
                        if (condition(pnd_Rcc_Var_Pnd_Ws_Period.equals(1)))                                                                                               //Natural: IF #WS-PERIOD = 1
                        {
                            pnd_Fund_Name.setValue("TIAA Pension   ");                                                                                                    //Natural: MOVE 'TIAA Pension   ' TO #FUND-NAME
                            //*  RCC 06/17/02
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  RCC 06/17/02
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 4
                    else if (condition((pnd_I_Fund_Seq.equals(4))))
                    {
                        decideConditionsMet733++;
                        if (condition(pnd_Rcc_Var_Pnd_Ws_Period.equals(1)))                                                                                               //Natural: IF #WS-PERIOD = 1
                        {
                            pnd_Fund_Name.setValue("TIAA Insurance ");                                                                                                    //Natural: MOVE 'TIAA Insurance ' TO #FUND-NAME
                            //*  RCC 06/17/02
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  RCC 06/17/02
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 5
                    else if (condition((pnd_I_Fund_Seq.equals(5))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Real Estate    ");                                                                                                        //Natural: MOVE 'Real Estate    ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 6
                    else if (condition((pnd_I_Fund_Seq.equals(6))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Stock          ");                                                                                                        //Natural: MOVE 'Stock          ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 7
                    else if (condition((pnd_I_Fund_Seq.equals(7))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Money Market   ");                                                                                                        //Natural: MOVE 'Money Market   ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 8
                    else if (condition((pnd_I_Fund_Seq.equals(8))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Social Choice  ");                                                                                                        //Natural: MOVE 'Social Choice  ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 9
                    else if (condition((pnd_I_Fund_Seq.equals(9))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Bond           ");                                                                                                        //Natural: MOVE 'Bond           ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 10
                    else if (condition((pnd_I_Fund_Seq.equals(10))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Global         ");                                                                                                        //Natural: MOVE 'Global         ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 11
                    else if (condition((pnd_I_Fund_Seq.equals(11))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Growth         ");                                                                                                        //Natural: MOVE 'Growth         ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 12
                    else if (condition((pnd_I_Fund_Seq.equals(12))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Equity Index   ");                                                                                                        //Natural: MOVE 'Equity Index   ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 13
                    else if (condition((pnd_I_Fund_Seq.equals(13))))
                    {
                        decideConditionsMet733++;
                        pnd_Fund_Name.setValue("Infl. Link Bond");                                                                                                        //Natural: MOVE 'Infl. Link Bond' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        pnd_Fund_Name.setValue("...............");                                                                                                        //Natural: MOVE '...............' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(pnd_Fund_Name.equals("...............") && pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero())  //Natural: IF #FUND-NAME = '...............' AND #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0 AND #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0 AND #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0
                        && pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero()) && pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,
                        pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero())))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().display(1, new ColumnSpacing(10),"Fund Type",                                                                                            //Natural: DISPLAY ( 01 ) 10X 'Fund Type' #FUND-NAME 10X 'Contractual/Amount' #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) 10X 'Dividend/Amount' #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) 10X 'Total/Gross' #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ )
                    		pnd_Fund_Name,new ColumnSpacing(10),"Contractual/Amount",
                    		pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq),new ColumnSpacing(10),"Dividend/Amount",
                        
                    		pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq),new ColumnSpacing(10),"Total/Gross",
                        
                    		pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25)),  //Natural: ASSIGN #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 ) := #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 3:24 ) + 0
                    pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,3,":",24).add(getZero()));
                pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25)),  //Natural: ASSIGN #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 ) := #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 3:24 ) + 0
                    pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,3,":",24).add(getZero()));
                pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25)),  //Natural: ASSIGN #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 ) := #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 3:24 ) + 0
                    pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,3,":",24).add(getZero()));
                //*  AND NEW FUNDS
                getReports().write(1, NEWLINE,"Sub Total",new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25),  //Natural: WRITE ( 01 ) / 'Sub Total' T*#CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 ) T*#DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 ) T*#GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rcc_Var_Pnd_Pay_Type_Cntrct_Amt.nadd(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25));                          //Natural: ASSIGN #PAY-TYPE-CNTRCT-AMT := #PAY-TYPE-CNTRCT-AMT + #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 )
                pnd_Rcc_Var_Pnd_Pay_Type_Dvdnd_Amt.nadd(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25));                            //Natural: ASSIGN #PAY-TYPE-DVDND-AMT := #PAY-TYPE-DVDND-AMT + #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 )
                pnd_Rcc_Var_Pnd_Pay_Type_Gross_Amt.nadd(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25));                            //Natural: ASSIGN #PAY-TYPE-GROSS-AMT := #PAY-TYPE-GROSS-AMT + #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 )
                pnd_Tot_Cntrct_Amt.nadd(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25));                                           //Natural: ASSIGN #TOT-CNTRCT-AMT := #TOT-CNTRCT-AMT + #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 )
                pnd_Tot_Dvdnd_Amt.nadd(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25));                                             //Natural: ASSIGN #TOT-DVDND-AMT := #TOT-DVDND-AMT + #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 )
                pnd_Tot_Gross_Amt.nadd(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,25));                                             //Natural: ASSIGN #TOT-GROSS-AMT := #TOT-GROSS-AMT + #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 25 )
                FOR05:                                                                                                                                                    //Natural: FOR #I-FUND-SEQ 32 41
                for (pnd_I_Fund_Seq.setValue(32); condition(pnd_I_Fund_Seq.lessOrEqual(41)); pnd_I_Fund_Seq.nadd(1))
                {
                    //* *  PA-SELECT - REMEMBER FUND NUMBER PLUS ONE (DUE TO ADD OF PA-ANNUITY                                                                            //Natural: DECIDE ON FIRST VALUE OF #I-FUND-SEQ
                    //* *  IN OCCURANCE 4 ABOVE
                    //* * PA-SELECT = FUND NUMBER - 10 + 1
                    short decideConditionsMet796 = 0;                                                                                                                     //Natural: VALUE 32
                    if (condition((pnd_I_Fund_Seq.equals(32))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("Fixed Fund     ");                                                                                                        //Natural: MOVE 'Fixed Fund     ' TO #FUND-NAME
                        //*        VALUE 33
                        //*          MOVE 'General Account' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 34
                    else if (condition((pnd_I_Fund_Seq.equals(34))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("Stock Index    ");                                                                                                        //Natural: MOVE 'Stock Index    ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 35
                    else if (condition((pnd_I_Fund_Seq.equals(35))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("Growth Equity  ");                                                                                                        //Natural: MOVE 'Growth Equity  ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 36
                    else if (condition((pnd_I_Fund_Seq.equals(36))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("Growth & Income");                                                                                                        //Natural: MOVE 'Growth & Income' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 37
                    else if (condition((pnd_I_Fund_Seq.equals(37))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("International  ");                                                                                                        //Natural: MOVE 'International  ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 38
                    else if (condition((pnd_I_Fund_Seq.equals(38))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("Social Choice  ");                                                                                                        //Natural: MOVE 'Social Choice  ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 39
                    else if (condition((pnd_I_Fund_Seq.equals(39))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("Large Cap Value");                                                                                                        //Natural: MOVE 'Large Cap Value' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 40
                    else if (condition((pnd_I_Fund_Seq.equals(40))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("Small Cap Value");                                                                                                        //Natural: MOVE 'Small Cap Value' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 41
                    else if (condition((pnd_I_Fund_Seq.equals(41))))
                    {
                        decideConditionsMet796++;
                        pnd_Fund_Name.setValue("Real Estate Sec");                                                                                                        //Natural: MOVE 'Real Estate Sec' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: ANY
                    if (condition(decideConditionsMet796 > 0))
                    {
                        if (condition(! (pa_Select_Hdr.getBoolean())))                                                                                                    //Natural: IF NOT PA-SELECT-HDR
                        {
                            //*  WRITE ONCE
                            getReports().write(1, NEWLINE,new ColumnSpacing(10),"PA-Select");                                                                             //Natural: WRITE ( 01 ) / 10X 'PA-Select'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pa_Select_Hdr.setValue(true);                                                                                                                 //Natural: ASSIGN PA-SELECT-HDR := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        pnd_Fund_Name.setValue("...............");                                                                                                        //Natural: MOVE '...............' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(pnd_Fund_Name.equals("...............") && pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero())  //Natural: IF #FUND-NAME = '...............' AND #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0 AND #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0 AND #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0
                        && pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero()) && pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,
                        pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero())))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, new ReportTAsterisk(pnd_Fund_Name),pnd_Fund_Name,new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq),  //Natural: WRITE ( 01 ) T*#FUND-NAME #FUND-NAME T*#CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) T*#DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) T*#GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ )
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq), 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq), 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  RCC 07/10/02
                    //*  RCC 07/10/02
                    //*  RCC
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43)),  //Natural: ASSIGN #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 ) := #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 32:41 ) + 0
                    pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,32,":",41).add(getZero()));
                pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43)),  //Natural: ASSIGN #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 ) := #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 32:41 ) + 0
                    pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,32,":",41).add(getZero()));
                pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43)),  //Natural: ASSIGN #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 ) := #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 32:41 ) + 0
                    pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,32,":",41).add(getZero()));
                //*  AND NEW FUNDS
                getReports().write(1, NEWLINE,"Sub Total",new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43),  //Natural: WRITE ( 01 ) / 'Sub Total' T*#CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 ) T*#DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 ) T*#GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rcc_Var_Pnd_Pay_Type_Cntrct_Amt.nadd(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43));                          //Natural: ASSIGN #PAY-TYPE-CNTRCT-AMT := #PAY-TYPE-CNTRCT-AMT + #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 )
                pnd_Rcc_Var_Pnd_Pay_Type_Dvdnd_Amt.nadd(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43));                            //Natural: ASSIGN #PAY-TYPE-DVDND-AMT := #PAY-TYPE-DVDND-AMT + #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 )
                pnd_Rcc_Var_Pnd_Pay_Type_Gross_Amt.nadd(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43));                            //Natural: ASSIGN #PAY-TYPE-GROSS-AMT := #PAY-TYPE-GROSS-AMT + #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 )
                pnd_Tot_Cntrct_Amt.nadd(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43));                                           //Natural: ASSIGN #TOT-CNTRCT-AMT := #TOT-CNTRCT-AMT + #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 )
                pnd_Tot_Dvdnd_Amt.nadd(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43));                                             //Natural: ASSIGN #TOT-DVDND-AMT := #TOT-DVDND-AMT + #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 )
                pnd_Tot_Gross_Amt.nadd(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,43));                                             //Natural: ASSIGN #TOT-GROSS-AMT := #TOT-GROSS-AMT + #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 43 )
                //* **
                FOR06:                                                                                                                                                    //Natural: FOR #I-FUND-SEQ 46 55
                for (pnd_I_Fund_Seq.setValue(46); condition(pnd_I_Fund_Seq.lessOrEqual(55)); pnd_I_Fund_Seq.nadd(1))
                {
                    //* *  PA-SELECT - REMEMBER FUND NUMBER PLUS ONE (DUE TO ADD OF PA-ANNUITY                                                                            //Natural: DECIDE ON FIRST VALUE OF #I-FUND-SEQ
                    //* *  IN OCCURANCE 4 ABOVE
                    //* * PA-SELECT = FUND NUMBER - 10 + 1
                    short decideConditionsMet857 = 0;                                                                                                                     //Natural: VALUE 46
                    if (condition((pnd_I_Fund_Seq.equals(46))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("Fixed Fund     ");                                                                                                        //Natural: MOVE 'Fixed Fund     ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 47
                    else if (condition((pnd_I_Fund_Seq.equals(47))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("General Account");                                                                                                        //Natural: MOVE 'General Account' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 48
                    else if (condition((pnd_I_Fund_Seq.equals(48))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("Stock Index    ");                                                                                                        //Natural: MOVE 'Stock Index    ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 49
                    else if (condition((pnd_I_Fund_Seq.equals(49))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("Growth Equity  ");                                                                                                        //Natural: MOVE 'Growth Equity  ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 50
                    else if (condition((pnd_I_Fund_Seq.equals(50))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("Growth & Income");                                                                                                        //Natural: MOVE 'Growth & Income' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 51
                    else if (condition((pnd_I_Fund_Seq.equals(51))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("International  ");                                                                                                        //Natural: MOVE 'International  ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 52
                    else if (condition((pnd_I_Fund_Seq.equals(52))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("Social Choice  ");                                                                                                        //Natural: MOVE 'Social Choice  ' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 53
                    else if (condition((pnd_I_Fund_Seq.equals(53))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("Large Cap Value");                                                                                                        //Natural: MOVE 'Large Cap Value' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 54
                    else if (condition((pnd_I_Fund_Seq.equals(54))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("Small Cap Value");                                                                                                        //Natural: MOVE 'Small Cap Value' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: VALUE 55
                    else if (condition((pnd_I_Fund_Seq.equals(55))))
                    {
                        decideConditionsMet857++;
                        pnd_Fund_Name.setValue("Real Estate Sec");                                                                                                        //Natural: MOVE 'Real Estate Sec' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: ANY
                    if (condition(decideConditionsMet857 > 0))
                    {
                        if (condition(! (spia_Hdr.getBoolean())))                                                                                                         //Natural: IF NOT SPIA-HDR
                        {
                            //*  WRITE ONCE
                            getReports().write(1, NEWLINE,new ColumnSpacing(10),"SPIA");                                                                                  //Natural: WRITE ( 01 ) / 10X 'SPIA'
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            spia_Hdr.setValue(true);                                                                                                                      //Natural: ASSIGN SPIA-HDR := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        pnd_Fund_Name.setValue("...............");                                                                                                        //Natural: MOVE '...............' TO #FUND-NAME
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(pnd_Fund_Name.equals("...............") && pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero())  //Natural: IF #FUND-NAME = '...............' AND #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0 AND #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0 AND #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) = 0
                        && pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero()) && pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,
                        pnd_I_Pay_Type,pnd_I_Fund_Seq).equals(getZero())))
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, new ReportTAsterisk(pnd_Fund_Name),pnd_Fund_Name,new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq),  //Natural: WRITE ( 01 ) T*#FUND-NAME #FUND-NAME T*#CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) T*#DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ ) T*#GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, #I-FUND-SEQ )
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq), 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,pnd_I_Fund_Seq), 
                        new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  RCC 07/10/02
                    //*  RCC 07/10/02
                    //*  RCC 07/10/02
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57)),  //Natural: ASSIGN #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 ) := #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 46:55 ) + 0
                    pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,46,":",55).add(getZero()));
                pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57)),  //Natural: ASSIGN #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 ) := #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 46:55 ) + 0
                    pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,46,":",55).add(getZero()));
                pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57).compute(new ComputeParameters(false, pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57)),  //Natural: ASSIGN #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 ) := #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 46:55 ) + 0
                    pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,46,":",55).add(getZero()));
                getReports().write(1, NEWLINE,"Sub Total",new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57),  //Natural: WRITE ( 01 ) / 'Sub Total' T*#CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 ) T*#DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 ) T*#GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 1 ) #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 )
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,1)),pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57), 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Rcc_Var_Pnd_Pay_Type_Cntrct_Amt.nadd(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57));                          //Natural: ASSIGN #PAY-TYPE-CNTRCT-AMT := #PAY-TYPE-CNTRCT-AMT + #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 )
                pnd_Rcc_Var_Pnd_Pay_Type_Dvdnd_Amt.nadd(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57));                            //Natural: ASSIGN #PAY-TYPE-DVDND-AMT := #PAY-TYPE-DVDND-AMT + #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 )
                pnd_Rcc_Var_Pnd_Pay_Type_Gross_Amt.nadd(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57));                            //Natural: ASSIGN #PAY-TYPE-GROSS-AMT := #PAY-TYPE-GROSS-AMT + #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 )
                pnd_Tot_Cntrct_Amt.nadd(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57));                                           //Natural: ASSIGN #TOT-CNTRCT-AMT := #TOT-CNTRCT-AMT + #CNTRCT-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 )
                pnd_Tot_Dvdnd_Amt.nadd(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57));                                             //Natural: ASSIGN #TOT-DVDND-AMT := #TOT-DVDND-AMT + #DVDND-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 )
                pnd_Tot_Gross_Amt.nadd(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_Rcc_Var_Pnd_Ws_Period,pnd_I_Pay_Type,57));                                             //Natural: ASSIGN #TOT-GROSS-AMT := #TOT-GROSS-AMT + #GROSS-AMT ( #WS-PERIOD,#I-PAY-TYPE, 57 )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(1, NEWLINE,"Total",pnd_Rcc_Var_Pnd_Ws_Pay_Type_Desc,new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_I_Pay_Type,1)),pnd_Rcc_Var_Pnd_Pay_Type_Cntrct_Amt,  //Natural: WRITE ( 01 ) / 'Total' #WS-PAY-TYPE-DESC T*#CNTRCT-AMT ( #I-PAY-TYPE, 1 ) #PAY-TYPE-CNTRCT-AMT T*#DVDND-AMT ( #I-PAY-TYPE, 1 ) #PAY-TYPE-DVDND-AMT T*#GROSS-AMT ( #I-PAY-TYPE, 1 ) #PAY-TYPE-GROSS-AMT
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_I_Pay_Type,1)),pnd_Rcc_Var_Pnd_Pay_Type_Dvdnd_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_I_Pay_Type,1)),pnd_Rcc_Var_Pnd_Pay_Type_Gross_Amt, 
                new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Rcc_Var_Pnd_Pay_Type_Cntrct_Amt.reset();                                                                                                                  //Natural: RESET #PAY-TYPE-CNTRCT-AMT #PAY-TYPE-DVDND-AMT #PAY-TYPE-GROSS-AMT
            pnd_Rcc_Var_Pnd_Pay_Type_Dvdnd_Amt.reset();
            pnd_Rcc_Var_Pnd_Pay_Type_Gross_Amt.reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,"Grand Total",new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Cntrct_Amt.getValue(pnd_I_Pay_Type,1)),pnd_Tot_Cntrct_Amt,           //Natural: WRITE ( 01 ) // 'Grand Total' T*#CNTRCT-AMT ( #I-PAY-TYPE, 1 ) #TOT-CNTRCT-AMT T*#DVDND-AMT ( #I-PAY-TYPE, 1 ) #TOT-DVDND-AMT T*#GROSS-AMT ( #I-PAY-TYPE, 1 ) #TOT-GROSS-AMT
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Dvdnd_Amt.getValue(pnd_I_Pay_Type,1)),pnd_Tot_Dvdnd_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"),new ReportTAsterisk(pnd_Ws_Valuation_Pnd_Gross_Amt.getValue(pnd_I_Pay_Type,1)),pnd_Tot_Gross_Amt, 
            new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ.99-"));
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-INTROLL-RECORDS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-DED-RECORDS
        //* ****************
        getReports().write(0, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"Total Payment Records   :",pnd_Rec_Ct, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),NEWLINE,NEWLINE,"  Internal Rollovers    :",pnd_Int_Roll_Ct,  //Natural: WRITE //// // 'Total Payment Records   :' #REC-CT ( EM = ZZZ,ZZZ,ZZ9 ) // '  Internal Rollovers    :' #INT-ROLL-CT ( EM = Z,ZZZ,ZZ9 ) // '  Voluntary Deductions  :' #DED-REC-CT ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,"  Voluntary Deductions  :",pnd_Ded_Rec_Ct, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }
    private void sub_Create_Introll_Records() throws Exception                                                                                                            //Natural: CREATE-INTROLL-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        getWorkFiles().write(2, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); //Natural: WRITE WORK FILE 2 VARIABLE PYMNT-ADDR-INFO INV-INFO ( 1:INV-ACCT-COUNT )
        pnd_Int_Roll_Ct.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #INT-ROLL-CT
        //*  CREATE-INTROLL-RECORDS
    }
    private void sub_Create_Ded_Records() throws Exception                                                                                                                //Natural: CREATE-DED-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        FOR07:                                                                                                                                                            //Natural: FOR #INDEX 1 10
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(10)); pnd_Index.nadd(1))
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Index).equals(getZero())))                                                         //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( #INDEX ) = 0
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl804.getDeduction_Rec().reset();                                                                                                                        //Natural: RESET DEDUCTION-REC
            ldaFcpl804.getDeduction_Rec_Cntrct_Company().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(1));                                        //Natural: MOVE WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( 1 ) TO DEDUCTION-REC.CNTRCT-COMPANY
            ldaFcpl804.getDeduction_Rec_Cntrct_Cmbn_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR TO DEDUCTION-REC.CNTRCT-CMBN-NBR
            ldaFcpl804.getDeduction_Rec_Cntrct_Ppcn_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR TO DEDUCTION-REC.CNTRCT-PPCN-NBR
            ldaFcpl804.getDeduction_Rec_Cntrct_Payee_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde());                                                  //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-PAYEE-CDE TO DEDUCTION-REC.CNTRCT-PAYEE-CDE
            ldaFcpl804.getDeduction_Rec_Cntrct_Mode_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-MODE-CDE TO DEDUCTION-REC.CNTRCT-MODE-CDE
            ldaFcpl804.getDeduction_Rec_Annt_Soc_Sec_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Soc_Sec_Nbr());                                                  //Natural: MOVE WF-PYMNT-ADDR-GRP.ANNT-SOC-SEC-NBR TO DEDUCTION-REC.ANNT-SOC-SEC-NBR
            ldaFcpl804.getDeduction_Rec_Pymnt_Suspend_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde());                                                //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE TO DEDUCTION-REC.PYMNT-SUSPEND-CDE
            ldaFcpl804.getDeduction_Rec_Cntrct_Hold_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE TO DEDUCTION-REC.CNTRCT-HOLD-CDE
            ldaFcpl804.getDeduction_Rec_Pymnt_Check_Dte().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE TO DEDUCTION-REC.PYMNT-CHECK-DTE
            ldaFcpl804.getDeduction_Rec_Ph_Last_Name().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name());                                                          //Natural: MOVE WF-PYMNT-ADDR-GRP.PH-LAST-NAME TO DEDUCTION-REC.PH-LAST-NAME
            ldaFcpl804.getDeduction_Rec_Ph_First_Name().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name());                                                        //Natural: MOVE WF-PYMNT-ADDR-GRP.PH-FIRST-NAME TO DEDUCTION-REC.PH-FIRST-NAME
            ldaFcpl804.getDeduction_Rec_Ph_Middle_Name().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Middle_Name());                                                      //Natural: MOVE WF-PYMNT-ADDR-GRP.PH-MIDDLE-NAME TO DEDUCTION-REC.PH-MIDDLE-NAME
            ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index));                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( #INDEX ) TO DEDUCTION-REC.PYMNT-DED-CDE
            ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Amt().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Index));                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( #INDEX ) TO DEDUCTION-REC.PYMNT-DED-AMT
            ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde().getValue(pnd_Index));                        //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-DED-PAYEE-CDE ( #INDEX ) TO DEDUCTION-REC.PYMNT-DED-PAYEE-CDE
            //*  THE PAYEE SEQUENCE FOR LONG TERM CARE DEDUCTION IS TEMPORARILY
            //*  STORED IN THE LAST 3 DIGITS OF THE DEDUCTION PAYEE CODE
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Index).equals(2)))                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( #INDEX ) = 002
            {
                pnd_Pymnt_Ded_Pay_Cde.setValue(ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde());                                                                        //Natural: MOVE DEDUCTION-REC.PYMNT-DED-PAYEE-CDE TO #PYMNT-DED-PAY-CDE
                ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Seq().setValue(pnd_Pymnt_Ded_Pay_Cde_Pnd_Pymnt_Ded_Pay_Seq);                                                  //Natural: MOVE #PYMNT-DED-PAY-SEQ TO DEDUCTION-REC.PYMNT-DED-PAYEE-SEQ
                setValueToSubstring("   ",ldaFcpl804.getDeduction_Rec_Pymnt_Ded_Payee_Cde(),6,3);                                                                         //Natural: MOVE '   ' TO SUBSTRING ( DEDUCTION-REC.PYMNT-DED-PAYEE-CDE,6,3 )
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl804.getDeduction_Rec_Cntrct_Crrncy_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Crrncy_Cde());                                                //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-CRRNCY-CDE TO DEDUCTION-REC.CNTRCT-CRRNCY-CDE
            getWorkFiles().write(3, false, ldaFcpl804.getDeduction_Rec());                                                                                                //Natural: WRITE WORK FILE 3 DEDUCTION-REC
            pnd_Ded_Rec_Ct.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #DED-REC-CT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  CREATE-DED-RECORDS
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=132 PS=62");
        Global.format(1, "LS=132 PS=62");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(46),"GROSS PAYMENT AMOUNT BY PAYMENT TYPE",NEWLINE,new 
            TabSetting(56),pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ColumnSpacing(10),"Fund Type",
        		pnd_Fund_Name,new ColumnSpacing(10),"Contractual/Amount",
        		pnd_Ws_Valuation_Pnd_Cntrct_Amt,new ColumnSpacing(10),"Dividend/Amount",
        		pnd_Ws_Valuation_Pnd_Dvdnd_Amt,new ColumnSpacing(10),"Total/Gross",
        		pnd_Ws_Valuation_Pnd_Gross_Amt);
    }
}
