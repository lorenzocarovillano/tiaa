/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:50 PM
**        * FROM NATURAL PROGRAM : Fcpp194c
************************************************************
**        * FILE NAME            : Fcpp194c.java
**        * CLASS NAME           : Fcpp194c
**        * INSTANCE NAME        : Fcpp194c
************************************************************
************************************************************************
* PROGRAM  : FCPP194C
* SYSTEM   : CPS
* TITLE    : IA DEATH SETTLEMENT REPORT
* GENERATED: SEP 14,93 AT 09:16 AM
* FUNCTION : THIS PROGRAM WILL GENERATE VARIOUS REGISTERS FOR
*            IA DEATH SETTLEMENTS.
*           ANY CHANGE TO THIS PGM MUST BE REVIEW IN FCPP194G
* HISTORY
*
* 02/27/01  MODIFIED BY T. TRAINOR   1.06B: RX RPT IDS IN HEADERS
* 12/22/98  MODIFIED BY A. YOUNG TO REVISE DEDUCTION PROCESSING.
*
* 12/12/98  MODIFIED BY A. YOUNG TO ACCEPT AND PROCESS MULTI-FUNDED
*           PAYMENTS.
*           MODIFIED TO PRINT CYCLE, LOGICAL END-OF-DAY, PHYSICAL END-
*           OF-DAY, AND MONTH-END REGISTERS.
*
* 06/12/96  MODIFIED BY A. YOUNG TO UTILIZE FUND CODE TABLE 'FCPL199A'.
*
* 06/04/96  MODIFIED BY A. YOUNG TO PRINT EFT NUMBERS AND INCREASE ALL
*           ACCUMULATOR FIELDS.
*           DECIMALS ---> (P15.2)
*           COUNTERS ---> (P9)
*
* 09/05/95  MODIFIED BY A. YOUNG TO INCREASE OVRPYMNT. DED. WORK FIELDS.
*           AND DELETE ALL UNUSED DEDUCTION FIELDS.
*
* 08/24/94  ADAPTED FROM FCPP298 BY A. YOUNG FOR USE IN THE ON-LINE
*           PROCESSING STREAM.
*
* 02/15/94  MODIFIED BY JUN TINIO TO ITEMIZE DEDUCTIONS BY INSTALLMENT
*           DATE.
*
* 01/12/98  COPY PRIOR TO CHANGE IN FCPP194V.
*           EXPAND UNIT-VALUE FROM 3 TO 4
*
* 09/13/00  DO 6 REPORTS IN ONE PASS -                     TT
*                                 PYMT     INS
*              CATEGORY           TYPE     TYPE
*              ---------------    ----     ----
*
*              1. LS REG DC       'L '     'A'
*              2. LS SPIA         'L '     'M'
*              3. LS PA-SELECT    'L '     'S'
*                    --------
*              4. PP REG DC       'PP'     'A'
*              5. PP SPIA         'PP'     'M'
*              6. PP PA-SELECT    'PP'     'S'
*
*        PYMT TYPE = CNTRCT-TYPE-CDE               (A2) POS= 28
*        INS TYPE  = CNTRCT-ANNTY-INS-TYPE         (A1) POS=674
*
*        --------------------------------------
*        ALSO: ALL CONSTRUCT REFERENCES REMOVED
*        --------------------------------------
*
* MODS:
*
* 1.04A: REMOVE 'Stock Units:  x,xxx  Unit Value:  x.xxxx' FROM
*        THE 'In Settlement Of:' LINE, AS THE MONEY VALUES QUOTED
*        REPRESENT AMALGAMATED SUMS FROM VARIOUS SOURCES, AND DO NOT
*        CORRESPOND TO A SINGLE UNIT VALUE CALCULATION.
*
* 1.05 : INV-ACCT-DCI-AMT / INV-ACCT-DPI-AMT
*        THE TWO ABOVE AMOUNTS ARE NOW ADDED TOGETHER AND REPORTED AS
*        "DCI/DPI". FORMERLY, THE DPI-AMT FIELD WAS EMPTY AND WAS NOT
*        REFERENCED.
* 1.05B: REMOVE VERSION ID FROM RPT HEADER
* 1.06 : RESET PAGE NUMBERS AND ADJUST HEADERS FOR (9) REPORTS
*          1 LUMP SUM REGULAR             6 CONT. PYMT PA-SELECT
*          2 LUMP SUM SPIA                7 LUMP SUM - ALL
*          3 LUMP SUM PA-SELECT           8 CONT. PYMT - ALL
*          4 CONT. PYMT REGULAR           9 ALL - LUMP SUM + CONT. PYMT
*          5 CONT. PYMT SPIA
* 1.06A: CONT. PYMT REG. HAS INS-TYPE 'P' ALSO (PERSONAL ANNUITY)
* 1.06B: RX RPT IDS IN HEADERS     T.TRAINOR
*
* 1.06C: IN DECIDE ON LINE 3325 #RPT-NDX = 1 WHEN CNTRCT-TYPE-CDE
*        IS NONE. 11/09/01
* RAMANA ANNE 03/01/2006 : INCREASED #DETAIL-LINES ARRAY FROM 100 TO 200
*                           DUE TO 1316 NATURAL ERROR
* 06/6/2006 : LANDRUM PAYEE MATCH.
*            - INCREASED CHECK NBR FROM N7 TO N10. FORMAT FOR REPORTS
*
* 04/9/2015 : INCREASED THE OCCURENCE OF DETAIL LINE ARRAY
*             TAG - MUKHERR1
* 4/2017    : JJG - PIN EXPANSION RESTOW
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp194c extends BLNatBase
{
    // Data Areas
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl190a ldaFcpl190a;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Program;
    private DbsField pnd_Vers_Id;
    private DbsField pnd_Input_Parm;
    private DbsField pnd_Rpt_Ndx;
    private DbsField pnd_Rpt_Ndx_Hdr;
    private DbsField pnd_Rpt_Ndx_Prev;
    private DbsField pnd_X_1;
    private DbsField pnd_X_2;
    private DbsField pnd_X_3;
    private DbsField pnd_Ndx;
    private DbsField pnd_Hdr_Ndx;
    private DbsField pnd_Hdr_Ndx_Lit;
    private DbsField pnd_Hdr_Pfx_Ndx;
    private DbsField pnd_Hdr_Pfx;
    private DbsField pnd_Eof_Sw;
    private DbsField pnd_Hdr_Sw;
    private DbsField pnd_Now;
    private DbsField pnd_Never;
    private DbsField pnd_Category_Code;

    private DbsGroup pnd_Category_Code__R_Field_1;
    private DbsField pnd_Category_Code_Pnd_Category_Settle_Type;
    private DbsField pnd_Category_Code_Pnd_Category_Insure_Type;
    private DbsField pnd_Rpt_Ind;
    private DbsField pnd_Hdr_Len_Max;
    private DbsField pnd_Header;
    private DbsField pnd_W_Fld;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Len;
    private DbsField pnd_Summary;
    private DbsField pnd_Rpt_Break;
    private DbsField pnd_Dollar_I;
    private DbsField pnd_Pi;
    private DbsField pnd_Ti;
    private DbsField pnd_Ln;
    private DbsField pnd_Ln1;
    private DbsField pnd_First;
    private DbsField pnd_Month_End;
    private DbsField pnd_Tiaa;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_Prcss_Seq_Nbr__R_Field_2;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Nbr;
    private DbsField pnd_Prev_Seq_Nbr;
    private DbsField pnd_Sub_Txt;
    private DbsField pnd_Sub_Txt_A;
    private DbsField pnd_Sub_Txt_M;
    private DbsField pnd_Dte_Txt;
    private DbsField pnd_No_Guar_Stck;
    private DbsField pnd_Init;
    private DbsField pnd_Sub_Prt;
    private DbsField pnd_Hdr_Dte;
    private DbsField pnd_O_Ftre_Ind;
    private DbsField pnd_O_Intrfce_Dte;
    private DbsField pnd_Cf_Txt;
    private DbsField pnd_Pay_Type_Num;
    private DbsField pnd_Acct_Chk_Dte_Txt;
    private DbsField pnd_Acct_Chk_Dte;

    private DbsGroup pnd_Dtl_Lines;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period;
    private DbsField pnd_Dtl_Lines_Pnd_Gross_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Ovrpt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Dcpi_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Dtl_Lines_Pnd_Rtb;

    private DbsGroup pnd_Item;
    private DbsField pnd_Item_Pnd_Gross_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Item_Pnd_Ovrpt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Dcpi_Amt;
    private DbsField pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt;

    private DbsGroup pnd_Intrfce;
    private DbsField pnd_Intrfce_Pnd_Gross_Amt;
    private DbsField pnd_Intrfce_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Intrfce_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Intrfce_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Intrfce_Pnd_Ovrpt;
    private DbsField pnd_Intrfce_Pnd_Inv_Acct_Dcpi_Amt;
    private DbsField pnd_Intrfce_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Intrfce_Pnd_Qty;

    private DbsGroup pnd_Misc;
    private DbsField pnd_Misc_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Misc_Pnd_Annt;
    private DbsField pnd_Misc_Pnd_Payee;
    private DbsField pnd_Misc_Pnd_Cntrct_Nbr;
    private DbsField pnd_Misc_Cntrct_Hold_Cde;
    private DbsField pnd_Misc_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Misc_Pymnt_Acctg_Dte;
    private DbsField pnd_Misc_Pymnt_Check_Dte;
    private DbsField pnd_Misc_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Misc_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Misc_Pymnt_Check_Nbr;
    private DbsField pnd_Misc_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Misc_Pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Misc_Pnd_Cge_Txt;
    private DbsField pnd_Misc_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_N10__R_Field_3;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_N7;

    private DbsGroup pnd_Check_Number_N10__R_Field_4;
    private DbsField pnd_Check_Number_N10_Pnd_Check_Number_A10;
    private DbsField pnd_Check_Datex;

    private DbsGroup pnd_Check_Datex__R_Field_5;
    private DbsField pnd_Check_Datex_Pnd_Check_Date;

    private DbsGroup pnd_Dte;
    private DbsField pnd_Dte_Pnd_Gross_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Dte_Pnd_Ovrpt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Dcpi_Amt;
    private DbsField pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Dte_Pnd_Qty;

    private DbsGroup pnd_Sub;
    private DbsField pnd_Sub_Pnd_Gross_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Sub_Pnd_Ovrpt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_Dcpi_Amt;
    private DbsField pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Sub_Pnd_Qty;

    private DbsGroup pnd_Sum;
    private DbsField pnd_Sum_Pnd_Current_A;
    private DbsField pnd_Sum_Pnd_Current_M;
    private DbsField pnd_Sum_Pnd_Future_A;
    private DbsField pnd_Sum_Pnd_Future_M;
    private DbsField pnd_Sum_Pnd_C_Qty;
    private DbsField pnd_Sum_Pnd_F_Qty;

    private DbsGroup pnd_Us_Sum;
    private DbsField pnd_Us_Sum_Pnd_Current_A;
    private DbsField pnd_Us_Sum_Pnd_Current_M;
    private DbsField pnd_Us_Sum_Pnd_Future_A;
    private DbsField pnd_Us_Sum_Pnd_Future_M;
    private DbsField pnd_Us_Sum_Pnd_C_Qty;
    private DbsField pnd_Us_Sum_Pnd_F_Qty;
    private DbsField pnd_T_Total_A;
    private DbsField pnd_T_Total_M;
    private DbsField pnd_C_Total;
    private DbsField pnd_F_Total;
    private DbsField pnd_T_Total;
    private DbsField pnd_Cqty;
    private DbsField pnd_Fqty;
    private DbsField pnd_Tqty;
    private DbsField pnd_Guar_Unit_Txt;
    private DbsField pnd_Div_Unit_Txt;
    private DbsField pnd_Month;
    private DbsField pnd_Year;
    private DbsField pnd_Printed;
    private DbsField pnd_Combined;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pymnt_Ftre_IndOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Vers_Id = localVariables.newFieldInRecord("pnd_Vers_Id", "#VERS_ID", FieldType.STRING, 6);
        pnd_Input_Parm = localVariables.newFieldInRecord("pnd_Input_Parm", "#INPUT-PARM", FieldType.STRING, 5);
        pnd_Rpt_Ndx = localVariables.newFieldInRecord("pnd_Rpt_Ndx", "#RPT_NDX", FieldType.PACKED_DECIMAL, 2);
        pnd_Rpt_Ndx_Hdr = localVariables.newFieldInRecord("pnd_Rpt_Ndx_Hdr", "#RPT_NDX_HDR", FieldType.NUMERIC, 2);
        pnd_Rpt_Ndx_Prev = localVariables.newFieldInRecord("pnd_Rpt_Ndx_Prev", "#RPT_NDX_PREV", FieldType.PACKED_DECIMAL, 2);
        pnd_X_1 = localVariables.newFieldInRecord("pnd_X_1", "#X_1", FieldType.PACKED_DECIMAL, 2);
        pnd_X_2 = localVariables.newFieldInRecord("pnd_X_2", "#X_2", FieldType.PACKED_DECIMAL, 2);
        pnd_X_3 = localVariables.newFieldInRecord("pnd_X_3", "#X_3", FieldType.PACKED_DECIMAL, 2);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.PACKED_DECIMAL, 2);
        pnd_Hdr_Ndx = localVariables.newFieldInRecord("pnd_Hdr_Ndx", "#HDR_NDX", FieldType.PACKED_DECIMAL, 2);
        pnd_Hdr_Ndx_Lit = localVariables.newFieldInRecord("pnd_Hdr_Ndx_Lit", "#HDR_NDX_LIT", FieldType.STRING, 3);
        pnd_Hdr_Pfx_Ndx = localVariables.newFieldInRecord("pnd_Hdr_Pfx_Ndx", "#HDR_PFX_NDX", FieldType.PACKED_DECIMAL, 2);
        pnd_Hdr_Pfx = localVariables.newFieldArrayInRecord("pnd_Hdr_Pfx", "#HDR_PFX", FieldType.STRING, 30, new DbsArrayController(1, 9));
        pnd_Eof_Sw = localVariables.newFieldInRecord("pnd_Eof_Sw", "#EOF_SW", FieldType.BOOLEAN, 1);
        pnd_Hdr_Sw = localVariables.newFieldInRecord("pnd_Hdr_Sw", "#HDR_SW", FieldType.BOOLEAN, 1);
        pnd_Now = localVariables.newFieldInRecord("pnd_Now", "#NOW", FieldType.BOOLEAN, 1);
        pnd_Never = localVariables.newFieldInRecord("pnd_Never", "#NEVER", FieldType.BOOLEAN, 1);
        pnd_Category_Code = localVariables.newFieldInRecord("pnd_Category_Code", "#CATEGORY-CODE", FieldType.STRING, 3);

        pnd_Category_Code__R_Field_1 = localVariables.newGroupInRecord("pnd_Category_Code__R_Field_1", "REDEFINE", pnd_Category_Code);
        pnd_Category_Code_Pnd_Category_Settle_Type = pnd_Category_Code__R_Field_1.newFieldInGroup("pnd_Category_Code_Pnd_Category_Settle_Type", "#CATEGORY-SETTLE-TYPE", 
            FieldType.STRING, 2);
        pnd_Category_Code_Pnd_Category_Insure_Type = pnd_Category_Code__R_Field_1.newFieldInGroup("pnd_Category_Code_Pnd_Category_Insure_Type", "#CATEGORY-INSURE-TYPE", 
            FieldType.STRING, 1);
        pnd_Rpt_Ind = localVariables.newFieldArrayInRecord("pnd_Rpt_Ind", "#RPT_IND", FieldType.BOOLEAN, 1, new DbsArrayController(1, 9));
        pnd_Hdr_Len_Max = localVariables.newFieldInRecord("pnd_Hdr_Len_Max", "#HDR_LEN_MAX", FieldType.PACKED_DECIMAL, 3);
        pnd_Header = localVariables.newFieldArrayInRecord("pnd_Header", "#HEADER", FieldType.STRING, 100, new DbsArrayController(1, 4));
        pnd_W_Fld = localVariables.newFieldInRecord("pnd_W_Fld", "#W-FLD", FieldType.STRING, 100);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.INTEGER, 2);
        pnd_Summary = localVariables.newFieldInRecord("pnd_Summary", "#SUMMARY", FieldType.BOOLEAN, 1);
        pnd_Rpt_Break = localVariables.newFieldInRecord("pnd_Rpt_Break", "#RPT_BREAK", FieldType.BOOLEAN, 1);
        pnd_Dollar_I = localVariables.newFieldInRecord("pnd_Dollar_I", "#$I", FieldType.INTEGER, 2);
        pnd_Pi = localVariables.newFieldInRecord("pnd_Pi", "#PI", FieldType.INTEGER, 2);
        pnd_Ti = localVariables.newFieldInRecord("pnd_Ti", "#TI", FieldType.INTEGER, 2);
        pnd_Ln = localVariables.newFieldInRecord("pnd_Ln", "#LN", FieldType.PACKED_DECIMAL, 4);
        pnd_Ln1 = localVariables.newFieldInRecord("pnd_Ln1", "#LN1", FieldType.PACKED_DECIMAL, 4);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Month_End = localVariables.newFieldInRecord("pnd_Month_End", "#MONTH-END", FieldType.BOOLEAN, 1);
        pnd_Tiaa = localVariables.newFieldInRecord("pnd_Tiaa", "#TIAA", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Prcss_Seq_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Prcss_Seq_Nbr", "#PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_Prcss_Seq_Nbr__R_Field_2 = localVariables.newGroupInRecord("pnd_Pymnt_Prcss_Seq_Nbr__R_Field_2", "REDEFINE", pnd_Pymnt_Prcss_Seq_Nbr);
        pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr = pnd_Pymnt_Prcss_Seq_Nbr__R_Field_2.newFieldInGroup("pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr", "#CURR-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Nbr = pnd_Pymnt_Prcss_Seq_Nbr__R_Field_2.newFieldInGroup("pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Nbr", "#INSTMNT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Seq_Nbr = localVariables.newFieldInRecord("pnd_Prev_Seq_Nbr", "#PREV-SEQ-NBR", FieldType.NUMERIC, 7);
        pnd_Sub_Txt = localVariables.newFieldInRecord("pnd_Sub_Txt", "#SUB-TXT", FieldType.STRING, 60);
        pnd_Sub_Txt_A = localVariables.newFieldInRecord("pnd_Sub_Txt_A", "#SUB-TXT-A", FieldType.STRING, 60);
        pnd_Sub_Txt_M = localVariables.newFieldInRecord("pnd_Sub_Txt_M", "#SUB-TXT-M", FieldType.STRING, 60);
        pnd_Dte_Txt = localVariables.newFieldInRecord("pnd_Dte_Txt", "#DTE-TXT", FieldType.STRING, 60);
        pnd_No_Guar_Stck = localVariables.newFieldInRecord("pnd_No_Guar_Stck", "#NO-GUAR-STCK", FieldType.BOOLEAN, 1);
        pnd_Init = localVariables.newFieldInRecord("pnd_Init", "#INIT", FieldType.BOOLEAN, 1);
        pnd_Sub_Prt = localVariables.newFieldInRecord("pnd_Sub_Prt", "#SUB-PRT", FieldType.BOOLEAN, 1);
        pnd_Hdr_Dte = localVariables.newFieldInRecord("pnd_Hdr_Dte", "#HDR-DTE", FieldType.STRING, 10);
        pnd_O_Ftre_Ind = localVariables.newFieldInRecord("pnd_O_Ftre_Ind", "#O-FTRE-IND", FieldType.STRING, 1);
        pnd_O_Intrfce_Dte = localVariables.newFieldInRecord("pnd_O_Intrfce_Dte", "#O-INTRFCE-DTE", FieldType.DATE);
        pnd_Cf_Txt = localVariables.newFieldInRecord("pnd_Cf_Txt", "#CF-TXT", FieldType.STRING, 9);
        pnd_Pay_Type_Num = localVariables.newFieldInRecord("pnd_Pay_Type_Num", "#PAY-TYPE-NUM", FieldType.NUMERIC, 10);
        pnd_Acct_Chk_Dte_Txt = localVariables.newFieldInRecord("pnd_Acct_Chk_Dte_Txt", "#ACCT-CHK-DTE-TXT", FieldType.STRING, 13);
        pnd_Acct_Chk_Dte = localVariables.newFieldInRecord("pnd_Acct_Chk_Dte", "#ACCT-CHK-DTE", FieldType.DATE);

        pnd_Dtl_Lines = localVariables.newGroupArrayInRecord("pnd_Dtl_Lines", "#DTL-LINES", new DbsArrayController(1, 9999));
        pnd_Dtl_Lines_Pnd_Inv_Acct = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct", "#INV-ACCT", FieldType.STRING, 8);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period", "#INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 3);
        pnd_Dtl_Lines_Pnd_Gross_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Dtl_Lines_Pnd_Ovrpt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Ovrpt", "#OVRPT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Dcpi_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Dcpi_Amt", "#INV-ACCT-DCPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Dtl_Lines_Pnd_Rtb = pnd_Dtl_Lines.newFieldInGroup("pnd_Dtl_Lines_Pnd_Rtb", "#RTB", FieldType.STRING, 5);

        pnd_Item = localVariables.newGroupInRecord("pnd_Item", "#ITEM");
        pnd_Item_Pnd_Gross_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Item_Pnd_Inv_Acct_State_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Item_Pnd_Ovrpt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Ovrpt", "#OVRPT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Item_Pnd_Inv_Acct_Dcpi_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Dcpi_Amt", "#INV-ACCT-DCPI-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Item.newFieldInGroup("pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Intrfce = localVariables.newGroupInRecord("pnd_Intrfce", "#INTRFCE");
        pnd_Intrfce_Pnd_Gross_Amt = pnd_Intrfce.newFieldInGroup("pnd_Intrfce_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Intrfce_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Intrfce.newFieldInGroup("pnd_Intrfce_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Intrfce_Pnd_Inv_Acct_State_Tax_Amt = pnd_Intrfce.newFieldInGroup("pnd_Intrfce_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Intrfce_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Intrfce.newFieldInGroup("pnd_Intrfce_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Intrfce_Pnd_Ovrpt = pnd_Intrfce.newFieldInGroup("pnd_Intrfce_Pnd_Ovrpt", "#OVRPT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Intrfce_Pnd_Inv_Acct_Dcpi_Amt = pnd_Intrfce.newFieldInGroup("pnd_Intrfce_Pnd_Inv_Acct_Dcpi_Amt", "#INV-ACCT-DCPI-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Intrfce_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Intrfce.newFieldInGroup("pnd_Intrfce_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Intrfce_Pnd_Qty = pnd_Intrfce.newFieldInGroup("pnd_Intrfce_Pnd_Qty", "#QTY", FieldType.PACKED_DECIMAL, 9);

        pnd_Misc = localVariables.newGroupInRecord("pnd_Misc", "#MISC");
        pnd_Misc_Cntrct_Ppcn_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 8);
        pnd_Misc_Pnd_Annt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Annt", "#ANNT", FieldType.STRING, 50);
        pnd_Misc_Pnd_Payee = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Payee", "#PAYEE", FieldType.STRING, 38);
        pnd_Misc_Pnd_Cntrct_Nbr = pnd_Misc.newFieldArrayInGroup("pnd_Misc_Pnd_Cntrct_Nbr", "#CNTRCT-NBR", FieldType.STRING, 8, new DbsArrayController(1, 
            5));
        pnd_Misc_Cntrct_Hold_Cde = pnd_Misc.newFieldInGroup("pnd_Misc_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);
        pnd_Misc_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Misc.newFieldInGroup("pnd_Misc_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 
            2);
        pnd_Misc_Pymnt_Acctg_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Misc_Pymnt_Check_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Misc_Pymnt_Settlmnt_Dte = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Misc_Pymnt_Check_Seq_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Misc_Pymnt_Check_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Misc_Pymnt_Check_Scrty_Nbr = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Misc_Pnd_Inv_Acct_Settl_Amt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Misc_Pnd_Inv_Acct_Unit_Qty = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Unit_Qty", "#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9, 
            3);
        pnd_Misc_Pnd_Inv_Acct_Unit_Value = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Inv_Acct_Unit_Value", "#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            10, 4);
        pnd_Misc_Pnd_Cge_Txt = pnd_Misc.newFieldInGroup("pnd_Misc_Pnd_Cge_Txt", "#CGE-TXT", FieldType.STRING, 7);
        pnd_Misc_Pymnt_Pay_Type_Req_Ind = pnd_Misc.newFieldInGroup("pnd_Misc_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.PACKED_DECIMAL, 
            2);
        pnd_Check_Number_N10 = localVariables.newFieldInRecord("pnd_Check_Number_N10", "#CHECK-NUMBER-N10", FieldType.NUMERIC, 10);

        pnd_Check_Number_N10__R_Field_3 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_3", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_N3 = pnd_Check_Number_N10__R_Field_3.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_N10_Pnd_Check_Number_N7 = pnd_Check_Number_N10__R_Field_3.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        pnd_Check_Number_N10__R_Field_4 = localVariables.newGroupInRecord("pnd_Check_Number_N10__R_Field_4", "REDEFINE", pnd_Check_Number_N10);
        pnd_Check_Number_N10_Pnd_Check_Number_A10 = pnd_Check_Number_N10__R_Field_4.newFieldInGroup("pnd_Check_Number_N10_Pnd_Check_Number_A10", "#CHECK-NUMBER-A10", 
            FieldType.STRING, 10);
        pnd_Check_Datex = localVariables.newFieldInRecord("pnd_Check_Datex", "#CHECK-DATEX", FieldType.STRING, 8);

        pnd_Check_Datex__R_Field_5 = localVariables.newGroupInRecord("pnd_Check_Datex__R_Field_5", "REDEFINE", pnd_Check_Datex);
        pnd_Check_Datex_Pnd_Check_Date = pnd_Check_Datex__R_Field_5.newFieldInGroup("pnd_Check_Datex_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);

        pnd_Dte = localVariables.newGroupInRecord("pnd_Dte", "#DTE");
        pnd_Dte_Pnd_Gross_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Dte_Pnd_Ovrpt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Ovrpt", "#OVRPT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Dte_Pnd_Inv_Acct_Dcpi_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Dcpi_Amt", "#INV-ACCT-DCPI-AMT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Dte_Pnd_Qty = pnd_Dte.newFieldInGroup("pnd_Dte_Pnd_Qty", "#QTY", FieldType.PACKED_DECIMAL, 9);

        pnd_Sub = localVariables.newGroupInRecord("pnd_Sub", "#SUB");
        pnd_Sub_Pnd_Gross_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Sub_Pnd_Ovrpt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Ovrpt", "#OVRPT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Sub_Pnd_Inv_Acct_Dcpi_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_Dcpi_Amt", "#INV-ACCT-DCPI-AMT", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            17, 2);
        pnd_Sub_Pnd_Qty = pnd_Sub.newFieldInGroup("pnd_Sub_Pnd_Qty", "#QTY", FieldType.PACKED_DECIMAL, 9);

        pnd_Sum = localVariables.newGroupArrayInRecord("pnd_Sum", "#SUM", new DbsArrayController(1, 9, 1, 3));
        pnd_Sum_Pnd_Current_A = pnd_Sum.newFieldInGroup("pnd_Sum_Pnd_Current_A", "#CURRENT-A", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Sum_Pnd_Current_M = pnd_Sum.newFieldInGroup("pnd_Sum_Pnd_Current_M", "#CURRENT-M", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Sum_Pnd_Future_A = pnd_Sum.newFieldInGroup("pnd_Sum_Pnd_Future_A", "#FUTURE-A", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Sum_Pnd_Future_M = pnd_Sum.newFieldInGroup("pnd_Sum_Pnd_Future_M", "#FUTURE-M", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Sum_Pnd_C_Qty = pnd_Sum.newFieldInGroup("pnd_Sum_Pnd_C_Qty", "#C-QTY", FieldType.PACKED_DECIMAL, 9);
        pnd_Sum_Pnd_F_Qty = pnd_Sum.newFieldInGroup("pnd_Sum_Pnd_F_Qty", "#F-QTY", FieldType.PACKED_DECIMAL, 9);

        pnd_Us_Sum = localVariables.newGroupArrayInRecord("pnd_Us_Sum", "#US-SUM", new DbsArrayController(1, 9));
        pnd_Us_Sum_Pnd_Current_A = pnd_Us_Sum.newFieldInGroup("pnd_Us_Sum_Pnd_Current_A", "#CURRENT-A", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Us_Sum_Pnd_Current_M = pnd_Us_Sum.newFieldInGroup("pnd_Us_Sum_Pnd_Current_M", "#CURRENT-M", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Us_Sum_Pnd_Future_A = pnd_Us_Sum.newFieldInGroup("pnd_Us_Sum_Pnd_Future_A", "#FUTURE-A", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Us_Sum_Pnd_Future_M = pnd_Us_Sum.newFieldInGroup("pnd_Us_Sum_Pnd_Future_M", "#FUTURE-M", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Us_Sum_Pnd_C_Qty = pnd_Us_Sum.newFieldInGroup("pnd_Us_Sum_Pnd_C_Qty", "#C-QTY", FieldType.PACKED_DECIMAL, 9);
        pnd_Us_Sum_Pnd_F_Qty = pnd_Us_Sum.newFieldInGroup("pnd_Us_Sum_Pnd_F_Qty", "#F-QTY", FieldType.PACKED_DECIMAL, 9);
        pnd_T_Total_A = localVariables.newFieldInRecord("pnd_T_Total_A", "#T-TOTAL-A", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_T_Total_M = localVariables.newFieldInRecord("pnd_T_Total_M", "#T-TOTAL-M", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_C_Total = localVariables.newFieldInRecord("pnd_C_Total", "#C-TOTAL", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_F_Total = localVariables.newFieldInRecord("pnd_F_Total", "#F-TOTAL", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_T_Total = localVariables.newFieldInRecord("pnd_T_Total", "#T-TOTAL", FieldType.PACKED_DECIMAL, 17, 2);
        pnd_Cqty = localVariables.newFieldInRecord("pnd_Cqty", "#CQTY", FieldType.PACKED_DECIMAL, 9);
        pnd_Fqty = localVariables.newFieldInRecord("pnd_Fqty", "#FQTY", FieldType.PACKED_DECIMAL, 9);
        pnd_Tqty = localVariables.newFieldInRecord("pnd_Tqty", "#TQTY", FieldType.PACKED_DECIMAL, 9);
        pnd_Guar_Unit_Txt = localVariables.newFieldInRecord("pnd_Guar_Unit_Txt", "#GUAR-UNIT-TXT", FieldType.STRING, 15);
        pnd_Div_Unit_Txt = localVariables.newFieldInRecord("pnd_Div_Unit_Txt", "#DIV-UNIT-TXT", FieldType.STRING, 11);
        pnd_Month = localVariables.newFieldInRecord("pnd_Month", "#MONTH", FieldType.STRING, 9);
        pnd_Year = localVariables.newFieldInRecord("pnd_Year", "#YEAR", FieldType.STRING, 4);
        pnd_Printed = localVariables.newFieldInRecord("pnd_Printed", "#PRINTED", FieldType.BOOLEAN, 1);
        pnd_Combined = localVariables.newFieldInRecord("pnd_Combined", "#COMBINED", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pymnt_Ftre_IndOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pymnt_Ftre_Ind_OLD", "Pymnt_Ftre_Ind_OLD", FieldType.STRING, 1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_Program.setInitialValue("FCPP194C");
        pnd_Vers_Id.setInitialValue("V1.06J");
        pnd_Rpt_Ndx_Prev.setInitialValue(1);
        pnd_Hdr_Pfx.getValue(1).setInitialValue("IA LUMP SUM");
        pnd_Hdr_Pfx.getValue(2).setInitialValue("IA LUMP SUM SPIA");
        pnd_Hdr_Pfx.getValue(3).setInitialValue("IA LUMP SUM PA-SELECT");
        pnd_Hdr_Pfx.getValue(4).setInitialValue("IA CONTINUED PAYMENT");
        pnd_Hdr_Pfx.getValue(5).setInitialValue("IA CONTINUED PAYMENT SPIA");
        pnd_Hdr_Pfx.getValue(6).setInitialValue("IA CONTINUED PAYMENT PA-SELECT");
        pnd_Hdr_Pfx.getValue(7).setInitialValue("IA LUMP SUM - ALL");
        pnd_Hdr_Pfx.getValue(8).setInitialValue("IA CONTINUED PAYMENT - ALL");
        pnd_Hdr_Pfx.getValue(9).setInitialValue("IA L/S & CONTINUED PYMT - ALL");
        pnd_Eof_Sw.setInitialValue(false);
        pnd_Hdr_Sw.setInitialValue(true);
        pnd_Now.setInitialValue(true);
        pnd_Never.setInitialValue(false);
        pnd_Hdr_Len_Max.setInitialValue(100);
        pnd_Dollar_I.setInitialValue(1);
        pnd_First.setInitialValue(true);
        pnd_Month_End.setInitialValue(false);
        pnd_Init.setInitialValue(true);
        pnd_Acct_Chk_Dte_Txt.setInitialValue(" ");
        pnd_Printed.setInitialValue(false);
        pnd_Combined.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp194c() throws Exception
    {
        super("Fcpp194c");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp194c|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                getReports().definePrinter(2, "PRT1");                                                                                                                    //Natural: FORMAT ( 0 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: DEFINE PRINTER ( PRT1 = 1 )
                //*                                                                                                                                                       //Natural: FORMAT ( PRT1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA = 'INFP9000'
                //*  ------------------------------------------------------------ *
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=MITL'_'"), new SignPosition (true), new InputPrompting(false),                //Natural: INPUT ( AD = MITL'_' SG = ON IP = OFF ZP = OFF ) 'Input Parm:' #INPUT-PARM
                    new ReportZeroPrint (false),"Input Parm:",pnd_Input_Parm);
                //*  12-12-98
                short decideConditionsMet706 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #INPUT-PARM;//Natural: VALUE 'DAILY', 'LEDGR', 'DAYND'
                if (condition((pnd_Input_Parm.equals("DAILY") || pnd_Input_Parm.equals("LEDGR") || pnd_Input_Parm.equals("DAYND"))))
                {
                    decideConditionsMet706++;
                    ignore();
                }                                                                                                                                                         //Natural: VALUE 'MONTH'
                else if (condition((pnd_Input_Parm.equals("MONTH"))))
                {
                    decideConditionsMet706++;
                    pnd_Month_End.setValue(true);                                                                                                                         //Natural: ASSIGN #MONTH-END := TRUE
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,"INPUT PARM IS EITHER BLANK OR INVALID.");                         //Natural: WRITE NOTITLE ///// 'INPUT PARM IS EITHER BLANK OR INVALID.'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(77);  if (true) return;                                                                                                             //Natural: TERMINATE 77
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  ------------------------------------------------------------ *
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE ( PRT1 )
                //*  RL
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
                sub_Get_Check_Formatting_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  ------------------------------------------------------------ *
                //*  READ THE EXTRACT FILE
                //*  ------------------------------------------------------------ *
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK 5 #RPT-EXT
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(5, ldaFcpl190a.getPnd_Rpt_Ext())))
                {
                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*  ------------------------------------------------------------ *
                    //*  ------------------------------------------------------------ *                                                                                   //Natural: AT END OF DATA
                    //BEFORE BREAK PROCESSING                                                                                                                             //Natural: BEFORE BREAK PROCESSING
                    pnd_Category_Code_Pnd_Category_Settle_Type.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde());                                                    //Natural: ASSIGN #CATEGORY-SETTLE-TYPE := CNTRCT-TYPE-CDE
                    pnd_Category_Code_Pnd_Category_Insure_Type.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type());                                              //Natural: ASSIGN #CATEGORY-INSURE-TYPE := CNTRCT-ANNTY-INS-TYPE
                    //*  1.06A: 'P' IS REG ALSO
                    //*  SO AVOID A CNTL BREAK
                    if (condition(pnd_Category_Code_Pnd_Category_Insure_Type.equals("P")))                                                                                //Natural: IF #CATEGORY-INSURE-TYPE = 'P' THEN
                    {
                        pnd_Category_Code_Pnd_Category_Insure_Type.setValue("A");                                                                                         //Natural: ASSIGN #CATEGORY-INSURE-TYPE := 'A'
                    }                                                                                                                                                     //Natural: END-IF
                    //END-BEFORE                                                                                                                                          //Natural: END-BEFORE
                    //*  ------------------------------------------------------------ *
                    //*  ------------------------------------------------------------ *                                                                                   //Natural: AT BREAK OF #RPT-EXT.PYMNT-INTRFCE-DTE
                    //*  ------------------------------------------------------------ *                                                                                   //Natural: AT BREAK OF #RPT-EXT.PYMNT-CHECK-DTE
                    //*  ------------------------------------------------------------ *                                                                                   //Natural: AT BREAK #RPT-EXT.PYMNT-FTRE-IND
                    //*  ------------------------------------------------------------ *                                                                                   //Natural: AT BREAK OF #CATEGORY-CODE
                    //*  ------------------------------------------------------------ *
                    //*                  RECORD PROCESS ROUTINE
                    //*  ------------------------------------------------------------ *
                    //*  ------------------------------------------------------------ *
                    pnd_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr());                                                                   //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := #RPT-EXT.PYMNT-PRCSS-SEQ-NBR
                    //*  1.06
                    if (condition(pnd_Init.getBoolean()))                                                                                                                 //Natural: IF #INIT
                    {
                        pnd_Rpt_Break.setValue(false);                                                                                                                    //Natural: ASSIGN #RPT_BREAK := FALSE
                        pnd_Init.setValue(false);                                                                                                                         //Natural: ASSIGN #INIT := FALSE
                        //*  2.00 TT
                                                                                                                                                                          //Natural: PERFORM SET_REPORT_NMBR
                        sub_Set_Report_Nmbr();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  12-12-98
                        if (condition(pnd_Input_Parm.equals("LEDGR")))                                                                                                    //Natural: IF #INPUT-PARM = 'LEDGR'
                        {
                            pnd_Hdr_Dte.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte(),new ReportEditMask("MM/DD/YYYY"));                                  //Natural: MOVE EDITED #RPT-EXT.PYMNT-INTRFCE-DTE ( EM = MM/DD/YYYY ) TO #HDR-DTE
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Hdr_Dte.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                    //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #HDR-DTE
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_O_Ftre_Ind.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());                                                                             //Natural: ASSIGN #O-FTRE-IND := #RPT-EXT.PYMNT-FTRE-IND
                        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().equals("F")))                                                                           //Natural: IF #RPT-EXT.PYMNT-FTRE-IND = 'F'
                        {
                            pnd_Cf_Txt.setValue("(FUTURE)");                                                                                                              //Natural: ASSIGN #CF-TXT := '(FUTURE)'
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Cf_Txt.setValue("(CURRENT)");                                                                                                             //Natural: ASSIGN #CF-TXT := '(CURRENT)'
                            //*  1.03A - SET SEP. INDEX
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Hdr_Pfx_Ndx.setValue(pnd_Rpt_Ndx);                                                                                                            //Natural: ASSIGN #HDR_PFX_NDX := #RPT_NDX
                        //*  1.02A
                        //*  1.02H THIS MIGHT HELP
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
                        sub_Initialize_Header();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        getReports().newPage(new ReportSpecification(0));                                                                                                 //Natural: NEWPAGE ( PRT1 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_First.getBoolean()))                                                                                                                //Natural: IF #FIRST
                    {
                        pnd_First.setValue(false);                                                                                                                        //Natural: ASSIGN #FIRST := FALSE
                        pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                              //Natural: ASSIGN #PREV-SEQ-NBR := #CURR-SEQ-NBR
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Prev_Seq_Nbr.equals(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr)))                                                                     //Natural: IF #PREV-SEQ-NBR = #CURR-SEQ-NBR
                    {
                                                                                                                                                                          //Natural: PERFORM BUILD-DETAIL-LINES
                        sub_Build_Detail_Lines();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ITEM-SUB-TOTALS
                        sub_Calculate_Item_Sub_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                        sub_Print_Detail_Lines();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM BUILD-DETAIL-LINES
                        sub_Build_Detail_Lines();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                                  //Natural: ASSIGN #PREV-SEQ-NBR := #CURR-SEQ-NBR
                    //*  1.2D - ISSUE 'no payments processed msg'
                    readWork01Pymnt_Ftre_IndOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind());                                                                    //Natural: END-WORK
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                    pnd_Eof_Sw.setValue(true);                                                                                                                            //Natural: ASSIGN #EOF_SW := TRUE
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                //*  ------------------------------------------------------------ *
                //*                   END OF FILE
                //*  ------------------------------------------------------------ *
                //*  --------------------------------------- *
                //*    PRINT THE SUPER GRAND SUMMARIES
                //*   FOR LUMP SUM AND THEN PERIODIC PAYMENT
                //*        AND THEN EV'rythin'
                //*  --------------------------------------- *
                pnd_Rpt_Ndx.setValue(6);                                                                                                                                  //Natural: ASSIGN #RPT_NDX := 6
                //*  FOR ANY REMAINING EMPTY CATEGORIES
                                                                                                                                                                          //Natural: PERFORM CHK_EMPTY_CATEGORY
                sub_Chk_Empty_Category();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                FOR01:                                                                                                                                                    //Natural: FOR #PI 1 TO 3
                for (pnd_Pi.setValue(1); condition(pnd_Pi.lessOrEqual(3)); pnd_Pi.nadd(1))
                {
                    //*    LUMP SUM CATEGORIES
                    pnd_Sum_Pnd_Current_A.getValue(7,pnd_Pi).nadd(pnd_Sum_Pnd_Current_A.getValue(1,":",3,pnd_Pi));                                                        //Natural: ADD #SUM.#CURRENT-A ( 1:3,#PI ) TO #SUM.#CURRENT-A ( 7,#PI )
                    pnd_Sum_Pnd_Current_M.getValue(7,pnd_Pi).nadd(pnd_Sum_Pnd_Current_M.getValue(1,":",3,pnd_Pi));                                                        //Natural: ADD #SUM.#CURRENT-M ( 1:3,#PI ) TO #SUM.#CURRENT-M ( 7,#PI )
                    pnd_Sum_Pnd_Future_A.getValue(7,pnd_Pi).nadd(pnd_Sum_Pnd_Future_A.getValue(1,":",3,pnd_Pi));                                                          //Natural: ADD #SUM.#FUTURE-A ( 1:3,#PI ) TO #SUM.#FUTURE-A ( 7,#PI )
                    pnd_Sum_Pnd_Future_M.getValue(7,pnd_Pi).nadd(pnd_Sum_Pnd_Future_M.getValue(1,":",3,pnd_Pi));                                                          //Natural: ADD #SUM.#FUTURE-M ( 1:3,#PI ) TO #SUM.#FUTURE-M ( 7,#PI )
                    pnd_Sum_Pnd_C_Qty.getValue(7,pnd_Pi).nadd(pnd_Sum_Pnd_C_Qty.getValue(1,":",3,pnd_Pi));                                                                //Natural: ADD #SUM.#C-QTY ( 1:3,#PI ) TO #SUM.#C-QTY ( 7,#PI )
                    pnd_Sum_Pnd_F_Qty.getValue(7,pnd_Pi).nadd(pnd_Sum_Pnd_F_Qty.getValue(1,":",3,pnd_Pi));                                                                //Natural: ADD #SUM.#F-QTY ( 1:3,#PI ) TO #SUM.#F-QTY ( 7,#PI )
                    //*    PERIODIC PAYMENT CATEGORIES
                    pnd_Sum_Pnd_Current_A.getValue(8,pnd_Pi).nadd(pnd_Sum_Pnd_Current_A.getValue(4,":",6,pnd_Pi));                                                        //Natural: ADD #SUM.#CURRENT-A ( 4:6,#PI ) TO #SUM.#CURRENT-A ( 8,#PI )
                    pnd_Sum_Pnd_Current_M.getValue(8,pnd_Pi).nadd(pnd_Sum_Pnd_Current_M.getValue(4,":",6,pnd_Pi));                                                        //Natural: ADD #SUM.#CURRENT-M ( 4:6,#PI ) TO #SUM.#CURRENT-M ( 8,#PI )
                    pnd_Sum_Pnd_Future_A.getValue(8,pnd_Pi).nadd(pnd_Sum_Pnd_Future_A.getValue(4,":",6,pnd_Pi));                                                          //Natural: ADD #SUM.#FUTURE-A ( 4:6,#PI ) TO #SUM.#FUTURE-A ( 8,#PI )
                    pnd_Sum_Pnd_Future_M.getValue(8,pnd_Pi).nadd(pnd_Sum_Pnd_Future_M.getValue(4,":",6,pnd_Pi));                                                          //Natural: ADD #SUM.#FUTURE-M ( 4:6,#PI ) TO #SUM.#FUTURE-M ( 8,#PI )
                    pnd_Sum_Pnd_C_Qty.getValue(8,pnd_Pi).nadd(pnd_Sum_Pnd_C_Qty.getValue(4,":",6,pnd_Pi));                                                                //Natural: ADD #SUM.#C-QTY ( 4:6,#PI ) TO #SUM.#C-QTY ( 8,#PI )
                    pnd_Sum_Pnd_F_Qty.getValue(8,pnd_Pi).nadd(pnd_Sum_Pnd_F_Qty.getValue(4,":",6,pnd_Pi));                                                                //Natural: ADD #SUM.#F-QTY ( 4:6,#PI ) TO #SUM.#F-QTY ( 8,#PI )
                    //*    LUMP SUM TOT & PER PMT TOT CATEGORIES
                    pnd_Sum_Pnd_Current_A.getValue(9,pnd_Pi).nadd(pnd_Sum_Pnd_Current_A.getValue(7,":",8,pnd_Pi));                                                        //Natural: ADD #SUM.#CURRENT-A ( 7:8,#PI ) TO #SUM.#CURRENT-A ( 9,#PI )
                    pnd_Sum_Pnd_Current_M.getValue(9,pnd_Pi).nadd(pnd_Sum_Pnd_Current_M.getValue(7,":",8,pnd_Pi));                                                        //Natural: ADD #SUM.#CURRENT-M ( 7:8,#PI ) TO #SUM.#CURRENT-M ( 9,#PI )
                    pnd_Sum_Pnd_Future_A.getValue(9,pnd_Pi).nadd(pnd_Sum_Pnd_Future_A.getValue(7,":",8,pnd_Pi));                                                          //Natural: ADD #SUM.#FUTURE-A ( 7:8,#PI ) TO #SUM.#FUTURE-A ( 9,#PI )
                    pnd_Sum_Pnd_Future_M.getValue(9,pnd_Pi).nadd(pnd_Sum_Pnd_Future_M.getValue(7,":",8,pnd_Pi));                                                          //Natural: ADD #SUM.#FUTURE-M ( 7:8,#PI ) TO #SUM.#FUTURE-M ( 9,#PI )
                    pnd_Sum_Pnd_C_Qty.getValue(9,pnd_Pi).nadd(pnd_Sum_Pnd_C_Qty.getValue(7,":",8,pnd_Pi));                                                                //Natural: ADD #SUM.#C-QTY ( 7:8,#PI ) TO #SUM.#C-QTY ( 9,#PI )
                    pnd_Sum_Pnd_F_Qty.getValue(9,pnd_Pi).nadd(pnd_Sum_Pnd_F_Qty.getValue(7,":",8,pnd_Pi));                                                                //Natural: ADD #SUM.#F-QTY ( 7:8,#PI ) TO #SUM.#F-QTY ( 9,#PI )
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  -- DEBUG --
                //*  POINT TO GRAND TOTAL DIMENSION 1.06G
                //*  1.06J - RESET NEXT REPORT TO PAGE 1 START
                                                                                                                                                                          //Natural: PERFORM DUMP_#SUM
                sub_Dump_Pnd_Sum();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Summary.setValue(true);                                                                                                                               //Natural: ASSIGN #SUMMARY := TRUE
                pnd_Rpt_Ndx.setValue(7);                                                                                                                                  //Natural: ASSIGN #RPT_NDX := #HDR_PFX_NDX := 7
                pnd_Hdr_Pfx_Ndx.setValue(7);
                //*  ----------------------- *
                getReports().getPageNumberDbs(2).setValue(0);                                                                                                             //Natural: ASSIGN *PAGE-NUMBER ( PRT1 ) := 0
                //*  ----------------------- *
                pnd_Header.getValue(2).setValue("IA LUMP SUM DEATH SETTLEMENTS - SUMMARY TOTALS - ALL TYPES");                                                            //Natural: ASSIGN #HEADER ( 2 ) := 'IA LUMP SUM DEATH SETTLEMENTS - SUMMARY TOTALS - ALL TYPES'
                if (condition(pnd_Rpt_Ind.getValue(1).getBoolean() || pnd_Rpt_Ind.getValue(2).getBoolean() || pnd_Rpt_Ind.getValue(3).getBoolean()))                      //Natural: IF #RPT_IND ( 1 ) OR #RPT_IND ( 2 ) OR #RPT_IND ( 3 ) THEN
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY
                    sub_Print_Summary();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().newPage(new ReportSpecification(0));                                                                                                     //Natural: NEWPAGE ( PRT1 )
                    if (condition(Global.isEscape())){return;}
                    getReports().write(2, NEWLINE,NEWLINE,new TabSetting(35),"*",new RepeatItem(56));                                                                     //Natural: WRITE ( PRT1 ) // 35T '*' ( 56 )
                    if (Global.isEscape()) return;
                    getReports().write(2, new TabSetting(35),"********** NO LUMP-SUM","DEATH CLAIMS PROCESSED **********");                                               //Natural: WRITE ( PRT1 ) 35T '********** NO LUMP-SUM' 'DEATH CLAIMS PROCESSED **********'
                    if (Global.isEscape()) return;
                    getReports().write(2, new TabSetting(35),"*",new RepeatItem(56));                                                                                     //Natural: WRITE ( PRT1 ) 35T '*' ( 56 )
                    if (Global.isEscape()) return;
                    //*  POINT TO GRAND TOTAL DIMENSION 1.06G
                    //*  1.06J - RESET NEXT REPORT TO PAGE 1 START
                }                                                                                                                                                         //Natural: END-IF
                pnd_Rpt_Ndx.setValue(8);                                                                                                                                  //Natural: ASSIGN #RPT_NDX := #HDR_PFX_NDX := 8
                pnd_Hdr_Pfx_Ndx.setValue(8);
                //*  ----------------------- *
                getReports().getPageNumberDbs(2).setValue(0);                                                                                                             //Natural: ASSIGN *PAGE-NUMBER ( PRT1 ) := 0
                //*  ----------------------- *
                pnd_Header.getValue(2).setValue("IA PERIODIC PAYMENT DEATH SETTLEMENTS - SUMMARY TOTALS - ALL TYPES");                                                    //Natural: ASSIGN #HEADER ( 2 ) := 'IA PERIODIC PAYMENT DEATH SETTLEMENTS - SUMMARY TOTALS - ALL TYPES'
                if (condition(pnd_Rpt_Ind.getValue(4).getBoolean() || pnd_Rpt_Ind.getValue(5).getBoolean() || pnd_Rpt_Ind.getValue(6).getBoolean()))                      //Natural: IF #RPT_IND ( 4 ) OR #RPT_IND ( 5 ) OR #RPT_IND ( 6 ) THEN
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY
                    sub_Print_Summary();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().newPage(new ReportSpecification(0));                                                                                                     //Natural: NEWPAGE ( PRT1 )
                    if (condition(Global.isEscape())){return;}
                    getReports().write(2, NEWLINE,NEWLINE,new TabSetting(35),"*",new RepeatItem(65));                                                                     //Natural: WRITE ( PRT1 ) // 35T '*' ( 65 )
                    if (Global.isEscape()) return;
                    getReports().write(2, new TabSetting(35),"********** NO CONTINUED PAYMENT","DEATH CLAIMS PROCESSED **********");                                      //Natural: WRITE ( PRT1 ) 35T '********** NO CONTINUED PAYMENT' 'DEATH CLAIMS PROCESSED **********'
                    if (Global.isEscape()) return;
                    getReports().write(2, new TabSetting(35),"*",new RepeatItem(65));                                                                                     //Natural: WRITE ( PRT1 ) 35T '*' ( 65 )
                    if (Global.isEscape()) return;
                    //*  POINT TO GRAND TOTAL DIMENSION 1.06G
                    //*  1.06J - RESET NEXT REPORT TO PAGE 1 START
                }                                                                                                                                                         //Natural: END-IF
                pnd_Header.getValue(2).setValue("IA L/S & PP DEATH SETTLEMENTS - SUMMARY TOTALS - ALL TYPES");                                                            //Natural: ASSIGN #HEADER ( 2 ) := 'IA L/S & PP DEATH SETTLEMENTS - SUMMARY TOTALS - ALL TYPES'
                pnd_Rpt_Ndx.setValue(9);                                                                                                                                  //Natural: ASSIGN #RPT_NDX := #HDR_PFX_NDX := 9
                pnd_Hdr_Pfx_Ndx.setValue(9);
                //*  ----------------------- *
                getReports().getPageNumberDbs(2).setValue(0);                                                                                                             //Natural: ASSIGN *PAGE-NUMBER ( PRT1 ) := 0
                //*  ----------------------- *
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY
                sub_Print_Summary();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(0, ReportOption.NOTITLE,"*** End of Program",Global.getPROGRAM(),pnd_Vers_Id,"***");                                                   //Natural: WRITE '*** End of Program' *PROGRAM #VERS_ID '***'
                if (Global.isEscape()) return;
                //*  ------------------------------------------------------------ *
                //*  == END OF PROGRAM EXECUTION ==
                //*  ------------------------------------------------------------ *
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-REPORT
                //*  ----------------------------------- *
                //*    END OF CODE SEGMENT
                //*  ----------------------------------- *
                //*  ----------------------- *
                //*  ------------- *
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET_REPORT_NMBR
                //* * NOTE: FOR THE FOLLOWING, SEE 'BEFORE BREAK PROCESSING' CLAUSE !!!
                //*  ------------- *
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHK_EMPTY_CATEGORY
                //* **************************************
                //*  --------------------------------------------
                //*   IDENTIFY ANY PAYMENT CATEGORY FOR WHICH NO RECORDS WERE FOUND
                //*  --------------------------------------------
                //* *  #RPT_IND(#NDX) := TRUE
                //* *  NEWPAGE(PRT1)
                //*  ------------------------- *
                //* * #HDR_SW := TRUE
                //*  ------------- *
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-HEADER
                //* **************************************
                //*  --------------*
                //* **************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CENTER_HEADER_TXT
                //* **************************************
                //*  --------------*
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-DETAIL-LINES
                //* ***********************************************************************
                //*  --------------*
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-MISCELLANEOUS-DATA
                //*  --------------*
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-ITEM-SUB-TOTALS
                //* ***********************************************************************
                //*  --------------*
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DTE-SUB-TOTAL
                //*  --------------*
                //* ***********************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-INTRFCE-DTE-SUB-TOTAL
                //*  --------------*
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUB-TOTAL
                //* ***********************************************************************
                //*  IF NOT #MONTH-END
                //*    PERFORM INITIALIZE-TXT
                //*    #SUB-PRT := TRUE
                //*  ELSE
                //*    COMPRESS #SUB-TXT 'GRAND TOTALS:' INTO  #SUB-TXT
                //*  END-IF
                //*  -- 1.02E --
                //*  --------------*
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-HEADER
                //* ***********************************************************************
                //*  ---------------*
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-TXT
                //*  ---------------*
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL-LINES
                //*      103T #DTL-LINES.#INV-ACCT-DCPI-AMT(#I)(EM=ZZ,ZZ9.99)
                //* *********************** RL PAYEE MATCH END ***************************
                //* *IF #MISC.PYMNT-PAY-TYPE-REQ-IND = 1
                //* *  #PAY-TYPE-NUM := #MISC.PYMNT-CHECK-NBR
                //* *ELSE
                //* *  #PAY-TYPE-NUM := #MISC.PYMNT-CHECK-SCRTY-NBR
                //* *END-IF
                //*  103T #ITEM.#INV-ACCT-DCPI-AMT(EM=ZZ,ZZ9.99)
                //*  ---------------*
                //* ***************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BUILD-SUMMARY-DATA
                //* ***************************************
                //*  ---------------*
                //* ***********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY
                //* ***********************************************************************
                //*  ---------------------------------------------------------------- *
                //*  ---------------------------------------------------------------- *
                //*  ---------------------------------------------------------------- *
                //*  -V1.03 ----------- END OF REPORT MARKER ------------------
                //*  ------------- *
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DUMP_#SUM
                //*  ------------- *
                //* *********************** RL BEGIN - PAYEE MATCH *********** JUN 06,2006
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
                //*  RL PAYEE MATCH
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //*  RL PAYEE MATCH
                //* ************************** RL END-PAYEE MATCH *************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_End_Report() throws Exception                                                                                                                        //Natural: END-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  ----------------------------------- *
        //*   FF MOVED HERE FROM AT BREAK FOR INS-TYPE (V2.01A)
        //*  ----------------------------------- *
        //*  12-12-98
        if (condition(pnd_Month_End.getBoolean()))                                                                                                                        //Natural: IF #MONTH-END
        {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ITEM-SUB-TOTALS
            sub_Calculate_Item_Sub_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
            sub_Print_Detail_Lines();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  1.03A - SET SEP. INDEX
                                                                                                                                                                          //Natural: PERFORM PRINT-SUB-TOTAL
        sub_Print_Sub_Total();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Hdr_Pfx_Ndx.setValue(pnd_Rpt_Ndx);                                                                                                                            //Natural: ASSIGN #HDR_PFX_NDX := #RPT_NDX
        //*   1.02A
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
        sub_Initialize_Header();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_First.setValue(true);                                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
        pnd_Summary.setValue(true);                                                                                                                                       //Natural: ASSIGN #SUMMARY := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY
        sub_Print_Summary();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_I.resetInitial();                                                                                                                                             //Natural: RESET INITIAL #I #J #LEN #SUMMARY #$I #PI #TI #LN #LN1 #FIRST #MONTH-END #TIAA #PYMNT-PRCSS-SEQ-NBR #PREV-SEQ-NBR #SUB-TXT #SUB-TXT-A #SUB-TXT-M #DTE-TXT #NO-GUAR-STCK #INIT #SUB-PRT #HDR-DTE #O-FTRE-IND #O-INTRFCE-DTE #CF-TXT #PAY-TYPE-NUM #ACCT-CHK-DTE-TXT #ACCT-CHK-DTE
        pnd_J.resetInitial();
        pnd_Len.resetInitial();
        pnd_Summary.resetInitial();
        pnd_Dollar_I.resetInitial();
        pnd_Pi.resetInitial();
        pnd_Ti.resetInitial();
        pnd_Ln.resetInitial();
        pnd_Ln1.resetInitial();
        pnd_First.resetInitial();
        pnd_Month_End.resetInitial();
        pnd_Tiaa.resetInitial();
        pnd_Pymnt_Prcss_Seq_Nbr.resetInitial();
        pnd_Prev_Seq_Nbr.resetInitial();
        pnd_Sub_Txt.resetInitial();
        pnd_Sub_Txt_A.resetInitial();
        pnd_Sub_Txt_M.resetInitial();
        pnd_Dte_Txt.resetInitial();
        pnd_No_Guar_Stck.resetInitial();
        pnd_Init.resetInitial();
        pnd_Sub_Prt.resetInitial();
        pnd_Hdr_Dte.resetInitial();
        pnd_O_Ftre_Ind.resetInitial();
        pnd_O_Intrfce_Dte.resetInitial();
        pnd_Cf_Txt.resetInitial();
        pnd_Pay_Type_Num.resetInitial();
        pnd_Acct_Chk_Dte_Txt.resetInitial();
        pnd_Acct_Chk_Dte.resetInitial();
        if (condition(pnd_Rpt_Ndx.equals(4) && pnd_Never.getBoolean()))                                                                                                   //Natural: IF #RPT_NDX = 4 AND #NEVER
        {
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(100));                                                                                  //Natural: WRITE / '-' ( 100 )
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"BEFORE RE-INIT");                                                                                                 //Natural: WRITE 'BEFORE RE-INIT'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DUMP_#SUM
            sub_Dump_Pnd_Sum();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*                            *DEBUG*
        if (condition(pnd_Rpt_Ndx.equals(4) && pnd_Never.getBoolean()))                                                                                                   //Natural: IF #RPT_NDX = 4 AND #NEVER
        {
            getReports().write(0, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(100));                                                                                  //Natural: WRITE / '-' ( 100 )
            if (Global.isEscape()) return;
            getReports().write(0, ReportOption.NOTITLE,"AFTER  RE-INIT");                                                                                                 //Natural: WRITE 'AFTER  RE-INIT'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM DUMP_#SUM
            sub_Dump_Pnd_Sum();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Dte.resetInitial();                                                                                                                                           //Natural: RESET INITIAL #DTE #SUB
        pnd_Sub.resetInitial();
        //*  1.06J - RESET NEXT REPORT TO PAGE 1 START
        pnd_T_Total_A.resetInitial();                                                                                                                                     //Natural: RESET INITIAL #T-TOTAL-A #T-TOTAL-M #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY #GUAR-UNIT-TXT #DIV-UNIT-TXT #MONTH #YEAR #PRINTED #COMBINED
        pnd_T_Total_M.resetInitial();
        pnd_C_Total.resetInitial();
        pnd_F_Total.resetInitial();
        pnd_T_Total.resetInitial();
        pnd_Cqty.resetInitial();
        pnd_Fqty.resetInitial();
        pnd_Tqty.resetInitial();
        pnd_Guar_Unit_Txt.resetInitial();
        pnd_Div_Unit_Txt.resetInitial();
        pnd_Month.resetInitial();
        pnd_Year.resetInitial();
        pnd_Printed.resetInitial();
        pnd_Combined.resetInitial();
        getReports().getPageNumberDbs(2).setValue(0);                                                                                                                     //Natural: ASSIGN *PAGE-NUMBER ( PRT1 ) := 0
        //*  ----------------------- *
        //*  ------------- *
    }
    private void sub_Set_Report_Nmbr() throws Exception                                                                                                                   //Natural: SET_REPORT_NMBR
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        //*  AMC 11/9/01
        //* L/S SPIA
        //* L/S PA-S
        //* PP REG
        //* PP SPIA
        //* PP PA-S
        //*  AMC 11-9-01
        short decideConditionsMet1154 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-TYPE-CDE = 'L ' AND ( CNTRCT-ANNTY-INS-TYPE = 'A' OR CNTRCT-ANNTY-INS-TYPE = 'I' )
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L ") && (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().equals("A") || 
            ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().equals("I")))))
        {
            decideConditionsMet1154++;
            pnd_Rpt_Ndx.setValue(1);                                                                                                                                      //Natural: ASSIGN #RPT_NDX := 1
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'L ' AND CNTRCT-ANNTY-INS-TYPE = 'M'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L ") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().equals("M")))
        {
            decideConditionsMet1154++;
            pnd_Rpt_Ndx.setValue(2);                                                                                                                                      //Natural: ASSIGN #RPT_NDX := 2
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'L ' AND CNTRCT-ANNTY-INS-TYPE = 'S'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L ") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().equals("S")))
        {
            decideConditionsMet1154++;
            pnd_Rpt_Ndx.setValue(3);                                                                                                                                      //Natural: ASSIGN #RPT_NDX := 3
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'PP' AND CNTRCT-ANNTY-INS-TYPE = 'A'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("PP") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().equals("A")))
        {
            decideConditionsMet1154++;
            pnd_Rpt_Ndx.setValue(4);                                                                                                                                      //Natural: ASSIGN #RPT_NDX := 4
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'PP' AND CNTRCT-ANNTY-INS-TYPE = 'M'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("PP") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().equals("M")))
        {
            decideConditionsMet1154++;
            pnd_Rpt_Ndx.setValue(5);                                                                                                                                      //Natural: ASSIGN #RPT_NDX := 5
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'PP' AND CNTRCT-ANNTY-INS-TYPE = 'S'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("PP") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type().equals("S")))
        {
            decideConditionsMet1154++;
            pnd_Rpt_Ndx.setValue(6);                                                                                                                                      //Natural: ASSIGN #RPT_NDX := 6
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            pnd_Rpt_Ndx.setValue(1);                                                                                                                                      //Natural: ASSIGN #RPT_NDX := 1
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Rpt_Ind.getValue(pnd_Rpt_Ndx).setValue(true);                                                                                                                 //Natural: ASSIGN #RPT_IND ( #RPT_NDX ) := TRUE
        //*  1.2D
                                                                                                                                                                          //Natural: PERFORM CHK_EMPTY_CATEGORY
        sub_Chk_Empty_Category();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  ------------- *
        //*    SET_REPORT_NMBR
    }
    //*  1.2D - NOW A SEPARATE ROUTINE
    //*  1.02H
    private void sub_Chk_Empty_Category() throws Exception                                                                                                                //Natural: CHK_EMPTY_CATEGORY
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #NDX = #RPT_NDX_PREV TO #RPT_NDX
        for (pnd_Ndx.setValue(pnd_Rpt_Ndx_Prev); condition(pnd_Ndx.lessOrEqual(pnd_Rpt_Ndx)); pnd_Ndx.nadd(1))
        {
            //*  1.03A - SET SEP. INDEX
            if (condition(! (pnd_Rpt_Ind.getValue(pnd_Ndx).equals(true))))                                                                                                //Natural: IF NOT #RPT_IND ( #NDX ) THEN
            {
                pnd_Hdr_Pfx_Ndx.setValue(pnd_Ndx);                                                                                                                        //Natural: ASSIGN #HDR_PFX_NDX := #NDX
                //*   1.03A
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
                sub_Initialize_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( PRT1 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //* *  #HDR_SW := FALSE
                //*     13 + 1 + 30 + 1 + 36 = 81
                getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(35),"*",new RepeatItem(81));                                                         //Natural: WRITE ( PRT1 ) //// 35T '*' ( 81 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(2, new TabSetting(35),"********** NO",pnd_Hdr_Pfx.getValue(pnd_Ndx),"DEATH PAYMENTS PROCESSED. **********");                           //Natural: WRITE ( PRT1 ) 35T '********** NO' #HDR_PFX ( #NDX ) 'DEATH PAYMENTS PROCESSED. **********'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  1.06J - RESET NEXT REPORT TO PAGE 1 START
                getReports().write(2, new TabSetting(35),"*",new RepeatItem(81));                                                                                         //Natural: WRITE ( PRT1 ) 35T '*' ( 81 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().getPageNumberDbs(2).setValue(0);                                                                                                             //Natural: ASSIGN *PAGE-NUMBER ( PRT1 ) := 0
                //*  ------------------------- *
            }                                                                                                                                                             //Natural: END-IF
            //*  1.02H
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Rpt_Ndx_Prev.setValue(pnd_Rpt_Ndx);                                                                                                                           //Natural: ASSIGN #RPT_NDX_PREV := #RPT_NDX
        //*  ------------- *
        //*    CHK_EMPTY_CATEGORY
    }
    //*  1.06F
    private void sub_Initialize_Header() throws Exception                                                                                                                 //Natural: INITIALIZE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Header.getValue(1).setValue("Consolidated Payment System");                                                                                                   //Natural: ASSIGN #HEADER ( 1 ) := 'Consolidated Payment System'
        pnd_Rpt_Ndx_Hdr.setValue(pnd_Hdr_Pfx_Ndx);                                                                                                                        //Natural: ASSIGN #RPT_NDX_HDR := #HDR_PFX_NDX
        //*  1.06D
        pnd_Hdr_Ndx_Lit.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "R", pnd_Rpt_Ndx_Hdr));                                                                  //Natural: COMPRESS 'R' #RPT_NDX_HDR INTO #HDR_NDX_LIT LEAVING NO SPACE
        //*  V2.01J
        if (condition(pnd_Eof_Sw.getBoolean()))                                                                                                                           //Natural: IF #EOF_SW THEN
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  12-12-98
            //*  12-12-98
            //*  12-12-98
            short decideConditionsMet1219 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #INPUT-PARM;//Natural: VALUE 'DAILY'
            if (condition((pnd_Input_Parm.equals("DAILY"))))
            {
                decideConditionsMet1219++;
                pnd_Header.getValue(2).setValue("DEATH SETTLEMENTS - U.S. CYCLE PAYMENT REGISTER");                                                                       //Natural: ASSIGN #HEADER ( 2 ) := 'DEATH SETTLEMENTS - U.S. CYCLE PAYMENT REGISTER'
            }                                                                                                                                                             //Natural: VALUE 'DAYND'
            else if (condition((pnd_Input_Parm.equals("DAYND"))))
            {
                decideConditionsMet1219++;
                pnd_Header.getValue(2).setValue("DEATH SETTL. - U.S. PHYSICAL END-OF-DAY REGISTER");                                                                      //Natural: ASSIGN #HEADER ( 2 ) := 'DEATH SETTL. - U.S. PHYSICAL END-OF-DAY REGISTER'
            }                                                                                                                                                             //Natural: VALUE 'LEDGR'
            else if (condition((pnd_Input_Parm.equals("LEDGR"))))
            {
                decideConditionsMet1219++;
                pnd_Header.getValue(2).setValue("DEATH SETTL. - U.S. LOGICAL END-OF-DAY REGISTER");                                                                       //Natural: ASSIGN #HEADER ( 2 ) := 'DEATH SETTL. - U.S. LOGICAL END-OF-DAY REGISTER'
            }                                                                                                                                                             //Natural: VALUE 'MONTH'
            else if (condition((pnd_Input_Parm.equals("MONTH"))))
            {
                decideConditionsMet1219++;
                pnd_Header.getValue(2).setValue("DEATH SETTLEMENTS - U.S. MONTH-END PAYMENT REGISTER");                                                                   //Natural: ASSIGN #HEADER ( 2 ) := 'DEATH SETTLEMENTS - U.S. MONTH-END PAYMENT REGISTER'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1219 > 0))
            {
                //* 1.03A
                pnd_Header.getValue(2).setValue(DbsUtil.compress(pnd_Hdr_Pfx.getValue(pnd_Hdr_Pfx_Ndx), pnd_Header.getValue(2)));                                         //Natural: COMPRESS #HDR_PFX ( #HDR_PFX_NDX ) #HEADER ( 2 ) INTO #HEADER ( 2 )
                if (condition(pnd_Month_End.getBoolean()))                                                                                                                //Natural: IF #MONTH-END
                {
                    pnd_Month.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("LLLLLLLLL"));                                               //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = L ( 9 ) ) TO #MONTH
                    pnd_Year.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYY"));                                                     //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYY ) TO #YEAR
                    pnd_Header.getValue(2).setValue(DbsUtil.compress(pnd_Header.getValue(2), pnd_Month, pnd_Year));                                                       //Natural: COMPRESS #HEADER ( 2 ) #MONTH #YEAR INTO #HEADER ( 2 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(! (pnd_Summary.getBoolean())))                                                                                                          //Natural: IF NOT #SUMMARY
                    {
                        pnd_Header.getValue(2).setValue(DbsUtil.compress(pnd_Header.getValue(2), "For", pnd_Hdr_Dte));                                                    //Natural: COMPRESS #HEADER ( 2 ) 'For' #HDR-DTE INTO #HEADER ( 2 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  ----------------------------------------------------------- *
                                                                                                                                                                          //Natural: PERFORM CENTER_HEADER_TXT
        sub_Center_Header_Txt();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //* 1.06F
        setValueToSubstring(pnd_Hdr_Ndx_Lit,pnd_Header.getValue(1),2,3);                                                                                                  //Natural: MOVE #HDR_NDX_LIT TO SUBSTRING ( #HEADER ( 1 ) ,2,3 )
        //*  ----------------------------------------------------------- *
        //*  --------------*
        //*   INITIALIZE-HEADER
    }
    //*   1.02
    //*  1.03C
    private void sub_Center_Header_Txt() throws Exception                                                                                                                 //Natural: CENTER_HEADER_TXT
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #HDR_NDX 1 TO 4
        for (pnd_Hdr_Ndx.setValue(1); condition(pnd_Hdr_Ndx.lessOrEqual(4)); pnd_Hdr_Ndx.nadd(1))
        {
            //*  ALREADY CENTERED
            if (condition(pnd_Header.getValue(pnd_Hdr_Ndx).getSubstring(1,1).equals(" ")))                                                                                //Natural: IF SUBSTRING ( #HEADER ( #HDR_NDX ) ,1,1 ) = ' '
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Len.reset();                                                                                                                                              //Natural: RESET #LEN #W-FLD
            pnd_W_Fld.reset();
            //*  1.01J
            DbsUtil.examine(new ExamineSource(pnd_Header.getValue(pnd_Hdr_Ndx)), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                               //Natural: EXAMINE #HEADER ( #HDR_NDX ) FOR ' ' GIVING LENGTH #LEN
            pnd_Len.compute(new ComputeParameters(false, pnd_Len), (pnd_Hdr_Len_Max.subtract(pnd_Len)).divide(2));                                                        //Natural: ASSIGN #LEN := ( #HDR_LEN_MAX - #LEN ) / 2
            pnd_W_Fld.moveAll("H'00'");                                                                                                                                   //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #LEN
            pnd_Header.getValue(pnd_Hdr_Ndx).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Header.getValue(pnd_Hdr_Ndx)));                      //Natural: COMPRESS #W-FLD #HEADER ( #HDR_NDX ) INTO #HEADER ( #HDR_NDX ) LEAVE NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Header.getValue(pnd_Hdr_Ndx),true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                         //Natural: EXAMINE FULL #HEADER ( #HDR_NDX ) FOR FULL H'00' REPLACE ' '
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  --------------*
        //*   CENTER_HEADER_TXT
    }
    private void sub_Build_Detail_Lines() throws Exception                                                                                                                //Natural: BUILD-DETAIL-LINES
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #I = 1 TO C-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); pnd_I.nadd(1))
        {
            pnd_Ln.nadd(1);                                                                                                                                               //Natural: ASSIGN #LN := #LN + 1
            getReports().write(0, ReportOption.NOTITLE,"VALUE OF INDEX #LN :",pnd_Ln);                                                                                    //Natural: WRITE 'VALUE OF INDEX #LN :' #LN
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Cde().getValue(pnd_I));                                         //Natural: ASSIGN #INV-ACCT-INPUT := #RPT-EXT.INV-ACCT-CDE ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Valuat_Period().getValue(pnd_I));                       //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := #RPT-EXT.INV-ACCT-VALUAT-PERIOD ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Ins_Type().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Ins_Type());                                         //Natural: ASSIGN #FUND-PDA.#CNTRCT-ANNTY-INS-TYPE := #RPT-EXT.CNTRCT-ANNTY-INS-TYPE
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Cntrct_Annty_Type_Cde().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Annty_Type_Cde());                                         //Natural: ASSIGN #FUND-PDA.#CNTRCT-ANNTY-TYPE-CDE := #RPT-EXT.CNTRCT-ANNTY-TYPE-CDE
            //*  1.04
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' #FUND-PDA
            if (condition(Global.isEscape())) return;
            pnd_Dtl_Lines_Pnd_Inv_Acct.getValue(pnd_Ln).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #INV-ACCT-DESC-8 TO #DTL-LINES.#INV-ACCT ( #LN )
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_Ln).reset();                                                                                        //Natural: RESET #DTL-LINES.#INV-ACCT-VALUAT-PERIOD ( #LN )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_Ln).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_3());                                      //Natural: ASSIGN #DTL-LINES.#INV-ACCT-VALUAT-PERIOD ( #LN ) := #FUND-PDA.#VALUAT-DESC-3
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ln.equals(1)))                                                                                                                              //Natural: IF #LN = 1
            {
                                                                                                                                                                          //Natural: PERFORM GET-MISCELLANEOUS-DATA
                sub_Get_Miscellaneous_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                //*  1.05 - ADD DCI & DPI
            }                                                                                                                                                             //Natural: END-IF
            pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_Ln).compute(new ComputeParameters(false, pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_Ln)), ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I))); //Natural: ASSIGN #DTL-LINES.#GROSS-AMT ( #LN ) := #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) + #RPT-EXT.INV-ACCT-DVDND-AMT ( #I )
            pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                        //Natural: ASSIGN #DTL-LINES.#INV-ACCT-FDRL-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
            pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                      //Natural: ASSIGN #DTL-LINES.#INV-ACCT-STATE-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I )
            pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                      //Natural: ASSIGN #DTL-LINES.#INV-ACCT-LOCAL-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
            pnd_Dtl_Lines_Pnd_Inv_Acct_Dcpi_Amt.getValue(pnd_Ln).compute(new ComputeParameters(false, pnd_Dtl_Lines_Pnd_Inv_Acct_Dcpi_Amt.getValue(pnd_Ln)),              //Natural: ASSIGN #DTL-LINES.#INV-ACCT-DCPI-AMT ( #LN ) := #RPT-EXT.INV-ACCT-DCI-AMT ( #I ) + #RPT-EXT.INV-ACCT-DPI-AMT ( #I )
                ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dci_Amt().getValue(pnd_I).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_I)));
            pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                      //Natural: ASSIGN #DTL-LINES.#INV-ACCT-NET-PYMNT-AMT ( #LN ) := #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
            //*  12-22-98
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Cde().getValue(pnd_I).equals(5)))                                                                    //Natural: IF #RPT-EXT.##INV-DED-CDE ( #I ) = 5
            {
                pnd_Dtl_Lines_Pnd_Ovrpt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Pnd_Inv_Ded_Amt().getValue(pnd_I));                                      //Natural: ASSIGN #DTL-LINES.#OVRPT ( #LN ) := #RPT-EXT.##INV-DED-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Misc_Pnd_Inv_Acct_Settl_Amt.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                                                //Natural: ASSIGN #MISC.#INV-ACCT-SETTL-AMT := #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
                pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                                                //Natural: ASSIGN #MISC.#INV-ACCT-DVDND-AMT := #RPT-EXT.INV-ACCT-DVDND-AMT ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Misc_Pnd_Inv_Acct_Unit_Qty.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Qty().getValue(pnd_I));                                                  //Natural: ASSIGN #MISC.#INV-ACCT-UNIT-QTY := #RPT-EXT.INV-ACCT-UNIT-QTY ( #I )
                pnd_Misc_Pnd_Inv_Acct_Unit_Value.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Value().getValue(pnd_I));                                              //Natural: ASSIGN #MISC.#INV-ACCT-UNIT-VALUE := #RPT-EXT.INV-ACCT-UNIT-VALUE ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().equals("N") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("X")))            //Natural: IF #RPT-EXT.CNTRCT-PYMNT-TYPE-IND = 'N' AND #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND = 'X'
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("20")))                                                                                 //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = '20'
                {
                    pnd_Dtl_Lines_Pnd_Rtb.getValue(pnd_Ln).setValue("TTB");                                                                                               //Natural: ASSIGN #DTL-LINES.#RTB ( #LN ) := 'TTB'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Dtl_Lines_Pnd_Rtb.getValue(pnd_Ln).setValue("RTB");                                                                                               //Natural: ASSIGN #DTL-LINES.#RTB ( #LN ) := 'RTB'
                    pnd_No_Guar_Stck.setValue(true);                                                                                                                      //Natural: ASSIGN #NO-GUAR-STCK := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dtl_Lines_Pnd_Rtb.getValue(pnd_Ln).setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/YY"));                       //Natural: MOVE EDITED #RPT-EXT.PYMNT-SETTLMNT-DTE ( EM = MM/YY ) TO #DTL-LINES.#RTB ( #LN )
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM BUILD-SUMMARY-DATA
            sub_Build_Summary_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  --------------*
    }
    private void sub_Get_Miscellaneous_Data() throws Exception                                                                                                            //Natural: GET-MISCELLANEOUS-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Misc.setValuesByName(ldaFcpl190a.getPnd_Rpt_Ext());                                                                                                           //Natural: MOVE BY NAME #RPT-EXT TO #MISC
        pnd_Misc_Pnd_Annt.setValue(DbsUtil.compress(ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name(), ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name())); //Natural: COMPRESS #RPT-EXT.PH-FIRST-NAME #RPT-EXT.PH-MIDDLE-NAME #RPT-EXT.PH-LAST-NAME INTO #MISC.#ANNT
        if (condition(DbsUtil.maskMatches(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1),"'CR '")))                                                                   //Natural: IF #RPT-EXT.PYMNT-NME ( 1 ) = MASK ( 'CR ' )
        {
            pnd_Misc_Pnd_Payee.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Addr_Line1_Txt().getValue(1));                                                                   //Natural: ASSIGN #MISC.#PAYEE := #RPT-EXT.PYMNT-ADDR-LINE1-TXT ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Misc_Pnd_Payee.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Nme().getValue(1));                                                                              //Natural: ASSIGN #MISC.#PAYEE := #RPT-EXT.PYMNT-NME ( 1 )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))                                                                                         //Natural: IF #FUND-PDA.#COMPANY-CDE = 'T'
        {
            pnd_Tiaa.setValue(true);                                                                                                                                      //Natural: ASSIGN #TIAA := TRUE
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(1).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_1_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 1 ) := #RPT-EXT.CNTRCT-DA-TIAA-1-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(2).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_2_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 2 ) := #RPT-EXT.CNTRCT-DA-TIAA-2-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(3).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_3_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 3 ) := #RPT-EXT.CNTRCT-DA-TIAA-3-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(4).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_4_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 4 ) := #RPT-EXT.CNTRCT-DA-TIAA-4-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(5).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Tiaa_5_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 5 ) := #RPT-EXT.CNTRCT-DA-TIAA-5-NBR
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Tiaa.reset();                                                                                                                                             //Natural: RESET #TIAA
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(1).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_1_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 1 ) := #RPT-EXT.CNTRCT-DA-CREF-1-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(2).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_2_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 2 ) := #RPT-EXT.CNTRCT-DA-CREF-2-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(3).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_3_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 3 ) := #RPT-EXT.CNTRCT-DA-CREF-3-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(4).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_4_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 4 ) := #RPT-EXT.CNTRCT-DA-CREF-4-NBR
            pnd_Misc_Pnd_Cntrct_Nbr.getValue(5).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Da_Cref_5_Nbr());                                                              //Natural: ASSIGN #CNTRCT-NBR ( 5 ) := #RPT-EXT.CNTRCT-DA-CREF-5-NBR
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Tiaa.getBoolean()))                                                                                                                             //Natural: IF #TIAA
        {
            pnd_Guar_Unit_Txt.setValue("GUAR AMOUNT:", MoveOption.RightJustified);                                                                                        //Natural: MOVE RIGHT 'GUAR AMOUNT:' TO #GUAR-UNIT-TXT
            pnd_Div_Unit_Txt.setValue("DIVIDEND:", MoveOption.RightJustified);                                                                                            //Natural: MOVE RIGHT 'DIVIDEND:' TO #DIV-UNIT-TXT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Guar_Unit_Txt.setValue(DbsUtil.compress(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(), "UNITS:"));                                                    //Natural: COMPRESS #INV-ACCT-DESC-8 'UNITS:' INTO #GUAR-UNIT-TXT
            pnd_Guar_Unit_Txt.setValue(pnd_Guar_Unit_Txt, MoveOption.RightJustified);                                                                                     //Natural: MOVE RIGHT #GUAR-UNIT-TXT TO #GUAR-UNIT-TXT
            pnd_Div_Unit_Txt.setValue("UNIT VALUE:");                                                                                                                     //Natural: MOVE 'UNIT VALUE:' TO #DIV-UNIT-TXT
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1361 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet1361++;
            pnd_Misc_Pnd_Cge_Txt.setValue(" CHECK:");                                                                                                                     //Natural: ASSIGN #CGE-TXT := ' CHECK:'
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet1361++;
            pnd_Misc_Pnd_Cge_Txt.setValue("   EFT:");                                                                                                                     //Natural: ASSIGN #CGE-TXT := '   EFT:'
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(3))))
        {
            decideConditionsMet1361++;
            pnd_Misc_Pnd_Cge_Txt.setValue("GLOBAL:");                                                                                                                     //Natural: ASSIGN #CGE-TXT := 'GLOBAL:'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Misc_Pnd_Cge_Txt.reset();                                                                                                                                 //Natural: RESET #CGE-TXT
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Sub_Pnd_Qty.nadd(1);                                                                                                                                          //Natural: ASSIGN #SUB.#QTY := #SUB.#QTY + 1
        if (condition(pnd_Month_End.equals(false)))                                                                                                                       //Natural: IF #MONTH-END = FALSE
        {
            pnd_Dte_Pnd_Qty.nadd(1);                                                                                                                                      //Natural: ASSIGN #DTE.#QTY := #DTE.#QTY + 1
            pnd_Intrfce_Pnd_Qty.nadd(1);                                                                                                                                  //Natural: ASSIGN #INTRFCE.#QTY := #INTRFCE.#QTY + 1
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------*
    }
    //*  1.05
    private void sub_Calculate_Item_Sub_Totals() throws Exception                                                                                                         //Natural: CALCULATE-ITEM-SUB-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Item_Pnd_Gross_Amt.nadd(pnd_Dtl_Lines_Pnd_Gross_Amt.getValue("*"));                                                                                           //Natural: ASSIGN #ITEM.#GROSS-AMT := #ITEM.#GROSS-AMT + #DTL-LINES.#GROSS-AMT ( * )
        pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue("*"));                                                                   //Natural: ASSIGN #ITEM.#INV-ACCT-FDRL-TAX-AMT := #ITEM.#INV-ACCT-FDRL-TAX-AMT + #DTL-LINES.#INV-ACCT-FDRL-TAX-AMT ( * )
        pnd_Item_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt.getValue("*"));                                                                 //Natural: ASSIGN #ITEM.#INV-ACCT-STATE-TAX-AMT := #ITEM.#INV-ACCT-STATE-TAX-AMT + #DTL-LINES.#INV-ACCT-STATE-TAX-AMT ( * )
        pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt.getValue("*"));                                                                 //Natural: ASSIGN #ITEM.#INV-ACCT-LOCAL-TAX-AMT := #ITEM.#INV-ACCT-LOCAL-TAX-AMT + #DTL-LINES.#INV-ACCT-LOCAL-TAX-AMT ( * )
        pnd_Item_Pnd_Ovrpt.nadd(pnd_Dtl_Lines_Pnd_Ovrpt.getValue("*"));                                                                                                   //Natural: ASSIGN #ITEM.#OVRPT := #ITEM.#OVRPT + #DTL-LINES.#OVRPT ( * )
        pnd_Item_Pnd_Inv_Acct_Dcpi_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_Dcpi_Amt.getValue("*"));                                                                           //Natural: ASSIGN #ITEM.#INV-ACCT-DCPI-AMT := #ITEM.#INV-ACCT-DCPI-AMT + #DTL-LINES.#INV-ACCT-DCPI-AMT ( * )
        pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue("*"));                                                                 //Natural: ASSIGN #ITEM.#INV-ACCT-NET-PYMNT-AMT := #ITEM.#INV-ACCT-NET-PYMNT-AMT + #DTL-LINES.#INV-ACCT-NET-PYMNT-AMT ( * )
        //*  1.05
        //*  1.05
        if (condition(! (pnd_Month_End.getBoolean())))                                                                                                                    //Natural: IF NOT #MONTH-END
        {
            pnd_Dte_Pnd_Gross_Amt.nadd(pnd_Item_Pnd_Gross_Amt);                                                                                                           //Natural: ASSIGN #DTE.#GROSS-AMT := #DTE.#GROSS-AMT + #ITEM.#GROSS-AMT
            pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt);                                                                                   //Natural: ASSIGN #DTE.#INV-ACCT-FDRL-TAX-AMT := #DTE.#INV-ACCT-FDRL-TAX-AMT + #ITEM.#INV-ACCT-FDRL-TAX-AMT
            pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_State_Tax_Amt);                                                                                 //Natural: ASSIGN #DTE.#INV-ACCT-STATE-TAX-AMT := #DTE.#INV-ACCT-STATE-TAX-AMT + #ITEM.#INV-ACCT-STATE-TAX-AMT
            pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt);                                                                                 //Natural: ASSIGN #DTE.#INV-ACCT-LOCAL-TAX-AMT := #DTE.#INV-ACCT-LOCAL-TAX-AMT + #ITEM.#INV-ACCT-LOCAL-TAX-AMT
            pnd_Dte_Pnd_Ovrpt.nadd(pnd_Item_Pnd_Ovrpt);                                                                                                                   //Natural: ASSIGN #DTE.#OVRPT := #DTE.#OVRPT + #ITEM.#OVRPT
            pnd_Dte_Pnd_Inv_Acct_Dcpi_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Dcpi_Amt);                                                                                           //Natural: ASSIGN #DTE.#INV-ACCT-DCPI-AMT := #DTE.#INV-ACCT-DCPI-AMT + #ITEM.#INV-ACCT-DCPI-AMT
            pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt);                                                                                 //Natural: ASSIGN #DTE.#INV-ACCT-NET-PYMNT-AMT := #DTE.#INV-ACCT-NET-PYMNT-AMT + #ITEM.#INV-ACCT-NET-PYMNT-AMT
            pnd_Intrfce_Pnd_Gross_Amt.nadd(pnd_Item_Pnd_Gross_Amt);                                                                                                       //Natural: ASSIGN #INTRFCE.#GROSS-AMT := #INTRFCE.#GROSS-AMT + #ITEM.#GROSS-AMT
            pnd_Intrfce_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt);                                                                               //Natural: ASSIGN #INTRFCE.#INV-ACCT-FDRL-TAX-AMT := #INTRFCE.#INV-ACCT-FDRL-TAX-AMT + #ITEM.#INV-ACCT-FDRL-TAX-AMT
            pnd_Intrfce_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_State_Tax_Amt);                                                                             //Natural: ASSIGN #INTRFCE.#INV-ACCT-STATE-TAX-AMT := #INTRFCE.#INV-ACCT-STATE-TAX-AMT + #ITEM.#INV-ACCT-STATE-TAX-AMT
            pnd_Intrfce_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt);                                                                             //Natural: ASSIGN #INTRFCE.#INV-ACCT-LOCAL-TAX-AMT := #INTRFCE.#INV-ACCT-LOCAL-TAX-AMT + #ITEM.#INV-ACCT-LOCAL-TAX-AMT
            pnd_Intrfce_Pnd_Ovrpt.nadd(pnd_Item_Pnd_Ovrpt);                                                                                                               //Natural: ASSIGN #INTRFCE.#OVRPT := #INTRFCE.#OVRPT + #ITEM.#OVRPT
            pnd_Intrfce_Pnd_Inv_Acct_Dcpi_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Dcpi_Amt);                                                                                       //Natural: ASSIGN #INTRFCE.#INV-ACCT-DCPI-AMT := #INTRFCE.#INV-ACCT-DCPI-AMT + #ITEM.#INV-ACCT-DCPI-AMT
            pnd_Intrfce_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt);                                                                             //Natural: ASSIGN #INTRFCE.#INV-ACCT-NET-PYMNT-AMT := #INTRFCE.#INV-ACCT-NET-PYMNT-AMT + #ITEM.#INV-ACCT-NET-PYMNT-AMT
            //*  1.05
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Sub_Pnd_Gross_Amt.nadd(pnd_Item_Pnd_Gross_Amt);                                                                                                               //Natural: ASSIGN #SUB.#GROSS-AMT := #SUB.#GROSS-AMT + #ITEM.#GROSS-AMT
        pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt);                                                                                       //Natural: ASSIGN #SUB.#INV-ACCT-FDRL-TAX-AMT := #SUB.#INV-ACCT-FDRL-TAX-AMT + #ITEM.#INV-ACCT-FDRL-TAX-AMT
        pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_State_Tax_Amt);                                                                                     //Natural: ASSIGN #SUB.#INV-ACCT-STATE-TAX-AMT := #SUB.#INV-ACCT-STATE-TAX-AMT + #ITEM.#INV-ACCT-STATE-TAX-AMT
        pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt);                                                                                     //Natural: ASSIGN #SUB.#INV-ACCT-LOCAL-TAX-AMT := #SUB.#INV-ACCT-LOCAL-TAX-AMT + #ITEM.#INV-ACCT-LOCAL-TAX-AMT
        pnd_Sub_Pnd_Ovrpt.nadd(pnd_Item_Pnd_Ovrpt);                                                                                                                       //Natural: ASSIGN #SUB.#OVRPT := #SUB.#OVRPT + #ITEM.#OVRPT
        pnd_Sub_Pnd_Inv_Acct_Dcpi_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Dcpi_Amt);                                                                                               //Natural: ASSIGN #SUB.#INV-ACCT-DCPI-AMT := #SUB.#INV-ACCT-DCPI-AMT + #ITEM.#INV-ACCT-DCPI-AMT
        pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt.nadd(pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt);                                                                                     //Natural: ASSIGN #SUB.#INV-ACCT-NET-PYMNT-AMT := #SUB.#INV-ACCT-NET-PYMNT-AMT + #ITEM.#INV-ACCT-NET-PYMNT-AMT
        //*  --------------*
        //*   CALCULATE-ITEM-SUB-TOTALS
    }
    private void sub_Print_Dte_Sub_Total() throws Exception                                                                                                               //Natural: PRINT-DTE-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        //*  12-12-98
        if (condition(pnd_Input_Parm.equals("DAILY") || pnd_Input_Parm.equals("DAYND")))                                                                                  //Natural: IF #INPUT-PARM = 'DAILY' OR = 'DAYND'
        {
            if (condition(pnd_O_Ftre_Ind.equals("F")))                                                                                                                    //Natural: IF #O-FTRE-IND = 'F'
            {
                pnd_Dte_Txt.setValue("FUTURE SUB-TOTALS:");                                                                                                               //Natural: MOVE 'FUTURE SUB-TOTALS:' TO #DTE-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dte_Txt.setValue("PAYMENT DATE SUB-TOTALS:");                                                                                                         //Natural: MOVE 'PAYMENT DATE SUB-TOTALS:' TO #DTE-TXT
            }                                                                                                                                                             //Natural: END-IF
            //*  12-12-98
            if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                       //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 6 LINES LEFT
            {
                getReports().newPage(0);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(2, NEWLINE,pnd_Dte_Txt,NEWLINE,NEWLINE,"GROSS:",new TabSetting(10),pnd_Dte_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new    //Natural: WRITE ( PRT1 ) / #DTE-TXT // 'GROSS:' 010T #DTE.#GROSS-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 032T 'FED:' 037T #DTE.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 056T 'ST:' 060T #DTE.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 080T 'LCL:' 085T #DTE.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 105T 'OVER:' 111T #DTE.#OVRPT ( EM = ZZZZZZZZZZZZZZ9.99 ) / 'DCI/DPI:' 10T #DTE.#INV-ACCT-DCPI-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 032T 'NET:' 037T #DTE.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 056T 'NUMBER OF CHECKS:' #DTE.#QTY ( EM = ZZZZZZZZ9 )
                TabSetting(32),"FED:",new TabSetting(37),pnd_Dte_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(56),"ST:",new 
                TabSetting(60),pnd_Dte_Pnd_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(80),"LCL:",new TabSetting(85),pnd_Dte_Pnd_Inv_Acct_Local_Tax_Amt, 
                new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(105),"OVER:",new TabSetting(111),pnd_Dte_Pnd_Ovrpt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),NEWLINE,"DCI/DPI:",new 
                TabSetting(10),pnd_Dte_Pnd_Inv_Acct_Dcpi_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(32),"NET:",new TabSetting(37),pnd_Dte_Pnd_Inv_Acct_Net_Pymnt_Amt, 
                new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(56),"NUMBER OF CHECKS:",pnd_Dte_Pnd_Qty, new ReportEditMask ("ZZZZZZZZ9"));
            if (Global.isEscape()) return;
            pnd_Dte.reset();                                                                                                                                              //Natural: RESET #DTE
            pnd_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr());                                                                           //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := #RPT-EXT.PYMNT-PRCSS-SEQ-NBR
            pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                                          //Natural: ASSIGN #PREV-SEQ-NBR := #CURR-SEQ-NBR
            //*  NEWPAGE(PRT1)             /*@@ 1.02E THIS SHOULD BE TURNED OFF
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------*
        //*   PRINT-DTE-SUB-TOTAL
    }
    //*  @@
    private void sub_Print_Intrfce_Dte_Sub_Total() throws Exception                                                                                                       //Natural: PRINT-INTRFCE-DTE-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************
        //*  12-12-98
        if (condition(pnd_Input_Parm.equals("LEDGR")))                                                                                                                    //Natural: IF #INPUT-PARM = 'LEDGR'
        {
            if (condition(pnd_O_Ftre_Ind.equals("F")))                                                                                                                    //Natural: IF #O-FTRE-IND = 'F'
            {
                pnd_Dte_Txt.setValue("FUTURE SUB-TOTALS:");                                                                                                               //Natural: MOVE 'FUTURE SUB-TOTALS:' TO #DTE-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Dte_Txt.setValue("ACCOUNTING DATE SUB-TOTALS:");                                                                                                      //Natural: MOVE 'ACCOUNTING DATE SUB-TOTALS:' TO #DTE-TXT
            }                                                                                                                                                             //Natural: END-IF
            //*  12-12-98
            if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                       //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 6 LINES LEFT
            {
                getReports().newPage(0);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(2, NEWLINE,pnd_Dte_Txt,NEWLINE,NEWLINE,"GROSS:",new TabSetting(10),pnd_Intrfce_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new  //Natural: WRITE ( PRT1 ) / #DTE-TXT // 'GROSS:' 010T #INTRFCE.#GROSS-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 032T 'FED:' 037T #INTRFCE.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 056T 'ST:' 060T #INTRFCE.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 080T 'LCL:' 085T #INTRFCE.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 105T 'OVER:' 111T #INTRFCE.#OVRPT ( EM = ZZZZZZZZZZZZZZ9.99 ) / 'DCI/DPI:' 10T #INTRFCE.#INV-ACCT-DCPI-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 032T 'NET:' 037T #INTRFCE.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 056T 'NUMBER OF CHECKS:' #INTRFCE.#QTY ( EM = ZZZZZZZZ9 )
                TabSetting(32),"FED:",new TabSetting(37),pnd_Intrfce_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(56),"ST:",new 
                TabSetting(60),pnd_Intrfce_Pnd_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(80),"LCL:",new TabSetting(85),pnd_Intrfce_Pnd_Inv_Acct_Local_Tax_Amt, 
                new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(105),"OVER:",new TabSetting(111),pnd_Intrfce_Pnd_Ovrpt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),NEWLINE,"DCI/DPI:",new 
                TabSetting(10),pnd_Intrfce_Pnd_Inv_Acct_Dcpi_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(32),"NET:",new TabSetting(37),pnd_Intrfce_Pnd_Inv_Acct_Net_Pymnt_Amt, 
                new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(56),"NUMBER OF CHECKS:",pnd_Intrfce_Pnd_Qty, new ReportEditMask ("ZZZZZZZZ9"));
            if (Global.isEscape()) return;
            pnd_Intrfce.reset();                                                                                                                                          //Natural: RESET #INTRFCE
            pnd_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr());                                                                           //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := #RPT-EXT.PYMNT-PRCSS-SEQ-NBR
            pnd_Prev_Seq_Nbr.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Nbr);                                                                                          //Natural: ASSIGN #PREV-SEQ-NBR := #CURR-SEQ-NBR
            //*  NEWPAGE(PRT1)                   /*@@ THIS SHOULD BE TURNED OFF
        }                                                                                                                                                                 //Natural: END-IF
        //*  --------------*
        //*   PRINT-INTRFCE-DTE-SUB-TOTAL
    }
    //*  1.02E
    //*  1.02E
    //*  1.02G
    //*  1.02G 4 -> 6
    private void sub_Print_Sub_Total() throws Exception                                                                                                                   //Natural: PRINT-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Sub_Txt.setValue("GRAND TOTALS");                                                                                                                             //Natural: ASSIGN #SUB-TXT := 'GRAND TOTALS'
        pnd_Sub_Prt.setValue(true);                                                                                                                                       //Natural: ASSIGN #SUB-PRT := TRUE
        pnd_Cf_Txt.setValue(" ");                                                                                                                                         //Natural: ASSIGN #CF-TXT := ' '
        if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                           //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 6 LINES LEFT
        {
            getReports().newPage(0);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Sub_Txt.setValue("GRAND TOTALS:");                                                                                                                            //Natural: ASSIGN #SUB-TXT := 'GRAND TOTALS:'
        getReports().write(2, NEWLINE,pnd_Sub_Txt,NEWLINE,NEWLINE,"GROSS:",new TabSetting(10),pnd_Sub_Pnd_Gross_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new        //Natural: WRITE ( PRT1 ) / #SUB-TXT // 'GROSS:' 010T #SUB.#GROSS-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 032T 'FED:' 037T #SUB.#INV-ACCT-FDRL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 056T 'ST:' 060T #SUB.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 080T 'LCL:' 085T #SUB.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 105T 'OVER:' 111T #SUB.#OVRPT ( EM = ZZZZZZZZZZZZZZ9.99 ) / 'DCI/DPI:' 10T #SUB.#INV-ACCT-DCPI-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 032T 'NET:' 037T #SUB.#INV-ACCT-NET-PYMNT-AMT ( EM = ZZZZZZZZZZZZZZ9.99 ) 056T 'NUMBER OF CHECKS:' #SUB.#QTY ( EM = ZZZZZZZZ9 )
            TabSetting(32),"FED:",new TabSetting(37),pnd_Sub_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(56),"ST:",new 
            TabSetting(60),pnd_Sub_Pnd_Inv_Acct_State_Tax_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(80),"LCL:",new TabSetting(85),pnd_Sub_Pnd_Inv_Acct_Local_Tax_Amt, 
            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(105),"OVER:",new TabSetting(111),pnd_Sub_Pnd_Ovrpt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),NEWLINE,"DCI/DPI:",new 
            TabSetting(10),pnd_Sub_Pnd_Inv_Acct_Dcpi_Amt, new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(32),"NET:",new TabSetting(37),pnd_Sub_Pnd_Inv_Acct_Net_Pymnt_Amt, 
            new ReportEditMask ("ZZZZZZZZZZZZZZ9.99"),new TabSetting(56),"NUMBER OF CHECKS:",pnd_Sub_Pnd_Qty, new ReportEditMask ("ZZZZZZZZ9"));
        if (Global.isEscape()) return;
        pnd_Sub.reset();                                                                                                                                                  //Natural: RESET #SUB
        //*  NEWPAGE(PRT1)          /* @@ 1.02E - DISABLE THIS
        //*  --------------*
        //*   PRINT-SUB-TOTAL
    }
    //*  1.05
    private void sub_Print_Detail_Header() throws Exception                                                                                                               //Natural: PRINT-DETAIL-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(2, new TabSetting(4),"IA",new TabSetting(25),"GROSS",new TabSetting(37),"FEDERAL",new TabSetting(53),"STATE",new TabSetting(64),"LOCAL",new    //Natural: WRITE ( PRT1 ) 004T 'IA' 025T 'GROSS' 037T 'FEDERAL' 053T 'STATE' 064T 'LOCAL' 074T 'OVER-' 105T 'DCI/DPI' 114T 'DUE' 125T 'NET' / 001T 'CONTRACT' 025T 'AMOUNT' 039T 'TAX' 054T 'TAX' 065T 'TAX' 074T 'PAYMENT' 105T 'ADDL' 114T 'DATE' 123T 'PAYMENT'
            TabSetting(74),"OVER-",new TabSetting(105),"DCI/DPI",new TabSetting(114),"DUE",new TabSetting(125),"NET",NEWLINE,new TabSetting(1),"CONTRACT",new 
            TabSetting(25),"AMOUNT",new TabSetting(39),"TAX",new TabSetting(54),"TAX",new TabSetting(65),"TAX",new TabSetting(74),"PAYMENT",new TabSetting(105),"ADDL",new 
            TabSetting(114),"DATE",new TabSetting(123),"PAYMENT");
        if (Global.isEscape()) return;
        //*  ---------------*
        //*   PRINT-DETAIL-HEADER
    }
    private void sub_Initialize_Txt() throws Exception                                                                                                                    //Natural: INITIALIZE-TXT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  V1.01D
        //*  V1.01D
        //*  V1.01D
        short decideConditionsMet1491 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #TI;//Natural: VALUE 1, 2, 3, 7
        if (condition((pnd_Ti.equals(1) || pnd_Ti.equals(2) || pnd_Ti.equals(3) || pnd_Ti.equals(7))))
        {
            decideConditionsMet1491++;
            pnd_Sub_Txt_A.setValue("LUMP SUM DEATH (A)");                                                                                                                 //Natural: ASSIGN #SUB-TXT-A := 'LUMP SUM DEATH (A)'
            pnd_Sub_Txt_M.setValue("LUMP SUM DEATH (M)");                                                                                                                 //Natural: ASSIGN #SUB-TXT-M := 'LUMP SUM DEATH (M)'
        }                                                                                                                                                                 //Natural: VALUE 4, 5, 6, 8
        else if (condition((pnd_Ti.equals(4) || pnd_Ti.equals(5) || pnd_Ti.equals(6) || pnd_Ti.equals(8))))
        {
            decideConditionsMet1491++;
            pnd_Sub_Txt_A.setValue("SUPPLEMENTAL DEATH (A)");                                                                                                             //Natural: ASSIGN #SUB-TXT-A := 'SUPPLEMENTAL DEATH (A)'
            pnd_Sub_Txt_M.setValue("SUPPLEMENTAL DEATH (M)");                                                                                                             //Natural: ASSIGN #SUB-TXT-M := 'SUPPLEMENTAL DEATH (M)'
        }                                                                                                                                                                 //Natural: VALUE 9
        else if (condition((pnd_Ti.equals(9))))
        {
            decideConditionsMet1491++;
            pnd_Sub_Txt_A.setValue("L/S + Per. Pmt (A)");                                                                                                                 //Natural: ASSIGN #SUB-TXT-A := 'L/S + Per. Pmt (A)'
            pnd_Sub_Txt_M.setValue("L/S + Per. PMT (M)");                                                                                                                 //Natural: ASSIGN #SUB-TXT-M := 'L/S + Per. PMT (M)'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ---------------*
        //*   INITIALIZE-TXT
    }
    private void sub_Print_Detail_Lines() throws Exception                                                                                                                //Natural: PRINT-DETAIL-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(getReports().getAstLineCount(2).add(pnd_Ln).add(9).greater(Global.getPAGESIZE())))                                                                  //Natural: IF *LINE-COUNT ( PRT1 ) + #LN + 9 GT *PAGESIZE
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( PRT1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, new TabSetting(1),pnd_Misc_Cntrct_Ppcn_Nbr,pnd_Misc_Pnd_Annt);                                                                              //Natural: WRITE ( PRT1 ) 1T #MISC.CNTRCT-PPCN-NBR #MISC.#ANNT
        if (Global.isEscape()) return;
        FOR05:                                                                                                                                                            //Natural: FOR #I 1 TO #LN
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ln)); pnd_I.nadd(1))
        {
            //*  09-05-95 : A. YOUNG
            //*  1.05 AER
            if (condition(pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_I).greater(getZero())))                                                                                //Natural: IF #DTL-LINES.#GROSS-AMT ( #I ) GT 0
            {
                getReports().write(2, new TabSetting(8),pnd_Dtl_Lines_Pnd_Inv_Acct.getValue(pnd_I),pnd_Dtl_Lines_Pnd_Inv_Acct_Valuat_Period.getValue(pnd_I),new           //Natural: WRITE ( PRT1 ) 008T #DTL-LINES.#INV-ACCT ( #I ) #DTL-LINES.#INV-ACCT-VALUAT-PERIOD ( #I ) 021T #DTL-LINES.#GROSS-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 035T #DTL-LINES.#INV-ACCT-FDRL-TAX-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 050T #DTL-LINES.#INV-ACCT-STATE-TAX-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 062T #DTL-LINES.#INV-ACCT-LOCAL-TAX-AMT ( #I ) ( EM = ZZ,ZZ9.99 ) 073T #DTL-LINES.#OVRPT ( #I ) ( EM = Z,ZZZ,ZZ9.99 ) 103T #DTL-LINES.#INV-ACCT-DCPI-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) 114T #DTL-LINES.#RTB ( #I ) 121T #DTL-LINES.#INV-ACCT-NET-PYMNT-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 )
                    TabSetting(21),pnd_Dtl_Lines_Pnd_Gross_Amt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(35),pnd_Dtl_Lines_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_I), 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Dtl_Lines_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_I), new ReportEditMask 
                    ("ZZZ,ZZ9.99"),new TabSetting(62),pnd_Dtl_Lines_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_I), new ReportEditMask ("ZZ,ZZ9.99"),new TabSetting(73),pnd_Dtl_Lines_Pnd_Ovrpt.getValue(pnd_I), 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(103),pnd_Dtl_Lines_Pnd_Inv_Acct_Dcpi_Amt.getValue(pnd_I), new ReportEditMask ("ZZZ,ZZ9.99"),new 
                    TabSetting(114),pnd_Dtl_Lines_Pnd_Rtb.getValue(pnd_I),new TabSetting(121),pnd_Dtl_Lines_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_I), 
                    new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pnd_Tiaa.getBoolean()))                                                                                                                             //Natural: IF #TIAA
        {
            if (condition(pnd_No_Guar_Stck.getBoolean() && pnd_Ln.equals(1)))                                                                                             //Natural: IF #NO-GUAR-STCK AND #LN = 1
            {
                getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                       //Natural: WRITE ( PRT1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  1.04B: REMOVE UNIT COUNT AND VALUE QUOTES IF LUMP SUM RPT (1-3)
                if (condition(pnd_Rpt_Ndx.greaterOrEqual(1) && pnd_Rpt_Ndx.lessOrEqual(3)))                                                                               //Natural: IF #RPT_NDX = 1 THRU 3
                {
                    getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                   //Natural: WRITE ( PRT1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"),new TabSetting(64),pnd_Guar_Unit_Txt,new            //Natural: WRITE ( PRT1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * ) 064T #GUAR-UNIT-TXT 080T #MISC.#INV-ACCT-SETTL-AMT ( EM = Z,ZZZ,ZZ9.99 ) 094T #DIV-UNIT-TXT 106T #MISC.#INV-ACCT-DVDND-AMT ( EM = Z,ZZZ,ZZ9.99 )
                        TabSetting(80),pnd_Misc_Pnd_Inv_Acct_Settl_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(94),pnd_Div_Unit_Txt,new TabSetting(106),pnd_Misc_Pnd_Inv_Acct_Dvdnd_Amt, 
                        new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_No_Guar_Stck.getBoolean() && pnd_Ln.equals(1)))                                                                                             //Natural: IF #NO-GUAR-STCK AND #LN = 1
            {
                getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                       //Natural: WRITE ( PRT1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  1.04B: REMOVE UNIT COUNT AND VALUE QUOTES IF LUMP SUM RPT (1-3)
                if (condition(pnd_Rpt_Ndx.greaterOrEqual(1) && pnd_Rpt_Ndx.lessOrEqual(3)))                                                                               //Natural: IF #RPT_NDX = 1 THRU 3
                {
                    getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"));                                                   //Natural: WRITE ( PRT1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * )
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getReports().write(2, new TabSetting(1),"IN SETTLEMENT OF:",pnd_Misc_Pnd_Cntrct_Nbr.getValue("*"),new TabSetting(64),pnd_Guar_Unit_Txt,new            //Natural: WRITE ( PRT1 ) 001T 'IN SETTLEMENT OF:' #MISC.#CNTRCT-NBR ( * ) 064T #GUAR-UNIT-TXT 082T #MISC.#INV-ACCT-UNIT-QTY ( EM = ZZZ,ZZ9.999 ) 094T #DIV-UNIT-TXT 108T #MISC.#INV-ACCT-UNIT-VALUE ( EM = ZZZ,ZZ9.9999 )
                        TabSetting(82),pnd_Misc_Pnd_Inv_Acct_Unit_Qty, new ReportEditMask ("ZZZ,ZZ9.999"),new TabSetting(94),pnd_Div_Unit_Txt,new TabSetting(108),pnd_Misc_Pnd_Inv_Acct_Unit_Value, 
                        new ReportEditMask ("ZZZ,ZZ9.9999"));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  12-12-98
        if (condition(! (pnd_Input_Parm.equals("LEDGR"))))                                                                                                                //Natural: IF NOT #INPUT-PARM = 'LEDGR'
        {
            pnd_Acct_Chk_Dte_Txt.setValue("  CHECK DATE:");                                                                                                               //Natural: ASSIGN #ACCT-CHK-DTE-TXT := '  CHECK DATE:'
            pnd_Acct_Chk_Dte.setValue(pnd_Misc_Pymnt_Check_Dte);                                                                                                          //Natural: ASSIGN #ACCT-CHK-DTE := #MISC.PYMNT-CHECK-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Acct_Chk_Dte_Txt.setValue("ACCTING DATE:");                                                                                                               //Natural: ASSIGN #ACCT-CHK-DTE-TXT := 'ACCTING DATE:'
            pnd_Acct_Chk_Dte.setValue(pnd_Misc_Pymnt_Acctg_Dte);                                                                                                          //Natural: ASSIGN #ACCT-CHK-DTE := #MISC.PYMNT-ACCTG-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************** RL PAYEE MATCH BEGIN **************************
        pnd_Check_Datex.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                      //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
        pnd_Check_Number_N10_Pnd_Check_Number_A10.reset();                                                                                                                //Natural: RESET #CHECK-NUMBER-A10
        if (condition(! (ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2) || ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8))))                //Natural: IF NOT ( #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8 )
        {
            //* N7 CHK-NBR - MAKE N10 W PREFIX
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().less(getZero())))                                                                                  //Natural: IF #RPT-EXT.PYMNT-CHECK-NBR LT 0
            {
                pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
                pnd_Check_Number_N10_Pnd_Check_Number_N7.compute(new ComputeParameters(false, pnd_Check_Number_N10_Pnd_Check_Number_N7), ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().multiply(-1)); //Natural: COMPUTE #CHECK-NUMBER-N7 = #RPT-EXT.PYMNT-CHECK-NBR * -1
                pnd_Pay_Type_Num.setValue(pnd_Check_Number_N10);                                                                                                          //Natural: ASSIGN #PAY-TYPE-NUM = #CHECK-NUMBER-N10
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  VALID OLD N7 CHECK-NBR
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr().greater(getZero())))                                                                           //Natural: IF #RPT-EXT.PYMNT-CHECK-NBR GT 0
                {
                    pnd_Check_Number_N10_Pnd_Check_Number_N7.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr());                                                      //Natural: ASSIGN #CHECK-NUMBER-N7 := #RPT-EXT.PYMNT-CHECK-NBR
                    pnd_Check_Number_N10_Pnd_Check_Number_N3.reset();                                                                                                     //Natural: RESET #CHECK-NUMBER-N3
                    if (condition(pnd_Check_Datex_Pnd_Check_Date.greater(20060430)))                                                                                      //Natural: IF #CHECK-DATE GT 20060430
                    {
                        pnd_Check_Number_N10_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                 //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Check_Number_N10_Pnd_Check_Number_N3.reset();                                                                                                 //Natural: RESET #CHECK-NUMBER-N3
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Check_Number_N10.reset();                                                                                                                         //Natural: RESET #CHECK-NUMBER-N10
                }                                                                                                                                                         //Natural: END-IF
                pnd_Pay_Type_Num.setValue(pnd_Check_Number_N10);                                                                                                          //Natural: ASSIGN #PAY-TYPE-NUM = #CHECK-NUMBER-N10
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Check_Number_N10.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr());                                                                            //Natural: ASSIGN #CHECK-NUMBER-N10 = #RPT-EXT.PYMNT-CHECK-SCRTY-NBR
            pnd_Pay_Type_Num.setValue(pnd_Check_Number_N10);                                                                                                              //Natural: ASSIGN #PAY-TYPE-NUM = #CHECK-NUMBER-N10
            //*  09-05-95 : A. YOUNG
            //*  1.05   /* AER 1/8/08
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, "CROSS REF:",pnd_Misc_Pymnt_Check_Seq_Nbr, new ReportEditMask ("9999999"),new ColumnSpacing(2),pnd_Acct_Chk_Dte_Txt,pnd_Acct_Chk_Dte,       //Natural: WRITE ( PRT1 ) 'CROSS REF:' #MISC.PYMNT-CHECK-SEQ-NBR ( EM = 9999999 ) 2X #ACCT-CHK-DTE-TXT #ACCT-CHK-DTE ( EM = MM/DD/YY ) 2X #CGE-TXT #PAY-TYPE-NUM ( EM = 9999999999 ) 2X 'PAYEE:' #MISC.#PAYEE 2X 'HOLD CODE:' #MISC.CNTRCT-HOLD-CDE / 021T #ITEM.#GROSS-AMT ( EM = Z,ZZZ,ZZ9.99 ) 037T #ITEM.#INV-ACCT-FDRL-TAX-AMT ( EM = Z,ZZZ,ZZ9.99 ) 050T #ITEM.#INV-ACCT-STATE-TAX-AMT ( EM = ZZZ,ZZ9.99 ) 062T #ITEM.#INV-ACCT-LOCAL-TAX-AMT ( EM = ZZ,ZZ9.99 ) 073T #ITEM.#OVRPT ( EM = Z,ZZZ,ZZ9.99 ) 103T #ITEM.#INV-ACCT-DCPI-AMT ( EM = ZZZ,ZZ9.99 ) 121T #ITEM.#INV-ACCT-NET-PYMNT-AMT ( EM = Z,ZZZ,ZZ9.99 ) / '-' ( 132 )
            new ReportEditMask ("MM/DD/YY"),new ColumnSpacing(2),pnd_Misc_Pnd_Cge_Txt,pnd_Pay_Type_Num, new ReportEditMask ("9999999999"),new ColumnSpacing(2),"PAYEE:",pnd_Misc_Pnd_Payee,new 
            ColumnSpacing(2),"HOLD CODE:",pnd_Misc_Cntrct_Hold_Cde,NEWLINE,new TabSetting(21),pnd_Item_Pnd_Gross_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new 
            TabSetting(37),pnd_Item_Pnd_Inv_Acct_Fdrl_Tax_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(50),pnd_Item_Pnd_Inv_Acct_State_Tax_Amt, 
            new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(62),pnd_Item_Pnd_Inv_Acct_Local_Tax_Amt, new ReportEditMask ("ZZ,ZZ9.99"),new TabSetting(73),pnd_Item_Pnd_Ovrpt, 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new TabSetting(103),pnd_Item_Pnd_Inv_Acct_Dcpi_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(121),pnd_Item_Pnd_Inv_Acct_Net_Pymnt_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),NEWLINE,"-",new RepeatItem(132));
        if (Global.isEscape()) return;
        //*  12-22-98
        //*  08-24-94 : A. YOUNG
        pnd_Dtl_Lines.getValue("*").reset();                                                                                                                              //Natural: RESET #DTL-LINES ( * ) #MISC #ITEM #MISC.#CNTRCT-NBR ( * ) #LN #LN1 #NO-GUAR-STCK
        pnd_Misc.reset();
        pnd_Item.reset();
        pnd_Misc_Pnd_Cntrct_Nbr.getValue("*").reset();
        pnd_Ln.reset();
        pnd_Ln1.reset();
        pnd_No_Guar_Stck.reset();
        pnd_Printed.setValue(true);                                                                                                                                       //Natural: ASSIGN #PRINTED := TRUE
        //*  ---------------*
        //*   PRINT-DETAIL-LINES
    }
    private void sub_Build_Summary_Data() throws Exception                                                                                                                //Natural: BUILD-SUMMARY-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ti.setValue(pnd_Rpt_Ndx);                                                                                                                                     //Natural: ASSIGN #TI := #RPT_NDX
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().greaterOrEqual(1) && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().lessOrEqual(3)))       //Natural: IF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND = 1 THRU 3
        {
            pnd_Pi.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind());                                                                                         //Natural: ASSIGN #PI := #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().equals("F")))                                                                                       //Natural: IF #RPT-EXT.PYMNT-FTRE-IND = 'F'
            {
                if (condition(pnd_Ln.equals(1)))                                                                                                                          //Natural: IF #LN = 1
                {
                    pnd_Sum_Pnd_F_Qty.getValue(pnd_Ti,pnd_Pi).nadd(1);                                                                                                    //Natural: ASSIGN #SUM.#F-QTY ( #TI,#PI ) := #SUM.#F-QTY ( #TI,#PI ) + 1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Valuat_Period().getValue(pnd_I).equals("M")))                                                           //Natural: IF #RPT-EXT.INV-ACCT-VALUAT-PERIOD ( #I ) = 'M'
                {
                    pnd_Sum_Pnd_Future_M.getValue(pnd_Ti,pnd_Pi).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                               //Natural: ASSIGN #SUM.#FUTURE-M ( #TI,#PI ) := #SUM.#FUTURE-M ( #TI,#PI ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sum_Pnd_Future_A.getValue(pnd_Ti,pnd_Pi).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                               //Natural: ASSIGN #SUM.#FUTURE-A ( #TI,#PI ) := #SUM.#FUTURE-A ( #TI,#PI ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ln.equals(1)))                                                                                                                          //Natural: IF #LN = 1
                {
                    pnd_Sum_Pnd_C_Qty.getValue(pnd_Ti,pnd_Pi).nadd(1);                                                                                                    //Natural: ASSIGN #SUM.#C-QTY ( #TI,#PI ) := #SUM.#C-QTY ( #TI,#PI ) + 1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Valuat_Period().getValue(pnd_I).equals("M")))                                                           //Natural: IF #RPT-EXT.INV-ACCT-VALUAT-PERIOD ( #I ) = 'M'
                {
                    pnd_Sum_Pnd_Current_M.getValue(pnd_Ti,pnd_Pi).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                              //Natural: ASSIGN #SUM.#CURRENT-M ( #TI,#PI ) := #SUM.#CURRENT-M ( #TI,#PI ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Sum_Pnd_Current_A.getValue(pnd_Ti,pnd_Pi).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                              //Natural: ASSIGN #SUM.#CURRENT-A ( #TI,#PI ) := #SUM.#CURRENT-A ( #TI,#PI ) + #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ---------------*
        //*   BUILD-SUMMARY-DATA
    }
    private void sub_Print_Summary() throws Exception                                                                                                                     //Natural: PRINT-SUMMARY
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ti.setValue(pnd_Rpt_Ndx);                                                                                                                                     //Natural: ASSIGN #TI := #RPT_NDX
        //*  1.02H - DO THIS HERE...
        pnd_Cf_Txt.reset();                                                                                                                                               //Natural: RESET #CF-TXT
        FOR06:                                                                                                                                                            //Natural: FOR #PI 1 TO 3
        for (pnd_Pi.setValue(1); condition(pnd_Pi.lessOrEqual(3)); pnd_Pi.nadd(1))
        {
            getReports().newPage(new ReportSpecification(0));                                                                                                             //Natural: NEWPAGE ( PRT1 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ---------------------------------------------------------------- *
            short decideConditionsMet1628 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #PI;//Natural: VALUE 1
            if (condition((pnd_Pi.equals(1))))
            {
                decideConditionsMet1628++;
                pnd_Header.getValue(4).setValue("CHECK SUMMARY TOTALS");                                                                                                  //Natural: ASSIGN #HEADER ( 4 ) := 'CHECK SUMMARY TOTALS'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Pi.equals(2))))
            {
                decideConditionsMet1628++;
                pnd_Header.getValue(4).setValue("EFT SUMMARY TOTALS");                                                                                                    //Natural: ASSIGN #HEADER ( 4 ) := 'EFT SUMMARY TOTALS'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Pi.equals(3))))
            {
                decideConditionsMet1628++;
                pnd_Header.getValue(4).setValue("GLOBAL SUMMARY TOTALS");                                                                                                 //Natural: ASSIGN #HEADER ( 4 ) := 'GLOBAL SUMMARY TOTALS'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1628 > 0))
            {
                //* *    PERFORM CENTER_HEADER_TXT         /* 1.06C
                //*  1.06H - OK BCZ EOF SW ON
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-HEADER
                sub_Initialize_Header();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                getReports().write(2, NEWLINE,NEWLINE,new TabSetting(56),"1ST PAYMENT",NEWLINE,new TabSetting(58),"CURRENT",new TabSetting(86),"FUTURE",new               //Natural: WRITE ( PRT1 ) / / 056T '1ST PAYMENT' / 058T 'CURRENT' 086T 'FUTURE' 112T 'TOTAL' /
                    TabSetting(112),"TOTAL",NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  ---------------------------------------------------------------- *
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TXT
            sub_Initialize_Txt();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            pnd_T_Total_A.compute(new ComputeParameters(false, pnd_T_Total_A), pnd_Sum_Pnd_Current_A.getValue(pnd_Ti,pnd_Pi).add(pnd_Sum_Pnd_Future_A.getValue(pnd_Ti,    //Natural: ASSIGN #T-TOTAL-A := #SUM.#CURRENT-A ( #TI,#PI ) + #SUM.#FUTURE-A ( #TI,#PI )
                pnd_Pi)));
            pnd_T_Total_M.compute(new ComputeParameters(false, pnd_T_Total_M), pnd_Sum_Pnd_Current_M.getValue(pnd_Ti,pnd_Pi).add(pnd_Sum_Pnd_Future_M.getValue(pnd_Ti,    //Natural: ASSIGN #T-TOTAL-M := #SUM.#CURRENT-M ( #TI,#PI ) + #SUM.#FUTURE-M ( #TI,#PI )
                pnd_Pi)));
            pnd_Us_Sum_Pnd_Current_A.getValue(pnd_Ti).nadd(pnd_Sum_Pnd_Current_A.getValue(pnd_Ti,pnd_Pi));                                                                //Natural: ASSIGN #US-SUM.#CURRENT-A ( #TI ) := #US-SUM.#CURRENT-A ( #TI ) + #SUM.#CURRENT-A ( #TI,#PI )
            pnd_Us_Sum_Pnd_Current_M.getValue(pnd_Ti).nadd(pnd_Sum_Pnd_Current_M.getValue(pnd_Ti,pnd_Pi));                                                                //Natural: ASSIGN #US-SUM.#CURRENT-M ( #TI ) := #US-SUM.#CURRENT-M ( #TI ) + #SUM.#CURRENT-M ( #TI,#PI )
            pnd_Us_Sum_Pnd_Future_A.getValue(pnd_Ti).nadd(pnd_Sum_Pnd_Future_A.getValue(pnd_Ti,pnd_Pi));                                                                  //Natural: ASSIGN #US-SUM.#FUTURE-A ( #TI ) := #US-SUM.#FUTURE-A ( #TI ) + #SUM.#FUTURE-A ( #TI,#PI )
            pnd_Us_Sum_Pnd_Future_M.getValue(pnd_Ti).nadd(pnd_Sum_Pnd_Future_M.getValue(pnd_Ti,pnd_Pi));                                                                  //Natural: ASSIGN #US-SUM.#FUTURE-M ( #TI ) := #US-SUM.#FUTURE-M ( #TI ) + #SUM.#FUTURE-M ( #TI,#PI )
            pnd_Us_Sum_Pnd_C_Qty.getValue(pnd_Ti).nadd(pnd_Sum_Pnd_C_Qty.getValue(pnd_Ti,pnd_Pi));                                                                        //Natural: ASSIGN #US-SUM.#C-QTY ( #TI ) := #US-SUM.#C-QTY ( #TI ) + #SUM.#C-QTY ( #TI,#PI )
            pnd_Us_Sum_Pnd_F_Qty.getValue(pnd_Ti).nadd(pnd_Sum_Pnd_F_Qty.getValue(pnd_Ti,pnd_Pi));                                                                        //Natural: ASSIGN #US-SUM.#F-QTY ( #TI ) := #US-SUM.#F-QTY ( #TI ) + #SUM.#F-QTY ( #TI,#PI )
            getReports().write(2, pnd_Sub_Txt_A, new AlphanumericLength (30),new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_Sum_Pnd_Current_A.getValue(pnd_Ti,pnd_Pi),  //Natural: WRITE ( PRT1 ) #SUB-TXT-A ( AL = 30 ) 032T 'CHECK AMOUNT' 045T #SUM.#CURRENT-A ( #TI,#PI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #SUM.#FUTURE-A ( #TI,#PI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL-A ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / #SUB-TXT-M ( AL = 30 ) 032T 'CHECK AMOUNT' 045T #SUM.#CURRENT-M ( #TI,#PI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #SUM.#FUTURE-M ( #TI,#PI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL-M ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) /
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Sum_Pnd_Future_A.getValue(pnd_Ti,pnd_Pi), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
                TabSetting(95),pnd_T_Total_A, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,pnd_Sub_Txt_M, new AlphanumericLength (30),new TabSetting(32),"CHECK AMOUNT",new 
                TabSetting(45),pnd_Sum_Pnd_Current_M.getValue(pnd_Ti,pnd_Pi), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Sum_Pnd_Future_M.getValue(pnd_Ti,pnd_Pi), 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total_M, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ---------------------------------------------------------------- *
            pnd_T_Total_A.reset();                                                                                                                                        //Natural: RESET #T-TOTAL-A #T-TOTAL-M #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
            pnd_T_Total_M.reset();
            pnd_C_Total.reset();
            pnd_F_Total.reset();
            pnd_T_Total.reset();
            pnd_Cqty.reset();
            pnd_Fqty.reset();
            pnd_Tqty.reset();
            //* V2.02G
            pnd_C_Total.nadd(pnd_Sum_Pnd_Current_A.getValue(pnd_Ti,pnd_Pi));                                                                                              //Natural: ADD #SUM.#CURRENT-A ( #TI,#PI ) TO #C-TOTAL
            pnd_C_Total.nadd(pnd_Sum_Pnd_Current_M.getValue(pnd_Ti,pnd_Pi));                                                                                              //Natural: ADD #SUM.#CURRENT-M ( #TI,#PI ) TO #C-TOTAL
            pnd_F_Total.nadd(pnd_Sum_Pnd_Future_A.getValue(pnd_Ti,pnd_Pi));                                                                                               //Natural: ADD #SUM.#FUTURE-A ( #TI,#PI ) TO #F-TOTAL
            //* V2.02G
            pnd_F_Total.nadd(pnd_Sum_Pnd_Future_M.getValue(pnd_Ti,pnd_Pi));                                                                                               //Natural: ADD #SUM.#FUTURE-M ( #TI,#PI ) TO #F-TOTAL
            pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_F_Total.add(pnd_C_Total));                                                                 //Natural: ASSIGN #T-TOTAL := #F-TOTAL + #C-TOTAL
            pnd_Cqty.nadd(pnd_Sum_Pnd_C_Qty.getValue(pnd_Ti,pnd_Pi));                                                                                                     //Natural: ASSIGN #CQTY := #CQTY + #SUM.#C-QTY ( #TI,#PI )
            pnd_Fqty.nadd(pnd_Sum_Pnd_F_Qty.getValue(pnd_Ti,pnd_Pi));                                                                                                     //Natural: ASSIGN #FQTY := #FQTY + #SUM.#F-QTY ( #TI,#PI )
            pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Fqty.add(pnd_Cqty));                                                                             //Natural: ASSIGN #TQTY := #FQTY + #CQTY
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_C_Total, new ReportEditMask                    //Natural: WRITE ( PRT1 ) / / / 'TOTAL' 032T 'CHECK AMOUNT' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #F-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #CQTY ( EM = ZZZ,ZZZ,ZZ9 ) 078T #FQTY ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 )
                ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, 
                new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(35),"CHECK QTY",new TabSetting(53),pnd_Cqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
                TabSetting(78),pnd_Fqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Header.getValue(4).setValue("U.S. SUMMARY TOTALS");                                                                                                           //Natural: ASSIGN #HEADER ( 4 ) := 'U.S. SUMMARY TOTALS'
        //*  1.06C
        //*  1.02G - DO THIS HERE...
                                                                                                                                                                          //Natural: PERFORM CENTER_HEADER_TXT
        sub_Center_Header_Txt();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRT1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, NEWLINE,NEWLINE,new TabSetting(56),"1ST PAYMENT",NEWLINE,new TabSetting(58),"CURRENT",new TabSetting(86),"FUTURE",new TabSetting(112),      //Natural: WRITE ( PRT1 ) / / 056T '1ST PAYMENT' / 058T 'CURRENT' 086T 'FUTURE' 112T 'TOTAL' /
            "TOTAL",NEWLINE);
        if (Global.isEscape()) return;
        //*  ---------------------------------------------------------------- *
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TXT
        sub_Initialize_Txt();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_T_Total_A.compute(new ComputeParameters(false, pnd_T_Total_A), pnd_Us_Sum_Pnd_Current_A.getValue(pnd_Ti).add(pnd_Us_Sum_Pnd_Future_A.getValue(pnd_Ti)));      //Natural: ASSIGN #T-TOTAL-A := #US-SUM.#CURRENT-A ( #TI ) + #US-SUM.#FUTURE-A ( #TI )
        pnd_T_Total_M.compute(new ComputeParameters(false, pnd_T_Total_M), pnd_Us_Sum_Pnd_Current_M.getValue(pnd_Ti).add(pnd_Us_Sum_Pnd_Future_M.getValue(pnd_Ti)));      //Natural: ASSIGN #T-TOTAL-M := #US-SUM.#CURRENT-M ( #TI ) + #US-SUM.#FUTURE-M ( #TI )
        getReports().write(2, pnd_Sub_Txt_A, new AlphanumericLength (30),new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_Us_Sum_Pnd_Current_A.getValue(pnd_Ti),  //Natural: WRITE ( PRT1 ) #SUB-TXT-A ( AL = 30 ) 032T 'CHECK AMOUNT' 045T #US-SUM.#CURRENT-A ( #TI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #US-SUM.#FUTURE-A ( #TI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL-A ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / #SUB-TXT-M ( AL = 30 ) 032T 'CHECK AMOUNT' 045T #US-SUM.#CURRENT-M ( #TI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #US-SUM.#FUTURE-M ( #TI ) ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL-M ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) /
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Us_Sum_Pnd_Future_A.getValue(pnd_Ti), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new 
            TabSetting(95),pnd_T_Total_A, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,pnd_Sub_Txt_M, new AlphanumericLength (30),new TabSetting(32),"CHECK AMOUNT",new 
            TabSetting(45),pnd_Us_Sum_Pnd_Current_M.getValue(pnd_Ti), new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(70),pnd_Us_Sum_Pnd_Future_M.getValue(pnd_Ti), 
            new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total_M, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE);
        if (Global.isEscape()) return;
        //*  ---------------------------------------------------------------- *
        pnd_T_Total_A.reset();                                                                                                                                            //Natural: RESET #T-TOTAL-A #T-TOTAL-M #C-TOTAL #F-TOTAL #T-TOTAL #CQTY #FQTY #TQTY
        pnd_T_Total_M.reset();
        pnd_C_Total.reset();
        pnd_F_Total.reset();
        pnd_T_Total.reset();
        pnd_Cqty.reset();
        pnd_Fqty.reset();
        pnd_Tqty.reset();
        //*  V1.00A
        pnd_C_Total.nadd(pnd_Us_Sum_Pnd_Current_A.getValue(pnd_Ti));                                                                                                      //Natural: ADD #US-SUM.#CURRENT-A ( #TI ) TO #C-TOTAL
        pnd_C_Total.nadd(pnd_Us_Sum_Pnd_Current_M.getValue(pnd_Ti));                                                                                                      //Natural: ADD #US-SUM.#CURRENT-M ( #TI ) TO #C-TOTAL
        pnd_F_Total.nadd(pnd_Us_Sum_Pnd_Future_A.getValue(pnd_Ti));                                                                                                       //Natural: ADD #US-SUM.#FUTURE-A ( #TI ) TO #F-TOTAL
        pnd_F_Total.nadd(pnd_Us_Sum_Pnd_Future_M.getValue(pnd_Ti));                                                                                                       //Natural: ADD #US-SUM.#FUTURE-M ( #TI ) TO #F-TOTAL
        pnd_T_Total.compute(new ComputeParameters(false, pnd_T_Total), pnd_F_Total.add(pnd_C_Total));                                                                     //Natural: ASSIGN #T-TOTAL := #F-TOTAL + #C-TOTAL
        pnd_Cqty.nadd(pnd_Us_Sum_Pnd_C_Qty.getValue(pnd_Ti));                                                                                                             //Natural: ASSIGN #CQTY := #CQTY + #US-SUM.#C-QTY ( #TI )
        pnd_Fqty.nadd(pnd_Us_Sum_Pnd_F_Qty.getValue(pnd_Ti));                                                                                                             //Natural: ASSIGN #FQTY := #FQTY + #US-SUM.#F-QTY ( #TI )
        pnd_Tqty.compute(new ComputeParameters(false, pnd_Tqty), pnd_Fqty.add(pnd_Cqty));                                                                                 //Natural: ASSIGN #TQTY := #FQTY + #CQTY
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,"TOTAL",new TabSetting(32),"CHECK AMOUNT",new TabSetting(45),pnd_C_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new  //Natural: WRITE ( PRT1 ) / / / 'TOTAL' 032T 'CHECK AMOUNT' 045T #C-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 070T #F-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) 095T #T-TOTAL ( EM = ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99 ) / 035T 'CHECK QTY' 053T #CQTY ( EM = ZZZ,ZZZ,ZZ9 ) 078T #FQTY ( EM = ZZZ,ZZZ,ZZ9 ) 103T #TQTY ( EM = ZZZ,ZZZ,ZZ9 )
            TabSetting(70),pnd_F_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(95),pnd_T_Total, new ReportEditMask ("ZZZ,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(35),"CHECK QTY",new TabSetting(53),pnd_Cqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new TabSetting(78),pnd_Fqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"),new 
            TabSetting(103),pnd_Tqty, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        if (condition(getReports().getAstLinesLeft(0).less(6)))                                                                                                           //Natural: NEWPAGE ( PRT1 ) IF LESS THAN 6 LINES LEFT
        {
            getReports().newPage(0);
            if (condition(Global.isEscape())){return;}
        }
        pnd_Header.getValue(4).setValue(DbsUtil.compress("END OF REPORT for", pnd_Hdr_Pfx.getValue(pnd_Rpt_Ndx), "Settlements"));                                         //Natural: COMPRESS 'END OF REPORT for' #HDR_PFX ( #RPT_NDX ) 'Settlements' INTO #HEADER ( 4 )
                                                                                                                                                                          //Natural: PERFORM CENTER_HEADER_TXT
        sub_Center_Header_Txt();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        getReports().write(2, NEWLINE,NEWLINE,new TabSetting(13),"*",new RepeatItem(106));                                                                                //Natural: WRITE ( PRT1 ) // 13T '*' ( 106 )
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(13),"**",pnd_Header.getValue(4),"**");                                                                                       //Natural: WRITE ( PRT1 ) 13T '**' #HEADER ( 4 ) '**'
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(13),"*",new RepeatItem(106));                                                                                                //Natural: WRITE ( PRT1 ) 13T '*' ( 106 )
        if (Global.isEscape()) return;
        //*  NEWPAGE(PRT1)
        //*  PERFORM DUMP_#SUM
        //*  ------------- *
        //*   PRINT-SUMMARY
    }
    //*  (DEBUG)
    private void sub_Dump_Pnd_Sum() throws Exception                                                                                                                      //Natural: DUMP_#SUM
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //*  1.04A - TURN THIS OFF FOR PROD
        if (condition(pnd_Now.getBoolean()))                                                                                                                              //Natural: IF #NOW THEN
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(100));                                                                                      //Natural: WRITE / '-' ( 100 )
        if (Global.isEscape()) return;
        FOR07:                                                                                                                                                            //Natural: FOR #X_1 1 TO 9
        for (pnd_X_1.setValue(1); condition(pnd_X_1.lessOrEqual(9)); pnd_X_1.nadd(1))
        {
            FOR08:                                                                                                                                                        //Natural: FOR #X_2 1 TO 3
            for (pnd_X_2.setValue(1); condition(pnd_X_2.lessOrEqual(3)); pnd_X_2.nadd(1))
            {
                getReports().write(0, ReportOption.NOTITLE,pnd_Rpt_Ndx,"#CURR-A(",pnd_X_1,pnd_X_2,")=",pnd_Sum_Pnd_Current_A.getValue(pnd_X_1,pnd_X_2),                   //Natural: WRITE #RPT_NDX '#CURR-A(' #X_1 #X_2 ')=' #SUM.#CURRENT-A ( #X_1,#X_2 ) '#CURR-M(' #X_1 #X_2 ')=' #SUM.#CURRENT-M ( #X_1,#X_2 )
                    "#CURR-M(",pnd_X_1,pnd_X_2,")=",pnd_Sum_Pnd_Current_M.getValue(pnd_X_1,pnd_X_2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,pnd_Rpt_Ndx,"#FUTR-A(",pnd_X_1,pnd_X_2,")=",pnd_Sum_Pnd_Future_A.getValue(pnd_X_1,pnd_X_2),                    //Natural: WRITE #RPT_NDX '#FUTR-A(' #X_1 #X_2 ')=' #SUM.#FUTURE-A ( #X_1,#X_2 ) '#FUTR-M(' #X_1 #X_2 ')=' #SUM.#FUTURE-A ( #X_1,#X_2 )
                    "#FUTR-M(",pnd_X_1,pnd_X_2,")=",pnd_Sum_Pnd_Future_A.getValue(pnd_X_1,pnd_X_2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, ReportOption.NOTITLE,pnd_Rpt_Ndx,"#C-QTY (",pnd_X_1,pnd_X_2,")=",new TabSetting(31),pnd_Sum_Pnd_C_Qty.getValue(pnd_X_1,pnd_X_2),new //Natural: WRITE #RPT_NDX '#C-QTY (' #X_1 #X_2 ')=' 31T #SUM.#C-QTY ( #X_1,#X_2 ) 41T '#F-QTY (' #X_1 #X_2 ')=' 68T #SUM.#F-QTY ( #X_1,#X_2 )
                    TabSetting(41),"#F-QTY (",pnd_X_1,pnd_X_2,")=",new TabSetting(68),pnd_Sum_Pnd_F_Qty.getValue(pnd_X_1,pnd_X_2));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,"-",new RepeatItem(100));                                                                                      //Natural: WRITE / '-' ( 100 )
        if (Global.isEscape()) return;
        //*  ------------- *
        //*   DUMP_#SUM
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET CHECK-NO PREFIX FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, ReportOption.NOTITLE,"**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Hdr_Sw.getBoolean()))                                                                                                               //Natural: IF #HDR_SW THEN
                    {
                        //*  1.02B
                        if (condition(! (pnd_Summary.getBoolean())))                                                                                                      //Natural: IF NOT #SUMMARY
                        {
                            pnd_Header.getValue(4).setValue(pnd_Sub_Txt);                                                                                                 //Natural: ASSIGN #HEADER ( 4 ) := #SUB-TXT
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-TXT
                            sub_Initialize_Txt();
                            if (condition(Global.isEscape())) {return;}
                            if (condition(Map.getDoInput())) {return;}
                            //*  02-13-95 : A. YOUNG
                            //*  1.04A
                            //*  1.2D
                            //*  1.05B
                        }                                                                                                                                                 //Natural: END-IF
                        //* *  PERFORM INITIALIZE-HEADER
                        getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),Global.getPROGRAM(),new TabSetting(21),pnd_Header.getValue(1),new                //Natural: WRITE ( PRT1 ) NOTITLE *INIT-USER *PROGRAM 21T #HEADER ( 1 ) 124T 'Page' *PAGE-NUMBER ( PRT1 ) ( NL = 4 )
                            TabSetting(124),"Page",getReports().getPageNumberDbs(2), new NumericLength (4));
                        getReports().write(2, Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(21),pnd_Header.getValue(2),new                     //Natural: WRITE ( PRT1 ) *DATX ( EM = LLL' 'DD', 'YYYY ) 21T #HEADER ( 2 ) 124T *TIMX ( EM = HH':'II' 'AP ) / 21T #HEADER ( 4 ) #CF-TXT
                            TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(21),pnd_Header.getValue(4),pnd_Cf_Txt);
                        getReports().skip(0, 1);                                                                                                                          //Natural: SKIP ( PRT1 ) 1
                        if (condition(! (pnd_Summary.getBoolean())))                                                                                                      //Natural: IF NOT #SUMMARY
                        {
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-HEADER
                            sub_Print_Detail_Header();
                            if (condition(Global.isEscape())) {return;}
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                        //*    IF #SUB-PRT                     /* @@ DISABLE THIS JUNK
                        //*      RESET #SUB-PRT
                        //*      COMPRESS #SUB-TXT 'GRAND TOTALS:' INTO #SUB-TXT /*@@
                        //*      MOVE RIGHT #SUB-TXT TO #SUB-TXT
                        //*    END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Intrfce_DteIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().isBreak(endOfData);
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Ftre_IndIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().isBreak(endOfData);
        boolean pnd_Category_CodeIsBreak = pnd_Category_Code.isBreak(endOfData);
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Intrfce_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Ftre_IndIsBreak 
            || pnd_Category_CodeIsBreak))
        {
            //*  12-12-98
            if (condition(pnd_Input_Parm.equals("LEDGR")))                                                                                                                //Natural: IF #INPUT-PARM = 'LEDGR'
            {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ITEM-SUB-TOTALS
                sub_Calculate_Item_Sub_Totals();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                sub_Print_Detail_Lines();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_O_Ftre_Ind.setValue(readWork01Pymnt_Ftre_IndOld);                                                                                                     //Natural: ASSIGN #O-FTRE-IND := OLD ( #RPT-EXT.PYMNT-FTRE-IND )
                //*  @@
                                                                                                                                                                          //Natural: PERFORM PRINT-INTRFCE-DTE-SUB-TOTAL
                sub_Print_Intrfce_Dte_Sub_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Hdr_Dte.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte(),new ReportEditMask("MM/DD/YYYY"));                                              //Natural: MOVE EDITED #RPT-EXT.PYMNT-INTRFCE-DTE ( EM = MM/DD/YYYY ) TO #HDR-DTE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak || ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Ftre_IndIsBreak || pnd_Category_CodeIsBreak))
        {
            //*  12-12-98
            if (condition(! (pnd_Input_Parm.equals("LEDGR"))))                                                                                                            //Natural: IF NOT #INPUT-PARM = 'LEDGR'
            {
                if (condition(! (pnd_Month_End.getBoolean())))                                                                                                            //Natural: IF NOT #MONTH-END
                {
                                                                                                                                                                          //Natural: PERFORM CALCULATE-ITEM-SUB-TOTALS
                    sub_Calculate_Item_Sub_Totals();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL-LINES
                    sub_Print_Detail_Lines();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_O_Ftre_Ind.setValue(readWork01Pymnt_Ftre_IndOld);                                                                                                 //Natural: ASSIGN #O-FTRE-IND := OLD ( #RPT-EXT.PYMNT-FTRE-IND )
                                                                                                                                                                          //Natural: PERFORM PRINT-DTE-SUB-TOTAL
                    sub_Print_Dte_Sub_Total();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Hdr_Dte.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                            //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #HDR-DTE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Ftre_IndIsBreak || pnd_Category_CodeIsBreak))
        {
            //*  1.06
            if (condition(! (pnd_Rpt_Break.getBoolean())))                                                                                                                //Natural: IF NOT #RPT_BREAK
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ftre_Ind().equals("F")))                                                                                   //Natural: IF #RPT-EXT.PYMNT-FTRE-IND = 'F'
                {
                    pnd_Cf_Txt.setValue("(FUTURE)");                                                                                                                      //Natural: ASSIGN #CF-TXT := '(FUTURE)'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cf_Txt.setValue("(CURRENT)");                                                                                                                     //Natural: ASSIGN #CF-TXT := '(CURRENT)'
                    //*  1.02F DO PAGE EJECT AT THIS BREAK
                }                                                                                                                                                         //Natural: END-IF
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( PRT1 )
                if (condition(Global.isEscape())){return;}
            }                                                                                                                                                             //Natural: END-IF
            //*  1.06
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pnd_Category_CodeIsBreak))
        {
            pnd_Rpt_Break.setValue(true);                                                                                                                                 //Natural: ASSIGN #RPT_BREAK := TRUE
                                                                                                                                                                          //Natural: PERFORM END-REPORT
            sub_End_Report();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
