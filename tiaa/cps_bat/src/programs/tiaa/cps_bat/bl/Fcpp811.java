/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:12 PM
**        * FROM NATURAL PROGRAM : Fcpp811
************************************************************
**        * FILE NAME            : Fcpp811.java
**        * CLASS NAME           : Fcpp811
**        * INSTANCE NAME        : Fcpp811
************************************************************
************************************************************************
****    LOGIC FOR DETERMINING IRS LIENS STILL UNKNOWN    !!!!!!!!!!!
************************************************************************
*
* PROGRAM   : FCPP811
*
* SYSTEM    : CPS
* TITLE     : PAYMENT REGISTERS
* GENERATED :
*
* FUNCTION  : GENERATE REPORTS AFTER CHECK PRINTING:
*           :    - PAYMENT REGISTER FOR ALL PROCESSED CONTRACTS
*           :    - TOTAL BY FUND REPORT
*           :    - GLOBAL PAY REGISTER
*           :    - IRS LIENS
*           :    - NET ZERO PAYMENTS
*
* HISTORY   :
* 01/99     : ROXAN CARREON
*                - PA SELECT PROCESSING
*
* 05/22/00  : ADD PASELECT SPIA VARIABLE - TMM
* 07/20/00  : ROXAN CARREON
*           : FIX ERROR IN REPORT CAUSED BY CODES FOR PA-SELECT/SPIA
* 06/13/02  : ROXAN CARREON
*           : INCLUDE NEW FUNDS FOR SPIA AND PA-SELECT
* 05/07/08  : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp811 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa803c pdaFcpa803c;
    private PdaFcpa199a pdaFcpa199a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Index;
    private DbsField pnd_Contract_Amt;
    private DbsField pnd_Dvdnd_Amt;
    private DbsField pnd_Tax_Amt;
    private DbsField pnd_Ded_Amt;
    private DbsField pnd_Net_Amt;
    private DbsField pnd_Total_Lit;

    private DbsGroup totals;
    private DbsField totals_Pnd_T_Count;
    private DbsField totals_Pnd_T_Cntrct;
    private DbsField totals_Pnd_T_Dvdnd;
    private DbsField totals_Pnd_T_Tax;
    private DbsField totals_Pnd_T_Ded;
    private DbsField totals_Pnd_T_Net;
    private DbsField pnd_Irs_Count;
    private DbsField pnd_Irs_Cntrct;
    private DbsField pnd_Irs_Dvdnd;
    private DbsField pnd_Irs_Tax;
    private DbsField pnd_Irs_Ded;
    private DbsField pnd_Irs_Net;
    private DbsField pnd_Irs_Check;

    private DbsGroup pnd_Accumulators;
    private DbsField pnd_Accumulators_Pnd_Ws_Total_Amt;
    private DbsField pnd_Accumulators_Pnd_Ws_Amount_Acc;
    private DbsField pnd_Ws_Mon_Ann_Tot;
    private DbsField pnd_Ws_Grand_Tot;
    private DbsField pnd_Ws_Grand_Tot_Pa;
    private DbsField pnd_Ws_Grand_Tot_Spia;
    private DbsField pnd_Ws_Strt;
    private DbsField pnd_Ws_Ndx;
    private DbsField pnd_Ws_Valuat;
    private DbsField pnd_Ws_Mon_Ann_Desc;

    private DbsGroup pnd_Ws_Descrition;
    private DbsField pnd_Ws_Descrition_Pnd_Filler1;
    private DbsField pnd_Ws_Descrition_Pnd_Filler2;
    private DbsField pnd_Ws_Descrition_Pnd_Filler3;
    private DbsField pnd_Ws_Descrition_Pnd_Filler4;
    private DbsField pnd_Ws_Descrition_Pnd_Filler5;
    private DbsField pnd_Ws_Descrition_Pnd_Filler7;
    private DbsField pnd_Ws_Descrition_Pnd_Filler8;
    private DbsField pnd_Ws_Descrition_Pnd_Filler6;
    private DbsField pnd_Ws_Descrition_Pnd_Filler9;
    private DbsField pnd_Ws_Descrition_Pnd_Filler10;
    private DbsField pnd_Ws_Descrition_Pnd_Filler11;
    private DbsField pnd_Ws_Descrition_Pnd_Filler12;
    private DbsField pnd_Ws_Descrition_Pnd_Filler13;
    private DbsField pnd_Ws_Descrition_Pnd_Filler14;
    private DbsField pnd_Ws_Descrition_Pnd_Filler15;
    private DbsField pnd_Ws_Descrition_Pnd_Filler16;
    private DbsField pnd_Ws_Descrition_Pnd_Filler17;
    private DbsField pnd_Ws_Descrition_Pnd_Filler18;
    private DbsField pnd_Ws_Descrition_Pnd_Filler19;
    private DbsField pnd_Ws_Descrition_Pnd_Filler19a;
    private DbsField pnd_Ws_Descrition_Pnd_Filler19b;
    private DbsField pnd_Ws_Descrition_Pnd_Filler19c;
    private DbsField pnd_Ws_Descrition_Pnd_Filler20;
    private DbsField pnd_Ws_Descrition_Pnd_Filler21;
    private DbsField pnd_Ws_Descrition_Pnd_Filler21f;
    private DbsField pnd_Ws_Descrition_Pnd_Filler22;
    private DbsField pnd_Ws_Descrition_Pnd_Filler23;
    private DbsField pnd_Ws_Descrition_Pnd_Filler24;
    private DbsField pnd_Ws_Descrition_Pnd_Filler25;
    private DbsField pnd_Ws_Descrition_Pnd_Filler26;
    private DbsField pnd_Ws_Descrition_Pnd_Filler26a;
    private DbsField pnd_Ws_Descrition_Pnd_Filler26b;
    private DbsField pnd_Ws_Descrition_Pnd_Filler26c;
    private DbsField pnd_Ws_Descrition_Pnd_Filler27;
    private DbsField pnd_Ws_Descrition_Pnd_Filler28;
    private DbsField pnd_Ws_Descrition_Pnd_Filler29;

    private DbsGroup pnd_Ws_Descrition__R_Field_1;
    private DbsField pnd_Ws_Descrition_Pnd_Ws_Desc_Table;
    private DbsField pnd_Pymnt_Nbr;

    private DbsGroup pnd_Pymnt_Nbr__R_Field_2;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Type;
    private DbsField pnd_Pymnt_Nbr_Pnd_Pymnt_Num;
    private DbsField pnd_Glb_Type;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa803c = new PdaFcpa803c(localVariables);
        pdaFcpa199a = new PdaFcpa199a(localVariables);

        // Local Variables
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Contract_Amt = localVariables.newFieldInRecord("pnd_Contract_Amt", "#CONTRACT-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Dvdnd_Amt = localVariables.newFieldInRecord("pnd_Dvdnd_Amt", "#DVDND-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Tax_Amt = localVariables.newFieldInRecord("pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ded_Amt = localVariables.newFieldInRecord("pnd_Ded_Amt", "#DED-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Net_Amt = localVariables.newFieldInRecord("pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Total_Lit = localVariables.newFieldInRecord("pnd_Total_Lit", "#TOTAL-LIT", FieldType.STRING, 25);

        totals = localVariables.newGroupArrayInRecord("totals", "TOTALS", new DbsArrayController(1, 9));
        totals_Pnd_T_Count = totals.newFieldInGroup("totals_Pnd_T_Count", "#T-COUNT", FieldType.PACKED_DECIMAL, 7);
        totals_Pnd_T_Cntrct = totals.newFieldInGroup("totals_Pnd_T_Cntrct", "#T-CNTRCT", FieldType.PACKED_DECIMAL, 11, 2);
        totals_Pnd_T_Dvdnd = totals.newFieldInGroup("totals_Pnd_T_Dvdnd", "#T-DVDND", FieldType.PACKED_DECIMAL, 11, 2);
        totals_Pnd_T_Tax = totals.newFieldInGroup("totals_Pnd_T_Tax", "#T-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        totals_Pnd_T_Ded = totals.newFieldInGroup("totals_Pnd_T_Ded", "#T-DED", FieldType.PACKED_DECIMAL, 11, 2);
        totals_Pnd_T_Net = totals.newFieldInGroup("totals_Pnd_T_Net", "#T-NET", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irs_Count = localVariables.newFieldInRecord("pnd_Irs_Count", "#IRS-COUNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Irs_Cntrct = localVariables.newFieldInRecord("pnd_Irs_Cntrct", "#IRS-CNTRCT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irs_Dvdnd = localVariables.newFieldInRecord("pnd_Irs_Dvdnd", "#IRS-DVDND", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irs_Tax = localVariables.newFieldInRecord("pnd_Irs_Tax", "#IRS-TAX", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irs_Ded = localVariables.newFieldInRecord("pnd_Irs_Ded", "#IRS-DED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irs_Net = localVariables.newFieldInRecord("pnd_Irs_Net", "#IRS-NET", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Irs_Check = localVariables.newFieldInRecord("pnd_Irs_Check", "#IRS-CHECK", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Accumulators = localVariables.newGroupArrayInRecord("pnd_Accumulators", "#ACCUMULATORS", new DbsArrayController(1, 2));
        pnd_Accumulators_Pnd_Ws_Total_Amt = pnd_Accumulators.newFieldInGroup("pnd_Accumulators_Pnd_Ws_Total_Amt", "#WS-TOTAL-AMT", FieldType.PACKED_DECIMAL, 
            12, 2);
        pnd_Accumulators_Pnd_Ws_Amount_Acc = pnd_Accumulators.newFieldArrayInGroup("pnd_Accumulators_Pnd_Ws_Amount_Acc", "#WS-AMOUNT-ACC", FieldType.PACKED_DECIMAL, 
            12, 2, new DbsArrayController(1, 36));
        pnd_Ws_Mon_Ann_Tot = localVariables.newFieldInRecord("pnd_Ws_Mon_Ann_Tot", "#WS-MON-ANN-TOT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Grand_Tot = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot", "#WS-GRAND-TOT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Grand_Tot_Pa = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Pa", "#WS-GRAND-TOT-PA", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Grand_Tot_Spia = localVariables.newFieldInRecord("pnd_Ws_Grand_Tot_Spia", "#WS-GRAND-TOT-SPIA", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Ws_Strt = localVariables.newFieldInRecord("pnd_Ws_Strt", "#WS-STRT", FieldType.NUMERIC, 2);
        pnd_Ws_Ndx = localVariables.newFieldInRecord("pnd_Ws_Ndx", "#WS-NDX", FieldType.NUMERIC, 2);
        pnd_Ws_Valuat = localVariables.newFieldInRecord("pnd_Ws_Valuat", "#WS-VALUAT", FieldType.NUMERIC, 2);
        pnd_Ws_Mon_Ann_Desc = localVariables.newFieldInRecord("pnd_Ws_Mon_Ann_Desc", "#WS-MON-ANN-DESC", FieldType.STRING, 10);

        pnd_Ws_Descrition = localVariables.newGroupInRecord("pnd_Ws_Descrition", "#WS-DESCRITION");
        pnd_Ws_Descrition_Pnd_Filler1 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler1", "#FILLER1", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler2 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler2", "#FILLER2", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler3 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler3", "#FILLER3", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler4 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler4", "#FILLER4", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler5 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler5", "#FILLER5", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler7 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler7", "#FILLER7", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler8 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler8", "#FILLER8", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler6 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler6", "#FILLER6", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler9 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler9", "#FILLER9", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler10 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler10", "#FILLER10", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler11 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler11", "#FILLER11", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler12 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler12", "#FILLER12", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler13 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler13", "#FILLER13", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler14 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler14", "#FILLER14", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler15 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler15", "#FILLER15", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler16 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler16", "#FILLER16", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler17 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler17", "#FILLER17", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler18 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler18", "#FILLER18", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler19 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler19", "#FILLER19", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler19a = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler19a", "#FILLER19A", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler19b = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler19b", "#FILLER19B", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler19c = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler19c", "#FILLER19C", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler20 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler20", "#FILLER20", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler21 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler21", "#FILLER21", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler21f = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler21f", "#FILLER21F", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler22 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler22", "#FILLER22", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler23 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler23", "#FILLER23", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler24 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler24", "#FILLER24", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler25 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler25", "#FILLER25", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler26 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler26", "#FILLER26", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler26a = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler26a", "#FILLER26A", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler26b = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler26b", "#FILLER26B", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler26c = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler26c", "#FILLER26C", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler27 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler27", "#FILLER27", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler28 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler28", "#FILLER28", FieldType.STRING, 30);
        pnd_Ws_Descrition_Pnd_Filler29 = pnd_Ws_Descrition.newFieldInGroup("pnd_Ws_Descrition_Pnd_Filler29", "#FILLER29", FieldType.STRING, 30);

        pnd_Ws_Descrition__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Descrition__R_Field_1", "REDEFINE", pnd_Ws_Descrition);
        pnd_Ws_Descrition_Pnd_Ws_Desc_Table = pnd_Ws_Descrition__R_Field_1.newFieldArrayInGroup("pnd_Ws_Descrition_Pnd_Ws_Desc_Table", "#WS-DESC-TABLE", 
            FieldType.STRING, 30, new DbsArrayController(1, 36));
        pnd_Pymnt_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Nbr", "#PYMNT-NBR", FieldType.STRING, 11);

        pnd_Pymnt_Nbr__R_Field_2 = localVariables.newGroupInRecord("pnd_Pymnt_Nbr__R_Field_2", "REDEFINE", pnd_Pymnt_Nbr);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Type = pnd_Pymnt_Nbr__R_Field_2.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Type", "#PYMNT-TYPE", FieldType.STRING, 1);
        pnd_Pymnt_Nbr_Pnd_Pymnt_Num = pnd_Pymnt_Nbr__R_Field_2.newFieldInGroup("pnd_Pymnt_Nbr_Pnd_Pymnt_Num", "#PYMNT-NUM", FieldType.NUMERIC, 10);
        pnd_Glb_Type = localVariables.newFieldInRecord("pnd_Glb_Type", "#GLB-TYPE", FieldType.STRING, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Ws_Strt.setInitialValue(0);
        pnd_Ws_Ndx.setInitialValue(0);
        pnd_Ws_Valuat.setInitialValue(0);
        pnd_Ws_Descrition_Pnd_Filler1.setInitialValue("  TIAA                        ");
        pnd_Ws_Descrition_Pnd_Filler2.setInitialValue("  TIAA Insurance              ");
        pnd_Ws_Descrition_Pnd_Filler3.setInitialValue("  Real Estate                 ");
        pnd_Ws_Descrition_Pnd_Filler4.setInitialValue("TIAA Total                    ");
        pnd_Ws_Descrition_Pnd_Filler5.setInitialValue("  Stock                       ");
        pnd_Ws_Descrition_Pnd_Filler7.setInitialValue("  Money Market                ");
        pnd_Ws_Descrition_Pnd_Filler8.setInitialValue("  Social Choice               ");
        pnd_Ws_Descrition_Pnd_Filler6.setInitialValue("  Bond                        ");
        pnd_Ws_Descrition_Pnd_Filler9.setInitialValue("  Global                      ");
        pnd_Ws_Descrition_Pnd_Filler10.setInitialValue("  Growth                      ");
        pnd_Ws_Descrition_Pnd_Filler11.setInitialValue("  Equity Index                ");
        pnd_Ws_Descrition_Pnd_Filler12.setInitialValue("  Infl.  Linked Bond          ");
        pnd_Ws_Descrition_Pnd_Filler13.setInitialValue("CREF Total                    ");
        pnd_Ws_Descrition_Pnd_Filler14.setInitialValue("  PA Select Fixed Fund        ");
        pnd_Ws_Descrition_Pnd_Filler15.setInitialValue("  PA Select Stock Index       ");
        pnd_Ws_Descrition_Pnd_Filler16.setInitialValue("  PA Select Growth Equity     ");
        pnd_Ws_Descrition_Pnd_Filler17.setInitialValue("  PA Select Growth Income     ");
        pnd_Ws_Descrition_Pnd_Filler18.setInitialValue("  PA Select International     ");
        pnd_Ws_Descrition_Pnd_Filler19.setInitialValue("  PA Select Social Choice     ");
        pnd_Ws_Descrition_Pnd_Filler19a.setInitialValue("  PA Select Large-Cap Value   ");
        pnd_Ws_Descrition_Pnd_Filler19b.setInitialValue("  PA Select Small-Cap Value   ");
        pnd_Ws_Descrition_Pnd_Filler19c.setInitialValue("  PA Select Real Estate Sec   ");
        pnd_Ws_Descrition_Pnd_Filler20.setInitialValue("PA Select Total               ");
        pnd_Ws_Descrition_Pnd_Filler21.setInitialValue("  SPIA Fixed Fund             ");
        pnd_Ws_Descrition_Pnd_Filler21f.setInitialValue("  SPIA General Account        ");
        pnd_Ws_Descrition_Pnd_Filler22.setInitialValue("  SPIA Stock Index            ");
        pnd_Ws_Descrition_Pnd_Filler23.setInitialValue("  SPIA Growth Equity          ");
        pnd_Ws_Descrition_Pnd_Filler24.setInitialValue("  SPIA Growth Income          ");
        pnd_Ws_Descrition_Pnd_Filler25.setInitialValue("  SPIA International          ");
        pnd_Ws_Descrition_Pnd_Filler26.setInitialValue("  SPIA Social Choice          ");
        pnd_Ws_Descrition_Pnd_Filler26a.setInitialValue("  SPIA Large-Cap Value   ");
        pnd_Ws_Descrition_Pnd_Filler26b.setInitialValue("  SPIA Small-Cap Value   ");
        pnd_Ws_Descrition_Pnd_Filler26c.setInitialValue("  SPIA Real Estate Sec   ");
        pnd_Ws_Descrition_Pnd_Filler27.setInitialValue("SPIA Total                    ");
        pnd_Ws_Descrition_Pnd_Filler28.setInitialValue("                              ");
        pnd_Ws_Descrition_Pnd_Filler29.setInitialValue("Grand Total                   ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp811() throws Exception
    {
        super("Fcpp811");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP811", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 135 PS = 55;//Natural: FORMAT ( 02 ) LS = 132 PS = 55;//Natural: FORMAT ( 03 ) LS = 135 PS = 55;//Natural: FORMAT ( 04 ) LS = 132 PS = 55;//Natural: FORMAT ( 05 ) LS = 132 PS = 55;//Natural: FORMAT ( 06 ) LS = 135 PS = 55;//Natural: FORMAT ( 07 ) LS = 135 PS = 55
        //*                                                                                                                                                               //Natural: ON ERROR
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '- 1' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 45T 'PAYMENT REGISTER FOR' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '- 2' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 45T 'TOTAL PAYMENTS BY FUND FOR' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '- 3' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 45T 'GLOBAL PAY REGISTER FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 04 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '- 4' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 04 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 51T 'IRS LIENS FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 05 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '- 5' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 05 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T 'ANNUITY PAYMENTS' / *TIMX ( EM = HH:II' 'AP ) 49T 'NET ZERO PAYMENTS' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 06 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '- 6' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 06 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T '   PA-Select    ' / *TIMX ( EM = HH:II' 'AP ) 45T 'PAYMENT REGISTER FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 07 ) TITLE LEFT JUSTIFIED *INIT-USER '-' *PROGRAM '- 7' 51T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 07 ) ( EM = ZZ,ZZ9 ) / *DATX ( EM = LLL' 'DD', 'YYYY ) 56T '      SPIA      ' / *TIMX ( EM = HH:II' 'AP ) 45T 'PAYMENT REGISTER FOR' PYMNT-CHECK-DTE ( EM = LLLLLLLLL', 'YYYY ) //
        //*  READ PAYMENT/NAME AND ADDRESS WORK FILE
        //*  READ WORK FILE 1 WF-PYMNT-ADDR-REC             /* RA 12/29/05
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CHECK-SORT-FIELDS.PYMNT-NBR WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            pnd_Dvdnd_Amt.reset();                                                                                                                                        //Natural: RESET #DVDND-AMT
            FOR01:                                                                                                                                                        //Natural: FOR #INDEX 1 INV-ACCT-COUNT
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
            {
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_Index).equals("M")))                                                  //Natural: IF INV-ACCT-VALUAT-PERIOD ( #INDEX ) = 'M'
                {
                    //*   MONTHLY
                    pnd_Ws_Valuat.setValue(2);                                                                                                                            //Natural: MOVE 2 TO #WS-VALUAT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*   ANNUAL
                    pnd_Ws_Valuat.setValue(1);                                                                                                                            //Natural: MOVE 1 TO #WS-VALUAT
                }                                                                                                                                                         //Natural: END-IF
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_Index));                          //Natural: ASSIGN #INV-ACCT-INPUT := WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-N ( #INDEX )
                DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                //Natural: CALLNAT 'FCPN199A' #FUND-PDA
                if (condition(Global.isEscape())) return;
                //*  06/13/02 FROM FCPN199A
                //*  FREELOOK
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Seq().equals(14)))                                                                                 //Natural: IF #INV-ACCT-SEQ EQ 14
                {
                    pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Seq().setValue(25);                                                                                          //Natural: ASSIGN #INV-ACCT-SEQ := 25
                }                                                                                                                                                         //Natural: END-IF
                //*  FIXED FUND
                //*  TO MATCH TABLE-DESC
                if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Seq().equals(13)))                                                                                 //Natural: IF #INV-ACCT-SEQ EQ 13
                {
                    pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Seq().nadd(1);                                                                                               //Natural: ASSIGN #INV-ACCT-SEQ := #INV-ACCT-SEQ + 1
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Ndx.setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Seq());                                                                                      //Natural: ASSIGN #WS-NDX := #INV-ACCT-SEQ
                if (condition(pnd_Ws_Ndx.greaterOrEqual(1) && pnd_Ws_Ndx.lessOrEqual(3)))                                                                                 //Natural: IF #WS-NDX = 1 THRU 3
                {
                    short decideConditionsMet680 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE;//Natural: VALUE 'I'
                    if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("I"))))
                    {
                        decideConditionsMet680++;
                        //*  WILL BE ADDED TO INSURANCE
                        pnd_Ws_Ndx.setValue(2);                                                                                                                           //Natural: MOVE 2 TO #WS-NDX
                    }                                                                                                                                                     //Natural: VALUE 'P'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("P"))))
                    {
                        decideConditionsMet680++;
                        pnd_Ws_Ndx.setValue(2);                                                                                                                           //Natural: MOVE 2 TO #WS-NDX
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pnd_Ws_Ndx.setValue(1);                                                                                                                           //Natural: MOVE 1 TO #WS-NDX
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                //* * SPLIT PA-SELECT SPIA-THEY HAVE SAME FUND CODES
                //*  23  = PA-SELECT TOTAL
                short decideConditionsMet693 = 0;                                                                                                                         //Natural: DECIDE ON FIRST #WS-NDX;//Natural: VALUE 4
                if (condition((pnd_Ws_Ndx.equals(4))))
                {
                    decideConditionsMet693++;
                    pnd_Ws_Ndx.setValue(3);                                                                                                                               //Natural: ASSIGN #WS-NDX := 3
                }                                                                                                                                                         //Natural: VALUE 14,16,17,18,19,20,21,22
                else if (condition((pnd_Ws_Ndx.equals(14) || pnd_Ws_Ndx.equals(16) || pnd_Ws_Ndx.equals(17) || pnd_Ws_Ndx.equals(18) || pnd_Ws_Ndx.equals(19) 
                    || pnd_Ws_Ndx.equals(20) || pnd_Ws_Ndx.equals(21) || pnd_Ws_Ndx.equals(22))))
                {
                    decideConditionsMet693++;
                    //*  PA-SELECT
                    //*  NEW FUNDS
                    short decideConditionsMet699 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE;//Natural: VALUE 'S'
                    if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S"))))
                    {
                        decideConditionsMet699++;
                        ignore();
                    }                                                                                                                                                     //Natural: VALUE 'M'
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M"))))
                    {
                        decideConditionsMet699++;
                        pnd_Ws_Ndx.nadd(10);                                                                                                                              //Natural: ADD 10 TO #WS-NDX
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                short decideConditionsMet712 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WS-NDX;//Natural: VALUE 1,2
                if (condition((pnd_Ws_Ndx.equals(1) || pnd_Ws_Ndx.equals(2))))
                {
                    decideConditionsMet712++;
                    //*  TIAA
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(1,4).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));                  //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( 1,4 )
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(1,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( 1,#WS-NDX )
                    pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_Ws_Ndx.equals(3))))
                {
                    decideConditionsMet712++;
                    //*  TIAA
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,4).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));      //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,4 )
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX )
                    pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #DVDND-AMT
                    //* * VALUE 4 = TIAA TOTALS
                }                                                                                                                                                         //Natural: VALUE 5,6,7,8,9,10,11,12
                else if (condition((pnd_Ws_Ndx.equals(5) || pnd_Ws_Ndx.equals(6) || pnd_Ws_Ndx.equals(7) || pnd_Ws_Ndx.equals(8) || pnd_Ws_Ndx.equals(9) 
                    || pnd_Ws_Ndx.equals(10) || pnd_Ws_Ndx.equals(11) || pnd_Ws_Ndx.equals(12))))
                {
                    decideConditionsMet712++;
                    //*  CREF
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,13).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,13 )
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX )
                    //*  FIXED PA-SELECT
                    pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #DVDND-AMT
                    //* * VALUE 13 = CREF TOTALS
                }                                                                                                                                                         //Natural: VALUE 14
                else if (condition((pnd_Ws_Ndx.equals(14))))
                {
                    decideConditionsMet712++;
                    //*  RCC 06/13/02
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,23).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,23 )
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX )
                    //*  VARIABLE PA-SELE /* RCC
                    pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT
                }                                                                                                                                                         //Natural: VALUE 15,16,17,18,19,20,21,22
                else if (condition((pnd_Ws_Ndx.equals(15) || pnd_Ws_Ndx.equals(16) || pnd_Ws_Ndx.equals(17) || pnd_Ws_Ndx.equals(18) || pnd_Ws_Ndx.equals(19) 
                    || pnd_Ws_Ndx.equals(20) || pnd_Ws_Ndx.equals(21) || pnd_Ws_Ndx.equals(22))))
                {
                    decideConditionsMet712++;
                    //*  PA-SELECT
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,23).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,23 )
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX )
                    //*  FIXED SPIA                      /* RCC
                    pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #DVDND-AMT
                    //* *
                }                                                                                                                                                         //Natural: VALUE 24
                else if (condition((pnd_Ws_Ndx.equals(24))))
                {
                    decideConditionsMet712++;
                    //*  RCC
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,34).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,34 )
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX )
                    //*  FREE LOOK                       /* RCC
                    pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( #INDEX ) TO #DVDND-AMT
                }                                                                                                                                                         //Natural: VALUE 25
                else if (condition((pnd_Ws_Ndx.equals(25))))
                {
                    decideConditionsMet712++;
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,34).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,34 )
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX )
                    //*  VARI SPIA  /* RCC
                    pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #DVDND-AMT
                }                                                                                                                                                         //Natural: VALUE 26,27,28,29,30,31,32,33
                else if (condition((pnd_Ws_Ndx.equals(26) || pnd_Ws_Ndx.equals(27) || pnd_Ws_Ndx.equals(28) || pnd_Ws_Ndx.equals(29) || pnd_Ws_Ndx.equals(30) 
                    || pnd_Ws_Ndx.equals(31) || pnd_Ws_Ndx.equals(32) || pnd_Ws_Ndx.equals(33))))
                {
                    decideConditionsMet712++;
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,34).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index));     //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,34 )
                    pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Index)); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( #INDEX ) TO #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX )
                    pnd_Dvdnd_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Index));                                                         //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( #INDEX ) TO #DVDND-AMT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Contract_Amt.compute(new ComputeParameters(false, pnd_Contract_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1,                    //Natural: ASSIGN #CONTRACT-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:INV-ACCT-COUNT ) + 0
                ":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero()));
            pnd_Tax_Amt.compute(new ComputeParameters(false, pnd_Tax_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #TAX-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT ) + 0
            pnd_Tax_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));                  //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #TAX-AMT
            pnd_Tax_Amt.nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));                  //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT ) TO #TAX-AMT
            pnd_Ded_Amt.compute(new ComputeParameters(false, pnd_Ded_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue("*").add(getZero()));                 //Natural: ASSIGN #DED-AMT := WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( * ) + 0
            pnd_Net_Amt.compute(new ComputeParameters(false, pnd_Net_Amt), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).add(getZero())); //Natural: ASSIGN #NET-AMT := WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:INV-ACCT-COUNT ) + 0
            //*  CHECK
            short decideConditionsMet772 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet772++;
                pnd_Index.setValue(1);                                                                                                                                    //Natural: MOVE 1 TO #INDEX
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("C");                                                                                                               //Natural: MOVE 'C' TO #PYMNT-TYPE
                //*  EFT
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr());                                                                   //Natural: MOVE #CHECK-SORT-FIELDS.PYMNT-NBR TO #PYMNT-NUM
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet772++;
                pnd_Index.setValue(2);                                                                                                                                    //Natural: MOVE 2 TO #INDEX
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("E");                                                                                                               //Natural: MOVE 'E' TO #PYMNT-TYPE
                //*  GLOBAL PAY   /* TMM-2001-11 CHECK
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr());                                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR TO #PYMNT-NUM
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3))))
            {
                decideConditionsMet772++;
                //*  TMM-2001-11
                pnd_Index.setValue(6);                                                                                                                                    //Natural: MOVE 6 TO #INDEX
                pnd_Glb_Type.setValue("CHK");                                                                                                                             //Natural: MOVE 'CHK' TO #GLB-TYPE
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("G");                                                                                                               //Natural: MOVE 'G' TO #PYMNT-TYPE
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr());                                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR TO #PYMNT-NUM
                //*  GLOBAL PAY   /* TMM-2001-11 EFT-USD
                                                                                                                                                                          //Natural: PERFORM PRINT-GLOBAL-REPORT
                sub_Print_Global_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4))))
            {
                decideConditionsMet772++;
                //*  TMM-2001-11
                pnd_Index.setValue(7);                                                                                                                                    //Natural: MOVE 7 TO #INDEX
                pnd_Glb_Type.setValue("USD");                                                                                                                             //Natural: MOVE 'USD' TO #GLB-TYPE
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("G");                                                                                                               //Natural: MOVE 'G' TO #PYMNT-TYPE
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr());                                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR TO #PYMNT-NUM
                //*  ROLLOVER
                                                                                                                                                                          //Natural: PERFORM PRINT-GLOBAL-REPORT
                sub_Print_Global_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8))))
            {
                decideConditionsMet772++;
                pnd_Index.setValue(4);                                                                                                                                    //Natural: MOVE 4 TO #INDEX
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("I");                                                                                                               //Natural: MOVE 'I' TO #PYMNT-TYPE
                //*  GLOBAL PAY /*TMM-2001-11 EFT
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.reset();                                                                                                                      //Natural: RESET #PYMNT-NUM
            }                                                                                                                                                             //Natural: VALUE 9
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9))))
            {
                decideConditionsMet772++;
                pnd_Index.setValue(8);                                                                                                                                    //Natural: MOVE 8 TO #INDEX
                pnd_Glb_Type.setValue("EFT");                                                                                                                             //Natural: MOVE 'EFT' TO #GLB-TYPE
                pnd_Pymnt_Nbr_Pnd_Pymnt_Type.setValue("G");                                                                                                               //Natural: MOVE 'G' TO #PYMNT-TYPE
                pnd_Pymnt_Nbr_Pnd_Pymnt_Num.setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Scrty_Nbr());                                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.PYMNT-CHECK-SCRTY-NBR TO #PYMNT-NUM
                                                                                                                                                                          //Natural: PERFORM PRINT-GLOBAL-REPORT
                sub_Print_Global_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ANY VALUE
            if (condition(decideConditionsMet772 > 0))
            {
                totals_Pnd_T_Count.getValue(pnd_Index).nadd(1);                                                                                                           //Natural: ADD 1 TO #T-COUNT ( #INDEX )
                totals_Pnd_T_Cntrct.getValue(pnd_Index).nadd(pnd_Contract_Amt);                                                                                           //Natural: ADD #CONTRACT-AMT TO #T-CNTRCT ( #INDEX )
                totals_Pnd_T_Dvdnd.getValue(pnd_Index).nadd(pnd_Dvdnd_Amt);                                                                                               //Natural: ADD #DVDND-AMT TO #T-DVDND ( #INDEX )
                totals_Pnd_T_Tax.getValue(pnd_Index).nadd(pnd_Tax_Amt);                                                                                                   //Natural: ADD #TAX-AMT TO #T-TAX ( #INDEX )
                totals_Pnd_T_Ded.getValue(pnd_Index).nadd(pnd_Ded_Amt);                                                                                                   //Natural: ADD #DED-AMT TO #T-DED ( #INDEX )
                totals_Pnd_T_Net.getValue(pnd_Index).nadd(pnd_Net_Amt);                                                                                                   //Natural: ADD #NET-AMT TO #T-NET ( #INDEX )
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().display(1, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                               //Natural: DISPLAY ( 01 ) ( UC = - ) 'Contract/Number' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR 'Combine/Contract Num' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR 'Geo/Cde' WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE 'Contract/Amount' #CONTRACT-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'CREF Amount/or Dividend' #DVDND-AMT ( EM = Z,ZZZ,ZZ9.99- ) ' /Taxes' #TAX-AMT ( EM = ZZZ,ZZ9.99- ) ' /Deductns' #DED-AMT ( EM = ZZ,ZZ9.99- ) ' /Net Payment' #NET-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Combined/Check Amount' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Payment/Number' #PYMNT-NBR ' /Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ( AL = 13 )
            		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
            		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
            		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"Contract/Amount",
            		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
            		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
            		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
            		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
            		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
            		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Payment/Number",
            		pnd_Pymnt_Nbr," /Payee",
            		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), new AlphanumericLength (13));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *** PA SELECT DETAIL
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S")))                                                                           //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'S'
            {
                getReports().display(6, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                           //Natural: DISPLAY ( 06 ) ( UC = - ) 'Contract/Number' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR 'Combine/Contract Num' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR 'Geo/Cde' WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE 'Contract/Amount' #CONTRACT-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'CREF Amount/or Dividend' #DVDND-AMT ( EM = Z,ZZZ,ZZ9.99- ) ' /Taxes' #TAX-AMT ( EM = ZZZ,ZZ9.99- ) ' /Deductns' #DED-AMT ( EM = ZZ,ZZ9.99- ) ' /Net Payment' #NET-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Combined/Check Amount' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Payment/Number' #PYMNT-NBR ' /Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ( AL = 13 )
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"Contract/Amount",
                		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
                		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
                		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
                		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
                		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Payment/Number",
                		pnd_Pymnt_Nbr," /Payee",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), new AlphanumericLength (13));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //* *** /SPIA DETAIL
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M")))                                                                           //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'M'
            {
                getReports().display(7, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                           //Natural: DISPLAY ( 07 ) ( UC = - ) 'Contract/Number' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR 'Combine/Contract Num' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR 'Geo/Cde' WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE 'Contract/Amount' #CONTRACT-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'CREF Amount/or Dividend' #DVDND-AMT ( EM = Z,ZZZ,ZZ9.99- ) ' /Taxes' #TAX-AMT ( EM = ZZZ,ZZ9.99- ) ' /Deductns' #DED-AMT ( EM = ZZ,ZZ9.99- ) ' /Net Payment' #NET-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Combined/Check Amount' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Payment/Number' #PYMNT-NBR ' /Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ( AL = 13 )
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"Contract/Amount",
                		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
                		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
                		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
                		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
                		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Payment/Number",
                		pnd_Pymnt_Nbr," /Payee",
                		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), new AlphanumericLength (13));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue("*").equals(1)))                                                                        //Natural: IF WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( * ) = 01
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-ZERO-NET
                sub_Print_Zero_Net();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTALS
            sub_Print_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #WS-VALUAT 1 2
        for (pnd_Ws_Valuat.setValue(1); condition(pnd_Ws_Valuat.lessOrEqual(2)); pnd_Ws_Valuat.nadd(1))
        {
            if (condition(pnd_Ws_Valuat.equals(1)))                                                                                                                       //Natural: IF #WS-VALUAT = 1
            {
                pnd_Ws_Mon_Ann_Desc.setValue("ANNUAL");                                                                                                                   //Natural: MOVE 'ANNUAL' TO #WS-MON-ANN-DESC
                pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(35).setValue("Annual Total      ");                                                                          //Natural: MOVE 'Annual Total      ' TO #WS-DESC-TABLE ( 35 )
                pnd_Ws_Strt.setValue(1);                                                                                                                                  //Natural: MOVE 1 TO #WS-STRT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Mon_Ann_Desc.setValue("MONTHLY");                                                                                                                  //Natural: MOVE 'MONTHLY' TO #WS-MON-ANN-DESC
                pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(35).setValue("Monthly Total     ");                                                                          //Natural: MOVE 'Monthly Total     ' TO #WS-DESC-TABLE ( 35 )
                pnd_Ws_Strt.setValue(3);                                                                                                                                  //Natural: MOVE 3 TO #WS-STRT
            }                                                                                                                                                             //Natural: END-IF
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(60),pnd_Ws_Mon_Ann_Desc);                                                                //Natural: WRITE ( 02 ) //// 60T #WS-MON-ANN-DESC
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(2, NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE ( 02 ) //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR03:                                                                                                                                                        //Natural: FOR #WS-NDX #WS-STRT TO 34
            for (pnd_Ws_Ndx.setValue(pnd_Ws_Strt); condition(pnd_Ws_Ndx.lessOrEqual(34)); pnd_Ws_Ndx.nadd(1))
            {
                getReports().write(2, NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(pnd_Ws_Ndx),pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx),  //Natural: WRITE ( 02 ) / 48T #WS-DESC-TABLE ( #WS-NDX ) #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX ) ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Mon_Ann_Tot.compute(new ComputeParameters(false, pnd_Ws_Mon_Ann_Tot), pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,4).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat, //Natural: ASSIGN #WS-MON-ANN-TOT := #WS-AMOUNT-ACC ( #WS-VALUAT,4 ) + #WS-AMOUNT-ACC ( #WS-VALUAT,13 ) + #WS-AMOUNT-ACC ( #WS-VALUAT,23 ) + #WS-AMOUNT-ACC ( #WS-VALUAT,34 )
                13)).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,23)).add(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,34)));
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(35),pnd_Ws_Mon_Ann_Tot,                 //Natural: WRITE ( 02 ) //// 48T #WS-DESC-TABLE ( 35 ) #WS-MON-ANN-TOT ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Grand_Tot.nadd(pnd_Ws_Mon_Ann_Tot);                                                                                                                    //Natural: ADD #WS-MON-ANN-TOT TO #WS-GRAND-TOT
            pnd_Ws_Mon_Ann_Tot.reset();                                                                                                                                   //Natural: RESET #WS-MON-ANN-TOT
            //* * PA-SELECT
            getReports().newPage(new ReportSpecification(6));                                                                                                             //Natural: NEWPAGE ( 06 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(6, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(60),pnd_Ws_Mon_Ann_Desc);                                                                //Natural: WRITE ( 06 ) //// 60T #WS-MON-ANN-DESC
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(6, NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE ( 06 ) //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Strt.setValue(14);                                                                                                                                     //Natural: ASSIGN #WS-STRT := 14
            FOR04:                                                                                                                                                        //Natural: FOR #WS-NDX #WS-STRT TO 23
            for (pnd_Ws_Ndx.setValue(pnd_Ws_Strt); condition(pnd_Ws_Ndx.lessOrEqual(23)); pnd_Ws_Ndx.nadd(1))
            {
                getReports().write(6, NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(pnd_Ws_Ndx),pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx),  //Natural: WRITE ( 06 ) / 48T #WS-DESC-TABLE ( #WS-NDX ) #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX ) ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Mon_Ann_Tot.setValue(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,23));                                                                   //Natural: ASSIGN #WS-MON-ANN-TOT := #WS-AMOUNT-ACC ( #WS-VALUAT,23 )
            getReports().write(6, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(35),pnd_Ws_Mon_Ann_Tot,                 //Natural: WRITE ( 06 ) //// 48T #WS-DESC-TABLE ( 35 ) #WS-MON-ANN-TOT ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Grand_Tot_Pa.nadd(pnd_Ws_Mon_Ann_Tot);                                                                                                                 //Natural: ADD #WS-MON-ANN-TOT TO #WS-GRAND-TOT-PA
            pnd_Ws_Mon_Ann_Tot.reset();                                                                                                                                   //Natural: RESET #WS-MON-ANN-TOT
            //* * SPIA
            getReports().newPage(new ReportSpecification(7));                                                                                                             //Natural: NEWPAGE ( 07 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().write(7, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(60),pnd_Ws_Mon_Ann_Desc);                                                                //Natural: WRITE ( 07 ) //// 60T #WS-MON-ANN-DESC
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  09/24/02
            getReports().write(7, NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE ( 07 ) //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Strt.setValue(24);                                                                                                                                     //Natural: ASSIGN #WS-STRT := 24
            FOR05:                                                                                                                                                        //Natural: FOR #WS-NDX #WS-STRT TO 34
            for (pnd_Ws_Ndx.setValue(pnd_Ws_Strt); condition(pnd_Ws_Ndx.lessOrEqual(34)); pnd_Ws_Ndx.nadd(1))
            {
                getReports().write(7, NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(pnd_Ws_Ndx),pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,pnd_Ws_Ndx),  //Natural: WRITE ( 07 ) / 48T #WS-DESC-TABLE ( #WS-NDX ) #WS-AMOUNT-ACC ( #WS-VALUAT,#WS-NDX ) ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                    new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Mon_Ann_Tot.setValue(pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue(pnd_Ws_Valuat,34));                                                                   //Natural: ASSIGN #WS-MON-ANN-TOT := #WS-AMOUNT-ACC ( #WS-VALUAT,34 )
            getReports().write(7, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(35),pnd_Ws_Mon_Ann_Tot,                 //Natural: WRITE ( 07 ) //// 48T #WS-DESC-TABLE ( 35 ) #WS-MON-ANN-TOT ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Grand_Tot_Spia.nadd(pnd_Ws_Mon_Ann_Tot);                                                                                                               //Natural: ADD #WS-MON-ANN-TOT TO #WS-GRAND-TOT-SPIA
            pnd_Ws_Mon_Ann_Tot.reset();                                                                                                                                   //Natural: RESET #WS-MON-ANN-TOT
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  06/13/02
        getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(36),pnd_Ws_Grand_Tot, new                   //Natural: WRITE ( 02 ) //// 48T #WS-DESC-TABLE ( 36 ) #WS-GRAND-TOT ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        getReports().write(6, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(36),pnd_Ws_Grand_Tot_Pa,                    //Natural: WRITE ( 06 ) //// 48T #WS-DESC-TABLE ( 36 ) #WS-GRAND-TOT-PA ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        //*  06/13/02
        getReports().write(7, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(48),pnd_Ws_Descrition_Pnd_Ws_Desc_Table.getValue(36),pnd_Ws_Grand_Tot_Spia,                  //Natural: WRITE ( 07 ) //// 48T #WS-DESC-TABLE ( 36 ) #WS-GRAND-TOT-SPIA ( EM = Z,ZZZ,ZZZ,ZZ9.99- )
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99-"));
        if (Global.isEscape()) return;
        pnd_Ws_Grand_Tot.reset();                                                                                                                                         //Natural: RESET #WS-GRAND-TOT #WS-AMOUNT-ACC ( *,* ) #WS-MON-ANN-DESC
        pnd_Accumulators_Pnd_Ws_Amount_Acc.getValue("*","*").reset();
        pnd_Ws_Mon_Ann_Desc.reset();
        pnd_Ws_Grand_Tot_Pa.reset();                                                                                                                                      //Natural: RESET #WS-GRAND-TOT-PA #WS-GRAND-TOT-SPIA
        pnd_Ws_Grand_Tot_Spia.reset();
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-GLOBAL-REPORT
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-IRS-LIENS
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTALS
        //* ****************
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ZERO-NET
        //* ****************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-GLOBAL-PAY-TOTALS
    }
    private void sub_Print_Global_Report() throws Exception                                                                                                               //Natural: PRINT-GLOBAL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        //*  TMM-2001-11
        getReports().display(3, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                                   //Natural: DISPLAY ( 03 ) ( UC = - ) 'Contract/Number' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR 'Combine/Contract Num' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR 'Geo/Cde' WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE 'CONTRACT/AMOUNT' #CONTRACT-AMT ( EM = ZZZZ,ZZ9.99- ) 'CREF AMOUNT/OR DIVIDEND' #DVDND-AMT ( EM = ZZZZ,ZZ9.99- ) ' /TAXES' #TAX-AMT ( EM = ZZZ,ZZ9.99 ) ' /DEDUCTNS' #DED-AMT ( EM = ZZ,ZZ9.99 ) ' /Net Payment' #NET-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Combined/Check Amount' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'TYP' #GLB-TYPE 'GLOBAL/PAY NBR' #PYMNT-NBR ' /Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ( AL = 13 )
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"CONTRACT/AMOUNT",
        		pnd_Contract_Amt, new ReportEditMask ("ZZZZ,ZZ9.99-"),"CREF AMOUNT/OR DIVIDEND",
        		pnd_Dvdnd_Amt, new ReportEditMask ("ZZZZ,ZZ9.99-")," /TAXES",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99")," /DEDUCTNS",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"TYP",
        		pnd_Glb_Type,"GLOBAL/PAY NBR",
        		pnd_Pymnt_Nbr," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), new AlphanumericLength (13));
        if (Global.isEscape()) return;
        //*  PRINT-GLOBAL-REPORT
    }
    private void sub_Print_Irs_Liens() throws Exception                                                                                                                   //Natural: PRINT-IRS-LIENS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        getReports().display(4, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                                   //Natural: DISPLAY ( 04 ) ( UC = - ) 'Contract/Number' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR 'Combine/Contract Num' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR 'Contract/Amount' #CONTRACT-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'CREF Amount/or Dividend' #DVDND-AMT ( EM = Z,ZZZ,ZZ9.99- ) ' /Taxes' #TAX-AMT ( EM = ZZZ,ZZ9.99- ) ' /Deductns' #DED-AMT ( EM = ZZ,ZZ9.99- ) ' /Net Payment' #NET-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Combined/Check Amount' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Check/Number' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-NBR ( EM = 9 ( 7 ) ) ' /Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ( AL = 18 )
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Contract/Amount",
        		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Check/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr(), new ReportEditMask ("9999999")," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), new AlphanumericLength (18));
        if (Global.isEscape()) return;
        //*  PRINT-IRS-LIENS
    }
    private void sub_Print_Totals() throws Exception                                                                                                                      //Natural: PRINT-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        totals_Pnd_T_Count.getValue(3).compute(new ComputeParameters(false, totals_Pnd_T_Count.getValue(3)), totals_Pnd_T_Count.getValue(6,":",8).add(getZero()));        //Natural: ASSIGN #T-COUNT ( 3 ) := #T-COUNT ( 6:8 ) + 0
        totals_Pnd_T_Cntrct.getValue(3).compute(new ComputeParameters(false, totals_Pnd_T_Cntrct.getValue(3)), totals_Pnd_T_Cntrct.getValue(6,":",8).add(getZero()));     //Natural: ASSIGN #T-CNTRCT ( 3 ) := #T-CNTRCT ( 6:8 ) + 0
        totals_Pnd_T_Dvdnd.getValue(3).compute(new ComputeParameters(false, totals_Pnd_T_Dvdnd.getValue(3)), totals_Pnd_T_Dvdnd.getValue(6,":",8).add(getZero()));        //Natural: ASSIGN #T-DVDND ( 3 ) := #T-DVDND ( 6:8 ) + 0
        totals_Pnd_T_Tax.getValue(3).compute(new ComputeParameters(false, totals_Pnd_T_Tax.getValue(3)), totals_Pnd_T_Tax.getValue(6,":",8).add(getZero()));              //Natural: ASSIGN #T-TAX ( 3 ) := #T-TAX ( 6:8 ) + 0
        totals_Pnd_T_Ded.getValue(3).compute(new ComputeParameters(false, totals_Pnd_T_Ded.getValue(3)), totals_Pnd_T_Ded.getValue(6,":",8).add(getZero()));              //Natural: ASSIGN #T-DED ( 3 ) := #T-DED ( 6:8 ) + 0
        totals_Pnd_T_Net.getValue(3).compute(new ComputeParameters(false, totals_Pnd_T_Net.getValue(3)), totals_Pnd_T_Net.getValue(6,":",8).add(getZero()));              //Natural: ASSIGN #T-NET ( 3 ) := #T-NET ( 6:8 ) + 0
        totals_Pnd_T_Count.getValue(5).compute(new ComputeParameters(false, totals_Pnd_T_Count.getValue(5)), totals_Pnd_T_Count.getValue(1,":",4).add(getZero()));        //Natural: ASSIGN #T-COUNT ( 5 ) := #T-COUNT ( 1:4 ) + 0
        totals_Pnd_T_Cntrct.getValue(5).compute(new ComputeParameters(false, totals_Pnd_T_Cntrct.getValue(5)), totals_Pnd_T_Cntrct.getValue(1,":",4).add(getZero()));     //Natural: ASSIGN #T-CNTRCT ( 5 ) := #T-CNTRCT ( 1:4 ) + 0
        totals_Pnd_T_Dvdnd.getValue(5).compute(new ComputeParameters(false, totals_Pnd_T_Dvdnd.getValue(5)), totals_Pnd_T_Dvdnd.getValue(1,":",4).add(getZero()));        //Natural: ASSIGN #T-DVDND ( 5 ) := #T-DVDND ( 1:4 ) + 0
        totals_Pnd_T_Tax.getValue(5).compute(new ComputeParameters(false, totals_Pnd_T_Tax.getValue(5)), totals_Pnd_T_Tax.getValue(1,":",4).add(getZero()));              //Natural: ASSIGN #T-TAX ( 5 ) := #T-TAX ( 1:4 ) + 0
        totals_Pnd_T_Ded.getValue(5).compute(new ComputeParameters(false, totals_Pnd_T_Ded.getValue(5)), totals_Pnd_T_Ded.getValue(1,":",4).add(getZero()));              //Natural: ASSIGN #T-DED ( 5 ) := #T-DED ( 1:4 ) + 0
        totals_Pnd_T_Net.getValue(5).compute(new ComputeParameters(false, totals_Pnd_T_Net.getValue(5)), totals_Pnd_T_Net.getValue(1,":",4).add(getZero()));              //Natural: ASSIGN #T-NET ( 5 ) := #T-NET ( 1:4 ) + 0
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 01 )
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(30),"     Total            Total                            Total   ",NEWLINE,new                    //Natural: WRITE ( 01 ) /// 30T '     Total            Total                            Total   ' / 30T 'Contract Amount   CREF or Dvdnd     Total Taxes      Deductions' '    Net Payment    Total Count'
            TabSetting(30),"Contract Amount   CREF or Dvdnd     Total Taxes      Deductions","    Net Payment    Total Count");
        if (Global.isEscape()) return;
        FOR06:                                                                                                                                                            //Natural: FOR #INDEX 1 5
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(5)); pnd_Index.nadd(1))
        {
            //*  CHECK
            short decideConditionsMet954 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #INDEX;//Natural: VALUE 1
            if (condition((pnd_Index.equals(1))))
            {
                decideConditionsMet954++;
                //*  EFT
                pnd_Total_Lit.setValue("Total Check Payments    ");                                                                                                       //Natural: MOVE 'Total Check Payments    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Index.equals(2))))
            {
                decideConditionsMet954++;
                //*  GLOBAL PAY
                pnd_Total_Lit.setValue("Total EFT Payments      ");                                                                                                       //Natural: MOVE 'Total EFT Payments      ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Index.equals(3))))
            {
                decideConditionsMet954++;
                //*  ROLLOVER
                pnd_Total_Lit.setValue("Total Global Pay        ");                                                                                                       //Natural: MOVE 'Total Global Pay        ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_Index.equals(4))))
            {
                decideConditionsMet954++;
                pnd_Total_Lit.setValue("Total Internal Rollovers");                                                                                                       //Natural: MOVE 'Total Internal Rollovers' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_Index.equals(5))))
            {
                decideConditionsMet954++;
                pnd_Total_Lit.setValue("Grand Total Payments    ");                                                                                                       //Natural: MOVE 'Grand Total Payments    ' TO #TOTAL-LIT
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,pnd_Total_Lit,new ColumnSpacing(5),totals_Pnd_T_Cntrct.getValue(pnd_Index), new ReportEditMask                  //Natural: WRITE ( 01 ) /// #TOTAL-LIT 5X #T-CNTRCT ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 1X #T-DVDND ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 1X #T-TAX ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 1X #T-DED ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 1X #T-NET ( #INDEX ) ( EM = ZZZ,ZZZ,ZZ9.99- ) 5X #T-COUNT ( #INDEX ) ( EM = Z,ZZZ,ZZ9 )
                ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Dvdnd.getValue(pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Tax.getValue(pnd_Index), 
                new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Ded.getValue(pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new 
                ColumnSpacing(1),totals_Pnd_T_Net.getValue(pnd_Index), new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(5),totals_Pnd_T_Count.getValue(pnd_Index), 
                new ReportEditMask ("Z,ZZZ,ZZ9"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM WRITE-GLOBAL-PAY-TOTALS
        sub_Write_Global_Pay_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(pnd_Irs_Count.equals(getZero())))                                                                                                                   //Natural: IF #IRS-COUNT = 0
        {
            getReports().write(4, NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(45),"*************************************************************",NEWLINE,new  //Natural: WRITE ( 04 ) ///// / 45T '*************************************************************' / 45T '*************************************************************' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '***    No IRS Liens for this process                      ***' / 45T '***                                                       ***' / 45T '***                                                       ***' / 45T '*************************************************************' / 45T '*************************************************************'
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***    No IRS Liens for this process                      ***",NEWLINE,new 
                TabSetting(45),"***                                                       ***",NEWLINE,new TabSetting(45),"***                                                       ***",NEWLINE,new 
                TabSetting(45),"*************************************************************",NEWLINE,new TabSetting(45),"*************************************************************");
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRINT-TOTALS
    }
    private void sub_Print_Zero_Net() throws Exception                                                                                                                    //Natural: PRINT-ZERO-NET
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        getReports().display(5, new ReportMatrixColumnUnderline("-"),"Contract/Number",                                                                                   //Natural: DISPLAY ( 05 ) ( UC = - ) 'Contract/Number' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR 'Combine/Contract Num' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR 'Contract/Amount' #CONTRACT-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'CREF Amount/or Dividend' #DVDND-AMT ( EM = Z,ZZZ,ZZ9.99- ) ' /Taxes' #TAX-AMT ( EM = ZZZ,ZZ9.99- ) ' /Deductns' #DED-AMT ( EM = ZZ,ZZ9.99- ) ' /Net Payment' #NET-AMT ( EM = Z,ZZZ,ZZ9.99- ) 'Combined/Check Amount' WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99- ) ' /Payee' WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) ( AL = 26 )
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Contract/Amount",
        		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1), new AlphanumericLength (26));
        if (Global.isEscape()) return;
        //*  PRINT-ZERO-NET
    }
    private void sub_Write_Global_Pay_Totals() throws Exception                                                                                                           //Natural: WRITE-GLOBAL-PAY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************
        getReports().write(3, NEWLINE,NEWLINE,NEWLINE,"TOTAL CHECK GLOBAL PAY   ",new ColumnSpacing(5),totals_Pnd_T_Cntrct.getValue(6), new ReportEditMask                //Natural: WRITE ( 03 ) /// 'TOTAL CHECK GLOBAL PAY   ' 5X #T-CNTRCT ( 6 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-DVDND ( 6 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-TAX ( 6 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-DED ( 6 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-NET ( 6 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X 'COUNT:' #T-COUNT ( 6 ) ( EM = ZZZ,ZZ9 )
            ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Dvdnd.getValue(6), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Tax.getValue(6), 
            new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Ded.getValue(6), new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Net.getValue(6), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),"COUNT:",totals_Pnd_T_Count.getValue(6), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,"  TL. EFT USD GLOBAL PAY ",new ColumnSpacing(5),totals_Pnd_T_Cntrct.getValue(7), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new          //Natural: WRITE ( 03 ) / '  TL. EFT USD GLOBAL PAY ' 5X #T-CNTRCT ( 7 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-DVDND ( 7 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-TAX ( 7 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-DED ( 7 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-NET ( 7 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X 'COUNT:' #T-COUNT ( 7 ) ( EM = ZZZ,ZZ9 )
            ColumnSpacing(1),totals_Pnd_T_Dvdnd.getValue(7), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Tax.getValue(7), new 
            ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Ded.getValue(7), new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Net.getValue(7), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),"COUNT:",totals_Pnd_T_Count.getValue(7), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,"  TL. EFT CUR GLOBAL PAY ",new ColumnSpacing(5),totals_Pnd_T_Cntrct.getValue(8), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new          //Natural: WRITE ( 03 ) / '  TL. EFT CUR GLOBAL PAY ' 5X #T-CNTRCT ( 8 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-DVDND ( 8 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-TAX ( 8 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-DED ( 8 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-NET ( 8 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X 'COUNT:' #T-COUNT ( 8 ) ( EM = ZZZ,ZZ9 )
            ColumnSpacing(1),totals_Pnd_T_Dvdnd.getValue(8), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Tax.getValue(8), new 
            ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Ded.getValue(8), new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Net.getValue(8), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),"COUNT:",totals_Pnd_T_Count.getValue(8), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        totals_Pnd_T_Cntrct.getValue(9).compute(new ComputeParameters(false, totals_Pnd_T_Cntrct.getValue(9)), totals_Pnd_T_Cntrct.getValue(7).add(totals_Pnd_T_Cntrct.getValue(8))); //Natural: COMPUTE #T-CNTRCT ( 9 ) = #T-CNTRCT ( 7 ) + #T-CNTRCT ( 8 )
        totals_Pnd_T_Dvdnd.getValue(9).compute(new ComputeParameters(false, totals_Pnd_T_Dvdnd.getValue(9)), totals_Pnd_T_Dvdnd.getValue(7).add(totals_Pnd_T_Dvdnd.getValue(8))); //Natural: COMPUTE #T-DVDND ( 9 ) = #T-DVDND ( 7 ) + #T-DVDND ( 8 )
        totals_Pnd_T_Tax.getValue(9).compute(new ComputeParameters(false, totals_Pnd_T_Tax.getValue(9)), totals_Pnd_T_Tax.getValue(7).add(totals_Pnd_T_Tax.getValue(8))); //Natural: COMPUTE #T-TAX ( 9 ) = #T-TAX ( 7 ) + #T-TAX ( 8 )
        totals_Pnd_T_Ded.getValue(9).compute(new ComputeParameters(false, totals_Pnd_T_Ded.getValue(9)), totals_Pnd_T_Ded.getValue(7).add(totals_Pnd_T_Ded.getValue(8))); //Natural: COMPUTE #T-DED ( 9 ) = #T-DED ( 7 ) + #T-DED ( 8 )
        totals_Pnd_T_Net.getValue(9).compute(new ComputeParameters(false, totals_Pnd_T_Net.getValue(9)), totals_Pnd_T_Net.getValue(7).add(totals_Pnd_T_Net.getValue(8))); //Natural: COMPUTE #T-NET ( 9 ) = #T-NET ( 7 ) + #T-NET ( 8 )
        totals_Pnd_T_Count.getValue(9).compute(new ComputeParameters(false, totals_Pnd_T_Count.getValue(9)), totals_Pnd_T_Count.getValue(7).add(totals_Pnd_T_Count.getValue(7))); //Natural: COMPUTE #T-COUNT ( 9 ) = #T-COUNT ( 7 ) + #T-COUNT ( 7 )
        getReports().write(3, NEWLINE,"TOTAL EFT                ",new ColumnSpacing(5),totals_Pnd_T_Cntrct.getValue(9), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new          //Natural: WRITE ( 03 ) / 'TOTAL EFT                ' 5X #T-CNTRCT ( 9 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-DVDND ( 9 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-TAX ( 9 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-DED ( 9 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-NET ( 9 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X 'COUNT:' #T-COUNT ( 9 ) ( EM = ZZZ,ZZ9 )
            ColumnSpacing(1),totals_Pnd_T_Dvdnd.getValue(9), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Tax.getValue(9), new 
            ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Ded.getValue(9), new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Net.getValue(9), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),"COUNT:",totals_Pnd_T_Count.getValue(9), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(3, NEWLINE,"TOTAL GLOBAL PAY         ",new ColumnSpacing(5),totals_Pnd_T_Cntrct.getValue(3), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new          //Natural: WRITE ( 03 ) / 'TOTAL GLOBAL PAY         ' 5X #T-CNTRCT ( 3 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-DVDND ( 3 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X #T-TAX ( 3 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-DED ( 3 ) ( EM = ZZZ,ZZ9.99- ) 1X #T-NET ( 3 ) ( EM = Z,ZZZ,ZZ9.99- ) 1X 'COUNT:' #T-COUNT ( 3 ) ( EM = ZZZ,ZZ9 )
            ColumnSpacing(1),totals_Pnd_T_Dvdnd.getValue(3), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Tax.getValue(3), new 
            ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Ded.getValue(3), new ReportEditMask ("ZZZ,ZZ9.99-"),new ColumnSpacing(1),totals_Pnd_T_Net.getValue(3), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new ColumnSpacing(1),"COUNT:",totals_Pnd_T_Count.getValue(3), new ReportEditMask ("ZZZ,ZZ9"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************", //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  Last record read:' / '***         Combine #:' WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR / '***              PPCN:' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR / '**************************************************************' / '**************************************************************'
            NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  Last record read:",NEWLINE,"***         Combine #:",
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),NEWLINE,"***              PPCN:",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),NEWLINE,
            "**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=135 PS=55");
        Global.format(2, "LS=132 PS=55");
        Global.format(3, "LS=135 PS=55");
        Global.format(4, "LS=132 PS=55");
        Global.format(5, "LS=132 PS=55");
        Global.format(6, "LS=135 PS=55");
        Global.format(7, "LS=135 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"- 1",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(45),"PAYMENT REGISTER FOR",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"- 2",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(45),"TOTAL PAYMENTS BY FUND FOR",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"- 3",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(45),"GLOBAL PAY REGISTER FOR",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);
        getReports().write(4, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"- 4",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(4), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(51),"IRS LIENS FOR",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);
        getReports().write(5, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"- 5",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(5), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"ANNUITY PAYMENTS",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(49),"NET ZERO PAYMENTS",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);
        getReports().write(6, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"- 6",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(6), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"   PA-Select    ",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(45),"PAYMENT REGISTER FOR",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);
        getReports().write(7, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),"-",Global.getPROGRAM(),"- 7",new TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new 
            TabSetting(120),"PAGE:",getReports().getPageNumberDbs(7), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),new 
            TabSetting(56),"      SPIA      ",NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(45),"PAYMENT REGISTER FOR",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(), 
            new ReportEditMask ("LLLLLLLLL', 'YYYY"),NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"Contract/Amount",
        		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Payment/Number",
        		pnd_Pymnt_Nbr," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme(), new AlphanumericLength (13));
        getReports().setDisplayColumns(6, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"Contract/Amount",
        		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Payment/Number",
        		pnd_Pymnt_Nbr," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme(), new AlphanumericLength (13));
        getReports().setDisplayColumns(7, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"Contract/Amount",
        		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Payment/Number",
        		pnd_Pymnt_Nbr," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme(), new AlphanumericLength (13));
        getReports().setDisplayColumns(3, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Geo/Cde",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"CONTRACT/AMOUNT",
        		pnd_Contract_Amt, new ReportEditMask ("ZZZZ,ZZ9.99-"),"CREF AMOUNT/OR DIVIDEND",
        		pnd_Dvdnd_Amt, new ReportEditMask ("ZZZZ,ZZ9.99-")," /TAXES",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99")," /DEDUCTNS",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"TYP",
        		pnd_Glb_Type,"GLOBAL/PAY NBR",
        		pnd_Pymnt_Nbr," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme(), new AlphanumericLength (13));
        getReports().setDisplayColumns(4, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Contract/Amount",
        		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Check/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Nbr(), new ReportEditMask ("9999999")," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme(), new AlphanumericLength (18));
        getReports().setDisplayColumns(5, new ReportMatrixColumnUnderline("-"),"Contract/Number",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(),"Combine/Contract Num",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),"Contract/Amount",
        		pnd_Contract_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"CREF Amount/or Dividend",
        		pnd_Dvdnd_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Taxes",
        		pnd_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99-")," /Deductns",
        		pnd_Ded_Amt, new ReportEditMask ("ZZ,ZZ9.99-")," /Net Payment",
        		pnd_Net_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),"Combined/Check Amount",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99-")," /Payee",
        		pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme(), new AlphanumericLength (26));
    }
}
