/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:24 PM
**        * FROM NATURAL PROGRAM : Fcpp560a
************************************************************
**        * FILE NAME            : Fcpp560a.java
**        * CLASS NAME           : Fcpp560a
**        * INSTANCE NAME        : Fcpp560a
************************************************************
***********************************************************************
* PROGRAM      : FCPP560A
* SYSTEM       : CPS
* FUNCTION     : GENERATE TREASURY CHECK REGISTER
*              : - SUMMARY BY CHECK DATE
* AUTHOR       : MARIA ACLAN
* DATE WRITTEN : APRIL 18, 2001
* 4/2017       : JJG - PIN EXPANSION RESTOW
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp560a extends BLNatBase
{
    // Data Areas
    private LdaFcpl190a ldaFcpl190a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Work_Variables;
    private DbsField pnd_Work_Variables_Pnd_Ws_Start;
    private DbsField pnd_Work_Variables_Pnd_Ws_Row;
    private DbsField pnd_Work_Variables_Pnd_Ws_Col;
    private DbsField pnd_Work_Variables_Pnd_Ws_Orgn_Cde;
    private DbsField pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table;
    private DbsField pnd_Work_Variables_Pnd_Ws_Check_Dte;
    private DbsField pnd_Work_Variables_Pnd_Ws_Work_Date;

    private DbsGroup pnd_Work_Variables_Pnd_Ws_Summary;

    private DbsGroup pnd_Work_Variables_Pnd_Ws_Vertical_Array;
    private DbsField pnd_Work_Variables_Pnd_Ws_Record_Count;
    private DbsField pnd_Work_Variables_Pnd_Ws_Check_Amt;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Pymnt_Check_DteOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Work_Variables = localVariables.newGroupInRecord("pnd_Work_Variables", "#WORK-VARIABLES");
        pnd_Work_Variables_Pnd_Ws_Start = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Start", "#WS-START", FieldType.NUMERIC, 1);
        pnd_Work_Variables_Pnd_Ws_Row = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Row", "#WS-ROW", FieldType.NUMERIC, 2);
        pnd_Work_Variables_Pnd_Ws_Col = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Col", "#WS-COL", FieldType.NUMERIC, 2);
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Orgn_Cde", "#WS-ORGN-CDE", FieldType.STRING, 
            3);
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table = pnd_Work_Variables.newFieldArrayInGroup("pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table", "#WS-ORGN-CDE-TABLE", 
            FieldType.STRING, 3, new DbsArrayController(1, 12));
        pnd_Work_Variables_Pnd_Ws_Check_Dte = pnd_Work_Variables.newFieldArrayInGroup("pnd_Work_Variables_Pnd_Ws_Check_Dte", "#WS-CHECK-DTE", FieldType.DATE, 
            new DbsArrayController(1, 3));
        pnd_Work_Variables_Pnd_Ws_Work_Date = pnd_Work_Variables.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Work_Date", "#WS-WORK-DATE", FieldType.STRING, 
            15);

        pnd_Work_Variables_Pnd_Ws_Summary = pnd_Work_Variables.newGroupArrayInGroup("pnd_Work_Variables_Pnd_Ws_Summary", "#WS-SUMMARY", new DbsArrayController(1, 
            12));

        pnd_Work_Variables_Pnd_Ws_Vertical_Array = pnd_Work_Variables_Pnd_Ws_Summary.newGroupArrayInGroup("pnd_Work_Variables_Pnd_Ws_Vertical_Array", 
            "#WS-VERTICAL-ARRAY", new DbsArrayController(1, 4));
        pnd_Work_Variables_Pnd_Ws_Record_Count = pnd_Work_Variables_Pnd_Ws_Vertical_Array.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Record_Count", "#WS-RECORD-COUNT", 
            FieldType.NUMERIC, 5);
        pnd_Work_Variables_Pnd_Ws_Check_Amt = pnd_Work_Variables_Pnd_Ws_Vertical_Array.newFieldInGroup("pnd_Work_Variables_Pnd_Ws_Check_Amt", "#WS-CHECK-AMT", 
            FieldType.NUMERIC, 12, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Pymnt_Check_DteOld = internalLoopRecord.newFieldInRecord("Sort01_Pymnt_Check_Dte_OLD", "Pymnt_Check_Dte_OLD", FieldType.DATE);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_Work_Variables_Pnd_Ws_Start.setInitialValue(0);
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(1).setInitialValue("SS");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(2).setInitialValue("MDO");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(3).setInitialValue("XX");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(4).setInitialValue("DC");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(5).setInitialValue("IA");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(6).setInitialValue("AP");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(7).setInitialValue("XX");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(8).setInitialValue("NZ");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(9).setInitialValue("AL");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(10).setInitialValue("EW");
        pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue(11).setInitialValue("OT");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp560a() throws Exception
    {
        super("Fcpp560a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 132 PS = 55
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT 001T *INIT-USER '-' *PROGRAM 056T 'CONSOLIDATED PAYMENT SYSTEM' 117T 'PAGE ' *PAGE-NUMBER ( 1 ) ( EM = ZZ9 ) / 001T *DATX '-' *TIMX ( EM = HH':'II' 'AP ) 056T '  TREASURY CHECK REGISTER' / 056T '   SUMMARY BY CHECK DATE' / 064T *DATX ( EM = LLL' 'DD', 'YYYY )
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #RPT-EXT
        while (condition(getWorkFiles().read(1, ldaFcpl190a.getPnd_Rpt_Ext())))
        {
            getSort().writeSortInData(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(), ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde(), ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Instmt_Nbr(),  //Natural: END-ALL
                ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind(), ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt(), ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde());
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getSort().sortData(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(), ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());                                                   //Natural: SORT BY PYMNT-CHECK-DTE CNTRCT-ORGN-CDE USING PYMNT-INSTMT-NBR PYMNT-PAY-TYPE-REQ-IND PYMNT-CHECK-AMT CNTRCT-TYPE-CDE
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(), ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde(), ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Instmt_Nbr(), 
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind(), ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt(), ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde())))
        {
            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            if (condition(!(((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Instmt_Nbr().equals(1) || ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Instmt_Nbr().equals(8)) && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1))))) //Natural: ACCEPT IF ( PYMNT-INSTMT-NBR EQ 01 OR = 8 ) AND PYMNT-PAY-TYPE-REQ-IND EQ 1
            {
                continue;
            }
            //*                                                                                                                                                           //Natural: AT BREAK OF PYMNT-CHECK-DTE
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("NZ") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AL")  //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS' OR = 'NZ' OR = 'AL' OR = 'DC' OR = 'IA' OR = 'AP' OR = 'EW'
                || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AP") 
                || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("EW")))
            {
                ignore();
                //*  DS, MS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().setValue("OT");                                                                                              //Natural: ASSIGN #RPT-EXT.CNTRCT-ORGN-CDE := 'OT'
            }                                                                                                                                                             //Natural: END-IF
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS") && (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30") ||                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS' AND #RPT-EXT.CNTRCT-TYPE-CDE = '30' OR = '31'
                ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31")))))
            {
                pnd_Work_Variables_Pnd_Ws_Orgn_Cde.setValue("MDO");                                                                                                       //Natural: ASSIGN #WS-ORGN-CDE := 'MDO'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Work_Variables_Pnd_Ws_Orgn_Cde.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());                                                                //Natural: ASSIGN #WS-ORGN-CDE := #RPT-EXT.CNTRCT-ORGN-CDE
            }                                                                                                                                                             //Natural: END-IF
            DbsUtil.examine(new ExamineSource(pnd_Work_Variables_Pnd_Ws_Orgn_Cde_Table.getValue("*"),true), new ExamineSearch(pnd_Work_Variables_Pnd_Ws_Orgn_Cde),        //Natural: EXAMINE FULL VALUE OF #WS-ORGN-CDE-TABLE ( * ) FOR #WS-ORGN-CDE GIVING INDEX #WS-ROW
                new ExamineGivingIndex(pnd_Work_Variables_Pnd_Ws_Row));
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(pnd_Work_Variables_Pnd_Ws_Row,1).nadd(1);                                                                     //Natural: ADD 1 TO #WS-RECORD-COUNT ( #WS-ROW,1 )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(pnd_Work_Variables_Pnd_Ws_Row,1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                             //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( #WS-ROW,1 )
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS'
            {
                pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,1).nadd(1);                                                                                             //Natural: ADD 1 TO #WS-RECORD-COUNT ( 3,1 )
                pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                     //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 3,1 )
                pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,2).nadd(1);                                                                                             //Natural: ADD 1 TO #WS-RECORD-COUNT ( 3,2 )
                pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,2).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                     //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 3,2 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("AP"))) //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' OR = 'IA' OR = 'AP'
            {
                pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,1).nadd(1);                                                                                             //Natural: ADD 1 TO #WS-RECORD-COUNT ( 7,1 )
                pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                     //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 7,1 )
                pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,2).nadd(1);                                                                                             //Natural: ADD 1 TO #WS-RECORD-COUNT ( 7,2 )
                pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,2).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                     //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 7,2 )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(pnd_Work_Variables_Pnd_Ws_Row,2).nadd(1);                                                                     //Natural: ADD 1 TO #WS-RECORD-COUNT ( #WS-ROW,2 )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(pnd_Work_Variables_Pnd_Ws_Row,2).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                             //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( #WS-ROW,2 )
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,1).nadd(1);                                                                                                //Natural: ADD 1 TO #WS-RECORD-COUNT ( 12,1 )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                        //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 12,1 )
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,2).nadd(1);                                                                                                //Natural: ADD 1 TO #WS-RECORD-COUNT ( 12,2 )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,2).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                        //Natural: ADD #RPT-EXT.PYMNT-CHECK-AMT TO #WS-CHECK-AMT ( 12,2 )
            sort01Pymnt_Check_DteOld.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                                              //Natural: END-SORT
        }
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",1).reset();                                                                                                   //Natural: RESET #WS-RECORD-COUNT ( *,1 ) #WS-CHECK-AMT ( *,1 )
        pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",1).reset();
        pnd_Work_Variables_Pnd_Ws_Work_Date.setValue("T O T A L S ");                                                                                                     //Natural: MOVE 'T O T A L S ' TO #WS-WORK-DATE
        pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",1).setValue(pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",2));                                          //Natural: MOVE #WS-RECORD-COUNT ( *,2 ) TO #WS-RECORD-COUNT ( *,1 )
        pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",1).setValue(pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",2));                                                //Natural: MOVE #WS-CHECK-AMT ( *,2 ) TO #WS-CHECK-AMT ( *,1 )
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
        sub_Write_Summary_Report();
        if (condition(Global.isEscape())) {return;}
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-SUMMARY-REPORT
        //* * -------------------------------------------------------------------
    }
    //* * -------------------------------------------------------------------
    private void sub_Write_Summary_Report() throws Exception                                                                                                              //Natural: WRITE-SUMMARY-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(59),pnd_Work_Variables_Pnd_Ws_Work_Date,NEWLINE,new TabSetting(53),"-",new RepeatItem(23));          //Natural: WRITE ( 1 ) /// 059T #WS-WORK-DATE / 053T '-' ( 23 )
        if (Global.isEscape()) return;
        getReports().write(1, NEWLINE,NEWLINE,new TabSetting(43),"SS",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(1,1), new ReportEditMask         //Natural: WRITE ( 1 ) // 043T 'SS' 052T #WS-RECORD-COUNT ( 1,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 1,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 043T 'MDO' 052T #WS-RECORD-COUNT ( 2,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 2,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 040T 'Single Sum' 052T #WS-RECORD-COUNT ( 3,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 3,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 043T 'DC' 052T #WS-RECORD-COUNT ( 4,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 4,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 043T 'IA' 052T #WS-RECORD-COUNT ( 5,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 5,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 043T 'AP' 052T #WS-RECORD-COUNT ( 6,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 6,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 040T 'Annuity' 052T #WS-RECORD-COUNT ( 7,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 7,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 040T 'ADAM  NZ' 052T #WS-RECORD-COUNT ( 8,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 8,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 040T 'Loan  AL' 052T #WS-RECORD-COUNT ( 9,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 9,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 040T 'E Warrant' 052T #WS-RECORD-COUNT ( 10,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 10,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 040T 'Others  ' 052T #WS-RECORD-COUNT ( 11,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 11,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) // 053T '-' ( 23 ) / 040T 'TOTALS  ' 052T #WS-RECORD-COUNT ( 12,1 ) ( EM = ZZ,ZZ9 ) 060T #WS-CHECK-AMT ( 12,1 ) ( EM = Z,ZZZ,ZZZ,ZZZ.99 ) / 053T '-' ( 23 )
            ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(1,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(43),"MDO",new 
            TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(2,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(2,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(40),"Single Sum",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(3,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(3,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new 
            TabSetting(43),"DC",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(4,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(4,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(43),"IA",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(5,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(5,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(43),"AP",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(6,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(6,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new TabSetting(40),"Annuity",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(7,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(7,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new 
            TabSetting(40),"ADAM  NZ",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(8,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(8,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new TabSetting(40),"Loan  AL",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(9,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(9,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new 
            TabSetting(40),"E Warrant",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(10,1), new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(10,1), 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new TabSetting(40),"Others  ",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(11,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(11,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,NEWLINE,new 
            TabSetting(53),"-",new RepeatItem(23),NEWLINE,new TabSetting(40),"TOTALS  ",new TabSetting(52),pnd_Work_Variables_Pnd_Ws_Record_Count.getValue(12,1), 
            new ReportEditMask ("ZZ,ZZ9"),new TabSetting(60),pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue(12,1), new ReportEditMask ("Z,ZZZ,ZZZ,ZZZ.99"),NEWLINE,new 
            TabSetting(53),"-",new RepeatItem(23));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak = ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte().isBreak(endOfData);
        if (condition(ldaFcpl190a_getPnd_Rpt_Ext_Pymnt_Check_DteIsBreak))
        {
            pnd_Work_Variables_Pnd_Ws_Work_Date.setValueEdited(sort01Pymnt_Check_DteOld,new ReportEditMask("MM/DD/YYYY"));                                                //Natural: MOVE EDITED OLD ( PYMNT-CHECK-DTE ) ( EM = MM/DD/YYYY ) TO #WS-WORK-DATE
                                                                                                                                                                          //Natural: PERFORM WRITE-SUMMARY-REPORT
            sub_Write_Summary_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Work_Variables_Pnd_Ws_Record_Count.getValue("*",1).reset();                                                                                               //Natural: RESET #WS-RECORD-COUNT ( *,1 ) #WS-CHECK-AMT ( *,1 )
            pnd_Work_Variables_Pnd_Ws_Check_Amt.getValue("*",1).reset();
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(1),Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(117),"PAGE ",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ9"),NEWLINE,new 
            TabSetting(1),Global.getDATX(),"-",Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),new TabSetting(56),"  TREASURY CHECK REGISTER",NEWLINE,new 
            TabSetting(56),"   SUMMARY BY CHECK DATE",NEWLINE,new TabSetting(64),Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"));
    }
}
