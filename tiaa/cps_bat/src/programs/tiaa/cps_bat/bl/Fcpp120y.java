/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:03 PM
**        * FROM NATURAL PROGRAM : Fcpp120y
************************************************************
**        * FILE NAME            : Fcpp120y.java
**        * CLASS NAME           : Fcpp120y
**        * INSTANCE NAME        : Fcpp120y
************************************************************
***********************************************************************
* PROGRAM  : FCPP120Y - NYSID
*          : FCPP120Y - TAMMY KENNEDY
*
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS LEDGER EXTRACT - SAME FORMAT AS IA-LEDGER EXTRACT
*          : MODIFIED TO EXTRACT X-NUMBER OF YEARS OF LEDGER ENTRIES
*          : REVERSE ENGENEERED TO GET RESIDENCY
* FUNCTION : THIS PROGRAM WILL READ FCP-CONS-LEDGER FILE
* CHANGES  : 11/08/2010 R.SACHARNY - WRITE OUT DAILY RECON LEDGER (RS0)
*          : 12/08/2010 B.HOLLOWAY - ADD NEW CODES FOR PENDING ACTIONS
*          : 01/11/2011 R.SACHARNY - ACCEPT ALL FOR RECON LEDGER (RS1)
*          : 01/31/2011 R.SACHARNY - PASS SETTLEMENT DATE        (RS2)
*          : 02/04/2011 R.SACHARNY - PASS REDRAW INDICATOR       (RS3)
*          : 03/02/2011 R.SACHARNY - ACCEPT PAY AND ARCH PAY     (RS4)
*          : 04/18/2011 R.SACHARNY - EXTRACT NZ OR ALL OTHER TRAN(RS5)
*          :            SACTIONS DEPENDING ON THE INPUT PARAMETER
*          : 06/08/2011 R.SACHARNY - IDENTIFY FOREIGN PAYMENTS   (RS6)
*          :            AND WRITE PAYMENTS WITH BLANK STATE TO REPORT
*          : 12/08/2011 R.SACHARNY - ADD IA-ORGN-CDE/CNTRCT-OPTION-CDE
*          :            TO DI-HUB AND RECONNET FILE (RS7)
*
* 20140823 F.ENDAYA  PH RECEIVABLE. ADD TWO NEW CODES AP AND AS AS VALID
* FE201408           CSR CODE.
* 04/2017  : JJG PIN EXPANSION
* 09/17/2020 CTS     CPS SUNSET (CHG857760) TAG: CTS-0917
**********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp120y extends BLNatBase
{
    // Data Areas
    private PdaFcpapmnt pdaFcpapmnt;
    private LdaFcplpmna ldaFcplpmna;
    private LdaFcplldgr ldaFcplldgr;
    private LdaFcpl801d ldaFcpl801d;
    private LdaFcplpmnt ldaFcplpmnt;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pay;
    private DbsField arcpay;
    private DbsField pnd_Process;
    private DbsField pnd_Count;
    private DbsField pnd_Max;
    private DbsField pnd_Input_Date;
    private DbsField pnd_Ledger_Superde_Start;

    private DbsGroup pnd_Ledger_Superde_Start__R_Field_1;
    private DbsField pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Cntrct_Rcrd_Typ;
    private DbsField pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Filler;
    private DbsField pnd_Temp_Date_Alpha;

    private DbsGroup pnd_Temp_Date_Alpha__R_Field_2;
    private DbsField pnd_Temp_Date_Alpha_Pnd_Temp_Date;
    private DbsField pnd_C_Inv_Acct;
    private DbsField pnd_Ledger_Records_Read;
    private DbsField pnd_Ledger_Read;
    private DbsField pnd_Ledger_Written;
    private DbsField pnd_Paymnt_Written;
    private DbsField pnd_I;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_S__R_Field_3;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;
    private DbsField pnd_Trans_Ind_Parm;

    private DbsGroup pnd_Trans_Ind_Parm__R_Field_4;
    private DbsField pnd_Trans_Ind_Parm_Pnd_Trans_Ind;
    private DbsField pnd_Trans_Ind_Parm_Pnd_Trans_Fil;
    private DbsField pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date;
    private DbsField pnd_Text;
    private DbsField pnd_All;
    private DbsField pnd_Only_Nz;
    private DbsField pnd_System_Date_Ccyymmdd;

    private DbsGroup pnd_System_Date_Ccyymmdd__R_Field_5;
    private DbsField pnd_System_Date_Ccyymmdd_Pnd_System_Date_Ccyymmdd_A;
    private DbsField pnd_Nostate_Cnt;
    private DbsField pnd_Nostate;
    private DbsField pnd_Print_Chk_Dte;
    private DbsField pnd_Print_Stl_Dte;
    private DbsField pnd_Print_Act_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpapmnt = new PdaFcpapmnt(localVariables);
        ldaFcplpmna = new LdaFcplpmna();
        registerRecord(ldaFcplpmna);
        registerRecord(ldaFcplpmna.getVw_fcp_Arch_Pymnt());
        ldaFcplldgr = new LdaFcplldgr();
        registerRecord(ldaFcplldgr);
        registerRecord(ldaFcplldgr.getVw_fcp_Cons_Ledger());
        ldaFcpl801d = new LdaFcpl801d();
        registerRecord(ldaFcpl801d);
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());

        // Local Variables
        pay = localVariables.newFieldInRecord("pay", "PAY", FieldType.BOOLEAN, 1);
        arcpay = localVariables.newFieldInRecord("arcpay", "ARCPAY", FieldType.BOOLEAN, 1);
        pnd_Process = localVariables.newFieldInRecord("pnd_Process", "#PROCESS", FieldType.BOOLEAN, 1);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 10);
        pnd_Max = localVariables.newFieldInRecord("pnd_Max", "#MAX", FieldType.NUMERIC, 10);
        pnd_Input_Date = localVariables.newFieldInRecord("pnd_Input_Date", "#INPUT-DATE", FieldType.DATE);
        pnd_Ledger_Superde_Start = localVariables.newFieldInRecord("pnd_Ledger_Superde_Start", "#LEDGER-SUPERDE-START", FieldType.STRING, 6);

        pnd_Ledger_Superde_Start__R_Field_1 = localVariables.newGroupInRecord("pnd_Ledger_Superde_Start__R_Field_1", "REDEFINE", pnd_Ledger_Superde_Start);
        pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Cntrct_Rcrd_Typ = pnd_Ledger_Superde_Start__R_Field_1.newFieldInGroup("pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Cntrct_Rcrd_Typ", 
            "#S-LDGR-CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte = pnd_Ledger_Superde_Start__R_Field_1.newFieldInGroup("pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte", 
            "#S-LDGR-PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Filler = pnd_Ledger_Superde_Start__R_Field_1.newFieldInGroup("pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Filler", 
            "#S-LDGR-FILLER", FieldType.STRING, 1);
        pnd_Temp_Date_Alpha = localVariables.newFieldInRecord("pnd_Temp_Date_Alpha", "#TEMP-DATE-ALPHA", FieldType.STRING, 8);

        pnd_Temp_Date_Alpha__R_Field_2 = localVariables.newGroupInRecord("pnd_Temp_Date_Alpha__R_Field_2", "REDEFINE", pnd_Temp_Date_Alpha);
        pnd_Temp_Date_Alpha_Pnd_Temp_Date = pnd_Temp_Date_Alpha__R_Field_2.newFieldInGroup("pnd_Temp_Date_Alpha_Pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 
            8);
        pnd_C_Inv_Acct = localVariables.newFieldInRecord("pnd_C_Inv_Acct", "#C-INV-ACCT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ledger_Records_Read = localVariables.newFieldInRecord("pnd_Ledger_Records_Read", "#LEDGER-RECORDS-READ", FieldType.PACKED_DECIMAL, 14);
        pnd_Ledger_Read = localVariables.newFieldInRecord("pnd_Ledger_Read", "#LEDGER-READ", FieldType.PACKED_DECIMAL, 14);
        pnd_Ledger_Written = localVariables.newFieldInRecord("pnd_Ledger_Written", "#LEDGER-WRITTEN", FieldType.PACKED_DECIMAL, 14);
        pnd_Paymnt_Written = localVariables.newFieldInRecord("pnd_Paymnt_Written", "#PAYMNT-WRITTEN", FieldType.PACKED_DECIMAL, 14);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_S__R_Field_3 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_3", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_3.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 29);
        pnd_Trans_Ind_Parm = localVariables.newFieldInRecord("pnd_Trans_Ind_Parm", "#TRANS-IND-PARM", FieldType.STRING, 40);

        pnd_Trans_Ind_Parm__R_Field_4 = localVariables.newGroupInRecord("pnd_Trans_Ind_Parm__R_Field_4", "REDEFINE", pnd_Trans_Ind_Parm);
        pnd_Trans_Ind_Parm_Pnd_Trans_Ind = pnd_Trans_Ind_Parm__R_Field_4.newFieldInGroup("pnd_Trans_Ind_Parm_Pnd_Trans_Ind", "#TRANS-IND", FieldType.STRING, 
            2);
        pnd_Trans_Ind_Parm_Pnd_Trans_Fil = pnd_Trans_Ind_Parm__R_Field_4.newFieldInGroup("pnd_Trans_Ind_Parm_Pnd_Trans_Fil", "#TRANS-FIL", FieldType.STRING, 
            30);
        pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date = pnd_Trans_Ind_Parm__R_Field_4.newFieldInGroup("pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date", "#TRANS-TEST-DATE", 
            FieldType.STRING, 8);
        pnd_Text = localVariables.newFieldInRecord("pnd_Text", "#TEXT", FieldType.STRING, 30);
        pnd_All = localVariables.newFieldInRecord("pnd_All", "#ALL", FieldType.BOOLEAN, 1);
        pnd_Only_Nz = localVariables.newFieldInRecord("pnd_Only_Nz", "#ONLY-NZ", FieldType.BOOLEAN, 1);
        pnd_System_Date_Ccyymmdd = localVariables.newFieldInRecord("pnd_System_Date_Ccyymmdd", "#SYSTEM-DATE-CCYYMMDD", FieldType.NUMERIC, 8);

        pnd_System_Date_Ccyymmdd__R_Field_5 = localVariables.newGroupInRecord("pnd_System_Date_Ccyymmdd__R_Field_5", "REDEFINE", pnd_System_Date_Ccyymmdd);
        pnd_System_Date_Ccyymmdd_Pnd_System_Date_Ccyymmdd_A = pnd_System_Date_Ccyymmdd__R_Field_5.newFieldInGroup("pnd_System_Date_Ccyymmdd_Pnd_System_Date_Ccyymmdd_A", 
            "#SYSTEM-DATE-CCYYMMDD-A", FieldType.STRING, 8);
        pnd_Nostate_Cnt = localVariables.newFieldInRecord("pnd_Nostate_Cnt", "#NOSTATE-CNT", FieldType.NUMERIC, 6);
        pnd_Nostate = localVariables.newFieldInRecord("pnd_Nostate", "#NOSTATE", FieldType.BOOLEAN, 1);
        pnd_Print_Chk_Dte = localVariables.newFieldInRecord("pnd_Print_Chk_Dte", "#PRINT-CHK-DTE", FieldType.STRING, 10);
        pnd_Print_Stl_Dte = localVariables.newFieldInRecord("pnd_Print_Stl_Dte", "#PRINT-STL-DTE", FieldType.STRING, 10);
        pnd_Print_Act_Dte = localVariables.newFieldInRecord("pnd_Print_Act_Dte", "#PRINT-ACT-DTE", FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmna.initializeValues();
        ldaFcplldgr.initializeValues();
        ldaFcpl801d.initializeValues();
        ldaFcplpmnt.initializeValues();

        localVariables.reset();
        pnd_Ledger_Superde_Start.setInitialValue("5");
        pnd_All.setInitialValue(false);
        pnd_Only_Nz.setInitialValue(false);
        pnd_Nostate.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp120y() throws Exception
    {
        super("Fcpp120y");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp120y|Main");
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        while(true)
        {
            try
            {
                pnd_Max.setValue(10);                                                                                                                                     //Natural: MOVE 10 TO #MAX
                //*  RS0                                                                                                                                                  //Natural: FORMAT ( 00 ) PS = 23 LS = 133 ZP = ON
                //* ********************************************************                                                                                              //Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON
                //*  MAIN PROGRAM *
                //* ********************************************************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Date, new FieldAttributes ("AD='_'"), new ReportEditMask ("YYYYMMDD"));                      //Natural: INPUT #INPUT-DATE ( AD = '_' EM = YYYYMMDD )
                if (condition(pnd_Input_Date.equals(getZero())))                                                                                                          //Natural: IF #INPUT-DATE = 0
                {
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"INPUT INTERFACE DATE WAS NOT SUPPLIED",new TabSetting(77),"***");                //Natural: WRITE ( 1 ) '***' 25T 'INPUT INTERFACE DATE WAS NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                    //*  RS5S (DATA IS 'NZ' OR 'AL')
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.invokeInput(setInputStatus(INPUT_2), this, pnd_Trans_Ind_Parm);                                                                                   //Natural: INPUT #TRANS-IND-PARM
                if (condition(pnd_Trans_Ind_Parm_Pnd_Trans_Ind.equals("  ")))                                                                                             //Natural: IF #TRANS-IND = '  '
                {
                    getReports().write(1, ReportOption.NOTITLE,"***",new TabSetting(25),"TRANSACTION INDICATOR NOT SUPPLIED",new TabSetting(77),"***");                   //Natural: WRITE ( 1 ) '***' 25T 'TRANSACTION INDICATOR NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Trans_Ind_Parm_Pnd_Trans_Ind.equals("NZ")))                                                                                             //Natural: IF #TRANS-IND = 'NZ'
                {
                    pnd_Text.setValue("NZ transactions only");                                                                                                            //Natural: ASSIGN #TEXT := 'NZ transactions only'
                    pnd_Only_Nz.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO #ONLY-NZ
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Text.setValue("ALL transactions except NZ");                                                                                                      //Natural: ASSIGN #TEXT := 'ALL transactions except NZ'
                    pnd_All.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO #ALL
                    //*  IN ORDER TO TEST THIS CONDITION A DATE WILL BE ADDED TO THE PARMCARD
                    //*  IN TEST ENVIRONMENT ONLY STARTING IN POSITION 33 THRU 40
                    if (condition(pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date.notEquals(" ")))                                                                                 //Natural: IF #TRANS-TEST-DATE NE ' '
                    {
                        pnd_Input_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Trans_Ind_Parm_Pnd_Trans_Test_Date);                                             //Natural: MOVE EDITED #TRANS-TEST-DATE TO #INPUT-DATE ( EM = YYYYMMDD )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_System_Date_Ccyymmdd.setValue(Global.getDATN());                                                                                              //Natural: ASSIGN #SYSTEM-DATE-CCYYMMDD := *DATN
                        //*  CTS-0917 >>
                        if (condition(Global.getTIMN().greaterOrEqual(0) && Global.getTIMN().lessOrEqual(800000)))                                                        //Natural: IF *TIMN GE 0000000 AND *TIMN LE 0800000
                        {
                            pnd_System_Date_Ccyymmdd.compute(new ComputeParameters(false, pnd_System_Date_Ccyymmdd), Global.getDATN().subtract(1));                       //Natural: ASSIGN #SYSTEM-DATE-CCYYMMDD := *DATN - 1
                        }                                                                                                                                                 //Natural: END-IF
                        //*  CTS-0917 <<
                        pnd_Input_Date.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_System_Date_Ccyymmdd_Pnd_System_Date_Ccyymmdd_A);                                //Natural: MOVE EDITED #SYSTEM-DATE-CCYYMMDD-A TO #INPUT-DATE ( EM = YYYYMMDD )
                    }                                                                                                                                                     //Natural: END-IF
                    //* RS5E
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(0, "= ",pnd_Input_Date);                                                                                                               //Natural: WRITE '= ' #INPUT-DATE
                if (Global.isEscape()) return;
                getReports().write(0, "=",pnd_Trans_Ind_Parm_Pnd_Trans_Ind);                                                                                              //Natural: WRITE '=' #TRANS-IND
                if (Global.isEscape()) return;
                pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte.setValue(pnd_Input_Date);                                                                           //Natural: ASSIGN #S-LDGR-PYMNT-INTRFCE-DTE := #INPUT-DATE
                pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Filler.setValue("H'00'");                                                                                             //Natural: ASSIGN #S-LDGR-FILLER := H'00'
                //*  SKIP SUMMARY "AP"
                ldaFcplldgr.getVw_fcp_Cons_Ledger().startDatabaseRead                                                                                                     //Natural: READ FCP-CONS-LEDGER BY RCRD-INTRFCE-SETTLMNT-ORGN STARTING FROM #LEDGER-SUPERDE-START WHERE FCP-CONS-LEDGER.CNTRCT-PPCN-NBR NE ' ' AND FCP-CONS-LEDGER.CNTRCT-RCRD-TYP = '5'
                (
                "READ01",
                new Wc[] { new Wc("CNTRCT_PPCN_NBR", "!=", " ", "And", WcType.WHERE) ,
                new Wc("CNTRCT_RCRD_TYP", "=", "5", WcType.WHERE) ,
                new Wc("RCRD_INTRFCE_SETTLMNT_ORGN", ">=", pnd_Ledger_Superde_Start.getBinary(), WcType.BY) },
                new Oc[] { new Oc("RCRD_INTRFCE_SETTLMNT_ORGN", "ASC") }
                );
                READ01:
                while (condition(ldaFcplldgr.getVw_fcp_Cons_Ledger().readNextRow("READ01")))
                {
                    //*  PROCESS PAYMENTS FOR ONE DATE ONLY  (RS1) START
                    //*  EXTRACT ONLY NZ TRANSACTIONS  RS5
                    if (condition(pnd_Only_Nz.getBoolean()))                                                                                                              //Natural: IF #ONLY-NZ
                    {
                        if (condition(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Orgn_Cde().notEquals("NZ")))                                                                  //Natural: IF FCP-CONS-LEDGER.CNTRCT-ORGN-CDE NE 'NZ'
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Intrfce_Dte().greater(pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte)))                     //Natural: IF FCP-CONS-LEDGER.PYMNT-INTRFCE-DTE GT #S-LDGR-PYMNT-INTRFCE-DTE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Intrfce_Dte().notEquals(pnd_Ledger_Superde_Start_Pnd_S_Ldgr_Pymnt_Intrfce_Dte)))                   //Natural: IF FCP-CONS-LEDGER.PYMNT-INTRFCE-DTE NE #S-LDGR-PYMNT-INTRFCE-DTE
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //*  RS1 END
                    //*  EXTRACT ALL EXCEPT NZ TRANS  RS5
                    if (condition(pnd_All.getBoolean()))                                                                                                                  //Natural: IF #ALL
                    {
                        if (condition(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Orgn_Cde().equals("NZ")))                                                                     //Natural: IF FCP-CONS-LEDGER.CNTRCT-ORGN-CDE = 'NZ'
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ledger_Records_Read.nadd(1);                                                                                                                      //Natural: ADD 1 TO #LEDGER-RECORDS-READ
                                                                                                                                                                          //Natural: PERFORM GET-PYMNT-RECORD
                    sub_Get_Pymnt_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    if (condition(! (pay.getBoolean()) && ! (arcpay.getBoolean())))                                                                                       //Natural: IF NOT PAY AND NOT ARCPAY
                    {
                        getReports().write(0, "Expanded Payment Data not available",pdaFcpapmnt.getPymnt_Cntrct_Ppcn_Nbr());                                              //Natural: WRITE 'Expanded Payment Data not available' PYMNT.CNTRCT-PPCN-NBR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        //*  RS4 DON't move old data to ledger
                        ldaFcplpmna.getVw_fcp_Arch_Pymnt().reset();                                                                                                       //Natural: RESET FCP-ARCH-PYMNT
                        //*      ESCAPE TOP                /* RS4 ACCEPT ALL TRANSACTIONS
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Nostate.setValue(false);                                                                                                                          //Natural: MOVE FALSE TO #NOSTATE
                                                                                                                                                                          //Natural: PERFORM MOVE-TO-LEDGER-EXTRACT
                    sub_Move_To_Ledger_Extract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*  ----------------------------------------------------------------------
                //*  RS6
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 01 ) 3 LINES
                getReports().write(1, ReportOption.NOTITLE,"Total payments with no state: ",pnd_Nostate_Cnt);                                                             //Natural: WRITE ( 01 ) 'Total payments with no state: ' #NOSTATE-CNT
                if (Global.isEscape()) return;
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 01 ) 3 LINES
                //*  RS5
                getReports().write(1, new FieldAttributes ("AD=I"),ReportOption.NOTITLE,new TabSetting(7),"EXTRACT FOR THE INTERFACE DATE :", new FieldAttributes         //Natural: WRITE ( 01 ) ( AD = I ) 7T 'EXTRACT FOR THE INTERFACE DATE :' ( AD = I ) #INPUT-DATE ( EM = MM/DD/YYYY ) / 7T 'EXTRACT FOR TRANSACTIONS       :' ( AD = I ) #TEXT / 7T 'NUMBER OF LEDGER RECORDS READ  :' ( AD = I ) #LEDGER-RECORDS-READ '(FROM FCP-CONS-LEDGER FILE)' ( AD = I ) / 7T '    (ACTUAL LEDGERS PROCESSED) :' ( AD = I ) #LEDGER-READ / 7T 'LEDGER  EXTRACT RECORDS WRITTEN:' ( AD = I ) #LEDGER-WRITTEN /
                    ("AD=I"),pnd_Input_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(7),"EXTRACT FOR TRANSACTIONS       :", new FieldAttributes 
                    ("AD=I"),pnd_Text,NEWLINE,new TabSetting(7),"NUMBER OF LEDGER RECORDS READ  :", new FieldAttributes ("AD=I"),pnd_Ledger_Records_Read, 
                    new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZ9"),"(FROM FCP-CONS-LEDGER FILE)", new FieldAttributes ("AD=I"),NEWLINE,new TabSetting(7),"    (ACTUAL LEDGERS PROCESSED) :", 
                    new FieldAttributes ("AD=I"),pnd_Ledger_Read, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE,new TabSetting(7),"LEDGER  EXTRACT RECORDS WRITTEN:", 
                    new FieldAttributes ("AD=I"),pnd_Ledger_Written, new ReportEditMask ("ZZ,ZZZ,ZZZ,ZZZ,ZZ9"),NEWLINE);
                if (Global.isEscape()) return;
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 01 ) 3 LINES
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE);                    //Natural: WRITE ( 01 ) 7T '************ E N D   O F   T H E   R E P O R T   ************' /
                if (Global.isEscape()) return;
                //* *----------------------------
                //*  ---------------------------------
                //*  ------------------------------------
                //* *--------------------------------
                //* *--------------------------------
                //* ******************
                //* *--------------------------------                                                                                                                     //Natural: AT TOP OF PAGE ( 1 )
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Move_To_Ledger_Extract() throws Exception                                                                                                            //Natural: MOVE-TO-LEDGER-EXTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        ldaFcpl801d.getPnd_Ledger_Ext_2().reset();                                                                                                                        //Natural: RESET #LEDGER-EXT-2
        ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Locality_Cde().setValue(pdaFcpapmnt.getPymnt_Annt_Locality_Cde());                                                           //Natural: MOVE PYMNT.ANNT-LOCALITY-CDE TO #LEDGER-EXT-2.ANNT-LOCALITY-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Ppcn_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Ppcn_Nbr());                                                     //Natural: MOVE FCP-CONS-LEDGER.CNTRCT-PPCN-NBR TO #LEDGER-EXT-2.CNTRCT-PPCN-NBR
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Payee_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Payee_Cde());                                                             //Natural: MOVE PYMNT.CNTRCT-PAYEE-CDE TO #LEDGER-EXT-2.CNTRCT-PAYEE-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Orgn_Cde().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Orgn_Cde());                                                     //Natural: MOVE FCP-CONS-LEDGER.CNTRCT-ORGN-CDE TO #LEDGER-EXT-2.CNTRCT-ORGN-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Check_Dte().setValueEdited(pdaFcpapmnt.getPymnt_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                          //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #LEDGER-EXT-2.PYMNT-CHECK-DTE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Acctg_Dte().setValueEdited(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Acctg_Dte(),new ReportEditMask("YYYYMMDD"));                //Natural: MOVE EDITED FCP-CONS-LEDGER.PYMNT-ACCTG-DTE ( EM = YYYYMMDD ) TO #LEDGER-EXT-2.PYMNT-ACCTG-DTE
        //*  (RS2)
        ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Settlmnt_Dte().setValueEdited(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD"));          //Natural: MOVE EDITED FCP-CONS-LEDGER.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO #LEDGER-EXT-2.PYMNT-SETTLMNT-DTE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Mode_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Mode_Cde());                                                               //Natural: MOVE PYMNT.CNTRCT-MODE-CDE TO #LEDGER-EXT-2.CNTRCT-MODE-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Annty_Ins_Type().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Annty_Ins_Type());                                         //Natural: MOVE FCP-CONS-LEDGER.CNTRCT-ANNTY-INS-TYPE TO #LEDGER-EXT-2.CNTRCT-ANNTY-INS-TYPE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Annty_Type_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Annty_Type_Cde());                                                   //Natural: MOVE PYMNT.CNTRCT-ANNTY-TYPE-CDE TO #LEDGER-EXT-2.CNTRCT-ANNTY-TYPE-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Insurance_Option().setValue(pdaFcpapmnt.getPymnt_Cntrct_Insurance_Option());                                               //Natural: MOVE PYMNT.CNTRCT-INSURANCE-OPTION TO #LEDGER-EXT-2.CNTRCT-INSURANCE-OPTION
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Life_Contingency().setValue(pdaFcpapmnt.getPymnt_Cntrct_Life_Contingency());                                               //Natural: MOVE PYMNT.CNTRCT-LIFE-CONTINGENCY TO #LEDGER-EXT-2.CNTRCT-LIFE-CONTINGENCY
        //*  RS3
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Redraw_Ind().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Cancel_Rdrw_Actvty_Cde());                                     //Natural: MOVE FCP-CONS-LEDGER.CNTRCT-CANCEL-RDRW-ACTVTY-CDE TO #LEDGER-EXT-2.CNTRCT-REDRAW-IND
        ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Rsdncy_Cde().setValue(pdaFcpapmnt.getPymnt_Annt_Rsdncy_Cde());                                                               //Natural: MOVE PYMNT.ANNT-RSDNCY-CDE TO #LEDGER-EXT-2.ANNT-RSDNCY-CDE
        //*  RS7
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Option_Cde().setValue(pdaFcpapmnt.getPymnt_Cntrct_Option_Cde());                                                           //Natural: MOVE PYMNT.CNTRCT-OPTION-CDE TO #LEDGER-EXT-2.CNTRCT-OPTION-CDE
        //*  RS7
        ldaFcpl801d.getPnd_Ledger_Ext_2_Ia_Orgn_Cde().setValue(pdaFcpapmnt.getPymnt_Ia_Orgn_Cde());                                                                       //Natural: MOVE PYMNT.IA-ORGN-CDE TO #LEDGER-EXT-2.IA-ORGN-CDE
        //*  RS6
        short decideConditionsMet1016 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PYMNT.ANNT-RSDNCY-CDE = MASK ( NN )
        if (condition(DbsUtil.maskMatches(pdaFcpapmnt.getPymnt_Annt_Rsdncy_Cde(),"NN")))
        {
            decideConditionsMet1016++;
            ignore();
        }                                                                                                                                                                 //Natural: WHEN PYMNT.ANNT-RSDNCY-CDE = MASK ( AA )
        else if (condition(DbsUtil.maskMatches(pdaFcpapmnt.getPymnt_Annt_Rsdncy_Cde(),"AA")))
        {
            decideConditionsMet1016++;
            ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Rsdncy_Cde().setValue("FO");                                                                                             //Natural: MOVE 'FO' TO #LEDGER-EXT-2.ANNT-RSDNCY-CDE
        }                                                                                                                                                                 //Natural: WHEN PYMNT.ANNT-RSDNCY-CDE = '  '
        else if (condition(pdaFcpapmnt.getPymnt_Annt_Rsdncy_Cde().equals("  ")))
        {
            decideConditionsMet1016++;
            pnd_Nostate.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO #NOSTATE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ledger_Read.nadd(ldaFcplldgr.getFcp_Cons_Ledger_Count_Castinv_Ledgr());                                                                                       //Natural: ADD FCP-CONS-LEDGER.C*INV-LEDGR TO #LEDGER-READ
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO C*INV-LEDGR
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcplldgr.getFcp_Cons_Ledger_Count_Castinv_Ledgr())); pnd_I.nadd(1))
        {
            ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Isa_Hash().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Inv_Acct_Cde().getValue(pnd_I));                                       //Natural: ASSIGN #LEDGER-EXT-2.INV-ISA-HASH := FCP-CONS-LEDGER.INV-ACCT-CDE ( #I )
            ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr().getValue(pnd_I));                           //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-NBR := FCP-CONS-LEDGER.INV-ACCT-LEDGR-NBR ( #I )
            ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Amt().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Inv_Acct_Ledgr_Amt().getValue(pnd_I));                           //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-AMT := FCP-CONS-LEDGER.INV-ACCT-LEDGR-AMT ( #I )
            ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Ind().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Inv_Acct_Ledgr_Ind().getValue(pnd_I));                           //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-IND := FCP-CONS-LEDGER.INV-ACCT-LEDGR-IND ( #I )
            if (condition(pnd_Nostate.getBoolean()))                                                                                                                      //Natural: IF #NOSTATE
            {
                pnd_Nostate_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NOSTATE-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                sub_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, ldaFcpl801d.getPnd_Ledger_Ext_2());                                                                                            //Natural: WRITE WORK FILE 01 #LEDGER-EXT-2
            pnd_Ledger_Written.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #LEDGER-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ledger_Read.nadd(ldaFcplldgr.getFcp_Cons_Ledger_Count_Castinv_Ledgr_Ovrfl());                                                                                 //Natural: ADD FCP-CONS-LEDGER.C*INV-LEDGR-OVRFL TO #LEDGER-READ
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO C*INV-LEDGR-OVRFL
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcplldgr.getFcp_Cons_Ledger_Count_Castinv_Ledgr_Ovrfl())); pnd_I.nadd(1))
        {
            ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Inv_Acct_Ledgr_Nbr_Ovrfl().getValue(pnd_I));                     //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-NBR := FCP-CONS-LEDGER.INV-ACCT-LEDGR-NBR-OVRFL ( #I )
            ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Amt().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Inv_Acct_Ledgr_Amt_Ovrfl().getValue(pnd_I));                     //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-AMT := FCP-CONS-LEDGER.INV-ACCT-LEDGR-AMT-OVRFL ( #I )
            ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Ind().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Inv_Acct_Ledgr_Ind_Ovrfl().getValue(pnd_I));                     //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-IND := FCP-CONS-LEDGER.INV-ACCT-LEDGR-IND-OVRFL ( #I )
            if (condition(pnd_Nostate.getBoolean()))                                                                                                                      //Natural: IF #NOSTATE
            {
                pnd_Nostate_Cnt.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #NOSTATE-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT
                sub_Write_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(1, false, ldaFcpl801d.getPnd_Ledger_Ext_2());                                                                                            //Natural: WRITE WORK FILE 01 #LEDGER-EXT-2
            pnd_Ledger_Written.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #LEDGER-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Get_Pymnt_Record() throws Exception                                                                                                                  //Natural: GET-PYMNT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------
        pay.setValue(false);                                                                                                                                              //Natural: MOVE FALSE TO PAY
        //*  RS0
        pnd_Process.setValue(false);                                                                                                                                      //Natural: MOVE FALSE TO #PROCESS
        pdaFcpapmnt.getPymnt_Cntrct_Ppcn_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Ppcn_Nbr());                                                                //Natural: ASSIGN PYMNT.CNTRCT-PPCN-NBR := FCP-CONS-LEDGER.CNTRCT-PPCN-NBR
        pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Orgn_Cde());                                                                //Natural: ASSIGN PYMNT.CNTRCT-ORGN-CDE := FCP-CONS-LEDGER.CNTRCT-ORGN-CDE
        pdaFcpapmnt.getPymnt_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr());                                                        //Natural: ASSIGN PYMNT.PYMNT-PRCSS-SEQ-NBR := FCP-CONS-LEDGER.PYMNT-PRCSS-SEQ-NBR
        pnd_Temp_Date_Alpha.setValueEdited(ldaFcplldgr.getFcp_Cons_Ledger_Cntl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED FCP-CONS-LEDGER.CNTL-CHECK-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-ALPHA
        pdaFcpapmnt.getPymnt_Cntrct_Invrse_Dte().compute(new ComputeParameters(false, pdaFcpapmnt.getPymnt_Cntrct_Invrse_Dte()), DbsField.subtract(100000000,             //Natural: SUBTRACT #TEMP-DATE FROM 100000000 GIVING PYMNT.CNTRCT-INVRSE-DTE
            pnd_Temp_Date_Alpha_Pnd_Temp_Date));
        //*  FROM FCPAPMNT TO SUPER DESCRIPTOR
        pnd_Pymnt_S.setValuesByName(pdaFcpapmnt.getPymnt());                                                                                                              //Natural: MOVE BY NAME PYMNT TO #PYMNT-S
        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-CONS-PYMNT BY PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE THRU #PYMNT-SUPERDE
        (
        "RPYMNT",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde, "And", WcType.BY) ,
        new Wc("PPCN_INV_ORGN_PRCSS_INST", "<=", pnd_Pymnt_S_Pnd_Pymnt_Superde, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        RPYMNT:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("RPYMNT")))
        {
            //*  ACCEPT IF FCP-CONS-PYMNT.CNR-CS-ACTVTY-CDE NE ' '      /* RS1
            //*  RS0
            pnd_Process.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO #PROCESS
            pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Isn().setValue(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstISN("RPYMNT"));                                                //Natural: MOVE *ISN TO PYMNT-ISN
            //*  FROM FCPLPMNT TO FCPAPMNT
            pdaFcpapmnt.getPymnt().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                   //Natural: MOVE BY NAME FCP-CONS-PYMNT TO PYMNT
            pdaFcpapmnt.getPymnt_C_Inv_Acct().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                               //Natural: MOVE C*INV-ACCT TO PYMNT.C-INV-ACCT
            pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                                     //Natural: MOVE C*PYMNT-DED-GRP TO PYMNT.C-PYMNT-DED-GRP
            pdaFcpapmnt.getPymnt_C_Inv_Rtb_Grp().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Rtb_Grp());                                                         //Natural: MOVE C*INV-RTB-GRP TO PYMNT.C-INV-RTB-GRP
            pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Return_Cde().reset();                                                                                                  //Natural: RESET PYMNT-RETURN-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().equals(getZero())))                                                                              //Natural: IF *COUNTER ( RPYMNT. ) = 0
        {
            pay.setValue(false);                                                                                                                                          //Natural: MOVE FALSE TO PAY
                                                                                                                                                                          //Natural: PERFORM GET-ARCH-PAYMENT
            sub_Get_Arch_Payment();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition((ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().notEquals(getZero()) && pnd_Process.getBoolean())))                                         //Natural: IF ( *COUNTER ( RPYMNT. ) NE 0 AND #PROCESS )
            {
                if (condition(pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde().equals("DC")))                                                                                       //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'DC'
                {
                    pay.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO PAY
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde().equals("NZ")))                                                                                       //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'NZ'
                {
                    pay.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO PAY
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde().equals("AP")))                                                                                       //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'AP'
                {
                    //*  FE201408
                    //*  FE201408
                    if (condition((pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C ") || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" C") //Natural: IF ( PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'C ' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ ' C' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'S ' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ ' S' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'PR' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'CP' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'SP' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'RP' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'AS' OR PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'AP' )
                        || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S ") || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" S") 
                        || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") 
                        || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") 
                        || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AS") || pdaFcpapmnt.getPymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("AP"))))
                    {
                        //*  B.HOLLOWAY 12/08/2010 END
                        pay.setValue(true);                                                                                                                               //Natural: MOVE TRUE TO PAY
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pay.setValue(false);                                                                                                                              //Natural: MOVE FALSE TO PAY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Arch_Payment() throws Exception                                                                                                                  //Natural: GET-ARCH-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ------------------------------------
        arcpay.setValue(false);                                                                                                                                           //Natural: MOVE FALSE TO ARCPAY
        //*  RS0
        pnd_Process.setValue(false);                                                                                                                                      //Natural: MOVE FALSE TO #PROCESS
        pdaFcpapmnt.getPymnt_Cntrct_Ppcn_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Ppcn_Nbr());                                                                //Natural: ASSIGN PYMNT.CNTRCT-PPCN-NBR := FCP-CONS-LEDGER.CNTRCT-PPCN-NBR
        pdaFcpapmnt.getPymnt_Cntrct_Orgn_Cde().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Orgn_Cde());                                                                //Natural: ASSIGN PYMNT.CNTRCT-ORGN-CDE := FCP-CONS-LEDGER.CNTRCT-ORGN-CDE
        pdaFcpapmnt.getPymnt_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcplldgr.getFcp_Cons_Ledger_Pymnt_Prcss_Seq_Nbr());                                                        //Natural: ASSIGN PYMNT.PYMNT-PRCSS-SEQ-NBR := FCP-CONS-LEDGER.PYMNT-PRCSS-SEQ-NBR
        pnd_Temp_Date_Alpha.setValueEdited(ldaFcplldgr.getFcp_Cons_Ledger_Cntl_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                               //Natural: MOVE EDITED FCP-CONS-LEDGER.CNTL-CHECK-DTE ( EM = YYYYMMDD ) TO #TEMP-DATE-ALPHA
        pdaFcpapmnt.getPymnt_Cntrct_Invrse_Dte().compute(new ComputeParameters(false, pdaFcpapmnt.getPymnt_Cntrct_Invrse_Dte()), DbsField.subtract(100000000,             //Natural: SUBTRACT #TEMP-DATE FROM 100000000 GIVING PYMNT.CNTRCT-INVRSE-DTE
            pnd_Temp_Date_Alpha_Pnd_Temp_Date));
        pnd_Pymnt_S.setValuesByName(pdaFcpapmnt.getPymnt());                                                                                                              //Natural: MOVE BY NAME PYMNT TO #PYMNT-S
        ldaFcplpmna.getVw_fcp_Arch_Pymnt().startDatabaseRead                                                                                                              //Natural: READ ( 1 ) FCP-ARCH-PYMNT BY PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE THRU #PYMNT-SUPERDE
        (
        "APYMNT",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde, "And", WcType.BY) ,
        new Wc("PPCN_INV_ORGN_PRCSS_INST", "<=", pnd_Pymnt_S_Pnd_Pymnt_Superde, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        APYMNT:
        while (condition(ldaFcplpmna.getVw_fcp_Arch_Pymnt().readNextRow("APYMNT")))
        {
            //*  RS0
            if (condition(!(ldaFcplpmna.getFcp_Arch_Pymnt_Cnr_Cs_Actvty_Cde().notEquals(" "))))                                                                           //Natural: ACCEPT IF FCP-ARCH-PYMNT.CNR-CS-ACTVTY-CDE NE ' '
            {
                continue;
            }
            //*  RS0
            pnd_Process.setValue(true);                                                                                                                                   //Natural: MOVE TRUE TO #PROCESS
            pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Isn().setValue(ldaFcplpmna.getVw_fcp_Arch_Pymnt().getAstISN("APYMNT"));                                                //Natural: MOVE *ISN TO PYMNT-ISN
            pdaFcpapmnt.getPymnt().setValuesByName(ldaFcplpmna.getVw_fcp_Arch_Pymnt());                                                                                   //Natural: MOVE BY NAME FCP-ARCH-PYMNT TO PYMNT
            pdaFcpapmnt.getPymnt_C_Inv_Acct().setValue(ldaFcplpmna.getFcp_Arch_Pymnt_Count_Castinv_Acct());                                                               //Natural: MOVE FCP-ARCH-PYMNT.C*INV-ACCT TO PYMNT.C-INV-ACCT
            pdaFcpapmnt.getPymnt_C_Pymnt_Ded_Grp().setValue(ldaFcplpmna.getFcp_Arch_Pymnt_Count_Castpymnt_Ded_Grp());                                                     //Natural: MOVE FCP-ARCH-PYMNT.C*PYMNT-DED-GRP TO PYMNT.C-PYMNT-DED-GRP
            pdaFcpapmnt.getPymnt_C_Inv_Rtb_Grp().setValue(ldaFcplpmna.getFcp_Arch_Pymnt_Count_Castinv_Rtb_Grp());                                                         //Natural: MOVE FCP-ARCH-PYMNT.C*INV-RTB-GRP TO PYMNT.C-INV-RTB-GRP
            pdaFcpapmnt.getPymnt_Work_Fields_Pymnt_Return_Cde().reset();                                                                                                  //Natural: RESET PYMNT-RETURN-CDE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcplpmna.getVw_fcp_Arch_Pymnt().getAstCOUNTER().equals(getZero())))                                                                              //Natural: IF *COUNTER ( APYMNT. ) = 0
        {
            arcpay.setValue(false);                                                                                                                                       //Natural: MOVE FALSE TO ARCPAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition((ldaFcplpmna.getVw_fcp_Arch_Pymnt().getAstCOUNTER().notEquals(getZero()) && pnd_Process.getBoolean())))                                         //Natural: IF ( *COUNTER ( APYMNT. ) NE 0 AND #PROCESS )
            {
                if (condition(ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Orgn_Cde().equals("DC")))                                                                              //Natural: IF FCP-ARCH-PYMNT.CNTRCT-ORGN-CDE = 'DC'
                {
                    arcpay.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO ARCPAY
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Orgn_Cde().equals("NZ")))                                                                              //Natural: IF FCP-ARCH-PYMNT.CNTRCT-ORGN-CDE = 'NZ'
                {
                    arcpay.setValue(true);                                                                                                                                //Natural: MOVE TRUE TO ARCPAY
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Orgn_Cde().equals("AP")))                                                                              //Natural: IF FCP-ARCH-PYMNT.CNTRCT-ORGN-CDE = 'AP'
                {
                    if (condition((ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C ") || ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" C")  //Natural: IF ( FCP-ARCH-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'C ' OR FCP-ARCH-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ ' C' OR FCP-ARCH-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'S ' OR FCP-ARCH-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ ' S' OR FCP-ARCH-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'PR' OR FCP-ARCH-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'CP' OR FCP-ARCH-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'SP' OR FCP-ARCH-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ 'RP' )
                        || ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S ") || ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" S") 
                        || ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR") || ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") 
                        || ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") || ldaFcplpmna.getFcp_Arch_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP"))))
                    {
                        //*  B.HOLLOWAY 12/08/2010 END
                        arcpay.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO ARCPAY
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  RS6
    private void sub_Write_Report() throws Exception                                                                                                                      //Natural: WRITE-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        pnd_Print_Chk_Dte.setValueEdited(pdaFcpapmnt.getPymnt_Pymnt_Check_Dte(),new ReportEditMask("MM/DD/YYYY"));                                                        //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) TO #PRINT-CHK-DTE
        pnd_Print_Act_Dte.setValueEdited(pdaFcpapmnt.getPymnt_Pymnt_Acctg_Dte(),new ReportEditMask("MM/DD/YYYY"));                                                        //Natural: MOVE EDITED PYMNT.PYMNT-ACCTG-DTE ( EM = MM/DD/YYYY ) TO #PRINT-ACT-DTE
        pnd_Print_Stl_Dte.setValueEdited(pdaFcpapmnt.getPymnt_Pymnt_Settlmnt_Dte(),new ReportEditMask("MM/DD/YYYY"));                                                     //Natural: MOVE EDITED PYMNT.PYMNT-SETTLMNT-DTE ( EM = MM/DD/YYYY ) TO #PRINT-STL-DTE
        getReports().write(1, ReportOption.NOTITLE,ldaFcplldgr.getFcp_Cons_Ledger_Cntrct_Ppcn_Nbr(),new ColumnSpacing(1),pnd_Print_Stl_Dte,new ColumnSpacing(1),pnd_Print_Chk_Dte,pnd_Print_Act_Dte,new  //Natural: WRITE ( 1 ) FCP-CONS-LEDGER.CNTRCT-PPCN-NBR 1X #PRINT-STL-DTE 1X #PRINT-CHK-DTE #PRINT-ACT-DTE 1X #LEDGER-EXT-2.INV-ACCT-LEDGR-NBR 1X #LEDGER-EXT-2.CNTRCT-COMPANY-CDE 1X #LEDGER-EXT-2.INV-ACCT-LEDGR-AMT
            ColumnSpacing(1),ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Nbr(),new ColumnSpacing(1),ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Company_Cde(),new 
            ColumnSpacing(1),ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Amt());
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    //*  WRITE (01) TITLE LEFT
                    getReports().write(1, ReportOption.NOTITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(53),"Consolidated Payment System",new  //Natural: WRITE ( 01 ) NOTITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Consolidated Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'Ledger Reporting Extract' / 'DATE:' *DATU 38X 'Transactions Missing State Code' / 'Contract   Settlmnt     Check       Acctg    Ledger     Comp          Ledger   ' / ' Number      Date       Date        Date     Account    Code          Amount   ' / '-------------------------------------------------------------------------------' //
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(55),"Ledger Reporting Extract",NEWLINE,"DATE:",Global.getDATU(),new ColumnSpacing(38),"Transactions Missing State Code",
                        NEWLINE,"Contract   Settlmnt     Check       Acctg    Ledger     Comp          Ledger   ",NEWLINE," Number      Date       Date        Date     Account    Code          Amount   ",
                        NEWLINE,"-------------------------------------------------------------------------------",NEWLINE,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
    }
}
