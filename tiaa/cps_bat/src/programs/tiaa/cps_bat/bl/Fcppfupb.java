/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:46 PM
**        * FROM NATURAL PROGRAM : Fcppfupb
************************************************************
**        * FILE NAME            : Fcppfupb.java
**        * CLASS NAME           : Fcppfupb
**        * INSTANCE NAME        : Fcppfupb
************************************************************
************************************************************************
* PROGRAM   : FCPPFUPB
*
* SYSTEM    : CPS
* TITLE     : LEDGER REVERSAL MODULE
* GENERATED : 08/13/2020  CTS CPS SUNSET (CHG830649)
*
* FUNCTION  : THIS MODULE READS CURRENT LEDGER FILE AND STORE
*           : FUTURE PAYMENTS IN GDS.
*
* HISTORY   :
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcppfupb extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ledger_Ext_1;

    private DbsGroup pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Orgn_Cde;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Payee_Cde;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Mode_Cde;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Insurance_Option;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Life_Contingency;
    private DbsField pnd_Ledger_Ext_1_Annt_Rsdncy_Cde;
    private DbsField pnd_Ledger_Ext_1_Annt_Locality_Cde;
    private DbsField pnd_Ledger_Ext_1_Pymnt_Check_Dte;
    private DbsField pnd_Ledger_Ext_1_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ledger_Ext_1_Pymnt_Acctg_Dte;

    private DbsGroup pnd_Ledger_Ext_1_Inv_Ledgr;
    private DbsField pnd_Ledger_Ext_1_Inv_Isa_Hash;
    private DbsField pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Amt;
    private DbsField pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Ind;

    private DbsGroup pnd_Ledger_Ext_1_Pnd_Misc_Fields;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Company_Cde;
    private DbsField pnd_Ledger_Ext_1_Inv_Pymnt_Ded_Cde;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Redraw_Ind;
    private DbsField pnd_Ledger_Ext_1_Ia_Orgn_Cde;
    private DbsField pnd_Ledger_Ext_1_Cntrct_Option_Cde;
    private DbsField pnd_Ledger_Ext_1_Pymnt_Intrfce_Dte;
    private DbsField pnd_Datx;
    private DbsField pnd_Input_Date;
    private DbsField pnd_Interface_Date;

    private DbsGroup pnd_Cntl_File;
    private DbsField pnd_Cntl_File_Pnd_Prev_Run_Date;
    private DbsField pnd_Cntl_File_Filler;
    private DbsField pnd_Curr_Run_Date;
    private DbsField pnd_Ws_Prev_Run_Date;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ledger_Ext_1 = localVariables.newGroupInRecord("pnd_Ledger_Ext_1", "#LEDGER-EXT-1");

        pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1 = pnd_Ledger_Ext_1.newGroupInGroup("pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1", "#LEDGER-GRP-1");
        pnd_Ledger_Ext_1_Cntrct_Orgn_Cde = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ledger_Ext_1_Cntrct_Ppcn_Nbr = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ledger_Ext_1_Cntrct_Payee_Cde = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ledger_Ext_1_Cntrct_Mode_Cde = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ledger_Ext_1_Cntrct_Annty_Ins_Type = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_1_Cntrct_Annty_Type_Cde = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_1_Cntrct_Insurance_Option = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_1_Cntrct_Life_Contingency = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_1_Annt_Rsdncy_Cde = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 
            2);
        pnd_Ledger_Ext_1_Annt_Locality_Cde = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Annt_Locality_Cde", "ANNT-LOCALITY-CDE", 
            FieldType.STRING, 2);
        pnd_Ledger_Ext_1_Pymnt_Check_Dte = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.STRING, 
            8);
        pnd_Ledger_Ext_1_Pymnt_Settlmnt_Dte = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", 
            FieldType.STRING, 8);
        pnd_Ledger_Ext_1_Pymnt_Acctg_Dte = pnd_Ledger_Ext_1_Pnd_Ledger_Grp_1.newFieldInGroup("pnd_Ledger_Ext_1_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.STRING, 
            8);

        pnd_Ledger_Ext_1_Inv_Ledgr = pnd_Ledger_Ext_1.newGroupInGroup("pnd_Ledger_Ext_1_Inv_Ledgr", "INV-LEDGR");
        pnd_Ledger_Ext_1_Inv_Isa_Hash = pnd_Ledger_Ext_1_Inv_Ledgr.newFieldInGroup("pnd_Ledger_Ext_1_Inv_Isa_Hash", "INV-ISA-HASH", FieldType.STRING, 
            2);
        pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Nbr = pnd_Ledger_Ext_1_Inv_Ledgr.newFieldInGroup("pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Nbr", "INV-ACCT-LEDGR-NBR", 
            FieldType.STRING, 15);
        pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Amt = pnd_Ledger_Ext_1_Inv_Ledgr.newFieldInGroup("pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Amt", "INV-ACCT-LEDGR-AMT", 
            FieldType.NUMERIC, 13, 2);
        pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Ind = pnd_Ledger_Ext_1_Inv_Ledgr.newFieldInGroup("pnd_Ledger_Ext_1_Inv_Acct_Ledgr_Ind", "INV-ACCT-LEDGR-IND", 
            FieldType.STRING, 1);

        pnd_Ledger_Ext_1_Pnd_Misc_Fields = pnd_Ledger_Ext_1.newGroupInGroup("pnd_Ledger_Ext_1_Pnd_Misc_Fields", "#MISC-FIELDS");
        pnd_Ledger_Ext_1_Cntrct_Company_Cde = pnd_Ledger_Ext_1_Pnd_Misc_Fields.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Company_Cde", "CNTRCT-COMPANY-CDE", 
            FieldType.STRING, 1);
        pnd_Ledger_Ext_1_Inv_Pymnt_Ded_Cde = pnd_Ledger_Ext_1_Pnd_Misc_Fields.newFieldInGroup("pnd_Ledger_Ext_1_Inv_Pymnt_Ded_Cde", "INV-PYMNT-DED-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Ledger_Ext_1_Cntrct_Redraw_Ind = pnd_Ledger_Ext_1_Pnd_Misc_Fields.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Redraw_Ind", "CNTRCT-REDRAW-IND", 
            FieldType.STRING, 2);
        pnd_Ledger_Ext_1_Ia_Orgn_Cde = pnd_Ledger_Ext_1_Pnd_Misc_Fields.newFieldInGroup("pnd_Ledger_Ext_1_Ia_Orgn_Cde", "IA-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ledger_Ext_1_Cntrct_Option_Cde = pnd_Ledger_Ext_1_Pnd_Misc_Fields.newFieldInGroup("pnd_Ledger_Ext_1_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ledger_Ext_1_Pymnt_Intrfce_Dte = pnd_Ledger_Ext_1_Pnd_Misc_Fields.newFieldInGroup("pnd_Ledger_Ext_1_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.STRING, 8);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Input_Date = localVariables.newFieldInRecord("pnd_Input_Date", "#INPUT-DATE", FieldType.DATE);
        pnd_Interface_Date = localVariables.newFieldInRecord("pnd_Interface_Date", "#INTERFACE-DATE", FieldType.STRING, 8);

        pnd_Cntl_File = localVariables.newGroupInRecord("pnd_Cntl_File", "#CNTL-FILE");
        pnd_Cntl_File_Pnd_Prev_Run_Date = pnd_Cntl_File.newFieldInGroup("pnd_Cntl_File_Pnd_Prev_Run_Date", "#PREV-RUN-DATE", FieldType.STRING, 8);
        pnd_Cntl_File_Filler = pnd_Cntl_File.newFieldInGroup("pnd_Cntl_File_Filler", "FILLER", FieldType.STRING, 72);
        pnd_Curr_Run_Date = localVariables.newFieldInRecord("pnd_Curr_Run_Date", "#CURR-RUN-DATE", FieldType.STRING, 8);
        pnd_Ws_Prev_Run_Date = localVariables.newFieldInRecord("pnd_Ws_Prev_Run_Date", "#WS-PREV-RUN-DATE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcppfupb() throws Exception
    {
        super("Fcppfupb");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcppfupb|Main");
        while(true)
        {
            try
            {
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Date, new FieldAttributes ("AD='_'"), new ReportEditMask ("YYYYMMDD"));                      //Natural: INPUT #INPUT-DATE ( AD = '_' EM = YYYYMMDD )
                if (condition(pnd_Input_Date.equals(getZero())))                                                                                                          //Natural: IF #INPUT-DATE = 0
                {
                    getReports().write(1, "***",new TabSetting(25),"INPUT INTERFACE DATE WAS NOT SUPPLIED",new TabSetting(77),"***");                                     //Natural: WRITE ( 1 ) '***' 25T 'INPUT INTERFACE DATE WAS NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 99
                }                                                                                                                                                         //Natural: END-IF
                pnd_Datx.setValue(Global.getDATX());                                                                                                                      //Natural: ASSIGN #DATX := *DATX
                pnd_Curr_Run_Date.setValueEdited(pnd_Datx,new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #CURR-RUN-DATE
                getReports().write(0, "#CURR-RUN-DATE: ",pnd_Curr_Run_Date);                                                                                              //Natural: WRITE '#CURR-RUN-DATE: ' #CURR-RUN-DATE
                if (Global.isEscape()) return;
                READWORK01:                                                                                                                                               //Natural: READ WORK 04 RECORD #CNTL-FILE
                while (condition(getWorkFiles().read(4, pnd_Cntl_File)))
                {
                    getReports().write(0, "#PREV-RUN-DATE: ",pnd_Cntl_File_Pnd_Prev_Run_Date);                                                                            //Natural: WRITE '#PREV-RUN-DATE: ' #PREV-RUN-DATE
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Ws_Prev_Run_Date.setValue(pnd_Cntl_File_Pnd_Prev_Run_Date);                                                                                       //Natural: MOVE #PREV-RUN-DATE TO #WS-PREV-RUN-DATE
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                READWORK02:                                                                                                                                               //Natural: READ WORK 01 RECORD #LEDGER-EXT-1
                while (condition(getWorkFiles().read(1, pnd_Ledger_Ext_1)))
                {
                    if (condition(pnd_Ledger_Ext_1_Cntrct_Orgn_Cde.equals("NZ")))                                                                                         //Natural: IF CNTRCT-ORGN-CDE = 'NZ'
                    {
                        if (condition(pnd_Curr_Run_Date.equals(pnd_Ws_Prev_Run_Date)))                                                                                    //Natural: IF #CURR-RUN-DATE = #WS-PREV-RUN-DATE
                        {
                                                                                                                                                                          //Natural: PERFORM ERROR-JOB-RUN
                            sub_Error_Job_Run();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition((Global.getTIMN().greaterOrEqual(0) && Global.getTIMN().lessOrEqual(2000000)) && (pnd_Curr_Run_Date.greater(pnd_Ws_Prev_Run_Date)))) //Natural: IF ( *TIMN GE 0000000 AND *TIMN LE 2000000 ) AND ( #CURR-RUN-DATE GT #WS-PREV-RUN-DATE )
                        {
                            pnd_Datx.compute(new ComputeParameters(false, pnd_Datx), Global.getDATX().subtract(1));                                                       //Natural: ASSIGN #DATX := *DATX - 1
                            pnd_Curr_Run_Date.setValueEdited(pnd_Datx,new ReportEditMask("YYYYMMDD"));                                                                    //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #CURR-RUN-DATE
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition((pnd_Curr_Run_Date.equals(pnd_Ws_Prev_Run_Date))))                                                                                  //Natural: IF ( #CURR-RUN-DATE EQ #WS-PREV-RUN-DATE )
                        {
                                                                                                                                                                          //Natural: PERFORM ERROR-JOB-RUN
                            sub_Error_Job_Run();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            if (condition(true)) return;                                                                                                                  //Natural: ESCAPE ROUTINE IMMEDIATE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ledger_Ext_1_Cntrct_Orgn_Cde.equals("NZ")))                                                                                         //Natural: IF CNTRCT-ORGN-CDE = 'NZ'
                    {
                        pnd_Interface_Date.setValueEdited(pnd_Input_Date,new ReportEditMask("YYYYMMDD"));                                                                 //Natural: MOVE EDITED #INPUT-DATE ( EM = YYYYMMDD ) TO #INTERFACE-DATE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Interface_Date.setValueEdited(pnd_Datx,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #INTERFACE-DATE
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ledger_Ext_1_Pymnt_Intrfce_Dte.greater(pnd_Interface_Date)))                                                                        //Natural: IF PYMNT-INTRFCE-DTE > #INTERFACE-DATE
                    {
                        getReports().write(0, pnd_Ledger_Ext_1_Cntrct_Orgn_Cde,"-",pnd_Ledger_Ext_1_Cntrct_Ppcn_Nbr,"-",pnd_Ledger_Ext_1_Pymnt_Intrfce_Dte,               //Natural: WRITE CNTRCT-ORGN-CDE '-' CNTRCT-PPCN-NBR '-' PYMNT-INTRFCE-DTE ' | INTERFACE DATE:' #INTERFACE-DATE ' ==> WRITING FUTURE FILE..'
                            " | INTERFACE DATE:",pnd_Interface_Date," ==> WRITING FUTURE FILE..");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        getWorkFiles().write(2, false, pnd_Ledger_Ext_1);                                                                                                 //Natural: WRITE WORK FILE 02 #LEDGER-EXT-1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().write(0, pnd_Ledger_Ext_1_Cntrct_Orgn_Cde,"-",pnd_Ledger_Ext_1_Cntrct_Ppcn_Nbr,"-",pnd_Ledger_Ext_1_Pymnt_Intrfce_Dte,               //Natural: WRITE CNTRCT-ORGN-CDE '-' CNTRCT-PPCN-NBR '-' PYMNT-INTRFCE-DTE ' | INTERFACE DATE:' #INTERFACE-DATE ' ==> WRITING CURRENT FILE..'
                            " | INTERFACE DATE:",pnd_Interface_Date," ==> WRITING CURRENT FILE..");
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Ledger_Ext_1_Pymnt_Intrfce_Dte.reset();                                                                                                       //Natural: RESET PYMNT-INTRFCE-DTE
                        getWorkFiles().write(3, false, pnd_Ledger_Ext_1);                                                                                                 //Natural: WRITE WORK FILE 03 #LEDGER-EXT-1
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK02_Exit:
                if (Global.isEscape()) return;
                pnd_Cntl_File_Pnd_Prev_Run_Date.setValue(pnd_Curr_Run_Date);                                                                                              //Natural: MOVE #CURR-RUN-DATE TO #PREV-RUN-DATE
                getWorkFiles().write(5, false, pnd_Cntl_File);                                                                                                            //Natural: WRITE WORK FILE 5 #CNTL-FILE
                //* ****************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-JOB-RUN
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Error_Job_Run() throws Exception                                                                                                                     //Natural: ERROR-JOB-RUN
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        getReports().write(0, "************      ERROR   ******************");                                                                                            //Natural: WRITE '************      ERROR   ******************'
        if (Global.isEscape()) return;
        getReports().write(0, "PREVIOUS RUN DATE IS SAME AS CURRENT DATE   ");                                                                                            //Natural: WRITE 'PREVIOUS RUN DATE IS SAME AS CURRENT DATE   '
        if (Global.isEscape()) return;
        getReports().write(0, "THIS STEP CAN NOT RUN TWICE IN THE SAME DATE");                                                                                            //Natural: WRITE 'THIS STEP CAN NOT RUN TWICE IN THE SAME DATE'
        if (Global.isEscape()) return;
        getReports().write(0, "PLEASE RUN FROM NEXT STEP OR CONTACT SUPPORT");                                                                                            //Natural: WRITE 'PLEASE RUN FROM NEXT STEP OR CONTACT SUPPORT'
        if (Global.isEscape()) return;
        getReports().write(0, "********************************************");                                                                                            //Natural: WRITE '********************************************'
        if (Global.isEscape()) return;
        pnd_Cntl_File_Pnd_Prev_Run_Date.setValue(pnd_Ws_Prev_Run_Date);                                                                                                   //Natural: MOVE #WS-PREV-RUN-DATE TO #PREV-RUN-DATE
        getWorkFiles().write(5, false, pnd_Cntl_File);                                                                                                                    //Natural: WRITE WORK FILE 5 #CNTL-FILE
        DbsUtil.terminate(92);  if (true) return;                                                                                                                         //Natural: TERMINATE 0092
        //*  ERROR-JOB-RUN
    }

    //
}
