/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:14 PM
**        * FROM NATURAL PROGRAM : Fcpp230b
************************************************************
**        * FILE NAME            : Fcpp230b.java
**        * CLASS NAME           : Fcpp230b
**        * INSTANCE NAME        : Fcpp230b
************************************************************
***********************************************************************
** PROGRAM:     FCPP230B                                             **
** SYSTEM:      CPS - CONSOLIDATED PAYMENT SYSTEM                    **
** DATE:        08-16-1999                                           **
** AUTHOR:      ALTHEA A. YOUNG                                      **
** DESCRIPTION: THIS PROGRAM PRODUCES A CANCEL / STOP NO REDRAW      **
**              REPORT.  ARMD COMMANDS WILL BE UTILIZED TO ASSIST IN **
**              FORMATTING THE OUTPUT.  THE #ORIGIN VARIABLE IN THE  **
**              TITLE SHOULD NOT BE MODIFIED WITHOUT CONSIDERING THE **
**              IMPACT ON ARMD.                                      **
**                                                                   **
***********************************************************************
** HISTORY:                                                          **
**                                                                   **
** 10/20/1999 : A. YOUNG - REVISED TO PRINT LEDGER NUMBER TOTALS.    **
**                       - INCLUDE RTB FLAG ON REPORT.               **
**                                                                   **
** 06/21/2001 : ROXAN    - ESCAPE TOP INSIDE START OF DATA WILL NOT  **
**                         STOW.  INSERT LOGIC TO FACILITATE ESCAPE  **
**                         TOP AFTER FIRST READ.                     **
* 4/2017    : JJG - PIN EXPANSION CHANGES                            **
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp230b extends BLNatBase
{
    // Data Areas
    private GdaFcpg230b gdaFcpg230b;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl190a ldaFcpl190a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Org_Cde_Break;
    private DbsField pnd_Detail_Recs;
    private DbsField pnd_Acct_Dte_Break;

    private DbsGroup pnd_Logical_Vars;
    private DbsField pnd_Logical_Vars_Pnd_Rtb;
    private DbsField pnd_Logical_Vars_Pnd_Nra;
    private DbsField pnd_Logical_Vars_Pnd_Canadian_Nra;
    private DbsField pnd_Logical_Vars_Pnd_Canadian_Fed;
    private DbsField pnd_Logical_Vars_Pnd_Ins_Deduction;
    private DbsField pnd_Logical_Vars_Pnd_Ltc_Deduction;
    private DbsField pnd_Logical_Vars_Pnd_Other_Deductn;
    private DbsField pnd_Logical_Vars_Pnd_Dividend;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_Prcss_Seq_Nbr__R_Field_1;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Num;
    private DbsField pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Num;
    private DbsField pnd_Prev_Seq_Num;

    private DbsGroup pnd_Title_Vars;
    private DbsField pnd_Title_Vars_Pnd_Origin;
    private DbsField pnd_Title_Vars_Pnd_Title;
    private DbsField pnd_Title_Vars_Pnd_Accounting_Date;

    private DbsGroup pnd_Install1_Vars;
    private DbsField pnd_Install1_Vars_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Install1_Vars_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Install1_Vars_Cntrct_Cref_Nbr;
    private DbsField pnd_Install1_Vars_Pymnt_Check_Dte;
    private DbsField pnd_Install1_Vars_Pymnt_Check_Nbr;
    private DbsField pnd_Install1_Vars_Pnd_Rtb_3;
    private DbsField pnd_Install1_Vars_Pnd_Soc_Sec_Tax_Id_Nbr;
    private DbsField pnd_Install1_Vars_Pnd_Ph_Name;
    private DbsField pnd_Install1_Vars_Pnd_Reason;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_N;

    private DbsGroup pnd_Install1_Vars__R_Field_2;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_A;

    private DbsGroup pnd_Install1_Vars__R_Field_3;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_A_Cc;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_A_Yy;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_A_Mm;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_A_Dd;

    private DbsGroup pnd_Install1_Vars__R_Field_4;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_Mm;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_Dd;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_Cc;
    private DbsField pnd_Install1_Vars_Pnd_Issue_Dte_Yy;
    private DbsField pnd_Install1_Vars_Pnd_Ins_Ded_Amt;
    private DbsField pnd_Install1_Vars_Pnd_Ltc_Ded_Amt;
    private DbsField pnd_Install1_Vars_Pnd_Other_Ded_Amt;

    private DbsGroup pnd_Inv_Acct_Vars;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Company;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Contract_Nbr;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Gross_Amt;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Ivc_Amt;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Cont_Amt_Unit_Qty_A;

    private DbsGroup pnd_Inv_Acct_Vars__R_Field_5;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Unit_Qty_Filler;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Unit_Qty;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Dvdnd_Amt_Unit_Val_A;

    private DbsGroup pnd_Inv_Acct_Vars__R_Field_6;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Unit_Val_Filler;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Unit_Value;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Fund_Desc_12;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Federal_Tax_Amt;
    private DbsField pnd_Inv_Acct_Vars_Pnd_State_Tax_Amt;
    private DbsField pnd_Inv_Acct_Vars_Pnd_Local_Tax_Amt;

    private DbsGroup pnd_Payee_Total_Vars;
    private DbsField pnd_Payee_Total_Vars_Pnd_Payee_Total_Gross_Amt;
    private DbsField pnd_Payee_Total_Vars_Pnd_Payee_Total_Ivc_Amt;
    private DbsField pnd_Payee_Total_Vars_Pnd_Payee_Total_Fed_Tax_Amt;
    private DbsField pnd_Payee_Total_Vars_Pnd_Payee_Total_Ste_Tax_Amt;
    private DbsField pnd_Payee_Total_Vars_Pnd_Payee_Total_Lcl_Tax_Amt;

    private DbsGroup pnd_Summary_Detail_Vars;
    private DbsField pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail;
    private DbsField pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Debit;
    private DbsField pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Credit;

    private DbsGroup pnd_Summary_Detail_Vars_Pnd_Stock_Mtuz_Pfx_Install_Data;
    private DbsField pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date;
    private DbsField pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_N;

    private DbsGroup pnd_Summary_Detail_Vars__R_Field_7;
    private DbsField pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_A;

    private DbsGroup pnd_Summary_Detail_Vars__R_Field_8;
    private DbsField pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_D;
    private DbsField pnd_Center_Length;
    private DbsField pnd_Center_Work;
    private DbsField pnd_Detail_Max;
    private DbsField pnd_State_Tax_Filler;
    private DbsField pnd_Local_Tax_Filler;
    private DbsField pnd_Check_Nbr_Filler;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_Ln;
    private DbsField pnd_Account;
    private DbsField pnd_Account_Max;
    private DbsField pnd_Ledger;
    private DbsField pnd_First_Read;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaFcpg230b = GdaFcpg230b.getInstance(getCallnatLevel());
        registerRecord(gdaFcpg230b);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);

        // Local Variables
        pnd_Org_Cde_Break = localVariables.newFieldInRecord("pnd_Org_Cde_Break", "#ORG-CDE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Detail_Recs = localVariables.newFieldInRecord("pnd_Detail_Recs", "#DETAIL-RECS", FieldType.BOOLEAN, 1);
        pnd_Acct_Dte_Break = localVariables.newFieldInRecord("pnd_Acct_Dte_Break", "#ACCT-DTE-BREAK", FieldType.BOOLEAN, 1);

        pnd_Logical_Vars = localVariables.newGroupInRecord("pnd_Logical_Vars", "#LOGICAL-VARS");
        pnd_Logical_Vars_Pnd_Rtb = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Rtb", "#RTB", FieldType.BOOLEAN, 1);
        pnd_Logical_Vars_Pnd_Nra = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Nra", "#NRA", FieldType.BOOLEAN, 1);
        pnd_Logical_Vars_Pnd_Canadian_Nra = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Canadian_Nra", "#CANADIAN-NRA", FieldType.BOOLEAN, 
            1);
        pnd_Logical_Vars_Pnd_Canadian_Fed = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Canadian_Fed", "#CANADIAN-FED", FieldType.BOOLEAN, 
            1);
        pnd_Logical_Vars_Pnd_Ins_Deduction = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Ins_Deduction", "#INS-DEDUCTION", FieldType.BOOLEAN, 
            1);
        pnd_Logical_Vars_Pnd_Ltc_Deduction = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Ltc_Deduction", "#LTC-DEDUCTION", FieldType.BOOLEAN, 
            1);
        pnd_Logical_Vars_Pnd_Other_Deductn = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Other_Deductn", "#OTHER-DEDUCTN", FieldType.BOOLEAN, 
            1);
        pnd_Logical_Vars_Pnd_Dividend = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Dividend", "#DIVIDEND", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Prcss_Seq_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Prcss_Seq_Nbr", "#PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_Prcss_Seq_Nbr__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_Prcss_Seq_Nbr__R_Field_1", "REDEFINE", pnd_Pymnt_Prcss_Seq_Nbr);
        pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Num = pnd_Pymnt_Prcss_Seq_Nbr__R_Field_1.newFieldInGroup("pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Num", "#CURR-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Num = pnd_Pymnt_Prcss_Seq_Nbr__R_Field_1.newFieldInGroup("pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Instmnt_Num", "#INSTMNT-NUM", 
            FieldType.NUMERIC, 2);
        pnd_Prev_Seq_Num = localVariables.newFieldInRecord("pnd_Prev_Seq_Num", "#PREV-SEQ-NUM", FieldType.NUMERIC, 7);

        pnd_Title_Vars = localVariables.newGroupInRecord("pnd_Title_Vars", "#TITLE-VARS");
        pnd_Title_Vars_Pnd_Origin = pnd_Title_Vars.newFieldInGroup("pnd_Title_Vars_Pnd_Origin", "#ORIGIN", FieldType.STRING, 2);
        pnd_Title_Vars_Pnd_Title = pnd_Title_Vars.newFieldInGroup("pnd_Title_Vars_Pnd_Title", "#TITLE", FieldType.STRING, 41);
        pnd_Title_Vars_Pnd_Accounting_Date = pnd_Title_Vars.newFieldInGroup("pnd_Title_Vars_Pnd_Accounting_Date", "#ACCOUNTING-DATE", FieldType.DATE);

        pnd_Install1_Vars = localVariables.newGroupInRecord("pnd_Install1_Vars", "#INSTALL1-VARS");
        pnd_Install1_Vars_Cntrct_Unq_Id_Nbr = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Install1_Vars_Cntrct_Ppcn_Nbr = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Install1_Vars_Cntrct_Cref_Nbr = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10);
        pnd_Install1_Vars_Pymnt_Check_Dte = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Install1_Vars_Pymnt_Check_Nbr = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Install1_Vars_Pnd_Rtb_3 = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pnd_Rtb_3", "#RTB-3", FieldType.STRING, 3);
        pnd_Install1_Vars_Pnd_Soc_Sec_Tax_Id_Nbr = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pnd_Soc_Sec_Tax_Id_Nbr", "#SOC-SEC-TAX-ID-NBR", 
            FieldType.STRING, 11);
        pnd_Install1_Vars_Pnd_Ph_Name = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pnd_Ph_Name", "#PH-NAME", FieldType.STRING, 35);
        pnd_Install1_Vars_Pnd_Reason = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pnd_Reason", "#REASON", FieldType.STRING, 14);
        pnd_Install1_Vars_Pnd_Issue_Dte_N = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_N", "#ISSUE-DTE-N", FieldType.NUMERIC, 
            8);

        pnd_Install1_Vars__R_Field_2 = pnd_Install1_Vars.newGroupInGroup("pnd_Install1_Vars__R_Field_2", "REDEFINE", pnd_Install1_Vars_Pnd_Issue_Dte_N);
        pnd_Install1_Vars_Pnd_Issue_Dte_A = pnd_Install1_Vars__R_Field_2.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_A", "#ISSUE-DTE-A", FieldType.STRING, 
            8);

        pnd_Install1_Vars__R_Field_3 = pnd_Install1_Vars__R_Field_2.newGroupInGroup("pnd_Install1_Vars__R_Field_3", "REDEFINE", pnd_Install1_Vars_Pnd_Issue_Dte_A);
        pnd_Install1_Vars_Pnd_Issue_Dte_A_Cc = pnd_Install1_Vars__R_Field_3.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_A_Cc", "#ISSUE-DTE-A-CC", 
            FieldType.STRING, 2);
        pnd_Install1_Vars_Pnd_Issue_Dte_A_Yy = pnd_Install1_Vars__R_Field_3.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_A_Yy", "#ISSUE-DTE-A-YY", 
            FieldType.STRING, 2);
        pnd_Install1_Vars_Pnd_Issue_Dte_A_Mm = pnd_Install1_Vars__R_Field_3.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_A_Mm", "#ISSUE-DTE-A-MM", 
            FieldType.STRING, 2);
        pnd_Install1_Vars_Pnd_Issue_Dte_A_Dd = pnd_Install1_Vars__R_Field_3.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_A_Dd", "#ISSUE-DTE-A-DD", 
            FieldType.STRING, 2);

        pnd_Install1_Vars__R_Field_4 = pnd_Install1_Vars__R_Field_2.newGroupInGroup("pnd_Install1_Vars__R_Field_4", "REDEFINE", pnd_Install1_Vars_Pnd_Issue_Dte_A);
        pnd_Install1_Vars_Pnd_Issue_Dte_Mm = pnd_Install1_Vars__R_Field_4.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_Mm", "#ISSUE-DTE-MM", FieldType.STRING, 
            2);
        pnd_Install1_Vars_Pnd_Issue_Dte_Dd = pnd_Install1_Vars__R_Field_4.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_Dd", "#ISSUE-DTE-DD", FieldType.STRING, 
            2);
        pnd_Install1_Vars_Pnd_Issue_Dte_Cc = pnd_Install1_Vars__R_Field_4.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_Cc", "#ISSUE-DTE-CC", FieldType.STRING, 
            2);
        pnd_Install1_Vars_Pnd_Issue_Dte_Yy = pnd_Install1_Vars__R_Field_4.newFieldInGroup("pnd_Install1_Vars_Pnd_Issue_Dte_Yy", "#ISSUE-DTE-YY", FieldType.STRING, 
            2);
        pnd_Install1_Vars_Pnd_Ins_Ded_Amt = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pnd_Ins_Ded_Amt", "#INS-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Install1_Vars_Pnd_Ltc_Ded_Amt = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pnd_Ltc_Ded_Amt", "#LTC-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Install1_Vars_Pnd_Other_Ded_Amt = pnd_Install1_Vars.newFieldInGroup("pnd_Install1_Vars_Pnd_Other_Ded_Amt", "#OTHER-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Inv_Acct_Vars = localVariables.newGroupArrayInRecord("pnd_Inv_Acct_Vars", "#INV-ACCT-VARS", new DbsArrayController(1, 100));
        pnd_Inv_Acct_Vars_Pnd_Company = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Company", "#COMPANY", FieldType.STRING, 13);
        pnd_Inv_Acct_Vars_Pnd_Contract_Nbr = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Contract_Nbr", "#CONTRACT-NBR", FieldType.STRING, 
            10);
        pnd_Inv_Acct_Vars_Pnd_Gross_Amt = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Inv_Acct_Vars_Pnd_Ivc_Amt = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Ivc_Amt", "#IVC-AMT", FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Inv_Acct_Vars_Pnd_Cont_Amt_Unit_Qty_A = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Cont_Amt_Unit_Qty_A", "#CONT-AMT-UNIT-QTY-A", 
            FieldType.STRING, 11);

        pnd_Inv_Acct_Vars__R_Field_5 = pnd_Inv_Acct_Vars.newGroupInGroup("pnd_Inv_Acct_Vars__R_Field_5", "REDEFINE", pnd_Inv_Acct_Vars_Pnd_Cont_Amt_Unit_Qty_A);
        pnd_Inv_Acct_Vars_Pnd_Unit_Qty_Filler = pnd_Inv_Acct_Vars__R_Field_5.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Unit_Qty_Filler", "#UNIT-QTY-FILLER", 
            FieldType.STRING, 1);
        pnd_Inv_Acct_Vars_Pnd_Unit_Qty = pnd_Inv_Acct_Vars__R_Field_5.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Unit_Qty", "#UNIT-QTY", FieldType.STRING, 
            10);
        pnd_Inv_Acct_Vars_Pnd_Dvdnd_Amt_Unit_Val_A = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Dvdnd_Amt_Unit_Val_A", "#DVDND-AMT-UNIT-VAL-A", 
            FieldType.STRING, 12);

        pnd_Inv_Acct_Vars__R_Field_6 = pnd_Inv_Acct_Vars.newGroupInGroup("pnd_Inv_Acct_Vars__R_Field_6", "REDEFINE", pnd_Inv_Acct_Vars_Pnd_Dvdnd_Amt_Unit_Val_A);
        pnd_Inv_Acct_Vars_Pnd_Unit_Val_Filler = pnd_Inv_Acct_Vars__R_Field_6.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Unit_Val_Filler", "#UNIT-VAL-FILLER", 
            FieldType.STRING, 2);
        pnd_Inv_Acct_Vars_Pnd_Unit_Value = pnd_Inv_Acct_Vars__R_Field_6.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Unit_Value", "#UNIT-VALUE", FieldType.STRING, 
            10);
        pnd_Inv_Acct_Vars_Pnd_Fund_Desc_12 = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Fund_Desc_12", "#FUND-DESC-12", FieldType.STRING, 
            12);
        pnd_Inv_Acct_Vars_Pnd_Federal_Tax_Amt = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Federal_Tax_Amt", "#FEDERAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Inv_Acct_Vars_Pnd_State_Tax_Amt = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_State_Tax_Amt", "#STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);
        pnd_Inv_Acct_Vars_Pnd_Local_Tax_Amt = pnd_Inv_Acct_Vars.newFieldInGroup("pnd_Inv_Acct_Vars_Pnd_Local_Tax_Amt", "#LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            7, 2);

        pnd_Payee_Total_Vars = localVariables.newGroupInRecord("pnd_Payee_Total_Vars", "#PAYEE-TOTAL-VARS");
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Gross_Amt = pnd_Payee_Total_Vars.newFieldInGroup("pnd_Payee_Total_Vars_Pnd_Payee_Total_Gross_Amt", "#PAYEE-TOTAL-GROSS-AMT", 
            FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Ivc_Amt = pnd_Payee_Total_Vars.newFieldInGroup("pnd_Payee_Total_Vars_Pnd_Payee_Total_Ivc_Amt", "#PAYEE-TOTAL-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Fed_Tax_Amt = pnd_Payee_Total_Vars.newFieldInGroup("pnd_Payee_Total_Vars_Pnd_Payee_Total_Fed_Tax_Amt", "#PAYEE-TOTAL-FED-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Ste_Tax_Amt = pnd_Payee_Total_Vars.newFieldInGroup("pnd_Payee_Total_Vars_Pnd_Payee_Total_Ste_Tax_Amt", "#PAYEE-TOTAL-STE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2);
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Lcl_Tax_Amt = pnd_Payee_Total_Vars.newFieldInGroup("pnd_Payee_Total_Vars_Pnd_Payee_Total_Lcl_Tax_Amt", "#PAYEE-TOTAL-LCL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 7, 2);

        pnd_Summary_Detail_Vars = localVariables.newGroupInRecord("pnd_Summary_Detail_Vars", "#SUMMARY-DETAIL-VARS");
        pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail = pnd_Summary_Detail_Vars.newFieldInGroup("pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail", 
            "#SUMMARY-DESC-NBR-DETAIL", FieldType.STRING, 66);
        pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Debit = pnd_Summary_Detail_Vars.newFieldInGroup("pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Debit", "#TIAA-TOTAL-DEBIT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Credit = pnd_Summary_Detail_Vars.newFieldInGroup("pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Credit", "#TIAA-TOTAL-CREDIT", 
            FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Summary_Detail_Vars_Pnd_Stock_Mtuz_Pfx_Install_Data = pnd_Summary_Detail_Vars.newGroupInGroup("pnd_Summary_Detail_Vars_Pnd_Stock_Mtuz_Pfx_Install_Data", 
            "#STOCK-MTUZ-PFX-INSTALL-DATA");
        pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date = pnd_Summary_Detail_Vars_Pnd_Stock_Mtuz_Pfx_Install_Data.newFieldArrayInGroup("pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date", 
            "#MTUZ-INSTALL-DATE", FieldType.STRING, 8, new DbsArrayController(3, 22));
        pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_N = pnd_Summary_Detail_Vars_Pnd_Stock_Mtuz_Pfx_Install_Data.newFieldInGroup("pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_N", 
            "#MTUZ-INSTALL-DATE-N", FieldType.NUMERIC, 8);

        pnd_Summary_Detail_Vars__R_Field_7 = pnd_Summary_Detail_Vars_Pnd_Stock_Mtuz_Pfx_Install_Data.newGroupInGroup("pnd_Summary_Detail_Vars__R_Field_7", 
            "REDEFINE", pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_N);
        pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_A = pnd_Summary_Detail_Vars__R_Field_7.newFieldInGroup("pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_A", 
            "#MTUZ-INSTALL-DATE-A", FieldType.STRING, 8);

        pnd_Summary_Detail_Vars__R_Field_8 = pnd_Summary_Detail_Vars_Pnd_Stock_Mtuz_Pfx_Install_Data.newGroupInGroup("pnd_Summary_Detail_Vars__R_Field_8", 
            "REDEFINE", pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_N);
        pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_D = pnd_Summary_Detail_Vars__R_Field_8.newFieldInGroup("pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_D", 
            "#MTUZ-INSTALL-DATE-D", FieldType.DATE);
        pnd_Center_Length = localVariables.newFieldInRecord("pnd_Center_Length", "#CENTER-LENGTH", FieldType.NUMERIC, 3);
        pnd_Center_Work = localVariables.newFieldInRecord("pnd_Center_Work", "#CENTER-WORK", FieldType.STRING, 66);
        pnd_Detail_Max = localVariables.newFieldInRecord("pnd_Detail_Max", "#DETAIL-MAX", FieldType.NUMERIC, 3);
        pnd_State_Tax_Filler = localVariables.newFieldInRecord("pnd_State_Tax_Filler", "#STATE-TAX-FILLER", FieldType.STRING, 9);
        pnd_Local_Tax_Filler = localVariables.newFieldInRecord("pnd_Local_Tax_Filler", "#LOCAL-TAX-FILLER", FieldType.STRING, 9);
        pnd_Check_Nbr_Filler = localVariables.newFieldInRecord("pnd_Check_Nbr_Filler", "#CHECK-NBR-FILLER", FieldType.STRING, 7);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 3);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 3);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.NUMERIC, 3);
        pnd_Ln = localVariables.newFieldInRecord("pnd_Ln", "#LN", FieldType.NUMERIC, 3);
        pnd_Account = localVariables.newFieldInRecord("pnd_Account", "#ACCOUNT", FieldType.NUMERIC, 3);
        pnd_Account_Max = localVariables.newFieldInRecord("pnd_Account_Max", "#ACCOUNT-MAX", FieldType.NUMERIC, 3);
        pnd_Ledger = localVariables.newFieldInRecord("pnd_Ledger", "#LEDGER", FieldType.NUMERIC, 3);
        pnd_First_Read = localVariables.newFieldInRecord("pnd_First_Read", "#FIRST-READ", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_Org_Cde_Break.setInitialValue(true);
        pnd_Detail_Recs.setInitialValue(false);
        pnd_Acct_Dte_Break.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Rtb.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Nra.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Canadian_Nra.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Canadian_Fed.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Ins_Deduction.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Ltc_Deduction.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Other_Deductn.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Dividend.setInitialValue(false);
        pnd_Title_Vars_Pnd_Title.setInitialValue("Daily Cancel/Stop with NO Redraw Payments");
        pnd_Account_Max.setInitialValue(1);
        pnd_First_Read.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp230b() throws Exception
    {
        super("Fcpp230b");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP230B", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60;//Natural: FORMAT ( 01 ) LS = 133 PS = 60
        //*                                                                                                                                                               //Natural: ON ERROR
        //*  IMPORTANT: THE DISPLACEMENT OF #ORIGIN SHOULD NOT BE MODIFIED.  ARMD
        //*             WILL BE AFFECTED.
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATX ( EM = MM/DD/YY ) 1X '-' 1X *TIMX ( EM = HH:II' 'AP ) 30T #ORIGIN 51T 'CONSOLIDATED PAYMENT SYSTEM' 119T 'PAGE:' 1X *PAGE-NUMBER ( 01 ) ( AD = L ) / *INIT-USER 1X '-' 1X *PROGRAM 45T #TITLE / *LIBRARY-ID 50T 'For Accounting Date' 1X #ACCOUNTING-DATE /
        //*  READ CANCEL / STOP NO REDRAW WORK FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 RECORD #RPT-EXT
        while (condition(getWorkFiles().read(1, ldaFcpl190a.getPnd_Rpt_Ext())))
        {
            CheckAtStartofData406();

            //*  USE THIS CODE INSTEAD /* 06/21/2001                                                                                                                      //Natural: AT START OF DATA
            if (condition(pnd_First_Read.getBoolean()))                                                                                                                   //Natural: IF #FIRST-READ
            {
                pnd_First_Read.setValue(false);                                                                                                                           //Natural: ASSIGN #FIRST-READ := FALSE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("H'0000'")))                                                                  //Natural: IF #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE EQ H'0000'
            {
                if (condition(pnd_Org_Cde_Break.getBoolean()))                                                                                                            //Natural: IF #ORG-CDE-BREAK
                {
                                                                                                                                                                          //Natural: PERFORM NO-PAYMENT-PROCESS
                    sub_No_Payment_Process();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-INV-ACCT-DETAILS
                    sub_Print_Inv_Acct_Details();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PRINT-PAYEE-TOTALS
                    sub_Print_Payee_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  10-20-1999
                    if (condition(gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue("*","*").notEquals(getZero())))                                             //Natural: IF SUMMARY-TOTAL-DATA.#LEDGER-AMT ( *,* ) NE 0
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-LEDGERS
                        sub_Print_Summary_Ledgers();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Title_Vars_Pnd_Title.resetInitial();                                                                                                          //Natural: RESET INITIAL #TITLE
                        gdaFcpg230b.getSummary_Total_Data_Pnd_Inv_Acct().getValue("*").reset();                                                                           //Natural: RESET SUMMARY-TOTAL-DATA.#INV-ACCT ( * ) SUMMARY-TOTAL-DATA.#LEDGER-AMT ( *,* )
                        gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue("*","*").reset();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-REPORT-PROCESSING
                sub_End_Report_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Title_Vars_Pnd_Origin.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());                                                                         //Natural: ASSIGN #ORIGIN := #RPT-EXT.CNTRCT-ORGN-CDE
                pnd_Org_Cde_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #ORG-CDE-BREAK := TRUE
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Title_Vars_Pnd_Accounting_Date.notEquals(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte())))                                                    //Natural: IF #ACCOUNTING-DATE NE #RPT-EXT.PYMNT-ACCTG-DTE
            {
                if (condition(pnd_Detail_Recs.getBoolean()))                                                                                                              //Natural: IF #DETAIL-RECS
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-INV-ACCT-DETAILS
                    sub_Print_Inv_Acct_Details();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PRINT-PAYEE-TOTALS
                    sub_Print_Payee_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM END-REPORT-PROCESSING
                    sub_End_Report_Processing();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Acct_Dte_Break.setValue(true);                                                                                                                        //Natural: ASSIGN #ACCT-DTE-BREAK := TRUE
            }                                                                                                                                                             //Natural: END-IF
            pnd_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr());                                                                           //Natural: ASSIGN #PYMNT-PRCSS-SEQ-NBR := #RPT-EXT.PYMNT-PRCSS-SEQ-NBR
            //*                                                                                                                                                           //Natural: DECIDE FOR EVERY CONDITION
            short decideConditionsMet464 = 0;                                                                                                                             //Natural: WHEN #ORG-CDE-BREAK OR #ACCT-DTE-BREAK
            if (condition(pnd_Org_Cde_Break.getBoolean() || pnd_Acct_Dte_Break.getBoolean()))
            {
                decideConditionsMet464++;
                pnd_Title_Vars_Pnd_Accounting_Date.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte());                                                                //Natural: MOVE #RPT-EXT.PYMNT-ACCTG-DTE TO #ACCOUNTING-DATE
                getReports().newPage(new ReportSpecification(1));                                                                                                         //Natural: NEWPAGE ( 01 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN #PREV-SEQ-NUM NE #CURR-SEQ-NUM
            if (condition(pnd_Prev_Seq_Num.notEquals(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Num)))
            {
                decideConditionsMet464++;
                if (condition(pnd_Detail_Recs.getBoolean()))                                                                                                              //Natural: IF #DETAIL-RECS
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-INV-ACCT-DETAILS
                    sub_Print_Inv_Acct_Details();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PRINT-PAYEE-TOTALS
                    sub_Print_Payee_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet464 > 0))
            {
                if (condition(pnd_Org_Cde_Break.getBoolean()))                                                                                                            //Natural: IF #ORG-CDE-BREAK
                {
                    pnd_Org_Cde_Break.setValue(false);                                                                                                                    //Natural: ASSIGN #ORG-CDE-BREAK := FALSE
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Acct_Dte_Break.getBoolean()))                                                                                                           //Natural: IF #ACCT-DTE-BREAK
                {
                    pnd_Acct_Dte_Break.setValue(false);                                                                                                                   //Natural: ASSIGN #ACCT-DTE-BREAK := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet464 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM POPULATE-DETAIL-LINES
            sub_Populate_Detail_Lines();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Prev_Seq_Num.setValue(pnd_Pymnt_Prcss_Seq_Nbr_Pnd_Curr_Seq_Num);                                                                                          //Natural: ASSIGN #PREV-SEQ-NUM := #CURR-SEQ-NUM
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(pnd_Detail_Recs.getBoolean()))                                                                                                                  //Natural: IF #DETAIL-RECS
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-INV-ACCT-DETAILS
                sub_Print_Inv_Acct_Details();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-PAYEE-TOTALS
                sub_Print_Payee_Totals();
                if (condition(Global.isEscape())) {return;}
                //*  10-20-1999
                if (condition(gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue("*","*").notEquals(getZero())))                                                 //Natural: IF SUMMARY-TOTAL-DATA.#LEDGER-AMT ( *,* ) NE 0
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-LEDGERS
                    sub_Print_Summary_Ledgers();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM END-REPORT-PROCESSING
                sub_End_Report_Processing();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Org_Cde_Break.getBoolean()))                                                                                                            //Natural: IF #ORG-CDE-BREAK
                {
                                                                                                                                                                          //Natural: PERFORM NO-PAYMENT-PROCESS
                    sub_No_Payment_Process();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM END-REPORT-PROCESSING
                    sub_End_Report_Processing();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* *-----------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-DETAIL-LINES
        //* *-----------------------------------**
        //* *----------**
        //* *------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-INV-ACCT-DETAILS
        //* *----------**
        //* *--------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PAYEE-TOTALS
        //* *----------**
        //* *--------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NO-PAYMENT-PROCESS
        //* *----------**
        //* *-----------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: END-REPORT-PROCESSING
        //* *----------**
        //* *-----------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY-LEDGERS
        //* *-----------------------------------**
        //* *----------**
        //*  L E V E L   2   S U B - R O U T I N E S
        //* *----------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
        //*       EXAMINE FULL #PH-NAME FOR FULL ' ' GIVING NUMBER #CENTER-LENGTH
        //*       #CENTER-LENGTH := (#CENTER-LENGTH / 2)
        //*       MOVE ALL '+' TO #CENTER-WORK UNTIL #CENTER-LENGTH
        //*       COMPRESS #CENTER-WORK #PH-NAME INTO #PH-NAME LEAVING NO SPACE
        //*       EXAMINE FULL #PH-NAME FOR FULL '*' REPLACE ' '
        //*       EXAMINE FULL #PH-NAME FOR FULL '+' REPLACE ' '
        //* *----------**
        //* *-------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-INSTALLMENT-DATA
        //* *-------------------------------------**
        //* *----------**
        //* *------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-PAYEE-TOTALS
        //* *----------**
        //* *--------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-SUMMARY-TOTALS
        //* *----------**
        //*  L E V E L   3   S U B - R O U T I N E S
        //* *------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SUMMARY-INITIALIZATION
        //*  CANADIAN FEDERAL HERE OR ABOVE DEPENDING ON CRITERIOR.
        //*  DEDUCTIONS
        //* *----------**
        //* *--------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-GENERAL-DEBIT
        //* *----------**
        //* *-------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-CREDIT
        //* *----------**
        //* *----------------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TIAA-DEBIT-INV-ACCT-CREDIT
        //* *----------**
        //* *-----------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-VARIABLES
        //* *----------**
        //* *--------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-PAYEE-HEADER
        //*      076T #REASON
        //* *----------**
        //*  L E V E L   4   S U B - R O U T I N E S
        //* *----------------------------------**
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STATE-TAX-PROCESSING
        //* *----------**
    }
    private void sub_Populate_Detail_Lines() throws Exception                                                                                                             //Natural: POPULATE-DETAIL-LINES
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Detail_Recs.setValue(true);                                                                                                                                   //Natural: ASSIGN #DETAIL-RECS := TRUE
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 #RPT-EXT.C-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); pnd_I.nadd(1))
        {
            pnd_Ln.nadd(1);                                                                                                                                               //Natural: ASSIGN #LN := #LN + 1
            //*  FIRST INSTALLMENT
            if (condition(pnd_Ln.equals(1)))                                                                                                                              //Natural: IF #LN = 1
            {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
                sub_Initialization();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  10-20-1999  EACH INSTALLMENT
            if (condition(pnd_I.equals(1)))                                                                                                                               //Natural: IF #I = 1
            {
                                                                                                                                                                          //Natural: PERFORM OBTAIN-INSTALLMENT-DATA
                sub_Obtain_Installment_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Cde().getValue(pnd_I));                                         //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := #RPT-EXT.INV-ACCT-CDE ( #I )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Valuat_Period().getValue(pnd_I));                       //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := #RPT-EXT.INV-ACCT-VALUAT-PERIOD ( #I )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            pnd_Inv_Acct_Vars_Pnd_Fund_Desc_12.getValue(pnd_Ln).setValue(DbsUtil.compress(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(), pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_3())); //Natural: COMPRESS #FUND-PDA.#INV-ACCT-DESC-8 #FUND-PDA.#VALUAT-DESC-3 INTO #FUND-DESC-12 ( #LN )
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))                                                                                     //Natural: IF #FUND-PDA.#COMPANY-CDE = 'T'
            {
                pnd_Inv_Acct_Vars_Pnd_Company.getValue(pnd_Ln).setValue("TIAA", MoveOption.RightJustified);                                                               //Natural: MOVE RIGHT 'TIAA' TO #COMPANY ( #LN )
                pnd_Inv_Acct_Vars_Pnd_Contract_Nbr.getValue(pnd_Ln).setValue(pnd_Install1_Vars_Cntrct_Ppcn_Nbr);                                                          //Natural: ASSIGN #CONTRACT-NBR ( #LN ) := #INSTALL1-VARS.CNTRCT-PPCN-NBR
                //*    #IVC-AMT      (#LN) := #RPT-EXT.INV-ACCT-IVC-AMT (#I)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Inv_Acct_Vars_Pnd_Company.getValue(pnd_Ln).setValue("CREF", MoveOption.RightJustified);                                                               //Natural: MOVE RIGHT 'CREF' TO #COMPANY ( #LN )
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("NZ")))                                                                                 //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'NZ'
                {
                    pnd_Inv_Acct_Vars_Pnd_Contract_Nbr.getValue(pnd_Ln).setValue(pnd_Install1_Vars_Cntrct_Ppcn_Nbr);                                                      //Natural: ASSIGN #CONTRACT-NBR ( #LN ) := #INSTALL1-VARS.CNTRCT-PPCN-NBR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Inv_Acct_Vars_Pnd_Contract_Nbr.getValue(pnd_Ln).setValue(pnd_Install1_Vars_Cntrct_Cref_Nbr);                                                      //Natural: ASSIGN #CONTRACT-NBR ( #LN ) := #INSTALL1-VARS.CNTRCT-CREF-NBR
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Inv_Acct_Vars_Pnd_Ivc_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_I));                                       //Natural: ASSIGN #IVC-AMT ( #LN ) := #RPT-EXT.INV-ACCT-IVC-AMT ( #I )
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Inv_Acct_Vars_Pnd_Cont_Amt_Unit_Qty_A.getValue(pnd_Ln).setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I),new             //Natural: MOVE EDITED #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) TO #CONT-AMT-UNIT-QTY-A ( #LN )
                    ReportEditMask("ZZZ,ZZ9.99"));
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I).notEquals(getZero())))                                                      //Natural: IF #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) NE 0
                {
                    pnd_Inv_Acct_Vars_Pnd_Dvdnd_Amt_Unit_Val_A.getValue(pnd_Ln).setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I),new        //Natural: MOVE EDITED #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) ( EM = ZZZ,ZZ9.99 ) TO #DVDND-AMT-UNIT-VAL-A ( #LN )
                        ReportEditMask("ZZZ,ZZ9.99"));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Inv_Acct_Vars_Pnd_Dvdnd_Amt_Unit_Val_A.getValue(pnd_Ln).setValue("      0.00  ");                                                                 //Natural: MOVE '      0.00  ' TO #DVDND-AMT-UNIT-VAL-A ( #LN )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln).compute(new ComputeParameters(false, pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln)),                  //Natural: ASSIGN #GROSS-AMT ( #LN ) := #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) + #RPT-EXT.INV-ACCT-DVDND-AMT ( #I )
                    ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I)));
                //*   WHAT ABOUT THESE ???      #RPT-EXT.INV-ACCT-DPI-AMT        (#I)  +
                //*                             #RPT-EXT.INV-ACCT-DCI-AMT        (#I)
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("NZ")))                                                                                 //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'NZ'
                {
                    pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                           //Natural: ASSIGN #GROSS-AMT ( #LN ) := #RPT-EXT.INV-ACCT-SETTL-AMT ( #I )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Inv_Acct_Vars_Pnd_Unit_Qty.getValue(pnd_Ln).setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Qty().getValue(pnd_I),new                     //Natural: MOVE EDITED #RPT-EXT.INV-ACCT-UNIT-QTY ( #I ) ( EM = ZZ,ZZZ.999 ) TO #UNIT-QTY ( #LN )
                        ReportEditMask("ZZ,ZZZ.999"));
                    pnd_Inv_Acct_Vars_Pnd_Unit_Value.getValue(pnd_Ln).setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Value().getValue(pnd_I),new                 //Natural: MOVE EDITED #RPT-EXT.INV-ACCT-UNIT-VALUE ( #I ) ( EM = Z,ZZ9.9999 ) TO #UNIT-VALUE ( #LN )
                        ReportEditMask("Z,ZZ9.9999"));
                    pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln).compute(new ComputeParameters(true, pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln)),               //Natural: MULTIPLY ROUNDED #RPT-EXT.INV-ACCT-UNIT-QTY ( #I ) BY #RPT-EXT.INV-ACCT-UNIT-VALUE ( #I ) GIVING #GROSS-AMT ( #LN )
                        ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Qty().getValue(pnd_I).multiply(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Unit_Value().getValue(pnd_I)));
                    //*   WHAT ABOUT THESE ???      #RPT-EXT.INV-ACCT-DPI-AMT        (#I)
                    //*                             #RPT-EXT.INV-ACCT-DCI-AMT        (#I)
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I).notEquals(getZero())))                                                       //Natural: IF #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) NE 0
            {
                pnd_Inv_Acct_Vars_Pnd_Federal_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                      //Natural: ASSIGN #FEDERAL-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I).notEquals(getZero())))                                                      //Natural: IF #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) NE 0
            {
                pnd_Inv_Acct_Vars_Pnd_State_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                       //Natural: ASSIGN #STATE-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I).notEquals(getZero())))                                                      //Natural: IF #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) NE 0
            {
                pnd_Inv_Acct_Vars_Pnd_Local_Tax_Amt.getValue(pnd_Ln).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                       //Natural: ASSIGN #LOCAL-TAX-AMT ( #LN ) := #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CALCULATE-PAYEE-TOTALS
            sub_Calculate_Payee_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM CALCULATE-SUMMARY-TOTALS
            sub_Calculate_Summary_Totals();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *----------**
    }
    private void sub_Print_Inv_Acct_Details() throws Exception                                                                                                            //Natural: PRINT-INV-ACCT-DETAILS
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------**
        if (condition(pnd_Detail_Recs.getBoolean()))                                                                                                                      //Natural: IF #DETAIL-RECS
        {
            pnd_Detail_Max.compute(new ComputeParameters(false, pnd_Detail_Max), pnd_Ln.add(6));                                                                          //Natural: ASSIGN #DETAIL-MAX := #LN + 6
            if (condition(getReports().getAstLinesLeft(1).less(pnd_Detail_Max)))                                                                                          //Natural: NEWPAGE ( 01 ) IF LESS THAN #DETAIL-MAX LINES LEFT
            {
                getReports().newPage(1);
                if (condition(Global.isEscape())){return;}
            }
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 01 ) 2
                                                                                                                                                                          //Natural: PERFORM PRINT-PAYEE-HEADER
            sub_Print_Payee_Header();
            if (condition(Global.isEscape())) {return;}
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 01 ) 1
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 #LN
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Ln)); pnd_I.nadd(1))
            {
                if (condition(pnd_I.equals(1)))                                                                                                                           //Natural: IF #I = 1
                {
                    Global.format("IS=OFF");                                                                                                                              //Natural: SUSPEND IDENTICAL ( 01 )
                }                                                                                                                                                         //Natural: END-IF
                getReports().display(1, "PIN/COMPANY//",                                                                                                                  //Natural: DISPLAY ( 01 ) 'PIN/COMPANY//' #COMPANY ( #I ) ( IS = ON ) 016T 'SSN# (TAX#)/CONTRACT #/' #CONTRACT-NBR ( #I ) ( IS = ON ) 029T '/ISSUE DT//ORIG DATE' #ISSUE-DTE-A ( IS = ON ) 040T 'PARTICIPANT NAME/GROSS/AMOUNT/' #GROSS-AMT ( #I ) 058T '/CONT AMT/UNITS/IVC' #CONT-AMT-UNIT-QTY-A ( #I ) 071T 'REASON/DIVIDEND/UNIT VALUE/' #DVDND-AMT-UNIT-VAL-A ( #I ) 085T '/INV ACCT//FEDERAL TAX' #FUND-DESC-12 ( #I ) 099T '///STATE TAX' #STATE-TAX-FILLER 110T '///LOCAL TAX' #LOCAL-TAX-FILLER 121T '///CHECK #' #CHECK-NBR-FILLER ( IS = ON ZP = OFF )
                		pnd_Inv_Acct_Vars_Pnd_Company.getValue(pnd_I), new IdenticalSuppress(true),new TabSetting(16),"SSN# (TAX#)/CONTRACT #/",
                		pnd_Inv_Acct_Vars_Pnd_Contract_Nbr.getValue(pnd_I), new IdenticalSuppress(true),new TabSetting(29),"/ISSUE DT//ORIG DATE",
                		pnd_Install1_Vars_Pnd_Issue_Dte_A, new IdenticalSuppress(true),new TabSetting(40),"PARTICIPANT NAME/GROSS/AMOUNT/",
                		pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_I),new TabSetting(58),"/CONT AMT/UNITS/IVC",
                		pnd_Inv_Acct_Vars_Pnd_Cont_Amt_Unit_Qty_A.getValue(pnd_I),new TabSetting(71),"REASON/DIVIDEND/UNIT VALUE/",
                		pnd_Inv_Acct_Vars_Pnd_Dvdnd_Amt_Unit_Val_A.getValue(pnd_I),new TabSetting(85),"/INV ACCT//FEDERAL TAX",
                		pnd_Inv_Acct_Vars_Pnd_Fund_Desc_12.getValue(pnd_I),new TabSetting(99),"///STATE TAX",
                		pnd_State_Tax_Filler,new TabSetting(110),"///LOCAL TAX",
                		pnd_Local_Tax_Filler,new TabSetting(121),"///CHECK #",
                		pnd_Check_Nbr_Filler, new IdenticalSuppress(true), new ReportZeroPrint (false));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }
    private void sub_Print_Payee_Totals() throws Exception                                                                                                                //Natural: PRINT-PAYEE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------**
        if (condition(pnd_Detail_Recs.getBoolean()))                                                                                                                      //Natural: IF #DETAIL-RECS
        {
            //*  10-20-1999
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(29),pnd_Install1_Vars_Pymnt_Check_Dte, new ReportEditMask ("MM/DD/YY"),new          //Natural: WRITE ( 01 ) // 029T #INSTALL1-VARS.PYMNT-CHECK-DTE 040T #PAYEE-TOTAL-GROSS-AMT 058T #PAYEE-TOTAL-IVC-AMT 085T #PAYEE-TOTAL-FED-TAX-AMT 099T #PAYEE-TOTAL-STE-TAX-AMT 110T #PAYEE-TOTAL-LCL-TAX-AMT 121T #INSTALL1-VARS.PYMNT-CHECK-NBR ( ZP = OFF ) 130T #INSTALL1-VARS.#RTB-3
                TabSetting(40),pnd_Payee_Total_Vars_Pnd_Payee_Total_Gross_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(58),pnd_Payee_Total_Vars_Pnd_Payee_Total_Ivc_Amt, 
                new ReportEditMask ("ZZZ,ZZ9.99"),new TabSetting(85),pnd_Payee_Total_Vars_Pnd_Payee_Total_Fed_Tax_Amt, new ReportEditMask ("ZZZ,ZZ9.99"),new 
                TabSetting(99),pnd_Payee_Total_Vars_Pnd_Payee_Total_Ste_Tax_Amt, new ReportEditMask ("ZZ,ZZ9.99"),new TabSetting(110),pnd_Payee_Total_Vars_Pnd_Payee_Total_Lcl_Tax_Amt, 
                new ReportEditMask ("ZZ,ZZ9.99"),new TabSetting(121),pnd_Install1_Vars_Pymnt_Check_Nbr, new ReportZeroPrint (false), new ReportEditMask 
                ("ZZZZ999"),new TabSetting(130),pnd_Install1_Vars_Pnd_Rtb_3);
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM RESET-VARIABLES
            sub_Reset_Variables();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }
    private void sub_No_Payment_Process() throws Exception                                                                                                                //Natural: NO-PAYMENT-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------**
        pnd_Title_Vars_Pnd_Accounting_Date.setValue(Global.getDATX());                                                                                                    //Natural: MOVE *DATX TO #ACCOUNTING-DATE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(40),"*** THERE WERE NO PAYMENTS FOR THIS ORIGIN CODE ***",              //Natural: WRITE ( 01 ) //// 040T '*** THERE WERE NO PAYMENTS FOR THIS ORIGIN CODE ***' //
            NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        //* *----------**
    }
    private void sub_End_Report_Processing() throws Exception                                                                                                             //Natural: END-REPORT-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------**
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(55),"*** END OF REPORT ***");                                                           //Natural: WRITE ( 01 ) // 55T '*** END OF REPORT ***'
        if (Global.isEscape()) return;
        getReports().getPageNumberDbs(1).reset();                                                                                                                         //Natural: RESET *PAGE-NUMBER ( 01 )
        //* *----------**
    }
    //*  10-20-1999
    private void sub_Print_Summary_Ledgers() throws Exception                                                                                                             //Natural: PRINT-SUMMARY-LEDGERS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Title_Vars_Pnd_Title.setValue("Daily Cancel/Stop with NO Redraw Summary");                                                                                    //Natural: ASSIGN #TITLE := 'Daily Cancel/Stop with NO Redraw Summary'
        pnd_Title_Vars_Pnd_Accounting_Date.setValue(Global.getDATX());                                                                                                    //Natural: MOVE *DATX TO #ACCOUNTING-DATE
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        FOR03:                                                                                                                                                            //Natural: FOR #ACCOUNT 1 #ACCOUNT-MAX
        for (pnd_Account.setValue(1); condition(pnd_Account.lessOrEqual(pnd_Account_Max)); pnd_Account.nadd(1))
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(25),gdaFcpg230b.getSummary_Total_Data_Pnd_Inv_Acct().getValue(pnd_Account),         //Natural: WRITE ( 01 ) // 025T SUMMARY-TOTAL-DATA.#INV-ACCT ( #ACCOUNT ) /
                NEWLINE);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            FOR04:                                                                                                                                                        //Natural: FOR #LEDGER 1 50
            for (pnd_Ledger.setValue(1); condition(pnd_Ledger.lessOrEqual(50)); pnd_Ledger.nadd(1))
            {
                if (condition(gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,pnd_Ledger).notEquals(getZero())))                                  //Natural: IF SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,#LEDGER ) NE 0
                {
                    DbsUtil.examine(new ExamineSource(gdaFcpg230b.getCs_Ledger_Table_Pnd_Ledger_Code().getValue("*"),true), new ExamineSearch(gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Number().getValue(pnd_Account,pnd_Ledger),  //Natural: EXAMINE FULL CS-LEDGER-TABLE.#LEDGER-CODE ( * ) FOR FULL SUMMARY-TOTAL-DATA.#LEDGER-NUMBER ( #ACCOUNT,#LEDGER ) GIVING INDEX #I
                        true), new ExamineGivingIndex(pnd_I));
                    short decideConditionsMet791 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #I = 0
                    if (condition(pnd_I.equals(getZero())))
                    {
                        decideConditionsMet791++;
                        pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "<WARNING> UNKNOWN LEDGER:  (",      //Natural: COMPRESS '<WARNING> UNKNOWN LEDGER:  (' SUMMARY-TOTAL-DATA.#LEDGER-NUMBER ( #ACCOUNT,#LEDGER ) ')' INTO #SUMMARY-DESC-NBR-DETAIL LEAVING NO SPACE
                            gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Number().getValue(pnd_Account,pnd_Ledger), ")"));
                        if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                           //Natural: NEWPAGE ( 01 ) IF LESS THAN 3
                        {
                            getReports().newPage(1);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }
                        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,new TabSetting(25),pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 25T #SUMMARY-DESC-NBR-DETAIL 095T SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,#LEDGER )
                            TabSetting(95),gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,pnd_Ledger));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: WHEN #I > 0
                    else if (condition(pnd_I.greater(getZero())))
                    {
                        decideConditionsMet791++;
                        if (condition(! (pnd_Account.equals(3) && pnd_Ledger.greaterOrEqual(3) && pnd_Ledger.lessOrEqual(22))))                                           //Natural: IF NOT ( ( #ACCOUNT = 3 ) AND ( #LEDGER EQ 3 THRU 22 ) )
                        {
                            pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, gdaFcpg230b.getCs_Ledger_Table_Pnd_Ledger_Desc().getValue(pnd_I),  //Natural: COMPRESS CS-LEDGER-TABLE.#LEDGER-DESC ( #I ) '  (' CS-LEDGER-TABLE.#LEDGER-CODE ( #I ) ')' INTO #SUMMARY-DESC-NBR-DETAIL LEAVING NO SPACE
                                "  (", gdaFcpg230b.getCs_Ledger_Table_Pnd_Ledger_Code().getValue(pnd_I), ")"));
                            //*  STOCK INSTALLMENT DATE TOTALS
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail.setValueEdited(pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date.getValue(pnd_Ledger),new     //Natural: MOVE EDITED #MTUZ-INSTALL-DATE ( #LEDGER ) ( EM = XX/XX/XXXX ) TO #SUMMARY-DESC-NBR-DETAIL
                                ReportEditMask("XX/XX/XXXX"));
                            if (condition(pnd_Ledger.equals(3)))                                                                                                          //Natural: IF #LEDGER = 3
                            {
                                pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, gdaFcpg230b.getCs_Ledger_Table_Pnd_Ledger_Desc().getValue(pnd_I),  //Natural: COMPRESS CS-LEDGER-TABLE.#LEDGER-DESC ( #I ) '  (' CS-LEDGER-TABLE.#LEDGER-CODE ( #I ) ')    ' #SUMMARY-DESC-NBR-DETAIL INTO #SUMMARY-DESC-NBR-DETAIL LEAVING NO SPACE
                                    "  (", gdaFcpg230b.getCs_Ledger_Table_Pnd_Ledger_Code().getValue(pnd_I), ")    ", pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail));
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail.setValue(pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail, MoveOption.RightJustified); //Natural: MOVE RIGHT #SUMMARY-DESC-NBR-DETAIL TO #SUMMARY-DESC-NBR-DETAIL
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(gdaFcpg230b.getCs_Ledger_Table_Pnd_Ledger_Desc().getValue(pnd_I).getSubstring(1,1).equals("D")))                                    //Natural: IF SUBSTR ( CS-LEDGER-TABLE.#LEDGER-DESC ( #I ) ,1,1 ) = 'D'
                        {
                            if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                       //Natural: NEWPAGE ( 01 ) IF LESS THAN 3
                            {
                                getReports().newPage(1);
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }
                            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,new TabSetting(25),pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 25T #SUMMARY-DESC-NBR-DETAIL 095T SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,#LEDGER )
                                TabSetting(95),gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,pnd_Ledger));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Debit.nadd(gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,                    //Natural: ADD SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,#LEDGER ) TO #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-DEBIT
                                pnd_Ledger));
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                       //Natural: NEWPAGE ( 01 ) IF LESS THAN 3
                            {
                                getReports().newPage(1);
                                if (condition(Global.isEscape()))
                                {
                                    if (condition(Global.isEscapeBottom())) break;
                                    else if (condition(Global.isEscapeBottomImmediate())) break;
                                    else if (condition(Global.isEscapeTop())) continue;
                                    else if (condition(Global.isEscapeRoutine())) return;
                                    else break;
                                }
                            }
                            getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,NEWLINE,new TabSetting(27),pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR / 27T #SUMMARY-DESC-NBR-DETAIL 098T SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,#LEDGER )
                                TabSetting(98),gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,pnd_Ledger));
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Credit.nadd(gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,                   //Natural: ADD SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,#LEDGER ) TO #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-CREDIT
                                pnd_Ledger));
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pnd_Summary_Detail_Vars_Pnd_Summary_Desc_Nbr_Detail.reset();                                                                                          //Natural: RESET #SUMMARY-DESC-NBR-DETAIL
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Account.equals(1)))                                                                                                                         //Natural: IF #ACCOUNT = 1
            {
                if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                                   //Natural: NEWPAGE ( 01 ) IF LESS THAN 3
                {
                    getReports().newPage(1);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(35),"TOTAL DEBIT:",pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Debit,                //Natural: WRITE ( 01 ) // 035T 'TOTAL DEBIT:' #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-DEBIT 010X 'TOTAL CREDIT:' #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-CREDIT
                    new ReportEditMask ("$ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),"TOTAL CREDIT:",pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Credit, new ReportEditMask 
                    ("$ZZZ,ZZZ,ZZ9.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Debit.reset();                                                                                                     //Natural: RESET #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-DEBIT #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-CREDIT
                pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Credit.reset();
            }                                                                                                                                                             //Natural: END-IF
            if (condition((pnd_Account.equals(pnd_Account_Max)) && (pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Debit.notEquals(getZero()))))                                  //Natural: IF ( #ACCOUNT = #ACCOUNT-MAX ) AND ( #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-DEBIT NE 0 )
            {
                if (condition(getReports().getAstLinesLeft(1).less(3)))                                                                                                   //Natural: NEWPAGE ( 01 ) IF LESS THAN 3
                {
                    getReports().newPage(1);
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(35),"TOTAL DEBIT:",pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Debit,                //Natural: WRITE ( 01 ) // 035T 'TOTAL DEBIT:' #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-DEBIT 010X 'TOTAL CREDIT:' #SUMMARY-DETAIL-VARS.#TIAA-TOTAL-CREDIT
                    new ReportEditMask ("$ZZZ,ZZZ,ZZ9.99-"),new ColumnSpacing(10),"TOTAL CREDIT:",pnd_Summary_Detail_Vars_Pnd_Tiaa_Total_Credit, new ReportEditMask 
                    ("$ZZZ,ZZZ,ZZ9.99-"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *----------**
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------**
        pnd_Install1_Vars.setValuesByName(ldaFcpl190a.getPnd_Rpt_Ext());                                                                                                  //Natural: MOVE BY NAME #RPT-EXT TO #INSTALL1-VARS
        //*  JWO 2010-11
        //*  JWO 2010-11
        //*  JWO 2010-11
        short decideConditionsMet854 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN' OR = 'CP' OR = 'RP' OR = 'PR'
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") 
            || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))
        {
            decideConditionsMet854++;
            pnd_Install1_Vars_Pnd_Reason.setValue("CANCEL NO RDRW");                                                                                                      //Natural: ASSIGN #REASON := 'CANCEL NO RDRW'
        }                                                                                                                                                                 //Natural: WHEN #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SN' OR = 'SP'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP")))
        {
            decideConditionsMet854++;
            pnd_Install1_Vars_Pnd_Reason.setValue("STOP NO RDRW  ");                                                                                                      //Natural: ASSIGN #REASON := 'STOP NO RDRW  '
        }                                                                                                                                                                 //Natural: WHEN NOT ( #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR' )
        else if (condition(! ((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
            || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
            || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))))
        {
            decideConditionsMet854++;
            pnd_Install1_Vars_Pnd_Reason.setValue(DbsUtil.compress("*INVALID CD", ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde()));                           //Natural: COMPRESS '*INVALID CD' #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE INTO #REASON
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet854 > 0))
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Soc_Sec_Ind().equals(getZero())))                                                                               //Natural: IF #RPT-EXT.ANNT-SOC-SEC-IND = 0
            {
                pnd_Install1_Vars_Pnd_Soc_Sec_Tax_Id_Nbr.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Soc_Sec_Nbr(),new ReportEditMask("999-99-9999"));                 //Natural: MOVE EDITED #RPT-EXT.ANNT-SOC-SEC-NBR ( EM = 999-99-9999 ) TO #SOC-SEC-TAX-ID-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Install1_Vars_Pnd_Soc_Sec_Tax_Id_Nbr.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Soc_Sec_Nbr(),new ReportEditMask("99-9999999"));                  //Natural: MOVE EDITED #RPT-EXT.ANNT-SOC-SEC-NBR ( EM = 99-9999999 ) TO #SOC-SEC-TAX-ID-NBR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name().notEquals(" ")))                                                                                    //Natural: IF PH-MIDDLE-NAME NE ' '
            {
                pnd_Install1_Vars_Pnd_Ph_Name.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Ph_Middle_Name().getSubstring(1,1));                                                    //Natural: MOVE SUBSTR ( PH-MIDDLE-NAME,1,1 ) TO #PH-NAME
                pnd_Install1_Vars_Pnd_Ph_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Install1_Vars_Pnd_Ph_Name, "."));                              //Natural: COMPRESS #PH-NAME '.' INTO #PH-NAME LEAVING NO SPACE
                pnd_Install1_Vars_Pnd_Ph_Name.setValue(DbsUtil.compress(ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), pnd_Install1_Vars_Pnd_Ph_Name, ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name())); //Natural: COMPRESS PH-FIRST-NAME #PH-NAME PH-LAST-NAME INTO #PH-NAME
                //*             WITH DELIMITER '*'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Install1_Vars_Pnd_Ph_Name.setValue(DbsUtil.compress(ldaFcpl190a.getPnd_Rpt_Ext_Ph_First_Name(), ldaFcpl190a.getPnd_Rpt_Ext_Ph_Last_Name()));          //Natural: COMPRESS PH-FIRST-NAME PH-LAST-NAME INTO #PH-NAME
                //*             WITH DELIMITER '*'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Install1_Vars_Pnd_Issue_Dte_N.compute(new ComputeParameters(false, pnd_Install1_Vars_Pnd_Issue_Dte_N), DbsField.subtract(100000000,ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Invrse_Dte())); //Natural: ASSIGN #ISSUE-DTE-N := 100000000 - #RPT-EXT.CNTRCT-INVRSE-DTE
            pnd_Install1_Vars_Pnd_Issue_Dte_A.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Install1_Vars_Pnd_Issue_Dte_A_Mm, "/", pnd_Install1_Vars_Pnd_Issue_Dte_A_Dd,  //Natural: COMPRESS #ISSUE-DTE-A-MM '/' #ISSUE-DTE-A-DD '/' #ISSUE-DTE-A-YY INTO #ISSUE-DTE-A LEAVING NO SPACE
                "/", pnd_Install1_Vars_Pnd_Issue_Dte_A_Yy));
            //*  SET RTB, US NRA, CANADIAN NRA, CANADIAN FEDERAL, DEDUCTION SWITCHES.
            //*  10-20-1999
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().equals("N") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("X"))            //Natural: IF ( #RPT-EXT.CNTRCT-PYMNT-TYPE-IND = 'N' AND #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND = 'X' ) OR ( #RPT-EXT.CNTRCT-PYMNT-TYPE-IND = 'I' AND #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND = 'R' )
                || (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().equals("I") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind().equals("R"))))
            {
                pnd_Logical_Vars_Pnd_Rtb.setValue(true);                                                                                                                  //Natural: ASSIGN #RTB := TRUE
                pnd_Install1_Vars_Pnd_Rtb_3.setValue("RTB");                                                                                                              //Natural: ASSIGN #INSTALL1-VARS.#RTB-3 := 'RTB'
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM SUMMARY-INITIALIZATION
            sub_Summary_Initialization();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *----------**
    }
    //*  10-20-1999
    private void sub_Obtain_Installment_Data() throws Exception                                                                                                           //Natural: OBTAIN-INSTALLMENT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_N.compute(new ComputeParameters(false, pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_N), DbsField.subtract(100000000, //Natural: ASSIGN #MTUZ-INSTALL-DATE-N := 100000000 - #RPT-EXT.CNTRCT-INVRSE-DTE
            ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Invrse_Dte()));
        DbsUtil.examine(new ExamineSource(pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date.getValue("*"),true), new ExamineSearch(pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_A,  //Natural: EXAMINE FULL #MTUZ-INSTALL-DATE ( * ) FOR FULL #MTUZ-INSTALL-DATE-A GIVING INDEX #J
            true), new ExamineGivingIndex(pnd_J));
        if (condition(pnd_J.equals(getZero())))                                                                                                                           //Natural: IF #J = 0
        {
            DbsUtil.examine(new ExamineSource(pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date.getValue("*"),true), new ExamineSearch("        ", true),                     //Natural: EXAMINE FULL #MTUZ-INSTALL-DATE ( * ) FOR FULL '        ' GIVING INDEX #J
                new ExamineGivingIndex(pnd_J));
            pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date.getValue(pnd_J).setValue(pnd_Summary_Detail_Vars_Pnd_Mtuz_Install_Date_N);                                      //Natural: MOVE #MTUZ-INSTALL-DATE-N TO #MTUZ-INSTALL-DATE ( #J )
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }
    private void sub_Calculate_Payee_Totals() throws Exception                                                                                                            //Natural: CALCULATE-PAYEE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------**
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Gross_Amt.nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                                            //Natural: ADD #GROSS-AMT ( #LN ) TO #PAYEE-TOTAL-GROSS-AMT
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")))                                                                                         //Natural: IF #FUND-PDA.#COMPANY-CDE = 'T'
        {
            pnd_Payee_Total_Vars_Pnd_Payee_Total_Ivc_Amt.nadd(pnd_Inv_Acct_Vars_Pnd_Ivc_Amt.getValue(pnd_Ln));                                                            //Natural: ADD #IVC-AMT ( #LN ) TO #PAYEE-TOTAL-IVC-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Fed_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                                        //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO #PAYEE-TOTAL-FED-TAX-AMT
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Ste_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I));                                       //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) TO #PAYEE-TOTAL-STE-TAX-AMT
        pnd_Payee_Total_Vars_Pnd_Payee_Total_Lcl_Tax_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I));                                       //Natural: ADD #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) TO #PAYEE-TOTAL-LCL-TAX-AMT
        //* *----------**
    }
    //*  10-20-1999
    private void sub_Calculate_Summary_Totals() throws Exception                                                                                                          //Natural: CALCULATE-SUMMARY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------**
        //*  DEBIT FR TIAA CASH/TAXES/ETC.
                                                                                                                                                                          //Natural: PERFORM TIAA-GENERAL-DEBIT
        sub_Tiaa_General_Debit();
        if (condition(Global.isEscape())) {return;}
        //*  CREDIT TO TIAA (ALL ACCOUNTS)
                                                                                                                                                                          //Natural: PERFORM TIAA-CREDIT
        sub_Tiaa_Credit();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("T") || pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("G")                     //Natural: IF NOT ( #FUND-PDA.#INV-ACCT-ALPHA = 'T' OR = 'G' OR = 'TG' )
            || pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("TG"))))
        {
            pnd_Account.reset();                                                                                                                                          //Natural: RESET #ACCOUNT
            DbsUtil.examine(new ExamineSource(gdaFcpg230b.getSummary_Total_Data_Pnd_Inv_Acct().getValue("*"),true), new ExamineSearch(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(),  //Natural: EXAMINE FULL SUMMARY-TOTAL-DATA.#INV-ACCT ( * ) FOR FULL #FUND-PDA.#INV-ACCT-DESC-8 GIVING INDEX #ACCOUNT
                true), new ExamineGivingIndex(pnd_Account));
            if (condition(pnd_Account.equals(getZero())))                                                                                                                 //Natural: IF #ACCOUNT = 0
            {
                pnd_Account.compute(new ComputeParameters(false, pnd_Account), (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Seq().subtract(2)));                             //Natural: ASSIGN #ACCOUNT := ( #FUND-PDA.#INV-ACCT-SEQ - 2 )
                gdaFcpg230b.getSummary_Total_Data_Pnd_Inv_Acct().getValue(pnd_Account).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8());                       //Natural: ASSIGN SUMMARY-TOTAL-DATA.#INV-ACCT ( #ACCOUNT ) := #FUND-PDA.#INV-ACCT-DESC-8
                pnd_Account_Max.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #ACCOUNT-MAX
            }                                                                                                                                                             //Natural: END-IF
            //*  DR FROM TIAA CR TO INV ACCT
                                                                                                                                                                          //Natural: PERFORM TIAA-DEBIT-INV-ACCT-CREDIT
            sub_Tiaa_Debit_Inv_Acct_Credit();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(gdaFcpg230b.getSummary_Total_Data_Pnd_Inv_Acct().getValue(1).equals(" ")))                                                                      //Natural: IF SUMMARY-TOTAL-DATA.#INV-ACCT ( 1 ) = ' '
            {
                gdaFcpg230b.getSummary_Total_Data_Pnd_Inv_Acct().getValue(1).setValue("TIAA");                                                                            //Natural: ASSIGN SUMMARY-TOTAL-DATA.#INV-ACCT ( 1 ) := 'TIAA'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }
    //*  10-20-1999
    private void sub_Summary_Initialization() throws Exception                                                                                                            //Natural: SUMMARY-INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------**
        //*  CRITERIOR FOR DETERMINING US NRA, CANADIAN NRA, CANADIAN FEDERAL MUST
        //*  BE OBTAINED AND/OR VERIFIED.
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Ctznshp_Cde().greater(1)))                                                                                          //Natural: IF #RPT-EXT.ANNT-CTZNSHP-CDE > 1
        {
            if (condition(DbsUtil.maskMatches(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde(),"NN")))                                                                        //Natural: IF #RPT-EXT.ANNT-RSDNCY-CDE = MASK ( NN )
            {
                if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde().greater("57")) && (ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde().notEquals("97"))))            //Natural: IF ( #RPT-EXT.ANNT-RSDNCY-CDE > '57' ) AND ( #RPT-EXT.ANNT-RSDNCY-CDE NE '97' )
                {
                    //*  CANADIAN
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Ctznshp_Cde().equals(2)))                                                                               //Natural: IF #RPT-EXT.ANNT-CTZNSHP-CDE = 2
                    {
                        pnd_Logical_Vars_Pnd_Canadian_Nra.setValue(true);                                                                                                 //Natural: ASSIGN #CANADIAN-NRA := TRUE
                        //*  OTHER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Logical_Vars_Pnd_Nra.setValue(true);                                                                                                          //Natural: ASSIGN #NRA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Rsdncy_Cde().notEquals("US")))                                                                              //Natural: IF #RPT-EXT.ANNT-RSDNCY-CDE NE 'US'
                {
                    //*  CANADIAN
                    if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Annt_Ctznshp_Cde().equals(2)))                                                                               //Natural: IF #RPT-EXT.ANNT-CTZNSHP-CDE = 2
                    {
                        pnd_Logical_Vars_Pnd_Canadian_Nra.setValue(true);                                                                                                 //Natural: ASSIGN #CANADIAN-NRA := TRUE
                        //*  OTHER
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Logical_Vars_Pnd_Nra.setValue(true);                                                                                                          //Natural: ASSIGN #NRA := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  10-20-1999
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #K 1 10
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(10)); pnd_K.nadd(1))
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(pnd_K).notEquals(getZero())))                                                               //Natural: IF #RPT-EXT.PYMNT-DED-AMT ( #K ) NE 0
            {
                //*  1=BLUE C/S, 3=MAJ. MED., 4= G. LIFE
                short decideConditionsMet989 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #RPT-EXT.PYMNT-DED-CDE ( #K );//Natural: VALUE 1, 3, 4
                if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Cde().getValue(pnd_K).equals(1) || ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Cde().getValue(pnd_K).equals(3) 
                    || ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Cde().getValue(pnd_K).equals(4))))
                {
                    decideConditionsMet989++;
                    pnd_Logical_Vars_Pnd_Ins_Deduction.setValue(true);                                                                                                    //Natural: ASSIGN #INS-DEDUCTION := TRUE
                    //*  LONG TERM CARE
                    pnd_Install1_Vars_Pnd_Ins_Ded_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(pnd_K));                                                   //Natural: ADD #RPT-EXT.PYMNT-DED-AMT ( #K ) TO #INS-DED-AMT
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Cde().getValue(pnd_K).equals(2))))
                {
                    decideConditionsMet989++;
                    pnd_Logical_Vars_Pnd_Ltc_Deduction.setValue(true);                                                                                                    //Natural: ASSIGN #LTC-DEDUCTION := TRUE
                    //*  5=OVPYMT, 6=NYSUT, 7=PERS. ANNTY.
                    pnd_Install1_Vars_Pnd_Ltc_Ded_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(pnd_K));                                                   //Natural: ADD #RPT-EXT.PYMNT-DED-AMT ( #K ) TO #LTC-DED-AMT
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Logical_Vars_Pnd_Other_Deductn.setValue(true);                                                                                                    //Natural: ASSIGN #OTHER-DEDUCTN := TRUE
                    pnd_Install1_Vars_Pnd_Other_Ded_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(pnd_K));                                                 //Natural: ADD #RPT-EXT.PYMNT-DED-AMT ( #K ) TO #OTHER-DED-AMT
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *----------**
    }
    //*  10-20-1999
    private void sub_Tiaa_General_Debit() throws Exception                                                                                                                //Natural: TIAA-GENERAL-DEBIT
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------**
        //*  ALL DEBITS IN THIS SECTION ARE FROM TIAA CASH, TAX, DEDUCTION LEDGERS
        //*  DR CASH FR CPS BANK
        gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,1).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                       //Natural: ADD #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,1 )
        if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")) && (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().notEquals("R"))))                  //Natural: IF ( #FUND-PDA.#COMPANY-CDE = 'T' ) AND ( #FUND-PDA.#INV-ACCT-ALPHA NE 'R' )
        {
            if (condition(! (pnd_Logical_Vars_Pnd_Nra.getBoolean() || pnd_Logical_Vars_Pnd_Canadian_Nra.getBoolean() || pnd_Logical_Vars_Pnd_Canadian_Fed.getBoolean()))) //Natural: IF NOT ( #NRA OR #CANADIAN-NRA OR #CANADIAN-FED )
            {
                //*  TIAA FEDERAL TAX
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,2).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,2 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Logical_Vars_Pnd_Nra.getBoolean()))                                                                                                     //Natural: IF #NRA
                {
                    //*  TIAA NRA TAX
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,6).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));            //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,6 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Logical_Vars_Pnd_Canadian_Nra.getBoolean()))                                                                                            //Natural: IF #CANADIAN-NRA
                {
                    //*  TIAA CANADIAN NRA
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,10).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));           //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,10 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Logical_Vars_Pnd_Canadian_Fed.getBoolean()))                                                                                            //Natural: IF #CANADIAN-FED
                {
                    //*  TIAA CANADIAN FED
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,8).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));            //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,8 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  TIAA STATE TAX
            gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,4).compute(new ComputeParameters(false, gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,4)),  //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,4 )
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,4).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I)).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I)));
            if (condition(pnd_Logical_Vars_Pnd_Ins_Deduction.getBoolean()))                                                                                               //Natural: IF #INS-DEDUCTION
            {
                //*  TIAA INSURANCE DED.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,12).nadd(pnd_Install1_Vars_Pnd_Ins_Ded_Amt);                                                //Natural: ADD #INS-DED-AMT TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,12 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Logical_Vars_Pnd_Ltc_Deduction.getBoolean()))                                                                                               //Natural: IF #LTC-DEDUCTION
            {
                //*  TIAA LTC DED.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,14).nadd(pnd_Install1_Vars_Pnd_Ltc_Ded_Amt);                                                //Natural: ADD #LTC-DED-AMT TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,14 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Logical_Vars_Pnd_Other_Deductn.getBoolean()))                                                                                               //Natural: IF #OTHER-DEDUCTN
            {
                //*  TIAA OTHER DED.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,16).nadd(pnd_Install1_Vars_Pnd_Other_Ded_Amt);                                              //Natural: ADD #OTHER-DED-AMT TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,16 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Logical_Vars_Pnd_Rtb.getBoolean())))                                                                                                     //Natural: IF NOT #RTB
            {
                //*  TIAA EXPENSE CHG.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,18).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I));                    //Natural: ADD #RPT-EXT.INV-ACCT-EXP-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,18 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(! (pnd_Logical_Vars_Pnd_Nra.getBoolean() || pnd_Logical_Vars_Pnd_Canadian_Nra.getBoolean() || pnd_Logical_Vars_Pnd_Canadian_Fed.getBoolean()))) //Natural: IF NOT ( #NRA OR #CANADIAN-NRA OR #CANADIAN-FED )
            {
                //*  CREF FEDERAL TAX
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,3).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));                //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,3 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Logical_Vars_Pnd_Nra.getBoolean()))                                                                                                     //Natural: IF #NRA
                {
                    //*  CREF NRA TAX
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,7).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));            //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,7 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Logical_Vars_Pnd_Canadian_Nra.getBoolean()))                                                                                            //Natural: IF #CANADIAN-NRA
                {
                    //*  CREF CANADIAN NRA
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,11).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));           //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,11 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Logical_Vars_Pnd_Canadian_Fed.getBoolean()))                                                                                            //Natural: IF #CANADIAN-FED
                {
                    //*  CREF CANADIAN FED
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,9).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I));            //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,9 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*  CREF STATE TAX
            gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,5).compute(new ComputeParameters(false, gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,5)),  //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,5 )
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,5).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I)).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I)));
            if (condition(pnd_Logical_Vars_Pnd_Ins_Deduction.getBoolean()))                                                                                               //Natural: IF #INS-DEDUCTION
            {
                //*  CREF INSURANCE DED.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,13).nadd(pnd_Install1_Vars_Pnd_Ins_Ded_Amt);                                                //Natural: ADD #INS-DED-AMT TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,13 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Logical_Vars_Pnd_Ltc_Deduction.getBoolean()))                                                                                               //Natural: IF #LTC-DEDUCTION
            {
                //*  CREF LTC DED.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,15).nadd(pnd_Install1_Vars_Pnd_Ltc_Ded_Amt);                                                //Natural: ADD #LTC-DED-AMT TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,15 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Logical_Vars_Pnd_Other_Deductn.getBoolean()))                                                                                               //Natural: IF #OTHER-DEDUCTN
            {
                //*  CREF OTHER DED.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,17).nadd(pnd_Install1_Vars_Pnd_Other_Ded_Amt);                                              //Natural: ADD #OTHER-DED-AMT TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,17 )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(! (pnd_Logical_Vars_Pnd_Rtb.getBoolean())))                                                                                                     //Natural: IF NOT #RTB
            {
                //*  CREF EXPENSE CHG.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,19).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(pnd_I));                    //Natural: ADD #RPT-EXT.INV-ACCT-EXP-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,19 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I).notEquals(getZero())) || (ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I).notEquals(getZero())))) //Natural: IF ( #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) NE 0 ) OR ( #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) NE 0 )
        {
                                                                                                                                                                          //Natural: PERFORM STATE-TAX-PROCESSING
            sub_State_Tax_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }
    //*  10-20-1999
    private void sub_Tiaa_Credit() throws Exception                                                                                                                       //Natural: TIAA-CREDIT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------**
        //*  ALL CREDITS IN THIS SECTION ARE FROM TIAA TO CONT. TYPES & INV. ACCTS.
        if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde().equals("T")) && (pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().notEquals("R"))))                  //Natural: IF ( #FUND-PDA.#COMPANY-CDE = 'T' ) AND ( #FUND-PDA.#INV-ACCT-ALPHA NE 'R' )
        {
            short decideConditionsMet1112 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN ( SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,2 ) = 'IA' OR = 'IB' OR = 'IC' OR = 'ID' OR = 'IE' OR = 'IF' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR = MASK ( 'Y'......... ) )
            if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("IA") || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("IB") 
                || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("IC") || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("ID") 
                || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("IE") || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("IF") 
                || DbsUtil.maskMatches(pnd_Install1_Vars_Cntrct_Ppcn_Nbr,"'Y'.........")))
            {
                decideConditionsMet1112++;
                //*  CR PREFIX
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,20).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,20 )
                //*  CR PREF. DIV.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,21).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,21 )
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,2 ) = 'GA'
            else if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("GA")))
            {
                decideConditionsMet1112++;
                //*  CR 'GA' PREFIX
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,22).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,22 )
                //*  CR 'GA' DIV.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,23).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,23 )
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,2 ) = 'S0'
            else if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("S0")))
            {
                decideConditionsMet1112++;
                //*  CR 'S0' PREFIX
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,24).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,24 )
                //*  CR 'S0' DIV.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,25).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,25 )
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,2 ) = 'W0'
            else if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("W0")))
            {
                decideConditionsMet1112++;
                if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0000000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0249999")))                 //Natural: IF #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0000000' THRU 'W0249999'
                {
                    //*  CR 'W0' PREFIX
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,26).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));              //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,26 )
                    //*  CR 'W0' DIV.
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,27).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));              //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,27 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition((((((pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0250000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0289999"))             //Natural: IF ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0250000' THRU 'W0289999' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0300000' THRU 'W0379999' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0390000' THRU 'W0399999' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0410000' THRU 'W0519999' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0530000' THRU 'W0799999' )
                    || (pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0300000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0379999"))) || (pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0390000") 
                    && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0399999"))) || (pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0410000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0519999"))) 
                    || (pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0530000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0799999")))))
                {
                    //*  CR 'W0' PREFIX
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,30).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));              //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,30 )
                }                                                                                                                                                         //Natural: END-IF
                if (condition((((((pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0290000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0299999"))             //Natural: IF ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0290000' THRU 'W0299999' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0380000' THRU 'W0389999' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0400000' THRU 'W0409999' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0520000' THRU 'W0529999' ) OR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR EQ 'W0800000' THRU 'W0900000' )
                    || (pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0380000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0389999"))) || (pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0400000") 
                    && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0409999"))) || (pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0520000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0529999"))) 
                    || (pnd_Install1_Vars_Cntrct_Ppcn_Nbr.greaterOrEqual("W0800000") && pnd_Install1_Vars_Cntrct_Ppcn_Nbr.lessOrEqual("W0900000")))))
                {
                    //*  CR 'W0' PREFIX
                    gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,31).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));              //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,31 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,2 ) = 'GW'
            else if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("GW")))
            {
                decideConditionsMet1112++;
                //*  CR 'GW' PREFIX
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,28).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,28 )
                //*  CR 'GW' DIV.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,29).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,29 )
            }                                                                                                                                                             //Natural: WHEN SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,2 ) = 'IP' OR = 'IQ'
            else if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("IP") || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("IQ")))
            {
                decideConditionsMet1112++;
                //*  CR 'GW' PREFIX
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,32).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,32 )
                //*  CR 'GW' DIV.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,33).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,33 )
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                //*  CR OTHER PREFIX
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,34).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,34 )
                //*  CR OTHER DIV.
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,35).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                  //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( #I ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,35 )
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  CR STOCK
            //*  CR BOND
            //*  CR MMA
            //*  CR SOCIAL
            //*  CR GLOBAL
            //*  CR GROWTH
            //*  CR EQUITY
            //*  CR ILB
            //*  CR REAL EST.
            short decideConditionsMet1172 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #FUND-PDA.#INV-ACCT-ALPHA;//Natural: VALUE 'C'
            if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("C"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,36).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,36 )
            }                                                                                                                                                             //Natural: VALUE 'B'
            else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("B"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,37).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,37 )
            }                                                                                                                                                             //Natural: VALUE 'M'
            else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("M"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,38).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,38 )
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("S"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,39).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,39 )
            }                                                                                                                                                             //Natural: VALUE 'W'
            else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("W"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,40).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,40 )
            }                                                                                                                                                             //Natural: VALUE 'L'
            else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("L"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,41).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,41 )
            }                                                                                                                                                             //Natural: VALUE 'E'
            else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("E"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,42).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,42 )
            }                                                                                                                                                             //Natural: VALUE 'I'
            else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("I"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,43).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,43 )
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("R"))))
            {
                decideConditionsMet1172++;
                gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(1,44).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                                 //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( 1,44 )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }
    //*  10-20-1999
    private void sub_Tiaa_Debit_Inv_Acct_Credit() throws Exception                                                                                                        //Natural: TIAA-DEBIT-INV-ACCT-CREDIT
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------**
        //*  DR FR TIAA
        short decideConditionsMet1203 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #FUND-PDA.#INV-ACCT-ALPHA;//Natural: VALUE 'R'
        if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("R"))))
        {
            decideConditionsMet1203++;
            gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,1).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                            //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,1 )
            if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("6M") || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,2).equals("Y")))           //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,2 ) = '6M' OR = 'Y'
            {
                pnd_Ledger.setValue(2);                                                                                                                                   //Natural: ASSIGN #LEDGER := 2
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("C"))))
        {
            decideConditionsMet1203++;
            if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("L")))                                                                               //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'L'
            {
                pnd_Ledger.setValue(2);                                                                                                                                   //Natural: ASSIGN #LEDGER := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("N")))                                                                           //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'N'
                {
                    pnd_Ledger.setValue(23);                                                                                                                              //Natural: ASSIGN #LEDGER := 23
                    //*  INSTALLMENT DATE BREAKDOWN 3:22
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  TOT FOR EA DATE
                    if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("M") || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("T")      //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'M' OR = 'T' OR = 'U' OR = 'Z'
                        || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("U") || pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("Z")))
                    {
                        pnd_Ledger.setValue(pnd_J);                                                                                                                       //Natural: ASSIGN #LEDGER := #J
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ledger.setValue(24);                                                                                                                          //Natural: ASSIGN #LEDGER := 24
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'L', 'I'
        else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("L") || pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("I"))))
        {
            decideConditionsMet1203++;
            if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("L")))                                                                               //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'L'
            {
                pnd_Ledger.setValue(2);                                                                                                                                   //Natural: ASSIGN #LEDGER := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("M")))                                                                           //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'M'
                {
                    pnd_Ledger.setValue(3);                                                                                                                               //Natural: ASSIGN #LEDGER := 3
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("N")))                                                                       //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'N'
                    {
                        pnd_Ledger.setValue(4);                                                                                                                           //Natural: ASSIGN #LEDGER := 4
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ledger.setValue(5);                                                                                                                           //Natural: ASSIGN #LEDGER := 5
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'B', 'M'
        else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("B") || pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("M"))))
        {
            decideConditionsMet1203++;
            if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("L")))                                                                               //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'L'
            {
                pnd_Ledger.setValue(2);                                                                                                                                   //Natural: ASSIGN #LEDGER := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("N")))                                                                           //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'N'
                {
                    pnd_Ledger.setValue(3);                                                                                                                               //Natural: ASSIGN #LEDGER := 3
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("M")))                                                                       //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'M'
                    {
                        pnd_Ledger.setValue(4);                                                                                                                           //Natural: ASSIGN #LEDGER := 4
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ledger.setValue(5);                                                                                                                           //Natural: ASSIGN #LEDGER := 5
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'S', 'W', 'E'
        else if (condition((pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("S") || pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("W") 
            || pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Alpha().equals("E"))))
        {
            decideConditionsMet1203++;
            if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("M")))                                                                               //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'M'
            {
                pnd_Ledger.setValue(2);                                                                                                                                   //Natural: ASSIGN #LEDGER := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("L")))                                                                           //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'L'
                {
                    pnd_Ledger.setValue(3);                                                                                                                               //Natural: ASSIGN #LEDGER := 3
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Install1_Vars_Cntrct_Ppcn_Nbr.getSubstring(1,1).equals("N")))                                                                       //Natural: IF SUBSTR ( #INSTALL1-VARS.CNTRCT-PPCN-NBR,1,1 ) = 'N'
                    {
                        pnd_Ledger.setValue(4);                                                                                                                           //Natural: ASSIGN #LEDGER := 4
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ledger.setValue(5);                                                                                                                           //Natural: ASSIGN #LEDGER := 5
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  DR FR TIAA
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet1203 > 0))
        {
            gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,1).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                            //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,1 )
            gdaFcpg230b.getSummary_Total_Data_Pnd_Ledger_Amt().getValue(pnd_Account,pnd_Ledger).nadd(pnd_Inv_Acct_Vars_Pnd_Gross_Amt.getValue(pnd_Ln));                   //Natural: ADD #INV-ACCT-VARS.#GROSS-AMT ( #LN ) TO SUMMARY-TOTAL-DATA.#LEDGER-AMT ( #ACCOUNT,#LEDGER )
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *----------**
    }
    private void sub_Reset_Variables() throws Exception                                                                                                                   //Natural: RESET-VARIABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------**
        pnd_Install1_Vars.reset();                                                                                                                                        //Natural: RESET #INSTALL1-VARS #INV-ACCT-VARS ( * ) #PAYEE-TOTAL-VARS #LN #DETAIL-RECS #LOGICAL-VARS
        pnd_Inv_Acct_Vars.getValue("*").reset();
        pnd_Payee_Total_Vars.reset();
        pnd_Ln.reset();
        pnd_Detail_Recs.reset();
        pnd_Logical_Vars.reset();
        //* *----------**
    }
    private void sub_Print_Payee_Header() throws Exception                                                                                                                //Natural: PRINT-PAYEE-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------**
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,pnd_Install1_Vars_Cntrct_Unq_Id_Nbr,new TabSetting(16),pnd_Install1_Vars_Pnd_Soc_Sec_Tax_Id_Nbr,new            //Natural: WRITE ( 01 ) / #INSTALL1-VARS.CNTRCT-UNQ-ID-NBR 016T #SOC-SEC-TAX-ID-NBR 040T #PH-NAME ( AL = 30 ) 071T #REASON /
            TabSetting(40),pnd_Install1_Vars_Pnd_Ph_Name, new AlphanumericLength (30),new TabSetting(71),pnd_Install1_Vars_Pnd_Reason,NEWLINE);
        if (Global.isEscape()) return;
        //* *----------**
    }
    //*  10-20-1999
    private void sub_State_Tax_Processing() throws Exception                                                                                                              //Natural: STATE-TAX-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------**
        DbsUtil.examine(new ExamineSource(gdaFcpg230b.getCs_State_Tax_Ledger_Table_Pnd_State_Code_A2().getValue("*")), new ExamineSearch(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Cde().getValue(pnd_I)),  //Natural: EXAMINE CS-STATE-TAX-LEDGER-TABLE.#STATE-CODE-A2 ( * ) FOR #RPT-EXT.INV-ACCT-STATE-CDE ( #I ) GIVING INDEX #K
            new ExamineGivingIndex(pnd_K));
        if (condition(pnd_K.greater(getZero())))                                                                                                                          //Natural: IF #K > 0
        {
            gdaFcpg230b.getCs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt().getValue(pnd_K).compute(new ComputeParameters(false, gdaFcpg230b.getCs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt().getValue(pnd_K)),  //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) TO CS-STATE-TAX-LEDGER-TABLE.#STATE-LEDGER-AMT ( #K )
                gdaFcpg230b.getCs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt().getValue(pnd_K).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I)).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I)));
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  UNKNOWN
            gdaFcpg230b.getCs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt().getValue(55).compute(new ComputeParameters(false, gdaFcpg230b.getCs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt().getValue(55)),  //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( #I ) #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) TO CS-STATE-TAX-LEDGER-TABLE.#STATE-LEDGER-AMT ( 55 )
                gdaFcpg230b.getCs_State_Tax_Ledger_Table_Pnd_State_Ledger_Amt().getValue(55).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(pnd_I)).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(pnd_I)));
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************",NEWLINE,NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***  LAST RECORD READ:",NEWLINE,"***   CNR Activity Cd:",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde(),NEWLINE,"***         Origin Cd:",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde(),NEWLINE,"***   Accounting Date:",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte(),  //Natural: WRITE // '**************************************************************' / '**************************************************************' // '***' *PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***  LAST RECORD READ:' / '***   CNR Activity Cd:' #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE / '***         Origin Cd:' #RPT-EXT.CNTRCT-ORGN-CDE / '***   Accounting Date:' #RPT-EXT.PYMNT-ACCTG-DTE ( EM = MM/DD/YYYY ) / '***              PPCN:' #RPT-EXT.CNTRCT-PPCN-NBR / '***             Payee:' #RPT-EXT.CNTRCT-PAYEE-CDE / '***        Sequence #:' #RPT-EXT.PYMNT-PRCSS-SEQ-NBR / '***       Pymnt. Type:' #RPT-EXT.CNTRCT-PYMNT-TYPE-IND / '***       Settl. Type:' #RPT-EXT.CNTRCT-STTLMNT-TYPE-IND / '***' / '**************************************************************' / '**************************************************************'
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"***              PPCN:",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr(),NEWLINE,"***             Payee:",
            ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde(),NEWLINE,"***        Sequence #:",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr(),NEWLINE,"***       Pymnt. Type:",
            ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind(),NEWLINE,"***       Settl. Type:",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Sttlmnt_Type_Ind(),NEWLINE,
            "***",NEWLINE,"**************************************************************",NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60");
        Global.format(1, "LS=133 PS=60");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATX(), new ReportEditMask ("MM/DD/YY"),new 
            ColumnSpacing(1),"-",new ColumnSpacing(1),Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(30),pnd_Title_Vars_Pnd_Origin,new 
            TabSetting(51),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(119),"PAGE:",new ColumnSpacing(1),getReports().getPageNumberDbs(1), new FieldAttributes 
            ("AD=L"),NEWLINE,Global.getINIT_USER(),new ColumnSpacing(1),"-",new ColumnSpacing(1),Global.getPROGRAM(),new TabSetting(45),pnd_Title_Vars_Pnd_Title,NEWLINE,Global.getLIBRARY_ID(),new 
            TabSetting(50),"For Accounting Date",new ColumnSpacing(1),pnd_Title_Vars_Pnd_Accounting_Date, new ReportEditMask ("MM/DD/YYYY"),NEWLINE);

        getReports().setDisplayColumns(1, "PIN/COMPANY//",
        		pnd_Inv_Acct_Vars_Pnd_Company, new IdenticalSuppress(true),new TabSetting(16),"SSN# (TAX#)/CONTRACT #/",
        		pnd_Inv_Acct_Vars_Pnd_Contract_Nbr, new IdenticalSuppress(true),new TabSetting(29),"/ISSUE DT//ORIG DATE",
        		pnd_Install1_Vars_Pnd_Issue_Dte_A, new IdenticalSuppress(true),new TabSetting(40),"PARTICIPANT NAME/GROSS/AMOUNT/",
        		pnd_Inv_Acct_Vars_Pnd_Gross_Amt,new TabSetting(58),"/CONT AMT/UNITS/IVC",
        		pnd_Inv_Acct_Vars_Pnd_Cont_Amt_Unit_Qty_A,new TabSetting(71),"REASON/DIVIDEND/UNIT VALUE/",
        		pnd_Inv_Acct_Vars_Pnd_Dvdnd_Amt_Unit_Val_A,new TabSetting(85),"/INV ACCT//FEDERAL TAX",
        		pnd_Inv_Acct_Vars_Pnd_Fund_Desc_12,new TabSetting(99),"///STATE TAX",
        		pnd_State_Tax_Filler,new TabSetting(110),"///LOCAL TAX",
        		pnd_Local_Tax_Filler,new TabSetting(121),"///CHECK #",
        		pnd_Check_Nbr_Filler, new IdenticalSuppress(true), new ReportZeroPrint (false));
    }
    private void CheckAtStartofData406() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Title_Vars_Pnd_Origin.setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde());                                                                             //Natural: ASSIGN #ORIGIN := #RPT-EXT.CNTRCT-ORGN-CDE
            pnd_Title_Vars_Pnd_Accounting_Date.setValue(Global.getDATX());                                                                                                //Natural: MOVE *DATX TO #ACCOUNTING-DATE
            //*  WILL NOT STOW IN 3.1.4
            //*   ESCAPE TOP
        }                                                                                                                                                                 //Natural: END-START
    }
}
