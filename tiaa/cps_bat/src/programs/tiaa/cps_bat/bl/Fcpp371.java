/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:59 PM
**        * FROM NATURAL PROGRAM : Fcpp371
************************************************************
**        * FILE NAME            : Fcpp371.java
**        * CLASS NAME           : Fcpp371
**        * INSTANCE NAME        : Fcpp371
************************************************************
************************************************************************
* PROGRAM  : FCPP371
* SYSTEM   : CPS
* TITLE    : EXTRACT
* GENERATED: FEB 13,97 AT 04:58 PM
* FUNCTION : THIS PROGRAM WILL EXTRACT RECORDS FOR A STATED
*            PERIOD OF TIME.
* HISTORY  :
*
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*  09/14/99 : A. YOUNG - POPULATE STATEMENT ORIGINAL INVERSE DATE FOR
*                        EACH INSTALLMENT FROM WORKING STORAGE.
*  10/29/99 - ROXAN CARREON
*             EW PROCESSING. INPUT INTERFACE-DATE AND FOR EVERY EW
*                          WRITE TO ANOTHER WORKFILE TO BE USED
*                          AS ADDITIONAL INPUT TO FCPP120 IN P1441CPD.
*  11/29/99 - USE ELEMENT 43 OF THE CONTROL RECORD FOR EW SUSPENSE
*           - USE FCPNCNT2 FOR NZ/AL/EW
*  12/03/99 - USE NEW LOCAL FCPAEXT
* 09/11/00 : A. YOUNG    -  RE-COMPILED DUE TO FCPLPMNT.
*
* 01-25-2001 : LEON G. RESTOW TO TAKE IN NEW PRODUCTION VERSION OF
*            : FCPAEXT (PDA WOS CHANGE FOR 'NZ' FIX) AND
*            :  'RESET EXT.PYMNT-IVC-AMT'  /*  APPLY T.MCGEE CHANGE
*            :                          /* FOR 'NZ' TO LUMP-SUM PROJECT
*    11-2002 : R. CARREON   STOW. EGTRRA CHANGES IN LDA
*    12-2002 : R. CARREON   CALENDAR ACCESS CHANGED TO CPWN115
*              ALL REFERENCE TO FCPA115 WAS CHANGED TO CPWA115
*    03-2003 : RESTOW FCPAEXT WAS EXPANDED
*    05-2008 : RESTOW FOR ROTH (AER) ROTH-MAJOR1
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
* 01/15/2020: CTS   -  CPS SUNSET (CHG694525) TAG: CTS-0115
*             MODIFED TO PROCESS STOPS ONLY. IT WILL NOT REISSUE CHECKS
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp371 extends BLNatBase
{
    // Data Areas
    private PdaFcpaaddr pdaFcpaaddr;
    private PdaFcpacntr pdaFcpacntr;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpanzcn pdaFcpanzcn;
    private PdaFcpanzc1 pdaFcpanzc1;
    private PdaFcpanzc2 pdaFcpanzc2;
    private PdaCpwa115 pdaCpwa115;
    private PdaFcpaext pdaFcpaext;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcplcntu ldaFcplcntu;
    private LdaFcplpmnt ldaFcplpmnt;
    private LdaFcplnzc2 ldaFcplnzc2;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Process_Orgn_Cde;
    private DbsField pnd_Ws_Pnd_Pymnt_S_Start;
    private DbsField pnd_Ws_Pnd_Pymnt_S_End;
    private DbsField pnd_Ws_Pnd_Date;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Date_X;
    private DbsField pnd_Ws_Pnd_Break_Ind;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_Det_Amts;
    private DbsField pnd_Ws_Pnd_Pymnt_Amt;
    private DbsField pnd_Ws_Pnd_Ws_Cnr_Orig_Invrse_Dt;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_PYMNTPnd_Break_IndOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaaddr = new PdaFcpaaddr(localVariables);
        pdaFcpacntr = new PdaFcpacntr(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpanzcn = new PdaFcpanzcn(localVariables);
        pdaFcpanzc1 = new PdaFcpanzc1(localVariables);
        pdaFcpanzc2 = new PdaFcpanzc2(localVariables);
        pdaCpwa115 = new PdaCpwa115(localVariables);
        pdaFcpaext = new PdaFcpaext(localVariables);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcplcntu = new LdaFcplcntu();
        registerRecord(ldaFcplcntu);
        registerRecord(ldaFcplcntu.getVw_fcp_Cons_Cntrl());
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        ldaFcplnzc2 = new LdaFcplnzc2();
        registerRecord(ldaFcplnzc2);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Process_Orgn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Process_Orgn_Cde", "#PROCESS-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Pymnt_S_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_S_Start", "#PYMNT-S-START", FieldType.STRING, 4);
        pnd_Ws_Pnd_Pymnt_S_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_S_End", "#PYMNT-S-END", FieldType.STRING, 4);
        pnd_Ws_Pnd_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Date);
        pnd_Ws_Pnd_Date_X = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Date_X", "#DATE-X", FieldType.STRING, 8);
        pnd_Ws_Pnd_Break_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Break_Ind", "#BREAK-IND", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Det_Amts = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Det_Amts", "#DET-AMTS", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Pymnt_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Amt", "#PYMNT-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_Ws_Cnr_Orig_Invrse_Dt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Cnr_Orig_Invrse_Dt", "#WS-CNR-ORIG-INVRSE-DT", FieldType.NUMERIC, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_PYMNTPnd_Break_IndOld = internalLoopRecord.newFieldInRecord("READ_PYMNT_Pnd_Break_Ind_OLD", "Pnd_Break_Ind_OLD", FieldType.PACKED_DECIMAL, 
            7);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcplcntu.initializeValues();
        ldaFcplpmnt.initializeValues();
        ldaFcplnzc2.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Pymnt_S_Start.setInitialValue("H'0000C400'");
        pnd_Ws_Pnd_Pymnt_S_End.setInitialValue("H'0000C4FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp371() throws Exception
    {
        super("Fcpp371");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //* *--------
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM RESET-CONTROL-VARIABLES
            sub_Reset_Control_Variables();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            short decideConditionsMet990 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #I;//Natural: VALUE 1
            if (condition((pnd_Ws_Pnd_I.equals(1))))
            {
                decideConditionsMet990++;
                pnd_Ws_Pnd_Process_Orgn_Cde.setValue("NZ");                                                                                                               //Natural: ASSIGN #PROCESS-ORGN-CDE := 'NZ'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_Ws_Pnd_I.equals(2))))
            {
                decideConditionsMet990++;
                pnd_Ws_Pnd_Process_Orgn_Cde.setValue("AL");                                                                                                               //Natural: ASSIGN #PROCESS-ORGN-CDE := 'AL'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_Ws_Pnd_I.equals(3))))
            {
                decideConditionsMet990++;
                pnd_Ws_Pnd_Process_Orgn_Cde.setValue("EW");                                                                                                               //Natural: ASSIGN #PROCESS-ORGN-CDE := 'EW'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Ws_Pnd_Break_Ind.reset();                                                                                                                                 //Natural: RESET #BREAK-IND #DET-AMTS #PYMNT-AMT
            pnd_Ws_Pnd_Det_Amts.reset();
            pnd_Ws_Pnd_Pymnt_Amt.reset();
            //*  CHECK THE NEW ANNUITIZATION OR ANNUITY LOAN CONTROL RECORD.
                                                                                                                                                                          //Natural: PERFORM CHECK-CONTROL-RECORD
            sub_Check_Control_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                               //Natural: MOVE TRUE TO #FCPANZC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 6 ) ADDR-ABEND-IND CURRENT-ADDR
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
            pdaFcpaaddr.getAddr_Work_Fields_Addr_Abend_Ind().setValue(true);
            pdaFcpaaddr.getAddr_Work_Fields_Current_Addr().setValue(true);
            setValueToSubstring(pnd_Ws_Pnd_Process_Orgn_Cde,pnd_Ws_Pnd_Pymnt_S_Start,1,2);                                                                                //Natural: MOVE #PROCESS-ORGN-CDE TO SUBSTRING ( #PYMNT-S-START,1,2 )
            setValueToSubstring(pnd_Ws_Pnd_Process_Orgn_Cde,pnd_Ws_Pnd_Pymnt_S_End,1,2);                                                                                  //Natural: MOVE #PROCESS-ORGN-CDE TO SUBSTRING ( #PYMNT-S-END,1,2 )
            ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                          //Natural: READ FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ = #PYMNT-S-START THRU #PYMNT-S-END
            (
            "READ_PYMNT",
            new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Pnd_Pymnt_S_Start, "And", WcType.BY) ,
            new Wc("ORGN_STATUS_INVRSE_SEQ", "<=", pnd_Ws_Pnd_Pymnt_S_End, WcType.BY) },
            new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
            );
            boolean endOfDataReadPymnt = true;
            boolean firstReadPymnt = true;
            READ_PYMNT:
            while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ_PYMNT")))
            {
                CheckAtStartofData1015();

                if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().greater(0)))
                {
                    atBreakEventRead_Pymnt();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom()))
                            break;
                        else if (condition(Global.isEscapeBottomImmediate()))
                        {
                            endOfDataReadPymnt = false;
                            break;
                        }
                        else if (condition(Global.isEscapeTop()))
                        continue;
                        else if (condition())
                        return;
                    }
                }
                //*                                                                                                                                                       //Natural: AT START OF DATA
                //BEFORE BREAK PROCESSING                                                                                                                                 //Natural: BEFORE BREAK PROCESSING
                //* CTS-0115
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                              //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
                {
                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                            //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 1
                    {
                        pnd_Ws_Pnd_Break_Ind.nadd(1);                                                                                                                     //Natural: ADD 1 TO #BREAK-IND
                    }                                                                                                                                                     //Natural: END-IF
                    //* CTS-0115
                }                                                                                                                                                         //Natural: END-IF
                //END-BEFORE                                                                                                                                              //Natural: END-BEFORE
                //*                                                                                                                                                       //Natural: AT BREAK OF #BREAK-IND
                //* CTS-0115
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                              //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
                {
                                                                                                                                                                          //Natural: PERFORM GEN-OUTPUT
                    sub_Gen_Output();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PYMNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PYMNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
                    sub_Accum_Control_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PYMNT"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PYMNT"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* CTS-0115
                }                                                                                                                                                         //Natural: END-IF
                rEAD_PYMNTPnd_Break_IndOld.setValue(pnd_Ws_Pnd_Break_Ind);                                                                                                //Natural: END-READ
            }
            if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().greater(0)))
            {
                atBreakEventRead_Pymnt(endOfDataReadPymnt);
            }
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM STORE-CNTRL-RECORD
            sub_Store_Cntrl_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
            sub_Print_Control_Report();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *-----
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //* *-------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-OUTPUT
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-STMNT-FIELDS
        //* *---------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-DATA
        //* *-----------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-DATA
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDR-RECORD
        //* *--------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CONTROL-RECORD
        //* *-------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-CNTRL-RECORD
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* *-------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESET-CONTROL-VARIABLES
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AMT-ERROR
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-FIELDS
        //* *------------
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    //*  9-14-99
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Ws_Cnr_Orig_Invrse_Dt.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Orgnl_Invrse_Dte());                                                                  //Natural: ASSIGN #WS-CNR-ORIG-INVRSE-DT := FCP-CONS-PYMNT.CNR-ORGNL-INVRSE-DTE
        pnd_Ws_Pnd_Pymnt_Amt.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                                                                   //Natural: ASSIGN #PYMNT-AMT := FCP-CONS-PYMNT.PYMNT-CHECK-AMT
        pdaFcpaext.getExt().getValue("*").reset();                                                                                                                        //Natural: RESET EXT ( * ) #DET-AMTS
        pnd_Ws_Pnd_Det_Amts.reset();
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #NEW-PYMNT-IND := TRUE
                                                                                                                                                                          //Natural: PERFORM GET-ADDR-RECORD
        sub_Get_Addr_Record();
        if (condition(Global.isEscape())) {return;}
        //*  SET CONTROL RECORD PYMNT INDEXES
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
        sub_Set_Control_Data();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Gen_Output() throws Exception                                                                                                                        //Natural: GEN-OUTPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------
                                                                                                                                                                          //Natural: PERFORM INIT-FIELDS
        sub_Init_Fields();
        if (condition(Global.isEscape())) {return;}
        pdaFcpaext.getExt_Extr().setValuesByName(pdaFcpaaddr.getAddr());                                                                                                  //Natural: MOVE BY NAME ADDR TO EXTR
        pdaFcpaext.getExt_Extr().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                     //Natural: MOVE BY NAME FCP-CONS-PYMNT TO EXTR
        if (condition(pdaFcpaext.getExt_Pymnt_Nme().getValue(2).greater(" ")))                                                                                            //Natural: IF EXT.PYMNT-NME ( 2 ) > ' '
        {
            pdaFcpaext.getExt_C_Pymnt_Nme_And_Addr().setValue(2);                                                                                                         //Natural: ASSIGN C-PYMNT-NME-AND-ADDR := 2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpaext.getExt_C_Pymnt_Nme_And_Addr().setValue(1);                                                                                                         //Natural: ASSIGN C-PYMNT-NME-AND-ADDR := 1
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaext.getExt_Pymnt_Check_Amt().setValue(pnd_Ws_Pnd_Pymnt_Amt);                                                                                               //Natural: ASSIGN EXT.PYMNT-CHECK-AMT := #PYMNT-AMT
        pdaFcpaext.getExt_C_Inv_Acct().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                                      //Natural: ASSIGN C-INV-ACCT := C*INV-ACCT
        pdaFcpaext.getExt_C_Inv_Rtb_Grp().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Rtb_Grp());                                                                //Natural: ASSIGN C-INV-RTB-GRP := C*INV-RTB-GRP
        pdaFcpaext.getExt_C_Pymnt_Ded_Grp().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                                            //Natural: ASSIGN C-PYMNT-DED-GRP := C*PYMNT-DED-GRP
        pdaFcpaext.getExt_Inv_Acct().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).setValuesByName(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct().getValue(1,                  //Natural: MOVE BY NAME FCP-CONS-PYMNT.INV-ACCT ( 1:C-INV-ACCT ) TO EXT.INV-ACCT ( 1:C-INV-ACCT )
            ":",pdaFcpaext.getExt_C_Inv_Acct()));
        FOR02:                                                                                                                                                            //Natural: FOR #J = 1 TO C-INV-ACCT
        for (pnd_Ws_Pnd_J.setValue(1); condition(pnd_Ws_Pnd_J.lessOrEqual(pdaFcpaext.getExt_C_Inv_Acct())); pnd_Ws_Pnd_J.nadd(1))
        {
            pdaFcpaext.getExt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_J).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_J));                        //Natural: ASSIGN EXT.INV-ACCT-CDE ( #J ) := FCP-CONS-PYMNT.INV-ACCT-CDE ( #J )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_J));                               //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := FCP-CONS-PYMNT.INV-ACCT-CDE ( #J )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            pdaFcpaext.getExt_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_J).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Num());                                           //Natural: ASSIGN EXT.INV-ACCT-CDE-N ( #J ) := #FUND-PDA.#INV-ACCT-NUM
            pdaFcpaext.getExt_Inv_Acct_Company().getValue(pnd_Ws_Pnd_J).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde());                                          //Natural: ASSIGN EXT.INV-ACCT-COMPANY ( #J ) := #FUND-PDA.#COMPANY-CDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* NZ-EXT.INV-ACCT-SETTL-AMT          (1:C-INV-ACCT) :=       /* ROXAN
        //*   FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT(1:C-INV-ACCT) +        /*
        //*   FCP-CONS-PYMNT.INV-ACCT-DVDND-AMT(1:C-INV-ACCT)          /*
        //*   NZ-EXT.INV-ACCT-CNTRCT-AMT       (1:C-INV-ACCT) :=       /*
        //*   FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT(1:C-INV-ACCT)          /*
        //*  DERIVE WHEN NEEDED IN PROGRAM BASIS ONLY
        //*  RESET EXT.PYMNT-GROSS-AMT
        //*  ADD EXT.INV-ACCT-SETTL-AMT      (1:C-INV-ACCT)
        //*    TO EXT.PYMNT-GROSS-AMT
        //*  ADD EXT.INV-ACCT-DPI-AMT        (1:C-INV-ACCT)
        //*    TO EXT.PYMNT-GROSS-AMT
        //*  ADD EXT.INV-ACCT-DCI-AMT        (1:C-INV-ACCT)
        //*    TO EXT.PYMNT-GROSS-AMT
        //*  NZ-EXT.CNTRCT-DA-NBR(1)   :=  FCP-CONS-PYMNT.CNTRCT-DA-TIAA-1-NBR
        //*  NZ-EXT.CNTRCT-DA-NBR(2)   :=  FCP-CONS-PYMNT.CNTRCT-DA-TIAA-2-NBR
        //*  NZ-EXT.CNTRCT-DA-NBR(3)   :=  FCP-CONS-PYMNT.CNTRCT-DA-TIAA-3-NBR
        //*  NZ-EXT.CNTRCT-DA-NBR(4)   :=  FCP-CONS-PYMNT.CNTRCT-DA-TIAA-4-NBR
        //*  NZ-EXT.CNTRCT-DA-NBR(5)   :=  FCP-CONS-PYMNT.CNTRCT-DA-TIAA-5-NBR
        //*  NZ-EXT.CNTRCT-DA-NBR(6)   :=  FCP-CONS-PYMNT.CNTRCT-DA-CREF-1-NBR
        //*  WILL BE GENERATED IN NEXT PROGRAM FCPP873
        //*  RESET  EXT.RTB IVC-FROM-RTB-ROLLOVER
        //*   LEON 01/24/01 APPLY T.MCGEE CHANGE
        pdaFcpaext.getExt_Pymnt_Record().reset();                                                                                                                         //Natural: RESET EXT.PYMNT-RECORD EXT.PYMNT-IVC-AMT
        pdaFcpaext.getExt_Pymnt_Ivc_Amt().reset();
        //*                                 /*  FOR 'NZ' TO LUMP-SUM PROJECT
        //* DECIDE FOR FIRST CONDITION
        //*   WHEN    FCP-CONS-PYMNT.CNTRCT-PYMNT-TYPE-IND   = 'N'
        //*       AND FCP-CONS-PYMNT.CNTRCT-STTLMNT-TYPE-IND = 'X'
        //*     EXT.RTB                   := TRUE
        //*   WHEN    FCP-CONS-PYMNT.CNTRCT-PYMNT-TYPE-IND   = 'I'
        //*       AND FCP-CONS-PYMNT.CNTRCT-STTLMNT-TYPE-IND = 'R'
        //*     EXT.IVC-FROM-RTB-ROLLOVER := TRUE
        //*   WHEN NONE
        //*     IGNORE
        //* END-DECIDE
        //*  LEON  08-05-99
        //*  JWO 2010-11
        if (condition(pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") ||                   //Natural: IF EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
            pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") 
            || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))
        {
            short decideConditionsMet1217 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #PROCESS-ORGN-CDE;//Natural: VALUE 'NZ'
            if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("NZ"))))
            {
                decideConditionsMet1217++;
                getWorkFiles().write(2, true, pdaFcpaext.getExt().getValue("*"));                                                                                         //Natural: WRITE WORK FILE 02 VARIABLE EXT ( * )
            }                                                                                                                                                             //Natural: VALUE 'AL'
            else if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("AL"))))
            {
                decideConditionsMet1217++;
                getWorkFiles().write(4, true, pdaFcpaext.getExt().getValue("*"));                                                                                         //Natural: WRITE WORK FILE 04 VARIABLE EXT ( * )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM GEN-STMNT-FIELDS
            sub_Gen_Stmnt_Fields();
            if (condition(Global.isEscape())) {return;}
            //*  DECIDE ON FIRST VALUE OF #PROCESS-ORGN-CDE
            //*  WILL GENERATE ONLY ONE FILE FOR NZ,AL,EW
            getWorkFiles().write(1, true, pdaFcpaext.getExt().getValue("*"));                                                                                             //Natural: WRITE WORK FILE 01 VARIABLE EXT ( * )
            if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW")))                                                                                              //Natural: IF EXT.CNTRCT-ORGN-CDE = 'EW'
            {
                getWorkFiles().write(6, false, ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr(), ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte(), ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde(),  //Natural: WRITE WORK FILE 06 FCP-CONS-PYMNT.CNTRCT-PPCN-NBR FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NBR FCP-CONS-PYMNT.PYMNT-INTRFCE-DTE
                    ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr(), ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Intrfce_Dte());
            }                                                                                                                                                             //Natural: END-IF
            //*     NONE IGNORE
            //*  END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Gen_Stmnt_Fields() throws Exception                                                                                                                  //Natural: GEN-STMNT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpaext.getExt_Cnr_Orgnl_Invrse_Dte().setValue(pnd_Ws_Pnd_Ws_Cnr_Orig_Invrse_Dt);                                                                              //Natural: ASSIGN EXT.CNR-ORGNL-INVRSE-DTE := #WS-CNR-ORIG-INVRSE-DT
        //* #CHECK-SORT-FIELDS.CNTRCT-COMPANY-CDE := EXT.INV-ACCT-COMPANY(1)
    }
    private void sub_Accum_Control_Data() throws Exception                                                                                                                //Natural: ACCUM-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                          //Natural: ASSIGN #MAX-FUND := C*INV-ACCT
        if (condition(ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().equals(getZero())))                                                                                      //Natural: IF #MAX-FUND = 0
        {
            ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().setValue(1);                                                                                                       //Natural: ASSIGN #MAX-FUND := 1
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()).setValuesByName(pdaFcpaext.getExt_Inv_Acct().getValue(1, //Natural: MOVE BY NAME EXT.INV-ACCT ( 1:#MAX-FUND ) TO #CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
        short decideConditionsMet1253 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #PROCESS-ORGN-CDE;//Natural: VALUE 'NZ'
        if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("NZ"))))
        {
            decideConditionsMet1253++;
            DbsUtil.callnat(Fcpnnzc1.class , getCurrentProcessState(), pdaFcpanzc1.getPnd_Fcpanzc1(), pdaFcpanzc2.getPnd_Fcpanzc2(), ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNNZC1' #FCPANZC1 #FCPANZC2 #FCPLNZC2.#MAX-FUND #FCPLNZC2.#CNTRL-INV-ACCT ( 1:#MAX-FUND )
                ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 'AL', 'EW'
        else if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("AL") || pnd_Ws_Pnd_Process_Orgn_Cde.equals("EW"))))
        {
            decideConditionsMet1253++;
            DbsUtil.callnat(Fcpnalc1.class , getCurrentProcessState(), pdaFcpanzc1.getPnd_Fcpanzc1(), pdaFcpanzc2.getPnd_Fcpanzc2(), ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNALC1' #FCPANZC1 #FCPANZC2 #FCPLNZC2.#MAX-FUND #FCPLNZC2.#CNTRL-INV-ACCT ( 1:#MAX-FUND )
                ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                                  //Natural: ASSIGN #NEW-PYMNT-IND := FALSE
        pnd_Ws_Pnd_Det_Amts.nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue("*"));                                                                   //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( * ) TO #DET-AMTS
    }
    private void sub_Set_Control_Data() throws Exception                                                                                                                  //Natural: SET-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pdaFcpanzcn.getPnd_Fcpanzcn().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #FCPANZCN
        short decideConditionsMet1271 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #PROCESS-ORGN-CDE;//Natural: VALUE 'NZ'
        if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("NZ"))))
        {
            decideConditionsMet1271++;
            DbsUtil.callnat(Fcpnnzcn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpanzcn.getPnd_Fcpanzcn(), pdaFcpanzc1.getPnd_Fcpanzc1());      //Natural: CALLNAT 'FCPNNZCN' #FCPACRPT #FCPANZCN #FCPANZC1
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 'AL', 'EW'
        else if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("AL") || pnd_Ws_Pnd_Process_Orgn_Cde.equals("EW"))))
        {
            decideConditionsMet1271++;
            DbsUtil.callnat(Fcpnalcn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpanzcn.getPnd_Fcpanzcn(), pdaFcpanzc1.getPnd_Fcpanzc1());      //Natural: CALLNAT 'FCPNALCN' #FCPACRPT #FCPANZCN #FCPANZC1
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Addr_Record() throws Exception                                                                                                                   //Natural: GET-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpaaddr.getAddr_Cntrct_Ppcn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                  //Natural: ASSIGN ADDR.CNTRCT-PPCN-NBR := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        pdaFcpaaddr.getAddr_Cntrct_Payee_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                                //Natural: ASSIGN ADDR.CNTRCT-PAYEE-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                  //Natural: ASSIGN ADDR.CNTRCT-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        pdaFcpaaddr.getAddr_Cntrct_Invrse_Dte().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                              //Natural: ASSIGN ADDR.CNTRCT-INVRSE-DTE := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
        pdaFcpaaddr.getAddr_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                          //Natural: ASSIGN ADDR.PYMNT-PRCSS-SEQ-NBR := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
        DbsUtil.callnat(Fcpnaddr.class , getCurrentProcessState(), pdaFcpaaddr.getAddr_Work_Fields(), pdaFcpaaddr.getAddr());                                             //Natural: CALLNAT 'FCPNADDR' USING ADDR-WORK-FIELDS ADDR
        if (condition(Global.isEscape())) return;
    }
    private void sub_Check_Control_Record() throws Exception                                                                                                              //Natural: CHECK-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpacntr.getCntrl_Cntl_Orgn_Cde().setValue(pnd_Ws_Pnd_Process_Orgn_Cde);                                                                                       //Natural: ASSIGN CNTRL.CNTL-ORGN-CDE := #PROCESS-ORGN-CDE
        DbsUtil.callnat(Fcpncntr.class , getCurrentProcessState(), pdaFcpacntr.getCntrl_Work_Fields(), pdaFcpacntr.getCntrl());                                           //Natural: CALLNAT 'FCPNCNTR' USING CNTRL-WORK-FIELDS CNTRL
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Return_Cde().equals(getZero())))                                                                             //Natural: IF CNTRL-RETURN-CDE = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"CONTROL RECORD FOUND FOR PRIOR JOB",new TabSetting(77),"***");                                                //Natural: WRITE '***' 25T 'CONTROL RECORD FOUND FOR PRIOR JOB' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Store_Cntrl_Record() throws Exception                                                                                                                //Natural: STORE-CNTRL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        //*  CALLNAT  'FCPN115'  FCPA115    /* CHANGED TO CPWN115
        DbsUtil.callnat(Cpwn115.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                               //Natural: CALLNAT 'CPWN115' CPWA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa115.getCpwa115_Error_Code().notEquals(getZero())))                                                                                           //Natural: IF CPWA115.ERROR-CODE NE 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"ERROR: FCPP371 DETECTS INVALID RETURN-CODE OF",pdaCpwa115.getCpwa115_Error_Code(),new TabSetting(77),"***",NEWLINE,"FROM",pdaCpwa115.getCpwa115_Error_Program(),new  //Natural: WRITE '***' 25T 'ERROR: FCPP371 DETECTS INVALID RETURN-CODE OF' CPWA115.ERROR-CODE 77T '***' / 'FROM' CPWA115.ERROR-PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().setValue(pnd_Ws_Pnd_Process_Orgn_Cde);                                                                              //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-ORGN-CDE := #PROCESS-ORGN-CDE
        pnd_Ws_Pnd_Date.setValue(pdaCpwa115.getCpwa115_Payment_Date());                                                                                                   //Natural: ASSIGN #DATE := CPWA115.PAYMENT-DATE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_X);                                                  //Natural: MOVE EDITED #DATE-X TO FCP-CONS-CNTRL.CNTL-CHECK-DTE ( EM = YYYYMMDD )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte().compute(new ComputeParameters(false, ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte()), DbsField.subtract(100000000, //Natural: SUBTRACT CPWA115.PAYMENT-DATE FROM 100000000 GIVING FCP-CONS-CNTRL.CNTL-INVRSE-DTE
            pdaCpwa115.getCpwa115_Payment_Date()));
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue("*").setValue(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue("*"));                                       //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-CNT ( * ) := #FCPANZC2.#PYMNT-CNT ( * )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue("*").setValue(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Settl_Amt().getValue("*"));                                 //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-GROSS-AMT ( * ) := #FCPANZC2.#SETTL-AMT ( * )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue("*").setValue(pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Net_Pymnt_Amt().getValue("*"));                               //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-NET-AMT ( * ) := #FCPANZC2.#NET-PYMNT-AMT ( * )
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().insertDBRow();                                                                                                                 //Natural: STORE FCP-CONS-CNTRL
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue("*").setValue(true);                                                                                       //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( * ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                       //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(43).setValue(false);                                                                                       //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 43 ) := FALSE
        if (condition(pnd_Ws_Pnd_Process_Orgn_Cde.equals("AL") || pnd_Ws_Pnd_Process_Orgn_Cde.equals("EW")))                                                              //Natural: IF #PROCESS-ORGN-CDE = 'AL' OR = 'EW'
        {
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1,":",3).setValue(false);                                                                              //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1:3 ) := FALSE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(17,":",19).setValue(false);                                                                            //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 17:19 ) := FALSE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(47).setValue(false);                                                                                   //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 47 ) := FALSE
            if (condition(pnd_Ws_Pnd_Process_Orgn_Cde.equals("EW") && pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue(43).greater(getZero())))                       //Natural: IF #PROCESS-ORGN-CDE = 'EW' AND #PYMNT-CNT ( 43 ) > 0
            {
                pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(43).setValue(true);                                                                                //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 43 ) := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet1342 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #PROCESS-ORGN-CDE;//Natural: VALUE 'NZ'
        if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("NZ"))))
        {
            decideConditionsMet1342++;
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("    NEW ANNUITIZATION CONTROL RECORD");                                                                     //Natural: ASSIGN #FCPACRPT.#TITLE := '    NEW ANNUITIZATION CONTROL RECORD'
            //*    CALLNAT  'FCPNNZC2'  #FCPANZC2 #FCPACRPT   /* 12/01/99  ROXAN
            DbsUtil.callnat(Fcpncnt2.class , getCurrentProcessState(), pdaFcpanzc2.getPnd_Fcpanzc2(), pdaFcpacrpt.getPnd_Fcpacrpt(), pnd_Ws_Pnd_Process_Orgn_Cde);        //Natural: CALLNAT 'FCPNCNT2' #FCPANZC2 #FCPACRPT #PROCESS-ORGN-CDE
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("AL"))))
        {
            decideConditionsMet1342++;
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("      ANNUITY LOAN CONTROL RECORD");                                                                        //Natural: ASSIGN #FCPACRPT.#TITLE := '      ANNUITY LOAN CONTROL RECORD'
            //*    CALLNAT  'FCPNALC2'  #FCPANZC2 #FCPACRPT   /* 11/30/99   ROXAN
            DbsUtil.callnat(Fcpncnt2.class , getCurrentProcessState(), pdaFcpanzc2.getPnd_Fcpanzc2(), pdaFcpacrpt.getPnd_Fcpacrpt(), pnd_Ws_Pnd_Process_Orgn_Cde);        //Natural: CALLNAT 'FCPNCNT2' #FCPANZC2 #FCPACRPT #PROCESS-ORGN-CDE
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: VALUE 'EW'
        else if (condition((pnd_Ws_Pnd_Process_Orgn_Cde.equals("EW"))))
        {
            decideConditionsMet1342++;
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("   ELECTRONIC WARRANTS CONTROL RECORD");                                                                    //Natural: ASSIGN #FCPACRPT.#TITLE := '   ELECTRONIC WARRANTS CONTROL RECORD'
            DbsUtil.callnat(Fcpncnt2.class , getCurrentProcessState(), pdaFcpanzc2.getPnd_Fcpanzc2(), pdaFcpacrpt.getPnd_Fcpacrpt(), pnd_Ws_Pnd_Process_Orgn_Cde);        //Natural: CALLNAT 'FCPNCNT2' #FCPANZC2 #FCPACRPT #PROCESS-ORGN-CDE
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Reset_Control_Variables() throws Exception                                                                                                           //Natural: RESET-CONTROL-VARIABLES
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pdaFcpanzcn.getPnd_Fcpanzcn().reset();                                                                                                                            //Natural: RESET #FCPANZCN #FCPANZC1 #FCPANZC2 #FCPACRPT
        pdaFcpanzc1.getPnd_Fcpanzc1().reset();
        pdaFcpanzc2.getPnd_Fcpanzc2().reset();
        pdaFcpacrpt.getPnd_Fcpacrpt().reset();
    }
    private void sub_Amt_Error() throws Exception                                                                                                                         //Natural: AMT-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "***",new TabSetting(25),"PAYMENT AND ACTUAL FUND AMOUNTS DO NOT AGREE",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN    :",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde(),new  //Natural: WRITE '***' 25T 'PAYMENT AND ACTUAL FUND AMOUNTS DO NOT AGREE' 77T '***' / '***' 25T 'ORIGIN    :' FCP-CONS-PYMNT.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'PPCN      :' FCP-CONS-PYMNT.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'SEQUENCE# :' FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NBR 77T '***' / '***' 25T 'PYMNT AMT :' #PYMNT-AMT 77T '***' / '***' 25T 'NET AMTS  :' #DET-AMTS 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PPCN      :",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr(),new TabSetting(77),"***",NEWLINE,"***",new 
            TabSetting(25),"SEQUENCE# :",ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PYMNT AMT :",pnd_Ws_Pnd_Pymnt_Amt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9.99"),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"NET AMTS  :",pnd_Ws_Pnd_Det_Amts, new ReportEditMask 
            ("-Z,ZZZ,ZZ9.99"),new TabSetting(77),"***");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.terminate(52);  if (true) return;                                                                                                                         //Natural: TERMINATE 52
    }
    //*  12/15/99
    private void sub_Init_Fields() throws Exception                                                                                                                       //Natural: INIT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------------
        pdaFcpaext.getExt().getValue("*").reset();                                                                                                                        //Natural: RESET EXT ( * ) EXT.CNTRCT-INVRSE-DTE EXT.PYMNT-PRCSS-SEQ-NBR EXT.PYMNT-TOTAL-PAGES EXT.FUNDS-ON-PAGE ( * ) EXT.CNTRCT-UNQ-ID-NBR EXT.PYMNT-RECORD EXT.ANNT-CTZNSHP-CDE EXT.ANNT-SOC-SEC-IND EXT.ANNT-SOC-SEC-NBR EXT.CNTRCT-DVDND-PAYEE-IND EXT.CNTRCT-RQST-SETTL-DTE EXT.CNTRCT-RQST-DTE EXT.CNTRCT-RQST-SETTL-TME EXT.CNTRCT-OPTION-CDE EXT.CNTRCT-MODE-CDE EXT.CNTRCT-CANCEL-RDRW-AMT EXT.CNTRCT-HOLD-TME EXT.PYMNT-PAY-TYPE-REQ-IND EXT.PYMNT-CHECK-DTE EXT.PYMNT-CYCLE-DTE EXT.PYMNT-EFT-DTE EXT.PYMNT-RQST-PCT EXT.PYMNT-RQST-AMT EXT.PYMNT-CHECK-NBR EXT.PYMNT-CHECK-SCRTY-NBR EXT.PYMNT-CHECK-AMT EXT.PYMNT-SETTLMNT-DTE EXT.PYMNT-CHECK-SEQ-NBR EXT.PYMNT-RPRNT-RQUST-DTE EXT.PYMNT-RPRNT-DTE EXT.PYMNT-ACCTG-DTE EXT.PYMNT-INTRFCE-DTE EXT.PYMNT-TIAA-MD-AMT EXT.PYMNT-CREF-MD-AMT EXT.PYMNT-IA-ISSUE-DTE EXT.PYMNT-EFT-TRANSIT-ID EXT.PYMNT-DOB EXT.PYMNT-DEATH-DTE EXT.PYMNT-PROOF-DTE EXT.C-INV-RTB-GRP EXT.C-PYMNT-DED-GRP EXT.C-INV-ACCT EXT.C-PYMNT-NME-AND-ADDR EXT.CNR-CS-PYMNT-INTRFCE-DTE EXT.CNR-CS-PYMNT-ACCTG-DTE EXT.CNR-CS-PYMNT-CHECK-DTE EXT.CNR-CS-RQUST-DTE EXT.CNR-CS-RQUST-TME EXT.CNR-RDRW-RQUST-TME EXT.CNR-ORGNL-INVRSE-DTE EXT.CNR-ORGNL-PRCSS-SEQ-NBR EXT.CNR-ORGNL-PYMNT-ACCTG-DTE EXT.CNR-ORGNL-PYMNT-INTRFCE-DTE EXT.CNR-RDRW-RQUST-DTE EXT.CNR-RDRW-PYMNT-INTRFCE-DTE EXT.CNR-RDRW-PYMNT-ACCTG-DTE EXT.CNR-RDRW-PYMNT-CHECK-DTE EXT.CNR-RPLCMNT-INVRSE-DTE EXT.CNR-RPLCMNT-PRCSS-SEQ-NBR EXT.TAX-FED-C-ALLOW-CNT EXT.TAX-FED-C-FIXED-AMT EXT.TAX-FED-C-FIXED-PCT EXT.TAX-STA-C-ALLOW-CNT EXT.TAX-STA-C-FIXED-AMT EXT.TAX-STA-C-FIXED-PCT EXT.TAX-STA-C-SPOUSE-CNT EXT.TAX-STA-C-BLIND-CNT EXT.TAX-LOC-C-FIXED-AMT EXT.TAX-LOC-C-FIXED-PCT EXT.PYMNT-ADDR-LAST-CHG-DTE ( * ) EXT.PYMNT-ADDR-LAST-CHG-TME ( * ) EXT.PYMNT-DED-CDE ( * ) EXT.PYMNT-DED-AMT ( * ) EXT.INV-ACCT-CDE-N ( * ) EXT.INV-ACCT-DED-CDE ( * ) EXT.INV-ACCT-DED-AMT ( * ) EXT.INV-ACCT-UNIT-QTY ( * ) EXT.INV-ACCT-UNIT-VALUE ( * ) EXT.INV-ACCT-SETTL-AMT ( * ) EXT.INV-ACCT-IVC-AMT ( * ) EXT.INV-ACCT-DCI-AMT ( * ) EXT.INV-ACCT-DPI-AMT ( * ) EXT.INV-ACCT-START-ACCUM-AMT ( * ) EXT.INV-ACCT-END-ACCUM-AMT ( * ) EXT.INV-ACCT-DVDND-AMT ( * ) EXT.INV-ACCT-NET-PYMNT-AMT ( * ) EXT.INV-ACCT-ADJ-IVC-AMT ( * ) EXT.INV-ACCT-FDRL-TAX-AMT ( * ) EXT.INV-ACCT-STATE-TAX-AMT ( * ) EXT.INV-ACCT-LOCAL-TAX-AMT ( * ) EXT.INV-ACCT-EXP-AMT ( * ) EXT.INV-ACCT-FED-DCI-TAX-AMT ( * ) EXT.INV-ACCT-STA-DCI-TAX-AMT ( * ) EXT.INV-ACCT-LOC-DCI-TAX-AMT ( * ) EXT.INV-ACCT-FED-DPI-TAX-AMT ( * ) EXT.INV-ACCT-STA-DPI-TAX-AMT ( * ) EXT.INV-ACCT-LOC-DPI-TAX-AMT ( * )
        pdaFcpaext.getExt_Cntrct_Invrse_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr().reset();
        pdaFcpaext.getExt_Pymnt_Total_Pages().reset();
        pdaFcpaext.getExt_Funds_On_Page().getValue("*").reset();
        pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr().reset();
        pdaFcpaext.getExt_Pymnt_Record().reset();
        pdaFcpaext.getExt_Annt_Ctznshp_Cde().reset();
        pdaFcpaext.getExt_Annt_Soc_Sec_Ind().reset();
        pdaFcpaext.getExt_Annt_Soc_Sec_Nbr().reset();
        pdaFcpaext.getExt_Cntrct_Dvdnd_Payee_Ind().reset();
        pdaFcpaext.getExt_Cntrct_Rqst_Settl_Dte().reset();
        pdaFcpaext.getExt_Cntrct_Rqst_Dte().reset();
        pdaFcpaext.getExt_Cntrct_Rqst_Settl_Tme().reset();
        pdaFcpaext.getExt_Cntrct_Option_Cde().reset();
        pdaFcpaext.getExt_Cntrct_Mode_Cde().reset();
        pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Amt().reset();
        pdaFcpaext.getExt_Cntrct_Hold_Tme().reset();
        pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().reset();
        pdaFcpaext.getExt_Pymnt_Check_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Cycle_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Eft_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Rqst_Pct().reset();
        pdaFcpaext.getExt_Pymnt_Rqst_Amt().reset();
        pdaFcpaext.getExt_Pymnt_Check_Nbr().reset();
        pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr().reset();
        pdaFcpaext.getExt_Pymnt_Check_Amt().reset();
        pdaFcpaext.getExt_Pymnt_Settlmnt_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Check_Seq_Nbr().reset();
        pdaFcpaext.getExt_Pymnt_Rprnt_Rqust_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Rprnt_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Acctg_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Intrfce_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Tiaa_Md_Amt().reset();
        pdaFcpaext.getExt_Pymnt_Cref_Md_Amt().reset();
        pdaFcpaext.getExt_Pymnt_Ia_Issue_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Eft_Transit_Id().reset();
        pdaFcpaext.getExt_Pymnt_Dob().reset();
        pdaFcpaext.getExt_Pymnt_Death_Dte().reset();
        pdaFcpaext.getExt_Pymnt_Proof_Dte().reset();
        pdaFcpaext.getExt_C_Inv_Rtb_Grp().reset();
        pdaFcpaext.getExt_C_Pymnt_Ded_Grp().reset();
        pdaFcpaext.getExt_C_Inv_Acct().reset();
        pdaFcpaext.getExt_C_Pymnt_Nme_And_Addr().reset();
        pdaFcpaext.getExt_Cnr_Cs_Pymnt_Intrfce_Dte().reset();
        pdaFcpaext.getExt_Cnr_Cs_Pymnt_Acctg_Dte().reset();
        pdaFcpaext.getExt_Cnr_Cs_Pymnt_Check_Dte().reset();
        pdaFcpaext.getExt_Cnr_Cs_Rqust_Dte().reset();
        pdaFcpaext.getExt_Cnr_Cs_Rqust_Tme().reset();
        pdaFcpaext.getExt_Cnr_Rdrw_Rqust_Tme().reset();
        pdaFcpaext.getExt_Cnr_Orgnl_Invrse_Dte().reset();
        pdaFcpaext.getExt_Cnr_Orgnl_Prcss_Seq_Nbr().reset();
        pdaFcpaext.getExt_Cnr_Orgnl_Pymnt_Acctg_Dte().reset();
        pdaFcpaext.getExt_Cnr_Orgnl_Pymnt_Intrfce_Dte().reset();
        pdaFcpaext.getExt_Cnr_Rdrw_Rqust_Dte().reset();
        pdaFcpaext.getExt_Cnr_Rdrw_Pymnt_Intrfce_Dte().reset();
        pdaFcpaext.getExt_Cnr_Rdrw_Pymnt_Acctg_Dte().reset();
        pdaFcpaext.getExt_Cnr_Rdrw_Pymnt_Check_Dte().reset();
        pdaFcpaext.getExt_Cnr_Rplcmnt_Invrse_Dte().reset();
        pdaFcpaext.getExt_Cnr_Rplcmnt_Prcss_Seq_Nbr().reset();
        pdaFcpaext.getExt_Tax_Fed_C_Allow_Cnt().reset();
        pdaFcpaext.getExt_Tax_Fed_C_Fixed_Amt().reset();
        pdaFcpaext.getExt_Tax_Fed_C_Fixed_Pct().reset();
        pdaFcpaext.getExt_Tax_Sta_C_Allow_Cnt().reset();
        pdaFcpaext.getExt_Tax_Sta_C_Fixed_Amt().reset();
        pdaFcpaext.getExt_Tax_Sta_C_Fixed_Pct().reset();
        pdaFcpaext.getExt_Tax_Sta_C_Spouse_Cnt().reset();
        pdaFcpaext.getExt_Tax_Sta_C_Blind_Cnt().reset();
        pdaFcpaext.getExt_Tax_Loc_C_Fixed_Amt().reset();
        pdaFcpaext.getExt_Tax_Loc_C_Fixed_Pct().reset();
        pdaFcpaext.getExt_Pymnt_Addr_Last_Chg_Dte().getValue("*").reset();
        pdaFcpaext.getExt_Pymnt_Addr_Last_Chg_Tme().getValue("*").reset();
        pdaFcpaext.getExt_Pymnt_Ded_Cde().getValue("*").reset();
        pdaFcpaext.getExt_Pymnt_Ded_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Cde_N().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Ded_Cde().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Ded_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Unit_Qty().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Unit_Value().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Start_Accum_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_End_Accum_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Adj_Ivc_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Exp_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Fed_Dci_Tax_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Sta_Dci_Tax_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Loc_Dci_Tax_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Fed_Dpi_Tax_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Sta_Dpi_Tax_Amt().getValue("*").reset();
        pdaFcpaext.getExt_Inv_Acct_Loc_Dpi_Tax_Amt().getValue("*").reset();
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventRead_Pymnt() throws Exception {atBreakEventRead_Pymnt(false);}
    private void atBreakEventRead_Pymnt(boolean endOfData) throws Exception
    {
        boolean pnd_Ws_Pnd_Break_IndIsBreak = pnd_Ws_Pnd_Break_Ind.isBreak(endOfData);
        if (condition(pnd_Ws_Pnd_Break_IndIsBreak))
        {
            //* CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                if (condition(pnd_Ws_Pnd_Pymnt_Amt.notEquals(pnd_Ws_Pnd_Det_Amts)))                                                                                       //Natural: IF #PYMNT-AMT NE #DET-AMTS
                {
                                                                                                                                                                          //Natural: PERFORM AMT-ERROR
                    sub_Amt_Error();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  NOT EOF
                if (condition(pnd_Ws_Pnd_Break_Ind.notEquals(rEAD_PYMNTPnd_Break_IndOld)))                                                                                //Natural: IF #BREAK-IND NE OLD ( #BREAK-IND )
                {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                    sub_Init_New_Pymnt();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //* CTS-0115
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void CheckAtStartofData1015() throws Exception
    {
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAtStartOfData()))
        {
            //* CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                sub_Init_New_Pymnt();
                if (condition(Global.isEscape())) {return;}
                //* CTS-0115
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
