/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:16 PM
**        * FROM NATURAL PROGRAM : Fcpp378
************************************************************
**        * FILE NAME            : Fcpp378.java
**        * CLASS NAME           : Fcpp378
**        * INSTANCE NAME        : Fcpp378
************************************************************
************************************************************************
* PROGRAM  : FCPP378
* SYSTEM   : CPS
* TITLE    : EXTRACT
* GENERATED: JUL 29,93 AT 04:58 PM
* FUNCTION : THIS PROGRAM WILL EXTRACT RECORDS FOR A STATED
*            PERIOD OF TIME.
*
* SINGLE SUM AND DAILY SETTLEMENTS SYSTEMS HAVE NO INSTALLMENTS (EVERY
* RECORD IS INSTALLMENT ONE). NEVERTHELESS THIS PROGRAM INCLUDES
* INSTALLMENT PROCESSING SO IT COULD BE USED BY OTHER SYSTEMS.
* HISTORY
*
*  03/12/99 :  R. CARREON
*             ADDITION OF PYMNT-SPOUSE-PAY-STATS IN FCPL378
* 09/11/00 : A. YOUNG    -  RE-COMPILED DUE TO FCPLPMNT.
*
*    11/02 : ROXAN       -  FCPLPMNT EXPANDED FOR EGTRRA
*    12/02 : ROXAN       -  FCPN115 WAS REPLACED BY CPWA115
*                           ALL REFERENCE TO FCPA115 WAS CHG TO CPWA115
*    05/08 : AER         -  RESTOW FOR ROTH - ROTH-MAJOR1
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
* 4/2017    : JJG - PIN EXPANSION RESTOW
* 08/23/2019 : REPLACING PDQ MODULES WITH ADS MODULES I.E.
*              PDQA360,PDQA362,PDQN360,PDQN362 TO
*              ADSA360,ADSA362,ADSN360,ADSN362 RESPECTIVELY. -TAG:VIKRAM
* 01/15/2020: CTS CPS SUNSET (CHG694525) TAG: CTS-0115
*             MODIFED TO PROCESS STOPS ONLY.IT WILL NOT REISSUE CHECKS.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp378 extends BLNatBase
{
    // Data Areas
    private PdaFcpaaddr pdaFcpaaddr;
    private PdaFcpacntr pdaFcpacntr;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpasscn pdaFcpasscn;
    private PdaFcpassc1 pdaFcpassc1;
    private PdaFcpassc2 pdaFcpassc2;
    private PdaCpwa115 pdaCpwa115;
    private PdaFcpa199a pdaFcpa199a;
    private PdaAdsa360 pdaAdsa360;
    private PdaAdsa362 pdaAdsa362;
    private LdaFcplcntu ldaFcplcntu;
    private LdaFcplpmnt ldaFcplpmnt;
    private LdaFcplssc2 ldaFcplssc2;
    private LdaFcpl378 ldaFcpl378;
    private LdaFcpl378d ldaFcpl378d;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Plan_S;
    private DbsField pnd_Plan_S_Cntrct_Rcrd_Typ;
    private DbsField pnd_Plan_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Plan_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Plan_S_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Plan_S_Cntl_Check_Dte;

    private DbsGroup pnd_Plan_S__R_Field_1;
    private DbsField pnd_Plan_S_Pnd_Plan_Superde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Pymnt_S_Start;
    private DbsField pnd_Ws_Pnd_Pymnt_S_End;
    private DbsField pnd_Ws_Pnd_Ws_Company;
    private DbsField pnd_Ws_Pnd_Stmnt_Lines;
    private DbsField pnd_Ws_Pnd_Break_Ind;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Counter;
    private DbsField pnd_Ws_Pnd_Counter_Ss;
    private DbsField pnd_Ws_Pnd_Counter_Ds;
    private DbsField pnd_Ws_Pnd_Amt;
    private DbsField pnd_Ws_Pnd_Amt1;
    private DbsField pnd_Ws_Pnd_Date;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Date_Alpha;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Date_Filler;
    private DbsField pnd_Ws_Pnd_Date_7;
    private DbsField pnd_Form;

    private DbsRecord internalLoopRecord;
    private DbsField rEAD_PYMNTPymnt_Check_AmtOld;
    private DbsField rEAD_PYMNTPnd_Break_IndOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaaddr = new PdaFcpaaddr(localVariables);
        pdaFcpacntr = new PdaFcpacntr(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpasscn = new PdaFcpasscn(localVariables);
        pdaFcpassc1 = new PdaFcpassc1(localVariables);
        pdaFcpassc2 = new PdaFcpassc2(localVariables);
        pdaCpwa115 = new PdaCpwa115(localVariables);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        pdaAdsa360 = new PdaAdsa360(localVariables);
        pdaAdsa362 = new PdaAdsa362(localVariables);
        ldaFcplcntu = new LdaFcplcntu();
        registerRecord(ldaFcplcntu);
        registerRecord(ldaFcplcntu.getVw_fcp_Cons_Cntrl());
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        ldaFcplssc2 = new LdaFcplssc2();
        registerRecord(ldaFcplssc2);
        ldaFcpl378 = new LdaFcpl378();
        registerRecord(ldaFcpl378);
        ldaFcpl378d = new LdaFcpl378d();
        registerRecord(ldaFcpl378d);
        registerRecord(ldaFcpl378d.getVw_fcp_Cons_Plan());

        // Local Variables

        pnd_Plan_S = localVariables.newGroupInRecord("pnd_Plan_S", "#PLAN-S");
        pnd_Plan_S_Cntrct_Rcrd_Typ = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1);
        pnd_Plan_S_Cntrct_Orgn_Cde = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Plan_S_Cntrct_Ppcn_Nbr = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Plan_S_Pymnt_Prcss_Seq_Nbr = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pnd_Plan_S_Cntl_Check_Dte = pnd_Plan_S.newFieldInGroup("pnd_Plan_S_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE);

        pnd_Plan_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Plan_S__R_Field_1", "REDEFINE", pnd_Plan_S);
        pnd_Plan_S_Pnd_Plan_Superde = pnd_Plan_S__R_Field_1.newFieldInGroup("pnd_Plan_S_Pnd_Plan_Superde", "#PLAN-SUPERDE", FieldType.STRING, 26);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Pymnt_S_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_S_Start", "#PYMNT-S-START", FieldType.STRING, 4);
        pnd_Ws_Pnd_Pymnt_S_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_S_End", "#PYMNT-S-END", FieldType.STRING, 4);
        pnd_Ws_Pnd_Ws_Company = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Company", "#WS-COMPANY", FieldType.STRING, 1);
        pnd_Ws_Pnd_Stmnt_Lines = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Stmnt_Lines", "#STMNT-LINES", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Break_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Break_Ind", "#BREAK-IND", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Counter = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Counter", "#COUNTER", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Counter_Ss = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Counter_Ss", "#COUNTER-SS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Counter_Ds = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Counter_Ds", "#COUNTER-DS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt", "#AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Amt1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Amt1", "#AMT1", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Date);
        pnd_Ws_Pnd_Date_Alpha = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Date_Alpha", "#DATE-ALPHA", FieldType.STRING, 8);

        pnd_Ws__R_Field_3 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Date);
        pnd_Ws_Pnd_Date_Filler = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Date_Filler", "#DATE-FILLER", FieldType.STRING, 1);
        pnd_Ws_Pnd_Date_7 = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Date_7", "#DATE-7", FieldType.NUMERIC, 7);
        pnd_Form = localVariables.newFieldInRecord("pnd_Form", "#FORM", FieldType.STRING, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        rEAD_PYMNTPymnt_Check_AmtOld = internalLoopRecord.newFieldInRecord("READ_PYMNT_Pymnt_Check_Amt_OLD", "Pymnt_Check_Amt_OLD", FieldType.PACKED_DECIMAL, 
            9, 2);
        rEAD_PYMNTPnd_Break_IndOld = internalLoopRecord.newFieldInRecord("READ_PYMNT_Pnd_Break_Ind_OLD", "Pnd_Break_Ind_OLD", FieldType.PACKED_DECIMAL, 
            7);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcplcntu.initializeValues();
        ldaFcplpmnt.initializeValues();
        ldaFcplssc2.initializeValues();
        ldaFcpl378.initializeValues();
        ldaFcpl378d.initializeValues();

        localVariables.reset();
        pnd_Plan_S_Cntrct_Rcrd_Typ.setInitialValue("6");
        pnd_Ws_Pnd_Pymnt_S_Start.setInitialValue("H'0000C400'");
        pnd_Ws_Pnd_Pymnt_S_End.setInitialValue("H'0000C4FF'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp378() throws Exception
    {
        super("Fcpp378");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 15 ) LS = 133 PS = 58 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T 'SINGLE SUM BATCH EXTRACT CONTROL REPORT' 120T 'REPORT: RPT1' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*  CHECK FOR SINGLE SUM CONTROL RECORD.
                                                                                                                                                                          //Natural: PERFORM CHECK-CONTROL-RECORD
        sub_Check_Control_Record();
        if (condition(Global.isEscape())) {return;}
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPASSC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPASSC2.#ACCUM-TRUTH-TABLE ( 6 )
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
        //*  READ SINGLE SUM AND SINGLE SUM CANCEL AND REDRAW CASES THAT ARE NOT
        //*  PROCESSED
        pdaFcpaaddr.getAddr_Work_Fields_Addr_Abend_Ind().setValue(true);                                                                                                  //Natural: MOVE TRUE TO ADDR-ABEND-IND CURRENT-ADDR
        pdaFcpaaddr.getAddr_Work_Fields_Current_Addr().setValue(true);
        setValueToSubstring("SS",pnd_Ws_Pnd_Pymnt_S_Start,1,2);                                                                                                           //Natural: MOVE 'SS' TO SUBSTRING ( #PYMNT-S-START,1,2 )
        setValueToSubstring("SS",pnd_Ws_Pnd_Pymnt_S_End,1,2);                                                                                                             //Natural: MOVE 'SS' TO SUBSTRING ( #PYMNT-S-END,1,2 )
                                                                                                                                                                          //Natural: PERFORM READ-PYMNTS
        sub_Read_Pymnts();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Counter_Ss.setValue(pnd_Ws_Pnd_Counter);                                                                                                               //Natural: MOVE #COUNTER TO #COUNTER-SS
        setValueToSubstring("DS",pnd_Ws_Pnd_Pymnt_S_Start,1,2);                                                                                                           //Natural: MOVE 'DS' TO SUBSTRING ( #PYMNT-S-START,1,2 )
        setValueToSubstring("DS",pnd_Ws_Pnd_Pymnt_S_End,1,2);                                                                                                             //Natural: MOVE 'DS' TO SUBSTRING ( #PYMNT-S-END,1,2 )
                                                                                                                                                                          //Natural: PERFORM READ-PYMNTS
        sub_Read_Pymnts();
        if (condition(Global.isEscape())) {return;}
        pnd_Ws_Pnd_Counter_Ds.setValue(pnd_Ws_Pnd_Counter);                                                                                                               //Natural: MOVE #COUNTER TO #COUNTER-DS
                                                                                                                                                                          //Natural: PERFORM STORE-CNTRL-RECORD
        sub_Store_Cntrl_Record();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PYMNTS
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-10
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-20
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-30
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RECORD-TYPE-40-45
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-ON-WHICH-SIDE-OF-PAPER
        //* *********************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-MIDDLE-NAME
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-DATA
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-DATA
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDR-RECORD
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-PLAN-RECORD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-CONTROL-RECORD
        //* *************************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: STORE-CNTRL-RECORD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: AMT-ERROR
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Read_Pymnts() throws Exception                                                                                                                       //Natural: READ-PYMNTS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ = #PYMNT-S-START THRU #PYMNT-S-END
        (
        "READ_PYMNT",
        new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Pnd_Pymnt_S_Start, "And", WcType.BY) ,
        new Wc("ORGN_STATUS_INVRSE_SEQ", "<=", pnd_Ws_Pnd_Pymnt_S_End, WcType.BY) },
        new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
        );
        boolean endOfDataReadPymnt = true;
        boolean firstReadPymnt = true;
        READ_PYMNT:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ_PYMNT")))
        {
            CheckAtStartofData960();

            if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().greater(0)))
            {
                atBreakEventRead_Pymnt();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadPymnt = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            //*  CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 1
                {
                    pnd_Ws_Pnd_Break_Ind.nadd(1);                                                                                                                         //Natural: ADD 1 TO #BREAK-IND
                }                                                                                                                                                         //Natural: END-IF
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*  CTS-0115                                                                                                                                                 //Natural: AT BREAK OF #BREAK-IND
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-20
                sub_Process_Record_Type_20();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PYMNT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PYMNT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
                sub_Accum_Control_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("READ_PYMNT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("READ_PYMNT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
            rEAD_PYMNTPymnt_Check_AmtOld.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt());                                                                       //Natural: END-READ
            rEAD_PYMNTPnd_Break_IndOld.setValue(pnd_Ws_Pnd_Break_Ind);
        }
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().greater(0)))
        {
            atBreakEventRead_Pymnt(endOfDataReadPymnt);
        }
        if (Global.isEscape()) return;
        //*  MOVE *COUNTER(READ-PYMNT.)      TO #COUNTER   /* CTS-0115
    }
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Ws_Pnd_Stmnt_Lines.reset();                                                                                                                                   //Natural: RESET #STMNT-LINES #PYMNT-EXT-KEY #AMT
        ldaFcpl378.getPnd_Pymnt_Ext_Key().reset();
        pnd_Ws_Pnd_Amt.reset();
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #NEW-PYMNT-IND := TRUE
        //*  SET CONTROL RECORD PYMNT INDEXES
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
        sub_Set_Control_Data();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Process_Record_Type_10() throws Exception                                                                                                            //Natural: PROCESS-RECORD-TYPE-10
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10().reset();                                                                                                //Natural: RESET #RECORD-TYPE-10 #FORM
        pnd_Form.reset();
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Seq_Num().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                  //Natural: MOVE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM TO #KEY-SEQ-NUM
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Inst_Num().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr());                                                    //Natural: MOVE FCP-CONS-PYMNT.PYMNT-INSTMT-NBR TO #KEY-INST-NUM
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Hold_Cde().equals(" ")))                                                                                       //Natural: IF FCP-CONS-PYMNT.CNTRCT-HOLD-CDE = ' '
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code().setValue("0000");                                                                                //Natural: MOVE '0000' TO #KEY-CONTRACT-HOLD-CODE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Contract_Hold_Code().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Hold_Cde());                                       //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-HOLD-CDE TO #KEY-CONTRACT-HOLD-CODE
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Cntrct_Cmbn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr());                                              //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-CMBN-NBR TO #KEY-CNTRCT-CMBN-NBR
        //*  SAVE LAST 7 CHARS OF INVERSE DATE IN THE CHECK # FOR SORTING.
        pnd_Ws_Pnd_Date.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                                                      //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE TO #DATE
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Pymnt_Check_Nbr().setValue(pnd_Ws_Pnd_Date_7);                                                                            //Natural: MOVE #DATE-7 TO #KEY-PYMNT-CHECK-NBR
        ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                    //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #RECORD-TYPE-10
        //*  JFT - DETERMINE IF CHECK OR FORM
        if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(2) || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT1")   //Natural: IF #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND = 2 OR #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ALT1' OR = 'ALT2'
            || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT2")))
        {
            pnd_Form.setValue("S");                                                                                                                                       //Natural: ASSIGN #FORM := 'S'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(((ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Pay_Type_Req_Ind().equals(6) && ! ((ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT1")  //Natural: IF #REC-TYPE-10-DETAIL.PYMNT-PAY-TYPE-REQ-IND = 6 AND NOT ( #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ALT1' OR = 'ALT2' ) AND #REC-TYPE-10-DETAIL.CNTRCT-TYPE-CDE = 'R'
            || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ALT2")))) && ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Type_Cde().equals("R"))))
        {
            pnd_Form.setValue("S");                                                                                                                                       //Natural: ASSIGN #FORM := 'S'
        }                                                                                                                                                                 //Natural: END-IF
        //*  ACFS - SINGLE SUM, DIRECT TRANSFERS EXCEPT IRAD, ALTERNATE PAYEE,
        //*         NOT CSR
        if (condition((((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("SS") && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde().getSubstring(1,3).equals("ALT"))  //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'SS' AND SUBSTR ( FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE,1,3 ) = 'ALT' AND FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' AND ( FCP-CONS-PYMNT.CNTRCT-LOB-CDE = 'RAD' OR = 'GRAD' OR = 'SRAD' OR = 'GSRD' )
            && ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ")) && (((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Lob_Cde().equals("RAD") 
            || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Lob_Cde().equals("GRAD")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Lob_Cde().equals("SRAD")) || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Lob_Cde().equals("GSRD")))))
        {
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().setValue(true);                                                                                               //Natural: MOVE TRUE TO #ACFS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().setValue(false);                                                                                              //Natural: MOVE FALSE TO #ACFS
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Record_Type_20() throws Exception                                                                                                            //Natural: PROCESS-RECORD-TYPE-20
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Record_Type_20().reset();                                                                                                //Natural: RESET #RECORD-TYPE-20
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(20);                                                                                               //Natural: MOVE 20 TO #KEY-REC-LVL-#
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Record_Type_20_Grp1().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                               //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #RECORD-TYPE-20-GRP1
        ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                   //Natural: MOVE FCP-CONS-PYMNT.C*PYMNT-DED-GRP TO #PYMNT-DED-GRP-TOP
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp().notEquals(getZero())))                                                                      //Natural: IF FCP-CONS-PYMNT.C*PYMNT-DED-GRP NE 0
        {
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pymnt_Ded_Cde().getValue(1,":",ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top()).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue(1, //Natural: MOVE FCP-CONS-PYMNT.PYMNT-DED-CDE ( 1:#PYMNT-DED-GRP-TOP ) TO #REC-TYPE-20-DETAIL.PYMNT-DED-CDE ( 1:#PYMNT-DED-GRP-TOP )
                ":",ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top()));
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pymnt_Ded_Payee_Cde().getValue(1,":",ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top()).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Payee_Cde().getValue(1, //Natural: MOVE FCP-CONS-PYMNT.PYMNT-DED-PAYEE-CDE ( 1:#PYMNT-DED-GRP-TOP ) TO #REC-TYPE-20-DETAIL.PYMNT-DED-PAYEE-CDE ( 1:#PYMNT-DED-GRP-TOP )
                ":",ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top()));
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pymnt_Ded_Amt().getValue(1,":",ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top()).setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Amt().getValue(1, //Natural: MOVE FCP-CONS-PYMNT.PYMNT-DED-AMT ( 1:#PYMNT-DED-GRP-TOP ) TO #REC-TYPE-20-DETAIL.PYMNT-DED-AMT ( 1:#PYMNT-DED-GRP-TOP )
                ":",ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Pymnt_Ded_Grp_Top()));
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO C*INV-ACCT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())); pnd_Ws_Pnd_I.nadd(1))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-ON-WHICH-SIDE-OF-PAPER
            sub_Print_On_Which_Side_Of_Paper();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Funds_Per_Pymnt().nadd(1);                                                                                           //Natural: ADD 1 TO #FUNDS-PER-PYMNT
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Funds_Per_Pymnt());                                 //Natural: MOVE #FUNDS-PER-PYMNT TO #KEY-REC-OCCUR-#
            ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Inv_Acct().setValuesByName(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct().getValue(pnd_Ws_Pnd_I));                         //Natural: MOVE BY NAME FCP-CONS-PYMNT.INV-ACCT ( #I ) TO #INV-ACCT
            getWorkFiles().write(1, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_20_Detail());                                                    //Natural: WRITE WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-20-DETAIL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Record_Type_30() throws Exception                                                                                                            //Natural: PROCESS-RECORD-TYPE-30
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
                                                                                                                                                                          //Natural: PERFORM GET-ADDR-RECORD
        sub_Get_Addr_Record();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30().reset();                                                                                                //Natural: RESET #RECORD-TYPE-30
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(30);                                                                                               //Natural: MOVE 30 TO #KEY-REC-LVL-#
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp1().setValuesByName(pdaFcpaaddr.getAddr());                                                            //Natural: MOVE BY NAME ADDR TO #RECORD-TYPE-30-GRP1
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Record_Type_30_Grp2().setValuesByName(pdaFcpaaddr.getAddr());                                                            //Natural: MOVE BY NAME ADDR TO #RECORD-TYPE-30-GRP2
        //*  JFT - DETERMINE IF CHECK OR FORM
        if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Payee_Cde().equals("ANNT") && pnd_Form.equals(" ")))                                                    //Natural: IF #REC-TYPE-10-DETAIL.CNTRCT-PAYEE-CDE = 'ANNT' AND #FORM = ' '
        {
            if (condition(pdaFcpaaddr.getAddr_Pymnt_Nme().getValue(2).equals(" ") && pdaFcpaaddr.getAddr_Pymnt_Addr_Line1_Txt().getValue(2).equals(" ")                   //Natural: IF ADDR.PYMNT-NME ( 2 ) = ' ' AND ADDR.PYMNT-ADDR-LINE1-TXT ( 2 ) = ' ' AND ADDR.PYMNT-ADDR-LINE2-TXT ( 2 ) = ' ' AND ADDR.PYMNT-ADDR-LINE3-TXT ( 2 ) = ' ' AND ADDR.PYMNT-ADDR-LINE4-TXT ( 2 ) = ' ' AND ADDR.PYMNT-ADDR-LINE5-TXT ( 2 ) = ' ' AND ADDR.PYMNT-ADDR-LINE6-TXT ( 2 ) = ' '
                && pdaFcpaaddr.getAddr_Pymnt_Addr_Line2_Txt().getValue(2).equals(" ") && pdaFcpaaddr.getAddr_Pymnt_Addr_Line3_Txt().getValue(2).equals(" ") 
                && pdaFcpaaddr.getAddr_Pymnt_Addr_Line4_Txt().getValue(2).equals(" ") && pdaFcpaaddr.getAddr_Pymnt_Addr_Line5_Txt().getValue(2).equals(" ") 
                && pdaFcpaaddr.getAddr_Pymnt_Addr_Line6_Txt().getValue(2).equals(" ")))
            {
                pnd_Form.setValue("C");                                                                                                                                   //Natural: ASSIGN #FORM := 'C'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Form.setValue("S");                                                                                                                                   //Natural: ASSIGN #FORM := 'S'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  JFT
                                                                                                                                                                          //Natural: PERFORM CONVERT-MIDDLE-NAME
        sub_Convert_Middle_Name();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Ph_Name().reset();                                                                                                           //Natural: RESET #REC-TYPE-30-DETAIL.PH-NAME
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Ph_First_Name().setValue(DbsUtil.compress(pdaAdsa362.getPnd_Out_Prefix(), pdaFcpaaddr.getAddr_Ph_First_Name()));             //Natural: COMPRESS #OUT-PREFIX ADDR.PH-FIRST-NAME INTO #REC-TYPE-30-DETAIL.PH-FIRST-NAME
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Ph_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                              //Natural: MOVE #OUT-MIDDLE-NAME TO #REC-TYPE-30-DETAIL.PH-MIDDLE-NAME
        ldaFcpl378.getPnd_Rec_Type_30_Detail_Ph_Last_Name().setValue(DbsUtil.compress(pdaFcpaaddr.getAddr_Ph_Last_Name(), pdaAdsa362.getPnd_Out_Suffix()));               //Natural: COMPRESS ADDR.PH-LAST-NAME #OUT-SUFFIX INTO #REC-TYPE-30-DETAIL.PH-LAST-NAME
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO C-PYMNT-NME-AND-ADDR-GRP
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpaaddr.getAddr_C_Pymnt_Nme_And_Addr_Grp())); pnd_Ws_Pnd_I.nadd(1))
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(pnd_Ws_Pnd_I);                                                                               //Natural: MOVE #I TO #KEY-REC-OCCUR-#
            ldaFcpl378.getPnd_Rec_Type_30_Detail_Pnd_Pymnt_Nme_And_Addr_Grp().setValuesByName(pdaFcpaaddr.getAddr_Pymnt_Nme_And_Addr_Grp().getValue(pnd_Ws_Pnd_I));       //Natural: MOVE BY NAME ADDR.PYMNT-NME-AND-ADDR-GRP ( #I ) TO #PYMNT-NME-AND-ADDR-GRP
            getWorkFiles().write(1, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_30_Detail());                                                    //Natural: WRITE WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-30-DETAIL
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Record_Type_40_45() throws Exception                                                                                                         //Natural: PROCESS-RECORD-TYPE-40-45
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
                                                                                                                                                                          //Natural: PERFORM GET-PLAN-RECORD
        sub_Get_Plan_Record();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Record_Type_40().reset();                                                                                                //Natural: RESET #RECORD-TYPE-40
        ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(40);                                                                                               //Natural: MOVE 40 TO #KEY-REC-LVL-#
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Record_Type_40_Grp1().setValuesByName(ldaFcpl378d.getVw_fcp_Cons_Plan());                                                //Natural: MOVE BY NAME FCP-CONS-PLAN TO #RECORD-TYPE-40-GRP1
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pymnt_Check_Dte().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Dte());                                                 //Natural: MOVE FCP-CONS-PYMNT.PYMNT-CHECK-DTE TO #REC-TYPE-40-DETAIL.PYMNT-CHECK-DTE
        ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display().setValue(true);                                                                                      //Natural: MOVE TRUE TO #PLAN-TYPE-DISPLAY
        if (condition(ldaFcpl378d.getFcp_Cons_Plan_Cntrct_Good_Contract().getBoolean() && ! (ldaFcpl378d.getFcp_Cons_Plan_Cntrct_Invalid_Cond_Ind().getBoolean())         //Natural: IF FCP-CONS-PLAN.CNTRCT-GOOD-CONTRACT AND NOT FCP-CONS-PLAN.CNTRCT-INVALID-COND-IND AND NOT FCP-CONS-PLAN.CNTRCT-MULTI-PAYEE
            && ! (ldaFcpl378d.getFcp_Cons_Plan_Cntrct_Multi_Payee().getBoolean())))
        {
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().setValue(true);                                                                                        //Natural: MOVE TRUE TO #GOOD-LETTER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().setValue(false);                                                                                       //Natural: MOVE FALSE TO #GOOD-LETTER
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl378d.getFcp_Cons_Plan_Count_Castplan_Data().equals(getZero())))                                                                              //Natural: IF C*PLAN-DATA = 0
        {
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(1);                                                                                          //Natural: MOVE 1 TO #KEY-REC-OCCUR-#
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top().setValue(1);                                                                                         //Natural: MOVE 1 TO #PLAN-DATA-TOP
            getWorkFiles().write(1, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_40_Detail());                                                    //Natural: WRITE WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-40-DETAIL
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top().setValue(ldaFcpl378d.getFcp_Cons_Plan_Count_Castplan_Data());                                        //Natural: MOVE C*PLAN-DATA TO #PLAN-DATA-TOP
            if (condition(! (ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Good_Letter().getBoolean()) && ldaFcpl378d.getFcp_Cons_Plan_Count_Castplan_Data().greater(1)        //Natural: IF NOT #GOOD-LETTER AND C*PLAN-DATA > 1 AND FCP-CONS-PLAN.PLAN-TYPE ( 2:#PLAN-DATA-TOP ) NE FCP-CONS-PLAN.PLAN-TYPE ( 1 )
                && ldaFcpl378d.getFcp_Cons_Plan_Plan_Type().getValue(2,":",ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Data_Top()).notEquals(ldaFcpl378d.getFcp_Cons_Plan_Plan_Type().getValue(1))))
            {
                ldaFcpl378.getPnd_Rec_Type_40_Detail_Pnd_Plan_Type_Display().setValue(false);                                                                             //Natural: MOVE FALSE TO #PLAN-TYPE-DISPLAY
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #I = 1 TO C*PLAN-DATA
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaFcpl378d.getFcp_Cons_Plan_Count_Castplan_Data())); pnd_Ws_Pnd_I.nadd(1))
            {
                ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(pnd_Ws_Pnd_I);                                                                           //Natural: MOVE #I TO #KEY-REC-OCCUR-#
                ldaFcpl378.getPnd_Rec_Type_40_Detail_Plan_Data().setValuesByName(ldaFcpl378d.getFcp_Cons_Plan_Plan_Data().getValue(pnd_Ws_Pnd_I));                        //Natural: MOVE BY NAME FCP-CONS-PLAN.PLAN-DATA ( #I ) TO #REC-TYPE-40-DETAIL.PLAN-DATA
                getWorkFiles().write(1, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_40_Detail());                                                //Natural: WRITE WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-40-DETAIL
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl378d.getFcp_Cons_Plan_Cntrct_Invalid_Cond_Ind().getBoolean()))                                                                               //Natural: IF FCP-CONS-PLAN.CNTRCT-INVALID-COND-IND
        {
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Record_Type_45().reset();                                                                                            //Natural: RESET #RECORD-TYPE-45
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(45);                                                                                           //Natural: MOVE 45 TO #KEY-REC-LVL-#
            ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(1);                                                                                          //Natural: MOVE 1 TO #KEY-REC-OCCUR-#
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Record_Type_45_Grp1().setValuesByName(ldaFcpl378d.getVw_fcp_Cons_Plan());                                            //Natural: MOVE BY NAME FCP-CONS-PLAN TO #RECORD-TYPE-45-GRP1
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Pnd_Invalid_Reasons_Top().setValue(ldaFcpl378d.getFcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons());                     //Natural: MOVE C*CNTRCT-INVALID-REASONS TO #REC-TYPE-45-DETAIL.#INVALID-REASONS-TOP
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Pymnt_Check_Dte().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Dte());                                             //Natural: MOVE FCP-CONS-PYMNT.PYMNT-CHECK-DTE TO #REC-TYPE-45-DETAIL.PYMNT-CHECK-DTE
            ldaFcpl378.getPnd_Rec_Type_45_Detail_Invalid_Reasons().getValue(1,":",8).setValue(ldaFcpl378d.getFcp_Cons_Plan_Cntrct_Invalid_Reasons().getValue(1,           //Natural: MOVE FCP-CONS-PLAN.CNTRCT-INVALID-REASONS ( 1:8 ) TO #REC-TYPE-45-DETAIL.INVALID-REASONS ( 1:8 )
                ":",8));
            getWorkFiles().write(1, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_45_Detail());                                                    //Natural: WRITE WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-45-DETAIL
            if (condition(ldaFcpl378d.getFcp_Cons_Plan_Count_Castcntrct_Invalid_Reasons().greater(8)))                                                                    //Natural: IF C*CNTRCT-INVALID-REASONS > 8
            {
                ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(2);                                                                                      //Natural: MOVE 2 TO #KEY-REC-OCCUR-#
                ldaFcpl378.getPnd_Rec_Type_45_Detail_Invalid_Reasons().getValue("*").reset();                                                                             //Natural: RESET #REC-TYPE-45-DETAIL.INVALID-REASONS ( * )
                ldaFcpl378.getPnd_Rec_Type_45_Detail_Invalid_Reasons().getValue(1,":",2).setValue(ldaFcpl378d.getFcp_Cons_Plan_Cntrct_Invalid_Reasons().getValue(9,       //Natural: MOVE FCP-CONS-PLAN.CNTRCT-INVALID-REASONS ( 9:10 ) TO #REC-TYPE-45-DETAIL.INVALID-REASONS ( 1:2 )
                    ":",10));
                getWorkFiles().write(1, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_45_Detail());                                                //Natural: WRITE WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-45-DETAIL
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_On_Which_Side_Of_Paper() throws Exception                                                                                                      //Natural: PRINT-ON-WHICH-SIDE-OF-PAPER
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_I));                                   //Natural: ASSIGN #INV-ACCT-INPUT := FCP-CONS-PYMNT.INV-ACCT-CDE ( #I )
        DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                        //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Ws_Pnd_I.equals(1)))                                                                                                                            //Natural: IF #I = 1
        {
            pnd_Ws_Pnd_Ws_Company.setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde());                                                                                //Natural: ASSIGN #WS-COMPANY := #COMPANY-CDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ws_Pnd_Ws_Company.notEquals(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde())))                                                                //Natural: IF #WS-COMPANY NE #COMPANY-CDE
            {
                pnd_Ws_Pnd_Stmnt_Lines.nadd(2);                                                                                                                           //Natural: ADD 2 TO #STMNT-LINES
                pnd_Ws_Pnd_Ws_Company.setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Company_Cde());                                                                            //Natural: ASSIGN #WS-COMPANY := #COMPANY-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  LINES PER STATEMENT: 5 LINES PER EACH FUND
        pnd_Ws_Pnd_Stmnt_Lines.nadd(5);                                                                                                                                   //Natural: ADD 5 TO #STMNT-LINES
        if (condition(pnd_Ws_Pnd_I.equals(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())))                                                                           //Natural: IF #I = C*INV-ACCT
        {
            pnd_Ws_Pnd_Stmnt_Lines.nadd(2);                                                                                                                               //Natural: ADD 2 TO #STMNT-LINES
        }                                                                                                                                                                 //Natural: END-IF
        //*  JFT - FORMAT STATEMENT
        if (condition(pnd_Form.equals("C")))                                                                                                                              //Natural: IF #FORM = 'C'
        {
            short decideConditionsMet1171 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #STMNT-LINES;//Natural: VALUE 1 : 18
            if (condition(((pnd_Ws_Pnd_Stmnt_Lines.greaterOrEqual(1) && pnd_Ws_Pnd_Stmnt_Lines.lessOrEqual(18)))))
            {
                decideConditionsMet1171++;
                ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Side().setValue("1");                                                                                            //Natural: MOVE '1' TO #SIDE
            }                                                                                                                                                             //Natural: VALUE 19 : 51
            else if (condition(((pnd_Ws_Pnd_Stmnt_Lines.greaterOrEqual(19) && pnd_Ws_Pnd_Stmnt_Lines.lessOrEqual(51)))))
            {
                decideConditionsMet1171++;
                ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Side().setValue("2");                                                                                            //Natural: MOVE '2' TO #SIDE
            }                                                                                                                                                             //Natural: VALUE 52 : 116
            else if (condition(((pnd_Ws_Pnd_Stmnt_Lines.greaterOrEqual(52) && pnd_Ws_Pnd_Stmnt_Lines.lessOrEqual(116)))))
            {
                decideConditionsMet1171++;
                ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Side().setValue("3");                                                                                            //Natural: MOVE '3' TO #SIDE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Form.equals("S")))                                                                                                                              //Natural: IF #FORM = 'S'
        {
            short decideConditionsMet1184 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE #STMNT-LINES;//Natural: VALUE 1 : 18
            if (condition(((pnd_Ws_Pnd_Stmnt_Lines.greaterOrEqual(1) && pnd_Ws_Pnd_Stmnt_Lines.lessOrEqual(18)))))
            {
                decideConditionsMet1184++;
                ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Side().setValue("1");                                                                                            //Natural: MOVE '1' TO #SIDE
            }                                                                                                                                                             //Natural: VALUE 19 : 83
            else if (condition(((pnd_Ws_Pnd_Stmnt_Lines.greaterOrEqual(19) && pnd_Ws_Pnd_Stmnt_Lines.lessOrEqual(83)))))
            {
                decideConditionsMet1184++;
                ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Side().setValue("2");                                                                                            //Natural: MOVE '2' TO #SIDE
            }                                                                                                                                                             //Natural: VALUE 84 : 148
            else if (condition(((pnd_Ws_Pnd_Stmnt_Lines.greaterOrEqual(84) && pnd_Ws_Pnd_Stmnt_Lines.lessOrEqual(148)))))
            {
                decideConditionsMet1184++;
                ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Side().setValue("3");                                                                                            //Natural: MOVE '3' TO #SIDE
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Convert_Middle_Name() throws Exception                                                                                                               //Natural: CONVERT-MIDDLE-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        pdaAdsa362.getPnd_In_Middle_Name().reset();                                                                                                                       //Natural: RESET #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE #IN-DATA #OUT-DATA
        pdaAdsa362.getPnd_Out_Middle_Name().reset();
        pdaAdsa362.getPnd_Out_Suffix().reset();
        pdaAdsa362.getPnd_Out_Prefix().reset();
        pdaAdsa362.getPnd_Out_Return_Code().reset();
        pdaAdsa360.getPnd_In_Data().reset();
        pdaAdsa360.getPnd_Out_Data().reset();
        pdaAdsa360.getPnd_In_Data_Pnd_First_Name().setValue(pdaFcpaaddr.getAddr_Ph_First_Name());                                                                         //Natural: MOVE ADDR.PH-FIRST-NAME TO #FIRST-NAME
        pdaAdsa362.getPnd_In_Middle_Name().setValue(pdaFcpaaddr.getAddr_Ph_Middle_Name());                                                                                //Natural: MOVE ADDR.PH-MIDDLE-NAME TO #IN-MIDDLE-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_Last_Name().setValue(pdaFcpaaddr.getAddr_Ph_Last_Name());                                                                           //Natural: MOVE ADDR.PH-LAST-NAME TO #LAST-NAME
        //* *CALLNAT 'PDQN362'  /* VIKRAM
        //*  VIKRAM
        DbsUtil.callnat(Adsn362.class , getCurrentProcessState(), pdaAdsa362.getPnd_In_Middle_Name(), pdaAdsa362.getPnd_Out_Middle_Name(), pdaAdsa362.getPnd_Out_Suffix(),  //Natural: CALLNAT 'ADSN362' #IN-MIDDLE-NAME #OUT-MIDDLE-NAME #OUT-SUFFIX #OUT-PREFIX #OUT-RETURN-CODE
            pdaAdsa362.getPnd_Out_Prefix(), pdaAdsa362.getPnd_Out_Return_Code());
        if (condition(Global.isEscape())) return;
        pdaAdsa360.getPnd_In_Data_Pnd_Prefix().setValue(pdaAdsa362.getPnd_Out_Prefix());                                                                                  //Natural: MOVE #OUT-PREFIX TO #PREFIX
        pdaAdsa360.getPnd_In_Data_Pnd_Middle_Name().setValue(pdaAdsa362.getPnd_Out_Middle_Name());                                                                        //Natural: MOVE #OUT-MIDDLE-NAME TO #MIDDLE-NAME
        pdaAdsa360.getPnd_In_Data_Pnd_Suffix().setValue(pdaAdsa362.getPnd_Out_Suffix());                                                                                  //Natural: MOVE #OUT-SUFFIX TO #SUFFIX
        pdaAdsa360.getPnd_In_Data_Pnd_Length().setValue(38);                                                                                                              //Natural: MOVE 38 TO #LENGTH
        pdaAdsa360.getPnd_In_Data_Pnd_Format().setValue("1");                                                                                                             //Natural: MOVE '1' TO #FORMAT
        //* *CALLNAT 'PDQN360'  /* VIKRAM
        //*  VIKRAM
        DbsUtil.callnat(Adsn360.class , getCurrentProcessState(), pdaAdsa360.getPnd_In_Data(), pdaAdsa360.getPnd_Out_Data());                                             //Natural: CALLNAT 'ADSN360' #IN-DATA #OUT-DATA
        if (condition(Global.isEscape())) return;
    }
    private void sub_Accum_Control_Data() throws Exception                                                                                                                //Natural: ACCUM-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        ldaFcplssc2.getPnd_Fcplssc2_Pnd_Max_Fund().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                          //Natural: MOVE C*INV-ACCT TO #MAX-FUND
        if (condition(ldaFcplssc2.getPnd_Fcplssc2_Pnd_Max_Fund().equals(getZero())))                                                                                      //Natural: IF #MAX-FUND = 0
        {
            ldaFcplssc2.getPnd_Fcplssc2_Pnd_Max_Fund().setValue(1);                                                                                                       //Natural: ASSIGN #MAX-FUND := 1
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplssc2.getPnd_Fcplssc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplssc2.getPnd_Fcplssc2_Pnd_Max_Fund()).setValuesByName(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct().getValue(1, //Natural: MOVE BY NAME FCP-CONS-PYMNT.INV-ACCT ( 1:#MAX-FUND ) TO #CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ":",ldaFcplssc2.getPnd_Fcplssc2_Pnd_Max_Fund()));
        DbsUtil.callnat(Fcpnssc1.class , getCurrentProcessState(), pdaFcpassc1.getPnd_Fcpassc1(), pdaFcpassc2.getPnd_Fcpassc2(), ldaFcplssc2.getPnd_Fcplssc2_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNSSC1' USING #FCPASSC1 #FCPASSC2 #FCPLSSC2.#MAX-FUND #FCPLSSC2.#CNTRL-INV-ACCT ( * )
            ldaFcplssc2.getPnd_Fcplssc2_Pnd_Cntrl_Inv_Acct().getValue("*"));
        if (condition(Global.isEscape())) return;
        pdaFcpassc2.getPnd_Fcpassc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                                  //Natural: ASSIGN #NEW-PYMNT-IND := FALSE
        pnd_Ws_Pnd_Amt.nadd(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Net_Pymnt_Amt().getValue("*"));                                                                        //Natural: ADD FCP-CONS-PYMNT.INV-ACCT-NET-PYMNT-AMT ( * ) TO #AMT
    }
    private void sub_Set_Control_Data() throws Exception                                                                                                                  //Natural: SET-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pdaFcpasscn.getPnd_Fcpasscn().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #FCPASSCN
        DbsUtil.callnat(Fcpnsscn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpasscn.getPnd_Fcpasscn(), pdaFcpassc1.getPnd_Fcpassc1());          //Natural: CALLNAT 'FCPNSSCN' USING #FCPACRPT #FCPASSCN #FCPASSC1
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Addr_Record() throws Exception                                                                                                                   //Natural: GET-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pdaFcpaaddr.getAddr_Cntrct_Ppcn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                  //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-PPCN-NBR TO ADDR.CNTRCT-PPCN-NBR
        pdaFcpaaddr.getAddr_Cntrct_Payee_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                                //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE TO ADDR.CNTRCT-PAYEE-CDE
        pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                  //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE TO ADDR.CNTRCT-ORGN-CDE
        pdaFcpaaddr.getAddr_Cntrct_Invrse_Dte().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                              //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE TO ADDR.CNTRCT-INVRSE-DTE
        pdaFcpaaddr.getAddr_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                          //Natural: MOVE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM TO ADDR.PYMNT-PRCSS-SEQ-NBR
        pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr());                                                      //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-CMBN-NBR TO ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR
        DbsUtil.callnat(Fcpnaddr.class , getCurrentProcessState(), pdaFcpaaddr.getAddr_Work_Fields(), pdaFcpaaddr.getAddr());                                             //Natural: CALLNAT 'FCPNADDR' USING ADDR-WORK-FIELDS ADDR
        if (condition(Global.isEscape())) return;
    }
    private void sub_Get_Plan_Record() throws Exception                                                                                                                   //Natural: GET-PLAN-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pnd_Plan_S_Cntrct_Orgn_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                             //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE TO #PLAN-S.CNTRCT-ORGN-CDE
        pnd_Plan_S_Cntrct_Ppcn_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                             //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-PPCN-NBR TO #PLAN-S.CNTRCT-PPCN-NBR
        pnd_Plan_S_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr());                                                                     //Natural: MOVE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NBR TO #PLAN-S.PYMNT-PRCSS-SEQ-NBR
        pnd_Plan_S_Cntl_Check_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Dte());                                                                              //Natural: MOVE FCP-CONS-PYMNT.PYMNT-CHECK-DTE TO #PLAN-S.CNTL-CHECK-DTE
        ldaFcpl378d.getVw_fcp_Cons_Plan().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) FCP-CONS-PLAN BY RCRD-ORGN-PPCN-PRCSS-CHKDT = #PLAN-SUPERDE THRU #PLAN-SUPERDE
        (
        "READ_PLAN",
        new Wc[] { new Wc("RCRD_ORGN_PPCN_PRCSS_CHKDT", ">=", pnd_Plan_S_Pnd_Plan_Superde.getBinary(), "And", WcType.BY) ,
        new Wc("RCRD_ORGN_PPCN_PRCSS_CHKDT", "<=", pnd_Plan_S_Pnd_Plan_Superde.getBinary(), WcType.BY) },
        new Oc[] { new Oc("RCRD_ORGN_PPCN_PRCSS_CHKDT", "ASC") },
        1
        );
        READ_PLAN:
        while (condition(ldaFcpl378d.getVw_fcp_Cons_Plan().readNextRow("READ_PLAN")))
        {
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcpl378d.getVw_fcp_Cons_Plan().getAstCOUNTER().equals(getZero())))                                                                               //Natural: IF *COUNTER ( READ-PLAN. ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"PLAN RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN    :",pnd_Plan_S_Cntrct_Orgn_Cde,new  //Natural: WRITE '***' 25T 'PLAN RECORD IS NOT FOUND' 77T '***' / '***' 25T 'ORIGIN    :' #PLAN-S.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'PPCN      :' #PLAN-S.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'SEQUENCE# :' #PLAN-S.PYMNT-PRCSS-SEQ-NBR 77T '***' / '***' 25T 'CHECK DATE:' #PLAN-S.CNTL-CHECK-DTE ( EM = MM/DD/YYYY ) 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PPCN      :",pnd_Plan_S_Cntrct_Ppcn_Nbr,new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"SEQUENCE# :",pnd_Plan_S_Pymnt_Prcss_Seq_Nbr,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"CHECK DATE:",pnd_Plan_S_Cntl_Check_Dte, 
                new ReportEditMask ("MM/DD/YYYY"),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Check_Control_Record() throws Exception                                                                                                              //Natural: CHECK-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpacntr.getCntrl_Cntl_Orgn_Cde().setValue("SS");                                                                                                              //Natural: ASSIGN CNTRL.CNTL-ORGN-CDE := 'SS'
        DbsUtil.callnat(Fcpncntr.class , getCurrentProcessState(), pdaFcpacntr.getCntrl_Work_Fields(), pdaFcpacntr.getCntrl());                                           //Natural: CALLNAT 'FCPNCNTR' USING CNTRL-WORK-FIELDS CNTRL
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Return_Cde().equals(getZero())))                                                                             //Natural: IF CNTRL-RETURN-CDE = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"CONTROL RECORD FOUND FOR PRIOR JOB",new TabSetting(77),"***");                                                //Natural: WRITE '***' 25T 'CONTROL RECORD FOUND FOR PRIOR JOB' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Store_Cntrl_Record() throws Exception                                                                                                                //Natural: STORE-CNTRL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  CALLNAT 'FCPN115' USING FCPA115   /* REPLACED BY CPWN115  ROXAN 12/02
        DbsUtil.callnat(Cpwn115.class , getCurrentProcessState(), pdaCpwa115.getCpwa115());                                                                               //Natural: CALLNAT 'CPWN115' USING CPWA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa115.getCpwa115_Error_Code().notEquals(getZero())))                                                                                           //Natural: IF CPWA115.ERROR-CODE NE 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"ERROR: FCPP378 DETECTS INVALID RETURN-CODE OF",pdaCpwa115.getCpwa115_Error_Code(),new TabSetting(77),"***",NEWLINE,"FROM",pdaCpwa115.getCpwa115_Error_Program(),new  //Natural: WRITE '***' 25T 'ERROR: FCPP378 DETECTS INVALID RETURN-CODE OF' CPWA115.ERROR-CODE 77T '***' / 'FROM' CPWA115.ERROR-PROGRAM 77T '***'
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Orgn_Cde().setValue("SS");                                                                                                     //Natural: MOVE 'SS' TO FCP-CONS-CNTRL.CNTL-ORGN-CDE
        pnd_Ws_Pnd_Date.setValue(pdaCpwa115.getCpwa115_Payment_Date());                                                                                                   //Natural: MOVE CPWA115.PAYMENT-DATE TO #DATE
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Date_Alpha);                                              //Natural: MOVE EDITED #DATE-ALPHA TO FCP-CONS-CNTRL.CNTL-CHECK-DTE ( EM = YYYYMMDD )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte().compute(new ComputeParameters(false, ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Invrse_Dte()), DbsField.subtract(100000000, //Natural: SUBTRACT CPWA115.PAYMENT-DATE FROM 100000000 GIVING FCP-CONS-CNTRL.CNTL-INVRSE-DTE
            pdaCpwa115.getCpwa115_Payment_Date()));
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Cnt().getValue("*").setValue(pdaFcpassc2.getPnd_Fcpassc2_Pnd_Rec_Cnt().getValue("*"));                                         //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-CNT ( * ) := #FCPASSC2.#REC-CNT ( * )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Gross_Amt().getValue("*").setValue(pdaFcpassc2.getPnd_Fcpassc2_Pnd_Settl_Amt().getValue("*"));                                 //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-GROSS-AMT ( * ) := #FCPASSC2.#SETTL-AMT ( * )
        ldaFcplcntu.getFcp_Cons_Cntrl_Cntl_Net_Amt().getValue("*").setValue(pdaFcpassc2.getPnd_Fcpassc2_Pnd_Net_Pymnt_Amt().getValue("*"));                               //Natural: ASSIGN FCP-CONS-CNTRL.CNTL-NET-AMT ( * ) := #FCPASSC2.#NET-PYMNT-AMT ( * )
        ldaFcplcntu.getVw_fcp_Cons_Cntrl().insertDBRow();                                                                                                                 //Natural: STORE FCP-CONS-CNTRL
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue("*").setValue(true);                                                                                       //Natural: MOVE TRUE TO #FCPACRPT.#TRUTH-TABLE ( * )
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue(" DS / SS / MDO CONTROL RECORD CREATION");                                                                       //Natural: ASSIGN #FCPACRPT.#TITLE := ' DS / SS / MDO CONTROL RECORD CREATION'
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                       //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
        DbsUtil.callnat(Fcpnssc2.class , getCurrentProcessState(), pdaFcpassc2.getPnd_Fcpassc2(), pdaFcpacrpt.getPnd_Fcpacrpt());                                         //Natural: CALLNAT 'FCPNSSC2' USING #FCPASSC2 #FCPACRPT
        if (condition(Global.isEscape())) return;
    }
    private void sub_Amt_Error() throws Exception                                                                                                                         //Natural: AMT-ERROR
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
        sub_Error_Display_Start();
        if (condition(Global.isEscape())) {return;}
        getReports().write(0, "***",new TabSetting(25),"PAYMENT AND ACTUAL FUND AMOUNTS DO NOT AGREE",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN    :",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde(),new  //Natural: WRITE '***' 25T 'PAYMENT AND ACTUAL FUND AMOUNTS DO NOT AGREE' 77T '***' / '***' 25T 'ORIGIN    :' FCP-CONS-PYMNT.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'PPCN      :' FCP-CONS-PYMNT.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'SEQUENCE# :' FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NBR 77T '***' / '***' 25T 'PYMNT AMT :' #AMT1 77T '***' / '***' 25T 'NET AMTS  :' #AMT 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PPCN      :",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr(),new TabSetting(77),"***",NEWLINE,"***",new 
            TabSetting(25),"SEQUENCE# :",ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr(),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PYMNT AMT :",pnd_Ws_Pnd_Amt1, 
            new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"NET AMTS  :",pnd_Ws_Pnd_Amt, new ReportEditMask 
            ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new TabSetting(77),"***");
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
        sub_Error_Display_End();
        if (condition(Global.isEscape())) {return;}
        DbsUtil.terminate(53);  if (true) return;                                                                                                                         //Natural: TERMINATE 53
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventRead_Pymnt() throws Exception {atBreakEventRead_Pymnt(false);}
    private void atBreakEventRead_Pymnt(boolean endOfData) throws Exception
    {
        boolean pnd_Ws_Pnd_Break_IndIsBreak = pnd_Ws_Pnd_Break_Ind.isBreak(endOfData);
        if (condition(pnd_Ws_Pnd_Break_IndIsBreak))
        {
            //*  CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                //*    WRITE OUT RECORD TYPE 10 AFTER SETTING THE LATEST SUMPLEX/DUP IND.
                ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().setValue(10);                                                                                       //Natural: MOVE 10 TO #KEY-REC-LVL-#
                ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Occur_Pnd().setValue(1);                                                                                      //Natural: MOVE 1 TO #KEY-REC-OCCUR-#
                ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Simplex_Duplex_Multiplex().setValue(ldaFcpl378.getPnd_Rec_Type_20_Detail_Pnd_Side());                             //Natural: MOVE #SIDE TO #KEY-SIMPLEX-DUPLEX-MULTIPLEX
                getWorkFiles().write(1, false, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_10_Detail());                                                //Natural: WRITE WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-10-DETAIL
                if (condition(rEAD_PYMNTPymnt_Check_AmtOld.notEquals(pnd_Ws_Pnd_Amt)))                                                                                    //Natural: IF OLD ( FCP-CONS-PYMNT.PYMNT-CHECK-AMT ) NE #AMT
                {
                    pnd_Ws_Pnd_Amt1.setValue(rEAD_PYMNTPymnt_Check_AmtOld);                                                                                               //Natural: ASSIGN #AMT1 := OLD ( FCP-CONS-PYMNT.PYMNT-CHECK-AMT )
                                                                                                                                                                          //Natural: PERFORM AMT-ERROR
                    sub_Amt_Error();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  NOT EOF
                if (condition(pnd_Ws_Pnd_Break_Ind.notEquals(rEAD_PYMNTPnd_Break_IndOld)))                                                                                //Natural: IF #BREAK-IND NE OLD ( #BREAK-IND )
                {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                    sub_Init_New_Pymnt();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-10
                    sub_Process_Record_Type_10();
                    if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-30
                    sub_Process_Record_Type_30();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().getBoolean()))                                                                          //Natural: IF #ACFS
                    {
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-40-45
                        sub_Process_Record_Type_40_45();
                        if (condition(Global.isEscape())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132 ZP=ON");
        Global.format(15, "LS=133 PS=58 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"SINGLE SUM BATCH EXTRACT CONTROL REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData960() throws Exception
    {
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAtStartOfData()))
        {
            //*  CTS-0115
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
            {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                sub_Init_New_Pymnt();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-10
                sub_Process_Record_Type_10();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-30
                sub_Process_Record_Type_30();
                if (condition(Global.isEscape())) {return;}
                if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Acfs().getBoolean()))                                                                              //Natural: IF #ACFS
                {
                                                                                                                                                                          //Natural: PERFORM PROCESS-RECORD-TYPE-40-45
                    sub_Process_Record_Type_40_45();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  CTS-0115
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-START
    }
}
