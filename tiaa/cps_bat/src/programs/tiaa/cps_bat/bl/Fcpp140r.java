/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:19 PM
**        * FROM NATURAL PROGRAM : Fcpp140r
************************************************************
**        * FILE NAME            : Fcpp140r.java
**        * CLASS NAME           : Fcpp140r
**        * INSTANCE NAME        : Fcpp140r
************************************************************
************************************************************************
* PROGRAM   : FCPP140R
*
* SYSTEM    : CPS
* TITLE     : LEDGER REVERSAL MODULE
* GENERATED : 01/15/2020  CTS CPS SUNSET (CHG694525)
*
* FUNCTION  : THIS MODULE DO THE LEDGER REVERSAL AND CREATE EXTRACT
*           : FILE WHICH WILL BE SENT TO DI-HUB.
*           : THIS PROGRAM WILL CONVERT THE #COBOL-PDA TO #GL-PDA AND
*           : CALL FCPN143V FOR LEDGER REVERSAL AND ALSO CREATE LEDGER
*           : EXTRACT FILE.
*
* HISTORY   :
* 08/13/2020  : CTS CPS SUNSET (CHG830649) TAG: CTS-0813
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp140r extends BLNatBase
{
    // Data Areas
    private PdaFcpa142l pdaFcpa142l;
    private PdaFcpa143 pdaFcpa143;
    private LdaFcpl801d ldaFcpl801d;
    private LdaFcpl121 ldaFcpl121;
    private PdaFcpa115 pdaFcpa115;
    private PdaFcpa142r pdaFcpa142r;
    private LdaFcpl199b ldaFcpl199b;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_F;
    private DbsField pnd_Original_Csr_Cde;
    private DbsField pnd_Pend_Pct_Omni;
    private DbsField pnd_Isa_Sub;
    private DbsField pnd_Alpha_Date;
    private DbsField pnd_Datx;
    private DbsField pnd_Temp_Date_Alpha;

    private DbsGroup pnd_Temp_Date_Alpha__R_Field_1;
    private DbsField pnd_Temp_Date_Alpha_Pnd_Temp_Date;
    private DbsField pnd_Action;

    private DbsGroup pnd_Action__R_Field_2;
    private DbsField pnd_Action_Pnd_Action_F;
    private DbsField pnd_Action_Pnd_Action_N;

    private DbsGroup pnd_Action__R_Field_3;
    private DbsField pnd_Action_Pnd_Action_1;
    private DbsField pnd_Selected_Key;

    private DbsGroup pnd_Selected_Key__R_Field_4;
    private DbsField pnd_Selected_Key_Pnd_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Selected_Key_Pnd_Cntrct_Invrse_Dte;
    private DbsField pnd_Selected_Key_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Selected_Key_Pnd_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Selected_Key__R_Field_5;
    private DbsField pnd_Selected_Key_Pnd_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Selected_Key_Pnd_Pymnt_Instmt_Nbr;
    private DbsField pnd_Pymnt_Check_Nbr;
    private DbsField pnd_Act_3;
    private DbsField pnd_Error;
    private DbsField pnd_Msg;
    private DbsField pnd_Hold_Code;
    private DbsField pnd_Environment;
    private DbsField pnd_Pymnt_Nbr;
    private DbsField pnd_Temp_Date_A;

    private DbsGroup pnd_Temp_Date_A__R_Field_6;
    private DbsField pnd_Temp_Date_A_Pnd_Temp_Date_N;
    private DbsField pls_Pnd_Original_Csr_Cde;
    private DbsField pls_Pnd_Pend_Pct;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa142l = new PdaFcpa142l(localVariables);
        pdaFcpa143 = new PdaFcpa143(localVariables);
        ldaFcpl801d = new LdaFcpl801d();
        registerRecord(ldaFcpl801d);
        ldaFcpl121 = new LdaFcpl121();
        registerRecord(ldaFcpl121);
        pdaFcpa115 = new PdaFcpa115(localVariables);
        pdaFcpa142r = new PdaFcpa142r(localVariables);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_F = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_F", "#F", FieldType.PACKED_DECIMAL, 3);
        pnd_Original_Csr_Cde = localVariables.newFieldInRecord("pnd_Original_Csr_Cde", "#ORIGINAL-CSR-CDE", FieldType.STRING, 3);
        pnd_Pend_Pct_Omni = localVariables.newFieldInRecord("pnd_Pend_Pct_Omni", "#PEND-PCT-OMNI", FieldType.NUMERIC, 5, 4);
        pnd_Isa_Sub = localVariables.newFieldInRecord("pnd_Isa_Sub", "#ISA-SUB", FieldType.NUMERIC, 2);
        pnd_Alpha_Date = localVariables.newFieldInRecord("pnd_Alpha_Date", "#ALPHA-DATE", FieldType.STRING, 8);
        pnd_Datx = localVariables.newFieldInRecord("pnd_Datx", "#DATX", FieldType.DATE);
        pnd_Temp_Date_Alpha = localVariables.newFieldInRecord("pnd_Temp_Date_Alpha", "#TEMP-DATE-ALPHA", FieldType.STRING, 8);

        pnd_Temp_Date_Alpha__R_Field_1 = localVariables.newGroupInRecord("pnd_Temp_Date_Alpha__R_Field_1", "REDEFINE", pnd_Temp_Date_Alpha);
        pnd_Temp_Date_Alpha_Pnd_Temp_Date = pnd_Temp_Date_Alpha__R_Field_1.newFieldInGroup("pnd_Temp_Date_Alpha_Pnd_Temp_Date", "#TEMP-DATE", FieldType.NUMERIC, 
            8);
        pnd_Action = localVariables.newFieldInRecord("pnd_Action", "#ACTION", FieldType.STRING, 3);

        pnd_Action__R_Field_2 = localVariables.newGroupInRecord("pnd_Action__R_Field_2", "REDEFINE", pnd_Action);
        pnd_Action_Pnd_Action_F = pnd_Action__R_Field_2.newFieldInGroup("pnd_Action_Pnd_Action_F", "#ACTION-F", FieldType.NUMERIC, 2);
        pnd_Action_Pnd_Action_N = pnd_Action__R_Field_2.newFieldInGroup("pnd_Action_Pnd_Action_N", "#ACTION-N", FieldType.NUMERIC, 1);

        pnd_Action__R_Field_3 = localVariables.newGroupInRecord("pnd_Action__R_Field_3", "REDEFINE", pnd_Action);
        pnd_Action_Pnd_Action_1 = pnd_Action__R_Field_3.newFieldInGroup("pnd_Action_Pnd_Action_1", "#ACTION-1", FieldType.STRING, 1);
        pnd_Selected_Key = localVariables.newFieldInRecord("pnd_Selected_Key", "#SELECTED-KEY", FieldType.STRING, 31);

        pnd_Selected_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Selected_Key__R_Field_4", "REDEFINE", pnd_Selected_Key);
        pnd_Selected_Key_Pnd_Cntrct_Unq_Id_Nbr = pnd_Selected_Key__R_Field_4.newFieldInGroup("pnd_Selected_Key_Pnd_Cntrct_Unq_Id_Nbr", "#CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Selected_Key_Pnd_Cntrct_Invrse_Dte = pnd_Selected_Key__R_Field_4.newFieldInGroup("pnd_Selected_Key_Pnd_Cntrct_Invrse_Dte", "#CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Selected_Key_Pnd_Cntrct_Orgn_Cde = pnd_Selected_Key__R_Field_4.newFieldInGroup("pnd_Selected_Key_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Selected_Key_Pnd_Pymnt_Prcss_Seq_Nbr = pnd_Selected_Key__R_Field_4.newFieldInGroup("pnd_Selected_Key_Pnd_Pymnt_Prcss_Seq_Nbr", "#PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);

        pnd_Selected_Key__R_Field_5 = pnd_Selected_Key__R_Field_4.newGroupInGroup("pnd_Selected_Key__R_Field_5", "REDEFINE", pnd_Selected_Key_Pnd_Pymnt_Prcss_Seq_Nbr);
        pnd_Selected_Key_Pnd_Pymnt_Prcss_Seq_Num = pnd_Selected_Key__R_Field_5.newFieldInGroup("pnd_Selected_Key_Pnd_Pymnt_Prcss_Seq_Num", "#PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Selected_Key_Pnd_Pymnt_Instmt_Nbr = pnd_Selected_Key__R_Field_5.newFieldInGroup("pnd_Selected_Key_Pnd_Pymnt_Instmt_Nbr", "#PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Pymnt_Check_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Check_Nbr", "#PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Act_3 = localVariables.newFieldInRecord("pnd_Act_3", "#ACT-3", FieldType.STRING, 3);
        pnd_Error = localVariables.newFieldInRecord("pnd_Error", "#ERROR", FieldType.BOOLEAN, 1);
        pnd_Msg = localVariables.newFieldInRecord("pnd_Msg", "#MSG", FieldType.STRING, 79);
        pnd_Hold_Code = localVariables.newFieldInRecord("pnd_Hold_Code", "#HOLD-CODE", FieldType.STRING, 4);
        pnd_Environment = localVariables.newFieldInRecord("pnd_Environment", "#ENVIRONMENT", FieldType.STRING, 4);
        pnd_Pymnt_Nbr = localVariables.newFieldInRecord("pnd_Pymnt_Nbr", "#PYMNT-NBR", FieldType.NUMERIC, 10);
        pnd_Temp_Date_A = localVariables.newFieldInRecord("pnd_Temp_Date_A", "#TEMP-DATE-A", FieldType.STRING, 8);

        pnd_Temp_Date_A__R_Field_6 = localVariables.newGroupInRecord("pnd_Temp_Date_A__R_Field_6", "REDEFINE", pnd_Temp_Date_A);
        pnd_Temp_Date_A_Pnd_Temp_Date_N = pnd_Temp_Date_A__R_Field_6.newFieldInGroup("pnd_Temp_Date_A_Pnd_Temp_Date_N", "#TEMP-DATE-N", FieldType.NUMERIC, 
            8);
        pls_Pnd_Original_Csr_Cde = WsIndependent.getInstance().newFieldInRecord("pls_Pnd_Original_Csr_Cde", "+#ORIGINAL-CSR-CDE", FieldType.STRING, 3);
        pls_Pnd_Pend_Pct = WsIndependent.getInstance().newFieldInRecord("pls_Pnd_Pend_Pct", "+#PEND-PCT", FieldType.NUMERIC, 5, 4);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl801d.initializeValues();
        ldaFcpl121.initializeValues();
        ldaFcpl199b.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp140r() throws Exception
    {
        super("Fcpp140r");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        pdaFcpa143.getPnd_Gl_Pda().reset();                                                                                                                               //Natural: RESET #GL-PDA
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error().setValue("0000");                                                                                                         //Natural: ASSIGN #GL-ERROR := '0000'
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN #GL-PROGRAM := *PROGRAM
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 RECORD #COBOL-PDA
        while (condition(getWorkFiles().read(1, pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_Cobol_Pda())))
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Orig_Cps_Pymnt_In().equals("X")))                                                                       //Natural: IF #C-ORIG-CPS-PYMNT-IN = 'X'
            {
                pnd_Action.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde());                                                                          //Natural: MOVE #C-CAN-RDRW-ACT-CDE TO #ACTION
                pnd_Act_3.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde());                                                                           //Natural: MOVE #C-CAN-RDRW-ACT-CDE TO #ACT-3
                pnd_Selected_Key_Pnd_Cntrct_Unq_Id_Nbr.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Unq_Id_Nbr());                                             //Natural: MOVE #C-CNTRCT-UNQ-ID-NBR TO #CNTRCT-UNQ-ID-NBR
                pnd_Temp_Date_A.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pmnt_Check_Dte());                                                                       //Natural: MOVE #C-PMNT-CHECK-DTE TO #TEMP-DATE-A
                pnd_Selected_Key_Pnd_Cntrct_Invrse_Dte.compute(new ComputeParameters(false, pnd_Selected_Key_Pnd_Cntrct_Invrse_Dte), DbsField.subtract(100000000,         //Natural: COMPUTE #CNTRCT-INVRSE-DTE = 100000000 - #TEMP-DATE-N
                    pnd_Temp_Date_A_Pnd_Temp_Date_N));
                pnd_Selected_Key_Pnd_Cntrct_Orgn_Cde.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Orgn_Cde());                                                 //Natural: MOVE #C-CNTRCT-ORGN-CDE TO #CNTRCT-ORGN-CDE
                pnd_Selected_Key_Pnd_Pymnt_Prcss_Seq_Num.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Prcss_Seq_Num());                                         //Natural: MOVE #C-PYMNT-PRCSS-SEQ-NUM TO #PYMNT-PRCSS-SEQ-NUM
                pls_Pnd_Pend_Pct.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pend_Pct());                                                                            //Natural: MOVE #C-PEND-PCT TO +#PEND-PCT
                DbsUtil.callnat(Fcpn800.class , getCurrentProcessState(), pnd_Action, pnd_Selected_Key, pnd_Pymnt_Check_Nbr, pnd_Act_3, pnd_Error, pnd_Msg,               //Natural: CALLNAT 'FCPN800' #ACTION #SELECTED-KEY #PYMNT-CHECK-NBR #ACT-3 #ERROR #MSG #HOLD-CODE #ENVIRONMENT #PYMNT-NBR
                    pnd_Hold_Code, pnd_Environment, pnd_Pymnt_Nbr);
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                if (condition(pnd_Error.getBoolean()))                                                                                                                    //Natural: IF #ERROR
                {
                    getReports().write(0, "##MSG :",pnd_Msg);                                                                                                             //Natural: WRITE '##MSG :' #MSG
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) return;                                                                                                                          //Natural: ESCAPE ROUTINE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    getReports().write(0, "UPDATE SUCCESSFUL");                                                                                                           //Natural: WRITE 'UPDATE SUCCESSFUL'
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CONVERT-LEGVSAM-DATA-AREA
            sub_Convert_Legvsam_Data_Area();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CALLNAT 'FCPN143' USING #GL-PDA
            DbsUtil.callnat(Fcpn143v.class , getCurrentProcessState(), pdaFcpa143.getPnd_Gl_Pda(), pnd_Original_Csr_Cde, pnd_Pend_Pct_Omni);                              //Natural: CALLNAT 'FCPN143V' USING #GL-PDA #ORIGINAL-CSR-CDE #PEND-PCT-OMNI
            if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error().equals("0000")))                                                                                        //Natural: IF #GL-ERROR = '0000'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Program().setValue(Global.getPROGRAM());                                                                                  //Natural: ASSIGN #GL-PROGRAM := *PROGRAM
                                                                                                                                                                          //Natural: PERFORM LEDGER-EXTRACT-WRITE-PARA
                sub_Ledger_Extract_Write_Para();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONVERT-LEGVSAM-DATA-AREA
        //*  #GL-IA-ORIGIN := #C-IA-ORIGIN
        //*   END-FOR
        //*  FOR #I = 1 TO 11
        //*    #F := #I
        //* *******************************************************
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LEDGER-EXTRACT-WRITE-PARA
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SETLMNT-LEDGER-WRITE-PARA
        //* *******************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DATE-CONVERT-AREA
        //* *******************************************************
    }
    //* * END OF PROGRAM ****
    private void sub_Convert_Legvsam_Data_Area() throws Exception                                                                                                         //Natural: CONVERT-LEGVSAM-DATA-AREA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        pdaFcpa143.getPnd_Gl_Pda().reset();                                                                                                                               //Natural: RESET #GL-PDA
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde().equals("CP") || pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde().equals("SP")  //Natural: IF #C-CAN-RDRW-ACT-CDE = 'CP' OR = 'SP' OR = 'RP' OR = 'PR' OR = 'AP'
            || pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde().equals("RP") || pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde().equals("PR") 
            || pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde().equals("AP")))
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ia_Origin().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ia_Origin());                                                    //Natural: ASSIGN #GL-IA-ORIGIN := #C-IA-ORIGIN
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pmnt_Check_Dte().notEquals(" ")))                                                                           //Natural: IF #C-PMNT-CHECK-DTE NE ' '
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pmnt_Check_Dte());    //Natural: MOVE EDITED #C-PMNT-CHECK-DTE TO #GL-PYMNT-CHECK-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Ppcn_Nbr().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cnt_Ppcn_Nbr());                                               //Natural: ASSIGN #GL-CNTRCT-PPCN-NBR := #C-CNT-PPCN-NBR
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Annty_Ins_Type());                                              //Natural: ASSIGN #GL-ANNTY-INS-TYPE := #C-ANNTY-INS-TYPE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Annty_Type_Cde());                                              //Natural: ASSIGN #GL-ANNTY-TYPE-CDE := #C-ANNTY-TYPE-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ins_Option().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ins_Option());                                                      //Natural: ASSIGN #GL-INS-OPTION := #C-INS-OPTION
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Life_Cont().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Life_Cont());                                                        //Natural: ASSIGN #GL-LIFE-CONT := #C-LIFE-CONT
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Orgn_Cde());                                            //Natural: ASSIGN #GL-CNTRCT-ORGN-CDE := #C-CNTRCT-ORGN-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Type_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Type_Cde());                                            //Natural: ASSIGN #GL-CNTRCT-TYPE-CDE := #C-CNTRCT-TYPE-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Spouse_Pay_Stats().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Spouse_Pay_Stats());                              //Natural: ASSIGN #GL-PYMNT-SPOUSE-PAY-STATS := #C-PYMNT-SPOUSE-PAY-STATS
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pay_Type_Req_Ind().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pay_Type_Req_Ind());                                          //Natural: ASSIGN #GL-PAY-TYPE-REQ-IND := #C-PAY-TYPE-REQ-IND
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Payee_Tx_Elct_Trggr().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Payee_Tx_Elct_Trggr());                        //Natural: ASSIGN #GL-PYMNT-PAYEE-TX-ELCT-TRGGR := #C-PYMNT-PAYEE-TX-ELCT-TRGGR
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rsdncy_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rsdncy_Cde());                                                      //Natural: ASSIGN #GL-RSDNCY-CDE := #C-RSDNCY-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Locality_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Locality_Cde());                                                  //Natural: ASSIGN #GL-LOCALITY-CDE := #C-LOCALITY-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lob_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Lob_Cde());                                                            //Natural: ASSIGN #GL-LOB-CDE := #C-LOB-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pend_Cde());                                                          //Natural: ASSIGN #GL-PEND-CDE := #C-PEND-CDE
        pnd_Pend_Pct_Omni.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pend_Pct());                                                                                   //Natural: ASSIGN #PEND-PCT-OMNI := #C-PEND-PCT
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Csr().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde());                                                       //Natural: ASSIGN #GL-CSR := #C-CAN-RDRW-ACT-CDE
        pnd_Original_Csr_Cde.setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde());                                                                        //Natural: ASSIGN #ORIGINAL-CSR-CDE := #C-CAN-RDRW-ACT-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Payee_Code().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Payee_Code());                                                      //Natural: ASSIGN #GL-PAYEE-CODE := #C-PAYEE-CODE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Option_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Option_Cde());                                                      //Natural: ASSIGN #GL-OPTION-CDE := #C-OPTION-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Roll_Dest_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Roll_Dest_Cde());                                                //Natural: ASSIGN #GL-ROLL-DEST-CDE := #C-ROLL-DEST-CDE
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dc_Pended().equals("Y")))                                                                                   //Natural: IF #C-DC-PENDED = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dc_Pended().setValue(true);                                                                                                   //Natural: ASSIGN #GL-DC-PENDED := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dc_Pended().equals("N")))                                                                               //Natural: IF #C-DC-PENDED = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dc_Pended().setValue(false);                                                                                              //Natural: ASSIGN #GL-DC-PENDED := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Internal_Rollover().equals("Y")))                                                                           //Natural: IF #C-INTERNAL-ROLLOVER = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Internal_Rollover().setValue(true);                                                                                           //Natural: ASSIGN #GL-INTERNAL-ROLLOVER := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Internal_Rollover().equals("N")))                                                                       //Natural: IF #C-INTERNAL-ROLLOVER = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Internal_Rollover().setValue(false);                                                                                      //Natural: ASSIGN #GL-INTERNAL-ROLLOVER := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rtb().equals("Y")))                                                                                         //Natural: IF #C-RTB = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb().setValue(true);                                                                                                         //Natural: ASSIGN #GL-RTB := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rtb().equals("N")))                                                                                     //Natural: IF #C-RTB = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb().setValue(false);                                                                                                    //Natural: ASSIGN #GL-RTB := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rtb_Rollover().equals("Y")))                                                                                //Natural: IF #C-RTB-ROLLOVER = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb_Rollover().setValue(true);                                                                                                //Natural: ASSIGN #GL-RTB-ROLLOVER := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rtb_Rollover().equals("N")))                                                                            //Natural: IF #C-RTB-ROLLOVER = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb_Rollover().setValue(false);                                                                                           //Natural: ASSIGN #GL-RTB-ROLLOVER := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ss_Rollover().equals("Y")))                                                                                 //Natural: IF #C-SS-ROLLOVER = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ss_Rollover().setValue(true);                                                                                                 //Natural: ASSIGN #GL-SS-ROLLOVER := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ss_Rollover().equals("N")))                                                                             //Natural: IF #C-SS-ROLLOVER = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ss_Rollover().setValue(false);                                                                                            //Natural: ASSIGN #GL-SS-ROLLOVER := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Rollover().equals("Y")))                                                                            //Natural: IF #C-CLASSIC-ROLLOVER = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Rollover().setValue(true);                                                                                            //Natural: ASSIGN #GL-CLASSIC-ROLLOVER := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Rollover().equals("N")))                                                                        //Natural: IF #C-CLASSIC-ROLLOVER = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Rollover().setValue(false);                                                                                       //Natural: ASSIGN #GL-CLASSIC-ROLLOVER := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_To_Roth().equals("Y")))                                                                             //Natural: IF #C-CLASSIC-TO-ROTH = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_To_Roth().setValue(true);                                                                                             //Natural: ASSIGN #GL-CLASSIC-TO-ROTH := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_To_Roth().equals("N")))                                                                         //Natural: IF #C-CLASSIC-TO-ROTH = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_To_Roth().setValue(false);                                                                                        //Natural: ASSIGN #GL-CLASSIC-TO-ROTH := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Withdraw().equals("Y")))                                                                            //Natural: IF #C-CLASSIC-WITHDRAW = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Withdraw().setValue(true);                                                                                            //Natural: ASSIGN #GL-CLASSIC-WITHDRAW := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Withdraw().equals("N")))                                                                        //Natural: IF #C-CLASSIC-WITHDRAW = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Withdraw().setValue(false);                                                                                       //Natural: ASSIGN #GL-CLASSIC-WITHDRAW := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Mdo_Withdraw().equals("Y")))                                                                        //Natural: IF #C-CLASSIC-MDO-WITHDRAW = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Mdo_Withdraw().setValue(true);                                                                                        //Natural: ASSIGN #GL-CLASSIC-MDO-WITHDRAW := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Mdo_Withdraw().equals("N")))                                                                    //Natural: IF #C-CLASSIC-MDO-WITHDRAW = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Mdo_Withdraw().setValue(false);                                                                                   //Natural: ASSIGN #GL-CLASSIC-MDO-WITHDRAW := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Roth_Withdraw().equals("Y")))                                                                               //Natural: IF #C-ROTH-WITHDRAW = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Roth_Withdraw().setValue(true);                                                                                               //Natural: ASSIGN #GL-ROTH-WITHDRAW := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Roth_Withdraw().equals("N")))                                                                           //Natural: IF #C-ROTH-WITHDRAW = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Roth_Withdraw().setValue(false);                                                                                          //Natural: ASSIGN #GL-ROTH-WITHDRAW := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ivc_From_Rollover().equals("Y")))                                                                           //Natural: IF #C-IVC-FROM-ROLLOVER = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ivc_From_Rollover().setValue(true);                                                                                           //Natural: ASSIGN #GL-IVC-FROM-ROLLOVER := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ivc_From_Rollover().equals("N")))                                                                       //Natural: IF #C-IVC-FROM-ROLLOVER = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ivc_From_Rollover().setValue(false);                                                                                      //Natural: ASSIGN #GL-IVC-FROM-ROLLOVER := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pa_Select_Dc().equals("Y")))                                                                                //Natural: IF #C-PA-SELECT-DC = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_Dc().setValue(true);                                                                                                //Natural: ASSIGN #GL-PA-SELECT-DC := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pa_Select_Dc().equals("N")))                                                                            //Natural: IF #C-PA-SELECT-DC = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_Dc().setValue(false);                                                                                           //Natural: ASSIGN #GL-PA-SELECT-DC := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pa_Select_M().equals("Y")))                                                                                 //Natural: IF #C-PA-SELECT-M = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_M().setValue(true);                                                                                                 //Natural: ASSIGN #GL-PA-SELECT-M := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pa_Select_M().equals("N")))                                                                             //Natural: IF #C-PA-SELECT-M = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_M().setValue(false);                                                                                            //Natural: ASSIGN #GL-PA-SELECT-M := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Spia_M().equals("Y")))                                                                                      //Natural: IF #C-SPIA-M = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_M().setValue(true);                                                                                                      //Natural: ASSIGN #GL-SPIA-M := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Spia_M().equals("N")))                                                                                  //Natural: IF #C-SPIA-M = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_M().setValue(false);                                                                                                 //Natural: ASSIGN #GL-SPIA-M := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Spia_Dc().equals("Y")))                                                                                     //Natural: IF #C-SPIA-DC = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_Dc().setValue(true);                                                                                                     //Natural: ASSIGN #GL-SPIA-DC := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Spia_Dc().equals("N")))                                                                                 //Natural: IF #C-SPIA-DC = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_Dc().setValue(false);                                                                                                //Natural: ASSIGN #GL-SPIA-DC := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Sci().equals("Y")))                                                                                         //Natural: IF #C-SCI = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Sci().setValue(true);                                                                                                         //Natural: ASSIGN #GL-SCI := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Sci().equals("N")))                                                                                     //Natural: IF #C-SCI = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Sci().setValue(false);                                                                                                    //Natural: ASSIGN #GL-SCI := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Scni().equals("Y")))                                                                                        //Natural: IF #C-SCNI = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Scni().setValue(true);                                                                                                        //Natural: ASSIGN #GL-SCNI := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Scni().equals("N")))                                                                                    //Natural: IF #C-SCNI = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Scni().setValue(false);                                                                                                   //Natural: ASSIGN #GL-SCNI := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Lump_Sum().equals("Y")))                                                                                    //Natural: IF #C-LUMP-SUM = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lump_Sum().setValue(true);                                                                                                    //Natural: ASSIGN #GL-LUMP-SUM := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Lump_Sum().equals("N")))                                                                                //Natural: IF #C-LUMP-SUM = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lump_Sum().setValue(false);                                                                                               //Natural: ASSIGN #GL-LUMP-SUM := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Stable_Return().equals("Y")))                                                                               //Natural: IF #C-STABLE-RETURN = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Stable_Return().setValue(true);                                                                                               //Natural: ASSIGN #GL-STABLE-RETURN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Stable_Return().equals("N")))                                                                           //Natural: IF #C-STABLE-RETURN = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Stable_Return().setValue(false);                                                                                          //Natural: ASSIGN #GL-STABLE-RETURN := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iva_Invest_Var_Annty().equals("Y")))                                                                        //Natural: IF #C-IVA-INVEST-VAR-ANNTY = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty().setValue(true);                                                                                        //Natural: ASSIGN #GL-IVA-INVEST-VAR-ANNTY := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iva_Invest_Var_Annty().equals("N")))                                                                    //Natural: IF #C-IVA-INVEST-VAR-ANNTY = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty().setValue(false);                                                                                   //Natural: ASSIGN #GL-IVA-INVEST-VAR-ANNTY := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iva_Invest_Var_Annty_Ac().equals("Y")))                                                                     //Natural: IF #C-IVA-INVEST-VAR-ANNTY-AC = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty_Ac().setValue(true);                                                                                     //Natural: ASSIGN #GL-IVA-INVEST-VAR-ANNTY-AC := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iva_Invest_Var_Annty_Ac().equals("N")))                                                                 //Natural: IF #C-IVA-INVEST-VAR-ANNTY-AC = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty_Ac().setValue(false);                                                                                //Natural: ASSIGN #GL-IVA-INVEST-VAR-ANNTY-AC := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iha_Invest_Horizon_Annty().equals("Y")))                                                                    //Natural: IF #C-IHA-INVEST-HORIZON-ANNTY = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty().setValue(true);                                                                                    //Natural: ASSIGN #GL-IHA-INVEST-HORIZON-ANNTY := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iha_Invest_Horizon_Annty().equals("N")))                                                                //Natural: IF #C-IHA-INVEST-HORIZON-ANNTY = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty().setValue(false);                                                                               //Natural: ASSIGN #GL-IHA-INVEST-HORIZON-ANNTY := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iha_Invest_Horizon_Annty_Ac().equals("Y")))                                                                 //Natural: IF #C-IHA-INVEST-HORIZON-ANNTY-AC = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty_Ac().setValue(true);                                                                                 //Natural: ASSIGN #GL-IHA-INVEST-HORIZON-ANNTY-AC := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iha_Invest_Horizon_Annty_Ac().equals("N")))                                                             //Natural: IF #C-IHA-INVEST-HORIZON-ANNTY-AC = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty_Ac().setValue(false);                                                                            //Natural: ASSIGN #GL-IHA-INVEST-HORIZON-ANNTY-AC := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tc_Lifetime_5_Annty().equals("Y")))                                                                         //Natural: IF #C-TC-LIFETIME-5-ANNTY = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty().setValue(true);                                                                                         //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tc_Lifetime_5_Annty().equals("N")))                                                                     //Natural: IF #C-TC-LIFETIME-5-ANNTY = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty().setValue(false);                                                                                    //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tc_Lifetime_5_Annty_Ac().equals("Y")))                                                                      //Natural: IF #C-TC-LIFETIME-5-ANNTY-AC = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty_Ac().setValue(true);                                                                                      //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY-AC := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tc_Lifetime_5_Annty_Ac().equals("N")))                                                                  //Natural: IF #C-TC-LIFETIME-5-ANNTY-AC = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty_Ac().setValue(false);                                                                                 //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY-AC := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pend_Lgrs().equals("Y")))                                                                                   //Natural: IF #C-PEND-LGRS = 'Y'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Lgrs().setValue(true);                                                                                                   //Natural: ASSIGN #GL-PEND-LGRS := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pend_Lgrs().equals("N")))                                                                               //Natural: IF #C-PEND-LGRS = 'N'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Lgrs().setValue(false);                                                                                              //Natural: ASSIGN #GL-PEND-LGRS := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dc_Past_Cv().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dc_Past_Cv());                                                      //Natural: ASSIGN #GL-DC-PAST-CV := #C-DC-PAST-CV
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Top().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Top());                                                                    //Natural: ASSIGN #GL-TOP := #C-TOP
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Company_Cde().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Company_Cde().getValue("*"));                        //Natural: ASSIGN #GL-COMPANY-CDE ( * ) := #C-COMPANY-CDE ( * )
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 40
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(40)); pnd_Ws_Pnd_I.nadd(1))
        {
            //*   #F := #I
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).equals("Y")))                                                //Natural: IF #C-MONTHLY-VALUATION ( #I ) = 'Y'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).setValue(true);                                                                //Natural: ASSIGN #GL-MONTHLY-VALUATION ( #I ) := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).equals("N")))                                            //Natural: IF #C-MONTHLY-VALUATION ( #I ) = 'N'
                {
                    pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).setValue(false);                                                           //Natural: ASSIGN #GL-MONTHLY-VALUATION ( #I ) := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tiaa_Access().getValue(pnd_Ws_Pnd_I).equals("Y")))                                                      //Natural: IF #C-TIAA-ACCESS ( #I ) = 'Y'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa_Access().getValue(pnd_Ws_Pnd_I).setValue(true);                                                                      //Natural: ASSIGN #GL-TIAA-ACCESS ( #I ) := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tiaa_Access().getValue(pnd_Ws_Pnd_I).equals("N")))                                                  //Natural: IF #C-TIAA-ACCESS ( #I ) = 'N'
                {
                    pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa_Access().getValue(pnd_Ws_Pnd_I).setValue(false);                                                                 //Natural: ASSIGN #GL-TIAA-ACCESS ( #I ) := FALSE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fund().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Fund().getValue("*"));                                      //Natural: ASSIGN #GL-FUND ( * ) := #C-FUND ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Settl_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Settl_Amt().getValue("*"));                            //Natural: ASSIGN #GL-SETTL-AMT ( * ) := #C-SETTL-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dpi_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dpi_Amt().getValue("*"));                                //Natural: ASSIGN #GL-DPI-AMT ( * ) := #C-DPI-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dci_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dci_Amt().getValue("*"));                                //Natural: ASSIGN #GL-DCI-AMT ( * ) := #C-DCI-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Exp_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Exp_Amt().getValue("*"));                                //Natural: ASSIGN #GL-EXP-AMT ( * ) := #C-EXP-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Can_Tax_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Tax_Amt().getValue("*"));                        //Natural: ASSIGN #GL-CAN-TAX-AMT ( * ) := #C-CAN-TAX-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fdrl_Tax_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Fdrl_Tax_Amt().getValue("*"));                      //Natural: ASSIGN #GL-FDRL-TAX-AMT ( * ) := #C-FDRL-TAX-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_State_Tax_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_State_Tax_Amt().getValue("*"));                    //Natural: ASSIGN #GL-STATE-TAX-AMT ( * ) := #C-STATE-TAX-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Local_Tax_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Local_Tax_Amt().getValue("*"));                    //Natural: ASSIGN #GL-LOCAL-TAX-AMT ( * ) := #C-LOCAL-TAX-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Net_Pymnt_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Net_Pymnt_Amt().getValue("*"));                    //Natural: ASSIGN #GL-NET-PYMNT-AMT ( * ) := #C-NET-PYMNT-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tiaa().getValue("*"));                                      //Natural: ASSIGN #GL-TIAA ( * ) := #C-TIAA ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Inv_Acct_Ded_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Inv_Acct_Ded_Amt().getValue("*"));              //Natural: ASSIGN #GL-INV-ACCT-DED-AMT ( * ) := #C-INV-ACCT-DED-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ivc_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ivc_Amt().getValue("*"));                                //Natural: ASSIGN #GL-IVC-AMT ( * ) := #C-IVC-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Unit_Value().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Unit_Value().getValue("*"));                          //Natural: ASSIGN #GL-UNIT-VALUE ( * ) := #C-UNIT-VALUE ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Unit_Qty().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Unit_Qty().getValue("*"));                              //Natural: ASSIGN #GL-UNIT-QTY ( * ) := #C-UNIT-QTY ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ded_Top().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ded_Top());                                                            //Natural: ASSIGN #GL-DED-TOP := #C-DED-TOP
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Ded_Cde().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Ded_Cde().getValue("*"));                    //Natural: ASSIGN #GL-PYMNT-DED-CDE ( * ) := #C-PYMNT-DED-CDE ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Ded_Amt().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Ded_Amt().getValue("*"));                    //Natural: ASSIGN #GL-PYMNT-DED-AMT ( * ) := #C-PYMNT-DED-AMT ( * )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Ded_Payee_Cde().getValue("*").setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Ded_Payee_Cde().getValue("*"));        //Natural: ASSIGN #GL-PYMNT-DED-PAYEE-CDE ( * ) := #C-PYMNT-DED-PAYEE-CDE ( * )
        //*  END-FOR
        //* CONVERT-LEGVSAM-DATA-AREA
    }
    private void sub_Ledger_Extract_Write_Para() throws Exception                                                                                                         //Natural: LEDGER-EXTRACT-WRITE-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Ppcn_Nbr().notEquals(" ")))                                                                                  //Natural: IF #GL-PDA.#GL-CNTRCT-PPCN-NBR NE " "
        {
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde().equals("NZ") || pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde().equals("DC")                //Natural: IF #GL-PDA.#GL-CNTRCT-ORGN-CDE = 'NZ' OR = 'DC' OR = 'AP'
                || pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde().equals("AP")))
            {
                                                                                                                                                                          //Natural: PERFORM SETLMNT-LEDGER-WRITE-PARA
                sub_Setlmnt_Ledger_Write_Para();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                Global.setEscape(true); Global.setEscapeCode(EscapeType.Bottom);                                                                                          //Natural: ESCAPE BOTTOM
                if (true) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* LEDGER-EXTRACT-WRITE-PARA
    }
    private void sub_Setlmnt_Ledger_Write_Para() throws Exception                                                                                                         //Natural: SETLMNT-LEDGER-WRITE-PARA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************
        ldaFcpl801d.getPnd_Ledger_Ext_2().reset();                                                                                                                        //Natural: RESET #LEDGER-EXT-2
        ldaFcpl121.getPnd_Ledger_Extract().reset();                                                                                                                       //Natural: RESET #LEDGER-EXTRACT
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Orgn_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde());                                                    //Natural: MOVE #GL-PDA.#GL-CNTRCT-ORGN-CDE TO #LEDGER-EXT-2.CNTRCT-ORGN-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Ppcn_Nbr().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Ppcn_Nbr());                                                    //Natural: MOVE #GL-PDA.#GL-CNTRCT-PPCN-NBR TO #LEDGER-EXT-2.CNTRCT-PPCN-NBR
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Payee_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Payee_Code());                                                        //Natural: MOVE #GL-PDA.#GL-PAYEE-CODE TO #LEDGER-EXT-2.CNTRCT-PAYEE-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Mode_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cnt_Mode_Cde());                                               //Natural: MOVE #C-CNT-MODE-CDE TO #LEDGER-EXT-2.CNTRCT-MODE-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Annty_Ins_Type().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type());                                               //Natural: MOVE #GL-PDA.#GL-ANNTY-INS-TYPE TO #LEDGER-EXT-2.CNTRCT-ANNTY-INS-TYPE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Annty_Type_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde());                                               //Natural: MOVE #GL-PDA.#GL-ANNTY-TYPE-CDE TO #LEDGER-EXT-2.CNTRCT-ANNTY-TYPE-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Insurance_Option().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ins_Option());                                                 //Natural: MOVE #GL-PDA.#GL-INS-OPTION TO #LEDGER-EXT-2.CNTRCT-INSURANCE-OPTION
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Life_Contingency().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Life_Cont());                                                  //Natural: MOVE #GL-PDA.#GL-LIFE-CONT TO #LEDGER-EXT-2.CNTRCT-LIFE-CONTINGENCY
        ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Rsdncy_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rsdncy_Cde());                                                         //Natural: MOVE #GL-PDA.#GL-RSDNCY-CDE TO #LEDGER-EXT-2.ANNT-RSDNCY-CDE
        //*  RS6
        short decideConditionsMet838 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #GL-PDA.#GL-RSDNCY-CDE = MASK ( NN )
        if (condition(DbsUtil.maskMatches(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rsdncy_Cde(),"NN")))
        {
            decideConditionsMet838++;
            ignore();
        }                                                                                                                                                                 //Natural: WHEN #GL-PDA.#GL-RSDNCY-CDE = MASK ( AA )
        else if (condition(DbsUtil.maskMatches(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rsdncy_Cde(),"AA")))
        {
            decideConditionsMet838++;
            ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Rsdncy_Cde().setValue("FO");                                                                                             //Natural: MOVE 'FO' TO #LEDGER-EXT-2.ANNT-RSDNCY-CDE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Locality_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Locality_Cde());                                                     //Natural: MOVE #GL-PDA.#GL-LOCALITY-CDE TO #LEDGER-EXT-2.ANNT-LOCALITY-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Ia_Orgn_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ia_Origin());                                                              //Natural: MOVE #GL-PDA.#GL-IA-ORIGIN TO #LEDGER-EXT-2.IA-ORGN-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Option_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Option_Cde());                                                       //Natural: MOVE #GL-PDA.#GL-OPTION-CDE TO #LEDGER-EXT-2.CNTRCT-OPTION-CDE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Redraw_Ind().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde());                                         //Natural: MOVE #C-CAN-RDRW-ACT-CDE TO #LEDGER-EXT-2.CNTRCT-REDRAW-IND
        ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Settlmnt_Dte().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Settlmnt_Dte());                                      //Natural: MOVE #C-PYMNT-SETTLMNT-DTE TO #LEDGER-EXT-2.PYMNT-SETTLMNT-DTE
        ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Check_Dte().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pmnt_Check_Dte());                                             //Natural: MOVE #C-PMNT-CHECK-DTE TO #LEDGER-EXT-2.PYMNT-CHECK-DTE
                                                                                                                                                                          //Natural: PERFORM DATE-CONVERT-AREA
        sub_Date_Convert_Area();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde().setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Orgn_Cde());                                                   //Natural: MOVE #LEDGER-EXT-2.CNTRCT-ORGN-CDE TO #LEDGER-EXTRACT.CNTRCT-ORGN-CDE
        //*  MOVE #C-CNTRCT-CANCEL-RDRW-IND TO     /* CTS-0813
        //*  #LEDGER-EXTRACT.CNTRCT-CANCEL-RDRW-IND   /* CTS-0813
        //*  CTS-0813
        //*  CTS-0813
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_Ind().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde());                                   //Natural: MOVE #C-CAN-RDRW-ACT-CDE TO #LEDGER-EXTRACT.CNTRCT-CANCEL-RDRW-IND
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr().setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Ppcn_Nbr());                                                   //Natural: MOVE #LEDGER-EXT-2.CNTRCT-PPCN-NBR TO #LEDGER-EXTRACT.CNTRCT-PPCN-NBR
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Payee_Cde().setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Payee_Cde());                                                 //Natural: MOVE #LEDGER-EXT-2.CNTRCT-PAYEE-CDE TO #LEDGER-EXTRACT.CNTRCT-PAYEE-CDE
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Acctg_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Acctg_Dte());              //Natural: MOVE EDITED #LEDGER-EXT-2.PYMNT-ACCTG-DTE TO #LEDGER-EXTRACT.PYMNT-ACCTG-DTE ( EM = YYYYMMDD )
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Settlmnt_Dte()); //Natural: MOVE EDITED #C-PYMNT-SETTLMNT-DTE TO #LEDGER-EXTRACT.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD )
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pmnt_Check_Dte());       //Natural: MOVE EDITED #C-PMNT-CHECK-DTE TO #LEDGER-EXTRACT.PYMNT-CHECK-DTE ( EM = YYYYMMDD )
        ldaFcpl121.getPnd_Ledger_Extract_Ph_Last_Name().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ph_Last_Name());                                                 //Natural: MOVE #C-PH-LAST-NAME TO #LEDGER-EXTRACT.PH-LAST-NAME
        ldaFcpl121.getPnd_Ledger_Extract_Ph_First_Name().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ph_First_Name());                                               //Natural: MOVE #C-PH-FIRST-NAME TO #LEDGER-EXTRACT.PH-FIRST-NAME
        ldaFcpl121.getPnd_Ledger_Extract_Ph_Middle_Name().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ph_Middle_Name());                                             //Natural: MOVE #C-PH-MIDDLE-NAME TO #LEDGER-EXTRACT.PH-MIDDLE-NAME
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Check_Crrncy_Cde().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Check_Crrncy_Cde());                           //Natural: MOVE #C-CNTRCT-CHECK-CRRNCY-CDE TO #LEDGER-EXTRACT.CNTRCT-CHECK-CRRNCY-CDE
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cref_Nbr().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Cref_Nbr());                                           //Natural: MOVE #C-CNTRCT-CREF-NBR TO #LEDGER-EXTRACT.CNTRCT-CREF-NBR
        ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Unq_Id_Nbr().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Unq_Id_Nbr());                                       //Natural: MOVE #C-CNTRCT-UNQ-ID-NBR TO #LEDGER-EXTRACT.CNTRCT-UNQ-ID-NBR
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Corp_Wpid().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Corp_Wpid());                                           //Natural: MOVE #C-PYMNT-CORP-WPID TO #LEDGER-EXTRACT.PYMNT-CORP-WPID
        //*  CTS-0813
        if (condition(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Cycle_Dte().notEquals(" ")))                                                                          //Natural: IF #C-PYMNT-CYCLE-DTE NE " "
        {
            ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Cycle_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Cycle_Dte());  //Natural: MOVE EDITED #C-PYMNT-CYCLE-DTE TO #LEDGER-EXTRACT.PYMNT-CYCLE-DTE ( EM = YYYYMMDD )
            //*  CTS-0813
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Intrfce_Dte());  //Natural: MOVE EDITED #C-PYMNT-INTRFCE-DTE TO #LEDGER-EXTRACT.PYMNT-INTRFCE-DTE ( EM = YYYYMMDD )
        //*  CTS-0813
        ldaFcpl801d.getPnd_Ledger_Ext_2_Pnd_Filler().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Intrfce_Dte());                                               //Natural: MOVE #C-PYMNT-INTRFCE-DTE TO #LEDGER-EXT-2.#FILLER
        ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Prcss_Seq_Nbr().setValue(pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Prcss_Seq_Nbr());                                   //Natural: MOVE #C-PYMNT-PRCSS-SEQ-NBR TO #LEDGER-EXTRACT.PYMNT-PRCSS-SEQ-NBR
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 198
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(198)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Isa_Hash_A().getValue(pnd_Ws_Pnd_I).notEquals(" ") && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Nbr().getValue(pnd_Ws_Pnd_I).notEquals(" ")  //Natural: IF #GL-LGR-ISA-HASH-A ( #I ) NE ' ' AND #GL-LGR-NBR ( #I ) NE ' ' AND #GL-LGR-AMT ( #I ) NE 0 AND #GL-LGR-TYPE ( #I ) NE ' '
                && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Amt().getValue(pnd_Ws_Pnd_I).notEquals(getZero()) && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Type().getValue(pnd_Ws_Pnd_I).notEquals(" ")))
            {
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Isa_Hash().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Isa_Hash_A().getValue(pnd_Ws_Pnd_I));                         //Natural: MOVE #GL-LGR-ISA-HASH-A ( #I ) TO #LEDGER-EXT-2.INV-ISA-HASH
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Nbr().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Nbr().getValue(pnd_Ws_Pnd_I));                          //Natural: MOVE #GL-LGR-NBR ( #I ) TO #LEDGER-EXT-2.INV-ACCT-LEDGR-NBR
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Amt().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Amt().getValue(pnd_Ws_Pnd_I));                          //Natural: MOVE #GL-LGR-AMT ( #I ) TO #LEDGER-EXT-2.INV-ACCT-LEDGR-AMT
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Ind().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Type().getValue(pnd_Ws_Pnd_I));                         //Natural: MOVE #GL-LGR-TYPE ( #I ) TO #LEDGER-EXT-2.INV-ACCT-LEDGR-IND
                ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Nbr());                                     //Natural: MOVE #LEDGER-EXT-2.INV-ACCT-LEDGR-NBR TO #LEDGER-EXTRACT.INV-ACCT-LEDGR-NBR
                ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Amt().setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Amt());                                     //Natural: MOVE #LEDGER-EXT-2.INV-ACCT-LEDGR-AMT TO #LEDGER-EXTRACT.INV-ACCT-LEDGR-AMT
                ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Ind().setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Ind());                                     //Natural: MOVE #LEDGER-EXT-2.INV-ACCT-LEDGR-IND TO #LEDGER-EXTRACT.INV-ACCT-LEDGR-IND
                ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Annty_Ins_Type().setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Annty_Ins_Type());                               //Natural: MOVE #LEDGER-EXT-2.CNTRCT-ANNTY-INS-TYPE TO #LEDGER-EXTRACT.CNTRCT-ANNTY-INS-TYPE
                ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde().setValue(ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Isa_Hash());                                                 //Natural: MOVE #LEDGER-EXT-2.INV-ISA-HASH TO #LEDGER-EXTRACT.INV-ACCT-CDE
                pnd_Isa_Sub.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde_Num());                                                                                //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-CDE-NUM TO #ISA-SUB
                ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Isa_Sub.getInt() + 1));                    //Natural: MOVE #ISA-LDA.#ISA-CDE ( #ISA-SUB ) TO #LEDGER-EXTRACT.INV-ACCT-ISA
                getWorkFiles().write(2, false, ldaFcpl801d.getPnd_Ledger_Ext_2());                                                                                        //Natural: WRITE WORK FILE 02 #LEDGER-EXT-2
                getWorkFiles().write(3, false, ldaFcpl121.getPnd_Ledger_Extract());                                                                                       //Natural: WRITE WORK FILE 03 #LEDGER-EXTRACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* SETLMNT-LEDGER-WRITE-PARA
    }
    private void sub_Date_Convert_Area() throws Exception                                                                                                                 //Natural: DATE-CONVERT-AREA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Datx.setValue(Global.getDATX());                                                                                                                              //Natural: ASSIGN #DATX := *DATX
        if (condition(Global.getTIMN().greaterOrEqual(0) && Global.getTIMN().lessOrEqual(800000)))                                                                        //Natural: IF *TIMN GE 0000000 AND *TIMN LE 0800000
        {
            pnd_Datx.compute(new ComputeParameters(false, pnd_Datx), Global.getDATX().subtract(1));                                                                       //Natural: ASSIGN #DATX := *DATX - 1
        }                                                                                                                                                                 //Natural: END-IF
        //*  FE201408
        if (condition((pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Check_Dte().greater(pnd_Datx) && (((((((((pnd_Original_Csr_Cde.equals("C") || pnd_Original_Csr_Cde.equals(" C"))  //Natural: IF #GL-PDA.#GL-PYMNT-CHECK-DTE > #DATX AND #ORIGINAL-CSR-CDE = 'C' OR = ' C' OR = 'S' OR = ' S' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR' OR = 'AS' OR = 'AP'
            || pnd_Original_Csr_Cde.equals("S")) || pnd_Original_Csr_Cde.equals(" S")) || pnd_Original_Csr_Cde.equals("CP")) || pnd_Original_Csr_Cde.equals("SP")) 
            || pnd_Original_Csr_Cde.equals("RP")) || pnd_Original_Csr_Cde.equals("PR")) || pnd_Original_Csr_Cde.equals("AS")) || pnd_Original_Csr_Cde.equals("AP")))))
        {
            pnd_Alpha_Date.setValueEdited(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                              //Natural: MOVE EDITED #GL-PDA.#GL-PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #ALPHA-DATE
            pdaFcpa115.getFcpa115_For_Date().compute(new ComputeParameters(false, pdaFcpa115.getFcpa115_For_Date()), pnd_Alpha_Date.val());                               //Natural: ASSIGN FCPA115.FOR-DATE := VAL ( #ALPHA-DATE )
            pdaFcpa115.getFcpa115_For_Time().reset();                                                                                                                     //Natural: RESET FCPA115.FOR-TIME
            DbsUtil.callnat(Fcpn119.class , getCurrentProcessState(), pdaFcpa115.getFcpa115());                                                                           //Natural: CALLNAT 'FCPN119' FCPA115
            if (condition(Global.isEscape())) return;
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde().equals("NZ")))                                                                                //Natural: IF #GL-CNTRCT-ORGN-CDE = 'NZ'
            {
                pnd_Alpha_Date.setValueEdited(pdaFcpa115.getFcpa115_Next_Business_Date(),new ReportEditMask("99999999"));                                                 //Natural: MOVE EDITED FCPA115.NEXT-BUSINESS-DATE ( EM = 99999999 ) TO #ALPHA-DATE
                //*  CTS-0813 >>
                pdaFcpa115.getFcpa115_Interface_Date().setValue(pdaFcpa115.getFcpa115_Next_Business_Date());                                                              //Natural: MOVE FCPA115.NEXT-BUSINESS-DATE TO FCPA115.INTERFACE-DATE
                //*  CTS-0813 <<
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Alpha_Date.setValueEdited(pdaFcpa115.getFcpa115_Accounting_Date(),new ReportEditMask("99999999"));                                                    //Natural: MOVE EDITED FCPA115.ACCOUNTING-DATE ( EM = 99999999 ) TO #ALPHA-DATE
            }                                                                                                                                                             //Natural: END-IF
            //*  DTE EDITED
            ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Acctg_Dte().setValue(pnd_Alpha_Date);                                                                                   //Natural: MOVE #ALPHA-DATE TO #LEDGER-EXT-2.PYMNT-ACCTG-DTE
            //*  CTS-0813
            //*  CTS-0813
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Intrfce_Dte().setValueEdited(pdaFcpa115.getFcpa115_Interface_Date(),new ReportEditMask("99999999"));            //Natural: MOVE EDITED FCPA115.INTERFACE-DATE ( EM = 99999999 ) TO #C-PYMNT-INTRFCE-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Alpha_Date.setValueEdited(pnd_Datx,new ReportEditMask("YYYYMMDD"));                                                                                       //Natural: MOVE EDITED #DATX ( EM = YYYYMMDD ) TO #ALPHA-DATE
            pdaFcpa115.getFcpa115_For_Date().compute(new ComputeParameters(false, pdaFcpa115.getFcpa115_For_Date()), pnd_Alpha_Date.val());                               //Natural: ASSIGN FCPA115.FOR-DATE := VAL ( #ALPHA-DATE )
            pdaFcpa115.getFcpa115_For_Time().reset();                                                                                                                     //Natural: RESET FCPA115.FOR-TIME
            DbsUtil.callnat(Fcpn119.class , getCurrentProcessState(), pdaFcpa115.getFcpa115());                                                                           //Natural: CALLNAT 'FCPN119' FCPA115
            if (condition(Global.isEscape())) return;
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde().equals("NZ")))                                                                                //Natural: IF #GL-CNTRCT-ORGN-CDE = 'NZ'
            {
                //*  CTS-0813
                if (condition(pdaFcpa115.getFcpa115_Payment_Date().less(pdaFcpa115.getFcpa115_Next_Business_Date())))                                                     //Natural: IF FCPA115.PAYMENT-DATE < FCPA115.NEXT-BUSINESS-DATE
                {
                    pdaFcpa115.getFcpa115_Interface_Date().setValue(pdaFcpa115.getFcpa115_Next_Business_Date());                                                          //Natural: ASSIGN FCPA115.INTERFACE-DATE := FCPA115.ACCOUNTING-DATE := FCPA115.NEXT-BUSINESS-DATE
                    pdaFcpa115.getFcpa115_Accounting_Date().setValue(pdaFcpa115.getFcpa115_Next_Business_Date());
                    //*  CTS-0813
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa115.getFcpa115_Interface_Date().setValue(pdaFcpa115.getFcpa115_Payment_Date());                                                                //Natural: ASSIGN FCPA115.INTERFACE-DATE := FCPA115.ACCOUNTING-DATE := FCPA115.PAYMENT-DATE
                    pdaFcpa115.getFcpa115_Accounting_Date().setValue(pdaFcpa115.getFcpa115_Payment_Date());
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pnd_Alpha_Date.setValueEdited(pdaFcpa115.getFcpa115_Accounting_Date(),new ReportEditMask("99999999"));                                                        //Natural: MOVE EDITED FCPA115.ACCOUNTING-DATE ( EM = 99999999 ) TO #ALPHA-DATE
            //*  DTE EDITED
            ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Acctg_Dte().setValue(pnd_Alpha_Date);                                                                                   //Natural: MOVE #ALPHA-DATE TO #LEDGER-EXT-2.PYMNT-ACCTG-DTE
            //*  CTS-0813
            //*  CTS-0813
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Intrfce_Dte().setValueEdited(pdaFcpa115.getFcpa115_Interface_Date(),new ReportEditMask("99999999"));            //Natural: MOVE EDITED FCPA115.INTERFACE-DATE ( EM = 99999999 ) TO #C-PYMNT-INTRFCE-DTE
        }                                                                                                                                                                 //Natural: END-IF
        //* DATE-CONVERT-AREA
    }

    //
}
