/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:45 PM
**        * FROM NATURAL PROGRAM : Fcpp259
************************************************************
**        * FILE NAME            : Fcpp259.java
**        * CLASS NAME           : Fcpp259
**        * INSTANCE NAME        : Fcpp259
************************************************************
* FCPP259 - MATCH SORTED VOID RECS TO EXTRACTED RECS FROM FCPP258
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp259 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status;
    private DbsField fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt;
    private DbsField pnd_Fcpp258_Work_Rec;

    private DbsGroup pnd_Fcpp258_Work_Rec__R_Field_1;
    private DbsField pnd_Fcpp258_Work_Rec_Pnd_Work_Flag;
    private DbsField pnd_Fcpp258_Work_Rec_Pnd_Work_Isn;
    private DbsField pnd_Fcpp258_Work_Rec_Pnd_Work_Ppcn_Nbr;
    private DbsField pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Nbr;
    private DbsField pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt;

    private DbsGroup pnd_Fcpp258_Work_Rec__R_Field_2;
    private DbsField pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt_N;
    private DbsField pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Issue_Dt;
    private DbsField pnd_Sorted_Void_Rec;

    private DbsGroup pnd_Sorted_Void_Rec__R_Field_3;
    private DbsField pnd_Sorted_Void_Rec_Pnd_Sv_Filler1;
    private DbsField pnd_Sorted_Void_Rec_Pnd_Sv_Begin_Check_Nbr;
    private DbsField pnd_Sorted_Void_Rec_Pnd_Sv_Check_Amt;
    private DbsField pnd_Sorted_Void_Rec_Pnd_Sv_Check_Issue_Dt;
    private DbsField pnd_Sorted_Void_Rec_Pnd_Sv_Filler2;
    private DbsField pnd_Sorted_Void_Rec_Pnd_Sv_Ppcn_Nbr;

    private DbsGroup pnd_Sorted_Void_Rec__R_Field_4;
    private DbsField pnd_Sorted_Void_Rec_Pnd_Sv_Header;
    private DbsField pnd_Work_Eof;
    private DbsField pnd_Void_Eof;
    private DbsField pnd_Work_Key;

    private DbsGroup pnd_Work_Key__R_Field_5;
    private DbsField pnd_Work_Key_Pnd_Wk_Ppcn_Nbr;
    private DbsField pnd_Work_Key_Pnd_Wk_Check_Nbr;
    private DbsField pnd_Work_Key_Pnd_Wk_Check_Amt;
    private DbsField pnd_Last_Work_Key;
    private DbsField pnd_Void_Key;

    private DbsGroup pnd_Void_Key__R_Field_6;
    private DbsField pnd_Void_Key_Pnd_Vk_Ppcn_Nbr;
    private DbsField pnd_Void_Key_Pnd_Vk_Check_Nbr;
    private DbsField pnd_Void_Key_Pnd_Vk_Check_Amt;

    private DbsGroup pnd_Void_Key__R_Field_7;
    private DbsField pnd_Void_Key_Pnd_Vk_Check_Amt_N;
    private DbsField pnd_Last_Void_Key;
    private DbsField pnd_Parm_Card;

    private DbsGroup pnd_Parm_Card__R_Field_8;
    private DbsField pnd_Parm_Card_Pnd_Parm_Dt;

    private DbsGroup pnd_Parm_Card__R_Field_9;
    private DbsField pnd_Parm_Card_Pnd_Parm_Dt_N;
    private DbsField pnd_Blank;
    private DbsField pnd_Work_In;
    private DbsField pnd_Void_In;
    private DbsField pnd_Matched_Voids;
    private DbsField pnd_Unmatched_Voids;
    private DbsField pnd_Unmatched_Work;
    private DbsField pnd_Transmit_Dt;

    private DbsGroup pnd_Transmit_Dt__R_Field_10;
    private DbsField pnd_Transmit_Dt_Pnd_Transmit_Dt_A;
    private DbsField pnd_Et_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status", "CNR-CS-STOP-CANCEL-STATUS", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNR_CS_STOP_CANCEL_STATUS");
        fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt", "CNR-CS-TRANSMIT-DT", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNR_CS_TRANSMIT_DT");
        registerRecord(vw_fcp_Cons_Pymnt);

        pnd_Fcpp258_Work_Rec = localVariables.newFieldInRecord("pnd_Fcpp258_Work_Rec", "#FCPP258-WORK-REC", FieldType.STRING, 80);

        pnd_Fcpp258_Work_Rec__R_Field_1 = localVariables.newGroupInRecord("pnd_Fcpp258_Work_Rec__R_Field_1", "REDEFINE", pnd_Fcpp258_Work_Rec);
        pnd_Fcpp258_Work_Rec_Pnd_Work_Flag = pnd_Fcpp258_Work_Rec__R_Field_1.newFieldInGroup("pnd_Fcpp258_Work_Rec_Pnd_Work_Flag", "#WORK-FLAG", FieldType.STRING, 
            1);
        pnd_Fcpp258_Work_Rec_Pnd_Work_Isn = pnd_Fcpp258_Work_Rec__R_Field_1.newFieldInGroup("pnd_Fcpp258_Work_Rec_Pnd_Work_Isn", "#WORK-ISN", FieldType.NUMERIC, 
            11);
        pnd_Fcpp258_Work_Rec_Pnd_Work_Ppcn_Nbr = pnd_Fcpp258_Work_Rec__R_Field_1.newFieldInGroup("pnd_Fcpp258_Work_Rec_Pnd_Work_Ppcn_Nbr", "#WORK-PPCN-NBR", 
            FieldType.STRING, 8);
        pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Nbr = pnd_Fcpp258_Work_Rec__R_Field_1.newFieldInGroup("pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Nbr", "#WORK-CHECK-NBR", 
            FieldType.STRING, 11);
        pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt = pnd_Fcpp258_Work_Rec__R_Field_1.newFieldInGroup("pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt", "#WORK-CHECK-AMT", 
            FieldType.STRING, 15);

        pnd_Fcpp258_Work_Rec__R_Field_2 = pnd_Fcpp258_Work_Rec__R_Field_1.newGroupInGroup("pnd_Fcpp258_Work_Rec__R_Field_2", "REDEFINE", pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt);
        pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt_N = pnd_Fcpp258_Work_Rec__R_Field_2.newFieldInGroup("pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt_N", "#WORK-CHECK-AMT-N", 
            FieldType.NUMERIC, 15, 2);
        pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Issue_Dt = pnd_Fcpp258_Work_Rec__R_Field_1.newFieldInGroup("pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Issue_Dt", 
            "#WORK-CHECK-ISSUE-DT", FieldType.STRING, 8);
        pnd_Sorted_Void_Rec = localVariables.newFieldInRecord("pnd_Sorted_Void_Rec", "#SORTED-VOID-REC", FieldType.STRING, 150);

        pnd_Sorted_Void_Rec__R_Field_3 = localVariables.newGroupInRecord("pnd_Sorted_Void_Rec__R_Field_3", "REDEFINE", pnd_Sorted_Void_Rec);
        pnd_Sorted_Void_Rec_Pnd_Sv_Filler1 = pnd_Sorted_Void_Rec__R_Field_3.newFieldInGroup("pnd_Sorted_Void_Rec_Pnd_Sv_Filler1", "#SV-FILLER1", FieldType.STRING, 
            13);
        pnd_Sorted_Void_Rec_Pnd_Sv_Begin_Check_Nbr = pnd_Sorted_Void_Rec__R_Field_3.newFieldInGroup("pnd_Sorted_Void_Rec_Pnd_Sv_Begin_Check_Nbr", "#SV-BEGIN-CHECK-NBR", 
            FieldType.NUMERIC, 10);
        pnd_Sorted_Void_Rec_Pnd_Sv_Check_Amt = pnd_Sorted_Void_Rec__R_Field_3.newFieldInGroup("pnd_Sorted_Void_Rec_Pnd_Sv_Check_Amt", "#SV-CHECK-AMT", 
            FieldType.NUMERIC, 10, 2);
        pnd_Sorted_Void_Rec_Pnd_Sv_Check_Issue_Dt = pnd_Sorted_Void_Rec__R_Field_3.newFieldInGroup("pnd_Sorted_Void_Rec_Pnd_Sv_Check_Issue_Dt", "#SV-CHECK-ISSUE-DT", 
            FieldType.STRING, 8);
        pnd_Sorted_Void_Rec_Pnd_Sv_Filler2 = pnd_Sorted_Void_Rec__R_Field_3.newFieldInGroup("pnd_Sorted_Void_Rec_Pnd_Sv_Filler2", "#SV-FILLER2", FieldType.STRING, 
            1);
        pnd_Sorted_Void_Rec_Pnd_Sv_Ppcn_Nbr = pnd_Sorted_Void_Rec__R_Field_3.newFieldInGroup("pnd_Sorted_Void_Rec_Pnd_Sv_Ppcn_Nbr", "#SV-PPCN-NBR", FieldType.STRING, 
            8);

        pnd_Sorted_Void_Rec__R_Field_4 = localVariables.newGroupInRecord("pnd_Sorted_Void_Rec__R_Field_4", "REDEFINE", pnd_Sorted_Void_Rec);
        pnd_Sorted_Void_Rec_Pnd_Sv_Header = pnd_Sorted_Void_Rec__R_Field_4.newFieldInGroup("pnd_Sorted_Void_Rec_Pnd_Sv_Header", "#SV-HEADER", FieldType.STRING, 
            20);
        pnd_Work_Eof = localVariables.newFieldInRecord("pnd_Work_Eof", "#WORK-EOF", FieldType.BOOLEAN, 1);
        pnd_Void_Eof = localVariables.newFieldInRecord("pnd_Void_Eof", "#VOID-EOF", FieldType.BOOLEAN, 1);
        pnd_Work_Key = localVariables.newFieldInRecord("pnd_Work_Key", "#WORK-KEY", FieldType.STRING, 34);

        pnd_Work_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Work_Key__R_Field_5", "REDEFINE", pnd_Work_Key);
        pnd_Work_Key_Pnd_Wk_Ppcn_Nbr = pnd_Work_Key__R_Field_5.newFieldInGroup("pnd_Work_Key_Pnd_Wk_Ppcn_Nbr", "#WK-PPCN-NBR", FieldType.STRING, 8);
        pnd_Work_Key_Pnd_Wk_Check_Nbr = pnd_Work_Key__R_Field_5.newFieldInGroup("pnd_Work_Key_Pnd_Wk_Check_Nbr", "#WK-CHECK-NBR", FieldType.STRING, 11);
        pnd_Work_Key_Pnd_Wk_Check_Amt = pnd_Work_Key__R_Field_5.newFieldInGroup("pnd_Work_Key_Pnd_Wk_Check_Amt", "#WK-CHECK-AMT", FieldType.STRING, 15);
        pnd_Last_Work_Key = localVariables.newFieldInRecord("pnd_Last_Work_Key", "#LAST-WORK-KEY", FieldType.STRING, 34);
        pnd_Void_Key = localVariables.newFieldInRecord("pnd_Void_Key", "#VOID-KEY", FieldType.STRING, 34);

        pnd_Void_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Void_Key__R_Field_6", "REDEFINE", pnd_Void_Key);
        pnd_Void_Key_Pnd_Vk_Ppcn_Nbr = pnd_Void_Key__R_Field_6.newFieldInGroup("pnd_Void_Key_Pnd_Vk_Ppcn_Nbr", "#VK-PPCN-NBR", FieldType.STRING, 8);
        pnd_Void_Key_Pnd_Vk_Check_Nbr = pnd_Void_Key__R_Field_6.newFieldInGroup("pnd_Void_Key_Pnd_Vk_Check_Nbr", "#VK-CHECK-NBR", FieldType.NUMERIC, 11);
        pnd_Void_Key_Pnd_Vk_Check_Amt = pnd_Void_Key__R_Field_6.newFieldInGroup("pnd_Void_Key_Pnd_Vk_Check_Amt", "#VK-CHECK-AMT", FieldType.STRING, 15);

        pnd_Void_Key__R_Field_7 = pnd_Void_Key__R_Field_6.newGroupInGroup("pnd_Void_Key__R_Field_7", "REDEFINE", pnd_Void_Key_Pnd_Vk_Check_Amt);
        pnd_Void_Key_Pnd_Vk_Check_Amt_N = pnd_Void_Key__R_Field_7.newFieldInGroup("pnd_Void_Key_Pnd_Vk_Check_Amt_N", "#VK-CHECK-AMT-N", FieldType.NUMERIC, 
            15, 2);
        pnd_Last_Void_Key = localVariables.newFieldInRecord("pnd_Last_Void_Key", "#LAST-VOID-KEY", FieldType.STRING, 34);
        pnd_Parm_Card = localVariables.newFieldInRecord("pnd_Parm_Card", "#PARM-CARD", FieldType.STRING, 80);

        pnd_Parm_Card__R_Field_8 = localVariables.newGroupInRecord("pnd_Parm_Card__R_Field_8", "REDEFINE", pnd_Parm_Card);
        pnd_Parm_Card_Pnd_Parm_Dt = pnd_Parm_Card__R_Field_8.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Dt", "#PARM-DT", FieldType.STRING, 8);

        pnd_Parm_Card__R_Field_9 = pnd_Parm_Card__R_Field_8.newGroupInGroup("pnd_Parm_Card__R_Field_9", "REDEFINE", pnd_Parm_Card_Pnd_Parm_Dt);
        pnd_Parm_Card_Pnd_Parm_Dt_N = pnd_Parm_Card__R_Field_9.newFieldInGroup("pnd_Parm_Card_Pnd_Parm_Dt_N", "#PARM-DT-N", FieldType.NUMERIC, 8);
        pnd_Blank = localVariables.newFieldInRecord("pnd_Blank", "#BLANK", FieldType.STRING, 1);
        pnd_Work_In = localVariables.newFieldInRecord("pnd_Work_In", "#WORK-IN", FieldType.PACKED_DECIMAL, 7);
        pnd_Void_In = localVariables.newFieldInRecord("pnd_Void_In", "#VOID-IN", FieldType.PACKED_DECIMAL, 7);
        pnd_Matched_Voids = localVariables.newFieldInRecord("pnd_Matched_Voids", "#MATCHED-VOIDS", FieldType.PACKED_DECIMAL, 7);
        pnd_Unmatched_Voids = localVariables.newFieldInRecord("pnd_Unmatched_Voids", "#UNMATCHED-VOIDS", FieldType.PACKED_DECIMAL, 7);
        pnd_Unmatched_Work = localVariables.newFieldInRecord("pnd_Unmatched_Work", "#UNMATCHED-WORK", FieldType.PACKED_DECIMAL, 7);
        pnd_Transmit_Dt = localVariables.newFieldInRecord("pnd_Transmit_Dt", "#TRANSMIT-DT", FieldType.NUMERIC, 8);

        pnd_Transmit_Dt__R_Field_10 = localVariables.newGroupInRecord("pnd_Transmit_Dt__R_Field_10", "REDEFINE", pnd_Transmit_Dt);
        pnd_Transmit_Dt_Pnd_Transmit_Dt_A = pnd_Transmit_Dt__R_Field_10.newFieldInGroup("pnd_Transmit_Dt_Pnd_Transmit_Dt_A", "#TRANSMIT-DT-A", FieldType.STRING, 
            8);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();

        localVariables.reset();
        pnd_Work_Eof.setInitialValue(false);
        pnd_Void_Eof.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp259() throws Exception
    {
        super("Fcpp259");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT PS = 55 LS = 80;//Natural: FORMAT ( 1 ) PS = 55 LS = 80;//Natural: FORMAT ( 2 ) PS = 55 LS = 80;//Natural: FORMAT ( 3 ) PS = 55 LS = 80
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE TITLE LEFT *DATU 7X 'VOID RECONCILIATION PROCESS - UPDATE TRANSMIT DATE' 6X *PROGRAM / *TIME ( EM = X ( 8 ) ) 7X '--------------------------------------------------' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU 6X 'VOID RECONCILIATION PROCESS - TRANSMIT DATE UPDATED' 6X *PROGRAM / *TIME ( EM = X ( 8 ) ) 6X '---------------------------------------------------' 6X 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = Z9 ) // 6X 'STAT' 7X 'PPCN' 7X 'CHECK NBR' 7X 'CHECK AMT    ISSUE DT    *ISN' / 6X '----' 7X '----' 7X '---------' 7X '---------    --------    ----' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *DATU 5X 'VOID RECONCILIATION PROCESS - UNMATCHED DATABASE RECS' 5X *PROGRAM / *TIME ( EM = X ( 8 ) ) 5X '-----------------------------------------------------' 5X 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = Z9 ) // 6X 'STAT' 7X 'PPCN' 7X 'CHECK NBR' 7X 'CHECK AMT    ISSUE DT    *ISN' / 6X '----' 7X '----' 7X '---------' 7X '---------    --------    ----' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT *DATU 4X 'VOID RECONCILIATION PROCESS - UNMATCHED WACHO VOID RECS' 4X *PROGRAM / *TIME ( EM = X ( 8 ) ) 4X '-------------------------------------------------------' 4X 'PAGE:' *PAGE-NUMBER ( 3 ) ( EM = Z9 ) // 15X 'PPCN' 11X 'CHECK NBR' 10X 'CHECK AMT' 5X 'ISSUE DT' / 15X '----' 11X '---------' 10X '---------' 5X '--------' //
                                                                                                                                                                          //Natural: PERFORM #P6000-READ-PARM-FILE
        sub_Pnd_P6000_Read_Parm_File();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #P1000-READ-WORK-FILE
        sub_Pnd_P1000_Read_Work_File();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM #P2000-READ-VOID-FILE
        sub_Pnd_P2000_Read_Void_File();
        if (condition(Global.isEscape())) {return;}
        RP1:                                                                                                                                                              //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Work_Eof.getBoolean() && pnd_Void_Eof.getBoolean())) {break;}                                                                               //Natural: UNTIL #WORK-EOF AND #VOID-EOF
            if (condition(pnd_Work_Key.less(pnd_Void_Key)))                                                                                                               //Natural: IF #WORK-KEY < #VOID-KEY
            {
                                                                                                                                                                          //Natural: PERFORM #P4000-WRITE-UNMATCHED-WORK
                sub_Pnd_P4000_Write_Unmatched_Work();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #P1000-READ-WORK-FILE
                sub_Pnd_P1000_Read_Work_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Key.greater(pnd_Void_Key)))                                                                                                            //Natural: IF #WORK-KEY > #VOID-KEY
            {
                                                                                                                                                                          //Natural: PERFORM #P5000-WRITE-UNMATCHED-VOID
                sub_Pnd_P5000_Write_Unmatched_Void();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #P2000-READ-VOID-FILE
                sub_Pnd_P2000_Read_Void_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Work_Key.equals(pnd_Void_Key) && ! (pnd_Work_Eof.getBoolean())))                                                                            //Natural: IF #WORK-KEY = #VOID-KEY AND NOT #WORK-EOF
            {
                                                                                                                                                                          //Natural: PERFORM #P3000-PROCESS-MATCH
                sub_Pnd_P3000_Process_Match();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #P1000-READ-WORK-FILE
                sub_Pnd_P1000_Read_Work_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM #P2000-READ-VOID-FILE
                sub_Pnd_P2000_Read_Void_File();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  (RP1.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(0, " ",NEWLINE,NEWLINE);                                                                                                                       //Natural: WRITE ' ' //
        if (Global.isEscape()) return;
        getReports().write(0, new ColumnSpacing(33),"CONTROL TOTALS",NEWLINE,new ColumnSpacing(33),"--------------",NEWLINE,NEWLINE,new ColumnSpacing(24),"DB WORK RECS READ......",pnd_Work_In,  //Natural: WRITE 33X 'CONTROL TOTALS' / 33X '--------------' // 24X 'DB WORK RECS READ......' #WORK-IN ( EM = Z,ZZZ,ZZ9 ) / 24X 'WACHO VOID RECS READ...' #VOID-IN ( EM = Z,ZZZ,ZZ9 ) / 24X 'MATCHES FOUND..........' #MATCHED-VOIDS ( EM = Z,ZZZ,ZZ9 ) / 24X 'UNMATCHED DB WORK RECS.' #UNMATCHED-WORK ( EM = Z,ZZZ,ZZ9 ) / 24X 'UNMATCHED WACHO VOIDS..' #UNMATCHED-VOIDS ( EM = Z,ZZZ,ZZ9 )
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(24),"WACHO VOID RECS READ...",pnd_Void_In, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new 
            ColumnSpacing(24),"MATCHES FOUND..........",pnd_Matched_Voids, new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(24),"UNMATCHED DB WORK RECS.",pnd_Unmatched_Work, 
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,new ColumnSpacing(24),"UNMATCHED WACHO VOIDS..",pnd_Unmatched_Voids, new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        if (condition(pnd_Matched_Voids.equals(getZero())))                                                                                                               //Natural: IF #MATCHED-VOIDS = 0
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(23),"***** NO MATCHES WERE FOUND *****");                                             //Natural: WRITE ( 1 ) //// 23X '***** NO MATCHES WERE FOUND *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Unmatched_Work.equals(getZero())))                                                                                                              //Natural: IF #UNMATCHED-WORK = 0
        {
            getReports().write(2, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(19),"***** NO UNMATCHED DB RECORDS WERE FOUND *****");                                //Natural: WRITE ( 2 ) //// 19X '***** NO UNMATCHED DB RECORDS WERE FOUND *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Unmatched_Voids.equals(getZero())))                                                                                                             //Natural: IF #UNMATCHED-VOIDS = 0
        {
            getReports().write(3, NEWLINE,NEWLINE,NEWLINE,NEWLINE,new ColumnSpacing(17),"***** NO UNMATCHED WACHO RECORDS FOUND *****");                                  //Natural: WRITE ( 3 ) //// 17X '***** NO UNMATCHED WACHO RECORDS FOUND *****'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.stop();  if (true) return;                                                                                                                                //Natural: STOP
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #P1000-READ-WORK-FILE
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #P2000-READ-VOID-FILE
        //* **************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #P3000-PROCESS-MATCH
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #P4000-WRITE-UNMATCHED-WORK
        //* ********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #P5000-WRITE-UNMATCHED-VOID
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: #P6000-READ-PARM-FILE
    }
    private void sub_Pnd_P1000_Read_Work_File() throws Exception                                                                                                          //Natural: #P1000-READ-WORK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getWorkFiles().read(2, pnd_Fcpp258_Work_Rec);                                                                                                                     //Natural: READ WORK FILE 2 ONCE #FCPP258-WORK-REC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            pnd_Work_Key.moveAll("9");                                                                                                                                    //Natural: MOVE ALL '9' TO #WORK-KEY
            pnd_Work_Eof.setValue(true);                                                                                                                                  //Natural: ASSIGN #WORK-EOF = TRUE
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE IMMEDIATE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Work_In.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #WORK-IN
        pnd_Last_Work_Key.setValue(pnd_Work_Key);                                                                                                                         //Natural: MOVE #WORK-KEY TO #LAST-WORK-KEY
        pnd_Work_Key_Pnd_Wk_Ppcn_Nbr.setValue(pnd_Fcpp258_Work_Rec_Pnd_Work_Ppcn_Nbr);                                                                                    //Natural: MOVE #WORK-PPCN-NBR TO #WK-PPCN-NBR
        pnd_Work_Key_Pnd_Wk_Check_Nbr.setValue(pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Nbr);                                                                                  //Natural: MOVE #WORK-CHECK-NBR TO #WK-CHECK-NBR
        pnd_Work_Key_Pnd_Wk_Check_Amt.setValue(pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt);                                                                                  //Natural: MOVE #WORK-CHECK-AMT TO #WK-CHECK-AMT
        if (condition(pnd_Work_Key.less(pnd_Last_Work_Key)))                                                                                                              //Natural: IF #WORK-KEY < #LAST-WORK-KEY
        {
            getReports().write(0, new ColumnSpacing(19),"*****************************************",NEWLINE,new ColumnSpacing(19),"*** WORK FILE 1 IS OUT OF SEQUENCE.   ***",NEWLINE,NEWLINE,new  //Natural: WRITE 19X '*****************************************' / 19X '*** WORK FILE 1 IS OUT OF SEQUENCE.   ***' // 19X '*** CORRECT ERROR AND RESTART.        ***' // 19X '***' '=' *PROGRAM 5X // 19X '*** DATE:' *DATU // 19X '*** TIME:' *TIME // 19X '*** RECORD COUNT =' #WORK-IN // 19X '*** LAST REC KEY =' #LAST-WORK-KEY / 19X '*** THIS REC KEY =' #WORK-KEY / 19X '*****************************************'
                ColumnSpacing(19),"*** CORRECT ERROR AND RESTART.        ***",NEWLINE,NEWLINE,new ColumnSpacing(19),"***","PROG: ",Global.getPROGRAM(),new 
                ColumnSpacing(5),NEWLINE,NEWLINE,new ColumnSpacing(19),"*** DATE:",Global.getDATU(),NEWLINE,NEWLINE,new ColumnSpacing(19),"*** TIME:",Global.getTIME(),NEWLINE,NEWLINE,new 
                ColumnSpacing(19),"*** RECORD COUNT =",pnd_Work_In,NEWLINE,NEWLINE,new ColumnSpacing(19),"*** LAST REC KEY =",pnd_Last_Work_Key,NEWLINE,new 
                ColumnSpacing(19),"*** THIS REC KEY =",pnd_Work_Key,NEWLINE,new ColumnSpacing(19),"*****************************************");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
        //*  (1630) #P1000-READ-WORK-FILE */
    }
    //*  DUMMY LOOP TO ALLOW ESCAPE TOP
    private void sub_Pnd_P2000_Read_Void_File() throws Exception                                                                                                          //Natural: #P2000-READ-VOID-FILE
    {
        if (BLNatReinput.isReinput()) return;

        RP2:                                                                                                                                                              //Natural: REPEAT
        while (condition(whileTrue))
        {
            getWorkFiles().read(3, pnd_Sorted_Void_Rec);                                                                                                                  //Natural: READ WORK FILE 3 ONCE #SORTED-VOID-REC
            if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                      //Natural: AT END OF FILE
            {
                pnd_Void_Key.moveAll("9");                                                                                                                                //Natural: MOVE ALL '9' TO #VOID-KEY
                pnd_Void_Eof.setValue(true);                                                                                                                              //Natural: ASSIGN #VOID-EOF = TRUE
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE IMMEDIATE
            }                                                                                                                                                             //Natural: END-ENDFILE
            if (condition(pnd_Sorted_Void_Rec_Pnd_Sv_Header.equals("RECONCILIATIONHEADER")))                                                                              //Natural: IF #SV-HEADER = 'RECONCILIATIONHEADER'
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Void_In.nadd(1);                                                                                                                                          //Natural: ADD 1 TO #VOID-IN
            pnd_Last_Void_Key.setValue(pnd_Void_Key);                                                                                                                     //Natural: MOVE #VOID-KEY TO #LAST-VOID-KEY
            pnd_Void_Key_Pnd_Vk_Ppcn_Nbr.setValue(pnd_Sorted_Void_Rec_Pnd_Sv_Ppcn_Nbr);                                                                                   //Natural: MOVE #SV-PPCN-NBR TO #VK-PPCN-NBR
            pnd_Void_Key_Pnd_Vk_Check_Nbr.setValue(pnd_Sorted_Void_Rec_Pnd_Sv_Begin_Check_Nbr);                                                                           //Natural: MOVE #SV-BEGIN-CHECK-NBR TO #VK-CHECK-NBR
            pnd_Void_Key_Pnd_Vk_Check_Amt_N.setValue(pnd_Sorted_Void_Rec_Pnd_Sv_Check_Amt);                                                                               //Natural: MOVE #SV-CHECK-AMT TO #VK-CHECK-AMT-N
            if (condition(pnd_Void_Key.less(pnd_Last_Void_Key)))                                                                                                          //Natural: IF #VOID-KEY < #LAST-VOID-KEY
            {
                getReports().write(0, new ColumnSpacing(19),"*****************************************",NEWLINE,new ColumnSpacing(19),"*** WORK FILE 2 IS OUT OF SEQUENCE.   ***",NEWLINE,NEWLINE,new  //Natural: WRITE 19X '*****************************************' / 19X '*** WORK FILE 2 IS OUT OF SEQUENCE.   ***' // 19X '*** CORRECT ERROR AND RESTART.        ***' // 19X '***' '=' *PROGRAM 5X // 19X '*** DATE:' *DATU // 19X '*** TIME:' *TIME // 19X '*** RECORD COUNT =' #VOID-IN // 19X '*** LAST REC KEY =' #LAST-VOID-KEY / 19X '*** THIS REC KEY =' #VOID-KEY / 19X '*****************************************'
                    ColumnSpacing(19),"*** CORRECT ERROR AND RESTART.        ***",NEWLINE,NEWLINE,new ColumnSpacing(19),"***","PROG: ",Global.getPROGRAM(),new 
                    ColumnSpacing(5),NEWLINE,NEWLINE,new ColumnSpacing(19),"*** DATE:",Global.getDATU(),NEWLINE,NEWLINE,new ColumnSpacing(19),"*** TIME:",Global.getTIME(),NEWLINE,NEWLINE,new 
                    ColumnSpacing(19),"*** RECORD COUNT =",pnd_Void_In,NEWLINE,NEWLINE,new ColumnSpacing(19),"*** LAST REC KEY =",pnd_Last_Void_Key,NEWLINE,new 
                    ColumnSpacing(19),"*** THIS REC KEY =",pnd_Void_Key,NEWLINE,new ColumnSpacing(19),"*****************************************");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RP2"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RP2"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                DbsUtil.terminate(91);  if (true) return;                                                                                                                 //Natural: TERMINATE 91
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM IMMEDIATE
            //*  (RP2.)
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        //*  (1960) #P2000-READ-VOID-FILE */
    }
    private void sub_Pnd_P3000_Process_Match() throws Exception                                                                                                           //Natural: #P3000-PROCESS-MATCH
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        pnd_Matched_Voids.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #MATCHED-VOIDS
        getReports().write(1, new ColumnSpacing(8),pnd_Fcpp258_Work_Rec_Pnd_Work_Flag,new ColumnSpacing(5),pnd_Fcpp258_Work_Rec_Pnd_Work_Ppcn_Nbr,new                     //Natural: WRITE ( 1 ) 8X #WORK-FLAG 5X #WORK-PPCN-NBR 5X #WORK-CHECK-NBR 2X #WORK-CHECK-AMT-N ( EM = ZZ,ZZZ,ZZ9.99 ) 4X #WORK-CHECK-ISSUE-DT 2X #WORK-ISN ( EM = ZZZZZZZ9 )
            ColumnSpacing(5),pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Nbr,new ColumnSpacing(2),pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt_N, new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(4),pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Issue_Dt,new ColumnSpacing(2),pnd_Fcpp258_Work_Rec_Pnd_Work_Isn, 
            new ReportEditMask ("ZZZZZZZ9"));
        if (Global.isEscape()) return;
        GT1:                                                                                                                                                              //Natural: GET FCP-CONS-PYMNT #WORK-ISN
        vw_fcp_Cons_Pymnt.readByID(pnd_Fcpp258_Work_Rec_Pnd_Work_Isn.getLong(), "GT1");
        fcp_Cons_Pymnt_Cnr_Cs_Stop_Cancel_Status.setValue("D");                                                                                                           //Natural: ASSIGN CNR-CS-STOP-CANCEL-STATUS = 'D'
        fcp_Cons_Pymnt_Cnr_Cs_Transmit_Dt.setValue(pnd_Transmit_Dt);                                                                                                      //Natural: ASSIGN CNR-CS-TRANSMIT-DT = #TRANSMIT-DT
        vw_fcp_Cons_Pymnt.updateDBRow("GT1");                                                                                                                             //Natural: UPDATE ( GT1. )
        pnd_Et_Count.nadd(1);                                                                                                                                             //Natural: ADD 1 TO #ET-COUNT
        if (condition(pnd_Et_Count.greater(24)))                                                                                                                          //Natural: IF #ET-COUNT > 24
        {
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
            pnd_Et_Count.reset();                                                                                                                                         //Natural: RESET #ET-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        //*  (2390) #P3000-PROCESS-MATCH */
    }
    private void sub_Pnd_P4000_Write_Unmatched_Work() throws Exception                                                                                                    //Natural: #P4000-WRITE-UNMATCHED-WORK
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        pnd_Unmatched_Work.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #UNMATCHED-WORK
        getReports().write(2, new ColumnSpacing(8),pnd_Fcpp258_Work_Rec_Pnd_Work_Flag,new ColumnSpacing(5),pnd_Fcpp258_Work_Rec_Pnd_Work_Ppcn_Nbr,new                     //Natural: WRITE ( 2 ) 8X #WORK-FLAG 5X #WORK-PPCN-NBR 5X #WORK-CHECK-NBR 2X #WORK-CHECK-AMT-N ( EM = ZZ,ZZZ,ZZ9.99 ) 4X #WORK-CHECK-ISSUE-DT 2X #WORK-ISN ( EM = ZZZZZZZ9 )
            ColumnSpacing(5),pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Nbr,new ColumnSpacing(2),pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Amt_N, new ReportEditMask 
            ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(4),pnd_Fcpp258_Work_Rec_Pnd_Work_Check_Issue_Dt,new ColumnSpacing(2),pnd_Fcpp258_Work_Rec_Pnd_Work_Isn, 
            new ReportEditMask ("ZZZZZZZ9"));
        if (Global.isEscape()) return;
        //*  (2630) #P4000-WRITE-UNMATCHED-WORK */
    }
    private void sub_Pnd_P5000_Write_Unmatched_Void() throws Exception                                                                                                    //Natural: #P5000-WRITE-UNMATCHED-VOID
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************************
        pnd_Unmatched_Voids.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #UNMATCHED-VOIDS
        getReports().write(3, new ColumnSpacing(13),pnd_Sorted_Void_Rec_Pnd_Sv_Ppcn_Nbr,new ColumnSpacing(8),pnd_Sorted_Void_Rec_Pnd_Sv_Begin_Check_Nbr,new               //Natural: WRITE ( 3 ) 13X #SV-PPCN-NBR 8X #SV-BEGIN-CHECK-NBR 5X #SV-CHECK-AMT ( EM = ZZ,ZZZ,ZZ9.99 ) 5X #SV-CHECK-ISSUE-DT
            ColumnSpacing(5),pnd_Sorted_Void_Rec_Pnd_Sv_Check_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99"),new ColumnSpacing(5),pnd_Sorted_Void_Rec_Pnd_Sv_Check_Issue_Dt);
        if (Global.isEscape()) return;
        //*  (2730) #P5000-WRITE-UNMATCHED-VOID */
    }
    private void sub_Pnd_P6000_Read_Parm_File() throws Exception                                                                                                          //Natural: #P6000-READ-PARM-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        getWorkFiles().read(1, pnd_Parm_Card);                                                                                                                            //Natural: READ WORK FILE 1 ONCE #PARM-CARD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            getReports().write(0, new ColumnSpacing(24),"********************************",NEWLINE,new ColumnSpacing(24),"*  PARAMETER CARD IS MISSING.  *",NEWLINE,new   //Natural: WRITE 24X '********************************' / 24X '*  PARAMETER CARD IS MISSING.  *' / 24X '*     JOB IS TERMINATING.      *' / 24X '********************************'
                ColumnSpacing(24),"*     JOB IS TERMINATING.      *",NEWLINE,new ColumnSpacing(24),"********************************");
            if (Global.isEscape()) return;
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(pnd_Parm_Card_Pnd_Parm_Dt.equals(" ") || DbsUtil.maskMatches(pnd_Parm_Card_Pnd_Parm_Dt,"YYYYMMDD")))                                                //Natural: IF #PARM-DT = ' ' OR #PARM-DT = MASK ( YYYYMMDD )
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(0, new ColumnSpacing(17),"**********************************************",NEWLINE,new ColumnSpacing(17),"*  OVERRIDE DATE MUST BE BLANK OR YYYYMMDD   *",NEWLINE,new  //Natural: WRITE 17X '**********************************************' / 17X '*  OVERRIDE DATE MUST BE BLANK OR YYYYMMDD   *' / 17X '*             JOB IS TERMINATING.            *' / 17X '**********************************************'
                ColumnSpacing(17),"*             JOB IS TERMINATING.            *",NEWLINE,new ColumnSpacing(17),"**********************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_Pnd_Parm_Dt);                                                                           //Natural: WRITE / 17X '=' #PARM-DT
            if (Global.isEscape()) return;
            DbsUtil.terminate(91);  if (true) return;                                                                                                                     //Natural: TERMINATE 91
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Card_Pnd_Parm_Dt.notEquals(" ") && pnd_Parm_Card_Pnd_Parm_Dt_N.greater(Global.getDATN())))                                                 //Natural: IF #PARM-DT NOT = ' ' AND #PARM-DT-N > *DATN
        {
            getReports().write(0, new ColumnSpacing(17),"**********************************************",NEWLINE,new ColumnSpacing(17),"*  OVERRIDE DATE CANNOT BE IN THE FUTURE.    *",NEWLINE,new  //Natural: WRITE 17X '**********************************************' / 17X '*  OVERRIDE DATE CANNOT BE IN THE FUTURE.    *' / 17X '*             JOB IS TERMINATING.            *' / 17X '**********************************************'
                ColumnSpacing(17),"*             JOB IS TERMINATING.            *",NEWLINE,new ColumnSpacing(17),"**********************************************");
            if (Global.isEscape()) return;
            getReports().write(0, NEWLINE,new ColumnSpacing(17),"=",pnd_Parm_Card_Pnd_Parm_Dt);                                                                           //Natural: WRITE / 17X '=' #PARM-DT
            if (Global.isEscape()) return;
            DbsUtil.terminate(92);  if (true) return;                                                                                                                     //Natural: TERMINATE 92
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Parm_Card_Pnd_Parm_Dt.notEquals(" ")))                                                                                                          //Natural: IF #PARM-DT NOT = ' '
        {
            pnd_Transmit_Dt.setValue(pnd_Parm_Card_Pnd_Parm_Dt_N);                                                                                                        //Natural: ASSIGN #TRANSMIT-DT = #PARM-DT-N
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Transmit_Dt.setValue(Global.getDATN());                                                                                                                   //Natural: ASSIGN #TRANSMIT-DT = *DATN
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, new ColumnSpacing(18),"*********************************************",NEWLINE,new ColumnSpacing(18),"*       PARM CARD EDIT WAS SUCCESSFUL       *",NEWLINE,new  //Natural: WRITE 18X '*********************************************' / 18X '*       PARM CARD EDIT WAS SUCCESSFUL       *' / 18X '*                                           *' / 18X '*        PARM OVERRIDE DATE:' #PARM-DT 7X '*' / 18X '*      DATE USED FOR UPDATE:' #TRANSMIT-DT-A 7X '*' / 18X '*********************************************'
            ColumnSpacing(18),"*                                           *",NEWLINE,new ColumnSpacing(18),"*        PARM OVERRIDE DATE:",pnd_Parm_Card_Pnd_Parm_Dt,new 
            ColumnSpacing(7),"*",NEWLINE,new ColumnSpacing(18),"*      DATE USED FOR UPDATE:",pnd_Transmit_Dt_Pnd_Transmit_Dt_A,new ColumnSpacing(7),"*",NEWLINE,new 
            ColumnSpacing(18),"*********************************************");
        if (Global.isEscape()) return;
        //*  (2840) #P6000-READ-PARM-FILE */
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=55 LS=80");
        Global.format(1, "PS=55 LS=80");
        Global.format(2, "PS=55 LS=80");
        Global.format(3, "PS=55 LS=80");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(7),"VOID RECONCILIATION PROCESS - UPDATE TRANSMIT DATE",new 
            ColumnSpacing(6),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(7),"--------------------------------------------------",
            NEWLINE,NEWLINE);
        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(6),"VOID RECONCILIATION PROCESS - TRANSMIT DATE UPDATED",new 
            ColumnSpacing(6),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(6),"---------------------------------------------------",new 
            ColumnSpacing(6),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("Z9"),NEWLINE,NEWLINE,new ColumnSpacing(6),"STAT",new ColumnSpacing(7),"PPCN",new 
            ColumnSpacing(7),"CHECK NBR",new ColumnSpacing(7),"CHECK AMT    ISSUE DT    *ISN",NEWLINE,new ColumnSpacing(6),"----",new ColumnSpacing(7),"----",new 
            ColumnSpacing(7),"---------",new ColumnSpacing(7),"---------    --------    ----",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(5),"VOID RECONCILIATION PROCESS - UNMATCHED DATABASE RECS",new 
            ColumnSpacing(5),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(5),"-----------------------------------------------------",new 
            ColumnSpacing(5),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("Z9"),NEWLINE,NEWLINE,new ColumnSpacing(6),"STAT",new ColumnSpacing(7),"PPCN",new 
            ColumnSpacing(7),"CHECK NBR",new ColumnSpacing(7),"CHECK AMT    ISSUE DT    *ISN",NEWLINE,new ColumnSpacing(6),"----",new ColumnSpacing(7),"----",new 
            ColumnSpacing(7),"---------",new ColumnSpacing(7),"---------    --------    ----",NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),new ColumnSpacing(4),"VOID RECONCILIATION PROCESS - UNMATCHED WACHO VOID RECS",new 
            ColumnSpacing(4),Global.getPROGRAM(),NEWLINE,Global.getTIME(), new ReportEditMask ("XXXXXXXX"),new ColumnSpacing(4),"-------------------------------------------------------",new 
            ColumnSpacing(4),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("Z9"),NEWLINE,NEWLINE,new ColumnSpacing(15),"PPCN",new ColumnSpacing(11),"CHECK NBR",new 
            ColumnSpacing(10),"CHECK AMT",new ColumnSpacing(5),"ISSUE DT",NEWLINE,new ColumnSpacing(15),"----",new ColumnSpacing(11),"---------",new ColumnSpacing(10),"---------",new 
            ColumnSpacing(5),"--------",NEWLINE,NEWLINE);
    }
}
