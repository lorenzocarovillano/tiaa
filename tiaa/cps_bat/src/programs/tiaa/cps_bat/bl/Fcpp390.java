/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:38 PM
**        * FROM NATURAL PROGRAM : Fcpp390
************************************************************
**        * FILE NAME            : Fcpp390.java
**        * CLASS NAME           : Fcpp390
**        * INSTANCE NAME        : Fcpp390
************************************************************
************************************************************************
* PROGRAM  : FCPP390
* SYSTEM   : CPS
* TITLE    : SUPPRESS CONTROL RECORD
* FUNCTION : THIS PROGRAM WILL SUPPRESS READING THE MOST CURRENT
*            CONTROL RECORD OF THE SYSTEM(S) SPECIFIED IN THE
*            INPUT.
*
* HISTORY  :   ORIGINAL DATE: 10/19/94
*
* RIAD LOUTFI : RETIRMENT LOAN (AL) LOGIC ADDED.
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp390 extends BLNatBase
{
    // Data Areas
    private PdaFcpacntr pdaFcpacntr;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Index;
    private DbsField pnd_Update_Cnt;
    private DbsField pnd_Message;
    private DbsField pnd_Input_Orgn_Codes;

    private DbsGroup pnd_Input_Orgn_Codes__R_Field_1;

    private DbsGroup pnd_Input_Orgn_Codes_Pnd_From_To_Orgn;
    private DbsField pnd_Input_Orgn_Codes_Pnd_Orgn_Cde;
    private DbsField pnd_Input_Orgn_Codes__Filler1;
    private DbsField pnd_Input_Orgn_Codes_Pnd_New_Orgn_Cde;
    private DbsField pnd_Input_Orgn_Codes__Filler2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpacntr = new PdaFcpacntr(localVariables);

        // Local Variables
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 3);
        pnd_Update_Cnt = localVariables.newFieldInRecord("pnd_Update_Cnt", "#UPDATE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Message = localVariables.newFieldInRecord("pnd_Message", "#MESSAGE", FieldType.STRING, 38);
        pnd_Input_Orgn_Codes = localVariables.newFieldInRecord("pnd_Input_Orgn_Codes", "#INPUT-ORGN-CODES", FieldType.STRING, 60);

        pnd_Input_Orgn_Codes__R_Field_1 = localVariables.newGroupInRecord("pnd_Input_Orgn_Codes__R_Field_1", "REDEFINE", pnd_Input_Orgn_Codes);

        pnd_Input_Orgn_Codes_Pnd_From_To_Orgn = pnd_Input_Orgn_Codes__R_Field_1.newGroupArrayInGroup("pnd_Input_Orgn_Codes_Pnd_From_To_Orgn", "#FROM-TO-ORGN", 
            new DbsArrayController(1, 10));
        pnd_Input_Orgn_Codes_Pnd_Orgn_Cde = pnd_Input_Orgn_Codes_Pnd_From_To_Orgn.newFieldInGroup("pnd_Input_Orgn_Codes_Pnd_Orgn_Cde", "#ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Input_Orgn_Codes__Filler1 = pnd_Input_Orgn_Codes_Pnd_From_To_Orgn.newFieldInGroup("pnd_Input_Orgn_Codes__Filler1", "_FILLER1", FieldType.STRING, 
            1);
        pnd_Input_Orgn_Codes_Pnd_New_Orgn_Cde = pnd_Input_Orgn_Codes_Pnd_From_To_Orgn.newFieldInGroup("pnd_Input_Orgn_Codes_Pnd_New_Orgn_Cde", "#NEW-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Input_Orgn_Codes__Filler2 = pnd_Input_Orgn_Codes_Pnd_From_To_Orgn.newFieldInGroup("pnd_Input_Orgn_Codes__Filler2", "_FILLER2", FieldType.STRING, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Index.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp390() throws Exception
    {
        super("Fcpp390");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp390|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*                                                                                                                                                       //Natural: FORMAT ( 0 ) LS = 133 PS = 58 ZP = ON
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Input_Orgn_Codes);                                                                                 //Natural: INPUT #INPUT-ORGN-CODES
                if (condition(pnd_Input_Orgn_Codes.equals(" ")))                                                                                                          //Natural: IF #INPUT-ORGN-CODES = ' '
                {
                    getReports().write(0, NEWLINE,NEWLINE,"*************************************************************",NEWLINE,"*************************************************************", //Natural: WRITE ( 0 ) // '*************************************************************' / '*************************************************************' / '***           Input Parameter is missing                  ***' / '***       Control records were not suppressed             ***' / '*************************************************************' / '*************************************************************'
                        NEWLINE,"***           Input Parameter is missing                  ***",NEWLINE,"***       Control records were not suppressed             ***",
                        NEWLINE,"*************************************************************",NEWLINE,"*************************************************************");
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(99);  if (true) return;                                                                                                             //Natural: TERMINATE 0099
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                REPEAT01:                                                                                                                                                 //Natural: REPEAT
                while (condition(whileTrue))
                {
                    if (condition(pnd_Index.greater(10) || pnd_Input_Orgn_Codes_Pnd_Orgn_Cde.getValue(pnd_Index).equals(" "))) {break;}                                   //Natural: UNTIL #INDEX > 10 OR #ORGN-CDE ( #INDEX ) = ' '
                    pdaFcpacntr.getCntrl_Cntl_Orgn_Cde().setValue(pnd_Input_Orgn_Codes_Pnd_Orgn_Cde.getValue(pnd_Index));                                                 //Natural: ASSIGN CNTRL.CNTL-ORGN-CDE := #ORGN-CDE ( #INDEX )
                    DbsUtil.callnat(Fcpncntr.class , getCurrentProcessState(), pdaFcpacntr.getCntrl_Work_Fields(), pdaFcpacntr.getCntrl());                               //Natural: CALLNAT 'FCPNCNTR' USING CNTRL-WORK-FIELDS CNTRL
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaFcpacntr.getCntrl_Work_Fields_Cntrl_Return_Cde().equals(getZero())))                                                                 //Natural: IF CNTRL-RETURN-CDE = 0
                    {
                        getReports().skip(0, 1);                                                                                                                          //Natural: SKIP ( 0 ) 1
                        pnd_Message.setValue("Status of Control Record before change");                                                                                   //Natural: ASSIGN #MESSAGE := 'Status of Control Record before change'
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT
                        sub_Print_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pdaFcpacntr.getCntrl_Cntl_Orgn_Cde().setValue(pnd_Input_Orgn_Codes_Pnd_New_Orgn_Cde.getValue(pnd_Index));                                         //Natural: ASSIGN CNTRL.CNTL-ORGN-CDE := #NEW-ORGN-CDE ( #INDEX )
                        DbsUtil.callnat(Fcpncntu.class , getCurrentProcessState(), pdaFcpacntr.getCntrl_Work_Fields(), pdaFcpacntr.getCntrl());                           //Natural: CALLNAT 'FCPNCNTU' USING CNTRL-WORK-FIELDS CNTRL
                        if (condition(Global.isEscape())) return;
                        pnd_Update_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #UPDATE-CNT
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION
                        pnd_Message.setValue("Status of Control Record after  change");                                                                                   //Natural: ASSIGN #MESSAGE := 'Status of Control Record after  change'
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT
                        sub_Print_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        getReports().skip(0, 1);                                                                                                                          //Natural: SKIP ( 0 ) 1
                        pnd_Message.setValue("No Control Record to reset");                                                                                               //Natural: ASSIGN #MESSAGE := 'No Control Record to reset'
                                                                                                                                                                          //Natural: PERFORM PRINT-REPORT
                        sub_Print_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Index.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #INDEX
                }                                                                                                                                                         //Natural: END-REPEAT
                if (Global.isEscape()) return;
                //* *--------
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-REPORT
                //* *-----------------------------
                //* *------------
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'CONTROL RECORD SUPPRESION' 120T 'REPORT:  RPT1' //
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Print_Report() throws Exception                                                                                                                      //Natural: PRINT-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        getReports().display(0, "/",                                                                                                                                      //Natural: DISPLAY ( 0 ) '/' #MESSAGE '/ORGN' CNTRL.CNTL-ORGN-CDE 'CHECK/DATE' CNTRL.CNTL-CHECK-DTE '/TIME' *TIMX ( EM = HH:II:SS.T )
        		pnd_Message,"/ORGN",
        		pdaFcpacntr.getCntrl_Cntl_Orgn_Cde(),"CHECK/DATE",
        		pdaFcpacntr.getCntrl_Cntl_Check_Dte(),"/TIME",
        		Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=58 ZP=ON");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(55),"CONTROL RECORD SUPPRESION",new TabSetting(120),"REPORT:  RPT1",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(0, "/",
        		pnd_Message,"/ORGN",
        		pdaFcpacntr.getCntrl_Cntl_Orgn_Cde(),"CHECK/DATE",
        		pdaFcpacntr.getCntrl_Cntl_Check_Dte(),"/TIME",
        		Global.getTIMX(), new ReportEditMask ("HH:II:SS.T"));
    }
}
