/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:26 PM
**        * FROM NATURAL PROGRAM : Fcpp233b
************************************************************
**        * FILE NAME            : Fcpp233b.java
**        * CLASS NAME           : Fcpp233b
**        * INSTANCE NAME        : Fcpp233b
************************************************************
***********************************************************************
* PROGRAM   : FCPP233B
* SYSTEM    : CPS
* TITLE     : TAX REPORT
* GENERATED :
* FUNCTION  : GENERATE TAX REPORT
*           :
* HISTORY   :
* 08/03/99  : R. CARREON  -  CREATED
* 10/11/99  : A. YOUNG    -  REVISED INPUT INTERFACE DATE FORMAT IN
*           :                ORDER TO UTILIZE EXISTING PARM 'P1440CP1'
* 10/21/99  : ILSE BOHM   -  EXTRACT SN/CN WITH/WITHOUT TAXES.
* 09/11/00  : A. YOUNG    -  RE-COMPILED DUE TO FCPLPMNT.
*    11/02  : R. CARREON  -  STOW. EGTRRA CHANGES IN LDA.
*           :
* 04/26/2008: ROTH-MAJOR1 -  (AER) STOW ONLY
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
* 4/2017    : JJG - PIN EXPANSION CHANGES
***********************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp233b extends BLNatBase
{
    // Data Areas
    private LdaFcpl233a ldaFcpl233a;
    private LdaFcplpmnt ldaFcplpmnt;
    private LdaFcpladdr ldaFcpladdr;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_payments;
    private DbsField payments_Cntrct_Orgn_Cde;
    private DbsField payments_Annt_Soc_Sec_Nbr;
    private DbsField payments_Annt_Soc_Sec_Ind;
    private DbsField payments_Cntrct_Ppcn_Nbr;
    private DbsField payments_Pymnt_Check_Dte;
    private DbsField payments_Pymnt_Check_Nbr;
    private DbsField payments_Pymnt_Check_Amt;
    private DbsField payments_Cntrct_Invrse_Dte;
    private DbsField payments_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField payments_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField payments_Cntrct_Unq_Id_Nbr;
    private DbsField payments_Cntrct_Cmbn_Nbr;
    private DbsField payments_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup payments_Inv_Acct;
    private DbsField payments_Inv_Acct_Cde;
    private DbsField payments_Inv_Acct_Settl_Amt;
    private DbsField payments_Inv_Acct_Ivc_Amt;
    private DbsField payments_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField payments_Inv_Acct_State_Tax_Amt;
    private DbsField payments_Inv_Acct_Local_Tax_Amt;
    private DbsField payments_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_X;
    private DbsField pnd_Rej_Rdrw;
    private DbsField pnd_Ws_Orig_Invrse_Dte;
    private DbsField pnd_Ws_Save_Curr_Addr;
    private DbsField pnd_Ws_Save_Prev_Addr;
    private DbsField pnd_Ws_Save_Part_Name;

    private DbsGroup pnd_Ws_Key_Addr;
    private DbsField pnd_Ws_Key_Addr_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Key_Addr_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Key_Addr_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Key_Addr_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Key_Addr_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Ws_Key_Addr__R_Field_1;
    private DbsField pnd_Ws_Key_Addr_Pnd_Addr_S;

    private DbsGroup pnd_Ws_Key_Addr_E;
    private DbsField pnd_Ws_Key_Addr_E_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Key_Addr_E_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Key_Addr_E_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Key_Addr_E_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Key_Addr_E_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Ws_Key_Addr_E__R_Field_2;
    private DbsField pnd_Ws_Key_Addr_E_Pnd_Addr_E;

    private DbsGroup pnd_Ws_Compare_Key;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Compare_Key__R_Field_3;
    private DbsField pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr1;

    private DbsGroup pnd_Ws_Compare_Key__R_Field_4;
    private DbsField pnd_Ws_Compare_Key_Pnd_Ws_Compare;

    private DbsGroup pnd_Unq_Inv_Orgn_Prcss_Inst;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Invrse_Dte;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Orgn_Cde;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr;

    private DbsGroup pnd_Unq_Inv_Orgn_Prcss_Inst__R_Field_5;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst;
    private DbsField pnd_Intrfce_Dte;
    private DbsField pnd_Ws_Key;

    private DbsGroup pnd_Ws_Key__R_Field_6;
    private DbsField pnd_Ws_Key_Pnd_Ws_Pymnt_Intrfce_Dte;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl233a = new LdaFcpl233a();
        registerRecord(ldaFcpl233a);
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        ldaFcpladdr = new LdaFcpladdr();
        registerRecord(ldaFcpladdr);
        registerRecord(ldaFcpladdr.getVw_fcp_Cons_Addr());

        // Local Variables
        localVariables = new DbsRecord();

        vw_payments = new DataAccessProgramView(new NameInfo("vw_payments", "PAYMENTS"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));
        payments_Cntrct_Orgn_Cde = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        payments_Annt_Soc_Sec_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "ANNT_SOC_SEC_NBR");
        payments_Annt_Soc_Sec_Ind = vw_payments.getRecord().newFieldInGroup("payments_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, 
            "ANNT_SOC_SEC_IND");
        payments_Cntrct_Ppcn_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        payments_Pymnt_Check_Dte = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        payments_Pymnt_Check_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_NBR");
        payments_Pymnt_Check_Amt = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        payments_Cntrct_Invrse_Dte = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        payments_Pymnt_Payee_Tx_Elct_Trggr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PYMNT_PAYEE_TX_ELCT_TRGGR");
        payments_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        payments_Cntrct_Unq_Id_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        payments_Cntrct_Cmbn_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        payments_Pymnt_Prcss_Seq_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        payments_Inv_Acct = vw_payments.getRecord().newGroupArrayInGroup("payments_Inv_Acct", "INV-ACCT", new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT");
        payments_Inv_Acct_Cde = payments_Inv_Acct.newFieldInGroup("payments_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_CDE", "FCP_CONS_PYMT_INV_ACCT");
        payments_Inv_Acct_Settl_Amt = payments_Inv_Acct.newFieldInGroup("payments_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_SETTL_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payments_Inv_Acct_Ivc_Amt = payments_Inv_Acct.newFieldInGroup("payments_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_IVC_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payments_Inv_Acct_Fdrl_Tax_Amt = payments_Inv_Acct.newFieldInGroup("payments_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FDRL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payments_Inv_Acct_State_Tax_Amt = payments_Inv_Acct.newFieldInGroup("payments_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STATE_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payments_Inv_Acct_Local_Tax_Amt = payments_Inv_Acct.newFieldInGroup("payments_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOCAL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        payments_Inv_Acct_Dvdnd_Amt = payments_Inv_Acct.newFieldInGroup("payments_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DVDND_AMT", "FCP_CONS_PYMT_INV_ACCT");
        registerRecord(vw_payments);

        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 2);
        pnd_Rej_Rdrw = localVariables.newFieldInRecord("pnd_Rej_Rdrw", "#REJ-RDRW", FieldType.BOOLEAN, 1);
        pnd_Ws_Orig_Invrse_Dte = localVariables.newFieldInRecord("pnd_Ws_Orig_Invrse_Dte", "#WS-ORIG-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Ws_Save_Curr_Addr = localVariables.newFieldInRecord("pnd_Ws_Save_Curr_Addr", "#WS-SAVE-CURR-ADDR", FieldType.STRING, 100);
        pnd_Ws_Save_Prev_Addr = localVariables.newFieldInRecord("pnd_Ws_Save_Prev_Addr", "#WS-SAVE-PREV-ADDR", FieldType.STRING, 100);
        pnd_Ws_Save_Part_Name = localVariables.newFieldInRecord("pnd_Ws_Save_Part_Name", "#WS-SAVE-PART-NAME", FieldType.STRING, 50);

        pnd_Ws_Key_Addr = localVariables.newGroupInRecord("pnd_Ws_Key_Addr", "#WS-KEY-ADDR");
        pnd_Ws_Key_Addr_Cntrct_Ppcn_Nbr = pnd_Ws_Key_Addr.newFieldInGroup("pnd_Ws_Key_Addr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Ws_Key_Addr_Cntrct_Payee_Cde = pnd_Ws_Key_Addr.newFieldInGroup("pnd_Ws_Key_Addr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Ws_Key_Addr_Cntrct_Orgn_Cde = pnd_Ws_Key_Addr.newFieldInGroup("pnd_Ws_Key_Addr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Key_Addr_Cntrct_Invrse_Dte = pnd_Ws_Key_Addr.newFieldInGroup("pnd_Ws_Key_Addr_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Key_Addr_Pymnt_Prcss_Seq_Num = pnd_Ws_Key_Addr.newFieldInGroup("pnd_Ws_Key_Addr_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);

        pnd_Ws_Key_Addr__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Key_Addr__R_Field_1", "REDEFINE", pnd_Ws_Key_Addr);
        pnd_Ws_Key_Addr_Pnd_Addr_S = pnd_Ws_Key_Addr__R_Field_1.newFieldInGroup("pnd_Ws_Key_Addr_Pnd_Addr_S", "#ADDR-S", FieldType.STRING, 31);

        pnd_Ws_Key_Addr_E = localVariables.newGroupInRecord("pnd_Ws_Key_Addr_E", "#WS-KEY-ADDR-E");
        pnd_Ws_Key_Addr_E_Cntrct_Ppcn_Nbr = pnd_Ws_Key_Addr_E.newFieldInGroup("pnd_Ws_Key_Addr_E_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Key_Addr_E_Cntrct_Payee_Cde = pnd_Ws_Key_Addr_E.newFieldInGroup("pnd_Ws_Key_Addr_E_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Key_Addr_E_Cntrct_Orgn_Cde = pnd_Ws_Key_Addr_E.newFieldInGroup("pnd_Ws_Key_Addr_E_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Key_Addr_E_Cntrct_Invrse_Dte = pnd_Ws_Key_Addr_E.newFieldInGroup("pnd_Ws_Key_Addr_E_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Key_Addr_E_Pymnt_Prcss_Seq_Num = pnd_Ws_Key_Addr_E.newFieldInGroup("pnd_Ws_Key_Addr_E_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);

        pnd_Ws_Key_Addr_E__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Key_Addr_E__R_Field_2", "REDEFINE", pnd_Ws_Key_Addr_E);
        pnd_Ws_Key_Addr_E_Pnd_Addr_E = pnd_Ws_Key_Addr_E__R_Field_2.newFieldInGroup("pnd_Ws_Key_Addr_E_Pnd_Addr_E", "#ADDR-E", FieldType.STRING, 31);

        pnd_Ws_Compare_Key = localVariables.newGroupInRecord("pnd_Ws_Compare_Key", "#WS-COMPARE-KEY");
        pnd_Ws_Compare_Key_Cntrct_Unq_Id_Nbr = pnd_Ws_Compare_Key.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Ws_Compare_Key_Cntrct_Invrse_Dte = pnd_Ws_Compare_Key.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Compare_Key_Cntrct_Orgn_Cde = pnd_Ws_Compare_Key.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Compare_Key.newFieldInGroup("pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);

        pnd_Ws_Compare_Key__R_Field_3 = pnd_Ws_Compare_Key.newGroupInGroup("pnd_Ws_Compare_Key__R_Field_3", "REDEFINE", pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr = pnd_Ws_Compare_Key__R_Field_3.newFieldInGroup("pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr1 = pnd_Ws_Compare_Key__R_Field_3.newFieldInGroup("pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr1", "PYMNT-INSTMT-NBR1", 
            FieldType.NUMERIC, 2);

        pnd_Ws_Compare_Key__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Compare_Key__R_Field_4", "REDEFINE", pnd_Ws_Compare_Key);
        pnd_Ws_Compare_Key_Pnd_Ws_Compare = pnd_Ws_Compare_Key__R_Field_4.newFieldInGroup("pnd_Ws_Compare_Key_Pnd_Ws_Compare", "#WS-COMPARE", FieldType.STRING, 
            31);

        pnd_Unq_Inv_Orgn_Prcss_Inst = localVariables.newGroupInRecord("pnd_Unq_Inv_Orgn_Prcss_Inst", "#UNQ-INV-ORGN-PRCSS-INST");
        pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Unq_Id_Nbr = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Invrse_Dte = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Orgn_Cde = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Prcss_Seq_Num = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);

        pnd_Unq_Inv_Orgn_Prcss_Inst__R_Field_5 = localVariables.newGroupInRecord("pnd_Unq_Inv_Orgn_Prcss_Inst__R_Field_5", "REDEFINE", pnd_Unq_Inv_Orgn_Prcss_Inst);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst = pnd_Unq_Inv_Orgn_Prcss_Inst__R_Field_5.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst", 
            "#WS-UNQ-INV-ORGN-PRCSS-INST", FieldType.STRING, 31);
        pnd_Intrfce_Dte = localVariables.newFieldInRecord("pnd_Intrfce_Dte", "#INTRFCE-DTE", FieldType.STRING, 8);
        pnd_Ws_Key = localVariables.newFieldInRecord("pnd_Ws_Key", "#WS-KEY", FieldType.BINARY, 15);

        pnd_Ws_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Key__R_Field_6", "REDEFINE", pnd_Ws_Key);
        pnd_Ws_Key_Pnd_Ws_Pymnt_Intrfce_Dte = pnd_Ws_Key__R_Field_6.newFieldInGroup("pnd_Ws_Key_Pnd_Ws_Pymnt_Intrfce_Dte", "#WS-PYMNT-INTRFCE-DTE", FieldType.DATE);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_payments.reset();

        ldaFcpl233a.initializeValues();
        ldaFcplpmnt.initializeValues();
        ldaFcpladdr.initializeValues();

        localVariables.reset();
        pnd_Ws_Key_Addr_E_Pymnt_Prcss_Seq_Num.setInitialValue(9999999);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp233b() throws Exception
    {
        super("Fcpp233b");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp233b|Main");
        while(true)
        {
            try
            {
                //*  10-11-1999
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Intrfce_Dte);                                                                                      //Natural: INPUT #INTRFCE-DTE
                //*  10-11-1999
                pnd_Ws_Key_Pnd_Ws_Pymnt_Intrfce_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Intrfce_Dte);                                                       //Natural: MOVE EDITED #INTRFCE-DTE TO #WS-PYMNT-INTRFCE-DTE ( EM = YYYYMMDD )
                //*  HEADER RECORDS
                ldaFcpl233a.getFcpl233a().reset();                                                                                                                        //Natural: RESET FCPL233A
                ldaFcpl233a.getFcpl233a_Pymnt_Check_Dte().setValue(pnd_Ws_Key_Pnd_Ws_Pymnt_Intrfce_Dte);                                                                  //Natural: MOVE #WS-PYMNT-INTRFCE-DTE TO FCPL233A.PYMNT-CHECK-DTE
                ldaFcpl233a.getFcpl233a_Pnd_Srt_Company_Cde().setValue("TIAA");                                                                                           //Natural: ASSIGN FCPL233A.#SRT-COMPANY-CDE := 'TIAA'
                getWorkFiles().write(1, false, ldaFcpl233a.getFcpl233a());                                                                                                //Natural: WRITE WORK FILE 1 FCPL233A
                ldaFcpl233a.getFcpl233a_Pnd_Srt_Company_Cde().setValue("CREF");                                                                                           //Natural: ASSIGN FCPL233A.#SRT-COMPANY-CDE := 'CREF'
                getWorkFiles().write(1, false, ldaFcpl233a.getFcpl233a());                                                                                                //Natural: WRITE WORK FILE 1 FCPL233A
                //*      CANCEL/STOP
                ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                      //Natural: READ FCP-CONS-PYMNT BY CS-INTRFCE-PYMNT-DTE-CHECK-NBR STARTING FROM #WS-KEY
                (
                "READ01",
                new Wc[] { new Wc("CS_INTRFCE_PYMNT_DTE_CHECK_NBR", ">=", pnd_Ws_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("CS_INTRFCE_PYMNT_DTE_CHECK_NBR", "ASC") }
                );
                READ01:
                while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ01")))
                {
                    //*  JWO 2010-11
                    if (condition(!(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN")  //Natural: ACCEPT IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
                        || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
                        || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
                    {
                        continue;
                    }
                    if (condition(pnd_Ws_Key_Pnd_Ws_Pymnt_Intrfce_Dte.notEquals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Cs_Pymnt_Intrfce_Dte())))                               //Natural: IF #WS-PYMNT-INTRFCE-DTE NE CNR-CS-PYMNT-INTRFCE-DTE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    ldaFcpl233a.getFcpl233a().reset();                                                                                                                    //Natural: RESET FCPL233A
                    pnd_Ws_Key_Addr.setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                  //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #WS-KEY-ADDR
                                                                                                                                                                          //Natural: PERFORM GET-ADDRESS-RECORD
                    sub_Get_Address_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    ldaFcpl233a.getFcpl233a_Pnd_Participant_Name().setValue(DbsUtil.compress(ldaFcpladdr.getFcp_Cons_Addr_Ph_First_Name(), ldaFcpladdr.getFcp_Cons_Addr_Ph_Middle_Name(),  //Natural: COMPRESS PH-FIRST-NAME PH-MIDDLE-NAME PH-LAST-NAME INTO #PARTICIPANT-NAME
                        ldaFcpladdr.getFcp_Cons_Addr_Ph_Last_Name()));
                    pnd_Ws_Save_Part_Name.setValue(ldaFcpl233a.getFcpl233a_Pnd_Participant_Name());                                                                       //Natural: ASSIGN #WS-SAVE-PART-NAME := #PARTICIPANT-NAME
                    pnd_Unq_Inv_Orgn_Prcss_Inst.setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                      //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #UNQ-INV-ORGN-PRCSS-INST
                                                                                                                                                                          //Natural: PERFORM GET-DETAIL-CONTRACT
                    sub_Get_Detail_Contract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //*      REDRAW
                ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                      //Natural: READ FCP-CONS-PYMNT BY RDRW-INTRFCE-PYMNT-DTE-CHECK-NBR STARTING FROM #WS-KEY
                (
                "READ02",
                new Wc[] { new Wc("RDRW_INTRFCE_PYMNT_DTE_CHECK_NBR", ">=", pnd_Ws_Key.getBinary(), WcType.BY) },
                new Oc[] { new Oc("RDRW_INTRFCE_PYMNT_DTE_CHECK_NBR", "ASC") }
                );
                READ02:
                while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ02")))
                {
                    if (condition(!(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R ") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" R")))) //Natural: ACCEPT IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R ' OR = ' R'
                    {
                        continue;
                    }
                    if (condition(pnd_Ws_Key_Pnd_Ws_Pymnt_Intrfce_Dte.notEquals(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Rdrw_Pymnt_Intrfce_Dte())))                             //Natural: IF #WS-PYMNT-INTRFCE-DTE NE CNR-RDRW-PYMNT-INTRFCE-DTE
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    ldaFcpl233a.getFcpl233a().reset();                                                                                                                    //Natural: RESET FCPL233A
                    pnd_Ws_Key_Addr.setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                  //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #WS-KEY-ADDR
                                                                                                                                                                          //Natural: PERFORM GET-ADDRESS-RECORD
                    sub_Get_Address_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    ldaFcpl233a.getFcpl233a_Pnd_Participant_Name().setValue(DbsUtil.compress(ldaFcpladdr.getFcp_Cons_Addr_Ph_First_Name(), ldaFcpladdr.getFcp_Cons_Addr_Ph_Middle_Name(),  //Natural: COMPRESS PH-FIRST-NAME PH-MIDDLE-NAME PH-LAST-NAME INTO #PARTICIPANT-NAME
                        ldaFcpladdr.getFcp_Cons_Addr_Ph_Last_Name()));
                    pnd_Ws_Save_Part_Name.setValue(ldaFcpl233a.getFcpl233a_Pnd_Participant_Name());                                                                       //Natural: ASSIGN #WS-SAVE-PART-NAME := #PARTICIPANT-NAME
                    ldaFcpl233a.getFcpl233a_Pnd_Current_Address().setValue(DbsUtil.compress(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line1_Txt().getValue(1),              //Natural: COMPRESS PYMNT-ADDR-LINE1-TXT ( 1 ) PYMNT-ADDR-LINE2-TXT ( 1 ) PYMNT-ADDR-LINE3-TXT ( 1 ) PYMNT-ADDR-LINE4-TXT ( 1 ) PYMNT-ADDR-LINE5-TXT ( 1 ) PYMNT-ADDR-LINE6-TXT ( 1 ) INTO #CURRENT-ADDRESS
                        ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line2_Txt().getValue(1), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line3_Txt().getValue(1), 
                        ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line4_Txt().getValue(1), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line5_Txt().getValue(1), 
                        ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line6_Txt().getValue(1)));
                    //*  PYMNT-ADDR-ZIP-CDE(1) INTO #CURRENT-ADDRESS /* ZIP ALREADY IN ADDRESS
                    if (condition(pnd_Rej_Rdrw.getBoolean()))                                                                                                             //Natural: IF #REJ-RDRW
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Key_Addr_Cntrct_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Orgnl_Invrse_Dte());                                                     //Natural: ASSIGN #WS-KEY-ADDR.CNTRCT-INVRSE-DTE := FCP-CONS-PYMNT.CNR-ORGNL-INVRSE-DTE
                    pnd_Ws_Orig_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cnr_Orgnl_Invrse_Dte());                                                                //Natural: ASSIGN #WS-ORIG-INVRSE-DTE := FCP-CONS-PYMNT.CNR-ORGNL-INVRSE-DTE
                                                                                                                                                                          //Natural: PERFORM GET-ADDRESS-RECORD
                    sub_Get_Address_Record();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    ldaFcpl233a.getFcpl233a_Pnd_Previous_Address().setValue(DbsUtil.compress(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line1_Txt().getValue(1),             //Natural: COMPRESS PYMNT-ADDR-LINE1-TXT ( 1 ) PYMNT-ADDR-LINE2-TXT ( 1 ) PYMNT-ADDR-LINE3-TXT ( 1 ) PYMNT-ADDR-LINE4-TXT ( 1 ) PYMNT-ADDR-LINE5-TXT ( 1 ) PYMNT-ADDR-LINE6-TXT ( 1 ) INTO #PREVIOUS-ADDRESS
                        ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line2_Txt().getValue(1), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line3_Txt().getValue(1), 
                        ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line4_Txt().getValue(1), ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line5_Txt().getValue(1), 
                        ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Line6_Txt().getValue(1)));
                    pnd_Ws_Save_Curr_Addr.setValue(ldaFcpl233a.getFcpl233a_Pnd_Current_Address());                                                                        //Natural: ASSIGN #WS-SAVE-CURR-ADDR := #CURRENT-ADDRESS
                    pnd_Ws_Save_Prev_Addr.setValue(ldaFcpl233a.getFcpl233a_Pnd_Previous_Address());                                                                       //Natural: ASSIGN #WS-SAVE-PREV-ADDR := #PREVIOUS-ADDRESS
                    pnd_Unq_Inv_Orgn_Prcss_Inst.setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                      //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #UNQ-INV-ORGN-PRCSS-INST
                                                                                                                                                                          //Natural: PERFORM GET-DETAIL-CONTRACT
                    sub_Get_Detail_Contract();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                //* -------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDRESS-RECORD
                //* -------------------------------------------------------------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DETAIL-CONTRACT
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Address_Record() throws Exception                                                                                                                //Natural: GET-ADDRESS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rej_Rdrw.setValue(true);                                                                                                                                      //Natural: ASSIGN #REJ-RDRW := TRUE
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("IA") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("AP")))                      //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'IA' OR = 'AP'
        {
            pnd_Ws_Key_Addr_Pymnt_Prcss_Seq_Num.reset();                                                                                                                  //Natural: RESET #WS-KEY-ADDR.PYMNT-PRCSS-SEQ-NUM
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpladdr.getVw_fcp_Cons_Addr().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) FCP-CONS-ADDR WITH PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #ADDR-S
        (
        "READ03",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Ws_Key_Addr_Pnd_Addr_S, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        READ03:
        while (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().readNextRow("READ03")))
        {
            if (condition(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Chg_Ind().getValue(1).equals("Y")))                                                                     //Natural: IF PYMNT-ADDR-CHG-IND ( 1 ) EQ 'Y'
            {
                pnd_Rej_Rdrw.setValue(false);                                                                                                                             //Natural: ASSIGN #REJ-RDRW := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }
    private void sub_Get_Detail_Contract() throws Exception                                                                                                               //Natural: GET-DETAIL-CONTRACT
    {
        if (BLNatReinput.isReinput()) return;

        //* -------------------------------------------------------------------
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr.reset();                                                                                                             //Natural: RESET #UNQ-INV-ORGN-PRCSS-INST.PYMNT-INSTMT-NBR
        vw_payments.startDatabaseRead                                                                                                                                     //Natural: READ PAYMENTS BY UNQ-INV-ORGN-PRCSS-INST FROM #WS-UNQ-INV-ORGN-PRCSS-INST
        (
        "RE2",
        new Wc[] { new Wc("UNQ_INV_ORGN_PRCSS_INST", ">=", pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst, WcType.BY) },
        new Oc[] { new Oc("UNQ_INV_ORGN_PRCSS_INST", "ASC") }
        );
        RE2:
        while (condition(vw_payments.readNextRow("RE2")))
        {
            //* *IF PAYMENTS.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN' OR = 'SN'
            //* *  ACCEPT IF PAYMENTS.INV-ACCT-STATE-TAX-AMT(*) > 0
            //* *       OR   PAYMENTS.INV-ACCT-FDRL-TAX-AMT(*)  > 0
            //* *       OR   PAYMENTS.INV-ACCT-LOCAL-TAX-AMT(*) > 0
            //* *END-IF   /* ** IB 10/21/99
            pnd_Ws_Compare_Key.setValuesByName(vw_payments);                                                                                                              //Natural: MOVE BY NAME PAYMENTS TO #WS-COMPARE-KEY
            pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr1.reset();                                                                                                                 //Natural: RESET PYMNT-INSTMT-NBR1 #UNQ-INV-ORGN-PRCSS-INST.PYMNT-INSTMT-NBR
            pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr.reset();
            if (condition(pnd_Ws_Compare_Key_Pnd_Ws_Compare.greater(pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst)))                                         //Natural: IF #WS-COMPARE GT #WS-UNQ-INV-ORGN-PRCSS-INST
            {
                if (true) break RE2;                                                                                                                                      //Natural: ESCAPE BOTTOM ( RE2. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Compare_Key_Pnd_Ws_Compare.notEquals(pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst)))                                       //Natural: IF #WS-COMPARE NE #WS-UNQ-INV-ORGN-PRCSS-INST
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(payments_Inv_Acct_Cde.getValue("*").equals("T") || payments_Inv_Acct_Cde.getValue("*").equals("TG") || payments_Inv_Acct_Cde.getValue("*").equals(" T")  //Natural: IF PAYMENTS.INV-ACCT-CDE ( * ) = 'T' OR = 'TG' OR = ' T' OR = 'R' OR = ' R'
                || payments_Inv_Acct_Cde.getValue("*").equals("R") || payments_Inv_Acct_Cde.getValue("*").equals(" R")))
            {
                ldaFcpl233a.getFcpl233a_Pnd_Srt_Company_Cde().setValue("TIAA");                                                                                           //Natural: ASSIGN FCPL233A.#SRT-COMPANY-CDE := 'TIAA'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl233a.getFcpl233a_Pnd_Srt_Company_Cde().setValue("CREF");                                                                                           //Natural: ASSIGN FCPL233A.#SRT-COMPANY-CDE := 'CREF'
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl233a.getFcpl233a_Pnd_Srt_Csr_Code().setValue(" ");                                                                                                     //Natural: ASSIGN FCPL233A.#SRT-CSR-CODE := ' '
            ldaFcpl233a.getFcpl233a_Cntrct_Orgn_Cde().setValue(payments_Cntrct_Orgn_Cde);                                                                                 //Natural: ASSIGN FCPL233A.CNTRCT-ORGN-CDE := PAYMENTS.CNTRCT-ORGN-CDE
            ldaFcpl233a.getFcpl233a_Annt_Soc_Sec_Nbr().setValue(payments_Annt_Soc_Sec_Nbr);                                                                               //Natural: ASSIGN FCPL233A.ANNT-SOC-SEC-NBR := PAYMENTS.ANNT-SOC-SEC-NBR
            ldaFcpl233a.getFcpl233a_Annt_Soc_Sec_Ind().setValue(payments_Annt_Soc_Sec_Ind);                                                                               //Natural: ASSIGN FCPL233A.ANNT-SOC-SEC-IND := PAYMENTS.ANNT-SOC-SEC-IND
            ldaFcpl233a.getFcpl233a_Cntrct_Ppcn_Nbr().setValue(payments_Cntrct_Ppcn_Nbr);                                                                                 //Natural: ASSIGN FCPL233A.CNTRCT-PPCN-NBR := PAYMENTS.CNTRCT-PPCN-NBR
            ldaFcpl233a.getFcpl233a_Pymnt_Check_Dte().setValue(payments_Pymnt_Check_Dte);                                                                                 //Natural: ASSIGN FCPL233A.PYMNT-CHECK-DTE := PAYMENTS.PYMNT-CHECK-DTE
            ldaFcpl233a.getFcpl233a_Pymnt_Check_Nbr().setValue(payments_Pymnt_Check_Nbr);                                                                                 //Natural: ASSIGN FCPL233A.PYMNT-CHECK-NBR := PAYMENTS.PYMNT-CHECK-NBR
            ldaFcpl233a.getFcpl233a_Pymnt_Check_Amt().setValue(payments_Pymnt_Check_Amt);                                                                                 //Natural: ASSIGN FCPL233A.PYMNT-CHECK-AMT := PAYMENTS.PYMNT-CHECK-AMT
            ldaFcpl233a.getFcpl233a_Pymnt_Payee_Tx_Elct_Trggr().setValue(payments_Pymnt_Payee_Tx_Elct_Trggr);                                                             //Natural: ASSIGN FCPL233A.PYMNT-PAYEE-TX-ELCT-TRGGR := PAYMENTS.PYMNT-PAYEE-TX-ELCT-TRGGR
            ldaFcpl233a.getFcpl233a_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(payments_Cntrct_Cancel_Rdrw_Actvty_Cde);                                                     //Natural: ASSIGN FCPL233A.CNTRCT-CANCEL-RDRW-ACTVTY-CDE := PAYMENTS.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            //*  JWO 2010-11
            if (condition(payments_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN") || payments_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN") || payments_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP")  //Natural: IF PAYMENTS.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
                || payments_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP") || payments_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP") || payments_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR")))
            {
                ldaFcpl233a.getFcpl233a_Cnr_Orgnl_Invrse_Dte().setValue(payments_Cntrct_Invrse_Dte);                                                                      //Natural: ASSIGN FCPL233A.CNR-ORGNL-INVRSE-DTE := PAYMENTS.CNTRCT-INVRSE-DTE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl233a.getFcpl233a_Cnr_Orgnl_Invrse_Dte().setValue(pnd_Ws_Orig_Invrse_Dte);                                                                          //Natural: ASSIGN FCPL233A.CNR-ORGNL-INVRSE-DTE := #WS-ORIG-INVRSE-DTE
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR #X 1 C*INV-ACCT
            for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct())); pnd_X.nadd(1))
            {
                ldaFcpl233a.getFcpl233a_Inv_Acct_Settl_Amt().nadd(payments_Inv_Acct_Settl_Amt.getValue(pnd_X));                                                           //Natural: ASSIGN FCPL233A.INV-ACCT-SETTL-AMT := FCPL233A.INV-ACCT-SETTL-AMT + PAYMENTS.INV-ACCT-SETTL-AMT ( #X )
                ldaFcpl233a.getFcpl233a_Inv_Acct_Ivc_Amt().nadd(payments_Inv_Acct_Ivc_Amt.getValue(pnd_X));                                                               //Natural: ASSIGN FCPL233A.INV-ACCT-IVC-AMT := FCPL233A.INV-ACCT-IVC-AMT + PAYMENTS.INV-ACCT-IVC-AMT ( #X )
                ldaFcpl233a.getFcpl233a_Inv_Acct_Fdrl_Tax_Amt().nadd(payments_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_X));                                                     //Natural: ASSIGN FCPL233A.INV-ACCT-FDRL-TAX-AMT := FCPL233A.INV-ACCT-FDRL-TAX-AMT + PAYMENTS.INV-ACCT-FDRL-TAX-AMT ( #X )
                ldaFcpl233a.getFcpl233a_Inv_Acct_State_Tax_Amt().nadd(payments_Inv_Acct_State_Tax_Amt.getValue(pnd_X));                                                   //Natural: ASSIGN FCPL233A.INV-ACCT-STATE-TAX-AMT := FCPL233A.INV-ACCT-STATE-TAX-AMT + PAYMENTS.INV-ACCT-STATE-TAX-AMT ( #X )
                ldaFcpl233a.getFcpl233a_Inv_Acct_Local_Tax_Amt().nadd(payments_Inv_Acct_Local_Tax_Amt.getValue(pnd_X));                                                   //Natural: ASSIGN FCPL233A.INV-ACCT-LOCAL-TAX-AMT := FCPL233A.INV-ACCT-LOCAL-TAX-AMT + PAYMENTS.INV-ACCT-LOCAL-TAX-AMT ( #X )
                ldaFcpl233a.getFcpl233a_Inv_Acct_Dvdnd_Amt().nadd(payments_Inv_Acct_Dvdnd_Amt.getValue(pnd_X));                                                           //Natural: ASSIGN FCPL233A.INV-ACCT-DVDND-AMT := FCPL233A.INV-ACCT-DVDND-AMT + PAYMENTS.INV-ACCT-DVDND-AMT ( #X )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RE2"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RE2"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(1, false, ldaFcpl233a.getFcpl233a());                                                                                                    //Natural: WRITE WORK FILE 1 FCPL233A
            ldaFcpl233a.getFcpl233a().reset();                                                                                                                            //Natural: RESET FCPL233A
            ldaFcpl233a.getFcpl233a_Pnd_Participant_Name().setValue(pnd_Ws_Save_Part_Name);                                                                               //Natural: ASSIGN #PARTICIPANT-NAME := #WS-SAVE-PART-NAME
            ldaFcpl233a.getFcpl233a_Pnd_Current_Address().setValue(pnd_Ws_Save_Curr_Addr);                                                                                //Natural: ASSIGN #CURRENT-ADDRESS := #WS-SAVE-CURR-ADDR
            ldaFcpl233a.getFcpl233a_Pnd_Previous_Address().setValue(pnd_Ws_Save_Prev_Addr);                                                                               //Natural: ASSIGN #PREVIOUS-ADDRESS := #WS-SAVE-PREV-ADDR
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //
}
