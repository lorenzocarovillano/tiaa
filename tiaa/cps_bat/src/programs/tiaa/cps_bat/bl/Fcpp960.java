/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:24 PM
**        * FROM NATURAL PROGRAM : Fcpp960
************************************************************
**        * FILE NAME            : Fcpp960.java
**        * CLASS NAME           : Fcpp960
**        * INSTANCE NAME        : Fcpp960
************************************************************
***********************************************************************
* PROGRAM:  FCPP960
*
* TITLE:    CPS NZ XML STATEMENT GENERATION
* AUTHOR:   FRANCIS ENDAYA
* DESC:     THE PROGRAM READS CHECK FILE AND CREATES XML FILE TO BE
*      SENT OVER TO CCP FOR CHECK CREATION FOR NZ CHECKS AND EFT.
******************************** FCPP874
* MODIFICATION LOG
* 2016/11/01 F.ENDAYA  NEW
* 2017/08/12 F.ENDAYA  PIN EXPANSION. RESTOW TO PICK UP NEW PDAS
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp960 extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private PdaFcpa873 pdaFcpa873;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa874a pdaFcpa874a;
    private PdaFcpa874h pdaFcpa874h;
    private PdaFcpa803l pdaFcpa803l;
    private PdaFcpanzcn pdaFcpanzcn;
    private PdaFcpanzc1 pdaFcpanzc1;
    private PdaFcpanzc2 pdaFcpanzc2;
    private PdaFcpacrpt pdaFcpacrpt;
    private LdaFcplbar1 ldaFcplbar1;
    private LdaFcplnzc2 ldaFcplnzc2;
    private PdaFcpa110 pdaFcpa110;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;

    private DbsGroup pnd_Key_Pnd_Key_Detail;
    private DbsField pnd_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Pymnt_Check_Dte;
    private DbsField pnd_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Old_Key;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Run_Rqust_Parameters;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_File_Type;
    private DbsField pnd_Ws_Pnd_Run_Type;
    private DbsField pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Pnd_Global_Pay_Run;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Next_Rollover_Nbr;
    private DbsField pnd_Error_Msg;
    private DbsField pnd_Batch_Cnt;
    private DbsField pnd_First_Record;
    private DbsField pnd_Last_Contract;
    private DbsField pnd_Last_Record;
    private DbsField pnd_Totals_Area;

    private DbsGroup pnd_Totals_Area__R_Field_4;
    private DbsField pnd_Totals_Area_Pnd_Total_Gross_Amt;
    private DbsField pnd_Totals_Area_Pnd_Cntrct_Ivc_Amt;
    private DbsField pnd_Totals_Area_Pnd_Total_Pymnt_Ded_Amt;
    private DbsField pnd_Totals_Area_Pnd_Total_Ded;
    private DbsField pnd_Totals_Area_Pnd_Ws_Interest_Amt;
    private DbsField pnd_Totals_Area_Pnd_Total_Payment_Amt;
    private DbsField pnd_Totals_Area_Pnd_Total_Net_Amt2;
    private DbsField pnd_Sve_Documentrequestid_Data;
    private DbsField pnd_Save_Eft_Acct;
    private DbsField pnd_Check_Eft;
    private DbsField pnd_Check_Rollover;
    private DbsField pnd_Sve_Institutioninfoline_Data;
    private DbsField pnd_Sve_Ext;

    private DbsGroup pnd_Sve_Nz_Check_Fields;
    private DbsField pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ivc_Amt;
    private DbsField pnd_Sve_Nz_Check_Fields_Pnd_Sve_Ded_Pymnt_Table;
    private DbsField pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Records;
    private DbsField pnd_Sve_Nz_Check_Fields_Pnd_Sve_Dpi_Dci_Ind;
    private DbsField pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind;
    private DbsField pnd_Save_First_Twenty;

    private DbsGroup pnd_Save_First_Twenty__R_Field_5;

    private DbsGroup pnd_Save_First_Twenty_Documentrequestid_Data;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Sys_Id;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Yyyy;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Mm;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Dd;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Hh;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Ii;
    private DbsField pnd_Save_First_Twenty_Documentrequestid_Ss;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Pymnt_S__R_Field_6;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde;

    private DataAccessProgramView vw_fcp_Cons_Pymnu;
    private DbsField fcp_Cons_Pymnu_Pymnt_Nbr;
    private DbsField fcp_Cons_Pymnu_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnu_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnu_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnu_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde;
    private DbsField fcp_Cons_Pymnu_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind;
    private DbsField fcp_Cons_Pymnu_Cntrct_Crrncy_Cde;
    private DbsField fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup fcp_Cons_Pymnu__R_Field_7;
    private DbsField fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Pymnu_Pymnt_Instmt_Nbr;
    private DbsField fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr;
    private DbsField fcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_KeyOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        pdaFcpa873 = new PdaFcpa873(localVariables);
        pdaFcpa803 = new PdaFcpa803(localVariables);
        pdaFcpa874a = new PdaFcpa874a(localVariables);
        pdaFcpa874h = new PdaFcpa874h(localVariables);
        pdaFcpa803l = new PdaFcpa803l(localVariables);
        pdaFcpanzcn = new PdaFcpanzcn(localVariables);
        pdaFcpanzc1 = new PdaFcpanzc1(localVariables);
        pdaFcpanzc2 = new PdaFcpanzc2(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        ldaFcplbar1 = new LdaFcplbar1();
        registerRecord(ldaFcplbar1);
        ldaFcplnzc2 = new LdaFcplnzc2();
        registerRecord(ldaFcplnzc2);
        pdaFcpa110 = new PdaFcpa110(localVariables);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);

        // Local Variables
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 27);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);

        pnd_Key_Pnd_Key_Detail = pnd_Key__R_Field_1.newGroupInGroup("pnd_Key_Pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Cntrct_Orgn_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Pymnt_Check_Dte = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Key_Cntrct_Ppcn_Nbr = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Key_Cntrct_Payee_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Key_Pymnt_Prcss_Seq_Num = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Old_Key = localVariables.newFieldInRecord("pnd_Old_Key", "#OLD-KEY", FieldType.STRING, 27);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Run_Rqust_Parameters = pnd_Ws.newFieldInGroup("pnd_Ws_Run_Rqust_Parameters", "RUN-RQUST-PARAMETERS", FieldType.STRING, 18);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Run_Rqust_Parameters);
        pnd_Ws_Pnd_File_Type = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_File_Type", "#FILE-TYPE", FieldType.STRING, 10);
        pnd_Ws_Pnd_Run_Type = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 8);
        pnd_Ws_Pymnt_Check_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_Global_Pay_Run = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Global_Pay_Run", "#GLOBAL-PAY-RUN", FieldType.BOOLEAN, 1);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_3", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Next_Rollover_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Rollover_Nbr", "#WS-NEXT-ROLLOVER-NBR", FieldType.NUMERIC, 7);
        pnd_Error_Msg = localVariables.newFieldInRecord("pnd_Error_Msg", "#ERROR-MSG", FieldType.STRING, 72);
        pnd_Batch_Cnt = localVariables.newFieldInRecord("pnd_Batch_Cnt", "#BATCH-CNT", FieldType.NUMERIC, 7);
        pnd_First_Record = localVariables.newFieldInRecord("pnd_First_Record", "#FIRST-RECORD", FieldType.BOOLEAN, 1);
        pnd_Last_Contract = localVariables.newFieldInRecord("pnd_Last_Contract", "#LAST-CONTRACT", FieldType.BOOLEAN, 1);
        pnd_Last_Record = localVariables.newFieldInRecord("pnd_Last_Record", "#LAST-RECORD", FieldType.BOOLEAN, 1);
        pnd_Totals_Area = localVariables.newFieldInRecord("pnd_Totals_Area", "#TOTALS-AREA", FieldType.STRING, 95);

        pnd_Totals_Area__R_Field_4 = localVariables.newGroupInRecord("pnd_Totals_Area__R_Field_4", "REDEFINE", pnd_Totals_Area);
        pnd_Totals_Area_Pnd_Total_Gross_Amt = pnd_Totals_Area__R_Field_4.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Gross_Amt", "#TOTAL-GROSS-AMT", FieldType.NUMERIC, 
            17, 2);
        pnd_Totals_Area_Pnd_Cntrct_Ivc_Amt = pnd_Totals_Area__R_Field_4.newFieldInGroup("pnd_Totals_Area_Pnd_Cntrct_Ivc_Amt", "#CNTRCT-IVC-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Totals_Area_Pnd_Total_Pymnt_Ded_Amt = pnd_Totals_Area__R_Field_4.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Pymnt_Ded_Amt", "#TOTAL-PYMNT-DED-AMT", 
            FieldType.NUMERIC, 11, 2);
        pnd_Totals_Area_Pnd_Total_Ded = pnd_Totals_Area__R_Field_4.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Ded", "#TOTAL-DED", FieldType.NUMERIC, 11, 
            2);
        pnd_Totals_Area_Pnd_Ws_Interest_Amt = pnd_Totals_Area__R_Field_4.newFieldInGroup("pnd_Totals_Area_Pnd_Ws_Interest_Amt", "#WS-INTEREST-AMT", FieldType.NUMERIC, 
            11, 2);
        pnd_Totals_Area_Pnd_Total_Payment_Amt = pnd_Totals_Area__R_Field_4.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Payment_Amt", "#TOTAL-PAYMENT-AMT", 
            FieldType.NUMERIC, 17, 2);
        pnd_Totals_Area_Pnd_Total_Net_Amt2 = pnd_Totals_Area__R_Field_4.newFieldInGroup("pnd_Totals_Area_Pnd_Total_Net_Amt2", "#TOTAL-NET-AMT2", FieldType.NUMERIC, 
            17, 2);
        pnd_Sve_Documentrequestid_Data = localVariables.newFieldInRecord("pnd_Sve_Documentrequestid_Data", "#SVE-DOCUMENTREQUESTID-DATA", FieldType.STRING, 
            40);
        pnd_Save_Eft_Acct = localVariables.newFieldInRecord("pnd_Save_Eft_Acct", "#SAVE-EFT-ACCT", FieldType.STRING, 21);
        pnd_Check_Eft = localVariables.newFieldInRecord("pnd_Check_Eft", "#CHECK-EFT", FieldType.BOOLEAN, 1);
        pnd_Check_Rollover = localVariables.newFieldInRecord("pnd_Check_Rollover", "#CHECK-ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Sve_Institutioninfoline_Data = localVariables.newFieldArrayInRecord("pnd_Sve_Institutioninfoline_Data", "#SVE-INSTITUTIONINFOLINE-DATA", FieldType.STRING, 
            70, new DbsArrayController(1, 8));
        pnd_Sve_Ext = localVariables.newFieldArrayInRecord("pnd_Sve_Ext", "#SVE-EXT", FieldType.STRING, 244, new DbsArrayController(1, 25));

        pnd_Sve_Nz_Check_Fields = localVariables.newGroupInRecord("pnd_Sve_Nz_Check_Fields", "#SVE-NZ-CHECK-FIELDS");
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ivc_Amt = pnd_Sve_Nz_Check_Fields.newFieldInGroup("pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ivc_Amt", "#SVE-PYMNT-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Ded_Pymnt_Table = pnd_Sve_Nz_Check_Fields.newFieldArrayInGroup("pnd_Sve_Nz_Check_Fields_Pnd_Sve_Ded_Pymnt_Table", 
            "#SVE-DED-PYMNT-TABLE", FieldType.STRING, 3, new DbsArrayController(1, 5));
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Records = pnd_Sve_Nz_Check_Fields.newFieldInGroup("pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Records", "#SVE-PYMNT-RECORDS", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Dpi_Dci_Ind = pnd_Sve_Nz_Check_Fields.newFieldInGroup("pnd_Sve_Nz_Check_Fields_Pnd_Sve_Dpi_Dci_Ind", "#SVE-DPI-DCI-IND", 
            FieldType.BOOLEAN, 1);
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind = pnd_Sve_Nz_Check_Fields.newFieldInGroup("pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind", "#SVE-PYMNT-DED-IND", 
            FieldType.BOOLEAN, 1);
        pnd_Save_First_Twenty = localVariables.newFieldInRecord("pnd_Save_First_Twenty", "#SAVE-FIRST-TWENTY", FieldType.STRING, 20);

        pnd_Save_First_Twenty__R_Field_5 = localVariables.newGroupInRecord("pnd_Save_First_Twenty__R_Field_5", "REDEFINE", pnd_Save_First_Twenty);

        pnd_Save_First_Twenty_Documentrequestid_Data = pnd_Save_First_Twenty__R_Field_5.newGroupInGroup("pnd_Save_First_Twenty_Documentrequestid_Data", 
            "DOCUMENTREQUESTID-DATA");
        pnd_Save_First_Twenty_Documentrequestid_Sys_Id = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Sys_Id", 
            "DOCUMENTREQUESTID-SYS-ID", FieldType.STRING, 3);
        pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Sub_Sys_Id", 
            "DOCUMENTREQUESTID-SUB-SYS-ID", FieldType.STRING, 3);
        pnd_Save_First_Twenty_Documentrequestid_Yyyy = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Yyyy", 
            "DOCUMENTREQUESTID-YYYY", FieldType.STRING, 4);
        pnd_Save_First_Twenty_Documentrequestid_Mm = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Mm", 
            "DOCUMENTREQUESTID-MM", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Dd = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Dd", 
            "DOCUMENTREQUESTID-DD", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Hh = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Hh", 
            "DOCUMENTREQUESTID-HH", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Ii = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Ii", 
            "DOCUMENTREQUESTID-II", FieldType.STRING, 2);
        pnd_Save_First_Twenty_Documentrequestid_Ss = pnd_Save_First_Twenty_Documentrequestid_Data.newFieldInGroup("pnd_Save_First_Twenty_Documentrequestid_Ss", 
            "DOCUMENTREQUESTID-SS", FieldType.STRING, 2);

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Num = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);

        pnd_Pymnt_S__R_Field_6 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_6", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde = pnd_Pymnt_S__R_Field_6.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde", "#PYMNT-SUPERDE", FieldType.STRING, 27);

        vw_fcp_Cons_Pymnu = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnu", "FCP-CONS-PYMNU"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnu_Pymnt_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        fcp_Cons_Pymnu_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnu_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnu_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnu_Cntrct_Invrse_Dte = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        fcp_Cons_Pymnu_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        fcp_Cons_Pymnu_Cntrct_Crrncy_Cde = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        fcp_Cons_Pymnu__R_Field_7 = vw_fcp_Cons_Pymnu.getRecord().newGroupInGroup("fcp_Cons_Pymnu__R_Field_7", "REDEFINE", fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num = fcp_Cons_Pymnu__R_Field_7.newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        fcp_Cons_Pymnu_Pymnt_Instmt_Nbr = fcp_Cons_Pymnu__R_Field_7.newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 
            2);
        fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", 
            FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SCRTY_NBR");
        fcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr = vw_fcp_Cons_Pymnu.getRecord().newFieldInGroup("fcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SEQ_NBR");
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Key_OLD", "Pnd_Key_OLD", FieldType.STRING, 27);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnu.reset();
        internalLoopRecord.reset();

        ldaFcplbar1.initializeValues();
        ldaFcplnzc2.initializeValues();
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();

        localVariables.reset();
        pnd_Last_Contract.setInitialValue(false);
        pnd_Last_Record.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp960() throws Exception
    {
        super("Fcpp960");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 133 ZP = ON;//Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T 'PROCESS NEW ANNUITIZATION' #FILE-TYPE 68T 'REPORT: RPT0' / 36T #RUN-TYPE //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pnd_First_Record.setValue(true);                                                                                                                                  //Natural: ASSIGN #FIRST-RECORD := TRUE
        //*  RL
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMAT-DATA
        sub_Get_Check_Format_Data();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARM
        sub_Process_Input_Parm();
        if (condition(Global.isEscape())) {return;}
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPANZC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 6 )
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 EXT ( * )
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            CheckAtStartofData872();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                             //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
            //*  PARALLEL MODE FE201703  START
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-NUMBER-DB196
            sub_Get_Check_Number_Db196();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  EXT.PYMNT-CHECK-SEQ-NBR          := #FCPL876.PYMNT-CHECK-SEQ-NBR  /*
            //*  IF #FCPA803.#CHECK OR #FCPA803.#ZERO-CHECK
            //*    EXT.PYMNT-CHECK-NBR             := #FCPL876.PYMNT-CHECK-NBR     /*
            //*  ELSE
            //*    EXT.PYMNT-CHECK-SCRTY-NBR     := #FCPL876.PYMNT-CHECK-SCRTY-NBR /*
            //*  END-IF                           /* FE201703 COMMENT OUT END
            //*  WRITE 'WILL CALL FCPNNZC1 1ST CNTR=' EXT.CNTRCT-PPCN-NBR /* TEST
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
            sub_Accum_Control_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                              //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := FALSE
            //* ********************** RL BEGIN PAYEE MATCH **************************
            pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().nadd(1);                                                                                                      //Natural: ADD 1 TO #RECORD-IN-PYMNT
            //*  12/15/99
            if (condition(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().notEquals(8)))                                                                                       //Natural: IF EXT.PYMNT-PAY-TYPE-REQ-IND NE 8
            {
                DbsUtil.callnat(Fcpnroth.class , getCurrentProcessState(), pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), pdaFcpaext.getExt_Cntrct_Invrse_Dte(),                    //Natural: CALLNAT 'FCPNROTH' EXT.CNTRCT-PPCN-NBR EXT.CNTRCT-INVRSE-DTE EXT.CNTRCT-ORGN-CDE EXT.PYMNT-PRCSS-SEQ-NBR #FCPA803
                    pdaFcpaext.getExt_Cntrct_Orgn_Cde(), pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr(), pdaFcpa803.getPnd_Fcpa803());
                if (condition(Global.isEscape())) return;
                //*    IF #ROTH-IND
                //*      WRITE '************* ROTH ROTH ROTH ****************'
                //*    END-IF
                //*    CALLNAT 'FCPN874B'  /* FE201611 START COMMENT OUT
                //*       USING #CHECK-SORT-FIELDS                             /*
                //*      #NZ-CHECK-FIELDS        /* TMM 8/8/00
                //*             NZ-EXT(*)                                      /*
                //*      EXT(*)                                         /*
                //*      #FCPA803
                //*      #FCPA874A
                //*      #FCPA874H
                //*      #FCPA803L
                //*      #BARCODE-LDA
                //*      FCPA110 /* RL CHECK&SEQ NBR & OTHER BANK DATA FOR CHECK PRINTING
                //*                       /* FE201611 END   COMMENT OUT
                //*    WRITE *PROGRAM '1650 RETURN FROM FCPN960' /
                //*      '=' FCPA110-NEW-CHECK-PREFIX-A3 /
                //*      '=' FCPA110-RETURN-MSG (AL=25) /
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Next_Rollover_Nbr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-NEXT-ROLLOVER-NBR
            }                                                                                                                                                             //Natural: END-IF
            //*  FE201611
                                                                                                                                                                          //Natural: PERFORM GENERATE-XML-LINES
            sub_Generate_Xml_Lines();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
            sub_Write_Output_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  AT END OF DATA
            //*    IF #GLOBAL-PAY-RUN
            //*      PERFORM PRINT-VOID-PAGE
            //*    END-IF
            //*  END-ENDDATA
            //*                                                                                                                                                           //Natural: AT END OF DATA
            readWork01Pnd_KeyOld.setValue(pnd_Key);                                                                                                                       //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            pnd_Last_Record.setValue(true);                                                                                                                               //Natural: ASSIGN #LAST-RECORD := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(false);                                                                                                    //Natural: ASSIGN #NEW-PYMNT := FALSE
            pdaFcpaext.getExt().getValue("*").setValue(pnd_Sve_Ext.getValue("*"));                                                                                        //Natural: ASSIGN EXT ( * ) := #SVE-EXT ( * )
            //*    #SVE-#NZ-CHECK-FIELDS        := #NZ-CHECK-FIELDS
            pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ivc_Amt.setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pymnt_Ivc_Amt());                                                    //Natural: ASSIGN #SVE-PYMNT-IVC-AMT := #NZ-CHECK-FIELDS.PYMNT-IVC-AMT
            pnd_Sve_Nz_Check_Fields_Pnd_Sve_Ded_Pymnt_Table.getValue("*").setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue("*"));                //Natural: ASSIGN #SVE-DED-PYMNT-TABLE ( * ) := #DED-PYMNT-TABLE ( * )
            pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Records.setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Records());                                                //Natural: ASSIGN #SVE-PYMNT-RECORDS := #PYMNT-RECORDS
            pnd_Sve_Nz_Check_Fields_Pnd_Sve_Dpi_Dci_Ind.setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean());                                       //Natural: ASSIGN #SVE-DPI-DCI-IND := #DPI-DCI-IND
            pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind.setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean());                                   //Natural: ASSIGN #SVE-PYMNT-DED-IND := #PYMNT-DED-IND
                                                                                                                                                                          //Natural: PERFORM GENERATE-XML-LINES
            sub_Generate_Xml_Lines();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Pnd_Run_Type.equals("NON HELD")))                                                                                                            //Natural: IF #RUN-TYPE EQ 'NON HELD'
        {
            pnd_Old_Key.reset();                                                                                                                                          //Natural: RESET #OLD-KEY
            READWORK02:                                                                                                                                                   //Natural: READ WORK FILE 10 EXT ( * )
            while (condition(getWorkFiles().read(10, pdaFcpaext.getExt().getValue("*"))))
            {
                pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                           //Natural: MOVE TRUE TO #FCPANZC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 6 )
                pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
                pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
                pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                         //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
                if (condition(pnd_Key.notEquals(pnd_Old_Key)))                                                                                                            //Natural: IF #KEY NE #OLD-KEY
                {
                    pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                       //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := TRUE
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
                    sub_Set_Control_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    pnd_Old_Key.setValue(pnd_Key);                                                                                                                        //Natural: MOVE #KEY TO #OLD-KEY
                }                                                                                                                                                         //Natural: END-IF
                //*  WRITE 'WILL CALL FCPNNZC1 2ND CNTR=' EXT.CNTRCT-PPCN-NBR /* TEST
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
                sub_Accum_Control_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                          //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := FALSE
            }                                                                                                                                                             //Natural: END-WORK
            READWORK02_Exit:
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  #FCPL876.PYMNT-CHECK-NBR        := #WS.PYMNT-CHECK-NBR + 1
        //*  ADD  1                          TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*  ADD  1                          TO #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*  #FCPL876.PYMNT-LAST-ROLLOVER-NBR :=  #WS-NEXT-ROLLOVER-NBR
        //*  WRITE *PROGRAM *TIME 'Counters etc, at end of program:'
        //*   / 'Next check  number...........:' #FCPL876.PYMNT-CHECK-NBR
        //*   / 'Next EFT    number...........:' #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*   / 'Next payment sequence number.:' #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*   / 'Next rollover number ........:' #FCPL876.PYMNT-LAST-ROLLOVER-NBR
        //*  #FCPL876.#BAR-BARCODE(*)        := #BARCODE-LDA.#BAR-BARCODE(*)
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        //*  IF NOT #GLOBAL-PAY-RUN
        //*   WRITE WORK FILE 7 #FCPL876
        //*  END-IF
        getReports().write(0, NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION ****");                                                                                    //Natural: WRITE // '****** END OF PROGRAM EXECUTION ****'
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        DbsUtil.callnat(Fcpn965.class , getCurrentProcessState(), pnd_Batch_Cnt);                                                                                         //Natural: CALLNAT 'FCPN965' USING #BATCH-CNT
        if (condition(Global.isEscape())) return;
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OUTPUT-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //*    #FCPL876A.CNTRCT-ORGN-CDE          := EXT.CNTRCT-ORGN-CDE
        //*    #FCPL876A.PYMNT-CHECK-SEQ-NBR      := #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*    #FCPL876A.PYMNT-CHECK-NBR          := #FCPL876.PYMNT-CHECK-NBR
        //*    WRITE WORK FILE 8 #FCPL876A
        //*  WHEN #FCPA803.#EFT
        //*    ADD  1                             TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*  WHEN #FCPA803.#GLOBAL-PAY
        //*    ADD  1                             TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-STMNT-TYPE
        //* *************************************
        //*    #FCPA803.#STMNT                    := TRUE
        //* ************************************
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-DATA
        //* ***********************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-DATA
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-VOID-PAGE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARM
        //*     END-IF
        //*     PERFORM GET-NEXT-CHECK-FILE
        //*     END-IF
        //* ************************************
        //*  DEFINE SUBROUTINE GET-NEXT-CHECK-FILE
        //* ************************************
        //*  READ WORK FILE 6 ONCE #FCPL876
        //*  AT END OF FILE
        //*   PERFORM ERROR-DISPLAY-START
        //*   WRITE '***' 25T 'MISSING "NEXT CHECK" FILE'            77T '***'
        //*   PERFORM ERROR-DISPLAY-END
        //*   TERMINATE 52
        //*  END-ENDFILE
        //* * S-CHECK-NBR-N3              := #FCPL876.PYMNT-CHECK-PREFIX /* RL
        //*  WRITE *PROGRAM '3490' /
        //*   '=' #WS-CHECK-NBR-N3          /* RL
        //*  #BARCODE-LDA.#BAR-BARCODE (*) := #FCPL876.#BAR-BARCODE(*)
        //*  #BARCODE-LDA.#BAR-ENVELOPES   := #FCPL876.#BAR-ENVELOPES
        //*  #BARCODE-LDA.#BAR-NEW-RUN     := #FCPL876.#BAR-NEW-RUN
        //*  WRITE *PROGRAM *TIME 'at start of program:'
        //*   / 'The program is going to use the following:'
        //*   / 'start assigned check   number:' #FCPL876.PYMNT-CHECK-NBR
        //*   / 'start assigned EFT     number:' #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*   / 'start sequence number........:' #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*   / 'start rollover-nbr ..........:' #FCPL876.PYMNT-LAST-ROLLOVER-NBR
        //*  SUBTRACT  1                 FROM #FCPL876.PYMNT-CHECK-NBR
        //*  SUBTRACT  1                 FROM #FCPL876.PYMNT-CHECK-SCRTY-NBR
        //*  SUBTRACT  1                 FROM #FCPL876.PYMNT-CHECK-SEQ-NBR
        //*  #WS.PYMNT-CHECK-NBR           := #FCPL876.PYMNT-CHECK-NBR
        //*  #WS-NEXT-ROLLOVER-NBR         := #FCPL876.PYMNT-LAST-ROLLOVER-NBR
        //*  CLOSE WORK 6
        //*  END-SUBROUTINE
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-HOLD-CONTROL-RECORD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* *************************************
        //*  IF #RUN-TYPE = 'HELD' OR = 'HOLD'
        //*   #FCPL876.#BAR-NEW-RUN     := TRUE
        //*   RESET                        #FCPL876.#BAR-ENVELOPES
        //*   WRITE WORK FILE 3 #FCPANZC2
        //*  ELSE
        //*   #FCPL876.#BAR-NEW-RUN     := #BARCODE-LDA.#BAR-NEW-RUN
        //*   #FCPL876.#BAR-ENVELOPES   := #BARCODE-LDA.#BAR-ENVELOPES
        //* *********************** RL BEGIN - PAYEE MATCH ************************
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMAT-DATA
        //* **************************
        //*  *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GENERATE-XML-LINES
        //*      #BARCODE-LDA
        //*    #SVE-#NZ-CHECK-FIELDS        := #NZ-CHECK-FIELDS
        //*  *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-NUMBER-DB196
        //*  *********************************
        //*  ON ERROR
        //*   COMPRESS *PROGRAM '- line ' *ERROR-LINE 'error # ' *ERROR-NR
        //*     INTO #ERROR-MSG
        //*   WRITE  #ERROR-MSG
        //*   TERMINATE 0004
        //*  END-ERROR
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Write_Output_File() throws Exception                                                                                                                 //Natural: WRITE-OUTPUT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getWorkFiles().write(9, true, pdaFcpaext.getExt().getValue("*"));                                                                                                 //Natural: WRITE WORK FILE 9 VARIABLE EXT ( * )
        //*   #CHECK-SORT-FIELDS
        //*   PYMNT-ADDR-INFO
        //*   #NZ-CHECK-FIELDS
        //*   INV-INFO(1:C-INV-ACCT)
        //*  END-IF
    }
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
                                                                                                                                                                          //Natural: PERFORM DETERMINE-STMNT-TYPE
        sub_Determine_Stmnt_Type();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa873.getPnd_Nz_Check_Fields().reset();                                                                                                                      //Natural: RESET #NZ-CHECK-FIELDS
        //*  D  1                                 TO #FCPL876.PYMNT-CHECK-SEQ-NBR
        short decideConditionsMet1115 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FCPA803.#CHECK
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean()))
        {
            decideConditionsMet1115++;
            //*    ADD  1                             TO #WS.PYMNT-CHECK-NBR
            //*    #FCPL876.PYMNT-CHECK-NBR           := #WS.PYMNT-CHECK-NBR
            //*   IF #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE = '0000'           /* 12/15/99
            if (condition(pdaFcpaext.getExt_Cntrct_Hold_Cde().equals("0000") || pdaFcpaext.getExt_Cntrct_Hold_Cde().equals(" ")))                                         //Natural: IF EXT.CNTRCT-HOLD-CDE = '0000' OR = ' '
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                               //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                                //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#ZERO-CHECK
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().getBoolean()))
        {
            decideConditionsMet1115++;
            pdaFcpaext.getExt_Pymnt_Check_Nbr().setValue(9999999);                                                                                                        //Natural: ASSIGN EXT.PYMNT-CHECK-NBR := 9999999
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().setValue(false);                                                                                                     //Natural: ASSIGN #END-OF-PYMNT := FALSE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().reset();                                                                                                          //Natural: RESET #RECORD-IN-PYMNT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page().compute(new ComputeParameters(false, pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page()), (pdaFcpaext.getExt_Pymnt_Total_Pages().add(1)).divide(2).multiply(2).subtract(1)); //Natural: COMPUTE #BAR-LAST-PAGE = ( PYMNT-TOTAL-PAGES + 1 ) / 2 * 2 - 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#NEW-PYMNT := TRUE
    }
    private void sub_Determine_Stmnt_Type() throws Exception                                                                                                              //Natural: DETERMINE-STMNT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := TRUE
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
        sub_Set_Control_Data();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Type().reset();                                                                                                               //Natural: RESET #FCPA803.#STMNT-TYPE
        //*  CHECK
        short decideConditionsMet1141 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #ACCUM-OCCUR-2;//Natural: VALUE 48
        if (condition((pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().equals(48))))
        {
            decideConditionsMet1141++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Check().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#CHECK := TRUE
            if (condition(pdaFcpaext.getExt_Pymnt_Eft_Acct_Nbr().equals(" ") && ! (pdaFcpaext.getExt_Pymnt_Alt_Addr_Ind().getBoolean())))                                 //Natural: IF EXT.PYMNT-EFT-ACCT-NBR = ' ' AND NOT EXT.PYMNT-ALT-ADDR-IND
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().setValue(true);                                                                                             //Natural: ASSIGN #FCPA803.#CHECK-TO-ANNT := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                     //Natural: ASSIGN #FCPA803.#STMNT := TRUE
                pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().setValue(true);                                                                                             //Natural: ASSIGN #FCPA803.#STMNT-TO-ANNT := TRUE
                //*  EFT
                //*  ZERO CHECK
                //*  GLOBAL PAY / INT. ROLLOVER
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 49
        else if (condition((pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().equals(49))))
        {
            decideConditionsMet1141++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#STMNT := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().setValue(true);                                                                                                 //Natural: ASSIGN #FCPA803.#STMNT-TO-ANNT := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Eft().setValue(true);                                                                                                           //Natural: ASSIGN #FCPA803.#EFT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 47
        else if (condition((pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().equals(47))))
        {
            decideConditionsMet1141++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#ZERO-CHECK := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#STMNT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 50
        else if (condition((pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().equals(50))))
        {
            decideConditionsMet1141++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#GLOBAL-PAY := TRUE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* IF #FCPA803.#STMNT AND PYMNT-ADDR-LINES(2) NE ' '         /* 12/15/99
        //*  CORRESPONDENCE ADDRESS
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean() && pdaFcpaext.getExt_Pymnt_Addr_Line_Txt().getValue(2,"*").notEquals(" ")))                      //Natural: IF #FCPA803.#STMNT AND EXT.PYMNT-ADDR-LINE-TXT ( 2,* ) NE ' '
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(2);                                                                                                         //Natural: ASSIGN #ADDR-IND := 2
            //*  PAYMENT ADDRESS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(1);                                                                                                         //Natural: ASSIGN #ADDR-IND := 1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Control_Data() throws Exception                                                                                                                //Natural: ACCUM-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().setValue(pdaFcpaext.getExt_C_Inv_Acct());                                                                              //Natural: ASSIGN #MAX-FUND := C-INV-ACCT
        if (condition(ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().equals(getZero())))                                                                                      //Natural: IF #MAX-FUND = 0
        {
            ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().setValue(1);                                                                                                       //Natural: ASSIGN #MAX-FUND := 1
        }                                                                                                                                                                 //Natural: END-IF
        //* VE BY NAME INV-INFO(1:#MAX-FUND) TO #CNTRL-INV-ACCT(1:#MAX-FUND)  /*
        ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()).setValuesByName(pdaFcpaext.getExt_Inv_Acct().getValue(1, //Natural: MOVE BY NAME INV-ACCT ( 1:#MAX-FUND ) TO #CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
        DbsUtil.callnat(Fcpnnzc1.class , getCurrentProcessState(), pdaFcpanzc1.getPnd_Fcpanzc1(), pdaFcpanzc2.getPnd_Fcpanzc2(), ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNNZC1' USING #FCPANZC1 #FCPANZC2 #FCPLNZC2.#MAX-FUND #FCPLNZC2.#CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
        if (condition(Global.isEscape())) return;
        //*  WRITE 'AFTER CALL FCPNNZC1 CNTR=' EXT.CNTRCT-PPCN-NBR /* TEST
    }
    private void sub_Set_Control_Data() throws Exception                                                                                                                  //Natural: SET-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //* MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #FCPANZCN           /* 12/15/99
        pdaFcpanzcn.getPnd_Fcpanzcn().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                          //Natural: MOVE BY NAME EXT.EXTR TO #FCPANZCN
        DbsUtil.callnat(Fcpnnzcn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpanzcn.getPnd_Fcpanzcn(), pdaFcpanzc1.getPnd_Fcpanzc1());          //Natural: CALLNAT 'FCPNNZCN' USING #FCPACRPT #FCPANZCN #FCPANZC1
        if (condition(Global.isEscape())) return;
    }
    private void sub_Print_Void_Page() throws Exception                                                                                                                   //Natural: PRINT-VOID-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Fcpn803v.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803());                                                                          //Natural: CALLNAT 'FCPN803V' USING #FCPA803
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Input_Parm() throws Exception                                                                                                                //Natural: PROCESS-INPUT-PARM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        getWorkFiles().read(2, pnd_Ws_Run_Rqust_Parameters);                                                                                                              //Natural: READ WORK FILE 2 ONCE RUN-RQUST-PARAMETERS
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING PARAMETER FILE",new TabSetting(77),"***");                                                            //Natural: WRITE '***' 25T 'MISSING PARAMETER FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-ENDFILE
        short decideConditionsMet1207 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #FILE-TYPE;//Natural: VALUE 'CHECK' , 'CHECKS'
        if (condition((pnd_Ws_Pnd_File_Type.equals("CHECK") || pnd_Ws_Pnd_File_Type.equals("CHECKS"))))
        {
            decideConditionsMet1207++;
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(47,":",49).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 47:49 ) := TRUE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("  NEW ANNUITIZATION CHECK STATEMENTS");                                                                     //Natural: ASSIGN #FCPACRPT.#TITLE := '  NEW ANNUITIZATION CHECK STATEMENTS'
            //*     IF #RUN-TYPE = 'HELD' OR = 'HOLD'
            //*       IGNORE
            //*     ELSE
                                                                                                                                                                          //Natural: PERFORM GET-HOLD-CONTROL-RECORD
            sub_Get_Hold_Control_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'GLOBAL'
        else if (condition((pnd_Ws_Pnd_File_Type.equals("GLOBAL"))))
        {
            decideConditionsMet1207++;
            pnd_Ws_Pnd_Global_Pay_Run.setValue(true);                                                                                                                     //Natural: ASSIGN #GLOBAL-PAY-RUN := TRUE
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                  //Natural: RESET #BARCODE-LDA.#BAR-ENV-ID-NUM
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(50).setValue(true);                                                                                    //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 50 ) := TRUE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("NEW ANNUITIZATION GLOBAL PAY STATEMENTS");                                                                  //Natural: ASSIGN #FCPACRPT.#TITLE := 'NEW ANNUITIZATION GLOBAL PAY STATEMENTS'
        }                                                                                                                                                                 //Natural: VALUE 'EFT' , 'EFTS'
        else if (condition((pnd_Ws_Pnd_File_Type.equals("EFT") || pnd_Ws_Pnd_File_Type.equals("EFTS"))))
        {
            decideConditionsMet1207++;
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(47,":",49).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 47:49 ) := TRUE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("  NEW ANNUITIZATION EFT STATEMENTS");                                                                       //Natural: ASSIGN #FCPACRPT.#TITLE := '  NEW ANNUITIZATION EFT STATEMENTS'
            //*     IF #RUN-TYPE = 'HELD' OR = 'HOLD'
            //*       IGNORE
            //*     ELSE
                                                                                                                                                                          //Natural: PERFORM GET-HOLD-CONTROL-RECORD
            sub_Get_Hold_Control_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"INVALID PARAMETER:",pnd_Ws_Pnd_File_Type,new TabSetting(77),"***");                                           //Natural: WRITE '***' 25T 'INVALID PARAMETER:' #FILE-TYPE 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            //*     TERMINATE 51
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Hold_Control_Record() throws Exception                                                                                                           //Natural: GET-HOLD-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        getWorkFiles().read(4, pdaFcpanzc2.getPnd_Fcpanzc2());                                                                                                            //Natural: READ WORK FILE 4 ONCE #FCPANZC2
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'HOLD CHECK' CONTROL FILE",new TabSetting(77),"***");                                                 //Natural: WRITE '***' 25T 'MISSING "HOLD CHECK" CONTROL FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                       //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                          //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(43).setValue(false);                                                                                       //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 43 ) := FALSE
        short decideConditionsMet1250 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE #FILE-TYPE;//Natural: VALUE 'CHECK' , 'CHECKS'
        if (condition((pnd_Ws_Pnd_File_Type.equals("CHECK") || pnd_Ws_Pnd_File_Type.equals("CHECKS"))))
        {
            decideConditionsMet1250++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue(49).reset();                                                                                             //Natural: RESET #PYMNT-CNT ( 49 ) #SETTL-AMT ( 49 ) #DVDND-AMT ( 49 ) #DCI-AMT ( 49 ) #DPI-AMT ( 49 ) #NET-PYMNT-AMT ( 49 ) #FDRL-TAX-AMT ( 49 ) #STATE-TAX-AMT ( 49 ) #LOCAL-TAX-AMT ( 49 ) #REC-CNT ( 49 )
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Settl_Amt().getValue(49).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dvdnd_Amt().getValue(49).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dci_Amt().getValue(49).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dpi_Amt().getValue(49).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Net_Pymnt_Amt().getValue(49).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Fdrl_Tax_Amt().getValue(49).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_State_Tax_Amt().getValue(49).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Local_Tax_Amt().getValue(49).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Rec_Cnt().getValue(49).reset();
        }                                                                                                                                                                 //Natural: VALUE 'EFT' , 'EFTS'
        else if (condition((pnd_Ws_Pnd_File_Type.equals("EFT") || pnd_Ws_Pnd_File_Type.equals("EFTS"))))
        {
            decideConditionsMet1250++;
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Pymnt_Cnt().getValue(48).reset();                                                                                             //Natural: RESET #PYMNT-CNT ( 48 ) #SETTL-AMT ( 48 ) #DVDND-AMT ( 48 ) #DCI-AMT ( 48 ) #DPI-AMT ( 48 ) #NET-PYMNT-AMT ( 48 ) #FDRL-TAX-AMT ( 48 ) #STATE-TAX-AMT ( 48 ) #LOCAL-TAX-AMT ( 48 ) #REC-CNT ( 48 )
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Settl_Amt().getValue(48).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dvdnd_Amt().getValue(48).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dci_Amt().getValue(48).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Dpi_Amt().getValue(48).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Net_Pymnt_Amt().getValue(48).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Fdrl_Tax_Amt().getValue(48).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_State_Tax_Amt().getValue(48).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Local_Tax_Amt().getValue(48).reset();
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Rec_Cnt().getValue(48).reset();
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        DbsUtil.callnat(Fcpn962.class , getCurrentProcessState(), pdaFcpanzc2.getPnd_Fcpanzc2(), pdaFcpacrpt.getPnd_Fcpacrpt());                                          //Natural: CALLNAT 'FCPN962' USING #FCPANZC2 #FCPACRPT
        if (condition(Global.isEscape())) return;
        //* ** 'NZ'
        //*  END-IF
    }
    //*  RL
    private void sub_Get_Check_Format_Data() throws Exception                                                                                                             //Natural: GET-CHECK-FORMAT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        //*  RL
        //* RL
        //* RL
        //* RL
        //* RL
        //* RL
        getReports().write(0, "6915",Global.getPROGRAM(),"CALL FCPN110 REF TABLE P14A1",NEWLINE,"=",pdaFcpa110.getFcpa110_Fcpa110_Source_Code(),NEWLINE,"=",pdaFcpa110.getFcpa110_Start_Check_No(),"=",pdaFcpa110.getFcpa110_Start_Check_Prefix_N3(),"=",pdaFcpa110.getFcpa110_Company_Name(),NEWLINE,"=",pdaFcpa110.getFcpa110_Company_Address(),  //Natural: WRITE '6915' *PROGRAM 'CALL FCPN110 REF TABLE P14A1' / '=' FCPA110.FCPA110-SOURCE-CODE / '=' FCPA110.START-CHECK-NO '=' START-CHECK-PREFIX-N3 '=' FCPA110.COMPANY-NAME / '=' FCPA110.COMPANY-ADDRESS ( AL = 25 ) '=' FCPA110.BANK-ADDRESS1 ( AL = 25 ) / '=' FCPA110.BANK-TRANSMISSION-CDE / '=' FCPA110.BANK-NAME ( AL = 10 ) /
            new AlphanumericLength (25),"=",pdaFcpa110.getFcpa110_Bank_Address1(), new AlphanumericLength (25),NEWLINE,"=",pdaFcpa110.getFcpa110_Bank_Transmission_Cde(),NEWLINE,"=",pdaFcpa110.getFcpa110_Bank_Name(), 
            new AlphanumericLength (10),NEWLINE);
        if (Global.isEscape()) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                      //Natural: RESET #BAR-ENV-ID-NUM
    }
    private void sub_Generate_Xml_Lines() throws Exception                                                                                                                //Natural: GENERATE-XML-LINES
    {
        if (BLNatReinput.isReinput()) return;

        //*  **********************************************
        if (condition(pnd_First_Record.getBoolean()))                                                                                                                     //Natural: IF #FIRST-RECORD
        {
            setValueToSubstring("005",pnd_Save_First_Twenty,1,3);                                                                                                         //Natural: MOVE '005' TO SUBSTR ( #SAVE-FIRST-TWENTY,01,03 )
            pnd_Save_First_Twenty_Documentrequestid_Yyyy.setValueEdited(Global.getTIMX(),new ReportEditMask("YYYY"));                                                     //Natural: MOVE EDITED *TIMX ( EM = YYYY ) TO DOCUMENTREQUESTID-YYYY
            pnd_Save_First_Twenty_Documentrequestid_Mm.setValueEdited(Global.getTIMX(),new ReportEditMask("MM"));                                                         //Natural: MOVE EDITED *TIMX ( EM = MM ) TO DOCUMENTREQUESTID-MM
            pnd_Save_First_Twenty_Documentrequestid_Dd.setValueEdited(Global.getTIMX(),new ReportEditMask("DD"));                                                         //Natural: MOVE EDITED *TIMX ( EM = DD ) TO DOCUMENTREQUESTID-DD
            pnd_Save_First_Twenty_Documentrequestid_Hh.setValueEdited(Global.getTIMX(),new ReportEditMask("HH"));                                                         //Natural: MOVE EDITED *TIMX ( EM = HH ) TO DOCUMENTREQUESTID-HH
            pnd_Save_First_Twenty_Documentrequestid_Ii.setValueEdited(Global.getTIMX(),new ReportEditMask("II"));                                                         //Natural: MOVE EDITED *TIMX ( EM = II ) TO DOCUMENTREQUESTID-II
            pnd_Save_First_Twenty_Documentrequestid_Ss.setValueEdited(Global.getTIMX(),new ReportEditMask("SS"));                                                         //Natural: MOVE EDITED *TIMX ( EM = SS ) TO DOCUMENTREQUESTID-SS
        }                                                                                                                                                                 //Natural: END-IF
        //* **
        //*      TESTING ONLY TO DUMP VALUES IN PDA   END
        //*  FCPA873
        //*  RL CHECK&SEQ
        //*  SAVE FOR LAST RECORD
        DbsUtil.callnat(Fcpn960.class , getCurrentProcessState(), pdaFcpa873.getPnd_Nz_Check_Fields(), pdaFcpaext.getExt().getValue("*"), pdaFcpa803.getPnd_Fcpa803(),    //Natural: CALLNAT 'FCPN960' USING #NZ-CHECK-FIELDS EXT ( * ) #FCPA803 #FCPA874A #FCPA874H #FCPA803L FCPA110 #BATCH-CNT #SAVE-FIRST-TWENTY #FIRST-RECORD #LAST-CONTRACT #LAST-RECORD #TOTALS-AREA #SVE-DOCUMENTREQUESTID-DATA #SAVE-EFT-ACCT #CHECK-EFT #CHECK-ROLLOVER #SVE-INSTITUTIONINFOLINE-DATA ( * )
            pdaFcpa874a.getPnd_Fcpa874a(), pdaFcpa874h.getPnd_Fcpa874h(), pdaFcpa803l.getPnd_Fcpa803l(), pdaFcpa110.getFcpa110(), pnd_Batch_Cnt, pnd_Save_First_Twenty, 
            pnd_First_Record, pnd_Last_Contract, pnd_Last_Record, pnd_Totals_Area, pnd_Sve_Documentrequestid_Data, pnd_Save_Eft_Acct, pnd_Check_Eft, pnd_Check_Rollover, 
            pnd_Sve_Institutioninfoline_Data.getValue("*"));
        if (condition(Global.isEscape())) return;
        pnd_Sve_Ext.getValue("*").setValue(pdaFcpaext.getExt().getValue("*"));                                                                                            //Natural: ASSIGN #SVE-EXT ( * ) := EXT ( * )
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ivc_Amt.setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pymnt_Ivc_Amt());                                                        //Natural: ASSIGN #SVE-PYMNT-IVC-AMT := #NZ-CHECK-FIELDS.PYMNT-IVC-AMT
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Ded_Pymnt_Table.getValue("*").setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Ded_Pymnt_Table().getValue("*"));                    //Natural: ASSIGN #SVE-DED-PYMNT-TABLE ( * ) := #DED-PYMNT-TABLE ( * )
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Records.setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Records());                                                    //Natural: ASSIGN #SVE-PYMNT-RECORDS := #PYMNT-RECORDS
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Dpi_Dci_Ind.setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Dpi_Dci_Ind().getBoolean());                                           //Natural: ASSIGN #SVE-DPI-DCI-IND := #DPI-DCI-IND
        pnd_Sve_Nz_Check_Fields_Pnd_Sve_Pymnt_Ded_Ind.setValue(pdaFcpa873.getPnd_Nz_Check_Fields_Pnd_Pymnt_Ded_Ind().getBoolean());                                       //Natural: ASSIGN #SVE-PYMNT-DED-IND := #PYMNT-DED-IND
        if (condition(pnd_First_Record.getBoolean()))                                                                                                                     //Natural: IF #FIRST-RECORD
        {
            pnd_First_Record.setValue(false);                                                                                                                             //Natural: ASSIGN #FIRST-RECORD := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  GENERATE-XML-LINES
    }
    private void sub_Get_Check_Number_Db196() throws Exception                                                                                                            //Natural: GET-CHECK-NUMBER-DB196
    {
        if (BLNatReinput.isReinput()) return;

        //*   #PYMNT-NOT-FOUND               := TRUE
        pnd_Ws_Check_Nbr_N10.reset();                                                                                                                                     //Natural: RESET #WS-CHECK-NBR-N10
        pnd_Pymnt_S.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                                            //Natural: MOVE BY NAME EXTR TO #PYMNT-S
        vw_fcp_Cons_Pymnu.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) FCP-CONS-PYMNU BY PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE
        (
        "READ03",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        READ03:
        while (condition(vw_fcp_Cons_Pymnu.readNextRow("READ03")))
        {
            //*  WRITE 'From FCPP960 #PYMNT-S ' /* TESTING ONLY START
            //*    / '=' #PYMNT-S.CNTRCT-PPCN-NBR
            //*    / '=' #PYMNT-S.CNTRCT-INVRSE-DTE
            //*    / '=' #PYMNT-S.CNTRCT-ORGN-CDE
            //*    / '=' #PYMNT-S.PYMNT-PRCSS-SEQ-NUM
            //*    /  '********************** FCP-CONS-PYMNT *******************'
            //*    / '='    FCP-CONS-PYMNU.CNTRCT-PPCN-NBR
            //*    / '='    FCP-CONS-PYMNU.CNTRCT-INVRSE-DTE
            //*    / '='    FCP-CONS-PYMNU.CNTRCT-ORGN-CDE
            //*    / '='    FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NUM   /* TESTING END
            if (condition(fcp_Cons_Pymnu_Cntrct_Ppcn_Nbr.equals(pnd_Pymnt_S_Cntrct_Ppcn_Nbr) && fcp_Cons_Pymnu_Cntrct_Invrse_Dte.equals(pnd_Pymnt_S_Cntrct_Invrse_Dte)    //Natural: IF FCP-CONS-PYMNU.CNTRCT-PPCN-NBR = #PYMNT-S.CNTRCT-PPCN-NBR AND FCP-CONS-PYMNU.CNTRCT-INVRSE-DTE = #PYMNT-S.CNTRCT-INVRSE-DTE AND FCP-CONS-PYMNU.CNTRCT-ORGN-CDE = #PYMNT-S.CNTRCT-ORGN-CDE AND FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NUM = #PYMNT-S.PYMNT-PRCSS-SEQ-NUM
                && fcp_Cons_Pymnu_Cntrct_Orgn_Cde.equals(pnd_Pymnt_S_Cntrct_Orgn_Cde) && fcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num.equals(pnd_Pymnt_S_Pymnt_Prcss_Seq_Num)))
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(fcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind.equals(1)))                                                                                               //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1
            {
                pnd_Ws_Check_Nbr_N10.setValue(fcp_Cons_Pymnu_Pymnt_Nbr);                                                                                                  //Natural: ASSIGN #WS-CHECK-NBR-N10 := FCP-CONS-PYMNU.PYMNT-NBR
                pdaFcpaext.getExt_Pymnt_Check_Nbr().setValue(pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7);                                                                   //Natural: ASSIGN EXT.PYMNT-CHECK-NBR := #WS-CHECK-NBR-N7
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(fcp_Cons_Pymnu_Pymnt_Check_Nbr.greater(getZero())))                                                                                         //Natural: IF FCP-CONS-PYMNU.PYMNT-CHECK-NBR GT 0
                {
                    pdaFcpaext.getExt_Pymnt_Check_Nbr().setValue(fcp_Cons_Pymnu_Pymnt_Check_Nbr);                                                                         //Natural: MOVE FCP-CONS-PYMNU.PYMNT-CHECK-NBR TO EXT.PYMNT-CHECK-NBR
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpaext.getExt_Pymnt_Check_Nbr().compute(new ComputeParameters(false, pdaFcpaext.getExt_Pymnt_Check_Nbr()), fcp_Cons_Pymnu_Pymnt_Check_Nbr.multiply(-1)); //Natural: COMPUTE EXT.PYMNT-CHECK-NBR = FCP-CONS-PYMNU.PYMNT-CHECK-NBR * -1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr().setValue(fcp_Cons_Pymnu_Pymnt_Check_Scrty_Nbr);                                                                     //Natural: ASSIGN EXT.PYMNT-CHECK-SCRTY-NBR := FCP-CONS-PYMNU.PYMNT-CHECK-SCRTY-NBR
            pdaFcpaext.getExt_Pymnt_Check_Seq_Nbr().setValue(fcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr);                                                                         //Natural: ASSIGN EXT.PYMNT-CHECK-SEQ-NBR := FCP-CONS-PYMNU.PYMNT-CHECK-SEQ-NBR
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET-CHECK-NUMBER-DB196
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        if (condition(pnd_KeyIsBreak))
        {
            //*  NOT EOF
            if (condition(pnd_Key.notEquals(readWork01Pnd_KeyOld)))                                                                                                       //Natural: IF #KEY NE OLD ( #KEY )
            {
                pnd_Last_Contract.setValue(true);                                                                                                                         //Natural: ASSIGN #LAST-CONTRACT := TRUE
                //*  PROCESS TRAILER RECORDS
                //*  FCPA873
                //*  RL CHECK&SEQ
                DbsUtil.callnat(Fcpn960.class , getCurrentProcessState(), pnd_Sve_Nz_Check_Fields, pnd_Sve_Ext.getValue("*"), pdaFcpa803.getPnd_Fcpa803(),                //Natural: CALLNAT 'FCPN960' USING #SVE-NZ-CHECK-FIELDS #SVE-EXT ( * ) #FCPA803 #FCPA874A #FCPA874H #FCPA803L FCPA110 #BATCH-CNT #SAVE-FIRST-TWENTY #FIRST-RECORD #LAST-CONTRACT #LAST-RECORD #TOTALS-AREA #SVE-DOCUMENTREQUESTID-DATA #SAVE-EFT-ACCT #CHECK-EFT #CHECK-ROLLOVER #SVE-INSTITUTIONINFOLINE-DATA ( * )
                    pdaFcpa874a.getPnd_Fcpa874a(), pdaFcpa874h.getPnd_Fcpa874h(), pdaFcpa803l.getPnd_Fcpa803l(), pdaFcpa110.getFcpa110(), pnd_Batch_Cnt, 
                    pnd_Save_First_Twenty, pnd_First_Record, pnd_Last_Contract, pnd_Last_Record, pnd_Totals_Area, pnd_Sve_Documentrequestid_Data, pnd_Save_Eft_Acct, 
                    pnd_Check_Eft, pnd_Check_Rollover, pnd_Sve_Institutioninfoline_Data.getValue("*"));
                if (condition(Global.isEscape())) return;
                //*       #BARCODE-LDA
                pnd_Last_Contract.setValue(false);                                                                                                                        //Natural: ASSIGN #LAST-CONTRACT := FALSE
                if (condition(pdaFcpaext.getExt_Pymnt_Check_Amt().greater(getZero())))                                                                                    //Natural: IF EXT.PYMNT-CHECK-AMT GT 0
                {
                    pnd_Batch_Cnt.nadd(1);                                                                                                                                //Natural: ADD 1 TO #BATCH-CNT
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                sub_Init_New_Pymnt();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");

        getReports().write(0, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(24),"PROCESS NEW ANNUITIZATION",pnd_Ws_Pnd_File_Type,new TabSetting(68),"REPORT: RPT0",NEWLINE,new 
            TabSetting(36),pnd_Ws_Pnd_Run_Type,NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData872() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*   MOVE BY NAME PYMNT-ADDR-INFO       TO #FCPA803                   /*
            pdaFcpa803.getPnd_Fcpa803().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                        //Natural: MOVE BY NAME EXTR TO #FCPA803
            //*    IF #GLOBAL-PAY-RUN        /* FE2016 COMMENT
            //*      PERFORM PRINT-VOID-PAGE /* FE2016
            //*    END-IF                    /* FE2016
            pnd_Batch_Cnt.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #BATCH-CNT
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
            sub_Init_New_Pymnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
