/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:45 PM
**        * FROM NATURAL PROGRAM : Fcpp802
************************************************************
**        * FILE NAME            : Fcpp802.java
**        * CLASS NAME           : Fcpp802
**        * INSTANCE NAME        : Fcpp802
************************************************************
************************************************************************
* PROGRAM  : FCPP802
*
* SYSTEM   : CPS
* TITLE    : IAR RESTRUCTURE
*
* FUNCTION : CALCULATE AND PROPOGATE CHECK FIELDS.
************************************************************************
* NOTE: WHEN MAKING CHANGES TO THIS MODULE, PLEASE CHECK TO SEE IF THE
*       SAME CHANGES NEED TO BE MADE TO FCPP802M.  THAT MODULE WAS
*       CLONED FROM THIS ONE SO THAT THE MONTHLY JOBSTREAM COULD WRITE
*       A LARGER RECORD WITHOUT DISTURBING THIS MODULE.  THIS PROGRAM
*       RUNS IN THE 1400 JOBSTREAM AND DOES NOT CURRENTLY NEED TO WRITE
*       ROTH FIELDS.
*
*       AER - 5/15/08
************************************************************************
*
* 05/23/00: MCGEE - ADD BREAK OF PA-SELECT VS SPIA
*          :      - ADD TO INTERNAL SORT CNTRCT-ANNTY-INS-TYPE
*          :      - DECENDING AFTER COMPANY CODE
*          :      - WILL RESULT IN COMPANY "L" LIFE (PASELECT /SPIA)
*          :      - DIVIDED INTO PA-SELECT "S" CNTRCT-ANNTY-INS-TYPE
*          :      -         AND  SPIA      "M" CNTRCT-ANNTY-INS-TYPE
*      05/15/2008   AER - ROTH-MAJOR1 - CHANGED FCPA800 TO FCPA800D
* 12/2015    :J.OSTEEN   - ADD CHILD SUPPORT DEDUCTION --    /* JWO1
*  4/2017  :JJG PIN EXPANSION, RESTOW (FCPA800D CHANGE)
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp802 extends BLNatBase
{
    // Data Areas
    private PdaFcpa800d pdaFcpa800d;
    private PdaFcpa801b pdaFcpa801b;
    private PdaFcpa801c pdaFcpa801c;
    private LdaFcpl802 ldaFcpl802;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;

    private DbsGroup pnd_Key_Pnd_Key_Detail;
    private DbsField pnd_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Key_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Cntrct_Annty_Ins_Type;

    private DbsGroup pnd_Save_Fields;
    private DbsField pnd_Save_Fields_Pymnt_Total_Pages;
    private DbsField pnd_Save_Fields_Pnd_Pymnt_Records;
    private DbsField pnd_Save_Fields_Pnd_Pymnt_Ded_Pnd;
    private DbsField pnd_Save_Fields_Pnd_Dpi_Ind;
    private DbsField pnd_Save_Fields_Pnd_Pymnt_Ded_Ind;
    private DbsField pnd_Save_Fields_Pnd_Ded_Pymnt_Table;

    private DbsGroup pnd_Save_Fields__R_Field_2;
    private DbsField pnd_Save_Fields_Pnd_Ded_Pymnt_Table_Num;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Accum_Ded_Table;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Table;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_K;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800d = new PdaFcpa800d(localVariables);
        pdaFcpa801b = new PdaFcpa801b(localVariables);
        pdaFcpa801c = new PdaFcpa801c(localVariables);
        ldaFcpl802 = new LdaFcpl802();
        registerRecord(ldaFcpl802);

        // Local Variables
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 31);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);

        pnd_Key_Pnd_Key_Detail = pnd_Key__R_Field_1.newGroupInGroup("pnd_Key_Pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Cntrct_Orgn_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Cntrct_Invrse_Dte = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Key_Cntrct_Cmbn_Nbr = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Key_Pymnt_Prcss_Seq_Num = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Cntrct_Annty_Ins_Type = localVariables.newFieldInRecord("pnd_Cntrct_Annty_Ins_Type", "#CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);

        pnd_Save_Fields = localVariables.newGroupInRecord("pnd_Save_Fields", "#SAVE-FIELDS");
        pnd_Save_Fields_Pymnt_Total_Pages = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pymnt_Total_Pages", "PYMNT-TOTAL-PAGES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Save_Fields_Pnd_Pymnt_Records = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Pymnt_Records", "#PYMNT-RECORDS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Save_Fields_Pnd_Pymnt_Ded_Pnd = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Pymnt_Ded_Pnd", "#PYMNT-DED-#", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Save_Fields_Pnd_Dpi_Ind = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Dpi_Ind", "#DPI-IND", FieldType.BOOLEAN, 1);
        pnd_Save_Fields_Pnd_Pymnt_Ded_Ind = pnd_Save_Fields.newFieldInGroup("pnd_Save_Fields_Pnd_Pymnt_Ded_Ind", "#PYMNT-DED-IND", FieldType.BOOLEAN, 
            1);
        pnd_Save_Fields_Pnd_Ded_Pymnt_Table = pnd_Save_Fields.newFieldArrayInGroup("pnd_Save_Fields_Pnd_Ded_Pymnt_Table", "#DED-PYMNT-TABLE", FieldType.STRING, 
            1, new DbsArrayController(1, 15));

        pnd_Save_Fields__R_Field_2 = pnd_Save_Fields.newGroupInGroup("pnd_Save_Fields__R_Field_2", "REDEFINE", pnd_Save_Fields_Pnd_Ded_Pymnt_Table);
        pnd_Save_Fields_Pnd_Ded_Pymnt_Table_Num = pnd_Save_Fields__R_Field_2.newFieldArrayInGroup("pnd_Save_Fields_Pnd_Ded_Pymnt_Table_Num", "#DED-PYMNT-TABLE-NUM", 
            FieldType.NUMERIC, 1, new DbsArrayController(1, 15));

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Accum_Ded_Table = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Accum_Ded_Table", "#ACCUM-DED-TABLE", FieldType.BOOLEAN, 1, new DbsArrayController(1, 
            15));
        pnd_Ws_Pnd_Pymnt_Ded_Table = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Table", "#PYMNT-DED-TABLE", FieldType.BOOLEAN, 1, new DbsArrayController(1, 
            15));
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_K = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_K", "#K", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl802.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp802() throws Exception
    {
        super("Fcpp802");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 57T 'IAR CONTROL REPORT' 120T 'REPORT: RPT ' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*                                       /* P2210CPM.SRTCHK
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #CHECK-SORT-FIELDS WF-PYMNT-ADDR-REC
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pdaFcpa801c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            CheckAtStartofData507();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                   //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #KEY-DETAIL
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CHECK-FIELDS
            sub_Determine_Check_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Cmbne_Ind().notEquals("Y")))                                                                             //Natural: IF PYMNT-CMBNE-IND NE 'Y'
            {
                pnd_Ws_Pnd_Pymnt_Ded_Table.getValue("*").setValue(pnd_Ws_Pnd_Accum_Ded_Table.getValue("*").getBoolean());                                                 //Natural: MOVE #WS.#ACCUM-DED-TABLE ( * ) TO #WS.#PYMNT-DED-TABLE ( * )
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PYMNT-CHECK-FIELDS
                sub_Determine_Pymnt_Check_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue("*").setValue(pnd_Save_Fields_Pnd_Ded_Pymnt_Table.getValue("*"));                          //Natural: ASSIGN #CHECK-FIELDS.#DED-PYMNT-TABLE ( * ) := #SAVE-FIELDS.#DED-PYMNT-TABLE ( * )
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Pnd().setValue(pnd_Save_Fields_Pnd_Pymnt_Ded_Pnd);                                                          //Natural: ASSIGN #CHECK-FIELDS.#PYMNT-DED-# := #SAVE-FIELDS.#PYMNT-DED-#
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind().setValue(pnd_Save_Fields_Pnd_Pymnt_Ded_Ind.getBoolean());                                             //Natural: ASSIGN #CHECK-FIELDS.#PYMNT-DED-IND := #SAVE-FIELDS.#PYMNT-DED-IND
                //*  CHECKS.EXTRA.FIELDS
                getWorkFiles().write(2, true, pdaFcpa801c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa801b.getPnd_Check_Fields(),  //Natural: WRITE WORK FILE 2 VARIABLE #CHECK-SORT-FIELDS PYMNT-ADDR-INFO #CHECK-FIELDS INV-INFO ( 1:INV-ACCT-COUNT )
                    pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            getSort().writeSortInData(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),  //Natural: END-ALL
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num(), pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type(), 
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr(), pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde(), pdaFcpa801c.getPnd_Check_Sort_Fields_Pymnt_Total_Pages(), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(1), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(2), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(3), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(4), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(5), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(6), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(7), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(8), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(9), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(10), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(11), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(12), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(13), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(14), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(15), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(16), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(17), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(18), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(19), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(20), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(1), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(2), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(3), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(4), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(5), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(6), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(7), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(8), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(9), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(10), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(11), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(12), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(13), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(14), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(15), pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded(), pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Records(), 
                pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Pnd(), pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind(), pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind(), 
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(1), pnd_Ws_Pnd_Accum_Ded_Table.getValue(2), pnd_Ws_Pnd_Accum_Ded_Table.getValue(3), pnd_Ws_Pnd_Accum_Ded_Table.getValue(4), 
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(5), pnd_Ws_Pnd_Accum_Ded_Table.getValue(6), pnd_Ws_Pnd_Accum_Ded_Table.getValue(7), pnd_Ws_Pnd_Accum_Ded_Table.getValue(8), 
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(9), pnd_Ws_Pnd_Accum_Ded_Table.getValue(10), pnd_Ws_Pnd_Accum_Ded_Table.getValue(11), pnd_Ws_Pnd_Accum_Ded_Table.getValue(12), 
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(13), pnd_Ws_Pnd_Accum_Ded_Table.getValue(14), pnd_Ws_Pnd_Accum_Ded_Table.getValue(15), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(1), 
                pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(2), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(3), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(4), 
                pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(5), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(6), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(7), 
                pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(8), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(9), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(10), 
                pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(11), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(12), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(13), 
                pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(14), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(15), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(16), 
                pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(17), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(18), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(19));
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //*  FOR PA-SELECT
        getSort().sortData(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(),  //Natural: SORT BY WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE WF-PYMNT-ADDR-GRP.CNTRCT-INVRSE-DTE WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NUM #CHECK-SORT-FIELDS.CNTRCT-COMPANY-CDE WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE DESC PYMNT-INSTMT-NBR DESC USING #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE #CHECK-SORT-FIELDS.PYMNT-TOTAL-PAGES #CHECK-FIELDS.#FUND-ON-PAGE ( * ) #CHECK-FIELDS.#DED-PYMNT-TABLE ( * ) #CHECK-FIELDS.#CNTRCT-#-DED #CHECK-FIELDS.#PYMNT-RECORDS #CHECK-FIELDS.#PYMNT-DED-# #CHECK-FIELDS.#DPI-IND #CHECK-FIELDS.#PYMNT-DED-IND #WS.#ACCUM-DED-TABLE ( * ) WF-PYMNT-ADDR-GRP ( * )
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num(), pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type(), 
            "DESCENDING", pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr(), "DESCENDING");
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte(), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Num(), pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde(), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr(), pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde(), 
            pdaFcpa801c.getPnd_Check_Sort_Fields_Pymnt_Total_Pages(), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(1), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(2), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(3), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(4), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(5), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(6), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(7), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(8), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(9), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(10), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(11), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(12), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(13), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(14), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(15), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(16), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(17), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(18), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(19), pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(20), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(1), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(2), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(3), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(4), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(5), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(6), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(7), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(8), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(9), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(10), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(11), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(12), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(13), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(14), pdaFcpa801b.getPnd_Check_Fields_Pnd_Ded_Pymnt_Table().getValue(15), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded(), pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Records(), pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Pnd(), 
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind(), pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Ded_Ind(), pnd_Ws_Pnd_Accum_Ded_Table.getValue(1), 
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(2), pnd_Ws_Pnd_Accum_Ded_Table.getValue(3), pnd_Ws_Pnd_Accum_Ded_Table.getValue(4), pnd_Ws_Pnd_Accum_Ded_Table.getValue(5), 
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(6), pnd_Ws_Pnd_Accum_Ded_Table.getValue(7), pnd_Ws_Pnd_Accum_Ded_Table.getValue(8), pnd_Ws_Pnd_Accum_Ded_Table.getValue(9), 
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(10), pnd_Ws_Pnd_Accum_Ded_Table.getValue(11), pnd_Ws_Pnd_Accum_Ded_Table.getValue(12), pnd_Ws_Pnd_Accum_Ded_Table.getValue(13), 
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(14), pnd_Ws_Pnd_Accum_Ded_Table.getValue(15), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(1), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(2), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(3), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(4), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(5), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(6), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(7), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(8), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(9), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(10), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(11), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(12), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(13), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(14), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(15), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(16), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(17), 
            pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(18), pdaFcpa800d.getWf_Pymnt_Addr_Grp().getValue(19))))
        {
            CheckAtStartofData542();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                   //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #KEY-DETAIL
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
            pdaFcpa801c.getPnd_Check_Sort_Fields().setValuesByName(pnd_Save_Fields);                                                                                      //Natural: MOVE BY NAME #SAVE-FIELDS TO #CHECK-SORT-FIELDS
            pdaFcpa801b.getPnd_Check_Fields().setValuesByName(pnd_Save_Fields);                                                                                           //Natural: MOVE BY NAME #SAVE-FIELDS TO #CHECK-FIELDS
            //*  CHECKS.EXTRA.FIELDS
            getWorkFiles().write(2, true, pdaFcpa801c.getPnd_Check_Sort_Fields(), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa801b.getPnd_Check_Fields(),  //Natural: WRITE WORK FILE 2 VARIABLE #CHECK-SORT-FIELDS PYMNT-ADDR-INFO #CHECK-FIELDS INV-INFO ( 1:INV-ACCT-COUNT )
                pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //* ***************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CHECK-FIELDS
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-PRINT-PAGES
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEWPAGE-PROCESSING
        //* *********************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PYMNT-CHECK-FIELDS
        //*  FOR #I = 1 TO 14
        //*      VALUE 4:14
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT-AFTER-SORT
    }
    private void sub_Determine_Check_Fields() throws Exception                                                                                                            //Natural: DETERMINE-CHECK-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***************************************
        pdaFcpa801b.getPnd_Check_Fields_Pnd_Pymnt_Records().nadd(1);                                                                                                      //Natural: ADD 1 TO #CHECK-FIELDS.#PYMNT-RECORDS
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).notEquals(new                 //Natural: IF INV-ACCT-DPI-AMT ( 1:INV-ACCT-COUNT ) NE 0.00
            DbsDecimal("0.00"))))
        {
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Dpi_Ind().setValue(true);                                                                                                 //Natural: ASSIGN #CHECK-FIELDS.#DPI-IND := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded().reset();                                                                                                    //Natural: RESET #CNTRCT-#-DED
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).notEquals(new            //Natural: IF INV-ACCT-FDRL-TAX-AMT ( 1:INV-ACCT-COUNT ) NE 0.00
            DbsDecimal("0.00"))))
        {
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(1).setValue(true);                                                                                                        //Natural: ASSIGN #WS.#ACCUM-DED-TABLE ( 1 ) := TRUE
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded().nadd(1);                                                                                                //Natural: ASSIGN #CNTRCT-#-DED := #CNTRCT-#-DED + 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).notEquals(new           //Natural: IF INV-ACCT-STATE-TAX-AMT ( 1:INV-ACCT-COUNT ) NE 0.00
            DbsDecimal("0.00"))))
        {
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(2).setValue(true);                                                                                                        //Natural: ASSIGN #WS.#ACCUM-DED-TABLE ( 2 ) := TRUE
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded().nadd(1);                                                                                                //Natural: ASSIGN #CNTRCT-#-DED := #CNTRCT-#-DED + 1
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).notEquals(new           //Natural: IF INV-ACCT-LOCAL-TAX-AMT ( 1:INV-ACCT-COUNT ) NE 0.00
            DbsDecimal("0.00"))))
        {
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(3).setValue(true);                                                                                                        //Natural: ASSIGN #WS.#ACCUM-DED-TABLE ( 3 ) := TRUE
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded().nadd(1);                                                                                                //Natural: ASSIGN #CNTRCT-#-DED := #CNTRCT-#-DED + 1
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(10)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).equals(getZero())))                                                     //Natural: IF PYMNT-DED-CDE ( #I ) = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  JWO1
                //*  JWO1
                if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).greaterOrEqual(900) && pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).lessOrEqual(999))) //Natural: IF PYMNT-DED-CDE ( #I ) = 900 THRU 999
                {
                    pnd_Ws_Pnd_J.setValue(15);                                                                                                                            //Natural: ASSIGN #J := 15
                    //*  JWO1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_J.compute(new ComputeParameters(false, pnd_Ws_Pnd_J), pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).add(3));     //Natural: ADD PYMNT-DED-CDE ( #I ) 3 GIVING #J
                    //*  JWO1
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Ws_Pnd_Accum_Ded_Table.getValue(pnd_Ws_Pnd_J).getBoolean()))                                                                            //Natural: IF #WS.#ACCUM-DED-TABLE ( #J )
                {
                    ignore();
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Accum_Ded_Table.getValue(pnd_Ws_Pnd_J).setValue(true);                                                                                     //Natural: ASSIGN #WS.#ACCUM-DED-TABLE ( #J ) := TRUE
                    pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded().nadd(1);                                                                                        //Natural: ASSIGN #CNTRCT-#-DED := #CNTRCT-#-DED + 1
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM CALC-PRINT-PAGES
        sub_Calc_Print_Pages();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Calc_Print_Pages() throws Exception                                                                                                                  //Natural: CALC-PRINT-PAGES
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        ldaFcpl802.getPnd_Fcpl802_Pnd_Deductions_Left().compute(new ComputeParameters(false, ldaFcpl802.getPnd_Fcpl802_Pnd_Deductions_Left()), pdaFcpa801b.getPnd_Check_Fields_Pnd_Cntrct_Pnd__Ded().subtract(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).multiply(ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Per_Fund())); //Natural: COMPUTE #DEDUCTIONS-LEFT = #CNTRCT-#-DED - INV-ACCT-COUNT * #LINES-PER-FUND
        if (condition(ldaFcpl802.getPnd_Fcpl802_Pnd_Deductions_Left().less(getZero())))                                                                                   //Natural: IF #DEDUCTIONS-LEFT LT 0
        {
            ldaFcpl802.getPnd_Fcpl802_Pnd_Deductions_Left().reset();                                                                                                      //Natural: RESET #DEDUCTIONS-LEFT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde().notEquals(ldaFcpl802.getPnd_Fcpl802_Pnd_Company_Cde()) || ((pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("S")  //Natural: IF CNTRCT-COMPANY-CDE NE #COMPANY-CDE OR ( ( CNTRCT-ANNTY-INS-TYPE EQ 'S' OR CNTRCT-ANNTY-INS-TYPE EQ 'M' ) AND ( CNTRCT-ANNTY-INS-TYPE NE #CNTRCT-ANNTY-INS-TYPE ) )
            || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("M")) && (pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().notEquals(pnd_Cntrct_Annty_Ins_Type)))))
        {
            ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Lines().nadd(5);                                                                                                        //Natural: ADD 5 TO #CURRENT-LINES
            ldaFcpl802.getPnd_Fcpl802_Pnd_Company_Cde().setValue(pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde());                                              //Natural: ASSIGN #COMPANY-CDE := CNTRCT-COMPANY-CDE
            pnd_Cntrct_Annty_Ins_Type.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type());                                                                 //Natural: ASSIGN #CNTRCT-ANNTY-INS-TYPE := CNTRCT-ANNTY-INS-TYPE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr().isBreak()))                                                                                  //Natural: IF BREAK OF CNTRCT-PPCN-NBR
            {
                ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Lines().nadd(2);                                                                                                    //Natural: ADD 2 TO #CURRENT-LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page().equals(1)))                                                                                            //Natural: IF #CURRENT-PAGE = 1
        {
                                                                                                                                                                          //Natural: PERFORM NEWPAGE-PROCESSING
            sub_Newpage_Processing();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO INV-ACCT-COUNT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Ws_Pnd_I.nadd(1))
        {
            ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines().setValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Per_Fund());                                                          //Natural: MOVE #LINES-PER-FUND TO #FUND-LINES
            if (condition(pnd_Ws_Pnd_I.equals(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())))                                                                        //Natural: IF #I = INV-ACCT-COUNT
            {
                ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines().nadd(ldaFcpl802.getPnd_Fcpl802_Pnd_Deductions_Left());                                                         //Natural: ADD #DEDUCTIONS-LEFT TO #FUND-LINES
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Lines().nadd(ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines());                                                               //Natural: ADD #FUND-LINES TO #CURRENT-LINES
            if (condition(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Lines().greater(ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Per_Page().getValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Idx())))) //Natural: IF #CURRENT-LINES GT #LINES-PER-PAGE ( #LINES-IDX )
            {
                ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page().nadd(1);                                                                                                     //Natural: ADD 1 TO #CURRENT-PAGE
                                                                                                                                                                          //Natural: PERFORM NEWPAGE-PROCESSING
                sub_Newpage_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Lines().setValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines());                                                       //Natural: MOVE #FUND-LINES TO #CURRENT-LINES
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa801b.getPnd_Check_Fields_Pnd_Fund_On_Page().getValue(pnd_Ws_Pnd_I).setValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page());                             //Natural: ASSIGN #CHECK-FIELDS.#FUND-ON-PAGE ( #I ) := #CURRENT-PAGE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa801c.getPnd_Check_Sort_Fields_Pymnt_Total_Pages().setValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page());                                                  //Natural: ASSIGN #CHECK-SORT-FIELDS.PYMNT-TOTAL-PAGES := #CURRENT-PAGE
    }
    private void sub_Newpage_Processing() throws Exception                                                                                                                //Natural: NEWPAGE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        short decideConditionsMet676 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURRENT-PAGE = 1
        if (condition(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page().equals(1)))
        {
            decideConditionsMet676++;
            ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Idx().setValue(1);                                                                                                        //Natural: ASSIGN #LINES-IDX := 1
        }                                                                                                                                                                 //Natural: WHEN #CURRENT-PAGE = 2 AND NOT #STMNT
        else if (condition(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page().equals(2) && ! (ldaFcpl802.getPnd_Fcpl802_Pnd_Stmnt().getBoolean())))
        {
            decideConditionsMet676++;
            ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Idx().setValue(2);                                                                                                        //Natural: ASSIGN #LINES-IDX := 2
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Idx().setValue(3);                                                                                                        //Natural: ASSIGN #LINES-IDX := 3
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Pymnt_Check_Fields() throws Exception                                                                                                      //Natural: DETERMINE-PYMNT-CHECK-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************
        //*  JWO 01/29/10
        pnd_Ws_Pnd_J.reset();                                                                                                                                             //Natural: RESET #J #K
        pnd_Ws_Pnd_K.reset();
        FOR03:                                                                                                                                                            //Natural: FOR #I = 1 TO 15
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(15)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Pymnt_Ded_Table.getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                                //Natural: IF #WS.#PYMNT-DED-TABLE ( #I )
            {
                pnd_Save_Fields_Pnd_Pymnt_Ded_Pnd.nadd(1);                                                                                                                //Natural: ADD 1 TO #SAVE-FIELDS.#PYMNT-DED-#
                pnd_Save_Fields_Pnd_Pymnt_Ded_Ind.setValue(true);                                                                                                         //Natural: ASSIGN #SAVE-FIELDS.#PYMNT-DED-IND := TRUE
                short decideConditionsMet695 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #I;//Natural: VALUE 1
                if (condition((pnd_Ws_Pnd_I.equals(1))))
                {
                    decideConditionsMet695++;
                    if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                              //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
                    {
                        pnd_Save_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("N");                                                                         //Natural: ASSIGN #SAVE-FIELDS.#DED-PYMNT-TABLE ( #I ) := 'N'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Save_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("F");                                                                         //Natural: ASSIGN #SAVE-FIELDS.#DED-PYMNT-TABLE ( #I ) := 'F'
                        //*  JWO1
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_K.nadd(1);                                                                                                                                 //Natural: ASSIGN #K := #K +1
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_Ws_Pnd_I.equals(2))))
                {
                    decideConditionsMet695++;
                    pnd_Save_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("S");                                                                             //Natural: ASSIGN #SAVE-FIELDS.#DED-PYMNT-TABLE ( #I ) := 'S'
                    pnd_Ws_Pnd_K.nadd(1);                                                                                                                                 //Natural: ASSIGN #K := #K +1
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_Ws_Pnd_I.equals(3))))
                {
                    decideConditionsMet695++;
                    pnd_Save_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("L");                                                                             //Natural: ASSIGN #SAVE-FIELDS.#DED-PYMNT-TABLE ( #I ) := 'L'
                    pnd_Ws_Pnd_K.nadd(1);                                                                                                                                 //Natural: ASSIGN #K := #K +1
                }                                                                                                                                                         //Natural: VALUE 4:15
                else if (condition(((pnd_Ws_Pnd_I.greaterOrEqual(4) && pnd_Ws_Pnd_I.lessOrEqual(15)))))
                {
                    decideConditionsMet695++;
                    pnd_Ws_Pnd_J.nadd(1);                                                                                                                                 //Natural: ASSIGN #J := #J + 1
                    if (condition(pnd_Ws_Pnd_J.less(DbsField.subtract(10,pnd_Ws_Pnd_K))))                                                                                 //Natural: IF #J LT 10 - #K
                    {
                        pnd_Save_Fields_Pnd_Ded_Pymnt_Table_Num.getValue(pnd_Ws_Pnd_I).setValue(pnd_Ws_Pnd_J);                                                            //Natural: ASSIGN #SAVE-FIELDS.#DED-PYMNT-TABLE-NUM ( #I ) := #J
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Save_Fields_Pnd_Ded_Pymnt_Table_Num.getValue(pnd_Ws_Pnd_I).setValue(0);                                                                       //Natural: ASSIGN #SAVE-FIELDS.#DED-PYMNT-TABLE-NUM ( #I ) := 0
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Save_Fields.resetInitial();                                                                                                                                   //Natural: RESET INITIAL #SAVE-FIELDS #CHECK-SORT-FIELDS.PYMNT-TOTAL-PAGES #CHECK-FIELDS #WS.#ACCUM-DED-TABLE ( * ) #FCPL802
        pdaFcpa801c.getPnd_Check_Sort_Fields_Pymnt_Total_Pages().resetInitial();
        pdaFcpa801b.getPnd_Check_Fields().resetInitial();
        pnd_Ws_Pnd_Accum_Ded_Table.getValue("*").resetInitial();
        ldaFcpl802.getPnd_Fcpl802().resetInitial();
        if (condition(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(getZero()) || pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3)))       //Natural: IF PYMNT-CHECK-AMT = 0 OR PYMNT-PAY-TYPE-REQ-IND = 3
        {
            ldaFcpl802.getPnd_Fcpl802_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #STMNT := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl802.getPnd_Fcpl802_Pnd_Stmnt().setValue(false);                                                                                                        //Natural: ASSIGN #STMNT := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl802.getPnd_Fcpl802_Pnd_Company_Cde().setValue(pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde());                                                  //Natural: ASSIGN #COMPANY-CDE := CNTRCT-COMPANY-CDE
        pnd_Cntrct_Annty_Ins_Type.setValue(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type());                                                                     //Natural: ASSIGN #CNTRCT-ANNTY-INS-TYPE := CNTRCT-ANNTY-INS-TYPE
    }
    private void sub_Init_New_Pymnt_After_Sort() throws Exception                                                                                                         //Natural: INIT-NEW-PYMNT-AFTER-SORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        pnd_Save_Fields.setValuesByName(pdaFcpa801c.getPnd_Check_Sort_Fields());                                                                                          //Natural: MOVE BY NAME #CHECK-SORT-FIELDS TO #SAVE-FIELDS
        pnd_Save_Fields.setValuesByName(pdaFcpa801b.getPnd_Check_Fields());                                                                                               //Natural: MOVE BY NAME #CHECK-FIELDS TO #SAVE-FIELDS
        pnd_Ws_Pnd_Pymnt_Ded_Table.getValue("*").setValue(pnd_Ws_Pnd_Accum_Ded_Table.getValue("*").getBoolean());                                                         //Natural: MOVE #WS.#ACCUM-DED-TABLE ( * ) TO #WS.#PYMNT-DED-TABLE ( * )
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PYMNT-CHECK-FIELDS
        sub_Determine_Pymnt_Check_Fields();
        if (condition(Global.isEscape())) {return;}
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        if (condition(pnd_KeyIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
            sub_Init_New_Pymnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        if (condition(pnd_KeyIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT-AFTER-SORT
            sub_Init_New_Pymnt_After_Sort();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(57),"IAR CONTROL REPORT",new TabSetting(120),"REPORT: RPT ",NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData507() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                   //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #KEY-DETAIL
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
            sub_Init_New_Pymnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
    private void CheckAtStartofData542() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpa800d.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info());                                                                   //Natural: MOVE BY NAME PYMNT-ADDR-INFO TO #KEY-DETAIL
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT-AFTER-SORT
            sub_Init_New_Pymnt_After_Sort();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
