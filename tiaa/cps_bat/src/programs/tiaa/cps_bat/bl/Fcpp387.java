/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:33 PM
**        * FROM NATURAL PROGRAM : Fcpp387
************************************************************
**        * FILE NAME            : Fcpp387.java
**        * CLASS NAME           : Fcpp387
**        * INSTANCE NAME        : Fcpp387
************************************************************
************************************************************************
* PROGRAM  : FCPP387
* SYSTEM   : CPS
* TITLE    : ANNUITANT STATEMENT
* GENERATED: JUL 26,93 AT 10:04 AM
* FUNCTION : THIS PROGRAM WILL BUILD ANNUITANT STATEMENT.
*
*
*
* HISTORY
************************************************************************
************************************************************************
*  NOTES:
*  ******
* MONTHLY SETTLEMENT SETTLES FEW SETTLEMENTS, WHICH ARE PRINTED IN
* DIFFERENT FORMATS, I.E. DIFFERENT PROTOTYPES, AS FOLLOWS:
*
*      SETTLEMENT            CNTRCT-TYPE-CDE   PROTOTYPE#
*      TYPE
*  --  --------------------- ----------------- ---------------
*
*  1.  MATURITY                10                4
*  2.  TPA                     20                8
*  3.  MDO                     30                6
*  4.  RECURRING MDO           31                6
*  5.  LUMP SUM DEATH          40                6
*  6.  SURVIVOR BENEFITS       50                4
*
* ----------------------------------------------------------------------

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp387 extends BLNatBase
{
    // Data Areas
    private LdaFcpl220 ldaFcpl220;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Cntl_Idx;
    private DbsField pnd_Program;
    private DbsField pnd_Abend_Cde;
    private DbsField pnd_Prime_Counter;
    private DbsField pnd_C_Rcrd_10;
    private DbsField pnd_C_Rcrd_20;
    private DbsField pnd_C_Rcrd_30;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Ws_Rec_1;
    private DbsField pnd_Ws_First_Time_In;
    private DbsField pnd_Ws_Next_Eft_Nbr;
    private DbsField pnd_Ws_Next_Global_Nbr;
    private DbsField pnd_Ws_Current_Pymnt_Id;
    private DbsField pnd_Ws_Current_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Current_Cntrct_Type_Cde;

    private DbsGroup pnd_Output_Parm;
    private DbsField pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr;
    private DbsField pnd_Output_Parm_Filler;
    private DbsField pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr;
    private DbsField pnd_Ws_One;
    private DbsField pnd_Ws_Side_Printed;
    private DbsField pnd_Ws_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Sim_Dup_Multiplex_Written;
    private DbsField pnd_Ws_Save_S_D_M_Plex;
    private DbsField pnd_Ws_Format_Notification_Letter;
    private DbsField pnd_Ws_Bottom_Part;
    private DbsField pnd_Ws_Jde;
    private DbsField pnd_Ws_Format;
    private DbsField pnd_Ws_Forms;
    private DbsField pnd_Ws_Feed;
    private DbsField pnd_Ws_Max;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_K;
    private DbsField pnd_L;

    private DbsGroup pnd_Ws_Work_Rec_Record;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes;

    private DbsGroup pnd_Ws_Work_Rec_Record__R_Field_1;

    private DbsGroup pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_A;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_B;
    private DbsField pnd_Ws_Header_Record;

    private DbsGroup pnd_Ws_Header_Record__R_Field_2;

    private DbsGroup pnd_Ws_Header_Record_Pnd_Ws_Header_Level2;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Header_Record__R_Field_3;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Header_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Header_Record_Filler;
    private DbsField pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type;
    private DbsField pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Insurance_Option;
    private DbsField pnd_Ws_Header_Record_Cntrct_Life_Contingency;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;

    private DbsGroup pnd_Ws_Header_Record__R_Field_4;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes;
    private DbsField pnd_Ws_Occurs;

    private DbsGroup pnd_Ws_Occurs__R_Field_5;

    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Side;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fed_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Occurs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Occurs_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Deductions;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Amt;

    private DbsGroup pnd_Ws_Occurs_Pnd_Pyhdr_Data;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte;

    private DbsGroup pnd_Ws_Occurs__R_Field_6;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler;

    private DbsGroup pnd_Ws_Name_N_Address_Record;
    private DbsField pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes;

    private DbsGroup pnd_Ws_Name_N_Address_Record__R_Field_7;

    private DbsGroup pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2;
    private DbsField pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_A;

    private DbsGroup pnd_Ws_Name_N_Address_Record_Nme_N_Addr;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Nme;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line1_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line2_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line3_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line4_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line5_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line6_Txt;
    private DbsField pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_B;
    private DbsField pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_C;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl220 = new LdaFcpl220();
        registerRecord(ldaFcpl220);
        registerRecord(ldaFcpl220.getVw_fcp_Cons_Addr_View());
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Cntl_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntl_Idx", "#CNTL-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);
        pnd_Prime_Counter = localVariables.newFieldInRecord("pnd_Prime_Counter", "#PRIME-COUNTER", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Rcrd_10 = localVariables.newFieldInRecord("pnd_C_Rcrd_10", "#C-RCRD-10", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Rcrd_20 = localVariables.newFieldInRecord("pnd_C_Rcrd_20", "#C-RCRD-20", FieldType.PACKED_DECIMAL, 7);
        pnd_C_Rcrd_30 = localVariables.newFieldInRecord("pnd_C_Rcrd_30", "#C-RCRD-30", FieldType.PACKED_DECIMAL, 7);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Ws_Rec_1 = localVariables.newFieldInRecord("pnd_Ws_Rec_1", "#WS-REC-1", FieldType.STRING, 143);
        pnd_Ws_First_Time_In = localVariables.newFieldInRecord("pnd_Ws_First_Time_In", "#WS-FIRST-TIME-IN", FieldType.STRING, 1);
        pnd_Ws_Next_Eft_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Eft_Nbr", "#WS-NEXT-EFT-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Next_Global_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Global_Nbr", "#WS-NEXT-GLOBAL-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Current_Pymnt_Id = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Id", "#WS-CURRENT-PYMNT-ID", FieldType.NUMERIC, 7);
        pnd_Ws_Current_Pymnt_Prcss_Seq_Num = localVariables.newFieldInRecord("pnd_Ws_Current_Pymnt_Prcss_Seq_Num", "#WS-CURRENT-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Current_Cntrct_Type_Cde = localVariables.newFieldInRecord("pnd_Ws_Current_Cntrct_Type_Cde", "#WS-CURRENT-CNTRCT-TYPE-CDE", FieldType.STRING, 
            2);

        pnd_Output_Parm = localVariables.newGroupInRecord("pnd_Output_Parm", "#OUTPUT-PARM");
        pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr = pnd_Output_Parm.newFieldInGroup("pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr", "#WS-NEXT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Output_Parm_Filler = pnd_Output_Parm.newFieldInGroup("pnd_Output_Parm_Filler", "FILLER", FieldType.STRING, 1);
        pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr = pnd_Output_Parm.newFieldInGroup("pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr", "#WS-CURRENT-PYMNT-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_One = localVariables.newFieldInRecord("pnd_Ws_One", "#WS-ONE", FieldType.PACKED_DECIMAL, 1);
        pnd_Ws_Side_Printed = localVariables.newFieldInRecord("pnd_Ws_Side_Printed", "#WS-SIDE-PRINTED", FieldType.STRING, 1);
        pnd_Ws_Cntr_Inv_Acct = localVariables.newFieldInRecord("pnd_Ws_Cntr_Inv_Acct", "#WS-CNTR-INV-ACCT", FieldType.NUMERIC, 3);
        pnd_Ws_Sim_Dup_Multiplex_Written = localVariables.newFieldInRecord("pnd_Ws_Sim_Dup_Multiplex_Written", "#WS-SIM-DUP-MULTIPLEX-WRITTEN", FieldType.STRING, 
            1);
        pnd_Ws_Save_S_D_M_Plex = localVariables.newFieldInRecord("pnd_Ws_Save_S_D_M_Plex", "#WS-SAVE-S-D-M-PLEX", FieldType.STRING, 1);
        pnd_Ws_Format_Notification_Letter = localVariables.newFieldInRecord("pnd_Ws_Format_Notification_Letter", "#WS-FORMAT-NOTIFICATION-LETTER", FieldType.STRING, 
            1);
        pnd_Ws_Bottom_Part = localVariables.newFieldInRecord("pnd_Ws_Bottom_Part", "#WS-BOTTOM-PART", FieldType.STRING, 1);
        pnd_Ws_Jde = localVariables.newFieldInRecord("pnd_Ws_Jde", "#WS-JDE", FieldType.STRING, 3);
        pnd_Ws_Format = localVariables.newFieldInRecord("pnd_Ws_Format", "#WS-FORMAT", FieldType.STRING, 8);
        pnd_Ws_Forms = localVariables.newFieldInRecord("pnd_Ws_Forms", "#WS-FORMS", FieldType.STRING, 6);
        pnd_Ws_Feed = localVariables.newFieldInRecord("pnd_Ws_Feed", "#WS-FEED", FieldType.STRING, 6);
        pnd_Ws_Max = localVariables.newFieldInRecord("pnd_Ws_Max", "#WS-MAX", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.INTEGER, 2);
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_L = localVariables.newFieldInRecord("pnd_L", "#L", FieldType.INTEGER, 2);

        pnd_Ws_Work_Rec_Record = localVariables.newGroupInRecord("pnd_Ws_Work_Rec_Record", "#WS-WORK-REC-RECORD");
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes = pnd_Ws_Work_Rec_Record.newFieldArrayInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes", "#WS-WORK-REC-BYTES", 
            FieldType.STRING, 1, new DbsArrayController(1, 500));

        pnd_Ws_Work_Rec_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Work_Rec_Record__R_Field_1", "REDEFINE", pnd_Ws_Work_Rec_Record);

        pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key = pnd_Ws_Work_Rec_Record__R_Field_1.newGroupInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key", "#WS-WORK-REC-KEY");
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num", 
            "#WS-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code", 
            "#WS-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_A = pnd_Ws_Work_Rec_Record__R_Field_1.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_A", "#WS-FILLER-A", 
            FieldType.STRING, 217);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_B = pnd_Ws_Work_Rec_Record__R_Field_1.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Filler_B", "#WS-FILLER-B", 
            FieldType.STRING, 242);
        pnd_Ws_Header_Record = localVariables.newFieldArrayInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD", FieldType.STRING, 1, new DbsArrayController(1, 
            500));

        pnd_Ws_Header_Record__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Header_Record__R_Field_2", "REDEFINE", pnd_Ws_Header_Record);

        pnd_Ws_Header_Record_Pnd_Ws_Header_Level2 = pnd_Ws_Header_Record__R_Field_2.newGroupInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Level2", "#WS-HEADER-LEVEL2");
        pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", 
            "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", 
            "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", 
            "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", 
            "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", 
            "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", 
            "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", 
            "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", 
            "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", 
            "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", 
            "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", 
            "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", 
            "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", 
            "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", 
            "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", 
            "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", 
            "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", 
            "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", 
            "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", 
            "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", 
            "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", 
            "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Ws_Header_Record__R_Field_3 = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newGroupInGroup("pnd_Ws_Header_Record__R_Field_3", "REDEFINE", pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record__R_Field_3.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record__R_Field_3.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", 
            "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", 
            "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", 
            "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", 
            "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", 
            "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time", 
            "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Ws_Header_Record_Pymnt_Intrfce_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Filler", "FILLER", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte", 
            "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type", 
            "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde", 
            "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Insurance_Option = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Insurance_Option", 
            "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Life_Contingency = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Life_Contingency", 
            "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", 
            "#WS-HEADER-FILLER", FieldType.STRING, 117);

        pnd_Ws_Header_Record__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Header_Record__R_Field_4", "REDEFINE", pnd_Ws_Header_Record);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes = pnd_Ws_Header_Record__R_Field_4.newFieldArrayInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes", "#WS-HEADER-BYTES", 
            FieldType.STRING, 1, new DbsArrayController(1, 500));
        pnd_Ws_Occurs = localVariables.newFieldArrayInRecord("pnd_Ws_Occurs", "#WS-OCCURS", FieldType.STRING, 1, new DbsArrayController(1, 40, 1, 500));

        pnd_Ws_Occurs__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Occurs__R_Field_5", "REDEFINE", pnd_Ws_Occurs);

        pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2 = pnd_Ws_Occurs__R_Field_5.newGroupArrayInGroup("pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2", "#WS-OCCURS-LEVEL2", 
            new DbsArrayController(1, 40));
        pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Ws_Side = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Side", "#WS-SIDE", FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Ws_Occurs_Inv_Acct_Unit_Value = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9, 4);
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_State_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Local_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Occurs_Cntrct_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Option_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Occurs_Cntrct_Mode_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Cntr_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Occurs_Pymnt_Ded_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 10));

        pnd_Ws_Occurs_Pnd_Pyhdr_Data = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newGroupInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Data", "#PYHDR-DATA");
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte", "#PYHDR-PYMNT-SETTLMNT-DTE", 
            FieldType.DATE);

        pnd_Ws_Occurs__R_Field_6 = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newGroupInGroup("pnd_Ws_Occurs__R_Field_6", "REDEFINE", pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte);
        pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4 = pnd_Ws_Occurs__R_Field_6.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Pymnt_Settlmnt_Dte_A4", "#PYHDR-PYMNT-SETTLMNT-DTE-A4", 
            FieldType.STRING, 4);
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Ctznshp_Cde", "#PYHDR-ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde = pnd_Ws_Occurs_Pnd_Pyhdr_Data.newFieldInGroup("pnd_Ws_Occurs_Pnd_Pyhdr_Annt_Rsdncy_Cde", "#PYHDR-ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler", "#WS-INV-ACCT-FILLER", 
            FieldType.STRING, 180);

        pnd_Ws_Name_N_Address_Record = localVariables.newGroupInRecord("pnd_Ws_Name_N_Address_Record", "#WS-NAME-N-ADDRESS-RECORD");
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes = pnd_Ws_Name_N_Address_Record.newFieldArrayInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes", 
            "#WS-NAME-N-ADDRESS-BYTES", FieldType.STRING, 1, new DbsArrayController(1, 2, 1, 500));

        pnd_Ws_Name_N_Address_Record__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_Name_N_Address_Record__R_Field_7", "REDEFINE", pnd_Ws_Name_N_Address_Record);

        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2 = pnd_Ws_Name_N_Address_Record__R_Field_7.newGroupArrayInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2", 
            "#WS-NAME-N-ADDR-LEVEL2", new DbsArrayController(1, 2));
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_A = pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_A", 
            "#WS-FILLER-A", FieldType.STRING, 111);

        pnd_Ws_Name_N_Address_Record_Nme_N_Addr = pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2.newGroupInGroup("pnd_Ws_Name_N_Address_Record_Nme_N_Addr", 
            "NME-N-ADDR");
        pnd_Ws_Name_N_Address_Record_Pymnt_Nme = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Nme", "PYMNT-NME", 
            FieldType.STRING, 38);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line1_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line1_Txt", 
            "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line2_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line2_Txt", 
            "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line3_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line3_Txt", 
            "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line4_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line4_Txt", 
            "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line5_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line5_Txt", 
            "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line6_Txt = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pymnt_Addr_Line6_Txt", 
            "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 35);
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_B = pnd_Ws_Name_N_Address_Record_Nme_N_Addr.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_B", 
            "#WS-FILLER-B", FieldType.STRING, 57);
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_C = pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Addr_Level2.newFieldInGroup("pnd_Ws_Name_N_Address_Record_Pnd_Ws_Filler_C", 
            "#WS-FILLER-C", FieldType.STRING, 84);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl220.initializeValues();
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_2.setInitialValue("               ANNUITANT STATEMENT");
        pnd_Ws_Rec_1.setInitialValue(" ");
        pnd_Ws_First_Time_In.setInitialValue("Y");
        pnd_Ws_One.setInitialValue(1);
        pnd_Ws_Cntr_Inv_Acct.setInitialValue(0);
        pnd_Ws_Format_Notification_Letter.setInitialValue("N");
        pnd_Ws_Bottom_Part.setInitialValue(" ");
        pnd_Ws_Jde.setInitialValue(" ");
        pnd_Ws_Format.setInitialValue(" ");
        pnd_Ws_Forms.setInitialValue(" ");
        pnd_Ws_Max.setInitialValue(0);
        pnd_I.setInitialValue(0);
        pnd_J.setInitialValue(0);
        pnd_K.setInitialValue(0);
        pnd_L.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp387() throws Exception
    {
        super("Fcpp387");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp387|Main");
        getReports().atTopOfPage(atTopEventRpt0, 0);
        setupReports();
        while(true)
        {
            try
            {
                //*  DEFINE PRINTERS AND FORMATS
                //* *FORMAT LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF
                //*                                                                                                                                                       //Natural: FORMAT LS = 133 PS = 60 ZP = ON
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM = *PROGRAM
                //*                                                                                                                                                       //Natural: AT TOP OF PAGE
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //* ***********************
                //*   MAIN PROGRAM LOGIC  *
                //* ***********************
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Cntl_Idx);                                                                                  //Natural: INPUT #CNTL-IDX
                getWorkFiles().read(6, ldaFcpl876.getPnd_Fcpl876());                                                                                                      //Natural: READ WORK FILE 6 ONCE #FCPL876
                if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                  //Natural: AT END OF FILE
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"MISSING 'NEXT CHECK' FILE",new TabSetting(77),"***");                            //Natural: WRITE '***' 25T 'MISSING "NEXT CHECK" FILE' 77T '***'
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(51);  if (true) return;                                                                                                             //Natural: TERMINATE 51
                }                                                                                                                                                         //Natural: END-ENDFILE
                pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                   //Natural: ASSIGN #WS-CURRENT-PYMNT-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
                pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                              //Natural: ASSIGN #WS-NEXT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
                pnd_Ws_Next_Eft_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr());                                                                          //Natural: ASSIGN #WS-NEXT-EFT-NBR := #FCPL876.PYMNT-CHECK-SCRTY-NBR
                pnd_Ws_Next_Global_Nbr.setValue(1);                                                                                                                       //Natural: ASSIGN #WS-NEXT-GLOBAL-NBR := 1
                getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"at start of program:",NEWLINE,"The program is going to use the following:", //Natural: WRITE *PROGRAM *TIME 'at start of program:' / 'The program is going to use the following:' / 'start assigned check   number:' #WS-NEXT-CHECK-NBR / 'start assigned EFT     number:' #WS-NEXT-EFT-NBR / 'start assigned Global  number:' #WS-NEXT-GLOBAL-NBR / 'start sequence number........:' #WS-CURRENT-PYMNT-SEQ-NBR
                    NEWLINE,"start assigned check   number:",pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr,NEWLINE,"start assigned EFT     number:",pnd_Ws_Next_Eft_Nbr,
                    NEWLINE,"start assigned Global  number:",pnd_Ws_Next_Global_Nbr,NEWLINE,"start sequence number........:",pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr);
                if (Global.isEscape()) return;
                //*  ADJUST CURRENT-PYMNT-SEQ-NBR FOR THE FIRST TIME
                pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr.nsubtract(1);                                                                                                //Natural: SUBTRACT 1 FROM #WS-CURRENT-PYMNT-SEQ-NBR
                pnd_Prime_Counter.reset();                                                                                                                                //Natural: RESET #PRIME-COUNTER
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 #WS-WORK-REC-BYTES ( * )
                while (condition(getWorkFiles().read(1, pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"))))
                {
                    pnd_Prime_Counter.nadd(1);                                                                                                                            //Natural: ADD 1 TO #PRIME-COUNTER
                    if (condition(pnd_Ws_First_Time_In.equals("Y")))                                                                                                      //Natural: IF #WS-FIRST-TIME-IN = 'Y'
                    {
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                        sub_Initialize_Fields();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                      //Natural: ASSIGN #WS-HEADER-BYTES ( * ) := #WS-WORK-REC-BYTES ( * )
                        //*  10=HEADER REC
                        if (condition(pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.notEquals(10)))                                                                     //Natural: IF #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR NE 10
                        {
                            pnd_Abend_Cde.setValue(43);                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 43
                            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"REASON: The extract file must start with a record type 10", //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / 'REASON: The extract file must start with a record type 10' / 'however it starts with a record with' / '=' #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR
                                NEWLINE,"however it starts with a record with",NEWLINE,"=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                          //Natural: TERMINATE #ABEND-CDE
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PYMNT-ID
                        sub_Determine_Pymnt_Id();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Ws_First_Time_In.setValue("N");                                                                                                               //Natural: ASSIGN #WS-FIRST-TIME-IN = 'N'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Current_Pymnt_Prcss_Seq_Num.equals(pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num)))                                          //Natural: IF #WS-CURRENT-PYMNT-PRCSS-SEQ-NUM = #WS-PYMNT-PRCSS-SEQ-NUM
                    {
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-N-SEQ-NMBRS
                        sub_Assign_Check_N_Seq_Nmbrs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
                        sub_Move_Small_Rec_To_Large_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Current_Pymnt_Prcss_Seq_Num.notEquals(pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num)))                                       //Natural: IF #WS-CURRENT-PYMNT-PRCSS-SEQ-NUM NE #WS-PYMNT-PRCSS-SEQ-NUM
                    {
                                                                                                                                                                          //Natural: PERFORM FORMAT-DOCUMENT
                        sub_Format_Document();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INITIALIZE-FIELDS
                        sub_Initialize_Fields();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PYMNT-ID
                        sub_Determine_Pymnt_Id();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ASSIGN-CHECK-N-SEQ-NMBRS
                        sub_Assign_Check_N_Seq_Nmbrs();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM MOVE-SMALL-REC-TO-LARGE-REC
                        sub_Move_Small_Rec_To_Large_Rec();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                //*  10-05-1999
                if (condition(pnd_Prime_Counter.greater(getZero())))                                                                                                      //Natural: IF #PRIME-COUNTER > 0
                {
                    //*  PROCESS LAST-RECORD
                                                                                                                                                                          //Natural: PERFORM FORMAT-DOCUMENT
                    sub_Format_Document();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),Global.getTIME(),"Counters etc, at end of program:",NEWLINE," number of records read......:", //Natural: WRITE *PROGRAM *TIME 'Counters etc, at end of program:' / ' number of records read......:' #PRIME-COUNTER / '              records type 10:' #C-RCRD-10 / '              records type 20:' #C-RCRD-20 / '              records type 30:' #C-RCRD-30
                        pnd_Prime_Counter,NEWLINE,"              records type 10:",pnd_C_Rcrd_10,NEWLINE,"              records type 20:",pnd_C_Rcrd_20,
                        NEWLINE,"              records type 30:",pnd_C_Rcrd_30);
                    if (Global.isEscape()) return;
                    pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr.nadd(1);                                                                                                 //Natural: ADD 1 TO #WS-CURRENT-PYMNT-SEQ-NBR
                    getReports().write(0, ReportOption.NOTITLE,"Next check  number...........:",pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr,NEWLINE,"Next EFT    number...........:", //Natural: WRITE 'Next check  number...........:' #WS-NEXT-CHECK-NBR / 'Next EFT    number...........:' #WS-NEXT-EFT-NBR / 'Next Global number...........:' #WS-NEXT-GLOBAL-NBR / 'Next payment sequence number.:' #WS-CURRENT-PYMNT-SEQ-NBR
                        pnd_Ws_Next_Eft_Nbr,NEWLINE,"Next Global number...........:",pnd_Ws_Next_Global_Nbr,NEWLINE,"Next payment sequence number.:",pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr);
                    if (Global.isEscape()) return;
                    ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().setValue(pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr);                                               //Natural: ASSIGN #FCPL876.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
                    ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().setValue(pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr);                                                          //Natural: ASSIGN #FCPL876.PYMNT-CHECK-NBR := #WS-NEXT-CHECK-NBR
                    ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().setValue(pnd_Ws_Next_Eft_Nbr);                                                                      //Natural: ASSIGN #FCPL876.PYMNT-CHECK-SCRTY-NBR := #WS-NEXT-EFT-NBR
                    getWorkFiles().write(7, false, ldaFcpl876.getPnd_Fcpl876());                                                                                          //Natural: WRITE WORK FILE 7 #FCPL876
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION ****");                                                   //Natural: WRITE // '****** END OF PROGRAM EXECUTION ****'
                    if (Global.isEscape()) return;
                    //* ***** LEON 9-25
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    getWorkFiles().write(7, false, ldaFcpl876.getPnd_Fcpl876());                                                                                          //Natural: WRITE WORK FILE 7 #FCPL876
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"***!! WORK FILE 1 CONTAIN NO RECORDS TO PROCESS !!**");                                   //Natural: WRITE // '***!! WORK FILE 1 CONTAIN NO RECORDS TO PROCESS !!**'
                    if (Global.isEscape()) return;
                    getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION  **");                                                    //Natural: WRITE // '****** END OF PROGRAM EXECUTION  **'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* *****
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PYMNT-ID
                //*  ======================================================================
                //*  DETERMINE THE PAYMENT IDENTIFIER OF THE CURRENT PAYMENT
                //*  WHETHER  IT IS A CHECK, AN EFT OR A GLOBAL PAY
                //*  ----------------------------------------------------------------------
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ASSIGN-CHECK-N-SEQ-NMBRS
                //*  ======================================================================
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MOVE-SMALL-REC-TO-LARGE-REC
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-DOCUMENT
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-STATEMENT
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-XEROX-COMMANDS
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZE-FIELDS
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-CHECK
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SENT-TO-FORM
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FORMAT-NOTIFICATION
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: BODY-OF-STATEMENT
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Determine_Pymnt_Id() throws Exception                                                                                                                //Natural: DETERMINE-PYMNT-ID
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                                      //Natural: ASSIGN #WS-HEADER-BYTES ( * ) := #WS-WORK-REC-BYTES ( * )
        pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr.nadd(1);                                                                                                             //Natural: ADD 1 TO #WS-CURRENT-PYMNT-SEQ-NBR
        //*  CHECKS
        short decideConditionsMet478 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1))))
        {
            decideConditionsMet478++;
            if (condition(pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code.equals("0000")))                                                                               //Natural: IF #WS-WORK-REC-RECORD.#WS-CONTRACT-HOLD-CODE = '0000'
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                               //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                                //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl876a.getPnd_Fcpl876a_Cntrct_Orgn_Cde().setValue(pnd_Ws_Header_Record_Cntrct_Orgn_Cde);                                                                 //Natural: ASSIGN #FCPL876A.CNTRCT-ORGN-CDE := #WS-HEADER-RECORD.CNTRCT-ORGN-CDE
            ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr().setValue(pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr);                                                     //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
            pnd_Ws_Current_Pymnt_Id.setValue(pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr);                                                                                      //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-CHECK-NBR
            ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().setValue(pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr);                                                                //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-NBR := #WS-NEXT-CHECK-NBR
            pnd_Output_Parm_Pnd_Ws_Next_Check_Nbr.nadd(1);                                                                                                                //Natural: ADD 1 TO #WS-NEXT-CHECK-NBR
            //*  EFT
            getWorkFiles().write(8, false, ldaFcpl876a.getPnd_Fcpl876a());                                                                                                //Natural: WRITE WORK FILE 8 #FCPL876A
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(2))))
        {
            decideConditionsMet478++;
            pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Eft_Nbr);                                                                                                        //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-EFT-NBR
            //*  GLOBAL PAY
            pnd_Ws_Next_Eft_Nbr.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #WS-NEXT-EFT-NBR
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(3))))
        {
            decideConditionsMet478++;
            pnd_Ws_Current_Pymnt_Id.setValue(pnd_Ws_Next_Global_Nbr);                                                                                                     //Natural: ASSIGN #WS-CURRENT-PYMNT-ID := #WS-NEXT-GLOBAL-NBR
            pnd_Ws_Next_Global_Nbr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #WS-NEXT-GLOBAL-NBR
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Abend_Cde.setValue(44);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 44
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Invalid data for","=",pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind); //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Invalid data for' '=' #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  DETERMINE-PYMNT-ID
    }
    private void sub_Assign_Check_N_Seq_Nmbrs() throws Exception                                                                                                          //Natural: ASSIGN-CHECK-N-SEQ-NMBRS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr.setValue(pnd_Ws_Current_Pymnt_Id);                                                                                  //Natural: ASSIGN #WS-WORK-REC-RECORD.#WS-PYMNT-CHECK-NBR := #WS-CURRENT-PYMNT-ID
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr.setValue(pnd_Output_Parm_Pnd_Ws_Current_Pymnt_Seq_Nbr);                                                         //Natural: ASSIGN #WS-WORK-REC-RECORD.#WS-PYMNT-CHECK-SEQ-NBR := #WS-CURRENT-PYMNT-SEQ-NBR
        getWorkFiles().write(9, false, pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                                                                       //Natural: WRITE WORK FILE 9 #WS-WORK-REC-BYTES ( * )
    }
    private void sub_Move_Small_Rec_To_Large_Rec() throws Exception                                                                                                       //Natural: MOVE-SMALL-REC-TO-LARGE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  READ THE WORK FILE RECORD INTO THE WORKING STORAGE TABLES
        //*  ----------------------------------------------------------------------
        //*  HEADER REC
        short decideConditionsMet516 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR;//Natural: VALUE 10
        if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(10))))
        {
            decideConditionsMet516++;
            //*  OCCURS REC
            pnd_C_Rcrd_10.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-RCRD-10
            pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                                  //Natural: ASSIGN #WS-HEADER-BYTES ( * ) := #WS-WORK-REC-BYTES ( * )
            pnd_Ws_Current_Cntrct_Type_Cde.setValue(pnd_Ws_Header_Record_Cntrct_Type_Cde);                                                                                //Natural: ASSIGN #WS-CURRENT-CNTRCT-TYPE-CDE := #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
        }                                                                                                                                                                 //Natural: VALUE 20
        else if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(20))))
        {
            decideConditionsMet516++;
            pnd_C_Rcrd_20.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-RCRD-20
            pnd_I.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #I
            pnd_Ws_Occurs.getValue(pnd_I,"*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                                                       //Natural: ASSIGN #WS-OCCURS ( #I,* ) := #WS-WORK-REC-BYTES ( * )
            //*  NAME/ADDR REC
            pnd_Ws_Cntr_Inv_Acct.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #WS-CNTR-INV-ACCT
        }                                                                                                                                                                 //Natural: VALUE 30
        else if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(30))))
        {
            decideConditionsMet516++;
            pnd_C_Rcrd_30.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #C-RCRD-30
            pnd_K.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #K
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue(pnd_K,"*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));            //Natural: ASSIGN #WS-NAME-N-ADDRESS-BYTES ( #K,* ) := #WS-WORK-REC-BYTES ( * )
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Format_Document() throws Exception                                                                                                                   //Natural: FORMAT-DOCUMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  FORMAT THE DOCUMENT SEND TO THE ANNUITENT.
        //*  IF AN ADDRESS IN THE SECOND ADDRESS FIELDS, INDICATES THAT THE CHECK
        //*     IS MAILED TO ONE ADDRESS, AND A NOTIFICATION TO ANOTHER ADDRESS.
        //*     OTHERWISE, ONLY A CHECK IS PRINTED AND MAILED.
        //*  ----------------------------------------------------------------------
        //*  .........  ONLY ONE ADDRESS - ONLY PRINT CHECK
        //*  CHECKS
        short decideConditionsMet546 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1))))
        {
            decideConditionsMet546++;
            if (condition(ldaFcpl220.getFcp_Cons_Addr_View_Pymnt_Nme().getValue(2).equals(" ") && ldaFcpl220.getFcp_Cons_Addr_View_Pymnt_Addr_Line1_Txt().getValue(2).equals(" ")  //Natural: IF FCP-CONS-ADDR-VIEW.PYMNT-NME ( 2 ) = ' ' AND FCP-CONS-ADDR-VIEW.PYMNT-ADDR-LINE1-TXT ( 2 ) = ' ' AND FCP-CONS-ADDR-VIEW.PYMNT-ADDR-LINE2-TXT ( 2 ) = ' ' AND FCP-CONS-ADDR-VIEW.PYMNT-ADDR-LINE3-TXT ( 2 ) = ' ' AND FCP-CONS-ADDR-VIEW.PYMNT-ADDR-LINE4-TXT ( 2 ) = ' ' AND FCP-CONS-ADDR-VIEW.PYMNT-ADDR-LINE5-TXT ( 2 ) = ' ' AND FCP-CONS-ADDR-VIEW.PYMNT-ADDR-LINE6-TXT ( 2 ) = ' ' AND FCP-CONS-ADDR-VIEW.PYMNT-ADDR-LINE6-TXT ( 2 ) = ' '
                && ldaFcpl220.getFcp_Cons_Addr_View_Pymnt_Addr_Line2_Txt().getValue(2).equals(" ") && ldaFcpl220.getFcp_Cons_Addr_View_Pymnt_Addr_Line3_Txt().getValue(2).equals(" ") 
                && ldaFcpl220.getFcp_Cons_Addr_View_Pymnt_Addr_Line4_Txt().getValue(2).equals(" ") && ldaFcpl220.getFcp_Cons_Addr_View_Pymnt_Addr_Line5_Txt().getValue(2).equals(" ") 
                && ldaFcpl220.getFcp_Cons_Addr_View_Pymnt_Addr_Line6_Txt().getValue(2).equals(" ") && ldaFcpl220.getFcp_Cons_Addr_View_Pymnt_Addr_Line6_Txt().getValue(2).equals(" ")))
            {
                //*  C=CHECK
                pnd_Ws_Bottom_Part.setValue("C");                                                                                                                         //Natural: ASSIGN #WS-BOTTOM-PART = 'C'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
                sub_Format_Statement();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  .......... A SECOND ADDRESS - PRINT CHECK AND NOTIFICATION
                //*  S=SENT TO FORM
                pnd_Ws_Bottom_Part.setValue("S");                                                                                                                         //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
                sub_Format_Statement();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* *<
                pnd_Ws_Format_Notification_Letter.setValue("Y");                                                                                                          //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'Y'
                                                                                                                                                                          //Natural: PERFORM FORMAT-NOTIFICATION
                sub_Format_Notification();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
                sub_Format_Check();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Ws_Format_Notification_Letter.setValue("N");                                                                                                          //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'N'
                //*  EFT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(2))))
        {
            decideConditionsMet546++;
            //*  .... FOR EFT PRODUCE A STATEMENT WITH "send to" BUT NO NOTIFICATION
            //*  S="SENT TO" FORM
            pnd_Ws_Bottom_Part.setValue("S");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
            //*  GLOBAL PAY
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
            sub_Format_Statement();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(3))))
        {
            decideConditionsMet546++;
            //*  .... GLOBAL: PRODUCE A STATEMENT WITH "send to" BUT NO NOTIFICATION
            //*  S="SENT TO" FORM
            pnd_Ws_Bottom_Part.setValue("S");                                                                                                                             //Natural: ASSIGN #WS-BOTTOM-PART = 'S'
                                                                                                                                                                          //Natural: PERFORM FORMAT-STATEMENT
            sub_Format_Statement();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Abend_Cde.setValue(41);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 41
            getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Invalid data for","=",pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind); //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Invalid data for' '=' #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* * END-IF
        //*  FORMAT-DOCUMENT
    }
    private void sub_Format_Statement() throws Exception                                                                                                                  //Natural: FORMAT-STATEMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  1. SET UP THE XEROX "format" IN ACCORD WITH THE DOCUMNET
        //*     TO BE PRINTED.
        //*  ----------------------------------------------------------------------
        //*  ......SET UP THE XEROX "format" IN ACCORD WITH DOCUMNET & PROTOTYPE
        pnd_Ws_Feed.reset();                                                                                                                                              //Natural: RESET #WS-FEED
        //*  MATURITY,          PROTOTYPE #4
        short decideConditionsMet594 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-CURRENT-CNTRCT-TYPE-CDE;//Natural: VALUE '10','50'
        if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("10") || pnd_Ws_Current_Cntrct_Type_Cde.equals("50"))))
        {
            decideConditionsMet594++;
            //*  C=CHECK
            short decideConditionsMet597 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WS-BOTTOM-PART;//Natural: VALUE 'C'
            if (condition((pnd_Ws_Bottom_Part.equals("C"))))
            {
                decideConditionsMet597++;
                pnd_Ws_Format.setValue("INIT");                                                                                                                           //Natural: ASSIGN #WS-FORMAT = 'INIT'
                pnd_Ws_Forms.setValue("PRO2AA");                                                                                                                          //Natural: ASSIGN #WS-FORMS = 'PRO2AA'
                //*  SECURITY PAPER
                pnd_Ws_Feed.setValue("CHECKP");                                                                                                                           //Natural: ASSIGN #WS-FEED = 'CHECKP'
                pnd_Ws_Max.setValue(12);                                                                                                                                  //Natural: ASSIGN #WS-MAX = 12
                //*  S=SENT TO FORM
                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
                sub_Format_Xerox_Commands();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pnd_Ws_Bottom_Part.equals("S"))))
            {
                decideConditionsMet597++;
                pnd_Ws_Format.setValue("INITA");                                                                                                                          //Natural: ASSIGN #WS-FORMAT = 'INITA'
                pnd_Ws_Forms.setValue("PRO3A");                                                                                                                           //Natural: ASSIGN #WS-FORMS = 'PRO3A'
                //*  BOND PAPER
                pnd_Ws_Feed.setValue("BOND");                                                                                                                             //Natural: ASSIGN #WS-FEED = 'BOND'
                pnd_Ws_Max.setValue(9);                                                                                                                                   //Natural: ASSIGN #WS-MAX = 9
                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
                sub_Format_Xerox_Commands();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
                //*   TPA                ... PROTOTYPE#8
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE '20','30','31','40'
        else if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("20") || pnd_Ws_Current_Cntrct_Type_Cde.equals("30") || pnd_Ws_Current_Cntrct_Type_Cde.equals("31") 
            || pnd_Ws_Current_Cntrct_Type_Cde.equals("40"))))
        {
            decideConditionsMet594++;
            //*  C=CHECK
            short decideConditionsMet619 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WS-BOTTOM-PART;//Natural: VALUE 'C'
            if (condition((pnd_Ws_Bottom_Part.equals("C"))))
            {
                decideConditionsMet619++;
                pnd_Ws_Format.setValue("MONTH1");                                                                                                                         //Natural: ASSIGN #WS-FORMAT = 'MONTH1'
                pnd_Ws_Forms.setValue("PRO2AA");                                                                                                                          //Natural: ASSIGN #WS-FORMS = 'PRO2AA'
                //*  SECURITY PAPER
                pnd_Ws_Feed.setValue("CHECKP");                                                                                                                           //Natural: ASSIGN #WS-FEED = 'CHECKP'
                pnd_Ws_Max.setValue(14);                                                                                                                                  //Natural: ASSIGN #WS-MAX = 14
                //*  S=SENT TO FORM
                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
                sub_Format_Xerox_Commands();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: VALUE 'S'
            else if (condition((pnd_Ws_Bottom_Part.equals("S"))))
            {
                decideConditionsMet619++;
                pnd_Ws_Format.setValue("MONTHA");                                                                                                                         //Natural: ASSIGN #WS-FORMAT = 'MONTHA'
                pnd_Ws_Forms.setValue("PRO3A");                                                                                                                           //Natural: ASSIGN #WS-FORMS = 'PRO3A'
                //*  BOND PAPER
                pnd_Ws_Feed.setValue("BOND");                                                                                                                             //Natural: ASSIGN #WS-FEED = 'BOND'
                pnd_Ws_Max.setValue(9);                                                                                                                                   //Natural: ASSIGN #WS-MAX = 9
                                                                                                                                                                          //Natural: PERFORM FORMAT-XEROX-COMMANDS
                sub_Format_Xerox_Commands();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ........ FORMAT THE "header" PORTION OF THE DOCUMENT
        //*  "Header Formatting" PROGRAM
        DbsUtil.callnat(Fcpn738.class , getCurrentProcessState(), pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*"), pnd_Ws_Occurs.getValue("*","*"),                //Natural: CALLNAT 'FCPN738' #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*"), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        //*  ....... INDICATE - FIRST PAGE OF DOCUMENT
        pnd_Ws_Side_Printed.setValue("1");                                                                                                                                //Natural: ASSIGN #WS-SIDE-PRINTED = '1'
        //*  ........ FORMAT THE "Body" PORTION OF THE DOCUMENT
        //*  FORMAT BODY
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
        sub_Body_Of_Statement();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        //*  ........ FORMAT THE "Trailer" PORTION OF THE DOCUMENT
        //*  TRAILER
        //*  TMM PASELECT 05/00
        //*  TMM PASELECT 05/00
        //*  TMM PASELECT 05/00
        //*  TMM PASELECT 05/00
        DbsUtil.callnat(Fcpn782.class , getCurrentProcessState(), pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*"), pnd_Ws_Occurs.getValue("*","*"),                //Natural: CALLNAT 'FCPN782' #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* ) #WS-CNTR-INV-ACCT #WS-HEADER-RECORD.CNTRCT-ANNTY-INS-TYPE #WS-HEADER-RECORD.CNTRCT-ANNTY-TYPE-CDE #WS-HEADER-RECORD.CNTRCT-INSURANCE-OPTION #WS-HEADER-RECORD.CNTRCT-LIFE-CONTINGENCY
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*"), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Header_Record_Cntrct_Annty_Ins_Type, 
            pnd_Ws_Header_Record_Cntrct_Annty_Type_Cde, pnd_Ws_Header_Record_Cntrct_Insurance_Option, pnd_Ws_Header_Record_Cntrct_Life_Contingency);
        if (condition(Global.isEscape())) return;
        //*  ...... CHECK OR SENT TO NOTIFICATION - AS NEEDED
        //*  C=CHECK
        if (condition(pnd_Ws_Bottom_Part.equals("C")))                                                                                                                    //Natural: IF #WS-BOTTOM-PART = 'C'
        {
                                                                                                                                                                          //Natural: PERFORM FORMAT-CHECK
            sub_Format_Check();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  S=SENT TO FORM
            if (condition(pnd_Ws_Bottom_Part.equals("S")))                                                                                                                //Natural: IF #WS-BOTTOM-PART = 'S'
            {
                                                                                                                                                                          //Natural: PERFORM SENT-TO-FORM
                sub_Sent_To_Form();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ......... IF MORE THAN 1 PAGE - FORMAT THE NEXT's page Body
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("2") || pnd_Ws_Save_S_D_M_Plex.equals("3")))                                                                          //Natural: IF #WS-SAVE-S-D-M-PLEX = '2' OR = '3'
        {
            pnd_Ws_Side_Printed.setValue("2");                                                                                                                            //Natural: ASSIGN #WS-SIDE-PRINTED = '2'
            //*  FORMAT BODY
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
            sub_Body_Of_Statement();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("3")))                                                                                                                //Natural: IF #WS-SAVE-S-D-M-PLEX = '3'
        {
            pnd_Ws_Side_Printed.setValue("3");                                                                                                                            //Natural: ASSIGN #WS-SIDE-PRINTED = '3'
            if (condition(pnd_Ws_Side_Printed.equals("3")))                                                                                                               //Natural: IF #WS-SIDE-PRINTED = '3'
            {
                pnd_Ws_Rec_1.setValue(" ");                                                                                                                               //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                //*  MATURITY,          PROTOTYPE #4
                short decideConditionsMet681 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WS-CURRENT-CNTRCT-TYPE-CDE;//Natural: VALUE '10','50'
                if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("10") || pnd_Ws_Current_Cntrct_Type_Cde.equals("50"))))
                {
                    decideConditionsMet681++;
                    //*   TPA                ... PROTOTYPE#8
                    pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=INITB,FORMS=BLANK,FEED=BOND,END;");                                                                            //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=INITB,FORMS=BLANK,FEED=BOND,END;'
                }                                                                                                                                                         //Natural: VALUE '20','30','31','40'
                else if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("20") || pnd_Ws_Current_Cntrct_Type_Cde.equals("30") || pnd_Ws_Current_Cntrct_Type_Cde.equals("31") 
                    || pnd_Ws_Current_Cntrct_Type_Cde.equals("40"))))
                {
                    decideConditionsMet681++;
                    pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=DABAC,FORMS=BLANK,FEED=BOND,END;");                                                                            //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=DABAC,FORMS=BLANK,FEED=BOND,END;'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(" ");                                                                                                                               //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
            //*  FORMAT BODY
                                                                                                                                                                          //Natural: PERFORM BODY-OF-STATEMENT
            sub_Body_Of_Statement();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  FORMAT-STATEMENT
    }
    private void sub_Format_Xerox_Commands() throws Exception                                                                                                             //Natural: FORMAT-XEROX-COMMANDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        if (condition(pnd_Ws_Sim_Dup_Multiplex_Written.notEquals(pnd_Ws_Save_S_D_M_Plex)))                                                                                //Natural: IF #WS-SIM-DUP-MULTIPLEX-WRITTEN NE #WS-SAVE-S-D-M-PLEX
        {
            pnd_Ws_Sim_Dup_Multiplex_Written.setValue(pnd_Ws_Save_S_D_M_Plex);                                                                                            //Natural: ASSIGN #WS-SIM-DUP-MULTIPLEX-WRITTEN := #WS-SAVE-S-D-M-PLEX
            short decideConditionsMet706 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WS-SAVE-S-D-M-PLEX;//Natural: VALUE '1'
            if (condition((pnd_Ws_Save_S_D_M_Plex.equals("1"))))
            {
                decideConditionsMet706++;
                pnd_Ws_Jde.setValue("SIM");                                                                                                                               //Natural: ASSIGN #WS-JDE = 'SIM'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Ws_Save_S_D_M_Plex.equals("2"))))
            {
                decideConditionsMet706++;
                pnd_Ws_Jde.setValue("DUP");                                                                                                                               //Natural: ASSIGN #WS-JDE = 'DUP'
            }                                                                                                                                                             //Natural: ANY VALUE
            if (condition(decideConditionsMet706 > 0))
            {
                pnd_Ws_Rec_1.setValue(" ");                                                                                                                               //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, " $$XEROX JDE=CHK", pnd_Ws_Jde, ",JDL=CHECK,FORMAT=", pnd_Ws_Format,                //Natural: COMPRESS ' $$XEROX JDE=CHK' #WS-JDE ',JDL=CHECK,FORMAT=' #WS-FORMAT ',FORMS=' #WS-FORMS ',FEED=' #WS-FEED ',END;' INTO #WS-REC-1 LEAVING NO SPACE
                    ",FORMS=", pnd_Ws_Forms, ",FEED=", pnd_Ws_Feed, ",END;"));
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(" ");                                                                                                                               //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Initialize_Fields() throws Exception                                                                                                                 //Natural: INITIALIZE-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  INITIALIZE FIELDS FOR A NEW PAYMENT SEQUENCE NUMBER, AND NEW CHECK
        //*  ----------------------------------------------------------------------
        pnd_I.reset();                                                                                                                                                    //Natural: RESET #I #J #K #L #WS-CNTR-INV-ACCT #WS-BOTTOM-PART #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* )
        pnd_J.reset();
        pnd_K.reset();
        pnd_L.reset();
        pnd_Ws_Cntr_Inv_Acct.reset();
        pnd_Ws_Bottom_Part.reset();
        pnd_Ws_Sim_Dup_Multiplex_Written.reset();
        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").reset();
        pnd_Ws_Occurs.getValue("*","*").reset();
        pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*").reset();
        pnd_Ws_Current_Pymnt_Prcss_Seq_Num.setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num);                                                                   //Natural: ASSIGN #WS-CURRENT-PYMNT-PRCSS-SEQ-NUM := #WS-PYMNT-PRCSS-SEQ-NUM
        pnd_Ws_Save_S_D_M_Plex.setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex);                                                                          //Natural: ASSIGN #WS-SAVE-S-D-M-PLEX := #WS-WORK-REC-RECORD.#WS-SIMPLEX-DUPLEX-MULTIPLEX
    }
    private void sub_Format_Check() throws Exception                                                                                                                      //Natural: FORMAT-CHECK
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  ----------------------------------------------------------------------
        DbsUtil.callnat(Fcpn750.class , getCurrentProcessState(), pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*"), pnd_Ws_Occurs.getValue("*","*"),                //Natural: CALLNAT 'FCPN750' #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*"), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        //*  SIMPLEX
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("1")))                                                                                                                //Natural: IF #WS-SAVE-S-D-M-PLEX = '1'
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+3"));                                                                                 //Natural: COMPRESS '+3' INTO #WS-REC-1 LEAVING NO SPACE
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pnd_Ws_Format_Notification_Letter.equals("Y")))                                                                                                 //Natural: IF #WS-FORMAT-NOTIFICATION-LETTER = 'Y'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Rec_1.setValue(" ");                                                                                                                               //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                //*  MATURITY,          PROTOTYPE #4
                short decideConditionsMet752 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WS-CURRENT-CNTRCT-TYPE-CDE;//Natural: VALUE '10','50'
                if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("10") || pnd_Ws_Current_Cntrct_Type_Cde.equals("50"))))
                {
                    decideConditionsMet752++;
                    //*   TPA                ... PROTOTYPE#8
                    pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=INITB,FORMS=BLANK,END;");                                                                                      //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=INITB,FORMS=BLANK,END;'
                }                                                                                                                                                         //Natural: VALUE '20','30','31','40'
                else if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("20") || pnd_Ws_Current_Cntrct_Type_Cde.equals("30") || pnd_Ws_Current_Cntrct_Type_Cde.equals("31") 
                    || pnd_Ws_Current_Cntrct_Type_Cde.equals("40"))))
                {
                    decideConditionsMet752++;
                    pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=DABAC,FORMS=BLANK,END;");                                                                                      //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=DABAC,FORMS=BLANK,END;'
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
                pnd_Ws_Rec_1.setValue(" ");                                                                                                                               //Natural: ASSIGN #WS-REC-1 = ' '
                getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                             //Natural: WRITE WORK FILE 8 #WS-REC-1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Sent_To_Form() throws Exception                                                                                                                      //Natural: SENT-TO-FORM
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  ----------------------------------------------------------------------
        DbsUtil.callnat(Fcpn760.class , getCurrentProcessState(), pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*"), pnd_Ws_Occurs.getValue("*","*"),                //Natural: CALLNAT 'FCPN760' #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* )
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*"));
        if (condition(Global.isEscape())) return;
        //*  SIMPLEX
        if (condition(pnd_Ws_Save_S_D_M_Plex.equals("1")))                                                                                                                //Natural: IF #WS-SAVE-S-D-M-PLEX = '1'
        {
            pnd_Ws_Rec_1.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "+3"));                                                                                 //Natural: COMPRESS '+3' INTO #WS-REC-1 LEAVING NO SPACE
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Rec_1.setValue(" ");                                                                                                                                   //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            //*  MATURITY,          PROTOTYPE #4
            short decideConditionsMet784 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WS-CURRENT-CNTRCT-TYPE-CDE;//Natural: VALUE '10','50'
            if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("10") || pnd_Ws_Current_Cntrct_Type_Cde.equals("50"))))
            {
                decideConditionsMet784++;
                //*   TPA                ... PROTOTYPE#8
                pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=INITB,FORMS=BLANK,END;");                                                                                          //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=INITB,FORMS=BLANK,END;'
            }                                                                                                                                                             //Natural: VALUE '20','30','31','40'
            else if (condition((pnd_Ws_Current_Cntrct_Type_Cde.equals("20") || pnd_Ws_Current_Cntrct_Type_Cde.equals("30") || pnd_Ws_Current_Cntrct_Type_Cde.equals("31") 
                || pnd_Ws_Current_Cntrct_Type_Cde.equals("40"))))
            {
                decideConditionsMet784++;
                pnd_Ws_Rec_1.setValue(" $$XEROX FORMAT=DABAC,FORMS=BLANK,END;");                                                                                          //Natural: ASSIGN #WS-REC-1 = ' $$XEROX FORMAT=DABAC,FORMS=BLANK,END;'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
            pnd_Ws_Rec_1.setValue(" ");                                                                                                                                   //Natural: ASSIGN #WS-REC-1 = ' '
            getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                 //Natural: WRITE WORK FILE 8 #WS-REC-1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Format_Notification() throws Exception                                                                                                               //Natural: FORMAT-NOTIFICATION
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  ----------------------------------------------------------------------
        pnd_Ws_Format_Notification_Letter.setValue("Y");                                                                                                                  //Natural: ASSIGN #WS-FORMAT-NOTIFICATION-LETTER = 'Y'
        pnd_Ws_Rec_1.setValue(" ");                                                                                                                                       //Natural: ASSIGN #WS-REC-1 = ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Rec_1.setValue(" $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=LET,FORMS=PRO4A,FEED=CHECKP,END;");                                                                   //Natural: ASSIGN #WS-REC-1 = ' $$XEROX JDE=CHKSIM,JDL=CHECK,FORMAT=LET,FORMS=PRO4A,FEED=CHECKP,END;'
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        pnd_Ws_Rec_1.setValue(" ");                                                                                                                                       //Natural: ASSIGN #WS-REC-1 = ' '
        getWorkFiles().write(8, false, pnd_Ws_Rec_1);                                                                                                                     //Natural: WRITE WORK FILE 8 #WS-REC-1
        //*  ........ FORMAT THE "header" PORTION OF THE LETTER
        DbsUtil.callnat(Fcpn739.class , getCurrentProcessState(), pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*"), pnd_Ws_Occurs.getValue("*","*"),                //Natural: CALLNAT 'FCPN739' #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*"), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
        //*  ........ FORMAT THE
        DbsUtil.callnat(Fcpn732.class , getCurrentProcessState(), pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*"), pnd_Ws_Occurs.getValue("*","*"),                //Natural: CALLNAT 'FCPN732' #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*"), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Body_Of_Statement() throws Exception                                                                                                                 //Natural: BODY-OF-STATEMENT
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  ----------------------------------------------------------------------
        //*  FORMAT BODY
        DbsUtil.callnat(Fcpn745.class , getCurrentProcessState(), pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*"), pnd_Ws_Occurs.getValue("*","*"),                //Natural: CALLNAT 'FCPN745' #WS-HEADER-BYTES ( * ) #WS-OCCURS ( *,* ) #WS-NAME-N-ADDRESS-BYTES ( *,* ) #WS-CNTR-INV-ACCT #WS-SIM-DUP-MULTIPLEX-WRITTEN #WS-SIDE-PRINTED
            pnd_Ws_Name_N_Address_Record_Pnd_Ws_Name_N_Address_Bytes.getValue("*","*"), pnd_Ws_Cntr_Inv_Acct, pnd_Ws_Sim_Dup_Multiplex_Written, pnd_Ws_Side_Printed);
        if (condition(Global.isEscape())) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, ReportOption.NOTITLE,"***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, ReportOption.NOTITLE,NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new                    //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt0 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(0, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),"Page",getReports().getPageNumberDbs(0),  //Natural: WRITE NOTITLE *PROGRAM 41T #HEADER0-1 124T 'Page' *PAGE-NUMBER ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON");
    }
}
