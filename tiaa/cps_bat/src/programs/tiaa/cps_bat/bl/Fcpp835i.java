/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:40 PM
**        * FROM NATURAL PROGRAM : Fcpp835i
************************************************************
**        * FILE NAME            : Fcpp835i.java
**        * CLASS NAME           : Fcpp835i
**        * INSTANCE NAME        : Fcpp835i
************************************************************
************************************************************************
* PROGRAM  : FCPP835I
*
* SYSTEM   : CPS
* TITLE    : EXTRACT
*
* FUNCTION : IT WILL EXTRACT REDRAWN RECORDS AND WRITE TO S462 FLAT
*          : FILE.
* DATE     : 01/15/2020
* CHANGES  : CTS CPS SUNSET (CHG694525)
* HISTORY
* MODIFICATIONS :
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp835i extends BLNatBase
{
    // Data Areas
    private LdaFcplpmnu ldaFcplpmnu;
    private LdaFcplpmnt ldaFcplpmnt;
    private LdaFcpladdr ldaFcpladdr;
    private LdaFcplptax ldaFcplptax;
    private PdaFcpa800 pdaFcpa800;
    private LdaFcpls462 ldaFcpls462;
    private LdaFcplcrpt ldaFcplcrpt;
    private PdaFcpaacum pdaFcpaacum;
    private PdaFcpacrpt pdaFcpacrpt;
    private PdaFcpa0610 pdaFcpa0610;
    private LdaTxwl2425 ldaTxwl2425;
    private LdaIaal200a ldaIaal200a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Prime_Key_Start;
    private DbsField pnd_Ws_Prime_Key_End;

    private DbsGroup pnd_Addr_S;
    private DbsField pnd_Addr_S_Pnd_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Addr_S_Pnd_Cntrct_Payee_Cde;
    private DbsField pnd_Addr_S_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Addr_S_Pnd_Cntrct_Invrse_Dte;
    private DbsField pnd_Addr_S_Pnd_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Addr_S__R_Field_1;
    private DbsField pnd_Addr_S_Pnd_Addr_Superde_Start;
    private DbsField pnd_Addr_Superde_End;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst;

    private DbsGroup pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_2;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Invrse_Dte;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Orgn_Cde;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_3;
    private DbsField pnd_Key_Tin;
    private DbsField pnd_Key_Tin_Type;
    private DbsField pnd_Key_Active_Ind;
    private DbsField pnd_Valid_Orgn_Codes;
    private DbsField pnd_K;
    private DbsField pnd_I;
    private DbsField pnd_Index;
    private DbsField pnd_Isn;
    private DbsField pnd_W9_Valid;
    private DbsField pnd_W9_Us_Forced_Ind;
    private DbsField pnd_Roth_First_Contrib_Dte_A;
    private DbsField pnd_Read_Counter_Pymnt;
    private DbsField pnd_Read_Counter_Addr;
    private DbsField pnd_Read_Counter_Pymnt_Tax;
    private DbsField pnd_Totl_Counter_Pymnt;
    private DbsField pnd_Totl_Counter_Addr;
    private DbsField pnd_Totl_Counter_Pymnt_Tax;
    private DbsField pnd_Totl_Records_Updated;
    private DbsField pnd_Gtn_W9_Ben_Ind;
    private DbsField pnd_Gtn_Fed_Tax_Type;
    private DbsField pnd_Gtn_Fed_Tax_Amt;
    private DbsField pnd_Gtn_Fed_Canadian_Amt;
    private DbsField pnd_Gtn_Elec_Cde;
    private DbsField pnd_Gtn_Cntrct_Money_Source;
    private DbsField pnd_Gtn_Hardship_Cde;
    private DbsField pnd_Dist;
    private DbsField pnd_Pymnt_Deceased_Nme;
    private DbsField s462_Ret_Code;
    private DbsField s462_Ret_Msg;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcplpmnu = new LdaFcplpmnu();
        registerRecord(ldaFcplpmnu);
        registerRecord(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        ldaFcpladdr = new LdaFcpladdr();
        registerRecord(ldaFcpladdr);
        registerRecord(ldaFcpladdr.getVw_fcp_Cons_Addr());
        ldaFcplptax = new LdaFcplptax();
        registerRecord(ldaFcplptax);
        registerRecord(ldaFcplptax.getVw_pymnt_Tax());
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        ldaFcpls462 = new LdaFcpls462();
        registerRecord(ldaFcpls462);
        ldaFcplcrpt = new LdaFcplcrpt();
        registerRecord(ldaFcplcrpt);
        pdaFcpaacum = new PdaFcpaacum(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        pdaFcpa0610 = new PdaFcpa0610(localVariables);
        ldaTxwl2425 = new LdaTxwl2425();
        registerRecord(ldaTxwl2425);
        registerRecord(ldaTxwl2425.getVw_form_Receipt_View());
        ldaIaal200a = new LdaIaal200a();
        registerRecord(ldaIaal200a);
        registerRecord(ldaIaal200a.getVw_iaa_Cntrct());

        // Local Variables
        pnd_Ws_Prime_Key_Start = localVariables.newFieldInRecord("pnd_Ws_Prime_Key_Start", "#WS-PRIME-KEY-START", FieldType.STRING, 20);
        pnd_Ws_Prime_Key_End = localVariables.newFieldInRecord("pnd_Ws_Prime_Key_End", "#WS-PRIME-KEY-END", FieldType.STRING, 20);

        pnd_Addr_S = localVariables.newGroupInRecord("pnd_Addr_S", "#ADDR-S");
        pnd_Addr_S_Pnd_Cntrct_Ppcn_Nbr = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Pnd_Cntrct_Ppcn_Nbr", "#CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Addr_S_Pnd_Cntrct_Payee_Cde = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Pnd_Cntrct_Payee_Cde", "#CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Addr_S_Pnd_Cntrct_Orgn_Cde = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Addr_S_Pnd_Cntrct_Invrse_Dte = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Pnd_Cntrct_Invrse_Dte", "#CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Addr_S_Pnd_Pymnt_Prcss_Seq_Nbr = pnd_Addr_S.newFieldInGroup("pnd_Addr_S_Pnd_Pymnt_Prcss_Seq_Nbr", "#PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            7);

        pnd_Addr_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Addr_S__R_Field_1", "REDEFINE", pnd_Addr_S);
        pnd_Addr_S_Pnd_Addr_Superde_Start = pnd_Addr_S__R_Field_1.newFieldInGroup("pnd_Addr_S_Pnd_Addr_Superde_Start", "#ADDR-SUPERDE-START", FieldType.STRING, 
            31);
        pnd_Addr_Superde_End = localVariables.newFieldInRecord("pnd_Addr_Superde_End", "#ADDR-SUPERDE-END", FieldType.STRING, 31);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst = localVariables.newFieldInRecord("pnd_Cntrct_Inv_Orgn_Prcss_Inst", "#CNTRCT-INV-ORGN-PRCSS-INST", FieldType.STRING, 
            29);

        pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_2 = localVariables.newGroupInRecord("pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_2", "REDEFINE", pnd_Cntrct_Inv_Orgn_Prcss_Inst);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr = pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_2.newFieldInGroup("pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr", 
            "#X-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Invrse_Dte = pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_2.newFieldInGroup("pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Invrse_Dte", 
            "#X-CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Orgn_Cde = pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_2.newFieldInGroup("pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Orgn_Cde", 
            "#X-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Pymnt_Prcss_Seq_Nbr = pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_2.newFieldInGroup("pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Pymnt_Prcss_Seq_Nbr", 
            "#X-PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 38);

        pnd_Key__R_Field_3 = localVariables.newGroupInRecord("pnd_Key__R_Field_3", "REDEFINE", pnd_Key);
        pnd_Key_Tin = pnd_Key__R_Field_3.newFieldInGroup("pnd_Key_Tin", "TIN", FieldType.STRING, 10);
        pnd_Key_Tin_Type = pnd_Key__R_Field_3.newFieldInGroup("pnd_Key_Tin_Type", "TIN-TYPE", FieldType.STRING, 1);
        pnd_Key_Active_Ind = pnd_Key__R_Field_3.newFieldInGroup("pnd_Key_Active_Ind", "ACTIVE-IND", FieldType.STRING, 1);
        pnd_Valid_Orgn_Codes = localVariables.newFieldArrayInRecord("pnd_Valid_Orgn_Codes", "#VALID-ORGN-CODES", FieldType.STRING, 2, new DbsArrayController(1, 
            8));
        pnd_K = localVariables.newFieldInRecord("pnd_K", "#K", FieldType.INTEGER, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.PACKED_DECIMAL, 2);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_W9_Valid = localVariables.newFieldInRecord("pnd_W9_Valid", "#W9-VALID", FieldType.BOOLEAN, 1);
        pnd_W9_Us_Forced_Ind = localVariables.newFieldInRecord("pnd_W9_Us_Forced_Ind", "#W9-US-FORCED-IND", FieldType.BOOLEAN, 1);
        pnd_Roth_First_Contrib_Dte_A = localVariables.newFieldInRecord("pnd_Roth_First_Contrib_Dte_A", "#ROTH-FIRST-CONTRIB-DTE-A", FieldType.STRING, 
            8);
        pnd_Read_Counter_Pymnt = localVariables.newFieldInRecord("pnd_Read_Counter_Pymnt", "#READ-COUNTER-PYMNT", FieldType.NUMERIC, 9);
        pnd_Read_Counter_Addr = localVariables.newFieldInRecord("pnd_Read_Counter_Addr", "#READ-COUNTER-ADDR", FieldType.NUMERIC, 9);
        pnd_Read_Counter_Pymnt_Tax = localVariables.newFieldInRecord("pnd_Read_Counter_Pymnt_Tax", "#READ-COUNTER-PYMNT-TAX", FieldType.NUMERIC, 9);
        pnd_Totl_Counter_Pymnt = localVariables.newFieldInRecord("pnd_Totl_Counter_Pymnt", "#TOTL-COUNTER-PYMNT", FieldType.NUMERIC, 9);
        pnd_Totl_Counter_Addr = localVariables.newFieldInRecord("pnd_Totl_Counter_Addr", "#TOTL-COUNTER-ADDR", FieldType.NUMERIC, 9);
        pnd_Totl_Counter_Pymnt_Tax = localVariables.newFieldInRecord("pnd_Totl_Counter_Pymnt_Tax", "#TOTL-COUNTER-PYMNT-TAX", FieldType.NUMERIC, 9);
        pnd_Totl_Records_Updated = localVariables.newFieldInRecord("pnd_Totl_Records_Updated", "#TOTL-RECORDS-UPDATED", FieldType.NUMERIC, 9);
        pnd_Gtn_W9_Ben_Ind = localVariables.newFieldInRecord("pnd_Gtn_W9_Ben_Ind", "#GTN-W9-BEN-IND", FieldType.STRING, 1);
        pnd_Gtn_Fed_Tax_Type = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Tax_Type", "#GTN-FED-TAX-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            2));
        pnd_Gtn_Fed_Tax_Amt = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Tax_Amt", "#GTN-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            2));
        pnd_Gtn_Fed_Canadian_Amt = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Canadian_Amt", "#GTN-FED-CANADIAN-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 2));
        pnd_Gtn_Elec_Cde = localVariables.newFieldInRecord("pnd_Gtn_Elec_Cde", "#GTN-ELEC-CDE", FieldType.NUMERIC, 3);
        pnd_Gtn_Cntrct_Money_Source = localVariables.newFieldInRecord("pnd_Gtn_Cntrct_Money_Source", "#GTN-CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        pnd_Gtn_Hardship_Cde = localVariables.newFieldInRecord("pnd_Gtn_Hardship_Cde", "#GTN-HARDSHIP-CDE", FieldType.STRING, 1);
        pnd_Dist = localVariables.newFieldInRecord("pnd_Dist", "#DIST", FieldType.STRING, 2);
        pnd_Pymnt_Deceased_Nme = localVariables.newFieldInRecord("pnd_Pymnt_Deceased_Nme", "#PYMNT-DECEASED-NME", FieldType.STRING, 38);
        s462_Ret_Code = localVariables.newFieldInRecord("s462_Ret_Code", "S462-RET-CODE", FieldType.STRING, 4);
        s462_Ret_Msg = localVariables.newFieldInRecord("s462_Ret_Msg", "S462-RET-MSG", FieldType.STRING, 60);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmnu.initializeValues();
        ldaFcplpmnt.initializeValues();
        ldaFcpladdr.initializeValues();
        ldaFcplptax.initializeValues();
        ldaFcpls462.initializeValues();
        ldaFcplcrpt.initializeValues();
        ldaTxwl2425.initializeValues();
        ldaIaal200a.initializeValues();

        localVariables.reset();
        pnd_Ws_Prime_Key_Start.setInitialValue("H'0000C40000000000000000000000000000000000'");
        pnd_Ws_Prime_Key_End.setInitialValue("H'0000C4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF'");
        pnd_K.setInitialValue(0);
        pnd_Read_Counter_Pymnt.setInitialValue(0);
        pnd_Read_Counter_Addr.setInitialValue(0);
        pnd_Read_Counter_Pymnt_Tax.setInitialValue(0);
        pnd_Totl_Counter_Pymnt.setInitialValue(0);
        pnd_Totl_Counter_Addr.setInitialValue(0);
        pnd_Totl_Counter_Pymnt_Tax.setInitialValue(0);
        pnd_Totl_Records_Updated.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp835i() throws Exception
    {
        super("Fcpp835i");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP835I", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 52
        //*                                                                                                                                                               //Natural: ON ERROR
        //* ****************************************
        getReports().write(0, "*********************************");                                                                                                       //Natural: WRITE '*********************************'
        if (Global.isEscape()) return;
        getReports().write(0, "******* FCPP835I STARTING *******");                                                                                                       //Natural: WRITE '******* FCPP835I STARTING *******'
        if (Global.isEscape()) return;
        getReports().write(0, "*********************************");                                                                                                       //Natural: WRITE '*********************************'
        if (Global.isEscape()) return;
        //* ****************************************
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: ASSIGN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) := #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 ) := #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 ) := TRUE
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).setValue(true);
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT JUSTIFIED *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 119T 'PAGE :' *PAGE-NUMBER ( 15 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 38T 'ANNUITY PAYMENTS CANCEL AND REDRAW EXTRACT CONTROL REPORT' //
        FOR01:                                                                                                                                                            //Natural: FOR #K = 1 TO 8
        for (pnd_K.setValue(1); condition(pnd_K.lessOrEqual(8)); pnd_K.nadd(1))
        {
            short decideConditionsMet2056 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #K;//Natural: VALUE 1
            if (condition((pnd_K.equals(1))))
            {
                decideConditionsMet2056++;
                pnd_Valid_Orgn_Codes.getValue(pnd_K).setValue("AL");                                                                                                      //Natural: ASSIGN #VALID-ORGN-CODES ( #K ) := 'AL'
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((pnd_K.equals(2))))
            {
                decideConditionsMet2056++;
                pnd_Valid_Orgn_Codes.getValue(pnd_K).setValue("AP");                                                                                                      //Natural: ASSIGN #VALID-ORGN-CODES ( #K ) := 'AP'
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((pnd_K.equals(3))))
            {
                decideConditionsMet2056++;
                pnd_Valid_Orgn_Codes.getValue(pnd_K).setValue("DC");                                                                                                      //Natural: ASSIGN #VALID-ORGN-CODES ( #K ) := 'DC'
            }                                                                                                                                                             //Natural: VALUE 4
            else if (condition((pnd_K.equals(4))))
            {
                decideConditionsMet2056++;
                pnd_Valid_Orgn_Codes.getValue(pnd_K).setValue("EW");                                                                                                      //Natural: ASSIGN #VALID-ORGN-CODES ( #K ) := 'EW'
            }                                                                                                                                                             //Natural: VALUE 5
            else if (condition((pnd_K.equals(5))))
            {
                decideConditionsMet2056++;
                pnd_Valid_Orgn_Codes.getValue(pnd_K).setValue("IA");                                                                                                      //Natural: ASSIGN #VALID-ORGN-CODES ( #K ) := 'IA'
            }                                                                                                                                                             //Natural: VALUE 6
            else if (condition((pnd_K.equals(6))))
            {
                decideConditionsMet2056++;
                pnd_Valid_Orgn_Codes.getValue(pnd_K).setValue("MS");                                                                                                      //Natural: ASSIGN #VALID-ORGN-CODES ( #K ) := 'MS'
            }                                                                                                                                                             //Natural: VALUE 7
            else if (condition((pnd_K.equals(7))))
            {
                decideConditionsMet2056++;
                pnd_Valid_Orgn_Codes.getValue(pnd_K).setValue("NZ");                                                                                                      //Natural: ASSIGN #VALID-ORGN-CODES ( #K ) := 'NZ'
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((pnd_K.equals(8))))
            {
                decideConditionsMet2056++;
                pnd_Valid_Orgn_Codes.getValue(pnd_K).setValue("SS");                                                                                                      //Natural: ASSIGN #VALID-ORGN-CODES ( #K ) := 'SS'
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet2056 > 0))
            {
                pnd_Totl_Counter_Pymnt.nadd(pnd_Read_Counter_Pymnt);                                                                                                      //Natural: ADD #READ-COUNTER-PYMNT TO #TOTL-COUNTER-PYMNT
                pnd_Totl_Counter_Addr.nadd(pnd_Read_Counter_Addr);                                                                                                        //Natural: ADD #READ-COUNTER-ADDR TO #TOTL-COUNTER-ADDR
                pnd_Totl_Counter_Pymnt_Tax.nadd(pnd_Read_Counter_Pymnt_Tax);                                                                                              //Natural: ADD #READ-COUNTER-PYMNT-TAX TO #TOTL-COUNTER-PYMNT-TAX
                pnd_Read_Counter_Pymnt.reset();                                                                                                                           //Natural: RESET #READ-COUNTER-PYMNT
                pnd_Read_Counter_Addr.reset();                                                                                                                            //Natural: RESET #READ-COUNTER-ADDR
                pnd_Read_Counter_Pymnt_Tax.reset();                                                                                                                       //Natural: RESET #READ-COUNTER-PYMNT-TAX
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
            setValueToSubstring(pnd_Valid_Orgn_Codes.getValue(pnd_K),pnd_Ws_Prime_Key_Start,1,2);                                                                         //Natural: MOVE #VALID-ORGN-CODES ( #K ) TO SUBSTRING ( #WS-PRIME-KEY-START,1,2 )
            setValueToSubstring(pnd_Valid_Orgn_Codes.getValue(pnd_K),pnd_Ws_Prime_Key_End,1,2);                                                                           //Natural: MOVE #VALID-ORGN-CODES ( #K ) TO SUBSTRING ( #WS-PRIME-KEY-END,1,2 )
            ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                          //Natural: READ FCP-CONS-PYMNT BY ORGN-STATUS-INVRSE-SEQ = #WS-PRIME-KEY-START THRU #WS-PRIME-KEY-END
            (
            "READ_CSR",
            new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Prime_Key_Start, "And", WcType.BY) ,
            new Wc("ORGN_STATUS_INVRSE_SEQ", "<=", pnd_Ws_Prime_Key_End, WcType.BY) },
            new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
            );
            READ_CSR:
            while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("READ_CSR")))
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Stats_Cde().notEquals("D")))                                                                            //Natural: IF FCP-CONS-PYMNT.PYMNT-STATS-CDE NE 'D'
                {
                    if (true) break READ_CSR;                                                                                                                             //Natural: ESCAPE BOTTOM ( READ-CSR. )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("R")))                                                          //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE 'R'
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Isn.setValue(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstISN("READ_CSR"));                                                                       //Natural: ASSIGN #ISN := *ISN ( READ-CSR. )
                        pnd_Read_Counter_Pymnt.nadd(1);                                                                                                                   //Natural: ASSIGN #READ-COUNTER-PYMNT := #READ-COUNTER-PYMNT + 1
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-ADDRESS-RECORD
                        sub_Retrieve_Address_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_CSR"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_CSR"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM RETRIEVE-PYMNT-TAX-RECORD
                        sub_Retrieve_Pymnt_Tax_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_CSR"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_CSR"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM CREATE-S462-RECORD
                        sub_Create_S462_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_CSR"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_CSR"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                   //Natural: ASSIGN #FCPAACUM.#NEW-PYMNT-IND := TRUE
                        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R")))                                                         //Natural: IF FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
                        {
                            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(21);                                                                                   //Natural: ASSIGN #ACCUM-OCCUR := 21
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                            sub_Accum_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom("READ_CSR"))) break;
                                else if (condition(Global.isEscapeBottomImmediate("READ_CSR"))) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Amt().equals(new DbsDecimal("0.00"))))                                                    //Natural: IF FCP-CONS-PYMNT.PYMNT-CHECK-AMT = 0.00
                        {
                            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(15);                                                                                   //Natural: ASSIGN #ACCUM-OCCUR := 15
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(1);                                                                                    //Natural: ASSIGN #ACCUM-OCCUR := 1
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                        sub_Accum_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_CSR"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_CSR"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                                                                                                                                                                          //Natural: PERFORM UPDATE-PYMNT-RECORD
                        sub_Update_Pymnt_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("READ_CSR"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("READ_CSR"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* ****************************************
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETRIEVE-ADDRESS-RECORD
        //* ****************************************
        //* ******************************************
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RETRIEVE-PYMNT-TAX-RECORD
        //* ******************************************
        //* ******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-S462-RECORD
        //* ****************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-PYMNT-RECORD
        //* ****************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-DIST-CODE
        //* ****************************************
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-W9-BEN-IND
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* ****************************************
        //* *************************************
        //* ****************************************
        //* ***********************************************************************
        //*  COPYCODE   : FCPCACUM
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : "AP" - CONTROL ACCUMULATIONS.
        //* ***********************************************************************
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-TOTALS
        getReports().write(0, "*********************************");                                                                                                       //Natural: WRITE '*********************************'
        if (Global.isEscape()) return;
        getReports().write(0, "****** FCPP835I COMPLETING ******");                                                                                                       //Natural: WRITE '****** FCPP835I COMPLETING ******'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL PYMNT DB READ    : ",pnd_Totl_Counter_Pymnt);                                                                                        //Natural: WRITE 'TOTAL PYMNT DB READ    : ' #TOTL-COUNTER-PYMNT
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL ADDR  DB READ    : ",pnd_Totl_Counter_Addr);                                                                                         //Natural: WRITE 'TOTAL ADDR  DB READ    : ' #TOTL-COUNTER-ADDR
        if (Global.isEscape()) return;
        getReports().write(0, "TOTAL PYMNT TAX DB READ: ",pnd_Totl_Counter_Pymnt_Tax);                                                                                    //Natural: WRITE 'TOTAL PYMNT TAX DB READ: ' #TOTL-COUNTER-PYMNT-TAX
        if (Global.isEscape()) return;
        getReports().write(0, "*********************************");                                                                                                       //Natural: WRITE '*********************************'
        if (Global.isEscape()) return;
        getReports().write(0, "TOTL RECORDS UPDATED:",pnd_Totl_Records_Updated);                                                                                          //Natural: WRITE 'TOTL RECORDS UPDATED:' #TOTL-RECORDS-UPDATED
        if (Global.isEscape()) return;
        getReports().write(0, "*********************************");                                                                                                       //Natural: WRITE '*********************************'
        if (Global.isEscape()) return;
        getReports().write(0, "****** END OF RUN FCPP835I ******");                                                                                                       //Natural: WRITE '****** END OF RUN FCPP835I ******'
        if (Global.isEscape()) return;
        getReports().write(0, "*********************************");                                                                                                       //Natural: WRITE '*********************************'
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* ****************************************
    }
    private void sub_Retrieve_Address_Record() throws Exception                                                                                                           //Natural: RETRIEVE-ADDRESS-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Addr_S_Pnd_Cntrct_Ppcn_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                         //Natural: ASSIGN #ADDR-S.#CNTRCT-PPCN-NBR := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        pnd_Addr_S_Pnd_Cntrct_Payee_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                                       //Natural: ASSIGN #ADDR-S.#CNTRCT-PAYEE-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        pnd_Addr_S_Pnd_Cntrct_Orgn_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                         //Natural: ASSIGN #ADDR-S.#CNTRCT-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        pnd_Addr_S_Pnd_Cntrct_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                                     //Natural: ASSIGN #ADDR-S.#CNTRCT-INVRSE-DTE := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
        pnd_Addr_S_Pnd_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                                 //Natural: ASSIGN #ADDR-S.#PYMNT-PRCSS-SEQ-NBR := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("AP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("IA")))                      //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'AP' OR = 'IA'
        {
            pnd_Addr_S_Pnd_Cntrct_Ppcn_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr().getSubstring(1,10));                                                  //Natural: MOVE SUBSTRING ( FCP-CONS-PYMNT.CNTRCT-CMBN-NBR,1,10 ) TO #ADDR-S.#CNTRCT-PPCN-NBR
            pnd_Addr_S_Pnd_Cntrct_Payee_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr().getSubstring(11,4));                                                 //Natural: MOVE SUBSTRING ( FCP-CONS-PYMNT.CNTRCT-CMBN-NBR,11,4 ) TO #ADDR-S.#CNTRCT-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Addr_Superde_End.setValue(pnd_Addr_S_Pnd_Addr_Superde_Start);                                                                                                 //Natural: MOVE #ADDR-SUPERDE-START TO #ADDR-SUPERDE-END
        setValueToSubstring("H'FF'",pnd_Addr_Superde_End,17,1);                                                                                                           //Natural: MOVE H'FF' TO SUBSTRING ( #ADDR-SUPERDE-END,17,1 )
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("AP") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde().equals("IA")))                      //Natural: IF FCP-CONS-PYMNT.CNTRCT-ORGN-CDE = 'AP' OR = 'IA'
        {
            setValueToSubstring("H'00'",pnd_Addr_S_Pnd_Addr_Superde_Start,17,1);                                                                                          //Natural: MOVE H'00' TO SUBSTRING ( #ADDR-SUPERDE-START,17,1 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpladdr.getVw_fcp_Cons_Addr().startDatabaseRead                                                                                                               //Natural: READ ( 1 ) FCP-CONS-ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ = #ADDR-SUPERDE-START THRU #ADDR-SUPERDE-END
        (
        "READ01",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Addr_S_Pnd_Addr_Superde_Start, "And", WcType.BY) ,
        new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", "<=", pnd_Addr_Superde_End, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        READ01:
        while (condition(ldaFcpladdr.getVw_fcp_Cons_Addr().readNextRow("READ01")))
        {
            pnd_Read_Counter_Addr.nadd(1);                                                                                                                                //Natural: ASSIGN #READ-COUNTER-ADDR := #READ-COUNTER-ADDR + 1
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  RETRIEVE-ADDRESS-RECORD
    }
    private void sub_Retrieve_Pymnt_Tax_Record() throws Exception                                                                                                         //Natural: RETRIEVE-PYMNT-TAX-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                        //Natural: ASSIGN #X-CNTRCT-NBR := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Invrse_Dte.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                               //Natural: ASSIGN #X-CNTRCT-INVRSE-DTE := FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Orgn_Cde.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                   //Natural: ASSIGN #X-CNTRCT-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Pymnt_Prcss_Seq_Nbr.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                           //Natural: ASSIGN #X-PYMNT-PRCSS-SEQ-NBR := FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM
        ldaFcplptax.getVw_pymnt_Tax().startDatabaseRead                                                                                                                   //Natural: READ ( 1 ) PYMNT-TAX BY CNTRCT-INV-ORGN-PRCSS-INST = #CNTRCT-INV-ORGN-PRCSS-INST
        (
        "READ02",
        new Wc[] { new Wc("CNTRCT_INV_ORGN_PRCSS_INST", ">=", pnd_Cntrct_Inv_Orgn_Prcss_Inst, WcType.BY) },
        new Oc[] { new Oc("CNTRCT_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        READ02:
        while (condition(ldaFcplptax.getVw_pymnt_Tax().readNextRow("READ02")))
        {
            pnd_Read_Counter_Pymnt_Tax.nadd(1);                                                                                                                           //Natural: ASSIGN #READ-COUNTER-PYMNT-TAX := #READ-COUNTER-PYMNT-TAX + 1
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  RETRIEVE-PYMNT-TAX-RECORD
    }
    private void sub_Create_S462_Record() throws Exception                                                                                                                //Natural: CREATE-S462-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************************
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().reset();                                                                                                      //Natural: RESET WF-PYMNT-ADDR-GRP.WF-PYMNT-ADDR-REC #GTN-W9-BEN-IND #GTN-FED-TAX-TYPE ( * ) #GTN-FED-TAX-AMT ( * ) #GTN-FED-CANADIAN-AMT ( * ) #GTN-ELEC-CDE #GTN-CNTRCT-MONEY-SOURCE #GTN-HARDSHIP-CDE #DIST #PYMNT-DECEASED-NME
        pnd_Gtn_W9_Ben_Ind.reset();
        pnd_Gtn_Fed_Tax_Type.getValue("*").reset();
        pnd_Gtn_Fed_Tax_Amt.getValue("*").reset();
        pnd_Gtn_Fed_Canadian_Amt.getValue("*").reset();
        pnd_Gtn_Elec_Cde.reset();
        pnd_Gtn_Cntrct_Money_Source.reset();
        pnd_Gtn_Hardship_Cde.reset();
        pnd_Dist.reset();
        pnd_Pymnt_Deceased_Nme.reset();
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().setValuesByName(ldaFcpladdr.getVw_fcp_Cons_Addr());                                                           //Natural: MOVE BY NAME FCP-CONS-ADDR TO WF-PYMNT-ADDR-GRP.WF-PYMNT-ADDR-REC
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                          //Natural: MOVE BY NAME FCP-CONS-PYMNT TO WF-PYMNT-ADDR-GRP.WF-PYMNT-ADDR-REC
        FOR02:                                                                                                                                                            //Natural: FOR #INDEX 1 2
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(2)); pnd_Index.nadd(1))
        {
            if (condition(DbsUtil.maskMatches(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Addr_Zip_Cde().getValue(pnd_Index),"NNNNN....")))                                        //Natural: IF FCP-CONS-ADDR.PYMNT-ADDR-ZIP-CDE ( #INDEX ) = MASK ( NNNNN.... )
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde().getValue(pnd_Index).setValue("N");                                                                    //Natural: MOVE 'N' TO WF-PYMNT-ADDR-GRP.PYMNT-FOREIGN-CDE ( #INDEX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde().getValue(pnd_Index).setValue("Y");                                                                    //Natural: MOVE 'Y' TO WF-PYMNT-ADDR-GRP.PYMNT-FOREIGN-CDE ( #INDEX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR03:                                                                                                                                                            //Natural: FOR #INDEX 1 10
        for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(10)); pnd_Index.nadd(1))
        {
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Ded_Cde().getValue(pnd_Index).equals(5)))                                                                   //Natural: IF FCP-CONS-PYMNT.PYMNT-DED-CDE ( #INDEX ) = 005
            {
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Payee_Cde().getValue(pnd_Index).setValue("01940   ");                                                           //Natural: MOVE '01940   ' TO WF-PYMNT-ADDR-GRP.PYMNT-DED-PAYEE-CDE ( #INDEX )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                    //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT := FCP-CONS-PYMNT.C*INV-ACCT
        pdaFcpa800.getWf_Pymnt_Addr_Grp_C_Pymnt_Ded_Grp().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                              //Natural: ASSIGN WF-PYMNT-ADDR-GRP.C-PYMNT-DED-GRP := FCP-CONS-PYMNT.C*PYMNT-DED-GRP
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue("*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue("*"));                          //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( * ) := FCP-CONS-PYMNT.INV-ACCT-CDE ( * )
        if (condition(((ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Annty_Ins_Type().equals("S") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Annty_Ins_Type().equals("M"))           //Natural: IF FCP-CONS-PYMNT.CNTRCT-ANNTY-INS-TYPE = 'S' OR = 'M' AND FCP-CONS-PYMNT.CNTRCT-ANNTY-TYPE-CDE = 'D' OR = 'M'
            && (ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Annty_Type_Cde().equals("D") || ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Annty_Type_Cde().equals("M")))))
        {
            FOR04:                                                                                                                                                        //Natural: FOR #INDEX 1 WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
            for (pnd_Index.setValue(1); condition(pnd_Index.lessOrEqual(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count())); pnd_Index.nadd(1))
            {
                if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Cde().getValue(pnd_Index).equals("T ")))                                                             //Natural: IF FCP-CONS-PYMNT.INV-ACCT-CDE ( #INDEX ) = 'T '
                {
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde().getValue(pnd_Index).setValue("40");                                                                    //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CDE ( #INDEX ) := '40'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue("*").setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct_Settl_Amt().getValue("*"));                   //Natural: ASSIGN WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( * ) := FCP-CONS-PYMNT.INV-ACCT-SETTL-AMT ( * )
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue("*").compute(new ComputeParameters(false, pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue("*")),  //Natural: COMPUTE WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( * ) = WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( * ) + WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( * )
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue("*").add(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue("*")));
        pnd_Gtn_Fed_Tax_Type.getValue(1).setValue(ldaFcplptax.getPymnt_Tax_Tax_Fed_Tax_Type());                                                                           //Natural: ASSIGN #GTN-FED-TAX-TYPE ( 1 ) := PYMNT-TAX.TAX-FED-TAX-TYPE
        pnd_Gtn_Fed_Tax_Amt.getValue(1).setValue(ldaFcplptax.getPymnt_Tax_Tax_Fed_Tax_Amt());                                                                             //Natural: ASSIGN #GTN-FED-TAX-AMT ( 1 ) := PYMNT-TAX.TAX-FED-TAX-AMT
        pnd_Gtn_Fed_Canadian_Amt.getValue(1).setValue(ldaFcplptax.getPymnt_Tax_Tax_Fed_Canadian_Amt());                                                                   //Natural: ASSIGN #GTN-FED-CANADIAN-AMT ( 1 ) := PYMNT-TAX.TAX-FED-CANADIAN-AMT
        pnd_Gtn_Elec_Cde.setValue(ldaFcplptax.getPymnt_Tax_Tax_Elec_Cde());                                                                                               //Natural: ASSIGN #GTN-ELEC-CDE := PYMNT-TAX.TAX-ELEC-CDE
        pnd_Gtn_Cntrct_Money_Source.setValue(ldaFcplptax.getPymnt_Tax_Cntrct_Money_Source());                                                                             //Natural: ASSIGN #GTN-CNTRCT-MONEY-SOURCE := PYMNT-TAX.CNTRCT-MONEY-SOURCE
        pnd_Gtn_Hardship_Cde.setValue(ldaFcplptax.getPymnt_Tax_Hardship_Cde());                                                                                           //Natural: ASSIGN #GTN-HARDSHIP-CDE := PYMNT-TAX.HARDSHIP-CDE
                                                                                                                                                                          //Natural: PERFORM GET-W9-BEN-IND
        sub_Get_W9_Ben_Ind();
        if (condition(Global.isEscape())) {return;}
        if (condition(ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde().equals("DC")))                                                                                       //Natural: IF FCP-CONS-ADDR.CNTRCT-ORGN-CDE = 'DC'
        {
            pnd_Pymnt_Deceased_Nme.setValue(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Deceased_Nme());                                                                           //Natural: ASSIGN #PYMNT-DECEASED-NME := FCP-CONS-ADDR.PYMNT-DECEASED-NME
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pymnt_Deceased_Nme.reset();                                                                                                                               //Natural: RESET #PYMNT-DECEASED-NME
        }                                                                                                                                                                 //Natural: END-IF
        ldaIaal200a.getVw_iaa_Cntrct().startDatabaseFind                                                                                                                  //Natural: FIND ( 1 ) IAA-CNTRCT WITH IAA-CNTRCT.CNTRCT-PPCN-NBR = FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        (
        "FIND01",
        new Wc[] { new Wc("CNTRCT_PPCN_NBR", "=", ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr(), WcType.WITH) },
        1
        );
        FIND01:
        while (condition(ldaIaal200a.getVw_iaa_Cntrct().readNextRow("FIND01", true)))
        {
            ldaIaal200a.getVw_iaa_Cntrct().setIfNotFoundControlFlag(false);
            if (condition(ldaIaal200a.getVw_iaa_Cntrct().getAstCOUNTER().equals(0)))                                                                                      //Natural: IF NO RECORD FOUND
            {
                getReports().write(0, "NO CONTRACT FOUND FOR ",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                          //Natural: WRITE 'NO CONTRACT FOUND FOR ' FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-NOREC
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number().setValue(ldaIaal200a.getIaa_Cntrct_Plan_Nmbr());                                                                //Natural: ASSIGN WF-PYMNT-ADDR-GRP.PLAN-NUMBER := IAA-CNTRCT.PLAN-NMBR
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan().setValue(ldaIaal200a.getIaa_Cntrct_Sub_Plan_Nmbr());                                                               //Natural: ASSIGN WF-PYMNT-ADDR-GRP.SUB-PLAN := IAA-CNTRCT.SUB-PLAN-NMBR
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr().setValue(ldaIaal200a.getIaa_Cntrct_Cntrct_Orig_Da_Cntrct_Nbr());                                            //Natural: ASSIGN WF-PYMNT-ADDR-GRP.ORIG-CNTRCT-NBR := IAA-CNTRCT.CNTRCT-ORIG-DA-CNTRCT-NBR
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan().setValue(ldaIaal200a.getIaa_Cntrct_Orgntng_Sub_Plan_Nmbr());                                                  //Natural: ASSIGN WF-PYMNT-ADDR-GRP.ORIG-SUB-PLAN := IAA-CNTRCT.ORGNTNG-SUB-PLAN-NMBR
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        if (condition(ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde().equals("DC") || ldaFcpladdr.getFcp_Cons_Addr_Cntrct_Orgn_Cde().equals("NZ")))                        //Natural: IF FCP-CONS-ADDR.CNTRCT-ORGN-CDE = 'DC' OR = 'NZ'
        {
                                                                                                                                                                          //Natural: PERFORM GET-DIST-CODE
            sub_Get_Dist_Code();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpns46b.class , getCurrentProcessState(), pdaFcpa800.getWf_Pymnt_Addr_Grp().getValue("*"), pnd_Gtn_W9_Ben_Ind, pnd_Gtn_Fed_Tax_Type.getValue("*"),  //Natural: CALLNAT 'FCPNS46B' USING WF-PYMNT-ADDR-GRP ( * ) #GTN-W9-BEN-IND #GTN-FED-TAX-TYPE ( * ) #GTN-FED-TAX-AMT ( * ) #GTN-FED-CANADIAN-AMT ( * ) #GTN-ELEC-CDE #GTN-CNTRCT-MONEY-SOURCE #GTN-HARDSHIP-CDE #DIST #PYMNT-DECEASED-NME S462-RET-CODE S462-RET-MSG
            pnd_Gtn_Fed_Tax_Amt.getValue("*"), pnd_Gtn_Fed_Canadian_Amt.getValue("*"), pnd_Gtn_Elec_Cde, pnd_Gtn_Cntrct_Money_Source, pnd_Gtn_Hardship_Cde, 
            pnd_Dist, pnd_Pymnt_Deceased_Nme, s462_Ret_Code, s462_Ret_Msg);
        if (condition(Global.isEscape())) return;
        if (condition(s462_Ret_Code.notEquals("    ")))                                                                                                                   //Natural: IF S462-RET-CODE NE '    '
        {
            getReports().write(0, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");                                                                                              //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            if (Global.isEscape()) return;
            getReports().write(0, "!!!!!! RECORD FAILED TO CONVERT !!!!!!");                                                                                              //Natural: WRITE '!!!!!! RECORD FAILED TO CONVERT !!!!!!'
            if (Global.isEscape()) return;
            getReports().write(0, "!!!!!!!!! FCPNS46B RC: ",s462_Ret_Code," !!!!!!!!");                                                                                   //Natural: WRITE '!!!!!!!!! FCPNS46B RC: ' S462-RET-CODE ' !!!!!!!!'
            if (Global.isEscape()) return;
            getReports().write(0, "CNTRCT-PPCN-NBR: ",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                   //Natural: WRITE 'CNTRCT-PPCN-NBR: ' FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
            if (Global.isEscape()) return;
            getReports().write(0, "CNTRCT-ORGN-CDE: ",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                   //Natural: WRITE 'CNTRCT-ORGN-CDE: ' FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
            if (Global.isEscape()) return;
            getReports().write(0, "PYMNT-STATS-CDE: ",ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Stats_Cde());                                                                   //Natural: WRITE 'PYMNT-STATS-CDE: ' FCP-CONS-PYMNT.PYMNT-STATS-CDE
            if (Global.isEscape()) return;
            getReports().write(0, "ACTVTY-CDE     : ",ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde());                                                     //Natural: WRITE 'ACTVTY-CDE     : ' FCP-CONS-PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            if (Global.isEscape()) return;
            getReports().write(0, "!!!!! JOB WILL ABEND WITH RC: 55 !!!!!");                                                                                              //Natural: WRITE '!!!!! JOB WILL ABEND WITH RC: 55 !!!!!'
            if (Global.isEscape()) return;
            getReports().write(0, "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");                                                                                              //Natural: WRITE '!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!'
            if (Global.isEscape()) return;
            getCurrentProcessState().getDbConv().dbRollback();                                                                                                            //Natural: BACKOUT TRANSACTION
            getReports().write(0, "ALL UPDATES BACKED OUT!!");                                                                                                            //Natural: WRITE 'ALL UPDATES BACKED OUT!!'
            if (Global.isEscape()) return;
            DbsUtil.terminate(55);  if (true) return;                                                                                                                     //Natural: TERMINATE 55
        }                                                                                                                                                                 //Natural: END-IF
        //*  CREATE-S462-RECORD
    }
    private void sub_Update_Pymnt_Record() throws Exception                                                                                                               //Natural: UPDATE-PYMNT-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        //*  LOCATE PAYMENT DATA BY GET FUNCTION TO MODIFY */
        GET_FOR_MODIFY:                                                                                                                                                   //Natural: GET FCP-CONS-PYMNU #ISN
        ldaFcplpmnu.getVw_fcp_Cons_Pymnu().readByID(pnd_Isn.getLong(), "GET_FOR_MODIFY");
        ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Stats_Cde().setValue("P");                                                                                                    //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-STATS-CDE := 'P'
        ldaFcplpmnu.getVw_fcp_Cons_Pymnu().updateDBRow("GET_FOR_MODIFY");                                                                                                 //Natural: UPDATE ( GET-FOR-MODIFY. )
        pnd_Totl_Records_Updated.nadd(1);                                                                                                                                 //Natural: ASSIGN #TOTL-RECORDS-UPDATED := #TOTL-RECORDS-UPDATED + 1
        //*  UPDATE-PYMNT-RECORD
    }
    private void sub_Get_Dist_Code() throws Exception                                                                                                                     //Natural: GET-DIST-CODE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Pymnt_Type_Ind().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind());                                              //Natural: ASSIGN #EOM-EXT1.CNTRCT-PYMNT-TYPE-IND := FCP-CONS-PYMNT.CNTRCT-PYMNT-TYPE-IND
        pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Sttlmnt_Type_Ind().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind());                                          //Natural: ASSIGN #EOM-EXT1.CNTRCT-STTLMNT-TYPE-IND := FCP-CONS-PYMNT.CNTRCT-STTLMNT-TYPE-IND
        pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Payee_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                        //Natural: ASSIGN #EOM-EXT1.CNTRCT-PAYEE-CDE := FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE
        pdaFcpa0610.getPnd_Eom_Ext1_Pymnt_Dob().setValue(ldaFcpladdr.getFcp_Cons_Addr_Pymnt_Dob());                                                                       //Natural: ASSIGN #EOM-EXT1.PYMNT-DOB := FCP-CONS-ADDR.PYMNT-DOB
        pdaFcpa0610.getPnd_Eom_Ext1_Pymnt_Check_Dte().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Dte());                                                          //Natural: ASSIGN #EOM-EXT1.PYMNT-CHECK-DTE := FCP-CONS-PYMNT.PYMNT-CHECK-DTE
        pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Roll_Dest_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Roll_Dest_Cde());                                                //Natural: ASSIGN #EOM-EXT1.CNTRCT-ROLL-DEST-CDE := FCP-CONS-PYMNT.CNTRCT-ROLL-DEST-CDE
        pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Ppcn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                          //Natural: ASSIGN #EOM-EXT1.CNTRCT-PPCN-NBR := FCP-CONS-PYMNT.CNTRCT-PPCN-NBR
        pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Option_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Option_Cde());                                                      //Natural: ASSIGN #EOM-EXT1.CNTRCT-OPTION-CDE := FCP-CONS-PYMNT.CNTRCT-OPTION-CDE
        pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Orgn_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                          //Natural: ASSIGN #EOM-EXT1.CNTRCT-ORGN-CDE := FCP-CONS-PYMNT.CNTRCT-ORGN-CDE
        pdaFcpa0610.getPnd_Eom_Ext1_Ia_Orgn_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Ia_Orgn_Cde());                                                                  //Natural: ASSIGN #EOM-EXT1.IA-ORGN-CDE := FCP-CONS-PYMNT.IA-ORGN-CDE
        pdaFcpa0610.getPnd_Eom_Ext1_Hardship_Cde().setValue(ldaFcplptax.getPymnt_Tax_Hardship_Cde());                                                                     //Natural: ASSIGN #EOM-EXT1.HARDSHIP-CDE := PYMNT-TAX.HARDSHIP-CDE
        pdaFcpa0610.getPnd_Eom_Ext1_Disability_Cde().setValue(ldaFcplptax.getPymnt_Tax_Disability_Cde());                                                                 //Natural: ASSIGN #EOM-EXT1.DISABILITY-CDE := PYMNT-TAX.DISABILITY-CDE
        if (condition(ldaFcplptax.getPymnt_Tax_Cntrct_Money_Source().equals("SR1")))                                                                                      //Natural: IF PYMNT-TAX.CNTRCT-MONEY-SOURCE = 'SR1'
        {
            pdaFcpa0610.getPnd_Eom_Ext1_New_Contract_Type().setValue(ldaFcplptax.getPymnt_Tax_Cntrct_Money_Source());                                                     //Natural: ASSIGN #EOM-EXT1.NEW-CONTRACT-TYPE := PYMNT-TAX.CNTRCT-MONEY-SOURCE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Roth_Money_Source().equals("ROTHP")))                                                                             //Natural: IF FCP-CONS-PYMNT.ROTH-MONEY-SOURCE = 'ROTHP'
            {
                pdaFcpa0610.getPnd_Eom_Ext1_New_Contract_Type().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Roth_Money_Source());                                              //Natural: ASSIGN #EOM-EXT1.NEW-CONTRACT-TYPE := FCP-CONS-PYMNT.ROTH-MONEY-SOURCE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcplptax.getPymnt_Tax_Cntrct_Money_Source().equals("IRA") || ldaFcplptax.getPymnt_Tax_Cntrct_Money_Source().equals("ROTH")               //Natural: IF PYMNT-TAX.CNTRCT-MONEY-SOURCE = 'IRA' OR = 'ROTH' OR = 'PA'
                    || ldaFcplptax.getPymnt_Tax_Cntrct_Money_Source().equals("PA")))
                {
                    pdaFcpa0610.getPnd_Eom_Ext1_New_Contract_Type().setValue(ldaFcplptax.getPymnt_Tax_Cntrct_Money_Source());                                             //Natural: ASSIGN #EOM-EXT1.NEW-CONTRACT-TYPE := PYMNT-TAX.CNTRCT-MONEY-SOURCE
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pdaFcpa0610.getPnd_Eom_Ext1_New_Contract_Type().setValue("DA-IA");                                                                                    //Natural: ASSIGN #EOM-EXT1.NEW-CONTRACT-TYPE := 'DA-IA'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Roth_Money_Source().equals("ROTHP")))                                                                                 //Natural: IF FCP-CONS-PYMNT.ROTH-MONEY-SOURCE = 'ROTHP'
        {
            pnd_Roth_First_Contrib_Dte_A.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Roth_First_Contrib_Dte());                                                                //Natural: MOVE FCP-CONS-PYMNT.ROTH-FIRST-CONTRIB-DTE TO #ROTH-FIRST-CONTRIB-DTE-A
            pdaFcpa0610.getPnd_Eom_Ext1_Rothp_1st_Contrib_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Roth_First_Contrib_Dte_A);                              //Natural: MOVE EDITED #ROTH-FIRST-CONTRIB-DTE-A TO #EOM-EXT1.ROTHP-1ST-CONTRIB-DTE ( EM = YYYYMMDD )
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.callnat(Fcpndcd.class , getCurrentProcessState(), pdaFcpa0610.getPnd_Eom_Ext1(), pnd_Dist);                                                               //Natural: CALLNAT 'FCPNDCD' USING #EOM-EXT1 #DIST
        if (condition(Global.isEscape())) return;
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind().setValue(pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Pymnt_Type_Ind());                                            //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-PYMNT-TYPE-IND := #EOM-EXT1.CNTRCT-PYMNT-TYPE-IND
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().setValue(pdaFcpa0610.getPnd_Eom_Ext1_Cntrct_Sttlmnt_Type_Ind());                                        //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-STTLMNT-TYPE-IND := #EOM-EXT1.CNTRCT-STTLMNT-TYPE-IND
        //*  GET-DIST-CODE
    }
    private void sub_Get_W9_Ben_Ind() throws Exception                                                                                                                    //Natural: GET-W9-BEN-IND
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pnd_W9_Us_Forced_Ind.reset();                                                                                                                                     //Natural: RESET #W9-US-FORCED-IND #W9-VALID
        pnd_W9_Valid.reset();
        pnd_Key_Tin.setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Annt_Soc_Sec_Nbr());                                                                                           //Natural: MOVE FCP-CONS-PYMNT.ANNT-SOC-SEC-NBR TO #KEY.TIN
        pnd_Key_Tin_Type.setValue("1");                                                                                                                                   //Natural: MOVE '1' TO #KEY.TIN-TYPE
        pnd_Key_Active_Ind.setValue("A");                                                                                                                                 //Natural: MOVE 'A' TO #KEY.ACTIVE-IND
        ldaTxwl2425.getVw_form_Receipt_View().startDatabaseRead                                                                                                           //Natural: READ ( 1 ) FORM-RECEIPT-VIEW BY SUPER-1 STARTING FROM #KEY
        (
        "READ03",
        new Wc[] { new Wc("SUPER_1", ">=", pnd_Key, WcType.BY) },
        new Oc[] { new Oc("SUPER_1", "ASC") },
        1
        );
        READ03:
        while (condition(ldaTxwl2425.getVw_form_Receipt_View().readNextRow("READ03")))
        {
            if (condition(ldaTxwl2425.getForm_Receipt_View_Tin().equals(pnd_Key_Tin) && ldaTxwl2425.getForm_Receipt_View_Tin_Type().equals(pnd_Key_Tin_Type)              //Natural: IF FORM-RECEIPT-VIEW.TIN = #KEY.TIN AND FORM-RECEIPT-VIEW.TIN-TYPE = #KEY.TIN-TYPE AND FORM-RECEIPT-VIEW.ACTIVE-IND = #KEY.ACTIVE-IND
                && ldaTxwl2425.getForm_Receipt_View_Active_Ind().equals(pnd_Key_Active_Ind)))
            {
                if (condition(ldaTxwl2425.getForm_Receipt_View_Citizen_Cde().equals("01")))                                                                               //Natural: IF FORM-RECEIPT-VIEW.CITIZEN-CDE = '01'
                {
                    if (condition(ldaTxwl2425.getForm_Receipt_View_Invalid_Tin_Ind().equals(" ") && ldaTxwl2425.getForm_Receipt_View_Tin_Type().notEquals("5")))          //Natural: IF FORM-RECEIPT-VIEW.INVALID-TIN-IND = ' ' AND FORM-RECEIPT-VIEW.TIN-TYPE NE '5'
                    {
                        pnd_W9_Valid.setValue(true);                                                                                                                      //Natural: ASSIGN #W9-VALID := TRUE
                        if (condition(! (ldaTxwl2425.getForm_Receipt_View_W9_Address_Ind().equals("Y") && ldaTxwl2425.getForm_Receipt_View_W9_Tin().equals("Y")           //Natural: IF NOT ( FORM-RECEIPT-VIEW.W9-ADDRESS-IND = 'Y' AND FORM-RECEIPT-VIEW.W9-TIN = 'Y' AND FORM-RECEIPT-VIEW.W9-SIGNATURE = 'Y' AND FORM-RECEIPT-VIEW.W9-SIGNED-DATE > 0 )
                            && ldaTxwl2425.getForm_Receipt_View_W9_Signature().equals("Y") && ldaTxwl2425.getForm_Receipt_View_W9_Signed_Date().greater(getZero()))))
                        {
                            pnd_W9_Us_Forced_Ind.setValue(true);                                                                                                          //Natural: ASSIGN #W9-US-FORCED-IND := TRUE
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_W9_Valid.setValue(false);                                                                                                                     //Natural: ASSIGN #W9-VALID := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaTxwl2425.getForm_Receipt_View_W9_Address_Ind().equals("Y") && ldaTxwl2425.getForm_Receipt_View_W9_Tin().equals("Y")                  //Natural: IF FORM-RECEIPT-VIEW.W9-ADDRESS-IND = 'Y' AND FORM-RECEIPT-VIEW.W9-TIN = 'Y' AND FORM-RECEIPT-VIEW.W9-SIGNATURE = 'Y' AND FORM-RECEIPT-VIEW.W9-SIGNED-DATE > 0 AND FORM-RECEIPT-VIEW.RES-TYPE = '1'
                        && ldaTxwl2425.getForm_Receipt_View_W9_Signature().equals("Y") && ldaTxwl2425.getForm_Receipt_View_W9_Signed_Date().greater(getZero()) 
                        && ldaTxwl2425.getForm_Receipt_View_Res_Type().equals("1")))
                    {
                        pnd_W9_Valid.setValue(true);                                                                                                                      //Natural: ASSIGN #W9-VALID := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_W9_Valid.setValue(false);                                                                                                                     //Natural: ASSIGN #W9-VALID := FALSE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_W9_Valid.getBoolean() && ! (pnd_W9_Us_Forced_Ind.getBoolean())))                                                                        //Natural: IF #W9-VALID AND NOT #W9-US-FORCED-IND
                {
                    pnd_Gtn_W9_Ben_Ind.setValue("Y");                                                                                                                     //Natural: MOVE 'Y' TO #GTN-W9-BEN-IND
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(0, "NO MATCHING RECORDS FOUND FOR W9-BEN-IND");                                                                                        //Natural: WRITE 'NO MATCHING RECORDS FOUND FOR W9-BEN-IND'
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  GET-W9-BEN-IND
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(1).setValue(true);                                                                                         //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 1 ) := #FCPACRPT.#TRUTH-TABLE ( 15 ) := #FCPACRPT.#TRUTH-TABLE ( 21 ) := TRUE
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(15).setValue(true);
        pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(21).setValue(true);
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        FOR05:                                                                                                                                                            //Natural: FOR #I = 1 TO #MAX-OCCUR
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur())); pnd_I.nadd(1))
        {
            if (condition(pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(pnd_I).getBoolean()))                                                                    //Natural: IF #FCPACRPT.#TRUTH-TABLE ( #I )
            {
                getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",                            //Natural: DISPLAY ( 01 ) ( HC = R ) 10T '/DESCRIPTION' #PYMNT-TYPE-DESC ( #I ) ( HC = L ) 'PYMNT/COUNT' #FCPAACUM.#PYMNT-CNT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 'RECORD/COUNT' #FCPAACUM.#REC-CNT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 'GROSS/AMOUNT' #FCPAACUM.#SETTL-AMT ( #I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ ) 'NET/AMOUNT' #FCPAACUM.#NET-PYMNT-AMT ( #I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ )
                		ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc().getValue(pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
                    "PYMNT/COUNT",
                		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"RECORD/COUNT",
                		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"GROSS/AMOUNT",
                		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),
                    "NET/AMOUNT",
                		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* PRINT-CONTROL-REPORT
    }
    private void sub_Accum_Control_Totals() throws Exception                                                                                                              //Natural: ACCUM-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  #MAX-OCCUR
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().greaterOrEqual(1) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().lessOrEqual(50)))                  //Natural: IF #ACCUM-OCCUR = 1 THRU 50
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                      //Natural: ASSIGN #@@MAX-FUND := WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        short decideConditionsMet2379 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPAACUM.#NEW-PYMNT-IND
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                  //Natural: ADD 1 TO #FCPAACUM.#PYMNT-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 3 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(3).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Cntrct_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#CNTRCT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 4 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(4).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dvdnd_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DVDND-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 5 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(5).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dci_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DCI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DCI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 6 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(6).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dpi_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DPI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DPI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#NET-PYMNT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 8 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(8).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Fdrl_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#FDRL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 9 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(9).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_State_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#STATE-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 10 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(10).equals(true)))
        {
            decideConditionsMet2379++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Local_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#LOCAL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet2379 > 0))
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                    //Natural: ADD 1 TO #FCPAACUM.#REC-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet2379 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getCurrentProcessState().getDbConv().dbRollback();                                                                                                                //Natural: BACKOUT TRANSACTION
        getReports().write(0, Global.getPROGRAM(),"- ERROR ON LINE",Global.getERROR_LINE(),"ERROR NBR",Global.getERROR_NR());                                             //Natural: WRITE *PROGRAM '- ERROR ON LINE' *ERROR-LINE 'ERROR NBR' *ERROR-NR
        getReports().write(0, "ALL UPDATES BACKED OUT!!");                                                                                                                //Natural: WRITE 'ALL UPDATES BACKED OUT!!'
        if (condition(true)) return;                                                                                                                                      //Natural: ESCAPE ROUTINE IMMEDIATE
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=52");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(119),"PAGE :",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(38),"ANNUITY PAYMENTS CANCEL AND REDRAW EXTRACT CONTROL REPORT",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",
        		ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"PYMNT/COUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"RECORD/COUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),"GROSS/AMOUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),"NET/AMOUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
    }
}
