/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:25:43 PM
**        * FROM NATURAL PROGRAM : Cpwp731
************************************************************
**        * FILE NAME            : Cpwp731.java
**        * CLASS NAME           : Cpwp731
**        * INSTANCE NAME        : Cpwp731
************************************************************
***********************************************************************
** PROGRAM:     CPWP731                                              **
** SYSTEM:      CONSOLIDATED PAYMENT SYSTEM - TOPS / OMNI PAY        **
** DATE:        10-20-2004                                           **
** AUTHOR:      THOMAS MCGEE                                         **
** DESCRIPTION: PAID CHECK UPDATE FOR TOPS / OMNIPAY                 **
**              JPMORGAN CHASE BANK                                  **
**                                                                   **
** HISTORY:                                                          **
** -----------------------                                           **
** 10-20-2004 : T. MCGEE     - ADAPTED FROM CPWP730.                 **
* 02/05/2013 J. OSTEEN - STORE ISN OF UPDATED RECORD ON REFERENCE TABLE
*                        CISNS FOR LATER FEED TO ODS. TAG JWO1
* 04/27/2017 SAI K     - PIN EXPANSION
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpwp731 extends BLNatBase
{
    // Data Areas
    private PdaCpwa110 pdaCpwa110;
    private LdaCpwl731 ldaCpwl731;
    private LdaCpwl730a ldaCpwl730a;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Check_Crrncy_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Crrncy_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind;
    private DbsField fcp_Cons_Pymnt_Cntrct_Orgn_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind;
    private DbsField fcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind;
    private DbsField fcp_Cons_Pymnt_Pymnt_Stats_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Cmbne_Ind;
    private DbsField fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Type_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Lob_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Cref_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Invrse_Dte;
    private DbsField fcp_Cons_Pymnt_Cntrct_Option_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Mode_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Pymnt_Dest_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Hold_Cde;
    private DbsField fcp_Cons_Pymnt_Annt_Soc_Sec_Nbr;
    private DbsField fcp_Cons_Pymnt_Annt_Ctznshp_Cde;
    private DbsField fcp_Cons_Pymnt_Annt_Rsdncy_Cde;
    private DbsField fcp_Cons_Pymnt_Pymnt_Eft_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Dte;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup fcp_Cons_Pymnt__R_Field_1;
    private DbsField fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num;
    private DbsField fcp_Cons_Pymnt_Pymnt_Prcss_Instmnt_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Amt;
    private DbsField fcp_Cons_Pymnt_Pymnt_Check_Seq_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Oia_Ind;
    private DbsField fcp_Cons_Pymnt_Pymnt_Reqst_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Instlmnt_Nbr;
    private DbsField fcp_Cons_Pymnt_Pymnt_Nbr;
    private DbsField fcp_Cons_Pymnt_Bank_Account;

    private DataAccessProgramView vw_payments;
    private DbsField payments_Cntrct_Orgn_Cde;
    private DbsField payments_Cntrct_Ppcn_Nbr;
    private DbsField payments_Pymnt_Check_Dte;
    private DbsField payments_Pymnt_Check_Nbr;
    private DbsField payments_Pymnt_Check_Amt;
    private DbsField payments_Cntrct_Invrse_Dte;
    private DbsField payments_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField payments_Cntrct_Unq_Id_Nbr;
    private DbsField payments_Cntrct_Cmbn_Nbr;
    private DbsField payments_Pymnt_Prcss_Seq_Nbr;
    private DbsField payments_Cntrct_Oia_Ind;
    private DbsField payments_Pymnt_Reqst_Nbr;
    private DbsField payments_Pymnt_Instlmnt_Nbr;
    private DbsField payments_Pymnt_Nbr;
    private DbsField payments_Bank_Account;
    private DbsField pnd_Hld_Account;
    private DbsField pnd_Pc_Trl_Total_Count;
    private DbsField pnd_Pc_Trl_Total_Amt;
    private DbsField acct_Difference_Count;
    private DbsField acct_Difference_Amount;
    private DbsField pnd_Rec_Upd;
    private DbsField pnd_Rec_Input_Total;
    private DbsField pnd_Rec_Stop;
    private DbsField pnd_Rec_Outstanding;
    private DbsField pnd_Rec_Misc_Debit;
    private DbsField pnd_Rec_Misc_Credit;
    private DbsField pnd_Rec_Void;
    private DbsField pnd_Rec_Paid_No_Issue;
    private DbsField pnd_Rec_Recon_Paid;
    private DbsField pnd_Rec_Unidentified;
    private DbsField pnd_Amt_Input_Total;
    private DbsField pnd_Amt_Stop;
    private DbsField pnd_Amt_Outstanding;
    private DbsField pnd_Amt_Void;
    private DbsField pnd_Amt_Misc_Debit;
    private DbsField pnd_Amt_Misc_Credit;
    private DbsField pnd_Amt_Paid_No_Issue;
    private DbsField pnd_Amt_Recon_Paid;
    private DbsField pnd_Amt_Unidentified;
    private DbsField pnd_Rec_Unknown_Acct;
    private DbsField pnd_Rec_Rej;
    private DbsField pnd_Rec_Not_Found;
    private DbsField pnd_Rec_Prev_Cashed;
    private DbsField pnd_Rec_10_Dig;
    private DbsField pnd_Rec_10_Dig_Omni;
    private DbsField pnd_Amt_10_Dig;
    private DbsField pnd_Amt_10_Dig_Omni;
    private DbsField pnd_Amt_Upd;
    private DbsField pnd_Amt_Prev_Cashed;
    private DbsField pnd_Amt_Unknown_Acct;
    private DbsField pnd_Amt_Rej;
    private DbsField pnd_Amt_Not_Found;
    private DbsField pnd_Found;
    private DbsField pnd_Paid;
    private DbsField pnd_Isn;
    private DbsField pnd_Et_Count;
    private DbsField pnd_Hdr_Date;
    private DbsField pnd_Det_Date;
    private DbsField pnd_Paid_Date;

    private DbsGroup pnd_Paid_Date__R_Field_2;
    private DbsField pnd_Paid_Date_Pnd_Paid_Date_Mmdd;
    private DbsField pnd_Paid_Date_Pnd_Paid_Date_Yy;
    private DbsField pnd_Program;
    private DbsField pnd_Valid_Acct;
    private DbsField pnd_B;
    private DbsField pnd_Bank_Rpt_Hdng;
    private DbsField pnd_Ws_Check_Dte;

    private DbsGroup pnd_Unq_Inv_Orgn_Prcss_Inst;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Invrse_Dte;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Orgn_Cde;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr;

    private DbsGroup pnd_Unq_Inv_Orgn_Prcss_Inst__R_Field_3;
    private DbsField pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst;

    private DbsGroup pnd_Ws_Compare_Key;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Compare_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Compare_Key__R_Field_4;
    private DbsField pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr1;

    private DbsGroup pnd_Ws_Compare_Key__R_Field_5;
    private DbsField pnd_Ws_Compare_Key_Pnd_Ws_Compare;

    private DbsGroup pnd_Pymnt_Reqst_Instmt;
    private DbsField pnd_Pymnt_Reqst_Instmt_Pymnt_Reqst_Nbr;
    private DbsField pnd_Pymnt_Reqst_Instmt_Pymnt_Instlmnt_Nbr;

    private DbsGroup pnd_Pymnt_Reqst_Instmt__R_Field_6;
    private DbsField pnd_Pymnt_Reqst_Instmt_Pnd_Ws_Pymnt_Reqst_Instmt;

    private DbsGroup pnd_Ws_Compare_Key_Omni;
    private DbsField pnd_Ws_Compare_Key_Omni_Pymnt_Reqst_Nbr;
    private DbsField pnd_Ws_Compare_Key_Omni_Pymnt_Instlmnt_Nbr;

    private DbsGroup pnd_Ws_Compare_Key_Omni__R_Field_7;
    private DbsField pnd_Ws_Compare_Key_Omni_Pnd_Ws_Compare_Omni;
    private DbsField pnd_Remarks;
    private DbsField pnd_Pymnt_System;
    private DbsField pnd_Ten_Digit_Chk;
    private DbsField pnd_Account_Nbr_A;

    private DbsGroup pnd_Account_Nbr_A__R_Field_8;
    private DbsField pnd_Account_Nbr_A_Pnd_Account_Nbr;
    private DbsField pnd_Rpt_Amt;
    private DbsField pnd_Rpt_Cnt;

    private DbsGroup bank_Data;
    private DbsField bank_Data_Pnd_Bank_Orgn_Cde;
    private DbsField bank_Data_Pnd_Bank_Routing;
    private DbsField bank_Data_Pnd_Bank_Name;
    private DbsField bank_Data_Pnd_Bank_Account;
    private DbsField bank_Data_Pnd_Bank_Account_Name;
    private DbsField bank_Data_Pnd_Check_Acct_Ind;
    private DbsField pnd_Hld_Index;
    private DbsField pnd_Tl_Index;

    private DbsGroup hold_Array;
    private DbsField hold_Array_Hld_Pc_Hdr_Acct_No;
    private DbsField hold_Array_Hld_Pc_Hdr_As_Of_Date;
    private DbsField hold_Array_Hld_Pc_Trl_Total_Amt;
    private DbsField hold_Array_Hld_Pc_Trl_Total_Count;

    private DbsGroup hold_Grand_Total;
    private DbsField hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Amt;
    private DbsField hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Count;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaCpwa110 = new PdaCpwa110(localVariables);
        ldaCpwl731 = new LdaCpwl731();
        registerRecord(ldaCpwl731);
        ldaCpwl730a = new LdaCpwl730a();
        registerRecord(ldaCpwl730a);
        registerRecord(ldaCpwl730a.getVw_fcp_Acct_Pymnt());

        // Local Variables

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnt_Cntrct_Check_Crrncy_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        fcp_Cons_Pymnt_Cntrct_Crrncy_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CRRNCY_CDE");
        fcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        fcp_Cons_Pymnt_Cntrct_Orgn_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ORGN_CDE");
        fcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_TYPE_IND");
        fcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "CNTRCT_STTLMNT_TYPE_IND");
        fcp_Cons_Pymnt_Pymnt_Stats_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_STATS_CDE");
        fcp_Cons_Pymnt_Pymnt_Cmbne_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_CMBNE_IND");
        fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        fcp_Cons_Pymnt_Cntrct_Type_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "CNTRCT_TYPE_CDE");
        fcp_Cons_Pymnt_Cntrct_Lob_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_LOB_CDE");
        fcp_Cons_Pymnt_Cntrct_Cref_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_CREF_NBR");
        fcp_Cons_Pymnt_Cntrct_Invrse_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        fcp_Cons_Pymnt_Cntrct_Option_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "CNTRCT_OPTION_CDE");
        fcp_Cons_Pymnt_Cntrct_Mode_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3, RepeatingFieldStrategy.None, "CNTRCT_MODE_CDE");
        fcp_Cons_Pymnt_Cntrct_Pymnt_Dest_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Pymnt_Dest_Cde", "CNTRCT-PYMNT-DEST-CDE", 
            FieldType.STRING, 4, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_DEST_CDE");
        fcp_Cons_Pymnt_Cntrct_Hold_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_HOLD_CDE");
        fcp_Cons_Pymnt_Annt_Soc_Sec_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "ANNT_SOC_SEC_NBR");
        fcp_Cons_Pymnt_Annt_Ctznshp_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "ANNT_CTZNSHP_CDE");
        fcp_Cons_Pymnt_Annt_Rsdncy_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 
            2, RepeatingFieldStrategy.None, "ANNT_RSDNCY_CDE");
        fcp_Cons_Pymnt_Pymnt_Eft_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_EFT_DTE");
        fcp_Cons_Pymnt_Pymnt_Check_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_DTE");
        fcp_Cons_Pymnt_Pymnt_Check_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        fcp_Cons_Pymnt__R_Field_1 = vw_fcp_Cons_Pymnt.getRecord().newGroupInGroup("fcp_Cons_Pymnt__R_Field_1", "REDEFINE", fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr);
        fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num = fcp_Cons_Pymnt__R_Field_1.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        fcp_Cons_Pymnt_Pymnt_Prcss_Instmnt_Nbr = fcp_Cons_Pymnt__R_Field_1.newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Prcss_Instmnt_Nbr", "PYMNT-PRCSS-INSTMNT-NBR", 
            FieldType.NUMERIC, 2);
        fcp_Cons_Pymnt_Pymnt_Check_Amt = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        fcp_Cons_Pymnt_Pymnt_Check_Seq_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Check_Seq_Nbr", "PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SEQ_NBR");
        fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 
            14, RepeatingFieldStrategy.None, "CNTRCT_CMBN_NBR");
        fcp_Cons_Pymnt_Cntrct_Oia_Ind = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Oia_Ind", "CNTRCT-OIA-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_OIA_IND");
        fcp_Cons_Pymnt_Pymnt_Reqst_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 
            35, RepeatingFieldStrategy.None, "PYMNT_REQST_NBR");
        fcp_Cons_Pymnt_Pymnt_Instlmnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PYMNT_INSTLMNT_NBR");
        fcp_Cons_Pymnt_Pymnt_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        fcp_Cons_Pymnt_Bank_Account = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21, 
            RepeatingFieldStrategy.None, "BANK_ACCOUNT");
        registerRecord(vw_fcp_Cons_Pymnt);

        vw_payments = new DataAccessProgramView(new NameInfo("vw_payments", "PAYMENTS"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        payments_Cntrct_Orgn_Cde = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        payments_Cntrct_Ppcn_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        payments_Pymnt_Check_Dte = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        payments_Pymnt_Check_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_NBR");
        payments_Pymnt_Check_Amt = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        payments_Cntrct_Invrse_Dte = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        payments_Pymnt_Payee_Tx_Elct_Trggr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", 
            FieldType.STRING, 1, RepeatingFieldStrategy.None, "PYMNT_PAYEE_TX_ELCT_TRGGR");
        payments_Cntrct_Unq_Id_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12, 
            RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        payments_Cntrct_Cmbn_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        payments_Pymnt_Prcss_Seq_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        payments_Cntrct_Oia_Ind = vw_payments.getRecord().newFieldInGroup("payments_Cntrct_Oia_Ind", "CNTRCT-OIA-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_OIA_IND");
        payments_Pymnt_Reqst_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        payments_Pymnt_Instlmnt_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PYMNT_INSTLMNT_NBR");
        payments_Pymnt_Nbr = vw_payments.getRecord().newFieldInGroup("payments_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        payments_Bank_Account = vw_payments.getRecord().newFieldInGroup("payments_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21, RepeatingFieldStrategy.None, 
            "BANK_ACCOUNT");
        registerRecord(vw_payments);

        pnd_Hld_Account = localVariables.newFieldInRecord("pnd_Hld_Account", "#HLD-ACCOUNT", FieldType.NUMERIC, 9);
        pnd_Pc_Trl_Total_Count = localVariables.newFieldInRecord("pnd_Pc_Trl_Total_Count", "#PC-TRL-TOTAL-COUNT", FieldType.NUMERIC, 10);
        pnd_Pc_Trl_Total_Amt = localVariables.newFieldInRecord("pnd_Pc_Trl_Total_Amt", "#PC-TRL-TOTAL-AMT", FieldType.NUMERIC, 12, 2);
        acct_Difference_Count = localVariables.newFieldInRecord("acct_Difference_Count", "ACCT-DIFFERENCE-COUNT", FieldType.NUMERIC, 10);
        acct_Difference_Amount = localVariables.newFieldInRecord("acct_Difference_Amount", "ACCT-DIFFERENCE-AMOUNT", FieldType.NUMERIC, 12, 2);
        pnd_Rec_Upd = localVariables.newFieldInRecord("pnd_Rec_Upd", "#REC-UPD", FieldType.NUMERIC, 10);
        pnd_Rec_Input_Total = localVariables.newFieldInRecord("pnd_Rec_Input_Total", "#REC-INPUT-TOTAL", FieldType.NUMERIC, 10);
        pnd_Rec_Stop = localVariables.newFieldInRecord("pnd_Rec_Stop", "#REC-STOP", FieldType.NUMERIC, 10);
        pnd_Rec_Outstanding = localVariables.newFieldInRecord("pnd_Rec_Outstanding", "#REC-OUTSTANDING", FieldType.NUMERIC, 10);
        pnd_Rec_Misc_Debit = localVariables.newFieldInRecord("pnd_Rec_Misc_Debit", "#REC-MISC-DEBIT", FieldType.NUMERIC, 10);
        pnd_Rec_Misc_Credit = localVariables.newFieldInRecord("pnd_Rec_Misc_Credit", "#REC-MISC-CREDIT", FieldType.NUMERIC, 10);
        pnd_Rec_Void = localVariables.newFieldInRecord("pnd_Rec_Void", "#REC-VOID", FieldType.NUMERIC, 10);
        pnd_Rec_Paid_No_Issue = localVariables.newFieldInRecord("pnd_Rec_Paid_No_Issue", "#REC-PAID-NO-ISSUE", FieldType.NUMERIC, 10);
        pnd_Rec_Recon_Paid = localVariables.newFieldInRecord("pnd_Rec_Recon_Paid", "#REC-RECON-PAID", FieldType.NUMERIC, 10);
        pnd_Rec_Unidentified = localVariables.newFieldInRecord("pnd_Rec_Unidentified", "#REC-UNIDENTIFIED", FieldType.NUMERIC, 10);
        pnd_Amt_Input_Total = localVariables.newFieldInRecord("pnd_Amt_Input_Total", "#AMT-INPUT-TOTAL", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Stop = localVariables.newFieldInRecord("pnd_Amt_Stop", "#AMT-STOP", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Outstanding = localVariables.newFieldInRecord("pnd_Amt_Outstanding", "#AMT-OUTSTANDING", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Void = localVariables.newFieldInRecord("pnd_Amt_Void", "#AMT-VOID", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Misc_Debit = localVariables.newFieldInRecord("pnd_Amt_Misc_Debit", "#AMT-MISC-DEBIT", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Misc_Credit = localVariables.newFieldInRecord("pnd_Amt_Misc_Credit", "#AMT-MISC-CREDIT", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Paid_No_Issue = localVariables.newFieldInRecord("pnd_Amt_Paid_No_Issue", "#AMT-PAID-NO-ISSUE", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Recon_Paid = localVariables.newFieldInRecord("pnd_Amt_Recon_Paid", "#AMT-RECON-PAID", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Unidentified = localVariables.newFieldInRecord("pnd_Amt_Unidentified", "#AMT-UNIDENTIFIED", FieldType.NUMERIC, 12, 2);
        pnd_Rec_Unknown_Acct = localVariables.newFieldInRecord("pnd_Rec_Unknown_Acct", "#REC-UNKNOWN-ACCT", FieldType.NUMERIC, 10);
        pnd_Rec_Rej = localVariables.newFieldInRecord("pnd_Rec_Rej", "#REC-REJ", FieldType.NUMERIC, 10);
        pnd_Rec_Not_Found = localVariables.newFieldInRecord("pnd_Rec_Not_Found", "#REC-NOT-FOUND", FieldType.NUMERIC, 10);
        pnd_Rec_Prev_Cashed = localVariables.newFieldInRecord("pnd_Rec_Prev_Cashed", "#REC-PREV-CASHED", FieldType.NUMERIC, 10);
        pnd_Rec_10_Dig = localVariables.newFieldInRecord("pnd_Rec_10_Dig", "#REC-10-DIG", FieldType.NUMERIC, 10);
        pnd_Rec_10_Dig_Omni = localVariables.newFieldInRecord("pnd_Rec_10_Dig_Omni", "#REC-10-DIG-OMNI", FieldType.NUMERIC, 10);
        pnd_Amt_10_Dig = localVariables.newFieldInRecord("pnd_Amt_10_Dig", "#AMT-10-DIG", FieldType.NUMERIC, 12, 2);
        pnd_Amt_10_Dig_Omni = localVariables.newFieldInRecord("pnd_Amt_10_Dig_Omni", "#AMT-10-DIG-OMNI", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Upd = localVariables.newFieldInRecord("pnd_Amt_Upd", "#AMT-UPD", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Prev_Cashed = localVariables.newFieldInRecord("pnd_Amt_Prev_Cashed", "#AMT-PREV-CASHED", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Unknown_Acct = localVariables.newFieldInRecord("pnd_Amt_Unknown_Acct", "#AMT-UNKNOWN-ACCT", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Rej = localVariables.newFieldInRecord("pnd_Amt_Rej", "#AMT-REJ", FieldType.NUMERIC, 12, 2);
        pnd_Amt_Not_Found = localVariables.newFieldInRecord("pnd_Amt_Not_Found", "#AMT-NOT-FOUND", FieldType.NUMERIC, 12, 2);
        pnd_Found = localVariables.newFieldInRecord("pnd_Found", "#FOUND", FieldType.STRING, 1);
        pnd_Paid = localVariables.newFieldInRecord("pnd_Paid", "#PAID", FieldType.STRING, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.NUMERIC, 11);
        pnd_Et_Count = localVariables.newFieldInRecord("pnd_Et_Count", "#ET-COUNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Hdr_Date = localVariables.newFieldInRecord("pnd_Hdr_Date", "#HDR-DATE", FieldType.DATE);
        pnd_Det_Date = localVariables.newFieldInRecord("pnd_Det_Date", "#DET-DATE", FieldType.DATE);
        pnd_Paid_Date = localVariables.newFieldInRecord("pnd_Paid_Date", "#PAID-DATE", FieldType.STRING, 6);

        pnd_Paid_Date__R_Field_2 = localVariables.newGroupInRecord("pnd_Paid_Date__R_Field_2", "REDEFINE", pnd_Paid_Date);
        pnd_Paid_Date_Pnd_Paid_Date_Mmdd = pnd_Paid_Date__R_Field_2.newFieldInGroup("pnd_Paid_Date_Pnd_Paid_Date_Mmdd", "#PAID-DATE-MMDD", FieldType.STRING, 
            4);
        pnd_Paid_Date_Pnd_Paid_Date_Yy = pnd_Paid_Date__R_Field_2.newFieldInGroup("pnd_Paid_Date_Pnd_Paid_Date_Yy", "#PAID-DATE-YY", FieldType.STRING, 
            2);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Valid_Acct = localVariables.newFieldInRecord("pnd_Valid_Acct", "#VALID-ACCT", FieldType.BOOLEAN, 1);
        pnd_B = localVariables.newFieldInRecord("pnd_B", "#B", FieldType.NUMERIC, 2);
        pnd_Bank_Rpt_Hdng = localVariables.newFieldInRecord("pnd_Bank_Rpt_Hdng", "#BANK-RPT-HDNG", FieldType.STRING, 70);
        pnd_Ws_Check_Dte = localVariables.newFieldInRecord("pnd_Ws_Check_Dte", "#WS-CHECK-DTE", FieldType.DATE);

        pnd_Unq_Inv_Orgn_Prcss_Inst = localVariables.newGroupInRecord("pnd_Unq_Inv_Orgn_Prcss_Inst", "#UNQ-INV-ORGN-PRCSS-INST");
        pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Unq_Id_Nbr = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Invrse_Dte = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Orgn_Cde = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Prcss_Seq_Num = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Prcss_Seq_Num", 
            "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr = pnd_Unq_Inv_Orgn_Prcss_Inst.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);

        pnd_Unq_Inv_Orgn_Prcss_Inst__R_Field_3 = localVariables.newGroupInRecord("pnd_Unq_Inv_Orgn_Prcss_Inst__R_Field_3", "REDEFINE", pnd_Unq_Inv_Orgn_Prcss_Inst);
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst = pnd_Unq_Inv_Orgn_Prcss_Inst__R_Field_3.newFieldInGroup("pnd_Unq_Inv_Orgn_Prcss_Inst_Pnd_Ws_Unq_Inv_Orgn_Prcss_Inst", 
            "#WS-UNQ-INV-ORGN-PRCSS-INST", FieldType.STRING, 31);

        pnd_Ws_Compare_Key = localVariables.newGroupInRecord("pnd_Ws_Compare_Key", "#WS-COMPARE-KEY");
        pnd_Ws_Compare_Key_Cntrct_Unq_Id_Nbr = pnd_Ws_Compare_Key.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Ws_Compare_Key_Cntrct_Invrse_Dte = pnd_Ws_Compare_Key.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Compare_Key_Cntrct_Orgn_Cde = pnd_Ws_Compare_Key.newFieldInGroup("pnd_Ws_Compare_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Compare_Key.newFieldInGroup("pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);

        pnd_Ws_Compare_Key__R_Field_4 = pnd_Ws_Compare_Key.newGroupInGroup("pnd_Ws_Compare_Key__R_Field_4", "REDEFINE", pnd_Ws_Compare_Key_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr = pnd_Ws_Compare_Key__R_Field_4.newFieldInGroup("pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr1 = pnd_Ws_Compare_Key__R_Field_4.newFieldInGroup("pnd_Ws_Compare_Key_Pymnt_Instmt_Nbr1", "PYMNT-INSTMT-NBR1", 
            FieldType.NUMERIC, 2);

        pnd_Ws_Compare_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Compare_Key__R_Field_5", "REDEFINE", pnd_Ws_Compare_Key);
        pnd_Ws_Compare_Key_Pnd_Ws_Compare = pnd_Ws_Compare_Key__R_Field_5.newFieldInGroup("pnd_Ws_Compare_Key_Pnd_Ws_Compare", "#WS-COMPARE", FieldType.STRING, 
            31);

        pnd_Pymnt_Reqst_Instmt = localVariables.newGroupInRecord("pnd_Pymnt_Reqst_Instmt", "#PYMNT-REQST-INSTMT");
        pnd_Pymnt_Reqst_Instmt_Pymnt_Reqst_Nbr = pnd_Pymnt_Reqst_Instmt.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 
            35);
        pnd_Pymnt_Reqst_Instmt_Pymnt_Instlmnt_Nbr = pnd_Pymnt_Reqst_Instmt.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", 
            FieldType.NUMERIC, 2);

        pnd_Pymnt_Reqst_Instmt__R_Field_6 = localVariables.newGroupInRecord("pnd_Pymnt_Reqst_Instmt__R_Field_6", "REDEFINE", pnd_Pymnt_Reqst_Instmt);
        pnd_Pymnt_Reqst_Instmt_Pnd_Ws_Pymnt_Reqst_Instmt = pnd_Pymnt_Reqst_Instmt__R_Field_6.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Pnd_Ws_Pymnt_Reqst_Instmt", 
            "#WS-PYMNT-REQST-INSTMT", FieldType.STRING, 37);

        pnd_Ws_Compare_Key_Omni = localVariables.newGroupInRecord("pnd_Ws_Compare_Key_Omni", "#WS-COMPARE-KEY-OMNI");
        pnd_Ws_Compare_Key_Omni_Pymnt_Reqst_Nbr = pnd_Ws_Compare_Key_Omni.newFieldInGroup("pnd_Ws_Compare_Key_Omni_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", 
            FieldType.STRING, 35);
        pnd_Ws_Compare_Key_Omni_Pymnt_Instlmnt_Nbr = pnd_Ws_Compare_Key_Omni.newFieldInGroup("pnd_Ws_Compare_Key_Omni_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", 
            FieldType.NUMERIC, 2);

        pnd_Ws_Compare_Key_Omni__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_Compare_Key_Omni__R_Field_7", "REDEFINE", pnd_Ws_Compare_Key_Omni);
        pnd_Ws_Compare_Key_Omni_Pnd_Ws_Compare_Omni = pnd_Ws_Compare_Key_Omni__R_Field_7.newFieldInGroup("pnd_Ws_Compare_Key_Omni_Pnd_Ws_Compare_Omni", 
            "#WS-COMPARE-OMNI", FieldType.STRING, 37);
        pnd_Remarks = localVariables.newFieldInRecord("pnd_Remarks", "#REMARKS", FieldType.STRING, 25);
        pnd_Pymnt_System = localVariables.newFieldInRecord("pnd_Pymnt_System", "#PYMNT-SYSTEM", FieldType.STRING, 14);
        pnd_Ten_Digit_Chk = localVariables.newFieldInRecord("pnd_Ten_Digit_Chk", "#TEN-DIGIT-CHK", FieldType.BOOLEAN, 1);
        pnd_Account_Nbr_A = localVariables.newFieldInRecord("pnd_Account_Nbr_A", "#ACCOUNT-NBR-A", FieldType.STRING, 9);

        pnd_Account_Nbr_A__R_Field_8 = localVariables.newGroupInRecord("pnd_Account_Nbr_A__R_Field_8", "REDEFINE", pnd_Account_Nbr_A);
        pnd_Account_Nbr_A_Pnd_Account_Nbr = pnd_Account_Nbr_A__R_Field_8.newFieldInGroup("pnd_Account_Nbr_A_Pnd_Account_Nbr", "#ACCOUNT-NBR", FieldType.NUMERIC, 
            9);
        pnd_Rpt_Amt = localVariables.newFieldInRecord("pnd_Rpt_Amt", "#RPT-AMT", FieldType.NUMERIC, 18, 2);
        pnd_Rpt_Cnt = localVariables.newFieldInRecord("pnd_Rpt_Cnt", "#RPT-CNT", FieldType.NUMERIC, 12);

        bank_Data = localVariables.newGroupInRecord("bank_Data", "BANK-DATA");
        bank_Data_Pnd_Bank_Orgn_Cde = bank_Data.newFieldArrayInGroup("bank_Data_Pnd_Bank_Orgn_Cde", "#BANK-ORGN-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            20));
        bank_Data_Pnd_Bank_Routing = bank_Data.newFieldArrayInGroup("bank_Data_Pnd_Bank_Routing", "#BANK-ROUTING", FieldType.STRING, 9, new DbsArrayController(1, 
            20));
        bank_Data_Pnd_Bank_Name = bank_Data.newFieldArrayInGroup("bank_Data_Pnd_Bank_Name", "#BANK-NAME", FieldType.STRING, 50, new DbsArrayController(1, 
            20));
        bank_Data_Pnd_Bank_Account = bank_Data.newFieldArrayInGroup("bank_Data_Pnd_Bank_Account", "#BANK-ACCOUNT", FieldType.STRING, 21, new DbsArrayController(1, 
            20));
        bank_Data_Pnd_Bank_Account_Name = bank_Data.newFieldArrayInGroup("bank_Data_Pnd_Bank_Account_Name", "#BANK-ACCOUNT-NAME", FieldType.STRING, 50, 
            new DbsArrayController(1, 20));
        bank_Data_Pnd_Check_Acct_Ind = bank_Data.newFieldArrayInGroup("bank_Data_Pnd_Check_Acct_Ind", "#CHECK-ACCT-IND", FieldType.STRING, 1, new DbsArrayController(1, 
            20));
        pnd_Hld_Index = localVariables.newFieldInRecord("pnd_Hld_Index", "#HLD-INDEX", FieldType.NUMERIC, 2);
        pnd_Tl_Index = localVariables.newFieldInRecord("pnd_Tl_Index", "#TL-INDEX", FieldType.NUMERIC, 2);

        hold_Array = localVariables.newGroupInRecord("hold_Array", "HOLD-ARRAY");
        hold_Array_Hld_Pc_Hdr_Acct_No = hold_Array.newFieldArrayInGroup("hold_Array_Hld_Pc_Hdr_Acct_No", "HLD-PC-HDR-ACCT-NO", FieldType.NUMERIC, 10, 
            new DbsArrayController(1, 30));
        hold_Array_Hld_Pc_Hdr_As_Of_Date = hold_Array.newFieldArrayInGroup("hold_Array_Hld_Pc_Hdr_As_Of_Date", "HLD-PC-HDR-AS-OF-DATE", FieldType.DATE, 
            new DbsArrayController(1, 30));
        hold_Array_Hld_Pc_Trl_Total_Amt = hold_Array.newFieldArrayInGroup("hold_Array_Hld_Pc_Trl_Total_Amt", "HLD-PC-TRL-TOTAL-AMT", FieldType.NUMERIC, 
            12, 2, new DbsArrayController(1, 30));
        hold_Array_Hld_Pc_Trl_Total_Count = hold_Array.newFieldArrayInGroup("hold_Array_Hld_Pc_Trl_Total_Count", "HLD-PC-TRL-TOTAL-COUNT", FieldType.NUMERIC, 
            10, new DbsArrayController(1, 30));

        hold_Grand_Total = localVariables.newGroupInRecord("hold_Grand_Total", "HOLD-GRAND-TOTAL");
        hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Amt = hold_Grand_Total.newFieldInGroup("hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Amt", "HLD-GRAND-PC-TRL-TOTAL-AMT", 
            FieldType.NUMERIC, 18, 2);
        hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Count = hold_Grand_Total.newFieldInGroup("hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Count", "HLD-GRAND-PC-TRL-TOTAL-COUNT", 
            FieldType.NUMERIC, 16);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();
        vw_payments.reset();

        ldaCpwl731.initializeValues();
        ldaCpwl730a.initializeValues();

        localVariables.reset();
        pnd_Valid_Acct.setInitialValue(false);
        pnd_Bank_Rpt_Hdng.setInitialValue(" ");
        pnd_Unq_Inv_Orgn_Prcss_Inst_Pymnt_Instmt_Nbr.setInitialValue(0);
        pnd_Pymnt_Reqst_Instmt_Pymnt_Instlmnt_Nbr.setInitialValue(0);
        pnd_Ten_Digit_Chk.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cpwp731() throws Exception
    {
        super("Cpwp731");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  ***************
        getReports().definePrinter(2, "NOT DEFINED");                                                                                                                     //Natural: DEFINE PRINTER ( 01 )
        //*  ***************
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: FORMAT ( 01 ) LS = 85 PS = 60;//Natural: FORMAT ( 02 ) LS = 85 PS = 60;//Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  **********************************************************************
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 RECORD PAID-CHECK-RECORD
        while (condition(getWorkFiles().read(1, ldaCpwl731.getPaid_Check_Record())))
        {
            CheckAtStartofData453();

            pnd_Remarks.reset();                                                                                                                                          //Natural: RESET #REMARKS
            pnd_Et_Count.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #ET-COUNT
            if (condition(pnd_Et_Count.greater(20)))                                                                                                                      //Natural: IF #ET-COUNT > 20
            {
                pnd_Et_Count.reset();                                                                                                                                     //Natural: RESET #ET-COUNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION
            }                                                                                                                                                             //Natural: END-IF
            //*                                                                                                                                                           //Natural: AT START OF DATA;//Natural: AT END OF DATA
            short decideConditionsMet462 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PC-HDR-REC-TYPE = 'HDR'
            if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_Rec_Type().equals("HDR")))
            {
                decideConditionsMet462++;
                pnd_Account_Nbr_A_Pnd_Account_Nbr.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_Acct_No_9());                                                           //Natural: ASSIGN #ACCOUNT-NBR := PC-HDR-ACCT-NO-9
                pnd_Hld_Index.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #HLD-INDEX
                pnd_Hld_Account.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_Acct_No_9());                                                                             //Natural: ASSIGN #HLD-ACCOUNT := PC-HDR-ACCT-NO-9
                hold_Array_Hld_Pc_Hdr_Acct_No.getValue(pnd_Hld_Index).setValue(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_Acct_No_9());                                       //Natural: ASSIGN HLD-PC-HDR-ACCT-NO ( #HLD-INDEX ) := PC-HDR-ACCT-NO-9
            }                                                                                                                                                             //Natural: WHEN PC-HDR-REC-TYPE = 'EOF'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_Rec_Type().equals("EOF")))
            {
                decideConditionsMet462++;
                pnd_Account_Nbr_A_Pnd_Account_Nbr.setValue(pnd_Hld_Account);                                                                                              //Natural: ASSIGN #ACCOUNT-NBR := #HLD-ACCOUNT
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                pnd_Account_Nbr_A_Pnd_Account_Nbr.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Data_Acct_No_9());                                                          //Natural: ASSIGN #ACCOUNT-NBR := PC-DATA-ACCT-NO-9
            }                                                                                                                                                             //Natural: END-DECIDE
            //*  03-04-2003
            //*  HEADER RECORD
            short decideConditionsMet476 = 0;                                                                                                                             //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PC-HDR-REC-TYPE = 'HDR'
            if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_Rec_Type().equals("HDR")))
            {
                decideConditionsMet476++;
                pnd_Account_Nbr_A_Pnd_Account_Nbr.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_Acct_No());                                                             //Natural: ASSIGN #ACCOUNT-NBR := PC-HDR-ACCT-NO
                                                                                                                                                                          //Natural: PERFORM A065-VALIDATE-ACCOUNT-NUMBER
                sub_A065_Validate_Account_Number();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A075-POPULATE-REPORT-HEADING
                sub_A075_Populate_Report_Heading();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  P = PAID NO ISSUE DETAIL RECOR
                                                                                                                                                                          //Natural: PERFORM A100-PAY-HDR-RTN
                sub_A100_Pay_Hdr_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN PC-DATA-TRANS-IND = 'P'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("P")))
            {
                decideConditionsMet476++;
                //*  R = RECON./PAID DETAIL RECORD
                                                                                                                                                                          //Natural: PERFORM A200-PAY-DATA-RTN
                sub_A200_Pay_Data_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN PC-DATA-TRANS-IND = 'R'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("R")))
            {
                decideConditionsMet476++;
                //*  S = STOP
                                                                                                                                                                          //Natural: PERFORM A200-PAY-DATA-RTN
                sub_A200_Pay_Data_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN PC-DATA-TRANS-IND = 'S'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("S")))
            {
                decideConditionsMet476++;
                //*  O = OUTSTANDING
                                                                                                                                                                          //Natural: PERFORM A200-PAY-DATA-RTN
                sub_A200_Pay_Data_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN PC-DATA-TRANS-IND = 'U'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("U")))
            {
                decideConditionsMet476++;
                //*  D = MISC. DEBIT
                                                                                                                                                                          //Natural: PERFORM A200-PAY-DATA-RTN
                sub_A200_Pay_Data_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN PC-DATA-TRANS-IND = 'D'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("D")))
            {
                decideConditionsMet476++;
                //*  C = MISC. CREDIT
                                                                                                                                                                          //Natural: PERFORM A200-PAY-DATA-RTN
                sub_A200_Pay_Data_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN PC-DATA-TRANS-IND = 'C'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("C")))
            {
                decideConditionsMet476++;
                //*  V = VOID
                                                                                                                                                                          //Natural: PERFORM A200-PAY-DATA-RTN
                sub_A200_Pay_Data_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN PC-DATA-TRANS-IND = 'V'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("V")))
            {
                decideConditionsMet476++;
                //*  TRAILER RECORD
                                                                                                                                                                          //Natural: PERFORM A200-PAY-DATA-RTN
                sub_A200_Pay_Data_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN PC-TRL-REC-TYPE = 'EOF'
            else if (condition(ldaCpwl731.getPaid_Check_Record_Pc_Trl_Rec_Type().equals("EOF")))
            {
                decideConditionsMet476++;
                //*  INVALID RECORD
                                                                                                                                                                          //Natural: PERFORM A300-PAY-TRL-RTN
                sub_A300_Pay_Trl_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM A400-ERROR-REC-RTN
                sub_A400_Error_Rec_Rtn();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            pnd_Account_Nbr_A_Pnd_Account_Nbr.reset();                                                                                                                    //Natural: RESET #ACCOUNT-NBR #HLD-ACCOUNT
            pnd_Hld_Account.reset();
                                                                                                                                                                          //Natural: PERFORM GRAND-TOTALS
            sub_Grand_Totals();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ESCAPE BOTTOM;//Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A000-DUMMY-DISPLAYS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A050-SAVE-BANK-DATA
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A065-VALIDATE-ACCOUNT-NUMBER
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A075-POPULATE-REPORT-HEADING
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A100-PAY-HDR-RTN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A200-PAY-DATA-RTN
        //* *
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A300-PAY-TRL-RTN
        //* ***********************************************************************
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A400-ERROR-REC-RTN
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: B100-PAYMENT-FILE-PROCESSING-OMNI
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C100-COMMON-PAYMENT-VALIDATIONS
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: C200-GET-OTHER-CONTRACTS-OMNI
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: D100-PRINT-DETAIL-REPORT
        //* **********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: D200-PRINT-EXCEPTION-REPORT
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCOUNT-BREAK
        //*  ---------------------------------------------------------------------
        //*  HOLD TOTALS
        //*  --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GRAND-TOTALS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-ONE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-TWO
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: RESETS
        //* *
        //* *
        //* *
        //*  ----------------------------------------------------------------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT / 001T *DATU '-' 012T *TIMX ( EM = HH':'IIAP ) 029T 'CONSOLIDATED PAYMENT SYSTEM' 066T 'PAGE:' *PAGE-NUMBER ( 1 ) ( AD = L ) / 001T *INIT-USER '-' 012T *PROGRAM 066T #HDR-DATE ( EM = MM/DD/YYYY ) // 005T #BANK-RPT-HDNG / 005T 'Account Number:' #ACCOUNT-NBR /
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT / 001T *DATU '-' 012T *TIMX ( EM = HH':'IIAP ) 029T 'CONSOLIDATED PAYMENT SYSTEM' 066T 'PAGE:' *PAGE-NUMBER ( 2 ) ( AD = L ) / 001T *INIT-USER '-' 012T *PROGRAM 066T #HDR-DATE ( EM = MM/DD/YYYY ) // 005T #BANK-RPT-HDNG / 005T 'Account Number:' #ACCOUNT-NBR // 005T 'EXCEPTION REPORT'/
    }
    private void sub_A000_Dummy_Displays() throws Exception                                                                                                               //Natural: A000-DUMMY-DISPLAYS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().display(1, "Check/Number",                                                                                                                           //Natural: DISPLAY ( 01 ) 'Check/Number' PC-DATA-CHK-NBR-10 ( EM = ZZZ9999999 ) 002X 'Check/Amount' PC-DATA-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99 ) 002X 'Paid/Date' #DET-DATE ( EM = MM/DD/YYYY ) 002X 'Trans/Ind ' PC-DATA-TRANS-IND 002X 'Refer./Number' PC-DATA-ADDITIONAL ( EM = XXXXXXXXXXXXXXX ) 002X 'Payment/System' #PYMNT-SYSTEM
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10(), new ReportEditMask ("ZZZ9999999"),new ColumnSpacing(2),"Check/Amount",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),"Paid/Date",
        		pnd_Det_Date, new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(2),"Trans/Ind ",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind(),new ColumnSpacing(2),"Refer./Number",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Additional(), new ReportEditMask ("XXXXXXXXXXXXXXX"),new ColumnSpacing(2),"Payment/System",
        		pnd_Pymnt_System);
        if (Global.isEscape()) return;
        getReports().display(2, "Check/Number",                                                                                                                           //Natural: DISPLAY ( 02 ) 'Check/Number' PC-DATA-CHK-NBR-10 ( EM = ZZZ9999999 ) 002X 'Check/Amount' PC-DATA-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99 ) 002X 'Refer./Number' PC-DATA-ADDITIONAL ( EM = XXXXXXXXXXXXXXX ) 002X 'System' #PYMNT-SYSTEM 002X 'Remarks' #REMARKS
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10(), new ReportEditMask ("ZZZ9999999"),new ColumnSpacing(2),"Check/Amount",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),"Refer./Number",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Additional(), new ReportEditMask ("XXXXXXXXXXXXXXX"),new ColumnSpacing(2),"System",
        		pnd_Pymnt_System,new ColumnSpacing(2),"Remarks",
        		pnd_Remarks);
        if (Global.isEscape()) return;
        //* *002X  'Seq No.'        PC-DATA-SEQ-NO
        //*  A000-DUMMY-DISPLAYS
    }
    //*  03-04-2003
    private void sub_A050_Save_Bank_Data() throws Exception                                                                                                               //Natural: A050-SAVE-BANK-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //*  BANK HEADER DATA
        BANK:                                                                                                                                                             //Natural: REPEAT
        while (condition(whileTrue))
        {
            pdaCpwa110.getCpwa110_Cpwa110_Function().setValue("NEXT");                                                                                                    //Natural: ASSIGN CPWA110-FUNCTION := 'NEXT'
            DbsUtil.callnat(Cpwn110.class , getCurrentProcessState(), pdaCpwa110.getCpwa110());                                                                           //Natural: CALLNAT 'CPWN110' CPWA110
            if (condition(Global.isEscape())) return;
            //*  END OF BANK TABLE
            if (condition(pdaCpwa110.getCpwa110_Cpwa110_Function().equals("END")))                                                                                        //Natural: IF CPWA110-FUNCTION EQ 'END'
            {
                if (true) break BANK;                                                                                                                                     //Natural: ESCAPE BOTTOM ( BANK. ) IMMEDIATE
            }                                                                                                                                                             //Natural: END-IF
            pdaCpwa110.getCpwa110_Cpwa110_Source_Code().setValue(pdaCpwa110.getCpwa110_Bank_Source_Code());                                                               //Natural: ASSIGN CPWA110-SOURCE-CODE := BANK-SOURCE-CODE
            //*  'OI' ORIGIN CODES
            //*  CHECKING ACCOUNTS
            if (condition(pdaCpwa110.getCpwa110_Bank_Orgn_Cde().equals("OP") && pdaCpwa110.getCpwa110_Check_Acct_Ind().equals("Y")))                                      //Natural: IF CPWA110.BANK-ORGN-CDE = 'OP' AND CPWA110.CHECK-ACCT-IND = 'Y'
            {
                pnd_B.nadd(1);                                                                                                                                            //Natural: ADD 1 TO #B
                bank_Data_Pnd_Bank_Routing.getValue(pnd_B).setValue(pdaCpwa110.getCpwa110_Bank_Routing());                                                                //Natural: ASSIGN BANK-DATA.#BANK-ROUTING ( #B ) := CPWA110.BANK-ROUTING
                bank_Data_Pnd_Bank_Account.getValue(pnd_B).setValue(pdaCpwa110.getCpwa110_Bank_Account());                                                                //Natural: ASSIGN BANK-DATA.#BANK-ACCOUNT ( #B ) := CPWA110.BANK-ACCOUNT
                bank_Data_Pnd_Bank_Name.getValue(pnd_B).setValue(pdaCpwa110.getCpwa110_Bank_Name());                                                                      //Natural: ASSIGN BANK-DATA.#BANK-NAME ( #B ) := CPWA110.BANK-NAME
                bank_Data_Pnd_Bank_Account_Name.getValue(pnd_B).setValue(pdaCpwa110.getCpwa110_Bank_Account_Name());                                                      //Natural: ASSIGN BANK-DATA.#BANK-ACCOUNT-NAME ( #B ) := CPWA110.BANK-ACCOUNT-NAME
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        pnd_B.reset();                                                                                                                                                    //Natural: RESET #B
        //*  A050-SAVE-BANK-DATA
    }
    //*  03-04-2003
    private void sub_A065_Validate_Account_Number() throws Exception                                                                                                      //Natural: A065-VALIDATE-ACCOUNT-NUMBER
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        DbsUtil.examine(new ExamineSource(bank_Data_Pnd_Bank_Account.getValue("*")), new ExamineSearch(pnd_Account_Nbr_A), new ExamineGivingIndex(pnd_B));                //Natural: EXAMINE BANK-DATA.#BANK-ACCOUNT ( * ) FOR #ACCOUNT-NBR-A GIVING INDEX #B
        if (condition(pnd_B.greater(getZero())))                                                                                                                          //Natural: IF #B > 0
        {
            pnd_Valid_Acct.setValue(true);                                                                                                                                //Natural: ASSIGN #VALID-ACCT := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Valid_Acct.setValue(false);                                                                                                                               //Natural: ASSIGN #VALID-ACCT := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  A065-VALIDATE-ACCOUNT-NUMBER
    }
    //*  03-04-2003
    private void sub_A075_Populate_Report_Heading() throws Exception                                                                                                      //Natural: A075-POPULATE-REPORT-HEADING
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        if (condition(pnd_Valid_Acct.getBoolean()))                                                                                                                       //Natural: IF #VALID-ACCT
        {
            pnd_Bank_Rpt_Hdng.setValue(DbsUtil.compress(bank_Data_Pnd_Bank_Name.getValue(pnd_B), "Paid Check Report"));                                                   //Natural: COMPRESS BANK-DATA.#BANK-NAME ( #B ) 'Paid Check Report' INTO #BANK-RPT-HDNG
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Bank_Rpt_Hdng.setValue(DbsUtil.compress("<<< UNKNOWN BANK ACCOUNT>>>", "Paid Check Report"));                                                             //Natural: COMPRESS '<<< UNKNOWN BANK ACCOUNT>>>' 'Paid Check Report' INTO #BANK-RPT-HDNG
        }                                                                                                                                                                 //Natural: END-IF
        //*  A075-POPULATE-REPORT-HEADING
    }
    private void sub_A100_Pay_Hdr_Rtn() throws Exception                                                                                                                  //Natural: A100-PAY-HDR-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Paid_Date_Pnd_Paid_Date_Yy.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_As_Of_Date_Yy());                                                                  //Natural: MOVE PC-HDR-AS-OF-DATE-YY TO #PAID-DATE-YY
        pnd_Paid_Date_Pnd_Paid_Date_Mmdd.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Hdr_As_Of_Date_Mmdd());                                                              //Natural: MOVE PC-HDR-AS-OF-DATE-MMDD TO #PAID-DATE-MMDD
        pnd_Hdr_Date.setValueEdited(new ReportEditMask("MMDDYY"),pnd_Paid_Date);                                                                                          //Natural: MOVE EDITED #PAID-DATE TO #HDR-DATE ( EM = MMDDYY )
        hold_Array_Hld_Pc_Hdr_As_Of_Date.getValue(pnd_Hld_Index).setValueEdited(new ReportEditMask("MMDDYY"),pnd_Paid_Date);                                              //Natural: MOVE EDITED #PAID-DATE TO HLD-PC-HDR-AS-OF-DATE ( #HLD-INDEX ) ( EM = MMDDYY )
        if (condition(pnd_Valid_Acct.getBoolean()))                                                                                                                       //Natural: IF #VALID-ACCT
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),"UNKNOWN HEADER ACCOUNT NUMBER = ",ldaCpwl731.getPaid_Check_Record_Pc_Hdr_Acct_No(),            //Natural: WRITE ( 01 ) 15T 'UNKNOWN HEADER ACCOUNT NUMBER = ' PC-HDR-ACCT-NO //
                NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  A100-PAY-HDR-RTN
    }
    private void sub_A200_Pay_Data_Rtn() throws Exception                                                                                                                 //Natural: A200-PAY-DATA-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        //* *
        //*  03-04-2003
        if (condition(pnd_Valid_Acct.getBoolean()))                                                                                                                       //Natural: IF #VALID-ACCT
        {
            ignore();
            //*  03-04-2003
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Remarks.setValue(DbsUtil.compress("Invalid Acct# =", ldaCpwl731.getPaid_Check_Record_Pc_Data_Acct_No()));                                                 //Natural: COMPRESS 'Invalid Acct# =' PC-DATA-ACCT-NO INTO #REMARKS
                                                                                                                                                                          //Natural: PERFORM D200-PRINT-EXCEPTION-REPORT
            sub_D200_Print_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Rec_Unknown_Acct.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-UNKNOWN-ACCT
            pnd_Amt_Unknown_Acct.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                               //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-UNKNOWN-ACCT
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        //*  CHOOSE TRANSACTION TYPES TO PROCESS
        //*  TOTAL AND PLACE ON EXCEPTION REPORT
        short decideConditionsMet681 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST PC-DATA-TRANS-IND;//Natural: VALUE 'S'
        if (condition((ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("S"))))
        {
            decideConditionsMet681++;
            pnd_Remarks.setValue("Stopped Payment");                                                                                                                      //Natural: ASSIGN #REMARKS := 'Stopped Payment'
                                                                                                                                                                          //Natural: PERFORM D200-PRINT-EXCEPTION-REPORT
            sub_D200_Print_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Rec_Stop.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-STOP
            pnd_Rec_Input_Total.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-INPUT-TOTAL
            pnd_Amt_Stop.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                       //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-STOP
            pnd_Amt_Input_Total.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-INPUT-TOTAL
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'U'
        else if (condition((ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("U"))))
        {
            decideConditionsMet681++;
            pnd_Remarks.setValue("Outstanding");                                                                                                                          //Natural: ASSIGN #REMARKS := 'Outstanding'
                                                                                                                                                                          //Natural: PERFORM D200-PRINT-EXCEPTION-REPORT
            sub_D200_Print_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Rec_Outstanding.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-OUTSTANDING
            pnd_Rec_Input_Total.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-INPUT-TOTAL
            pnd_Amt_Outstanding.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-OUTSTANDING
            pnd_Amt_Input_Total.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-INPUT-TOTAL
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'V'
        else if (condition((ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("V"))))
        {
            decideConditionsMet681++;
            pnd_Remarks.setValue("Void");                                                                                                                                 //Natural: ASSIGN #REMARKS := 'Void'
                                                                                                                                                                          //Natural: PERFORM D200-PRINT-EXCEPTION-REPORT
            sub_D200_Print_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Rec_Void.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-VOID
            pnd_Rec_Input_Total.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-INPUT-TOTAL
            pnd_Amt_Void.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                       //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-VOID
            pnd_Amt_Input_Total.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-INPUT-TOTAL
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'D'
        else if (condition((ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("D"))))
        {
            decideConditionsMet681++;
            pnd_Remarks.setValue("Misc. Debit");                                                                                                                          //Natural: ASSIGN #REMARKS := 'Misc. Debit'
                                                                                                                                                                          //Natural: PERFORM D200-PRINT-EXCEPTION-REPORT
            sub_D200_Print_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Rec_Misc_Debit.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-MISC-DEBIT
            pnd_Rec_Input_Total.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-INPUT-TOTAL
            pnd_Amt_Misc_Debit.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                 //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-MISC-DEBIT
            pnd_Amt_Input_Total.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-INPUT-TOTAL
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: VALUE 'P'
        else if (condition((ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("P"))))
        {
            decideConditionsMet681++;
            pnd_Remarks.setValue("Paid no Issue");                                                                                                                        //Natural: ASSIGN #REMARKS := 'Paid no Issue'
            //*  PERFORM
            pnd_Rec_Paid_No_Issue.nadd(1);                                                                                                                                //Natural: ADD 1 TO #REC-PAID-NO-ISSUE
            pnd_Rec_Input_Total.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-INPUT-TOTAL
            pnd_Amt_Paid_No_Issue.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                              //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-PAID-NO-ISSUE
            pnd_Amt_Input_Total.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-INPUT-TOTAL
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind().equals("R"))))
        {
            decideConditionsMet681++;
            pnd_Remarks.setValue("Recon./Paid");                                                                                                                          //Natural: ASSIGN #REMARKS := 'Recon./Paid'
            //*  PERFORM
            pnd_Rec_Recon_Paid.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-RECON-PAID
            pnd_Rec_Input_Total.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-INPUT-TOTAL
            pnd_Amt_Recon_Paid.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                 //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-RECON-PAID
            pnd_Amt_Input_Total.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-INPUT-TOTAL
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Remarks.setValue("Unidentified   ");                                                                                                                      //Natural: ASSIGN #REMARKS := 'Unidentified   '
                                                                                                                                                                          //Natural: PERFORM D200-PRINT-EXCEPTION-REPORT
            sub_D200_Print_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Rec_Unidentified.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #REC-UNIDENTIFIED
            pnd_Rec_Input_Total.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-INPUT-TOTAL
            pnd_Amt_Unidentified.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                               //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-UNIDENTIFIED
            pnd_Amt_Input_Total.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-INPUT-TOTAL
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ten_Digit_Chk.setValue(true);                                                                                                                                 //Natural: ASSIGN #TEN-DIGIT-CHK := TRUE
        //*  03-04-2003
        pnd_Paid_Date_Pnd_Paid_Date_Yy.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Data_Paid_Date_Yy());                                                                  //Natural: MOVE PC-DATA-PAID-DATE-YY TO #PAID-DATE-YY
        //*  03-04-2003
        pnd_Paid_Date_Pnd_Paid_Date_Mmdd.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Data_Paid_Date_Mmdd());                                                              //Natural: MOVE PC-DATA-PAID-DATE-MMDD TO #PAID-DATE-MMDD
        //*  03-04-2003
        pnd_Det_Date.setValueEdited(new ReportEditMask("MMDDYY"),pnd_Paid_Date);                                                                                          //Natural: MOVE EDITED #PAID-DATE TO #DET-DATE ( EM = MMDDYY )
        pnd_Found.setValue("N");                                                                                                                                          //Natural: ASSIGN #FOUND := 'N'
        pnd_Paid.setValue("N");                                                                                                                                           //Natural: ASSIGN #PAID := 'N'
        pnd_Pymnt_System.reset();                                                                                                                                         //Natural: RESET #PYMNT-SYSTEM
        if (condition(pnd_Ten_Digit_Chk.getBoolean()))                                                                                                                    //Natural: IF #TEN-DIGIT-CHK
        {
                                                                                                                                                                          //Natural: PERFORM B100-PAYMENT-FILE-PROCESSING-OMNI
            sub_B100_Payment_File_Processing_Omni();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Paid.equals("N") && pnd_Found.equals("N")))                                                                                                     //Natural: IF #PAID = 'N' AND #FOUND = 'N'
        {
            pnd_Remarks.setValue("Check not Found");                                                                                                                      //Natural: ASSIGN #REMARKS := 'Check not Found'
                                                                                                                                                                          //Natural: PERFORM D200-PRINT-EXCEPTION-REPORT
            sub_D200_Print_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Rec_Not_Found.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #REC-NOT-FOUND
            pnd_Amt_Not_Found.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                  //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-NOT-FOUND
        }                                                                                                                                                                 //Natural: END-IF
        //*  A200-PAY-DATA-RTN
    }
    //*  03-04-2003
    private void sub_A300_Pay_Trl_Rtn() throws Exception                                                                                                                  //Natural: A300-PAY-TRL-RTN
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pc_Trl_Total_Count.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Trl_Total_Count());                                                                            //Natural: ASSIGN #PC-TRL-TOTAL-COUNT := PC-TRL-TOTAL-COUNT
        pnd_Pc_Trl_Total_Amt.setValue(ldaCpwl731.getPaid_Check_Record_Pc_Trl_Total_Amt());                                                                                //Natural: ASSIGN #PC-TRL-TOTAL-AMT := PC-TRL-TOTAL-AMT
                                                                                                                                                                          //Natural: PERFORM ACCOUNT-BREAK
        sub_Account_Break();
        if (condition(Global.isEscape())) {return;}
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        if (condition(pnd_Valid_Acct.getBoolean()))                                                                                                                       //Natural: IF #VALID-ACCT
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,new TabSetting(15),"UNKNOWN TRAILER ACCOUNT NUMBER = ",pnd_Hld_Account);                                           //Natural: WRITE ( 01 ) 15T 'UNKNOWN TRAILER ACCOUNT NUMBER = ' #HLD-ACCOUNT
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  A300-PAY-TRL-RTN
    }
    private void sub_A400_Error_Rec_Rtn() throws Exception                                                                                                                //Natural: A400-ERROR-REC-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),ldaCpwl731.getPaid_Check_Record(),NEWLINE,new TabSetting(30),"UNKNOWN RECORD",NEWLINE);              //Natural: WRITE ( 02 ) 2T PAID-CHECK-RECORD / 30T 'UNKNOWN RECORD' /
        if (Global.isEscape()) return;
        //*  A400-ERROR-REC-RTN
    }
    //*  03-04-2003
    private void sub_B100_Payment_File_Processing_Omni() throws Exception                                                                                                 //Natural: B100-PAYMENT-FILE-PROCESSING-OMNI
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        vw_fcp_Cons_Pymnt.startDatabaseFind                                                                                                                               //Natural: FIND FCP-CONS-PYMNT WITH PYMNT-NBR = PC-DATA-CHK-NBR-10
        (
        "FIND1",
        new Wc[] { new Wc("PYMNT_NBR", "=", ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10(), WcType.WITH) }
        );
        FIND1:
        while (condition(vw_fcp_Cons_Pymnt.readNextRow("FIND1", true)))
        {
            vw_fcp_Cons_Pymnt.setIfNotFoundControlFlag(false);
            if (condition(vw_fcp_Cons_Pymnt.getAstCOUNTER().equals(0)))                                                                                                   //Natural: IF NO RECORDS FOUND
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-NOREC
            //*  03-04-2003
            if (condition(fcp_Cons_Pymnt_Pymnt_Instlmnt_Nbr.notEquals(1)))                                                                                                //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTLMNT-NBR NE 1
            {
                //*  GET FIRST INSTALL ONLY
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  BEGIN ROXAN 1-09-2002
            //*  VALIDATE CHECK AMT - CHECK NUMBERS  ARE RECYCLED.
            //*  ASSUMING CORRECT AMOUNT IN ALL CHECKS THAT WERE CASHED
            if (condition(fcp_Cons_Pymnt_Pymnt_Pay_Type_Req_Ind.equals(1) && fcp_Cons_Pymnt_Pymnt_Reqst_Nbr.notEquals(" ") && fcp_Cons_Pymnt_Pymnt_Check_Amt.notEquals(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt())  //Natural: IF FCP-CONS-PYMNT.PYMNT-PAY-TYPE-REQ-IND = 1 AND FCP-CONS-PYMNT.PYMNT-REQST-NBR NE ' ' AND FCP-CONS-PYMNT.PYMNT-CHECK-AMT NE PC-DATA-CHECK-AMT AND SUBSTRING ( FCP-CONS-PYMNT.BANK-ACCOUNT,1,9 ) NE SUBSTRING ( PC-DATA-CHK-NBR-10-A,2,9 )
                && !fcp_Cons_Pymnt_Bank_Account.getSubstring(1,9).equals(ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10_A().getSubstring(2,9))))
            {
                //*  PAYMENT RECORD WITH SAME CHECK #. READ AGAIN
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM C100-COMMON-PAYMENT-VALIDATIONS
            sub_C100_Common_Payment_Validations();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FIND1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FIND1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Pymnt_System.setValue("Omni Payment  ");                                                                                                                  //Natural: ASSIGN #PYMNT-SYSTEM := 'Omni Payment  '
            pnd_Found.setValue("Y");                                                                                                                                      //Natural: ASSIGN #FOUND := 'Y'
            if (condition(fcp_Cons_Pymnt_Pymnt_Stats_Cde.equals("P")))                                                                                                    //Natural: IF FCP-CONS-PYMNT.PYMNT-STATS-CDE = 'P'
            {
                //*  GETTING COMBINED CONTRACTS
                                                                                                                                                                          //Natural: PERFORM C200-GET-OTHER-CONTRACTS-OMNI
                sub_C200_Get_Other_Contracts_Omni();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("FIND1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("FIND1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
        //*  B100-PAYMENT-FILE-PROCESSING-OMNI
    }
    //*  03-04-2003
    private void sub_C100_Common_Payment_Validations() throws Exception                                                                                                   //Natural: C100-COMMON-PAYMENT-VALIDATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*   CASHED CHECKS
        if (condition(fcp_Cons_Pymnt_Pymnt_Stats_Cde.equals("C")))                                                                                                        //Natural: IF FCP-CONS-PYMNT.PYMNT-STATS-CDE = 'C'
        {
            pnd_Remarks.setValue("Check already cashed");                                                                                                                 //Natural: ASSIGN #REMARKS := 'Check already cashed'
                                                                                                                                                                          //Natural: PERFORM D200-PRINT-EXCEPTION-REPORT
            sub_D200_Print_Exception_Report();
            if (condition(Global.isEscape())) {return;}
            pnd_Rec_Prev_Cashed.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-PREV-CASHED
            pnd_Amt_Prev_Cashed.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-PREV-CASHED
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //*  C100-COMMON-PAYMENT-VALIDATIONS
    }
    private void sub_C200_Get_Other_Contracts_Omni() throws Exception                                                                                                     //Natural: C200-GET-OTHER-CONTRACTS-OMNI
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        //*  COMBINED CONTRACTS
        pnd_Pymnt_Reqst_Instmt.setValuesByName(vw_fcp_Cons_Pymnt);                                                                                                        //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #PYMNT-REQST-INSTMT
        vw_payments.startDatabaseRead                                                                                                                                     //Natural: READ PAYMENTS BY PYMNT-REQST-INSTMT FROM #WS-PYMNT-REQST-INSTMT
        (
        "READ02",
        new Wc[] { new Wc("PYMNT_REQST_INSTMT", ">=", pnd_Pymnt_Reqst_Instmt_Pnd_Ws_Pymnt_Reqst_Instmt, WcType.BY) },
        new Oc[] { new Oc("PYMNT_REQST_INSTMT", "ASC") }
        );
        READ02:
        while (condition(vw_payments.readNextRow("READ02")))
        {
            pnd_Ws_Compare_Key_Omni.setValuesByName(vw_payments);                                                                                                         //Natural: MOVE BY NAME PAYMENTS TO #WS-COMPARE-KEY-OMNI
            pnd_Ws_Compare_Key_Omni_Pymnt_Instlmnt_Nbr.reset();                                                                                                           //Natural: RESET #WS-COMPARE-KEY-OMNI.PYMNT-INSTLMNT-NBR #PYMNT-REQST-INSTMT.PYMNT-INSTLMNT-NBR
            pnd_Pymnt_Reqst_Instmt_Pymnt_Instlmnt_Nbr.reset();
            if (condition(pnd_Ws_Compare_Key_Omni_Pnd_Ws_Compare_Omni.greater(pnd_Pymnt_Reqst_Instmt_Pnd_Ws_Pymnt_Reqst_Instmt)))                                         //Natural: IF #WS-COMPARE-OMNI GT #WS-PYMNT-REQST-INSTMT
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(payments_Pymnt_Check_Amt.notEquals(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt()) && !payments_Bank_Account.getSubstring(1,               //Natural: IF PAYMENTS.PYMNT-CHECK-AMT NE PC-DATA-CHECK-AMT AND SUBSTRING ( PAYMENTS.BANK-ACCOUNT,1,9 ) NE SUBSTRING ( PC-DATA-CHK-NBR-10-A,2,9 )
                9).equals(ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10_A().getSubstring(2,9))))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Compare_Key_Omni_Pnd_Ws_Compare_Omni.notEquals(pnd_Pymnt_Reqst_Instmt_Pnd_Ws_Pymnt_Reqst_Instmt)))                                       //Natural: IF #WS-COMPARE-OMNI NE #WS-PYMNT-REQST-INSTMT
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            pnd_Isn.setValue(vw_payments.getAstISN("Read02"));                                                                                                            //Natural: ASSIGN #ISN = *ISN
            UPD1:                                                                                                                                                         //Natural: GET FCP-CONS-PYMNT #ISN
            vw_fcp_Cons_Pymnt.readByID(pnd_Isn.getLong(), "UPD1");
            if (condition(fcp_Cons_Pymnt_Pymnt_Instlmnt_Nbr.equals(1)))                                                                                                   //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTLMNT-NBR EQ 1
            {
                pnd_Rec_Upd.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-UPD
                pnd_Amt_Upd.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                    //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-UPD
                pnd_Ws_Check_Dte.reset();                                                                                                                                 //Natural: RESET #WS-CHECK-DTE
                pnd_Ws_Check_Dte.setValue(fcp_Cons_Pymnt_Pymnt_Check_Dte);                                                                                                //Natural: ASSIGN #WS-CHECK-DTE := FCP-CONS-PYMNT.PYMNT-CHECK-DTE
                                                                                                                                                                          //Natural: PERFORM D100-PRINT-DETAIL-REPORT
                sub_D100_Print_Detail_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            fcp_Cons_Pymnt_Pymnt_Stats_Cde.setValue("C");                                                                                                                 //Natural: ASSIGN FCP-CONS-PYMNT.PYMNT-STATS-CDE := 'C'
            fcp_Cons_Pymnt_Pymnt_Eft_Dte.setValueEdited(new ReportEditMask("MMDDYY"),pnd_Paid_Date);                                                                      //Natural: MOVE EDITED #PAID-DATE TO FCP-CONS-PYMNT.PYMNT-EFT-DTE ( EM = MMDDYY )
            vw_fcp_Cons_Pymnt.updateDBRow("UPD1");                                                                                                                        //Natural: UPDATE ( UPD1. )
            //*  JWO1 02/2013
            DbsUtil.callnat(Cponisns.class , getCurrentProcessState(), pnd_Isn);                                                                                          //Natural: CALLNAT 'CPONISNS' #ISN
            if (condition(Global.isEscape())) return;
            pnd_Paid.setValue("Y");                                                                                                                                       //Natural: ASSIGN #PAID := 'Y'
            pnd_Amt_10_Dig_Omni.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt());                                                                                //Natural: ADD PC-DATA-CHECK-AMT TO #AMT-10-DIG-OMNI
            pnd_Rec_10_Dig_Omni.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #REC-10-DIG-OMNI
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  C200-GET-OTHER-CONTRACTS-OMNI
    }
    //*  03-04-2003
    private void sub_D100_Print_Detail_Report() throws Exception                                                                                                          //Natural: D100-PRINT-DETAIL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        getReports().write(1, ReportOption.NOTITLE,new ReportTAsterisk(ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10()),ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10(),  //Natural: WRITE ( 01 ) T*PC-DATA-CHK-NBR-10 PC-DATA-CHK-NBR-10 ( EM = ZZZ9999999 ) T*PC-DATA-CHECK-AMT PC-DATA-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99 ) T*#DET-DATE #DET-DATE ( EM = MM/DD/YYYY ) T*PC-DATA-TRANS-IND PC-DATA-TRANS-IND T*PC-DATA-ADDITIONAL PC-DATA-ADDITIONAL ( EM = XXXXXXXXXXXXXXX ) T*#PYMNT-SYSTEM #PYMNT-SYSTEM
            new ReportEditMask ("ZZZ9999999"),new ReportTAsterisk(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt()),ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Det_Date),pnd_Det_Date, new ReportEditMask ("MM/DD/YYYY"),new ReportTAsterisk(ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind()),ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind(),new 
            ReportTAsterisk(ldaCpwl731.getPaid_Check_Record_Pc_Data_Additional()),ldaCpwl731.getPaid_Check_Record_Pc_Data_Additional(), new ReportEditMask 
            ("XXXXXXXXXXXXXXX"),new ReportTAsterisk(pnd_Pymnt_System),pnd_Pymnt_System);
        if (Global.isEscape()) return;
        //* *     T*PC-DATA-SEQ-NO      PC-DATA-SEQ-NO
        //*  D100-PRINT-DETAIL-REPORT
    }
    //*  03-04-2003
    private void sub_D200_Print_Exception_Report() throws Exception                                                                                                       //Natural: D200-PRINT-EXCEPTION-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        getReports().write(2, ReportOption.NOTITLE,new ReportTAsterisk(ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10()),ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10(),  //Natural: WRITE ( 02 ) T*PC-DATA-CHK-NBR-10 PC-DATA-CHK-NBR-10 ( EM = ZZZ9999999 ) T*PC-DATA-CHECK-AMT PC-DATA-CHECK-AMT ( EM = Z,ZZZ,ZZ9.99 ) T*PC-DATA-ADDITIONAL PC-DATA-ADDITIONAL ( EM = XXXXXXXXXXXXXXX ) T*#PYMNT-SYSTEM #PYMNT-SYSTEM T*#REMARKS #REMARKS
            new ReportEditMask ("ZZZ9999999"),new ReportTAsterisk(ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt()),ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt(), 
            new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ReportTAsterisk(ldaCpwl731.getPaid_Check_Record_Pc_Data_Additional()),ldaCpwl731.getPaid_Check_Record_Pc_Data_Additional(), 
            new ReportEditMask ("XXXXXXXXXXXXXXX"),new ReportTAsterisk(pnd_Pymnt_System),pnd_Pymnt_System,new ReportTAsterisk(pnd_Remarks),pnd_Remarks);
        if (Global.isEscape()) return;
        //*  D200-PRINT-EXCEPTION-REPORT
    }
    private void sub_Account_Break() throws Exception                                                                                                                     //Natural: ACCOUNT-BREAK
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Rpt_Amt.nadd(pnd_Amt_Input_Total);                                                                                                                            //Natural: ASSIGN #RPT-AMT := #RPT-AMT + #AMT-INPUT-TOTAL
        pnd_Rpt_Cnt.nadd(pnd_Rec_Input_Total);                                                                                                                            //Natural: ASSIGN #RPT-CNT := #RPT-CNT + #REC-INPUT-TOTAL
        hold_Array_Hld_Pc_Hdr_Acct_No.getValue(pnd_Hld_Index).setValue(pnd_Hld_Account);                                                                                  //Natural: ASSIGN HLD-PC-HDR-ACCT-NO ( #HLD-INDEX ) := #HLD-ACCOUNT
        hold_Array_Hld_Pc_Trl_Total_Amt.getValue(pnd_Hld_Index).setValue(ldaCpwl731.getPaid_Check_Record_Pc_Trl_Total_Amt());                                             //Natural: ASSIGN HLD-PC-TRL-TOTAL-AMT ( #HLD-INDEX ) := PC-TRL-TOTAL-AMT
        hold_Array_Hld_Pc_Trl_Total_Count.getValue(pnd_Hld_Index).setValue(ldaCpwl731.getPaid_Check_Record_Pc_Trl_Total_Count());                                         //Natural: ASSIGN HLD-PC-TRL-TOTAL-COUNT ( #HLD-INDEX ) := PC-TRL-TOTAL-COUNT
        //* (N16.2)
        hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Amt.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Trl_Total_Amt());                                                             //Natural: COMPUTE HLD-GRAND-PC-TRL-TOTAL-AMT = HLD-GRAND-PC-TRL-TOTAL-AMT + PC-TRL-TOTAL-AMT
        //* (N16)
        hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Count.nadd(ldaCpwl731.getPaid_Check_Record_Pc_Trl_Total_Count());                                                         //Natural: COMPUTE HLD-GRAND-PC-TRL-TOTAL-COUNT = HLD-GRAND-PC-TRL-TOTAL-COUNT + PC-TRL-TOTAL-COUNT
                                                                                                                                                                          //Natural: PERFORM REPORT-ONE
        sub_Report_One();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM REPORT-TWO
        sub_Report_Two();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM RESETS
        sub_Resets();
        if (condition(Global.isEscape())) {return;}
        //*  ACCOUNT-BREAK
    }
    private void sub_Grand_Totals() throws Exception                                                                                                                      //Natural: GRAND-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //*  ---------------------------------------------------------------------
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(2),"*",new RepeatItem(70),NEWLINE,new TabSetting(2),"** REPORT TOTALS ALL ACCOUNTS",NEWLINE,new  //Natural: WRITE ( 01 ) NOTITLE NOHDR 002T '*' ( 70 ) / 002T '** REPORT TOTALS ALL ACCOUNTS' / 002T '*' ( 70 ) / 002T '**  ACCOUNT' 020T 'DATE' 039T 'COUNT' 055T 'AMOUNT' / 002T '**'
            TabSetting(2),"*",new RepeatItem(70),NEWLINE,new TabSetting(2),"**  ACCOUNT",new TabSetting(20),"DATE",new TabSetting(39),"COUNT",new TabSetting(55),"AMOUNT",NEWLINE,new 
            TabSetting(2),"**");
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #TL-INDEX 1 TO #HLD-INDEX
        for (pnd_Tl_Index.setValue(1); condition(pnd_Tl_Index.lessOrEqual(pnd_Hld_Index)); pnd_Tl_Index.nadd(1))
        {
            if (condition(hold_Array_Hld_Pc_Trl_Total_Amt.getValue(pnd_Tl_Index).greater(getZero())))                                                                     //Natural: IF HLD-PC-TRL-TOTAL-AMT ( #TL-INDEX ) GT 0
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"**",hold_Array_Hld_Pc_Hdr_Acct_No.getValue(pnd_Tl_Index),new TabSetting(20),hold_Array_Hld_Pc_Hdr_As_Of_Date.getValue(pnd_Tl_Index),  //Natural: WRITE ( 01 ) 2T '**' HLD-PC-HDR-ACCT-NO ( #TL-INDEX ) 020T HLD-PC-HDR-AS-OF-DATE ( #TL-INDEX ) ( EM = MM/DD/YYYY ) 031T HLD-PC-TRL-TOTAL-COUNT ( #TL-INDEX ) 050T HLD-PC-TRL-TOTAL-AMT ( #TL-INDEX )
                    new ReportEditMask ("MM/DD/YYYY"),new TabSetting(31),hold_Array_Hld_Pc_Trl_Total_Count.getValue(pnd_Tl_Index), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new 
                    TabSetting(50),hold_Array_Hld_Pc_Trl_Total_Amt.getValue(pnd_Tl_Index), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(2),"**",new TabSetting(31),"--------------",new TabSetting(53),"--------------",NEWLINE,new             //Natural: WRITE ( 01 ) 2T '**' 31T '--------------' 53T '--------------' / 2T '**' 31T HLD-GRAND-PC-TRL-TOTAL-COUNT 50T HLD-GRAND-PC-TRL-TOTAL-AMT / 002T '**' / 002T '**' / 002T '*' ( 70 ) /
            TabSetting(2),"**",new TabSetting(31),hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Count, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(50),hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"**",NEWLINE,new TabSetting(2),"**",NEWLINE,new TabSetting(2),"*",new RepeatItem(70),
            NEWLINE);
        if (Global.isEscape()) return;
        //*  ******************************************
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(2),"*",new RepeatItem(70),NEWLINE,new TabSetting(2),"** REPORT TOTALS ALL ACCOUNTS",NEWLINE,new  //Natural: WRITE ( 02 ) NOTITLE NOHDR 002T '*' ( 70 ) / 002T '** REPORT TOTALS ALL ACCOUNTS' / 002T '*' ( 70 ) / 002T '**  ACCOUNT' 020T 'DATE' 039T 'COUNT' 055T 'AMOUNT' / 002T '**'
            TabSetting(2),"*",new RepeatItem(70),NEWLINE,new TabSetting(2),"**  ACCOUNT",new TabSetting(20),"DATE",new TabSetting(39),"COUNT",new TabSetting(55),"AMOUNT",NEWLINE,new 
            TabSetting(2),"**");
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #TL-INDEX 1 TO #HLD-INDEX
        for (pnd_Tl_Index.setValue(1); condition(pnd_Tl_Index.lessOrEqual(pnd_Hld_Index)); pnd_Tl_Index.nadd(1))
        {
            if (condition(hold_Array_Hld_Pc_Trl_Total_Amt.getValue(pnd_Tl_Index).greater(getZero())))                                                                     //Natural: IF HLD-PC-TRL-TOTAL-AMT ( #TL-INDEX ) GT 0
            {
                getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"**",hold_Array_Hld_Pc_Hdr_Acct_No.getValue(pnd_Tl_Index),new TabSetting(20),hold_Array_Hld_Pc_Hdr_As_Of_Date.getValue(pnd_Tl_Index),  //Natural: WRITE ( 02 ) 2T '**' HLD-PC-HDR-ACCT-NO ( #TL-INDEX ) 020T HLD-PC-HDR-AS-OF-DATE ( #TL-INDEX ) ( EM = MM/DD/YYYY ) 031T HLD-PC-TRL-TOTAL-COUNT ( #TL-INDEX ) 050T HLD-PC-TRL-TOTAL-AMT ( #TL-INDEX )
                    new ReportEditMask ("MM/DD/YYYY"),new TabSetting(31),hold_Array_Hld_Pc_Trl_Total_Count.getValue(pnd_Tl_Index), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new 
                    TabSetting(50),hold_Array_Hld_Pc_Trl_Total_Amt.getValue(pnd_Tl_Index), new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(2),"**",new TabSetting(31),"--------------",new TabSetting(53),"--------------",NEWLINE,new             //Natural: WRITE ( 02 ) 2T '**' 31T '--------------' 53T '--------------' / 2T '**' 31T HLD-GRAND-PC-TRL-TOTAL-COUNT 50T HLD-GRAND-PC-TRL-TOTAL-AMT / 002T '**' / 002T '**' / 002T '*' ( 70 ) /
            TabSetting(2),"**",new TabSetting(31),hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Count, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(50),hold_Grand_Total_Hld_Grand_Pc_Trl_Total_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"**",NEWLINE,new TabSetting(2),"**",NEWLINE,new TabSetting(2),"*",new RepeatItem(70),
            NEWLINE);
        if (Global.isEscape()) return;
        //*  GRAND-TOTALS
    }
    private void sub_Report_One() throws Exception                                                                                                                        //Natural: REPORT-ONE
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        acct_Difference_Count.compute(new ComputeParameters(false, acct_Difference_Count), pnd_Rec_Input_Total.subtract(pnd_Pc_Trl_Total_Count));                         //Natural: COMPUTE ACCT-DIFFERENCE-COUNT = #REC-INPUT-TOTAL - #PC-TRL-TOTAL-COUNT
        acct_Difference_Amount.compute(new ComputeParameters(false, acct_Difference_Amount), pnd_Amt_Input_Total.subtract(pnd_Pc_Trl_Total_Amt));                         //Natural: COMPUTE ACCT-DIFFERENCE-AMOUNT = #AMT-INPUT-TOTAL - #PC-TRL-TOTAL-AMT
        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(2),"*",new RepeatItem(70),NEWLINE,new TabSetting(42),"Count",new TabSetting(66),"Amount",NEWLINE,NEWLINE,new  //Natural: WRITE ( 01 ) // 2T '*' ( 70 ) / 42T 'Count' 66T 'Amount' // 2T 'CPS  Paid Check File  Totals: ' 34T #REC-INPUT-TOTAL 56T #AMT-INPUT-TOTAL / 2T 'Bank Paid Check File  Totals: ' 34T #PC-TRL-TOTAL-COUNT 56T #PC-TRL-TOTAL-AMT / 2T '-' ( 70 ) / 2T 'Difference CPS vs Bank      : ' 34T ACCT-DIFFERENCE-COUNT 56T ACCT-DIFFERENCE-AMOUNT / 2T '**' // 2T 'Bank File breakdowns by transaction indicators'/ 2T '       Paid No Issue   Checks: ' 34T #REC-PAID-NO-ISSUE 56T #AMT-PAID-NO-ISSUE / 2T '       Recon./paid     Checks: ' 34T #REC-RECON-PAID 56T #AMT-RECON-PAID / 2T '               Stopped Checks: ' 34T #REC-STOP 56T #AMT-STOP / 2T '               Void    Checks: ' 34T #REC-VOID 56T #AMT-VOID / 2T '               Misc    Debits: ' 34T #REC-MISC-DEBIT 56T #AMT-MISC-DEBIT / 2T '               Misc   Credits: ' 34T #REC-MISC-CREDIT 56T #AMT-MISC-CREDIT / 2T '               Outstanding   : ' 34T #REC-OUTSTANDING 56T #AMT-OUTSTANDING / 2T '             Unidentified    : ' 34T #REC-UNIDENTIFIED 56T #AMT-UNIDENTIFIED / 2T '**' // 2T 'Applied to CPS Payment System As Follows ' / 2T 'Items marked Paid            : ' 34T #REC-10-DIG-OMNI 56T #AMT-10-DIG-OMNI / 2T '-' ( 70 ) / 2T 'Total Checks Marked Paid     : ' 34T #REC-UPD 56T #AMT-UPD / 2T '-' ( 70 ) / 2T 'Checks NOT Marked Paid and Bypassed ' / 2T 'Already Marked Paid(BYPASSED): ' 34T #REC-PREV-CASHED 56T #AMT-PREV-CASHED / 2T 'Amounts Not Equal  (BYPASSED): ' 34T #REC-REJ 56T #AMT-REJ / 2T 'Payment Not Found  (BYPASSED): ' 34T #REC-NOT-FOUND 56T #AMT-NOT-FOUND / 2T 'Unknown Account No (BYPASSED): ' 34T #REC-UNKNOWN-ACCT 56T #AMT-UNKNOWN-ACCT / 2T '*' ( 70 ) /
            TabSetting(2),"CPS  Paid Check File  Totals: ",new TabSetting(34),pnd_Rec_Input_Total, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Input_Total, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"Bank Paid Check File  Totals: ",new TabSetting(34),pnd_Pc_Trl_Total_Count, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Pc_Trl_Total_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"-",new 
            RepeatItem(70),NEWLINE,new TabSetting(2),"Difference CPS vs Bank      : ",new TabSetting(34),acct_Difference_Count, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new 
            TabSetting(56),acct_Difference_Amount, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"**",NEWLINE,NEWLINE,new TabSetting(2),"Bank File breakdowns by transaction indicators",NEWLINE,new 
            TabSetting(2),"       Paid No Issue   Checks: ",new TabSetting(34),pnd_Rec_Paid_No_Issue, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Paid_No_Issue, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"       Recon./paid     Checks: ",new TabSetting(34),pnd_Rec_Recon_Paid, new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Recon_Paid, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"               Stopped Checks: ",new 
            TabSetting(34),pnd_Rec_Stop, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Stop, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"               Void    Checks: ",new TabSetting(34),pnd_Rec_Void, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Void, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"               Misc    Debits: ",new TabSetting(34),pnd_Rec_Misc_Debit, new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Misc_Debit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"               Misc   Credits: ",new 
            TabSetting(34),pnd_Rec_Misc_Credit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Misc_Credit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"               Outstanding   : ",new TabSetting(34),pnd_Rec_Outstanding, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Outstanding, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"             Unidentified    : ",new TabSetting(34),pnd_Rec_Unidentified, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Unidentified, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"**",NEWLINE,NEWLINE,new 
            TabSetting(2),"Applied to CPS Payment System As Follows ",NEWLINE,new TabSetting(2),"Items marked Paid            : ",new TabSetting(34),pnd_Rec_10_Dig_Omni, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_10_Dig_Omni, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"-",new 
            RepeatItem(70),NEWLINE,new TabSetting(2),"Total Checks Marked Paid     : ",new TabSetting(34),pnd_Rec_Upd, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new 
            TabSetting(56),pnd_Amt_Upd, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"-",new RepeatItem(70),NEWLINE,new TabSetting(2),"Checks NOT Marked Paid and Bypassed ",NEWLINE,new 
            TabSetting(2),"Already Marked Paid(BYPASSED): ",new TabSetting(34),pnd_Rec_Prev_Cashed, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Prev_Cashed, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"Amounts Not Equal  (BYPASSED): ",new TabSetting(34),pnd_Rec_Rej, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Rej, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"Payment Not Found  (BYPASSED): ",new 
            TabSetting(34),pnd_Rec_Not_Found, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Not_Found, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"Unknown Account No (BYPASSED): ",new TabSetting(34),pnd_Rec_Unknown_Acct, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Unknown_Acct, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"*",new RepeatItem(70),NEWLINE);
        if (Global.isEscape()) return;
        //* *
        if (condition(pnd_Rec_Prev_Cashed.greater(getZero()) || pnd_Rec_Rej.greater(getZero()) || pnd_Rec_Unknown_Acct.greater(getZero()) || pnd_Rec_Not_Found.greater(getZero())  //Natural: IF #REC-PREV-CASHED > 0 OR #REC-REJ > 0 OR #REC-UNKNOWN-ACCT > 0 OR #REC-NOT-FOUND > 0 OR ( #PC-TRL-TOTAL-COUNT NE #REC-INPUT-TOTAL ) OR ( #PC-TRL-TOTAL-AMT NE #AMT-INPUT-TOTAL )
            || (pnd_Pc_Trl_Total_Count.notEquals(pnd_Rec_Input_Total)) || (pnd_Pc_Trl_Total_Amt.notEquals(pnd_Amt_Input_Total))))
        {
            if (condition(getReports().getAstLinesLeft(1).less(11)))                                                                                                      //Natural: NEWPAGE ( 01 ) IF LESS THAN 11
            {
                getReports().newPage(1);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(5),"*********************************************************",NEWLINE,new          //Natural: WRITE ( 01 ) // 5T '*********************************************************' / 5T '**                                                     **' / 5T '**                                                     **' / 5T '**                SEE EXCEPTION REPORT                 **' / 5T '**                                                     **' / 5T '**                                                     **' / 5T '*********************************************************' /
                TabSetting(5),"**                                                     **",NEWLINE,new TabSetting(5),"**                                                     **",NEWLINE,new 
                TabSetting(5),"**                SEE EXCEPTION REPORT                 **",NEWLINE,new TabSetting(5),"**                                                     **",NEWLINE,new 
                TabSetting(5),"**                                                     **",NEWLINE,new TabSetting(5),"*********************************************************",
                NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  03-04-2003
            if (condition(getReports().getAstLinesLeft(1).less(12)))                                                                                                      //Natural: NEWPAGE ( 01 ) IF LESS THAN 12
            {
                getReports().newPage(1);
                if (condition(Global.isEscape())){return;}
            }
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(5),"*********************************************************",NEWLINE,new          //Natural: WRITE ( 01 ) // 5T '*********************************************************' / 5T '**                                                     **' / 5T '**                     NO ERRORS                       **' / 5T '**            CPS DATABASE UPDATE SUCESSFUL            **' / 5T '**            ACCOUNT NUMBER: ' #ACCOUNT-NBR 60T '**' / 5T '**                                                     **' / 5T '*********************************************************' /
                TabSetting(5),"**                                                     **",NEWLINE,new TabSetting(5),"**                     NO ERRORS                       **",NEWLINE,new 
                TabSetting(5),"**            CPS DATABASE UPDATE SUCESSFUL            **",NEWLINE,new TabSetting(5),"**            ACCOUNT NUMBER: ",pnd_Account_Nbr_A_Pnd_Account_Nbr,new 
                TabSetting(60),"**",NEWLINE,new TabSetting(5),"**                                                     **",NEWLINE,new TabSetting(5),"*********************************************************",
                NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Pc_Trl_Total_Count.notEquals(pnd_Rec_Input_Total) || pnd_Pc_Trl_Total_Amt.notEquals(pnd_Amt_Input_Total)))                                      //Natural: IF #PC-TRL-TOTAL-COUNT NE #REC-INPUT-TOTAL OR #PC-TRL-TOTAL-AMT NE #AMT-INPUT-TOTAL
        {
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(5),"********************************************************",NEWLINE,new           //Natural: WRITE ( 01 ) // 5T '********************************************************' / 5T '**                                                    **' / 5T '** PAID CHECK FILE RECORDS AND TRAILER OUT OF BALANCE **' / 5T '** ACCOUNT NUMBER: ' #ACCOUNT-NBR 60T '**' / 5T '** Trailer amts  : ' #PC-TRL-TOTAL-COUNT 60T '**' / 5T '**               : ' #PC-TRL-TOTAL-AMT 60T '**' / 5T '** Program amts  : ' #REC-INPUT-TOTAL 60T '**' / 5T '**               : ' #AMT-INPUT-TOTAL 60T '**' / 5T '**                                                    **' / 5T '********************************************************' /
                TabSetting(5),"**                                                    **",NEWLINE,new TabSetting(5),"** PAID CHECK FILE RECORDS AND TRAILER OUT OF BALANCE **",NEWLINE,new 
                TabSetting(5),"** ACCOUNT NUMBER: ",pnd_Account_Nbr_A_Pnd_Account_Nbr,new TabSetting(60),"**",NEWLINE,new TabSetting(5),"** Trailer amts  : ",pnd_Pc_Trl_Total_Count, 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(60),"**",NEWLINE,new TabSetting(5),"**               : ",pnd_Pc_Trl_Total_Amt, new ReportEditMask 
                ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"**",NEWLINE,new TabSetting(5),"** Program amts  : ",pnd_Rec_Input_Total, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new 
                TabSetting(60),"**",NEWLINE,new TabSetting(5),"**               : ",pnd_Amt_Input_Total, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"**",NEWLINE,new 
                TabSetting(5),"**                                                    **",NEWLINE,new TabSetting(5),"********************************************************",
                NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  REPORT-ONE
    }
    private void sub_Report_Two() throws Exception                                                                                                                        //Natural: REPORT-TWO
    {
        if (BLNatReinput.isReinput()) return;

        //*  ----------------------------------------------------------------------
        acct_Difference_Count.compute(new ComputeParameters(false, acct_Difference_Count), pnd_Rec_Input_Total.subtract(pnd_Pc_Trl_Total_Count));                         //Natural: COMPUTE ACCT-DIFFERENCE-COUNT = #REC-INPUT-TOTAL - #PC-TRL-TOTAL-COUNT
        acct_Difference_Amount.compute(new ComputeParameters(false, acct_Difference_Amount), pnd_Amt_Input_Total.subtract(pnd_Pc_Trl_Total_Amt));                         //Natural: COMPUTE ACCT-DIFFERENCE-AMOUNT = #AMT-INPUT-TOTAL - #PC-TRL-TOTAL-AMT
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 2 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,ReportOption.NOHDR,new TabSetting(2),"*",new RepeatItem(70),NEWLINE,new TabSetting(42),"Count",new                     //Natural: WRITE ( 02 ) NOTITLE NOHDR 2T '*' ( 70 ) / 42T 'Count' 66T 'Amount' // 2T 'CPS  Paid Check File  Totals: ' 34T #REC-INPUT-TOTAL 56T #AMT-INPUT-TOTAL / 2T 'Bank Paid Check File  Totals: ' 34T #PC-TRL-TOTAL-COUNT 56T #PC-TRL-TOTAL-AMT / 2T '-' ( 70 ) / 2T 'Difference CPS vs Bank      : ' 34T ACCT-DIFFERENCE-COUNT 56T ACCT-DIFFERENCE-AMOUNT // 2T '**' // 2T 'Bank File breakdowns by transaction indicators'/ 2T '       Paid No Issue   Checks: ' 34T #REC-PAID-NO-ISSUE 56T #AMT-PAID-NO-ISSUE / 2T '       Recon./paid     Checks: ' 34T #REC-RECON-PAID 56T #AMT-RECON-PAID / 2T '               Stopped Checks: ' 34T #REC-STOP 56T #AMT-STOP / 2T '               Void    Checks: ' 34T #REC-VOID 56T #AMT-VOID / 2T '               Misc    Debits: ' 34T #REC-MISC-DEBIT 56T #AMT-MISC-DEBIT / 2T '               Misc   Credits: ' 34T #REC-MISC-CREDIT 56T #AMT-MISC-CREDIT / 2T '               Outstanding   : ' 34T #REC-OUTSTANDING 56T #AMT-OUTSTANDING / 2T '             Unidentified    : ' 34T #REC-UNIDENTIFIED 56T #AMT-UNIDENTIFIED / 2T '**' // 2T 'Applied to CPS Payment System As Follows ' / 2T 'Investment Solutions Paid    : ' 34T #REC-10-DIG-OMNI 56T #AMT-10-DIG-OMNI / 2T '-' ( 70 ) / 2T 'Total Checks Marked Paid     : ' 34T #REC-UPD 56T #AMT-UPD / 2T '-' ( 70 ) / 2T 'Checks NOT Marked Paid and Bypassed ' / 2T 'Already Marked Paid(BYPASSED): ' 34T #REC-PREV-CASHED 56T #AMT-PREV-CASHED / 2T 'Amounts Not Equal  (BYPASSED): ' 34T #REC-REJ 56T #AMT-REJ / 2T 'Payment Not Found  (BYPASSED): ' 34T #REC-NOT-FOUND 56T #AMT-NOT-FOUND / 2T 'Unknown Account No (BYPASSED): ' 34T #REC-UNKNOWN-ACCT 56T #AMT-UNKNOWN-ACCT / 2T '*' ( 70 ) /
            TabSetting(66),"Amount",NEWLINE,NEWLINE,new TabSetting(2),"CPS  Paid Check File  Totals: ",new TabSetting(34),pnd_Rec_Input_Total, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Input_Total, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"Bank Paid Check File  Totals: ",new 
            TabSetting(34),pnd_Pc_Trl_Total_Count, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Pc_Trl_Total_Amt, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"-",new RepeatItem(70),NEWLINE,new TabSetting(2),"Difference CPS vs Bank      : ",new TabSetting(34),acct_Difference_Count, new 
            ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),acct_Difference_Amount, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(2),"**",NEWLINE,NEWLINE,new 
            TabSetting(2),"Bank File breakdowns by transaction indicators",NEWLINE,new TabSetting(2),"       Paid No Issue   Checks: ",new TabSetting(34),pnd_Rec_Paid_No_Issue, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Paid_No_Issue, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"       Recon./paid     Checks: ",new 
            TabSetting(34),pnd_Rec_Recon_Paid, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Recon_Paid, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"               Stopped Checks: ",new TabSetting(34),pnd_Rec_Stop, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Stop, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"               Void    Checks: ",new TabSetting(34),pnd_Rec_Void, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Void, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"               Misc    Debits: ",new 
            TabSetting(34),pnd_Rec_Misc_Debit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Misc_Debit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"               Misc   Credits: ",new TabSetting(34),pnd_Rec_Misc_Credit, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Misc_Credit, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"               Outstanding   : ",new TabSetting(34),pnd_Rec_Outstanding, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Outstanding, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"             Unidentified    : ",new 
            TabSetting(34),pnd_Rec_Unidentified, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Unidentified, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"**",NEWLINE,NEWLINE,new TabSetting(2),"Applied to CPS Payment System As Follows ",NEWLINE,new TabSetting(2),"Investment Solutions Paid    : ",new 
            TabSetting(34),pnd_Rec_10_Dig_Omni, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_10_Dig_Omni, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"-",new RepeatItem(70),NEWLINE,new TabSetting(2),"Total Checks Marked Paid     : ",new TabSetting(34),pnd_Rec_Upd, new ReportEditMask 
            ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Upd, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"-",new RepeatItem(70),NEWLINE,new 
            TabSetting(2),"Checks NOT Marked Paid and Bypassed ",NEWLINE,new TabSetting(2),"Already Marked Paid(BYPASSED): ",new TabSetting(34),pnd_Rec_Prev_Cashed, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Prev_Cashed, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"Amounts Not Equal  (BYPASSED): ",new 
            TabSetting(34),pnd_Rec_Rej, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Rej, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(2),"Payment Not Found  (BYPASSED): ",new TabSetting(34),pnd_Rec_Not_Found, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Not_Found, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"Unknown Account No (BYPASSED): ",new TabSetting(34),pnd_Rec_Unknown_Acct, 
            new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(56),pnd_Amt_Unknown_Acct, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(2),"*",new 
            RepeatItem(70),NEWLINE);
        if (Global.isEscape()) return;
        if (condition(pnd_Pc_Trl_Total_Count.notEquals(pnd_Rec_Input_Total) || pnd_Pc_Trl_Total_Amt.notEquals(pnd_Amt_Input_Total)))                                      //Natural: IF #PC-TRL-TOTAL-COUNT NE #REC-INPUT-TOTAL OR #PC-TRL-TOTAL-AMT NE #AMT-INPUT-TOTAL
        {
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape())){return;}
            getReports().write(2, ReportOption.NOTITLE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(5),"********************************************************",NEWLINE,new  //Natural: WRITE ( 02 ) ///// 5T '********************************************************' / 5T '**                                                    **' / 5T '** PAID CHECK FILE RECORDS AND TRAILER OUT OF BALANCE **' / 5T '** ACCOUNT NUMBER: ' #ACCOUNT-NBR 60T '**' / 5T '** Trailer amts  : ' #PC-TRL-TOTAL-COUNT 60T '**' / 5T '**               : ' #PC-TRL-TOTAL-AMT 60T '**' / 5T '** Program amts  : ' #REC-INPUT-TOTAL 60T '**' / 5T '**               : ' #AMT-INPUT-TOTAL 60T '**' / 5T '**                                                    **' / 5T '********************************************************' /
                TabSetting(5),"**                                                    **",NEWLINE,new TabSetting(5),"** PAID CHECK FILE RECORDS AND TRAILER OUT OF BALANCE **",NEWLINE,new 
                TabSetting(5),"** ACCOUNT NUMBER: ",pnd_Account_Nbr_A_Pnd_Account_Nbr,new TabSetting(60),"**",NEWLINE,new TabSetting(5),"** Trailer amts  : ",pnd_Pc_Trl_Total_Count, 
                new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new TabSetting(60),"**",NEWLINE,new TabSetting(5),"**               : ",pnd_Pc_Trl_Total_Amt, new ReportEditMask 
                ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"**",NEWLINE,new TabSetting(5),"** Program amts  : ",pnd_Rec_Input_Total, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9"),new 
                TabSetting(60),"**",NEWLINE,new TabSetting(5),"**               : ",pnd_Amt_Input_Total, new ReportEditMask ("Z,ZZZ,ZZZ,ZZ9.99"),new TabSetting(60),"**",NEWLINE,new 
                TabSetting(5),"**                                                    **",NEWLINE,new TabSetting(5),"********************************************************",
                NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  REPORT-TWO
    }
    private void sub_Resets() throws Exception                                                                                                                            //Natural: RESETS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pc_Trl_Total_Count.reset();                                                                                                                                   //Natural: RESET #PC-TRL-TOTAL-COUNT #PC-TRL-TOTAL-AMT #REC-UPD #REC-INPUT-TOTAL #REC-STOP #REC-OUTSTANDING #REC-MISC-DEBIT #REC-MISC-CREDIT #REC-VOID #REC-PAID-NO-ISSUE #REC-RECON-PAID #REC-UNIDENTIFIED #AMT-INPUT-TOTAL #AMT-STOP #AMT-OUTSTANDING #AMT-VOID #AMT-MISC-DEBIT #AMT-MISC-CREDIT #AMT-PAID-NO-ISSUE #AMT-RECON-PAID #AMT-UNIDENTIFIED #REC-UNKNOWN-ACCT #REC-REJ #REC-NOT-FOUND #REC-PREV-CASHED #REC-10-DIG #REC-10-DIG-OMNI #AMT-10-DIG #AMT-10-DIG-OMNI #AMT-UPD #AMT-PREV-CASHED #AMT-UNKNOWN-ACCT #AMT-REJ
        pnd_Pc_Trl_Total_Amt.reset();
        pnd_Rec_Upd.reset();
        pnd_Rec_Input_Total.reset();
        pnd_Rec_Stop.reset();
        pnd_Rec_Outstanding.reset();
        pnd_Rec_Misc_Debit.reset();
        pnd_Rec_Misc_Credit.reset();
        pnd_Rec_Void.reset();
        pnd_Rec_Paid_No_Issue.reset();
        pnd_Rec_Recon_Paid.reset();
        pnd_Rec_Unidentified.reset();
        pnd_Amt_Input_Total.reset();
        pnd_Amt_Stop.reset();
        pnd_Amt_Outstanding.reset();
        pnd_Amt_Void.reset();
        pnd_Amt_Misc_Debit.reset();
        pnd_Amt_Misc_Credit.reset();
        pnd_Amt_Paid_No_Issue.reset();
        pnd_Amt_Recon_Paid.reset();
        pnd_Amt_Unidentified.reset();
        pnd_Rec_Unknown_Acct.reset();
        pnd_Rec_Rej.reset();
        pnd_Rec_Not_Found.reset();
        pnd_Rec_Prev_Cashed.reset();
        pnd_Rec_10_Dig.reset();
        pnd_Rec_10_Dig_Omni.reset();
        pnd_Amt_10_Dig.reset();
        pnd_Amt_10_Dig_Omni.reset();
        pnd_Amt_Upd.reset();
        pnd_Amt_Prev_Cashed.reset();
        pnd_Amt_Unknown_Acct.reset();
        pnd_Amt_Rej.reset();
        //*  RESETS
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=85 PS=60");
        Global.format(2, "LS=85 PS=60");

        getReports().write(1, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,new TabSetting(1),Global.getDATU(),"-",new 
            TabSetting(12),Global.getTIMX(), new ReportEditMask ("HH':'IIAP"),new TabSetting(29),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(66),"PAGE:",getReports().getPageNumberDbs(1), 
            new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(1),Global.getINIT_USER(),"-",new TabSetting(12),Global.getPROGRAM(),new TabSetting(66),pnd_Hdr_Date, 
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(5),pnd_Bank_Rpt_Hdng,NEWLINE,new TabSetting(5),"Account Number:",pnd_Account_Nbr_A_Pnd_Account_Nbr,
            NEWLINE);
        getReports().write(2, ReportOption.NOTITLE,ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,NEWLINE,new TabSetting(1),Global.getDATU(),"-",new 
            TabSetting(12),Global.getTIMX(), new ReportEditMask ("HH':'IIAP"),new TabSetting(29),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(66),"PAGE:",getReports().getPageNumberDbs(2), 
            new FieldAttributes ("AD=L"),NEWLINE,new TabSetting(1),Global.getINIT_USER(),"-",new TabSetting(12),Global.getPROGRAM(),new TabSetting(66),pnd_Hdr_Date, 
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,NEWLINE,new TabSetting(5),pnd_Bank_Rpt_Hdng,NEWLINE,new TabSetting(5),"Account Number:",pnd_Account_Nbr_A_Pnd_Account_Nbr,NEWLINE,NEWLINE,new 
            TabSetting(5),"EXCEPTION REPORT",NEWLINE);

        getReports().setDisplayColumns(1, "Check/Number",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10(), new ReportEditMask ("ZZZ9999999"),new ColumnSpacing(2),"Check/Amount",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),"Paid/Date",
        		pnd_Det_Date, new ReportEditMask ("MM/DD/YYYY"),new ColumnSpacing(2),"Trans/Ind ",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Trans_Ind(),new ColumnSpacing(2),"Refer./Number",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Additional(), new ReportEditMask ("XXXXXXXXXXXXXXX"),new ColumnSpacing(2),"Payment/System",
        		pnd_Pymnt_System);
        getReports().setDisplayColumns(2, "Check/Number",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Chk_Nbr_10(), new ReportEditMask ("ZZZ9999999"),new ColumnSpacing(2),"Check/Amount",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Check_Amt(), new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),"Refer./Number",
        		ldaCpwl731.getPaid_Check_Record_Pc_Data_Additional(), new ReportEditMask ("XXXXXXXXXXXXXXX"),new ColumnSpacing(2),"System",
        		pnd_Pymnt_System,new ColumnSpacing(2),"Remarks",
        		pnd_Remarks);
    }
    private void CheckAtStartofData453() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM A050-SAVE-BANK-DATA
            sub_A050_Save_Bank_Data();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
