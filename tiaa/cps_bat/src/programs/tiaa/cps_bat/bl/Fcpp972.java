/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:35 PM
**        * FROM NATURAL PROGRAM : Fcpp972
************************************************************
**        * FILE NAME            : Fcpp972.java
**        * CLASS NAME           : Fcpp972
**        * INSTANCE NAME        : Fcpp972
************************************************************
************************************************************************
* PROGRAM  : FCPP972 - DC TO XML  DCS ELIMINATION (CLONE OF FCPP372)
* SYSTEM   : CPS
* TITLE    : SEPARATE CHECKS FOR EFT's and Globals
* FUNCTION : THE PROGRAM SEPARATES THE EXTRACTED-SORTED RECORDS, READY
*            TO BE PRINTED AS CHECKS, STATEMENTS, NOTIFICATIONS ETC,
*            INTO 4 FILES:
*              . CHECKS NO HOLD
*              . CHECKS WITH HOLD CODE
*              . EFT's no hold
*              . EFT's with hold code
*            THESE FILES ARE INPUT DOWN THE PROCESSING LINE INTO THE
*            CHECK CUTTING AND THE STATEMENT PRINTING PROGRAM.
* UPDATES:
*        -  03/XX/2017 FENDAYA - DCS ELIMINATION
* ----------------------------------------------------------------------
*
* ABENDS:
*
*  40  = INPUT FILE IS EMPTY
*  42  = PROGRAM PROBLEM - INVALID VALUE SET TO #WS-XFILE
*  43  = INPUT FILE DOES NOT START WITH
*        #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR = 10
*  44  = INVALID  #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND
*  45  = INVALID  #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR
* ----------------------------------------------------------------------
* WORK FILES:
*
*  WF1 = SORTED INPUT OF EXTRACTED FILE (FROM DATA BASE BY FCPP710)
*  WF2 = WF1 CHECK       RECORDS ONLY
*  WF3 = WF1 EFT/GLOBALS RECORDS ONLY
* ----------------------------------------------------------------------
* HISTORY
* MODIFICATION LOG
* 2017/03/17 F.ENDAYA  NEW
* 2017/08/12 F.ENDAYA  PIN EXPANSION. FE201708
*
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp972 extends BLNatBase
{
    // Data Areas
    private LdaFcpl720a ldaFcpl720a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Program;
    private DbsField pnd_Abend_Cde;
    private DbsField pnd_C_In_Rcrd_All;
    private DbsField pnd_C_In_Rcrd_10;
    private DbsField pnd_C_In_Rcrd_20;
    private DbsField pnd_C_In_Rcrd_30;
    private DbsField pnd_C_In_Checks;
    private DbsField pnd_C_In_Efts;
    private DbsField pnd_C_In_Globals;

    private DbsGroup pnd_Ws_Work_Rec_Record;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes;

    private DbsGroup pnd_Ws_Work_Rec_Record__R_Field_1;

    private DbsGroup pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record;

    private DbsGroup pnd_Ws_Header_Record__R_Field_2;

    private DbsGroup pnd_Ws_Header_Record_Pnd_Ws_Header_Level2;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Header_Record__R_Field_3;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Header_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Header_Record_Filler;
    private DbsField pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;

    private DbsGroup pnd_Ws_Header_Record__R_Field_4;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes;
    private DbsField pnd_Ov00_Usps_Sw;
    private DbsField pnd_New_Ndx;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl720a = new LdaFcpl720a();
        registerRecord(ldaFcpl720a);

        // Local Variables
        localVariables = new DbsRecord();
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);
        pnd_C_In_Rcrd_All = localVariables.newFieldInRecord("pnd_C_In_Rcrd_All", "#C-IN-RCRD-ALL", FieldType.NUMERIC, 7);
        pnd_C_In_Rcrd_10 = localVariables.newFieldInRecord("pnd_C_In_Rcrd_10", "#C-IN-RCRD-10", FieldType.NUMERIC, 7);
        pnd_C_In_Rcrd_20 = localVariables.newFieldInRecord("pnd_C_In_Rcrd_20", "#C-IN-RCRD-20", FieldType.NUMERIC, 7);
        pnd_C_In_Rcrd_30 = localVariables.newFieldInRecord("pnd_C_In_Rcrd_30", "#C-IN-RCRD-30", FieldType.NUMERIC, 7);
        pnd_C_In_Checks = localVariables.newFieldInRecord("pnd_C_In_Checks", "#C-IN-CHECKS", FieldType.NUMERIC, 7);
        pnd_C_In_Efts = localVariables.newFieldInRecord("pnd_C_In_Efts", "#C-IN-EFTS", FieldType.NUMERIC, 7);
        pnd_C_In_Globals = localVariables.newFieldInRecord("pnd_C_In_Globals", "#C-IN-GLOBALS", FieldType.NUMERIC, 7);

        pnd_Ws_Work_Rec_Record = localVariables.newGroupInRecord("pnd_Ws_Work_Rec_Record", "#WS-WORK-REC-RECORD");
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes = pnd_Ws_Work_Rec_Record.newFieldArrayInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes", "#WS-WORK-REC-BYTES", 
            FieldType.STRING, 1, new DbsArrayController(1, 500));

        pnd_Ws_Work_Rec_Record__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Work_Rec_Record__R_Field_1", "REDEFINE", pnd_Ws_Work_Rec_Record);

        pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key = pnd_Ws_Work_Rec_Record__R_Field_1.newGroupInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key", "#WS-WORK-REC-KEY");
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Prcss_Seq_Num", 
            "#WS-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code", 
            "#WS-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Key.newFieldInGroup("pnd_Ws_Work_Rec_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Header_Record = localVariables.newFieldArrayInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD", FieldType.STRING, 1, new DbsArrayController(1, 
            500));

        pnd_Ws_Header_Record__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Header_Record__R_Field_2", "REDEFINE", pnd_Ws_Header_Record);

        pnd_Ws_Header_Record_Pnd_Ws_Header_Level2 = pnd_Ws_Header_Record__R_Field_2.newGroupInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Level2", "#WS-HEADER-LEVEL2");
        pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", 
            "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", 
            "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", 
            "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", 
            "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", 
            "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", 
            "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", 
            "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", 
            "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", 
            "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", 
            "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", 
            "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", 
            "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", 
            "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", 
            "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", 
            "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", 
            "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", 
            "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", 
            "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", 
            "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", 
            "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", 
            "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Ws_Header_Record__R_Field_3 = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newGroupInGroup("pnd_Ws_Header_Record__R_Field_3", "REDEFINE", pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record__R_Field_3.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record__R_Field_3.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", 
            "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", 
            "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", 
            "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", 
            "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", 
            "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time", 
            "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Ws_Header_Record_Pymnt_Intrfce_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Filler", "FILLER", FieldType.STRING, 
            4);
        pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cnr_Orgnl_Invrse_Dte", 
            "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", 
            "#WS-HEADER-FILLER", FieldType.STRING, 121);

        pnd_Ws_Header_Record__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Header_Record__R_Field_4", "REDEFINE", pnd_Ws_Header_Record);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes = pnd_Ws_Header_Record__R_Field_4.newFieldArrayInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes", "#WS-HEADER-BYTES", 
            FieldType.STRING, 1, new DbsArrayController(1, 500));
        pnd_Ov00_Usps_Sw = localVariables.newFieldInRecord("pnd_Ov00_Usps_Sw", "#OV00-USPS-SW", FieldType.BOOLEAN, 1);
        pnd_New_Ndx = localVariables.newFieldInRecord("pnd_New_Ndx", "#NEW-NDX", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl720a.initializeValues();

        localVariables.reset();
        pnd_Ov00_Usps_Sw.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp972() throws Exception
    {
        super("Fcpp972");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp972|Main");
        setupReports();
        while(true)
        {
            try
            {
                if (Global.isEscape()) return;                                                                                                                            //Natural: FORMAT ( 0 ) PS = 58 LS = 80 ZP = ON;//Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 28T #CNTRCT-ORGN-CDE 'EXTRACT CONTROL REPORT' 68T 'REPORT: RPT0' //
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM := *PROGRAM
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA = 'INFP9000'
                //* ***********************
                //*   MAIN PROGRAM LOGIC  *
                //* ***********************
                if (condition(Global.getSTACK().getDatacount().notEquals(1)))                                                                                             //Natural: IF *DATA NE 1
                {
                    pnd_Abend_Cde.setValue(50);                                                                                                                           //Natural: ASSIGN #ABEND-CDE := 50
                    getReports().write(0, Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Missing input parameter");                                  //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Missing input parameter'
                    if (Global.isEscape()) return;
                    DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                  //Natural: TERMINATE #ABEND-CDE
                }                                                                                                                                                         //Natural: END-IF
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Header_Record_Cntrct_Orgn_Cde);                                                                 //Natural: INPUT CNTRCT-ORGN-CDE
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 #WS-WORK-REC-BYTES ( * )
                while (condition(getWorkFiles().read(1, pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"))))
                {
                    pnd_C_In_Rcrd_All.nadd(1);                                                                                                                            //Natural: ADD 1 TO #C-IN-RCRD-ALL
                    //*  FIRST TIME
                    if (condition(pnd_C_In_Rcrd_All.lessOrEqual(1)))                                                                                                      //Natural: IF #C-IN-RCRD-ALL LE 1
                    {
                                                                                                                                                                          //Natural: PERFORM VALIDATE-FIRST-RECORD
                        sub_Validate_First_Record();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().compute(new ComputeParameters(false, ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx()),                  //Natural: ASSIGN #REC-TYPE-IDX := #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR / 10
                        pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.divide(10));
                    //*  HEADER REC
                    short decideConditionsMet202 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE OF #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR;//Natural: VALUE 10
                    if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(10))))
                    {
                        decideConditionsMet202++;
                        //*   LEON
                        //*  FE201611
                        pnd_Ov00_Usps_Sw.reset();                                                                                                                         //Natural: RESET #OV00-USPS-SW #NEW-NDX
                        pnd_New_Ndx.reset();
                        pnd_C_In_Rcrd_10.nadd(1);                                                                                                                         //Natural: ADD 1 TO #C-IN-RCRD-10
                        pnd_Ws_Header_Record_Pnd_Ws_Header_Bytes.getValue("*").setValue(pnd_Ws_Work_Rec_Record_Pnd_Ws_Work_Rec_Bytes.getValue("*"));                      //Natural: ASSIGN #WS-HEADER-BYTES ( * ) := #WS-WORK-REC-BYTES ( * )
                        //*  CHECKS
                        short decideConditionsMet212 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
                        if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1))))
                        {
                            decideConditionsMet212++;
                            if (condition(pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code.equals("0000")))                                                               //Natural: IF #WS-WORK-REC-RECORD.#WS-CONTRACT-HOLD-CODE = '0000'
                            {
                                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(2);                                                                                   //Natural: ASSIGN #CNTL-IDX := 2
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(1);                                                                                   //Natural: ASSIGN #CNTL-IDX := 1
                                //*  LEON USPS
                                //*  LEON
                                if (condition(pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code.equals("OV00") || pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code.equals("USPS"))) //Natural: IF #WS-WORK-REC-RECORD.#WS-CONTRACT-HOLD-CODE = 'OV00' OR = 'USPS'
                                {
                                    //*  LEON
                                    pnd_Ov00_Usps_Sw.setValue(true);                                                                                                      //Natural: MOVE TRUE TO #OV00-USPS-SW
                                    //*  LEON
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            //*  FE201611
                            //*  EFT
                            pnd_C_In_Checks.nadd(1);                                                                                                                      //Natural: ADD 1 TO #C-IN-CHECKS
                            pnd_New_Ndx.setValue(1);                                                                                                                      //Natural: ASSIGN #NEW-NDX := 1
                        }                                                                                                                                                 //Natural: VALUE 2
                        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(2))))
                        {
                            decideConditionsMet212++;
                            ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(4);                                                                                       //Natural: ASSIGN #CNTL-IDX := 4
                            pnd_C_In_Efts.nadd(1);                                                                                                                        //Natural: ADD 1 TO #C-IN-EFTS
                            //*  FE201611
                            if (condition(pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code.equals("0000") || pnd_Ws_Work_Rec_Record_Pnd_Ws_Contract_Hold_Code.equals("    "))) //Natural: IF #WS-WORK-REC-RECORD.#WS-CONTRACT-HOLD-CODE = '0000' OR #WS-WORK-REC-RECORD.#WS-CONTRACT-HOLD-CODE = '    '
                            {
                                pnd_New_Ndx.setValue(2);                                                                                                                  //Natural: ASSIGN #NEW-NDX := 2
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_New_Ndx.setValue(3);                                                                                                                  //Natural: ASSIGN #NEW-NDX := 3
                                //*  FE201611 END
                                //*  GLOBAL PAY
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: VALUE 3
                        else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(3))))
                        {
                            decideConditionsMet212++;
                            ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(4);                                                                                       //Natural: ASSIGN #CNTL-IDX := 4
                            //*  FE201611
                            pnd_C_In_Globals.nadd(1);                                                                                                                     //Natural: ADD 1 TO #C-IN-GLOBALS
                            pnd_New_Ndx.setValue(4);                                                                                                                      //Natural: ASSIGN #NEW-NDX := 4
                        }                                                                                                                                                 //Natural: NONE VALUE
                        else if (condition())
                        {
                            pnd_Abend_Cde.setValue(44);                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 44
                            getReports().write(0, Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Invalid data for","=",pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind); //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Invalid data for' '=' #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                          //Natural: TERMINATE #ABEND-CDE
                            //*  OCCURS REC
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: VALUE 20
                    else if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(20))))
                    {
                        decideConditionsMet202++;
                        //*  NAME/ADDR REC
                        pnd_C_In_Rcrd_20.nadd(1);                                                                                                                         //Natural: ADD 1 TO #C-IN-RCRD-20
                    }                                                                                                                                                     //Natural: VALUE 30
                    else if (condition((pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.equals(30))))
                    {
                        decideConditionsMet202++;
                        pnd_C_In_Rcrd_30.nadd(1);                                                                                                                         //Natural: ADD 1 TO #C-IN-RCRD-30
                    }                                                                                                                                                     //Natural: ANY VALUE
                    if (condition(decideConditionsMet202 > 0))
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-XFILE
                        sub_Write_Xfile();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: NONE VALUE
                    else if (condition())
                    {
                        pnd_Abend_Cde.setValue(45);                                                                                                                       //Natural: ASSIGN #ABEND-CDE := 45
                        getReports().write(0, Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"- Invalid","=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr); //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / '- Invalid' '=' #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                              //Natural: TERMINATE #ABEND-CDE
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue("*",0).nadd(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue("*",1));                      //Natural: ADD #REC-TYPE-CNT ( *,1 ) TO #REC-TYPE-CNT ( *,0 )
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue("*",0).nadd(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue("*",2));                      //Natural: ADD #REC-TYPE-CNT ( *,2 ) TO #REC-TYPE-CNT ( *,0 )
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue("*",0).nadd(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue("*",3));                      //Natural: ADD #REC-TYPE-CNT ( *,3 ) TO #REC-TYPE-CNT ( *,0 )
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(3 + 1,"*").nadd(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(1 + 1,                    //Natural: ADD #REC-TYPE-CNT ( 1,* ) TO #REC-TYPE-CNT ( 3,* )
                    "*"));
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(3 + 1,"*").nadd(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(2 + 1,                    //Natural: ADD #REC-TYPE-CNT ( 2,* ) TO #REC-TYPE-CNT ( 3,* )
                    "*"));
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde().setValue(pnd_Ws_Header_Record_Cntrct_Orgn_Cde);                                                         //Natural: ASSIGN #FCPL720A.#CNTRCT-ORGN-CDE := CNTRCT-ORGN-CDE
                //*  WRITE WORK FILE 5 #FCPL720A
                getReports().write(0, " NUMBER OF RECORDS READ........:",pnd_C_In_Rcrd_All, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                RECORDS TYPE 10:",pnd_C_In_Rcrd_10,  //Natural: WRITE ' NUMBER OF RECORDS READ........:' #C-IN-RCRD-ALL ( EM = -Z,ZZZ,ZZ9 ) / '                RECORDS TYPE 10:' #C-IN-RCRD-10 ( EM = -Z,ZZZ,ZZ9 ) / '                RECORDS TYPE 20:' #C-IN-RCRD-20 ( EM = -Z,ZZZ,ZZ9 ) / '                RECORDS TYPE 30:' #C-IN-RCRD-30 ( EM = -Z,ZZZ,ZZ9 ) / ' INPUT "CASES" BY PAYMENT TYPE.:' / '                        CHECK  :' #C-IN-CHECKS ( EM = -Z,ZZZ,ZZ9 ) / '                        EFT    :' #C-IN-EFTS ( EM = -Z,ZZZ,ZZ9 ) / '                        GLOBAL :' #C-IN-GLOBALS ( EM = -Z,ZZZ,ZZ9 ) /
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                RECORDS TYPE 20:",pnd_C_In_Rcrd_20, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                RECORDS TYPE 30:",pnd_C_In_Rcrd_30, 
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE," INPUT 'CASES' BY PAYMENT TYPE.:",NEWLINE,"                        CHECK  :",pnd_C_In_Checks, 
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                        EFT    :",pnd_C_In_Efts, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,"                        GLOBAL :",pnd_C_In_Globals, 
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE);
                if (Global.isEscape()) return;
                FOR01:                                                                                                                                                    //Natural: FOR #CNTL-IDX = 1 TO 4
                for (ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().setValue(1); condition(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().lessOrEqual(4)); ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().nadd(1))
                {
                    getReports().write(0, NEWLINE,ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Text().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx()));                      //Natural: WRITE / #CNTL-TEXT ( #CNTL-IDX )
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    FOR02:                                                                                                                                                //Natural: FOR #REC-TYPE-IDX = 0 TO 3
                    for (ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().setValue(0); condition(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().lessOrEqual(3)); 
                        ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().nadd(1))
                    {
                        getReports().write(0, new TabSetting(17),ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Types_Text().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx().getInt() + 1),ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().getInt() + 1,ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx()),  //Natural: WRITE 17T #REC-TYPES-TEXT ( #REC-TYPE-IDX ) #REC-TYPE-CNT ( #CNTL-IDX,#REC-TYPE-IDX ) ( EM = -Z,ZZZ,ZZ9 )
                            new ReportEditMask ("-Z,ZZZ,ZZ9"));
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: VALIDATE-FIRST-RECORD
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-XFILE
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Validate_First_Record() throws Exception                                                                                                             //Natural: VALIDATE-FIRST-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  10=HEADER REC
        if (condition(pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr.notEquals(10)))                                                                                     //Natural: IF #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR NE 10
        {
            pnd_Abend_Cde.setValue(43);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 43
            getReports().write(0, Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"REASON: The extract file must start with a record type 10",           //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / 'REASON: The extract file must start with a record type 10' / 'however it starts with a record with' / '=' #WS-WORK-REC-RECORD.#WS-RECORD-LEVEL-NMBR
                NEWLINE,"however it starts with a record with",NEWLINE,"=",pnd_Ws_Work_Rec_Record_Pnd_Ws_Record_Level_Nmbr);
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  VALIDATE-FIRST-RECORD
    }
    private void sub_Write_Xfile() throws Exception                                                                                                                       //Natural: WRITE-XFILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  WRITE RECORDS TO THE PROPER WORK FILE AND ACCUM INTO COUNTERS PER
        //*  WORK FILE BY RECORD TYPE.
        //*  ----------------------------------------------------------------------
        short decideConditionsMet306 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #CNTL-IDX;//Natural: VALUE 1
        if (condition((ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().equals(1))))
        {
            decideConditionsMet306++;
            if (condition(pnd_Ov00_Usps_Sw.getBoolean()))                                                                                                                 //Natural: IF #OV00-USPS-SW
            {
                //*          WRITE WORK FILE 6 #WS-WORK-REC-RECORD   /* FE201703 COMMENT
                //*  FE201703
                getWorkFiles().write(4, false, pnd_Ws_Work_Rec_Record);                                                                                                   //Natural: WRITE WORK FILE 4 #WS-WORK-REC-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(4, false, pnd_Ws_Work_Rec_Record);                                                                                                   //Natural: WRITE WORK FILE 4 #WS-WORK-REC-RECORD
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().equals(2))))
        {
            decideConditionsMet306++;
            getWorkFiles().write(2, false, pnd_Ws_Work_Rec_Record);                                                                                                       //Natural: WRITE WORK FILE 2 #WS-WORK-REC-RECORD
        }                                                                                                                                                                 //Natural: VALUE 4
        else if (condition((ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().equals(4))))
        {
            decideConditionsMet306++;
            //*    WRITE WORK FILE 3 #WS-WORK-REC-RECORD      /* FE201703 COMMENT OUT
            //*  FE201611 START EFT NO HOLD
            if (condition(pnd_New_Ndx.equals(2)))                                                                                                                         //Natural: IF #NEW-NDX = 2
            {
                getWorkFiles().write(8, false, pnd_Ws_Work_Rec_Record);                                                                                                   //Natural: WRITE WORK FILE 8 #WS-WORK-REC-RECORD
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getWorkFiles().write(7, false, pnd_Ws_Work_Rec_Record);                                                                                                   //Natural: WRITE WORK FILE 7 #WS-WORK-REC-RECORD
                //*  FE201611 END
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Abend_Cde.setValue(42);                                                                                                                                   //Natural: ASSIGN #ABEND-CDE := 42
            getReports().write(0, Global.getPROGRAM(),"Terminates with:","=",pnd_Abend_Cde,NEWLINE,"REASON: Program problem - invalid value set to","=",                  //Natural: WRITE *PROGRAM 'Terminates with:' '=' #ABEND-CDE / 'REASON: Program problem - invalid value set to' '=' #CNTL-IDX
                ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx());
            if (Global.isEscape()) return;
            DbsUtil.terminate(pnd_Abend_Cde);  if (true) return;                                                                                                          //Natural: TERMINATE #ABEND-CDE
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Cnt().getValue(ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntl_Idx().getInt() + 1,ldaFcpl720a.getPnd_Fcpl720a_Pnd_Rec_Type_Idx()).nadd(1); //Natural: ADD 1 TO #REC-TYPE-CNT ( #CNTL-IDX,#REC-TYPE-IDX )
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=80 ZP=ON");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(28),ldaFcpl720a.getPnd_Fcpl720a_Pnd_Cntrct_Orgn_Cde(),"EXTRACT CONTROL REPORT",new TabSetting(68),"REPORT: RPT0",NEWLINE,NEWLINE);
    }
}
