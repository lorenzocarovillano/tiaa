/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 08:25:10 PM
**        * FROM NATURAL PROGRAM : Cpup1062
************************************************************
**        * FILE NAME            : Cpup1062.java
**        * CLASS NAME           : Cpup1062
**        * INSTANCE NAME        : Cpup1062
************************************************************
**--------------------------------------------------------------------
** PROGRAM    :  CPUP1052  - CLONED FROM CPWN450D
** APPLICATION:  CONSOLIDATED PAYMENT SYSTEM
**            :  INVESTMENT SOLUTIONS - RETIREE INCOME WEB (DETAIL)
** DATE       :  2011/05/12
** DESCRIPTION:  GET 2 YEARS WORTH OF INFORMATION - MONTHLY / WEEKLY
**            :
** AUTHOR     :  R.CARREON
**            :
********************************
** HISTORY
*  07/17/12 MB  INTERNAL ROLLOVERS ARE NOT TAXABLE EVENTS. DO NOT PASS
*  BRISCOE      A TAXABLE AMOUNT FOR INTERNAL ROLLOVERS. (NON PAYMENTS)
*  07/05/13 B.HOLLOWAY - COMMENT OUT CODE THAT ROLLS UP ALL AMOUNTS FOR
*                        CONTRACTS WITH PYMNT.CNTRCT-ORGN-CDE = 'AP' AND
*                        PYMNT.PYMNT-CMBNE-IND = 'Y' - INC2086318
*                        - CHANGE TAG IS DBH1
*                        RESET TAXABLE AMOUNT FOR ROLLOVERS BASED ON
*                        PYMNT-PAY-TYPE-REQ-IND = 8 OR
*                        CNTRCT-ROLL-DEST-CDE NE ' '
*                        - CHANGE TAG IS DBH3
*  11/21/13 F.ENDAYA   - TRANSFER RMW CPS PAYMENT AND FUNDS RECORDS
*                      - EXTRACT OF ALL UPDATES FOR THE DAY IN THIS
*                      - PROGRAM. FE201311
*  12/13/13 B.HOLLOWAY - ADD LOGIC TO RETRIEVE FCP-CONS-ADDR RECORD BY
*                        PYMNT-REQST-NBR FOR 'OP' RECORDS TO ENSURE THAT
*                        THE CORRECT PAYMENT DESTINATION IS WRITTEN TO
*                        THE EXTRACT FILE
*                        - CHANGE TAG IS DBH4
*  02/19/14 CTS        - TYPO ISSUE NEED TO BE FIXED IN ROLLOVER LOGIC
*                        TO RESOLVE PAYMENT ISSUE REPORTED IN PRB63004
*                         CONDITION  PYMT-PAY-TYPE-REQ-IND = '8'
*                         SHOULD BE CHANGED AS BELOW  :
*                         PYMNT.PYMNT-PAY-TYPE-REQ-IND = 8
*                        - CHANGE TAG IS CTS1
*                      - INC2317434 - CONTROL FILE DATE FIX
*                        CONTROL FILE HAS DATE LOGIC CAUSING THE END
*                        DATE FOR FEBRUARY 1 TO BE JAN 1 INSTEAD OF
*                        JAN 31
*                        - CHANGE TAG IS CTS2
*  3/5/2014 CTS          - INC2300011 - PLAN NUMBER MISMATCH FIX
*                          PAYMENTS WHICH ARE ANNOTATED DON't have
*                          CORRECT PLAN NUMBER & SUB PLAN NUMBER
*                          READ PLAN RECORDS WITH CONTRCT-RCRD-TYPE AS 6
*                        - CHANGE TAG -  CTS3
*  03/20/2014            - MODIFIED TO PREVENT IVC OVERFLOW FOR PLAN
*                          FEE AD PAYMENTS --- TAG JWO2
*  04/02/2014  CTS       - PRB63004 - NEED TO RESET TAXABLE AMOUNT FOR
*                          LOAN PAYMENTS
*                        - DATE RANGE IN CONTROL FILE IS NOT UPDATED
*                          CORRECTLY FOR ADHOC RUN (PCP1050R) IF
*                          EXTRACT IS CREATED BY SPECIFYING DATE RANGE
*                          CHANGE TAG IS --->  CTS4
*  04/15/2014  CTS       - REMOVED THE CALLNAT LOGIC FOR 'CPWNPYTP',
*                          INCLUDED THE REFERENCE TABLE (CPYTP) LOGIC
*                          IN THIS PROGRAM. CHANGE TAG IS --->  CTS5
*  04/30/2014  J.OSTEEN  - ADD THE FOLLOWING FIELDS TO THE ODS FEED
*                            PIN
*                            STATUS-TEXT
*                            ANNOTATION-IND
*                            RESIDENCY
*                            DEDUCTION-GRP
*                              DEDUCTION-CODE
*                              DEDUCTION-AMT
*                            IVC-AMT
*                            NRA-IND
*                          TAG WITH JWO3
*
*  04/30/2014  J.OSTEEN  - FIX GROUP ANNUITY TICKER SYMBOL -- TAG JWO4
*  06/2014     J.OSTEEN  - ADD INTEREST AND ROTH           -- TAG JWO5
*  06/2014     J.OSTEEN  - ADD ANNOTATIONS                 -- TAG JWO6
*  08/2014     J.OSTEEN  - ADD 2 BYTE ACCOUNT CODE         -- TAG JWO7
*  08/2014     J.OSTEEN  - ADD DATA AND PROCESS FOR RCA PYMNTS- TAG JWO8
*  08/2014     J.OSTEEN  - ADD ADDRESS INFO TO LEGACY EFTS -- TAG JWO9
*                          AND RCA PAYMENTS. ALSO ADD      -- TAG JWO9
*                          CONTRACT TO DATA FOR EFTO HOLD  -- TAG JWO9
*  09/2014     J.OSTEEN  - MODIFY NEW-EXT-CNTRL-FND PROCESSING
*                          TO ONLY READ IN FUNDS FOR LEGACY
*                          PAYMENTS                        -- TAG JWO10
*  09/2014     J.OSTEEN  - FIX FUND CODE DEFECT FOR AP PAYMENTS - JWO11
*  10/2014     J.OSTEEN  - FIX DEFECT 84161 ROTH FOR OPPAYMENTS - JWO12
*  10/2014     J.OSTEEN  - FIX DEFECT 90614 PAYMENT INTEREST    - JWO13
*  01/2015     J.OSTEEN  - FIX DEFECT 93037 PAYEE NAME          - JWO14
*  02/2015     RAHUL DAS - RESETTING TAXABLE AMOUNT FOR DIRECT  - RAH
*                          TRANSFER - INC2692196
*  10/2015     J.OSTEEN  - ADD EFCP PROCESSING                  - JWO15
*  12/2015     FENDAYA   - INCLUDE FUND INFORMATION FOR AP AND NZ
*                        - EXTRACT TO RMW.                     FE201509
*  12/2015 SAURAV VIKRAM - MOVING ZERO TO CNTRCT-EXP-AMT FOR    -VIKRAM
*                          CNTRCT-ORGN-CDE = 'AD' REF-PRB72193
* 03/2015  SAURAV VIKRAM - CHANGING THE VALIDATION FOR          -VIKRAM
*                          PYMNT.CNTRCT-ROLL-DEST-CDE FROM NE
*                          ' ' TO EQ 'ROLL' OR 'R457' - REF. INC3082286
* 04/2017  SNEHA SINHA   - FIXING ADDRESS ISSUE FOR OUTBOUND ESP FILE
*                          FOR COMBINED CONTRACTS              - SINHASN
*  05/2017     J.OSTEEN  - FIX STATEMENT ADDRESS ISSUES         - JWO16
* 08/2018  SAURAV VIKRAM - FIXING 2 VALUES OF CHECK-NBR AS 0 AND
*       VALID CHECK-NBR ON 2 DAYS IN 2200 SERIES JOB INTERVAL  - VIKRAM2
* 05/03/2019 CTS    : SAURAV VIKRAM - ADDING LOGIC TO HANDLE ZERO
*                    CHECK NUMBER FOR NZ PAYMENTS     TAG: VIKRAM3
* 11/22/2019 CTS    : RESET THE TAXABLE AMMOUNT FOR AP ROLLOVER PAYMENT
*                     TAG : ROLL
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Cpup1062 extends BLNatBase
{
    // Data Areas
    private GdaMdmg0001 gdaMdmg0001;
    private PdaCpwa452 pdaCpwa452;
    private PdaCpwa453 pdaCpwa453;
    private PdaCpwa454 pdaCpwa454;
    private LdaCpwlfund ldaCpwlfund;
    private LdaFcplpmnt ldaFcplpmnt;
    private PdaTwraplok pdaTwraplok;
    private LdaCpulpym2 ldaCpulpym2;
    private LdaCpulfnd ldaCpulfnd;
    private PdaCpwapytp pdaCpwapytp;
    private LdaCpsl100 ldaCpsl100;
    private LdaCpulannt ldaCpulannt;
    private LdaCpulann2 ldaCpulann2;
    private LdaFcpl448 ldaFcpl448;
    private PdaMdma211 pdaMdma211;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_cmbn_Pay;
    private DbsField cmbn_Pay_Cntrct_Orgn_Cde;
    private DbsField cmbn_Pay_Cntrct_Ppcn_Nbr;
    private DbsField cmbn_Pay_Cntrct_Cmbn_Nbr;
    private DbsField cmbn_Pay_Pymnt_Check_Nbr;
    private DbsField cmbn_Pay_Pymnt_Check_Scrty_Nbr;
    private DbsField cmbn_Pay_Pymnt_Stats_Cde;
    private DbsField cmbn_Pay_Cntrct_Invrse_Dte;
    private DbsField cmbn_Pay_Pymnt_Nbr;
    private DbsField cmbn_Pay_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup cmbn_Pay__R_Field_1;
    private DbsField cmbn_Pay_Pymnt_Prcss_Seq_Num;
    private DbsField cmbn_Pay_Pymnt_Instmt_Nbr;

    private DbsGroup cmbn_Pay_Inv_Acct;
    private DbsField cmbn_Pay_Inv_Acct_Settl_Amt;
    private DbsField cmbn_Pay_Inv_Acct_Dvdnd_Amt;
    private DbsField cmbn_Pay_Inv_Acct_Ivc_Amt;
    private DbsField cmbn_Pay_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField cmbn_Pay_Inv_Acct_State_Tax_Amt;
    private DbsField cmbn_Pay_Inv_Acct_Local_Tax_Amt;

    private DbsGroup cmbn_Pay_Inv_Acct_Part_2;
    private DbsField cmbn_Pay_Inv_Acct_Can_Tax_Amt;
    private DbsField cmbn_Pay_Inv_Acct_Exp_Amt;
    private DbsField cmbn_Pay_Inv_Acct_Net_Pymnt_Amt;

    private DataAccessProgramView vw_pymnt;
    private DbsField pymnt_Cntrct_Ppcn_Nbr;
    private DbsField pymnt_Cntrct_Payee_Cde;
    private DbsField pymnt_Cntrct_Invrse_Dte;
    private DbsField pymnt_Cntrct_Check_Crrncy_Cde;
    private DbsField pymnt_Cntrct_Crrncy_Cde;
    private DbsField pymnt_Cntrct_Orgn_Cde;
    private DbsField pymnt_Cntrct_Hold_Cde;
    private DbsField pymnt_Cntrct_Pymnt_Type_Ind;
    private DbsField pymnt_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pymnt_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pymnt_Pymnt_Stats_Cde;
    private DbsField pymnt_Pymnt_Pay_Type_Req_Ind;
    private DbsField pymnt_Pymnt_Annot_Ind;
    private DbsField pymnt_Annt_Rsdncy_Cde;
    private DbsField pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField pymnt_Cntrct_Type_Cde;
    private DbsField pymnt_Cntrct_Lob_Cde;
    private DbsField pymnt_Cntrct_Sub_Lob_Cde;
    private DbsField pymnt_Cntrct_Ia_Lob_Cde;
    private DbsField pymnt_Cntrct_Option_Cde;
    private DbsField pymnt_Cntrct_Mode_Cde;
    private DbsField pymnt_Cntrct_Roll_Dest_Cde;
    private DbsField pymnt_Annt_Soc_Sec_Nbr;
    private DbsField pymnt_Pymnt_Split_Reasn_Cde;
    private DbsField pymnt_Pymnt_Check_Scrty_Nbr;
    private DbsField pymnt_Pymnt_Check_Nbr;
    private DbsField pymnt_Pymnt_Check_Dte;

    private DbsGroup pymnt__R_Field_2;
    private DbsField pymnt_Pymnt_Dte;
    private DbsField pymnt_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pymnt__R_Field_3;
    private DbsField pymnt_Pymnt_Prcss_Seq_Num;
    private DbsField pymnt_Pymnt_Instmt_Nbr;
    private DbsField pymnt_Pymnt_Settlmnt_Dte;
    private DbsField pymnt_Count_Castinv_Acct;

    private DbsGroup pymnt_Inv_Acct;
    private DbsField pymnt_Inv_Acct_Cde;
    private DbsField pymnt_Inv_Acct_Unit_Qty;
    private DbsField pymnt_Inv_Acct_Unit_Value;
    private DbsField pymnt_Inv_Acct_Settl_Amt;
    private DbsField pymnt_Inv_Acct_Fed_Cde;
    private DbsField pymnt_Inv_Acct_State_Cde;
    private DbsField pymnt_Inv_Acct_Local_Cde;
    private DbsField pymnt_Inv_Acct_Ivc_Amt;
    private DbsField pymnt_Inv_Acct_Dci_Amt;
    private DbsField pymnt_Inv_Acct_Dpi_Amt;
    private DbsField pymnt_Inv_Acct_Start_Accum_Amt;
    private DbsField pymnt_Inv_Acct_End_Accum_Amt;
    private DbsField pymnt_Inv_Acct_Dvdnd_Amt;
    private DbsField pymnt_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pymnt_Inv_Acct_Ivc_Ind;
    private DbsField pymnt_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pymnt_Inv_Acct_Valuat_Period;
    private DbsField pymnt_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pymnt_Inv_Acct_State_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Local_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Exp_Amt;

    private DbsGroup pymnt_Inv_Acct_Part_2;
    private DbsField pymnt_Inv_Acct_Can_Tax_Amt;
    private DbsField pymnt_Count_Castpymnt_Ded_Grp;

    private DbsGroup pymnt_Pymnt_Ded_Grp;
    private DbsField pymnt_Pymnt_Ded_Cde;
    private DbsField pymnt_Pymnt_Ded_Payee_Cde;
    private DbsField pymnt_Pymnt_Ded_Amt;
    private DbsField pymnt_Count_Castinv_Rtb_Grp;

    private DbsGroup pymnt_Inv_Rtb_Grp;
    private DbsField pymnt_Inv_Rtb_Cde;
    private DbsField pymnt_Inv_Acct_Fed_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Sta_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Loc_Dci_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Fed_Dpi_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Sta_Dpi_Tax_Amt;
    private DbsField pymnt_Inv_Acct_Loc_Dpi_Tax_Amt;
    private DbsField pymnt_Pymnt_Spouse_Pay_Stats;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pymnt_Cntrct_Cancel_Rdrw_Amt;
    private DbsField pymnt_Cnr_Orgnl_Invrse_Dte;
    private DbsField pymnt_Cnr_Orgnl_Prcss_Seq_Nbr;
    private DbsField pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte;
    private DbsField pymnt_Cnr_Orgnl_Pymnt_Nbr;
    private DbsField pymnt_Cntrct_Annty_Ins_Type;
    private DbsField pymnt_Cntrct_Annty_Type_Cde;
    private DbsField pymnt_Cntrct_Insurance_Option;
    private DbsField pymnt_Cntrct_Life_Contingency;
    private DbsField pymnt_Invrse_Process_Dte;
    private DbsField pymnt_Pymnt_Nbr;
    private DbsField pymnt_Pymnt_Reqst_Nbr;

    private DbsGroup pymnt__R_Field_4;
    private DbsField pymnt_Pymnt_Orgn_Cde;
    private DbsField pymnt_Pymnt_Term_Id;
    private DbsField pymnt_Pymnt_User_Id;
    private DbsField pymnt_Pymnt_Seq_Nbr;
    private DbsField pymnt_Nbr_Of_Funds;
    private DbsField pymnt_Cntrct_Dvdnd_Amt;
    private DbsField pymnt_Cntrct_Settl_Amt;
    private DbsField pymnt_Cntrct_Net_Pymnt_Amt;
    private DbsField pymnt_Cntrct_Exp_Amt;
    private DbsField pymnt_Cntrct_Ivc_Amt;
    private DbsField pymnt_Cntrct_Dci_Amt;
    private DbsField pymnt_Cntrct_Dpi_Amt;
    private DbsField pymnt_Cntrct_Fdrl_Tax_Amt;
    private DbsField pymnt_Cntrct_State_Tax_Amt;
    private DbsField pymnt_Cntrct_Can_Tax_Amt;
    private DbsField pymnt_Cntrct_Local_Tax_Amt;
    private DbsField pymnt_Cntrct_Adj_Ivc_Amt;
    private DbsField pymnt_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pymnt_Annt_Ctznshp_Cde;
    private DbsField pymnt_Pymnt_Instlmnt_Nbr;
    private DbsField pymnt_Bank_Account;
    private DbsField pymnt_Bank_Routing;
    private DbsField pymnt_Pymnt_Cmbne_Ind;
    private DbsField pymnt_Cntrct_Cmbn_Nbr;
    private DbsField pymnt_Notused1;
    private DbsField pymnt_Plan_Cnt;
    private DbsField pymnt_Roth_Money_Source;
    private DbsField pymnt_Roth_First_Contrib_Dte;
    private DbsField pymnt_Pymnt_Eft_Dte;
    private DbsField pymnt_Advisor_Pin;
    private DbsField pymnt_Cntrct_Da_Cref_1_Nbr;
    private DbsField pymnt_Cntrct_Da_Cref_2_Nbr;

    private DataAccessProgramView vw_plan_View;
    private DbsField plan_View_Plan_Num;
    private DbsField plan_View_Sub_Plan_Num;

    private DbsGroup plan_View_Plan_Data;
    private DbsField plan_View_Plan_Employer_Name;
    private DbsField plan_View_Plan_Type;
    private DbsField plan_View_Plan_Cash_Avail;
    private DbsField plan_View_Pymnt_Reqst_Nbr;
    private DbsField plan_View_Cntrct_Rcrd_Typ;
    private DbsField plan_View_Roth_Contrb;
    private DbsField plan_View_Roth_Earnings;

    private DataAccessProgramView vw_addr;
    private DbsField addr_Cntrct_Orgn_Cde;
    private DbsField addr_Cntrct_Ppcn_Nbr;
    private DbsField addr_Cntrct_Payee_Cde;
    private DbsField addr_Cntrct_Invrse_Dte;
    private DbsField addr_Pymnt_Prcss_Seq_Nbr;
    private DbsField addr_Ph_Last_Name;
    private DbsField addr_Ph_First_Name;
    private DbsField addr_Ph_Middle_Name;

    private DbsGroup addr_Pymnt_Nme_And_Addr_Grp;
    private DbsField addr_Pymnt_Nme;
    private DbsField addr_Pymnt_Addr_Line1_Txt;
    private DbsField addr_Pymnt_Addr_Line2_Txt;
    private DbsField addr_Pymnt_Addr_Line3_Txt;
    private DbsField addr_Pymnt_Addr_Line4_Txt;
    private DbsField addr_Pymnt_Addr_Line5_Txt;
    private DbsField addr_Pymnt_Addr_Line6_Txt;
    private DbsField addr_Pymnt_Eft_Transit_Id;
    private DbsField addr_Pymnt_Eft_Acct_Nbr;
    private DbsField addr_Pymnt_Chk_Sav_Ind;
    private DbsField pnd_Addr_Key;

    private DbsGroup pnd_Addr_Key__R_Field_5;
    private DbsField pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde;
    private DbsField pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde;
    private DbsField pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte;
    private DbsField pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Detail;
    private DbsField pnd_Detail_Pnd_Taxable;
    private DbsField pnd_Detail_Pnd_Gross;
    private DbsField pnd_Detail_Pnd_Ivc;
    private DbsField pnd_Detail_Pnd_Tax;
    private DbsField pnd_Detail_Pnd_Deduc;
    private DbsField pnd_Detail_Pnd_Int;
    private DbsField pnd_Detail_Pnd_Net;
    private DbsField pnd_Detail_Pnd_Qty;
    private DbsField pnd_Detail_Pnd_Value;

    private DbsGroup pnd_Total_Control;
    private DbsField pnd_Total_Control_Pnd_T_Gross;
    private DbsField pnd_Total_Control_Pnd_T_Taxable;
    private DbsField pnd_Total_Control_Pnd_T_Ivc;
    private DbsField pnd_Total_Control_Pnd_T_Tax;
    private DbsField pnd_Total_Control_Pnd_T_Deduc;
    private DbsField pnd_Total_Control_Pnd_T_Int;
    private DbsField pnd_Total_Control_Pnd_T_Net;
    private DbsField pnd_Total_Control_Pnd_T_Qty;
    private DbsField pnd_Total_Control_Pnd_T_Value;
    private DbsField pnd_Pymnt_Split_Reasn_Cde;

    private DbsGroup pnd_Pymnt_Split_Reasn_Cde__R_Field_6;
    private DbsField pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde;
    private DbsField pnd_Pymnt_Split_Reasn_Cde_Pnd_Pymnt_Init_Rqst_Ind;
    private DbsField pnd_Sum_Int_Ind;
    private DbsField pnd_Cntrct_Payee_Cde_A4;

    private DbsGroup pnd_Cntrct_Payee_Cde_A4__R_Field_7;
    private DbsField pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde_A2;

    private DbsGroup pnd_Cntrct_Payee_Cde_A4__R_Field_8;
    private DbsField pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde;

    private DbsGroup pnd_Ia_Fields;
    private DbsField pnd_Ia_Fields_Pnd_Ia_Last_Pymt_Dte;
    private DbsField pnd_Ia_Fields_Pnd_Ia_Next_Pymt_Dte;
    private DbsField pnd_Ia_Fields_Pnd_Ia_Next_Fctr_Dte;
    private DbsField pnd_Ia_Fields_Pnd_Ia_Msg;
    private DbsField pnd_Next_Pymnt_Date;
    private DbsField pnd_Special_Pymnt_Type;
    private DbsField pnd_Isn;
    private DbsField pnd_D;
    private DbsField pnd_R;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_9;
    private DbsField pnd_Date_Pnd_Datn;

    private DbsGroup pnd_Date__R_Field_10;
    private DbsField pnd_Date_Pnd_Date_Yyyy;
    private DbsField pnd_Date_Pnd_Date_Mm;
    private DbsField pnd_Date_Pnd_Date_Dd;
    private DbsField pnd_Date_Fr;

    private DbsGroup pnd_Date_Fr__R_Field_11;
    private DbsField pnd_Date_Fr_Pnd_Datn_Fr;
    private DbsField pnd_Date_To;

    private DbsGroup pnd_Date_To__R_Field_12;
    private DbsField pnd_Date_To_Pnd_Datn_To;
    private DbsField pnd_Ppcn_Inv_Orgn_Prcss_Fr;

    private DbsGroup pnd_Ppcn_Inv_Orgn_Prcss_Fr__R_Field_13;
    private DbsField pnd_Ppcn_Inv_Orgn_Prcss_Fr_Pnd_Cntrct_Ppcn_Nbr_Fr;
    private DbsField pnd_Ppcn_Inv_Orgn_Prcss_Fr_Pnd_Cntrct_Invrse_Dte_Fr;
    private DbsField pnd_Ppcn_Inv_Orgn_Prcss_To;

    private DbsGroup pnd_Ppcn_Inv_Orgn_Prcss_To__R_Field_14;
    private DbsField pnd_Ppcn_Inv_Orgn_Prcss_To_Pnd_Cntrct_Ppcn_Nbr_To;
    private DbsField pnd_Ppcn_Inv_Orgn_Prcss_To_Pnd_Cntrct_Invrse_Dte_To;
    private DbsField pnd_Pymnt_Reqst_Instmt_Seq;

    private DbsGroup pnd_Pymnt_Reqst_Instmt_Seq__R_Field_15;
    private DbsField pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr;
    private DbsField pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr;
    private DbsField pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Inv_Acct_Seq_Nbr;

    private DbsGroup pnd_Work_File;
    private DbsField pnd_Work_File_Pnd_Ppcn;
    private DbsField pnd_Work_File_Pnd_Ssn;
    private DbsField pnd_Work_File_Pnd_Pin;
    private DbsField pnd_Work_File_Pnd_Orgn;
    private DbsField pnd_Work_File_Pnd_Start;
    private DbsField pnd_Work_File_Pnd_End;
    private DbsField pnd_Data_Count;
    private DbsField pnd_Control_Count;
    private DbsField pnd_Insert_Count;
    private DbsField pnd_W_Date_X;
    private DbsField pnd_W_Datx;

    private DbsGroup pnd_W_Datx__R_Field_16;
    private DbsField pnd_W_Datx_Pnd_W_Datn;
    private DbsField pnd_Parm;
    private DbsField quo;

    private DbsGroup quo__R_Field_17;
    private DbsField quo_Quo_1;
    private DbsField quo_Rem;
    private DbsField pnd_Date_Start;

    private DbsGroup pnd_Date_Start__R_Field_18;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Ccyy;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Mm;
    private DbsField pnd_Date_Start_Pnd_Date_Strt_Dd;

    private DbsGroup pnd_Date_Start__R_Field_19;
    private DbsField pnd_Date_Start_Pnd_Date_Start_A;
    private DbsField pnd_Date_End;

    private DbsGroup pnd_Date_End__R_Field_20;
    private DbsField pnd_Date_End_Pnd_Date_End_A;

    private DbsGroup pnd_Date_End__R_Field_21;
    private DbsField pnd_Date_End_Pnd_Date_End_Ccyy;
    private DbsField pnd_Date_End_Pnd_Date_End_Mm;
    private DbsField pnd_Date_End_Pnd_Date_End_Dd;
    private DbsField pnd_Start_Invrse_Date;
    private DbsField pnd_End_Invrse_Date;
    private DbsField cpua1051_Detail;

    private DbsGroup cpua1051_Detail__R_Field_22;
    private DbsField cpua1051_Detail_Cntrct_Ppcn_Nbr;
    private DbsField cpua1051_Detail_Pymt_Payee_Code;
    private DbsField cpua1051_Detail_Annt_Soc_Sec_Nbr;
    private DbsField cpua1051_Detail_Annt_Pin;
    private DbsField cpua1051_Detail_Pymt_Annt_Name;
    private DbsField cpua1051_Detail_Pymt_Date_From;
    private DbsField cpua1051_Detail_Pymt_Date_To;
    private DbsField cpua1051_Detail_Pymt_Orgn_Cde;
    private DbsField cpua1051_Detail_Pymt_Cnr_Ind;
    private DbsField cpua1051_Detail_Pymt_Type;
    private DbsField cpua1051_Detail_Lob_Code;
    private DbsField cpua1051_Detail_Option_Code;
    private DbsField cpua1051_Detail_Type_Code;
    private DbsField cpua1051_Detail_Pymt_Calc_Pymt_Date;
    private DbsField cpua1051_Detail_Pymt_Next_Pymt_Date;
    private DbsField cpua1051_Detail_Pymt_Freq_Ind;
    private DbsField cpua1051_Detail_Pymt_Check_Pymt_Date;
    private DbsField cpua1051_Detail_Pymt_Ticker;
    private DbsField cpua1051_Detail_Pymt_Gross_Amt;
    private DbsField cpua1051_Detail_Pymt_Settl_Amt;
    private DbsField cpua1051_Detail_Pymt_Dvdnd_Amt;
    private DbsField cpua1051_Detail_Pymt_Ivc_Amt;
    private DbsField cpua1051_Detail_Pymt_Unit_Qty;
    private DbsField cpua1051_Detail_Pymt_Unit_Value;
    private DbsField cpua1051_Detail_Pymt_Tax_Amt;
    private DbsField cpua1051_Detail_Pymt_Deduc_Amt;
    private DbsField cpua1051_Detail_Pymt_Net_Amt;
    private DbsField cpua1051_Detail_Pymt_Int_Amt;
    private DbsField cpua1051_Detail_Pymt_Taxable;
    private DbsField cpua1051_Detail_Pymt_Pay_Type_Req_Ind;
    private DbsField cpua1051_Detail_Pymt_Int_Ind;
    private DbsField cpua1051_Detail_Pymt_Stats_Cde;
    private DbsField cpua1051_Detail_Pymt_Spouse_Pay_Stats;
    private DbsField cpua1051_Detail_Pynt_Cntrct_Roll_Dest_Cde;
    private DbsField cpua1051_Detail_Pynt_Cntrct_Annty_Ins_Type;
    private DbsField cpua1051_Detail_Pynt_Cntrct_Pymnt_Type_Ind;
    private DbsField cpua1051_Detail_Pynt_Cntrct_Sttlmnt_Type_Ind;
    private DbsField cpua1051_Detail_Pynt_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField cpua1051_Detail_Pynt_Check_Nbr;
    private DbsField cpua1051_Detail_Pynt_Settlmnt_Dte;

    private DataAccessProgramView vw_new_Ext_Cntrl_Fnd_View;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Table_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Functional_Cmpny;
    private DbsField new_Ext_Cntrl_Fnd_View_Nec_Ia_Fund_Cde;

    private DbsGroup pnd_Tick_Array;
    private DbsField pnd_Tick_Array_Pnd_Acct_Alpha;
    private DbsField pnd_Tick_Array_Pnd_Tick;
    private DbsField pnd_Index;
    private DbsField pnd_Acct_Alpha_1;
    private DbsField pnd_Ws_Prime_Key;

    private DbsGroup pnd_Ws_Prime_Key__R_Field_23;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Prime_Key_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Prime_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Prime_Key__R_Field_24;
    private DbsField pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Prime_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Vtran_Id;

    private DbsGroup pnd_Vtran_Id__R_Field_25;
    private DbsField pnd_Vtran_Id_Pnd_Vtd_Rec_Code;
    private DbsField pnd_Vtran_Id_Pnd_Vtd_35;
    private DbsField pnd_Vtran_Id_Pnd_Vtd_10;
    private DbsField pnd_Vtran_Id_Pnd_Vtd_6;
    private DbsField pnd_Vtran_Id_Pnd_Vtd_Filler;
    private DbsField pnd_Vtran_Id_Pnd_Vtd_Seq;

    private DataAccessProgramView vw_ref;

    private DbsGroup ref_Rt_Record;
    private DbsField ref_Rt_A_I_Ind;
    private DbsField ref_Rt_Table_Id;
    private DbsField ref_Rt_Short_Key;
    private DbsField pnd_Input_Isn;

    private DbsGroup pnd_Input_Isn__R_Field_26;
    private DbsField pnd_Input_Isn_Pnd_Input_Isn_A;
    private DbsField pnd_Rollover_Sw;
    private DbsField pnd_Reqst_Nbr_Rec_Type;

    private DbsGroup pnd_Reqst_Nbr_Rec_Type__R_Field_27;
    private DbsField pnd_Reqst_Nbr_Rec_Type_Pnd_Reqst_Nbr;
    private DbsField pnd_Reqst_Nbr_Rec_Type_Pnd_Rcrd_Typ;
    private DbsField pnd_Loan_Pymnt_Sw;
    private DbsField pnd_Rt_Super1;

    private DbsGroup pnd_Rt_Super1__R_Field_28;
    private DbsField pnd_Rt_Super1_A_I_Ind;
    private DbsField pnd_Rt_Super1_Table_Id;
    private DbsField pnd_Rt_Super1_Short_Key;
    private DbsField pnd_Rt_Super1_Long_Key;
    private DbsField pnd_Short_Key_Array;
    private DbsField pnd_Long_Key_Array;
    private DbsField pnd_Prefix_Array;
    private DbsField pnd_Short_Key;

    private DbsGroup pnd_Short_Key__R_Field_29;
    private DbsField pnd_Short_Key_Pnd_Rf_Pay_Type;
    private DbsField pnd_Short_Key_Pnd_Rf_Roll_Dest_Cde;
    private DbsField pnd_Short_Key_Pnd_Rf_Orgn;
    private DbsField pnd_Short_Key_Pnd_Rf_Pymnt_Type;
    private DbsField pnd_Short_Key_Pnd_Rf_Settl_Type;
    private DbsField pnd_I;
    private DbsField pnd_J;
    private DbsField pnd_Pos;
    private DbsField pnd_Prefix;
    private DbsField pnd_Roth_First_Contrib_Dte;
    private DbsField pnd_Roth_First_Contrib_Dte_N;

    private DbsGroup pnd_Roth_First_Contrib_Dte_N__R_Field_30;
    private DbsField pnd_Roth_First_Contrib_Dte_N_Pnd_Roth_First_Contrib_Dte_A;

    private DbsGroup pnd_Roth_First_Contrib_Dte_N__R_Field_31;
    private DbsField pnd_Roth_First_Contrib_Dte_N_Roth_Cc;
    private DbsField pnd_Roth_First_Contrib_Dte_N_Roth_Yy;
    private DbsField pnd_Roth_First_Contrib_Dte_N_Roth_Mm;
    private DbsField pnd_Roth_First_Contrib_Dte_N_Roth_Dd;
    private DbsField pnd_Annot_Key;

    private DbsGroup pnd_Annot_Key__R_Field_32;
    private DbsField pnd_Annot_Key_Pnd_Annot_Ppcn;
    private DbsField pnd_Annot_Key_Pnd_Annot_Invrs;
    private DbsField pnd_Annot_Key_Pnd_Annot_Orgn;
    private DbsField pnd_Annot_Key_Pnd_Annot_Seq;
    private DbsField pnd_Annot_Key_Pnd_Annot_Inst;
    private DbsField pnd_Annot_Key2;

    private DbsGroup pnd_Annot_Key2__R_Field_33;
    private DbsField pnd_Annot_Key2_Pnd_Annot_Pymnt_Reqst_Nbr;
    private DbsField pnd_Annot_Key2_Pnd_Annot_Rec_Type;

    private DataAccessProgramView vw_fcp_Cons_Annot1;
    private DbsField fcp_Cons_Annot1_Cntrct_Rcrd_Typ;
    private DbsField fcp_Cons_Annot1_Pymnt_Rmrk_Line1_Txt;
    private DbsField fcp_Cons_Annot1_Pymnt_Rmrk_Line2_Txt;
    private DbsField fcp_Cons_Annot1_Count_Castpymnt_Annot_Grp;

    private DbsGroup fcp_Cons_Annot1_Pymnt_Annot_Grp;
    private DbsField fcp_Cons_Annot1_Pymnt_Annot_Dte;
    private DbsField fcp_Cons_Annot1_Pymnt_Annot_Id_Nbr;
    private DbsField fcp_Cons_Annot1_Pymnt_Annot_Flag_Ind;
    private DbsField fcp_Cons_Annot1_Count_Castpymnt_Annot_Expand_Grp;

    private DbsGroup fcp_Cons_Annot1_Pymnt_Annot_Expand_Grp;
    private DbsField fcp_Cons_Annot1_Pymnt_Annot_Expand_Cmnt;
    private DbsField fcp_Cons_Annot1_Pymnt_Annot_Expand_Date;
    private DbsField fcp_Cons_Annot1_Pymnt_Annot_Expand_Time;
    private DbsField fcp_Cons_Annot1_Pymnt_Annot_Expand_Uid;
    private DbsField pnd_Eft_Eff_Date;
    private DbsField dte;

    private DbsGroup dte__R_Field_34;
    private DbsField dte_Mm;
    private DbsField dte_Hypen;
    private DbsField dte_Day;
    private DbsField pnd_Rc;
    private DbsField pnd_I2;
    private DbsField pnd_Flag_Check_Nbr_0;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        gdaMdmg0001 = GdaMdmg0001.getInstance(getCallnatLevel());
        registerRecord(gdaMdmg0001);
        if (gdaOnly) return;

        localVariables = new DbsRecord();
        pdaCpwa452 = new PdaCpwa452(localVariables);
        pdaCpwa453 = new PdaCpwa453(localVariables);
        pdaCpwa454 = new PdaCpwa454(localVariables);
        ldaCpwlfund = new LdaCpwlfund();
        registerRecord(ldaCpwlfund);
        registerRecord(ldaCpwlfund.getVw_cps_Fund());
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        pdaTwraplok = new PdaTwraplok(localVariables);
        ldaCpulpym2 = new LdaCpulpym2();
        registerRecord(ldaCpulpym2);
        ldaCpulfnd = new LdaCpulfnd();
        registerRecord(ldaCpulfnd);
        pdaCpwapytp = new PdaCpwapytp(localVariables);
        ldaCpsl100 = new LdaCpsl100();
        registerRecord(ldaCpsl100);
        registerRecord(ldaCpsl100.getVw_rt());
        ldaCpulannt = new LdaCpulannt();
        registerRecord(ldaCpulannt);
        ldaCpulann2 = new LdaCpulann2();
        registerRecord(ldaCpulann2);
        ldaFcpl448 = new LdaFcpl448();
        registerRecord(ldaFcpl448);
        pdaMdma211 = new PdaMdma211(localVariables);

        // Local Variables

        vw_cmbn_Pay = new DataAccessProgramView(new NameInfo("vw_cmbn_Pay", "CMBN-PAY"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));
        cmbn_Pay_Cntrct_Orgn_Cde = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        cmbn_Pay_Cntrct_Ppcn_Nbr = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        cmbn_Pay_Cntrct_Cmbn_Nbr = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        cmbn_Pay_Pymnt_Check_Nbr = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_NBR");
        cmbn_Pay_Pymnt_Check_Scrty_Nbr = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SCRTY_NBR");
        cmbn_Pay_Pymnt_Stats_Cde = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        cmbn_Pay_Cntrct_Invrse_Dte = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        cmbn_Pay_Pymnt_Nbr = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        cmbn_Pay_Pymnt_Prcss_Seq_Nbr = vw_cmbn_Pay.getRecord().newFieldInGroup("cmbn_Pay_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        cmbn_Pay__R_Field_1 = vw_cmbn_Pay.getRecord().newGroupInGroup("cmbn_Pay__R_Field_1", "REDEFINE", cmbn_Pay_Pymnt_Prcss_Seq_Nbr);
        cmbn_Pay_Pymnt_Prcss_Seq_Num = cmbn_Pay__R_Field_1.newFieldInGroup("cmbn_Pay_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        cmbn_Pay_Pymnt_Instmt_Nbr = cmbn_Pay__R_Field_1.newFieldInGroup("cmbn_Pay_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);

        cmbn_Pay_Inv_Acct = vw_cmbn_Pay.getRecord().newGroupInGroup("cmbn_Pay_Inv_Acct", "INV-ACCT", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT");
        cmbn_Pay_Inv_Acct_Settl_Amt = cmbn_Pay_Inv_Acct.newFieldArrayInGroup("cmbn_Pay_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_SETTL_AMT", "FCP_CONS_PYMT_INV_ACCT");
        cmbn_Pay_Inv_Acct_Dvdnd_Amt = cmbn_Pay_Inv_Acct.newFieldArrayInGroup("cmbn_Pay_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DVDND_AMT", "FCP_CONS_PYMT_INV_ACCT");
        cmbn_Pay_Inv_Acct_Ivc_Amt = cmbn_Pay_Inv_Acct.newFieldArrayInGroup("cmbn_Pay_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_IVC_AMT", "FCP_CONS_PYMT_INV_ACCT");
        cmbn_Pay_Inv_Acct_Fdrl_Tax_Amt = cmbn_Pay_Inv_Acct.newFieldArrayInGroup("cmbn_Pay_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FDRL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        cmbn_Pay_Inv_Acct_State_Tax_Amt = cmbn_Pay_Inv_Acct.newFieldArrayInGroup("cmbn_Pay_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STATE_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        cmbn_Pay_Inv_Acct_Local_Tax_Amt = cmbn_Pay_Inv_Acct.newFieldArrayInGroup("cmbn_Pay_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOCAL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");

        cmbn_Pay_Inv_Acct_Part_2 = vw_cmbn_Pay.getRecord().newGroupInGroup("cmbn_Pay_Inv_Acct_Part_2", "INV-ACCT-PART-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT_PART_2");
        cmbn_Pay_Inv_Acct_Can_Tax_Amt = cmbn_Pay_Inv_Acct_Part_2.newFieldArrayInGroup("cmbn_Pay_Inv_Acct_Can_Tax_Amt", "INV-ACCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CAN_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT_PART_2");
        cmbn_Pay_Inv_Acct_Exp_Amt = vw_cmbn_Pay.getRecord().newFieldArrayInGroup("cmbn_Pay_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_EXP_AMT", "FCP_CONS_PYMT_INV_ACCT");
        cmbn_Pay_Inv_Acct_Net_Pymnt_Amt = vw_cmbn_Pay.getRecord().newFieldArrayInGroup("cmbn_Pay_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_NET_PYMNT_AMT", "FCP_CONS_PYMT_INV_ACCT");
        registerRecord(vw_cmbn_Pay);

        vw_pymnt = new DataAccessProgramView(new NameInfo("vw_pymnt", "PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PYMNT"));
        pymnt_Cntrct_Ppcn_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        pymnt_Cntrct_Payee_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        pymnt_Cntrct_Invrse_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        pymnt_Cntrct_Check_Crrncy_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CHECK_CRRNCY_CDE");
        pymnt_Cntrct_Crrncy_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_CRRNCY_CDE");
        pymnt_Cntrct_Orgn_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        pymnt_Cntrct_Hold_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_CDE");
        pymnt_Cntrct_Pymnt_Type_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_PYMNT_TYPE_IND");
        pymnt_Cntrct_Sttlmnt_Type_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_STTLMNT_TYPE_IND");
        pymnt_Pymnt_Rqst_Rmndr_Pct = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Rqst_Rmndr_Pct", "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_RQST_RMNDR_PCT");
        pymnt_Pymnt_Stats_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        pymnt_Pymnt_Pay_Type_Req_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        pymnt_Pymnt_Annot_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_ANNOT_IND");
        pymnt_Annt_Rsdncy_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "ANNT_RSDNCY_CDE");
        pymnt_Cntrct_Unq_Id_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12, RepeatingFieldStrategy.None, 
            "CNTRCT_UNQ_ID_NBR");
        pymnt_Cntrct_Type_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_TYPE_CDE");
        pymnt_Cntrct_Lob_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_LOB_CDE");
        pymnt_Cntrct_Sub_Lob_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Sub_Lob_Cde", "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_SUB_LOB_CDE");
        pymnt_Cntrct_Ia_Lob_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_IA_LOB_CDE");
        pymnt_Cntrct_Option_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_OPTION_CDE");
        pymnt_Cntrct_Mode_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_MODE_CDE");
        pymnt_Cntrct_Roll_Dest_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Roll_Dest_Cde", "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_ROLL_DEST_CDE");
        pymnt_Annt_Soc_Sec_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "ANNT_SOC_SEC_NBR");
        pymnt_Pymnt_Split_Reasn_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Split_Reasn_Cde", "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6, 
            RepeatingFieldStrategy.None, "PYMNT_SPLIT_REASN_CDE");
        pymnt_Pymnt_Check_Scrty_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Scrty_Nbr", "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_CHECK_SCRTY_NBR");
        pymnt_Pymnt_Check_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_NBR");
        pymnt_Pymnt_Check_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");

        pymnt__R_Field_2 = vw_pymnt.getRecord().newGroupInGroup("pymnt__R_Field_2", "REDEFINE", pymnt_Pymnt_Check_Dte);
        pymnt_Pymnt_Dte = pymnt__R_Field_2.newFieldInGroup("pymnt_Pymnt_Dte", "PYMNT-DTE", FieldType.DATE);
        pymnt_Pymnt_Prcss_Seq_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_PRCSS_SEQ_NBR");

        pymnt__R_Field_3 = vw_pymnt.getRecord().newGroupInGroup("pymnt__R_Field_3", "REDEFINE", pymnt_Pymnt_Prcss_Seq_Nbr);
        pymnt_Pymnt_Prcss_Seq_Num = pymnt__R_Field_3.newFieldInGroup("pymnt_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pymnt_Pymnt_Instmt_Nbr = pymnt__R_Field_3.newFieldInGroup("pymnt_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pymnt_Pymnt_Settlmnt_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_SETTLMNT_DTE");
        pymnt_Count_Castinv_Acct = vw_pymnt.getRecord().newFieldInGroup("pymnt_Count_Castinv_Acct", "C*INV-ACCT", RepeatingFieldStrategy.CAsteriskVariable, 
            "FCP_CONS_PYMT_INV_ACCT");

        pymnt_Inv_Acct = vw_pymnt.getRecord().newGroupArrayInGroup("pymnt_Inv_Acct", "INV-ACCT", new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Cde = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Unit_Qty = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 9, 3, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_UNIT_QTY", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Unit_Value = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 9, 4, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_UNIT_VALUE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Settl_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 11, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_SETTL_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Fed_Cde = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_FED_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_State_Cde = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_STATE_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Local_Cde = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_LOCAL_CDE", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Ivc_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_IVC_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Dci_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_DCI_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Dpi_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_DPI_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Start_Accum_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_START_ACCUM_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_End_Accum_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_END_ACCUM_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Dvdnd_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_DVDND_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Net_Pymnt_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_NET_PYMNT_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Ivc_Ind = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_IVC_IND", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Adj_Ivc_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 
            2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_ADJ_IVC_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Valuat_Period = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 1, null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_VALUAT_PERIOD", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Valuat_Period.setDdmHeader("RE-/VALUATION/PERIOD");
        pymnt_Inv_Acct_Fdrl_Tax_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FDRL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_State_Tax_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STATE_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Local_Tax_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOCAL_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT");
        pymnt_Inv_Acct_Exp_Amt = pymnt_Inv_Acct.newFieldInGroup("pymnt_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 9, 2, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "INV_ACCT_EXP_AMT", "FCP_CONS_PYMT_INV_ACCT");

        pymnt_Inv_Acct_Part_2 = vw_pymnt.getRecord().newGroupInGroup("pymnt_Inv_Acct_Part_2", "INV-ACCT-PART-2", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_ACCT_PART_2");
        pymnt_Inv_Acct_Can_Tax_Amt = pymnt_Inv_Acct_Part_2.newFieldArrayInGroup("pymnt_Inv_Acct_Can_Tax_Amt", "INV-ACCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 40) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_CAN_TAX_AMT", "FCP_CONS_PYMT_INV_ACCT_PART_2");
        pymnt_Count_Castpymnt_Ded_Grp = vw_pymnt.getRecord().newFieldInGroup("pymnt_Count_Castpymnt_Ded_Grp", "C*PYMNT-DED-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "FCP_CONS_PYMT_PYMNT_DED_GRP");

        pymnt_Pymnt_Ded_Grp = vw_pymnt.getRecord().newGroupInGroup("pymnt_Pymnt_Ded_Grp", "PYMNT-DED-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Pymnt_Ded_Cde = pymnt_Pymnt_Ded_Grp.newFieldArrayInGroup("pymnt_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 3, new DbsArrayController(1, 
            10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_DED_CDE", "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Pymnt_Ded_Payee_Cde = pymnt_Pymnt_Ded_Grp.newFieldArrayInGroup("pymnt_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", FieldType.STRING, 8, 
            new DbsArrayController(1, 10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_DED_PAYEE_CDE", "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Pymnt_Ded_Amt = pymnt_Pymnt_Ded_Grp.newFieldArrayInGroup("pymnt_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            10) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_DED_AMT", "FCP_CONS_PYMT_PYMNT_DED_GRP");
        pymnt_Count_Castinv_Rtb_Grp = vw_pymnt.getRecord().newFieldInGroup("pymnt_Count_Castinv_Rtb_Grp", "C*INV-RTB-GRP", RepeatingFieldStrategy.CAsteriskVariable, 
            "FCP_CONS_PYMT_INV_RTB_GRP");

        pymnt_Inv_Rtb_Grp = vw_pymnt.getRecord().newGroupInGroup("pymnt_Inv_Rtb_Grp", "INV-RTB-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Rtb_Cde = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Rtb_Cde", "INV-RTB-CDE", FieldType.STRING, 1, new DbsArrayController(1, 
            20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_RTB_CDE", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Fed_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Fed_Dci_Tax_Amt", "INV-ACCT-FED-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FED_DCI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Sta_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Sta_Dci_Tax_Amt", "INV-ACCT-STA-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STA_DCI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Loc_Dci_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Loc_Dci_Tax_Amt", "INV-ACCT-LOC-DCI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOC_DCI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Fed_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Fed_Dpi_Tax_Amt", "INV-ACCT-FED-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_FED_DPI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Sta_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Sta_Dpi_Tax_Amt", "INV-ACCT-STA-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_STA_DPI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Inv_Acct_Loc_Dpi_Tax_Amt = pymnt_Inv_Rtb_Grp.newFieldArrayInGroup("pymnt_Inv_Acct_Loc_Dpi_Tax_Amt", "INV-ACCT-LOC-DPI-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "INV_ACCT_LOC_DPI_TAX_AMT", "FCP_CONS_PYMT_INV_RTB_GRP");
        pymnt_Pymnt_Spouse_Pay_Stats = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Spouse_Pay_Stats", "PYMNT-SPOUSE-PAY-STATS", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_SPOUSE_PAY_STATS");
        pymnt_Cntrct_Cancel_Rdrw_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Ind", "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_IND");
        pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_ACTVTY_CDE");
        pymnt_Cntrct_Cancel_Rdrw_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cancel_Rdrw_Amt", "CNTRCT-CANCEL-RDRW-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, RepeatingFieldStrategy.None, "CNTRCT_CANCEL_RDRW_AMT");
        pymnt_Cnr_Orgnl_Invrse_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 8, 
            RepeatingFieldStrategy.None, "CNR_ORGNL_INVRSE_DTE");
        pymnt_Cnr_Orgnl_Prcss_Seq_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Prcss_Seq_Nbr", "CNR-ORGNL-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNR_ORGNL_PRCSS_SEQ_NBR");
        pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Pymnt_Acctg_Dte", "CNR-ORGNL-PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_ORGNL_PYMNT_ACCTG_DTE");
        pymnt_Cnr_Orgnl_Pymnt_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cnr_Orgnl_Pymnt_Nbr", "CNR-ORGNL-PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "CNR_ORGNL_PYMNT_NBR");
        pymnt_Cntrct_Annty_Ins_Type = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Annty_Ins_Type", "CNTRCT-ANNTY-INS-TYPE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_INS_TYPE");
        pymnt_Cntrct_Annty_Type_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Annty_Type_Cde", "CNTRCT-ANNTY-TYPE-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNTRCT_ANNTY_TYPE_CDE");
        pymnt_Cntrct_Insurance_Option = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Insurance_Option", "CNTRCT-INSURANCE-OPTION", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_INSURANCE_OPTION");
        pymnt_Cntrct_Life_Contingency = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Life_Contingency", "CNTRCT-LIFE-CONTINGENCY", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_LIFE_CONTINGENCY");
        pymnt_Invrse_Process_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Invrse_Process_Dte", "INVRSE-PROCESS-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "INVRSE_PROCESS_DTE");
        pymnt_Pymnt_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, "PYMNT_NBR");
        pymnt_Pymnt_Reqst_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");

        pymnt__R_Field_4 = vw_pymnt.getRecord().newGroupInGroup("pymnt__R_Field_4", "REDEFINE", pymnt_Pymnt_Reqst_Nbr);
        pymnt_Pymnt_Orgn_Cde = pymnt__R_Field_4.newFieldInGroup("pymnt_Pymnt_Orgn_Cde", "PYMNT-ORGN-CDE", FieldType.STRING, 4);
        pymnt_Pymnt_Term_Id = pymnt__R_Field_4.newFieldInGroup("pymnt_Pymnt_Term_Id", "PYMNT-TERM-ID", FieldType.STRING, 8);
        pymnt_Pymnt_User_Id = pymnt__R_Field_4.newFieldInGroup("pymnt_Pymnt_User_Id", "PYMNT-USER-ID", FieldType.STRING, 8);
        pymnt_Pymnt_Seq_Nbr = pymnt__R_Field_4.newFieldInGroup("pymnt_Pymnt_Seq_Nbr", "PYMNT-SEQ-NBR", FieldType.STRING, 15);
        pymnt_Nbr_Of_Funds = vw_pymnt.getRecord().newFieldInGroup("pymnt_Nbr_Of_Funds", "NBR-OF-FUNDS", FieldType.PACKED_DECIMAL, 5, RepeatingFieldStrategy.None, 
            "NBR_OF_FUNDS");
        pymnt_Cntrct_Dvdnd_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Dvdnd_Amt", "CNTRCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_DVDND_AMT");
        pymnt_Cntrct_Settl_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Settl_Amt", "CNTRCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_SETTL_AMT");
        pymnt_Cntrct_Net_Pymnt_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Net_Pymnt_Amt", "CNTRCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_NET_PYMNT_AMT");
        pymnt_Cntrct_Exp_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Exp_Amt", "CNTRCT-EXP-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_EXP_AMT");
        pymnt_Cntrct_Ivc_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Ivc_Amt", "CNTRCT-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_IVC_AMT");
        pymnt_Cntrct_Dci_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Dci_Amt", "CNTRCT-DCI-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_DCI_AMT");
        pymnt_Cntrct_Dpi_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Dpi_Amt", "CNTRCT-DPI-AMT", FieldType.PACKED_DECIMAL, 13, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_DPI_AMT");
        pymnt_Cntrct_Fdrl_Tax_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Fdrl_Tax_Amt", "CNTRCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_FDRL_TAX_AMT");
        pymnt_Cntrct_State_Tax_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_State_Tax_Amt", "CNTRCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_STATE_TAX_AMT");
        pymnt_Cntrct_Can_Tax_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Can_Tax_Amt", "CNTRCT-CAN-TAX-AMT", FieldType.PACKED_DECIMAL, 13, 
            2, RepeatingFieldStrategy.None, "CNTRCT_CAN_TAX_AMT");
        pymnt_Cntrct_Local_Tax_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Local_Tax_Amt", "CNTRCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            13, 2, RepeatingFieldStrategy.None, "CNTRCT_LOCAL_TAX_AMT");
        pymnt_Cntrct_Adj_Ivc_Amt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Adj_Ivc_Amt", "CNTRCT-ADJ-IVC-AMT", FieldType.PACKED_DECIMAL, 13, 
            2, RepeatingFieldStrategy.None, "CNTRCT_ADJ_IVC_AMT");
        pymnt_Pymnt_Payee_Tx_Elct_Trggr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Payee_Tx_Elct_Trggr", "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAYEE_TX_ELCT_TRGGR");
        pymnt_Annt_Ctznshp_Cde = vw_pymnt.getRecord().newFieldInGroup("pymnt_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "ANNT_CTZNSHP_CDE");
        pymnt_Pymnt_Instlmnt_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 2, RepeatingFieldStrategy.None, 
            "PYMNT_INSTLMNT_NBR");
        pymnt_Bank_Account = vw_pymnt.getRecord().newFieldInGroup("pymnt_Bank_Account", "BANK-ACCOUNT", FieldType.STRING, 21, RepeatingFieldStrategy.None, 
            "BANK_ACCOUNT");
        pymnt_Bank_Routing = vw_pymnt.getRecord().newFieldInGroup("pymnt_Bank_Routing", "BANK-ROUTING", FieldType.STRING, 9, RepeatingFieldStrategy.None, 
            "BANK_ROUTING");
        pymnt_Pymnt_Cmbne_Ind = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CMBNE_IND");
        pymnt_Cntrct_Cmbn_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", FieldType.STRING, 14, RepeatingFieldStrategy.None, 
            "CNTRCT_CMBN_NBR");
        pymnt_Notused1 = vw_pymnt.getRecord().newFieldInGroup("pymnt_Notused1", "NOTUSED1", FieldType.STRING, 8, RepeatingFieldStrategy.None, "NOTUSED1");
        pymnt_Plan_Cnt = vw_pymnt.getRecord().newFieldInGroup("pymnt_Plan_Cnt", "PLAN-CNT", FieldType.PACKED_DECIMAL, 4, RepeatingFieldStrategy.None, "PLAN_CNT");
        pymnt_Roth_Money_Source = vw_pymnt.getRecord().newFieldInGroup("pymnt_Roth_Money_Source", "ROTH-MONEY-SOURCE", FieldType.STRING, 5, RepeatingFieldStrategy.None, 
            "ROTH_MONEY_SOURCE");
        pymnt_Roth_First_Contrib_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Roth_First_Contrib_Dte", "ROTH-FIRST-CONTRIB-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "ROTH_FIRST_CONTRIB_DTE");
        pymnt_Pymnt_Eft_Dte = vw_pymnt.getRecord().newFieldInGroup("pymnt_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_EFT_DTE");
        pymnt_Advisor_Pin = vw_pymnt.getRecord().newFieldInGroup("pymnt_Advisor_Pin", "ADVISOR-PIN", FieldType.STRING, 7, RepeatingFieldStrategy.None, 
            "ADVISOR_PIN");
        pymnt_Cntrct_Da_Cref_1_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Cref_1_Nbr", "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_CREF_1_NBR");
        pymnt_Cntrct_Da_Cref_2_Nbr = vw_pymnt.getRecord().newFieldInGroup("pymnt_Cntrct_Da_Cref_2_Nbr", "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_DA_CREF_2_NBR");
        registerRecord(vw_pymnt);

        vw_plan_View = new DataAccessProgramView(new NameInfo("vw_plan_View", "PLAN-VIEW"), "FCP_CONS_PLAN", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_PLAN"));
        plan_View_Plan_Num = vw_plan_View.getRecord().newFieldInGroup("plan_View_Plan_Num", "PLAN-NUM", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "PLAN_NUM");
        plan_View_Sub_Plan_Num = vw_plan_View.getRecord().newFieldInGroup("plan_View_Sub_Plan_Num", "SUB-PLAN-NUM", FieldType.STRING, 6, RepeatingFieldStrategy.None, 
            "SUB_PLAN_NUM");

        plan_View_Plan_Data = vw_plan_View.getRecord().newGroupArrayInGroup("plan_View_Plan_Data", "PLAN-DATA", new DbsArrayController(1, 15) , RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_PLAN_DATA");
        plan_View_Plan_Employer_Name = plan_View_Plan_Data.newFieldInGroup("plan_View_Plan_Employer_Name", "PLAN-EMPLOYER-NAME", FieldType.STRING, 35, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PLAN_EMPLOYER_NAME", "FCP_EFT_GLBL_PLAN_DATA");
        plan_View_Plan_Type = plan_View_Plan_Data.newFieldInGroup("plan_View_Plan_Type", "PLAN-TYPE", FieldType.STRING, 10, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "PLAN_TYPE", "FCP_EFT_GLBL_PLAN_DATA");
        plan_View_Plan_Cash_Avail = plan_View_Plan_Data.newFieldInGroup("plan_View_Plan_Cash_Avail", "PLAN-CASH-AVAIL", FieldType.STRING, 50, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "PLAN_CASH_AVAIL", "FCP_EFT_GLBL_PLAN_DATA");
        plan_View_Pymnt_Reqst_Nbr = vw_plan_View.getRecord().newFieldInGroup("plan_View_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        plan_View_Cntrct_Rcrd_Typ = vw_plan_View.getRecord().newFieldInGroup("plan_View_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_RCRD_TYP");
        plan_View_Roth_Contrb = vw_plan_View.getRecord().newFieldInGroup("plan_View_Roth_Contrb", "ROTH-CONTRB", FieldType.PACKED_DECIMAL, 11, 2, RepeatingFieldStrategy.None, 
            "ROTH_CONTRB");
        plan_View_Roth_Earnings = vw_plan_View.getRecord().newFieldInGroup("plan_View_Roth_Earnings", "ROTH-EARNINGS", FieldType.PACKED_DECIMAL, 11, 2, 
            RepeatingFieldStrategy.None, "ROTH_EARNINGS");
        registerRecord(vw_plan_View);

        vw_addr = new DataAccessProgramView(new NameInfo("vw_addr", "ADDR"), "FCP_CONS_ADDR", "FCP_CONS_LEDGR", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ADDR"));
        addr_Cntrct_Orgn_Cde = vw_addr.getRecord().newFieldInGroup("addr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        addr_Cntrct_Ppcn_Nbr = vw_addr.getRecord().newFieldInGroup("addr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        addr_Cntrct_Payee_Cde = vw_addr.getRecord().newFieldInGroup("addr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        addr_Cntrct_Invrse_Dte = vw_addr.getRecord().newFieldInGroup("addr_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8, RepeatingFieldStrategy.None, 
            "CNTRCT_INVRSE_DTE");
        addr_Pymnt_Prcss_Seq_Nbr = vw_addr.getRecord().newFieldInGroup("addr_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_PRCSS_SEQ_NBR");
        addr_Ph_Last_Name = vw_addr.getRecord().newFieldInGroup("addr_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16, RepeatingFieldStrategy.None, 
            "PH_LAST_NAME");
        addr_Ph_First_Name = vw_addr.getRecord().newFieldInGroup("addr_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "PH_FIRST_NAME");
        addr_Ph_Middle_Name = vw_addr.getRecord().newFieldInGroup("addr_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "PH_MIDDLE_NAME");

        addr_Pymnt_Nme_And_Addr_Grp = vw_addr.getRecord().newGroupInGroup("addr_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Nme = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38, new DbsArrayController(1, 
            2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_NME", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line1_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line1_Txt", "PYMNT-ADDR-LINE1-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE1_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line2_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line2_Txt", "PYMNT-ADDR-LINE2-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE2_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line3_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line3_Txt", "PYMNT-ADDR-LINE3-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE3_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line4_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line4_Txt", "PYMNT-ADDR-LINE4-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE4_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line5_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line5_Txt", "PYMNT-ADDR-LINE5-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE5_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Addr_Line6_Txt = addr_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_Pymnt_Addr_Line6_Txt", "PYMNT-ADDR-LINE6-TXT", FieldType.STRING, 
            35, new DbsArrayController(1, 2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ADDR_LINE6_TXT", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_Pymnt_Eft_Transit_Id = vw_addr.getRecord().newFieldInGroup("addr_Pymnt_Eft_Transit_Id", "PYMNT-EFT-TRANSIT-ID", FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, 
            "PYMNT_EFT_TRANSIT_ID");
        addr_Pymnt_Eft_Acct_Nbr = vw_addr.getRecord().newFieldInGroup("addr_Pymnt_Eft_Acct_Nbr", "PYMNT-EFT-ACCT-NBR", FieldType.STRING, 21, RepeatingFieldStrategy.None, 
            "PYMNT_EFT_ACCT_NBR");
        addr_Pymnt_Chk_Sav_Ind = vw_addr.getRecord().newFieldInGroup("addr_Pymnt_Chk_Sav_Ind", "PYMNT-CHK-SAV-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_CHK_SAV_IND");
        registerRecord(vw_addr);

        pnd_Addr_Key = localVariables.newFieldInRecord("pnd_Addr_Key", "#ADDR-KEY", FieldType.STRING, 31);

        pnd_Addr_Key__R_Field_5 = localVariables.newGroupInRecord("pnd_Addr_Key__R_Field_5", "REDEFINE", pnd_Addr_Key);
        pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr = pnd_Addr_Key__R_Field_5.newFieldInGroup("pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr", "#A-CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde = pnd_Addr_Key__R_Field_5.newFieldInGroup("pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde", "#A-CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde = pnd_Addr_Key__R_Field_5.newFieldInGroup("pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde", "#A-CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte = pnd_Addr_Key__R_Field_5.newFieldInGroup("pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte", "#A-CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr = pnd_Addr_Key__R_Field_5.newFieldInGroup("pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr", "#A-PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 7);

        pnd_Detail = localVariables.newGroupInRecord("pnd_Detail", "#DETAIL");
        pnd_Detail_Pnd_Taxable = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Taxable", "#TAXABLE", FieldType.NUMERIC, 11, 2);
        pnd_Detail_Pnd_Gross = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Gross", "#GROSS", FieldType.NUMERIC, 11, 2);
        pnd_Detail_Pnd_Ivc = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Ivc", "#IVC", FieldType.NUMERIC, 11, 2);
        pnd_Detail_Pnd_Tax = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Tax", "#TAX", FieldType.NUMERIC, 11, 2);
        pnd_Detail_Pnd_Deduc = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Deduc", "#DEDUC", FieldType.NUMERIC, 11, 2);
        pnd_Detail_Pnd_Int = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Int", "#INT", FieldType.NUMERIC, 11, 2);
        pnd_Detail_Pnd_Net = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Net", "#NET", FieldType.NUMERIC, 11, 2);
        pnd_Detail_Pnd_Qty = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Qty", "#QTY", FieldType.NUMERIC, 12, 3);
        pnd_Detail_Pnd_Value = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Value", "#VALUE", FieldType.NUMERIC, 13, 4);

        pnd_Total_Control = localVariables.newGroupInRecord("pnd_Total_Control", "#TOTAL-CONTROL");
        pnd_Total_Control_Pnd_T_Gross = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Gross", "#T-GROSS", FieldType.NUMERIC, 12, 2);
        pnd_Total_Control_Pnd_T_Taxable = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Taxable", "#T-TAXABLE", FieldType.NUMERIC, 12, 2);
        pnd_Total_Control_Pnd_T_Ivc = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Ivc", "#T-IVC", FieldType.NUMERIC, 12, 2);
        pnd_Total_Control_Pnd_T_Tax = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Tax", "#T-TAX", FieldType.NUMERIC, 12, 2);
        pnd_Total_Control_Pnd_T_Deduc = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Deduc", "#T-DEDUC", FieldType.NUMERIC, 12, 2);
        pnd_Total_Control_Pnd_T_Int = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Int", "#T-INT", FieldType.NUMERIC, 12, 2);
        pnd_Total_Control_Pnd_T_Net = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Net", "#T-NET", FieldType.NUMERIC, 12, 2);
        pnd_Total_Control_Pnd_T_Qty = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Qty", "#T-QTY", FieldType.NUMERIC, 13, 3);
        pnd_Total_Control_Pnd_T_Value = pnd_Total_Control.newFieldInGroup("pnd_Total_Control_Pnd_T_Value", "#T-VALUE", FieldType.NUMERIC, 14, 4);
        pnd_Pymnt_Split_Reasn_Cde = localVariables.newFieldInRecord("pnd_Pymnt_Split_Reasn_Cde", "#PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);

        pnd_Pymnt_Split_Reasn_Cde__R_Field_6 = localVariables.newGroupInRecord("pnd_Pymnt_Split_Reasn_Cde__R_Field_6", "REDEFINE", pnd_Pymnt_Split_Reasn_Cde);
        pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde = pnd_Pymnt_Split_Reasn_Cde__R_Field_6.newFieldInGroup("pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde", 
            "#SWAT-FRQNCY-CDE", FieldType.STRING, 1);
        pnd_Pymnt_Split_Reasn_Cde_Pnd_Pymnt_Init_Rqst_Ind = pnd_Pymnt_Split_Reasn_Cde__R_Field_6.newFieldInGroup("pnd_Pymnt_Split_Reasn_Cde_Pnd_Pymnt_Init_Rqst_Ind", 
            "#PYMNT-INIT-RQST-IND", FieldType.STRING, 1);
        pnd_Sum_Int_Ind = localVariables.newFieldInRecord("pnd_Sum_Int_Ind", "#SUM-INT-IND", FieldType.STRING, 1);
        pnd_Cntrct_Payee_Cde_A4 = localVariables.newFieldInRecord("pnd_Cntrct_Payee_Cde_A4", "#CNTRCT-PAYEE-CDE-A4", FieldType.STRING, 4);

        pnd_Cntrct_Payee_Cde_A4__R_Field_7 = localVariables.newGroupInRecord("pnd_Cntrct_Payee_Cde_A4__R_Field_7", "REDEFINE", pnd_Cntrct_Payee_Cde_A4);
        pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde_A2 = pnd_Cntrct_Payee_Cde_A4__R_Field_7.newFieldInGroup("pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde_A2", 
            "#CNTRCT-PAYEE-CDE-A2", FieldType.STRING, 2);

        pnd_Cntrct_Payee_Cde_A4__R_Field_8 = pnd_Cntrct_Payee_Cde_A4__R_Field_7.newGroupInGroup("pnd_Cntrct_Payee_Cde_A4__R_Field_8", "REDEFINE", pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde_A2);
        pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde = pnd_Cntrct_Payee_Cde_A4__R_Field_8.newFieldInGroup("pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde", 
            "#CNTRCT-PAYEE-CDE", FieldType.NUMERIC, 2);

        pnd_Ia_Fields = localVariables.newGroupInRecord("pnd_Ia_Fields", "#IA-FIELDS");
        pnd_Ia_Fields_Pnd_Ia_Last_Pymt_Dte = pnd_Ia_Fields.newFieldInGroup("pnd_Ia_Fields_Pnd_Ia_Last_Pymt_Dte", "#IA-LAST-PYMT-DTE", FieldType.DATE);
        pnd_Ia_Fields_Pnd_Ia_Next_Pymt_Dte = pnd_Ia_Fields.newFieldInGroup("pnd_Ia_Fields_Pnd_Ia_Next_Pymt_Dte", "#IA-NEXT-PYMT-DTE", FieldType.DATE);
        pnd_Ia_Fields_Pnd_Ia_Next_Fctr_Dte = pnd_Ia_Fields.newFieldInGroup("pnd_Ia_Fields_Pnd_Ia_Next_Fctr_Dte", "#IA-NEXT-FCTR-DTE", FieldType.DATE);
        pnd_Ia_Fields_Pnd_Ia_Msg = pnd_Ia_Fields.newFieldInGroup("pnd_Ia_Fields_Pnd_Ia_Msg", "#IA-MSG", FieldType.STRING, 79);
        pnd_Next_Pymnt_Date = localVariables.newFieldInRecord("pnd_Next_Pymnt_Date", "#NEXT-PYMNT-DATE", FieldType.BOOLEAN, 1);
        pnd_Special_Pymnt_Type = localVariables.newFieldInRecord("pnd_Special_Pymnt_Type", "#SPECIAL-PYMNT-TYPE", FieldType.BOOLEAN, 1);
        pnd_Isn = localVariables.newFieldInRecord("pnd_Isn", "#ISN", FieldType.PACKED_DECIMAL, 10);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.PACKED_DECIMAL, 3);
        pnd_R = localVariables.newFieldInRecord("pnd_R", "#R", FieldType.PACKED_DECIMAL, 3);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.STRING, 8);

        pnd_Date__R_Field_9 = localVariables.newGroupInRecord("pnd_Date__R_Field_9", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Datn = pnd_Date__R_Field_9.newFieldInGroup("pnd_Date_Pnd_Datn", "#DATN", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_10 = localVariables.newGroupInRecord("pnd_Date__R_Field_10", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Yyyy = pnd_Date__R_Field_10.newFieldInGroup("pnd_Date_Pnd_Date_Yyyy", "#DATE-YYYY", FieldType.NUMERIC, 4);
        pnd_Date_Pnd_Date_Mm = pnd_Date__R_Field_10.newFieldInGroup("pnd_Date_Pnd_Date_Mm", "#DATE-MM", FieldType.NUMERIC, 2);
        pnd_Date_Pnd_Date_Dd = pnd_Date__R_Field_10.newFieldInGroup("pnd_Date_Pnd_Date_Dd", "#DATE-DD", FieldType.NUMERIC, 2);
        pnd_Date_Fr = localVariables.newFieldInRecord("pnd_Date_Fr", "#DATE-FR", FieldType.STRING, 8);

        pnd_Date_Fr__R_Field_11 = localVariables.newGroupInRecord("pnd_Date_Fr__R_Field_11", "REDEFINE", pnd_Date_Fr);
        pnd_Date_Fr_Pnd_Datn_Fr = pnd_Date_Fr__R_Field_11.newFieldInGroup("pnd_Date_Fr_Pnd_Datn_Fr", "#DATN-FR", FieldType.NUMERIC, 8);
        pnd_Date_To = localVariables.newFieldInRecord("pnd_Date_To", "#DATE-TO", FieldType.STRING, 8);

        pnd_Date_To__R_Field_12 = localVariables.newGroupInRecord("pnd_Date_To__R_Field_12", "REDEFINE", pnd_Date_To);
        pnd_Date_To_Pnd_Datn_To = pnd_Date_To__R_Field_12.newFieldInGroup("pnd_Date_To_Pnd_Datn_To", "#DATN-TO", FieldType.NUMERIC, 8);
        pnd_Ppcn_Inv_Orgn_Prcss_Fr = localVariables.newFieldInRecord("pnd_Ppcn_Inv_Orgn_Prcss_Fr", "#PPCN-INV-ORGN-PRCSS-FR", FieldType.STRING, 29);

        pnd_Ppcn_Inv_Orgn_Prcss_Fr__R_Field_13 = localVariables.newGroupInRecord("pnd_Ppcn_Inv_Orgn_Prcss_Fr__R_Field_13", "REDEFINE", pnd_Ppcn_Inv_Orgn_Prcss_Fr);
        pnd_Ppcn_Inv_Orgn_Prcss_Fr_Pnd_Cntrct_Ppcn_Nbr_Fr = pnd_Ppcn_Inv_Orgn_Prcss_Fr__R_Field_13.newFieldInGroup("pnd_Ppcn_Inv_Orgn_Prcss_Fr_Pnd_Cntrct_Ppcn_Nbr_Fr", 
            "#CNTRCT-PPCN-NBR-FR", FieldType.STRING, 10);
        pnd_Ppcn_Inv_Orgn_Prcss_Fr_Pnd_Cntrct_Invrse_Dte_Fr = pnd_Ppcn_Inv_Orgn_Prcss_Fr__R_Field_13.newFieldInGroup("pnd_Ppcn_Inv_Orgn_Prcss_Fr_Pnd_Cntrct_Invrse_Dte_Fr", 
            "#CNTRCT-INVRSE-DTE-FR", FieldType.NUMERIC, 8);
        pnd_Ppcn_Inv_Orgn_Prcss_To = localVariables.newFieldInRecord("pnd_Ppcn_Inv_Orgn_Prcss_To", "#PPCN-INV-ORGN-PRCSS-TO", FieldType.STRING, 29);

        pnd_Ppcn_Inv_Orgn_Prcss_To__R_Field_14 = localVariables.newGroupInRecord("pnd_Ppcn_Inv_Orgn_Prcss_To__R_Field_14", "REDEFINE", pnd_Ppcn_Inv_Orgn_Prcss_To);
        pnd_Ppcn_Inv_Orgn_Prcss_To_Pnd_Cntrct_Ppcn_Nbr_To = pnd_Ppcn_Inv_Orgn_Prcss_To__R_Field_14.newFieldInGroup("pnd_Ppcn_Inv_Orgn_Prcss_To_Pnd_Cntrct_Ppcn_Nbr_To", 
            "#CNTRCT-PPCN-NBR-TO", FieldType.STRING, 10);
        pnd_Ppcn_Inv_Orgn_Prcss_To_Pnd_Cntrct_Invrse_Dte_To = pnd_Ppcn_Inv_Orgn_Prcss_To__R_Field_14.newFieldInGroup("pnd_Ppcn_Inv_Orgn_Prcss_To_Pnd_Cntrct_Invrse_Dte_To", 
            "#CNTRCT-INVRSE-DTE-TO", FieldType.NUMERIC, 8);
        pnd_Pymnt_Reqst_Instmt_Seq = localVariables.newFieldInRecord("pnd_Pymnt_Reqst_Instmt_Seq", "#PYMNT-REQST-INSTMT-SEQ", FieldType.STRING, 42);

        pnd_Pymnt_Reqst_Instmt_Seq__R_Field_15 = localVariables.newGroupInRecord("pnd_Pymnt_Reqst_Instmt_Seq__R_Field_15", "REDEFINE", pnd_Pymnt_Reqst_Instmt_Seq);
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr = pnd_Pymnt_Reqst_Instmt_Seq__R_Field_15.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr", 
            "#PYMNT-REQST-NBR", FieldType.STRING, 35);
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr = pnd_Pymnt_Reqst_Instmt_Seq__R_Field_15.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr", 
            "#PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 2);
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Inv_Acct_Seq_Nbr = pnd_Pymnt_Reqst_Instmt_Seq__R_Field_15.newFieldInGroup("pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Inv_Acct_Seq_Nbr", 
            "#INV-ACCT-SEQ-NBR", FieldType.NUMERIC, 5);

        pnd_Work_File = localVariables.newGroupInRecord("pnd_Work_File", "#WORK-FILE");
        pnd_Work_File_Pnd_Ppcn = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Ppcn", "#PPCN", FieldType.STRING, 10);
        pnd_Work_File_Pnd_Ssn = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Ssn", "#SSN", FieldType.NUMERIC, 9);
        pnd_Work_File_Pnd_Pin = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Pin", "#PIN", FieldType.NUMERIC, 12);
        pnd_Work_File_Pnd_Orgn = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Orgn", "#ORGN", FieldType.STRING, 2);
        pnd_Work_File_Pnd_Start = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_Start", "#START", FieldType.NUMERIC, 8);
        pnd_Work_File_Pnd_End = pnd_Work_File.newFieldInGroup("pnd_Work_File_Pnd_End", "#END", FieldType.NUMERIC, 8);
        pnd_Data_Count = localVariables.newFieldInRecord("pnd_Data_Count", "#DATA-COUNT", FieldType.NUMERIC, 10);
        pnd_Control_Count = localVariables.newFieldInRecord("pnd_Control_Count", "#CONTROL-COUNT", FieldType.NUMERIC, 10);
        pnd_Insert_Count = localVariables.newFieldInRecord("pnd_Insert_Count", "#INSERT-COUNT", FieldType.NUMERIC, 10);
        pnd_W_Date_X = localVariables.newFieldInRecord("pnd_W_Date_X", "#W-DATE-X", FieldType.DATE);
        pnd_W_Datx = localVariables.newFieldInRecord("pnd_W_Datx", "#W-DATX", FieldType.STRING, 8);

        pnd_W_Datx__R_Field_16 = localVariables.newGroupInRecord("pnd_W_Datx__R_Field_16", "REDEFINE", pnd_W_Datx);
        pnd_W_Datx_Pnd_W_Datn = pnd_W_Datx__R_Field_16.newFieldInGroup("pnd_W_Datx_Pnd_W_Datn", "#W-DATN", FieldType.NUMERIC, 8);
        pnd_Parm = localVariables.newFieldInRecord("pnd_Parm", "#PARM", FieldType.STRING, 10);
        quo = localVariables.newFieldInRecord("quo", "QUO", FieldType.NUMERIC, 9, 5);

        quo__R_Field_17 = localVariables.newGroupInRecord("quo__R_Field_17", "REDEFINE", quo);
        quo_Quo_1 = quo__R_Field_17.newFieldInGroup("quo_Quo_1", "QUO-1", FieldType.NUMERIC, 4);
        quo_Rem = quo__R_Field_17.newFieldInGroup("quo_Rem", "REM", FieldType.NUMERIC, 5);
        pnd_Date_Start = localVariables.newFieldInRecord("pnd_Date_Start", "#DATE-START", FieldType.NUMERIC, 8);

        pnd_Date_Start__R_Field_18 = localVariables.newGroupInRecord("pnd_Date_Start__R_Field_18", "REDEFINE", pnd_Date_Start);
        pnd_Date_Start_Pnd_Date_Strt_Ccyy = pnd_Date_Start__R_Field_18.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Ccyy", "#DATE-STRT-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Date_Start_Pnd_Date_Strt_Mm = pnd_Date_Start__R_Field_18.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Mm", "#DATE-STRT-MM", FieldType.NUMERIC, 
            2);
        pnd_Date_Start_Pnd_Date_Strt_Dd = pnd_Date_Start__R_Field_18.newFieldInGroup("pnd_Date_Start_Pnd_Date_Strt_Dd", "#DATE-STRT-DD", FieldType.NUMERIC, 
            2);

        pnd_Date_Start__R_Field_19 = localVariables.newGroupInRecord("pnd_Date_Start__R_Field_19", "REDEFINE", pnd_Date_Start);
        pnd_Date_Start_Pnd_Date_Start_A = pnd_Date_Start__R_Field_19.newFieldInGroup("pnd_Date_Start_Pnd_Date_Start_A", "#DATE-START-A", FieldType.STRING, 
            8);
        pnd_Date_End = localVariables.newFieldInRecord("pnd_Date_End", "#DATE-END", FieldType.NUMERIC, 8);

        pnd_Date_End__R_Field_20 = localVariables.newGroupInRecord("pnd_Date_End__R_Field_20", "REDEFINE", pnd_Date_End);
        pnd_Date_End_Pnd_Date_End_A = pnd_Date_End__R_Field_20.newFieldInGroup("pnd_Date_End_Pnd_Date_End_A", "#DATE-END-A", FieldType.STRING, 8);

        pnd_Date_End__R_Field_21 = localVariables.newGroupInRecord("pnd_Date_End__R_Field_21", "REDEFINE", pnd_Date_End);
        pnd_Date_End_Pnd_Date_End_Ccyy = pnd_Date_End__R_Field_21.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Ccyy", "#DATE-END-CCYY", FieldType.NUMERIC, 
            4);
        pnd_Date_End_Pnd_Date_End_Mm = pnd_Date_End__R_Field_21.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Mm", "#DATE-END-MM", FieldType.NUMERIC, 2);
        pnd_Date_End_Pnd_Date_End_Dd = pnd_Date_End__R_Field_21.newFieldInGroup("pnd_Date_End_Pnd_Date_End_Dd", "#DATE-END-DD", FieldType.NUMERIC, 2);
        pnd_Start_Invrse_Date = localVariables.newFieldInRecord("pnd_Start_Invrse_Date", "#START-INVRSE-DATE", FieldType.NUMERIC, 8);
        pnd_End_Invrse_Date = localVariables.newFieldInRecord("pnd_End_Invrse_Date", "#END-INVRSE-DATE", FieldType.NUMERIC, 8);
        cpua1051_Detail = localVariables.newFieldInRecord("cpua1051_Detail", "CPUA1051-DETAIL", FieldType.STRING, 303);

        cpua1051_Detail__R_Field_22 = localVariables.newGroupInRecord("cpua1051_Detail__R_Field_22", "REDEFINE", cpua1051_Detail);
        cpua1051_Detail_Cntrct_Ppcn_Nbr = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        cpua1051_Detail_Pymt_Payee_Code = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Payee_Code", "PYMT-PAYEE-CODE", FieldType.STRING, 
            4);
        cpua1051_Detail_Annt_Soc_Sec_Nbr = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", FieldType.NUMERIC, 
            9);
        cpua1051_Detail_Annt_Pin = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Annt_Pin", "ANNT-PIN", FieldType.NUMERIC, 12);
        cpua1051_Detail_Pymt_Annt_Name = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Annt_Name", "PYMT-ANNT-NAME", FieldType.STRING, 
            60);
        cpua1051_Detail_Pymt_Date_From = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Date_From", "PYMT-DATE-FROM", FieldType.STRING, 
            8);
        cpua1051_Detail_Pymt_Date_To = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Date_To", "PYMT-DATE-TO", FieldType.STRING, 8);
        cpua1051_Detail_Pymt_Orgn_Cde = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Orgn_Cde", "PYMT-ORGN-CDE", FieldType.STRING, 
            2);
        cpua1051_Detail_Pymt_Cnr_Ind = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Cnr_Ind", "PYMT-CNR-IND", FieldType.STRING, 1);
        cpua1051_Detail_Pymt_Type = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Type", "PYMT-TYPE", FieldType.STRING, 4);
        cpua1051_Detail_Lob_Code = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Lob_Code", "LOB-CODE", FieldType.STRING, 4);
        cpua1051_Detail_Option_Code = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Option_Code", "OPTION-CODE", FieldType.NUMERIC, 2);
        cpua1051_Detail_Type_Code = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Type_Code", "TYPE-CODE", FieldType.STRING, 2);
        cpua1051_Detail_Pymt_Calc_Pymt_Date = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Calc_Pymt_Date", "PYMT-CALC-PYMT-DATE", 
            FieldType.STRING, 8);
        cpua1051_Detail_Pymt_Next_Pymt_Date = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Next_Pymt_Date", "PYMT-NEXT-PYMT-DATE", 
            FieldType.STRING, 8);
        cpua1051_Detail_Pymt_Freq_Ind = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Freq_Ind", "PYMT-FREQ-IND", FieldType.STRING, 
            1);
        cpua1051_Detail_Pymt_Check_Pymt_Date = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Check_Pymt_Date", "PYMT-CHECK-PYMT-DATE", 
            FieldType.STRING, 8);
        cpua1051_Detail_Pymt_Ticker = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Ticker", "PYMT-TICKER", FieldType.STRING, 10);
        cpua1051_Detail_Pymt_Gross_Amt = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Gross_Amt", "PYMT-GROSS-AMT", FieldType.NUMERIC, 
            11, 2);
        cpua1051_Detail_Pymt_Settl_Amt = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Settl_Amt", "PYMT-SETTL-AMT", FieldType.NUMERIC, 
            11, 2);
        cpua1051_Detail_Pymt_Dvdnd_Amt = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Dvdnd_Amt", "PYMT-DVDND-AMT", FieldType.NUMERIC, 
            9, 2);
        cpua1051_Detail_Pymt_Ivc_Amt = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Ivc_Amt", "PYMT-IVC-AMT", FieldType.NUMERIC, 
            9, 2);
        cpua1051_Detail_Pymt_Unit_Qty = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Unit_Qty", "PYMT-UNIT-QTY", FieldType.NUMERIC, 
            10, 3);
        cpua1051_Detail_Pymt_Unit_Value = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Unit_Value", "PYMT-UNIT-VALUE", FieldType.NUMERIC, 
            9, 4);
        cpua1051_Detail_Pymt_Tax_Amt = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Tax_Amt", "PYMT-TAX-AMT", FieldType.NUMERIC, 
            11, 2);
        cpua1051_Detail_Pymt_Deduc_Amt = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Deduc_Amt", "PYMT-DEDUC-AMT", FieldType.NUMERIC, 
            11, 2);
        cpua1051_Detail_Pymt_Net_Amt = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Net_Amt", "PYMT-NET-AMT", FieldType.NUMERIC, 
            11, 2);
        cpua1051_Detail_Pymt_Int_Amt = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Int_Amt", "PYMT-INT-AMT", FieldType.NUMERIC, 
            11, 2);
        cpua1051_Detail_Pymt_Taxable = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Taxable", "PYMT-TAXABLE", FieldType.NUMERIC, 
            11, 2);
        cpua1051_Detail_Pymt_Pay_Type_Req_Ind = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Pay_Type_Req_Ind", "PYMT-PAY-TYPE-REQ-IND", 
            FieldType.STRING, 1);
        cpua1051_Detail_Pymt_Int_Ind = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Int_Ind", "PYMT-INT-IND", FieldType.STRING, 1);
        cpua1051_Detail_Pymt_Stats_Cde = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Stats_Cde", "PYMT-STATS-CDE", FieldType.STRING, 
            1);
        cpua1051_Detail_Pymt_Spouse_Pay_Stats = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pymt_Spouse_Pay_Stats", "PYMT-SPOUSE-PAY-STATS", 
            FieldType.STRING, 1);
        cpua1051_Detail_Pynt_Cntrct_Roll_Dest_Cde = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pynt_Cntrct_Roll_Dest_Cde", "PYNT-CNTRCT-ROLL-DEST-CDE", 
            FieldType.STRING, 4);
        cpua1051_Detail_Pynt_Cntrct_Annty_Ins_Type = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pynt_Cntrct_Annty_Ins_Type", "PYNT-CNTRCT-ANNTY-INS-TYPE", 
            FieldType.STRING, 1);
        cpua1051_Detail_Pynt_Cntrct_Pymnt_Type_Ind = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pynt_Cntrct_Pymnt_Type_Ind", "PYNT-CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        cpua1051_Detail_Pynt_Cntrct_Sttlmnt_Type_Ind = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pynt_Cntrct_Sttlmnt_Type_Ind", "PYNT-CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        cpua1051_Detail_Pynt_Cntrct_Cancel_Rdrw_Actvty_Cde = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pynt_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "PYNT-CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        cpua1051_Detail_Pynt_Check_Nbr = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pynt_Check_Nbr", "PYNT-CHECK-NBR", FieldType.NUMERIC, 
            7);
        cpua1051_Detail_Pynt_Settlmnt_Dte = cpua1051_Detail__R_Field_22.newFieldInGroup("cpua1051_Detail_Pynt_Settlmnt_Dte", "PYNT-SETTLMNT-DTE", FieldType.STRING, 
            8);

        vw_new_Ext_Cntrl_Fnd_View = new DataAccessProgramView(new NameInfo("vw_new_Ext_Cntrl_Fnd_View", "NEW-EXT-CNTRL-FND-VIEW"), "NEW_EXT_CNTRL_FND", 
            "NEW_EXT_CNTRL");
        new_Ext_Cntrl_Fnd_View_Nec_Table_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Table_Cde", "NEC-TABLE-CDE", 
            FieldType.STRING, 3, RepeatingFieldStrategy.None, "NEC_TABLE_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol", "NEC-TICKER-SYMBOL", 
            FieldType.STRING, 10, RepeatingFieldStrategy.None, "NEC_TICKER_SYMBOL");
        new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde", 
            "NEC-ALPHA-FUND-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_ALPHA_FUND_CDE");
        new_Ext_Cntrl_Fnd_View_Nec_Functional_Cmpny = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Functional_Cmpny", 
            "NEC-FUNCTIONAL-CMPNY", FieldType.STRING, 2, RepeatingFieldStrategy.None, "NEC_FUNCTIONAL_CMPNY");
        new_Ext_Cntrl_Fnd_View_Nec_Ia_Fund_Cde = vw_new_Ext_Cntrl_Fnd_View.getRecord().newFieldInGroup("new_Ext_Cntrl_Fnd_View_Nec_Ia_Fund_Cde", "NEC-IA-FUND-CDE", 
            FieldType.STRING, 5, RepeatingFieldStrategy.None, "NEC_IA_FUND_CDE");
        registerRecord(vw_new_Ext_Cntrl_Fnd_View);

        pnd_Tick_Array = localVariables.newGroupArrayInRecord("pnd_Tick_Array", "#TICK-ARRAY", new DbsArrayController(1, 9999));
        pnd_Tick_Array_Pnd_Acct_Alpha = pnd_Tick_Array.newFieldInGroup("pnd_Tick_Array_Pnd_Acct_Alpha", "#ACCT-ALPHA", FieldType.STRING, 2);
        pnd_Tick_Array_Pnd_Tick = pnd_Tick_Array.newFieldInGroup("pnd_Tick_Array_Pnd_Tick", "#TICK", FieldType.STRING, 10);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.INTEGER, 2);
        pnd_Acct_Alpha_1 = localVariables.newFieldInRecord("pnd_Acct_Alpha_1", "#ACCT-ALPHA-1", FieldType.STRING, 1);
        pnd_Ws_Prime_Key = localVariables.newFieldInRecord("pnd_Ws_Prime_Key", "#WS-PRIME-KEY", FieldType.STRING, 20);

        pnd_Ws_Prime_Key__R_Field_23 = localVariables.newGroupInRecord("pnd_Ws_Prime_Key__R_Field_23", "REDEFINE", pnd_Ws_Prime_Key);
        pnd_Ws_Prime_Key_Cntrct_Orgn_Cde = pnd_Ws_Prime_Key__R_Field_23.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Prime_Key_Pymnt_Stats_Cde = pnd_Ws_Prime_Key__R_Field_23.newFieldInGroup("pnd_Ws_Prime_Key_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Prime_Key_Cntrct_Invrse_Dte = pnd_Ws_Prime_Key__R_Field_23.newFieldInGroup("pnd_Ws_Prime_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Prime_Key__R_Field_23.newFieldInGroup("pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);

        pnd_Ws_Prime_Key__R_Field_24 = pnd_Ws_Prime_Key__R_Field_23.newGroupInGroup("pnd_Ws_Prime_Key__R_Field_24", "REDEFINE", pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Num = pnd_Ws_Prime_Key__R_Field_24.newFieldInGroup("pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Prime_Key_Pymnt_Instmt_Nbr = pnd_Ws_Prime_Key__R_Field_24.newFieldInGroup("pnd_Ws_Prime_Key_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 
            2);
        pnd_Vtran_Id = localVariables.newFieldInRecord("pnd_Vtran_Id", "#VTRAN-ID", FieldType.STRING, 68);

        pnd_Vtran_Id__R_Field_25 = localVariables.newGroupInRecord("pnd_Vtran_Id__R_Field_25", "REDEFINE", pnd_Vtran_Id);
        pnd_Vtran_Id_Pnd_Vtd_Rec_Code = pnd_Vtran_Id__R_Field_25.newFieldInGroup("pnd_Vtran_Id_Pnd_Vtd_Rec_Code", "#VTD-REC-CODE", FieldType.STRING, 1);
        pnd_Vtran_Id_Pnd_Vtd_35 = pnd_Vtran_Id__R_Field_25.newFieldInGroup("pnd_Vtran_Id_Pnd_Vtd_35", "#VTD-35", FieldType.STRING, 35);
        pnd_Vtran_Id_Pnd_Vtd_10 = pnd_Vtran_Id__R_Field_25.newFieldInGroup("pnd_Vtran_Id_Pnd_Vtd_10", "#VTD-10", FieldType.STRING, 10);
        pnd_Vtran_Id_Pnd_Vtd_6 = pnd_Vtran_Id__R_Field_25.newFieldInGroup("pnd_Vtran_Id_Pnd_Vtd_6", "#VTD-6", FieldType.STRING, 6);
        pnd_Vtran_Id_Pnd_Vtd_Filler = pnd_Vtran_Id__R_Field_25.newFieldInGroup("pnd_Vtran_Id_Pnd_Vtd_Filler", "#VTD-FILLER", FieldType.STRING, 8);
        pnd_Vtran_Id_Pnd_Vtd_Seq = pnd_Vtran_Id__R_Field_25.newFieldInGroup("pnd_Vtran_Id_Pnd_Vtd_Seq", "#VTD-SEQ", FieldType.STRING, 8);

        vw_ref = new DataAccessProgramView(new NameInfo("vw_ref", "REF"), "REFERENCE_TABLE", "REFERNCE_TABLE");

        ref_Rt_Record = vw_ref.getRecord().newGroupInGroup("REF_RT_RECORD", "RT-RECORD");
        ref_Rt_A_I_Ind = ref_Rt_Record.newFieldInGroup("ref_Rt_A_I_Ind", "RT-A-I-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, "RT_A_I_IND");
        ref_Rt_Table_Id = ref_Rt_Record.newFieldInGroup("ref_Rt_Table_Id", "RT-TABLE-ID", FieldType.STRING, 5, RepeatingFieldStrategy.None, "RT_TABLE_ID");
        ref_Rt_Short_Key = ref_Rt_Record.newFieldInGroup("ref_Rt_Short_Key", "RT-SHORT-KEY", FieldType.STRING, 20, RepeatingFieldStrategy.None, "RT_SHORT_KEY");
        registerRecord(vw_ref);

        pnd_Input_Isn = localVariables.newFieldInRecord("pnd_Input_Isn", "#INPUT-ISN", FieldType.NUMERIC, 11);

        pnd_Input_Isn__R_Field_26 = localVariables.newGroupInRecord("pnd_Input_Isn__R_Field_26", "REDEFINE", pnd_Input_Isn);
        pnd_Input_Isn_Pnd_Input_Isn_A = pnd_Input_Isn__R_Field_26.newFieldInGroup("pnd_Input_Isn_Pnd_Input_Isn_A", "#INPUT-ISN-A", FieldType.STRING, 11);
        pnd_Rollover_Sw = localVariables.newFieldInRecord("pnd_Rollover_Sw", "#ROLLOVER-SW", FieldType.BOOLEAN, 1);
        pnd_Reqst_Nbr_Rec_Type = localVariables.newFieldInRecord("pnd_Reqst_Nbr_Rec_Type", "#REQST-NBR-REC-TYPE", FieldType.STRING, 36);

        pnd_Reqst_Nbr_Rec_Type__R_Field_27 = localVariables.newGroupInRecord("pnd_Reqst_Nbr_Rec_Type__R_Field_27", "REDEFINE", pnd_Reqst_Nbr_Rec_Type);
        pnd_Reqst_Nbr_Rec_Type_Pnd_Reqst_Nbr = pnd_Reqst_Nbr_Rec_Type__R_Field_27.newFieldInGroup("pnd_Reqst_Nbr_Rec_Type_Pnd_Reqst_Nbr", "#REQST-NBR", 
            FieldType.STRING, 35);
        pnd_Reqst_Nbr_Rec_Type_Pnd_Rcrd_Typ = pnd_Reqst_Nbr_Rec_Type__R_Field_27.newFieldInGroup("pnd_Reqst_Nbr_Rec_Type_Pnd_Rcrd_Typ", "#RCRD-TYP", FieldType.STRING, 
            1);
        pnd_Loan_Pymnt_Sw = localVariables.newFieldInRecord("pnd_Loan_Pymnt_Sw", "#LOAN-PYMNT-SW", FieldType.BOOLEAN, 1);
        pnd_Rt_Super1 = localVariables.newFieldInRecord("pnd_Rt_Super1", "#RT-SUPER1", FieldType.STRING, 66);

        pnd_Rt_Super1__R_Field_28 = localVariables.newGroupInRecord("pnd_Rt_Super1__R_Field_28", "REDEFINE", pnd_Rt_Super1);
        pnd_Rt_Super1_A_I_Ind = pnd_Rt_Super1__R_Field_28.newFieldInGroup("pnd_Rt_Super1_A_I_Ind", "A-I-IND", FieldType.STRING, 1);
        pnd_Rt_Super1_Table_Id = pnd_Rt_Super1__R_Field_28.newFieldInGroup("pnd_Rt_Super1_Table_Id", "TABLE-ID", FieldType.STRING, 5);
        pnd_Rt_Super1_Short_Key = pnd_Rt_Super1__R_Field_28.newFieldInGroup("pnd_Rt_Super1_Short_Key", "SHORT-KEY", FieldType.STRING, 20);
        pnd_Rt_Super1_Long_Key = pnd_Rt_Super1__R_Field_28.newFieldInGroup("pnd_Rt_Super1_Long_Key", "LONG-KEY", FieldType.STRING, 40);
        pnd_Short_Key_Array = localVariables.newFieldArrayInRecord("pnd_Short_Key_Array", "#SHORT-KEY-ARRAY", FieldType.STRING, 20, new DbsArrayController(1, 
            50));
        pnd_Long_Key_Array = localVariables.newFieldArrayInRecord("pnd_Long_Key_Array", "#LONG-KEY-ARRAY", FieldType.STRING, 40, new DbsArrayController(1, 
            50));
        pnd_Prefix_Array = localVariables.newFieldArrayInRecord("pnd_Prefix_Array", "#PREFIX-ARRAY", FieldType.STRING, 1, new DbsArrayController(1, 50));
        pnd_Short_Key = localVariables.newFieldInRecord("pnd_Short_Key", "#SHORT-KEY", FieldType.STRING, 20);

        pnd_Short_Key__R_Field_29 = localVariables.newGroupInRecord("pnd_Short_Key__R_Field_29", "REDEFINE", pnd_Short_Key);
        pnd_Short_Key_Pnd_Rf_Pay_Type = pnd_Short_Key__R_Field_29.newFieldInGroup("pnd_Short_Key_Pnd_Rf_Pay_Type", "#RF-PAY-TYPE", FieldType.STRING, 2);
        pnd_Short_Key_Pnd_Rf_Roll_Dest_Cde = pnd_Short_Key__R_Field_29.newFieldInGroup("pnd_Short_Key_Pnd_Rf_Roll_Dest_Cde", "#RF-ROLL-DEST-CDE", FieldType.STRING, 
            5);
        pnd_Short_Key_Pnd_Rf_Orgn = pnd_Short_Key__R_Field_29.newFieldInGroup("pnd_Short_Key_Pnd_Rf_Orgn", "#RF-ORGN", FieldType.STRING, 3);
        pnd_Short_Key_Pnd_Rf_Pymnt_Type = pnd_Short_Key__R_Field_29.newFieldInGroup("pnd_Short_Key_Pnd_Rf_Pymnt_Type", "#RF-PYMNT-TYPE", FieldType.STRING, 
            2);
        pnd_Short_Key_Pnd_Rf_Settl_Type = pnd_Short_Key__R_Field_29.newFieldInGroup("pnd_Short_Key_Pnd_Rf_Settl_Type", "#RF-SETTL-TYPE", FieldType.STRING, 
            2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_Pos = localVariables.newFieldInRecord("pnd_Pos", "#POS", FieldType.NUMERIC, 2);
        pnd_Prefix = localVariables.newFieldInRecord("pnd_Prefix", "#PREFIX", FieldType.STRING, 1);
        pnd_Roth_First_Contrib_Dte = localVariables.newFieldInRecord("pnd_Roth_First_Contrib_Dte", "#ROTH-FIRST-CONTRIB-DTE", FieldType.DATE);
        pnd_Roth_First_Contrib_Dte_N = localVariables.newFieldInRecord("pnd_Roth_First_Contrib_Dte_N", "#ROTH-FIRST-CONTRIB-DTE-N", FieldType.NUMERIC, 
            8);

        pnd_Roth_First_Contrib_Dte_N__R_Field_30 = localVariables.newGroupInRecord("pnd_Roth_First_Contrib_Dte_N__R_Field_30", "REDEFINE", pnd_Roth_First_Contrib_Dte_N);
        pnd_Roth_First_Contrib_Dte_N_Pnd_Roth_First_Contrib_Dte_A = pnd_Roth_First_Contrib_Dte_N__R_Field_30.newFieldInGroup("pnd_Roth_First_Contrib_Dte_N_Pnd_Roth_First_Contrib_Dte_A", 
            "#ROTH-FIRST-CONTRIB-DTE-A", FieldType.STRING, 8);

        pnd_Roth_First_Contrib_Dte_N__R_Field_31 = localVariables.newGroupInRecord("pnd_Roth_First_Contrib_Dte_N__R_Field_31", "REDEFINE", pnd_Roth_First_Contrib_Dte_N);
        pnd_Roth_First_Contrib_Dte_N_Roth_Cc = pnd_Roth_First_Contrib_Dte_N__R_Field_31.newFieldInGroup("pnd_Roth_First_Contrib_Dte_N_Roth_Cc", "ROTH-CC", 
            FieldType.STRING, 2);
        pnd_Roth_First_Contrib_Dte_N_Roth_Yy = pnd_Roth_First_Contrib_Dte_N__R_Field_31.newFieldInGroup("pnd_Roth_First_Contrib_Dte_N_Roth_Yy", "ROTH-YY", 
            FieldType.STRING, 2);
        pnd_Roth_First_Contrib_Dte_N_Roth_Mm = pnd_Roth_First_Contrib_Dte_N__R_Field_31.newFieldInGroup("pnd_Roth_First_Contrib_Dte_N_Roth_Mm", "ROTH-MM", 
            FieldType.STRING, 2);
        pnd_Roth_First_Contrib_Dte_N_Roth_Dd = pnd_Roth_First_Contrib_Dte_N__R_Field_31.newFieldInGroup("pnd_Roth_First_Contrib_Dte_N_Roth_Dd", "ROTH-DD", 
            FieldType.STRING, 2);
        pnd_Annot_Key = localVariables.newFieldInRecord("pnd_Annot_Key", "#ANNOT-KEY", FieldType.STRING, 29);

        pnd_Annot_Key__R_Field_32 = localVariables.newGroupInRecord("pnd_Annot_Key__R_Field_32", "REDEFINE", pnd_Annot_Key);
        pnd_Annot_Key_Pnd_Annot_Ppcn = pnd_Annot_Key__R_Field_32.newFieldInGroup("pnd_Annot_Key_Pnd_Annot_Ppcn", "#ANNOT-PPCN", FieldType.STRING, 10);
        pnd_Annot_Key_Pnd_Annot_Invrs = pnd_Annot_Key__R_Field_32.newFieldInGroup("pnd_Annot_Key_Pnd_Annot_Invrs", "#ANNOT-INVRS", FieldType.NUMERIC, 
            8);
        pnd_Annot_Key_Pnd_Annot_Orgn = pnd_Annot_Key__R_Field_32.newFieldInGroup("pnd_Annot_Key_Pnd_Annot_Orgn", "#ANNOT-ORGN", FieldType.STRING, 2);
        pnd_Annot_Key_Pnd_Annot_Seq = pnd_Annot_Key__R_Field_32.newFieldInGroup("pnd_Annot_Key_Pnd_Annot_Seq", "#ANNOT-SEQ", FieldType.NUMERIC, 7);
        pnd_Annot_Key_Pnd_Annot_Inst = pnd_Annot_Key__R_Field_32.newFieldInGroup("pnd_Annot_Key_Pnd_Annot_Inst", "#ANNOT-INST", FieldType.NUMERIC, 2);
        pnd_Annot_Key2 = localVariables.newFieldInRecord("pnd_Annot_Key2", "#ANNOT-KEY2", FieldType.STRING, 36);

        pnd_Annot_Key2__R_Field_33 = localVariables.newGroupInRecord("pnd_Annot_Key2__R_Field_33", "REDEFINE", pnd_Annot_Key2);
        pnd_Annot_Key2_Pnd_Annot_Pymnt_Reqst_Nbr = pnd_Annot_Key2__R_Field_33.newFieldInGroup("pnd_Annot_Key2_Pnd_Annot_Pymnt_Reqst_Nbr", "#ANNOT-PYMNT-REQST-NBR", 
            FieldType.STRING, 35);
        pnd_Annot_Key2_Pnd_Annot_Rec_Type = pnd_Annot_Key2__R_Field_33.newFieldInGroup("pnd_Annot_Key2_Pnd_Annot_Rec_Type", "#ANNOT-REC-TYPE", FieldType.STRING, 
            1);

        vw_fcp_Cons_Annot1 = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Annot1", "FCP-CONS-ANNOT1"), "FCP_CONS_ANNOT", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ANNOT"));
        fcp_Cons_Annot1_Cntrct_Rcrd_Typ = vw_fcp_Cons_Annot1.getRecord().newFieldInGroup("fcp_Cons_Annot1_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_RCRD_TYP");
        fcp_Cons_Annot1_Pymnt_Rmrk_Line1_Txt = vw_fcp_Cons_Annot1.getRecord().newFieldInGroup("fcp_Cons_Annot1_Pymnt_Rmrk_Line1_Txt", "PYMNT-RMRK-LINE1-TXT", 
            FieldType.STRING, 70, RepeatingFieldStrategy.None, "PYMNT_RMRK_LINE1_TXT");
        fcp_Cons_Annot1_Pymnt_Rmrk_Line2_Txt = vw_fcp_Cons_Annot1.getRecord().newFieldInGroup("fcp_Cons_Annot1_Pymnt_Rmrk_Line2_Txt", "PYMNT-RMRK-LINE2-TXT", 
            FieldType.STRING, 70, RepeatingFieldStrategy.None, "PYMNT_RMRK_LINE2_TXT");
        fcp_Cons_Annot1_Count_Castpymnt_Annot_Grp = vw_fcp_Cons_Annot1.getRecord().newFieldInGroup("fcp_Cons_Annot1_Count_Castpymnt_Annot_Grp", "C*PYMNT-ANNOT-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");

        fcp_Cons_Annot1_Pymnt_Annot_Grp = vw_fcp_Cons_Annot1.getRecord().newGroupArrayInGroup("fcp_Cons_Annot1_Pymnt_Annot_Grp", "PYMNT-ANNOT-GRP", new 
            DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annot1_Pymnt_Annot_Dte = fcp_Cons_Annot1_Pymnt_Annot_Grp.newFieldInGroup("fcp_Cons_Annot1_Pymnt_Annot_Dte", "PYMNT-ANNOT-DTE", FieldType.DATE, 
            null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_DTE", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annot1_Pymnt_Annot_Id_Nbr = fcp_Cons_Annot1_Pymnt_Annot_Grp.newFieldInGroup("fcp_Cons_Annot1_Pymnt_Annot_Id_Nbr", "PYMNT-ANNOT-ID-NBR", 
            FieldType.STRING, 3, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_ID_NBR", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annot1_Pymnt_Annot_Flag_Ind = fcp_Cons_Annot1_Pymnt_Annot_Grp.newFieldInGroup("fcp_Cons_Annot1_Pymnt_Annot_Flag_Ind", "PYMNT-ANNOT-FLAG-IND", 
            FieldType.STRING, 1, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_FLAG_IND", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        fcp_Cons_Annot1_Count_Castpymnt_Annot_Expand_Grp = vw_fcp_Cons_Annot1.getRecord().newFieldInGroup("fcp_Cons_Annot1_Count_Castpymnt_Annot_Expand_Grp", 
            "C*PYMNT-ANNOT-EXPAND-GRP", RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");

        fcp_Cons_Annot1_Pymnt_Annot_Expand_Grp = vw_fcp_Cons_Annot1.getRecord().newGroupArrayInGroup("fcp_Cons_Annot1_Pymnt_Annot_Expand_Grp", "PYMNT-ANNOT-EXPAND-GRP", 
            new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        fcp_Cons_Annot1_Pymnt_Annot_Expand_Cmnt = fcp_Cons_Annot1_Pymnt_Annot_Expand_Grp.newFieldInGroup("fcp_Cons_Annot1_Pymnt_Annot_Expand_Cmnt", "PYMNT-ANNOT-EXPAND-CMNT", 
            FieldType.STRING, 50, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_CMNT", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        fcp_Cons_Annot1_Pymnt_Annot_Expand_Date = fcp_Cons_Annot1_Pymnt_Annot_Expand_Grp.newFieldInGroup("fcp_Cons_Annot1_Pymnt_Annot_Expand_Date", "PYMNT-ANNOT-EXPAND-DATE", 
            FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_DATE", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        fcp_Cons_Annot1_Pymnt_Annot_Expand_Time = fcp_Cons_Annot1_Pymnt_Annot_Expand_Grp.newFieldInGroup("fcp_Cons_Annot1_Pymnt_Annot_Expand_Time", "PYMNT-ANNOT-EXPAND-TIME", 
            FieldType.STRING, 4, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_TIME", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        fcp_Cons_Annot1_Pymnt_Annot_Expand_Uid = fcp_Cons_Annot1_Pymnt_Annot_Expand_Grp.newFieldInGroup("fcp_Cons_Annot1_Pymnt_Annot_Expand_Uid", "PYMNT-ANNOT-EXPAND-UID", 
            FieldType.STRING, 8, null, RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_UID", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        registerRecord(vw_fcp_Cons_Annot1);

        pnd_Eft_Eff_Date = localVariables.newFieldInRecord("pnd_Eft_Eff_Date", "#EFT-EFF-DATE", FieldType.DATE);
        dte = localVariables.newFieldInRecord("dte", "DTE", FieldType.STRING, 13);

        dte__R_Field_34 = localVariables.newGroupInRecord("dte__R_Field_34", "REDEFINE", dte);
        dte_Mm = dte__R_Field_34.newFieldInGroup("dte_Mm", "MM", FieldType.STRING, 2);
        dte_Hypen = dte__R_Field_34.newFieldInGroup("dte_Hypen", "HYPEN", FieldType.STRING, 1);
        dte_Day = dte__R_Field_34.newFieldInGroup("dte_Day", "DAY", FieldType.STRING, 10);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.STRING, 74);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.PACKED_DECIMAL, 3);
        pnd_Flag_Check_Nbr_0 = localVariables.newFieldInRecord("pnd_Flag_Check_Nbr_0", "#FLAG-CHECK-NBR-0", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_cmbn_Pay.reset();
        vw_pymnt.reset();
        vw_plan_View.reset();
        vw_addr.reset();
        vw_new_Ext_Cntrl_Fnd_View.reset();
        vw_ref.reset();
        vw_fcp_Cons_Annot1.reset();

        ldaCpwlfund.initializeValues();
        ldaFcplpmnt.initializeValues();
        ldaCpulpym2.initializeValues();
        ldaCpulfnd.initializeValues();
        ldaCpsl100.initializeValues();
        ldaCpulannt.initializeValues();
        ldaCpulann2.initializeValues();
        ldaFcpl448.initializeValues();

        localVariables.reset();
        pnd_Rt_Super1.setInitialValue(" ");
        pnd_J.setInitialValue(1);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Cpup1062() throws Exception
    {
        super("Cpup1062");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        //*  JWO15
                                                                                                                                                                          //Natural: PERFORM OPEN-MQ
        sub_Open_Mq();
        if (condition(Global.isEscape())) {return;}
        //* ******************************************************
        //* * READ REFERENCE TABLE TO LOAD THE CHEQUE-PREFIX INTO ARRAY
        //* ***************************************************************
        pnd_Rt_Super1_A_I_Ind.setValue("A");                                                                                                                              //Natural: ASSIGN #RT-SUPER1.A-I-IND := 'A'
        pnd_Rt_Super1_Table_Id.setValue("CPYTP");                                                                                                                         //Natural: ASSIGN #RT-SUPER1.TABLE-ID := 'CPYTP'
        pnd_Rt_Super1_Short_Key.setValue(" ");                                                                                                                            //Natural: ASSIGN #RT-SUPER1.SHORT-KEY := ' '
        pnd_Rt_Super1_Long_Key.setValue(" ");                                                                                                                             //Natural: ASSIGN #RT-SUPER1.LONG-KEY := ' '
        ldaCpsl100.getVw_rt().startDatabaseRead                                                                                                                           //Natural: READ RT BY RT-SUPER1 STARTING FROM #RT-SUPER1
        (
        "READ01",
        new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        READ01:
        while (condition(ldaCpsl100.getVw_rt().readNextRow("READ01")))
        {
            if (condition(ldaCpsl100.getRt_Rt_Table_Id().notEquals(pnd_Rt_Super1_Table_Id)))                                                                              //Natural: IF RT.RT-TABLE-ID NE #RT-SUPER1.TABLE-ID
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Short_Key_Array.getValue(pnd_J).setValue(ldaCpsl100.getRt_Rt_Short_Key());                                                                                //Natural: MOVE RT.RT-SHORT-KEY TO #SHORT-KEY-ARRAY ( #J )
            pnd_Long_Key_Array.getValue(pnd_J).setValue(ldaCpsl100.getRt_Rt_Long_Key());                                                                                  //Natural: MOVE RT.RT-LONG-KEY TO #LONG-KEY-ARRAY ( #J )
            pnd_Prefix_Array.getValue(pnd_J).setValue(ldaCpsl100.getRt_Rt_Desc1().getSubstring(37,1));                                                                    //Natural: MOVE SUBSTR ( RT.RT-DESC1,37,1 ) TO #PREFIX-ARRAY ( #J )
            pnd_J.nadd(1);                                                                                                                                                //Natural: ADD 1 TO #J
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //* ********************************************************/* CTS5 - ENDS
        vw_new_Ext_Cntrl_Fnd_View.startDatabaseRead                                                                                                                       //Natural: READ NEW-EXT-CNTRL-FND-VIEW BY NEC-FND-SUPER1 FROM 'FND'
        (
        "READ02",
        new Wc[] { new Wc("NEC_FND_SUPER1", ">=", "FND", WcType.BY) },
        new Oc[] { new Oc("NEC_FND_SUPER1", "ASC") }
        );
        READ02:
        while (condition(vw_new_Ext_Cntrl_Fnd_View.readNextRow("READ02")))
        {
            //*  IF NEC-FUNCTIONAL-CMPNY NE ' ' AND                  /* JWO10
            //*     NEC-IA-FUND-CDE NE ' '                           /* JWO10
            //*  JWO11
            //*  JWO10
            if (condition(new_Ext_Cntrl_Fnd_View_Nec_Ia_Fund_Cde.notEquals(" ")))                                                                                         //Natural: IF NEC-IA-FUND-CDE NE ' '
            {
                ignore();
                //*  JWO10
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  JWO10
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
                //*  JWO10
            }                                                                                                                                                             //Natural: END-IF
            if (condition(new_Ext_Cntrl_Fnd_View_Nec_Table_Cde.notEquals("FND")))                                                                                         //Natural: IF NEW-EXT-CNTRL-FND-VIEW.NEC-TABLE-CDE NE 'FND'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
                //*  JWO10
            }                                                                                                                                                             //Natural: END-IF
            //* *#INDEX := *COUNTER
            pnd_Index.nadd(1);                                                                                                                                            //Natural: ASSIGN #INDEX := #INDEX + 1
            pnd_Tick_Array_Pnd_Tick.getValue(pnd_Index).setValue(new_Ext_Cntrl_Fnd_View_Nec_Ticker_Symbol);                                                               //Natural: MOVE NEC-TICKER-SYMBOL TO #TICK ( #INDEX )
            pnd_Tick_Array_Pnd_Acct_Alpha.getValue(pnd_Index).setValue(new_Ext_Cntrl_Fnd_View_Nec_Alpha_Fund_Cde);                                                        //Natural: MOVE NEC-ALPHA-FUND-CDE TO #ACCT-ALPHA ( #INDEX )
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        vw_ref.startDatabaseRead                                                                                                                                          //Natural: READ REF BY RT-SUPER1 STARTING FROM 'ICISNS'
        (
        "RD1",
        new Wc[] { new Wc("RT_SUPER1", ">=", "ICISNS", WcType.BY) },
        new Oc[] { new Oc("RT_SUPER1", "ASC") }
        );
        RD1:
        while (condition(vw_ref.readNextRow("RD1")))
        {
            if (condition(ref_Rt_Table_Id.notEquals("CISNS") || ref_Rt_A_I_Ind.notEquals("I")))                                                                           //Natural: IF REF.RT-TABLE-ID NE 'CISNS' OR RT-A-I-IND NE 'I'
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pnd_Input_Isn.compute(new ComputeParameters(false, pnd_Input_Isn), ref_Rt_Short_Key.val());                                                                   //Natural: ASSIGN #INPUT-ISN := VAL ( RT-SHORT-KEY )
                                                                                                                                                                          //Natural: PERFORM A000-INITIALIZATION
            sub_A000_Initialization();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ppcn_Inv_Orgn_Prcss_To_Pnd_Cntrct_Ppcn_Nbr_To.setValue(pnd_Work_File_Pnd_Ppcn);                                                                           //Natural: MOVE #PPCN TO #CNTRCT-PPCN-NBR-TO
            pnd_Ppcn_Inv_Orgn_Prcss_To_Pnd_Cntrct_Invrse_Dte_To.setValue(pnd_Start_Invrse_Date);                                                                          //Natural: MOVE #START-INVRSE-DATE TO #CNTRCT-INVRSE-DTE-TO
            cpua1051_Detail_Cntrct_Ppcn_Nbr.setValue(pnd_Work_File_Pnd_Ppcn);                                                                                             //Natural: ASSIGN CPUA1051-DETAIL.CNTRCT-PPCN-NBR := #PPCN
            cpua1051_Detail_Annt_Soc_Sec_Nbr.setValue(pnd_Work_File_Pnd_Ssn);                                                                                             //Natural: ASSIGN CPUA1051-DETAIL.ANNT-SOC-SEC-NBR := #SSN
            cpua1051_Detail_Annt_Pin.setValue(pnd_Work_File_Pnd_Pin);                                                                                                     //Natural: ASSIGN CPUA1051-DETAIL.ANNT-PIN := #PIN
            cpua1051_Detail_Pymt_Date_From.setValue(pnd_Date_Start_Pnd_Date_Start_A);                                                                                     //Natural: ASSIGN CPUA1051-DETAIL.PYMT-DATE-FROM := #DATE-START-A
            cpua1051_Detail_Pymt_Date_To.setValue(pnd_Date_End_Pnd_Date_End_A);                                                                                           //Natural: ASSIGN CPUA1051-DETAIL.PYMT-DATE-TO := #DATE-END-A
            cpua1051_Detail_Pymt_Orgn_Cde.setValue(pnd_Work_File_Pnd_Orgn);                                                                                               //Natural: ASSIGN CPUA1051-DETAIL.PYMT-ORGN-CDE := #ORGN
            OIA_RD1:                                                                                                                                                      //Natural: GET PYMNT #INPUT-ISN
            vw_pymnt.readByID(pnd_Input_Isn.getLong(), "OIA_RD1");
            pnd_Isn.reset();                                                                                                                                              //Natural: RESET #ISN
            //*      AP PAYMENT HAVING CHECK-NBR AS 0 IN ODS EXTRACT -  VIKRAM2 START
            //*    IF PYMNT.CNTRCT-ORGN-CDE = 'AP'              /* VIKRAM3
            //*  VIKRAM3
            if (condition(pymnt_Cntrct_Orgn_Cde.equals("AP") || pymnt_Cntrct_Orgn_Cde.equals("NZ")))                                                                      //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'AP' OR = 'NZ'
            {
                //*  COMBINED-CONTRACT
                if (condition((pymnt_Pymnt_Cmbne_Ind.equals("Y")) && (pymnt_Pymnt_Nbr.equals(getZero())) && (pymnt_Pymnt_Check_Nbr.equals(getZero()))                     //Natural: IF ( PYMNT.PYMNT-CMBNE-IND EQ 'Y' ) AND ( PYMNT.PYMNT-NBR EQ 0 ) AND ( PYMNT.PYMNT-CHECK-NBR EQ 0 ) AND ( PYMNT.PYMNT-CHECK-SCRTY-NBR EQ 0 )
                    && (pymnt_Pymnt_Check_Scrty_Nbr.equals(getZero()))))
                {
                    if (condition(pymnt_Cntrct_Ppcn_Nbr.equals(pymnt_Cntrct_Cmbn_Nbr)))                                                                                   //Natural: IF PYMNT.CNTRCT-PPCN-NBR EQ PYMNT.CNTRCT-CMBN-NBR
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        vw_cmbn_Pay.startDatabaseRead                                                                                                                     //Natural: READ ( 1 ) CMBN-PAY BY PPCN-INV-ORGN-PRCSS-INST FROM PYMNT.CNTRCT-CMBN-NBR
                        (
                        "READ03",
                        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pymnt_Cntrct_Cmbn_Nbr, WcType.BY) },
                        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
                        1
                        );
                        READ03:
                        while (condition(vw_cmbn_Pay.readNextRow("READ03")))
                        {
                            if (condition(!(cmbn_Pay_Cntrct_Ppcn_Nbr.equals(pymnt_Cntrct_Cmbn_Nbr))))                                                                     //Natural: ACCEPT IF CMBN-PAY.CNTRCT-PPCN-NBR EQ PYMNT.CNTRCT-CMBN-NBR
                            {
                                continue;
                            }
                            pnd_Flag_Check_Nbr_0.reset();                                                                                                                 //Natural: RESET #FLAG-CHECK-NBR-0
                            if (condition((cmbn_Pay_Pymnt_Nbr.equals(getZero())) && (cmbn_Pay_Pymnt_Check_Nbr.equals(getZero())) && (cmbn_Pay_Pymnt_Check_Scrty_Nbr.equals(getZero())))) //Natural: IF ( CMBN-PAY.PYMNT-NBR EQ 0 ) AND ( CMBN-PAY.PYMNT-CHECK-NBR EQ 0 ) AND ( CMBN-PAY.PYMNT-CHECK-SCRTY-NBR EQ 0 )
                            {
                                pnd_Flag_Check_Nbr_0.setValue(true);                                                                                                      //Natural: ASSIGN #FLAG-CHECK-NBR-0 := TRUE
                                if (condition(true)) break;                                                                                                               //Natural: ESCAPE BOTTOM
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                ignore();
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-READ
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom("RD1"))) break;
                            else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(pnd_Flag_Check_Nbr_0.getBoolean()))                                                                                                 //Natural: IF #FLAG-CHECK-NBR-0
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  NON COMBINED-CONTRACT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition((pymnt_Pymnt_Nbr.equals(getZero())) && (pymnt_Pymnt_Check_Nbr.equals(getZero())) && (pymnt_Pymnt_Check_Scrty_Nbr.equals(getZero()))))   //Natural: IF ( PYMNT.PYMNT-NBR EQ 0 ) AND ( PYMNT.PYMNT-CHECK-NBR EQ 0 ) AND ( PYMNT.PYMNT-CHECK-SCRTY-NBR EQ 0 )
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            //*      AP PAYMENT HAVING CHECK-NBR AS 0 IN ODS EXTRACT -  VIKRAM2 END
                                                                                                                                                                          //Natural: PERFORM D200-DETAIL-ROUTINE
            sub_D200_Detail_Routine();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            cpua1051_Detail_Pymt_Cnr_Ind.setValue(pymnt_Cntrct_Cancel_Rdrw_Ind);                                                                                          //Natural: ASSIGN PYMT-CNR-IND := PYMNT.CNTRCT-CANCEL-RDRW-IND
            cpua1051_Detail_Pymt_Payee_Code.setValue(pymnt_Cntrct_Payee_Cde);                                                                                             //Natural: ASSIGN PYMT-PAYEE-CODE := PYMNT.CNTRCT-PAYEE-CDE
            cpua1051_Detail_Lob_Code.setValue(pymnt_Cntrct_Lob_Cde);                                                                                                      //Natural: ASSIGN CPUA1051-DETAIL.LOB-CODE := PYMNT.CNTRCT-LOB-CDE
            cpua1051_Detail_Option_Code.setValue(pymnt_Cntrct_Option_Cde);                                                                                                //Natural: ASSIGN OPTION-CODE := PYMNT.CNTRCT-OPTION-CDE
            cpua1051_Detail_Type_Code.setValue(pymnt_Cntrct_Type_Cde);                                                                                                    //Natural: ASSIGN TYPE-CODE := PYMNT.CNTRCT-TYPE-CDE
            cpua1051_Detail_Pymt_Stats_Cde.setValue(pymnt_Pymnt_Stats_Cde);                                                                                               //Natural: ASSIGN PYMT-STATS-CDE := PYMNT.PYMNT-STATS-CDE
            cpua1051_Detail_Pymt_Spouse_Pay_Stats.setValue(pymnt_Pymnt_Spouse_Pay_Stats);                                                                                 //Natural: ASSIGN PYMT-SPOUSE-PAY-STATS := PYMNT.PYMNT-SPOUSE-PAY-STATS
            cpua1051_Detail_Pynt_Cntrct_Roll_Dest_Cde.setValue(pymnt_Cntrct_Roll_Dest_Cde);                                                                               //Natural: ASSIGN PYNT-CNTRCT-ROLL-DEST-CDE := PYMNT.CNTRCT-ROLL-DEST-CDE
            cpua1051_Detail_Pynt_Cntrct_Annty_Ins_Type.setValue(pymnt_Cntrct_Annty_Ins_Type);                                                                             //Natural: ASSIGN PYNT-CNTRCT-ANNTY-INS-TYPE := PYMNT.CNTRCT-ANNTY-INS-TYPE
            cpua1051_Detail_Pynt_Cntrct_Pymnt_Type_Ind.setValue(pymnt_Cntrct_Pymnt_Type_Ind);                                                                             //Natural: ASSIGN PYNT-CNTRCT-PYMNT-TYPE-IND := PYMNT.CNTRCT-PYMNT-TYPE-IND
            cpua1051_Detail_Pynt_Cntrct_Sttlmnt_Type_Ind.setValue(pymnt_Cntrct_Sttlmnt_Type_Ind);                                                                         //Natural: ASSIGN PYNT-CNTRCT-STTLMNT-TYPE-IND := PYMNT.CNTRCT-STTLMNT-TYPE-IND
            cpua1051_Detail_Pynt_Cntrct_Cancel_Rdrw_Actvty_Cde.setValue(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde);                                                             //Natural: ASSIGN PYNT-CNTRCT-CANCEL-RDRW-ACTVTY-CDE := PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            cpua1051_Detail_Pynt_Check_Nbr.setValue(pymnt_Pymnt_Check_Nbr);                                                                                               //Natural: ASSIGN PYNT-CHECK-NBR := PYMNT.PYMNT-CHECK-NBR
            cpua1051_Detail_Pynt_Settlmnt_Dte.setValue(pymnt_Pymnt_Settlmnt_Dte);                                                                                         //Natural: ASSIGN PYNT-SETTLMNT-DTE := PYMNT.PYMNT-SETTLMNT-DTE
            //*  DON'T NEED NAME /* JWO
                                                                                                                                                                          //Natural: PERFORM GET-NAME
            sub_Get_Name();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  04-01-2003 : PAYMENT LEVEL TOTALS PROCESSED FOR INVESTMENT SOLUTIONS
            //*               COMPLIANCE.  INTEREST, TAX, DEDUCTION, EXPENSE, AND
            //*               NET AMOUNTS ARE DISPLAYED AS 0.00 ON THE FUND LEVEL.
            //*  NON-INV. SOL.
            if (condition(pymnt_Pymnt_Reqst_Nbr.equals(" ")))                                                                                                             //Natural: IF PYMNT.PYMNT-REQST-NBR = ' '
            {
                //*  FOR FUND INFORMATION RETRIEVAL
                pnd_Isn.setValue(vw_pymnt.getAstISN("RD1"));                                                                                                              //Natural: MOVE *ISN ( OIA-RD1. ) TO #ISN
                                                                                                                                                                          //Natural: PERFORM A100-NON-INV-SOL-FUND-PROCESS
                sub_A100_Non_Inv_Sol_Fund_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  INVESTMENT SOLUTIONS
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM A150-INV-SOL-PYMNT-LEVEL-FIELDS
                sub_A150_Inv_Sol_Pymnt_Level_Fields();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM A200-INV-SOL-FUND-PROCESS
                sub_A200_Inv_Sol_Fund_Process();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Detail_Pnd_Int.notEquals(getZero())))                                                                                                       //Natural: IF #INT NE 0
            {
                cpua1051_Detail_Pymt_Int_Ind.setValue("Y");                                                                                                               //Natural: ASSIGN PYMT-INT-IND := 'Y'
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM POPULATE-PAYMENT-DETAIL
            sub_Populate_Payment_Detail();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Total_Control_Pnd_T_Gross.reset();                                                                                                                        //Natural: RESET #T-GROSS #T-IVC #T-TAX #T-DEDUC #T-INT #T-NET #T-QTY #T-VALUE #T-TAXABLE
            pnd_Total_Control_Pnd_T_Ivc.reset();
            pnd_Total_Control_Pnd_T_Tax.reset();
            pnd_Total_Control_Pnd_T_Deduc.reset();
            pnd_Total_Control_Pnd_T_Int.reset();
            pnd_Total_Control_Pnd_T_Net.reset();
            pnd_Total_Control_Pnd_T_Qty.reset();
            pnd_Total_Control_Pnd_T_Value.reset();
            pnd_Total_Control_Pnd_T_Taxable.reset();
            //*  JWO1 02/2013
            vw_ref.deleteDBRow("RD1");                                                                                                                                    //Natural: DELETE ( RD1. )
            //*  JWO1 02/2013
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        getWorkFiles().write(4, false, pnd_Date_Start, pnd_Date_End, pnd_Data_Count);                                                                                     //Natural: WRITE WORK FILE 4 #DATE-START #DATE-END #DATA-COUNT
        getWorkFiles().write(5, false, pnd_Date_Start, pnd_Date_End, pnd_Control_Count);                                                                                  //Natural: WRITE WORK FILE 5 #DATE-START #DATE-END #CONTROL-COUNT
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A000-INITIALIZATION
        //*  -------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A100-NON-INV-SOL-FUND-PROCESS
        //*                                                    MB    END     07/17
        //*  -------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A150-INV-SOL-PYMNT-LEVEL-FIELDS
        //* ***********************************************************
        //*  06-02-2011 : ADDED TAXABLE AMT. CALCULATION.
        //*  -------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: A200-INV-SOL-FUND-PROCESS
        //*  POPULATE CPS-FUND KEY
        //*                                                    MB    END     07/17
        //*  -------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: D200-DETAIL-ROUTINE
        //*  -------------------------------------------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: N100-NEXT-PYMNT-DTE
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NAME
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-TICKER-SYM
        //*  -------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-PAYMENT-DETAIL
        //*  CTS3 START
        //* ***********************************************************************
        //*  CTS3 END
        //* * START FIX DEFECT 90614
        //* *  GROSS-AMT := PYMNT.CNTRCT-SETTL-AMT + PYMNT.CNTRCT-DVDND-AMT +
        //* *    PYMNT.CNTRCT-DCI-AMT   + PYMNT.CNTRCT-DPI-AMT
        //* *    PYMNT.CNTRCT-DCI-AMT   + PYMNT.CNTRCT-DPI-AMT +
        //* * END FIX DEFECT 90614
        //* *IF PYMNT.PYMNT-PAY-TYPE-REQ-IND NE 2
        //* *END-IF
        //* *IF PYMNT.PYMNT-PAY-TYPE-REQ-IND = 2 OR = 8
        //* *END-IF
        //* *****************************  START CHANGES ******
        //*  IF (PYMNT.CNTRCT-ORGN-CDE = 'VT' OR = 'AP' OR = 'NZ' OR = 'DC'
        //*    AND PYMNT.PYMNT-PAY-TYPE-REQ-IND = 2)
        //*    OR (PAYMENT-RECORD.PLAN = SCAN '999ER')
        //*  SA-PYMNT-NAME            := PYMNT-NME (1)
        //*  SA-ADDR-1                := PYMNT-ADDR-LINE1-TXT (1)
        //*  SA-ADDR-2                := PYMNT-ADDR-LINE2-TXT (1)
        //*  SA-ADDR-3                := PYMNT-ADDR-LINE3-TXT (1)
        //*  SA-ADDR-4                := PYMNT-ADDR-LINE4-TXT (1)
        //*  SA-ADDR-5                := PYMNT-ADDR-LINE5-TXT (1)
        //*  SA-ADDR-6                := PYMNT-ADDR-LINE6-TXT (1)
        //*  << SINHASN
        //*  IF PYMNT.PYMNT-CMBNE-IND = 'Y'
        //* *****************************    END CHANGES ******
        //* *
        //* *                           START                         JWO 20130611
        //* *
        //* *
        //* *                           END                           JWO 20130611
        //* *
        //* *      START CHANGES TO ADD FIELDS
        //* **********   START CHANGES FOR TAG
        //*  -------------------------------------------------------------------
        //* ****************************************************/* CTS5 - STARTS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-NUM-PREFIX
        //* ****************************************************/* CTS5 - ENDS
        //* **************************************************** /* JWO6 - STARTS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ANNOTATIONS
        //* **************************************************** /* JWO6 - ENDS
        //* **************************************************** /* JWO8 - ENDS
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADJUST-EFFECTIVE-DATE
        //* **************************************************** /* JWO8 - ENDS
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-IBAN
        //*  #MDMA210.#I-CONTRACT-NUMBER := PYMNT.CNTRCT-CMBN-NBR
        //*  #MDMA210.#I-PAYEE-CODE-A2   := '01'
        //*  #MDMA210.#I-PAYEE-CODE-A2   := PYMNT.CNTRCT-PAYEE-CDE
        //* ************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OPEN-MQ
        //*  ************************************************
        //*  CLOSE MQ                       /* JWO15 END
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0012"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0012'
        if (condition(Global.isEscape())) return;
        //*  ********************************************************************
    }
    private void sub_A000_Initialization() throws Exception                                                                                                               //Natural: A000-INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        cpua1051_Detail_Pymt_Orgn_Cde.reset();                                                                                                                            //Natural: RESET CPUA1051-DETAIL.PYMT-ORGN-CDE CPUA1051-DETAIL.PYMT-TYPE CPUA1051-DETAIL.PYMT-NEXT-PYMT-DATE CPUA1051-DETAIL.PYMT-CALC-PYMT-DATE CPUA1051-DETAIL.PYMT-FREQ-IND CPUA1051-DETAIL.PYMT-CHECK-PYMT-DATE CPUA1051-DETAIL.PYMT-GROSS-AMT CPUA1051-DETAIL.PYMT-UNIT-QTY CPUA1051-DETAIL.PYMT-UNIT-VALUE CPUA1051-DETAIL.PYMT-TAX-AMT CPUA1051-DETAIL.PYMT-DEDUC-AMT CPUA1051-DETAIL.PYMT-INT-AMT CPUA1051-DETAIL.PYMT-NET-AMT CPUA1051-DETAIL.PYMT-PAY-TYPE-REQ-IND CPUA1051-DETAIL.PYMT-TICKER #DETAIL #NEXT-PYMNT-DATE
        cpua1051_Detail_Pymt_Type.reset();
        cpua1051_Detail_Pymt_Next_Pymt_Date.reset();
        cpua1051_Detail_Pymt_Calc_Pymt_Date.reset();
        cpua1051_Detail_Pymt_Freq_Ind.reset();
        cpua1051_Detail_Pymt_Check_Pymt_Date.reset();
        cpua1051_Detail_Pymt_Gross_Amt.reset();
        cpua1051_Detail_Pymt_Unit_Qty.reset();
        cpua1051_Detail_Pymt_Unit_Value.reset();
        cpua1051_Detail_Pymt_Tax_Amt.reset();
        cpua1051_Detail_Pymt_Deduc_Amt.reset();
        cpua1051_Detail_Pymt_Int_Amt.reset();
        cpua1051_Detail_Pymt_Net_Amt.reset();
        cpua1051_Detail_Pymt_Pay_Type_Req_Ind.reset();
        cpua1051_Detail_Pymt_Ticker.reset();
        pnd_Detail.reset();
        pnd_Next_Pymnt_Date.reset();
        //*  A000-INITIALIZATION
    }
    private void sub_A100_Non_Inv_Sol_Fund_Process() throws Exception                                                                                                     //Natural: A100-NON-INV-SOL-FUND-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        //* *   FCP-GET1. GET  FCP-CONS-PYMNT  #ISN  /* DON'T NEED TO REREAD - JWO
        //* **********************************************************
        //*  ROXANNE  REMOVED FROM FOR LOOP
        pnd_Date.setValueEdited(pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                                    //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE
        if (condition(! (pnd_Next_Pymnt_Date.getBoolean())))                                                                                                              //Natural: IF NOT #NEXT-PYMNT-DATE
        {
            pnd_Cntrct_Payee_Cde_A4.setValue(pymnt_Cntrct_Payee_Cde);                                                                                                     //Natural: ASSIGN #CNTRCT-PAYEE-CDE-A4 = PYMNT.CNTRCT-PAYEE-CDE
            if (condition((((DbsUtil.maskMatches(pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde_A2,"99") && ((pymnt_Cntrct_Orgn_Cde.equals("AP") || pymnt_Cntrct_Orgn_Cde.equals("IA"))  //Natural: IF #CNTRCT-PAYEE-CDE-A2 = MASK ( 99 ) AND ( PYMNT.CNTRCT-ORGN-CDE = 'AP' OR = 'IA' OR = 'MS' ) AND #DATE = MASK ( YYYYMMDD ) AND #DATN <= *DATN
                || pymnt_Cntrct_Orgn_Cde.equals("MS"))) && DbsUtil.maskMatches(pnd_Date,"YYYYMMDD")) && pnd_Date_Pnd_Datn.lessOrEqual(Global.getDATN()))))
            {
                //*  NEXT PAYMENT DATE
                pdaCpwa454.getCpwa454().reset();                                                                                                                          //Natural: RESET CPWA454
                pdaCpwa454.getCpwa454_Pnd_Cntrct_Mode_Cde().setValue(pymnt_Cntrct_Mode_Cde);                                                                              //Natural: ASSIGN CPWA454.#CNTRCT-MODE-CDE := PYMNT.CNTRCT-MODE-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        cpua1051_Detail_Pymt_Orgn_Cde.setValue(pymnt_Cntrct_Orgn_Cde);                                                                                                    //Natural: ASSIGN CPUA1051-DETAIL.PYMT-ORGN-CDE = PYMNT.CNTRCT-ORGN-CDE
        //*  SPECIALIZED PAYMENT TYPES
        if (condition(! (pnd_Special_Pymnt_Type.getBoolean())))                                                                                                           //Natural: IF NOT #SPECIAL-PYMNT-TYPE
        {
            pnd_Special_Pymnt_Type.setValue(true);                                                                                                                        //Natural: ASSIGN #SPECIAL-PYMNT-TYPE := TRUE
            pdaCpwa453.getCpwa453().reset();                                                                                                                              //Natural: RESET CPWA453
            pdaCpwa453.getCpwa453().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                                                  //Natural: MOVE BY NAME FCP-CONS-PYMNT TO CPWA453
            DbsUtil.callnat(Cpwn453.class , getCurrentProcessState(), pdaCpwa453.getCpwa453());                                                                           //Natural: CALLNAT 'CPWN453' CPWA453
            if (condition(Global.isEscape())) return;
            cpua1051_Detail_Pymt_Type.setValue(pdaCpwa453.getCpwa453_Payment_Type());                                                                                     //Natural: MOVE CPWA453.PAYMENT-TYPE TO CPUA1051-DETAIL.PYMT-TYPE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #D 1 PYMNT.C*INV-ACCT
        for (pnd_D.setValue(1); condition(pnd_D.lessOrEqual(pymnt_Count_Castinv_Acct)); pnd_D.nadd(1))
        {
            pnd_Detail.reset();                                                                                                                                           //Natural: RESET #DETAIL
            //*  PAYMENT LEVEL TOTALS ARE 0.00 ON THE FUND LEVEL.
            if (condition(pnd_D.equals(1)))                                                                                                                               //Natural: IF #D = 1
            {
                pnd_Detail_Pnd_Deduc.nadd(pymnt_Pymnt_Ded_Amt.getValue("*"));                                                                                             //Natural: ADD PYMNT.PYMNT-DED-AMT ( * ) TO #DEDUC
                pnd_Detail_Pnd_Deduc.nadd(pymnt_Inv_Acct_Exp_Amt.getValue("*"));                                                                                          //Natural: ADD PYMNT.INV-ACCT-EXP-AMT ( * ) TO #DEDUC
            }                                                                                                                                                             //Natural: END-IF
            pnd_Detail_Pnd_Int.compute(new ComputeParameters(false, pnd_Detail_Pnd_Int), pnd_Detail_Pnd_Int.add((pymnt_Inv_Acct_Dci_Amt.getValue(pnd_D).add(pymnt_Inv_Acct_Dpi_Amt.getValue(pnd_D))))); //Natural: ADD ( PYMNT.INV-ACCT-DCI-AMT ( #D ) + PYMNT.INV-ACCT-DPI-AMT ( #D ) ) TO #INT
            pnd_Detail_Pnd_Tax.compute(new ComputeParameters(false, pnd_Detail_Pnd_Tax), pnd_Detail_Pnd_Tax.add((pymnt_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_D).add(pymnt_Inv_Acct_State_Tax_Amt.getValue(pnd_D)).add(pymnt_Inv_Acct_Local_Tax_Amt.getValue(pnd_D))))); //Natural: ADD ( PYMNT.INV-ACCT-FDRL-TAX-AMT ( #D ) + PYMNT.INV-ACCT-STATE-TAX-AMT ( #D ) + PYMNT.INV-ACCT-LOCAL-TAX-AMT ( #D ) ) TO #TAX
            pnd_Detail_Pnd_Net.nadd(pymnt_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_D));                                                                                        //Natural: ADD PYMNT.INV-ACCT-NET-PYMNT-AMT ( #D ) TO #NET
            //*  FUND LEVEL TOTALS
            pnd_Detail_Pnd_Qty.nadd(pymnt_Inv_Acct_Unit_Qty.getValue(pnd_D));                                                                                             //Natural: ADD PYMNT.INV-ACCT-UNIT-QTY ( #D ) TO #QTY
            pnd_Detail_Pnd_Value.setValue(pymnt_Inv_Acct_Unit_Value.getValue(pnd_D));                                                                                     //Natural: MOVE PYMNT.INV-ACCT-UNIT-VALUE ( #D ) TO #VALUE
            pnd_Detail_Pnd_Gross.compute(new ComputeParameters(false, pnd_Detail_Pnd_Gross), pnd_Detail_Pnd_Gross.add((pymnt_Inv_Acct_Settl_Amt.getValue(pnd_D).add(pymnt_Inv_Acct_Dvdnd_Amt.getValue(pnd_D))))); //Natural: ADD ( PYMNT.INV-ACCT-SETTL-AMT ( #D ) + PYMNT.INV-ACCT-DVDND-AMT ( #D ) ) TO #GROSS
            pnd_Detail_Pnd_Ivc.nadd(pymnt_Inv_Acct_Ivc_Amt.getValue(pnd_D));                                                                                              //Natural: ADD PYMNT.INV-ACCT-IVC-AMT ( #D ) TO #DETAIL.#IVC
            //*  06-02-2011 : ADDED TAXABLE AMT. CALCULATION.
            pnd_Detail_Pnd_Taxable.compute(new ComputeParameters(false, pnd_Detail_Pnd_Taxable), pnd_Detail_Pnd_Taxable.add((pymnt_Inv_Acct_Settl_Amt.getValue(pnd_D).add(pymnt_Inv_Acct_Dvdnd_Amt.getValue(pnd_D)).subtract(pymnt_Inv_Acct_Ivc_Amt.getValue(pnd_D))))); //Natural: ADD ( PYMNT.INV-ACCT-SETTL-AMT ( #D ) + PYMNT.INV-ACCT-DVDND-AMT ( #D ) - PYMNT.INV-ACCT-IVC-AMT ( #D ) ) TO #TAXABLE
            //*  TICKER TRANSLATION
                                                                                                                                                                          //Natural: PERFORM GET-TICKER-SYM
            sub_Get_Ticker_Sym();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                    MB    START   07/17
            //*  IF PYMNT.PYMNT-PAY-TYPE-REQ-IND = 8         /*    NON PAYMENT
            //*  DBH3
            //*  CTS4
            if (condition(pnd_Rollover_Sw.getBoolean() || pnd_Loan_Pymnt_Sw.getBoolean()))                                                                                //Natural: IF #ROLLOVER-SW OR #LOAN-PYMNT-SW
            {
                pnd_Detail_Pnd_Taxable.reset();                                                                                                                           //Natural: RESET #TAXABLE
            }                                                                                                                                                             //Natural: END-IF
            cpua1051_Detail_Pymt_Gross_Amt.setValue(pnd_Detail_Pnd_Gross);                                                                                                //Natural: ASSIGN PYMT-GROSS-AMT := #GROSS
            cpua1051_Detail_Pymt_Taxable.setValue(pnd_Detail_Pnd_Taxable);                                                                                                //Natural: ASSIGN PYMT-TAXABLE := #TAXABLE
            cpua1051_Detail_Pymt_Tax_Amt.setValue(pnd_Detail_Pnd_Tax);                                                                                                    //Natural: ASSIGN PYMT-TAX-AMT := #TAX
            cpua1051_Detail_Pymt_Unit_Qty.setValue(pnd_Detail_Pnd_Qty);                                                                                                   //Natural: ASSIGN PYMT-UNIT-QTY := #QTY
            cpua1051_Detail_Pymt_Unit_Value.setValue(pnd_Detail_Pnd_Value);                                                                                               //Natural: ASSIGN PYMT-UNIT-VALUE := #VALUE
            cpua1051_Detail_Pymt_Deduc_Amt.setValue(pnd_Detail_Pnd_Deduc);                                                                                                //Natural: ASSIGN PYMT-DEDUC-AMT := #DEDUC
            cpua1051_Detail_Pymt_Int_Amt.setValue(pnd_Detail_Pnd_Int);                                                                                                    //Natural: ASSIGN PYMT-INT-AMT := #INT
            cpua1051_Detail_Pymt_Net_Amt.setValue(pnd_Detail_Pnd_Net);                                                                                                    //Natural: ASSIGN PYMT-NET-AMT := #NET
            cpua1051_Detail_Pymt_Settl_Amt.setValue(pymnt_Inv_Acct_Settl_Amt.getValue(pnd_D));                                                                            //Natural: ASSIGN PYMT-SETTL-AMT := PYMNT.INV-ACCT-SETTL-AMT ( #D )
            cpua1051_Detail_Pymt_Dvdnd_Amt.setValue(pymnt_Inv_Acct_Dvdnd_Amt.getValue(pnd_D));                                                                            //Natural: ASSIGN PYMT-DVDND-AMT := PYMNT.INV-ACCT-DVDND-AMT ( #D )
            cpua1051_Detail_Pymt_Ivc_Amt.setValue(pymnt_Inv_Acct_Ivc_Amt.getValue(pnd_D));                                                                                //Natural: ASSIGN PYMT-IVC-AMT := PYMNT.INV-ACCT-IVC-AMT ( #D )
            ldaCpulfnd.getDetail_Record().reset();                                                                                                                        //Natural: RESET DETAIL-RECORD
            short decideConditionsMet1942 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PYMNT.PYMNT-NBR NE 0
            if (condition(pymnt_Pymnt_Nbr.notEquals(getZero())))
            {
                decideConditionsMet1942++;
                ldaCpulfnd.getDetail_Record_Check_Number().setValue(pymnt_Pymnt_Nbr);                                                                                     //Natural: ASSIGN DETAIL-RECORD.CHECK-NUMBER := PYMNT.PYMNT-NBR
            }                                                                                                                                                             //Natural: WHEN PYMNT.PYMNT-CHECK-NBR NE 0
            else if (condition(pymnt_Pymnt_Check_Nbr.notEquals(getZero())))
            {
                decideConditionsMet1942++;
                ldaCpulfnd.getDetail_Record_Check_Alpha().setValueEdited(pymnt_Pymnt_Check_Nbr,new ReportEditMask("9999999"));                                            //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-NBR ( EM = 9999999 ) TO DETAIL-RECORD.CHECK-ALPHA
            }                                                                                                                                                             //Natural: WHEN PYMNT.PYMNT-CHECK-SCRTY-NBR NE 0
            else if (condition(pymnt_Pymnt_Check_Scrty_Nbr.notEquals(getZero())))
            {
                decideConditionsMet1942++;
                ldaCpulfnd.getDetail_Record_Check_Alpha().setValueEdited(pymnt_Pymnt_Check_Scrty_Nbr,new ReportEditMask("9999999"));                                      //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-SCRTY-NBR ( EM = 9999999 ) TO DETAIL-RECORD.CHECK-ALPHA
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                if (condition(pymnt_Cntrct_Ppcn_Nbr.notEquals(pymnt_Cntrct_Cmbn_Nbr)))                                                                                    //Natural: IF PYMNT.CNTRCT-PPCN-NBR NE PYMNT.CNTRCT-CMBN-NBR
                {
                    pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.setValue(pymnt_Cntrct_Orgn_Cde);                                                                                     //Natural: ASSIGN #WS-PRIME-KEY.CNTRCT-ORGN-CDE := PYMNT.CNTRCT-ORGN-CDE
                    pnd_Ws_Prime_Key_Pymnt_Stats_Cde.setValue(pymnt_Pymnt_Stats_Cde);                                                                                     //Natural: ASSIGN #WS-PRIME-KEY.PYMNT-STATS-CDE := PYMNT.PYMNT-STATS-CDE
                    pnd_Ws_Prime_Key_Cntrct_Invrse_Dte.setValue(pymnt_Cntrct_Invrse_Dte);                                                                                 //Natural: ASSIGN #WS-PRIME-KEY.CNTRCT-INVRSE-DTE := PYMNT.CNTRCT-INVRSE-DTE
                    pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Nbr.setValue(pymnt_Pymnt_Prcss_Seq_Nbr);                                                                             //Natural: ASSIGN #WS-PRIME-KEY.PYMNT-PRCSS-SEQ-NBR := PYMNT.PYMNT-PRCSS-SEQ-NBR
                    pnd_Ws_Prime_Key_Pymnt_Instmt_Nbr.reset();                                                                                                            //Natural: RESET #WS-PRIME-KEY.PYMNT-INSTMT-NBR
                    vw_cmbn_Pay.startDatabaseRead                                                                                                                         //Natural: READ CMBN-PAY BY ORGN-STATUS-INVRSE-SEQ STARTING FROM #WS-PRIME-KEY
                    (
                    "READ04",
                    new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Prime_Key, WcType.BY) },
                    new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
                    );
                    READ04:
                    while (condition(vw_cmbn_Pay.readNextRow("READ04")))
                    {
                        if (condition(pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.notEquals(cmbn_Pay_Cntrct_Orgn_Cde) || pnd_Ws_Prime_Key_Pymnt_Stats_Cde.notEquals(cmbn_Pay_Pymnt_Stats_Cde)  //Natural: IF #WS-PRIME-KEY.CNTRCT-ORGN-CDE NE CMBN-PAY.CNTRCT-ORGN-CDE OR #WS-PRIME-KEY.PYMNT-STATS-CDE NE CMBN-PAY.PYMNT-STATS-CDE OR #WS-PRIME-KEY.CNTRCT-INVRSE-DTE NE CMBN-PAY.CNTRCT-INVRSE-DTE OR #WS-PRIME-KEY.PYMNT-PRCSS-SEQ-NUM NE CMBN-PAY.PYMNT-PRCSS-SEQ-NUM
                            || pnd_Ws_Prime_Key_Cntrct_Invrse_Dte.notEquals(cmbn_Pay_Cntrct_Invrse_Dte) || pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Num.notEquals(cmbn_Pay_Pymnt_Prcss_Seq_Num)))
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        ldaCpulfnd.getDetail_Record_Check_Number().setValue(cmbn_Pay_Pymnt_Nbr);                                                                          //Natural: ASSIGN DETAIL-RECORD.CHECK-NUMBER := CMBN-PAY.PYMNT-NBR
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaCpulfnd.getDetail_Record_Check_Alpha().equals(" ")))                                                                                         //Natural: IF DETAIL-RECORD.CHECK-ALPHA = ' '
            {
                ldaCpulfnd.getDetail_Record_Check_Number().reset();                                                                                                       //Natural: RESET DETAIL-RECORD.CHECK-NUMBER
                //*  JWO5
                //*  JWO5
                //*  JWO7
                //*  JWO7
                //*  JWO
            }                                                                                                                                                             //Natural: END-IF
            ldaCpulpym2.getPayment_Record_Contract_Number().setValue(pymnt_Cntrct_Ppcn_Nbr);                                                                              //Natural: ASSIGN CONTRACT-NUMBER := PYMNT.CNTRCT-PPCN-NBR
            ldaCpulfnd.getDetail_Record_Investment_Id().setValue(cpua1051_Detail_Pymt_Ticker);                                                                            //Natural: ASSIGN INVESTMENT-ID := PYMT-TICKER
            ldaCpulpym2.getPayment_Record_Payee_Code().setValue(pymnt_Cntrct_Payee_Cde);                                                                                  //Natural: ASSIGN PAYEE-CODE := PYMNT.CNTRCT-PAYEE-CDE
            ldaCpulfnd.getDetail_Record_Unit_Shares().setValue(pymnt_Inv_Acct_Unit_Qty.getValue(pnd_D));                                                                  //Natural: ASSIGN UNIT-SHARES := PYMNT.INV-ACCT-UNIT-QTY ( #D )
            ldaCpulfnd.getDetail_Record_Unit_Price().setValue(pymnt_Inv_Acct_Unit_Value.getValue(pnd_D));                                                                 //Natural: ASSIGN UNIT-PRICE := PYMNT.INV-ACCT-UNIT-VALUE ( #D )
            ldaCpulfnd.getDetail_Record_Settlement_Amt().setValue(pymnt_Inv_Acct_Settl_Amt.getValue(pnd_D));                                                              //Natural: ASSIGN SETTLEMENT-AMT := PYMNT.INV-ACCT-SETTL-AMT ( #D )
            ldaCpulfnd.getDetail_Record_Dividend_Amt().setValue(pymnt_Inv_Acct_Dvdnd_Amt.getValue(pnd_D));                                                                //Natural: ASSIGN DIVIDEND-AMT := PYMNT.INV-ACCT-DVDND-AMT ( #D )
            ldaCpulfnd.getDetail_Record_Interest_Amt().compute(new ComputeParameters(false, ldaCpulfnd.getDetail_Record_Interest_Amt()), pymnt_Inv_Acct_Dci_Amt.getValue(pnd_D).add(pymnt_Inv_Acct_Dpi_Amt.getValue(pnd_D))); //Natural: ASSIGN INTEREST-AMT := PYMNT.INV-ACCT-DCI-AMT ( #D ) + PYMNT.INV-ACCT-DPI-AMT ( #D )
            ldaCpulfnd.getDetail_Record_Account_Code().setValue(pymnt_Inv_Acct_Cde.getValue(pnd_D));                                                                      //Natural: ASSIGN ACCOUNT-CODE := PYMNT.INV-ACCT-CDE ( #D )
            ldaCpulfnd.getDetail_Record_Insurance_Type().setValue(pymnt_Cntrct_Annty_Ins_Type);                                                                           //Natural: ASSIGN INSURANCE-TYPE := PYMNT.CNTRCT-ANNTY-INS-TYPE
            ldaCpulpym2.getPayment_Record_Settlement_Date().setValue(pymnt_Pymnt_Settlmnt_Dte);                                                                           //Natural: ASSIGN SETTLEMENT-DATE := PYMNT.PYMNT-SETTLMNT-DTE
            ldaCpulfnd.getDetail_Record_Key_Contract().setValue(pymnt_Cntrct_Cmbn_Nbr);                                                                                   //Natural: ASSIGN DETAIL-RECORD.KEY-CONTRACT := PYMNT.CNTRCT-CMBN-NBR
            ldaCpulfnd.getDetail_Record_Key_Payee().setValue(pymnt_Cntrct_Payee_Cde);                                                                                     //Natural: ASSIGN DETAIL-RECORD.KEY-PAYEE := PYMNT.CNTRCT-PAYEE-CDE
            ldaCpulfnd.getDetail_Record_Key_Origin().setValue(pymnt_Cntrct_Orgn_Cde);                                                                                     //Natural: ASSIGN DETAIL-RECORD.KEY-ORIGIN := PYMNT.CNTRCT-ORGN-CDE
            ldaCpulfnd.getDetail_Record_Key_Inverse_Date().setValue(pymnt_Cntrct_Invrse_Dte);                                                                             //Natural: ASSIGN DETAIL-RECORD.KEY-INVERSE-DATE := PYMNT.CNTRCT-INVRSE-DTE
            ldaCpulfnd.getDetail_Record_Key_Process_Seq().setValue(pymnt_Pymnt_Prcss_Seq_Nbr);                                                                            //Natural: ASSIGN DETAIL-RECORD.KEY-PROCESS-SEQ := PYMNT.PYMNT-PRCSS-SEQ-NBR
            getWorkFiles().write(7, false, ldaCpulfnd.getDetail_Record());                                                                                                //Natural: WRITE WORK FILE 7 DETAIL-RECORD
            pnd_Data_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #DATA-COUNT
            pnd_Total_Control_Pnd_T_Gross.nadd(pnd_Detail_Pnd_Gross);                                                                                                     //Natural: ASSIGN #T-GROSS := #T-GROSS + #GROSS
            pnd_Total_Control_Pnd_T_Taxable.nadd(pnd_Detail_Pnd_Taxable);                                                                                                 //Natural: ASSIGN #T-TAXABLE := #T-TAXABLE + #TAXABLE
            pnd_Total_Control_Pnd_T_Ivc.nadd(pnd_Detail_Pnd_Ivc);                                                                                                         //Natural: ASSIGN #T-IVC := #T-IVC + #DETAIL.#IVC
            pnd_Total_Control_Pnd_T_Tax.nadd(pnd_Detail_Pnd_Tax);                                                                                                         //Natural: ASSIGN #T-TAX := #T-TAX + #TAX
            pnd_Total_Control_Pnd_T_Deduc.nadd(pnd_Detail_Pnd_Deduc);                                                                                                     //Natural: ASSIGN #T-DEDUC := #T-DEDUC + #DEDUC
            pnd_Total_Control_Pnd_T_Int.nadd(pnd_Detail_Pnd_Int);                                                                                                         //Natural: ASSIGN #T-INT := #T-INT + #INT
            pnd_Total_Control_Pnd_T_Net.nadd(pnd_Detail_Pnd_Net);                                                                                                         //Natural: ASSIGN #T-NET := #T-NET + #NET
            pnd_Total_Control_Pnd_T_Qty.compute(new ComputeParameters(false, pnd_Total_Control_Pnd_T_Qty), pnd_Total_Control_Pnd_T_Value.add(pnd_Detail_Pnd_Value));      //Natural: ASSIGN #T-QTY := #T-VALUE + #VALUE
            pnd_Detail_Pnd_Gross.reset();                                                                                                                                 //Natural: RESET #GROSS #DETAIL.#IVC #TAX #DEDUC #INT #QTY #VALUE #NET #TAXABLE
            pnd_Detail_Pnd_Ivc.reset();
            pnd_Detail_Pnd_Tax.reset();
            pnd_Detail_Pnd_Deduc.reset();
            pnd_Detail_Pnd_Int.reset();
            pnd_Detail_Pnd_Qty.reset();
            pnd_Detail_Pnd_Value.reset();
            pnd_Detail_Pnd_Net.reset();
            pnd_Detail_Pnd_Taxable.reset();
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  A100-NON-INV-SOL-FUND-PROCESS
    }
    private void sub_A150_Inv_Sol_Pymnt_Level_Fields() throws Exception                                                                                                   //Natural: A150-INV-SOL-PYMNT-LEVEL-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //*  PAYMENT LEVEL TOTALS ARE 0.00 ON THE FUND LEVEL.
        pnd_Detail.reset();                                                                                                                                               //Natural: RESET #DETAIL
        pdaTwraplok.getPnd_Twraplok_Pnd_Gross_Amt().compute(new ComputeParameters(false, pdaTwraplok.getPnd_Twraplok_Pnd_Gross_Amt()), pymnt_Cntrct_Settl_Amt.add(pymnt_Cntrct_Dvdnd_Amt)); //Natural: ASSIGN #GROSS-AMT := PYMNT.CNTRCT-SETTL-AMT + PYMNT.CNTRCT-DVDND-AMT
        pdaTwraplok.getPnd_Twraplok_Pnd_Int_Amt().compute(new ComputeParameters(false, pdaTwraplok.getPnd_Twraplok_Pnd_Int_Amt()), pymnt_Cntrct_Dci_Amt.add(pymnt_Cntrct_Dpi_Amt)); //Natural: ASSIGN #INT-AMT := PYMNT.CNTRCT-DCI-AMT + PYMNT.CNTRCT-DPI-AMT
        if (condition(((pymnt_Cntrct_Orgn_Cde.equals("OP") || pymnt_Cntrct_Orgn_Cde.equals("NV")) && pymnt_Cntrct_Dpi_Amt.notEquals(getZero()))))                         //Natural: IF PYMNT.CNTRCT-ORGN-CDE EQ 'OP' OR EQ 'NV' AND PYMNT.CNTRCT-DPI-AMT NE 0
        {
            pdaTwraplok.getPnd_Twraplok_Pnd_Gross_Amt().nsubtract(pymnt_Cntrct_Dpi_Amt);                                                                                  //Natural: ASSIGN #GROSS-AMT := #GROSS-AMT - PYMNT.CNTRCT-DPI-AMT
            pdaTwraplok.getPnd_Twraplok_Pnd_Int_Amt().nsubtract(pymnt_Cntrct_Dpi_Amt);                                                                                    //Natural: ASSIGN #INT-AMT := #INT-AMT - PYMNT.CNTRCT-DPI-AMT
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Detail_Pnd_Ivc.setValue(pymnt_Cntrct_Ivc_Amt);                                                                                                                //Natural: ASSIGN #DETAIL.#IVC := PYMNT.CNTRCT-IVC-AMT
        pnd_Detail_Pnd_Int.compute(new ComputeParameters(false, pnd_Detail_Pnd_Int), pymnt_Cntrct_Dci_Amt.add(pymnt_Cntrct_Dpi_Amt));                                     //Natural: ASSIGN #INT := PYMNT.CNTRCT-DCI-AMT + PYMNT.CNTRCT-DPI-AMT
        pnd_Detail_Pnd_Tax.compute(new ComputeParameters(false, pnd_Detail_Pnd_Tax), pymnt_Cntrct_Fdrl_Tax_Amt.add(pymnt_Cntrct_State_Tax_Amt).add(pymnt_Cntrct_Local_Tax_Amt)); //Natural: ASSIGN #TAX := PYMNT.CNTRCT-FDRL-TAX-AMT + PYMNT.CNTRCT-STATE-TAX-AMT + PYMNT.CNTRCT-LOCAL-TAX-AMT
        pnd_Detail_Pnd_Deduc.nadd(pymnt_Pymnt_Ded_Amt.getValue("*"));                                                                                                     //Natural: ADD PYMNT.PYMNT-DED-AMT ( * ) TO #DEDUC
        //*  VIKRAM START
        if (condition(pymnt_Cntrct_Orgn_Cde.equals("AD")))                                                                                                                //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'AD'
        {
            pymnt_Cntrct_Exp_Amt.setValue(0);                                                                                                                             //Natural: ASSIGN PYMNT.CNTRCT-EXP-AMT := 0
            //*  VIKRAM END
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Detail_Pnd_Deduc.nadd(pymnt_Cntrct_Exp_Amt);                                                                                                                  //Natural: ADD PYMNT.CNTRCT-EXP-AMT TO #DEDUC
        pnd_Detail_Pnd_Net.setValue(pymnt_Cntrct_Net_Pymnt_Amt);                                                                                                          //Natural: ASSIGN #NET := PYMNT.CNTRCT-NET-PYMNT-AMT
        pnd_Detail_Pnd_Taxable.compute(new ComputeParameters(false, pnd_Detail_Pnd_Taxable), pymnt_Cntrct_Settl_Amt.add(pymnt_Cntrct_Dvdnd_Amt).subtract(pymnt_Cntrct_Ivc_Amt)); //Natural: ASSIGN #TAXABLE := PYMNT.CNTRCT-SETTL-AMT + PYMNT.CNTRCT-DVDND-AMT - PYMNT.CNTRCT-IVC-AMT
        //*  A150-INV-SOL-PYMNT-LEVEL-FIELDS
    }
    private void sub_A200_Inv_Sol_Fund_Process() throws Exception                                                                                                         //Natural: A200-INV-SOL-FUND-PROCESS
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Pymnt_Reqst_Instmt_Seq.reset();                                                                                                                               //Natural: RESET #PYMNT-REQST-INSTMT-SEQ #D
        pnd_D.reset();
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr.setValue(pymnt_Pymnt_Reqst_Nbr);                                                                                   //Natural: ASSIGN #PYMNT-REQST-NBR := PYMNT.PYMNT-REQST-NBR
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr.setValue(pymnt_Pymnt_Instlmnt_Nbr);                                                                             //Natural: ASSIGN #PYMNT-INSTLMNT-NBR := PYMNT.PYMNT-INSTLMNT-NBR
        pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Inv_Acct_Seq_Nbr.setValue(1);                                                                                                      //Natural: ASSIGN #INV-ACCT-SEQ-NBR := 1
        ldaCpwlfund.getVw_cps_Fund().startDatabaseRead                                                                                                                    //Natural: READ CPS-FUND BY PYMNT-REQST-INSTMT-SEQ STARTING FROM #PYMNT-REQST-INSTMT-SEQ
        (
        "RD_FUND1",
        new Wc[] { new Wc("PYMNT_REQST_INSTMT_SEQ", ">=", pnd_Pymnt_Reqst_Instmt_Seq, WcType.BY) },
        new Oc[] { new Oc("PYMNT_REQST_INSTMT_SEQ", "ASC") }
        );
        RD_FUND1:
        while (condition(ldaCpwlfund.getVw_cps_Fund().readNextRow("RD_FUND1")))
        {
            if (condition((ldaCpwlfund.getCps_Fund_Pymnt_Reqst_Nbr().notEquals(pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Reqst_Nbr) || ldaCpwlfund.getCps_Fund_Pymnt_Instlmnt_Nbr().notEquals(pnd_Pymnt_Reqst_Instmt_Seq_Pnd_Pymnt_Instlmnt_Nbr))  //Natural: IF ( CPS-FUND.PYMNT-REQST-NBR NE #PYMNT-REQST-NBR OR CPS-FUND.PYMNT-INSTLMNT-NBR NE #PYMNT-INSTLMNT-NBR ) OR ( PYMNT.NBR-OF-FUNDS EQ 0 )
                || (pymnt_Nbr_Of_Funds.equals(getZero()))))
            {
                if (true) break RD_FUND1;                                                                                                                                 //Natural: ESCAPE BOTTOM ( RD-FUND1. )
            }                                                                                                                                                             //Natural: END-IF
            //*  FUND LEVEL TOTALS
            pnd_Detail_Pnd_Qty.setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Unit_Qty());                                                                                     //Natural: MOVE CPS-FUND.INV-ACCT-UNIT-QTY TO #QTY
            pnd_Detail_Pnd_Value.setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Unit_Value());                                                                                 //Natural: MOVE CPS-FUND.INV-ACCT-UNIT-VALUE TO #VALUE
            pnd_Detail_Pnd_Gross.setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Settl_Amt());                                                                                  //Natural: MOVE CPS-FUND.INV-ACCT-SETTL-AMT TO #GROSS
            pnd_Detail_Pnd_Gross.nadd(ldaCpwlfund.getCps_Fund_Inv_Acct_Dvdnd_Amt());                                                                                      //Natural: ADD CPS-FUND.INV-ACCT-DVDND-AMT TO #GROSS
            pnd_Detail_Pnd_Deduc.setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Exp_Amt());                                                                                    //Natural: MOVE CPS-FUND.INV-ACCT-EXP-AMT TO #DEDUC
            pnd_Date.setValueEdited(pymnt_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                                                //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #DATE
            if (condition(! (pnd_Next_Pymnt_Date.getBoolean())))                                                                                                          //Natural: IF NOT #NEXT-PYMNT-DATE
            {
                pnd_Cntrct_Payee_Cde_A4.setValue(pymnt_Cntrct_Payee_Cde);                                                                                                 //Natural: ASSIGN #CNTRCT-PAYEE-CDE-A4 = PYMNT.CNTRCT-PAYEE-CDE
                if (condition((((DbsUtil.maskMatches(pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde_A2,"99") && ((pymnt_Cntrct_Orgn_Cde.equals("AP") ||                     //Natural: IF #CNTRCT-PAYEE-CDE-A2 = MASK ( 99 ) AND ( PYMNT.CNTRCT-ORGN-CDE = 'AP' OR = 'IA' OR = 'MS' ) AND #DATE = MASK ( YYYYMMDD ) AND #DATN <= *DATN
                    pymnt_Cntrct_Orgn_Cde.equals("IA")) || pymnt_Cntrct_Orgn_Cde.equals("MS"))) && DbsUtil.maskMatches(pnd_Date,"YYYYMMDD")) && pnd_Date_Pnd_Datn.lessOrEqual(Global.getDATN()))))
                {
                    //*  NEXT PAYMENT DATE
                    pdaCpwa454.getCpwa454().reset();                                                                                                                      //Natural: RESET CPWA454
                    pdaCpwa454.getCpwa454_Pnd_Cntrct_Mode_Cde().setValue(pymnt_Cntrct_Mode_Cde);                                                                          //Natural: ASSIGN CPWA454.#CNTRCT-MODE-CDE := PYMNT.CNTRCT-MODE-CDE
                                                                                                                                                                          //Natural: PERFORM N100-NEXT-PYMNT-DTE
                    sub_N100_Next_Pymnt_Dte();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FUND1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FUND1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            cpua1051_Detail_Pymt_Orgn_Cde.setValue(pymnt_Cntrct_Orgn_Cde);                                                                                                //Natural: ASSIGN CPUA1051-DETAIL.PYMT-ORGN-CDE = PYMNT.CNTRCT-ORGN-CDE
            //*  SPECIALIZED PAYMENT TYPES
            if (condition(! (pnd_Special_Pymnt_Type.getBoolean())))                                                                                                       //Natural: IF NOT #SPECIAL-PYMNT-TYPE
            {
                pnd_Special_Pymnt_Type.setValue(true);                                                                                                                    //Natural: ASSIGN #SPECIAL-PYMNT-TYPE := TRUE
                pdaCpwa453.getCpwa453().reset();                                                                                                                          //Natural: RESET CPWA453
                pdaCpwa453.getCpwa453().setValuesByName(vw_pymnt);                                                                                                        //Natural: MOVE BY NAME PYMNT TO CPWA453
                DbsUtil.callnat(Cpwn453.class , getCurrentProcessState(), pdaCpwa453.getCpwa453());                                                                       //Natural: CALLNAT 'CPWN453' CPWA453
                if (condition(Global.isEscape())){if (Global.isEscapeBottom()) break; else if (Global.isEscapeTop()) continue; else return;}
                cpua1051_Detail_Pymt_Type.setValue(pdaCpwa453.getCpwa453_Payment_Type());                                                                                 //Natural: MOVE CPWA453.PAYMENT-TYPE TO CPUA1051-DETAIL.PYMT-TYPE
            }                                                                                                                                                             //Natural: END-IF
            //*  PER ILSE, NO REPORTING SEQUENCE FOR INVESTMENT SOLUTIONS PAYMENTS.
            cpua1051_Detail_Pymt_Ticker.setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Ticker());                                                                              //Natural: MOVE CPS-FUND.INV-ACCT-TICKER TO PYMT-TICKER
            //*  06-02-2011 : ADDED TAXABLE AMT. CALCULATION.
            //*                                                    MB    START   07/17
            //*  DBH3
            //*  CTS4
            if (condition(pnd_Rollover_Sw.getBoolean() || pnd_Loan_Pymnt_Sw.getBoolean()))                                                                                //Natural: IF #ROLLOVER-SW OR #LOAN-PYMNT-SW
            {
                pnd_Detail_Pnd_Taxable.reset();                                                                                                                           //Natural: RESET #TAXABLE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Detail_Pnd_Taxable.compute(new ComputeParameters(false, pnd_Detail_Pnd_Taxable), ldaCpwlfund.getCps_Fund_Inv_Acct_Settl_Amt().add(ldaCpwlfund.getCps_Fund_Inv_Acct_Dvdnd_Amt()).subtract(ldaCpwlfund.getCps_Fund_Inv_Acct_Ivc_Amt())); //Natural: ASSIGN #TAXABLE := CPS-FUND.INV-ACCT-SETTL-AMT + CPS-FUND.INV-ACCT-DVDND-AMT - CPS-FUND.INV-ACCT-IVC-AMT
            }                                                                                                                                                             //Natural: END-IF
            cpua1051_Detail_Pymt_Gross_Amt.setValue(pnd_Detail_Pnd_Gross);                                                                                                //Natural: ASSIGN PYMT-GROSS-AMT := #GROSS
            cpua1051_Detail_Pymt_Taxable.setValue(pnd_Detail_Pnd_Taxable);                                                                                                //Natural: ASSIGN PYMT-TAXABLE := #TAXABLE
            cpua1051_Detail_Pymt_Tax_Amt.setValue(pnd_Detail_Pnd_Tax);                                                                                                    //Natural: ASSIGN PYMT-TAX-AMT := #TAX
            cpua1051_Detail_Pymt_Unit_Qty.setValue(pnd_Detail_Pnd_Qty);                                                                                                   //Natural: ASSIGN PYMT-UNIT-QTY := #QTY
            cpua1051_Detail_Pymt_Unit_Value.setValue(pnd_Detail_Pnd_Value);                                                                                               //Natural: ASSIGN PYMT-UNIT-VALUE := #VALUE
            cpua1051_Detail_Pymt_Deduc_Amt.setValue(pnd_Detail_Pnd_Deduc);                                                                                                //Natural: ASSIGN PYMT-DEDUC-AMT := #DEDUC
            cpua1051_Detail_Pymt_Int_Amt.setValue(pnd_Detail_Pnd_Int);                                                                                                    //Natural: ASSIGN PYMT-INT-AMT := #INT
            cpua1051_Detail_Pymt_Net_Amt.setValue(pnd_Detail_Pnd_Net);                                                                                                    //Natural: ASSIGN PYMT-NET-AMT := #NET
            cpua1051_Detail_Pymt_Settl_Amt.setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Settl_Amt());                                                                        //Natural: ASSIGN PYMT-SETTL-AMT := CPS-FUND.INV-ACCT-SETTL-AMT
            cpua1051_Detail_Pymt_Dvdnd_Amt.setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Dvdnd_Amt());                                                                        //Natural: ASSIGN PYMT-DVDND-AMT := CPS-FUND.INV-ACCT-DVDND-AMT
            //*  JWO2 03/2014
            if (condition(pymnt_Cntrct_Orgn_Cde.equals("OP") || pymnt_Cntrct_Orgn_Cde.equals("NV")))                                                                      //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'OP' OR = 'NV'
            {
                cpua1051_Detail_Pymt_Ivc_Amt.setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Ivc_Amt());                                                                        //Natural: ASSIGN PYMT-IVC-AMT := CPS-FUND.INV-ACCT-IVC-AMT
                //*  JWO2 03/2014
            }                                                                                                                                                             //Natural: END-IF
            ldaCpulfnd.getDetail_Record().reset();                                                                                                                        //Natural: RESET DETAIL-RECORD
            short decideConditionsMet2108 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PYMNT.PYMNT-NBR NE 0
            if (condition(pymnt_Pymnt_Nbr.notEquals(getZero())))
            {
                decideConditionsMet2108++;
                ldaCpulfnd.getDetail_Record_Check_Number().setValue(pymnt_Pymnt_Nbr);                                                                                     //Natural: ASSIGN DETAIL-RECORD.CHECK-NUMBER := PYMNT.PYMNT-NBR
            }                                                                                                                                                             //Natural: WHEN PYMNT.PYMNT-CHECK-NBR NE 0
            else if (condition(pymnt_Pymnt_Check_Nbr.notEquals(getZero())))
            {
                decideConditionsMet2108++;
                ldaCpulfnd.getDetail_Record_Check_Alpha().setValueEdited(pymnt_Pymnt_Check_Nbr,new ReportEditMask("9999999"));                                            //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-NBR ( EM = 9999999 ) TO DETAIL-RECORD.CHECK-ALPHA
            }                                                                                                                                                             //Natural: WHEN PYMNT.PYMNT-CHECK-SCRTY-NBR NE 0
            else if (condition(pymnt_Pymnt_Check_Scrty_Nbr.notEquals(getZero())))
            {
                decideConditionsMet2108++;
                ldaCpulfnd.getDetail_Record_Check_Alpha().setValueEdited(pymnt_Pymnt_Check_Scrty_Nbr,new ReportEditMask("9999999"));                                      //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-SCRTY-NBR ( EM = 9999999 ) TO DETAIL-RECORD.CHECK-ALPHA
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                if (condition(pymnt_Cntrct_Ppcn_Nbr.notEquals(pymnt_Cntrct_Cmbn_Nbr)))                                                                                    //Natural: IF PYMNT.CNTRCT-PPCN-NBR NE PYMNT.CNTRCT-CMBN-NBR
                {
                    pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.setValue(pymnt_Cntrct_Orgn_Cde);                                                                                     //Natural: ASSIGN #WS-PRIME-KEY.CNTRCT-ORGN-CDE := PYMNT.CNTRCT-ORGN-CDE
                    pnd_Ws_Prime_Key_Pymnt_Stats_Cde.setValue(pymnt_Pymnt_Stats_Cde);                                                                                     //Natural: ASSIGN #WS-PRIME-KEY.PYMNT-STATS-CDE := PYMNT.PYMNT-STATS-CDE
                    pnd_Ws_Prime_Key_Cntrct_Invrse_Dte.setValue(pymnt_Cntrct_Invrse_Dte);                                                                                 //Natural: ASSIGN #WS-PRIME-KEY.CNTRCT-INVRSE-DTE := PYMNT.CNTRCT-INVRSE-DTE
                    pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Nbr.setValue(pymnt_Pymnt_Prcss_Seq_Nbr);                                                                             //Natural: ASSIGN #WS-PRIME-KEY.PYMNT-PRCSS-SEQ-NBR := PYMNT.PYMNT-PRCSS-SEQ-NBR
                    pnd_Ws_Prime_Key_Pymnt_Instmt_Nbr.reset();                                                                                                            //Natural: RESET #WS-PRIME-KEY.PYMNT-INSTMT-NBR
                    vw_cmbn_Pay.startDatabaseRead                                                                                                                         //Natural: READ CMBN-PAY BY ORGN-STATUS-INVRSE-SEQ STARTING FROM #WS-PRIME-KEY
                    (
                    "READ05",
                    new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Prime_Key, WcType.BY) },
                    new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
                    );
                    READ05:
                    while (condition(vw_cmbn_Pay.readNextRow("READ05")))
                    {
                        if (condition(pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.notEquals(cmbn_Pay_Cntrct_Orgn_Cde) || pnd_Ws_Prime_Key_Pymnt_Stats_Cde.notEquals(cmbn_Pay_Pymnt_Stats_Cde)  //Natural: IF #WS-PRIME-KEY.CNTRCT-ORGN-CDE NE CMBN-PAY.CNTRCT-ORGN-CDE OR #WS-PRIME-KEY.PYMNT-STATS-CDE NE CMBN-PAY.PYMNT-STATS-CDE OR #WS-PRIME-KEY.CNTRCT-INVRSE-DTE NE CMBN-PAY.CNTRCT-INVRSE-DTE OR #WS-PRIME-KEY.PYMNT-PRCSS-SEQ-NUM NE CMBN-PAY.PYMNT-PRCSS-SEQ-NUM
                            || pnd_Ws_Prime_Key_Cntrct_Invrse_Dte.notEquals(cmbn_Pay_Cntrct_Invrse_Dte) || pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Num.notEquals(cmbn_Pay_Pymnt_Prcss_Seq_Num)))
                        {
                            if (condition(true)) break;                                                                                                                   //Natural: ESCAPE BOTTOM
                        }                                                                                                                                                 //Natural: END-IF
                        ldaCpulfnd.getDetail_Record_Check_Number().setValue(cmbn_Pay_Pymnt_Nbr);                                                                          //Natural: ASSIGN DETAIL-RECORD.CHECK-NUMBER := CMBN-PAY.PYMNT-NBR
                    }                                                                                                                                                     //Natural: END-READ
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD_FUND1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD_FUND1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(ldaCpulfnd.getDetail_Record_Check_Alpha().equals(" ")))                                                                                         //Natural: IF DETAIL-RECORD.CHECK-ALPHA = ' '
            {
                ldaCpulfnd.getDetail_Record_Check_Number().reset();                                                                                                       //Natural: RESET DETAIL-RECORD.CHECK-NUMBER
                //*  JWO7
                //*  JWO7
                //*  JWO
            }                                                                                                                                                             //Natural: END-IF
            ldaCpulpym2.getPayment_Record_Contract_Number().setValue(pymnt_Cntrct_Ppcn_Nbr);                                                                              //Natural: ASSIGN CONTRACT-NUMBER := PYMNT.CNTRCT-PPCN-NBR
            ldaCpulfnd.getDetail_Record_Investment_Id().setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Ticker());                                                              //Natural: ASSIGN INVESTMENT-ID := CPS-FUND.INV-ACCT-TICKER
            ldaCpulpym2.getPayment_Record_Payee_Code().setValue(pymnt_Cntrct_Payee_Cde);                                                                                  //Natural: ASSIGN PAYEE-CODE := PYMNT.CNTRCT-PAYEE-CDE
            ldaCpulfnd.getDetail_Record_Unit_Shares().setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Unit_Qty());                                                              //Natural: ASSIGN UNIT-SHARES := CPS-FUND.INV-ACCT-UNIT-QTY
            ldaCpulfnd.getDetail_Record_Unit_Price().setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Unit_Value());                                                             //Natural: ASSIGN UNIT-PRICE := CPS-FUND.INV-ACCT-UNIT-VALUE
            ldaCpulfnd.getDetail_Record_Settlement_Amt().setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Settl_Amt());                                                          //Natural: ASSIGN SETTLEMENT-AMT := CPS-FUND.INV-ACCT-SETTL-AMT
            ldaCpulfnd.getDetail_Record_Dividend_Amt().setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Dvdnd_Amt());                                                            //Natural: ASSIGN DIVIDEND-AMT := CPS-FUND.INV-ACCT-DVDND-AMT
            ldaCpulfnd.getDetail_Record_Account_Code().setValue(ldaCpwlfund.getCps_Fund_Inv_Acct_Cde());                                                                  //Natural: ASSIGN ACCOUNT-CODE := CPS-FUND.INV-ACCT-CDE
            ldaCpulfnd.getDetail_Record_Insurance_Type().setValue(pymnt_Cntrct_Annty_Ins_Type);                                                                           //Natural: ASSIGN INSURANCE-TYPE := PYMNT.CNTRCT-ANNTY-INS-TYPE
            ldaCpulpym2.getPayment_Record_Settlement_Date().setValue(pymnt_Pymnt_Settlmnt_Dte);                                                                           //Natural: ASSIGN SETTLEMENT-DATE := PYMNT.PYMNT-SETTLMNT-DTE
            ldaCpulfnd.getDetail_Record_Key_Contract().setValue(pymnt_Cntrct_Cmbn_Nbr);                                                                                   //Natural: ASSIGN DETAIL-RECORD.KEY-CONTRACT := PYMNT.CNTRCT-CMBN-NBR
            ldaCpulfnd.getDetail_Record_Key_Payee().setValue(pymnt_Cntrct_Payee_Cde);                                                                                     //Natural: ASSIGN DETAIL-RECORD.KEY-PAYEE := PYMNT.CNTRCT-PAYEE-CDE
            ldaCpulfnd.getDetail_Record_Key_Origin().setValue(pymnt_Cntrct_Orgn_Cde);                                                                                     //Natural: ASSIGN DETAIL-RECORD.KEY-ORIGIN := PYMNT.CNTRCT-ORGN-CDE
            ldaCpulfnd.getDetail_Record_Key_Inverse_Date().setValue(pymnt_Cntrct_Invrse_Dte);                                                                             //Natural: ASSIGN DETAIL-RECORD.KEY-INVERSE-DATE := PYMNT.CNTRCT-INVRSE-DTE
            ldaCpulfnd.getDetail_Record_Key_Process_Seq().setValue(pymnt_Pymnt_Prcss_Seq_Nbr);                                                                            //Natural: ASSIGN DETAIL-RECORD.KEY-PROCESS-SEQ := PYMNT.PYMNT-PRCSS-SEQ-NBR
            ldaCpulfnd.getDetail_Record_Pnd_Seq_A().setValue(pymnt_Pymnt_Reqst_Nbr.getSubstring(29,7));                                                                   //Natural: ASSIGN DETAIL-RECORD.#SEQ-A := SUBSTRING ( PYMNT.PYMNT-REQST-NBR,29,7 )
            getWorkFiles().write(7, false, ldaCpulfnd.getDetail_Record());                                                                                                //Natural: WRITE WORK FILE 7 DETAIL-RECORD
            pnd_Data_Count.nadd(1);                                                                                                                                       //Natural: ADD 1 TO #DATA-COUNT
            pnd_Total_Control_Pnd_T_Gross.nadd(pnd_Detail_Pnd_Gross);                                                                                                     //Natural: ASSIGN #T-GROSS := #T-GROSS + #GROSS
            pnd_Total_Control_Pnd_T_Ivc.nadd(pnd_Detail_Pnd_Ivc);                                                                                                         //Natural: ASSIGN #T-IVC := #T-IVC + #DETAIL.#IVC
            pnd_Total_Control_Pnd_T_Tax.nadd(pnd_Detail_Pnd_Tax);                                                                                                         //Natural: ASSIGN #T-TAX := #T-TAX + #TAX
            pnd_Total_Control_Pnd_T_Deduc.nadd(pnd_Detail_Pnd_Deduc);                                                                                                     //Natural: ASSIGN #T-DEDUC := #T-DEDUC + #DEDUC
            pnd_Total_Control_Pnd_T_Int.nadd(pnd_Detail_Pnd_Int);                                                                                                         //Natural: ASSIGN #T-INT := #T-INT + #INT
            pnd_Total_Control_Pnd_T_Net.nadd(pnd_Detail_Pnd_Net);                                                                                                         //Natural: ASSIGN #T-NET := #T-NET + #NET
            pnd_Total_Control_Pnd_T_Qty.compute(new ComputeParameters(false, pnd_Total_Control_Pnd_T_Qty), pnd_Total_Control_Pnd_T_Value.add(pnd_Detail_Pnd_Value));      //Natural: ASSIGN #T-QTY := #T-VALUE + #VALUE
            pnd_Detail_Pnd_Gross.reset();                                                                                                                                 //Natural: RESET #GROSS #TAX #QTY #VALUE #DEDUC #INT #NET #TAXABLE
            pnd_Detail_Pnd_Tax.reset();
            pnd_Detail_Pnd_Qty.reset();
            pnd_Detail_Pnd_Value.reset();
            pnd_Detail_Pnd_Deduc.reset();
            pnd_Detail_Pnd_Int.reset();
            pnd_Detail_Pnd_Net.reset();
            pnd_Detail_Pnd_Taxable.reset();
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  A200-INV-SOL-FUND-PROCESS
    }
    private void sub_D200_Detail_Routine() throws Exception                                                                                                               //Natural: D200-DETAIL-ROUTINE
    {
        if (BLNatReinput.isReinput()) return;

        cpua1051_Detail_Pymt_Check_Pymt_Date.setValue(pnd_Date);                                                                                                          //Natural: MOVE #DATE TO PYMT-CHECK-PYMT-DATE
        if (condition(pymnt_Cntrct_Mode_Cde.greater(getZero())))                                                                                                          //Natural: IF PYMNT.CNTRCT-MODE-CDE > 0
        {
            short decideConditionsMet2171 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF PYMNT.CNTRCT-MODE-CDE;//Natural: VALUE 100
            if (condition((pymnt_Cntrct_Mode_Cde.equals(100))))
            {
                decideConditionsMet2171++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("M");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'M'
            }                                                                                                                                                             //Natural: VALUE 600 : 699
            else if (condition(((pymnt_Cntrct_Mode_Cde.greaterOrEqual(600) && pymnt_Cntrct_Mode_Cde.lessOrEqual(699)))))
            {
                decideConditionsMet2171++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("Q");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'Q'
            }                                                                                                                                                             //Natural: VALUE 700 : 799
            else if (condition(((pymnt_Cntrct_Mode_Cde.greaterOrEqual(700) && pymnt_Cntrct_Mode_Cde.lessOrEqual(799)))))
            {
                decideConditionsMet2171++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("S");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'S'
            }                                                                                                                                                             //Natural: VALUE 800 : 899
            else if (condition(((pymnt_Cntrct_Mode_Cde.greaterOrEqual(800) && pymnt_Cntrct_Mode_Cde.lessOrEqual(899)))))
            {
                decideConditionsMet2171++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("A");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'A'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                cpua1051_Detail_Pymt_Freq_Ind.setValue(" ");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := ' '
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pymnt_Split_Reasn_Cde.setValue(pymnt_Pymnt_Split_Reasn_Cde);                                                                                              //Natural: ASSIGN #PYMNT-SPLIT-REASN-CDE := PYMNT.PYMNT-SPLIT-REASN-CDE
            short decideConditionsMet2185 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #SWAT-FRQNCY-CDE;//Natural: VALUE '1'
            if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("1"))))
            {
                decideConditionsMet2185++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("M");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'M'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("2"))))
            {
                decideConditionsMet2185++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("Q");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'Q'
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("3"))))
            {
                decideConditionsMet2185++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("S");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'S'
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("4"))))
            {
                decideConditionsMet2185++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("A");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'A'
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("5"))))
            {
                decideConditionsMet2185++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("2");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := '2'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                cpua1051_Detail_Pymt_Freq_Ind.setValue(" ");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := ' '
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DBH3 START
        pnd_Rollover_Sw.reset();                                                                                                                                          //Natural: RESET #ROLLOVER-SW
        //* ***IF PYMNT.CNTRCT-ROLL-DEST-CDE NE ' '
        //* ***IF PYMNT.CNTRCT-ROLL-DEST-CDE = 'ROLL' OR = 'R457'         /*VIKRAM
        //* ***   OR PYMT-PAY-TYPE-REQ-IND = '8'            /* CTS1
        //* ***    OR PYMNT.PYMNT-PAY-TYPE-REQ-IND = 8       /* CTS1
        //* ROLL
        //* ROLL
        //* ROLL
        //* ROLL
        if (condition(pymnt_Pymnt_Pay_Type_Req_Ind.equals(8) || (pymnt_Cntrct_Roll_Dest_Cde.notEquals("  ") && pymnt_Cntrct_Roll_Dest_Cde.notEquals("3RD"))))             //Natural: IF PYMNT.PYMNT-PAY-TYPE-REQ-IND = 8 OR ( PYMNT.CNTRCT-ROLL-DEST-CDE NE '  ' AND PYMNT.CNTRCT-ROLL-DEST-CDE NE '3RD' )
        {
            pnd_Rollover_Sw.setValue(true);                                                                                                                               //Natural: ASSIGN #ROLLOVER-SW := TRUE
            //* ROLL
        }                                                                                                                                                                 //Natural: END-IF
        //*  DBH3 END
        //*  CTS4 STARTS
        pnd_Loan_Pymnt_Sw.reset();                                                                                                                                        //Natural: RESET #LOAN-PYMNT-SW
        if (condition(pymnt_Cntrct_Orgn_Cde.equals("OP")))                                                                                                                //Natural: IF PYMNT.CNTRCT-ORGN-CDE EQ 'OP'
        {
            if (condition(DbsUtil.maskMatches(pymnt_Cntrct_Type_Cde,"'L'.")))                                                                                             //Natural: IF PYMNT.CNTRCT-TYPE-CDE EQ MASK ( 'L'. )
            {
                pnd_Loan_Pymnt_Sw.setValue(true);                                                                                                                         //Natural: ASSIGN #LOAN-PYMNT-SW := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pymnt_Cntrct_Pymnt_Type_Ind.equals("L") && pymnt_Cntrct_Sttlmnt_Type_Ind.equals("O")))                                                      //Natural: IF PYMNT.CNTRCT-PYMNT-TYPE-IND = 'L' AND PYMNT.CNTRCT-STTLMNT-TYPE-IND = 'O'
                {
                    pnd_Loan_Pymnt_Sw.setValue(true);                                                                                                                     //Natural: ASSIGN #LOAN-PYMNT-SW := TRUE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  CTS4 ENDS
        //*  CHECK
        //*  EFT
        //*  GLOBAL
        //*  NON-PAYMENT
        //*  OTHER
        short decideConditionsMet2233 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE PYMNT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pymnt_Pymnt_Pay_Type_Req_Ind.equals(1))))
        {
            decideConditionsMet2233++;
            cpua1051_Detail_Pymt_Pay_Type_Req_Ind.setValue("C");                                                                                                          //Natural: ASSIGN PYMT-PAY-TYPE-REQ-IND := 'C'
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pymnt_Pymnt_Pay_Type_Req_Ind.equals(2))))
        {
            decideConditionsMet2233++;
            cpua1051_Detail_Pymt_Pay_Type_Req_Ind.setValue("E");                                                                                                          //Natural: ASSIGN PYMT-PAY-TYPE-REQ-IND := 'E'
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pymnt_Pymnt_Pay_Type_Req_Ind.equals(3))))
        {
            decideConditionsMet2233++;
            cpua1051_Detail_Pymt_Pay_Type_Req_Ind.setValue("G");                                                                                                          //Natural: ASSIGN PYMT-PAY-TYPE-REQ-IND := 'G'
        }                                                                                                                                                                 //Natural: VALUE 7:8
        else if (condition(((pymnt_Pymnt_Pay_Type_Req_Ind.greaterOrEqual(7) && pymnt_Pymnt_Pay_Type_Req_Ind.lessOrEqual(8)))))
        {
            decideConditionsMet2233++;
            cpua1051_Detail_Pymt_Pay_Type_Req_Ind.setValue("N");                                                                                                          //Natural: ASSIGN PYMT-PAY-TYPE-REQ-IND := 'N'
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            cpua1051_Detail_Pymt_Pay_Type_Req_Ind.setValue("O");                                                                                                          //Natural: ASSIGN PYMT-PAY-TYPE-REQ-IND := 'O'
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  D200-DETAIL-ROUTINE
    }
    private void sub_N100_Next_Pymnt_Dte() throws Exception                                                                                                               //Natural: N100-NEXT-PYMNT-DTE
    {
        if (BLNatReinput.isReinput()) return;

        pdaCpwa454.getCpwa454_Pnd_Cntrct_Ppcn_Nbr().setValue(cpua1051_Detail_Cntrct_Ppcn_Nbr);                                                                            //Natural: ASSIGN CPWA454.#CNTRCT-PPCN-NBR := CPUA1051-DETAIL.CNTRCT-PPCN-NBR
        pdaCpwa454.getCpwa454_Pnd_Cntrct_Payee_Cde().compute(new ComputeParameters(false, pdaCpwa454.getCpwa454_Pnd_Cntrct_Payee_Cde()), pnd_Cntrct_Payee_Cde_A4_Pnd_Cntrct_Payee_Cde_A2.val()); //Natural: ASSIGN CPWA454.#CNTRCT-PAYEE-CDE := VAL ( #CNTRCT-PAYEE-CDE-A2 )
        pdaCpwa454.getCpwa454_Pnd_Lst_Pymnt_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Date);                                                                //Natural: MOVE EDITED #DATE TO CPWA454.#LST-PYMNT-DTE ( EM = YYYYMMDD )
        DbsUtil.callnat(Cpwn454.class , getCurrentProcessState(), pdaCpwa454.getCpwa454());                                                                               //Natural: CALLNAT 'CPWN454' CPWA454
        if (condition(Global.isEscape())) return;
        cpua1051_Detail_Pymt_Next_Pymt_Date.setValueEdited(pdaCpwa454.getCpwa454_Pnd_Nxt_Pymnt_Dte(),new ReportEditMask("YYYYMMDD"));                                     //Natural: MOVE EDITED CPWA454.#NXT-PYMNT-DTE ( EM = YYYYMMDD ) TO CPUA1051-DETAIL.PYMT-NEXT-PYMT-DATE
        cpua1051_Detail_Pymt_Calc_Pymt_Date.setValueEdited(pdaCpwa454.getCpwa454_Pnd_Nxt_Fctr_Dte(),new ReportEditMask("YYYYMMDD"));                                      //Natural: MOVE EDITED CPWA454.#NXT-FCTR-DTE ( EM = YYYYMMDD ) TO CPUA1051-DETAIL.PYMT-CALC-PYMT-DATE
        pnd_Next_Pymnt_Date.setValue(true);                                                                                                                               //Natural: ASSIGN #NEXT-PYMNT-DATE := TRUE
        //*  N100-NEXT-PYMNT-DTE
    }
    private void sub_Get_Name() throws Exception                                                                                                                          //Natural: GET-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------
        cpua1051_Detail_Pymt_Annt_Name.reset();                                                                                                                           //Natural: RESET PYMT-ANNT-NAME
        pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr.setValue(pymnt_Cntrct_Ppcn_Nbr);                                                                                               //Natural: ASSIGN #A-CNTRCT-PPCN-NBR := PYMNT.CNTRCT-PPCN-NBR
        pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde.setValue(pymnt_Cntrct_Payee_Cde);                                                                                             //Natural: ASSIGN #A-CNTRCT-PAYEE-CDE := PYMNT.CNTRCT-PAYEE-CDE
        pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde.setValue(pymnt_Cntrct_Orgn_Cde);                                                                                               //Natural: ASSIGN #A-CNTRCT-ORGN-CDE := PYMNT.CNTRCT-ORGN-CDE
        pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte.setValue(pymnt_Cntrct_Invrse_Dte);                                                                                           //Natural: ASSIGN #A-CNTRCT-INVRSE-DTE := PYMNT.CNTRCT-INVRSE-DTE
        if (condition(pymnt_Cntrct_Orgn_Cde.equals("VT")))                                                                                                                //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'VT'
        {
            pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr.setValue(pymnt_Pymnt_Prcss_Seq_Num);                                                                                   //Natural: ASSIGN #A-PYMNT-PRCSS-SEQ-NBR := PYMNT.PYMNT-PRCSS-SEQ-NUM
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Addr_Key_Pnd_A_Pymnt_Prcss_Seq_Nbr.setValue(0);                                                                                                           //Natural: ASSIGN #A-PYMNT-PRCSS-SEQ-NBR := 0
        }                                                                                                                                                                 //Natural: END-IF
        //*  DBH4 START ADD CONDITION TO TEST FOR 'OP' PAYMENTS
        //*  IF PYMNT.CNTRCT-ORGN-CDE = 'OP'                             /* JWO14
        //*  JWO14
        if (condition(pymnt_Cntrct_Orgn_Cde.equals("OP") || pymnt_Cntrct_Orgn_Cde.equals("AD") || pymnt_Cntrct_Orgn_Cde.equals("IR") || pymnt_Cntrct_Orgn_Cde.equals("NV"))) //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'OP' OR = 'AD' OR = 'IR' OR = 'NV'
        {
            vw_addr.startDatabaseFind                                                                                                                                     //Natural: FIND ADDR WITH PYMNT-REQST-NBR = PYMNT.PYMNT-REQST-NBR
            (
            "FIND01",
            new Wc[] { new Wc("PYMNT_REQST_NBR", "=", pymnt_Pymnt_Reqst_Nbr, WcType.WITH) }
            );
            FIND01:
            while (condition(vw_addr.readNextRow("FIND01", true)))
            {
                vw_addr.setIfNotFoundControlFlag(false);
                if (condition(vw_addr.getAstCOUNTER().equals(0)))                                                                                                         //Natural: IF NO RECORDS FOUND
                {
                    cpua1051_Detail_Pymt_Annt_Name.setValue("NAME NOT IN ADDRESS FILE");                                                                                  //Natural: MOVE 'NAME NOT IN ADDRESS FILE' TO PYMT-ANNT-NAME
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-NOREC
                cpua1051_Detail_Pymt_Annt_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, addr_Ph_First_Name, "*", addr_Ph_Last_Name, "*",                  //Natural: COMPRESS ADDR.PH-FIRST-NAME '*' ADDR.PH-LAST-NAME '*' ADDR.PH-MIDDLE-NAME '*' INTO PYMT-ANNT-NAME LEAVING NO
                    addr_Ph_Middle_Name, "*"));
                DbsUtil.examine(new ExamineSource(cpua1051_Detail_Pymt_Annt_Name), new ExamineSearch("*"), new ExamineReplace(" "));                                      //Natural: EXAMINE PYMT-ANNT-NAME '*' REPLACE WITH ' '
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  DBH4 END
            //*  << SINHASN
            if (condition(pymnt_Pymnt_Cmbne_Ind.equals("Y") || pymnt_Cntrct_Ppcn_Nbr.notEquals(pymnt_Cntrct_Cmbn_Nbr.getSubstring(1,8))))                                 //Natural: IF PYMNT.PYMNT-CMBNE-IND = 'Y' OR PYMNT.CNTRCT-PPCN-NBR NE SUBSTRING ( PYMNT.CNTRCT-CMBN-NBR,1,8 )
            {
                pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr.setValue(pymnt_Cntrct_Cmbn_Nbr.getSubstring(1,8));                                                                     //Natural: ASSIGN #A-CNTRCT-PPCN-NBR := SUBSTRING ( PYMNT.CNTRCT-CMBN-NBR,1,8 )
            }                                                                                                                                                             //Natural: END-IF
            //*  >> SINHASN
            vw_addr.startDatabaseRead                                                                                                                                     //Natural: READ ( 1 ) ADDR BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #ADDR-KEY
            (
            "READ06",
            new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Addr_Key, WcType.BY) },
            new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
            1
            );
            READ06:
            while (condition(vw_addr.readNextRow("READ06")))
            {
                if (condition(pnd_Addr_Key_Pnd_A_Cntrct_Ppcn_Nbr.equals(addr_Cntrct_Ppcn_Nbr) && pnd_Addr_Key_Pnd_A_Cntrct_Payee_Cde.equals(addr_Cntrct_Payee_Cde)        //Natural: IF #A-CNTRCT-PPCN-NBR = ADDR.CNTRCT-PPCN-NBR AND #A-CNTRCT-PAYEE-CDE = ADDR.CNTRCT-PAYEE-CDE AND #A-CNTRCT-ORGN-CDE = ADDR.CNTRCT-ORGN-CDE AND #A-CNTRCT-INVRSE-DTE = ADDR.CNTRCT-INVRSE-DTE
                    && pnd_Addr_Key_Pnd_A_Cntrct_Orgn_Cde.equals(addr_Cntrct_Orgn_Cde) && pnd_Addr_Key_Pnd_A_Cntrct_Invrse_Dte.equals(addr_Cntrct_Invrse_Dte)))
                {
                    cpua1051_Detail_Pymt_Annt_Name.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, addr_Ph_First_Name, "*", addr_Ph_Last_Name,                   //Natural: COMPRESS ADDR.PH-FIRST-NAME '*' ADDR.PH-LAST-NAME '*' ADDR.PH-MIDDLE-NAME '*' INTO PYMT-ANNT-NAME LEAVING NO
                        "*", addr_Ph_Middle_Name, "*"));
                    DbsUtil.examine(new ExamineSource(cpua1051_Detail_Pymt_Annt_Name), new ExamineSearch("*"), new ExamineReplace(" "));                                  //Natural: EXAMINE PYMT-ANNT-NAME '*' REPLACE WITH ' '
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    cpua1051_Detail_Pymt_Annt_Name.setValue("NAME NOT IN ADDRESS FILE");                                                                                  //Natural: MOVE 'NAME NOT IN ADDRESS FILE' TO PYMT-ANNT-NAME
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            //*  DBH4
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Ticker_Sym() throws Exception                                                                                                                    //Natural: GET-TICKER-SYM
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------
        //* *NECA4000.REQUEST-IND               := ' '
        //* *NECA4000.FUNCTION-CDE              := 'FND'
        //* *NECA4000.INPT-KEY-OPTION-CDE       := '02'
        //* *NECA4000.FND-KEY2-ALPHA-FUND-CDE   := PYMNT.INV-ACCT-CDE (#D)
        //* *CALLNAT    'NECN4000'  NECA4000
        pnd_Acct_Alpha_1.setValue(pymnt_Inv_Acct_Cde.getValue(pnd_D));                                                                                                    //Natural: MOVE PYMNT.INV-ACCT-CDE ( #D ) TO #ACCT-ALPHA-1
        //*  JWO4 05/22/2014
        if (condition(pymnt_Inv_Acct_Cde.getValue(pnd_D).equals("G") || pymnt_Inv_Acct_Cde.getValue(pnd_D).equals("TG")))                                                 //Natural: IF PYMNT.INV-ACCT-CDE ( #D ) = 'G' OR = 'TG'
        {
            //*  JWO4 05/22/2014
            pnd_Acct_Alpha_1.setValue("T");                                                                                                                               //Natural: MOVE 'T' TO #ACCT-ALPHA-1
            //*  JWO4 05/22/2014
        }                                                                                                                                                                 //Natural: END-IF
        DbsUtil.examine(new ExamineSource(pnd_Tick_Array_Pnd_Acct_Alpha.getValue("*")), new ExamineSearch(pnd_Acct_Alpha_1), new ExamineGivingIndex(pnd_Index));          //Natural: EXAMINE #ACCT-ALPHA ( * ) FOR #ACCT-ALPHA-1 GIVING INDEX #INDEX
        //* *IF NECA4000.RETURN-CDE = '00'
        if (condition(pnd_Index.greater(getZero())))                                                                                                                      //Natural: IF #INDEX > 0
        {
            cpua1051_Detail_Pymt_Ticker.setValue(pnd_Tick_Array_Pnd_Tick.getValue(pnd_Index));                                                                            //Natural: MOVE #TICK ( #INDEX ) TO PYMT-TICKER
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            cpua1051_Detail_Pymt_Ticker.setValue(DbsUtil.compress(pymnt_Inv_Acct_Cde.getValue(pnd_D), "-NO TICKER"));                                                     //Natural: COMPRESS PYMNT.INV-ACCT-CDE ( #D ) '-NO TICKER' INTO PYMT-TICKER
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Populate_Payment_Detail() throws Exception                                                                                                           //Natural: POPULATE-PAYMENT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //*  -------------------------------------------------------------------
        ldaCpulpym2.getPayment_Record().reset();                                                                                                                          //Natural: RESET PAYMENT-RECORD
        short decideConditionsMet2329 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PYMNT.PYMNT-NBR NE 0
        if (condition(pymnt_Pymnt_Nbr.notEquals(getZero())))
        {
            decideConditionsMet2329++;
            ldaCpulpym2.getPayment_Record_Check_Number().setValue(pymnt_Pymnt_Nbr);                                                                                       //Natural: ASSIGN PAYMENT-RECORD.CHECK-NUMBER := PYMNT.PYMNT-NBR
        }                                                                                                                                                                 //Natural: WHEN PYMNT.PYMNT-CHECK-NBR NE 0
        else if (condition(pymnt_Pymnt_Check_Nbr.notEquals(getZero())))
        {
            decideConditionsMet2329++;
            ldaCpulpym2.getPayment_Record_Check_Alpha().setValueEdited(pymnt_Pymnt_Check_Nbr,new ReportEditMask("9999999"));                                              //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-NBR ( EM = 9999999 ) TO PAYMENT-RECORD.CHECK-ALPHA
        }                                                                                                                                                                 //Natural: WHEN PYMNT.PYMNT-CHECK-SCRTY-NBR NE 0
        else if (condition(pymnt_Pymnt_Check_Scrty_Nbr.notEquals(getZero())))
        {
            decideConditionsMet2329++;
            ldaCpulpym2.getPayment_Record_Check_Alpha().setValueEdited(pymnt_Pymnt_Check_Scrty_Nbr,new ReportEditMask("9999999"));                                        //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-SCRTY-NBR ( EM = 9999999 ) TO PAYMENT-RECORD.CHECK-ALPHA
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            if (condition(pymnt_Cntrct_Ppcn_Nbr.notEquals(pymnt_Cntrct_Cmbn_Nbr)))                                                                                        //Natural: IF PYMNT.CNTRCT-PPCN-NBR NE PYMNT.CNTRCT-CMBN-NBR
            {
                pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.setValue(pymnt_Cntrct_Orgn_Cde);                                                                                         //Natural: ASSIGN #WS-PRIME-KEY.CNTRCT-ORGN-CDE := PYMNT.CNTRCT-ORGN-CDE
                pnd_Ws_Prime_Key_Pymnt_Stats_Cde.setValue("C");                                                                                                           //Natural: ASSIGN #WS-PRIME-KEY.PYMNT-STATS-CDE := 'C'
                pnd_Ws_Prime_Key_Cntrct_Invrse_Dte.setValue(pymnt_Cntrct_Invrse_Dte);                                                                                     //Natural: ASSIGN #WS-PRIME-KEY.CNTRCT-INVRSE-DTE := PYMNT.CNTRCT-INVRSE-DTE
                pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Nbr.setValue(pymnt_Pymnt_Prcss_Seq_Nbr);                                                                                 //Natural: ASSIGN #WS-PRIME-KEY.PYMNT-PRCSS-SEQ-NBR := PYMNT.PYMNT-PRCSS-SEQ-NBR
                pnd_Ws_Prime_Key_Pymnt_Instmt_Nbr.reset();                                                                                                                //Natural: RESET #WS-PRIME-KEY.PYMNT-INSTMT-NBR
                vw_cmbn_Pay.startDatabaseRead                                                                                                                             //Natural: READ CMBN-PAY BY ORGN-STATUS-INVRSE-SEQ STARTING FROM #WS-PRIME-KEY
                (
                "READ07",
                new Wc[] { new Wc("ORGN_STATUS_INVRSE_SEQ", ">=", pnd_Ws_Prime_Key, WcType.BY) },
                new Oc[] { new Oc("ORGN_STATUS_INVRSE_SEQ", "ASC") }
                );
                READ07:
                while (condition(vw_cmbn_Pay.readNextRow("READ07")))
                {
                    if (condition(pnd_Ws_Prime_Key_Cntrct_Orgn_Cde.notEquals(cmbn_Pay_Cntrct_Orgn_Cde) || pnd_Ws_Prime_Key_Pymnt_Stats_Cde.notEquals(cmbn_Pay_Pymnt_Stats_Cde)  //Natural: IF #WS-PRIME-KEY.CNTRCT-ORGN-CDE NE CMBN-PAY.CNTRCT-ORGN-CDE OR #WS-PRIME-KEY.PYMNT-STATS-CDE NE CMBN-PAY.PYMNT-STATS-CDE OR #WS-PRIME-KEY.CNTRCT-INVRSE-DTE NE CMBN-PAY.CNTRCT-INVRSE-DTE OR #WS-PRIME-KEY.PYMNT-PRCSS-SEQ-NUM NE CMBN-PAY.PYMNT-PRCSS-SEQ-NUM
                        || pnd_Ws_Prime_Key_Cntrct_Invrse_Dte.notEquals(cmbn_Pay_Cntrct_Invrse_Dte) || pnd_Ws_Prime_Key_Pymnt_Prcss_Seq_Num.notEquals(cmbn_Pay_Pymnt_Prcss_Seq_Num)))
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                    ldaCpulpym2.getPayment_Record_Check_Number().setValue(cmbn_Pay_Pymnt_Nbr);                                                                            //Natural: ASSIGN PAYMENT-RECORD.CHECK-NUMBER := CMBN-PAY.PYMNT-NBR
                    if (condition(ldaCpulpym2.getPayment_Record_Check_Number().greater(getZero())))                                                                       //Natural: IF PAYMENT-RECORD.CHECK-NUMBER > 0
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaCpulpym2.getPayment_Record_Check_Alpha().equals(" ")))                                                                                           //Natural: IF PAYMENT-RECORD.CHECK-ALPHA = ' '
        {
            ldaCpulpym2.getPayment_Record_Check_Number().reset();                                                                                                         //Natural: RESET PAYMENT-RECORD.CHECK-NUMBER
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Origin_Code().setValue(pymnt_Cntrct_Orgn_Cde);                                                                                      //Natural: ASSIGN ORIGIN-CODE := PYMNT.CNTRCT-ORGN-CDE
        ldaCpulpym2.getPayment_Record_Ssn().setValue(pymnt_Annt_Soc_Sec_Nbr);                                                                                             //Natural: ASSIGN SSN := PYMNT.ANNT-SOC-SEC-NBR
        ldaCpulpym2.getPayment_Record_Vtran_Id().setValue(" ");                                                                                                           //Natural: ASSIGN VTRAN-ID := ' '
        pnd_Vtran_Id.reset();                                                                                                                                             //Natural: RESET #VTRAN-ID
        if (condition(pymnt_Pymnt_Reqst_Nbr.notEquals(" ")))                                                                                                              //Natural: IF PYMNT.PYMNT-REQST-NBR NE ' '
        {
            pnd_Reqst_Nbr_Rec_Type_Pnd_Reqst_Nbr.setValue(pymnt_Pymnt_Reqst_Nbr);                                                                                         //Natural: ASSIGN #REQST-NBR := PYMNT.PYMNT-REQST-NBR
            pnd_Reqst_Nbr_Rec_Type_Pnd_Rcrd_Typ.setValue("6");                                                                                                            //Natural: ASSIGN #RCRD-TYP := '6'
            vw_plan_View.startDatabaseRead                                                                                                                                //Natural: READ PLAN-VIEW BY REQST-NBR-REC-TYPE FROM #REQST-NBR-REC-TYPE
            (
            "READ08",
            new Wc[] { new Wc("REQST_NBR_REC_TYPE", ">=", pnd_Reqst_Nbr_Rec_Type, WcType.BY) },
            new Oc[] { new Oc("REQST_NBR_REC_TYPE", "ASC") }
            );
            READ08:
            while (condition(vw_plan_View.readNextRow("READ08")))
            {
                if (condition(plan_View_Pymnt_Reqst_Nbr.notEquals(pnd_Reqst_Nbr_Rec_Type_Pnd_Reqst_Nbr)))                                                                 //Natural: IF PLAN-VIEW.PYMNT-REQST-NBR NE #REQST-NBR
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                if (condition(plan_View_Cntrct_Rcrd_Typ.equals(pnd_Reqst_Nbr_Rec_Type_Pnd_Rcrd_Typ)))                                                                     //Natural: IF PLAN-VIEW.CNTRCT-RCRD-TYP EQ #RCRD-TYP
                {
                    ldaCpulpym2.getPayment_Record_Plan().setValue(plan_View_Plan_Num);                                                                                    //Natural: ASSIGN PAYMENT-RECORD.PLAN := PLAN-VIEW.PLAN-NUM
                    ldaCpulpym2.getPayment_Record_Subplan().setValue(plan_View_Sub_Plan_Num);                                                                             //Natural: ASSIGN PAYMENT-RECORD.SUBPLAN := PLAN-VIEW.SUB-PLAN-NUM
                    pnd_Vtran_Id_Pnd_Vtd_35.setValue(plan_View_Plan_Employer_Name.getValue(4));                                                                           //Natural: ASSIGN #VTD-35 := PLAN-VIEW.PLAN-EMPLOYER-NAME ( 4 )
                    pnd_Vtran_Id_Pnd_Vtd_10.setValue(plan_View_Plan_Type.getValue(4));                                                                                    //Natural: ASSIGN #VTD-10 := PLAN-VIEW.PLAN-TYPE ( 4 )
                    pnd_Vtran_Id_Pnd_Vtd_6.setValue(plan_View_Plan_Cash_Avail.getValue(4).getSubstring(1,6));                                                             //Natural: ASSIGN #VTD-6 := SUBSTRING ( PLAN-VIEW.PLAN-CASH-AVAIL ( 4 ) ,1,6 )
                    pnd_Vtran_Id_Pnd_Vtd_Filler.setValue("        ");                                                                                                     //Natural: ASSIGN #VTD-FILLER := '        '
                    pnd_Vtran_Id_Pnd_Vtd_Seq.setValue(plan_View_Plan_Cash_Avail.getValue(4).getSubstring(7,8));                                                           //Natural: ASSIGN #VTD-SEQ := SUBSTRING ( PLAN-VIEW.PLAN-CASH-AVAIL ( 4 ) ,7,8 )
                    if (condition(pnd_Vtran_Id.notEquals(" ")))                                                                                                           //Natural: IF #VTRAN-ID NE ' '
                    {
                        pnd_Vtran_Id_Pnd_Vtd_Rec_Code.setValue("5");                                                                                                      //Natural: ASSIGN #VTD-REC-CODE := '5'
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(ldaCpulpym2.getPayment_Record_Origin_Code().equals("IR")))                                                                              //Natural: IF ORIGIN-CODE = 'IR'
                    {
                        ldaCpulpym2.getPayment_Record_Plan().setValue(plan_View_Plan_Employer_Name.getValue(3));                                                          //Natural: ASSIGN PAYMENT-RECORD.PLAN := PLAN-VIEW.PLAN-EMPLOYER-NAME ( 3 )
                    }                                                                                                                                                     //Natural: END-IF
                    ldaCpulpym2.getPayment_Record_Vtran_Id().setValue(pnd_Vtran_Id);                                                                                      //Natural: ASSIGN PAYMENT-RECORD.VTRAN-ID := #VTRAN-ID
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            if (condition(ldaCpulpym2.getPayment_Record_Plan().equals(" ")))                                                                                              //Natural: IF PAYMENT-RECORD.PLAN EQ ' '
            {
                getReports().write(0, "Request Number",pymnt_Pymnt_Reqst_Nbr,"has no PLAN NUM");                                                                          //Natural: WRITE 'Request Number' PYMNT.PYMNT-REQST-NBR 'has no PLAN NUM'
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Transaction_Type().setValue(pymnt_Cntrct_Type_Cde);                                                                                 //Natural: ASSIGN TRANSACTION-TYPE := PYMNT.CNTRCT-TYPE-CDE
        ldaCpulpym2.getPayment_Record_Status().setValue(pymnt_Pymnt_Stats_Cde);                                                                                           //Natural: ASSIGN STATUS := PYMNT.PYMNT-STATS-CDE
        ldaCpulpym2.getPayment_Record_Spouse_Pay_Status().setValue(pymnt_Pymnt_Spouse_Pay_Stats);                                                                         //Natural: ASSIGN SPOUSE-PAY-STATUS := PYMNT.PYMNT-SPOUSE-PAY-STATS
        ldaCpulpym2.getPayment_Record_Lob_Code().setValue(pymnt_Cntrct_Lob_Cde);                                                                                          //Natural: ASSIGN PAYMENT-RECORD.LOB-CODE := PYMNT.CNTRCT-LOB-CDE
        ldaCpulpym2.getPayment_Record_Cancel_Redraw_Code().setValue(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde);                                                                 //Natural: ASSIGN CANCEL-REDRAW-CODE := PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        if (condition(pymnt_Notused1.equals("EFT REJ") || pymnt_Notused1.equals("EFT REV")))                                                                              //Natural: IF PYMNT.NOTUSED1 = 'EFT REJ' OR = 'EFT REV'
        {
            ldaCpulpym2.getPayment_Record_Cancel_Redraw_Code().setValue("Y");                                                                                             //Natural: ASSIGN CANCEL-REDRAW-CODE := 'Y'
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2400 = 0;                                                                                                                                //Natural: DECIDE ON FIRST PYMNT.CNTRCT-ORGN-CDE;//Natural: VALUE 'AP', 'NZ', 'DC', 'VT'
        if (condition((pymnt_Cntrct_Orgn_Cde.equals("AP") || pymnt_Cntrct_Orgn_Cde.equals("NZ") || pymnt_Cntrct_Orgn_Cde.equals("DC") || pymnt_Cntrct_Orgn_Cde.equals("VT"))))
        {
            decideConditionsMet2400++;
            ldaCpulpym2.getPayment_Record_Gross_Amt().nadd(pymnt_Inv_Acct_Settl_Amt.getValue("*"));                                                                       //Natural: ADD PYMNT.INV-ACCT-SETTL-AMT ( * ) TO GROSS-AMT
            ldaCpulpym2.getPayment_Record_Gross_Amt().nadd(pymnt_Inv_Acct_Dci_Amt.getValue("*"));                                                                         //Natural: ADD PYMNT.INV-ACCT-DCI-AMT ( * ) TO GROSS-AMT
            ldaCpulpym2.getPayment_Record_Gross_Amt().nadd(pymnt_Inv_Acct_Dpi_Amt.getValue("*"));                                                                         //Natural: ADD PYMNT.INV-ACCT-DPI-AMT ( * ) TO GROSS-AMT
            ldaCpulpym2.getPayment_Record_Gross_Amt().nadd(pymnt_Inv_Acct_Dvdnd_Amt.getValue("*"));                                                                       //Natural: ADD PYMNT.INV-ACCT-DVDND-AMT ( * ) TO GROSS-AMT
            ldaCpulpym2.getPayment_Record_Taxable_Amt().nadd(pymnt_Inv_Acct_Settl_Amt.getValue("*"));                                                                     //Natural: ADD PYMNT.INV-ACCT-SETTL-AMT ( * ) TO TAXABLE-AMT
            ldaCpulpym2.getPayment_Record_Taxable_Amt().nadd(pymnt_Inv_Acct_Dci_Amt.getValue("*"));                                                                       //Natural: ADD PYMNT.INV-ACCT-DCI-AMT ( * ) TO TAXABLE-AMT
            ldaCpulpym2.getPayment_Record_Taxable_Amt().nadd(pymnt_Inv_Acct_Dpi_Amt.getValue("*"));                                                                       //Natural: ADD PYMNT.INV-ACCT-DPI-AMT ( * ) TO TAXABLE-AMT
            ldaCpulpym2.getPayment_Record_Taxable_Amt().nadd(pymnt_Inv_Acct_Dvdnd_Amt.getValue("*"));                                                                     //Natural: ADD PYMNT.INV-ACCT-DVDND-AMT ( * ) TO TAXABLE-AMT
            ldaCpulpym2.getPayment_Record_Taxable_Amt().nsubtract(pymnt_Inv_Acct_Ivc_Amt.getValue("*"));                                                                  //Natural: SUBTRACT PYMNT.INV-ACCT-IVC-AMT ( * ) FROM TAXABLE-AMT
            ldaCpulpym2.getPayment_Record_Federal_Tax().nadd(pymnt_Inv_Acct_Fdrl_Tax_Amt.getValue("*"));                                                                  //Natural: ADD PYMNT.INV-ACCT-FDRL-TAX-AMT ( * ) TO FEDERAL-TAX
            ldaCpulpym2.getPayment_Record_State_Tax().nadd(pymnt_Inv_Acct_State_Tax_Amt.getValue("*"));                                                                   //Natural: ADD PYMNT.INV-ACCT-STATE-TAX-AMT ( * ) TO STATE-TAX
            ldaCpulpym2.getPayment_Record_Local_Tax().nadd(pymnt_Inv_Acct_Local_Tax_Amt.getValue("*"));                                                                   //Natural: ADD PYMNT.INV-ACCT-LOCAL-TAX-AMT ( * ) TO LOCAL-TAX
            ldaCpulpym2.getPayment_Record_Canadian_Tax().nadd(pymnt_Inv_Acct_Can_Tax_Amt.getValue("*"));                                                                  //Natural: ADD PYMNT.INV-ACCT-CAN-TAX-AMT ( * ) TO CANADIAN-TAX
            ldaCpulpym2.getPayment_Record_Fees().nadd(pymnt_Inv_Acct_Exp_Amt.getValue("*"));                                                                              //Natural: ADD PYMNT.INV-ACCT-EXP-AMT ( * ) TO FEES
            ldaCpulpym2.getPayment_Record_Net_Amt().nadd(pymnt_Inv_Acct_Net_Pymnt_Amt.getValue("*"));                                                                     //Natural: ADD PYMNT.INV-ACCT-NET-PYMNT-AMT ( * ) TO NET-AMT
            ldaCpulpym2.getPayment_Record_Deductions().nadd(pymnt_Pymnt_Ded_Amt.getValue("*"));                                                                           //Natural: ADD PYMNT.PYMNT-DED-AMT ( * ) TO DEDUCTIONS
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            ldaCpulpym2.getPayment_Record_Gross_Amt().compute(new ComputeParameters(false, ldaCpulpym2.getPayment_Record_Gross_Amt()), pymnt_Cntrct_Settl_Amt.add(pymnt_Cntrct_Dvdnd_Amt)); //Natural: ASSIGN GROSS-AMT := PYMNT.CNTRCT-SETTL-AMT + PYMNT.CNTRCT-DVDND-AMT
            ldaCpulpym2.getPayment_Record_Taxable_Amt().compute(new ComputeParameters(false, ldaCpulpym2.getPayment_Record_Taxable_Amt()), pymnt_Cntrct_Settl_Amt.add(pymnt_Cntrct_Dvdnd_Amt).subtract(pymnt_Cntrct_Ivc_Amt)); //Natural: ASSIGN TAXABLE-AMT := PYMNT.CNTRCT-SETTL-AMT + PYMNT.CNTRCT-DVDND-AMT - PYMNT.CNTRCT-IVC-AMT
            ldaCpulpym2.getPayment_Record_Federal_Tax().setValue(pymnt_Cntrct_Fdrl_Tax_Amt);                                                                              //Natural: ASSIGN FEDERAL-TAX := PYMNT.CNTRCT-FDRL-TAX-AMT
            ldaCpulpym2.getPayment_Record_State_Tax().setValue(pymnt_Cntrct_State_Tax_Amt);                                                                               //Natural: ASSIGN STATE-TAX := PYMNT.CNTRCT-STATE-TAX-AMT
            ldaCpulpym2.getPayment_Record_Local_Tax().setValue(pymnt_Cntrct_Local_Tax_Amt);                                                                               //Natural: ASSIGN LOCAL-TAX := PYMNT.CNTRCT-LOCAL-TAX-AMT
            ldaCpulpym2.getPayment_Record_Canadian_Tax().setValue(pymnt_Cntrct_Can_Tax_Amt);                                                                              //Natural: ASSIGN CANADIAN-TAX := PYMNT.CNTRCT-CAN-TAX-AMT
            ldaCpulpym2.getPayment_Record_Deductions().nadd(pymnt_Pymnt_Ded_Amt.getValue("*"));                                                                           //Natural: ADD PYMNT.PYMNT-DED-AMT ( * ) TO DEDUCTIONS
            ldaCpulpym2.getPayment_Record_Net_Amt().setValue(pymnt_Cntrct_Net_Pymnt_Amt);                                                                                 //Natural: ASSIGN NET-AMT := PYMNT.CNTRCT-NET-PYMNT-AMT
        }                                                                                                                                                                 //Natural: END-DECIDE
        ldaCpulpym2.getPayment_Record_Check_Date().setValue(pymnt_Pymnt_Check_Dte);                                                                                       //Natural: ASSIGN CHECK-DATE := PYMNT.PYMNT-CHECK-DTE
        ldaCpulpym2.getPayment_Record_Pymnt_Name().setValue(addr_Pymnt_Nme.getValue(1));                                                                                  //Natural: ASSIGN PYMNT-NAME := PYMNT-NME ( 1 )
        ldaCpulpym2.getPayment_Record_Addr_1().setValue(addr_Pymnt_Addr_Line1_Txt.getValue(1));                                                                           //Natural: ASSIGN ADDR-1 := PYMNT-ADDR-LINE1-TXT ( 1 )
        ldaCpulpym2.getPayment_Record_Addr_2().setValue(addr_Pymnt_Addr_Line2_Txt.getValue(1));                                                                           //Natural: ASSIGN ADDR-2 := PYMNT-ADDR-LINE2-TXT ( 1 )
        ldaCpulpym2.getPayment_Record_Addr_3().setValue(addr_Pymnt_Addr_Line3_Txt.getValue(1));                                                                           //Natural: ASSIGN ADDR-3 := PYMNT-ADDR-LINE3-TXT ( 1 )
        ldaCpulpym2.getPayment_Record_Addr_4().setValue(addr_Pymnt_Addr_Line4_Txt.getValue(1));                                                                           //Natural: ASSIGN ADDR-4 := PYMNT-ADDR-LINE4-TXT ( 1 )
        ldaCpulpym2.getPayment_Record_Addr_5().setValue(addr_Pymnt_Addr_Line5_Txt.getValue(1));                                                                           //Natural: ASSIGN ADDR-5 := PYMNT-ADDR-LINE5-TXT ( 1 )
        ldaCpulpym2.getPayment_Record_Addr_6().setValue(addr_Pymnt_Addr_Line6_Txt.getValue(1));                                                                           //Natural: ASSIGN ADDR-6 := PYMNT-ADDR-LINE6-TXT ( 1 )
        ldaCpulpym2.getPayment_Record_Bank_Name().setValue(" ");                                                                                                          //Natural: ASSIGN BANK-NAME := ' '
        ldaCpulpym2.getPayment_Record_Account_Number().setValue(addr_Pymnt_Eft_Acct_Nbr);                                                                                 //Natural: ASSIGN ACCOUNT-NUMBER := PYMNT-EFT-ACCT-NBR
        ldaCpulpym2.getPayment_Record_Routing_Number().setValue(addr_Pymnt_Eft_Transit_Id);                                                                               //Natural: ASSIGN ROUTING-NUMBER := PYMNT-EFT-TRANSIT-ID
        ldaCpulpym2.getPayment_Record_Sa_Pymnt_Name().setValue(addr_Pymnt_Nme.getValue(2));                                                                               //Natural: ASSIGN SA-PYMNT-NAME := PYMNT-NME ( 2 )
        ldaCpulpym2.getPayment_Record_Sa_Addr_1().setValue(addr_Pymnt_Addr_Line1_Txt.getValue(2));                                                                        //Natural: ASSIGN SA-ADDR-1 := PYMNT-ADDR-LINE1-TXT ( 2 )
        ldaCpulpym2.getPayment_Record_Sa_Addr_2().setValue(addr_Pymnt_Addr_Line2_Txt.getValue(2));                                                                        //Natural: ASSIGN SA-ADDR-2 := PYMNT-ADDR-LINE2-TXT ( 2 )
        ldaCpulpym2.getPayment_Record_Sa_Addr_3().setValue(addr_Pymnt_Addr_Line3_Txt.getValue(2));                                                                        //Natural: ASSIGN SA-ADDR-3 := PYMNT-ADDR-LINE3-TXT ( 2 )
        ldaCpulpym2.getPayment_Record_Sa_Addr_4().setValue(addr_Pymnt_Addr_Line4_Txt.getValue(2));                                                                        //Natural: ASSIGN SA-ADDR-4 := PYMNT-ADDR-LINE4-TXT ( 2 )
        ldaCpulpym2.getPayment_Record_Sa_Addr_5().setValue(addr_Pymnt_Addr_Line5_Txt.getValue(2));                                                                        //Natural: ASSIGN SA-ADDR-5 := PYMNT-ADDR-LINE5-TXT ( 2 )
        ldaCpulpym2.getPayment_Record_Sa_Addr_6().setValue(addr_Pymnt_Addr_Line6_Txt.getValue(2));                                                                        //Natural: ASSIGN SA-ADDR-6 := PYMNT-ADDR-LINE6-TXT ( 2 )
        //*  END-IF                                                      /* JWO16
        //*  SINHASN >>
        //*  END-IF                                                      /* JWO16
        //*  JWO
        //*  JWO15
        ldaCpulpym2.getPayment_Record_Contract_To().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pymnt_Cntrct_Da_Cref_1_Nbr, pymnt_Cntrct_Da_Cref_2_Nbr));    //Natural: COMPRESS PYMNT.CNTRCT-DA-CREF-1-NBR PYMNT.CNTRCT-DA-CREF-2-NBR INTO PAYMENT-RECORD.CONTRACT-TO LEAVING NO SPACE
        ldaCpulpym2.getPayment_Record_Key_Contract().setValue(pymnt_Cntrct_Cmbn_Nbr);                                                                                     //Natural: ASSIGN PAYMENT-RECORD.KEY-CONTRACT := PYMNT.CNTRCT-CMBN-NBR
        ldaCpulpym2.getPayment_Record_Key_Payee().setValue(pymnt_Cntrct_Payee_Cde);                                                                                       //Natural: ASSIGN PAYMENT-RECORD.KEY-PAYEE := PYMNT.CNTRCT-PAYEE-CDE
        ldaCpulpym2.getPayment_Record_Key_Origin().setValue(pymnt_Cntrct_Orgn_Cde);                                                                                       //Natural: ASSIGN PAYMENT-RECORD.KEY-ORIGIN := PYMNT.CNTRCT-ORGN-CDE
        ldaCpulpym2.getPayment_Record_Payment_Req_Type().setValue(pymnt_Pymnt_Pay_Type_Req_Ind);                                                                          //Natural: ASSIGN PAYMENT-RECORD.PAYMENT-REQ-TYPE := PYMNT.PYMNT-PAY-TYPE-REQ-IND
        //*  JWO15
        //*  JWO15
        if (condition(pymnt_Cntrct_Orgn_Cde.equals("VT")))                                                                                                                //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'VT'
        {
            ldaCpulpym2.getPayment_Record_Pay_Group().setValue(plan_View_Plan_Num);                                                                                       //Natural: ASSIGN PAYMENT-RECORD.PAY-GROUP := PLAN-VIEW.PLAN-NUM
            //*  JWO15
        }                                                                                                                                                                 //Natural: END-IF
        //*  JWO15
        if (condition(pymnt_Pymnt_Pay_Type_Req_Ind.equals(3) || pymnt_Pymnt_Pay_Type_Req_Ind.equals(4) || pymnt_Pymnt_Pay_Type_Req_Ind.equals(9)))                        //Natural: IF PYMNT.PYMNT-PAY-TYPE-REQ-IND = 3 OR = 4 OR = 9
        {
            //*  JWO15
                                                                                                                                                                          //Natural: PERFORM GET-IBAN
            sub_Get_Iban();
            if (condition(Global.isEscape())) {return;}
            //*  JWO15
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Hold_Code().setValue(pymnt_Cntrct_Hold_Cde);                                                                                        //Natural: ASSIGN PAYMENT-RECORD.HOLD-CODE := PYMNT.CNTRCT-HOLD-CDE
        ldaCpulpym2.getPayment_Record_Combined_Ind().setValue(pymnt_Pymnt_Cmbne_Ind);                                                                                     //Natural: ASSIGN PAYMENT-RECORD.COMBINED-IND := PYMNT.PYMNT-CMBNE-IND
        ldaCpulpym2.getPayment_Record_Checking_Savings_Ind().setValue(addr_Pymnt_Chk_Sav_Ind);                                                                            //Natural: ASSIGN PAYMENT-RECORD.CHECKING-SAVINGS-IND := ADDR.PYMNT-CHK-SAV-IND
        if (condition(((pymnt_Cntrct_Pymnt_Type_Ind.equals("N") && pymnt_Cntrct_Sttlmnt_Type_Ind.equals("Z")) ||pymnt_Roth_Money_Source.contains ("ROTH"))))              //Natural: IF ( PYMNT.CNTRCT-PYMNT-TYPE-IND = 'N' AND PYMNT.CNTRCT-STTLMNT-TYPE-IND = 'Z' ) OR PYMNT.ROTH-MONEY-SOURCE = SCAN ( 'ROTH' )
        {
            ldaCpulpym2.getPayment_Record_Check_Num_Suffix().setValue("R");                                                                                               //Natural: MOVE 'R' TO CHECK-NUM-SUFFIX
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pymnt_Cntrct_Type_Cde.equals("30")))                                                                                                                //Natural: IF PYMNT.CNTRCT-TYPE-CDE = '30'
        {
            ldaCpulpym2.getPayment_Record_Check_Num_Suffix().setValue("S");                                                                                               //Natural: MOVE 'S' TO CHECK-NUM-SUFFIX
        }                                                                                                                                                                 //Natural: END-IF
        //* *CPWAPYTP.CNTRCT-ORGN-CDE        :=PYMNT.CNTRCT-ORGN-CDE       /* CTS5
        //* *CPWAPYTP.PYMNT-PAY-TYPE-REQ-IND:= PYMNT.PYMNT-PAY-TYPE-REQ-IND
        //* *CALLNAT 'CPWNPYTP' CPWAPYTP
        //* *MOVE CPWAPYTP.PREFIX TO PAYMENT-RECORD.CHECK-NUM-PREFIX
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-NUM-PREFIX
        sub_Get_Check_Num_Prefix();
        if (condition(Global.isEscape())) {return;}
        //*  CTS5
        ldaCpulpym2.getPayment_Record_Check_Num_Prefix().setValue(pnd_Prefix);                                                                                            //Natural: MOVE #PREFIX TO PAYMENT-RECORD.CHECK-NUM-PREFIX
        if (condition(pymnt_Cntrct_Orgn_Cde.equals("OP")))                                                                                                                //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'OP'
        {
            short decideConditionsMet2485 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN PYMNT.CNTRCT-ROLL-DEST-CDE = 'ROLL' OR = 'R457'
            if (condition(pymnt_Cntrct_Roll_Dest_Cde.equals("ROLL") || pymnt_Cntrct_Roll_Dest_Cde.equals("R457")))
            {
                decideConditionsMet2485++;
                ldaCpulpym2.getPayment_Record_Omni_Trans_Type().setValue("R");                                                                                            //Natural: ASSIGN OMNI-TRANS-TYPE := 'R'
            }                                                                                                                                                             //Natural: WHEN PYMNT.PLAN-CNT = 1
            else if (condition(pymnt_Plan_Cnt.equals(1)))
            {
                decideConditionsMet2485++;
                ldaCpulpym2.getPayment_Record_Omni_Trans_Type().setValue("D");                                                                                            //Natural: ASSIGN OMNI-TRANS-TYPE := 'D'
            }                                                                                                                                                             //Natural: WHEN PYMNT.CNTRCT-TYPE-CDE = '30'
            else if (condition(pymnt_Cntrct_Type_Cde.equals("30")))
            {
                decideConditionsMet2485++;
                ldaCpulpym2.getPayment_Record_Omni_Trans_Type().setValue("S");                                                                                            //Natural: ASSIGN OMNI-TRANS-TYPE := 'S'
            }                                                                                                                                                             //Natural: WHEN PYMNT.CNTRCT-TYPE-CDE = '31'
            else if (condition(pymnt_Cntrct_Type_Cde.equals("31")))
            {
                decideConditionsMet2485++;
                ldaCpulpym2.getPayment_Record_Omni_Trans_Type().setValue("M");                                                                                            //Natural: ASSIGN OMNI-TRANS-TYPE := 'M'
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ldaCpulpym2.getPayment_Record_Omni_Trans_Type().setValue("C");                                                                                            //Natural: ASSIGN OMNI-TRANS-TYPE := 'C'
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Key_Inverse_Date().setValue(pymnt_Cntrct_Invrse_Dte);                                                                               //Natural: ASSIGN PAYMENT-RECORD.KEY-INVERSE-DATE := PYMNT.CNTRCT-INVRSE-DTE
        ldaCpulpym2.getPayment_Record_Key_Process_Seq().setValue(pymnt_Pymnt_Prcss_Seq_Nbr);                                                                              //Natural: ASSIGN PAYMENT-RECORD.KEY-PROCESS-SEQ := PYMNT.PYMNT-PRCSS-SEQ-NBR
        if (condition(pymnt_Pymnt_Reqst_Nbr.notEquals(" ")))                                                                                                              //Natural: IF PYMNT.PYMNT-REQST-NBR NE ' '
        {
            ldaCpulpym2.getPayment_Record_Pnd_Seq_A().setValue(pymnt_Pymnt_Reqst_Nbr.getSubstring(29,7));                                                                 //Natural: ASSIGN PAYMENT-RECORD.#SEQ-A := SUBSTRING ( PYMNT.PYMNT-REQST-NBR,29,7 )
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Combined_Contract_Number().setValue(pymnt_Cntrct_Cmbn_Nbr);                                                                         //Natural: ASSIGN PAYMENT-RECORD.COMBINED-CONTRACT-NUMBER := PYMNT.CNTRCT-CMBN-NBR
        ldaCpulpym2.getPayment_Record_Payee_Code().setValue(pymnt_Cntrct_Payee_Cde);                                                                                      //Natural: ASSIGN PAYEE-CODE := PYMNT.CNTRCT-PAYEE-CDE
        ldaCpulpym2.getPayment_Record_Contract_Number().setValue(pymnt_Cntrct_Ppcn_Nbr);                                                                                  //Natural: ASSIGN CONTRACT-NUMBER := PYMNT.CNTRCT-PPCN-NBR
        ldaCpulpym2.getPayment_Record_Settlement_Date().setValue(pymnt_Pymnt_Settlmnt_Dte);                                                                               //Natural: ASSIGN SETTLEMENT-DATE := PYMNT.PYMNT-SETTLMNT-DTE
        if (condition(pymnt_Cntrct_Mode_Cde.greater(getZero())))                                                                                                          //Natural: IF PYMNT.CNTRCT-MODE-CDE > 0
        {
            short decideConditionsMet2508 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF PYMNT.CNTRCT-MODE-CDE;//Natural: VALUE 100
            if (condition((pymnt_Cntrct_Mode_Cde.equals(100))))
            {
                decideConditionsMet2508++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("M");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'M'
            }                                                                                                                                                             //Natural: VALUE 600 : 699
            else if (condition(((pymnt_Cntrct_Mode_Cde.greaterOrEqual(600) && pymnt_Cntrct_Mode_Cde.lessOrEqual(699)))))
            {
                decideConditionsMet2508++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("Q");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'Q'
            }                                                                                                                                                             //Natural: VALUE 700 : 799
            else if (condition(((pymnt_Cntrct_Mode_Cde.greaterOrEqual(700) && pymnt_Cntrct_Mode_Cde.lessOrEqual(799)))))
            {
                decideConditionsMet2508++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("S");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'S'
            }                                                                                                                                                             //Natural: VALUE 800 : 899
            else if (condition(((pymnt_Cntrct_Mode_Cde.greaterOrEqual(800) && pymnt_Cntrct_Mode_Cde.lessOrEqual(899)))))
            {
                decideConditionsMet2508++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("A");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'A'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                cpua1051_Detail_Pymt_Freq_Ind.setValue(" ");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := ' '
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Pymnt_Split_Reasn_Cde.setValue(pymnt_Pymnt_Split_Reasn_Cde);                                                                                              //Natural: ASSIGN #PYMNT-SPLIT-REASN-CDE := PYMNT.PYMNT-SPLIT-REASN-CDE
            short decideConditionsMet2522 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #SWAT-FRQNCY-CDE;//Natural: VALUE '1'
            if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("1"))))
            {
                decideConditionsMet2522++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("M");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'M'
            }                                                                                                                                                             //Natural: VALUE '2'
            else if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("2"))))
            {
                decideConditionsMet2522++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("Q");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'Q'
            }                                                                                                                                                             //Natural: VALUE '3'
            else if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("3"))))
            {
                decideConditionsMet2522++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("S");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'S'
            }                                                                                                                                                             //Natural: VALUE '4'
            else if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("4"))))
            {
                decideConditionsMet2522++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("A");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := 'A'
            }                                                                                                                                                             //Natural: VALUE '5'
            else if (condition((pnd_Pymnt_Split_Reasn_Cde_Pnd_Swat_Frqncy_Cde.equals("5"))))
            {
                decideConditionsMet2522++;
                cpua1051_Detail_Pymt_Freq_Ind.setValue("2");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := '2'
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                cpua1051_Detail_Pymt_Freq_Ind.setValue(" ");                                                                                                              //Natural: ASSIGN PYMT-FREQ-IND := ' '
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Frequency().setValue(cpua1051_Detail_Pymt_Freq_Ind);                                                                                //Natural: ASSIGN PAYMENT-RECORD.FREQUENCY := PYMT-FREQ-IND
        pnd_Control_Count.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #CONTROL-COUNT
        //*  IF PYMNT.PYMNT-PAY-TYPE-REQ-IND = 8         /*    NON PAYMENT
        //*  DBH3
        //*  CTS4
        //*  RAH
        if (condition(pnd_Rollover_Sw.getBoolean() || pnd_Loan_Pymnt_Sw.getBoolean() || ldaCpulpym2.getPayment_Record_Omni_Trans_Type().equals("D")))                     //Natural: IF #ROLLOVER-SW OR #LOAN-PYMNT-SW OR OMNI-TRANS-TYPE = 'D'
        {
            //*    OR PYMNT.CNTRCT-HOLD-CDE = 'BCU '          /* JWO FIX DEFECT 75110
            ldaCpulpym2.getPayment_Record_Taxable_Amt().reset();                                                                                                          //Natural: RESET TAXABLE-AMT #TAXABLE
            pnd_Detail_Pnd_Taxable.reset();
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Pin().setValue(pymnt_Cntrct_Unq_Id_Nbr);                                                                                            //Natural: ASSIGN PIN := PYMNT.CNTRCT-UNQ-ID-NBR
        ldaCpulpym2.getPayment_Record_Annotation_Ind().setValue(pymnt_Pymnt_Annot_Ind);                                                                                   //Natural: ASSIGN ANNOTATION-IND := PYMNT.PYMNT-ANNOT-IND
        ldaCpulpym2.getPayment_Record_Residency().setValue(pymnt_Annt_Rsdncy_Cde);                                                                                        //Natural: ASSIGN RESIDENCY := PYMNT.ANNT-RSDNCY-CDE
        ldaCpulpym2.getPayment_Record_Deduction_Code().getValue(1,":",10).setValue(pymnt_Pymnt_Ded_Cde.getValue(1,":",10));                                               //Natural: MOVE PYMNT.PYMNT-DED-CDE ( 1:10 ) TO DEDUCTION-CODE ( 1:10 )
        ldaCpulpym2.getPayment_Record_Deduction_Amt().getValue(1,":",10).setValue(pymnt_Pymnt_Ded_Amt.getValue(1,":",10));                                                //Natural: MOVE PYMNT.PYMNT-DED-AMT ( 1:10 ) TO DEDUCTION-AMT ( 1:10 )
        ldaCpulpym2.getPayment_Record_Ivc_Amt().compute(new ComputeParameters(false, ldaCpulpym2.getPayment_Record_Ivc_Amt()), pymnt_Cntrct_Ivc_Amt.add(pymnt_Inv_Acct_Ivc_Amt.getValue("*"))); //Natural: ASSIGN IVC-AMT := PYMNT.CNTRCT-IVC-AMT + PYMNT.INV-ACCT-IVC-AMT ( * )
        if (condition(pymnt_Pymnt_Payee_Tx_Elct_Trggr.equals("N") || (pymnt_Pymnt_Payee_Tx_Elct_Trggr.equals(" ") && pymnt_Annt_Ctznshp_Cde.greater(1))))                 //Natural: IF PYMNT.PYMNT-PAYEE-TX-ELCT-TRGGR = 'N' OR ( PYMNT.PYMNT-PAYEE-TX-ELCT-TRGGR = ' ' AND PYMNT.ANNT-CTZNSHP-CDE > 1 ) THEN
        {
            ldaCpulpym2.getPayment_Record_Nra_Ind().setValue("Y");                                                                                                        //Natural: ASSIGN NRA-IND := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCpulpym2.getPayment_Record_Nra_Ind().reset();                                                                                                              //Natural: RESET NRA-IND
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(((pymnt_Cntrct_Pymnt_Type_Ind.equals("N") && pymnt_Cntrct_Sttlmnt_Type_Ind.equals("Z")) ||pymnt_Roth_Money_Source.contains ("ROTH"))))              //Natural: IF PYMNT.CNTRCT-PYMNT-TYPE-IND = 'N' AND PYMNT.CNTRCT-STTLMNT-TYPE-IND = 'Z' OR PYMNT.ROTH-MONEY-SOURCE = SCAN ( 'ROTH' )
        {
            ldaCpulpym2.getPayment_Record_Roth_Ind().setValue("Y");                                                                                                       //Natural: ASSIGN ROTH-IND := 'Y'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCpulpym2.getPayment_Record_Roth_Ind().reset();                                                                                                             //Natural: RESET ROTH-IND
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2563 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE PYMNT.PYMNT-STATS-CDE;//Natural: VALUE 'C'
        if (condition((pymnt_Pymnt_Stats_Cde.equals("C"))))
        {
            decideConditionsMet2563++;
            ldaCpulpym2.getPayment_Record_Status_Text().setValue("CASHED");                                                                                               //Natural: ASSIGN STATUS-TEXT := 'CASHED'
            //*  JWO 2010-11
            //*  JWO 2010-11
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'PR'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("PEND RETR");                                                                                        //Natural: ASSIGN STATUS-TEXT := 'PEND RETR'
                //*  JWO 2010-11
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'P', 'W'
        else if (condition((pymnt_Pymnt_Stats_Cde.equals("P") || pymnt_Pymnt_Stats_Cde.equals("W"))))
        {
            decideConditionsMet2563++;
            ldaCpulpym2.getPayment_Record_Status_Text().setValue("PROCESSED");                                                                                            //Natural: ASSIGN STATUS-TEXT := 'PROCESSED'
            //*  RS1
            if (condition(pymnt_Notused1.equals("EFT REJ")))                                                                                                              //Natural: IF PYMNT.NOTUSED1 = 'EFT REJ'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("REJECTED");                                                                                         //Natural: ASSIGN STATUS-TEXT := 'REJECTED'
            }                                                                                                                                                             //Natural: END-IF
            //*  MB        7/26
            if (condition(pymnt_Notused1.equals("EFT REV")))                                                                                                              //Natural: IF PYMNT.NOTUSED1 = 'EFT REV'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("REVERSED");                                                                                         //Natural: ASSIGN STATUS-TEXT := 'REVERSED'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CN'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("CANC NO RD");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'CANC NO RD'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SN'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("STOP NO RD");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'STOP NO RD'
            }                                                                                                                                                             //Natural: END-IF
            //*  JWO 2010-11
            //*  JWO 2010-11
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CP'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("CANC PEND");                                                                                        //Natural: ASSIGN STATUS-TEXT := 'CANC PEND'
                //*  JWO 2010-11
            }                                                                                                                                                             //Natural: END-IF
            //*  JWO 2010-11
            //*  JWO 2010-11
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SP'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("STOP PEND");                                                                                        //Natural: ASSIGN STATUS-TEXT := 'STOP PEND'
                //*  JWO 2010-11
            }                                                                                                                                                             //Natural: END-IF
            //*  JWO 2010-11
            //*  JWO 2010-11
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'RP'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("RDRW PEND");                                                                                        //Natural: ASSIGN STATUS-TEXT := 'RDRW PEND'
                //*  JWO 2010-11
            }                                                                                                                                                             //Natural: END-IF
            //*  JWO 2010-11
            //*  JWO 2010-11
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'PR'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("PEND RETR");                                                                                        //Natural: ASSIGN STATUS-TEXT := 'PEND RETR'
                //*  JWO 2010-11
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C")))                                                                                               //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("CANCELLED");                                                                                        //Natural: ASSIGN STATUS-TEXT := 'CANCELLED'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S")))                                                                                               //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'S'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("STOPPED");                                                                                          //Natural: ASSIGN STATUS-TEXT := 'STOPPED'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                               //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("REDRAWN");                                                                                          //Natural: ASSIGN STATUS-TEXT := 'REDRAWN'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("MA")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'MA'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("MAN ST NR");                                                                                        //Natural: ASSIGN STATUS-TEXT := 'MAN ST NR'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("MB")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'MB'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("MAN ST RE");                                                                                        //Natural: ASSIGN STATUS-TEXT := 'MAN ST RE'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("MC")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'MC'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("MAN CAN NR");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'MAN CAN NR'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("MD")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'MD'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("MAN CAN RE");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'MAN CAN RE'
                //*  VALUES 'D','W'(WAITING),'X'(CANCELLED)     /* 12/11/02 AR02
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT PRCSD");                                                                                           //Natural: ASSIGN STATUS-TEXT := 'WAIT PRCSD'
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C") || pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN")))                                           //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'CN'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT CANCL");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'WAIT CANCL'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S") || pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN")))                                           //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'S' OR = 'SN'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT STOPD");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'WAIT STOPD'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'CP'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT CP");                                                                                          //Natural: ASSIGN STATUS-TEXT := 'WAIT CP'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'SP'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT SP");                                                                                          //Natural: ASSIGN STATUS-TEXT := 'WAIT SP'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'RP'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT RP");                                                                                          //Natural: ASSIGN STATUS-TEXT := 'WAIT RP'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'PR'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT PR");                                                                                          //Natural: ASSIGN STATUS-TEXT := 'WAIT PR'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                               //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT RDRWN");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'WAIT RDRWN'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("AS")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'AS'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT AR ST");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'WAIT AR ST'
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pymnt_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("AP")))                                                                                              //Natural: IF PYMNT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'AP'
            {
                ldaCpulpym2.getPayment_Record_Status_Text().setValue("WAIT AR PN");                                                                                       //Natural: ASSIGN STATUS-TEXT := 'WAIT AR PN'
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *        END CHANGES TO ADD FIELDS                 /* JWO3  04/14/2014
        //*  JWO5
        ldaCpulpym2.getPayment_Record_Contract_Interest_Amt().nadd(pymnt_Inv_Acct_Dci_Amt.getValue("*"));                                                                 //Natural: ADD PYMNT.INV-ACCT-DCI-AMT ( * ) TO CONTRACT-INTEREST-AMT
        //*  JWO5
        ldaCpulpym2.getPayment_Record_Contract_Interest_Amt().nadd(pymnt_Inv_Acct_Dpi_Amt.getValue("*"));                                                                 //Natural: ADD PYMNT.INV-ACCT-DPI-AMT ( * ) TO CONTRACT-INTEREST-AMT
        //*  JWO5
        ldaCpulpym2.getPayment_Record_Contract_Interest_Amt().nadd(pymnt_Cntrct_Dci_Amt);                                                                                 //Natural: ADD PYMNT.CNTRCT-DCI-AMT TO CONTRACT-INTEREST-AMT
        //*  JWO5
        ldaCpulpym2.getPayment_Record_Contract_Interest_Amt().nadd(pymnt_Cntrct_Dpi_Amt);                                                                                 //Natural: ADD PYMNT.CNTRCT-DPI-AMT TO CONTRACT-INTEREST-AMT
        //*  JWO12
        ldaCpulpym2.getPayment_Record_Roth_Contrib().reset();                                                                                                             //Natural: RESET ROTH-CONTRIB PAYMENT-RECORD.ROTH-EARNINGS ROTH-FIRST-DTE
        ldaCpulpym2.getPayment_Record_Roth_Earnings().reset();
        ldaCpulpym2.getPayment_Record_Roth_First_Dte().reset();
        //*  JWO5
        if (condition(pymnt_Roth_First_Contrib_Dte.notEquals(getZero())))                                                                                                 //Natural: IF PYMNT.ROTH-FIRST-CONTRIB-DTE NE 0
        {
            //*  JWO5
            //*  JWO5
            pnd_Roth_First_Contrib_Dte_N_Pnd_Roth_First_Contrib_Dte_A.setValue(pymnt_Roth_First_Contrib_Dte);                                                             //Natural: MOVE PYMNT.ROTH-FIRST-CONTRIB-DTE TO #ROTH-FIRST-CONTRIB-DTE-A
            //*  JWO5
            //*  JWO5
            ldaCpulpym2.getPayment_Record_Roth_First_Dte().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Roth_First_Contrib_Dte_N_Roth_Mm,                 //Natural: COMPRESS ROTH-MM '/' ROTH-DD '/' ROTH-YY INTO ROTH-FIRST-DTE LEAVING NO SPACE
                "/", pnd_Roth_First_Contrib_Dte_N_Roth_Dd, "/", pnd_Roth_First_Contrib_Dte_N_Roth_Yy));
            //*  JWO5
            //*  JWO12
            //*  JWO12
            //*  JWO12
            if (condition(pymnt_Pymnt_Reqst_Nbr.notEquals(" ")))                                                                                                          //Natural: IF PYMNT.PYMNT-REQST-NBR NE ' '
            {
                ldaCpulpym2.getPayment_Record_Roth_Contrib().setValue(plan_View_Roth_Contrb);                                                                             //Natural: ASSIGN ROTH-CONTRIB := PLAN-VIEW.ROTH-CONTRB
                ldaCpulpym2.getPayment_Record_Roth_Earnings().setValue(plan_View_Roth_Earnings);                                                                          //Natural: ASSIGN PAYMENT-RECORD.ROTH-EARNINGS := PLAN-VIEW.ROTH-EARNINGS
                //*  JWO5
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  JWO5
                ldaCpulpym2.getPayment_Record_Roth_Contrib().nadd(pymnt_Inv_Acct_Ivc_Amt.getValue("*"));                                                                  //Natural: ADD PYMNT.INV-ACCT-IVC-AMT ( * ) TO ROTH-CONTRIB
                //*  JWO12
                //*  JWO12
                ldaCpulpym2.getPayment_Record_Roth_Earnings().nadd(pymnt_Inv_Acct_Settl_Amt.getValue("*"));                                                               //Natural: ADD PYMNT.INV-ACCT-SETTL-AMT ( * ) TO PAYMENT-RECORD.ROTH-EARNINGS
                //*  JWO12
                //*  JWO12
                ldaCpulpym2.getPayment_Record_Roth_Earnings().nadd(pymnt_Inv_Acct_Dvdnd_Amt.getValue("*"));                                                               //Natural: ADD PYMNT.INV-ACCT-DVDND-AMT ( * ) TO PAYMENT-RECORD.ROTH-EARNINGS
                //*  JWO12
                ldaCpulpym2.getPayment_Record_Roth_Earnings().nsubtract(ldaCpulpym2.getPayment_Record_Roth_Contrib());                                                    //Natural: SUBTRACT ROTH-CONTRIB FROM PAYMENT-RECORD.ROTH-EARNINGS
                //*  JWO5
            }                                                                                                                                                             //Natural: END-IF
            //*  JWO5
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  JWO5
            //*  JWO5
            //*  JWO12
            //*  JWO5
            ldaCpulpym2.getPayment_Record_Roth_Contrib().reset();                                                                                                         //Natural: RESET ROTH-CONTRIB PAYMENT-RECORD.ROTH-EARNINGS ROTH-FIRST-DTE
            ldaCpulpym2.getPayment_Record_Roth_Earnings().reset();
            ldaCpulpym2.getPayment_Record_Roth_First_Dte().reset();
            //*  JWO5
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Check_Cash_Dte().setValue(pymnt_Pymnt_Eft_Dte);                                                                                     //Natural: ASSIGN CHECK-CASH-DTE := PYMNT.PYMNT-EFT-DTE
        ldaCpulpym2.getPayment_Record_Original_Check_Nbr().setValue(pymnt_Cnr_Orgnl_Pymnt_Nbr);                                                                           //Natural: ASSIGN ORIGINAL-CHECK-NBR := PYMNT.CNR-ORGNL-PYMNT-NBR
        if (condition(pymnt_Pymnt_Pay_Type_Req_Ind.equals(2)))                                                                                                            //Natural: IF PYMNT.PYMNT-PAY-TYPE-REQ-IND = 2
        {
                                                                                                                                                                          //Natural: PERFORM ADJUST-EFFECTIVE-DATE
            sub_Adjust_Effective_Date();
            if (condition(Global.isEscape())) {return;}
            ldaCpulpym2.getPayment_Record_Eft_Effective_Date().setValue(pnd_Eft_Eff_Date);                                                                                //Natural: ASSIGN EFT-EFFECTIVE-DATE := #EFT-EFF-DATE
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulpym2.getPayment_Record_Apin().setValue(pymnt_Advisor_Pin);                                                                                                 //Natural: ASSIGN APIN := PYMNT.ADVISOR-PIN
        //* **********     END CHANGES FOR TAG                         /* JWO8
        if (condition(pymnt_Cntrct_Orgn_Cde.equals("IR")))                                                                                                                //Natural: IF PYMNT.CNTRCT-ORGN-CDE = 'IR'
        {
            ldaCpwlfund.getVw_cps_Fund().startDatabaseRead                                                                                                                //Natural: READ ( 1 ) CPS-FUND BY PYMNT-REQST-INSTMT-SEQ STARTING FROM PYMNT.PYMNT-REQST-NBR
            (
            "READ09",
            new Wc[] { new Wc("PYMNT_REQST_INSTMT_SEQ", ">=", pymnt_Pymnt_Reqst_Nbr, WcType.BY) },
            new Oc[] { new Oc("PYMNT_REQST_INSTMT_SEQ", "ASC") },
            1
            );
            READ09:
            while (condition(ldaCpwlfund.getVw_cps_Fund().readNextRow("READ09")))
            {
                ldaCpulpym2.getPayment_Record_Reason_Code().setValueEdited(new ReportEditMask("9999"),ldaCpwlfund.getCps_Fund_Refund_Reason());                           //Natural: MOVE EDITED REFUND-REASON TO REASON-CODE ( EM = 9999 )
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
            pnd_Rt_Super1_A_I_Ind.setValue("A");                                                                                                                          //Natural: ASSIGN #RT-SUPER1.A-I-IND := 'A'
            pnd_Rt_Super1_Table_Id.setValue("CREAS");                                                                                                                     //Natural: ASSIGN #RT-SUPER1.TABLE-ID := 'CREAS'
            pnd_Rt_Super1_Short_Key.setValue(ldaCpwlfund.getCps_Fund_Refund_Reason());                                                                                    //Natural: ASSIGN #RT-SUPER1.SHORT-KEY := REFUND-REASON
            pnd_Rt_Super1_Long_Key.setValue(" ");                                                                                                                         //Natural: ASSIGN #RT-SUPER1.LONG-KEY := ' '
            ldaCpsl100.getVw_rt().startDatabaseRead                                                                                                                       //Natural: READ ( 1 ) RT BY RT-SUPER1 STARTING FROM #RT-SUPER1
            (
            "READ10",
            new Wc[] { new Wc("RT_SUPER1", ">=", pnd_Rt_Super1, WcType.BY) },
            new Oc[] { new Oc("RT_SUPER1", "ASC") },
            1
            );
            READ10:
            while (condition(ldaCpsl100.getVw_rt().readNextRow("READ10")))
            {
                if (condition(ldaCpsl100.getRt_Rt_Table_Id().notEquals(pnd_Rt_Super1_Table_Id)))                                                                          //Natural: IF RT.RT-TABLE-ID NE #RT-SUPER1.TABLE-ID
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                ldaCpulpym2.getPayment_Record_Reason_Description().setValue(ldaCpsl100.getRt_Rt_Desc1());                                                                 //Natural: MOVE RT.RT-DESC1 TO REASON-DESCRIPTION
            }                                                                                                                                                             //Natural: END-READ
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet2739 = 0;                                                                                                                                //Natural: DECIDE ON EVERY VALUE PYMNT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 3
        if (condition(pymnt_Pymnt_Pay_Type_Req_Ind.equals(3)))
        {
            decideConditionsMet2739++;
            ldaCpulpym2.getPayment_Record_Country_Code().setValue(pymnt_Annt_Rsdncy_Cde);                                                                                 //Natural: ASSIGN COUNTRY-CODE := PYMNT.ANNT-RSDNCY-CDE
        }                                                                                                                                                                 //Natural: VALUE 4, 9
        if (condition((pymnt_Pymnt_Pay_Type_Req_Ind.equals(4)) || (pymnt_Pymnt_Pay_Type_Req_Ind.equals(9))))
        {
            decideConditionsMet2739++;
            if (condition(pymnt_Annt_Rsdncy_Cde.equals(ldaCpulpym2.getPayment_Record_Intl_Iban().getSubstring(1,2))))                                                     //Natural: IF PYMNT.ANNT-RSDNCY-CDE = SUBSTRING ( INTL-IBAN,1,2 )
            {
                ldaCpulpym2.getPayment_Record_Iban_Ind().setValue("I");                                                                                                   //Natural: ASSIGN IBAN-IND := 'I'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaCpulpym2.getPayment_Record_Iban_Ind().setValue("A");                                                                                                   //Natural: ASSIGN IBAN-IND := 'A'
            }                                                                                                                                                             //Natural: END-IF
            ldaCpulpym2.getPayment_Record_Country_Code().setValue(pymnt_Annt_Rsdncy_Cde);                                                                                 //Natural: ASSIGN COUNTRY-CODE := PYMNT.ANNT-RSDNCY-CDE
        }                                                                                                                                                                 //Natural: NONE VALUE
        if (condition(decideConditionsMet2739 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        getWorkFiles().write(6, false, ldaCpulpym2.getPayment_Record());                                                                                                  //Natural: WRITE WORK FILE 6 PAYMENT-RECORD
        //*  JWO6
        if (condition(pymnt_Pymnt_Annot_Ind.equals("Y")))                                                                                                                 //Natural: IF PYMNT.PYMNT-ANNOT-IND = 'Y'
        {
            //*  JWO6
                                                                                                                                                                          //Natural: PERFORM WRITE-ANNOTATIONS
            sub_Write_Annotations();
            if (condition(Global.isEscape())) {return;}
            //*  JWO6
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Check_Num_Prefix() throws Exception                                                                                                              //Natural: GET-CHECK-NUM-PREFIX
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        pnd_Prefix.reset();                                                                                                                                               //Natural: RESET #PREFIX #SHORT-KEY
        pnd_Short_Key.reset();
        if (condition(pymnt_Pymnt_Pay_Type_Req_Ind.equals(8)))                                                                                                            //Natural: IF PYMNT.PYMNT-PAY-TYPE-REQ-IND = 8
        {
            FOR02:                                                                                                                                                        //Natural: FOR #I 1 TO 3
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(3)); pnd_I.nadd(1))
            {
                pnd_Short_Key.reset();                                                                                                                                    //Natural: RESET #SHORT-KEY
                short decideConditionsMet2767 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE OF #I;//Natural: VALUE 1
                if (condition((pnd_I.equals(1))))
                {
                    decideConditionsMet2767++;
                    pnd_Short_Key_Pnd_Rf_Pay_Type.setValue(pymnt_Pymnt_Pay_Type_Req_Ind);                                                                                 //Natural: ASSIGN #RF-PAY-TYPE := PYMNT.PYMNT-PAY-TYPE-REQ-IND
                    pnd_Short_Key_Pnd_Rf_Roll_Dest_Cde.setValue(pymnt_Cntrct_Roll_Dest_Cde);                                                                              //Natural: ASSIGN #RF-ROLL-DEST-CDE := PYMNT.CNTRCT-ROLL-DEST-CDE
                    pnd_Short_Key_Pnd_Rf_Orgn.setValue(pymnt_Cntrct_Orgn_Cde);                                                                                            //Natural: ASSIGN #RF-ORGN := PYMNT.CNTRCT-ORGN-CDE
                    pnd_Short_Key_Pnd_Rf_Pymnt_Type.setValue(pymnt_Cntrct_Pymnt_Type_Ind);                                                                                //Natural: ASSIGN #RF-PYMNT-TYPE := PYMNT.CNTRCT-PYMNT-TYPE-IND
                    pnd_Short_Key_Pnd_Rf_Settl_Type.setValue(pymnt_Cntrct_Sttlmnt_Type_Ind);                                                                              //Natural: ASSIGN #RF-SETTL-TYPE := PYMNT.CNTRCT-STTLMNT-TYPE-IND
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_I.equals(2))))
                {
                    decideConditionsMet2767++;
                    pnd_Short_Key_Pnd_Rf_Pay_Type.setValue(pymnt_Pymnt_Pay_Type_Req_Ind);                                                                                 //Natural: ASSIGN #RF-PAY-TYPE := PYMNT.PYMNT-PAY-TYPE-REQ-IND
                    pnd_Short_Key_Pnd_Rf_Roll_Dest_Cde.setValue(pymnt_Cntrct_Roll_Dest_Cde);                                                                              //Natural: ASSIGN #RF-ROLL-DEST-CDE := PYMNT.CNTRCT-ROLL-DEST-CDE
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_I.equals(3))))
                {
                    decideConditionsMet2767++;
                    pnd_Short_Key_Pnd_Rf_Pay_Type.setValue(pymnt_Pymnt_Pay_Type_Req_Ind);                                                                                 //Natural: ASSIGN #RF-PAY-TYPE := PYMNT.PYMNT-PAY-TYPE-REQ-IND
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                DbsUtil.examine(new ExamineSource(pnd_Short_Key_Array.getValue("*")), new ExamineSearch(pnd_Short_Key), new ExamineGivingIndex(pnd_Pos));                 //Natural: EXAMINE #SHORT-KEY-ARRAY ( * ) FOR #SHORT-KEY GIVING INDEX IN #POS
                //*  SUCCESSFUL
                if (condition(pnd_Pos.greater(getZero())))                                                                                                                //Natural: IF #POS GT 0
                {
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Short_Key_Pnd_Rf_Pay_Type.setValue(pymnt_Pymnt_Pay_Type_Req_Ind);                                                                                         //Natural: ASSIGN #RF-PAY-TYPE := PYMNT.PYMNT-PAY-TYPE-REQ-IND
            DbsUtil.examine(new ExamineSource(pnd_Short_Key_Array.getValue("*")), new ExamineSearch(pnd_Short_Key), new ExamineGivingIndex(pnd_Pos));                     //Natural: EXAMINE #SHORT-KEY-ARRAY ( * ) FOR #SHORT-KEY GIVING INDEX IN #POS
        }                                                                                                                                                                 //Natural: END-IF
        //*  SUCCESSFUL
        if (condition(pnd_Pos.greater(getZero())))                                                                                                                        //Natural: IF #POS GT 0
        {
            pnd_Prefix.setValue(pnd_Prefix_Array.getValue(pnd_Pos));                                                                                                      //Natural: MOVE #PREFIX-ARRAY ( #POS ) TO #PREFIX
            pnd_Pos.reset();                                                                                                                                              //Natural: RESET #POS
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CHECK-NUM-PREFIX
    }
    private void sub_Write_Annotations() throws Exception                                                                                                                 //Natural: WRITE-ANNOTATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************************************************
        ldaCpulannt.getAnnot1_Record().reset();                                                                                                                           //Natural: RESET ANNOT1-RECORD ANNOT2-RECORD
        ldaCpulann2.getAnnot2_Record().reset();
        if (condition(pymnt_Pymnt_Reqst_Nbr.notEquals(" ")))                                                                                                              //Natural: IF PYMNT.PYMNT-REQST-NBR NE ' '
        {
            pnd_Annot_Key2_Pnd_Annot_Pymnt_Reqst_Nbr.setValue(pymnt_Pymnt_Reqst_Nbr);                                                                                     //Natural: ASSIGN #ANNOT-PYMNT-REQST-NBR = PYMNT.PYMNT-REQST-NBR
            pnd_Annot_Key2_Pnd_Annot_Rec_Type.setValue("4");                                                                                                              //Natural: ASSIGN #ANNOT-REC-TYPE = '4'
            vw_fcp_Cons_Annot1.startDatabaseFind                                                                                                                          //Natural: FIND FCP-CONS-ANNOT1 WITH REQST-NBR-REC-TYPE = #ANNOT-KEY2
            (
            "FIND02",
            new Wc[] { new Wc("REQST_NBR_REC_TYPE", "=", pnd_Annot_Key2, WcType.WITH) }
            );
            FIND02:
            while (condition(vw_fcp_Cons_Annot1.readNextRow("FIND02")))
            {
                vw_fcp_Cons_Annot1.setIfNotFoundControlFlag(false);
                if (condition(!(fcp_Cons_Annot1_Cntrct_Rcrd_Typ.equals("4"))))                                                                                            //Natural: ACCEPT IF FCP-CONS-ANNOT1.CNTRCT-RCRD-TYP = '4'
                {
                    continue;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Annot_Key_Pnd_Annot_Ppcn.setValue(pymnt_Cntrct_Ppcn_Nbr);                                                                                                 //Natural: ASSIGN #ANNOT-PPCN = PYMNT.CNTRCT-PPCN-NBR
            pnd_Annot_Key_Pnd_Annot_Inst.setValue(pymnt_Pymnt_Instmt_Nbr);                                                                                                //Natural: ASSIGN #ANNOT-INST = PYMNT.PYMNT-INSTMT-NBR
            pnd_Annot_Key_Pnd_Annot_Orgn.setValue(pymnt_Cntrct_Orgn_Cde);                                                                                                 //Natural: ASSIGN #ANNOT-ORGN = PYMNT.CNTRCT-ORGN-CDE
            pnd_Annot_Key_Pnd_Annot_Invrs.setValue(pymnt_Cntrct_Invrse_Dte);                                                                                              //Natural: ASSIGN #ANNOT-INVRS = PYMNT.CNTRCT-INVRSE-DTE
            pnd_Annot_Key_Pnd_Annot_Seq.setValue(pymnt_Pymnt_Prcss_Seq_Num);                                                                                              //Natural: ASSIGN #ANNOT-SEQ = PYMNT.PYMNT-PRCSS-SEQ-NUM
            vw_fcp_Cons_Annot1.startDatabaseFind                                                                                                                          //Natural: FIND FCP-CONS-ANNOT1 WITH PPCN-INV-ORGN-PRCSS-INST = #ANNOT-KEY
            (
            "FIND03",
            new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", "=", pnd_Annot_Key, WcType.WITH) }
            );
            FIND03:
            while (condition(vw_fcp_Cons_Annot1.readNextRow("FIND03")))
            {
                vw_fcp_Cons_Annot1.setIfNotFoundControlFlag(false);
                if (condition(!(fcp_Cons_Annot1_Cntrct_Rcrd_Typ.equals("4"))))                                                                                            //Natural: ACCEPT IF FCP-CONS-ANNOT1.CNTRCT-RCRD-TYP = '4'
                {
                    continue;
                }
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        ldaCpulannt.getAnnot1_Record_Legacy_Eft_Key().setValue(ldaCpulpym2.getPayment_Record_Legacy_Eft_Key());                                                           //Natural: MOVE PAYMENT-RECORD.LEGACY-EFT-KEY TO ANNOT1-RECORD.LEGACY-EFT-KEY
        ldaCpulannt.getAnnot1_Record_Check_Number().setValue(ldaCpulpym2.getPayment_Record_Check_Number());                                                               //Natural: MOVE PAYMENT-RECORD.CHECK-NUMBER TO ANNOT1-RECORD.CHECK-NUMBER
        ldaCpulannt.getAnnot1_Record_Text_Line1().setValue(fcp_Cons_Annot1_Pymnt_Rmrk_Line1_Txt);                                                                         //Natural: ASSIGN TEXT-LINE1 := PYMNT-RMRK-LINE1-TXT
        ldaCpulannt.getAnnot1_Record_Text_Line2().setValue(fcp_Cons_Annot1_Pymnt_Rmrk_Line2_Txt);                                                                         //Natural: ASSIGN TEXT-LINE2 := PYMNT-RMRK-LINE2-TXT
        ldaCpulannt.getAnnot1_Record_Annot_Dte().getValue("*").setValue(fcp_Cons_Annot1_Pymnt_Annot_Dte.getValue("*"));                                                   //Natural: MOVE PYMNT-ANNOT-DTE ( * ) TO ANNOT-DTE ( * )
        ldaCpulannt.getAnnot1_Record_Annot_Id().getValue("*").setValue(fcp_Cons_Annot1_Pymnt_Annot_Id_Nbr.getValue("*"));                                                 //Natural: MOVE PYMNT-ANNOT-ID-NBR ( * ) TO ANNOT-ID ( * )
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO 20
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(20)); pnd_I.nadd(1))
        {
            if (condition(ldaCpulannt.getAnnot1_Record_Annot_Dte().getValue(pnd_I).notEquals(" ")))                                                                       //Natural: IF ANNOT-DTE ( #I ) NE ' '
            {
                ldaCpulannt.getAnnot1_Record_Annot_Desc().getValue(pnd_I).setValue(ldaFcpl448.getPnd_Annot_Desc().getValue(pnd_I));                                       //Natural: ASSIGN ANNOT-DESC ( #I ) := #ANNOT-DESC ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getWorkFiles().write(15, false, ldaCpulannt.getAnnot1_Record());                                                                                                  //Natural: WRITE WORK FILE 15 ANNOT1-RECORD
        ldaCpulann2.getAnnot2_Record_Legacy_Eft_Key().setValue(ldaCpulpym2.getPayment_Record_Legacy_Eft_Key());                                                           //Natural: MOVE PAYMENT-RECORD.LEGACY-EFT-KEY TO ANNOT2-RECORD.LEGACY-EFT-KEY
        ldaCpulann2.getAnnot2_Record_Check_Number().setValue(ldaCpulpym2.getPayment_Record_Check_Number());                                                               //Natural: MOVE PAYMENT-RECORD.CHECK-NUMBER TO ANNOT2-RECORD.CHECK-NUMBER
        FOR04:                                                                                                                                                            //Natural: FOR #I 1 TO 99
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(99)); pnd_I.nadd(1))
        {
            if (condition(fcp_Cons_Annot1_Pymnt_Annot_Expand_Cmnt.getValue(pnd_I).equals(" ") && fcp_Cons_Annot1_Pymnt_Annot_Expand_Date.getValue(pnd_I).equals(" ")      //Natural: IF PYMNT-ANNOT-EXPAND-CMNT ( #I ) = ' ' AND PYMNT-ANNOT-EXPAND-DATE ( #I ) = ' ' AND PYMNT-ANNOT-EXPAND-TIME ( #I ) = ' ' AND PYMNT-ANNOT-EXPAND-UID ( #I ) = ' '
                && fcp_Cons_Annot1_Pymnt_Annot_Expand_Time.getValue(pnd_I).equals(" ") && fcp_Cons_Annot1_Pymnt_Annot_Expand_Uid.getValue(pnd_I).equals(" ")))
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            ldaCpulann2.getAnnot2_Record_Sequence().setValue(pnd_I);                                                                                                      //Natural: ASSIGN SEQUENCE := #I
            ldaCpulann2.getAnnot2_Record_Annot_Expand_Cmnt().setValue(fcp_Cons_Annot1_Pymnt_Annot_Expand_Cmnt.getValue(pnd_I));                                           //Natural: ASSIGN ANNOT-EXPAND-CMNT := PYMNT-ANNOT-EXPAND-CMNT ( #I )
            ldaCpulann2.getAnnot2_Record_Annot_Expand_Date().setValue(fcp_Cons_Annot1_Pymnt_Annot_Expand_Date.getValue(pnd_I));                                           //Natural: ASSIGN ANNOT-EXPAND-DATE := PYMNT-ANNOT-EXPAND-DATE ( #I )
            ldaCpulann2.getAnnot2_Record_Annot_Expand_Time().setValue(fcp_Cons_Annot1_Pymnt_Annot_Expand_Time.getValue(pnd_I));                                           //Natural: ASSIGN ANNOT-EXPAND-TIME := PYMNT-ANNOT-EXPAND-TIME ( #I )
            ldaCpulann2.getAnnot2_Record_Annot_Expand_Uid().setValue(fcp_Cons_Annot1_Pymnt_Annot_Expand_Uid.getValue(pnd_I));                                             //Natural: ASSIGN ANNOT-EXPAND-UID := PYMNT-ANNOT-EXPAND-UID ( #I )
            getWorkFiles().write(16, false, ldaCpulann2.getAnnot2_Record());                                                                                              //Natural: WRITE WORK FILE 16 ANNOT2-RECORD
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Adjust_Effective_Date() throws Exception                                                                                                             //Natural: ADJUST-EFFECTIVE-DATE
    {
        if (BLNatReinput.isReinput()) return;

        dte.setValueEdited(pymnt_Pymnt_Check_Dte,new ReportEditMask("MM-NNNNNNNNNN"));                                                                                    //Natural: MOVE EDITED PYMNT.PYMNT-CHECK-DTE ( EM = MM-N ( 10 ) ) TO DTE
        if (condition(dte_Mm.equals("01")))                                                                                                                               //Natural: IF MM = '01'
        {
            pnd_Eft_Eff_Date.setValue(pymnt_Pymnt_Check_Dte);                                                                                                             //Natural: ASSIGN #EFT-EFF-DATE := PYMNT.PYMNT-CHECK-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  TO PREVIOUS YEAR
            short decideConditionsMet2851 = 0;                                                                                                                            //Natural: DECIDE ON FIRST DAY;//Natural: VALUE 'Monday    ','Tuesday   ','Wednesday ','Thursday  ','Friday    '
            if (condition((dte_Day.equals("Monday    ") || dte_Day.equals("Tuesday   ") || dte_Day.equals("Wednesday ") || dte_Day.equals("Thursday  ") 
                || dte_Day.equals("Friday    "))))
            {
                decideConditionsMet2851++;
                pnd_Eft_Eff_Date.setValue(pymnt_Pymnt_Check_Dte);                                                                                                         //Natural: ASSIGN #EFT-EFF-DATE := PYMNT.PYMNT-CHECK-DTE
            }                                                                                                                                                             //Natural: VALUE 'Saturday  '
            else if (condition((dte_Day.equals("Saturday  "))))
            {
                decideConditionsMet2851++;
                pnd_Eft_Eff_Date.compute(new ComputeParameters(false, pnd_Eft_Eff_Date), pymnt_Pymnt_Check_Dte.subtract(1));                                              //Natural: ASSIGN #EFT-EFF-DATE := PYMNT.PYMNT-CHECK-DTE - 1
            }                                                                                                                                                             //Natural: VALUE 'Sunday    '
            else if (condition((dte_Day.equals("Sunday    "))))
            {
                decideConditionsMet2851++;
                pnd_Eft_Eff_Date.compute(new ComputeParameters(false, pnd_Eft_Eff_Date), pymnt_Pymnt_Check_Dte.subtract(2));                                              //Natural: ASSIGN #EFT-EFF-DATE := PYMNT.PYMNT-CHECK-DTE - 2
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  JWO15 START
    private void sub_Get_Iban() throws Exception                                                                                                                          //Natural: GET-IBAN
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        //*  RESET #MDMA210 /* PIN EXP
        //*  PIN EXP
        //*  PIN EXP
        pdaMdma211.getPnd_Mdma211().reset();                                                                                                                              //Natural: RESET #MDMA211
        pdaMdma211.getPnd_Mdma211_Pnd_I_Contract_Number().setValue(pymnt_Cntrct_Cmbn_Nbr);                                                                                //Natural: ASSIGN #MDMA211.#I-CONTRACT-NUMBER := PYMNT.CNTRCT-CMBN-NBR
        //*  PIN EXP
        if (condition(pymnt_Cntrct_Payee_Cde.equals(" ")))                                                                                                                //Natural: IF PYMNT.CNTRCT-PAYEE-CDE = ' '
        {
            pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue("01");                                                                                               //Natural: ASSIGN #MDMA211.#I-PAYEE-CODE-A2 := '01'
            //*  PIN EXP
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaMdma211.getPnd_Mdma211_Pnd_I_Payee_Code_A2().setValue(pymnt_Cntrct_Payee_Cde);                                                                             //Natural: ASSIGN #MDMA211.#I-PAYEE-CODE-A2 := PYMNT.CNTRCT-PAYEE-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALLNAT 'MDMN210A' #MDMA210 /* PIN EXP
        //*  PIN EXP
        DbsUtil.callnat(Mdmn211a.class , getCurrentProcessState(), pdaMdma211.getPnd_Mdma211());                                                                          //Natural: CALLNAT 'MDMN211A' #MDMA211
        if (condition(Global.isEscape())) return;
        //*  IF #MDMA210.#O-RETURN-CODE = '0000'  /* PIN EXP
        //*  PIN EXP
        if (condition(pdaMdma211.getPnd_Mdma211_Pnd_O_Return_Code().equals("0000")))                                                                                      //Natural: IF #MDMA211.#O-RETURN-CODE = '0000'
        {
            ldaCpulpym2.getPayment_Record_Intl_Aba_Eft().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Bank_Aba_Eft_Number());                                              //Natural: MOVE #O-CM-BANK-ABA-EFT-NUMBER TO INTL-ABA-EFT
            ldaCpulpym2.getPayment_Record_Intl_Iban().setValue(pdaMdma211.getPnd_Mdma211_Pnd_O_Cm_Bank_Account_Number());                                                 //Natural: MOVE #O-CM-BANK-ACCOUNT-NUMBER TO INTL-IBAN
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaCpulpym2.getPayment_Record_Intl_Aba_Eft().setValue(" ");                                                                                                   //Natural: MOVE ' ' TO INTL-ABA-EFT
            ldaCpulpym2.getPayment_Record_Intl_Iban().setValue(" ");                                                                                                      //Natural: MOVE ' ' TO INTL-IBAN
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-IBAN
    }
    private void sub_Open_Mq() throws Exception                                                                                                                           //Natural: OPEN-MQ
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************
        //*  OPEN MQ
        DbsUtil.invokeMain(DbsUtil.getBlType("MDMP0011"), getCurrentProcessState());                                                                                      //Natural: FETCH RETURN 'MDMP0011'
        if (condition(Global.isEscape())) return;
        if (condition(gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(1).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(2).equals("0")  //Natural: IF ##DATA-RESPONSE ( 1 ) = '0' AND ##DATA-RESPONSE ( 2 ) = '0' AND ##DATA-RESPONSE ( 3 ) = '0' AND ##DATA-RESPONSE ( 4 ) = '0'
            && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(3).equals("0") && gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(4).equals("0")))
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        FOR05:                                                                                                                                                            //Natural: FOR #I2 = 1 TO 60
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(60)); pnd_I2.nadd(1))
        {
            pnd_Rc.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Rc, gdaMdmg0001.getPnd_Pnd_Mdmg0001_Pnd_Pnd_Data_Response().getValue(pnd_I2)));           //Natural: COMPRESS #RC ##DATA-RESPONSE ( #I2 ) INTO #RC LEAVING NO SPACE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(0, "=",pnd_Rc);                                                                                                                                //Natural: WRITE '=' #RC
        if (Global.isEscape()) return;
        getReports().write(0, ReportOption.NOHDR,"****************************",NEWLINE,"MQ OPEN ERROR","RETURN CODE= ",NEWLINE,pnd_Rc,NEWLINE,"****************************", //Natural: WRITE NOHDR '****************************' / 'MQ OPEN ERROR' 'RETURN CODE= ' / #RC / '****************************' /
            NEWLINE);
        if (Global.isEscape()) return;
        DbsUtil.terminate(4);  if (true) return;                                                                                                                          //Natural: TERMINATE 4
    }

    //
}
