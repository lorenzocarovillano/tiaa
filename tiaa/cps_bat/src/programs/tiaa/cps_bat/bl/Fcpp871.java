/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:56 PM
**        * FROM NATURAL PROGRAM : Fcpp871
************************************************************
**        * FILE NAME            : Fcpp871.java
**        * CLASS NAME           : Fcpp871
**        * INSTANCE NAME        : Fcpp871
************************************************************
************************************************************************
* PROGRAM  : FCPP871
* SYSTEM   : CPS
* TITLE    : NEW ANNUITIZATION
* FUNCTION : FORMAT WARRANT EXTRACT.
* UPDATES  :
*
* 5/26/1998 - RIAD LOUTFI - ADDED RETIREMENT LOAN PROCESSING ("AL").
* 11/1/1999 - ROXAN C.    - ADDED "EW"  PROCESSING
* 12/07/99                - REPLACE FCPA371 WITH FCPAEXT
* 04/22/03                - STOW. EXPAND FCPAEXT
*  4/2017  - JJG - PIN EXPANSION RESTOW
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp871 extends BLNatBase
{
    // Data Areas
    private PdaFcpaldgr pdaFcpaldgr;
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl871 ldaFcpl871;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws_Nz;
    private DbsField pnd_Ws_Nz_Pnd_I_Cnt;
    private DbsField pnd_Ws_Nz_Pnd_O_Cnt;
    private DbsField pnd_Ws_Nz_Pnd_L_Rec_Cnt;
    private DbsField pnd_Ws_Nz_Pnd_L_Cnt;

    private DbsGroup pnd_Ws_Al;
    private DbsField pnd_Ws_Al_Pnd_I_Cnt;
    private DbsField pnd_Ws_Al_Pnd_O_Cnt;
    private DbsField pnd_Ws_Al_Pnd_L_Rec_Cnt;
    private DbsField pnd_Ws_Al_Pnd_L_Cnt;

    private DbsGroup pnd_Ws_Ew;
    private DbsField pnd_Ws_Ew_Pnd_I_Cnt;
    private DbsField pnd_Ws_Ew_Pnd_O_Cnt;
    private DbsField pnd_Ws_Ew_Pnd_L_Rec_Cnt;
    private DbsField pnd_Ws_Ew_Pnd_L_Cnt;
    private DbsField pnd_Idx;
    private DbsField pnd_Display;
    private DbsField pnd_Display2;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaldgr = new PdaFcpaldgr(localVariables);
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl871 = new LdaFcpl871();
        registerRecord(ldaFcpl871);

        // Local Variables

        pnd_Ws_Nz = localVariables.newGroupInRecord("pnd_Ws_Nz", "#WS-NZ");
        pnd_Ws_Nz_Pnd_I_Cnt = pnd_Ws_Nz.newFieldInGroup("pnd_Ws_Nz_Pnd_I_Cnt", "#I-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Nz_Pnd_O_Cnt = pnd_Ws_Nz.newFieldInGroup("pnd_Ws_Nz_Pnd_O_Cnt", "#O-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Nz_Pnd_L_Rec_Cnt = pnd_Ws_Nz.newFieldInGroup("pnd_Ws_Nz_Pnd_L_Rec_Cnt", "#L-REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Nz_Pnd_L_Cnt = pnd_Ws_Nz.newFieldInGroup("pnd_Ws_Nz_Pnd_L_Cnt", "#L-CNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Ws_Al = localVariables.newGroupInRecord("pnd_Ws_Al", "#WS-AL");
        pnd_Ws_Al_Pnd_I_Cnt = pnd_Ws_Al.newFieldInGroup("pnd_Ws_Al_Pnd_I_Cnt", "#I-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Al_Pnd_O_Cnt = pnd_Ws_Al.newFieldInGroup("pnd_Ws_Al_Pnd_O_Cnt", "#O-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Al_Pnd_L_Rec_Cnt = pnd_Ws_Al.newFieldInGroup("pnd_Ws_Al_Pnd_L_Rec_Cnt", "#L-REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Al_Pnd_L_Cnt = pnd_Ws_Al.newFieldInGroup("pnd_Ws_Al_Pnd_L_Cnt", "#L-CNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Ws_Ew = localVariables.newGroupInRecord("pnd_Ws_Ew", "#WS-EW");
        pnd_Ws_Ew_Pnd_I_Cnt = pnd_Ws_Ew.newFieldInGroup("pnd_Ws_Ew_Pnd_I_Cnt", "#I-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Ew_Pnd_O_Cnt = pnd_Ws_Ew.newFieldInGroup("pnd_Ws_Ew_Pnd_O_Cnt", "#O-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Ew_Pnd_L_Rec_Cnt = pnd_Ws_Ew.newFieldInGroup("pnd_Ws_Ew_Pnd_L_Rec_Cnt", "#L-REC-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Ew_Pnd_L_Cnt = pnd_Ws_Ew.newFieldInGroup("pnd_Ws_Ew_Pnd_L_Cnt", "#L-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Idx = localVariables.newFieldInRecord("pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 7);
        pnd_Display = localVariables.newFieldInRecord("pnd_Display", "#DISPLAY", FieldType.STRING, 30);
        pnd_Display2 = localVariables.newFieldInRecord("pnd_Display2", "#DISPLAY2", FieldType.STRING, 30);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl871.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp871() throws Exception
    {
        super("Fcpp871");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 03 ) PS = 60 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pdaFcpaldgr.getLedger_Work_Fields_Ledger_Abend_Ind().setValue(true);                                                                                              //Natural: ASSIGN LEDGER-ABEND-IND := TRUE
        //*  READ  WORK FILE 01  PYMNT-ADDR-INFO  INV-INFO (*)         /*  ROXAN
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 EXT ( * )
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            short decideConditionsMet525 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF EXT.CNTRCT-ORGN-CDE;//Natural: VALUE 'NZ'
            if (condition((pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("NZ"))))
            {
                decideConditionsMet525++;
                pnd_Ws_Nz_Pnd_I_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-NZ.#I-CNT
                pnd_Ws_Nz_Pnd_O_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-NZ.#O-CNT
                getWorkFiles().write(2, true, pdaFcpaext.getExt().getValue("*"));                                                                                         //Natural: WRITE WORK FILE 02 VARIABLE EXT ( * )
                //*     WRITE  WORK FILE 02  VARIABLE  PYMNT-ADDR-INFO
                //*                                    INV-INFO (1:C-INV-ACCT) /*
                                                                                                                                                                          //Natural: PERFORM GET-LEDGER-INFO
                sub_Get_Ledger_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'AL'
            else if (condition((pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("AL"))))
            {
                decideConditionsMet525++;
                pnd_Ws_Al_Pnd_I_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-AL.#I-CNT
                pnd_Ws_Al_Pnd_O_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-AL.#O-CNT
                getWorkFiles().write(3, true, pdaFcpaext.getExt().getValue("*"));                                                                                         //Natural: WRITE WORK FILE 03 VARIABLE EXT ( * )
                                                                                                                                                                          //Natural: PERFORM GET-LEDGER-INFO
                sub_Get_Ledger_Info();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: VALUE 'EW'
            else if (condition((pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW"))))
            {
                decideConditionsMet525++;
                pnd_Ws_Ew_Pnd_I_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-EW.#I-CNT
                pnd_Ws_Ew_Pnd_O_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-EW.#O-CNT
                getWorkFiles().write(4, true, pdaFcpaext.getExt().getValue("*"));                                                                                         //Natural: WRITE WORK FILE 04 VARIABLE EXT ( * )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(14),"COUNTERS AT THE END OF PROGRAM:",NEWLINE,new TabSetting(17),"RECORDS READ..................:",pnd_Ws_Nz_Pnd_I_Cnt,      //Natural: WRITE ( 01 ) 14T 'COUNTERS AT THE END OF PROGRAM:' / 17T 'RECORDS READ..................:' #WS-NZ.#I-CNT / 17T 'RECORDS WRITTEN...............:' #WS-NZ.#O-CNT / / 17T 'LEDGER RECORDS READ...........:' #WS-NZ.#L-REC-CNT / 17T 'LEDGER RECORDS WRITTEN........:' #WS-NZ.#L-CNT
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"RECORDS WRITTEN...............:",pnd_Ws_Nz_Pnd_O_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(17),"LEDGER RECORDS READ...........:",pnd_Ws_Nz_Pnd_L_Rec_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"LEDGER RECORDS WRITTEN........:",pnd_Ws_Nz_Pnd_L_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(14),"COUNTERS AT THE END OF PROGRAM:",NEWLINE,new TabSetting(17),"RECORDS READ..................:",pnd_Ws_Al_Pnd_I_Cnt,      //Natural: WRITE ( 02 ) 14T 'COUNTERS AT THE END OF PROGRAM:' / 17T 'RECORDS READ..................:' #WS-AL.#I-CNT / 17T 'RECORDS WRITTEN...............:' #WS-AL.#O-CNT / / 17T 'LEDGER RECORDS READ...........:' #WS-AL.#L-REC-CNT / 17T 'LEDGER RECORDS WRITTEN........:' #WS-AL.#L-CNT
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"RECORDS WRITTEN...............:",pnd_Ws_Al_Pnd_O_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(17),"LEDGER RECORDS READ...........:",pnd_Ws_Al_Pnd_L_Rec_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"LEDGER RECORDS WRITTEN........:",pnd_Ws_Al_Pnd_L_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().write(3, new TabSetting(14),"COUNTERS AT THE END OF PROGRAM:",NEWLINE,new TabSetting(17),"RECORDS READ..................:",pnd_Ws_Ew_Pnd_I_Cnt,      //Natural: WRITE ( 03 ) 14T 'COUNTERS AT THE END OF PROGRAM:' / 17T 'RECORDS READ..................:' #WS-EW.#I-CNT / 17T 'RECORDS WRITTEN...............:' #WS-EW.#O-CNT / / 17T 'LEDGER RECORDS READ...........:' #WS-EW.#L-REC-CNT / 17T 'LEDGER RECORDS WRITTEN........:' #WS-EW.#L-CNT
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"RECORDS WRITTEN...............:",pnd_Ws_Ew_Pnd_O_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(17),"LEDGER RECORDS READ...........:",pnd_Ws_Ew_Pnd_L_Rec_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"LEDGER RECORDS WRITTEN........:",pnd_Ws_Ew_Pnd_L_Cnt, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-LEDGER-INFO
        //*  MOVE  BY NAME  PYMNT-ADDR-INFO  TO  LEDGER
        //* *#GL-RECORD (*)           :=  EXT (1:2)
        //* *------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T '"NZ" WARRANT EXTRACT' 120T 'REPORT: RPT1' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T '"AL" WARRANT EXTRACT' 120T 'REPORT: RPT1' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 03 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 03 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T '"EW" WARRANT EXTRACT' 120T 'REPORT: RPT1' //
    }
    private void sub_Get_Ledger_Info() throws Exception                                                                                                                   //Natural: GET-LEDGER-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        ldaFcpl871.getPnd_Gl_Rec().reset();                                                                                                                               //Natural: RESET #GL-REC
        pdaFcpaldgr.getLedger_Cntl_Check_Dte().setValue(pdaFcpaext.getExt_Pymnt_Check_Dte());                                                                             //Natural: ASSIGN LEDGER.CNTL-CHECK-DTE := EXT.PYMNT-CHECK-DTE
        pdaFcpaldgr.getLedger_Cntl_Check_Dte().setValue(pdaFcpaext.getExt_Pymnt_Check_Dte());                                                                             //Natural: ASSIGN LEDGER.CNTL-CHECK-DTE := EXT.PYMNT-CHECK-DTE
        pdaFcpaldgr.getLedger_Cntrct_Check_Crrncy_Cde().setValue(pdaFcpaext.getExt_Cntrct_Check_Crrncy_Cde());                                                            //Natural: ASSIGN LEDGER.CNTRCT-CHECK-CRRNCY-CDE := EXT.CNTRCT-CHECK-CRRNCY-CDE
        pdaFcpaldgr.getLedger_Cntrct_Ppcn_Nbr().setValue(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr());                                                                            //Natural: ASSIGN LEDGER.CNTRCT-PPCN-NBR := EXT.CNTRCT-PPCN-NBR
        pdaFcpaldgr.getLedger_Cntrct_Orgn_Cde().setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                            //Natural: ASSIGN LEDGER.CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
        pdaFcpaldgr.getLedger_Pymnt_Settlmnt_Dte().setValue(pdaFcpaext.getExt_Pymnt_Settlmnt_Dte());                                                                      //Natural: ASSIGN LEDGER.PYMNT-SETTLMNT-DTE := EXT.PYMNT-SETTLMNT-DTE
        pdaFcpaldgr.getLedger_Pymnt_Acctg_Dte().setValue(pdaFcpaext.getExt_Pymnt_Acctg_Dte());                                                                            //Natural: ASSIGN LEDGER.PYMNT-ACCTG-DTE := EXT.PYMNT-ACCTG-DTE
        pdaFcpaldgr.getLedger_Pymnt_Intrfce_Dte().setValue(pdaFcpaext.getExt_Pymnt_Intrfce_Dte());                                                                        //Natural: ASSIGN LEDGER.PYMNT-INTRFCE-DTE := EXT.PYMNT-INTRFCE-DTE
        pdaFcpaldgr.getLedger_Pymnt_Prcss_Seq_Nbr().setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                                    //Natural: ASSIGN LEDGER.PYMNT-PRCSS-SEQ-NBR := EXT.PYMNT-PRCSS-SEQ-NBR
        pdaFcpaldgr.getLedger_Inv_Acct_Cde().getValue(1,":",40).setValue(pdaFcpaext.getExt_Inv_Acct_Cde().getValue(1,":",40));                                            //Natural: ASSIGN LEDGER.INV-ACCT-CDE ( 1:40 ) := EXT.INV-ACCT-CDE ( 1:40 )
        pdaFcpaldgr.getLedger_Pymnt_Prcss_Seq_Nbr().setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                                    //Natural: ASSIGN LEDGER.PYMNT-PRCSS-SEQ-NBR := EXT.PYMNT-PRCSS-SEQ-NBR
        pdaFcpaldgr.getLedger_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde());                                                //Natural: ASSIGN LEDGER.CNTRCT-CANCEL-RDRW-ACTVTY-CDE := EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        //*  ROXAN
        DbsUtil.callnat(Fcpnldgr.class , getCurrentProcessState(), pdaFcpaldgr.getLedger_Work_Fields(), pdaFcpaldgr.getLedger());                                         //Natural: CALLNAT 'FCPNLDGR' USING LEDGER-WORK-FIELDS LEDGER
        if (condition(Global.isEscape())) return;
        pdaFcpaext.getExt_Pymnt_Instmt_Nbr().setValue(99);                                                                                                                //Natural: ASSIGN EXT.PYMNT-INSTMT-NBR := 99
        ldaFcpl871.getPnd_Gl_Rec_Pnd_Gl_Rec_Ext().getValue(1,":",2).setValue(pdaFcpaext.getExt().getValue(1,":",2));                                                      //Natural: ASSIGN #GL-REC-EXT ( 1:2 ) := EXT ( 1:2 )
        if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("NZ")))                                                                                                  //Natural: IF EXT.CNTRCT-ORGN-CDE = 'NZ'
        {
            pnd_Ws_Nz_Pnd_L_Rec_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-NZ.#L-REC-CNT
            pnd_Ws_Nz_Pnd_L_Cnt.nadd(pdaFcpaldgr.getLedger_C_Inv_Ledgr());                                                                                                //Natural: ADD C-INV-LEDGR TO #WS-NZ.#L-CNT
            FOR01:                                                                                                                                                        //Natural: FOR #IDX = 1 TO C-INV-LEDGR
            for (pnd_Idx.setValue(1); condition(pnd_Idx.lessOrEqual(pdaFcpaldgr.getLedger_C_Inv_Ledgr())); pnd_Idx.nadd(1))
            {
                ldaFcpl871.getPnd_Gl_Rec_Pnd_Gl_Detail().setValuesByName(pdaFcpaldgr.getLedger_Inv_Ledgr().getValue(pnd_Idx));                                            //Natural: MOVE BY NAME LEDGER.INV-LEDGR ( #IDX ) TO #GL-DETAIL
                getWorkFiles().write(2, true, ldaFcpl871.getPnd_Gl_Rec());                                                                                                //Natural: WRITE WORK FILE 02 VARIABLE #GL-REC
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl().notEquals(getZero())))                                                                                //Natural: IF C-INV-LEDGR-OVRFL NE 0
            {
                pnd_Ws_Nz_Pnd_L_Cnt.nadd(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl());                                                                                      //Natural: ADD C-INV-LEDGR-OVRFL TO #WS-NZ.#L-CNT
                FOR02:                                                                                                                                                    //Natural: FOR #IDX = 1 TO C-INV-LEDGR-OVRFL
                for (pnd_Idx.setValue(1); condition(pnd_Idx.lessOrEqual(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl())); pnd_Idx.nadd(1))
                {
                    ldaFcpl871.getPnd_Gl_Rec_Pnd_Gl_Detail().setValuesByName(pdaFcpaldgr.getLedger_Inv_Ledgr_Ovrfl().getValue(pnd_Idx));                                  //Natural: MOVE BY POSITION LEDGER.INV-LEDGR-OVRFL ( #IDX ) TO #GL-DETAIL
                    getWorkFiles().write(2, true, ldaFcpl871.getPnd_Gl_Rec());                                                                                            //Natural: WRITE WORK FILE 02 VARIABLE #GL-REC
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("AL")))                                                                                                  //Natural: IF EXT.CNTRCT-ORGN-CDE = 'AL'
        {
            pnd_Ws_Al_Pnd_L_Rec_Cnt.nadd(1);                                                                                                                              //Natural: ADD 1 TO #WS-AL.#L-REC-CNT
            pnd_Ws_Al_Pnd_L_Cnt.nadd(pdaFcpaldgr.getLedger_C_Inv_Ledgr());                                                                                                //Natural: ADD C-INV-LEDGR TO #WS-AL.#L-CNT
            FOR03:                                                                                                                                                        //Natural: FOR #IDX = 1 TO C-INV-LEDGR
            for (pnd_Idx.setValue(1); condition(pnd_Idx.lessOrEqual(pdaFcpaldgr.getLedger_C_Inv_Ledgr())); pnd_Idx.nadd(1))
            {
                ldaFcpl871.getPnd_Gl_Rec_Pnd_Gl_Detail().setValuesByName(pdaFcpaldgr.getLedger_Inv_Ledgr().getValue(pnd_Idx));                                            //Natural: MOVE BY NAME LEDGER.INV-LEDGR ( #IDX ) TO #GL-DETAIL
                getWorkFiles().write(3, true, ldaFcpl871.getPnd_Gl_Rec());                                                                                                //Natural: WRITE WORK FILE 03 VARIABLE #GL-REC
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            if (condition(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl().notEquals(getZero())))                                                                                //Natural: IF C-INV-LEDGR-OVRFL NE 0
            {
                pnd_Ws_Al_Pnd_L_Cnt.nadd(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl());                                                                                      //Natural: ADD C-INV-LEDGR-OVRFL TO #WS-AL.#L-CNT
                FOR04:                                                                                                                                                    //Natural: FOR #IDX = 1 TO C-INV-LEDGR-OVRFL
                for (pnd_Idx.setValue(1); condition(pnd_Idx.lessOrEqual(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl())); pnd_Idx.nadd(1))
                {
                    ldaFcpl871.getPnd_Gl_Rec_Pnd_Gl_Detail().setValuesByName(pdaFcpaldgr.getLedger_Inv_Ledgr_Ovrfl().getValue(pnd_Idx));                                  //Natural: MOVE BY POSITION LEDGER.INV-LEDGR-OVRFL ( #IDX ) TO #GL-DETAIL
                    getWorkFiles().write(3, true, ldaFcpl871.getPnd_Gl_Rec());                                                                                            //Natural: WRITE WORK FILE 03 VARIABLE #GL-REC
                }                                                                                                                                                         //Natural: END-FOR
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");
        Global.format(3, "PS=60 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"'NZ' WARRANT EXTRACT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"'AL' WARRANT EXTRACT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(3), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"'EW' WARRANT EXTRACT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
}
