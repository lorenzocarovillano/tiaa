/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:49 PM
**        * FROM NATURAL PROGRAM : Fcpp362
************************************************************
**        * FILE NAME            : Fcpp362.java
**        * CLASS NAME           : Fcpp362
**        * INSTANCE NAME        : Fcpp362
************************************************************
***********************************************************************
** PROGRAM:     FCPP362                                              **
** SYSTEM:      CPS - CONSOLIDATED PAYMENT SYSTEM                    **
** DATE:        11-11-1999                                           **
** AUTHOR:      ALTHEA A. YOUNG                                      **
** DESCRIPTION: THIS PROGRAM PRODUCES VARIOUS ELECTRONIC WARRANTS    **
**              REGISTERS.                                           **
**                                                                   **
***********************************************************************
** HISTORY:                                                          **
** 02/14/00   - ROXAN CARREON                                        **
**              CORRECTED PROGRAM TO INCLUDE ALL PAYMENT RECORD IN   **
**              THE REPORT. REMOVE CALL TO SUBPROGRAM FCPN199A.      **
**              CORRECTED EXTRACT PROGRAM SO THAT SUB-TOTALS AND     **
**              SUMMARY TOTALS HAVE THE SAME FORMAT AS THE EOD       **
**              REGISTER.                                            **
** 04/22/03   - STOW. FCPAEXT WAS EXPANDED                           **
** 06/7/2006 : LANDRUM PAYEE MATCH.                                 **
**           - INCREASED CHECK NBR FROM N7 TO N10. FORMAT FOR REPORTS**
** 4/2017 : JJG - PIN EXPANSION RESTOW                               **
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp362 extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl362 ldaFcpl362;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Report_Cycle;
    private DbsField pnd_Report_Title;
    private DbsField pnd_Trans_Type_Category;
    private DbsField pnd_Summary_Category;
    private DbsField pnd_Trans;
    private DbsField pnd_Sum_Trans;
    private DbsField pnd_Pymnt;
    private DbsField pnd_I;
    private DbsField pnd_Check_Date_A;
    private DbsField pnd_Center_Work;
    private DbsField pnd_Center_Length;
    private DbsField pnd_Prev_Seq_Number;

    private DbsGroup pnd_Prev_Seq_Number__R_Field_1;
    private DbsField pnd_Prev_Seq_Number_Pnd_Prev_Seq_Nbr;
    private DbsField pnd_Prev_Seq_Number_Pnd_Prev_Instmt_Nbr;
    private DbsField pnd_Curr_Seq_Number;

    private DbsGroup pnd_Curr_Seq_Number__R_Field_2;
    private DbsField pnd_Curr_Seq_Number_Pnd_Curr_Seq_Nbr;
    private DbsField pnd_Curr_Seq_Number_Pnd_Curr_Instmt_Nbr;

    private DbsGroup pnd_Detail;
    private DbsField pnd_Detail_Pnd_Pymnt_Number;

    private DbsGroup pnd_Detail__R_Field_3;
    private DbsField pnd_Detail_Pnd_Pymnt_Nbr_N3;
    private DbsField pnd_Detail_Pnd_Pymnt_Nbr_N7;
    private DbsField pnd_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Detail_Pymnt_Check_Dte;
    private DbsField pnd_Detail_Annt_Rsdncy_Cde;
    private DbsField pnd_Detail_Pnd_Det_Gross_Amt;
    private DbsField pnd_Detail_Pnd_Det_Tax_Amt;
    private DbsField pnd_Detail_Pnd_Det_Ded_Amt;
    private DbsField pnd_Detail_Pnd_Det_Int_Amt;
    private DbsField pnd_Detail_Pnd_Det_Net_Amt;
    private DbsField pnd_Detail_Pnd_Pymnt_Nme;
    private DbsField pnd_Check_Datex;

    private DbsGroup pnd_Check_Datex__R_Field_4;
    private DbsField pnd_Check_Datex_Pnd_Check_Date;
    private DbsField pnd_Contractual_Amt;
    private DbsField pnd_Dvdnd_Cref_Amt;
    private DbsField pnd_Cref_Gross_Amt;

    private DbsGroup pnd_Dte_Sub_Totals;
    private DbsField pnd_Dte_Sub_Totals_Pnd_Gross_Amt;
    private DbsField pnd_Dte_Sub_Totals_Pnd_Tax_Amt;
    private DbsField pnd_Dte_Sub_Totals_Pnd_Deduction_Amt;
    private DbsField pnd_Dte_Sub_Totals_Pnd_Interest_Amt;
    private DbsField pnd_Dte_Sub_Totals_Pnd_Net_Amt;
    private DbsField pnd_Dte_Sub_Totals_Pnd_Qty;

    private DbsGroup pnd_Summary_Totals;
    private DbsField pnd_Summary_Totals_Pnd_Trans_Category;
    private DbsField pnd_Summary_Totals_Pnd_Contract_Amt;
    private DbsField pnd_Summary_Totals_Pnd_Dividend_Amt;
    private DbsField pnd_Summary_Totals_Pnd_Total_Tax_Amt;
    private DbsField pnd_Summary_Totals_Pnd_Total_Int_Amt;
    private DbsField pnd_Summary_Totals_Pnd_Total_Net_Amt;
    private DbsField pnd_Summary_Totals_Pnd_Quantity;

    private DbsGroup pnd_Logical_Vars;
    private DbsField pnd_Logical_Vars_Pnd_Month_End;
    private DbsField pnd_Logical_Vars_Pnd_First;
    private DbsField pnd_Logical_Vars_Pnd_Detail_Recs;
    private DbsField pnd_Logical_Vars_Pnd_Date_Break;
    private DbsField pnd_Logical_Vars_Pnd_Trans_Type_Break;
    private DbsField pnd_Logical_Vars_Pnd_Totals_Printed;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl362 = new LdaFcpl362();
        registerRecord(ldaFcpl362);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Report_Cycle = localVariables.newFieldInRecord("pnd_Report_Cycle", "#REPORT-CYCLE", FieldType.STRING, 8);
        pnd_Report_Title = localVariables.newFieldInRecord("pnd_Report_Title", "#REPORT-TITLE", FieldType.STRING, 80);
        pnd_Trans_Type_Category = localVariables.newFieldInRecord("pnd_Trans_Type_Category", "#TRANS-TYPE-CATEGORY", FieldType.STRING, 80);
        pnd_Summary_Category = localVariables.newFieldInRecord("pnd_Summary_Category", "#SUMMARY-CATEGORY", FieldType.STRING, 42);
        pnd_Trans = localVariables.newFieldInRecord("pnd_Trans", "#TRANS", FieldType.NUMERIC, 2);
        pnd_Sum_Trans = localVariables.newFieldInRecord("pnd_Sum_Trans", "#SUM-TRANS", FieldType.NUMERIC, 2);
        pnd_Pymnt = localVariables.newFieldInRecord("pnd_Pymnt", "#PYMNT", FieldType.NUMERIC, 2);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 2);
        pnd_Check_Date_A = localVariables.newFieldInRecord("pnd_Check_Date_A", "#CHECK-DATE-A", FieldType.STRING, 18);
        pnd_Center_Work = localVariables.newFieldInRecord("pnd_Center_Work", "#CENTER-WORK", FieldType.STRING, 80);
        pnd_Center_Length = localVariables.newFieldInRecord("pnd_Center_Length", "#CENTER-LENGTH", FieldType.NUMERIC, 2);
        pnd_Prev_Seq_Number = localVariables.newFieldInRecord("pnd_Prev_Seq_Number", "#PREV-SEQ-NUMBER", FieldType.NUMERIC, 9);

        pnd_Prev_Seq_Number__R_Field_1 = localVariables.newGroupInRecord("pnd_Prev_Seq_Number__R_Field_1", "REDEFINE", pnd_Prev_Seq_Number);
        pnd_Prev_Seq_Number_Pnd_Prev_Seq_Nbr = pnd_Prev_Seq_Number__R_Field_1.newFieldInGroup("pnd_Prev_Seq_Number_Pnd_Prev_Seq_Nbr", "#PREV-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Prev_Seq_Number_Pnd_Prev_Instmt_Nbr = pnd_Prev_Seq_Number__R_Field_1.newFieldInGroup("pnd_Prev_Seq_Number_Pnd_Prev_Instmt_Nbr", "#PREV-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Curr_Seq_Number = localVariables.newFieldInRecord("pnd_Curr_Seq_Number", "#CURR-SEQ-NUMBER", FieldType.NUMERIC, 9);

        pnd_Curr_Seq_Number__R_Field_2 = localVariables.newGroupInRecord("pnd_Curr_Seq_Number__R_Field_2", "REDEFINE", pnd_Curr_Seq_Number);
        pnd_Curr_Seq_Number_Pnd_Curr_Seq_Nbr = pnd_Curr_Seq_Number__R_Field_2.newFieldInGroup("pnd_Curr_Seq_Number_Pnd_Curr_Seq_Nbr", "#CURR-SEQ-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Curr_Seq_Number_Pnd_Curr_Instmt_Nbr = pnd_Curr_Seq_Number__R_Field_2.newFieldInGroup("pnd_Curr_Seq_Number_Pnd_Curr_Instmt_Nbr", "#CURR-INSTMT-NBR", 
            FieldType.NUMERIC, 2);

        pnd_Detail = localVariables.newGroupInRecord("pnd_Detail", "#DETAIL");
        pnd_Detail_Pnd_Pymnt_Number = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Pymnt_Number", "#PYMNT-NUMBER", FieldType.STRING, 11);

        pnd_Detail__R_Field_3 = pnd_Detail.newGroupInGroup("pnd_Detail__R_Field_3", "REDEFINE", pnd_Detail_Pnd_Pymnt_Number);
        pnd_Detail_Pnd_Pymnt_Nbr_N3 = pnd_Detail__R_Field_3.newFieldInGroup("pnd_Detail_Pnd_Pymnt_Nbr_N3", "#PYMNT-NBR-N3", FieldType.NUMERIC, 3);
        pnd_Detail_Pnd_Pymnt_Nbr_N7 = pnd_Detail__R_Field_3.newFieldInGroup("pnd_Detail_Pnd_Pymnt_Nbr_N7", "#PYMNT-NBR-N7", FieldType.NUMERIC, 7);
        pnd_Detail_Cntrct_Ppcn_Nbr = pnd_Detail.newFieldInGroup("pnd_Detail_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Detail_Pymnt_Check_Dte = pnd_Detail.newFieldInGroup("pnd_Detail_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Detail_Annt_Rsdncy_Cde = pnd_Detail.newFieldInGroup("pnd_Detail_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", FieldType.STRING, 2);
        pnd_Detail_Pnd_Det_Gross_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Det_Gross_Amt", "#DET-GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Detail_Pnd_Det_Tax_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Det_Tax_Amt", "#DET-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Detail_Pnd_Det_Ded_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Det_Ded_Amt", "#DET-DED-AMT", FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Detail_Pnd_Det_Int_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Det_Int_Amt", "#DET-INT-AMT", FieldType.PACKED_DECIMAL, 8, 2);
        pnd_Detail_Pnd_Det_Net_Amt = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Det_Net_Amt", "#DET-NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Detail_Pnd_Pymnt_Nme = pnd_Detail.newFieldInGroup("pnd_Detail_Pnd_Pymnt_Nme", "#PYMNT-NME", FieldType.STRING, 38);
        pnd_Check_Datex = localVariables.newFieldInRecord("pnd_Check_Datex", "#CHECK-DATEX", FieldType.STRING, 8);

        pnd_Check_Datex__R_Field_4 = localVariables.newGroupInRecord("pnd_Check_Datex__R_Field_4", "REDEFINE", pnd_Check_Datex);
        pnd_Check_Datex_Pnd_Check_Date = pnd_Check_Datex__R_Field_4.newFieldInGroup("pnd_Check_Datex_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        pnd_Contractual_Amt = localVariables.newFieldInRecord("pnd_Contractual_Amt", "#CONTRACTUAL-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Dvdnd_Cref_Amt = localVariables.newFieldInRecord("pnd_Dvdnd_Cref_Amt", "#DVDND-CREF-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Cref_Gross_Amt = localVariables.newFieldInRecord("pnd_Cref_Gross_Amt", "#CREF-GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Dte_Sub_Totals = localVariables.newGroupInRecord("pnd_Dte_Sub_Totals", "#DTE-SUB-TOTALS");
        pnd_Dte_Sub_Totals_Pnd_Gross_Amt = pnd_Dte_Sub_Totals.newFieldInGroup("pnd_Dte_Sub_Totals_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            10, 2);
        pnd_Dte_Sub_Totals_Pnd_Tax_Amt = pnd_Dte_Sub_Totals.newFieldInGroup("pnd_Dte_Sub_Totals_Pnd_Tax_Amt", "#TAX-AMT", FieldType.PACKED_DECIMAL, 9, 
            2);
        pnd_Dte_Sub_Totals_Pnd_Deduction_Amt = pnd_Dte_Sub_Totals.newFieldInGroup("pnd_Dte_Sub_Totals_Pnd_Deduction_Amt", "#DEDUCTION-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Dte_Sub_Totals_Pnd_Interest_Amt = pnd_Dte_Sub_Totals.newFieldInGroup("pnd_Dte_Sub_Totals_Pnd_Interest_Amt", "#INTEREST-AMT", FieldType.PACKED_DECIMAL, 
            8, 2);
        pnd_Dte_Sub_Totals_Pnd_Net_Amt = pnd_Dte_Sub_Totals.newFieldInGroup("pnd_Dte_Sub_Totals_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Dte_Sub_Totals_Pnd_Qty = pnd_Dte_Sub_Totals.newFieldInGroup("pnd_Dte_Sub_Totals_Pnd_Qty", "#QTY", FieldType.NUMERIC, 6);

        pnd_Summary_Totals = localVariables.newGroupArrayInRecord("pnd_Summary_Totals", "#SUMMARY-TOTALS", new DbsArrayController(1, 13));
        pnd_Summary_Totals_Pnd_Trans_Category = pnd_Summary_Totals.newFieldInGroup("pnd_Summary_Totals_Pnd_Trans_Category", "#TRANS-CATEGORY", FieldType.STRING, 
            22);
        pnd_Summary_Totals_Pnd_Contract_Amt = pnd_Summary_Totals.newFieldArrayInGroup("pnd_Summary_Totals_Pnd_Contract_Amt", "#CONTRACT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 4));
        pnd_Summary_Totals_Pnd_Dividend_Amt = pnd_Summary_Totals.newFieldArrayInGroup("pnd_Summary_Totals_Pnd_Dividend_Amt", "#DIVIDEND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 4));
        pnd_Summary_Totals_Pnd_Total_Tax_Amt = pnd_Summary_Totals.newFieldArrayInGroup("pnd_Summary_Totals_Pnd_Total_Tax_Amt", "#TOTAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 4));
        pnd_Summary_Totals_Pnd_Total_Int_Amt = pnd_Summary_Totals.newFieldArrayInGroup("pnd_Summary_Totals_Pnd_Total_Int_Amt", "#TOTAL-INT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 4));
        pnd_Summary_Totals_Pnd_Total_Net_Amt = pnd_Summary_Totals.newFieldArrayInGroup("pnd_Summary_Totals_Pnd_Total_Net_Amt", "#TOTAL-NET-AMT", FieldType.PACKED_DECIMAL, 
            11, 2, new DbsArrayController(1, 4));
        pnd_Summary_Totals_Pnd_Quantity = pnd_Summary_Totals.newFieldArrayInGroup("pnd_Summary_Totals_Pnd_Quantity", "#QUANTITY", FieldType.NUMERIC, 6, 
            new DbsArrayController(1, 4));

        pnd_Logical_Vars = localVariables.newGroupInRecord("pnd_Logical_Vars", "#LOGICAL-VARS");
        pnd_Logical_Vars_Pnd_Month_End = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Month_End", "#MONTH-END", FieldType.BOOLEAN, 1);
        pnd_Logical_Vars_Pnd_First = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Logical_Vars_Pnd_Detail_Recs = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Detail_Recs", "#DETAIL-RECS", FieldType.BOOLEAN, 1);
        pnd_Logical_Vars_Pnd_Date_Break = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Date_Break", "#DATE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Logical_Vars_Pnd_Trans_Type_Break = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Trans_Type_Break", "#TRANS-TYPE-BREAK", FieldType.BOOLEAN, 
            1);
        pnd_Logical_Vars_Pnd_Totals_Printed = pnd_Logical_Vars.newFieldInGroup("pnd_Logical_Vars_Pnd_Totals_Printed", "#TOTALS-PRINTED", FieldType.BOOLEAN, 
            1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl362.initializeValues();

        localVariables.reset();
        pnd_Report_Title.setInitialValue("PAYMENT REGISTER");
        pnd_Logical_Vars_Pnd_Month_End.setInitialValue(false);
        pnd_Logical_Vars_Pnd_First.setInitialValue(true);
        pnd_Logical_Vars_Pnd_Detail_Recs.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Date_Break.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Trans_Type_Break.setInitialValue(false);
        pnd_Logical_Vars_Pnd_Totals_Printed.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp362() throws Exception
    {
        super("Fcpp362");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp362|Main");
        OnErrorManager.pushEvent("FCPP362", onError);
        setupReports();
        while(true)
        {
            try
            {
                //*                                                                                                                                                       //Natural: FORMAT LS = 133 PS = 66;//Natural: FORMAT ( 01 ) LS = 133 PS = 66;//Natural: FORMAT ( 02 ) LS = 133 PS = 66
                //*                                                                                                                                                       //Natural: ON ERROR
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT *INIT-USER 1X '-' 1X *PROGRAM / *DATX ( EM = LLL' 'DD', 'YYYY ) 54T 'ELECTRONIC WARRANTS PAYMENTS' 119T 'PAGE:' 1X *PAGE-NUMBER ( 01 ) ( AD = L ) / *TIMX ( EM = HH:II' 'AP ) 27T #REPORT-TITLE / 27T #TRANS-TYPE-CATEGORY //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 02 ) TITLE LEFT *INIT-USER 1X '-' 1X *PROGRAM / *DATX ( EM = LLL' 'DD', 'YYYY ) 54T 'ELECTRONIC WARRANTS PAYMENTS' 119T 'PAGE:' 1X *PAGE-NUMBER ( 02 ) ( AD = L ) / *TIMX ( EM = HH:II' 'AP ) 60T 'Summary Totals' //
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=MITL'_'"), new SignPosition (true), new InputPrompting(false),                //Natural: INPUT ( AD = MITL'_' SG = ON IP = OFF ZP = OFF ) 'REPORT CYCLE:' #REPORT-CYCLE
                    new ReportZeroPrint (false),"REPORT CYCLE:",pnd_Report_Cycle);
                if (condition(pnd_Report_Cycle.equals("MONTHEND")))                                                                                                       //Natural: IF #REPORT-CYCLE = 'MONTHEND'
                {
                    pnd_Logical_Vars_Pnd_Month_End.setValue(true);                                                                                                        //Natural: ASSIGN #MONTH-END := TRUE
                }                                                                                                                                                         //Natural: END-IF
                //*  RL PAYEE MATCH JUNE 7 2006
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
                sub_Get_Check_Formatting_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  MAIN PROGRAM LOGIC
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK 1 EXT ( * )
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
                {
                    CheckAtStartofData707();

                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*                                                                                                                                                   //Natural: AT START OF DATA
                    //*                                                                                                                                                   //Natural: AT BREAK OF EXT.PYMNT-ACCTG-DTE
                    //*                                                                                                                                                   //Natural: AT BREAK OF EXT.PYMNT-CHECK-DTE
                    //*                                                                                                                                                   //Natural: AT BREAK OF EXT.PYMNT-WRRNT-TRANS-TYPE
                    if (condition(pnd_Logical_Vars_Pnd_Date_Break.getBoolean() || pnd_Logical_Vars_Pnd_Trans_Type_Break.getBoolean()))                                    //Natural: IF #DATE-BREAK OR #TRANS-TYPE-BREAK
                    {
                        getReports().newPage(new ReportSpecification(1));                                                                                                 //Natural: NEWPAGE ( 01 )
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        pnd_Logical_Vars_Pnd_Date_Break.reset();                                                                                                          //Natural: RESET #DATE-BREAK #TRANS-TYPE-BREAK
                        pnd_Logical_Vars_Pnd_Trans_Type_Break.reset();
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Curr_Seq_Number.setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                                                //Natural: ASSIGN #CURR-SEQ-NUMBER := EXT.PYMNT-PRCSS-SEQ-NBR
                    //*  ROXAN - PERFORM HERE 02/14/00
                                                                                                                                                                          //Natural: PERFORM POPULATE-DETAIL
                    sub_Populate_Detail();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-DETAIL
                    sub_Print_Detail();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Prev_Seq_Number.setValue(pnd_Curr_Seq_Number);                                                                                                    //Natural: ASSIGN #PREV-SEQ-NUMBER := #CURR-SEQ-NUMBER
                    //*                                                                                                                                                   //Natural: AT END OF DATA
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (condition(getWorkFiles().getAtEndOfData()))
                {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-TOTALS
                    sub_Print_Summary_Totals();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-ENDDATA
                if (Global.isEscape()) return;
                if (condition(! (pnd_Logical_Vars_Pnd_Detail_Recs.getBoolean())))                                                                                         //Natural: IF NOT #DETAIL-RECS
                {
                    pnd_Center_Work.setValue(pnd_Report_Title);                                                                                                           //Natural: MOVE #REPORT-TITLE TO #CENTER-WORK
                                                                                                                                                                          //Natural: PERFORM LINE-CENTERING
                    sub_Line_Centering();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    pnd_Report_Title.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Center_Work, pnd_Report_Title));                                        //Natural: COMPRESS #CENTER-WORK #REPORT-TITLE INTO #REPORT-TITLE LEAVING NO
                    DbsUtil.examine(new ExamineSource(pnd_Report_Title,true), new ExamineSearch("*", true), new ExamineReplace(" "));                                     //Natural: EXAMINE FULL #REPORT-TITLE FOR FULL '*' REPLACE ' '
                    getReports().skip(1, 10);                                                                                                                             //Natural: SKIP ( 01 ) 10
                    getReports().write(1, new ColumnSpacing(48),"NO PAYMENTS PROCESSED IN THIS CYCLE.");                                                                  //Natural: WRITE ( 01 ) 48X 'NO PAYMENTS PROCESSED IN THIS CYCLE.'
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //* *-------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: REPORT-TITLE-PROCESSING
                //* *----------**
                //* *-----------------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TRANSACTION-TYPE-PROCESSING
                //* *----------**
                //* *----------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: LINE-CENTERING
                //* *----------**
                //* *-----------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POPULATE-DETAIL
                //* *----------**
                //* *--------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DETAIL
                //* *----------**
                //* *---------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-DTE-SUB-TOTAL
                //* *----------**
                //* *----------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY-TOTALS
                //* *----------------------------------**
                //* *----------**
                //*  L E V E L   2   S U B - R O U T I N E S
                //* *----------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INITIALIZATION
                //* *----------**
                //* *----------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-DEDUCTIONS
                //* *----------------------------------**
                //* *----------**
                //* *------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATE-TOTALS
                //* *----------**
                //* *--------------------------------**
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SUMMARY-LINE
                //* *----------**
                //* *********************** RL BEGIN - PAYEE MATCH ************JUN 07,2006
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
                //*  RL PAYEE MATCH
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //*  RL PAYEE MATCH
                //* ************************** RL END-PAYEE MATCH *************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Report_Title_Processing() throws Exception                                                                                                           //Natural: REPORT-TITLE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------------**
        pnd_Report_Title.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #REPORT-TITLE
        short decideConditionsMet862 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #REPORT-CYCLE;//Natural: VALUE 'CYCLE'
        if (condition((pnd_Report_Cycle.equals("CYCLE"))))
        {
            decideConditionsMet862++;
            pnd_Report_Title.setValue(DbsUtil.compress("Cycle", pnd_Report_Title));                                                                                       //Natural: COMPRESS 'Cycle' #REPORT-TITLE INTO #REPORT-TITLE
        }                                                                                                                                                                 //Natural: VALUE 'LOGICAL'
        else if (condition((pnd_Report_Cycle.equals("LOGICAL"))))
        {
            decideConditionsMet862++;
            pnd_Report_Title.setValue(DbsUtil.compress("Logical End Of Day", pnd_Report_Title));                                                                          //Natural: COMPRESS 'Logical End Of Day' #REPORT-TITLE INTO #REPORT-TITLE
        }                                                                                                                                                                 //Natural: VALUE 'PHYSICAL'
        else if (condition((pnd_Report_Cycle.equals("PHYSICAL"))))
        {
            decideConditionsMet862++;
            pnd_Report_Title.setValue(DbsUtil.compress("Physical End Of Day", pnd_Report_Title));                                                                         //Natural: COMPRESS 'Physical End Of Day' #REPORT-TITLE INTO #REPORT-TITLE
        }                                                                                                                                                                 //Natural: VALUE 'MONTHEND'
        else if (condition((pnd_Report_Cycle.equals("MONTHEND"))))
        {
            decideConditionsMet862++;
            pnd_Report_Title.setValue(DbsUtil.compress("Month-End", pnd_Report_Title));                                                                                   //Natural: COMPRESS 'Month-End' #REPORT-TITLE INTO #REPORT-TITLE
        }                                                                                                                                                                 //Natural: ANY
        if (condition(decideConditionsMet862 > 0))
        {
            if (condition(! (pnd_Report_Cycle.equals("LOGICAL"))))                                                                                                        //Natural: IF NOT ( #REPORT-CYCLE = 'LOGICAL' )
            {
                //*  REMOVE TEST FOR MONTHLY
                //*           IF NOT #MONTH-END
                pnd_Check_Date_A.setValueEdited(pdaFcpaext.getExt_Pymnt_Check_Dte(),new ReportEditMask("LLLLLLLLL' 'DD', 'YYYY"));                                        //Natural: MOVE EDITED EXT.PYMNT-CHECK-DTE ( EM = LLLLLLLLL' 'DD', 'YYYY ) TO #CHECK-DATE-A
                //*           ELSE
                //*             MOVE EDITED EXT.PYMNT-CHECK-DTE (EM=LLLLLLLLL)
                //*               TO #CHECK-DATE-A
                //*           END-IF
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Check_Date_A.setValueEdited(pdaFcpaext.getExt_Pymnt_Acctg_Dte(),new ReportEditMask("LLLLLLLLL' 'DD', 'YYYY"));                                        //Natural: MOVE EDITED EXT.PYMNT-ACCTG-DTE ( EM = LLLLLLLLL' 'DD', 'YYYY ) TO #CHECK-DATE-A
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Check_Date_A.setValueEdited(Global.getDATX(),new ReportEditMask("LLLLLLLLL' 'DD', 'YYYY"));                                                               //Natural: MOVE EDITED *DATX ( EM = LLLLLLLLL' 'DD', 'YYYY ) TO #CHECK-DATE-A
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Report_Title.setValue(DbsUtil.compress(pnd_Report_Title, pnd_Check_Date_A));                                                                                  //Natural: COMPRESS #REPORT-TITLE #CHECK-DATE-A INTO #REPORT-TITLE
        pnd_Center_Work.setValue(pnd_Report_Title);                                                                                                                       //Natural: MOVE #REPORT-TITLE TO #CENTER-WORK
                                                                                                                                                                          //Natural: PERFORM LINE-CENTERING
        sub_Line_Centering();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Report_Title.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Center_Work, pnd_Report_Title));                                                    //Natural: COMPRESS #CENTER-WORK #REPORT-TITLE INTO #REPORT-TITLE LEAVING NO
        DbsUtil.examine(new ExamineSource(pnd_Report_Title,true), new ExamineSearch("*", true), new ExamineReplace(" "));                                                 //Natural: EXAMINE FULL #REPORT-TITLE FOR FULL '*' REPLACE ' '
        pnd_Center_Work.reset();                                                                                                                                          //Natural: RESET #CENTER-WORK
        //* *----------**
    }
    private void sub_Transaction_Type_Processing() throws Exception                                                                                                       //Natural: TRANSACTION-TYPE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------------**
        pnd_Trans.reset();                                                                                                                                                //Natural: RESET #TRANS #SUM-TRANS #TRANS-TYPE-CATEGORY
        pnd_Sum_Trans.reset();
        pnd_Trans_Type_Category.reset();
        DbsUtil.examine(new ExamineSource(ldaFcpl362.getEw_Trans_Type_Table_Pnd_Ew_Trans_Code().getValue("*"),true), new ExamineSearch(pdaFcpaext.getExt_Pymnt_Wrrnt_Trans_Type(),  //Natural: EXAMINE FULL EW-TRANS-TYPE-TABLE.#EW-TRANS-CODE ( * ) FOR FULL EXT.PYMNT-WRRNT-TRANS-TYPE GIVING INDEX #TRANS
            true), new ExamineGivingIndex(pnd_Trans));
        pnd_Trans_Type_Category.setValue(ldaFcpl362.getEw_Trans_Type_Table_Pnd_Ew_Trans_Desc_40().getValue(pnd_Trans));                                                   //Natural: MOVE EW-TRANS-TYPE-TABLE.#EW-TRANS-DESC-40 ( #TRANS ) TO #TRANS-TYPE-CATEGORY #CENTER-WORK
        pnd_Center_Work.setValue(ldaFcpl362.getEw_Trans_Type_Table_Pnd_Ew_Trans_Desc_40().getValue(pnd_Trans));
                                                                                                                                                                          //Natural: PERFORM LINE-CENTERING
        sub_Line_Centering();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Trans_Type_Category.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_Center_Work, pnd_Trans_Type_Category));                                      //Natural: COMPRESS #CENTER-WORK #TRANS-TYPE-CATEGORY INTO #TRANS-TYPE-CATEGORY LEAVING NO
        DbsUtil.examine(new ExamineSource(pnd_Trans_Type_Category,true), new ExamineSearch("*", true), new ExamineReplace(" "));                                          //Natural: EXAMINE FULL #TRANS-TYPE-CATEGORY FOR FULL '*' REPLACE ' '
        pnd_Center_Work.reset();                                                                                                                                          //Natural: RESET #CENTER-WORK
        pnd_Sum_Trans.setValue(ldaFcpl362.getEw_Trans_Type_Table_Pnd_Summary_Index().getValue(pnd_Trans));                                                                //Natural: ASSIGN #SUM-TRANS := EW-TRANS-TYPE-TABLE.#SUMMARY-INDEX ( #TRANS )
        pnd_Summary_Totals_Pnd_Trans_Category.getValue(pnd_Sum_Trans).setValue(ldaFcpl362.getEw_Trans_Type_Table_Pnd_Ew_Trans_Category().getValue(pnd_Trans));            //Natural: MOVE EW-TRANS-TYPE-TABLE.#EW-TRANS-CATEGORY ( #TRANS ) TO #SUMMARY-TOTALS.#TRANS-CATEGORY ( #SUM-TRANS )
        //* *----------**
    }
    private void sub_Line_Centering() throws Exception                                                                                                                    //Natural: LINE-CENTERING
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------**
        DbsUtil.examine(new ExamineSource(pnd_Center_Work,true), new ExamineSearch(" "), new ExamineGivingNumber(pnd_Center_Length));                                     //Natural: EXAMINE FULL #CENTER-WORK FOR ' ' GIVING NUMBER #CENTER-LENGTH
        pnd_Center_Work.reset();                                                                                                                                          //Natural: RESET #CENTER-WORK
        pnd_Center_Length.compute(new ComputeParameters(false, pnd_Center_Length), (pnd_Center_Length.divide(2)));                                                        //Natural: ASSIGN #CENTER-LENGTH := ( #CENTER-LENGTH / 2 )
        pnd_Center_Work.moveAll("*");                                                                                                                                     //Natural: MOVE ALL '*' TO #CENTER-WORK UNTIL #CENTER-LENGTH
        //* *----------**
    }
    private void sub_Populate_Detail() throws Exception                                                                                                                   //Natural: POPULATE-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------**
        if (condition(! (pnd_Logical_Vars_Pnd_Detail_Recs.getBoolean())))                                                                                                 //Natural: IF NOT #DETAIL-RECS
        {
            pnd_Logical_Vars_Pnd_Detail_Recs.setValue(true);                                                                                                              //Natural: ASSIGN #DETAIL-RECS := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FIRST INSTALLMENT
        if (condition(pnd_Logical_Vars_Pnd_First.getBoolean()))                                                                                                           //Natural: IF #FIRST
        {
                                                                                                                                                                          //Natural: PERFORM INITIALIZATION
            sub_Initialization();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Logical_Vars_Pnd_First.setValue(false);                                                                                                                   //Natural: ASSIGN #FIRST := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 EXT.C-INV-ACCT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaFcpaext.getExt_C_Inv_Acct())); pnd_I.nadd(1))
        {
            //*  EACH INSTALLMENT
            if (condition(pnd_I.equals(1)))                                                                                                                               //Natural: IF #I = 1
            {
                //*  INSTALLMENT LEVEL
                                                                                                                                                                          //Natural: PERFORM CALCULATE-DEDUCTIONS
                sub_Calculate_Deductions();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            pnd_Detail_Pnd_Det_Ded_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Exp_Amt().getValue(pnd_I));                                                                        //Natural: ADD EXT.INV-ACCT-EXP-AMT ( #I ) TO #DETAIL.#DET-DED-AMT
            pnd_Detail_Pnd_Det_Gross_Amt.compute(new ComputeParameters(false, pnd_Detail_Pnd_Det_Gross_Amt), pnd_Detail_Pnd_Det_Gross_Amt.add(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(pnd_I)).add(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(pnd_I))); //Natural: ASSIGN #DETAIL.#DET-GROSS-AMT := #DETAIL.#DET-GROSS-AMT + EXT.INV-ACCT-SETTL-AMT ( #I ) + EXT.INV-ACCT-DVDND-AMT ( #I )
            pnd_Contractual_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(pnd_I));                                                                             //Natural: ADD EXT.INV-ACCT-SETTL-AMT ( #I ) TO #CONTRACTUAL-AMT
            pnd_Dvdnd_Cref_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(pnd_I));                                                                              //Natural: ADD EXT.INV-ACCT-DVDND-AMT ( #I ) TO #DVDND-CREF-AMT
            pnd_Detail_Pnd_Det_Tax_Amt.compute(new ComputeParameters(false, pnd_Detail_Pnd_Det_Tax_Amt), pnd_Detail_Pnd_Det_Tax_Amt.add(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_I)).add(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(pnd_I)).add(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(pnd_I))); //Natural: ASSIGN #DETAIL.#DET-TAX-AMT := #DETAIL.#DET-TAX-AMT + EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) + EXT.INV-ACCT-STATE-TAX-AMT ( #I ) + EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
            pnd_Detail_Pnd_Det_Int_Amt.compute(new ComputeParameters(false, pnd_Detail_Pnd_Det_Int_Amt), pnd_Detail_Pnd_Det_Int_Amt.add(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(pnd_I)).add(pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue(pnd_I))); //Natural: ASSIGN #DETAIL.#DET-INT-AMT := #DETAIL.#DET-INT-AMT + EXT.INV-ACCT-DPI-AMT ( #I ) + EXT.INV-ACCT-DCI-AMT ( #I )
            pnd_Detail_Pnd_Det_Net_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_I));                                                                  //Natural: ASSIGN #DETAIL.#DET-NET-AMT := #DETAIL.#DET-NET-AMT + EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *----------**
    }
    private void sub_Print_Detail() throws Exception                                                                                                                      //Natural: PRINT-DETAIL
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------**
        getReports().display(1, "Payment/Number",                                                                                                                         //Natural: DISPLAY ( 01 ) 'Payment/Number' #DETAIL.#PYMNT-NUMBER 'Contract/Number' #DETAIL.CNTRCT-PPCN-NBR 'Check/Date' #DETAIL.PYMNT-CHECK-DTE 'Geo/Cde' #DETAIL.ANNT-RSDNCY-CDE 'Gross/Amount' #DETAIL.#DET-GROSS-AMT '/Taxes' #DETAIL.#DET-TAX-AMT '/Deductions' #DETAIL.#DET-DED-AMT '/Interest' #DETAIL.#DET-INT-AMT 'Net/Payment' #DETAIL.#DET-NET-AMT '/PAYEE' #DETAIL.#PYMNT-NME ( AL = 28 )
        		pnd_Detail_Pnd_Pymnt_Number,"Contract/Number",
        		pnd_Detail_Cntrct_Ppcn_Nbr,"Check/Date",
        		pnd_Detail_Pymnt_Check_Dte,"Geo/Cde",
        		pnd_Detail_Annt_Rsdncy_Cde,"Gross/Amount",
        		pnd_Detail_Pnd_Det_Gross_Amt,"/Taxes",
        		pnd_Detail_Pnd_Det_Tax_Amt,"/Deductions",
        		pnd_Detail_Pnd_Det_Ded_Amt,"/Interest",
        		pnd_Detail_Pnd_Det_Int_Amt,"Net/Payment",
        		pnd_Detail_Pnd_Det_Net_Amt,"/PAYEE",
        		pnd_Detail_Pnd_Pymnt_Nme, new AlphanumericLength (28));
        if (Global.isEscape()) return;
        //*              '/PAYEE'          #DETAIL.#PYMNT-NME (AL=34)
                                                                                                                                                                          //Natural: PERFORM CALCULATE-TOTALS
        sub_Calculate_Totals();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Detail.reset();                                                                                                                                               //Natural: RESET #DETAIL #CONTRACTUAL-AMT #DVDND-CREF-AMT #CREF-GROSS-AMT
        pnd_Contractual_Amt.reset();
        pnd_Dvdnd_Cref_Amt.reset();
        pnd_Cref_Gross_Amt.reset();
        pnd_Logical_Vars_Pnd_First.resetInitial();                                                                                                                        //Natural: RESET INITIAL #FIRST
        //* *----------**
    }
    private void sub_Print_Dte_Sub_Total() throws Exception                                                                                                               //Natural: PRINT-DTE-SUB-TOTAL
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------**
        getReports().write(1, NEWLINE,NEWLINE,"Sub-Total:",new TabSetting(34),pnd_Dte_Sub_Totals_Pnd_Gross_Amt, new ReportEditMask ("ZZ,ZZZ,ZZ9.99-"),new                 //Natural: WRITE ( 01 ) // 'Sub-Total:' 034T #DTE-SUB-TOTALS.#GROSS-AMT 049T #DTE-SUB-TOTALS.#TAX-AMT 064T #DTE-SUB-TOTALS.#DEDUCTION-AMT 076T #DTE-SUB-TOTALS.#INTEREST-AMT 088T #DTE-SUB-TOTALS.#NET-AMT 104T #DTE-SUB-TOTALS.#QTY
            TabSetting(49),pnd_Dte_Sub_Totals_Pnd_Tax_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(64),pnd_Dte_Sub_Totals_Pnd_Deduction_Amt, 
            new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(76),pnd_Dte_Sub_Totals_Pnd_Interest_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99-"),new TabSetting(88),pnd_Dte_Sub_Totals_Pnd_Net_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99-"),new TabSetting(104),pnd_Dte_Sub_Totals_Pnd_Qty, new ReportEditMask ("ZZZ,ZZ9-"));
        if (Global.isEscape()) return;
        pnd_Dte_Sub_Totals.reset();                                                                                                                                       //Natural: RESET #DTE-SUB-TOTALS
        pnd_Prev_Seq_Number.setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                                                            //Natural: ASSIGN #PREV-SEQ-NUMBER := EXT.PYMNT-PRCSS-SEQ-NBR
        //* *----------**
    }
    private void sub_Print_Summary_Totals() throws Exception                                                                                                              //Natural: PRINT-SUMMARY-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #TRANS 1 13
        for (pnd_Trans.setValue(1); condition(pnd_Trans.lessOrEqual(13)); pnd_Trans.nadd(1))
        {
            if (condition(getReports().getAstLinesLeft(2).less(6)))                                                                                                       //Natural: NEWPAGE ( 02 ) IF LESS THAN 6
            {
                getReports().newPage(2);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }
            if (condition(pnd_Logical_Vars_Pnd_Totals_Printed.getBoolean()))                                                                                              //Natural: IF #TOTALS-PRINTED
            {
                getReports().skip(2, 2);                                                                                                                                  //Natural: SKIP ( 02 ) 2
                pnd_Logical_Vars_Pnd_Totals_Printed.setValue(false);                                                                                                      //Natural: ASSIGN #TOTALS-PRINTED := FALSE
            }                                                                                                                                                             //Natural: END-IF
            FOR03:                                                                                                                                                        //Natural: FOR #PYMNT 1 4
            for (pnd_Pymnt.setValue(1); condition(pnd_Pymnt.lessOrEqual(4)); pnd_Pymnt.nadd(1))
            {
                if (condition(pnd_Summary_Totals_Pnd_Quantity.getValue(pnd_Trans,pnd_Pymnt).notEquals(getZero())))                                                        //Natural: IF #SUMMARY-TOTALS.#QUANTITY ( #TRANS,#PYMNT ) NE 0
                {
                    //*  CHECK PYMNT & OTHER TOTALS
                    short decideConditionsMet1002 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE #PYMNT;//Natural: VALUE 1
                    if (condition((pnd_Pymnt.equals(1))))
                    {
                        decideConditionsMet1002++;
                        if (condition(pnd_Trans.lessOrEqual(7)))                                                                                                          //Natural: IF #TRANS LE 7
                        {
                            pnd_Summary_Category.setValue(DbsUtil.compress(pnd_Summary_Totals_Pnd_Trans_Category.getValue(pnd_Trans), "Payments Total Check"));           //Natural: COMPRESS #SUMMARY-TOTALS.#TRANS-CATEGORY ( #TRANS ) 'Payments Total Check' INTO #SUMMARY-CATEGORY
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  CHECK GRAND TOTALS
                            if (condition(pnd_Trans.equals(8)))                                                                                                           //Natural: IF #TRANS EQ 8
                            {
                                pnd_Summary_Category.setValue("Total Check Payments");                                                                                    //Natural: ASSIGN #SUMMARY-CATEGORY := 'Total Check Payments'
                            }                                                                                                                                             //Natural: END-IF
                            //*  EFT PYMNT & OTHER TOTALS
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 2
                    else if (condition((pnd_Pymnt.equals(2))))
                    {
                        decideConditionsMet1002++;
                        if (condition(pnd_Trans.lessOrEqual(7)))                                                                                                          //Natural: IF #TRANS LE 7
                        {
                            pnd_Summary_Category.setValue(DbsUtil.compress(pnd_Summary_Totals_Pnd_Trans_Category.getValue(pnd_Trans), "Payments Total EFT"));             //Natural: COMPRESS #SUMMARY-TOTALS.#TRANS-CATEGORY ( #TRANS ) 'Payments Total EFT' INTO #SUMMARY-CATEGORY
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  EFT GRAND TOTALS
                            if (condition(pnd_Trans.equals(8)))                                                                                                           //Natural: IF #TRANS EQ 8
                            {
                                pnd_Summary_Category.setValue("Total EFT Payments");                                                                                      //Natural: ASSIGN #SUMMARY-CATEGORY := 'Total EFT Payments'
                            }                                                                                                                                             //Natural: END-IF
                            //*  PAYMENT & OTHER GRAND TOTALS
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 3
                    else if (condition((pnd_Pymnt.equals(3))))
                    {
                        decideConditionsMet1002++;
                        if (condition(pnd_Trans.lessOrEqual(7)))                                                                                                          //Natural: IF #TRANS LE 7
                        {
                            pnd_Summary_Category.setValue(DbsUtil.compress(pnd_Summary_Totals_Pnd_Trans_Category.getValue(pnd_Trans), "Payments Total"));                 //Natural: COMPRESS #SUMMARY-TOTALS.#TRANS-CATEGORY ( #TRANS ) 'Payments Total' INTO #SUMMARY-CATEGORY
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            //*  PAYMENT & OTHER GRAND TOTALS
                            if (condition(pnd_Trans.equals(8)))                                                                                                           //Natural: IF #TRANS EQ 8
                            {
                                pnd_Summary_Category.setValue("Total Check/EFT Payments");                                                                                //Natural: ASSIGN #SUMMARY-CATEGORY := 'Total Check/EFT Payments'
                            }                                                                                                                                             //Natural: END-IF
                            //*  OTHER NON CHK / EFT TOTALS
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: VALUE 4
                    else if (condition((pnd_Pymnt.equals(4))))
                    {
                        decideConditionsMet1002++;
                        if (condition(pnd_Trans.equals(7)))                                                                                                               //Natural: IF #TRANS EQ 7
                        {
                            pnd_Summary_Category.setValue(DbsUtil.compress(pnd_Summary_Totals_Pnd_Trans_Category.getValue(pnd_Trans), "Non Check/EFT Total"));            //Natural: COMPRESS #SUMMARY-TOTALS.#TRANS-CATEGORY ( #TRANS ) 'Non Check/EFT Total' INTO #SUMMARY-CATEGORY
                        }                                                                                                                                                 //Natural: END-IF
                        //*  OTHER NON CHK/EFT GRAND TOTALS
                        if (condition(pnd_Trans.equals(8)))                                                                                                               //Natural: IF #TRANS EQ 8
                        {
                            pnd_Summary_Category.setValue("Total Non Check/EFT");                                                                                         //Natural: ASSIGN #SUMMARY-CATEGORY := 'Total Non Check/EFT'
                        }                                                                                                                                                 //Natural: END-IF
                        //*  NON-PAYMENT TOTALS
                        if (condition(pnd_Trans.greaterOrEqual(9)))                                                                                                       //Natural: IF #TRANS GE 9
                        {
                            pnd_Summary_Category.setValue(DbsUtil.compress("Total", pnd_Summary_Totals_Pnd_Trans_Category.getValue(pnd_Trans)));                          //Natural: COMPRESS 'Total' #SUMMARY-TOTALS.#TRANS-CATEGORY ( #TRANS ) INTO #SUMMARY-CATEGORY
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ANY
                    if (condition(decideConditionsMet1002 > 0))
                    {
                                                                                                                                                                          //Natural: PERFORM PRINT-SUMMARY-LINE
                        sub_Print_Summary_Line();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pnd_Logical_Vars_Pnd_Totals_Printed.setValue(true);                                                                                               //Natural: ASSIGN #TOTALS-PRINTED := TRUE
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *----------**
    }
    private void sub_Initialization() throws Exception                                                                                                                    //Natural: INITIALIZATION
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------**
        //*  USE NAME 1
        pnd_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                                             //Natural: MOVE BY NAME EXTR TO #DETAIL
        pnd_Detail_Pnd_Pymnt_Nme.setValue(pdaFcpaext.getExt_Pymnt_Nme().getValue(1));                                                                                     //Natural: ASSIGN #DETAIL.#PYMNT-NME := EXT.PYMNT-NME ( 1 )
        //* ******************** RL PAYEE MATCH BEGIN MAY 26 2006 *****************
        pnd_Check_Datex.setValueEdited(pdaFcpaext.getExt_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                               //Natural: MOVE EDITED EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
        if (condition(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                                              //Natural: IF EXT.PYMNT-PAY-TYPE-REQ-IND = 1
        {
            //* *MOVE EDITED EXT.PYMNT-CHECK-NBR (EM=9999999)
            //* *  TO #DETAIL.#PYMNT-NUMBER
            pnd_Detail_Pnd_Pymnt_Number.reset();                                                                                                                          //Natural: RESET #DETAIL.#PYMNT-NUMBER
            //* N7 CHK-NBR - MAKE N10 W PREFIX
            if (condition(pdaFcpaext.getExt_Pymnt_Check_Nbr().less(getZero())))                                                                                           //Natural: IF EXT.PYMNT-CHECK-NBR LT 0
            {
                pnd_Detail_Pnd_Pymnt_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                      //Natural: ASSIGN #DETAIL.#PYMNT-NBR-N3 := FCPA110.START-CHECK-PREFIX-N3
                pnd_Detail_Pnd_Pymnt_Nbr_N7.compute(new ComputeParameters(false, pnd_Detail_Pnd_Pymnt_Nbr_N7), pdaFcpaext.getExt_Pymnt_Check_Nbr().multiply(-1));         //Natural: COMPUTE #DETAIL.#PYMNT-NBR-N7 = EXT.PYMNT-CHECK-NBR * -1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  VALID OLD N7 CHECK-NBR
                if (condition(pdaFcpaext.getExt_Pymnt_Check_Nbr().greater(getZero())))                                                                                    //Natural: IF EXT.PYMNT-CHECK-NBR GT 0
                {
                    pnd_Detail_Pnd_Pymnt_Nbr_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                                            //Natural: ASSIGN #DETAIL.#PYMNT-NBR-N7 := EXT.PYMNT-CHECK-NBR
                    if (condition(pnd_Check_Datex_Pnd_Check_Date.greater(20060430)))                                                                                      //Natural: IF #CHECK-DATE GT 20060430
                    {
                        pnd_Detail_Pnd_Pymnt_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                              //Natural: ASSIGN #DETAIL.#PYMNT-NBR-N3 := FCPA110.START-CHECK-PREFIX-N3
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Detail_Pnd_Pymnt_Nbr_N3.reset();                                                                                                              //Natural: RESET #DETAIL.#PYMNT-NBR-N3
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Detail_Pnd_Pymnt_Number.reset();                                                                                                                  //Natural: RESET #DETAIL.#PYMNT-NUMBER
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  MOVE EDITED EXT.PYMNT-CHECK-SCRTY-NBR (EM=9999999)TO #DETAIL.#PYMNT-N
            pnd_Detail_Pnd_Pymnt_Nbr_N3.reset();                                                                                                                          //Natural: RESET #DETAIL.#PYMNT-NBR-N3
            pnd_Detail_Pnd_Pymnt_Nbr_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr());                                                                              //Natural: MOVE EXT.PYMNT-CHECK-SCRTY-NBR TO #DETAIL.#PYMNT-NBR-N7
        }                                                                                                                                                                 //Natural: END-IF
        //* *********************** RL PAYEE MATCH END ***************************
        //*  CHECK
        //*  EFT
        //*  NON CHK/EFT
        //*  NON CHK/EFT
        //*  NON CHK/EFT
        short decideConditionsMet1102 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE EXT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet1102++;
            pnd_Detail_Pnd_Pymnt_Number.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "C", pnd_Detail_Pnd_Pymnt_Number));                                      //Natural: COMPRESS 'C' #DETAIL.#PYMNT-NUMBER INTO #DETAIL.#PYMNT-NUMBER LEAVING NO SPACE
            pnd_Pymnt.setValue(1);                                                                                                                                        //Natural: ASSIGN #PYMNT := 1
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet1102++;
            pnd_Detail_Pnd_Pymnt_Number.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "E", pnd_Detail_Pnd_Pymnt_Number));                                      //Natural: COMPRESS 'E' #DETAIL.#PYMNT-NUMBER INTO #DETAIL.#PYMNT-NUMBER LEAVING NO SPACE
            pnd_Pymnt.setValue(2);                                                                                                                                        //Natural: ASSIGN #PYMNT := 2
        }                                                                                                                                                                 //Natural: VALUE 6
        else if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(6))))
        {
            decideConditionsMet1102++;
            pnd_Detail_Pnd_Pymnt_Number.setValue("SUSPENSE");                                                                                                             //Natural: ASSIGN #DETAIL.#PYMNT-NUMBER := 'SUSPENSE'
            pnd_Pymnt.setValue(4);                                                                                                                                        //Natural: ASSIGN #PYMNT := 4
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8))))
        {
            decideConditionsMet1102++;
            pnd_Detail_Pnd_Pymnt_Number.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "X", pnd_Detail_Pnd_Pymnt_Number));                                      //Natural: COMPRESS 'X' #DETAIL.#PYMNT-NUMBER INTO #DETAIL.#PYMNT-NUMBER LEAVING NO SPACE
            pnd_Pymnt.setValue(4);                                                                                                                                        //Natural: ASSIGN #PYMNT := 4
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Detail_Pnd_Pymnt_Number.setValue(pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr(), MoveOption.RightJustified);                                                   //Natural: MOVE RIGHT EXT.PYMNT-CHECK-SCRTY-NBR TO #DETAIL.#PYMNT-NUMBER
            pnd_Pymnt.setValue(4);                                                                                                                                        //Natural: ASSIGN #PYMNT := 4
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* *----------**
    }
    private void sub_Calculate_Deductions() throws Exception                                                                                                              //Natural: CALCULATE-DEDUCTIONS
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #I 1 EXT.C-PYMNT-DED-GRP
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaFcpaext.getExt_C_Pymnt_Ded_Grp())); pnd_I.nadd(1))
        {
            if (condition(pdaFcpaext.getExt_Pymnt_Ded_Amt().getValue(pnd_I).notEquals(getZero())))                                                                        //Natural: IF EXT.PYMNT-DED-AMT ( #I ) NE 0
            {
                pnd_Detail_Pnd_Det_Ded_Amt.nadd(pdaFcpaext.getExt_Pymnt_Ded_Amt().getValue(pnd_I));                                                                       //Natural: ADD EXT.PYMNT-DED-AMT ( #I ) TO #DETAIL.#DET-DED-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* *----------**
    }
    private void sub_Calculate_Totals() throws Exception                                                                                                                  //Natural: CALCULATE-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------**
        pnd_Dte_Sub_Totals_Pnd_Qty.nadd(1);                                                                                                                               //Natural: ADD 1 TO #DTE-SUB-TOTALS.#QTY
        pnd_Dte_Sub_Totals_Pnd_Gross_Amt.nadd(pnd_Detail_Pnd_Det_Gross_Amt);                                                                                              //Natural: ADD #DETAIL.#DET-GROSS-AMT TO #DTE-SUB-TOTALS.#GROSS-AMT
        pnd_Dte_Sub_Totals_Pnd_Tax_Amt.nadd(pnd_Detail_Pnd_Det_Tax_Amt);                                                                                                  //Natural: ADD #DETAIL.#DET-TAX-AMT TO #DTE-SUB-TOTALS.#TAX-AMT
        pnd_Dte_Sub_Totals_Pnd_Deduction_Amt.nadd(pnd_Detail_Pnd_Det_Ded_Amt);                                                                                            //Natural: ADD #DETAIL.#DET-DED-AMT TO #DTE-SUB-TOTALS.#DEDUCTION-AMT
        pnd_Dte_Sub_Totals_Pnd_Interest_Amt.nadd(pnd_Detail_Pnd_Det_Int_Amt);                                                                                             //Natural: ADD #DETAIL.#DET-INT-AMT TO #DTE-SUB-TOTALS.#INTEREST-AMT
        pnd_Dte_Sub_Totals_Pnd_Net_Amt.nadd(pnd_Detail_Pnd_Det_Net_Amt);                                                                                                  //Natural: ADD #DETAIL.#DET-NET-AMT TO #DTE-SUB-TOTALS.#NET-AMT
        pnd_Summary_Totals_Pnd_Quantity.getValue(pnd_Sum_Trans,pnd_Pymnt).nadd(1);                                                                                        //Natural: ADD 1 TO #SUMMARY-TOTALS.#QUANTITY ( #SUM-TRANS,#PYMNT )
        pnd_Summary_Totals_Pnd_Contract_Amt.getValue(pnd_Sum_Trans,pnd_Pymnt).nadd(pnd_Contractual_Amt);                                                                  //Natural: ADD #CONTRACTUAL-AMT TO #SUMMARY-TOTALS.#CONTRACT-AMT ( #SUM-TRANS,#PYMNT )
        pnd_Summary_Totals_Pnd_Dividend_Amt.getValue(pnd_Sum_Trans,pnd_Pymnt).nadd(pnd_Dvdnd_Cref_Amt);                                                                   //Natural: ADD #DVDND-CREF-AMT TO #SUMMARY-TOTALS.#DIVIDEND-AMT ( #SUM-TRANS,#PYMNT )
        pnd_Summary_Totals_Pnd_Total_Tax_Amt.getValue(pnd_Sum_Trans,pnd_Pymnt).nadd(pnd_Detail_Pnd_Det_Tax_Amt);                                                          //Natural: ADD #DETAIL.#DET-TAX-AMT TO #SUMMARY-TOTALS.#TOTAL-TAX-AMT ( #SUM-TRANS,#PYMNT )
        pnd_Summary_Totals_Pnd_Total_Int_Amt.getValue(pnd_Sum_Trans,pnd_Pymnt).nadd(pnd_Detail_Pnd_Det_Int_Amt);                                                          //Natural: ADD #DETAIL.#DET-INT-AMT TO #SUMMARY-TOTALS.#TOTAL-INT-AMT ( #SUM-TRANS,#PYMNT )
        pnd_Summary_Totals_Pnd_Total_Net_Amt.getValue(pnd_Sum_Trans,pnd_Pymnt).nadd(pnd_Detail_Pnd_Det_Net_Amt);                                                          //Natural: ADD #DETAIL.#DET-NET-AMT TO #SUMMARY-TOTALS.#TOTAL-NET-AMT ( #SUM-TRANS,#PYMNT )
        if (condition(pnd_Sum_Trans.lessOrEqual(7)))                                                                                                                      //Natural: IF #SUM-TRANS LE 7
        {
            pnd_Summary_Totals_Pnd_Quantity.getValue(8,pnd_Pymnt).nadd(1);                                                                                                //Natural: ADD 1 TO #SUMMARY-TOTALS.#QUANTITY ( 8,#PYMNT )
            pnd_Summary_Totals_Pnd_Contract_Amt.getValue(8,pnd_Pymnt).nadd(pnd_Contractual_Amt);                                                                          //Natural: ADD #CONTRACTUAL-AMT TO #SUMMARY-TOTALS.#CONTRACT-AMT ( 8,#PYMNT )
            pnd_Summary_Totals_Pnd_Dividend_Amt.getValue(8,pnd_Pymnt).nadd(pnd_Dvdnd_Cref_Amt);                                                                           //Natural: ADD #DVDND-CREF-AMT TO #SUMMARY-TOTALS.#DIVIDEND-AMT ( 8,#PYMNT )
            pnd_Summary_Totals_Pnd_Total_Tax_Amt.getValue(8,pnd_Pymnt).nadd(pnd_Detail_Pnd_Det_Tax_Amt);                                                                  //Natural: ADD #DETAIL.#DET-TAX-AMT TO #SUMMARY-TOTALS.#TOTAL-TAX-AMT ( 8,#PYMNT )
            pnd_Summary_Totals_Pnd_Total_Int_Amt.getValue(8,pnd_Pymnt).nadd(pnd_Detail_Pnd_Det_Int_Amt);                                                                  //Natural: ADD #DETAIL.#DET-INT-AMT TO #SUMMARY-TOTALS.#TOTAL-INT-AMT ( 8,#PYMNT )
            pnd_Summary_Totals_Pnd_Total_Net_Amt.getValue(8,pnd_Pymnt).nadd(pnd_Detail_Pnd_Det_Net_Amt);                                                                  //Natural: ADD #DETAIL.#DET-NET-AMT TO #SUMMARY-TOTALS.#TOTAL-NET-AMT ( 8,#PYMNT )
            //*  PAYMENTS ONLY
            if (condition(pnd_Pymnt.notEquals(4)))                                                                                                                        //Natural: IF #PYMNT NE 4
            {
                pnd_Summary_Totals_Pnd_Quantity.getValue(pnd_Sum_Trans,3).nadd(1);                                                                                        //Natural: ADD 1 TO #SUMMARY-TOTALS.#QUANTITY ( #SUM-TRANS,3 )
                pnd_Summary_Totals_Pnd_Quantity.getValue(8,3).nadd(1);                                                                                                    //Natural: ADD 1 TO #SUMMARY-TOTALS.#QUANTITY ( 8,3 )
                pnd_Summary_Totals_Pnd_Contract_Amt.getValue(pnd_Sum_Trans,3).nadd(pnd_Contractual_Amt);                                                                  //Natural: ADD #CONTRACTUAL-AMT TO #SUMMARY-TOTALS.#CONTRACT-AMT ( #SUM-TRANS,3 )
                pnd_Summary_Totals_Pnd_Contract_Amt.getValue(8,3).nadd(pnd_Contractual_Amt);                                                                              //Natural: ADD #CONTRACTUAL-AMT TO #SUMMARY-TOTALS.#CONTRACT-AMT ( 8,3 )
                pnd_Summary_Totals_Pnd_Dividend_Amt.getValue(pnd_Sum_Trans,3).nadd(pnd_Dvdnd_Cref_Amt);                                                                   //Natural: ADD #DVDND-CREF-AMT TO #SUMMARY-TOTALS.#DIVIDEND-AMT ( #SUM-TRANS,3 )
                pnd_Summary_Totals_Pnd_Dividend_Amt.getValue(8,3).nadd(pnd_Dvdnd_Cref_Amt);                                                                               //Natural: ADD #DVDND-CREF-AMT TO #SUMMARY-TOTALS.#DIVIDEND-AMT ( 8,3 )
                pnd_Summary_Totals_Pnd_Total_Tax_Amt.getValue(pnd_Sum_Trans,3).nadd(pnd_Detail_Pnd_Det_Tax_Amt);                                                          //Natural: ADD #DETAIL.#DET-TAX-AMT TO #SUMMARY-TOTALS.#TOTAL-TAX-AMT ( #SUM-TRANS,3 )
                pnd_Summary_Totals_Pnd_Total_Tax_Amt.getValue(8,3).nadd(pnd_Detail_Pnd_Det_Tax_Amt);                                                                      //Natural: ADD #DETAIL.#DET-TAX-AMT TO #SUMMARY-TOTALS.#TOTAL-TAX-AMT ( 8,3 )
                pnd_Summary_Totals_Pnd_Total_Int_Amt.getValue(pnd_Sum_Trans,3).nadd(pnd_Detail_Pnd_Det_Int_Amt);                                                          //Natural: ADD #DETAIL.#DET-INT-AMT TO #SUMMARY-TOTALS.#TOTAL-INT-AMT ( #SUM-TRANS,3 )
                pnd_Summary_Totals_Pnd_Total_Int_Amt.getValue(8,3).nadd(pnd_Detail_Pnd_Det_Int_Amt);                                                                      //Natural: ADD #DETAIL.#DET-INT-AMT TO #SUMMARY-TOTALS.#TOTAL-INT-AMT ( 8,3 )
                pnd_Summary_Totals_Pnd_Total_Net_Amt.getValue(pnd_Sum_Trans,3).nadd(pnd_Detail_Pnd_Det_Net_Amt);                                                          //Natural: ADD #DETAIL.#DET-NET-AMT TO #SUMMARY-TOTALS.#TOTAL-NET-AMT ( #SUM-TRANS,3 )
                pnd_Summary_Totals_Pnd_Total_Net_Amt.getValue(8,3).nadd(pnd_Detail_Pnd_Det_Net_Amt);                                                                      //Natural: ADD #DETAIL.#DET-NET-AMT TO #SUMMARY-TOTALS.#TOTAL-NET-AMT ( 8,3 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //* *----------**
    }
    private void sub_Print_Summary_Line() throws Exception                                                                                                                //Natural: PRINT-SUMMARY-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------**
        getReports().display(2, "/Transaction Type",                                                                                                                      //Natural: DISPLAY ( 02 ) '/Transaction Type' #SUMMARY-CATEGORY 'Contractual/Amount' #SUMMARY-TOTALS.#CONTRACT-AMT ( #TRANS,#PYMNT ) 'Dividend/Amount' #SUMMARY-TOTALS.#DIVIDEND-AMT ( #TRANS,#PYMNT ) 'Total Tax/Amount' #SUMMARY-TOTALS.#TOTAL-TAX-AMT ( #TRANS,#PYMNT ) 'Interest/Amount' #SUMMARY-TOTALS.#TOTAL-INT-AMT ( #TRANS,#PYMNT ) 'Net/Amount' #SUMMARY-TOTALS.#TOTAL-NET-AMT ( #TRANS,#PYMNT ) '/Quantity' #SUMMARY-TOTALS.#QUANTITY ( #TRANS,#PYMNT )
        		pnd_Summary_Category,"Contractual/Amount",
        		pnd_Summary_Totals_Pnd_Contract_Amt.getValue(pnd_Trans,pnd_Pymnt),"Dividend/Amount",
        		pnd_Summary_Totals_Pnd_Dividend_Amt.getValue(pnd_Trans,pnd_Pymnt),"Total Tax/Amount",
        		pnd_Summary_Totals_Pnd_Total_Tax_Amt.getValue(pnd_Trans,pnd_Pymnt),"Interest/Amount",
        		pnd_Summary_Totals_Pnd_Total_Int_Amt.getValue(pnd_Trans,pnd_Pymnt),"Net/Amount",
        		pnd_Summary_Totals_Pnd_Total_Net_Amt.getValue(pnd_Trans,pnd_Pymnt),"/Quantity",
        		pnd_Summary_Totals_Pnd_Quantity.getValue(pnd_Trans,pnd_Pymnt));
        if (Global.isEscape()) return;
        pnd_Summary_Category.reset();                                                                                                                                     //Natural: RESET #SUMMARY-CATEGORY
        //* *----------**
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET CHECK-NO PREFIX FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"**************************************************************",NEWLINE,"***",NEWLINE,"***",Global.getPROGRAM(),"  Error:",Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"***",NEWLINE,"***  LAST RECORD READ:",NEWLINE,"***   CNR Activity Cd:",pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde(),NEWLINE,"***         Origin Cd:",pdaFcpaext.getExt_Cntrct_Orgn_Cde(),NEWLINE,"***  Transaction Type:",pdaFcpaext.getExt_Pymnt_Wrrnt_Trans_Type(),NEWLINE,"***        Check Date:",pdaFcpaext.getExt_Pymnt_Check_Dte(),  //Natural: WRITE // '**************************************************************' / '**************************************************************' / '***' / '***' *PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '***' / '***  LAST RECORD READ:' / '***   CNR Activity Cd:' EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE / '***         Origin Cd:' EXT.CNTRCT-ORGN-CDE / '***  Transaction Type:' EXT.PYMNT-WRRNT-TRANS-TYPE / '***        Check Date:' EXT.PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) / '***    Interface Date:' EXT.PYMNT-INTRFCE-DTE ( EM = MM/DD/YYYY ) / '***   Accounting Date:' EXT.PYMNT-ACCTG-DTE ( EM = MM/DD/YYYY ) / '***              PPCN:' EXT.CNTRCT-PPCN-NBR / '***             Payee:' EXT.CNTRCT-PAYEE-CDE / '***        Sequence #:' EXT.PYMNT-PRCSS-SEQ-NBR / '***       Pymnt. Type:' EXT.CNTRCT-PYMNT-TYPE-IND / '***       Settl. Type:' EXT.CNTRCT-STTLMNT-TYPE-IND / '***' / '**************************************************************' / '**************************************************************'
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"***    Interface Date:",pdaFcpaext.getExt_Pymnt_Intrfce_Dte(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"***   Accounting Date:",pdaFcpaext.getExt_Pymnt_Acctg_Dte(), 
            new ReportEditMask ("MM/DD/YYYY"),NEWLINE,"***              PPCN:",pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(),NEWLINE,"***             Payee:",pdaFcpaext.getExt_Cntrct_Payee_Cde(),
            NEWLINE,"***        Sequence #:",pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr(),NEWLINE,"***       Pymnt. Type:",pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind(),
            NEWLINE,"***       Settl. Type:",pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind(),NEWLINE,"***",NEWLINE,"**************************************************************",
            NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pdaFcpaext_getExt_Pymnt_Acctg_DteIsBreak = pdaFcpaext.getExt_Pymnt_Acctg_Dte().isBreak(endOfData);
        boolean pdaFcpaext_getExt_Pymnt_Check_DteIsBreak = pdaFcpaext.getExt_Pymnt_Check_Dte().isBreak(endOfData);
        boolean pdaFcpaext_getExt_Pymnt_Wrrnt_Trans_TypeIsBreak = pdaFcpaext.getExt_Pymnt_Wrrnt_Trans_Type().isBreak(endOfData);
        if (condition(pdaFcpaext_getExt_Pymnt_Acctg_DteIsBreak || pdaFcpaext_getExt_Pymnt_Check_DteIsBreak || pdaFcpaext_getExt_Pymnt_Wrrnt_Trans_TypeIsBreak))
        {
            if (condition(pnd_Report_Cycle.equals("LOGICAL")))                                                                                                            //Natural: IF #REPORT-CYCLE = 'LOGICAL'
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-DTE-SUB-TOTAL
                sub_Print_Dte_Sub_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM REPORT-TITLE-PROCESSING
                sub_Report_Title_Processing();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Logical_Vars_Pnd_Date_Break.setValue(true);                                                                                                           //Natural: ASSIGN #DATE-BREAK := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpaext_getExt_Pymnt_Check_DteIsBreak || pdaFcpaext_getExt_Pymnt_Wrrnt_Trans_TypeIsBreak))
        {
            if (condition(! (pnd_Report_Cycle.equals("LOGICAL"))))                                                                                                        //Natural: IF NOT ( #REPORT-CYCLE = 'LOGICAL' )
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-DTE-SUB-TOTAL
                sub_Print_Dte_Sub_Total();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM REPORT-TITLE-PROCESSING
                sub_Report_Title_Processing();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                pnd_Logical_Vars_Pnd_Date_Break.setValue(true);                                                                                                           //Natural: ASSIGN #DATE-BREAK := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpaext_getExt_Pymnt_Wrrnt_Trans_TypeIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-TYPE-PROCESSING
            sub_Transaction_Type_Processing();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Logical_Vars_Pnd_Trans_Type_Break.setValue(true);                                                                                                         //Natural: ASSIGN #TRANS-TYPE-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=66");
        Global.format(1, "LS=133 PS=66");
        Global.format(2, "LS=133 PS=66");

        getReports().write(1, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),new ColumnSpacing(1),"-",new ColumnSpacing(1),Global.getPROGRAM(),NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(54),"ELECTRONIC WARRANTS PAYMENTS",new TabSetting(119),"PAGE:",new ColumnSpacing(1),getReports().getPageNumberDbs(1), 
            new FieldAttributes ("AD=L"),NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(27),pnd_Report_Title,NEWLINE,new TabSetting(27),
            pnd_Trans_Type_Category,NEWLINE,NEWLINE);
        getReports().write(2, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getINIT_USER(),new ColumnSpacing(1),"-",new ColumnSpacing(1),Global.getPROGRAM(),NEWLINE,Global.getDATX(), 
            new ReportEditMask ("LLL' 'DD', 'YYYY"),new TabSetting(54),"ELECTRONIC WARRANTS PAYMENTS",new TabSetting(119),"PAGE:",new ColumnSpacing(1),getReports().getPageNumberDbs(2), 
            new FieldAttributes ("AD=L"),NEWLINE,Global.getTIMX(), new ReportEditMask ("HH:II' 'AP"),new TabSetting(60),"Summary Totals",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, "Payment/Number",
        		pnd_Detail_Pnd_Pymnt_Number,"Contract/Number",
        		pnd_Detail_Cntrct_Ppcn_Nbr,"Check/Date",
        		pnd_Detail_Pymnt_Check_Dte,"Geo/Cde",
        		pnd_Detail_Annt_Rsdncy_Cde,"Gross/Amount",
        		pnd_Detail_Pnd_Det_Gross_Amt,"/Taxes",
        		pnd_Detail_Pnd_Det_Tax_Amt,"/Deductions",
        		pnd_Detail_Pnd_Det_Ded_Amt,"/Interest",
        		pnd_Detail_Pnd_Det_Int_Amt,"Net/Payment",
        		pnd_Detail_Pnd_Det_Net_Amt,"/PAYEE",
        		pnd_Detail_Pnd_Pymnt_Nme, new AlphanumericLength (28));
        getReports().setDisplayColumns(2, "/Transaction Type",
        		pnd_Summary_Category,"Contractual/Amount",
        		pnd_Summary_Totals_Pnd_Contract_Amt,"Dividend/Amount",
        		pnd_Summary_Totals_Pnd_Dividend_Amt,"Total Tax/Amount",
        		pnd_Summary_Totals_Pnd_Total_Tax_Amt,"Interest/Amount",
        		pnd_Summary_Totals_Pnd_Total_Int_Amt,"Net/Amount",
        		pnd_Summary_Totals_Pnd_Total_Net_Amt,"/Quantity",
        		pnd_Summary_Totals_Pnd_Quantity);
    }
    private void CheckAtStartofData707() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM REPORT-TITLE-PROCESSING
            sub_Report_Title_Processing();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM TRANSACTION-TYPE-PROCESSING
            sub_Transaction_Type_Processing();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Prev_Seq_Number.setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                                                        //Natural: ASSIGN #PREV-SEQ-NUMBER := EXT.PYMNT-PRCSS-SEQ-NBR
        }                                                                                                                                                                 //Natural: END-START
    }
}
