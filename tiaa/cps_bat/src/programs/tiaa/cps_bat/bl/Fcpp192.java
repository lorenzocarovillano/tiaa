/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:40 PM
**        * FROM NATURAL PROGRAM : Fcpp192
************************************************************
**        * FILE NAME            : Fcpp192.java
**        * CLASS NAME           : Fcpp192
**        * INSTANCE NAME        : Fcpp192
************************************************************
***********************************************************************
** PROGRAM:     FCPP192                                              **
** SYSTEM:      FULL CONSOLIDATED PAYMENT SYSTEM                     **
** DATE:        07-12-94                                             **
** AUTHOR:      ALTHEA A. YOUNG                                      **
** DESCRIPTION: THIS PROGRAM READS THE CHECK EXTRACT CONTAINING ALL  **
**              STATUS 'D' (DAYTIME) RECORDS AND CREATES A REPORT    **
**              BASED ON THE ORIGIN CODES.                           **
**                                                                   **
** JOB STREAM:  CPS DAYTIME (RA CASH, IA DEATH)                      **
**                                                                   **
** 07/19/96 LZ  - INCLUDED MDO IN THE REPORT.                        **
** 05/18/98 AAY - BYPASS INTERNAL ROLLOVERS (CLASSIC, ROTH).         **
** 02/02/2000 ILSE BOHM & LEON GURTOVNIK -- PRODUCTION FIX --
**              - ENLARGED ALL P2, P4 FIELDS TO P5
**              - ENLARGED ALL P9.2 FIELDS TO P10.2
** 07/11/00 AAY - INCREASED LENGTH OF IVC P2 AND P4 FIELDS TO P5.    **
** 09/19/00 JHH - ADD SUB-TOTALS FOR LUMP SUM DC                     **
* 4/2017     : JJG - PIN EXPANSION RESTOW                            **
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp192 extends BLNatBase
{
    // Data Areas
    private LdaFcpl190a ldaFcpl190a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Accumulators_Flags_Counters;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Recs_Read;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Recs_Written;
    private DbsField pnd_Accumulators_Flags_Counters_X;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Recs_Read;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Recs_Written;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Gross_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ss_Dpi_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Recs_Read;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Recs_Written;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Gross_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Dvdnd_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Dpi_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Dc_Dci_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Gross_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Dvdnd_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Dpi_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Ls_Dci_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Tot_Gross_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Tot_Dvdnd_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Tot_Dpi_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Tot_Dci_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Inv_Reg_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Mdo_Pymnt_Cnt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Mdo_Gross_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Mdo_Dpi_Amt;
    private DbsField pnd_Accumulators_Flags_Counters_Pnd_Mdo_Net_Amt;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Accumulators_Flags_Counters = localVariables.newGroupInRecord("pnd_Accumulators_Flags_Counters", "#ACCUMULATORS-FLAGS-COUNTERS");
        pnd_Accumulators_Flags_Counters_Pnd_Recs_Read = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Recs_Read", 
            "#RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Accumulators_Flags_Counters_Pnd_Recs_Written = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Recs_Written", 
            "#RECS-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Accumulators_Flags_Counters_X = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_X", "X", FieldType.PACKED_DECIMAL, 
            2);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Recs_Read = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Recs_Read", 
            "#SS-RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Recs_Written = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Recs_Written", 
            "#SS-RECS-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt", 
            "#SS-REG-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt", 
            "#SS-IVC-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt", 
            "#SS-TOT-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt_Amt", 
            "#SS-REG-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt_Amt", 
            "#SS-IVC-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt_Amt", 
            "#SS-TOT-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Gross_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Gross_Amt", 
            "#SS-GROSS-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Dpi_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ss_Dpi_Amt", 
            "#SS-DPI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Recs_Read = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Recs_Read", 
            "#DC-RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Recs_Written = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Recs_Written", 
            "#DC-RECS-WRITTEN", FieldType.PACKED_DECIMAL, 7);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt", 
            "#DC-REG-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt", 
            "#DC-IVC-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt", 
            "#DC-TOT-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt_Amt", 
            "#DC-REG-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt_Amt", 
            "#DC-IVC-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt_Amt", 
            "#DC-TOT-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Gross_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Gross_Amt", 
            "#DC-GROSS-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Dvdnd_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Dvdnd_Amt", 
            "#DC-DVDND-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Dpi_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Dpi_Amt", 
            "#DC-DPI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Dci_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Dc_Dci_Amt", 
            "#DC-DCI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt", 
            "#LS-REG-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt", 
            "#LS-IVC-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt", 
            "#LS-TOT-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt_Amt", 
            "#LS-REG-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt_Amt", 
            "#LS-IVC-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt_Amt", 
            "#LS-TOT-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Gross_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Gross_Amt", 
            "#LS-GROSS-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Dvdnd_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Dvdnd_Amt", 
            "#LS-DVDND-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Dpi_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Dpi_Amt", 
            "#LS-DPI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Dci_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Ls_Dci_Amt", 
            "#LS-DCI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt", 
            "#TOT-REG-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt", 
            "#TOT-IVC-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt", 
            "#GTOT-PYMNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt_Amt", 
            "#TOT-REG-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt_Amt", 
            "#TOT-IVC-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt_Amt", 
            "#GTOT-PYMNT-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Gross_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Tot_Gross_Amt", 
            "#TOT-GROSS-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Dvdnd_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Tot_Dvdnd_Amt", 
            "#TOT-DVDND-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Dpi_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Tot_Dpi_Amt", 
            "#TOT-DPI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Dci_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Tot_Dci_Amt", 
            "#TOT-DCI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs", 
            "#INV-IVCS", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt", 
            "#INV-IVC-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Reg_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Inv_Reg_Amt", 
            "#INV-REG-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt", 
            "#INV-DPI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt", 
            "#INV-DCI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt", 
            "#INV-DVDND-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt", 
            "#INV-GROSS-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt", 
            "#INV-NET-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Mdo_Pymnt_Cnt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Mdo_Pymnt_Cnt", 
            "#MDO-PYMNT-CNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Accumulators_Flags_Counters_Pnd_Mdo_Gross_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Mdo_Gross_Amt", 
            "#MDO-GROSS-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Mdo_Dpi_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Mdo_Dpi_Amt", 
            "#MDO-DPI-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        pnd_Accumulators_Flags_Counters_Pnd_Mdo_Net_Amt = pnd_Accumulators_Flags_Counters.newFieldInGroup("pnd_Accumulators_Flags_Counters_Pnd_Mdo_Net_Amt", 
            "#MDO-NET-AMT", FieldType.PACKED_DECIMAL, 12, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_Accumulators_Flags_Counters_X.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Gross_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ss_Dpi_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Gross_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Dvdnd_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Dpi_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Dc_Dci_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Gross_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Dvdnd_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Dpi_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Ls_Dci_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Gross_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Dvdnd_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Dpi_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Tot_Dci_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Reg_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt.setInitialValue(0);
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp192() throws Exception
    {
        super("Fcpp192");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  --------------------------------------------------------
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "PRINT1");                                                                                                                          //Natural: DEFINE PRINTER ( PRINT1 = 1 ) OUTPUT 'CMPRT01'
        //*  -------------       SINGLE SUM / MDO / IA DEATH PAYMENTS       '                                                                                             //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( PRINT1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        //*  --------------                                                                                                                                               //Natural: AT TOP OF PAGE ( PRINT1 )
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        READWORK01:                                                                                                                                                       //Natural: READ WORK 3 #RPT-EXT
        while (condition(getWorkFiles().read(3, ldaFcpl190a.getPnd_Rpt_Ext())))
        {
            //*  05-18-98
            if (condition(!((((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS"))                    //Natural: ACCEPT IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' OR = 'SS' AND #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' AND #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND NE 8
                && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" ")) && ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().notEquals(8)))))
            {
                continue;
            }
                                                                                                                                                                          //Natural: PERFORM TOTAL-CALCULATIONS
            sub_Total_Calculations();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-TOTAL-REPORT
            sub_Print_Total_Report();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-CALCULATIONS
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-TOTAL-REPORT
        //*     7T 'RA CASH  - REGULAR PAYMENT COUNT'    64T #SS-REG-PYMNT
        //*  ------------------------------ JHH 9/19/2000
        //*  ------------------------------ JHH 9/19/2000 END
    }
    private void sub_Total_Calculations() throws Exception                                                                                                                //Natural: TOTAL-CALCULATIONS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs.reset();                                                                                                             //Natural: RESET #INV-IVCS #INV-IVC-AMT #INV-REG-AMT #INV-GROSS-AMT #INV-NET-AMT #INV-DPI-AMT #INV-DCI-AMT #INV-DVDND-AMT
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt.reset();
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Reg_Amt.reset();
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt.reset();
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt.reset();
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt.reset();
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt.reset();
        pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt.reset();
        pnd_Accumulators_Flags_Counters_Pnd_Recs_Read.nadd(1);                                                                                                            //Natural: ADD 1 TO #RECS-READ
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS")))                                                                                         //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'SS'
        {
            pnd_Accumulators_Flags_Counters_Pnd_Ss_Recs_Read.nadd(1);                                                                                                     //Natural: ADD 1 TO #SS-RECS-READ
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC'
            {
                pnd_Accumulators_Flags_Counters_Pnd_Dc_Recs_Read.nadd(1);                                                                                                 //Natural: ADD 1 TO #DC-RECS-READ
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR X = 1 TO #RPT-EXT.C-INV-ACCT
        for (pnd_Accumulators_Flags_Counters_X.setValue(1); condition(pnd_Accumulators_Flags_Counters_X.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); 
            pnd_Accumulators_Flags_Counters_X.nadd(1))
        {
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Ivc_Amt().getValue(pnd_Accumulators_Flags_Counters_X).equals(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_Accumulators_Flags_Counters_X)))) //Natural: IF #RPT-EXT.INV-ACCT-IVC-AMT ( X ) = #RPT-EXT.INV-ACCT-SETTL-AMT ( X )
            {
                pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs.nadd(1);                                                                                                     //Natural: ADD 1 TO #INV-IVCS
                pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Accumulators_Flags_Counters_X));    //Natural: ADD #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( X ) TO #INV-IVC-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accumulators_Flags_Counters_Pnd_Inv_Reg_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Accumulators_Flags_Counters_X));    //Natural: ADD #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( X ) TO #INV-REG-AMT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(pnd_Accumulators_Flags_Counters_X));          //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( X ) TO #INV-GROSS-AMT
            pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Accumulators_Flags_Counters_X));        //Natural: ADD #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( X ) TO #INV-NET-AMT
            pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(pnd_Accumulators_Flags_Counters_X));              //Natural: ADD #RPT-EXT.INV-ACCT-DPI-AMT ( X ) TO #INV-DPI-AMT
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC'
            {
                pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dci_Amt().getValue(pnd_Accumulators_Flags_Counters_X));          //Natural: ADD #RPT-EXT.INV-ACCT-DCI-AMT ( X ) TO #INV-DCI-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt.nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(pnd_Accumulators_Flags_Counters_X));      //Natural: ADD #RPT-EXT.INV-ACCT-DVDND-AMT ( X ) TO #INV-DVDND-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        short decideConditionsMet367 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #RPT-EXT.CNTRCT-ORGN-CDE = 'SS'
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("SS")))
        {
            decideConditionsMet367++;
            //*  MDO
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("30") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("31")))                        //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = '30' OR = '31'
            {
                pnd_Accumulators_Flags_Counters_Pnd_Mdo_Pymnt_Cnt.nadd(1);                                                                                                //Natural: ADD 1 TO #MDO-PYMNT-CNT
                pnd_Accumulators_Flags_Counters_Pnd_Mdo_Gross_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt);                                                //Natural: ADD #INV-GROSS-AMT TO #MDO-GROSS-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Mdo_Dpi_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt);                                                    //Natural: ADD #INV-DPI-AMT TO #MDO-DPI-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Mdo_Net_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                                    //Natural: ADD #INV-NET-AMT TO #MDO-NET-AMT
                //*  SINGLE SUM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs.greater(getZero())))                                                                           //Natural: IF #INV-IVCS > 0
                {
                    pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt.nadd(1);                                                                                             //Natural: ADD 1 TO #SS-IVC-PYMNT
                    pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt);                                           //Natural: ADD #INV-IVC-AMT TO #SS-IVC-PYMNT-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt.nadd(1);                                                                                             //Natural: ADD 1 TO #SS-REG-PYMNT
                    pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                           //Natural: ADD #INV-NET-AMT TO #SS-REG-PYMNT-AMT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Accumulators_Flags_Counters_Pnd_Ss_Recs_Written.nadd(1);                                                                                              //Natural: ADD 1 TO #SS-RECS-WRITTEN
                pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt.nadd(1);                                                                                                 //Natural: ADD 1 TO #SS-TOT-PYMNT
                pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                               //Natural: ADD #INV-NET-AMT TO #SS-TOT-PYMNT-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Ss_Gross_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt);                                                 //Natural: ADD #INV-GROSS-AMT TO #SS-GROSS-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Ss_Dpi_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt);                                                     //Natural: ADD #INV-DPI-AMT TO #SS-DPI-AMT
                //*  IA DEATH
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN #RPT-EXT.CNTRCT-ORGN-CDE = 'DC'
        else if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC")))
        {
            decideConditionsMet367++;
            pnd_Accumulators_Flags_Counters_Pnd_Dc_Recs_Written.nadd(1);                                                                                                  //Natural: ADD 1 TO #DC-RECS-WRITTEN
            //*  -----------------------------------------  JHH 9/19/2000
            //*  LUMP SUM
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L ")))                                                                                     //Natural: IF #RPT-EXT.CNTRCT-TYPE-CDE = 'L '
            {
                if (condition(pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs.greater(getZero())))                                                                           //Natural: IF #INV-IVCS > 0
                {
                    pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt.nadd(1);                                                                                             //Natural: ADD 1 TO #LS-IVC-PYMNT
                    pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt);                                           //Natural: ADD #INV-IVC-AMT TO #LS-IVC-PYMNT-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt.nadd(1);                                                                                             //Natural: ADD 1 TO #LS-REG-PYMNT
                    pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                           //Natural: ADD #INV-NET-AMT TO #LS-REG-PYMNT-AMT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt.nadd(1);                                                                                                 //Natural: ADD 1 TO #LS-TOT-PYMNT
                pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                               //Natural: ADD #INV-NET-AMT TO #LS-TOT-PYMNT-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Ls_Gross_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt);                                                 //Natural: ADD #INV-GROSS-AMT TO #LS-GROSS-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Ls_Dvdnd_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt);                                                 //Natural: ADD #INV-DVDND-AMT TO #LS-DVDND-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Ls_Dpi_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt);                                                     //Natural: ADD #INV-DPI-AMT TO #LS-DPI-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Ls_Dci_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt);                                                     //Natural: ADD #INV-DCI-AMT TO #LS-DCI-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  -----------------------------------------  JHH 9/19/2000 END
                if (condition(pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs.greater(getZero())))                                                                           //Natural: IF #INV-IVCS > 0
                {
                    pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt.nadd(1);                                                                                             //Natural: ADD 1 TO #DC-IVC-PYMNT
                    pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivc_Amt);                                           //Natural: ADD #INV-IVC-AMT TO #DC-IVC-PYMNT-AMT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt.nadd(1);                                                                                             //Natural: ADD 1 TO #DC-REG-PYMNT
                    pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                           //Natural: ADD #INV-NET-AMT TO #DC-REG-PYMNT-AMT
                }                                                                                                                                                         //Natural: END-IF
                pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt.nadd(1);                                                                                                 //Natural: ADD 1 TO #DC-TOT-PYMNT
                pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                               //Natural: ADD #INV-NET-AMT TO #DC-TOT-PYMNT-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Dc_Gross_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Gross_Amt);                                                 //Natural: ADD #INV-GROSS-AMT TO #DC-GROSS-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Dc_Dvdnd_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt);                                                 //Natural: ADD #INV-DVDND-AMT TO #DC-DVDND-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Dc_Dpi_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt);                                                     //Natural: ADD #INV-DPI-AMT TO #DC-DPI-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Dc_Dci_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt);                                                     //Natural: ADD #INV-DCI-AMT TO #DC-DCI-AMT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet367 > 0))
        {
            if (condition(pnd_Accumulators_Flags_Counters_Pnd_Inv_Ivcs.greater(getZero())))                                                                               //Natural: IF #INV-IVCS > 0
            {
                pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt.nadd(1);                                                                                                //Natural: ADD 1 TO #TOT-IVC-PYMNT
                pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                              //Natural: ADD #INV-NET-AMT TO #TOT-IVC-PYMNT-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt.nadd(1);                                                                                                //Natural: ADD 1 TO #TOT-REG-PYMNT
                pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Net_Amt);                                              //Natural: ADD #INV-NET-AMT TO #TOT-REG-PYMNT-AMT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Accumulators_Flags_Counters_Pnd_Recs_Written.nadd(1);                                                                                                     //Natural: ADD 1 TO #RECS-WRITTEN
            pnd_Accumulators_Flags_Counters_Pnd_Tot_Dpi_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dpi_Amt);                                                        //Natural: ADD #INV-DPI-AMT TO #TOT-DPI-AMT
            pnd_Accumulators_Flags_Counters_Pnd_Tot_Dci_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dci_Amt);                                                        //Natural: ADD #INV-DCI-AMT TO #TOT-DCI-AMT
            pnd_Accumulators_Flags_Counters_Pnd_Tot_Dvdnd_Amt.nadd(pnd_Accumulators_Flags_Counters_Pnd_Inv_Dvdnd_Amt);                                                    //Natural: ADD #INV-DVDND-AMT TO #TOT-DVDND-AMT
            pnd_Accumulators_Flags_Counters_Pnd_Tot_Gross_Amt.compute(new ComputeParameters(false, pnd_Accumulators_Flags_Counters_Pnd_Tot_Gross_Amt),                    //Natural: COMPUTE #TOT-GROSS-AMT = #SS-GROSS-AMT + #DC-GROSS-AMT + #MDO-GROSS-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Ss_Gross_Amt.add(pnd_Accumulators_Flags_Counters_Pnd_Dc_Gross_Amt).add(pnd_Accumulators_Flags_Counters_Pnd_Mdo_Gross_Amt));
            pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt.compute(new ComputeParameters(false, pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt), pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt.add(pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt)); //Natural: COMPUTE #GTOT-PYMNT = #TOT-REG-PYMNT + #TOT-IVC-PYMNT
            pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt_Amt.compute(new ComputeParameters(false, pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt_Amt),                  //Natural: COMPUTE #GTOT-PYMNT-AMT = #TOT-REG-PYMNT-AMT + #TOT-IVC-PYMNT-AMT
                pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt_Amt.add(pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt_Amt));
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_Total_Report() throws Exception                                                                                                                //Natural: PRINT-TOTAL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRINT1 )
        if (condition(Global.isEscape())){return;}
        //*  LZ
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(7),"SINGLE SUM - REGULAR PAYMENT COUNT",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt,  //Natural: WRITE ( PRINT1 ) NOTITLE 7T 'SINGLE SUM - REGULAR PAYMENT COUNT' 64T #SS-REG-PYMNT / 10T '(INCLUDES ALT1, ALT2, CHECK, EFT, GBL)' / 15T 'IVC CHECK' 64T #SS-IVC-PYMNT / 65T '-----' / 10T 'TOTAL' 64T #SS-TOT-PYMNT //10T 'GROSS AMOUNT (CONTRACTUAL)' 55T #SS-GROSS-AMT / 10T 'DPI AMOUNT' 55T #SS-DPI-AMT //10T 'REGULAR PAYMENT NET DOLLAR AMOUNT' 55T #SS-REG-PYMNT-AMT / 10T 'IVC CHECK AMOUNT' 55T #SS-IVC-PYMNT-AMT / 56T '--------------' / 10T 'TOTAL AMOUNT' 55T #SS-TOT-PYMNT-AMT /// 7T 'MINIMUM DISTRIBUTION OPTION PAYMENT COUNT' 64T #MDO-PYMNT-CNT //10T 'GROSS AMOUNT (CONTRACTUAL)' 56T #MDO-GROSS-AMT / 10T 'DPI AMOUNT' 56T #MDO-DPI-AMT //10T 'NET AMOUNT' 56T #MDO-NET-AMT ///
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(10),"(INCLUDES ALT1, ALT2, CHECK, EFT, GBL)",NEWLINE,new TabSetting(15),"IVC CHECK",new 
            TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(65),"-----",NEWLINE,new 
            TabSetting(10),"TOTAL",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"GROSS AMOUNT (CONTRACTUAL)",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ss_Gross_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(10),"DPI AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ss_Dpi_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"REGULAR PAYMENT NET DOLLAR AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ss_Reg_Pymnt_Amt, new ReportEditMask 
            ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(10),"IVC CHECK AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ss_Ivc_Pymnt_Amt, new 
            ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(56),"--------------",NEWLINE,new TabSetting(10),"TOTAL AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ss_Tot_Pymnt_Amt, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"MINIMUM DISTRIBUTION OPTION PAYMENT COUNT",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Mdo_Pymnt_Cnt, 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(10),"GROSS AMOUNT (CONTRACTUAL)",new TabSetting(56),pnd_Accumulators_Flags_Counters_Pnd_Mdo_Gross_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(10),"DPI AMOUNT",new TabSetting(56),pnd_Accumulators_Flags_Counters_Pnd_Mdo_Dpi_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new TabSetting(10),"NET AMOUNT",new TabSetting(56),pnd_Accumulators_Flags_Counters_Pnd_Mdo_Net_Amt, 
            new ReportEditMask ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( PRINT1 )
        if (condition(Global.isEscape())){return;}
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(7),"IA DEATH-CONTINUED PAYMENT COUNT",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt,  //Natural: WRITE ( PRINT1 ) NOTITLE 7T 'IA DEATH-CONTINUED PAYMENT COUNT' 64T #DC-REG-PYMNT / 10T '(INCLUDES ALT1, ALT2, CHECK, EFT, GBL)' / 15T 'IVC CHECK' 64T #DC-IVC-PYMNT / 65T '-----' / 10T 'TOTAL' 64T #DC-TOT-PYMNT //10T 'GROSS AMOUNT (CONTRACTUAL)' 55T #DC-GROSS-AMT //10T 'DIVIDEND AMOUNT' 55T #DC-DVDND-AMT / 10T 'DPI AMOUNT' 55T #DC-DPI-AMT / 10T 'DCI AMOUNT' 55T #DC-DCI-AMT //10T 'REGULAR PAYMENT NET DOLLAR AMOUNT' 56T #DC-REG-PYMNT-AMT / 10T 'IVC CHECK AMOUNT' 55T #DC-IVC-PYMNT-AMT / 56T '--------------' / 10T 'TOTAL AMOUNT' 55T #DC-TOT-PYMNT-AMT /// 7T 'IA DEATH-LUMP SUM PAYMENT COUNT' 64T #LS-REG-PYMNT / 10T '(INCLUDES ALT1, ALT2, CHECK, EFT, GBL)' / 15T 'IVC CHECK' 64T #LS-IVC-PYMNT / 65T '-----' / 10T 'TOTAL' 64T #LS-TOT-PYMNT //10T 'GROSS AMOUNT (CONTRACTUAL)' 55T #LS-GROSS-AMT //10T 'DIVIDEND AMOUNT' 55T #LS-DVDND-AMT / 10T 'DPI AMOUNT' 55T #LS-DPI-AMT / 10T 'DCI AMOUNT' 55T #LS-DCI-AMT //10T 'REGULAR PAYMENT NET DOLLAR AMOUNT' 56T #LS-REG-PYMNT-AMT / 10T 'IVC CHECK AMOUNT' 55T #LS-IVC-PYMNT-AMT / 56T '--------------' / 10T 'TOTAL AMOUNT' 55T #LS-TOT-PYMNT-AMT /// 7T 'GRAND TOTAL REGULAR PAYMENT COUNT' 64T #TOT-REG-PYMNT / 10T '(INCLUDES ALT1, ALT2, CHECK, EFT, GBL)' / 15T 'IVC CHECK' 64T #TOT-IVC-PYMNT / 65T '-----' / 10T 'GRAND TOTAL' 64T #GTOT-PYMNT //10T 'GROSS AMOUNT (CONTRACTUAL)' 55T #TOT-GROSS-AMT //10T 'DIVIDEND AMOUNT' 55T #TOT-DVDND-AMT / 10T 'DPI AMOUNT' 55T #TOT-DPI-AMT / 10T 'DCI AMOUNT' 55T #TOT-DCI-AMT //10T 'REGULAR PAYMENT NET DOLLAR AMOUNT' 55T #TOT-REG-PYMNT-AMT / 10T 'IVC CHECK AMOUNT' 55T #TOT-IVC-PYMNT-AMT / 56T '--------------' / 10T 'GRAND TOTAL DOLLAR AMOUNT' 55T #GTOT-PYMNT-AMT
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(10),"(INCLUDES ALT1, ALT2, CHECK, EFT, GBL)",NEWLINE,new TabSetting(15),"IVC CHECK",new 
            TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(65),"-----",NEWLINE,new 
            TabSetting(10),"TOTAL",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"GROSS AMOUNT (CONTRACTUAL)",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Dc_Gross_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"DIVIDEND AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Dc_Dvdnd_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(10),"DPI AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Dc_Dpi_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(10),"DCI AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Dc_Dci_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"REGULAR PAYMENT NET DOLLAR AMOUNT",new TabSetting(56),pnd_Accumulators_Flags_Counters_Pnd_Dc_Reg_Pymnt_Amt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(10),"IVC CHECK AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Dc_Ivc_Pymnt_Amt, new 
            ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(56),"--------------",NEWLINE,new TabSetting(10),"TOTAL AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Dc_Tot_Pymnt_Amt, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"IA DEATH-LUMP SUM PAYMENT COUNT",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt, 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(10),"(INCLUDES ALT1, ALT2, CHECK, EFT, GBL)",NEWLINE,new TabSetting(15),"IVC CHECK",new 
            TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(65),"-----",NEWLINE,new 
            TabSetting(10),"TOTAL",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"GROSS AMOUNT (CONTRACTUAL)",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ls_Gross_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"DIVIDEND AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ls_Dvdnd_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(10),"DPI AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ls_Dpi_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(10),"DCI AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ls_Dci_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"REGULAR PAYMENT NET DOLLAR AMOUNT",new TabSetting(56),pnd_Accumulators_Flags_Counters_Pnd_Ls_Reg_Pymnt_Amt, new ReportEditMask 
            ("ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(10),"IVC CHECK AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ls_Ivc_Pymnt_Amt, new 
            ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(56),"--------------",NEWLINE,new TabSetting(10),"TOTAL AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Ls_Tot_Pymnt_Amt, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,NEWLINE,new TabSetting(7),"GRAND TOTAL REGULAR PAYMENT COUNT",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt, 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(10),"(INCLUDES ALT1, ALT2, CHECK, EFT, GBL)",NEWLINE,new TabSetting(15),"IVC CHECK",new 
            TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(65),"-----",NEWLINE,new 
            TabSetting(10),"GRAND TOTAL",new TabSetting(64),pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(10),"GROSS AMOUNT (CONTRACTUAL)",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Tot_Gross_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"DIVIDEND AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Tot_Dvdnd_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(10),"DPI AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Tot_Dpi_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new 
            TabSetting(10),"DCI AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Tot_Dci_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE,new 
            TabSetting(10),"REGULAR PAYMENT NET DOLLAR AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Tot_Reg_Pymnt_Amt, new ReportEditMask 
            ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(10),"IVC CHECK AMOUNT",new TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Tot_Ivc_Pymnt_Amt, 
            new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(56),"--------------",NEWLINE,new TabSetting(10),"GRAND TOTAL DOLLAR AMOUNT",new 
            TabSetting(55),pnd_Accumulators_Flags_Counters_Pnd_Gtot_Pymnt_Amt, new ReportEditMask ("ZZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),"           CONSOLIDATED PAYMENT SYSTEM            ",new  //Natural: WRITE ( PRINT1 ) NOTITLE *INIT-USER '-' *PROGRAM 41T '           CONSOLIDATED PAYMENT SYSTEM            ' 122T *DATX ( EM = LLL' 'DD','YY ) / 41T ' ON-LINE PAYMENT ADDITIONAL INTEREST / IVC REPORT ' 122T *TIMX ( EM = HH':'II' 'AP ) / 41T '       SINGLE SUM / MDO / IA DEATH PAYMENTS       '
                        TabSetting(122),Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),NEWLINE,new TabSetting(41)," ON-LINE PAYMENT ADDITIONAL INTEREST / IVC REPORT ",new 
                        TabSetting(122),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,new TabSetting(41),"       SINGLE SUM / MDO / IA DEATH PAYMENTS       ");
                    getReports().skip(0, 1);                                                                                                                              //Natural: SKIP ( PRINT1 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
