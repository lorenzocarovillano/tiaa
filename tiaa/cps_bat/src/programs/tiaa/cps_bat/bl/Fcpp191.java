/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:39 PM
**        * FROM NATURAL PROGRAM : Fcpp191
************************************************************
**        * FILE NAME            : Fcpp191.java
**        * CLASS NAME           : Fcpp191
**        * INSTANCE NAME        : Fcpp191
************************************************************
***********************************************************************
** PROGRAM:     FCPP191                                              **
** SYSTEM:      FULL CONSOLIDATED PAYMENT SYSTEM                     **
** DATE:        08-24-94                                             **
** AUTHOR:      ALTHEA A. YOUNG                                      **
** DESCRIPTION: THIS PROGRAM READS THE CHECK EXTRACT CONTAINING ALL  **
**              DAYTIME RECORDS AND PRINTS A SUMMARY REPORT BASED ON **
**              THE ORIGIN CODES.                                    **
**                                                                   **
** JOB STREAM:  CPS DAYTIME (SINGLE SUM, IA DEATH, CANCELS, REDRAWS) **
**                                                                   **
** 07/19/96 LZ  - INCLUDED MDO IN THE REPORT                         **
** 05/18/98 AAY - BYPASS INTERNAL ROLLOVERS (CLASSIC, ROTH).         **
** 06/02/99 RCC - USE INV-DED-AMT FOR OVERPAYMENT                    **
** 10/04/99 RCC - USE DED-AMT(*) FOR OVERPAYMENT                     **
** 09/13/09 JHH - ADD IA DEATH LUMP SUM TOTALS                       **
* 4/2017     : JJG - PIN EXPANSION RESTOW                            **
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp191 extends BLNatBase
{
    // Data Areas
    private LdaFcpl190a ldaFcpl190a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Totals;
    private DbsField pnd_Totals_Pnd_Gross_Amt;
    private DbsField pnd_Totals_Pnd_Dpi_Amt;
    private DbsField pnd_Totals_Pnd_Process_Chg;
    private DbsField pnd_Totals_Pnd_Fed_Ded;
    private DbsField pnd_Totals_Pnd_State_Ded;
    private DbsField pnd_Totals_Pnd_Local_Ded;
    private DbsField pnd_Totals_Pnd_Vlntry_Ded;
    private DbsField pnd_Totals_Pnd_Net_Amt;
    private DbsField pnd_Totals_Pnd_Pymnt_Rqsts;
    private DbsField pnd_System;
    private DbsField i;
    private DbsField pnd_X;
    private DbsField pnd_Read_Cnt;

    private DbsGroup pnd_Prev_Ext;
    private DbsField pnd_Prev_Ext_Cntrct_Orgn_Cde;
    private DbsField pnd_Prev_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Prev_Ext_Cntrct_Type_Cde;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);

        // Local Variables
        localVariables = new DbsRecord();

        pnd_Totals = localVariables.newGroupArrayInRecord("pnd_Totals", "#TOTALS", new DbsArrayController(1, 3));
        pnd_Totals_Pnd_Gross_Amt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Dpi_Amt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Dpi_Amt", "#DPI-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Process_Chg = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Process_Chg", "#PROCESS-CHG", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Fed_Ded = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Fed_Ded", "#FED-DED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_State_Ded = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_State_Ded", "#STATE-DED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Local_Ded = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Local_Ded", "#LOCAL-DED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Vlntry_Ded = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Vlntry_Ded", "#VLNTRY-DED", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Net_Amt = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Totals_Pnd_Pymnt_Rqsts = pnd_Totals.newFieldInGroup("pnd_Totals_Pnd_Pymnt_Rqsts", "#PYMNT-RQSTS", FieldType.NUMERIC, 9);
        pnd_System = localVariables.newFieldInRecord("pnd_System", "#SYSTEM", FieldType.STRING, 16);
        i = localVariables.newFieldInRecord("i", "I", FieldType.NUMERIC, 2);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.PACKED_DECIMAL, 3);
        pnd_Read_Cnt = localVariables.newFieldInRecord("pnd_Read_Cnt", "#READ-CNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Prev_Ext = localVariables.newGroupInRecord("pnd_Prev_Ext", "#PREV-EXT");
        pnd_Prev_Ext_Cntrct_Orgn_Cde = pnd_Prev_Ext.newFieldInGroup("pnd_Prev_Ext_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Prev_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Prev_Ext.newFieldInGroup("pnd_Prev_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Prev_Ext_Cntrct_Type_Cde = pnd_Prev_Ext.newFieldInGroup("pnd_Prev_Ext_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", FieldType.STRING, 2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();

        localVariables.reset();
        pnd_Totals_Pnd_Gross_Amt.getValue(1).setInitialValue(0);
        pnd_Totals_Pnd_Dpi_Amt.getValue(1).setInitialValue(0);
        pnd_Totals_Pnd_Process_Chg.getValue(1).setInitialValue(0);
        pnd_Totals_Pnd_Fed_Ded.getValue(1).setInitialValue(0);
        pnd_Totals_Pnd_State_Ded.getValue(1).setInitialValue(0);
        pnd_Totals_Pnd_Local_Ded.getValue(1).setInitialValue(0);
        pnd_Totals_Pnd_Vlntry_Ded.getValue(1).setInitialValue(0);
        pnd_Totals_Pnd_Net_Amt.getValue(1).setInitialValue(0);
        pnd_Totals_Pnd_Pymnt_Rqsts.getValue(1).setInitialValue(0);
        pnd_System.setInitialValue(" ");
        i.setInitialValue(0);
        pnd_X.setInitialValue(0);
        pnd_Read_Cnt.setInitialValue(0);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp191() throws Exception
    {
        super("Fcpp191");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  -----------------------------------------------
        getReports().definePrinter(2, "'CMPRT01'");                                                                                                                       //Natural: DEFINE PRINTER ( 1 ) OUTPUT 'CMPRT01'
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        //*  --------------------------                                                                                                                                   //Natural: AT TOP OF PAGE ( 1 )
        READWORK01:                                                                                                                                                       //Natural: READ WORK 3 #RPT-EXT
        while (condition(getWorkFiles().read(3, ldaFcpl190a.getPnd_Rpt_Ext())))
        {
            //*  05-18-98
            if (condition(!(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().notEquals(8))))                                                                           //Natural: ACCEPT IF #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND NE 8
            {
                continue;
            }
            if (condition(pnd_Read_Cnt.equals(getZero())))                                                                                                                //Natural: IF #READ-CNT = 0
            {
                pnd_Prev_Ext.setValuesByName(ldaFcpl190a.getPnd_Rpt_Ext());                                                                                               //Natural: MOVE BY NAME #RPT-EXT TO #PREV-EXT
            }                                                                                                                                                             //Natural: END-IF
            pnd_Read_Cnt.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #READ-CNT
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().notEquals(pnd_Prev_Ext_Cntrct_Orgn_Cde)) || (ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals(pnd_Prev_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde)))) //Natural: IF ( #RPT-EXT.CNTRCT-ORGN-CDE NOT = #PREV-EXT.CNTRCT-ORGN-CDE ) OR ( #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NOT = #PREV-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE )
            {
                                                                                                                                                                          //Natural: PERFORM SYSTEM-EVALUATION
                sub_System_Evaluation();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PRINT-SYSTEM-TOTALS
                sub_Print_System_Totals();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Prev_Ext.setValuesByName(ldaFcpl190a.getPnd_Rpt_Ext());                                                                                               //Natural: MOVE BY NAME #RPT-EXT TO #PREV-EXT
            }                                                                                                                                                             //Natural: END-IF
            //*  --------------------------------------------
            //*  IA DEATH AND
            //*  LUMP SUM
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") && ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde().equals("L ")))                        //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' AND #RPT-EXT.CNTRCT-TYPE-CDE = 'L '
            {
                pnd_X.setValue(2);                                                                                                                                        //Natural: ASSIGN #X := 2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_X.setValue(1);                                                                                                                                        //Natural: ASSIGN #X := 1
            }                                                                                                                                                             //Natural: END-IF
            FOR01:                                                                                                                                                        //Natural: FOR I = 1 TO #RPT-EXT.C-INV-ACCT
            for (i.setValue(1); condition(i.lessOrEqual(ldaFcpl190a.getPnd_Rpt_Ext_C_Inv_Acct())); i.nadd(1))
            {
                if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("DC") || ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("MS") ||                   //Natural: IF #RPT-EXT.CNTRCT-ORGN-CDE = 'DC' OR = 'MS' OR = 'IA'
                    ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde().equals("IA")))
                {
                    pnd_Totals_Pnd_Gross_Amt.getValue(pnd_X).compute(new ComputeParameters(false, pnd_Totals_Pnd_Gross_Amt.getValue(pnd_X)), pnd_Totals_Pnd_Gross_Amt.getValue(pnd_X).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(i)).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dvdnd_Amt().getValue(i))); //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( I ) #RPT-EXT.INV-ACCT-DVDND-AMT ( I ) TO #GROSS-AMT ( #X )
                    pnd_Totals_Pnd_Dpi_Amt.getValue(pnd_X).compute(new ComputeParameters(false, pnd_Totals_Pnd_Dpi_Amt.getValue(pnd_X)), pnd_Totals_Pnd_Dpi_Amt.getValue(pnd_X).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dci_Amt().getValue(i)).add(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(i))); //Natural: ADD #RPT-EXT.INV-ACCT-DCI-AMT ( I ) #RPT-EXT.INV-ACCT-DPI-AMT ( I ) TO #DPI-AMT ( #X )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Totals_Pnd_Gross_Amt.getValue(pnd_X).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Settl_Amt().getValue(i));                                           //Natural: ADD #RPT-EXT.INV-ACCT-SETTL-AMT ( I ) TO #GROSS-AMT ( #X )
                    pnd_Totals_Pnd_Dpi_Amt.getValue(pnd_X).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Dpi_Amt().getValue(i));                                               //Natural: ADD #RPT-EXT.INV-ACCT-DPI-AMT ( I ) TO #DPI-AMT ( #X )
                }                                                                                                                                                         //Natural: END-IF
                pnd_Totals_Pnd_Process_Chg.getValue(pnd_X).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Exp_Amt().getValue(i));                                               //Natural: ADD #RPT-EXT.INV-ACCT-EXP-AMT ( I ) TO #PROCESS-CHG ( #X )
                pnd_Totals_Pnd_Fed_Ded.getValue(pnd_X).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Fdrl_Tax_Amt().getValue(i));                                              //Natural: ADD #RPT-EXT.INV-ACCT-FDRL-TAX-AMT ( I ) TO #FED-DED ( #X )
                pnd_Totals_Pnd_State_Ded.getValue(pnd_X).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_State_Tax_Amt().getValue(i));                                           //Natural: ADD #RPT-EXT.INV-ACCT-STATE-TAX-AMT ( I ) TO #STATE-DED ( #X )
                pnd_Totals_Pnd_Local_Ded.getValue(pnd_X).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Local_Tax_Amt().getValue(i));                                           //Natural: ADD #RPT-EXT.INV-ACCT-LOCAL-TAX-AMT ( I ) TO #LOCAL-DED ( #X )
                pnd_Totals_Pnd_Net_Amt.getValue(pnd_X).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Inv_Acct_Net_Pymnt_Amt().getValue(i));                                             //Natural: ADD #RPT-EXT.INV-ACCT-NET-PYMNT-AMT ( I ) TO #NET-AMT ( #X )
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  ---------------------------
            //*    ROXAN  6/2/99
            if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions().notEquals(getZero())))                                                                         //Natural: IF #RPT-EXT.#CNTR-DEDUCTIONS NE 0
            {
                pnd_Totals_Pnd_Vlntry_Ded.getValue(pnd_X).nadd(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Ded_Amt().getValue(1,":",ldaFcpl190a.getPnd_Rpt_Ext_Pnd_Cntr_Deductions())); //Natural: ADD #RPT-EXT.PYMNT-DED-AMT ( 1:#CNTR-DEDUCTIONS ) TO #VLNTRY-DED ( #X )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Totals_Pnd_Pymnt_Rqsts.getValue(pnd_X).nadd(1);                                                                                                           //Natural: ADD 1 TO #PYMNT-RQSTS ( #X )
            //*  --------------------------------------------
            //*                                                                                                                                                           //Natural: AT END OF DATA
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM SYSTEM-EVALUATION
            sub_System_Evaluation();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-SYSTEM-TOTALS
            sub_Print_System_Totals();
            if (condition(Global.isEscape())) {return;}
            pnd_System.setValue("GRAND TOTAL     ");                                                                                                                      //Natural: ASSIGN #SYSTEM := 'GRAND TOTAL     '
            pnd_X.setValue(3);                                                                                                                                            //Natural: ASSIGN #X := 3
            getReports().skip(1, 2);                                                                                                                                      //Natural: SKIP ( 1 ) 2
            getReports().write(1, ReportOption.NOTITLE,pnd_System,new ColumnSpacing(4),pnd_Totals_Pnd_Gross_Amt.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new  //Natural: WRITE ( 1 ) NOTITLE #SYSTEM 4X #GROSS-AMT ( #X ) 2X #DPI-AMT ( #X ) 2X #PROCESS-CHG ( #X ) 2X #FED-DED ( #X ) 2X #STATE-DED ( #X ) 2X #LOCAL-DED ( #X ) 2X #VLNTRY-DED ( #X ) 2X #NET-AMT ( #X ) / 7X #PYMNT-RQSTS ( #X )
                ColumnSpacing(2),pnd_Totals_Pnd_Dpi_Amt.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Process_Chg.getValue(pnd_X), 
                new ReportEditMask ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Fed_Ded.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new 
                ColumnSpacing(2),pnd_Totals_Pnd_State_Ded.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Local_Ded.getValue(pnd_X), 
                new ReportEditMask ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Vlntry_Ded.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new 
                ColumnSpacing(2),pnd_Totals_Pnd_Net_Amt.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),NEWLINE,new ColumnSpacing(7),pnd_Totals_Pnd_Pymnt_Rqsts.getValue(pnd_X), 
                new ReportEditMask ("ZZZZZZZZ9"));
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SYSTEM-EVALUATION
        //* *    #SYSTEM := 'IA DEATH REDRAWS'
        //* *    #SYSTEM := 'IA DEATH        '
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-SYSTEM-TOTALS
        //* ***********************************************************************
    }
    private void sub_System_Evaluation() throws Exception                                                                                                                 //Natural: SYSTEM-EVALUATION
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_System.reset();                                                                                                                                               //Natural: RESET #SYSTEM
        short decideConditionsMet308 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE #PREV-EXT.CNTRCT-ORGN-CDE;//Natural: VALUE 'IA'
        if (condition((pnd_Prev_Ext_Cntrct_Orgn_Cde.equals("IA"))))
        {
            decideConditionsMet308++;
            pnd_System.setValue("IA REDRAWS      ");                                                                                                                      //Natural: ASSIGN #SYSTEM := 'IA REDRAWS      '
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((pnd_Prev_Ext_Cntrct_Orgn_Cde.equals("DC"))))
        {
            decideConditionsMet308++;
            if (condition(pnd_Prev_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                        //Natural: IF #PREV-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                pnd_System.setValue("DEATH PP REDRAWS");                                                                                                                  //Natural: ASSIGN #SYSTEM := 'DEATH PP REDRAWS'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_System.setValue("DEATH PP        ");                                                                                                                  //Natural: ASSIGN #SYSTEM := 'DEATH PP        '
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'DS'
        else if (condition((pnd_Prev_Ext_Cntrct_Orgn_Cde.equals("DS"))))
        {
            decideConditionsMet308++;
            pnd_System.setValue("DS REDRAWS      ");                                                                                                                      //Natural: ASSIGN #SYSTEM := 'DS REDRAWS      '
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((pnd_Prev_Ext_Cntrct_Orgn_Cde.equals("MS"))))
        {
            decideConditionsMet308++;
            pnd_System.setValue("MS REDRAWS      ");                                                                                                                      //Natural: ASSIGN #SYSTEM := 'MS REDRAWS      '
        }                                                                                                                                                                 //Natural: VALUE 'SS'
        else if (condition((pnd_Prev_Ext_Cntrct_Orgn_Cde.equals("SS"))))
        {
            decideConditionsMet308++;
            //*  MDO
            if (condition(pnd_Prev_Ext_Cntrct_Type_Cde.equals("30") || pnd_Prev_Ext_Cntrct_Type_Cde.equals("31")))                                                        //Natural: IF #PREV-EXT.CNTRCT-TYPE-CDE = '30' OR = '31'
            {
                pnd_System.setValue("MDO             ");                                                                                                                  //Natural: ASSIGN #SYSTEM := 'MDO             '
                //*  SINGLE SUM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(DbsUtil.maskMatches(pnd_Prev_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde,"'R'.")))                                                                    //Natural: IF #PREV-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = MASK ( 'R'. )
                {
                    pnd_System.setValue("SINGLE SUM REDRW");                                                                                                              //Natural: ASSIGN #SYSTEM := 'SINGLE SUM REDRW'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_System.setValue("SINGLE SUM      ");                                                                                                              //Natural: ASSIGN #SYSTEM := 'SINGLE SUM      '
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_System.setValue(DbsUtil.compress("INVALID TYPE:", pnd_Prev_Ext_Cntrct_Orgn_Cde));                                                                         //Natural: COMPRESS 'INVALID TYPE:' #PREV-EXT.CNTRCT-ORGN-CDE INTO #SYSTEM
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Print_System_Totals() throws Exception                                                                                                               //Natural: PRINT-SYSTEM-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #X = 1 TO 2
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(2)); pnd_X.nadd(1))
        {
            if (condition(pnd_Totals_Pnd_Pymnt_Rqsts.getValue(pnd_X).greater(getZero())))                                                                                 //Natural: IF #PYMNT-RQSTS ( #X ) > 0
            {
                getReports().skip(1, 2);                                                                                                                                  //Natural: SKIP ( 1 ) 2
                //*  LUMP SUM
                if (condition(pnd_X.equals(2)))                                                                                                                           //Natural: IF #X = 2
                {
                    setValueToSubstring("LS",pnd_System,7,2);                                                                                                             //Natural: MOVE 'LS' TO SUBSTRING ( #SYSTEM,7,2 )
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE,pnd_System,new ColumnSpacing(4),pnd_Totals_Pnd_Gross_Amt.getValue(pnd_X), new ReportEditMask                   //Natural: WRITE ( 1 ) NOTITLE #SYSTEM 4X #GROSS-AMT ( #X ) 2X #DPI-AMT ( #X ) 2X #PROCESS-CHG ( #X ) 2X #FED-DED ( #X ) 2X #STATE-DED ( #X ) 2X #LOCAL-DED ( #X ) 2X #VLNTRY-DED ( #X ) 2X #NET-AMT ( #X ) / 7X #PYMNT-RQSTS ( #X )
                    ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Dpi_Amt.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Process_Chg.getValue(pnd_X), 
                    new ReportEditMask ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Fed_Ded.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new 
                    ColumnSpacing(2),pnd_Totals_Pnd_State_Ded.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Local_Ded.getValue(pnd_X), 
                    new ReportEditMask ("ZZZZZZZ99.99"),new ColumnSpacing(2),pnd_Totals_Pnd_Vlntry_Ded.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),new 
                    ColumnSpacing(2),pnd_Totals_Pnd_Net_Amt.getValue(pnd_X), new ReportEditMask ("ZZZZZZZ99.99"),NEWLINE,new ColumnSpacing(7),pnd_Totals_Pnd_Pymnt_Rqsts.getValue(pnd_X), 
                    new ReportEditMask ("ZZZZZZZZ9"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pnd_Totals_Pnd_Pymnt_Rqsts.getValue(3).nadd(pnd_Totals_Pnd_Pymnt_Rqsts.getValue(pnd_X));                                                                  //Natural: ADD #PYMNT-RQSTS ( #X ) TO #PYMNT-RQSTS ( 3 )
                pnd_Totals_Pnd_Gross_Amt.getValue(3).nadd(pnd_Totals_Pnd_Gross_Amt.getValue(pnd_X));                                                                      //Natural: ADD #GROSS-AMT ( #X ) TO #GROSS-AMT ( 3 )
                pnd_Totals_Pnd_Process_Chg.getValue(3).nadd(pnd_Totals_Pnd_Process_Chg.getValue(pnd_X));                                                                  //Natural: ADD #PROCESS-CHG ( #X ) TO #PROCESS-CHG ( 3 )
                pnd_Totals_Pnd_Dpi_Amt.getValue(3).nadd(pnd_Totals_Pnd_Dpi_Amt.getValue(pnd_X));                                                                          //Natural: ADD #DPI-AMT ( #X ) TO #DPI-AMT ( 3 )
                pnd_Totals_Pnd_Fed_Ded.getValue(3).nadd(pnd_Totals_Pnd_Fed_Ded.getValue(pnd_X));                                                                          //Natural: ADD #FED-DED ( #X ) TO #FED-DED ( 3 )
                pnd_Totals_Pnd_State_Ded.getValue(3).nadd(pnd_Totals_Pnd_State_Ded.getValue(pnd_X));                                                                      //Natural: ADD #STATE-DED ( #X ) TO #STATE-DED ( 3 )
                pnd_Totals_Pnd_Local_Ded.getValue(3).nadd(pnd_Totals_Pnd_Local_Ded.getValue(pnd_X));                                                                      //Natural: ADD #LOCAL-DED ( #X ) TO #LOCAL-DED ( 3 )
                pnd_Totals_Pnd_Vlntry_Ded.getValue(3).nadd(pnd_Totals_Pnd_Vlntry_Ded.getValue(pnd_X));                                                                    //Natural: ADD #VLNTRY-DED ( #X ) TO #VLNTRY-DED ( 3 )
                pnd_Totals_Pnd_Net_Amt.getValue(3).nadd(pnd_Totals_Pnd_Net_Amt.getValue(pnd_X));                                                                          //Natural: ADD #NET-AMT ( #X ) TO #NET-AMT ( 3 )
                pnd_Totals_Pnd_Pymnt_Rqsts.getValue(pnd_X).reset();                                                                                                       //Natural: RESET #PYMNT-RQSTS ( #X ) #GROSS-AMT ( #X ) #PROCESS-CHG ( #X ) #DPI-AMT ( #X ) #FED-DED ( #X ) #STATE-DED ( #X ) #LOCAL-DED ( #X ) #VLNTRY-DED ( #X ) #NET-AMT ( #X )
                pnd_Totals_Pnd_Gross_Amt.getValue(pnd_X).reset();
                pnd_Totals_Pnd_Process_Chg.getValue(pnd_X).reset();
                pnd_Totals_Pnd_Dpi_Amt.getValue(pnd_X).reset();
                pnd_Totals_Pnd_Fed_Ded.getValue(pnd_X).reset();
                pnd_Totals_Pnd_State_Ded.getValue(pnd_X).reset();
                pnd_Totals_Pnd_Local_Ded.getValue(pnd_X).reset();
                pnd_Totals_Pnd_Vlntry_Ded.getValue(pnd_X).reset();
                pnd_Totals_Pnd_Net_Amt.getValue(pnd_X).reset();
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(41),"           CONSOLIDATED PAYMENT SYSTEM",new  //Natural: WRITE ( 1 ) NOTITLE *INIT-USER '-' *PROGRAM 41T '           CONSOLIDATED PAYMENT SYSTEM' 119T *DATX ( EM = LLL' 'DD', 'YYYY ) / 41T '          ON-LINE PAYMENT SUMMARY REPORT' 122T *TIMX ( EM = HH':'II' 'AP ) / / 24T 'SETTLEMENT ' 38T 'ADDITIONAL                  *********   TAX DEDUCTIONS  ' '*********                   PAYMENT' / 3T 'SYSTEM / QTY           AMOUNT       INTEREST     EXP CHARGE' 5X ' FEDERAL         STATE         LOCAL     OTHER DED.' 4X 'NET AMOUNT' / '----------------     ------------    ---------- ' 2X ' ----------    ----------    ----------    ---------- ' 2X ' ----------    ---------- '
                        TabSetting(119),Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YYYY"),NEWLINE,new TabSetting(41),"          ON-LINE PAYMENT SUMMARY REPORT",new 
                        TabSetting(122),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE,NEWLINE,new TabSetting(24),"SETTLEMENT ",new TabSetting(38),"ADDITIONAL                  *********   TAX DEDUCTIONS  ","*********                   PAYMENT",NEWLINE,new 
                        TabSetting(3),"SYSTEM / QTY           AMOUNT       INTEREST     EXP CHARGE",new ColumnSpacing(5)," FEDERAL         STATE         LOCAL     OTHER DED.",new 
                        ColumnSpacing(4),"NET AMOUNT",NEWLINE,"----------------     ------------    ---------- ",new ColumnSpacing(2)," ----------    ----------    ----------    ---------- ",new 
                        ColumnSpacing(2)," ----------    ---------- ");
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
