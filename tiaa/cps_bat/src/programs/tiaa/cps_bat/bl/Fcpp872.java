/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:59 PM
**        * FROM NATURAL PROGRAM : Fcpp872
************************************************************
**        * FILE NAME            : Fcpp872.java
**        * CLASS NAME           : Fcpp872
**        * INSTANCE NAME        : Fcpp872
************************************************************
************************************************************************
* PROGRAM  : FCPP872
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS NEW ANNUITIZATION PAYMENT WARRANT REPORT
* CREATED  : 08/24/94
* FUNCTION : THIS PROGRAM READS SORTED EXTRACT PRODUCED BY FCPP871
*            AND PRODUCES PAYMENT WARRANT REPORT.
*
* 06/25/1998 - RIAD LOUTFI - PROGRAM CHANGED TO PROCESS THE "NZ" IRA
*              ROLLOVER WARRANTS.
* 09/30/1998 - RIAD LOUTFI - CHANGED TO PROCESS ANNUITY LOAN "AL"
*              WARRANTS.
* 03/17/1999 - RITA SALGADO
*              FOR 'AL' RESET TAXABLE AMOUNTS TO 0 AND
*              EFFECTIVE DATE IS SAME AS SETTLEMENT DATE
* 12/08/99   - ROXAN C. CARREON
*              FCPAEXT REPLACED FCPA371
* 08/01/01   - A. REYHANIAN
*              READ PYMNT-TAX TO DISPLAY TAX WITHHOLDING INFORMATION
*
* 03/07/2006 : LANDRUM  PAYEE MATCH
*              - INCREASED CHECK NBR TO N10.
*  4/2017    : JJG - PIN EXPANSION CHANGES
*  5/2018    : PALDE - INCREASE OF LINE SIZD FOR OUTPUT REPORT
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp872 extends BLNatBase
{
    // Data Areas
    private PdaTbldcoda pdaTbldcoda;
    private LdaFcpltbcd ldaFcpltbcd;
    private PdaFcpamode pdaFcpamode;
    private PdaFcpaoptn pdaFcpaoptn;
    private PdaFcpa121 pdaFcpa121;
    private PdaFcpa199a pdaFcpa199a;
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl199b ldaFcpl199b;
    private LdaFcplptax ldaFcplptax;
    private PdaTxwa2430 pdaTxwa2430;
    private LdaFcpl871 ldaFcpl871;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Check_Number_A11;

    private DbsGroup pnd_Check_Number_A11__R_Field_1;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Letter_A1;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_A11__R_Field_2;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Number_N7;

    private DbsGroup pnd_Check_Number_A11__R_Field_3;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Number_A10;

    private DbsGroup pnd_Key_Detail;
    private DbsField pnd_Key_Detail_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Detail_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Key_Detail_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Key_Detail_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Key_Detail_Cntrct_Payee_Cde;
    private DbsField pnd_Key_Detail_Pymnt_Check_Dte;
    private DbsField pnd_Key_Detail_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Key_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde;

    private DbsGroup pnd_Key_Detail__R_Field_4;
    private DbsField pnd_Key_Detail_Pnd_Key;

    private DbsGroup pnd_Key_Efm;
    private DbsField pnd_Key_Efm_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Efm_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Program;
    private DbsField pnd_Ws_Pnd_Run_Time;
    private DbsField pnd_Ws_Pnd_Timestmp;

    private DbsGroup pnd_Ws__R_Field_5;
    private DbsField pnd_Ws_Pnd_Timestmp_A;
    private DbsField pnd_Ws_Pnd_Page_Number;
    private DbsField pnd_Ws_Pnd_Tax_Fed_C_Filing_Stat_Disp;
    private DbsField pnd_Ws_Pnd_Ctznshp_Display;
    private DbsField pnd_Ws_Pnd_State_Disp_X;
    private DbsField pnd_Ws_Pnd_State_Disp_Desc;
    private DbsField pnd_Ws_Pnd_Option_Display;
    private DbsField pnd_Ws_Pnd_Ws_Mode;
    private DbsField pnd_Ws_Pnd_Option;
    private DbsField pnd_Ws_Pnd_Mode_Display;
    private DbsField pnd_Ws_Pnd_Csr_Display;
    private DbsField pnd_Ws_Pnd_Csr_Array;
    private DbsField pnd_Ws_Pnd_Pymnt_Display;
    private DbsField pnd_Ws_Pnd_Pymnt_Array;
    private DbsField pnd_Ws_Pnd_Fund_Display;
    private DbsField pnd_Ws_Pnd_Fund_Array;
    private DbsField pnd_Ws_Pnd_Fund_Array_Idx;
    private DbsField pnd_Ws_Pnd_Fund_Idx;
    private DbsField pnd_Ws_Pnd_Gross_Amt_Array;
    private DbsField pnd_Ws_Pnd_Gross_Amt_Disp;
    private DbsField pnd_Ws_Pnd_Fund_Sub;
    private DbsField pnd_Ws_Pnd_Fund_Cde;
    private DbsField pnd_Ws_Pnd_Csr_Idx;
    private DbsField pnd_Ws_Pnd_Gross_Idx;
    private DbsField pnd_Ws_Pnd_Disp_Idx;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Records_Read;
    private DbsField pnd_Ws_Pnd_Line_Count;
    private DbsField pnd_Ws_Pnd_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Pnd_Effective_Dte;
    private DbsField pnd_Ws_Pnd_Ivc_Ind;
    private DbsField pnd_Ws_Pnd_Annuity_Certain;
    private DbsField pnd_Ws_Pnd_First_Periodic;

    private DbsGroup pnd_Ws_Pnd_Eft_Line;

    private DbsGroup pnd_Ws_Pnd_Eft_Line_Cvs;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text;
    private DbsField pnd_Ws_Pnd_Fed_Display;

    private DbsGroup pnd_Ws_Pnd_Inv_Acct;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Txble_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_State_Tax_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Total_Tax_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cde_Desc;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Gross_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Txble_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_State_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Local_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Total_Tax;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Net_Pymnt;

    private DbsGroup pnd_Ws_Pnd_Prev_Ledger_Group;
    private DbsField pnd_Ws_Pnd_Prev_Ledger;
    private DbsField pnd_Ws_Pnd_Prev_Ledger_Ind;
    private DbsField pnd_Ws_Pnd_Prev_Ledger_Hash;

    private DbsGroup pnd_Ws__R_Field_6;
    private DbsField pnd_Ws_Pnd_Prev_L_Hash;
    private DbsField pnd_Ws_Pnd_L_Idx;

    private DbsGroup pnd_Ws_Pnd_Ledger_Disp_Array;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Disp_Ledger_Nbr;
    private DbsField pnd_Ws_Pnd_Disp_Ledger_Isa;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc;

    private DbsGroup pnd_Ws_Pnd_Rtb_Tax_Info;
    private DbsField pnd_Ws_Pnd_Internal_Rollover;
    private DbsField pnd_Ws_Pnd_Rollover_Ind;
    private DbsField pnd_Ws_Pnd_20_Per_Withold;
    private DbsField pnd_Ws_Pnd_Rollover_Ind_Cv;
    private DbsField pnd_Ws_Pnd_Rtb_Percent_Cv;
    private DbsField pnd_Ws_Pnd_20_Per_Withold_Cv;
    private DbsField pnd_Ws_Pnd_Rtb_Percent;
    private DbsField pnd_Ws_Pnd_Temp_Amt;

    private DbsGroup pnd_Pmnt_Tax;
    private DbsField pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc;
    private DbsField pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc;
    private DbsField pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc;
    private DbsField pnd_Ws_Display;
    private DbsField pnd_Ws_Display2;
    private DbsField pnd_Screen_Header;
    private DbsField pnd_Screen_Header2;
    private DbsField pnd_Screen_Trailer;
    private DbsField pnd_Input_Parm_Orgn_Cde;
    private DbsField pnd_Ia_Da_Contract;
    private DbsField rtb;
    private DbsField ivc_From_Rtb_Rollover;
    private DbsField pnd_Ws_Pymnt_Gross_Amt;
    private DbsField pnd_Xx;
    private DbsField pnd_W_Fld;
    private DbsField pnd_Ws_Pay_Settl_Type;

    private DbsGroup pnd_Ws_Pay_Settl_Type__R_Field_7;
    private DbsField pnd_Ws_Pay_Settl_Type_Pnd_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Pay_Settl_Type_Pnd_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_X;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst;

    private DbsGroup pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_8;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Invrse_Dte;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Orgn_Cde;
    private DbsField pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Pymnt_Prcss_Seq_Nbr;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_KeyOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTbldcoda = new PdaTbldcoda(localVariables);
        ldaFcpltbcd = new LdaFcpltbcd();
        registerRecord(ldaFcpltbcd);
        pdaFcpamode = new PdaFcpamode(localVariables);
        pdaFcpaoptn = new PdaFcpaoptn(localVariables);
        pdaFcpa121 = new PdaFcpa121(localVariables);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);
        ldaFcplptax = new LdaFcplptax();
        registerRecord(ldaFcplptax);
        registerRecord(ldaFcplptax.getVw_pymnt_Tax());
        pdaTxwa2430 = new PdaTxwa2430(localVariables);
        ldaFcpl871 = new LdaFcpl871();
        registerRecord(ldaFcpl871);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Check_Number_A11 = localVariables.newFieldInRecord("pnd_Check_Number_A11", "#CHECK-NUMBER-A11", FieldType.STRING, 11);

        pnd_Check_Number_A11__R_Field_1 = localVariables.newGroupInRecord("pnd_Check_Number_A11__R_Field_1", "REDEFINE", pnd_Check_Number_A11);
        pnd_Check_Number_A11_Pnd_Check_Letter_A1 = pnd_Check_Number_A11__R_Field_1.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Letter_A1", "#CHECK-LETTER-A1", 
            FieldType.STRING, 1);
        pnd_Check_Number_A11_Pnd_Check_Number_N10 = pnd_Check_Number_A11__R_Field_1.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Number_N10", "#CHECK-NUMBER-N10", 
            FieldType.NUMERIC, 10);

        pnd_Check_Number_A11__R_Field_2 = pnd_Check_Number_A11__R_Field_1.newGroupInGroup("pnd_Check_Number_A11__R_Field_2", "REDEFINE", pnd_Check_Number_A11_Pnd_Check_Number_N10);
        pnd_Check_Number_A11_Pnd_Check_Number_N3 = pnd_Check_Number_A11__R_Field_2.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_A11_Pnd_Check_Number_N7 = pnd_Check_Number_A11__R_Field_2.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        pnd_Check_Number_A11__R_Field_3 = pnd_Check_Number_A11__R_Field_1.newGroupInGroup("pnd_Check_Number_A11__R_Field_3", "REDEFINE", pnd_Check_Number_A11_Pnd_Check_Number_N10);
        pnd_Check_Number_A11_Pnd_Check_Number_A10 = pnd_Check_Number_A11__R_Field_3.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Number_A10", "#CHECK-NUMBER-A10", 
            FieldType.STRING, 10);

        pnd_Key_Detail = localVariables.newGroupInRecord("pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Detail_Cntrct_Orgn_Cde = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Detail_Cntrct_Unq_Id_Nbr = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Key_Detail_Pymnt_Reqst_Log_Dte_Time = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Key_Detail_Cntrct_Ppcn_Nbr = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Key_Detail_Cntrct_Payee_Cde = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Key_Detail_Pymnt_Check_Dte = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Key_Detail_Pymnt_Prcss_Seq_Num = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Key_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Key_Detail.newFieldInGroup("pnd_Key_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);

        pnd_Key_Detail__R_Field_4 = localVariables.newGroupInRecord("pnd_Key_Detail__R_Field_4", "REDEFINE", pnd_Key_Detail);
        pnd_Key_Detail_Pnd_Key = pnd_Key_Detail__R_Field_4.newFieldInGroup("pnd_Key_Detail_Pnd_Key", "#KEY", FieldType.STRING, 56);

        pnd_Key_Efm = localVariables.newGroupInRecord("pnd_Key_Efm", "#KEY-EFM");
        pnd_Key_Efm_Cntrct_Orgn_Cde = pnd_Key_Efm.newFieldInGroup("pnd_Key_Efm_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Efm_Cntrct_Unq_Id_Nbr = pnd_Key_Efm.newFieldInGroup("pnd_Key_Efm_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time = pnd_Key_Efm.newFieldInGroup("pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time", "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 
            15);
        pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Key_Efm.newFieldInGroup("pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde", "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Program = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Ws_Pnd_Run_Time = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Run_Time", "#RUN-TIME", FieldType.TIME);
        pnd_Ws_Pnd_Timestmp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timestmp", "#TIMESTMP", FieldType.BINARY, 8);

        pnd_Ws__R_Field_5 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_5", "REDEFINE", pnd_Ws_Pnd_Timestmp);
        pnd_Ws_Pnd_Timestmp_A = pnd_Ws__R_Field_5.newFieldInGroup("pnd_Ws_Pnd_Timestmp_A", "#TIMESTMP-A", FieldType.STRING, 8);
        pnd_Ws_Pnd_Page_Number = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_Tax_Fed_C_Filing_Stat_Disp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Tax_Fed_C_Filing_Stat_Disp", "#TAX-FED-C-FILING-STAT-DISP", FieldType.STRING, 
            2);
        pnd_Ws_Pnd_Ctznshp_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ctznshp_Display", "#CTZNSHP-DISPLAY", FieldType.STRING, 9);
        pnd_Ws_Pnd_State_Disp_X = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Disp_X", "#STATE-DISP-X", FieldType.STRING, 2);
        pnd_Ws_Pnd_State_Disp_Desc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Disp_Desc", "#STATE-DISP-DESC", FieldType.STRING, 20);
        pnd_Ws_Pnd_Option_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Option_Display", "#OPTION-DISPLAY", FieldType.STRING, 35);
        pnd_Ws_Pnd_Ws_Mode = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Mode", "#WS-MODE", FieldType.STRING, 5);
        pnd_Ws_Pnd_Option = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Option", "#OPTION", FieldType.STRING, 7);
        pnd_Ws_Pnd_Mode_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mode_Display", "#MODE-DISPLAY", FieldType.STRING, 18);
        pnd_Ws_Pnd_Csr_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Csr_Display", "#CSR-DISPLAY", FieldType.STRING, 9);
        pnd_Ws_Pnd_Csr_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Csr_Array", "#CSR-ARRAY", FieldType.STRING, 9, new DbsArrayController(1, 3));
        pnd_Ws_Pnd_Pymnt_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Display", "#PYMNT-DISPLAY", FieldType.STRING, 17);
        pnd_Ws_Pnd_Pymnt_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Array", "#PYMNT-ARRAY", FieldType.STRING, 17, new DbsArrayController(1, 
            5));
        pnd_Ws_Pnd_Fund_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fund_Display", "#FUND-DISPLAY", FieldType.STRING, 60);
        pnd_Ws_Pnd_Fund_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Fund_Array", "#FUND-ARRAY", FieldType.STRING, 60, new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Fund_Array_Idx = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Fund_Array_Idx", "#FUND-ARRAY-IDX", FieldType.PACKED_DECIMAL, 3, new DbsArrayController(1, 
            3));
        pnd_Ws_Pnd_Fund_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fund_Idx", "#FUND-IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Gross_Amt_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Gross_Amt_Array", "#GROSS-AMT-ARRAY", FieldType.STRING, 19, new DbsArrayController(1, 
            2));
        pnd_Ws_Pnd_Gross_Amt_Disp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Amt_Disp", "#GROSS-AMT-DISP", FieldType.STRING, 19);
        pnd_Ws_Pnd_Fund_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fund_Sub", "#FUND-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Fund_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fund_Cde", "#FUND-CDE", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_Csr_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Csr_Idx", "#CSR-IDX", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Gross_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Gross_Idx", "#GROSS-IDX", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Disp_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Idx", "#DISP-IDX", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Records_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Records_Read", "#RECORDS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Line_Count = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Line_Count", "#LINE-COUNT", FieldType.PACKED_DECIMAL, 2, new DbsArrayController(1, 
            15));
        pnd_Ws_Pnd_Pymnt_Acctg_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Acctg_Dte", "#PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Effective_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Effective_Dte", "#EFFECTIVE-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Ivc_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ivc_Ind", "#IVC-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Annuity_Certain = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Annuity_Certain", "#ANNUITY-CERTAIN", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_First_Periodic = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Periodic", "#FIRST-PERIODIC", FieldType.BOOLEAN, 1);

        pnd_Ws_Pnd_Eft_Line = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Eft_Line", "#EFT-LINE");

        pnd_Ws_Pnd_Eft_Line_Cvs = pnd_Ws_Pnd_Eft_Line.newGroupInGroup("pnd_Ws_Pnd_Eft_Line_Cvs", "#EFT-LINE-CVS");
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv", "#PYMNT-EFT-TRANSIT-ID-TEXT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv", "#PYMNT-EFT-TRANSIT-ID-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv", "#PYMNT-EFT-ACCT-NBR-TEXT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv", "#PYMNT-EFT-ACCT-NBR-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv", "#PYMNT-CHK-SAV-IND-TEXT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv", "#PYMNT-CHK-SAV-IND-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text = pnd_Ws_Pnd_Eft_Line.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text", "#PYMNT-EFT-TRANSIT-ID-TEXT", 
            FieldType.STRING, 16);
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text = pnd_Ws_Pnd_Eft_Line.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text", "#PYMNT-EFT-ACCT-NBR-TEXT", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text = pnd_Ws_Pnd_Eft_Line.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text", "#PYMNT-CHK-SAV-IND-TEXT", FieldType.STRING, 
            17);
        pnd_Ws_Pnd_Fed_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fed_Display", "#FED-DISPLAY", FieldType.STRING, 3);

        pnd_Ws_Pnd_Inv_Acct = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Inv_Acct", "#INV-ACCT", new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv", "#INV-ACCT-CDE-DESC-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv", "#INV-ACCT-GROSS-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv", "#INV-ACCT-SETTL-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv", "#INV-ACCT-CNTRCT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv", "#INV-ACCT-DVDND-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv", "#INV-ACCT-UNIT-QTY-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv", "#INV-ACCT-UNIT-VALUE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_Cv", "#INV-ACCT-DPI-DCI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv", "#INV-ACCT-IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Txble_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Txble_Amt_Cv", "#INV-ACCT-TXBLE-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Cv", "#INV-ACCT-FDRL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_State_Tax_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_State_Tax_Cv", "#INV-ACCT-STATE-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv", "#INV-ACCT-LOCAL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Total_Tax_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Total_Tax_Cv", "#INV-ACCT-TOTAL-TAX-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv", "#INV-ACCT-NET-PYMNT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Cde_Desc", "#INV-ACCT-CDE-DESC", FieldType.STRING, 11);
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Gross_Amt", "#INV-ACCT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt", "#INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Inv_Acct_Txble_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Txble_Amt", "#INV-ACCT-TXBLE-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Qty", "#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Ws_Pnd_Inv_Acct_Unit_Value = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Value", "#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt", "#INV-ACCT-DPI-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt", "#INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax", "#INV-ACCT-FDRL-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_State_Tax = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_State_Tax", "#INV-ACCT-STATE-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Local_Tax = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Local_Tax", "#INV-ACCT-LOCAL-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Total_Tax = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Total_Tax", "#INV-ACCT-TOTAL-TAX", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt", "#INV-ACCT-NET-PYMNT", FieldType.PACKED_DECIMAL, 
            9, 2);

        pnd_Ws_Pnd_Prev_Ledger_Group = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Prev_Ledger_Group", "#PREV-LEDGER-GROUP");
        pnd_Ws_Pnd_Prev_Ledger = pnd_Ws_Pnd_Prev_Ledger_Group.newFieldInGroup("pnd_Ws_Pnd_Prev_Ledger", "#PREV-LEDGER", FieldType.STRING, 15);
        pnd_Ws_Pnd_Prev_Ledger_Ind = pnd_Ws_Pnd_Prev_Ledger_Group.newFieldInGroup("pnd_Ws_Pnd_Prev_Ledger_Ind", "#PREV-LEDGER-IND", FieldType.STRING, 
            1);
        pnd_Ws_Pnd_Prev_Ledger_Hash = pnd_Ws_Pnd_Prev_Ledger_Group.newFieldInGroup("pnd_Ws_Pnd_Prev_Ledger_Hash", "#PREV-LEDGER-HASH", FieldType.STRING, 
            2);

        pnd_Ws__R_Field_6 = pnd_Ws_Pnd_Prev_Ledger_Group.newGroupInGroup("pnd_Ws__R_Field_6", "REDEFINE", pnd_Ws_Pnd_Prev_Ledger_Hash);
        pnd_Ws_Pnd_Prev_L_Hash = pnd_Ws__R_Field_6.newFieldInGroup("pnd_Ws_Pnd_Prev_L_Hash", "#PREV-L-HASH", FieldType.NUMERIC, 2);
        pnd_Ws_Pnd_L_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_L_Idx", "#L-IDX", FieldType.PACKED_DECIMAL, 3);

        pnd_Ws_Pnd_Ledger_Disp_Array = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Ledger_Disp_Array", "#LEDGER-DISP-ARRAY", new DbsArrayController(1, 15));
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_Cv = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_Cv", "#DISP-ACCT-LEDGR-AMT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Disp_Ledger_Nbr = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Ledger_Nbr", "#DISP-LEDGER-NBR", FieldType.STRING, 
            15);
        pnd_Ws_Pnd_Disp_Ledger_Isa = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Ledger_Isa", "#DISP-LEDGER-ISA", FieldType.STRING, 
            5);
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt", "#DISP-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind", "#DISP-ACCT-LEDGR-IND", FieldType.STRING, 
            2);
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc", "#DISP-ACCT-LEDGR-DESC", FieldType.STRING, 
            37);

        pnd_Ws_Pnd_Rtb_Tax_Info = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Rtb_Tax_Info", "#RTB-TAX-INFO");
        pnd_Ws_Pnd_Internal_Rollover = pnd_Ws_Pnd_Rtb_Tax_Info.newFieldInGroup("pnd_Ws_Pnd_Internal_Rollover", "#INTERNAL-ROLLOVER", FieldType.BOOLEAN, 
            1);
        pnd_Ws_Pnd_Rollover_Ind = pnd_Ws_Pnd_Rtb_Tax_Info.newFieldInGroup("pnd_Ws_Pnd_Rollover_Ind", "#ROLLOVER-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_20_Per_Withold = pnd_Ws_Pnd_Rtb_Tax_Info.newFieldInGroup("pnd_Ws_Pnd_20_Per_Withold", "#20-PER-WITHOLD", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Rollover_Ind_Cv = pnd_Ws_Pnd_Rtb_Tax_Info.newFieldInGroup("pnd_Ws_Pnd_Rollover_Ind_Cv", "#ROLLOVER-IND-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Rtb_Percent_Cv = pnd_Ws_Pnd_Rtb_Tax_Info.newFieldInGroup("pnd_Ws_Pnd_Rtb_Percent_Cv", "#RTB-PERCENT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_20_Per_Withold_Cv = pnd_Ws_Pnd_Rtb_Tax_Info.newFieldInGroup("pnd_Ws_Pnd_20_Per_Withold_Cv", "#20-PER-WITHOLD-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Rtb_Percent = pnd_Ws_Pnd_Rtb_Tax_Info.newFieldInGroup("pnd_Ws_Pnd_Rtb_Percent", "#RTB-PERCENT", FieldType.PACKED_DECIMAL, 7, 4);
        pnd_Ws_Pnd_Temp_Amt = pnd_Ws_Pnd_Rtb_Tax_Info.newFieldInGroup("pnd_Ws_Pnd_Temp_Amt", "#TEMP-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Pmnt_Tax = localVariables.newGroupInRecord("pnd_Pmnt_Tax", "#PMNT-TAX");
        pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc = pnd_Pmnt_Tax.newFieldInGroup("pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc", "TAX-FED-FILING-STAT-DESC", FieldType.STRING, 
            3);
        pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc = pnd_Pmnt_Tax.newFieldInGroup("pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc", "TAX-STA-FILING-STAT-DESC", FieldType.STRING, 
            3);
        pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc = pnd_Pmnt_Tax.newFieldInGroup("pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc", "TAX-LOC-FILING-STAT-DESC", FieldType.STRING, 
            3);
        pnd_Ws_Display = localVariables.newFieldInRecord("pnd_Ws_Display", "#WS-DISPLAY", FieldType.STRING, 30);
        pnd_Ws_Display2 = localVariables.newFieldInRecord("pnd_Ws_Display2", "#WS-DISPLAY2", FieldType.STRING, 30);
        pnd_Screen_Header = localVariables.newFieldInRecord("pnd_Screen_Header", "#SCREEN-HEADER", FieldType.STRING, 35);
        pnd_Screen_Header2 = localVariables.newFieldInRecord("pnd_Screen_Header2", "#SCREEN-HEADER2", FieldType.STRING, 60);
        pnd_Screen_Trailer = localVariables.newFieldInRecord("pnd_Screen_Trailer", "#SCREEN-TRAILER", FieldType.STRING, 75);
        pnd_Input_Parm_Orgn_Cde = localVariables.newFieldInRecord("pnd_Input_Parm_Orgn_Cde", "#INPUT-PARM-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ia_Da_Contract = localVariables.newFieldInRecord("pnd_Ia_Da_Contract", "#IA-DA-CONTRACT", FieldType.STRING, 2);
        rtb = localVariables.newFieldInRecord("rtb", "RTB", FieldType.BOOLEAN, 1);
        ivc_From_Rtb_Rollover = localVariables.newFieldInRecord("ivc_From_Rtb_Rollover", "IVC-FROM-RTB-ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Ws_Pymnt_Gross_Amt = localVariables.newFieldInRecord("pnd_Ws_Pymnt_Gross_Amt", "#WS-PYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Xx = localVariables.newFieldInRecord("pnd_Xx", "#XX", FieldType.INTEGER, 2);
        pnd_W_Fld = localVariables.newFieldInRecord("pnd_W_Fld", "#W-FLD", FieldType.STRING, 60);
        pnd_Ws_Pay_Settl_Type = localVariables.newFieldInRecord("pnd_Ws_Pay_Settl_Type", "#WS-PAY-SETTL-TYPE", FieldType.STRING, 2);

        pnd_Ws_Pay_Settl_Type__R_Field_7 = localVariables.newGroupInRecord("pnd_Ws_Pay_Settl_Type__R_Field_7", "REDEFINE", pnd_Ws_Pay_Settl_Type);
        pnd_Ws_Pay_Settl_Type_Pnd_Cntrct_Pymnt_Type_Ind = pnd_Ws_Pay_Settl_Type__R_Field_7.newFieldInGroup("pnd_Ws_Pay_Settl_Type_Pnd_Cntrct_Pymnt_Type_Ind", 
            "#CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Pay_Settl_Type_Pnd_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Pay_Settl_Type__R_Field_7.newFieldInGroup("pnd_Ws_Pay_Settl_Type_Pnd_Cntrct_Sttlmnt_Type_Ind", 
            "#CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_X = localVariables.newFieldInRecord("pnd_X", "#X", FieldType.NUMERIC, 1);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst = localVariables.newFieldInRecord("pnd_Cntrct_Inv_Orgn_Prcss_Inst", "#CNTRCT-INV-ORGN-PRCSS-INST", FieldType.STRING, 
            29);

        pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_8 = localVariables.newGroupInRecord("pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_8", "REDEFINE", pnd_Cntrct_Inv_Orgn_Prcss_Inst);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr = pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_8.newFieldInGroup("pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr", 
            "#X-CNTRCT-NBR", FieldType.STRING, 10);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Invrse_Dte = pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_8.newFieldInGroup("pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Invrse_Dte", 
            "#X-CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Orgn_Cde = pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_8.newFieldInGroup("pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Orgn_Cde", 
            "#X-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Pymnt_Prcss_Seq_Nbr = pnd_Cntrct_Inv_Orgn_Prcss_Inst__R_Field_8.newFieldInGroup("pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Pymnt_Prcss_Seq_Nbr", 
            "#X-PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Key_OLD", "Pnd_Key_OLD", FieldType.STRING, 56);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpltbcd.initializeValues();
        ldaFcpl199b.initializeValues();
        ldaFcplptax.initializeValues();
        ldaFcpl871.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Csr_Array.getValue(1).setInitialValue(" ");
        pnd_Ws_Pnd_Csr_Array.getValue(2).setInitialValue("(REDRAW)");
        pnd_Ws_Pnd_Csr_Array.getValue(3).setInitialValue("(UNKNOWN)");
        pnd_Ws_Pnd_Pymnt_Array.getValue(1).setInitialValue("CHECK");
        pnd_Ws_Pnd_Pymnt_Array.getValue(2).setInitialValue("EFT");
        pnd_Ws_Pnd_Pymnt_Array.getValue(3).setInitialValue("GLOBAL PAY");
        pnd_Ws_Pnd_Pymnt_Array.getValue(4).setInitialValue("CLASSIC          ");
        pnd_Ws_Pnd_Pymnt_Array.getValue(5).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Fund_Array.getValue(0 + 1).setInitialValue("                   Unknown Payment Type");
        pnd_Ws_Pnd_Fund_Array.getValue(1 + 1).setInitialValue("                    RTB Rollover(    )");
        pnd_Ws_Pnd_Fund_Array.getValue(2 + 1).setInitialValue("                  RTB to Annuitant(    )");
        pnd_Ws_Pnd_Fund_Array.getValue(3 + 1).setInitialValue("               RTB to Alternate Payee(    )");
        pnd_Ws_Pnd_Fund_Array.getValue(4 + 1).setInitialValue("                     Periodic Payment");
        pnd_Ws_Pnd_Fund_Array.getValue(5 + 1).setInitialValue("                    IVC from Rollover");
        pnd_Ws_Pnd_Fund_Array.getValue(6 + 1).setInitialValue("                            ");
        pnd_Ws_Pnd_Fund_Array_Idx.getValue(1).setInitialValue(34);
        pnd_Ws_Pnd_Fund_Array_Idx.getValue(2).setInitialValue(36);
        pnd_Ws_Pnd_Fund_Array_Idx.getValue(3).setInitialValue(39);
        pnd_Ws_Pnd_Gross_Amt_Array.getValue(1).setInitialValue("Per Payment Amount:");
        pnd_Ws_Pnd_Gross_Amt_Array.getValue(2).setInitialValue("RTB Amount........:");
        pnd_Ws_Pnd_Gross_Idx.setInitialValue(1);
        pnd_Ws_Pnd_Annuity_Certain.setInitialValue(false);
        pnd_Ws_Pnd_First_Periodic.setInitialValue(true);
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text.setInitialValue("Bank Transit ID:");
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text.setInitialValue("Account:");
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text.setInitialValue("Checking/Savings:");
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-CDE-DESC-CV(2)	(AD=N)#WS.#INV-ACCT-CDE-DESC-CV(3)	(AD=N)#WS.#INV-ACCT-CDE-DESC-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-GROSS-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-GROSS-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-GROSS-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-SETTL-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-SETTL-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-SETTL-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-CNTRCT-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-CNTRCT-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-CNTRCT-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-DVDND-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-DVDND-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-DVDND-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-UNIT-QTY-CV(2)	(AD=N)#WS.#INV-ACCT-UNIT-QTY-CV(3)	(AD=N)#WS.#INV-ACCT-UNIT-QTY-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-UNIT-VALUE-CV(2)	(AD=N)#WS.#INV-ACCT-UNIT-VALUE-CV(3)	(AD=N)#WS.#INV-ACCT-UNIT-VALUE-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-DPI-DCI-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-DPI-DCI-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-DPI-DCI-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-IVC-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-IVC-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-IVC-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Txble_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-TXBLE-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-TXBLE-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-TXBLE-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-FDRL-TAX-CV(2)	(AD=N)#WS.#INV-ACCT-FDRL-TAX-CV(3)	(AD=N)#WS.#INV-ACCT-FDRL-TAX-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_State_Tax_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-STATE-TAX-CV(2)	(AD=N)#WS.#INV-ACCT-STATE-TAX-CV(3)	(AD=N)#WS.#INV-ACCT-STATE-TAX-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-LOCAL-TAX-CV(2)	(AD=N)#WS.#INV-ACCT-LOCAL-TAX-CV(3)	(AD=N)#WS.#INV-ACCT-LOCAL-TAX-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Total_Tax_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-TOTAL-TAX-CV(2)	(AD=N)#WS.#INV-ACCT-TOTAL-TAX-CV(3)	(AD=N)#WS.#INV-ACCT-TOTAL-TAX-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-NET-PYMNT-CV(2)	(AD=N)#WS.#INV-ACCT-NET-PYMNT-CV(3)	(AD=N)#WS.#INV-ACCT-NET-PYMNT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Prev_Ledger.setInitialValue("H'00'");
        pnd_Ws_Pnd_Prev_Ledger_Ind.setInitialValue("H'00'");
        pnd_Ws_Pnd_Prev_Ledger_Hash.setInitialValue("H'00'");
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(2)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(3)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(4)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(5)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(6)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(7)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(8)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(9)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(10)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(11)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(12)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(13)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(14)	(AD=N)#WS.#DISP-ACCT-LEDGR-AMT-CV(15)	(AD=N");
        pnd_Ws_Pnd_Internal_Rollover.setInitialValue(false);
        pnd_Ws_Pnd_Rollover_Ind.setInitialValue(false);
        pnd_Ws_Pnd_20_Per_Withold.setInitialValue(false);
        pnd_Ws_Pnd_Rollover_Ind_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Rtb_Percent_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_20_Per_Withold_Cv.setInitialAttributeValue("AD=N");
        pnd_Screen_Trailer.setInitialValue(" ");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp872() throws Exception
    {
        super("Fcpp872");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp872|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //* PALDE                                                                                                                                                 //Natural: FORMAT ( 00 ) PS = 23 LS = 132 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 80 ZP = ON
                //*                                                                                                                                                       //Natural: FORMAT ( 02 ) PS = 20 LS = 124 ZP = ON
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, new FieldAttributes ("AD=A"),pnd_Input_Parm_Orgn_Cde);                                                 //Natural: INPUT ( AD = A ) #INPUT-PARM-ORGN-CDE
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                pnd_Ws_Pnd_Run_Time.setValue(Global.getTIMX());                                                                                                           //Natural: ASSIGN #RUN-TIME := *TIMX
                pnd_Ws_Pnd_Program.setValue(Global.getPROGRAM());                                                                                                         //Natural: ASSIGN #PROGRAM := *PROGRAM
                pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("T");                                                                                                         //Natural: ASSIGN TBLDCODA.#MODE := 'T'
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
                sub_Get_Check_Formatting_Data();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //* AD  WORK FILE 01  PYMNT-ADDR-INFO  INV-INFO (*)         /* ROXAN 12/8
                boolean endOfDataReadwork01 = true;                                                                                                                       //Natural: READ WORK FILE 01 EXT ( * )
                boolean firstReadwork01 = true;
                READWORK01:
                while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
                {
                    CheckAtStartofData1038();

                    if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventReadwork01();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataReadwork01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //*  ---------                                                                                                                                        //Natural: AT START OF DATA
                    //BEFORE BREAK PROCESSING                                                                                                                             //Natural: BEFORE BREAK PROCESSING
                    //*  -----------------------
                    pnd_Key_Detail_Cntrct_Orgn_Cde.setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                         //Natural: ASSIGN #KEY-DETAIL.CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
                    pnd_Key_Detail_Cntrct_Unq_Id_Nbr.setValue(pdaFcpaext.getExt_Cntrct_Unq_Id_Nbr());                                                                     //Natural: ASSIGN #KEY-DETAIL.CNTRCT-UNQ-ID-NBR := EXT.CNTRCT-UNQ-ID-NBR
                    pnd_Key_Detail_Pymnt_Reqst_Log_Dte_Time.setValue(pdaFcpaext.getExt_Pymnt_Reqst_Log_Dte_Time());                                                       //Natural: ASSIGN #KEY-DETAIL.PYMNT-REQST-LOG-DTE-TIME := EXT.PYMNT-REQST-LOG-DTE-TIME
                    pnd_Key_Detail_Cntrct_Ppcn_Nbr.setValue(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr());                                                                         //Natural: ASSIGN #KEY-DETAIL.CNTRCT-PPCN-NBR := EXT.CNTRCT-PPCN-NBR
                    pnd_Key_Detail_Cntrct_Payee_Cde.setValue(pdaFcpaext.getExt_Cntrct_Payee_Cde());                                                                       //Natural: ASSIGN #KEY-DETAIL.CNTRCT-PAYEE-CDE := EXT.CNTRCT-PAYEE-CDE
                    pnd_Key_Detail_Pymnt_Check_Dte.setValue(pdaFcpaext.getExt_Pymnt_Check_Dte());                                                                         //Natural: ASSIGN #KEY-DETAIL.PYMNT-CHECK-DTE := EXT.PYMNT-CHECK-DTE
                    pnd_Key_Detail_Pymnt_Prcss_Seq_Num.setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Num());                                                                 //Natural: ASSIGN #KEY-DETAIL.PYMNT-PRCSS-SEQ-NUM := EXT.PYMNT-PRCSS-SEQ-NUM
                    pnd_Key_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde.setValue(pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde());                                             //Natural: ASSIGN #KEY-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE := EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
                    //END-BEFORE                                                                                                                                          //Natural: END-BEFORE
                    //*  ----------
                    //*  ---------                                                                                                                                        //Natural: AT BREAK OF #KEY
                    pnd_Ws_Pnd_Records_Read.nadd(1);                                                                                                                      //Natural: ADD 1 TO #RECORDS-READ
                    if (condition(pdaFcpaext.getExt_Pymnt_Instmt_Nbr().equals(99)))                                                                                       //Natural: IF EXT.PYMNT-INSTMT-NBR = 99
                    {
                        //*  ROX
                        if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().notEquals("EW")))                                                                               //Natural: IF EXT.CNTRCT-ORGN-CDE NE 'EW'
                        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-LEDGER
                            sub_Process_Ledger();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                                                                                                                                                                          //Natural: PERFORM PROCESS-FUNDS
                        sub_Process_Funds();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    readWork01Pnd_KeyOld.setValue(pnd_Key_Detail_Pnd_Key);                                                                                                //Natural: END-WORK
                }
                READWORK01_Exit:
                if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
                {
                    atBreakEventReadwork01(endOfDataReadwork01);
                }
                if (Global.isEscape()) return;
                getReports().eject(1, true);                                                                                                                              //Natural: EJECT ( 1 )
                getReports().skip(1, 7);                                                                                                                                  //Natural: SKIP ( 1 ) 7 LINES
                getReports().write(1, new FieldAttributes ("AD=I"),ReportOption.NOTITLE,new TabSetting(7),"INPUT RECORDS          :", new FieldAttributes                 //Natural: WRITE ( 1 ) ( AD = I ) 7T 'INPUT RECORDS          :' ( AD = I ) #RECORDS-READ
                    ("AD=I"),pnd_Ws_Pnd_Records_Read, new ReportEditMask ("Z,ZZZ,ZZ9"));
                if (Global.isEscape()) return;
                getReports().skip(1, 3);                                                                                                                                  //Natural: SKIP ( 1 ) 3 LINES
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new                  //Natural: WRITE ( 1 ) 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' /
                    TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",
                    NEWLINE);
                if (Global.isEscape()) return;
                pnd_Ws_Pnd_Timestmp.setValue(Global.getTIMESTMP());                                                                                                       //Natural: ASSIGN #TIMESTMP := *TIMESTMP
                if (condition(pnd_Input_Parm_Orgn_Cde.equals("NZ")))                                                                                                      //Natural: IF #INPUT-PARM-ORGN-CDE = 'NZ'
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),new TabSetting(81),pnd_Ws_Pnd_Timestmp_A,new  //Natural: WRITE ( 02 ) *TIMX ( EM = MM/DD/YYYY�HH:II:SS.T ) 81T #TIMESTMP-A 97T 'HD WRN1004'
                        TabSetting(97),"HD WRN1004");
                    if (Global.isEscape()) return;
                    //*  'AL'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pnd_Input_Parm_Orgn_Cde.equals("AL")))                                                                                                  //Natural: IF #INPUT-PARM-ORGN-CDE = 'AL'
                    {
                        getReports().write(2, ReportOption.NOTITLE,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),new TabSetting(81),pnd_Ws_Pnd_Timestmp_A,new  //Natural: WRITE ( 02 ) *TIMX ( EM = MM/DD/YYYY�HH:II:SS.T ) 81T #TIMESTMP-A 97T 'HD WRN1005'
                            TabSetting(97),"HD WRN1005");
                        if (Global.isEscape()) return;
                        //*  'AL'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Input_Parm_Orgn_Cde.equals("EW")))                                                                                              //Natural: IF #INPUT-PARM-ORGN-CDE = 'EW'
                        {
                            getReports().write(2, ReportOption.NOTITLE,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),new TabSetting(81),pnd_Ws_Pnd_Timestmp_A,new  //Natural: WRITE ( 02 ) *TIMX ( EM = MM/DD/YYYY�HH:II:SS.T ) 81T #TIMESTMP-A 97T 'HD WRN1006'
                                TabSetting(97),"HD WRN1006");
                            if (Global.isEscape()) return;
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WHAT-LOB
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEW-PAYMENT
                //* *---------------------------
                //* *----------------------------------------------------------------
                //* *--------
                //* *#CHECK-NUMBER-N7  :=  EXT.PYMNT-CHECK-NBR
                //* *  #CHECK-NUMBER-N7  :=  EXT.PYMNT-CHECK-SCRTY-NBR
                //* *  #CHECK-NUMBER-N7    :=  EXT.PYMNT-CHECK-SCRTY-NBR
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-FUND-PAGE
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-FUND-PAGE-1
                //* *--------
                //* ******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FUNDS
                //* ******************************
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DIV-UNITS
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-TAX-INFO
                //*  NEW LOGIC ADDED FOR TAX WITHHOLDING TO READ PYMNT-TAX INFORMATION
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-RTB-AC-INFO
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-LEDGER-PAGE
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-LEDGER
                //* *******************************
                //* * #GL-RECORD (1)                 := EXT(1)
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-END-LINE
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *********************** RL BEGIN - PAYEE MATCH ************MAR 06,2006
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
                //* ************************** RL END-PAYEE MATCH *************************
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_What_Lob() throws Exception                                                                                                                          //Natural: WHAT-LOB
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Screen_Header2.reset();                                                                                                                                       //Natural: RESET #SCREEN-HEADER2 #W-FLD
        pnd_W_Fld.reset();
        //*  ONLY EW WILL HAVE THIS LOB
        short decideConditionsMet1231 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN CNTRCT-TYPE-CDE = 'R' AND PYMNT-PAY-TYPE-REQ-IND = 6
        if (condition(pdaFcpaext.getExt_Cntrct_Type_Cde().equals("R") && pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(6)))
        {
            decideConditionsMet1231++;
            pnd_Screen_Header2.setValue("Repruchase to Suspense");                                                                                                        //Natural: ASSIGN #SCREEN-HEADER2 := 'Repruchase to Suspense'
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-TYPE-CDE = 'R'
        else if (condition(pdaFcpaext.getExt_Cntrct_Type_Cde().equals("R")))
        {
            decideConditionsMet1231++;
            pnd_Screen_Header2.setValue("Repurchase");                                                                                                                    //Natural: ASSIGN #SCREEN-HEADER2 := 'Repurchase'
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-LOB-CDE = 'RA'
        else if (condition(pdaFcpaext.getExt_Cntrct_Lob_Cde().equals("RA")))
        {
            decideConditionsMet1231++;
            pnd_Screen_Header2.setValue("Retirement Annuity Withdrawal Payment");                                                                                         //Natural: ASSIGN #SCREEN-HEADER2 := 'Retirement Annuity Withdrawal Payment'
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-LOB-CDE = 'SRA'
        else if (condition(pdaFcpaext.getExt_Cntrct_Lob_Cde().equals("SRA")))
        {
            decideConditionsMet1231++;
            pnd_Screen_Header2.setValue("Supplemental Retirement Annuity Withdrawal Payment");                                                                            //Natural: ASSIGN #SCREEN-HEADER2 := 'Supplemental Retirement Annuity Withdrawal Payment'
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-LOB-CDE = 'GSRA'
        else if (condition(pdaFcpaext.getExt_Cntrct_Lob_Cde().equals("GSRA")))
        {
            decideConditionsMet1231++;
            pnd_Screen_Header2.setValue("Group Supplemental Retirement Annuity Withdrawal Payment");                                                                      //Natural: ASSIGN #SCREEN-HEADER2 := 'Group Supplemental Retirement Annuity Withdrawal Payment'
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-LOB-CDE = 'GRA'
        else if (condition(pdaFcpaext.getExt_Cntrct_Lob_Cde().equals("GRA")))
        {
            decideConditionsMet1231++;
            pnd_Screen_Header2.setValue("Group Retirement Annuity Withdrawal Payment");                                                                                   //Natural: ASSIGN #SCREEN-HEADER2 := 'Group Retirement Annuity Withdrawal Payment'
        }                                                                                                                                                                 //Natural: WHEN CNTRCT-LOB-CDE = 'IRA'
        else if (condition(pdaFcpaext.getExt_Cntrct_Lob_Cde().equals("IRA")))
        {
            decideConditionsMet1231++;
            pnd_Ws_Pay_Settl_Type_Pnd_Cntrct_Pymnt_Type_Ind.setValue(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind());                                                          //Natural: ASSIGN #CNTRCT-PYMNT-TYPE-IND := CNTRCT-PYMNT-TYPE-IND
            pnd_Ws_Pay_Settl_Type_Pnd_Cntrct_Sttlmnt_Type_Ind.setValue(pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind());                                                      //Natural: ASSIGN #CNTRCT-STTLMNT-TYPE-IND := CNTRCT-STTLMNT-TYPE-IND
            short decideConditionsMet1247 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF #WS-PAY-SETTL-TYPE;//Natural: VALUE 'RJ', 'RH'
            if (condition((pnd_Ws_Pay_Settl_Type.equals("RJ") || pnd_Ws_Pay_Settl_Type.equals("RH"))))
            {
                decideConditionsMet1247++;
                pnd_Screen_Header2.setValue("IRA Classic Internal Rollover to ROTH IRA");                                                                                 //Natural: ASSIGN #SCREEN-HEADER2 := 'IRA Classic Internal Rollover to ROTH IRA'
            }                                                                                                                                                             //Natural: VALUE 'CH', 'CT', 'NH', 'NT'
            else if (condition((pnd_Ws_Pay_Settl_Type.equals("CH") || pnd_Ws_Pay_Settl_Type.equals("CT") || pnd_Ws_Pay_Settl_Type.equals("NH") || pnd_Ws_Pay_Settl_Type.equals("NT"))))
            {
                decideConditionsMet1247++;
                pnd_Screen_Header2.setValue("ROTH IRA - Withdrawal Payment");                                                                                             //Natural: ASSIGN #SCREEN-HEADER2 := 'ROTH IRA - Withdrawal Payment'
            }                                                                                                                                                             //Natural: VALUE 'C ', 'N '
            else if (condition((pnd_Ws_Pay_Settl_Type.equals("C ") || pnd_Ws_Pay_Settl_Type.equals("N "))))
            {
                decideConditionsMet1247++;
                pnd_Screen_Header2.setValue("Classic IRA - Withdrawal Payment");                                                                                          //Natural: ASSIGN #SCREEN-HEADER2 := 'Classic IRA - Withdrawal Payment'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet1231 > 0))
        {
            //*  01/07/2000  ROXAN  - CENTER HEADER
            DbsUtil.examine(new ExamineSource(pnd_Screen_Header2,true), new ExamineSearch(" "), new ExamineGivingLength(pnd_Xx));                                         //Natural: EXAMINE FULL #SCREEN-HEADER2 ' ' GIVING LENGTH #XX
            pnd_Xx.compute(new ComputeParameters(false, pnd_Xx), (DbsField.subtract(60,pnd_Xx)).divide(2));                                                               //Natural: COMPUTE #XX = ( 60 - #XX ) / 2
            pnd_W_Fld.moveAll("H'00'");                                                                                                                                   //Natural: MOVE ALL H'00' TO #W-FLD UNTIL #XX
            pnd_Screen_Header2.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Screen_Header2));                                                  //Natural: COMPRESS #W-FLD #SCREEN-HEADER2 INTO #SCREEN-HEADER2 LEAVING NO SPACE
            DbsUtil.examine(new ExamineSource(pnd_Screen_Header2,true), new ExamineSearch("H'00'", true), new ExamineReplace(" "));                                       //Natural: EXAMINE FULL #SCREEN-HEADER2 FOR FULL H'00' REPLACE ' '
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_New_Payment() throws Exception                                                                                                                       //Natural: NEW-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Screen_Trailer.setValue(" ");                                                                                                                                 //Natural: ASSIGN #SCREEN-TRAILER := ' '
        if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW")))                                                                                                  //Natural: IF EXT.CNTRCT-ORGN-CDE = 'EW'
        {
            pnd_Screen_Header.setValue("Electronic Warrant Payment Warrant");                                                                                             //Natural: ASSIGN #SCREEN-HEADER := 'Electronic Warrant Payment Warrant'
                                                                                                                                                                          //Natural: PERFORM WHAT-LOB
            sub_What_Lob();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Screen_Trailer.setValue("* SEE ELECTRONIC WARRANT TEXT DOCUMENT FOR COMPLETE FINANCIAL INFORMATION *");                                                   //Natural: ASSIGN #SCREEN-TRAILER := '* SEE ELECTRONIC WARRANT TEXT DOCUMENT FOR COMPLETE FINANCIAL INFORMATION *'
            pnd_Ia_Da_Contract.setValue("IA");                                                                                                                            //Natural: ASSIGN #IA-DA-CONTRACT := 'IA'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Eft_Line_Cvs.resetInitial();                                                                                                                           //Natural: RESET INITIAL #EFT-LINE-CVS #FIRST-PERIODIC #RTB-TAX-INFO
        pnd_Ws_Pnd_First_Periodic.resetInitial();
        pnd_Ws_Pnd_Rtb_Tax_Info.resetInitial();
        pnd_Key_Efm.setValuesByName(pnd_Key_Detail);                                                                                                                      //Natural: MOVE BY NAME #KEY-DETAIL TO #KEY-EFM
        short decideConditionsMet1281 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #KEY-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE ' '
        if (condition((pnd_Key_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" "))))
        {
            decideConditionsMet1281++;
            pnd_Ws_Pnd_Csr_Idx.setValue(1);                                                                                                                               //Natural: ASSIGN #CSR-IDX := 1
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Key_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R"))))
        {
            decideConditionsMet1281++;
            pnd_Ws_Pnd_Csr_Idx.setValue(2);                                                                                                                               //Natural: ASSIGN #CSR-IDX := 2
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Csr_Idx.setValue(3);                                                                                                                               //Natural: ASSIGN #CSR-IDX := 3
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_Csr_Display.setValue(pnd_Ws_Pnd_Csr_Array.getValue(pnd_Ws_Pnd_Csr_Idx));                                                                               //Natural: ASSIGN #CSR-DISPLAY := #CSR-ARRAY ( #CSR-IDX )
        //*  ROXAN
        if (condition(pdaFcpaext.getExt_Annt_Ctznshp_Cde().isBreak() || pnd_Ws_Pnd_Records_Read.equals(getZero())))                                                       //Natural: IF BREAK OF EXT.ANNT-CTZNSHP-CDE OR #RECORDS-READ = 0
        {
            pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(58);                                                                                                          //Natural: ASSIGN TBLDCODA.#TABLE-ID := 58
            pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", pdaFcpaext.getExt_Annt_Ctznshp_Cde_X()));                    //Natural: COMPRESS '0' EXT.ANNT-CTZNSHP-CDE-X INTO TBLDCODA.#CODE LEAVING NO SPACE
            DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), ldaFcpltbcd.getTbldcoda_Msg());                                          //Natural: CALLNAT 'TBLDCOD' TBLDCODA TBLDCODA-MSG
            if (condition(Global.isEscape())) return;
            if (condition(ldaFcpltbcd.getTbldcoda_Msg_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                        //Natural: IF ##MSG-DATA ( 1 ) = ' '
            {
                pnd_Ws_Pnd_Ctznshp_Display.setValue(pdaTbldcoda.getTbldcoda_Pnd_Target());                                                                                //Natural: ASSIGN #CTZNSHP-DISPLAY := TBLDCODA.#TARGET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Ctznshp_Display.setValue(pdaFcpaext.getExt_Annt_Ctznshp_Cde_X());                                                                              //Natural: ASSIGN #CTZNSHP-DISPLAY := EXT.ANNT-CTZNSHP-CDE-X
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpaext.getExt_Annt_Rsdncy_Cde().isBreak() || pnd_Ws_Pnd_Records_Read.equals(getZero())))                                                        //Natural: IF BREAK OF EXT.ANNT-RSDNCY-CDE OR #RECORDS-READ = 0
        {
            pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(3);                                                                                                           //Natural: ASSIGN TBLDCODA.#TABLE-ID := 3
            pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(pdaFcpaext.getExt_Annt_Rsdncy_Cde());                                                                             //Natural: ASSIGN TBLDCODA.#CODE := EXT.ANNT-RSDNCY-CDE
            DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), ldaFcpltbcd.getTbldcoda_Msg());                                          //Natural: CALLNAT 'TBLDCOD' TBLDCODA TBLDCODA-MSG
            if (condition(Global.isEscape())) return;
            if (condition(ldaFcpltbcd.getTbldcoda_Msg_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                        //Natural: IF ##MSG-DATA ( 1 ) = ' '
            {
                pnd_Ws_Pnd_State_Disp_X.setValue(pdaTbldcoda.getTbldcoda_Pnd_Target());                                                                                   //Natural: ASSIGN #STATE-DISP-X := TBLDCODA.#TARGET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_State_Disp_X.setValue(pdaFcpaext.getExt_Annt_Rsdncy_Cde());                                                                                    //Natural: ASSIGN #STATE-DISP-X := EXT.ANNT-RSDNCY-CDE
            }                                                                                                                                                             //Natural: END-IF
            pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(19);                                                                                                          //Natural: ASSIGN TBLDCODA.#TABLE-ID := 19
            pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(pdaFcpaext.getExt_Annt_Rsdncy_Cde());                                                                             //Natural: ASSIGN TBLDCODA.#CODE := EXT.ANNT-RSDNCY-CDE
            DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), ldaFcpltbcd.getTbldcoda_Msg());                                          //Natural: CALLNAT 'TBLDCOD' TBLDCODA TBLDCODA-MSG
            if (condition(Global.isEscape())) return;
            if (condition(ldaFcpltbcd.getTbldcoda_Msg_Pnd_Pnd_Msg_Data().getValue(1).equals(" ")))                                                                        //Natural: IF ##MSG-DATA ( 1 ) = ' '
            {
                pnd_Ws_Pnd_State_Disp_Desc.setValue(pdaTbldcoda.getTbldcoda_Pnd_Target());                                                                                //Natural: ASSIGN #STATE-DISP-DESC := TBLDCODA.#TARGET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_State_Disp_Desc.setValue(pnd_Ws_Pnd_State_Disp_X);                                                                                             //Natural: ASSIGN #STATE-DISP-DESC := #STATE-DISP-X
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  ROXAN 12/8
        if (condition(pdaFcpaext.getExt_Cntrct_Option_Cde().equals(21)))                                                                                                  //Natural: IF EXT.CNTRCT-OPTION-CDE = 21
        {
            pnd_Ws_Pnd_Annuity_Certain.setValue(true);                                                                                                                    //Natural: ASSIGN #ANNUITY-CERTAIN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Annuity_Certain.setValue(false);                                                                                                                   //Natural: ASSIGN #ANNUITY-CERTAIN := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpaext.getExt_Cntrct_Option_Cde().isBreak() || pnd_Ws_Pnd_Records_Read.equals(getZero())))                                                      //Natural: IF BREAK OF EXT.CNTRCT-OPTION-CDE OR #RECORDS-READ = 0
        {
            //* MOVE  BY NAME  NZ-PYMNT-ADDR  TO  #FCPAOPTN             /*
            pdaFcpaoptn.getPnd_Fcpaoptn().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                      //Natural: MOVE BY NAME EXTR TO #FCPAOPTN
            pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Predict_Table().setValue(true);                                                                                               //Natural: ASSIGN #FCPAOPTN.#PREDICT-TABLE := TRUE
            //*  ROXAN 01/06
            DbsUtil.callnat(Fcpnoptn.class , getCurrentProcessState(), pdaFcpaoptn.getPnd_Fcpaoptn());                                                                    //Natural: CALLNAT 'FCPNOPTN' #FCPAOPTN
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Option_Display.setValue(pdaFcpaoptn.getPnd_Fcpaoptn_Pnd_Optn_Desc());                                                                              //Natural: ASSIGN #OPTION-DISPLAY := #FCPAOPTN.#OPTN-DESC
            pnd_Ws_Pnd_Option.setValue("OPTION:");                                                                                                                        //Natural: ASSIGN #OPTION := 'OPTION:'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpaext.getExt_Cntrct_Mode_Cde().isBreak() || pnd_Ws_Pnd_Records_Read.equals(getZero())))                                                        //Natural: IF BREAK OF EXT.CNTRCT-MODE-CDE OR #RECORDS-READ = 0
        {
            //* MOVE  BY NAME  NZ-PYMNT-ADDR  TO  #FCPAMODE
            pdaFcpamode.getPnd_Fcpamode().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                      //Natural: MOVE BY NAME EXTR TO #FCPAMODE
            pdaFcpamode.getPnd_Fcpamode_Pnd_Predict_Table().setValue(true);                                                                                               //Natural: ASSIGN #FCPAMODE.#PREDICT-TABLE := TRUE
            //*  ROXAN 01/06
            DbsUtil.callnat(Fcpnmode.class , getCurrentProcessState(), pdaFcpamode.getPnd_Fcpamode());                                                                    //Natural: CALLNAT 'FCPNMODE' #FCPAMODE
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Mode_Display.setValue(pdaFcpamode.getPnd_Fcpamode_Pnd_Mode_Desc());                                                                                //Natural: ASSIGN #MODE-DISPLAY := #FCPAMODE.#MODE-DESC
            pnd_Ws_Pnd_Ws_Mode.setValue("MODE:");                                                                                                                         //Natural: ASSIGN #WS-MODE := 'MODE:'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue(" ");                                                                                                           //Natural: ASSIGN #CHECK-LETTER-A1 := ' '
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        pnd_Check_Number_A11.reset();                                                                                                                                     //Natural: RESET #CHECK-NUMBER-A11
        pnd_Check_Number_A11_Pnd_Check_Number_A10.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                                          //Natural: ASSIGN #CHECK-NUMBER-A10 := EXT.PYMNT-CHECK-NBR
        //*  RL PAYEE MATCH
        short decideConditionsMet1374 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF EXT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet1374++;
            pnd_Ws_Pnd_Pymnt_Display.setValue(pnd_Ws_Pnd_Pymnt_Array.getValue(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind()));                                               //Natural: ASSIGN #PYMNT-DISPLAY := #PYMNT-ARRAY ( PYMNT-PAY-TYPE-REQ-IND )
            pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue(" ");                                                                                                       //Natural: ASSIGN #CHECK-LETTER-A1 := ' '
            pnd_Check_Number_A11_Pnd_Check_Number_N7.setValue(pdaFcpaext.getExt_Pymnt_Check_Nbr());                                                                       //Natural: ASSIGN #CHECK-NUMBER-N7 := EXT.PYMNT-CHECK-NBR
            pnd_Check_Number_A11_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                             //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
            if (condition(pdaFcpaext.getExt_Pymnt_Eft_Acct_Nbr().notEquals(" ")))                                                                                         //Natural: IF EXT.PYMNT-EFT-ACCT-NBR NE ' '
            {
                pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv.setValue("AD=I");                                                                                                   //Natural: MOVE ( AD = I ) TO #PYMNT-EFT-ACCT-NBR-TEXT-CV #PYMNT-EFT-ACCT-NBR-CV #PYMNT-CHK-SAV-IND-TEXT-CV #PYMNT-CHK-SAV-IND-CV
                pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv.setValue("AD=I");
                pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv.setValue("AD=I");
                pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv.setValue("AD=I");
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2
        else if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(2))))
        {
            decideConditionsMet1374++;
            pnd_Ws_Pnd_Pymnt_Display.setValue(pnd_Ws_Pnd_Pymnt_Array.getValue(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind()));                                               //Natural: ASSIGN #PYMNT-DISPLAY := #PYMNT-ARRAY ( PYMNT-PAY-TYPE-REQ-IND )
            //*  RL PAYEE MATCH
            //*  RL PAYEE MATCH
            pnd_Check_Number_A11.reset();                                                                                                                                 //Natural: RESET #CHECK-NUMBER-A11
            pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue("E");                                                                                                       //Natural: ASSIGN #CHECK-LETTER-A1 := 'E'
            pnd_Check_Number_A11_Pnd_Check_Number_A10.setValue(pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr());                                                                //Natural: ASSIGN #CHECK-NUMBER-A10 := EXT.PYMNT-CHECK-SCRTY-NBR
            pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv.setValue("AD=I");                                                                                                     //Natural: MOVE ( AD = I ) TO #PYMNT-EFT-TRANSIT-ID-TEXT-CV #PYMNT-EFT-TRANSIT-ID-CV #PYMNT-EFT-ACCT-NBR-TEXT-CV #PYMNT-EFT-ACCT-NBR-CV #PYMNT-CHK-SAV-IND-TEXT-CV #PYMNT-CHK-SAV-IND-CV
            pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv.setValue("AD=I");
            pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv.setValue("AD=I");
            pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv.setValue("AD=I");
            pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv.setValue("AD=I");
            pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv.setValue("AD=I");
        }                                                                                                                                                                 //Natural: VALUE 3
        else if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(3))))
        {
            decideConditionsMet1374++;
            pnd_Ws_Pnd_Pymnt_Display.setValue(pnd_Ws_Pnd_Pymnt_Array.getValue(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind()));                                               //Natural: ASSIGN #PYMNT-DISPLAY := #PYMNT-ARRAY ( PYMNT-PAY-TYPE-REQ-IND )
        }                                                                                                                                                                 //Natural: VALUE 8
        else if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8))))
        {
            decideConditionsMet1374++;
            //*  RL PAYEE MATCH
            //*  RL PAYEE MATCH
            pnd_Check_Number_A11.reset();                                                                                                                                 //Natural: RESET #CHECK-NUMBER-A11
            pnd_Ws_Pnd_Internal_Rollover.setValue(true);                                                                                                                  //Natural: ASSIGN #INTERNAL-ROLLOVER := TRUE
            pnd_Ws_Pnd_Pymnt_Display.setValue(pnd_Ws_Pnd_Pymnt_Array.getValue(4));                                                                                        //Natural: ASSIGN #PYMNT-DISPLAY := #PYMNT-ARRAY ( 4 )
            pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue("R");                                                                                                       //Natural: ASSIGN #CHECK-LETTER-A1 := 'R'
            pnd_Check_Number_A11_Pnd_Check_Number_A10.setValue(pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr());                                                                //Natural: ASSIGN #CHECK-NUMBER-A10 := EXT.PYMNT-CHECK-SCRTY-NBR
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Pymnt_Display.setValue(pnd_Ws_Pnd_Pymnt_Array.getValue(5));                                                                                        //Natural: ASSIGN #PYMNT-DISPLAY := #PYMNT-ARRAY ( 5 )
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pdaFcpaext.getExt_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                                         //Natural: IF EXT.PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
        {
            pnd_Ws_Pnd_Fed_Display.setValue("NRA");                                                                                                                       //Natural: ASSIGN #FED-DISPLAY := 'NRA'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Fed_Display.setValue("USA");                                                                                                                       //Natural: ASSIGN #FED-DISPLAY := 'USA'
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PAGE-NUMBER
        //*   COMPUTE FOR GROSS-AMT FORMERLY DONE IN FCPA371   /* ROXAN 12/09
        pnd_Ws_Pymnt_Gross_Amt.reset();                                                                                                                                   //Natural: RESET #WS-PYMNT-GROSS-AMT
        if (condition(pdaFcpaext.getExt_C_Inv_Acct().greater(getZero())))                                                                                                 //Natural: IF C-INV-ACCT GT 0
        {
            pnd_Ws_Pymnt_Gross_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                                           //Natural: ADD EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) TO #WS-PYMNT-GROSS-AMT
            pnd_Ws_Pymnt_Gross_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                                           //Natural: ADD EXT.INV-ACCT-DVDND-AMT ( 1:C-INV-ACCT ) TO #WS-PYMNT-GROSS-AMT
            pnd_Ws_Pymnt_Gross_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                                             //Natural: ADD EXT.INV-ACCT-DPI-AMT ( 1:C-INV-ACCT ) TO #WS-PYMNT-GROSS-AMT
            pnd_Ws_Pymnt_Gross_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                                             //Natural: ADD EXT.INV-ACCT-DCI-AMT ( 1:C-INV-ACCT ) TO #WS-PYMNT-GROSS-AMT
        }                                                                                                                                                                 //Natural: END-IF
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872.class));                                                                               //Natural: WRITE ( 1 ) NOTITLE USING MAP 'FCPM872'
        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872e.class));                                                                              //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM872E'
        pnd_Ws_Pnd_Pymnt_Acctg_Dte.setValue(pdaFcpaext.getExt_Pymnt_Acctg_Dte());                                                                                         //Natural: ASSIGN #PYMNT-ACCTG-DTE := EXT.PYMNT-ACCTG-DTE
                                                                                                                                                                          //Natural: PERFORM INIT-LEDGER-PAGE
        sub_Init_Ledger_Page();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Init_Fund_Page() throws Exception                                                                                                                    //Natural: INIT-FUND-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        pnd_Ws_Pnd_Inv_Acct.getValue("*").resetInitial();                                                                                                                 //Natural: RESET INITIAL #WS.#INV-ACCT ( * ) #DISP-IDX #GROSS-IDX
        pnd_Ws_Pnd_Disp_Idx.resetInitial();
        pnd_Ws_Pnd_Gross_Idx.resetInitial();
    }
    private void sub_Init_Fund_Page_1() throws Exception                                                                                                                  //Natural: INIT-FUND-PAGE-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        pnd_Ws_Pnd_Fund_Idx.reset();                                                                                                                                      //Natural: RESET #FUND-IDX
        //*   ROXAN
        short decideConditionsMet1451 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN RTB
        if (condition(rtb.getBoolean()))
        {
            decideConditionsMet1451++;
            pnd_Ws_Pnd_Gross_Idx.setValue(2);                                                                                                                             //Natural: ASSIGN #GROSS-IDX := 2
            short decideConditionsMet1457 = 0;                                                                                                                            //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EXT.CNTRCT-ROLL-DEST-CDE NE ' '
            if (condition(pdaFcpaext.getExt_Cntrct_Roll_Dest_Cde().notEquals(" ")))
            {
                decideConditionsMet1457++;
                pnd_Ws_Pnd_Fund_Idx.setValue(1);                                                                                                                          //Natural: ASSIGN #FUND-IDX := 1
                pnd_Ws_Pnd_Rollover_Ind.setValue(true);                                                                                                                   //Natural: ASSIGN #ROLLOVER-IND := TRUE
            }                                                                                                                                                             //Natural: WHEN EXT.PYMNT-RTB-DEST-TYP = 'PART'
            else if (condition(pdaFcpaext.getExt_Pymnt_Rtb_Dest_Typ().equals("PART")))
            {
                decideConditionsMet1457++;
                pnd_Ws_Pnd_Fund_Idx.setValue(2);                                                                                                                          //Natural: ASSIGN #FUND-IDX := 2
            }                                                                                                                                                             //Natural: WHEN EXT.PYMNT-RTB-DEST-TYP = 'ALT1' OR = 'ALT2'
            else if (condition(pdaFcpaext.getExt_Pymnt_Rtb_Dest_Typ().equals("ALT1") || pdaFcpaext.getExt_Pymnt_Rtb_Dest_Typ().equals("ALT2")))
            {
                decideConditionsMet1457++;
                pnd_Ws_Pnd_Fund_Idx.setValue(3);                                                                                                                          //Natural: ASSIGN #FUND-IDX := 3
            }                                                                                                                                                             //Natural: WHEN NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: WHEN IVC-FROM-RTB-ROLLOVER
        else if (condition(ivc_From_Rtb_Rollover.getBoolean()))
        {
            decideConditionsMet1451++;
            pnd_Ws_Pnd_Fund_Idx.setValue(5);                                                                                                                              //Natural: ASSIGN #FUND-IDX := 5
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            if (condition(pnd_Input_Parm_Orgn_Cde.equals("AL")))                                                                                                          //Natural: IF #INPUT-PARM-ORGN-CDE = 'AL'
            {
                pnd_Ws_Pnd_Fund_Idx.setValue(6);                                                                                                                          //Natural: ASSIGN #FUND-IDX := 6
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Fund_Idx.setValue(4);                                                                                                                          //Natural: ASSIGN #FUND-IDX := 4
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_Fund_Display.setValue(pnd_Ws_Pnd_Fund_Array.getValue(pnd_Ws_Pnd_Fund_Idx.getInt() + 1));                                                               //Natural: ASSIGN #FUND-DISPLAY := #FUND-ARRAY ( #FUND-IDX )
        //*  ROX
        //*  RITA 03/17/99 /*
        if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("AL") || pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW")))                                              //Natural: IF EXT.CNTRCT-ORGN-CDE = 'AL' OR EXT.CNTRCT-ORGN-CDE = 'EW'
        {
            pnd_Ws_Pnd_Effective_Dte.setValue(pdaFcpaext.getExt_Pymnt_Settlmnt_Dte());                                                                                    //Natural: ASSIGN #EFFECTIVE-DTE := EXT.PYMNT-SETTLMNT-DTE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Effective_Dte.compute(new ComputeParameters(false, pnd_Ws_Pnd_Effective_Dte), pdaFcpaext.getExt_Pymnt_Settlmnt_Dte().subtract(1));                 //Natural: ASSIGN #EFFECTIVE-DTE := EXT.PYMNT-SETTLMNT-DTE - 1
        }                                                                                                                                                                 //Natural: END-IF
        //*  ROXAN 01/14/00
        if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW")))                                                                                                  //Natural: IF EXT.CNTRCT-ORGN-CDE = 'EW'
        {
            pnd_Ws_Pnd_Fund_Display.setValue(pnd_Screen_Header2);                                                                                                         //Natural: ASSIGN #FUND-DISPLAY := #SCREEN-HEADER2
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(rtb.getBoolean() || ivc_From_Rtb_Rollover.getBoolean()))                                                                                            //Natural: IF RTB OR IVC-FROM-RTB-ROLLOVER
        {
            if (condition(pnd_Ws_Pnd_Fund_Idx.greaterOrEqual(1) && pnd_Ws_Pnd_Fund_Idx.lessOrEqual(3)))                                                                   //Natural: IF #FUND-IDX = 1 THRU 3
            {
                pnd_Ws_Pnd_Fund_Sub.setValue(pnd_Ws_Pnd_Fund_Array_Idx.getValue(pnd_Ws_Pnd_Fund_Idx));                                                                    //Natural: ASSIGN #FUND-SUB := #FUND-ARRAY-IDX ( #FUND-IDX )
                setValueToSubstring(pdaFcpaext.getExt_Pymnt_Rtb_Dest_Typ(),pnd_Ws_Pnd_Fund_Display,pnd_Ws_Pnd_Fund_Sub.getInt(),4);                                       //Natural: MOVE EXT.PYMNT-RTB-DEST-TYP TO SUBSTR ( #FUND-DISPLAY,#FUND-SUB,4 )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Gross_Amt_Disp.setValue(pnd_Ws_Pnd_Gross_Amt_Array.getValue(pnd_Ws_Pnd_Gross_Idx));                                                                    //Natural: ASSIGN #GROSS-AMT-DISP := #GROSS-AMT-ARRAY ( #GROSS-IDX )
        if (condition(rtb.getBoolean() || pnd_Ws_Pnd_First_Periodic.getBoolean()))                                                                                        //Natural: IF RTB OR #FIRST-PERIODIC
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-RTB-AC-INFO
            sub_Display_Rtb_Ac_Info();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            if (condition(! (ivc_From_Rtb_Rollover.getBoolean())))                                                                                                        //Natural: IF NOT IVC-FROM-RTB-ROLLOVER
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-TAX-INFO
                sub_Display_Tax_Info();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Funds() throws Exception                                                                                                                     //Natural: PROCESS-FUNDS
    {
        if (BLNatReinput.isReinput()) return;

        rtb.setValue(false);                                                                                                                                              //Natural: ASSIGN RTB := FALSE
        short decideConditionsMet1516 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EXT.CNTRCT-PYMNT-TYPE-IND = 'N' AND EXT.CNTRCT-STTLMNT-TYPE-IND = 'X'
        if (condition(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind().equals("N") && pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("X")))
        {
            decideConditionsMet1516++;
            rtb.setValue(true);                                                                                                                                           //Natural: ASSIGN RTB := TRUE
        }                                                                                                                                                                 //Natural: WHEN EXT.CNTRCT-PYMNT-TYPE-IND = 'I' AND EXT.CNTRCT-STTLMNT-TYPE-IND = 'R'
        else if (condition(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind().equals("I") && pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("R")))
        {
            decideConditionsMet1516++;
            ivc_From_Rtb_Rollover.setValue(true);                                                                                                                         //Natural: ASSIGN IVC-FROM-RTB-ROLLOVER := TRUE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM INIT-FUND-PAGE
        sub_Init_Fund_Page();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM INIT-FUND-PAGE-1
        sub_Init_Fund_Page_1();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
        pnd_Ws_Pnd_Disp_Idx.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #DISP-IDX
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                                   //Natural: ASSIGN #INV-ACCT-CDE-DESC-CV ( #DISP-IDX ) := ( AD = I )
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("      TOTAL");                                                                               //Natural: ASSIGN #INV-ACCT-CDE-DESC ( #DISP-IDX ) := '      TOTAL'
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                                  //Natural: ASSIGN #INV-ACCT-SETTL-AMT-CV ( #DISP-IDX ) := ( AD = I )
        //*  ROXAN
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));          //Natural: ADD EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-SETTL-AMT ( #DISP-IDX )
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));          //Natural: ADD EXT.INV-ACCT-DVDND-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-SETTL-AMT ( #DISP-IDX )
        pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()).nadd(pdaFcpaext.getExt_Inv_Acct_Dci_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct())); //Natural: ADD EXT.INV-ACCT-DCI-AMT ( 1:C-INV-ACCT ) TO EXT.INV-ACCT-DPI-AMT ( 1:C-INV-ACCT )
        pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));          //Natural: ADD EXT.INV-ACCT-DPI-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-DPI-DCI-AMT ( #DISP-IDX )
        if (condition(pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                   //Natural: IF #INV-ACCT-DPI-DCI-AMT ( #DISP-IDX ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                            //Natural: ASSIGN #INV-ACCT-DPI-DCI-AMT-CV ( #DISP-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));          //Natural: ADD EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-GROSS-AMT ( #DISP-IDX )
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));          //Natural: ADD EXT.INV-ACCT-DVDND-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-GROSS-AMT ( #DISP-IDX )
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));            //Natural: ADD EXT.INV-ACCT-DPI-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-GROSS-AMT ( #DISP-IDX )
        pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                                  //Natural: ASSIGN #INV-ACCT-GROSS-AMT-CV ( #DISP-IDX ) := ( AD = I )
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));              //Natural: ADD EXT.INV-ACCT-IVC-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-IVC-AMT ( #DISP-IDX )
        if (condition(pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                       //Natural: IF #INV-ACCT-IVC-AMT ( #DISP-IDX ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                                //Natural: ASSIGN #INV-ACCT-IVC-AMT-CV ( #DISP-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).compute(new ComputeParameters(false, pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(pnd_Ws_Pnd_Disp_Idx)),    //Natural: ASSIGN #INV-ACCT-TXBLE-AMT ( #DISP-IDX ) := ( #INV-ACCT-DVDND-AMT ( #DISP-IDX ) + #INV-ACCT-SETTL-AMT ( #DISP-IDX ) ) - #INV-ACCT-IVC-AMT ( #DISP-IDX )
            (pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).add(pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_Ws_Pnd_Disp_Idx))).subtract(pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(pnd_Ws_Pnd_Disp_Idx)));
        if (condition(pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                     //Natural: IF #INV-ACCT-TXBLE-AMT ( #DISP-IDX ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Txble_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                              //Natural: ASSIGN #INV-ACCT-TXBLE-AMT-CV ( #DISP-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));        //Natural: ADD EXT.INV-ACCT-FDRL-TAX-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-FDRL-TAX ( #DISP-IDX )
        if (condition(pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                      //Natural: IF #INV-ACCT-FDRL-TAX ( #DISP-IDX ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                               //Natural: ASSIGN #INV-ACCT-FDRL-TAX-CV ( #DISP-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));      //Natural: ADD EXT.INV-ACCT-STATE-TAX-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-STATE-TAX ( #DISP-IDX )
        if (condition(pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                     //Natural: IF #INV-ACCT-STATE-TAX ( #DISP-IDX ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_State_Tax_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                              //Natural: ASSIGN #INV-ACCT-STATE-TAX-CV ( #DISP-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));      //Natural: ADD EXT.INV-ACCT-LOCAL-TAX-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-LOCAL-TAX ( #DISP-IDX )
        if (condition(pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                     //Natural: IF #INV-ACCT-LOCAL-TAX ( #DISP-IDX ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                              //Natural: ASSIGN #INV-ACCT-LOCAL-TAX-CV ( #DISP-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));       //Natural: ADD EXT.INV-ACCT-FDRL-TAX-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-TOTAL-TAX ( #DISP-IDX )
        pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));      //Natural: ADD EXT.INV-ACCT-STATE-TAX-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-TOTAL-TAX ( #DISP-IDX )
        pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));      //Natural: ADD EXT.INV-ACCT-LOCAL-TAX-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-TOTAL-TAX ( #DISP-IDX )
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(pnd_Ws_Pnd_Disp_Idx).nadd(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));      //Natural: ADD EXT.INV-ACCT-NET-PYMNT-AMT ( 1:C-INV-ACCT ) TO #INV-ACCT-NET-PYMNT ( #DISP-IDX )
        if (condition(pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                     //Natural: IF #INV-ACCT-TOTAL-TAX ( #DISP-IDX ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Total_Tax_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                              //Natural: ASSIGN #INV-ACCT-TOTAL-TAX-CV ( #DISP-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                                  //Natural: ASSIGN #INV-ACCT-NET-PYMNT-CV ( #DISP-IDX ) := ( AD = I )
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO C-INV-ACCT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpaext.getExt_C_Inv_Acct())); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Disp_Idx.equals(4)))                                                                                                                 //Natural: IF #DISP-IDX = 4
            {
                pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                           //Natural: ADD 1 TO #PAGE-NUMBER
                //*     PRINT '=' NZ-EXT.PYMNT-SETTLMNT-DTE
                //*           '=' #WS.#EFFECTIVE-DTE
                //*  RITA 03/17/99          /* ROXAN
                if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("AL")))                                                                                          //Natural: IF EXT.CNTRCT-ORGN-CDE = 'AL'
                {
                    //*  ROXAN
                    pnd_Ws_Pnd_Fund_Display.reset();                                                                                                                      //Natural: RESET #WS.#FUND-DISPLAY #WS.#INV-ACCT-TXBLE-AMT ( * ) #WS.#INV-ACCT-CDE-DESC-CV ( 1 ) #WS.#INV-ACCT-SETTL-AMT-CV ( 1 ) #WS.#INV-ACCT-CNTRCT-AMT-CV ( 1 ) #WS.#INV-ACCT-DVDND-AMT-CV ( 1 ) #WS.#INV-ACCT-GROSS-AMT-CV ( 1 ) #WS.#INV-ACCT-NET-PYMNT-CV ( 1 )
                    pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue("*").reset();
                    pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.getValue(1).reset();
                    pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.getValue(1).reset();
                    pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(1).reset();
                    pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(1).reset();
                    pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv.getValue(1).reset();
                    pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv.getValue(1).reset();
                    pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.getValue(2,":",4).setValue("AD=N");                                                                                   //Natural: ASSIGN #INV-ACCT-CDE-DESC-CV ( 2:4 ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.getValue(2,":",4).setValue("AD=N");                                                                                  //Natural: ASSIGN #INV-ACCT-SETTL-AMT-CV ( 2:4 ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(2,":",4).setValue("AD=N");                                                                                 //Natural: ASSIGN #INV-ACCT-CNTRCT-AMT-CV ( 2:4 ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv.getValue(2,":",4).setValue("AD=N");                                                                                  //Natural: ASSIGN #INV-ACCT-GROSS-AMT-CV ( 2:4 ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv.getValue(2,":",4).setValue("AD=N");                                                                                  //Natural: ASSIGN #INV-ACCT-NET-PYMNT-CV ( 2:4 ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_Txble_Amt_Cv.getValue("*").setValue("AD=N");                                                                                      //Natural: ASSIGN #INV-ACCT-TXBLE-AMT-CV ( * ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Cv.getValue("*").setValue("AD=N");                                                                                       //Natural: ASSIGN #INV-ACCT-FDRL-TAX-CV ( * ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_State_Tax_Cv.getValue("*").setValue("AD=N");                                                                                      //Natural: ASSIGN #INV-ACCT-STATE-TAX-CV ( * ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv.getValue("*").setValue("AD=N");                                                                                      //Natural: ASSIGN #INV-ACCT-LOCAL-TAX-CV ( * ) := ( AD = N )
                    pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv.getValue("*").setValue("AD=N");                                                                                      //Natural: ASSIGN #INV-ACCT-LOCAL-TAX-CV ( * ) := ( AD = N )
                }                                                                                                                                                         //Natural: END-IF
                if (condition(getReports().getAstLinesLeft(1).less(21)))                                                                                                  //Natural: EJECT ( 1 ) IF LESS THAN 21 LINES LEFT
                {
                    getReports().eject(1);
                }
                if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW")))                                                                                          //Natural: IF EXT.CNTRCT-ORGN-CDE = 'EW'
                {
                    //*     MOVE ' ' TO #MODE-V #OPTION
                    //*  ROXAN 01/06
                    pnd_Ws_Pnd_Ws_Mode.setValue(" ");                                                                                                                     //Natural: MOVE ' ' TO #WS-MODE #OPTION #OPTION-DISPLAY #MODE-DISPLAY
                    pnd_Ws_Pnd_Option.setValue(" ");
                    pnd_Ws_Pnd_Option_Display.setValue(" ");
                    pnd_Ws_Pnd_Mode_Display.setValue(" ");
                    //*  01/06
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Ws_Mode.setValue("MODE:");                                                                                                                 //Natural: ASSIGN #WS-MODE := 'MODE:'
                    pnd_Ws_Pnd_Option.setValue("OPTION:");                                                                                                                //Natural: ASSIGN #OPTION := 'OPTION:'
                }                                                                                                                                                         //Natural: END-IF
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872a.class));                                                                      //Natural: WRITE ( 1 ) NOTITLE USING MAP 'FCPM872A'
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872f.class));                                                                      //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM872F'
                                                                                                                                                                          //Natural: PERFORM INIT-FUND-PAGE
                sub_Init_Fund_Page();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
                //*  ROXAN
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(pdaFcpaext.getExt_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I));                                         //Natural: ASSIGN #FUND-PDA.#INV-ACCT-INPUT := #FUND-CDE := EXT.INV-ACCT-CDE-N ( #I )
            pnd_Ws_Pnd_Fund_Cde.setValue(pdaFcpaext.getExt_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I));
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(pdaFcpaext.getExt_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := EXT.INV-ACCT-VALUAT-PERIOD ( #I )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' USING #FUND-PDA
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Disp_Idx.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #DISP-IDX
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                               //Natural: ASSIGN #INV-ACCT-CDE-DESC-CV ( #DISP-IDX ) := ( AD = I )
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean() || rtb.getBoolean() || ivc_From_Rtb_Rollover.getBoolean()))                            //Natural: IF #FUND-PDA.#DIVIDENDS OR RTB OR IVC-FROM-RTB-ROLLOVER
            {
                pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(), MoveOption.RightJustified);        //Natural: MOVE RIGHT #FUND-PDA.#INV-ACCT-DESC-8 TO #INV-ACCT-CDE-DESC ( #DISP-IDX )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(),  //Natural: COMPRESS #FUND-PDA.#INV-ACCT-DESC-8 #FUND-PDA.#VALUAT-DESC-3 INTO #INV-ACCT-CDE-DESC ( #DISP-IDX ) LEAVING NO SPACE
                    pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_3()));
                pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(pnd_Ws_Pnd_Disp_Idx), MoveOption.RightJustified); //Natural: MOVE RIGHT #INV-ACCT-CDE-DESC ( #DISP-IDX ) TO #INV-ACCT-CDE-DESC ( #DISP-IDX )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                              //Natural: ASSIGN #INV-ACCT-SETTL-AMT-CV ( #DISP-IDX ) := ( AD = I )
            pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).compute(new ComputeParameters(false, pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_Ws_Pnd_Disp_Idx)),  //Natural: ASSIGN #INV-ACCT-SETTL-AMT ( #DISP-IDX ) := EXT.INV-ACCT-SETTL-AMT ( #I ) + EXT.INV-ACCT-DVDND-AMT ( #I )
                pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_I).add(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I)));
            pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).compute(new ComputeParameters(false, pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(pnd_Ws_Pnd_Disp_Idx)),  //Natural: ADD EXT.INV-ACCT-SETTL-AMT ( #I ) EXT.INV-ACCT-DPI-AMT ( #I ) EXT.INV-ACCT-DVDND-AMT ( #I ) TO #INV-ACCT-GROSS-AMT ( #DISP-IDX )
                pnd_Ws_Pnd_Inv_Acct_Gross_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).add(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_I)).add(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_I)).add(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I)));
            if (condition(! (rtb.getBoolean())))                                                                                                                          //Natural: IF NOT RTB
            {
                                                                                                                                                                          //Natural: PERFORM PROCESS-DIV-UNITS
                sub_Process_Div_Units();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                                 //Natural: IF EXT.INV-ACCT-DPI-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                        //Natural: ASSIGN #INV-ACCT-DPI-DCI-AMT-CV ( #DISP-IDX ) := ( AD = I )
                pnd_Ws_Pnd_Inv_Acct_Dpi_Dci_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_I));                      //Natural: ASSIGN #INV-ACCT-DPI-DCI-AMT ( #DISP-IDX ) := EXT.INV-ACCT-DPI-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                              //Natural: ASSIGN #INV-ACCT-GROSS-AMT-CV ( #DISP-IDX ) := ( AD = I )
            if (condition(pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                                 //Natural: IF EXT.INV-ACCT-IVC-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                            //Natural: ASSIGN #INV-ACCT-IVC-AMT-CV ( #DISP-IDX ) := ( AD = I )
                pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Pnd_I));                          //Natural: ASSIGN #INV-ACCT-IVC-AMT ( #DISP-IDX ) := EXT.INV-ACCT-IVC-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).compute(new ComputeParameters(false, pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(pnd_Ws_Pnd_Disp_Idx)),  //Natural: ASSIGN #INV-ACCT-TXBLE-AMT ( #DISP-IDX ) := ( #INV-ACCT-SETTL-AMT ( #DISP-IDX ) + #INV-ACCT-DVDND-AMT ( #DISP-IDX ) ) - #INV-ACCT-IVC-AMT ( #DISP-IDX )
                (pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).add(pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(pnd_Ws_Pnd_Disp_Idx))).subtract(pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(pnd_Ws_Pnd_Disp_Idx)));
            if (condition(pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                 //Natural: IF #INV-ACCT-TXBLE-AMT ( #DISP-IDX ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Txble_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                          //Natural: ASSIGN #INV-ACCT-TXBLE-AMT-CV ( #DISP-IDX ) := ( AD = I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                            //Natural: IF EXT.INV-ACCT-FDRL-TAX-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I));                    //Natural: ASSIGN #INV-ACCT-FDRL-TAX ( #DISP-IDX ) := EXT.INV-ACCT-FDRL-TAX-AMT ( #I )
                pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                           //Natural: ASSIGN #INV-ACCT-FDRL-TAX-CV ( #DISP-IDX ) := ( AD = I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                           //Natural: IF EXT.INV-ACCT-STATE-TAX-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_I));                  //Natural: ASSIGN #INV-ACCT-STATE-TAX ( #DISP-IDX ) := EXT.INV-ACCT-STATE-TAX-AMT ( #I )
                pnd_Ws_Pnd_Inv_Acct_State_Tax_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                          //Natural: ASSIGN #INV-ACCT-STATE-TAX-CV ( #DISP-IDX ) := ( AD = I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                           //Natural: IF EXT.INV-ACCT-LOCAL-TAX-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I));                  //Natural: ASSIGN #INV-ACCT-LOCAL-TAX ( #DISP-IDX ) := EXT.INV-ACCT-LOCAL-TAX-AMT ( #I )
                pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                          //Natural: ASSIGN #INV-ACCT-LOCAL-TAX-CV ( #DISP-IDX ) := ( AD = I )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).compute(new ComputeParameters(false, pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(pnd_Ws_Pnd_Disp_Idx)),  //Natural: ASSIGN #INV-ACCT-TOTAL-TAX ( #DISP-IDX ) := #INV-ACCT-FDRL-TAX ( #DISP-IDX ) + #INV-ACCT-STATE-TAX ( #DISP-IDX ) + #INV-ACCT-LOCAL-TAX ( #DISP-IDX )
                pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).add(pnd_Ws_Pnd_Inv_Acct_State_Tax.getValue(pnd_Ws_Pnd_Disp_Idx)).add(pnd_Ws_Pnd_Inv_Acct_Local_Tax.getValue(pnd_Ws_Pnd_Disp_Idx)));
            if (condition(pnd_Ws_Pnd_Inv_Acct_Total_Tax.getValue(pnd_Ws_Pnd_Disp_Idx).notEquals(new DbsDecimal("0.00"))))                                                 //Natural: IF #INV-ACCT-TOTAL-TAX ( #DISP-IDX ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Total_Tax_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                          //Natural: ASSIGN #INV-ACCT-TOTAL-TAX-CV ( #DISP-IDX ) := ( AD = I )
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Inv_Acct_Net_Pymnt.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I));                      //Natural: ASSIGN #INV-ACCT-NET-PYMNT ( #DISP-IDX ) := EXT.INV-ACCT-NET-PYMNT-AMT ( #I )
            pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                              //Natural: ASSIGN #INV-ACCT-NET-PYMNT-CV ( #DISP-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PAGE-NUMBER
        //*     PRINT '=' NZ-EXT.PYMNT-SETTLMNT-DTE
        //*           '=' #WS.#EFFECTIVE-DTE
        //*           '=' #RUN-TIME (EM=MM/DD/YYYY�-�HH:IIAP)
        //*  RITA 03/17/99            /* ROXAN
        if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("AL")))                                                                                                  //Natural: IF EXT.CNTRCT-ORGN-CDE = 'AL'
        {
            //*  ROXAN
            pnd_Ws_Pnd_Fund_Display.reset();                                                                                                                              //Natural: RESET #WS.#FUND-DISPLAY #WS.#INV-ACCT-TXBLE-AMT ( * ) #WS.#INV-ACCT-CDE-DESC-CV ( 1 ) #WS.#INV-ACCT-SETTL-AMT-CV ( 1 ) #WS.#INV-ACCT-CNTRCT-AMT-CV ( 1 ) #WS.#INV-ACCT-DVDND-AMT-CV ( 1 ) #WS.#INV-ACCT-GROSS-AMT-CV ( 1 ) #WS.#INV-ACCT-NET-PYMNT-CV ( 1 )
            pnd_Ws_Pnd_Inv_Acct_Txble_Amt.getValue("*").reset();
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.getValue(1).reset();
            pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.getValue(1).reset();
            pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(1).reset();
            pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(1).reset();
            pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv.getValue(1).reset();
            pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv.getValue(1).reset();
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.getValue(2,":",4).setValue("AD=N");                                                                                           //Natural: ASSIGN #INV-ACCT-CDE-DESC-CV ( 2:4 ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.getValue(2,":",4).setValue("AD=N");                                                                                          //Natural: ASSIGN #INV-ACCT-SETTL-AMT-CV ( 2:4 ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(2,":",4).setValue("AD=N");                                                                                         //Natural: ASSIGN #INV-ACCT-CNTRCT-AMT-CV ( 2:4 ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_Gross_Amt_Cv.getValue(2,":",4).setValue("AD=N");                                                                                          //Natural: ASSIGN #INV-ACCT-GROSS-AMT-CV ( 2:4 ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Cv.getValue(2,":",4).setValue("AD=N");                                                                                          //Natural: ASSIGN #INV-ACCT-NET-PYMNT-CV ( 2:4 ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_Txble_Amt_Cv.getValue("*").setValue("AD=N");                                                                                              //Natural: ASSIGN #INV-ACCT-TXBLE-AMT-CV ( * ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Cv.getValue("*").setValue("AD=N");                                                                                               //Natural: ASSIGN #INV-ACCT-FDRL-TAX-CV ( * ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_State_Tax_Cv.getValue("*").setValue("AD=N");                                                                                              //Natural: ASSIGN #INV-ACCT-STATE-TAX-CV ( * ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv.getValue("*").setValue("AD=N");                                                                                              //Natural: ASSIGN #INV-ACCT-LOCAL-TAX-CV ( * ) := ( AD = N )
            pnd_Ws_Pnd_Inv_Acct_Local_Tax_Cv.getValue("*").setValue("AD=N");                                                                                              //Natural: ASSIGN #INV-ACCT-LOCAL-TAX-CV ( * ) := ( AD = N )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(getReports().getAstLinesLeft(1).less(21)))                                                                                                          //Natural: EJECT ( 1 ) IF LESS THAN 21 LINES LEFT
        {
            getReports().eject(1);
        }
        if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("EW")))                                                                                                  //Natural: IF EXT.CNTRCT-ORGN-CDE = 'EW'
        {
            //* MOVE ' ' TO #MODE-V #OPTION
            //*  ROXAN 01/06
            pnd_Ws_Pnd_Ws_Mode.setValue(" ");                                                                                                                             //Natural: MOVE ' ' TO #WS-MODE #OPTION #OPTION-DISPLAY #MODE-DISPLAY
            pnd_Ws_Pnd_Option.setValue(" ");
            pnd_Ws_Pnd_Option_Display.setValue(" ");
            pnd_Ws_Pnd_Mode_Display.setValue(" ");
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Ws_Mode.setValue("MODE:");                                                                                                                         //Natural: ASSIGN #WS-MODE := 'MODE:'
            pnd_Ws_Pnd_Option.setValue("OPTION:");                                                                                                                        //Natural: ASSIGN #OPTION := 'OPTION:'
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872a.class));                                                                              //Natural: WRITE ( 1 ) NOTITLE USING MAP 'FCPM872A'
        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872f.class));                                                                              //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM872F'
    }
    private void sub_Process_Div_Units() throws Exception                                                                                                                 //Natural: PROCESS-DIV-UNITS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                             //Natural: ASSIGN #INV-ACCT-CNTRCT-AMT-CV ( #DISP-IDX ) := ( AD = I )
            pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #INV-ACCT-CNTRCT-AMT ( #DISP-IDX ) := EXT.INV-ACCT-SETTL-AMT ( #I )
            if (condition(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                               //Natural: IF EXT.INV-ACCT-DVDND-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                          //Natural: ASSIGN #INV-ACCT-DVDND-AMT-CV ( #DISP-IDX ) := ( AD = I )
                pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I));                      //Natural: ASSIGN #INV-ACCT-DVDND-AMT ( #DISP-IDX ) := EXT.INV-ACCT-DVDND-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpaext.getExt_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.000"))))                                               //Natural: IF EXT.INV-ACCT-UNIT-QTY ( #I ) NE 0.000
            {
                pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                           //Natural: ASSIGN #INV-ACCT-UNIT-QTY-CV ( #DISP-IDX ) := ( AD = I )
                pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Pnd_I));                        //Natural: ASSIGN #INV-ACCT-UNIT-QTY ( #DISP-IDX ) := EXT.INV-ACCT-UNIT-QTY ( #I )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.0000"))))                                            //Natural: IF EXT.INV-ACCT-UNIT-VALUE ( #I ) NE 0.0000
            {
                pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv.getValue(pnd_Ws_Pnd_Disp_Idx).setValue("AD=I");                                                                         //Natural: ASSIGN #INV-ACCT-UNIT-VALUE-CV ( #DISP-IDX ) := ( AD = I )
                pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(pnd_Ws_Pnd_Disp_Idx).setValue(pdaFcpaext.getExt_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_I));                    //Natural: ASSIGN #INV-ACCT-UNIT-VALUE ( #DISP-IDX ) := EXT.INV-ACCT-UNIT-VALUE ( #I )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Tax_Info() throws Exception                                                                                                                  //Natural: DISPLAY-TAX-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(pdaFcpaext.getExt_Inv_Acct_Ivc_Ind().getValue(1).equals("2")))                                                                                      //Natural: IF EXT.INV-ACCT-IVC-IND ( 1 ) = '2'
        {
            pnd_Ws_Pnd_Ivc_Ind.setValue(true);                                                                                                                            //Natural: ASSIGN #IVC-IND := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Ivc_Ind.setValue(false);                                                                                                                           //Natural: ASSIGN #IVC-IND := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpaext.getExt_Tax_Fed_C_Filing_Stat().equals("Z")))                                                                                             //Natural: IF EXT.TAX-FED-C-FILING-STAT = 'Z'
        {
            pnd_Ws_Pnd_Tax_Fed_C_Filing_Stat_Disp.setValue("MS");                                                                                                         //Natural: ASSIGN #TAX-FED-C-FILING-STAT-DISP := 'MS'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Tax_Fed_C_Filing_Stat_Disp.setValue(pdaFcpaext.getExt_Tax_Fed_C_Filing_Stat());                                                                    //Natural: ASSIGN #TAX-FED-C-FILING-STAT-DISP := EXT.TAX-FED-C-FILING-STAT
        }                                                                                                                                                                 //Natural: END-IF
        FOR02:                                                                                                                                                            //Natural: FOR #X FROM 1 TO 2
        for (pnd_X.setValue(1); condition(pnd_X.lessOrEqual(2)); pnd_X.nadd(1))
        {
            if (condition(pnd_X.equals(1)))                                                                                                                               //Natural: IF #X = 1 THEN
            {
                pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr.setValue(pdaFcpaext.getExt_Cntrct_Ppcn_Nbr());                                                            //Natural: ASSIGN #X-CNTRCT-NBR := EXT.CNTRCT-PPCN-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Nbr.setValue(pdaFcpaext.getExt_Cntrct_Cref_Nbr());                                                            //Natural: ASSIGN #X-CNTRCT-NBR := EXT.CNTRCT-CREF-NBR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Invrse_Dte.setValue(pdaFcpaext.getExt_Cntrct_Invrse_Dte());                                                       //Natural: ASSIGN #X-CNTRCT-INVRSE-DTE := EXT.CNTRCT-INVRSE-DTE
            pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Cntrct_Orgn_Cde.setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                           //Natural: ASSIGN #X-CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
            pnd_Cntrct_Inv_Orgn_Prcss_Inst_Pnd_X_Pymnt_Prcss_Seq_Nbr.setValue(pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr());                                                   //Natural: ASSIGN #X-PYMNT-PRCSS-SEQ-NBR := EXT.PYMNT-PRCSS-SEQ-NBR
            ldaFcplptax.getVw_pymnt_Tax().startDatabaseFind                                                                                                               //Natural: FIND ( 1 ) PYMNT-TAX WITH CNTRCT-INV-ORGN-PRCSS-INST = #CNTRCT-INV-ORGN-PRCSS-INST
            (
            "FIND01",
            new Wc[] { new Wc("CNTRCT_INV_ORGN_PRCSS_INST", "=", pnd_Cntrct_Inv_Orgn_Prcss_Inst, WcType.WITH) },
            1
            );
            FIND01:
            while (condition(ldaFcplptax.getVw_pymnt_Tax().readNextRow("FIND01")))
            {
                ldaFcplptax.getVw_pymnt_Tax().setIfNotFoundControlFlag(false);
                pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                           //Natural: ADD 1 TO #PAGE-NUMBER
                if (condition(getReports().getAstLinesLeft(1).less(19)))                                                                                                  //Natural: EJECT ( 1 ) IF LESS THAN 19 LINES LEFT
                {
                    getReports().eject(1);
                }
                pdaTxwa2430.getTxwa2430_Pnd_Filling_St_Cde().setValue(ldaFcplptax.getPymnt_Tax_Tax_Fed_Filing_Stat());                                                    //Natural: ASSIGN TXWA2430.#FILLING-ST-CDE = PYMNT-TAX.TAX-FED-FILING-STAT
                DbsUtil.callnat(Txwn2430.class , getCurrentProcessState(), pdaTxwa2430.getTxwa2430());                                                                    //Natural: CALLNAT 'TXWN2430' TXWA2430
                if (condition(Global.isEscape())) return;
                pnd_Pmnt_Tax_Tax_Fed_Filing_Stat_Desc.setValue(pdaTxwa2430.getTxwa2430_Pnd_Status_Short());                                                               //Natural: MOVE TXWA2430.#STATUS-SHORT TO #PMNT-TAX.TAX-FED-FILING-STAT-DESC
                pdaTxwa2430.getTxwa2430_Pnd_Filling_St_Cde().setValue(ldaFcplptax.getPymnt_Tax_Tax_Sta_Filing_Stat());                                                    //Natural: ASSIGN TXWA2430.#FILLING-ST-CDE = PYMNT-TAX.TAX-STA-FILING-STAT
                DbsUtil.callnat(Txwn2430.class , getCurrentProcessState(), pdaTxwa2430.getTxwa2430());                                                                    //Natural: CALLNAT 'TXWN2430' TXWA2430
                if (condition(Global.isEscape())) return;
                pnd_Pmnt_Tax_Tax_Sta_Filing_Stat_Desc.setValue(pdaTxwa2430.getTxwa2430_Pnd_Status_Short());                                                               //Natural: MOVE TXWA2430.#STATUS-SHORT TO #PMNT-TAX.TAX-STA-FILING-STAT-DESC
                pdaTxwa2430.getTxwa2430_Pnd_Filling_St_Cde().setValue(ldaFcplptax.getPymnt_Tax_Tax_Loc_Filing_Stat());                                                    //Natural: ASSIGN TXWA2430.#FILLING-ST-CDE = PYMNT-TAX.TAX-LOC-FILING-STAT
                DbsUtil.callnat(Txwn2430.class , getCurrentProcessState(), pdaTxwa2430.getTxwa2430());                                                                    //Natural: CALLNAT 'TXWN2430' TXWA2430
                if (condition(Global.isEscape())) return;
                pnd_Pmnt_Tax_Tax_Loc_Filing_Stat_Desc.setValue(pdaTxwa2430.getTxwa2430_Pnd_Status_Short());                                                               //Natural: MOVE TXWA2430.#STATUS-SHORT TO #PMNT-TAX.TAX-LOC-FILING-STAT-DESC
                if (condition(getReports().getAstLinesLeft(1).less(19)))                                                                                                  //Natural: EJECT ( 1 ) IF LESS THAN 19 LINES LEFT
                {
                    getReports().eject(1);
                }
                if (condition(getReports().getAstLinesLeft(1).less(19)))                                                                                                  //Natural: EJECT ( 1 ) IF LESS THAN 19 LINES LEFT
                {
                    getReports().eject(1);
                }
                if (condition(getReports().getAstLinesLeft(1).less(19)))                                                                                                  //Natural: EJECT ( 1 ) IF LESS THAN 19 LINES LEFT
                {
                    getReports().eject(1);
                }
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872d.class));                                                                      //Natural: WRITE ( 1 ) NOTITLE USING MAP 'FCPM872D'
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872i.class));                                                                      //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM872I'
            }                                                                                                                                                             //Natural: END-FIND
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //* IF NOT NZ-EXT.RTB                                                  /*
        if (condition(! (rtb.getBoolean())))                                                                                                                              //Natural: IF NOT RTB
        {
            pnd_Ws_Pnd_First_Periodic.setValue(false);                                                                                                                    //Natural: ASSIGN #FIRST-PERIODIC := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Rtb_Ac_Info() throws Exception                                                                                                               //Natural: DISPLAY-RTB-AC-INFO
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        //* IF #ANNUITY-CERTAIN OR NZ-EXT.RTB OR NZ-EXT.IVC-FROM-RTB-ROLLOVER  /*
        if (condition(pnd_Ws_Pnd_Annuity_Certain.getBoolean() || rtb.getBoolean() || ivc_From_Rtb_Rollover.getBoolean()))                                                 //Natural: IF #ANNUITY-CERTAIN OR RTB OR IVC-FROM-RTB-ROLLOVER
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        //* IF NZ-EXT.RTB OR NZ-EXT.IVC-FROM-RTB-ROLLOVER                      /*
        if (condition(rtb.getBoolean() || ivc_From_Rtb_Rollover.getBoolean()))                                                                                            //Natural: IF RTB OR IVC-FROM-RTB-ROLLOVER
        {
            pnd_Ws_Pnd_Rtb_Percent_Cv.setValue("AD=I");                                                                                                                   //Natural: ASSIGN #RTB-PERCENT-CV := ( AD = I )
            if (condition(pdaFcpaext.getExt_Pymnt_Rqst_Amt().equals(new DbsDecimal("0.00"))))                                                                             //Natural: IF EXT.PYMNT-RQST-AMT = 0.00
            {
                pnd_Ws_Pnd_Rtb_Percent.setValue(0);                                                                                                                       //Natural: ASSIGN #RTB-PERCENT := 0.0000
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ADD DVDND TO THE SETTL-AMT                /* ROXAN 12/10         /*
                //*   ADD  NZ-EXT.INV-ACCT-SETTL-AMT(1:C-INV-ACCT) TO #TEMP-AMT       /*
                pnd_Ws_Pnd_Temp_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                                          //Natural: ADD EXT.INV-ACCT-SETTL-AMT ( 1:C-INV-ACCT ) TO #TEMP-AMT
                pnd_Ws_Pnd_Temp_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Dvdnd_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                                          //Natural: ADD EXT.INV-ACCT-DVDND-AMT ( 1:C-INV-ACCT ) TO #TEMP-AMT
                pnd_Ws_Pnd_Rtb_Percent.compute(new ComputeParameters(false, pnd_Ws_Pnd_Rtb_Percent), pnd_Ws_Pnd_Temp_Amt.divide(pdaFcpaext.getExt_Pymnt_Rqst_Amt()).multiply(100)); //Natural: ASSIGN #RTB-PERCENT := #TEMP-AMT / EXT.PYMNT-RQST-AMT * 100
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Rollover_Ind_Cv.setValue("AD=I");                                                                                                                      //Natural: ASSIGN #ROLLOVER-IND-CV := ( AD = I )
        if (condition(pnd_Ws_Pnd_Internal_Rollover.getBoolean()))                                                                                                         //Natural: IF #INTERNAL-ROLLOVER
        {
            pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv.setValue("AD=I");                                                                                                            //Natural: ASSIGN #PYMNT-EFT-ACCT-NBR-CV := ( AD = I )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv.resetInitial();                                                                                                              //Natural: RESET INITIAL #PYMNT-EFT-ACCT-NBR-CV
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pdaFcpaext.getExt_Cntrct_Ac_Lt_10yrs().getBoolean() || (rtb.getBoolean() && ! (pnd_Ws_Pnd_Rollover_Ind.getBoolean())))))                           //Natural: IF EXT.CNTRCT-AC-LT-10YRS OR RTB AND NOT #ROLLOVER-IND
        {
            pnd_Ws_Pnd_20_Per_Withold_Cv.setValue("AD=I");                                                                                                                //Natural: ASSIGN #20-PER-WITHOLD-CV := ( AD = I )
            pnd_Ws_Pnd_20_Per_Withold.setValue(true);                                                                                                                     //Natural: ASSIGN #20-PER-WITHOLD := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PAGE-NUMBER
        if (condition(getReports().getAstLinesLeft(1).less(12)))                                                                                                          //Natural: EJECT ( 1 ) IF LESS THAN 12 LINES LEFT
        {
            getReports().eject(1);
        }
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872c.class));                                                                              //Natural: WRITE ( 1 ) NOTITLE USING MAP 'FCPM872C'
        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872h.class));                                                                              //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM872H'
        pnd_Ws_Pnd_Rtb_Tax_Info.resetInitial();                                                                                                                           //Natural: RESET INITIAL #RTB-TAX-INFO
    }
    private void sub_Init_Ledger_Page() throws Exception                                                                                                                  //Natural: INIT-LEDGER-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Ws_Pnd_L_Idx.resetInitial();                                                                                                                                  //Natural: RESET INITIAL #L-IDX #PREV-LEDGER-GROUP #LINE-COUNT ( * ) #LEDGER-DISP-ARRAY ( * )
        pnd_Ws_Pnd_Prev_Ledger_Group.resetInitial();
        pnd_Ws_Pnd_Line_Count.getValue("*").resetInitial();
        pnd_Ws_Pnd_Ledger_Disp_Array.getValue("*").resetInitial();
    }
    //*  ROXAN
    private void sub_Process_Ledger() throws Exception                                                                                                                    //Natural: PROCESS-LEDGER
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl871.getPnd_Gl_Rec_Pnd_Gl_Rec_Ext().getValue("*").setValue(pdaFcpaext.getExt().getValue(1,":",2));                                                          //Natural: ASSIGN #GL-REC-EXT ( * ) := EXT ( 1:2 )
        if (condition(pnd_Ws_Pnd_L_Idx.equals(15)))                                                                                                                       //Natural: IF #L-IDX = 15
        {
            pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PAGE-NUMBER
            if (condition(getReports().getAstLinesLeft(1).less(22)))                                                                                                      //Natural: EJECT ( 1 ) IF LESS THAN 22 LINES LEFT
            {
                getReports().eject(1);
            }
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872b.class));                                                                          //Natural: WRITE ( 1 ) NOTITLE USING MAP 'FCPM872B'
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872g.class));                                                                          //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM872G'
                                                                                                                                                                          //Natural: PERFORM INIT-LEDGER-PAGE
            sub_Init_Ledger_Page();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_L_Idx.notEquals(getZero()) && ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Ledgr_Nbr().equals(pnd_Ws_Pnd_Prev_Ledger) && ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Ledgr_Ind().equals(pnd_Ws_Pnd_Prev_Ledger_Ind)  //Natural: IF #L-IDX NE 0 AND #GL-REC.INV-ACCT-LEDGR-NBR = #PREV-LEDGER AND #GL-REC.INV-ACCT-LEDGR-IND = #PREV-LEDGER-IND AND #GL-REC.INV-ACCT-CDE = #PREV-LEDGER-HASH
            && ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Cde().equals(pnd_Ws_Pnd_Prev_Ledger_Hash)))
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_L_Idx.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #L-IDX
            pnd_Ws_Pnd_Line_Count.getValue(pnd_Ws_Pnd_L_Idx).compute(new ComputeParameters(false, pnd_Ws_Pnd_Line_Count.getValue(pnd_Ws_Pnd_L_Idx)), pnd_Ws_Pnd_L_Idx.add(5)); //Natural: ASSIGN #LINE-COUNT ( #L-IDX ) := #L-IDX + 5
            pnd_Ws_Pnd_Prev_Ledger.setValue(ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Ledgr_Nbr());                                                                               //Natural: ASSIGN #PREV-LEDGER := #GL-REC.INV-ACCT-LEDGR-NBR
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Ledgr_Nbr());                                                       //Natural: ASSIGN #FCPA121.INV-ACCT-LEDGR-NBR := #GL-REC.INV-ACCT-LEDGR-NBR
            pnd_Ws_Pnd_Prev_Ledger_Ind.setValue(ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Ledgr_Ind());                                                                           //Natural: ASSIGN #PREV-LEDGER-IND := #GL-REC.INV-ACCT-LEDGR-IND
            pnd_Ws_Pnd_Prev_Ledger_Hash.setValue(ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Cde());                                                                                //Natural: ASSIGN #PREV-LEDGER-HASH := #GL-REC.INV-ACCT-CDE
            pnd_Ws_Pnd_Disp_Ledger_Nbr.getValue(pnd_Ws_Pnd_L_Idx).setValue(ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Ledgr_Nbr());                                                //Natural: ASSIGN #DISP-LEDGER-NBR ( #L-IDX ) := #GL-REC.INV-ACCT-LEDGR-NBR
            pnd_Ws_Pnd_Disp_Ledger_Isa.getValue(pnd_Ws_Pnd_L_Idx).setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Prev_L_Hash.getInt()              //Natural: ASSIGN #DISP-LEDGER-ISA ( #L-IDX ) := #ISA-LDA.#ISA-CDE ( #PREV-L-HASH )
                + 1));
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Isa().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Prev_L_Hash.getInt() + 1));                    //Natural: ASSIGN #FCPA121.INV-ACCT-ISA := #ISA-LDA.#ISA-CDE ( #PREV-L-HASH )
            DbsUtil.callnat(Fcpn121.class , getCurrentProcessState(), pdaFcpa121.getPnd_Fcpa121());                                                                       //Natural: CALLNAT 'FCPN121' USING #FCPA121
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.getValue(pnd_Ws_Pnd_L_Idx).setValue(pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc());                                         //Natural: ASSIGN #DISP-ACCT-LEDGR-DESC ( #L-IDX ) := #FCPA121.INV-ACCT-LEDGR-DESC
            if (condition(pnd_Ws_Pnd_Prev_Ledger_Ind.equals("C")))                                                                                                        //Natural: IF #PREV-LEDGER-IND = 'C'
            {
                pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(pnd_Ws_Pnd_L_Idx).setValue("CR");                                                                                 //Natural: ASSIGN #DISP-ACCT-LEDGR-IND ( #L-IDX ) := 'CR'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.getValue(pnd_Ws_Pnd_L_Idx).setValue("DR");                                                                                 //Natural: ASSIGN #DISP-ACCT-LEDGR-IND ( #L-IDX ) := 'DR'
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt_Cv.getValue(pnd_Ws_Pnd_L_Idx).setValue("AD=I");                                                                                //Natural: ASSIGN #DISP-ACCT-LEDGR-AMT-CV ( #L-IDX ) := ( AD = I )
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.getValue(pnd_Ws_Pnd_L_Idx).nadd(ldaFcpl871.getPnd_Gl_Rec_Inv_Acct_Ledgr_Amt());                                                    //Natural: ADD #GL-REC.INV-ACCT-LEDGR-AMT TO #DISP-ACCT-LEDGR-AMT ( #L-IDX )
    }
    private void sub_Write_End_Line() throws Exception                                                                                                                    //Natural: WRITE-END-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*  PIN EXPANSION
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(81),pnd_Key_Efm_Pymnt_Reqst_Log_Dte_Time,pnd_Key_Efm_Cntrct_Orgn_Cde,pnd_Key_Efm_Cntrct_Unq_Id_Nbr,     //Natural: WRITE ( 2 ) 81T #KEY-EFM.PYMNT-REQST-LOG-DTE-TIME #KEY-EFM.CNTRCT-ORGN-CDE #KEY-EFM.CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) '99' #KEY-EFM.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Page_Number, new ReportEditMask ("99999"),"99",pnd_Key_Efm_Cntrct_Cancel_Rdrw_Actvty_Cde);
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE FCPA110.START-CHECK-PREFIX-N3 TO #CHECK-NUMBER-N3          /*RL
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Key_Detail_Pnd_KeyIsBreak = pnd_Key_Detail_Pnd_Key.isBreak(endOfData);
        if (condition(pnd_Key_Detail_Pnd_KeyIsBreak))
        {
            //*  ----------------
            if (condition(pnd_Ws_Pnd_L_Idx.notEquals(getZero())))                                                                                                         //Natural: IF #L-IDX NE 0
            {
                pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                           //Natural: ADD 1 TO #PAGE-NUMBER
                if (condition(getReports().getAstLinesLeft(1).less(22)))                                                                                                  //Natural: EJECT ( 1 ) IF LESS THAN 22 LINES LEFT
                {
                    getReports().eject(1);
                }
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872b.class));                                                                      //Natural: WRITE ( 1 ) NOTITLE USING MAP 'FCPM872B'
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm872g.class));                                                                      //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM872G'
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-END-LINE
            sub_Write_End_Line();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*  NOT EOF
            if (condition(pnd_Key_Detail_Pnd_Key.notEquals(readWork01Pnd_KeyOld)))                                                                                        //Natural: IF #KEY NE OLD ( #KEY )
            {
                                                                                                                                                                          //Natural: PERFORM NEW-PAYMENT
                sub_New_Payment();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=20 LS=124 ZP=ON");
    }
    private void CheckAtStartofData1038() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*  ----------------
            //*  MOVED INSIDE NEW-PAYMNT SUBROUTINE                   /*  ROXAN 01/07
            //*    IF EXT.CNTRCT-ORGN-CDE = 'EW'                         /*
            //*      #SCREEN-HEADER   :=
            //*        'ELECTRONIC WARRANT PAYMENT WARRENT'
            //*      PERFORM WHAT-LOB
            //*      #SCREEN-TRAILER        :=
            //*       '* SEE ELECTRONIC WARRANT TEXT DOCUMENT FOR COMPLETE FINANCIAL INFORMATION *'
            //*      #IA-DA-CONTRACT  :=  'IA'
            //*   ELSE
            if (condition(pdaFcpaext.getExt_Cntrct_Orgn_Cde().equals("NZ")))                                                                                              //Natural: IF EXT.CNTRCT-ORGN-CDE = 'NZ'
            {
                pnd_Screen_Header.setValue("  Annuitization Payment Warrant ");                                                                                           //Natural: ASSIGN #SCREEN-HEADER := '  Annuitization Payment Warrant '
                pnd_Screen_Header2.setValue("Retirement Transition Benefit &/or Initial Periodic Payment");                                                               //Natural: ASSIGN #SCREEN-HEADER2 := 'Retirement Transition Benefit &/or Initial Periodic Payment'
                pnd_Ia_Da_Contract.setValue("IA");                                                                                                                        //Natural: ASSIGN #IA-DA-CONTRACT := 'IA'
                //*   'AL'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Screen_Header.setValue("      Annuity Loan Warrant     ");                                                                                            //Natural: ASSIGN #SCREEN-HEADER := '      Annuity Loan Warrant     '
                pnd_Screen_Header2.setValue("  ");                                                                                                                        //Natural: ASSIGN #SCREEN-HEADER2 := '  '
                pnd_Ia_Da_Contract.setValue("DA");                                                                                                                        //Natural: ASSIGN #IA-DA-CONTRACT := 'DA'
            }                                                                                                                                                             //Natural: END-IF
            //*   MOVE  BY NAME  PYMNT-ADDR-INFO  TO  #KEY-DETAIL            /* ROXAN
            pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                                     //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
                                                                                                                                                                          //Natural: PERFORM NEW-PAYMENT
            sub_New_Payment();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
