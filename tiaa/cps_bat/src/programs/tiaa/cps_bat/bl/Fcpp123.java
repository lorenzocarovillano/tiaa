/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:09 PM
**        * FROM NATURAL PROGRAM : Fcpp123
************************************************************
**        * FILE NAME            : Fcpp123.java
**        * CLASS NAME           : Fcpp123
**        * INSTANCE NAME        : Fcpp123
************************************************************
************************************************************************
* PROGRAM  : FCPP123
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS PAYMENT WARRANT REPORT
* CREATED  : 08/24/94
* FUNCTION : THIS PROGRAM READS SORTED EXTRACT PRODUCED BY FCPP122
*            AND PRODUCES PAYMENT WARRANT REPORT.
*
* 07/23/96 L. ZHENG   - INCLUDED MDO IN THE REPORTS
* 04/01/97 : RITA SALGADO
*          : GET LEDGER DESCRIPTION USING ISA NBR INSTEAD OF FUND CODE
*
* 06/23/1998 - RIAD LOUTFI - ADD LOGIC FOR PROCESSING CLASSIC AND ROTH
*              IRA ROLLOVERS.
*
* 02/12/1999 - JCARMON     - ADDED LOGIC FOR PROCESSING MULTIPLE FUNDS
*              INTO IA DEATH.
*
* 06/03/99   - ROXAN CARREON - FIX OVERPAYMENT AMOUNT
*
* 08/05/99   - LEON GURTOVNIK
*              MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*              NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
* 01/07/2000 - LEON GURTOVNIK
*              RESTOW TO ACCEPT CHNG IN FCPL236 (ADDED 25TH OCCURANCE)
*
* 09/11/2000 : ALTHEA A. YOUNG
*              - REVISED #TYPE-ARRAY TO INCLUDE 'LUMP SUM PAYMENT'.
*              - DEFINED VARIABLE #INSTALLMENT-HDR TO ACCOMMODATE
*                MULTIPLE INSTALLMENT COLUMN HEADINGS.
*              - REVISED SUBROUTINES INIT-PAGE-2 AND PROCESS-IA-DEATH-
*                PAGE-2 TO POPULATE AND RESET VARIABLE ACCORDINGLY.
*              - REVISED UNIT QTY. AND UNIT VAL. PROCESSING SO THAT
*                BLANKS WILL BE PRINTED FOR LUMP-SUMS ONLY.
*
* 03/06/2006 : LANDRUM  PAYEE MATCH
*              - INCREASED CHECK NBR TO N10.
* 04/2017    : JJG - PIN EXPANSION
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp123 extends BLNatBase
{
    // Data Areas
    private PdaTbldcoda pdaTbldcoda;
    private PdaFcpa121 pdaFcpa121;
    private PdaFcpa199a pdaFcpa199a;
    private LdaFcpl199b ldaFcpl199b;
    private LdaFcpl123 ldaFcpl123;
    private LdaFcpl236 ldaFcpl236;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Program;
    private DbsField pnd_Ws_Pnd_Timestmp;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Timestmp_A;

    private DbsGroup pnd_Ws_Pnd_Eft_Line;

    private DbsGroup pnd_Ws_Pnd_Eft_Line_Cvs;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text;
    private DbsField pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Pnd_Ctznshp_Display;
    private DbsField pnd_Ws_Pnd_Currency_Display;
    private DbsField pnd_Ws_Pnd_Fed_Display;
    private DbsField pnd_Ws_Pnd_State_Display;
    private DbsField pnd_Ws_Pnd_Mode_Display;
    private DbsField pnd_Ws_Pnd_Orgn_Display;
    private DbsField pnd_Ws_Pnd_Orgn_Array;
    private DbsField pnd_Ws_Pnd_Type_Display;
    private DbsField pnd_Ws_Pnd_Type_Array;
    private DbsField pnd_Ws_Pnd_Pymnt_Display;
    private DbsField pnd_Ws_Pnd_Pymnt_Array;
    private DbsField pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Pnd_Csr_Display;
    private DbsField pnd_Ws_Pnd_Csr_Array;

    private DbsGroup pnd_Ws_Pnd_Inv_Acct;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cde_Desc;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Value_A;

    private DbsGroup pnd_Ws__R_Field_3;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dpi_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dci_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Exp_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Pnd_Pymnt_Settlmnt_Dte_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Pnd_Installment_Hdr;

    private DbsGroup pnd_Ws_Pnd_Pymnt_Ded_Grp;

    private DbsGroup pnd_Ws_Pnd_Pymnt_Ded_Grp_Init;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Cde_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Amt_Cv;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_10;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Amt_10;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Amt;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Col;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Row;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Cde_10;
    private DbsField pnd_Ws_Pnd_Prev_Ledger;
    private DbsField pnd_Ws_Pnd_Prev_Ledger_Ind;

    private DbsGroup pnd_Ws_Pnd_Ledger_Disp_Array;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Nbr;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind;
    private DbsField pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc;
    private DbsField pnd_Ws_Pnd_Inv_Acct_Sub;
    private DbsField pnd_Ws_Pnd_Disp_Sub;
    private DbsField pnd_Ws_Pnd_Orgn_Sub;
    private DbsField pnd_Ws_Pnd_Ded_Sub;
    private DbsField pnd_Ws_Pnd_Ded_Top;
    private DbsField pnd_Ws_Pnd_Type_Sub;
    private DbsField pnd_Ws_Pnd_Csr_Sub;
    private DbsField pnd_Ws_Pnd_Col;
    private DbsField pnd_Ws_Pnd_Row;
    private DbsField pnd_Ws_Pnd_Records_Read;
    private DbsField pnd_Ws_Pnd_Line_Count;
    private DbsField pnd_Ws_Pnd_Page_Number;
    private DbsField pnd_Ws_Pnd_Run_Time;

    private DbsGroup pnd_Ws_Tbldcoda_Msg;
    private DbsField pnd_Ws_Pnd_Pnd_Msg;
    private DbsField pnd_Ws_Pnd_Pnd_Msg_Nr;
    private DbsField pnd_Ws_Pnd_Pnd_Msg_Data;
    private DbsField pnd_Ws_Pnd_Pnd_Return_Code;
    private DbsField pnd_Ws_Pnd_Pnd_Error_Field;
    private DbsField pnd_Ws_Pnd_Pnd_Error_Field_Index1;
    private DbsField pnd_Ws_Pnd_Pnd_Error_Field_Index2;
    private DbsField pnd_Ws_Pnd_Pnd_Error_Field_Index3;
    private DbsField pnd_Ws_Pnd_Pnd_Inv_Acct_Cde_A;

    private DbsGroup pnd_Ws__R_Field_4;
    private DbsField pnd_Ws_Pnd_Pnd_Inv_Acct_Cde;
    private DbsField pnd_Len;
    private DbsField pnd_W_Fld;
    private DbsField pnd_Lob_Cde;
    private DbsField pnd_Lob_Sub;
    private DbsField pnd_Pymnt_Sttlmnt_Type;

    private DbsGroup pnd_Pymnt_Sttlmnt_Type__R_Field_5;
    private DbsField pnd_Pymnt_Sttlmnt_Type_Pnd_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Pymnt_Sttlmnt_Type_Pnd_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Check_Number_A11;

    private DbsGroup pnd_Check_Number_A11__R_Field_6;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Letter_A1;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Number_N10;

    private DbsGroup pnd_Check_Number_A11__R_Field_7;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Number_N3;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Number_N7;

    private DbsGroup pnd_Check_Number_A11__R_Field_8;
    private DbsField pnd_Check_Number_A11_Pnd_Check_Number_A10;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Record_TypeOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaTbldcoda = new PdaTbldcoda(localVariables);
        pdaFcpa121 = new PdaFcpa121(localVariables);
        pdaFcpa199a = new PdaFcpa199a(localVariables);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);
        ldaFcpl123 = new LdaFcpl123();
        registerRecord(ldaFcpl123);
        ldaFcpl236 = new LdaFcpl236();
        registerRecord(ldaFcpl236);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Program = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Ws_Pnd_Timestmp = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Timestmp", "#TIMESTMP", FieldType.BINARY, 8);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Timestmp);
        pnd_Ws_Pnd_Timestmp_A = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Timestmp_A", "#TIMESTMP-A", FieldType.STRING, 8);

        pnd_Ws_Pnd_Eft_Line = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Eft_Line", "#EFT-LINE");

        pnd_Ws_Pnd_Eft_Line_Cvs = pnd_Ws_Pnd_Eft_Line.newGroupInGroup("pnd_Ws_Pnd_Eft_Line_Cvs", "#EFT-LINE-CVS");
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv", "#PYMNT-EFT-TRANSIT-ID-TEXT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv", "#PYMNT-EFT-TRANSIT-ID-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv", "#PYMNT-EFT-ACCT-NBR-TEXT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv", "#PYMNT-EFT-ACCT-NBR-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv", "#PYMNT-CHK-SAV-IND-TEXT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv = pnd_Ws_Pnd_Eft_Line_Cvs.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv", "#PYMNT-CHK-SAV-IND-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text = pnd_Ws_Pnd_Eft_Line.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text", "#PYMNT-EFT-TRANSIT-ID-TEXT", 
            FieldType.STRING, 16);
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text = pnd_Ws_Pnd_Eft_Line.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text", "#PYMNT-EFT-ACCT-NBR-TEXT", FieldType.STRING, 
            8);
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text = pnd_Ws_Pnd_Eft_Line.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text", "#PYMNT-CHK-SAV-IND-TEXT", FieldType.STRING, 
            14);
        pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time", "#PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 
            15);
        pnd_Ws_Pnd_Cntrct_Orgn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr", "#CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        pnd_Ws_Pnd_Ctznshp_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ctznshp_Display", "#CTZNSHP-DISPLAY", FieldType.STRING, 9);
        pnd_Ws_Pnd_Currency_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Currency_Display", "#CURRENCY-DISPLAY", FieldType.STRING, 4);
        pnd_Ws_Pnd_Fed_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fed_Display", "#FED-DISPLAY", FieldType.STRING, 3);
        pnd_Ws_Pnd_State_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_State_Display", "#STATE-DISPLAY", FieldType.STRING, 2);
        pnd_Ws_Pnd_Mode_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mode_Display", "#MODE-DISPLAY", FieldType.STRING, 18);
        pnd_Ws_Pnd_Orgn_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Orgn_Display", "#ORGN-DISPLAY", FieldType.STRING, 43);
        pnd_Ws_Pnd_Orgn_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Orgn_Array", "#ORGN-ARRAY", FieldType.STRING, 27, new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Type_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Type_Display", "#TYPE-DISPLAY", FieldType.STRING, 56);
        pnd_Ws_Pnd_Type_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Type_Array", "#TYPE-ARRAY", FieldType.STRING, 56, new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Pymnt_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Display", "#PYMNT-DISPLAY", FieldType.STRING, 11);
        pnd_Ws_Pnd_Pymnt_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Array", "#PYMNT-ARRAY", FieldType.STRING, 11, new DbsArrayController(1, 
            8));
        pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde", "#CNTRCT-CANCEL-RDRW-ACTVTY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Pnd_Csr_Display = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Csr_Display", "#CSR-DISPLAY", FieldType.STRING, 18);
        pnd_Ws_Pnd_Csr_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Csr_Array", "#CSR-ARRAY", FieldType.STRING, 18, new DbsArrayController(1, 7));

        pnd_Ws_Pnd_Inv_Acct = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Inv_Acct", "#INV-ACCT", new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv", "#INV-ACCT-CDE-DESC-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Cde_Desc", "#INV-ACCT-CDE-DESC", FieldType.STRING, 8);
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv", "#INV-ACCT-SETTL-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Settl_Amt", "#INV-ACCT-SETTL-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv", "#INV-ACCT-CNTRCT-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt", "#INV-ACCT-CNTRCT-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv", "#INV-ACCT-DVDND-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt", "#INV-ACCT-DVDND-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv", "#INV-ACCT-UNIT-QTY-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A", "#INV-ACCT-UNIT-QTY-A", FieldType.STRING, 
            12);

        pnd_Ws__R_Field_2 = pnd_Ws_Pnd_Inv_Acct.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A);
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Qty", "#INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv", "#INV-ACCT-UNIT-VALUE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Unit_Value_A = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Value_A", "#INV-ACCT-UNIT-VALUE-A", FieldType.STRING, 
            12);

        pnd_Ws__R_Field_3 = pnd_Ws_Pnd_Inv_Acct.newGroupInGroup("pnd_Ws__R_Field_3", "REDEFINE", pnd_Ws_Pnd_Inv_Acct_Unit_Value_A);
        pnd_Ws_Pnd_Inv_Acct_Unit_Value = pnd_Ws__R_Field_3.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Unit_Value", "#INV-ACCT-UNIT-VALUE", FieldType.PACKED_DECIMAL, 
            9, 4);
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv", "#INV-ACCT-IVC-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Ivc_Amt", "#INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Dpi_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dpi_Amt_Cv", "#INV-ACCT-DPI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Dpi_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dpi_Amt", "#INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Dci_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dci_Amt_Cv", "#INV-ACCT-DCI-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Dci_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Dci_Amt", "#INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Exp_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Exp_Amt_Cv", "#INV-ACCT-EXP-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Inv_Acct_Exp_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Exp_Amt", "#INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt_Cv", "#INV-ACCT-FDRL-TAX-AMT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt", "#INV-ACCT-FDRL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt_Cv", "#INV-ACCT-STATE-TAX-AMT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt", "#INV-ACCT-STATE-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt_Cv", "#INV-ACCT-LOCAL-TAX-AMT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt", "#INV-ACCT-LOCAL-TAX-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt_Cv", "#INV-ACCT-NET-PYMNT-AMT-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt", "#INV-ACCT-NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Pymnt_Settlmnt_Dte_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Settlmnt_Dte_Cv", "#PYMNT-SETTLMNT-DTE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Pymnt_Settlmnt_Dte = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Settlmnt_Dte", "#PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt_Cv = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt_Cv", "##PYMNT-DED-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2);
        pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt = pnd_Ws_Pnd_Inv_Acct.newFieldInGroup("pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt", "##PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc", "#INV-ACCT-CDE-DESC-DC", FieldType.STRING, 11, 
            new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Inv_Acct_Valuat_Period = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Valuat_Period", "#INV-ACCT-VALUAT-PERIOD", FieldType.STRING, 3);
        pnd_Ws_Pnd_Installment_Hdr = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Installment_Hdr", "#INSTALLMENT-HDR", FieldType.STRING, 14, new DbsArrayController(1, 
            4));

        pnd_Ws_Pnd_Pymnt_Ded_Grp = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Pymnt_Ded_Grp", "#PYMNT-DED-GRP");

        pnd_Ws_Pnd_Pymnt_Ded_Grp_Init = pnd_Ws_Pnd_Pymnt_Ded_Grp.newGroupInGroup("pnd_Ws_Pnd_Pymnt_Ded_Grp_Init", "#PYMNT-DED-GRP-INIT");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_Cv = pnd_Ws_Pnd_Pymnt_Ded_Grp_Init.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Cde_Cv", "#PYMNT-DED-CDE-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 2, 1, 5));
        pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_Cv = pnd_Ws_Pnd_Pymnt_Ded_Grp_Init.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_Cv", "#PYMNT-DED-PAYEE-CDE-CV", 
            FieldType.ATTRIBUTE_CONTROL, 2, new DbsArrayController(1, 2, 1, 5));
        pnd_Ws_Pnd_Pymnt_Ded_Amt_Cv = pnd_Ws_Pnd_Pymnt_Ded_Grp_Init.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Amt_Cv", "#PYMNT-DED-AMT-CV", FieldType.ATTRIBUTE_CONTROL, 
            2, new DbsArrayController(1, 2, 1, 5));
        pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_10 = pnd_Ws_Pnd_Pymnt_Ded_Grp_Init.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_10", "#PYMNT-DED-PAYEE-CDE-10", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ws_Pnd_Pymnt_Ded_Amt_10 = pnd_Ws_Pnd_Pymnt_Ded_Grp_Init.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Amt_10", "#PYMNT-DED-AMT-10", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 10));
        pnd_Ws_Pnd_Pymnt_Ded_Cde = pnd_Ws_Pnd_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Cde", "#PYMNT-DED-CDE", FieldType.STRING, 14, new 
            DbsArrayController(1, 2, 1, 5));
        pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde = pnd_Ws_Pnd_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde", "#PYMNT-DED-PAYEE-CDE", FieldType.STRING, 
            8, new DbsArrayController(1, 2, 1, 5));
        pnd_Ws_Pnd_Pymnt_Ded_Amt = pnd_Ws_Pnd_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Amt", "#PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 2, 1, 5));
        pnd_Ws_Pnd_Pymnt_Ded_Col = pnd_Ws_Pnd_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Col", "#PYMNT-DED-COL", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Pnd_Pymnt_Ded_Row = pnd_Ws_Pnd_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Row", "#PYMNT-DED-ROW", FieldType.PACKED_DECIMAL, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10 = pnd_Ws_Pnd_Pymnt_Ded_Grp.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Cde_10", "#PYMNT-DED-CDE-10", FieldType.STRING, 
            14, new DbsArrayController(1, 10));
        pnd_Ws_Pnd_Prev_Ledger = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Ledger", "#PREV-LEDGER", FieldType.STRING, 15);
        pnd_Ws_Pnd_Prev_Ledger_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Ledger_Ind", "#PREV-LEDGER-IND", FieldType.STRING, 1);

        pnd_Ws_Pnd_Ledger_Disp_Array = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Ledger_Disp_Array", "#LEDGER-DISP-ARRAY");
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Nbr = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Acct_Ledgr_Nbr", "#DISP-ACCT-LEDGR-NBR", FieldType.STRING, 
            15);
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt", "#DISP-ACCT-LEDGR-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind", "#DISP-ACCT-LEDGR-IND", FieldType.STRING, 
            6);
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc = pnd_Ws_Pnd_Ledger_Disp_Array.newFieldInGroup("pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc", "#DISP-ACCT-LEDGR-DESC", FieldType.STRING, 
            40);
        pnd_Ws_Pnd_Inv_Acct_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Inv_Acct_Sub", "#INV-ACCT-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Disp_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Sub", "#DISP-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Orgn_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Orgn_Sub", "#ORGN-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ded_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ded_Sub", "#DED-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ded_Top = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ded_Top", "#DED-TOP", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Type_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Type_Sub", "#TYPE-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Csr_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Csr_Sub", "#CSR-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Col = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Col", "#COL", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Row = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Row", "#ROW", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Records_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Records_Read", "#RECORDS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Line_Count = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Line_Count", "#LINE-COUNT", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_Page_Number = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Page_Number", "#PAGE-NUMBER", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_Run_Time = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Run_Time", "#RUN-TIME", FieldType.TIME);

        pnd_Ws_Tbldcoda_Msg = pnd_Ws.newGroupInGroup("pnd_Ws_Tbldcoda_Msg", "TBLDCODA-MSG");
        pnd_Ws_Pnd_Pnd_Msg = pnd_Ws_Tbldcoda_Msg.newFieldInGroup("pnd_Ws_Pnd_Pnd_Msg", "##MSG", FieldType.STRING, 79);
        pnd_Ws_Pnd_Pnd_Msg_Nr = pnd_Ws_Tbldcoda_Msg.newFieldInGroup("pnd_Ws_Pnd_Pnd_Msg_Nr", "##MSG-NR", FieldType.NUMERIC, 4);
        pnd_Ws_Pnd_Pnd_Msg_Data = pnd_Ws_Tbldcoda_Msg.newFieldArrayInGroup("pnd_Ws_Pnd_Pnd_Msg_Data", "##MSG-DATA", FieldType.STRING, 32, new DbsArrayController(1, 
            3));
        pnd_Ws_Pnd_Pnd_Return_Code = pnd_Ws_Tbldcoda_Msg.newFieldInGroup("pnd_Ws_Pnd_Pnd_Return_Code", "##RETURN-CODE", FieldType.STRING, 1);
        pnd_Ws_Pnd_Pnd_Error_Field = pnd_Ws_Tbldcoda_Msg.newFieldInGroup("pnd_Ws_Pnd_Pnd_Error_Field", "##ERROR-FIELD", FieldType.STRING, 32);
        pnd_Ws_Pnd_Pnd_Error_Field_Index1 = pnd_Ws_Tbldcoda_Msg.newFieldInGroup("pnd_Ws_Pnd_Pnd_Error_Field_Index1", "##ERROR-FIELD-INDEX1", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Pnd_Pnd_Error_Field_Index2 = pnd_Ws_Tbldcoda_Msg.newFieldInGroup("pnd_Ws_Pnd_Pnd_Error_Field_Index2", "##ERROR-FIELD-INDEX2", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Pnd_Pnd_Error_Field_Index3 = pnd_Ws_Tbldcoda_Msg.newFieldInGroup("pnd_Ws_Pnd_Pnd_Error_Field_Index3", "##ERROR-FIELD-INDEX3", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Ws_Pnd_Pnd_Inv_Acct_Cde_A = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pnd_Inv_Acct_Cde_A", "##INV-ACCT-CDE-A", FieldType.STRING, 2);

        pnd_Ws__R_Field_4 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_4", "REDEFINE", pnd_Ws_Pnd_Pnd_Inv_Acct_Cde_A);
        pnd_Ws_Pnd_Pnd_Inv_Acct_Cde = pnd_Ws__R_Field_4.newFieldInGroup("pnd_Ws_Pnd_Pnd_Inv_Acct_Cde", "##INV-ACCT-CDE", FieldType.NUMERIC, 2);
        pnd_Len = localVariables.newFieldInRecord("pnd_Len", "#LEN", FieldType.INTEGER, 2);
        pnd_W_Fld = localVariables.newFieldInRecord("pnd_W_Fld", "#W-FLD", FieldType.STRING, 43);
        pnd_Lob_Cde = localVariables.newFieldInRecord("pnd_Lob_Cde", "#LOB-CDE", FieldType.STRING, 4);
        pnd_Lob_Sub = localVariables.newFieldInRecord("pnd_Lob_Sub", "#LOB-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Pymnt_Sttlmnt_Type = localVariables.newFieldInRecord("pnd_Pymnt_Sttlmnt_Type", "#PYMNT-STTLMNT-TYPE", FieldType.STRING, 2);

        pnd_Pymnt_Sttlmnt_Type__R_Field_5 = localVariables.newGroupInRecord("pnd_Pymnt_Sttlmnt_Type__R_Field_5", "REDEFINE", pnd_Pymnt_Sttlmnt_Type);
        pnd_Pymnt_Sttlmnt_Type_Pnd_Cntrct_Pymnt_Type_Ind = pnd_Pymnt_Sttlmnt_Type__R_Field_5.newFieldInGroup("pnd_Pymnt_Sttlmnt_Type_Pnd_Cntrct_Pymnt_Type_Ind", 
            "#CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Pymnt_Sttlmnt_Type_Pnd_Cntrct_Sttlmnt_Type_Ind = pnd_Pymnt_Sttlmnt_Type__R_Field_5.newFieldInGroup("pnd_Pymnt_Sttlmnt_Type_Pnd_Cntrct_Sttlmnt_Type_Ind", 
            "#CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Check_Number_A11 = localVariables.newFieldInRecord("pnd_Check_Number_A11", "#CHECK-NUMBER-A11", FieldType.STRING, 11);

        pnd_Check_Number_A11__R_Field_6 = localVariables.newGroupInRecord("pnd_Check_Number_A11__R_Field_6", "REDEFINE", pnd_Check_Number_A11);
        pnd_Check_Number_A11_Pnd_Check_Letter_A1 = pnd_Check_Number_A11__R_Field_6.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Letter_A1", "#CHECK-LETTER-A1", 
            FieldType.STRING, 1);
        pnd_Check_Number_A11_Pnd_Check_Number_N10 = pnd_Check_Number_A11__R_Field_6.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Number_N10", "#CHECK-NUMBER-N10", 
            FieldType.NUMERIC, 10);

        pnd_Check_Number_A11__R_Field_7 = pnd_Check_Number_A11__R_Field_6.newGroupInGroup("pnd_Check_Number_A11__R_Field_7", "REDEFINE", pnd_Check_Number_A11_Pnd_Check_Number_N10);
        pnd_Check_Number_A11_Pnd_Check_Number_N3 = pnd_Check_Number_A11__R_Field_7.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Number_N3", "#CHECK-NUMBER-N3", 
            FieldType.NUMERIC, 3);
        pnd_Check_Number_A11_Pnd_Check_Number_N7 = pnd_Check_Number_A11__R_Field_7.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Number_N7", "#CHECK-NUMBER-N7", 
            FieldType.NUMERIC, 7);

        pnd_Check_Number_A11__R_Field_8 = pnd_Check_Number_A11__R_Field_6.newGroupInGroup("pnd_Check_Number_A11__R_Field_8", "REDEFINE", pnd_Check_Number_A11_Pnd_Check_Number_N10);
        pnd_Check_Number_A11_Pnd_Check_Number_A10 = pnd_Check_Number_A11__R_Field_8.newFieldInGroup("pnd_Check_Number_A11_Pnd_Check_Number_A10", "#CHECK-NUMBER-A10", 
            FieldType.STRING, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Record_TypeOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Record_Type_OLD", "Pnd_Record_Type_OLD", FieldType.NUMERIC, 
            1);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl199b.initializeValues();
        ldaFcpl123.initializeValues();
        ldaFcpl236.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv.setInitialAttributeValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text.setInitialValue("BANK TRANSIT ID:");
        pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text.setInitialValue("ACCOUNT:");
        pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text.setInitialValue("CHECK/SAVINGS:");
        pnd_Ws_Pnd_Orgn_Array.getValue(1).setInitialValue("SINGLE SUM");
        pnd_Ws_Pnd_Orgn_Array.getValue(2).setInitialValue("IA DEATH CLAIM");
        pnd_Ws_Pnd_Orgn_Array.getValue(3).setInitialValue("DAILY");
        pnd_Ws_Pnd_Orgn_Array.getValue(4).setInitialValue("MONTHLY");
        pnd_Ws_Pnd_Orgn_Array.getValue(5).setInitialValue("IA");
        pnd_Ws_Pnd_Orgn_Array.getValue(6).setInitialValue("MINIMUM DISTRIBUTION OPTION");
        pnd_Ws_Pnd_Orgn_Array.getValue(7).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Type_Array.getValue(1).setInitialValue("                   LUMP SUM PAYMENT");
        pnd_Ws_Pnd_Type_Array.getValue(2).setInitialValue("                   PERIODIC PAYMENT");
        pnd_Ws_Pnd_Type_Array.getValue(3).setInitialValue("                        UNKNOWN");
        pnd_Ws_Pnd_Pymnt_Array.getValue(1).setInitialValue("CHECK");
        pnd_Ws_Pnd_Pymnt_Array.getValue(2).setInitialValue("EFT");
        pnd_Ws_Pnd_Pymnt_Array.getValue(3).setInitialValue("GLOBAL");
        pnd_Ws_Pnd_Pymnt_Array.getValue(4).setInitialValue("WIRE");
        pnd_Ws_Pnd_Pymnt_Array.getValue(5).setInitialValue("CHECK/FEDEX");
        pnd_Ws_Pnd_Pymnt_Array.getValue(6).setInitialValue("SUSPENSE");
        pnd_Ws_Pnd_Pymnt_Array.getValue(7).setInitialValue("NON PAYMENT");
        pnd_Ws_Pnd_Pymnt_Array.getValue(8).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Csr_Array.getValue(1).setInitialValue(" ");
        pnd_Ws_Pnd_Csr_Array.getValue(2).setInitialValue("(REDRAW)");
        pnd_Ws_Pnd_Csr_Array.getValue(3).setInitialValue("(CANCEL)");
        pnd_Ws_Pnd_Csr_Array.getValue(4).setInitialValue(" (STOP)");
        pnd_Ws_Pnd_Csr_Array.getValue(5).setInitialValue("(CANCEL NO REDRAW)");
        pnd_Ws_Pnd_Csr_Array.getValue(6).setInitialValue("(STOP NO REDRAW)");
        pnd_Ws_Pnd_Csr_Array.getValue(7).setInitialValue("(UNKNOWN)");
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-CDE-DESC-CV(2)	(AD=N)#WS.#INV-ACCT-CDE-DESC-CV(3)	(AD=N)#WS.#INV-ACCT-CDE-DESC-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-SETTL-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-SETTL-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-SETTL-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-CNTRCT-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-CNTRCT-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-CNTRCT-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-DVDND-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-DVDND-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-DVDND-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-UNIT-QTY-CV(2)	(AD=N)#WS.#INV-ACCT-UNIT-QTY-CV(3)	(AD=N)#WS.#INV-ACCT-UNIT-QTY-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-UNIT-VALUE-CV(2)	(AD=N)#WS.#INV-ACCT-UNIT-VALUE-CV(3)	(AD=N)#WS.#INV-ACCT-UNIT-VALUE-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-IVC-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-IVC-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-IVC-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Dpi_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-DPI-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-DPI-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-DPI-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Dci_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-DCI-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-DCI-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-DCI-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Exp_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-EXP-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-EXP-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-EXP-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-FDRL-TAX-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-FDRL-TAX-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-FDRL-TAX-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-STATE-TAX-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-STATE-TAX-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-STATE-TAX-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-LOCAL-TAX-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-LOCAL-TAX-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-LOCAL-TAX-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt_Cv.setInitialAttributeValue("AD=N)#WS.#INV-ACCT-NET-PYMNT-AMT-CV(2)	(AD=N)#WS.#INV-ACCT-NET-PYMNT-AMT-CV(3)	(AD=N)#WS.#INV-ACCT-NET-PYMNT-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Pymnt_Settlmnt_Dte_Cv.setInitialAttributeValue("AD=N)#WS.#PYMNT-SETTLMNT-DTE-CV(2)	(AD=N)#WS.#PYMNT-SETTLMNT-DTE-CV(3)	(AD=N)#WS.#PYMNT-SETTLMNT-DTE-CV(4)	(AD=N");
        pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt_Cv.setInitialAttributeValue("AD=N)#WS.##PYMNT-DED-AMT-CV(2)	(AD=N)#WS.##PYMNT-DED-AMT-CV(3)	(AD=N)#WS.##PYMNT-DED-AMT-CV(4)	(AD=N");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_Cv.getValue("*").setInitialValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_Cv.getValue("*").setInitialValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Ded_Amt_Cv.getValue("*").setInitialValue("AD=N");
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(1).setInitialValue(1);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(2).setInitialValue(2);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(3).setInitialValue(1);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(4).setInitialValue(2);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(5).setInitialValue(1);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(6).setInitialValue(2);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(7).setInitialValue(1);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(8).setInitialValue(2);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(9).setInitialValue(1);
        pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(10).setInitialValue(2);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(1).setInitialValue(1);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(2).setInitialValue(1);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(3).setInitialValue(2);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(4).setInitialValue(2);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(5).setInitialValue(3);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(6).setInitialValue(3);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(7).setInitialValue(4);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(8).setInitialValue(4);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(9).setInitialValue(5);
        pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(10).setInitialValue(5);
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(1).setInitialValue("BLUE CROSS");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(2).setInitialValue("LONG TERM CARE");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(3).setInitialValue("MAJOR MEDICAL");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(4).setInitialValue("GROUP LIFE");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(5).setInitialValue("OVERPAYMENT");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(6).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(7).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(8).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(9).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(10).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Prev_Ledger.setInitialValue("H'00'");
        pnd_Ws_Pnd_Prev_Ledger_Ind.setInitialValue("H'00'");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp123() throws Exception
    {
        super("Fcpp123");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 23 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 80 ZP = ON;//Natural: FORMAT ( 02 ) PS = 20 LS = 119 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        pnd_Ws_Pnd_Program.setValue(Global.getPROGRAM());                                                                                                                 //Natural: ASSIGN #PROGRAM := *PROGRAM
        pnd_Ws_Pnd_Run_Time.setValue(Global.getTIMX());                                                                                                                   //Natural: ASSIGN #RUN-TIME := *TIMX
        pdaTbldcoda.getTbldcoda_Pnd_Mode().setValue("T");                                                                                                                 //Natural: ASSIGN TBLDCODA.#MODE := 'T'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 #WARRANT-EXTRACT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl123.getPnd_Warrant_Extract())))
        {
            CheckAtStartofData525();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*  ---------                                                                                                                                                //Natural: AT START OF DATA
            //*  ---------                                                                                                                                                //Natural: AT BREAK OF #RECORD-TYPE
            pnd_Ws_Pnd_Records_Read.nadd(1);                                                                                                                              //Natural: ADD 1 TO #RECORDS-READ
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DC") && ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().equals(1)))             //Natural: IF CNTRCT-ORGN-CDE = 'DC' AND #RECORD-TYPE = 1
            {
                //*    IF #DISP-SUB  =  4                           /* JC
                //*      ADD  1  TO  #PAGE-NUMBER                   /*
                //*      WRITE (01) NOTITLE  USING MAP  'FCPM123C'  /*
                //*      WRITE (02) NOTITLE  USING MAP  'FCPM123H'  /*
                //*      PERFORM  INIT-PAGE-2                       /*
                //*    END-IF                                       /* JC
                                                                                                                                                                          //Natural: PERFORM PROCESS-IA-DEATH-PAGE-2
                sub_Process_Ia_Death_Page_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  LEDGERS
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().equals(2)))                                                                                 //Natural: IF #RECORD-TYPE = 2
            {
                if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr().notEquals(pnd_Ws_Pnd_Prev_Ledger) || ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind().notEquals(pnd_Ws_Pnd_Prev_Ledger_Ind))) //Natural: IF #INV-ACCT-LEDGR-NBR NE #PREV-LEDGER OR #INV-ACCT-LEDGR-IND NE #PREV-LEDGER-IND
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-LEDGER-LINE
                    sub_Write_Ledger_Line();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                                                                                                                                                                          //Natural: PERFORM PROCESS-NEW-LEDGER
                    sub_Process_New_Ledger();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.nadd(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt());                                                          //Natural: ADD #INV-ACCT-LEDGR-AMT TO #DISP-ACCT-LEDGR-AMT
            }                                                                                                                                                             //Natural: END-IF
            readWork01Pnd_Record_TypeOld.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type());                                                                   //Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //* *------
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 01 )
        getReports().skip(1, 7);                                                                                                                                          //Natural: SKIP ( 01 ) 7 LINES
        getReports().write(1, new FieldAttributes ("AD=I"),ReportOption.NOTITLE,new TabSetting(7),"INPUT RECORDS          :", new FieldAttributes ("AD=I"),pnd_Ws_Pnd_Records_Read,  //Natural: WRITE ( 01 ) ( AD = I ) 7T 'INPUT RECORDS          :' ( AD = I ) #RECORDS-READ
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 01 ) 3 LINES
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new  //Natural: WRITE ( 01 ) 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' /
            TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE);
        if (Global.isEscape()) return;
        //*    WRITE A HEADER RECORD FOR EFM UPDATE FILE.
        pnd_Ws_Pnd_Timestmp.setValue(Global.getTIMESTMP());                                                                                                               //Natural: ASSIGN #TIMESTMP := *TIMESTMP
        getReports().write(2, ReportOption.NOTITLE,Global.getTIMX(), new ReportEditMask ("MM/DD/YYYY HH:II:SS.T"),new TabSetting(81),pnd_Ws_Pnd_Timestmp_A,new            //Natural: WRITE ( 02 ) *TIMX ( EM = MM/DD/YYYY�HH:II:SS.T ) 81T #TIMESTMP-A 97T 'HD WRNT003'
            TabSetting(97),"HD WRNT003");
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-PAGE-1
        //* *----------------------------
        //* *********************** RL PAYEE MATCH END ****************************
        //* *#CHECK-NUMBER-N7  :=  PYMNT-CHECK-NBR
        //* *MOVE FCPA110.START-CHECK-PREFIX-N3 TO #CHECK-NUMBER-N3
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-PYMNT-ADDR-PAGE-1
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-PAGE-2
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-RA-CASH-PAGE-2
        //* *---------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-FIRST-IA-DEATH-RECORD
        //* *----------------------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-IA-DEATH-PAGE-2
        //* *----------------------------------------
        //* *
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-COMMON-FIELDS
        //* * IF  INV-ACCT-SETTL-AMT(#INV-ACCT-SUB) > 0.00
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-NEW-LEDGER
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-LEDGER-LINE
        //* *----------------------------------
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEWPAGE-DISPLAY
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-END-LINE
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* *------------
        //* *********************** RL BEGIN - PAYEE MATCH ************MAR 06,2006
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //* *INCLUDE FCPCERRE /* RL PAYEE MATCH
        //* *INCLUDE FCPCERRS /* RL PAYEE MATCH
        //* ************************** RL END-PAYEE MATCH *************************
    }
    //*  FCPL123 EXTRACT RL PAYEE MATCH
    //*  RL PAYEE MATCH
    private void sub_Init_Page_1() throws Exception                                                                                                                       //Natural: INIT-PAGE-1
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue(" ");                                                                                                           //Natural: ASSIGN #CHECK-LETTER-A1 := ' '
        pnd_Check_Number_A11_Pnd_Check_Number_N7.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Check_Nbr());                                                           //Natural: ASSIGN #CHECK-NUMBER-N7 := PYMNT-CHECK-NBR
        pnd_Check_Number_A11_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
        pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Reqst_Log_Dte_Time());                                                       //Natural: ASSIGN #PYMNT-REQST-LOG-DTE-TIME := #WARRANT-EXTRACT.PYMNT-REQST-LOG-DTE-TIME
        pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr.setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Unq_Id_Nbr());                                                                     //Natural: ASSIGN #CNTRCT-UNQ-ID-NBR := #WARRANT-EXTRACT.CNTRCT-UNQ-ID-NBR
        pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde.setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde());                                             //Natural: ASSIGN #CNTRCT-CANCEL-RDRW-ACTVTY-CDE := #WARRANT-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        //*  LEON 08-10-99
        //*  LEON 08-10-99
        short decideConditionsMet752 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF #CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE ' '
        if (condition((pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" "))))
        {
            decideConditionsMet752++;
            pnd_Ws_Pnd_Csr_Sub.setValue(1);                                                                                                                               //Natural: ASSIGN #CSR-SUB := 1
        }                                                                                                                                                                 //Natural: VALUE 'R'
        else if (condition((pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R"))))
        {
            decideConditionsMet752++;
            pnd_Ws_Pnd_Csr_Sub.setValue(2);                                                                                                                               //Natural: ASSIGN #CSR-SUB := 2
        }                                                                                                                                                                 //Natural: VALUE 'C'
        else if (condition((pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C"))))
        {
            decideConditionsMet752++;
            pnd_Ws_Pnd_Csr_Sub.setValue(3);                                                                                                                               //Natural: ASSIGN #CSR-SUB := 3
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S"))))
        {
            decideConditionsMet752++;
            pnd_Ws_Pnd_Csr_Sub.setValue(4);                                                                                                                               //Natural: ASSIGN #CSR-SUB := 4
        }                                                                                                                                                                 //Natural: VALUE 'CN'
        else if (condition((pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN"))))
        {
            decideConditionsMet752++;
            pnd_Ws_Pnd_Csr_Sub.setValue(5);                                                                                                                               //Natural: ASSIGN #CSR-SUB := 5
        }                                                                                                                                                                 //Natural: VALUE 'SN'
        else if (condition((pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN"))))
        {
            decideConditionsMet752++;
            pnd_Ws_Pnd_Csr_Sub.setValue(6);                                                                                                                               //Natural: ASSIGN #CSR-SUB := 6
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Csr_Sub.setValue(7);                                                                                                                               //Natural: ASSIGN #CSR-SUB := 7
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_Csr_Display.setValue(pnd_Ws_Pnd_Csr_Array.getValue(pnd_Ws_Pnd_Csr_Sub));                                                                               //Natural: ASSIGN #CSR-DISPLAY := #CSR-ARRAY ( #CSR-SUB )
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind().greaterOrEqual(1) && ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind().lessOrEqual(7))) //Natural: IF PYMNT-PAY-TYPE-REQ-IND = 1 THRU 7
        {
            //* ********************** RL PAYEE MATCH BEGIN ***************************
            //*  CHECK, FED-EX, SUSPENSE
            //*  RL PAYEE MATCH
            //*  RL PAYEE
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind().equals(1) || ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind().equals(5)    //Natural: IF PYMNT-PAY-TYPE-REQ-IND = 1 OR = 5 OR = 6
                || ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind().equals(6)))
            {
                pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue(" ");                                                                                                   //Natural: ASSIGN #CHECK-LETTER-A1 := ' '
                pnd_Check_Number_A11_Pnd_Check_Number_N7.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Check_Nbr());                                                   //Natural: ASSIGN #CHECK-NUMBER-N7 := PYMNT-CHECK-NBR
                pnd_Check_Number_A11_Pnd_Check_Number_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                         //Natural: ASSIGN #CHECK-NUMBER-N3 := FCPA110.START-CHECK-PREFIX-N3
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  RL PAYEE MATCH
                //*  RL PAYEE MATCH
                pnd_Check_Number_A11.reset();                                                                                                                             //Natural: RESET #CHECK-NUMBER-A11
                pnd_Check_Number_A11_Pnd_Check_Number_A10.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Check_Scrty_Nbr());                                            //Natural: ASSIGN #CHECK-NUMBER-A10 := PYMNT-CHECK-SCRTY-NBR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Pymnt_Display.setValue(pnd_Ws_Pnd_Pymnt_Array.getValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind()));                               //Natural: ASSIGN #PYMNT-DISPLAY := #PYMNT-ARRAY ( PYMNT-PAY-TYPE-REQ-IND )
            //*   IF CNTRCT-PYMNT-TYPE-IND   =  'C'  OR =  'N'
            //*      DECIDE ON FIRST VALUE OF CNTRCT-STTLMNT-TYPE-IND
            //*        VALUE ' '      #PYMNT-DISPLAY  :=  'CLASSIC IRA'
            //*        VALUE 'H','T'  #PYMNT-DISPLAY  :=  'ROTH IRA   '
            //*         NONE  IGNORE
            //*      END-DECIDE
            //*   END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  UNKNOWN
            if (condition((ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind().equals(8) && (ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Pymnt_Type_Ind().equals("N") //Natural: IF PYMNT-PAY-TYPE-REQ-IND = 8 AND ( CNTRCT-PYMNT-TYPE-IND = 'N' OR = 'R' )
                || ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Pymnt_Type_Ind().equals("R")))))
            {
                pnd_Ws_Pnd_Pymnt_Display.setValue(pnd_Ws_Pnd_Pymnt_Array.getValue(8));                                                                                    //Natural: ASSIGN #PYMNT-DISPLAY := #PYMNT-ARRAY ( 8 )
                short decideConditionsMet802 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF CNTRCT-STTLMNT-TYPE-IND;//Natural: VALUE 'L','A','X'
                if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind().equals("L") || ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind().equals("A") 
                    || ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind().equals("X"))))
                {
                    decideConditionsMet802++;
                    pnd_Ws_Pnd_Pymnt_Display.setValue("CLASSIC    ");                                                                                                     //Natural: ASSIGN #PYMNT-DISPLAY := 'CLASSIC    '
                    pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue("R");                                                                                               //Natural: ASSIGN #CHECK-LETTER-A1 := 'R'
                }                                                                                                                                                         //Natural: VALUE 'H','J'
                else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind().equals("H") || ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind().equals("J"))))
                {
                    decideConditionsMet802++;
                    pnd_Ws_Pnd_Pymnt_Display.setValue("ROTH       ");                                                                                                     //Natural: ASSIGN #PYMNT-DISPLAY := 'ROTH       '
                    pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue("X");                                                                                               //Natural: ASSIGN #CHECK-LETTER-A1 := 'X'
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet802 > 0))
                {
                    //*  RL PAYEE MATCH
                    //*  RL PAYEE MATCH
                    pnd_Check_Number_A11.reset();                                                                                                                         //Natural: RESET #CHECK-NUMBER-A11
                    pnd_Check_Number_A11_Pnd_Check_Number_A10.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Check_Scrty_Nbr());                                        //Natural: ASSIGN #CHECK-NUMBER-A10 := PYMNT-CHECK-SCRTY-NBR
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        //*  EFT
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind().equals(2)))                                                                              //Natural: IF PYMNT-PAY-TYPE-REQ-IND = 2
        {
            ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Check_Nbr().setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Check_Scrty_Nbr());                                      //Natural: ASSIGN PYMNT-CHECK-NBR := PYMNT-CHECK-SCRTY-NBR
            //*  RL PAYEE MATCH
            //*  RL PAYEE MATCH
            pnd_Check_Number_A11.reset();                                                                                                                                 //Natural: RESET #CHECK-NUMBER-A11
            pnd_Check_Number_A11_Pnd_Check_Letter_A1.setValue("E");                                                                                                       //Natural: ASSIGN #CHECK-LETTER-A1 := 'E'
            pnd_Check_Number_A11_Pnd_Check_Number_A10.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Check_Nbr());                                                      //Natural: ASSIGN #CHECK-NUMBER-A10 := PYMNT-CHECK-NBR
            pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Text_Cv.setValue("AD=I");                                                                                                     //Natural: ASSIGN #PYMNT-EFT-TRANSIT-ID-TEXT-CV := #PYMNT-EFT-TRANSIT-ID-CV := #PYMNT-EFT-ACCT-NBR-TEXT-CV := #PYMNT-EFT-ACCT-NBR-CV := #PYMNT-CHK-SAV-IND-TEXT-CV := #PYMNT-CHK-SAV-IND-CV := ( AD = I )
            pnd_Ws_Pnd_Pymnt_Eft_Transit_Id_Cv.setValue("AD=I");
            pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Text_Cv.setValue("AD=I");
            pnd_Ws_Pnd_Pymnt_Eft_Acct_Nbr_Cv.setValue("AD=I");
            pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Text_Cv.setValue("AD=I");
            pnd_Ws_Pnd_Pymnt_Chk_Sav_Ind_Cv.setValue("AD=I");
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Eft_Line_Cvs.resetInitial();                                                                                                                       //Natural: RESET INITIAL #EFT-LINE-CVS
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet833 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-CHECK-CRRNCY-CDE;//Natural: VALUE '1'
        if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Check_Crrncy_Cde().equals("1"))))
        {
            decideConditionsMet833++;
            pnd_Ws_Pnd_Currency_Display.setValue("U.S.");                                                                                                                 //Natural: ASSIGN #CURRENCY-DISPLAY := 'U.S.'
        }                                                                                                                                                                 //Natural: VALUE '2'
        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Check_Crrncy_Cde().equals("2"))))
        {
            decideConditionsMet833++;
            pnd_Ws_Pnd_Currency_Display.setValue("CAN");                                                                                                                  //Natural: ASSIGN #CURRENCY-DISPLAY := 'CAN'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Currency_Display.setValue("OTH.");                                                                                                                 //Natural: ASSIGN #CURRENCY-DISPLAY := 'OTH.'
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Annt_Ctznshp_Cde().isBreak() || pnd_Ws_Pnd_Records_Read.equals(getZero())))                                       //Natural: IF BREAK OF ANNT-CTZNSHP-CDE OR #RECORDS-READ = 0
        {
            pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(58);                                                                                                          //Natural: ASSIGN TBLDCODA.#TABLE-ID := 58
            pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "0", ldaFcpl123.getPnd_Warrant_Extract_Annt_Ctznshp_Cde_A()));    //Natural: COMPRESS '0' ANNT-CTZNSHP-CDE-A INTO TBLDCODA.#CODE LEAVING NO SPACE
            DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pnd_Ws_Tbldcoda_Msg);                                                    //Natural: CALLNAT 'TBLDCOD' TBLDCODA TBLDCODA-MSG
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Ctznshp_Display.setValue(pdaTbldcoda.getTbldcoda_Pnd_Target());                                                                                    //Natural: ASSIGN #CTZNSHP-DISPLAY := TBLDCODA.#TARGET
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                         //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
        {
            pnd_Ws_Pnd_Fed_Display.setValue("NRA");                                                                                                                       //Natural: ASSIGN #FED-DISPLAY := 'NRA'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Fed_Display.setValue("USA");                                                                                                                       //Natural: ASSIGN #FED-DISPLAY := 'USA'
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Annt_Rsdncy_Cde().isBreak() || pnd_Ws_Pnd_Records_Read.equals(getZero())))                                        //Natural: IF BREAK OF ANNT-RSDNCY-CDE OR #RECORDS-READ = 0
        {
            pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(3);                                                                                                           //Natural: ASSIGN TBLDCODA.#TABLE-ID := 3
            pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(ldaFcpl123.getPnd_Warrant_Extract_Annt_Rsdncy_Cde());                                                             //Natural: ASSIGN TBLDCODA.#CODE := ANNT-RSDNCY-CDE
            DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pnd_Ws_Tbldcoda_Msg);                                                    //Natural: CALLNAT 'TBLDCOD' TBLDCODA TBLDCODA-MSG
            if (condition(Global.isEscape())) return;
            if (condition(pnd_Ws_Pnd_Pnd_Msg_Data.getValue(1).equals(" ")))                                                                                               //Natural: IF ##MSG-DATA ( 1 ) = ' '
            {
                pnd_Ws_Pnd_State_Display.setValue(pdaTbldcoda.getTbldcoda_Pnd_Target());                                                                                  //Natural: ASSIGN #STATE-DISPLAY := TBLDCODA.#TARGET
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_State_Display.setValue(ldaFcpl123.getPnd_Warrant_Extract_Annt_Rsdncy_Cde());                                                                   //Natural: ASSIGN #STATE-DISPLAY := ANNT-RSDNCY-CDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        short decideConditionsMet869 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ORGN-CDE;//Natural: VALUE 'SS'
        if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("SS"))))
        {
            decideConditionsMet869++;
            //*  LZ
            short decideConditionsMet873 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-TYPE-CDE;//Natural: VALUE 'L'
            if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("L"))))
            {
                decideConditionsMet873++;
                pnd_Lob_Cde.setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde());                                                                                 //Natural: ASSIGN #LOB-CDE := CNTRCT-LOB-CDE
                pnd_Pymnt_Sttlmnt_Type_Pnd_Cntrct_Pymnt_Type_Ind.setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Pymnt_Type_Ind());                                     //Natural: ASSIGN #CNTRCT-PYMNT-TYPE-IND := CNTRCT-PYMNT-TYPE-IND
                pnd_Pymnt_Sttlmnt_Type_Pnd_Cntrct_Sttlmnt_Type_Ind.setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Sttlmnt_Type_Ind());                                 //Natural: ASSIGN #CNTRCT-STTLMNT-TYPE-IND := CNTRCT-STTLMNT-TYPE-IND
                short decideConditionsMet879 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #PYMNT-STTLMNT-TYPE;//Natural: VALUE 'RL','RA'
                if (condition((pnd_Pymnt_Sttlmnt_Type.equals("RL") || pnd_Pymnt_Sttlmnt_Type.equals("RA"))))
                {
                    decideConditionsMet879++;
                    if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                                  //Natural: IF PYMNT-PAY-TYPE-REQ-IND = 8
                    {
                        short decideConditionsMet884 = 0;                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-LOB-CDE;//Natural: VALUE 'RA'
                        if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("RA"))))
                        {
                            decideConditionsMet884++;
                            pnd_Lob_Cde.setValue("R1");                                                                                                                   //Natural: ASSIGN #LOB-CDE := 'R1'
                        }                                                                                                                                                 //Natural: VALUE 'GRA'
                        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("GRA"))))
                        {
                            decideConditionsMet884++;
                            pnd_Lob_Cde.setValue("R2");                                                                                                                   //Natural: ASSIGN #LOB-CDE := 'R2'
                        }                                                                                                                                                 //Natural: VALUE 'GSRA'
                        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("GSRA"))))
                        {
                            decideConditionsMet884++;
                            pnd_Lob_Cde.setValue("R3");                                                                                                                   //Natural: ASSIGN #LOB-CDE := 'R3'
                        }                                                                                                                                                 //Natural: VALUE 'SRA'
                        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("SRA"))))
                        {
                            decideConditionsMet884++;
                            pnd_Lob_Cde.setValue("R4");                                                                                                                   //Natural: ASSIGN #LOB-CDE := 'R4'
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Lob_Cde.setValue("GRA");                                                                                                                      //Natural: ASSIGN #LOB-CDE := 'GRA'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'RH','RJ'
                else if (condition((pnd_Pymnt_Sttlmnt_Type.equals("RH") || pnd_Pymnt_Sttlmnt_Type.equals("RJ"))))
                {
                    decideConditionsMet879++;
                    if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("IRA")))                                                                      //Natural: IF CNTRCT-LOB-CDE = 'IRA'
                    {
                        pnd_Lob_Cde.setValue("R5");                                                                                                                       //Natural: ASSIGN #LOB-CDE := 'R5'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'C ','N '
                else if (condition((pnd_Pymnt_Sttlmnt_Type.equals("C ") || pnd_Pymnt_Sttlmnt_Type.equals("N "))))
                {
                    decideConditionsMet879++;
                    if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("IRA") || ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("IRAD"))) //Natural: IF CNTRCT-LOB-CDE = 'IRA' OR = 'IRAD'
                    {
                        pnd_Lob_Cde.setValue("R6");                                                                                                                       //Natural: ASSIGN #LOB-CDE := 'R6'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 'CH','CT','NH','NT'
                else if (condition((pnd_Pymnt_Sttlmnt_Type.equals("CH") || pnd_Pymnt_Sttlmnt_Type.equals("CT") || pnd_Pymnt_Sttlmnt_Type.equals("NH") 
                    || pnd_Pymnt_Sttlmnt_Type.equals("NT"))))
                {
                    decideConditionsMet879++;
                    if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("IRA") || ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Lob_Cde().equals("IRAD"))) //Natural: IF CNTRCT-LOB-CDE = 'IRA' OR = 'IRAD'
                    {
                        pnd_Lob_Cde.setValue("R7");                                                                                                                       //Natural: ASSIGN #LOB-CDE := 'R7'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                    //*   MDO
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: VALUE '30','31'
            else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("30") || ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("31"))))
            {
                decideConditionsMet873++;
                pnd_Lob_Cde.setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde());                                                                                //Natural: ASSIGN #LOB-CDE := CNTRCT-TYPE-CDE
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet873 > 0))
            {
                DbsUtil.examine(new ExamineSource(ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Fcpl236_Lob_Cde().getValue("*"),true), new ExamineSearch(pnd_Lob_Cde,                 //Natural: EXAMINE FULL #FCPL236-LOB-CDE ( * ) FOR FULL #LOB-CDE GIVING INDEX IN #LOB-SUB
                    true), new ExamineGivingIndex(pnd_Lob_Sub));
                if (condition(pnd_Lob_Sub.notEquals(getZero())))                                                                                                          //Natural: IF #LOB-SUB NE 0
                {
                    pnd_Ws_Pnd_Type_Array.getValue(4).setValue(ldaFcpl236.getPnd_Fcpl236_Lda_Pnd_Warrant_Hdr_Data().getValue(pnd_Lob_Sub));                               //Natural: ASSIGN #TYPE-ARRAY ( 4 ) := #WARRANT-HDR-DATA ( #LOB-SUB )
                    pnd_Ws_Pnd_Type_Sub.setValue(4);                                                                                                                      //Natural: ASSIGN #TYPE-SUB := 4
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Type_Sub.setValue(3);                                                                                                                      //Natural: ASSIGN #TYPE-SUB := 3
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Type_Sub.setValue(3);                                                                                                                          //Natural: ASSIGN #TYPE-SUB := 3
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet869++;
            short decideConditionsMet934 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-TYPE-CDE;//Natural: VALUE 'L'
            if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("L"))))
            {
                decideConditionsMet934++;
                pnd_Ws_Pnd_Type_Sub.setValue(1);                                                                                                                          //Natural: ASSIGN #TYPE-SUB := 1
            }                                                                                                                                                             //Natural: VALUE 'PP'
            else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("PP"))))
            {
                decideConditionsMet934++;
                pnd_Ws_Pnd_Type_Sub.setValue(2);                                                                                                                          //Natural: ASSIGN #TYPE-SUB := 2
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Type_Sub.setValue(3);                                                                                                                          //Natural: ASSIGN #TYPE-SUB := 3
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Type_Sub.setValue(3);                                                                                                                              //Natural: ASSIGN #TYPE-SUB := 3
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_Type_Display.setValue(pnd_Ws_Pnd_Type_Array.getValue(pnd_Ws_Pnd_Type_Sub));                                                                            //Natural: ASSIGN #TYPE-DISPLAY := #TYPE-ARRAY ( #TYPE-SUB )
        pnd_Ws_Pnd_Cntrct_Orgn_Cde.setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde());                                                                         //Natural: ASSIGN #CNTRCT-ORGN-CDE := CNTRCT-ORGN-CDE
        short decideConditionsMet948 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ORGN-CDE;//Natural: VALUE 'SS'
        if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("SS"))))
        {
            decideConditionsMet948++;
            //*  MDO
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("30") || ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("31")))          //Natural: IF CNTRCT-TYPE-CDE = '30' OR = '31'
            {
                pnd_Ws_Pnd_Orgn_Sub.setValue(6);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 6
                //*  SINGLE SUM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Orgn_Sub.setValue(1);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 1
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet948++;
            pnd_Ws_Pnd_Orgn_Sub.setValue(2);                                                                                                                              //Natural: ASSIGN #ORGN-SUB := 2
        }                                                                                                                                                                 //Natural: VALUE 'DS'
        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DS"))))
        {
            decideConditionsMet948++;
            pnd_Ws_Pnd_Orgn_Sub.setValue(3);                                                                                                                              //Natural: ASSIGN #ORGN-SUB := 3
        }                                                                                                                                                                 //Natural: VALUE 'MS'
        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("MS"))))
        {
            decideConditionsMet948++;
            pnd_Ws_Pnd_Orgn_Sub.setValue(4);                                                                                                                              //Natural: ASSIGN #ORGN-SUB := 4
        }                                                                                                                                                                 //Natural: VALUE 'IA'
        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("IA"))))
        {
            decideConditionsMet948++;
            pnd_Ws_Pnd_Orgn_Sub.setValue(5);                                                                                                                              //Natural: ASSIGN #ORGN-SUB := 5
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pnd_Ws_Pnd_Orgn_Sub.setValue(7);                                                                                                                              //Natural: ASSIGN #ORGN-SUB := 7
        }                                                                                                                                                                 //Natural: END-DECIDE
        pnd_Ws_Pnd_Orgn_Display.setValue(DbsUtil.compress(pnd_Ws_Pnd_Orgn_Array.getValue(pnd_Ws_Pnd_Orgn_Sub), "PAYMENT WARRANT"));                                       //Natural: COMPRESS #ORGN-ARRAY ( #ORGN-SUB ) 'PAYMENT WARRANT' INTO #ORGN-DISPLAY
        //*  CENTER #ORGN-DISPLAY
        pnd_Ws_Pnd_Orgn_Display.setValue(pnd_Ws_Pnd_Orgn_Display, MoveOption.LeftJustified);                                                                              //Natural: MOVE LEFT #ORGN-DISPLAY TO #ORGN-DISPLAY
        pnd_Len.reset();                                                                                                                                                  //Natural: RESET #LEN #W-FLD
        pnd_W_Fld.reset();
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Orgn_Display), new ExamineSearch(" "), new ExamineGivingLength(pnd_Len));                                            //Natural: EXAMINE #ORGN-DISPLAY FOR ' ' GIVING LENGTH #LEN
        pnd_Len.compute(new ComputeParameters(false, pnd_Len), (DbsField.subtract(43,pnd_Len)).divide(2));                                                                //Natural: COMPUTE #LEN = ( 43 - #LEN ) / 2
        pnd_W_Fld.moveAll("*");                                                                                                                                           //Natural: MOVE ALL '*' TO #W-FLD UNTIL #LEN
        pnd_Ws_Pnd_Orgn_Display.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pnd_W_Fld, pnd_Ws_Pnd_Orgn_Display));                                            //Natural: COMPRESS #W-FLD #ORGN-DISPLAY INTO #ORGN-DISPLAY LEAVE NO SPACE
        DbsUtil.examine(new ExamineSource(pnd_Ws_Pnd_Orgn_Display), new ExamineSearch("*"), new ExamineReplace(" "));                                                     //Natural: EXAMINE #ORGN-DISPLAY FOR '*' REPLACE ' '
        pnd_Ws_Pnd_Prev_Ledger.resetInitial();                                                                                                                            //Natural: RESET INITIAL #PREV-LEDGER #PREV-LEDGER-IND #LEDGER-DISP-ARRAY
        pnd_Ws_Pnd_Prev_Ledger_Ind.resetInitial();
        pnd_Ws_Pnd_Ledger_Disp_Array.resetInitial();
    }
    private void sub_Display_Pymnt_Addr_Page_1() throws Exception                                                                                                         //Natural: DISPLAY-PYMNT-ADDR-PAGE-1
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().newPage(new ReportSpecification(2));                                                                                                                 //Natural: NEWPAGE ( 02 )
        if (condition(Global.isEscape())){return;}
        pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PAGE-NUMBER
        short decideConditionsMet1000 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF CNTRCT-ORGN-CDE;//Natural: VALUE 'SS'
        if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("SS"))))
        {
            decideConditionsMet1000++;
            //*  RL PAYEE MATCH
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123.class));                                                                           //Natural: WRITE ( 01 ) NOTITLE USING MAP 'FCPM123'
            //*  RL PAYEE MATCH
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123e.class));                                                                          //Natural: WRITE ( 02 ) NOTITLE USING MAP 'FCPM123E'
            //*  JESSE MARKER
                                                                                                                                                                          //Natural: PERFORM PROCESS-RA-CASH-PAGE-2
            sub_Process_Ra_Cash_Page_2();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'DC'
        else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DC"))))
        {
            decideConditionsMet1000++;
            //*  RL PAYEE MATCH
            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123a.class));                                                                          //Natural: WRITE ( 01 ) NOTITLE USING MAP 'FCPM123A'
            //*  RL PAYEE MATCH
            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123f.class));                                                                          //Natural: WRITE ( 02 ) NOTITLE USING MAP 'FCPM123F'
                                                                                                                                                                          //Natural: PERFORM PROCESS-FIRST-IA-DEATH-RECORD
            sub_Process_First_Ia_Death_Record();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"UNKNOWN/INVALID ORIGIN CODE",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN:",ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde(),new  //Natural: WRITE '***' 25T 'UNKNOWN/INVALID ORIGIN CODE' 77T '***' / '***' 25T 'ORIGIN:' CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'PPCN# :' CNTRCT-PPCN-NBR 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PPCN# :",ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Ppcn_Nbr(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Init_Page_2() throws Exception                                                                                                                       //Natural: INIT-PAGE-2
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------
        //*  JC
        pnd_Ws_Pnd_Inv_Acct.getValue("*").resetInitial();                                                                                                                 //Natural: RESET INITIAL #WS.#INV-ACCT ( * ) #DISP-SUB #INV-ACCT-CDE-DESC-DC ( * )
        pnd_Ws_Pnd_Disp_Sub.resetInitial();
        pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue("*").resetInitial();
        //*  09-11-2000
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DC")))                                                                                  //Natural: IF CNTRCT-ORGN-CDE = 'DC'
        {
            pnd_Ws_Pnd_Installment_Hdr.getValue("*").reset();                                                                                                             //Natural: RESET #WS.#INSTALLMENT-HDR ( * ) #WS.#INV-ACCT-UNIT-QTY-A ( * ) #WS.#INV-ACCT-UNIT-VALUE-A ( * )
            pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue("*").reset();
            pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue("*").reset();
            //*   IF #TYPE-SUB = 1                                        /* LUMP-SUM
            //*      #INSTALLMENT-LABEL := 'INSTALL / EFF DT.:'
            //*   ELSE
            //*      #INSTALLMENT-LABEL := 'INSTALLMENT......:'
            //*   END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Ra_Cash_Page_2() throws Exception                                                                                                            //Natural: PROCESS-RA-CASH-PAGE-2
    {
        if (BLNatReinput.isReinput()) return;

        FOR02:                                                                                                                                                            //Natural: FOR #INV-ACCT-SUB = 1 TO C-INV-ACCT
        for (pnd_Ws_Pnd_Inv_Acct_Sub.setValue(1); condition(pnd_Ws_Pnd_Inv_Acct_Sub.lessOrEqual(ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct())); pnd_Ws_Pnd_Inv_Acct_Sub.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Disp_Sub.equals(4)))                                                                                                                 //Natural: IF #DISP-SUB = 4
            {
                pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                           //Natural: ADD 1 TO #PAGE-NUMBER
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123b.class));                                                                      //Natural: WRITE ( 01 ) NOTITLE USING MAP 'FCPM123B'
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123g.class));                                                                      //Natural: WRITE ( 02 ) NOTITLE USING MAP 'FCPM123G'
                                                                                                                                                                          //Natural: PERFORM INIT-PAGE-2
                sub_Init_Page_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Disp_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #DISP-SUB
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                               //Natural: MOVE ( AD = I ) TO #INV-ACCT-CDE-DESC-CV ( #DISP-SUB )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_Inv_Acct_Sub));                //Natural: ASSIGN #INV-ACCT-INPUT := #WARRANT-EXTRACT.INV-ACCT-CDE ( #INV-ACCT-SUB )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' #FUND-PDA
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(), MoveOption.RightJustified);            //Natural: MOVE RIGHT #FUND-PDA.#INV-ACCT-DESC-8 TO #INV-ACCT-CDE-DESC ( #DISP-SUB )
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMMON-FIELDS
            sub_Process_Common_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                      //Natural: IF INV-ACCT-EXP-AMT ( #INV-ACCT-SUB ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Exp_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                            //Natural: ASSIGN #INV-ACCT-EXP-AMT-CV ( #DISP-SUB ) := ( AD = I )
                pnd_Ws_Pnd_Inv_Acct_Exp_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: ASSIGN #INV-ACCT-EXP-AMT ( #DISP-SUB ) := INV-ACCT-EXP-AMT ( #INV-ACCT-SUB )
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #PAGE-NUMBER
        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123b.class));                                                                              //Natural: WRITE ( 01 ) NOTITLE USING MAP 'FCPM123B'
        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123g.class));                                                                              //Natural: WRITE ( 02 ) NOTITLE USING MAP 'FCPM123G'
    }
    private void sub_Process_First_Ia_Death_Record() throws Exception                                                                                                     //Natural: PROCESS-FIRST-IA-DEATH-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Cde().getValue(1));                                          //Natural: ASSIGN #INV-ACCT-INPUT := #WARRANT-EXTRACT.INV-ACCT-CDE ( 1 )
        pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Valuat_Period().getValue(1));                        //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := #WARRANT-EXTRACT.INV-ACCT-VALUAT-PERIOD ( 1 )
        DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                        //Natural: CALLNAT 'FCPN199A' #FUND-PDA
        if (condition(Global.isEscape())) return;
        //*  JESSE   DC(1)
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS
        {
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue(1).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(), MoveOption.RightJustified);                           //Natural: MOVE RIGHT #FUND-PDA.#INV-ACCT-DESC-8 TO #INV-ACCT-CDE-DESC-DC ( 1 )
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*     IF INV-ACCT-SETTL-AMT(1) > 0     /* UNCOMMENTED THIS LINE  JC
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue(1).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(),       //Natural: COMPRESS #INV-ACCT-DESC-8 #VALUAT-DESC-3 INTO #INV-ACCT-CDE-DESC-DC ( 1 ) LEAVING NO SPACE
                pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_3()));
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue(1).setValue(pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue(1), MoveOption.RightJustified);                                 //Natural: MOVE RIGHT #INV-ACCT-CDE-DESC-DC ( 1 ) TO #INV-ACCT-CDE-DESC-DC ( 1 )
            //*     END-IF                          /* UNCOMMENTED THIS LINE  JC
        }                                                                                                                                                                 //Natural: END-IF
        pdaTbldcoda.getTbldcoda_Pnd_Table_Id().setValue(5);                                                                                                               //Natural: MOVE 5 TO TBLDCODA.#TABLE-ID
        pdaTbldcoda.getTbldcoda_Pnd_Code().setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Mode_Cde());                                                                 //Natural: MOVE CNTRCT-MODE-CDE TO TBLDCODA.#CODE
        DbsUtil.callnat(Tbldcod.class , getCurrentProcessState(), pdaTbldcoda.getTbldcoda(), pnd_Ws_Tbldcoda_Msg);                                                        //Natural: CALLNAT 'TBLDCOD' TBLDCODA TBLDCODA-MSG
        if (condition(Global.isEscape())) return;
        if (condition(pnd_Ws_Pnd_Pnd_Msg_Data.getValue(1).equals(" ")))                                                                                                   //Natural: IF ##MSG-DATA ( 1 ) = ' '
        {
            pnd_Ws_Pnd_Mode_Display.setValue(pdaTbldcoda.getTbldcoda_Pnd_Target());                                                                                       //Natural: MOVE TBLDCODA.#TARGET TO #MODE-DISPLAY
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Mode_Display.setValue(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Mode_Cde());                                                                        //Natural: MOVE CNTRCT-MODE-CDE TO #MODE-DISPLAY
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_Ia_Death_Page_2() throws Exception                                                                                                           //Natural: PROCESS-IA-DEATH-PAGE-2
    {
        if (BLNatReinput.isReinput()) return;

        FOR03:                                                                                                                                                            //Natural: FOR #INV-ACCT-SUB = 1 TO C-INV-ACCT
        for (pnd_Ws_Pnd_Inv_Acct_Sub.setValue(1); condition(pnd_Ws_Pnd_Inv_Acct_Sub.lessOrEqual(ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct())); pnd_Ws_Pnd_Inv_Acct_Sub.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Disp_Sub.equals(4)))                                                                                                                 //Natural: IF #DISP-SUB = 4
            {
                pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                           //Natural: ADD 1 TO #PAGE-NUMBER
                getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123c.class));                                                                      //Natural: WRITE ( 01 ) NOTITLE USING MAP 'FCPM123C'
                getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123h.class));                                                                      //Natural: WRITE ( 02 ) NOTITLE USING MAP 'FCPM123H'
                                                                                                                                                                          //Natural: PERFORM INIT-PAGE-2
                sub_Init_Page_2();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Disp_Sub.nadd(1);                                                                                                                                  //Natural: ADD 1 TO #DISP-SUB
            //*  09-11-2000
            if (condition(pnd_Ws_Pnd_Type_Sub.equals(1)))                                                                                                                 //Natural: IF #TYPE-SUB = 1
            {
                short decideConditionsMet1115 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE #WARRANT-EXTRACT.PYMNT-COMMUTED-VALUE;//Natural: VALUE 'PP'
                if (condition((ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Commuted_Value().equals("PP"))))
                {
                    decideConditionsMet1115++;
                    pnd_Ws_Pnd_Installment_Hdr.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("      PAST DUE");                                                                  //Natural: ASSIGN #WS.#INSTALLMENT-HDR ( #DISP-SUB ) := '      PAST DUE'
                }                                                                                                                                                         //Natural: VALUE 'CV'
                else if (condition((ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Commuted_Value().equals("CV"))))
                {
                    decideConditionsMet1115++;
                    pnd_Ws_Pnd_Installment_Hdr.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("COMMUTED VALUE");                                                                  //Natural: ASSIGN #WS.#INSTALLMENT-HDR ( #DISP-SUB ) := 'COMMUTED VALUE'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    pnd_Ws_Pnd_Installment_Hdr.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("         OTHER");                                                                  //Natural: ASSIGN #WS.#INSTALLMENT-HDR ( #DISP-SUB ) := '         OTHER'
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Installment_Hdr.getValue(1).setValue("TOTAL(S):     ");                                                                                        //Natural: ASSIGN #WS.#INSTALLMENT-HDR ( 1 ) := 'TOTAL(S):     '
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                               //Natural: MOVE ( AD = I ) TO #INV-ACCT-CDE-DESC-CV ( #DISP-SUB )
            pnd_Ws_Pnd_Pymnt_Settlmnt_Dte_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                              //Natural: MOVE ( AD = I ) TO #PYMNT-SETTLMNT-DTE-CV ( #DISP-SUB )
            pnd_Ws_Pnd_Pymnt_Settlmnt_Dte.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Settlmnt_Dte());                                 //Natural: MOVE PYMNT-SETTLMNT-DTE TO #PYMNT-SETTLMNT-DTE ( #DISP-SUB )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Input().setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_Inv_Acct_Sub));                //Natural: ASSIGN #INV-ACCT-INPUT := #WARRANT-EXTRACT.INV-ACCT-CDE ( #INV-ACCT-SUB )
            pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Valuat_Period().setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: ASSIGN #FUND-PDA.#INV-ACCT-VALUAT-PERIOD := #WARRANT-EXTRACT.INV-ACCT-VALUAT-PERIOD ( #INV-ACCT-SUB )
            DbsUtil.callnat(Fcpn199a.class , getCurrentProcessState(), pdaFcpa199a.getPnd_Fund_Pda());                                                                    //Natural: CALLNAT 'FCPN199A' #FUND-PDA
            if (condition(Global.isEscape())) return;
            if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().getBoolean()))                                                                                      //Natural: IF #FUND-PDA.#DIVIDENDS
            {
                pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(), MoveOption.RightJustified);     //Natural: MOVE RIGHT #FUND-PDA.#INV-ACCT-DESC-8 TO #INV-ACCT-CDE-DESC-DC ( #DISP-SUB )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, pdaFcpa199a.getPnd_Fund_Pda_Pnd_Inv_Acct_Desc_8(),  //Natural: COMPRESS #INV-ACCT-DESC-8 #VALUAT-DESC-3 INTO #INV-ACCT-CDE-DESC-DC ( #DISP-SUB ) LEAVING NO
                    pdaFcpa199a.getPnd_Fund_Pda_Pnd_Valuat_Desc_3()));
                pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(pnd_Ws_Pnd_Inv_Acct_Cde_Desc_Dc.getValue(pnd_Ws_Pnd_Disp_Sub),                     //Natural: MOVE RIGHT #INV-ACCT-CDE-DESC-DC ( #DISP-SUB ) TO #INV-ACCT-CDE-DESC-DC ( #DISP-SUB )
                    MoveOption.RightJustified);
            }                                                                                                                                                             //Natural: END-IF
            //* *
                                                                                                                                                                          //Natural: PERFORM PROCESS-COMMON-FIELDS
            sub_Process_Common_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                      //Natural: IF INV-ACCT-DCI-AMT ( #INV-ACCT-SUB ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Dci_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                            //Natural: MOVE ( AD = I ) TO #INV-ACCT-DCI-AMT-CV ( #DISP-SUB )
                pnd_Ws_Pnd_Inv_Acct_Dci_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: MOVE INV-ACCT-DCI-AMT ( #INV-ACCT-SUB ) TO #INV-ACCT-DCI-AMT ( #DISP-SUB )
            }                                                                                                                                                             //Natural: END-IF
            //*  OVERPAYMENT IN IF CLAUSE COMMENTED OUT        06/03/99  ROXAN
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Ded_Grp_Top().notEquals(getZero())))                                                                    //Natural: IF PYMNT-DED-GRP-TOP NE 0
            {
                FOR04:                                                                                                                                                    //Natural: FOR #ROW = 1 TO PYMNT-DED-GRP-TOP
                for (pnd_Ws_Pnd_Row.setValue(1); condition(pnd_Ws_Pnd_Row.lessOrEqual(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Ded_Grp_Top())); pnd_Ws_Pnd_Row.nadd(1))
                {
                    if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_Row).greaterOrEqual(1) && ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_Row).lessOrEqual(10))) //Natural: IF PYMNT-DED-CDE ( #ROW ) = 1 THRU 10
                    {
                        pnd_Ws_Pnd_Ded_Sub.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_Row));                                          //Natural: MOVE PYMNT-DED-CDE ( #ROW ) TO #DED-SUB
                        //*           IF PYMNT-DED-CDE(#ROW) = 5                  /* OVERPAYMENT
                        //*              MOVE (AD=I) TO  ##PYMNT-DED-AMT-CV(#DISP-SUB)
                        //*             MOVE PYMNT-DED-AMT(#ROW) TO ##PYMNT-DED-AMT(#DISP-SUB)
                        //*           END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_Ded_Sub.setValue(10);                                                                                                                  //Natural: ASSIGN #DED-SUB := 10
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_10.getValue(pnd_Ws_Pnd_Ded_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Ded_Payee_Cde().getValue(pnd_Ws_Pnd_Row)); //Natural: MOVE PYMNT-DED-PAYEE-CDE ( #ROW ) TO #PYMNT-DED-PAYEE-CDE-10 ( #DED-SUB )
                    //*        ADD  PYMNT-DED-AMT(#ROW) TO #PYMNT-DED-AMT-10(#DED-SUB)
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*  AND REPLACED IT BY THE NEXT 2 STATEMENTS
                pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                           //Natural: MOVE ( AD = I ) TO ##PYMNT-DED-AMT-CV ( #DISP-SUB )
                pnd_Ws_Pnd_Pnd_Pymnt_Ded_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: MOVE ##INV-ACCT-OVR-PAY-DED-AMT ( #INV-ACCT-SUB ) TO ##PYMNT-DED-AMT ( #DISP-SUB )
                pnd_Ws_Pnd_Pymnt_Ded_Amt_10.getValue(pnd_Ws_Pnd_Ded_Sub).nadd(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: ADD ##INV-ACCT-OVR-PAY-DED-AMT ( #INV-ACCT-SUB ) TO #PYMNT-DED-AMT-10 ( #DED-SUB )
                //*  06/03/99
            }                                                                                                                                                             //Natural: END-IF
            //* *DISPLAY C-INV-ACCT
            //*   CNTRCT-ORGN-CDE
            //*   CNTRCT-UNQ-ID-NBR
            //*   CNTRCT-PPCN-NBR
            //*   PYMNT-CHECK-DTE
            //*   #RECORD-TYPE
            //*   #PYMNT-SETTLMNT-DTE(#DISP-SUB)
            //*   #INV-ACCT-CDE-DESC-DC(#DISP-SUB)
            //*   #INV-ACCT-SETTL-AMT(#DISP-SUB)
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Process_Common_Fields() throws Exception                                                                                                             //Natural: PROCESS-COMMON-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------------
        if (condition(pdaFcpa199a.getPnd_Fund_Pda_Pnd_Dividends().equals(true)))                                                                                          //Natural: IF #FUND-PDA.#DIVIDENDS = TRUE
        {
            pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                             //Natural: MOVE ( AD = I ) TO #INV-ACCT-CNTRCT-AMT-CV ( #DISP-SUB )
            pnd_Ws_Pnd_Inv_Acct_Cntrct_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: MOVE INV-ACCT-SETTL-AMT ( #INV-ACCT-SUB ) TO #INV-ACCT-CNTRCT-AMT ( #DISP-SUB )
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                    //Natural: IF INV-ACCT-DVDND-AMT ( #INV-ACCT-SUB ) NE 0.00
            {
                pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                          //Natural: MOVE ( AD = I ) TO #INV-ACCT-DVDND-AMT-CV ( #DISP-SUB )
                pnd_Ws_Pnd_Inv_Acct_Dvdnd_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: MOVE INV-ACCT-DVDND-AMT ( #INV-ACCT-SUB ) TO #INV-ACCT-DVDND-AMT ( #DISP-SUB )
                ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).nadd(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: ADD INV-ACCT-DVDND-AMT ( #INV-ACCT-SUB ) TO INV-ACCT-SETTL-AMT ( #INV-ACCT-SUB )
            }                                                                                                                                                             //Natural: END-IF
            //*  CREF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                     //Natural: IF INV-ACCT-UNIT-QTY ( #INV-ACCT-SUB ) NE 0.00
            {
                //*  09-11-2000
                if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DC")))                                                                          //Natural: IF CNTRCT-ORGN-CDE = 'DC'
                {
                    if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("L")))                                                                       //Natural: IF CNTRCT-TYPE-CDE = 'L'
                    {
                        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue(pnd_Ws_Pnd_Disp_Sub).moveAll(" ");                                                                        //Natural: MOVE ALL ' ' TO #INV-ACCT-UNIT-QTY-A ( #DISP-SUB )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                   //Natural: MOVE ( AD = I ) TO #INV-ACCT-UNIT-QTY-CV ( #DISP-SUB )
                        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue(pnd_Ws_Pnd_Disp_Sub).setValueEdited(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Pnd_Inv_Acct_Sub),new  //Natural: MOVE EDITED INV-ACCT-UNIT-QTY ( #INV-ACCT-SUB ) ( EM = ZZZZZ9.999 ) TO #INV-ACCT-UNIT-QTY-A ( #DISP-SUB )
                            ReportEditMask("ZZZZZ9.999"));
                        pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(pnd_Ws_Pnd_Inv_Acct_Unit_Qty_A.getValue(pnd_Ws_Pnd_Disp_Sub),               //Natural: MOVE RIGHT #INV-ACCT-UNIT-QTY-A ( #DISP-SUB ) TO #INV-ACCT-UNIT-QTY-A ( #DISP-SUB )
                            MoveOption.RightJustified);
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Inv_Acct_Unit_Qty_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                       //Natural: MOVE ( AD = I ) TO #INV-ACCT-UNIT-QTY-CV ( #DISP-SUB )
                    pnd_Ws_Pnd_Inv_Acct_Unit_Qty.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Unit_Qty().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: MOVE INV-ACCT-UNIT-QTY ( #INV-ACCT-SUB ) TO #INV-ACCT-UNIT-QTY ( #DISP-SUB )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                   //Natural: IF INV-ACCT-UNIT-VALUE ( #INV-ACCT-SUB ) NE 0.00
            {
                //*  09-11-2000
                if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DC")))                                                                          //Natural: IF CNTRCT-ORGN-CDE = 'DC'
                {
                    if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Type_Cde().equals("L")))                                                                       //Natural: IF CNTRCT-TYPE-CDE = 'L'
                    {
                        pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue(pnd_Ws_Pnd_Disp_Sub).moveAll(" ");                                                                      //Natural: MOVE ALL ' ' TO #INV-ACCT-UNIT-VALUE-A ( #DISP-SUB )
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                 //Natural: MOVE ( AD = I ) TO #INV-ACCT-UNIT-VALUE-CV ( #DISP-SUB )
                        pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue(pnd_Ws_Pnd_Disp_Sub).setValueEdited(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_Inv_Acct_Sub),new  //Natural: MOVE EDITED INV-ACCT-UNIT-VALUE ( #INV-ACCT-SUB ) ( EM = ZZZZ9.9999 ) TO #INV-ACCT-UNIT-VALUE-A ( #DISP-SUB )
                            ReportEditMask("ZZZZ9.9999"));
                        pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(pnd_Ws_Pnd_Inv_Acct_Unit_Value_A.getValue(pnd_Ws_Pnd_Disp_Sub),           //Natural: MOVE RIGHT #INV-ACCT-UNIT-VALUE-A ( #DISP-SUB ) TO #INV-ACCT-UNIT-VALUE-A ( #DISP-SUB )
                            MoveOption.RightJustified);
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Inv_Acct_Unit_Value_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                     //Natural: MOVE ( AD = I ) TO #INV-ACCT-UNIT-VALUE-CV ( #DISP-SUB )
                    pnd_Ws_Pnd_Inv_Acct_Unit_Value.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Unit_Value().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: MOVE INV-ACCT-UNIT-VALUE ( #INV-ACCT-SUB ) TO #INV-ACCT-UNIT-VALUE ( #DISP-SUB )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                                  //Natural: ASSIGN #INV-ACCT-SETTL-AMT-CV ( #DISP-SUB ) := ( AD = I )
        pnd_Ws_Pnd_Inv_Acct_Settl_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub));   //Natural: ASSIGN #INV-ACCT-SETTL-AMT ( #DISP-SUB ) := INV-ACCT-SETTL-AMT ( #INV-ACCT-SUB )
        //* * END-IF
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                          //Natural: IF INV-ACCT-IVC-AMT ( #INV-ACCT-SUB ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Ivc_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                                //Natural: ASSIGN #INV-ACCT-IVC-AMT-CV ( #DISP-SUB ) := ( AD = I )
            pnd_Ws_Pnd_Inv_Acct_Ivc_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Ivc_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub));   //Natural: ASSIGN #INV-ACCT-IVC-AMT ( #DISP-SUB ) := INV-ACCT-IVC-AMT ( #INV-ACCT-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                          //Natural: IF INV-ACCT-DPI-AMT ( #INV-ACCT-SUB ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Dpi_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                                //Natural: ASSIGN #INV-ACCT-DPI-AMT-CV ( #DISP-SUB ) := ( AD = I )
            pnd_Ws_Pnd_Inv_Acct_Dpi_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub));   //Natural: ASSIGN #INV-ACCT-DPI-AMT ( #DISP-SUB ) := INV-ACCT-DPI-AMT ( #INV-ACCT-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                     //Natural: IF INV-ACCT-FDRL-TAX-AMT ( #INV-ACCT-SUB ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                           //Natural: ASSIGN #INV-ACCT-FDRL-TAX-AMT-CV ( #DISP-SUB ) := ( AD = I )
            pnd_Ws_Pnd_Inv_Acct_Fdrl_Tax_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: ASSIGN #INV-ACCT-FDRL-TAX-AMT ( #DISP-SUB ) := INV-ACCT-FDRL-TAX-AMT ( #INV-ACCT-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                    //Natural: IF INV-ACCT-STATE-TAX-AMT ( #INV-ACCT-SUB ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                          //Natural: ASSIGN #INV-ACCT-STATE-TAX-AMT-CV ( #DISP-SUB ) := ( AD = I )
            pnd_Ws_Pnd_Inv_Acct_State_Tax_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: ASSIGN #INV-ACCT-STATE-TAX-AMT ( #DISP-SUB ) := INV-ACCT-STATE-TAX-AMT ( #INV-ACCT-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                    //Natural: IF INV-ACCT-LOCAL-TAX-AMT ( #INV-ACCT-SUB ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                          //Natural: ASSIGN #INV-ACCT-LOCAL-TAX-AMT-CV ( #DISP-SUB ) := ( AD = I )
            pnd_Ws_Pnd_Inv_Acct_Local_Tax_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: ASSIGN #INV-ACCT-LOCAL-TAX-AMT ( #DISP-SUB ) := INV-ACCT-LOCAL-TAX-AMT ( #INV-ACCT-SUB )
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub).notEquals(new DbsDecimal("0.00"))))                    //Natural: IF INV-ACCT-NET-PYMNT-AMT ( #INV-ACCT-SUB ) NE 0.00
        {
            pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt_Cv.getValue(pnd_Ws_Pnd_Disp_Sub).setValue("AD=I");                                                                          //Natural: ASSIGN #INV-ACCT-NET-PYMNT-AMT-CV ( #DISP-SUB ) := ( AD = I )
            pnd_Ws_Pnd_Inv_Acct_Net_Pymnt_Amt.getValue(pnd_Ws_Pnd_Disp_Sub).setValue(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_Inv_Acct_Sub)); //Natural: ASSIGN #INV-ACCT-NET-PYMNT-AMT ( #DISP-SUB ) := INV-ACCT-NET-PYMNT-AMT ( #INV-ACCT-SUB )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Process_New_Ledger() throws Exception                                                                                                                //Natural: PROCESS-NEW-LEDGER
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        pnd_Ws_Pnd_Prev_Ledger.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr());                                                                      //Natural: MOVE #INV-ACCT-LEDGR-NBR TO #PREV-LEDGER #DISP-ACCT-LEDGR-NBR #FCPA121.INV-ACCT-LEDGR-NBR
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Nbr.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr());
        pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr());
        pnd_Ws_Pnd_Prev_Ledger_Ind.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind());                                                                  //Natural: MOVE #INV-ACCT-LEDGR-IND TO #PREV-LEDGER-IND
        if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind().equals("C")))                                                                            //Natural: IF #INV-ACCT-LEDGR-IND = 'C'
        {
            pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.setValue("CREDIT");                                                                                                            //Natural: MOVE 'CREDIT' TO #DISP-ACCT-LEDGR-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind.setValue("DEBIT");                                                                                                             //Natural: MOVE 'DEBIT' TO #DISP-ACCT-LEDGR-IND
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Pnd_Inv_Acct_Cde_A.setValue(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Cde());                                                                     //Natural: ASSIGN ##INV-ACCT-CDE-A := #WARRANT-EXTRACT.#INV-ACCT-CDE
        pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Isa().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Pnd_Inv_Acct_Cde.getInt() + 1));                   //Natural: ASSIGN #FCPA121.INV-ACCT-ISA := #ISA-LDA.#ISA-CDE ( ##INV-ACCT-CDE )
        pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr());                                              //Natural: ASSIGN #FCPA121.INV-ACCT-LEDGR-NBR := #WARRANT-EXTRACT.#INV-ACCT-LEDGR-NBR
        DbsUtil.callnat(Fcpn121.class , getCurrentProcessState(), pdaFcpa121.getPnd_Fcpa121());                                                                           //Natural: CALLNAT 'FCPN121' #FCPA121
        if (condition(Global.isEscape())) return;
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc.setValue(pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc());                                                                        //Natural: MOVE #FCPA121.INV-ACCT-LEDGR-DESC TO #DISP-ACCT-LEDGR-DESC
    }
    private void sub_Write_Ledger_Line() throws Exception                                                                                                                 //Natural: WRITE-LEDGER-LINE
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ws_Pnd_Line_Count.setValue(1);                                                                                                                                //Natural: ASSIGN #LINE-COUNT := 1
                                                                                                                                                                          //Natural: PERFORM NEWPAGE-DISPLAY
        sub_Newpage_Display();
        if (condition(Global.isEscape())) {return;}
        getReports().write(1, ReportOption.NOTITLE,pnd_Ws_Pnd_Disp_Acct_Ledgr_Nbr,pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc,pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind,pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt,  //Natural: WRITE ( 01 ) #DISP-ACCT-LEDGR-NBR #DISP-ACCT-LEDGR-DESC #DISP-ACCT-LEDGR-IND #DISP-ACCT-LEDGR-AMT
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,pnd_Ws_Pnd_Disp_Acct_Ledgr_Nbr,pnd_Ws_Pnd_Disp_Acct_Ledgr_Desc,pnd_Ws_Pnd_Disp_Acct_Ledgr_Ind,pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt,  //Natural: WRITE ( 02 ) #DISP-ACCT-LEDGR-NBR #DISP-ACCT-LEDGR-DESC #DISP-ACCT-LEDGR-IND #DISP-ACCT-LEDGR-AMT 81T #PYMNT-REQST-LOG-DTE-TIME #CNTRCT-ORGN-CDE #CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) *LINE-COUNT ( 02 ) ( EM = 99 ) #CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new TabSetting(81),pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time,pnd_Ws_Pnd_Cntrct_Orgn_Cde,pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr, 
            new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Page_Number, new ReportEditMask ("99999"),getReports().getAstLineCount(2), new ReportEditMask 
            ("99"),pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde);
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Disp_Acct_Ledgr_Amt.reset();                                                                                                                           //Natural: RESET #DISP-ACCT-LEDGR-AMT
        if (condition(getReports().getAstLineCount(2).equals(1)))                                                                                                         //Natural: IF *LINE-COUNT ( 02 ) = 1
        {
            pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                               //Natural: ADD 1 TO #PAGE-NUMBER
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Newpage_Display() throws Exception                                                                                                                   //Natural: NEWPAGE-DISPLAY
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------------
        getReports().write(1, ReportOption.TITLE,  ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(2),pnd_Ws_Pnd_Run_Time, new ReportEditMask ("MM/DD/YYYY - HH:IIAP"),new  //Natural: NEWPAGE ( 01 ) IF LESS THAN #LINE-COUNT LINES LEFT WITH TITLE LEFT 2T #RUN-TIME ( EM = MM/DD/YYYY�-�HH:IIAP ) 68T 'PAGE:' #PAGE-NUMBER ( EM = ZZ,ZZ9 ) / 2T *INIT-USER '-' *PROGRAM 79T ' ' / 21T #ORGN-DISPLAY / 14T #TYPE-DISPLAY // 'LEDGER NUMBER' 17T 'LEDGER DESCRIPTION' 74T 'AMOUNT' / '-' ( 79 )
            TabSetting(68),"PAGE:",pnd_Ws_Pnd_Page_Number, new ReportEditMask ("ZZ,ZZ9"),NEWLINE,new TabSetting(2),Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(79)," ",NEWLINE,new TabSetting(21),pnd_Ws_Pnd_Orgn_Display,NEWLINE,new TabSetting(14),pnd_Ws_Pnd_Type_Display,NEWLINE,NEWLINE,"LEDGER NUMBER",new 
            TabSetting(17),"LEDGER DESCRIPTION",new TabSetting(74),"AMOUNT",NEWLINE,"-",new RepeatItem(79));
        getReports().write(2, ReportOption.TITLE,  ReportTitleOptions.LEFTJUSTIFIED,new TabSetting(21),pnd_Ws_Pnd_Orgn_Display,new TabSetting(81),pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time,pnd_Ws_Pnd_Cntrct_Orgn_Cde,pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr,  //Natural: NEWPAGE ( 02 ) IF LESS THAN #LINE-COUNT LINES LEFT WITH TITLE LEFT 21T #ORGN-DISPLAY 81T #PYMNT-REQST-LOG-DTE-TIME #CNTRCT-ORGN-CDE #CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) *LINE-COUNT ( 02 ) ( EM = 99 ) #CNTRCT-CANCEL-RDRW-ACTVTY-CDE / 14T #TYPE-DISPLAY 81T #PYMNT-REQST-LOG-DTE-TIME #CNTRCT-ORGN-CDE #CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) *LINE-COUNT ( 02 ) ( EM = 99 ) #CNTRCT-CANCEL-RDRW-ACTVTY-CDE / 81T #PYMNT-REQST-LOG-DTE-TIME #CNTRCT-ORGN-CDE #CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) *LINE-COUNT ( 02 ) ( EM = 99 ) #CNTRCT-CANCEL-RDRW-ACTVTY-CDE / 'LEDGER NUMBER' 17T 'LEDGER DESCRIPTION' 74T 'AMOUNT' 81T #PYMNT-REQST-LOG-DTE-TIME #CNTRCT-ORGN-CDE #CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) *LINE-COUNT ( 02 ) ( EM = 99 ) #CNTRCT-CANCEL-RDRW-ACTVTY-CDE / '-' ( 79 ) 81T #PYMNT-REQST-LOG-DTE-TIME #CNTRCT-ORGN-CDE #CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) *LINE-COUNT ( 02 ) ( EM = 99 ) #CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Page_Number, new ReportEditMask ("99999"),getReports().getAstLineCount(2), new ReportEditMask 
            ("99"),pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde,NEWLINE,new TabSetting(14),pnd_Ws_Pnd_Type_Display,new TabSetting(81),pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time,pnd_Ws_Pnd_Cntrct_Orgn_Cde,pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr, 
            new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Page_Number, new ReportEditMask ("99999"),getReports().getAstLineCount(2), new ReportEditMask 
            ("99"),pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde,NEWLINE,new TabSetting(81),pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time,pnd_Ws_Pnd_Cntrct_Orgn_Cde,pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr, 
            new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Page_Number, new ReportEditMask ("99999"),getReports().getAstLineCount(2), new ReportEditMask 
            ("99"),pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde,NEWLINE,"LEDGER NUMBER",new TabSetting(17),"LEDGER DESCRIPTION",new TabSetting(74),"AMOUNT",new 
            TabSetting(81),pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time,pnd_Ws_Pnd_Cntrct_Orgn_Cde,pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr, new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Page_Number, 
            new ReportEditMask ("99999"),getReports().getAstLineCount(2), new ReportEditMask ("99"),pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde,NEWLINE,"-",new 
            RepeatItem(79),new TabSetting(81),pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time,pnd_Ws_Pnd_Cntrct_Orgn_Cde,pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr, new ReportEditMask 
            ("999999999999"),pnd_Ws_Pnd_Page_Number, new ReportEditMask ("99999"),getReports().getAstLineCount(2), new ReportEditMask ("99"),pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde);
    }
    private void sub_Write_End_Line() throws Exception                                                                                                                    //Natural: WRITE-END-LINE
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        getReports().write(2, ReportOption.NOTITLE,new TabSetting(81),pnd_Ws_Pnd_Pymnt_Reqst_Log_Dte_Time,pnd_Ws_Pnd_Cntrct_Orgn_Cde,pnd_Ws_Pnd_Cntrct_Unq_Id_Nbr,        //Natural: WRITE ( 02 ) 81T #PYMNT-REQST-LOG-DTE-TIME #CNTRCT-ORGN-CDE #CNTRCT-UNQ-ID-NBR ( EM = 999999999999 ) #PAGE-NUMBER ( EM = 99999 ) '99' #CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            new ReportEditMask ("999999999999"),pnd_Ws_Pnd_Page_Number, new ReportEditMask ("99999"),"99",pnd_Ws_Pnd_Cntrct_Cancel_Rdrw_Actvty_Cde);
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* *MOVE FCPA110.START-CHECK-PREFIX-N3 TO #CHECK-NUMBER-N3          /*RL
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl123_getPnd_Warrant_Extract_Pnd_Record_TypeIsBreak = ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().isBreak(endOfData);
        if (condition(ldaFcpl123_getPnd_Warrant_Extract_Pnd_Record_TypeIsBreak))
        {
            //*  ------------------------
            //*  PYMNT-ADDR
            if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().equals(1)))                                                                                 //Natural: IF #RECORD-TYPE = 1
            {
                                                                                                                                                                          //Natural: PERFORM WRITE-LEDGER-LINE
                sub_Write_Ledger_Line();
                if (condition(Global.isEscape())) {return;}
                //*  END OF PAYEE
                                                                                                                                                                          //Natural: PERFORM WRITE-END-LINE
                sub_Write_End_Line();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM INIT-PAGE-1
                sub_Init_Page_1();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM INIT-PAGE-2
                sub_Init_Page_2();
                if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PYMNT-ADDR-PAGE-1
                sub_Display_Pymnt_Addr_Page_1();
                if (condition(Global.isEscape())) {return;}
                //*  LEDGER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  EOF
                if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().equals(readWork01Pnd_Record_TypeOld)))                                                  //Natural: IF #RECORD-TYPE = OLD ( #RECORD-TYPE )
                {
                                                                                                                                                                          //Natural: PERFORM WRITE-LEDGER-LINE
                    sub_Write_Ledger_Line();
                    if (condition(Global.isEscape())) {return;}
                    //*  END OF PAYEE
                                                                                                                                                                          //Natural: PERFORM WRITE-END-LINE
                    sub_Write_End_Line();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  DEDUCTIONS
                    if (condition(ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Orgn_Cde().equals("DC")))                                                                      //Natural: IF CNTRCT-ORGN-CDE = 'DC'
                    {
                        FOR01:                                                                                                                                            //Natural: FOR #DED-SUB = 1 TO 10
                        for (pnd_Ws_Pnd_Ded_Sub.setValue(1); condition(pnd_Ws_Pnd_Ded_Sub.lessOrEqual(10)); pnd_Ws_Pnd_Ded_Sub.nadd(1))
                        {
                            if (condition(pnd_Ws_Pnd_Pymnt_Ded_Amt_10.getValue(pnd_Ws_Pnd_Ded_Sub).notEquals(new DbsDecimal("0.00"))))                                    //Natural: IF #PYMNT-DED-AMT-10 ( #DED-SUB ) NE 0.00
                            {
                                pnd_Ws_Pnd_Ded_Top.nadd(1);                                                                                                               //Natural: ADD 1 TO #DED-TOP
                                pnd_Ws_Pnd_Col.setValue(pnd_Ws_Pnd_Pymnt_Ded_Col.getValue(pnd_Ws_Pnd_Ded_Top));                                                           //Natural: ASSIGN #COL := #PYMNT-DED-COL ( #DED-TOP )
                                pnd_Ws_Pnd_Row.setValue(pnd_Ws_Pnd_Pymnt_Ded_Row.getValue(pnd_Ws_Pnd_Ded_Top));                                                           //Natural: ASSIGN #ROW := #PYMNT-DED-ROW ( #DED-TOP )
                                pnd_Ws_Pnd_Pymnt_Ded_Cde_Cv.getValue(pnd_Ws_Pnd_Col,pnd_Ws_Pnd_Row).setValue("AD=I");                                                     //Natural: MOVE ( AD = I ) TO #PYMNT-DED-CDE-CV ( #COL,#ROW ) #PYMNT-DED-PAYEE-CDE-CV ( #COL,#ROW ) #PYMNT-DED-AMT-CV ( #COL,#ROW )
                                pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_Cv.getValue(pnd_Ws_Pnd_Col,pnd_Ws_Pnd_Row).setValue("AD=I");
                                pnd_Ws_Pnd_Pymnt_Ded_Amt_Cv.getValue(pnd_Ws_Pnd_Col,pnd_Ws_Pnd_Row).setValue("AD=I");
                                pnd_Ws_Pnd_Pymnt_Ded_Cde.getValue(pnd_Ws_Pnd_Col,pnd_Ws_Pnd_Row).setValue(pnd_Ws_Pnd_Pymnt_Ded_Cde_10.getValue(pnd_Ws_Pnd_Ded_Sub));      //Natural: MOVE #PYMNT-DED-CDE-10 ( #DED-SUB ) TO #PYMNT-DED-CDE ( #COL,#ROW )
                                pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde.getValue(pnd_Ws_Pnd_Col,pnd_Ws_Pnd_Row).setValue(pnd_Ws_Pnd_Pymnt_Ded_Payee_Cde_10.getValue(pnd_Ws_Pnd_Ded_Sub)); //Natural: MOVE #PYMNT-DED-PAYEE-CDE-10 ( #DED-SUB ) TO #PYMNT-DED-PAYEE-CDE ( #COL,#ROW )
                                pnd_Ws_Pnd_Pymnt_Ded_Amt.getValue(pnd_Ws_Pnd_Col,pnd_Ws_Pnd_Row).setValue(pnd_Ws_Pnd_Pymnt_Ded_Amt_10.getValue(pnd_Ws_Pnd_Ded_Sub));      //Natural: MOVE #PYMNT-DED-AMT-10 ( #DED-SUB ) TO #PYMNT-DED-AMT ( #COL,#ROW )
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-FOR
                        if (condition(Global.isEscape())) return;
                        pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                   //Natural: ADD 1 TO #PAGE-NUMBER
                        getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123c.class));                                                              //Natural: WRITE ( 01 ) NOTITLE USING MAP 'FCPM123C'
                        getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123h.class));                                                              //Natural: WRITE ( 02 ) NOTITLE USING MAP 'FCPM123H'
                        if (condition(pnd_Ws_Pnd_Ded_Top.greater(getZero())))                                                                                             //Natural: IF #DED-TOP > 0
                        {
                            getReports().write(1, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123d.class));                                                          //Natural: WRITE ( 1 ) NOTITLE USING MAP 'FCPM123D'
                            getReports().write(2, ReportOption.NOTITLE, writeMapToStringOutput(Fcpm123i.class));                                                          //Natural: WRITE ( 2 ) NOTITLE USING MAP 'FCPM123I'
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INIT-PAGE-2
                        sub_Init_Page_2();
                        if (condition(Global.isEscape())) {return;}
                        pnd_Ws_Pnd_Pymnt_Ded_Grp_Init.resetInitial();                                                                                                     //Natural: RESET INITIAL #PYMNT-DED-GRP-INIT
                        pnd_Ws_Pnd_Ded_Top.reset();                                                                                                                       //Natural: RESET #DED-TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Line_Count.setValue(100);                                                                                                                  //Natural: MOVE 100 TO #LINE-COUNT
                                                                                                                                                                          //Natural: PERFORM PROCESS-NEW-LEDGER
                    sub_Process_New_Ledger();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
                //*  NOT EOF
                if (condition(ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().notEquals(readWork01Pnd_Record_TypeOld)))                                               //Natural: IF #RECORD-TYPE NE OLD ( #RECORD-TYPE )
                {
                    pnd_Ws_Pnd_Page_Number.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PAGE-NUMBER
                                                                                                                                                                          //Natural: PERFORM NEWPAGE-DISPLAY
                    sub_Newpage_Display();
                    if (condition(Global.isEscape())) {return;}
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=80 ZP=ON");
        Global.format(2, "PS=20 LS=119 ZP=ON");
    }
    private void CheckAtStartofData525() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*  ----------------
                                                                                                                                                                          //Natural: PERFORM INIT-PAGE-1
            sub_Init_Page_1();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM DISPLAY-PYMNT-ADDR-PAGE-1
            sub_Display_Pymnt_Addr_Page_1();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
