/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:29 PM
**        * FROM NATURAL PROGRAM : Fcpp182a
************************************************************
**        * FILE NAME            : Fcpp182a.java
**        * CLASS NAME           : Fcpp182a
**        * INSTANCE NAME        : Fcpp182a
************************************************************
************************************************************************
* PROGRAM  : FCPP182A
* SYSTEM   : CPS
* TITLE    : NEW ANNUITIZATION PAYMENT REGISTER MERGE FILES
* FUNCTION : THIS PROGRAM CREATES SORT KEY FOR EACH RECORD AND ALSO
*            CREATES SORT HEADER RECORDS FOR USE IN FCPP182.
* AUTHOR   : M. MURILLO
* CREATED  : MARCH 21, 2001
************************************************************************
* HISTORY
* 04/18/01 M. MURILLO  BREAKOUT TPA,IPRO AND P&I FOR INTERNAL ROLLOVERS
* 08/10/01 M. ACLAN    IF NO RECORDS STILL PRINT THE HEADERS AND ALWAYS
*                      AL AND NZ.
* 4/2017    : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp182a extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private PdaFcpaext2 pdaFcpaext2;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_I1;
    private DbsField pnd_I2;
    private DbsField pnd_Max;
    private DbsField pnd_Max2;
    private DbsField pnd_Sort_Cde_Tbl;
    private DbsField pnd_Sort_Cde_Desc_Tbl;
    private DbsField pnd_Cntrct_Orgn_Cde;
    private DbsField pnd_Save_Cntrct_Orgn_Cde;
    private DbsField pnd_Ndx;
    private DbsField pnd_Date;
    private DbsField pnd_Time;
    private DbsField pnd_Rec_Read;
    private DbsField pnd_Rec_Read_Cancel;
    private DbsField pnd_Rec_Read_Cancelfslash_Noredraw;
    private DbsField pnd_Rec_Read_Stop;
    private DbsField pnd_Rec_Read_Stopfslash_Noredraw;
    private DbsField pnd_Rec_Read_Redraw;
    private DbsField pnd_Rec_Read_Reg;
    private DbsField pnd_Rec_Read_Pamp_I;
    private DbsField pnd_Rec_Read_Ipro;
    private DbsField pnd_Rec_Read_Tpa;
    private DbsField pnd_Rec_Read_Int_Roll_Classic;
    private DbsField pnd_Rec_Read_Int_Roll_Roth;
    private DbsField pnd_Rec_Read_Int_Roll_Others;
    private DbsField pnd_Rec_Read_Int_Roll_Pamp_I;
    private DbsField pnd_Rec_Read_Int_Roll_Ipro;
    private DbsField pnd_Rec_Read_Int_Roll_Tpa;
    private DbsField pnd_Rec_Written;
    private DbsField pnd_Rec_Written_Hdr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        pdaFcpaext2 = new PdaFcpaext2(localVariables);

        // Local Variables
        pnd_I1 = localVariables.newFieldInRecord("pnd_I1", "#I1", FieldType.INTEGER, 2);
        pnd_I2 = localVariables.newFieldInRecord("pnd_I2", "#I2", FieldType.INTEGER, 2);
        pnd_Max = localVariables.newFieldInRecord("pnd_Max", "#MAX", FieldType.INTEGER, 2);
        pnd_Max2 = localVariables.newFieldInRecord("pnd_Max2", "#MAX2", FieldType.INTEGER, 2);
        pnd_Sort_Cde_Tbl = localVariables.newFieldArrayInRecord("pnd_Sort_Cde_Tbl", "#SORT-CDE-TBL", FieldType.STRING, 6, new DbsArrayController(1, 15));
        pnd_Sort_Cde_Desc_Tbl = localVariables.newFieldArrayInRecord("pnd_Sort_Cde_Desc_Tbl", "#SORT-CDE-DESC-TBL", FieldType.STRING, 50, new DbsArrayController(1, 
            15));
        pnd_Cntrct_Orgn_Cde = localVariables.newFieldArrayInRecord("pnd_Cntrct_Orgn_Cde", "#CNTRCT-ORGN-CDE", FieldType.STRING, 2, new DbsArrayController(1, 
            5));
        pnd_Save_Cntrct_Orgn_Cde = localVariables.newFieldInRecord("pnd_Save_Cntrct_Orgn_Cde", "#SAVE-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ndx = localVariables.newFieldInRecord("pnd_Ndx", "#NDX", FieldType.INTEGER, 2);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.DATE);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.TIME);
        pnd_Rec_Read = localVariables.newFieldInRecord("pnd_Rec_Read", "#REC-READ", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read_Cancel = localVariables.newFieldInRecord("pnd_Rec_Read_Cancel", "#REC-READ-CANCEL", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read_Cancelfslash_Noredraw = localVariables.newFieldInRecord("pnd_Rec_Read_Cancelfslash_Noredraw", "#REC-READ-CANCEL/NOREDRAW", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Rec_Read_Stop = localVariables.newFieldInRecord("pnd_Rec_Read_Stop", "#REC-READ-STOP", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read_Stopfslash_Noredraw = localVariables.newFieldInRecord("pnd_Rec_Read_Stopfslash_Noredraw", "#REC-READ-STOP/NOREDRAW", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Rec_Read_Redraw = localVariables.newFieldInRecord("pnd_Rec_Read_Redraw", "#REC-READ-REDRAW", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read_Reg = localVariables.newFieldInRecord("pnd_Rec_Read_Reg", "#REC-READ-REG", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read_Pamp_I = localVariables.newFieldInRecord("pnd_Rec_Read_Pamp_I", "#REC-READ-P&I", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read_Ipro = localVariables.newFieldInRecord("pnd_Rec_Read_Ipro", "#REC-READ-IPRO", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read_Tpa = localVariables.newFieldInRecord("pnd_Rec_Read_Tpa", "#REC-READ-TPA", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Read_Int_Roll_Classic = localVariables.newFieldInRecord("pnd_Rec_Read_Int_Roll_Classic", "#REC-READ-INT-ROLL-CLASSIC", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Rec_Read_Int_Roll_Roth = localVariables.newFieldInRecord("pnd_Rec_Read_Int_Roll_Roth", "#REC-READ-INT-ROLL-ROTH", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Rec_Read_Int_Roll_Others = localVariables.newFieldInRecord("pnd_Rec_Read_Int_Roll_Others", "#REC-READ-INT-ROLL-OTHERS", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Rec_Read_Int_Roll_Pamp_I = localVariables.newFieldInRecord("pnd_Rec_Read_Int_Roll_Pamp_I", "#REC-READ-INT-ROLL-P&I", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Rec_Read_Int_Roll_Ipro = localVariables.newFieldInRecord("pnd_Rec_Read_Int_Roll_Ipro", "#REC-READ-INT-ROLL-IPRO", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Rec_Read_Int_Roll_Tpa = localVariables.newFieldInRecord("pnd_Rec_Read_Int_Roll_Tpa", "#REC-READ-INT-ROLL-TPA", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Written = localVariables.newFieldInRecord("pnd_Rec_Written", "#REC-WRITTEN", FieldType.PACKED_DECIMAL, 9);
        pnd_Rec_Written_Hdr = localVariables.newFieldInRecord("pnd_Rec_Written_Hdr", "#REC-WRITTEN-HDR", FieldType.PACKED_DECIMAL, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
        pnd_Max.setInitialValue(15);
        pnd_Max2.setInitialValue(2);
        pnd_Sort_Cde_Tbl.getValue(1).setInitialValue(" CSR1 ");
        pnd_Sort_Cde_Tbl.getValue(2).setInitialValue(" CSR2 ");
        pnd_Sort_Cde_Tbl.getValue(3).setInitialValue(" CSR3 ");
        pnd_Sort_Cde_Tbl.getValue(4).setInitialValue(" CSR4 ");
        pnd_Sort_Cde_Tbl.getValue(5).setInitialValue(" CSR5 ");
        pnd_Sort_Cde_Tbl.getValue(6).setInitialValue(" RREG ");
        pnd_Sort_Cde_Tbl.getValue(7).setInitialValue(" RP&I ");
        pnd_Sort_Cde_Tbl.getValue(8).setInitialValue(" RIPRO");
        pnd_Sort_Cde_Tbl.getValue(9).setInitialValue(" RTPA ");
        pnd_Sort_Cde_Tbl.getValue(10).setInitialValue("IRLC  ");
        pnd_Sort_Cde_Tbl.getValue(11).setInitialValue("IRLR  ");
        pnd_Sort_Cde_Tbl.getValue(12).setInitialValue("IRLX  ");
        pnd_Sort_Cde_Tbl.getValue(13).setInitialValue("IP&I  ");
        pnd_Sort_Cde_Tbl.getValue(14).setInitialValue("IIPRO ");
        pnd_Sort_Cde_Tbl.getValue(15).setInitialValue("ITPA  ");
        pnd_Sort_Cde_Desc_Tbl.getValue(1).setInitialValue("CANCEL");
        pnd_Sort_Cde_Desc_Tbl.getValue(2).setInitialValue("CANCEL NO REDRAW");
        pnd_Sort_Cde_Desc_Tbl.getValue(3).setInitialValue("STOP");
        pnd_Sort_Cde_Desc_Tbl.getValue(4).setInitialValue("STOP NO REDRAW");
        pnd_Sort_Cde_Desc_Tbl.getValue(5).setInitialValue("REDRAW");
        pnd_Sort_Cde_Desc_Tbl.getValue(6).setInitialValue("REGULAR");
        pnd_Sort_Cde_Desc_Tbl.getValue(7).setInitialValue("P&I");
        pnd_Sort_Cde_Desc_Tbl.getValue(8).setInitialValue("IPRO");
        pnd_Sort_Cde_Desc_Tbl.getValue(9).setInitialValue("TPA");
        pnd_Sort_Cde_Desc_Tbl.getValue(10).setInitialValue("CLASSIC INT. ROLLOVER");
        pnd_Sort_Cde_Desc_Tbl.getValue(11).setInitialValue("ROTH INT. ROLLOVER");
        pnd_Sort_Cde_Desc_Tbl.getValue(12).setInitialValue("OTHER INT. ROLLOVER");
        pnd_Sort_Cde_Desc_Tbl.getValue(13).setInitialValue("P&I INT. ROLLOVER");
        pnd_Sort_Cde_Desc_Tbl.getValue(14).setInitialValue("IPRO INT. ROLLOVER");
        pnd_Sort_Cde_Desc_Tbl.getValue(15).setInitialValue("TPA INT. ROLLOVER");
        pnd_Cntrct_Orgn_Cde.getValue(1).setInitialValue("NZ");
        pnd_Cntrct_Orgn_Cde.getValue(2).setInitialValue("AL");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp182a() throws Exception
    {
        super("Fcpp182a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*  DEFINE PRINTERS AND FORMATS
        getReports().definePrinter(2, "R1");                                                                                                                              //Natural: DEFINE PRINTER ( R1 = 1 )
        //*                                                                                                                                                               //Natural: FORMAT ( R1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        //*   MLA 8/10
                                                                                                                                                                          //Natural: PERFORM INITIALIZE
        sub_Initialize();
        if (condition(Global.isEscape())) {return;}
        //*  READ THE EXTRACT FILE
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 EXT ( * )
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            pnd_Rec_Read.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #REC-READ
            pdaFcpaext2.getSort_Key().moveAll("H'00'");                                                                                                                   //Natural: MOVE ALL H'00' TO SORT-KEY
            //*   IF EXT.CNTRCT-ORGN-CDE NE #SAVE-CNTRCT-ORGN-CDE          /* MLA 8/10
            //*      ADD 1 TO #NDX                                         /* MLA 8/10
            //*      ASSIGN #CNTRCT-ORGN-CDE (#NDX) = EXT.CNTRCT-ORGN-CDE  /* MLA 8/10
            //*      ASSIGN #SAVE-CNTRCT-ORGN-CDE = EXT.CNTRCT-ORGN-CDE    /* MLA 8/10
            //*   END-IF
            //*  NOT INT ROLLOVER
            if (condition(! (pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8))))                                                                                      //Natural: IF NOT EXT.PYMNT-PAY-TYPE-REQ-IND = 8
            {
                //*  JWO 2010-11
                short decideConditionsMet923 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'CP', 'RP', 'PR'
                if (condition((pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") 
                    || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR"))))
                {
                    decideConditionsMet923++;
                    pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(1));                                                                     //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 1 )
                    pnd_Rec_Read_Cancel.nadd(1);                                                                                                                          //Natural: ADD 1 TO #REC-READ-CANCEL
                }                                                                                                                                                         //Natural: VALUE 'CN'
                else if (condition((pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN"))))
                {
                    decideConditionsMet923++;
                    pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(2));                                                                     //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 2 )
                    //*  JWO 2010-11
                    pnd_Rec_Read_Cancelfslash_Noredraw.nadd(1);                                                                                                           //Natural: ADD 1 TO #REC-READ-CANCEL/NOREDRAW
                }                                                                                                                                                         //Natural: VALUE 'S', 'SP'
                else if (condition((pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") || pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP"))))
                {
                    decideConditionsMet923++;
                    pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(3));                                                                     //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 3 )
                    pnd_Rec_Read_Stop.nadd(1);                                                                                                                            //Natural: ADD 1 TO #REC-READ-STOP
                }                                                                                                                                                         //Natural: VALUE 'SN'
                else if (condition((pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN"))))
                {
                    decideConditionsMet923++;
                    pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(4));                                                                     //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 4 )
                    pnd_Rec_Read_Stopfslash_Noredraw.nadd(1);                                                                                                             //Natural: ADD 1 TO #REC-READ-STOP/NOREDRAW
                }                                                                                                                                                         //Natural: VALUE 'R'
                else if (condition((pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("R"))))
                {
                    decideConditionsMet923++;
                    pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(5));                                                                     //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 5 )
                    pnd_Rec_Read_Redraw.nadd(1);                                                                                                                          //Natural: ADD 1 TO #REC-READ-REDRAW
                }                                                                                                                                                         //Natural: VALUE ' '
                else if (condition((pdaFcpaext.getExt_Cntrct_Cancel_Rdrw_Actvty_Cde().equals(" "))))
                {
                    decideConditionsMet923++;
                    short decideConditionsMet941 = 0;                                                                                                                     //Natural: DECIDE ON FIRST VALUE EXT.CNTRCT-OPTION-CDE;//Natural: VALUE 22
                    if (condition((pdaFcpaext.getExt_Cntrct_Option_Cde().equals(22))))
                    {
                        decideConditionsMet941++;
                        pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(7));                                                                 //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 7 )
                        pnd_Rec_Read_Pamp_I.nadd(1);                                                                                                                      //Natural: ADD 1 TO #REC-READ-P&I
                    }                                                                                                                                                     //Natural: VALUE 25,27
                    else if (condition((pdaFcpaext.getExt_Cntrct_Option_Cde().equals(25) || pdaFcpaext.getExt_Cntrct_Option_Cde().equals(27))))
                    {
                        decideConditionsMet941++;
                        pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(8));                                                                 //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 8 )
                        pnd_Rec_Read_Ipro.nadd(1);                                                                                                                        //Natural: ADD 1 TO #REC-READ-IPRO
                    }                                                                                                                                                     //Natural: VALUE 28,30
                    else if (condition((pdaFcpaext.getExt_Cntrct_Option_Cde().equals(28) || pdaFcpaext.getExt_Cntrct_Option_Cde().equals(30))))
                    {
                        decideConditionsMet941++;
                        pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(9));                                                                 //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 9 )
                        pnd_Rec_Read_Tpa.nadd(1);                                                                                                                         //Natural: ADD 1 TO #REC-READ-TPA
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(6));                                                                 //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 6 )
                        pnd_Rec_Read_Reg.nadd(1);                                                                                                                         //Natural: ADD 1 TO #REC-READ-REG
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet923 > 0))
                {
                    pdaFcpaext2.getExt2_Extr2().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                //Natural: MOVE BY NAME EXTR TO EXTR2
                    pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde().setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                         //Natural: ASSIGN SORT-CNTRCT-ORGN-CDE = EXT.CNTRCT-ORGN-CDE
                    getWorkFiles().write(2, true, pdaFcpaext2.getSort_Key(), pdaFcpaext2.getExt2().getValue("*"));                                                        //Natural: WRITE WORK FILE 2 VARIABLE SORT-KEY EXT2 ( * )
                    pnd_Rec_Written.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-WRITTEN
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
                //*  INTERNAL ROLLOVER
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                short decideConditionsMet965 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE EXT.CNTRCT-OPTION-CDE;//Natural: VALUE 22
                if (condition((pdaFcpaext.getExt_Cntrct_Option_Cde().equals(22))))
                {
                    decideConditionsMet965++;
                    pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(13));                                                                    //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 13 )
                    pnd_Rec_Read_Int_Roll_Pamp_I.nadd(1);                                                                                                                 //Natural: ADD 1 TO #REC-READ-INT-ROLL-P&I
                }                                                                                                                                                         //Natural: VALUE 25,27
                else if (condition((pdaFcpaext.getExt_Cntrct_Option_Cde().equals(25) || pdaFcpaext.getExt_Cntrct_Option_Cde().equals(27))))
                {
                    decideConditionsMet965++;
                    pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(14));                                                                    //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 14 )
                    pnd_Rec_Read_Int_Roll_Ipro.nadd(1);                                                                                                                   //Natural: ADD 1 TO #REC-READ-INT-ROLL-IPRO
                }                                                                                                                                                         //Natural: VALUE 28,30
                else if (condition((pdaFcpaext.getExt_Cntrct_Option_Cde().equals(28) || pdaFcpaext.getExt_Cntrct_Option_Cde().equals(30))))
                {
                    decideConditionsMet965++;
                    pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(15));                                                                    //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 15 )
                    pnd_Rec_Read_Int_Roll_Tpa.nadd(1);                                                                                                                    //Natural: ADD 1 TO #REC-READ-INT-ROLL-TPA
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet965 > 0))
                {
                    pdaFcpaext2.getExt2_Extr2().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                //Natural: MOVE BY NAME EXTR TO EXTR2
                    pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde().setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                         //Natural: ASSIGN SORT-CNTRCT-ORGN-CDE = EXT.CNTRCT-ORGN-CDE
                    getWorkFiles().write(2, true, pdaFcpaext2.getSort_Key(), pdaFcpaext2.getExt2().getValue("*"));                                                        //Natural: WRITE WORK FILE 2 VARIABLE SORT-KEY EXT2 ( * )
                    pnd_Rec_Written.nadd(1);                                                                                                                              //Natural: ADD 1 TO #REC-WRITTEN
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    short decideConditionsMet981 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN SUBSTR ( EXT.CNTRCT-ROLL-DEST-CDE,1,3 ) = 'IRA'
                    if (condition(pdaFcpaext.getExt_Cntrct_Roll_Dest_Cde().getSubstring(1,3).equals("IRA")))
                    {
                        decideConditionsMet981++;
                        pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(10));                                                                //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 10 )
                        pnd_Rec_Read_Int_Roll_Classic.nadd(1);                                                                                                            //Natural: ADD 1 TO #REC-READ-INT-ROLL-CLASSIC
                    }                                                                                                                                                     //Natural: WHEN SUBSTR ( EXT.CNTRCT-ROLL-DEST-CDE,1,3 ) = 'RTH'
                    else if (condition(pdaFcpaext.getExt_Cntrct_Roll_Dest_Cde().getSubstring(1,3).equals("RTH")))
                    {
                        decideConditionsMet981++;
                        if (condition(! (pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("H") || pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("J")               //Natural: IF NOT ( EXT.CNTRCT-STTLMNT-TYPE-IND = 'H' OR = 'J' OR = 'T' )
                            || pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("T"))))
                        {
                            pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(10));                                                            //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 10 )
                            pnd_Rec_Read_Int_Roll_Classic.nadd(1);                                                                                                        //Natural: ADD 1 TO #REC-READ-INT-ROLL-CLASSIC
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(11));                                                            //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 11 )
                            pnd_Rec_Read_Int_Roll_Roth.nadd(1);                                                                                                           //Natural: ADD 1 TO #REC-READ-INT-ROLL-ROTH
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet981 > 0))
                    {
                        pdaFcpaext2.getExt2_Extr2().setValuesByName(pdaFcpaext.getExt_Extr());                                                                            //Natural: MOVE BY NAME EXTR TO EXTR2
                        pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde().setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                     //Natural: ASSIGN SORT-CNTRCT-ORGN-CDE = EXT.CNTRCT-ORGN-CDE
                        getWorkFiles().write(2, true, pdaFcpaext2.getSort_Key(), pdaFcpaext2.getExt2().getValue("*"));                                                    //Natural: WRITE WORK FILE 2 VARIABLE SORT-KEY EXT2 ( * )
                        pnd_Rec_Written.nadd(1);                                                                                                                          //Natural: ADD 1 TO #REC-WRITTEN
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(12));                                                                //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( 12 )
                        pnd_Rec_Read_Int_Roll_Others.nadd(1);                                                                                                             //Natural: ADD 1 TO #REC-READ-INT-ROLL-OTHERS
                        pdaFcpaext2.getExt2_Extr2().setValuesByName(pdaFcpaext.getExt_Extr());                                                                            //Natural: MOVE BY NAME EXTR TO EXTR2
                        pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde().setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                     //Natural: ASSIGN SORT-CNTRCT-ORGN-CDE = EXT.CNTRCT-ORGN-CDE
                        getWorkFiles().write(2, true, pdaFcpaext2.getSort_Key(), pdaFcpaext2.getExt2().getValue("*"));                                                    //Natural: WRITE WORK FILE 2 VARIABLE SORT-KEY EXT2 ( * )
                        pnd_Rec_Written.nadd(1);                                                                                                                          //Natural: ADD 1 TO #REC-WRITTEN
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),"NO OF EXTRACT RECORDS READ        :",pnd_Rec_Read, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                                 //Natural: WRITE ( R1 ) 40T 'NO OF EXTRACT RECORDS READ        :' #REC-READ
        if (Global.isEscape()) return;
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP ( R1 ) 1
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(1),":",pnd_Rec_Read_Cancel, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                          //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 1 ) ':' #REC-READ-CANCEL
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(2),":",pnd_Rec_Read_Cancelfslash_Noredraw, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));           //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 2 ) ':' #REC-READ-CANCEL/NOREDRAW
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(3),":",pnd_Rec_Read_Stop, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                            //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 3 ) ':' #REC-READ-STOP
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(4),":",pnd_Rec_Read_Stopfslash_Noredraw, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));             //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 4 ) ':' #REC-READ-STOP/NOREDRAW
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(5),":",pnd_Rec_Read_Redraw, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                          //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 5 ) ':' #REC-READ-REDRAW
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(6),":",pnd_Rec_Read_Reg, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                             //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 6 ) ':' #REC-READ-REG
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(7),":",pnd_Rec_Read_Pamp_I, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                          //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 7 ) ':' #REC-READ-P&I
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(8),":",pnd_Rec_Read_Ipro, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                            //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 8 ) ':' #REC-READ-IPRO
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(9),":",pnd_Rec_Read_Tpa, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                             //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 9 ) ':' #REC-READ-TPA
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(10),":",pnd_Rec_Read_Int_Roll_Classic, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));               //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 10 ) ':' #REC-READ-INT-ROLL-CLASSIC
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(11),":",pnd_Rec_Read_Int_Roll_Roth, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                  //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 11 ) ':' #REC-READ-INT-ROLL-ROTH
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(12),":",pnd_Rec_Read_Int_Roll_Others, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 12 ) ':' #REC-READ-INT-ROLL-OTHERS
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(13),":",pnd_Rec_Read_Int_Roll_Pamp_I, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 13 ) ':' #REC-READ-INT-ROLL-P&I
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(14),":",pnd_Rec_Read_Int_Roll_Ipro, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                  //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 14 ) ':' #REC-READ-INT-ROLL-IPRO
        if (Global.isEscape()) return;
        getReports().write(2, new TabSetting(40),pnd_Sort_Cde_Desc_Tbl.getValue(15),":",pnd_Rec_Read_Int_Roll_Tpa, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                   //Natural: WRITE ( R1 ) 40T #SORT-CDE-DESC-TBL ( 15 ) ':' #REC-READ-INT-ROLL-TPA
        if (Global.isEscape()) return;
        getReports().skip(0, 1);                                                                                                                                          //Natural: SKIP ( R1 ) 1
        getReports().write(2, new TabSetting(40),"NO OF EXTRACT RECORDS WRITTEN     :",pnd_Rec_Written, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                              //Natural: WRITE ( R1 ) 40T 'NO OF EXTRACT RECORDS WRITTEN     :' #REC-WRITTEN
        if (Global.isEscape()) return;
        getReports().skip(0, 2);                                                                                                                                          //Natural: SKIP ( R1 ) 2
        getReports().write(2, new TabSetting(40),"NO OF HEADER RECORDS WRITTEN      :",pnd_Rec_Written_Hdr, new ReportEditMask ("ZZZ,ZZZ,ZZ9"));                          //Natural: WRITE ( R1 ) 40T 'NO OF HEADER RECORDS WRITTEN      :' #REC-WRITTEN-HDR
        if (Global.isEscape()) return;
        getReports().skip(0, 2);                                                                                                                                          //Natural: SKIP ( R1 ) 2
        getReports().write(2, new TabSetting(45),"CONTRCT CODES PROCESSED:",NEWLINE);                                                                                     //Natural: WRITE ( R1 ) 45T 'CONTRCT CODES PROCESSED:' /
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #I2 = 1 TO #NDX
        for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(pnd_Ndx)); pnd_I2.nadd(1))
        {
            getReports().write(2, new TabSetting(50),"=>",pnd_Cntrct_Orgn_Cde.getValue(pnd_I2));                                                                          //Natural: WRITE ( R1 ) 50T '=>' #CNTRCT-ORGN-CDE ( #I2 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().skip(0, 5);                                                                                                                                          //Natural: SKIP ( R1 ) 5
        if (condition(pnd_Rec_Read.notEquals(pnd_Rec_Written)))                                                                                                           //Natural: IF #REC-READ NE #REC-WRITTEN
        {
            getReports().write(2, new TabSetting(40),"****** NO OF RECORDS READ DOES NOT MATCH NO OF RECORDS WRITTEN ******");                                            //Natural: WRITE ( R1 ) 40T '****** NO OF RECORDS READ DOES NOT MATCH NO OF RECORDS WRITTEN ******'
            if (Global.isEscape()) return;
            getReports().skip(0, 3);                                                                                                                                      //Natural: SKIP ( R1 ) 3
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(2, new TabSetting(45),"***** END OF REPORT *****");                                                                                            //Natural: WRITE ( R1 ) 45T '***** END OF REPORT *****'
        if (Global.isEscape()) return;
        //* *****************************************************************
        //* *******************************************************************
        //* *MOVE ALL H'00' TO EXT2(*)
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( R1 )
    }
    private void sub_Write_Header_Record() throws Exception                                                                                                               //Natural: WRITE-HEADER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************************************
        pdaFcpaext2.getSort_Key_Sort_Option_Cde().setValue(pnd_Sort_Cde_Tbl.getValue(pnd_I1));                                                                            //Natural: ASSIGN SORT-OPTION-CDE = #SORT-CDE-TBL ( #I1 )
        pdaFcpaext2.getExt2_Hdr_Title().getValue(2).setValue(pnd_Sort_Cde_Desc_Tbl.getValue(pnd_I1));                                                                     //Natural: ASSIGN EXT2.HDR-TITLE ( 2 ) = #SORT-CDE-DESC-TBL ( #I1 )
        short decideConditionsMet1067 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE SORT-CNTRCT-ORGN-CDE;//Natural: VALUE 'NZ'
        if (condition((pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde().equals("NZ"))))
        {
            decideConditionsMet1067++;
            pdaFcpaext2.getExt2_Hdr_Title().getValue(1).setValue("ADAM");                                                                                                 //Natural: ASSIGN EXT2.HDR-TITLE ( 1 ) = 'ADAM'
        }                                                                                                                                                                 //Natural: VALUE 'AL'
        else if (condition((pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde().equals("AL"))))
        {
            decideConditionsMet1067++;
            pdaFcpaext2.getExt2_Hdr_Title().getValue(1).setValue("ANNUITY LOAN");                                                                                         //Natural: ASSIGN EXT2.HDR-TITLE ( 1 ) = 'ANNUITY LOAN'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            pdaFcpaext2.getExt2_Hdr_Title().getValue(1).setValue("UNKNOWN PAYMENT TITLE");                                                                                //Natural: ASSIGN EXT2.HDR-TITLE ( 1 ) = 'UNKNOWN PAYMENT TITLE'
        }                                                                                                                                                                 //Natural: END-DECIDE
        getWorkFiles().write(3, true, pdaFcpaext2.getSort_Key(), pdaFcpaext2.getExt2().getValue("*"));                                                                    //Natural: WRITE WORK FILE 3 VARIABLE SORT-KEY EXT2 ( * )
        pnd_Rec_Written_Hdr.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #REC-WRITTEN-HDR
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }
    private void sub_Initialize() throws Exception                                                                                                                        //Natural: INITIALIZE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************************************
        //*  PRODUCE HEADER RECORDS
        pdaFcpaext2.getExt2().getValue("*").moveAll("H'00'");                                                                                                             //Natural: MOVE ALL H'00' TO EXT2 ( * )
        pdaFcpaext2.getExt2_Cntrct_Rqst_Settl_Dte().setValue(pnd_Date);                                                                                                   //Natural: MOVE #DATE TO EXT2.CNTRCT-RQST-SETTL-DTE EXT2.CNTRCT-RQST-SETTL-DTE EXT2.CNTRCT-RQST-DTE EXT2.PYMNT-CHECK-DTE EXT2.PYMNT-CYCLE-DTE EXT2.PYMNT-EFT-DTE EXT2.PYMNT-SETTLMNT-DTE EXT2.PYMNT-RPRNT-RQUST-DTE EXT2.PYMNT-RPRNT-DTE EXT2.PYMNT-IA-ISSUE-DTE EXT2.PYMNT-DOB EXT2.PYMNT-DEATH-DTE EXT2.PYMNT-PROOF-DTE EXT2.CNR-CS-PYMNT-INTRFCE-DTE EXT2.CNR-CS-PYMNT-ACCTG-DTE EXT2.CNR-CS-PYMNT-CHECK-DTE EXT2.CNR-CS-RQUST-DTE EXT2.CNR-ORGNL-PYMNT-ACCTG-DTE EXT2.CNR-ORGNL-PYMNT-INTRFCE-DTE EXT2.CNR-RDRW-RQUST-DTE EXT2.CNR-RDRW-PYMNT-INTRFCE-DTE EXT2.CNR-RDRW-PYMNT-ACCTG-DTE EXT2.CNR-RDRW-PYMNT-CHECK-DTE
        pdaFcpaext2.getExt2_Cntrct_Rqst_Settl_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cntrct_Rqst_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Check_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Cycle_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Eft_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Settlmnt_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Rprnt_Rqust_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Rprnt_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Ia_Issue_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Dob().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Death_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Pymnt_Proof_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Cs_Pymnt_Intrfce_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Cs_Pymnt_Acctg_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Cs_Pymnt_Check_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Cs_Rqust_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Orgnl_Pymnt_Acctg_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Orgnl_Pymnt_Intrfce_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Rdrw_Rqust_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Rdrw_Pymnt_Intrfce_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Rdrw_Pymnt_Acctg_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cnr_Rdrw_Pymnt_Check_Dte().setValue(pnd_Date);
        pdaFcpaext2.getExt2_Cntrct_Rqst_Settl_Tme().setValue(pnd_Time);                                                                                                   //Natural: MOVE #TIME TO EXT2.CNTRCT-RQST-SETTL-TME EXT2.CNTRCT-RQST-SETTL-TME
        pdaFcpaext2.getExt2_Cntrct_Rqst_Settl_Tme().setValue(pnd_Time);
        FOR02:                                                                                                                                                            //Natural: FOR #I1 = 1 TO #MAX
        for (pnd_I1.setValue(1); condition(pnd_I1.lessOrEqual(pnd_Max)); pnd_I1.nadd(1))
        {
            //*  MLA 8/10
            pdaFcpaext2.getSort_Key().moveAll("H'00'");                                                                                                                   //Natural: MOVE ALL H'00' TO SORT-KEY
            FOR03:                                                                                                                                                        //Natural: FOR #I2 = 1 TO #MAX2
            for (pnd_I2.setValue(1); condition(pnd_I2.lessOrEqual(pnd_Max2)); pnd_I2.nadd(1))
            {
                pdaFcpaext2.getSort_Key_Sort_Cntrct_Orgn_Cde().setValue(pnd_Cntrct_Orgn_Cde.getValue(pnd_I2));                                                            //Natural: ASSIGN SORT-CNTRCT-ORGN-CDE = #CNTRCT-ORGN-CDE ( #I2 )
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADER-RECORD
                sub_Write_Header_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (true) return;                                                                                                                                                 //Natural: ESCAPE ROUTINE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new      //Natural: WRITE ( R1 ) *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( R1 ) ( AD = L NL = 4 ) / *INIT-USER '-' *PROGRAM 45T 'PAYMENT REGISTER MERGE FILES - SORT KEY/HEADER CREATION'
                        TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new FieldAttributes ("AD=L"), new NumericLength (4),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(45),"PAYMENT REGISTER MERGE FILES - SORT KEY/HEADER CREATION");
                    getReports().skip(0, 5);                                                                                                                              //Natural: SKIP ( R1 ) 5
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
