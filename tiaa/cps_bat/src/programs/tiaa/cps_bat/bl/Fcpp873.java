/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:02 PM
**        * FROM NATURAL PROGRAM : Fcpp873
************************************************************
**        * FILE NAME            : Fcpp873.java
**        * CLASS NAME           : Fcpp873
**        * INSTANCE NAME        : Fcpp873
************************************************************
************************************************************************
* PROGRAM  : FCPP873
* SYSTEM   : CPS
* TITLE    :
* FUNCTION : CALCULATE AND PROPOGATE CHECK FIELDS.
*
* 06/10/1998 - RIAD LOUTFI - ADDED RETIREMENT LOAN PROCESSING.
* 10/29/1999 - ROXAN C.    - ADDED ELECTRONIC WARRANTS PROCESSING.
*              FCPAEXT - REPLACED FCPA371, FCPA801C AND FCPA873
* 04/17/2003 - ROXAN C.    - EXPANDED FCPAEXT
* 04/2017 - JJG PIN EXPANSION STOW TO PICK UP CHANGE TO FCPAEXT
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp873 extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private LdaFcpl802 ldaFcpl802;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;

    private DbsGroup pnd_Key_Pnd_Key_Detail;
    private DbsField pnd_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Pymnt_Check_Dte;
    private DbsField pnd_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Key_Pymnt_Prcss_Seq_Num;

    private DbsGroup pnd_Pymnt_Fields;
    private DbsField pnd_Pymnt_Fields_Pymnt_Gross_Amt;
    private DbsField pnd_Pymnt_Fields_Pymnt_Ivc_Amt;
    private DbsField pnd_Pymnt_Fields_Pymnt_Total_Pages;
    private DbsField pnd_Pymnt_Fields_Pnd_Pymnt_Records;
    private DbsField pnd_Pymnt_Fields_Pnd_Dpi_Dci_Ind;
    private DbsField pnd_Pymnt_Fields_Pnd_Pymnt_Ded_Ind;
    private DbsField pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Accum_Ded_Table;
    private DbsField pnd_Ws_Pnd_Pymnt_Ded_Table;
    private DbsField pnd_Ws_Pymnt_Gross_Amt;
    private DbsField pnd_Ws_Pymnt_Ivc_Amt;
    private DbsField pnd_Ws_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_J;
    private DbsField pnd_Ws_Pnd_Check_To_Annt;
    private DbsField pnd_Count;
    private DbsField rtb;
    private DbsField ivc_From_Rtb_Rollover;
    private DbsField pnd_Ws_Save_Number_Records;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        ldaFcpl802 = new LdaFcpl802();
        registerRecord(ldaFcpl802);

        // Local Variables
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 31);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);

        pnd_Key_Pnd_Key_Detail = pnd_Key__R_Field_1.newGroupInGroup("pnd_Key_Pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Cntrct_Orgn_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Pymnt_Check_Dte = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Key_Cntrct_Ppcn_Nbr = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Key_Cntrct_Payee_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Key_Pymnt_Prcss_Seq_Num = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);

        pnd_Pymnt_Fields = localVariables.newGroupInRecord("pnd_Pymnt_Fields", "#PYMNT-FIELDS");
        pnd_Pymnt_Fields_Pymnt_Gross_Amt = pnd_Pymnt_Fields.newFieldInGroup("pnd_Pymnt_Fields_Pymnt_Gross_Amt", "PYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Pymnt_Fields_Pymnt_Ivc_Amt = pnd_Pymnt_Fields.newFieldInGroup("pnd_Pymnt_Fields_Pymnt_Ivc_Amt", "PYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Pymnt_Fields_Pymnt_Total_Pages = pnd_Pymnt_Fields.newFieldInGroup("pnd_Pymnt_Fields_Pymnt_Total_Pages", "PYMNT-TOTAL-PAGES", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Pymnt_Fields_Pnd_Pymnt_Records = pnd_Pymnt_Fields.newFieldInGroup("pnd_Pymnt_Fields_Pnd_Pymnt_Records", "#PYMNT-RECORDS", FieldType.PACKED_DECIMAL, 
            3);
        pnd_Pymnt_Fields_Pnd_Dpi_Dci_Ind = pnd_Pymnt_Fields.newFieldInGroup("pnd_Pymnt_Fields_Pnd_Dpi_Dci_Ind", "#DPI-DCI-IND", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Fields_Pnd_Pymnt_Ded_Ind = pnd_Pymnt_Fields.newFieldInGroup("pnd_Pymnt_Fields_Pnd_Pymnt_Ded_Ind", "#PYMNT-DED-IND", FieldType.BOOLEAN, 
            1);
        pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table = pnd_Pymnt_Fields.newFieldArrayInGroup("pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table", "#DED-PYMNT-TABLE", FieldType.STRING, 
            3, new DbsArrayController(1, 4));

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Accum_Ded_Table = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Accum_Ded_Table", "#ACCUM-DED-TABLE", FieldType.BOOLEAN, 1, new DbsArrayController(1, 
            14));
        pnd_Ws_Pnd_Pymnt_Ded_Table = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Pymnt_Ded_Table", "#PYMNT-DED-TABLE", FieldType.BOOLEAN, 1, new DbsArrayController(1, 
            14));
        pnd_Ws_Pymnt_Gross_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Gross_Amt", "PYMNT-GROSS-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pymnt_Ivc_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Ivc_Amt", "PYMNT-IVC-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pymnt_Check_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_J = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_J", "#J", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Check_To_Annt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Check_To_Annt", "#CHECK-TO-ANNT", FieldType.BOOLEAN, 1);
        pnd_Count = localVariables.newFieldInRecord("pnd_Count", "#COUNT", FieldType.NUMERIC, 5);
        rtb = localVariables.newFieldInRecord("rtb", "RTB", FieldType.BOOLEAN, 1);
        ivc_From_Rtb_Rollover = localVariables.newFieldInRecord("ivc_From_Rtb_Rollover", "IVC-FROM-RTB-ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Ws_Save_Number_Records = localVariables.newFieldInRecord("pnd_Ws_Save_Number_Records", "#WS-SAVE-NUMBER-RECORDS", FieldType.PACKED_DECIMAL, 
            3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl802.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp873() throws Exception
    {
        super("Fcpp873");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //* *--------
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*  READ  WORK FILE 01  #CHECK-SORT-FIELDS  NZ-PYMNT-ADDR   /* ROXAN 12/6
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 EXT ( * )
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            CheckAtStartofData491();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            //*   MOVE BY NAME NZ-PYMNT-ADDR   TO #KEY-DETAIL            /*
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                             //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
                                                                                                                                                                          //Natural: PERFORM DETERMINE-CHECK-FIELDS
            sub_Determine_Check_Fields();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpaext.getExt_Pymnt_Check_Amt().setValue(pnd_Ws_Pymnt_Check_Amt);                                                                                         //Natural: ASSIGN EXT.PYMNT-CHECK-AMT := #WS.PYMNT-CHECK-AMT
            getSort().writeSortInData(pdaFcpaext.getExt_Cntrct_Orgn_Cde(), pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), pdaFcpaext.getExt_Cntrct_Payee_Cde(),                     //Natural: END-ALL
                pdaFcpaext.getExt_Pymnt_Check_Dte(), pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr(), pnd_Pymnt_Fields_Pnd_Pymnt_Records, pnd_Ws_Pnd_Check_To_Annt, 
                pnd_Ws_Pymnt_Gross_Amt, pnd_Ws_Pymnt_Ivc_Amt, pnd_Ws_Pnd_Accum_Ded_Table.getValue(1), pnd_Ws_Pnd_Accum_Ded_Table.getValue(2), pnd_Ws_Pnd_Accum_Ded_Table.getValue(3), 
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(4), pnd_Ws_Pnd_Accum_Ded_Table.getValue(5), pnd_Ws_Pnd_Accum_Ded_Table.getValue(6), pnd_Ws_Pnd_Accum_Ded_Table.getValue(7), 
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(8), pnd_Ws_Pnd_Accum_Ded_Table.getValue(9), pnd_Ws_Pnd_Accum_Ded_Table.getValue(10), pnd_Ws_Pnd_Accum_Ded_Table.getValue(11), 
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(12), pnd_Ws_Pnd_Accum_Ded_Table.getValue(13), pnd_Ws_Pnd_Accum_Ded_Table.getValue(14), pdaFcpaext.getExt().getValue(1), 
                pdaFcpaext.getExt().getValue(2), pdaFcpaext.getExt().getValue(3), pdaFcpaext.getExt().getValue(4), pdaFcpaext.getExt().getValue(5), pdaFcpaext.getExt().getValue(6), 
                pdaFcpaext.getExt().getValue(7), pdaFcpaext.getExt().getValue(8), pdaFcpaext.getExt().getValue(9), pdaFcpaext.getExt().getValue(10), pdaFcpaext.getExt().getValue(11), 
                pdaFcpaext.getExt().getValue(12), pdaFcpaext.getExt().getValue(13), pdaFcpaext.getExt().getValue(14), pdaFcpaext.getExt().getValue(15), 
                pdaFcpaext.getExt().getValue(16), pdaFcpaext.getExt().getValue(17), pdaFcpaext.getExt().getValue(18), pdaFcpaext.getExt().getValue(19), 
                pdaFcpaext.getExt().getValue(20), pdaFcpaext.getExt().getValue(21), pdaFcpaext.getExt().getValue(22), pdaFcpaext.getExt().getValue(23), 
                pdaFcpaext.getExt().getValue(24), pdaFcpaext.getExt().getValue(25));
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (Global.isEscape()) return;
        //*  ROXAN
        getSort().sortData(pdaFcpaext.getExt_Cntrct_Orgn_Cde(), pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), pdaFcpaext.getExt_Cntrct_Payee_Cde(), pdaFcpaext.getExt_Pymnt_Check_Dte(),  //Natural: SORT BY EXT.CNTRCT-ORGN-CDE EXT.CNTRCT-PPCN-NBR EXT.CNTRCT-PAYEE-CDE EXT.PYMNT-CHECK-DTE EXT.PYMNT-PRCSS-SEQ-NBR DESC USING #PYMNT-FIELDS.#PYMNT-RECORDS #CHECK-TO-ANNT #WS.PYMNT-GROSS-AMT #WS.PYMNT-IVC-AMT #WS.#ACCUM-DED-TABLE ( * ) EXT ( * )
            pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr(), "DESCENDING");
        boolean endOfDataSort01 = true;
        boolean firstSort01 = true;
        SORT01:
        while (condition(getSort().readSortOutData(pdaFcpaext.getExt_Cntrct_Orgn_Cde(), pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), pdaFcpaext.getExt_Cntrct_Payee_Cde(), 
            pdaFcpaext.getExt_Pymnt_Check_Dte(), pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr(), pnd_Pymnt_Fields_Pnd_Pymnt_Records, pnd_Ws_Pnd_Check_To_Annt, 
            pnd_Ws_Pymnt_Gross_Amt, pnd_Ws_Pymnt_Ivc_Amt, pnd_Ws_Pnd_Accum_Ded_Table.getValue(1), pnd_Ws_Pnd_Accum_Ded_Table.getValue(2), pnd_Ws_Pnd_Accum_Ded_Table.getValue(3), 
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(4), pnd_Ws_Pnd_Accum_Ded_Table.getValue(5), pnd_Ws_Pnd_Accum_Ded_Table.getValue(6), pnd_Ws_Pnd_Accum_Ded_Table.getValue(7), 
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(8), pnd_Ws_Pnd_Accum_Ded_Table.getValue(9), pnd_Ws_Pnd_Accum_Ded_Table.getValue(10), pnd_Ws_Pnd_Accum_Ded_Table.getValue(11), 
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(12), pnd_Ws_Pnd_Accum_Ded_Table.getValue(13), pnd_Ws_Pnd_Accum_Ded_Table.getValue(14), pdaFcpaext.getExt().getValue(1), 
            pdaFcpaext.getExt().getValue(2), pdaFcpaext.getExt().getValue(3), pdaFcpaext.getExt().getValue(4), pdaFcpaext.getExt().getValue(5), pdaFcpaext.getExt().getValue(6), 
            pdaFcpaext.getExt().getValue(7), pdaFcpaext.getExt().getValue(8), pdaFcpaext.getExt().getValue(9), pdaFcpaext.getExt().getValue(10), pdaFcpaext.getExt().getValue(11), 
            pdaFcpaext.getExt().getValue(12), pdaFcpaext.getExt().getValue(13), pdaFcpaext.getExt().getValue(14), pdaFcpaext.getExt().getValue(15), pdaFcpaext.getExt().getValue(16), 
            pdaFcpaext.getExt().getValue(17), pdaFcpaext.getExt().getValue(18), pdaFcpaext.getExt().getValue(19), pdaFcpaext.getExt().getValue(20), pdaFcpaext.getExt().getValue(21), 
            pdaFcpaext.getExt().getValue(22), pdaFcpaext.getExt().getValue(23), pdaFcpaext.getExt().getValue(24), pdaFcpaext.getExt().getValue(25))))
        {
            CheckAtStartofData523();

            if (condition(getSort().getAstCOUNTER().greater(0)))
            {
                atBreakEventSort01(false);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataSort01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            //*   MOVE BY NAME PYMNT-ADDR-INFO  TO #KEY-DETAIL           /*
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                             //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
            //* MOVE BY NAME #PYMNT-FIELDS      TO #CHECK-SORT-FIELDS            /*
            pdaFcpaext.getExt_Extr().setValuesByName(pnd_Pymnt_Fields);                                                                                                   //Natural: MOVE BY NAME #PYMNT-FIELDS TO EXTR
            pdaFcpaext.getExt_Pymnt_Record().setValue(pnd_Ws_Save_Number_Records);                                                                                        //Natural: ASSIGN EXT.PYMNT-RECORD := #WS-SAVE-NUMBER-RECORDS
            getWorkFiles().write(2, true, pdaFcpaext.getExt().getValue("*"));                                                                                             //Natural: WRITE WORK FILE 02 VARIABLE EXT ( * )
            //*   NONE  IGNORE
            //* END-DECIDE
        }                                                                                                                                                                 //Natural: END-SORT
        if (condition(getSort().getAstCOUNTER().greater(0)))
        {
            atBreakEventSort01(endOfDataSort01);
        }
        endSort();
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-CHECK-FIELDS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: NEWPAGE-PROCESSING
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-PYMNT-CHECK-FIELDS
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT-AFTER-SORT
        //* *------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'New Annuitization Control Report' 120T 'REPORT: RPT1' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 54T 'Annuity Loan Control Report' 120T 'REPORT: RPT2' //
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Determine_Check_Fields() throws Exception                                                                                                            //Natural: DETERMINE-CHECK-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------
        //*  ADD  NZ-EXT.PYMNT-GROSS-AMT  TO  #WS.PYMNT-GROSS-AMT            /*
        //*  ADD  1  TO  #NZ-CHECK-FIELDS.#PYMNT-RECORDS                     /*
        pnd_Pymnt_Fields_Pnd_Pymnt_Records.nadd(1);                                                                                                                       //Natural: ADD 1 TO #PYMNT-FIELDS.#PYMNT-RECORDS
        pnd_Ws_Pymnt_Ivc_Amt.nadd(pdaFcpaext.getExt_Inv_Acct_Ivc_Amt().getValue(1,":",pdaFcpaext.getExt_C_Inv_Acct()));                                                   //Natural: ADD EXT.INV-ACCT-IVC-AMT ( 1:C-INV-ACCT ) TO #WS.PYMNT-IVC-AMT
        //*  IF INV-ACCT-DPI-AMT  (1:C-INV-ACCT) NE 0.00                     /*
        //*  OR INV-ACCT-DCI-AMT  (1:C-INV-ACCT) NE 0.00                     /*
        //*   #NZ-CHECK-FIELDS.#DPI-DCI-IND  :=  TRUE                        /*
        //*  END-IF                                                          /*
        //*  ROXAN
        rtb.reset();                                                                                                                                                      //Natural: RESET RTB IVC-FROM-RTB-ROLLOVER
        ivc_From_Rtb_Rollover.reset();
        short decideConditionsMet607 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN EXT.CNTRCT-PYMNT-TYPE-IND = 'N' AND EXT.CNTRCT-STTLMNT-TYPE-IND = 'X'
        if (condition(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind().equals("N") && pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("X")))
        {
            decideConditionsMet607++;
            rtb.setValue(true);                                                                                                                                           //Natural: ASSIGN RTB := TRUE
        }                                                                                                                                                                 //Natural: WHEN EXT.CNTRCT-PYMNT-TYPE-IND = 'I' AND EXT.CNTRCT-STTLMNT-TYPE-IND = 'R'
        else if (condition(pdaFcpaext.getExt_Cntrct_Pymnt_Type_Ind().equals("I") && pdaFcpaext.getExt_Cntrct_Sttlmnt_Type_Ind().equals("R")))
        {
            decideConditionsMet607++;
            ivc_From_Rtb_Rollover.setValue(true);                                                                                                                         //Natural: ASSIGN IVC-FROM-RTB-ROLLOVER := TRUE
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(rtb.getBoolean()))                                                                                                                                  //Natural: IF RTB
        {
            pnd_Ws_Pnd_Accum_Ded_Table.getValue(1).setValue(true);                                                                                                        //Natural: ASSIGN #WS.#ACCUM-DED-TABLE ( 1 ) := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO C-INV-ACCT
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpaext.getExt_C_Inv_Acct())); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_J.reset();                                                                                                                                         //Natural: RESET #J
            if (condition(pdaFcpaext.getExt_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                            //Natural: IF INV-ACCT-FDRL-TAX-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(2).setValue(true);                                                                                                    //Natural: ASSIGN #WS.#ACCUM-DED-TABLE ( 2 ) := TRUE
                pnd_Ws_Pnd_J.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #J
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                           //Natural: IF INV-ACCT-STATE-TAX-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(3).setValue(true);                                                                                                    //Natural: ASSIGN #WS.#ACCUM-DED-TABLE ( 3 ) := TRUE
                pnd_Ws_Pnd_J.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #J
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpaext.getExt_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I).notEquals(new DbsDecimal("0.00"))))                                           //Natural: IF INV-ACCT-LOCAL-TAX-AMT ( #I ) NE 0.00
            {
                pnd_Ws_Pnd_Accum_Ded_Table.getValue(4).setValue(true);                                                                                                    //Natural: ASSIGN #WS.#ACCUM-DED-TABLE ( 4 ) := TRUE
                pnd_Ws_Pnd_J.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #J
            }                                                                                                                                                             //Natural: END-IF
            if (condition(rtb.getBoolean() || ivc_From_Rtb_Rollover.getBoolean()))                                                                                        //Natural: IF RTB OR IVC-FROM-RTB-ROLLOVER
            {
                if (condition(pnd_Ws_Pnd_J.equals(2) || pnd_Ws_Pnd_J.equals(3)))                                                                                          //Natural: IF #J = 2 OR = 3
                {
                    pnd_Ws_Pnd_J.nsubtract(1);                                                                                                                            //Natural: SUBTRACT 1 FROM #J
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_J.reset();                                                                                                                                 //Natural: RESET #J
                }                                                                                                                                                         //Natural: END-IF
                ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines().compute(new ComputeParameters(false, ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines()), DbsField.add(3,              //Natural: ASSIGN #FUND-LINES := 3 + #J
                    pnd_Ws_Pnd_J));
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pnd_Ws_Pnd_J.equals(3)))                                                                                                                    //Natural: IF #J = 3
                {
                    ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines().setValue(5);                                                                                               //Natural: ASSIGN #FUND-LINES := 5
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines().setValue(4);                                                                                               //Natural: ASSIGN #FUND-LINES := 4
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Lines().nadd(ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines());                                                               //Natural: ADD #FUND-LINES TO #CURRENT-LINES
            if (condition(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Lines().greater(ldaFcpl802.getPnd_Fcpl802_Pnd_Nz_Lines_Per_Page().getValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Idx())))) //Natural: IF #CURRENT-LINES > #NZ-LINES-PER-PAGE ( #LINES-IDX )
            {
                ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page().nadd(1);                                                                                                     //Natural: ADD 1 TO #CURRENT-PAGE
                                                                                                                                                                          //Natural: PERFORM NEWPAGE-PROCESSING
                sub_Newpage_Processing();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Lines().setValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Fund_Lines());                                                       //Natural: ASSIGN #CURRENT-LINES := #FUND-LINES
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpaext.getExt_Funds_On_Page().getValue(pnd_Ws_Pnd_I).setValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page());                                              //Natural: ASSIGN EXT.FUNDS-ON-PAGE ( #I ) := #CURRENT-PAGE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpaext.getExt_Pymnt_Total_Pages().setValue(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page());                                                                     //Natural: ASSIGN EXT.PYMNT-TOTAL-PAGES := #CURRENT-PAGE
    }
    private void sub_Newpage_Processing() throws Exception                                                                                                                //Natural: NEWPAGE-PROCESSING
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------------
        short decideConditionsMet664 = 0;                                                                                                                                 //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #CURRENT-PAGE = 1
        if (condition(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page().equals(1)))
        {
            decideConditionsMet664++;
            ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Idx().setValue(1);                                                                                                        //Natural: ASSIGN #LINES-IDX := 1
        }                                                                                                                                                                 //Natural: WHEN #CURRENT-PAGE = 2 AND #CHECK-TO-ANNT
        else if (condition(ldaFcpl802.getPnd_Fcpl802_Pnd_Current_Page().equals(2) && pnd_Ws_Pnd_Check_To_Annt.getBoolean()))
        {
            decideConditionsMet664++;
            ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Idx().setValue(2);                                                                                                        //Natural: ASSIGN #LINES-IDX := 2
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ldaFcpl802.getPnd_Fcpl802_Pnd_Lines_Idx().setValue(3);                                                                                                        //Natural: ASSIGN #LINES-IDX := 3
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Determine_Pymnt_Check_Fields() throws Exception                                                                                                      //Natural: DETERMINE-PYMNT-CHECK-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------------------
        pnd_Pymnt_Fields_Pnd_Pymnt_Ded_Ind.reset();                                                                                                                       //Natural: RESET #PYMNT-FIELDS.#PYMNT-DED-IND #PYMNT-FIELDS.#DED-PYMNT-TABLE ( * )
        pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table.getValue("*").reset();
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 4
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(4)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pnd_Ws_Pnd_Pymnt_Ded_Table.getValue(pnd_Ws_Pnd_I).getBoolean()))                                                                                //Natural: IF #WS.#PYMNT-DED-TABLE ( #I )
            {
                short decideConditionsMet680 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #I;//Natural: VALUE 1
                if (condition((pnd_Ws_Pnd_I.equals(1))))
                {
                    decideConditionsMet680++;
                    pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("RTB");                                                                          //Natural: ASSIGN #PYMNT-FIELDS.#DED-PYMNT-TABLE ( #I ) := 'RTB'
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_Ws_Pnd_I.equals(2))))
                {
                    decideConditionsMet680++;
                    pnd_Pymnt_Fields_Pnd_Pymnt_Ded_Ind.setValue(true);                                                                                                    //Natural: ASSIGN #PYMNT-FIELDS.#PYMNT-DED-IND := TRUE
                    if (condition(pdaFcpaext.getExt_Pymnt_Payee_Tx_Elct_Trggr().equals("N")))                                                                             //Natural: IF PYMNT-PAYEE-TX-ELCT-TRGGR = 'N'
                    {
                        pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("  N");                                                                      //Natural: ASSIGN #PYMNT-FIELDS.#DED-PYMNT-TABLE ( #I ) := '  N'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("  F");                                                                      //Natural: ASSIGN #PYMNT-FIELDS.#DED-PYMNT-TABLE ( #I ) := '  F'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE 3
                else if (condition((pnd_Ws_Pnd_I.equals(3))))
                {
                    decideConditionsMet680++;
                    pnd_Pymnt_Fields_Pnd_Pymnt_Ded_Ind.setValue(true);                                                                                                    //Natural: ASSIGN #PYMNT-FIELDS.#PYMNT-DED-IND := TRUE
                    pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("  S");                                                                          //Natural: ASSIGN #PYMNT-FIELDS.#DED-PYMNT-TABLE ( #I ) := '  S'
                }                                                                                                                                                         //Natural: VALUE 4
                else if (condition((pnd_Ws_Pnd_I.equals(4))))
                {
                    decideConditionsMet680++;
                    pnd_Pymnt_Fields_Pnd_Pymnt_Ded_Ind.setValue(true);                                                                                                    //Natural: ASSIGN #PYMNT-FIELDS.#PYMNT-DED-IND := TRUE
                    pnd_Pymnt_Fields_Pnd_Ded_Pymnt_Table.getValue(pnd_Ws_Pnd_I).setValue("  L");                                                                          //Natural: ASSIGN #PYMNT-FIELDS.#DED-PYMNT-TABLE ( #I ) := '  L'
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------------
        pdaFcpaext.getExt_Pymnt_Record().reset();                                                                                                                         //Natural: RESET EXT.PYMNT-RECORD EXT.PYMNT-TOTAL-PAGES
        pdaFcpaext.getExt_Pymnt_Total_Pages().reset();
        pnd_Ws_Pnd_Accum_Ded_Table.getValue("*").resetInitial();                                                                                                          //Natural: RESET INITIAL #WS.#ACCUM-DED-TABLE ( * ) #WS.PYMNT-GROSS-AMT #WS.PYMNT-IVC-AMT #FCPL802 #PYMNT-FIELDS
        pnd_Ws_Pymnt_Gross_Amt.resetInitial();
        pnd_Ws_Pymnt_Ivc_Amt.resetInitial();
        ldaFcpl802.getPnd_Fcpl802().resetInitial();
        pnd_Pymnt_Fields.resetInitial();
        pnd_Ws_Pymnt_Check_Amt.setValue(pdaFcpaext.getExt_Pymnt_Check_Amt());                                                                                             //Natural: ASSIGN #WS.PYMNT-CHECK-AMT := EXT.PYMNT-CHECK-AMT
        pnd_Ws_Pnd_Check_To_Annt.setValue(false);                                                                                                                         //Natural: ASSIGN #CHECK-TO-ANNT := FALSE
        short decideConditionsMet710 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
        if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(1))))
        {
            decideConditionsMet710++;
            if (condition(pdaFcpaext.getExt_Pymnt_Check_Amt().notEquals(new DbsDecimal("0.00")) && pdaFcpaext.getExt_Pymnt_Eft_Acct_Nbr().equals(" ")                     //Natural: IF EXT.PYMNT-CHECK-AMT NE 0.00 AND EXT.PYMNT-EFT-ACCT-NBR = ' ' AND EXT.PYMNT-ALT-ADDR-IND = FALSE
                && pdaFcpaext.getExt_Pymnt_Alt_Addr_Ind().equals(false)))
            {
                pnd_Ws_Pnd_Check_To_Annt.setValue(true);                                                                                                                  //Natural: ASSIGN #CHECK-TO-ANNT := TRUE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 2, 3, 8,6
        else if (condition((pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(2) || pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(8) 
            || pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().equals(6))))
        {
            decideConditionsMet710++;
            ignore();
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"UNKNOWN PAYMENT METHOD:",pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind(),new TabSetting(77),"***",                 //Natural: WRITE '***' 25T 'UNKNOWN PAYMENT METHOD:' PYMNT-PAY-TYPE-REQ-IND 77T '***' /
                NEWLINE);
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM NEWPAGE-PROCESSING
        sub_Newpage_Processing();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Init_New_Pymnt_After_Sort() throws Exception                                                                                                         //Natural: INIT-NEW-PYMNT-AFTER-SORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------------
        pnd_Pymnt_Fields.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                                       //Natural: MOVE BY NAME EXTR TO #PYMNT-FIELDS
        //*  MOVE BY NAME #NZ-CHECK-FIELDS   TO #PYMNT-FIELDS        /*
        pnd_Ws_Pnd_Pymnt_Ded_Table.getValue("*").setValue(pnd_Ws_Pnd_Accum_Ded_Table.getValue("*").getBoolean());                                                         //Natural: MOVE #WS.#ACCUM-DED-TABLE ( * ) TO #WS.#PYMNT-DED-TABLE ( * )
        pnd_Pymnt_Fields_Pymnt_Gross_Amt.setValue(pnd_Ws_Pymnt_Gross_Amt);                                                                                                //Natural: ASSIGN #PYMNT-FIELDS.PYMNT-GROSS-AMT := #WS.PYMNT-GROSS-AMT
        pnd_Pymnt_Fields_Pymnt_Ivc_Amt.setValue(pnd_Ws_Pymnt_Ivc_Amt);                                                                                                    //Natural: ASSIGN #PYMNT-FIELDS.PYMNT-IVC-AMT := #WS.PYMNT-IVC-AMT
                                                                                                                                                                          //Natural: PERFORM DETERMINE-PYMNT-CHECK-FIELDS
        sub_Determine_Pymnt_Check_Fields();
        if (condition(Global.isEscape())) {return;}
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        if (condition(pnd_KeyIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
            sub_Init_New_Pymnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-BREAK
    }

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        if (condition(pnd_KeyIsBreak))
        {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT-AFTER-SORT
            sub_Init_New_Pymnt_After_Sort();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Save_Number_Records.setValue(pnd_Pymnt_Fields_Pnd_Pymnt_Records);                                                                                      //Natural: ASSIGN #WS-SAVE-NUMBER-RECORDS := #PYMNT-FIELDS.#PYMNT-RECORDS
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(51),"New Annuitization Control Report",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(54),"Annuity Loan Control Report",new TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData491() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*    MOVE BY NAME NZ-PYMNT-ADDR   TO #KEY-DETAIL
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                             //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
            sub_Init_New_Pymnt();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
    private void CheckAtStartofData523() throws Exception
    {
        if (condition(getSort().getAtStartOfData()))
        {
            //*   MOVE BY NAME PYMNT-ADDR-INFO  TO #KEY-DETAIL           /*
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                             //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT-AFTER-SORT
            sub_Init_New_Pymnt_After_Sort();
            if (condition(Global.isEscape())) {return;}
            pnd_Ws_Save_Number_Records.setValue(pnd_Pymnt_Fields_Pnd_Pymnt_Records);                                                                                      //Natural: ASSIGN #WS-SAVE-NUMBER-RECORDS := #PYMNT-FIELDS.#PYMNT-RECORDS
        }                                                                                                                                                                 //Natural: END-START
    }
}
