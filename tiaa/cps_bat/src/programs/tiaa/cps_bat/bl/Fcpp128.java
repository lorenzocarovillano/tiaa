/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:12 PM
**        * FROM NATURAL PROGRAM : Fcpp128
************************************************************
**        * FILE NAME            : Fcpp128.java
**        * CLASS NAME           : Fcpp128
**        * INSTANCE NAME        : Fcpp128
************************************************************
************************************************************************
* PROGRAM  : FCPP128
* SYSTEM   : CPS
* TITLE    : M.I.T. UPDATE.
* GENERATED: 12/07/94
* FUNCTION : THIS PROGRAM WILL READ RACASH CHECK FILE CREATED BY CPS
*          : AND UPDATE MIT.
*          :
* HISTORY  :
* 06/18/97 : RITA SALGADO
*          : ADD LOGIC TO UPDATE MIT WITH NZ CONTRACTS
* 10/09/97 : RITA SALGADO
*          : MAKE STATUS CODE FOR RTB SAME AS PP& RTB
* 05/10/99 : ROXAN CARREON
*          : INITIALIZE VALUE OF TABLE ELEMENT "#stat 3,6" TO  7533
* 05/25/99 : LEON  GURTOVNIK
*          : ADD ENTRY  5 TO TABLE ELEMENT AND INITIALIZE "#stat 5,1"
*          : TO '8200' TO BE USED FOR USPS OR OV00 DELEVERY METOD.
* 01/13/00 : ROXAN CARREON
*          : INCLUDE EW
* 4/2017   : JJG  PIN EXPANSION
* 01/15/2020  : CTS CPS SUNSET (CHG694525) TAG: CTS-0115
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp128 extends BLNatBase
{
    // Data Areas
    private PdaFcpa124 pdaFcpa124;
    private PdaFcpa127 pdaFcpa127;
    private LdaFcpl128 ldaFcpl128;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Restart_Fields;
    private DbsField pnd_Ws_Pnd_Restart_Text;
    private DbsField pnd_Ws_Pnd_Restart_Timestmp;
    private DbsField pnd_Ws_Pnd_Restart_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Pnd_Restart_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Pnd_Restart_Pymnt_Prcss_Seq_Nbr;
    private DbsField pnd_Ws_Pnd_Restart_Date_Time;
    private DbsField pnd_Ws_Pnd_Restart_Rec_Cnt;

    private DbsGroup pnd_Ws_Pnd_Status_Table;
    private DbsField pnd_Ws_Pnd_Status_Code;
    private DbsField pnd_Ws_Pnd_Records_Read;
    private DbsField pnd_Ws_Pnd_Records_Skipped;
    private DbsField pnd_Ws_Pnd_Mit_Rej_Read_Cnt;
    private DbsField pnd_Ws_Pnd_Mit_Rej_Update_Cnt;
    private DbsField pnd_Ws_Pnd_Mit_War_Update_Cnt;
    private DbsField pnd_Ws_Pnd_Invalid_Data_Cnt;
    private DbsField pnd_Ws_Pnd_Mit_Updated_Cnt;
    private DbsField pnd_Ws_Pnd_Et_Cnt;
    private DbsField pnd_Ws_Pnd_Idx1;
    private DbsField pnd_Ws_Pnd_Idx2;
    private DbsField pnd_Ws_Pnd_Disp_Msg;
    private DbsField pnd_Ws_Pnd_Reset;
    private DbsField pnd_Ws_Pnd_Restart_Ind;
    private DbsField pnd_Ws_Pnd_Rollover;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa124 = new PdaFcpa124(localVariables);
        pdaFcpa127 = new PdaFcpa127(localVariables);
        ldaFcpl128 = new LdaFcpl128();
        registerRecord(ldaFcpl128);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Restart_Fields = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Restart_Fields", "#RESTART-FIELDS");
        pnd_Ws_Pnd_Restart_Text = pnd_Ws_Pnd_Restart_Fields.newFieldInGroup("pnd_Ws_Pnd_Restart_Text", "#RESTART-TEXT", FieldType.STRING, 1);
        pnd_Ws_Pnd_Restart_Timestmp = pnd_Ws_Pnd_Restart_Fields.newFieldInGroup("pnd_Ws_Pnd_Restart_Timestmp", "#RESTART-TIMESTMP", FieldType.STRING, 
            16);
        pnd_Ws_Pnd_Restart_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Pnd_Restart_Fields.newFieldInGroup("pnd_Ws_Pnd_Restart_Pymnt_Reqst_Log_Dte_Time", "#RESTART-PYMNT-REQST-LOG-DTE-TIME", 
            FieldType.STRING, 15);
        pnd_Ws_Pnd_Restart_Cntrct_Unq_Id_Nbr = pnd_Ws_Pnd_Restart_Fields.newFieldInGroup("pnd_Ws_Pnd_Restart_Cntrct_Unq_Id_Nbr", "#RESTART-CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Ws_Pnd_Restart_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Pnd_Restart_Fields.newFieldInGroup("pnd_Ws_Pnd_Restart_Pymnt_Prcss_Seq_Nbr", "#RESTART-PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Pnd_Restart_Date_Time = pnd_Ws_Pnd_Restart_Fields.newFieldInGroup("pnd_Ws_Pnd_Restart_Date_Time", "#RESTART-DATE-TIME", FieldType.STRING, 
            21);
        pnd_Ws_Pnd_Restart_Rec_Cnt = pnd_Ws_Pnd_Restart_Fields.newFieldInGroup("pnd_Ws_Pnd_Restart_Rec_Cnt", "#RESTART-REC-CNT", FieldType.PACKED_DECIMAL, 
            7);

        pnd_Ws_Pnd_Status_Table = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Status_Table", "#STATUS-TABLE", new DbsArrayController(1, 5));
        pnd_Ws_Pnd_Status_Code = pnd_Ws_Pnd_Status_Table.newFieldArrayInGroup("pnd_Ws_Pnd_Status_Code", "#STATUS-CODE", FieldType.STRING, 4, new DbsArrayController(1, 
            6));
        pnd_Ws_Pnd_Records_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Records_Read", "#RECORDS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Records_Skipped = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Records_Skipped", "#RECORDS-SKIPPED", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Mit_Rej_Read_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mit_Rej_Read_Cnt", "#MIT-REJ-READ-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Mit_Rej_Update_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mit_Rej_Update_Cnt", "#MIT-REJ-UPDATE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Mit_War_Update_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mit_War_Update_Cnt", "#MIT-WAR-UPDATE-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Invalid_Data_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Invalid_Data_Cnt", "#INVALID-DATA-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Mit_Updated_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Mit_Updated_Cnt", "#MIT-UPDATED-CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Et_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Et_Cnt", "#ET-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Idx1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx1", "#IDX1", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Idx2 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx2", "#IDX2", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Disp_Msg = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Msg", "#DISP-MSG", FieldType.STRING, 22);
        pnd_Ws_Pnd_Reset = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Reset", "#RESET", FieldType.STRING, 5);
        pnd_Ws_Pnd_Restart_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Restart_Ind", "#RESTART-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Rollover = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rollover", "#ROLLOVER", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl128.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Status_Code.getValue(1,1).setInitialValue("8000");
        pnd_Ws_Pnd_Status_Code.getValue(1,2).setInitialValue("8004");
        pnd_Ws_Pnd_Status_Code.getValue(1,3).setInitialValue("8102");
        pnd_Ws_Pnd_Status_Code.getValue(1,4).setInitialValue("8106");
        pnd_Ws_Pnd_Status_Code.getValue(1,5).setInitialValue("8106");
        pnd_Ws_Pnd_Status_Code.getValue(1,6).setInitialValue("8020");
        pnd_Ws_Pnd_Status_Code.getValue(2,1).setInitialValue("8101");
        pnd_Ws_Pnd_Status_Code.getValue(2,2).setInitialValue("8101");
        pnd_Ws_Pnd_Status_Code.getValue(2,3).setInitialValue("8103");
        pnd_Ws_Pnd_Status_Code.getValue(2,4).setInitialValue("8107");
        pnd_Ws_Pnd_Status_Code.getValue(2,5).setInitialValue("8107");
        pnd_Ws_Pnd_Status_Code.getValue(2,6).setInitialValue("8019");
        pnd_Ws_Pnd_Status_Code.getValue(3,1).setInitialValue("7533");
        pnd_Ws_Pnd_Status_Code.getValue(3,2).setInitialValue("7536");
        pnd_Ws_Pnd_Status_Code.getValue(3,3).setInitialValue("7533");
        pnd_Ws_Pnd_Status_Code.getValue(3,4).setInitialValue("7533");
        pnd_Ws_Pnd_Status_Code.getValue(3,5).setInitialValue("7533");
        pnd_Ws_Pnd_Status_Code.getValue(3,6).setInitialValue("7533");
        pnd_Ws_Pnd_Status_Code.getValue(4,1).setInitialValue("8111");
        pnd_Ws_Pnd_Status_Code.getValue(4,2).setInitialValue("0000");
        pnd_Ws_Pnd_Status_Code.getValue(4,3).setInitialValue("0000");
        pnd_Ws_Pnd_Status_Code.getValue(5,1).setInitialValue("8200");
        pnd_Ws_Pnd_Status_Code.getValue(5,2).setInitialValue("0000");
        pnd_Ws_Pnd_Status_Code.getValue(5,3).setInitialValue("0000");
        pnd_Ws_Pnd_Restart_Ind.setInitialValue(false);
        pnd_Ws_Pnd_Rollover.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp128() throws Exception
    {
        super("Fcpp128");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp128|Main");
        setupReports();
        while(true)
        {
            try
            {
                //* *--------
                //*                                                                                                                                                       //Natural: FORMAT ( 00 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 58 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 58 LS = 133 ZP = ON
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA := 'INFP9000'
                //* ***********************
                //*   MAIN PROGRAM LOGIC  *
                //* ***********************
                if (condition(Global.getSTACK().getDatacount().equals(1), INPUT_1))                                                                                       //Natural: IF *DATA = 1
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Reset);                                                                                 //Natural: INPUT #RESET
                    //*  RESET RESTART
                    if (condition(pnd_Ws_Pnd_Reset.equals("RESET")))                                                                                                      //Natural: IF #RESET = 'RESET'
                    {
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION ' '
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM CHECK-HEADER
                sub_Check_Header();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(1, new TabSetting(7),"INPUT FILE:",NEWLINE,new TabSetting(22),"RUN AS OF..........:",ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Date_Time(),NEWLINE,new  //Natural: WRITE ( 01 ) 7T 'INPUT FILE:' / 22T 'RUN AS OF..........:' #MIT-HEADER-RECORD.#DATE-TIME / 22T 'RECORDS IN THE FILE:' #MIT-HEADER-RECORD.#REC-CNT
                    TabSetting(22),"RECORDS IN THE FILE:",ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Rec_Cnt(), new ReportEditMask ("-Z,ZZZ,ZZ9"));
                if (Global.isEscape()) return;
                //*   RESTART
                //*                                                                                                                                                       //Natural: GET TRANSACTION DATA #RESTART-TEXT #RESTART-TIMESTMP #RESTART-PYMNT-REQST-LOG-DTE-TIME #RESTART-CNTRCT-UNQ-ID-NBR #RESTART-PYMNT-PRCSS-SEQ-NBR #RESTART-DATE-TIME #RESTART-REC-CNT
                if (condition(pnd_Ws_Pnd_Restart_Text.notEquals(" ")))                                                                                                    //Natural: IF #RESTART-TEXT NE ' '
                {
                    pnd_Ws_Pnd_Restart_Ind.setValue(true);                                                                                                                //Natural: ASSIGN #RESTART-IND := TRUE
                    getReports().write(1, new TabSetting(7),"RESTART DATA:",NEWLINE,new TabSetting(22),"RUN AS OF..........:",pnd_Ws_Pnd_Restart_Date_Time,NEWLINE,new    //Natural: WRITE ( 01 ) 7T 'RESTART DATA:' / 22T 'RUN AS OF..........:' #RESTART-DATE-TIME / 22T 'RECORDS PROCESSED..:' #RESTART-REC-CNT / 22T 'LOG DATE TIME......:' #RESTART-PYMNT-REQST-LOG-DTE-TIME / 22T 'PIN................:' #RESTART-CNTRCT-UNQ-ID-NBR
                        TabSetting(22),"RECORDS PROCESSED..:",pnd_Ws_Pnd_Restart_Rec_Cnt, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(22),"LOG DATE TIME......:",pnd_Ws_Pnd_Restart_Pymnt_Reqst_Log_Dte_Time,NEWLINE,new 
                        TabSetting(22),"PIN................:",pnd_Ws_Pnd_Restart_Cntrct_Unq_Id_Nbr);
                    if (Global.isEscape()) return;
                    if (condition(ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Timestmp().notEquals(pnd_Ws_Pnd_Restart_Timestmp)))                                             //Natural: IF #MIT-HEADER-RECORD.#TIMESTMP NE #RESTART-TIMESTMP
                    {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                        sub_Error_Display_Start();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        getReports().write(1, "***",new TabSetting(25),"INPUT FILE AND RESTART DATES DO NOT AGREE",new TabSetting(77),"***",NEWLINE);                     //Natural: WRITE ( 01 ) '***' 25T 'INPUT FILE AND RESTART DATES DO NOT AGREE' 77T '***' /
                        if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                        sub_Error_Display_End();
                        if (condition(Global.isEscape())) {return;}
                        if (condition(Map.getDoInput())) {return;}
                        DbsUtil.terminate(53);  if (true) return;                                                                                                         //Natural: TERMINATE 53
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                getReports().skip(1, 4);                                                                                                                                  //Natural: SKIP ( 01 ) 4 LINES
                //*    DETAIL   - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 01 RECORD #MIT-UPDATE-RECORD
                while (condition(getWorkFiles().read(1, ldaFcpl128.getPnd_Mit_Update_Record())))
                {
                    pnd_Ws_Pnd_Records_Read.nadd(1);                                                                                                                      //Natural: ADD 1 TO #RECORDS-READ
                    if (condition(pnd_Ws_Pnd_Restart_Ind.getBoolean()))                                                                                                   //Natural: IF #RESTART-IND
                    {
                        pnd_Ws_Pnd_Records_Skipped.nadd(1);                                                                                                               //Natural: ADD 1 TO #RECORDS-SKIPPED
                        if (condition(pnd_Ws_Pnd_Records_Read.less(pnd_Ws_Pnd_Restart_Rec_Cnt)))                                                                          //Natural: IF #RECORDS-READ < #RESTART-REC-CNT
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: END-IF
                        pnd_Ws_Pnd_Restart_Ind.setValue(false);                                                                                                           //Natural: ASSIGN #RESTART-IND := FALSE
                        if (condition(pnd_Ws_Pnd_Restart_Pymnt_Reqst_Log_Dte_Time.equals(ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time())                  //Natural: IF #RESTART-PYMNT-REQST-LOG-DTE-TIME = #MIT-UPDATE-RECORD.PYMNT-REQST-LOG-DTE-TIME AND #RESTART-CNTRCT-UNQ-ID-NBR = #MIT-UPDATE-RECORD.CNTRCT-UNQ-ID-NBR AND #RESTART-PYMNT-PRCSS-SEQ-NBR = #MIT-UPDATE-RECORD.PYMNT-PRCSS-SEQ-NBR
                            && pnd_Ws_Pnd_Restart_Cntrct_Unq_Id_Nbr.equals(ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr()) && pnd_Ws_Pnd_Restart_Pymnt_Prcss_Seq_Nbr.equals(ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr())))
                        {
                            if (condition(true)) continue;                                                                                                                //Natural: ESCAPE TOP
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                            sub_Error_Display_Start();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            getReports().write(1, "***",new TabSetting(25),"RESTART POINT IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"INPUT FILE IS PROBABLY DIFFERENT",new  //Natural: WRITE ( 01 ) '***' 25T 'RESTART POINT IS NOT FOUND' 77T '***' / '***' 25T 'INPUT FILE IS PROBABLY DIFFERENT' 77T '***' /
                                TabSetting(77),"***",NEWLINE);
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                            sub_Error_Display_End();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            DbsUtil.terminate(54);  if (true) return;                                                                                                     //Natural: TERMINATE 54
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pdaFcpa124.getPnd_Mit_Read_Pda_Pin_Nbr().setValue(ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr());                                           //Natural: ASSIGN #MIT-READ-PDA.PIN-NBR := #MIT-UPDATE-RECORD.CNTRCT-UNQ-ID-NBR
                    pdaFcpa124.getPnd_Mit_Read_Pda_Rqst_Log_Dte_Tme().setValue(ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time());                           //Natural: ASSIGN #MIT-READ-PDA.RQST-LOG-DTE-TME := #MIT-UPDATE-RECORD.PYMNT-REQST-LOG-DTE-TIME
                    //*  READ MIT
                    DbsUtil.callnat(Fcpn127.class , getCurrentProcessState(), pdaFcpa124.getPnd_Mit_Read_Pda(), pdaFcpa127.getPnd_Mit_Update_Pda());                      //Natural: CALLNAT 'FCPN127' #MIT-READ-PDA #MIT-UPDATE-PDA
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Ok_Return_Code().getBoolean()))                                                                    //Natural: IF #OK-RETURN-CODE
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_Disp_Msg.setValue("REJECTED ON MIT READ");                                                                                             //Natural: ASSIGN #DISP-MSG := 'REJECTED ON MIT READ'
                        pnd_Ws_Pnd_Mit_Rej_Read_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #MIT-REJ-READ-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJ-REPORT
                        sub_Write_Rej_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Rollover.setValue(false);                                                                                                                  //Natural: ASSIGN #ROLLOVER := FALSE
                    pnd_Ws_Pnd_Idx1.setValue(0);                                                                                                                          //Natural: ASSIGN #IDX1 := #IDX2 := 0
                    pnd_Ws_Pnd_Idx2.setValue(0);
                    if (condition(ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Hold_Cde().notEquals(" ")))                                                                  //Natural: IF #MIT-UPDATE-RECORD.CNTRCT-HOLD-CDE NE ' '
                    {
                        pnd_Ws_Pnd_Idx1.setValue(3);                                                                                                                      //Natural: ASSIGN #IDX1 := 3
                        pnd_Ws_Pnd_Idx2.setValue(1);                                                                                                                      //Natural: ASSIGN #IDX2 := 1
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*   ROLLOVERS
                        //*       "
                        //*       "
                        if (condition(ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind().equals(8)))                                                            //Natural: IF #MIT-UPDATE-RECORD.PYMNT-PAY-TYPE-REQ-IND = 8
                        {
                            pnd_Ws_Pnd_Rollover.setValue(true);                                                                                                           //Natural: ASSIGN #ROLLOVER := TRUE
                            pnd_Ws_Pnd_Idx1.setValue(4);                                                                                                                  //Natural: ASSIGN #IDX1 := 4
                            pnd_Ws_Pnd_Idx2.setValue(1);                                                                                                                  //Natural: ASSIGN #IDX2 := 1
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pnd_Ws_Pnd_Idx1.setValue(ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind());                                                       //Natural: ASSIGN #IDX1 := #MIT-UPDATE-RECORD.PYMNT-PAY-TYPE-REQ-IND
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Pnd_Idx1.greater(4)))                                                                                                            //Natural: IF #IDX1 > 4
                    {
                        pnd_Ws_Pnd_Invalid_Data_Cnt.nadd(1);                                                                                                              //Natural: ADD 1 TO #INVALID-DATA-CNT
                        pnd_Ws_Pnd_Disp_Msg.setValue("INVALID INPUT DATA");                                                                                               //Natural: ASSIGN #DISP-MSG := 'INVALID INPUT DATA'
                        pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Return_Message().setValue(DbsUtil.compress("INVALID PYMNT-PAY-TYPE-REQ-IND ON THE INPUT FILE:",              //Natural: COMPRESS 'INVALID PYMNT-PAY-TYPE-REQ-IND ON THE INPUT FILE:' PYMNT-PAY-TYPE-REQ-IND INTO #RETURN-MESSAGE
                            ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Pay_Type_Req_Ind()));
                                                                                                                                                                          //Natural: PERFORM WRITE-REJ-REPORT
                        sub_Write_Rej_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Pnd_Rollover.equals(true)))                                                                                                      //Natural: IF #ROLLOVER = TRUE
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*                                                                                                                                               //Natural: DECIDE ON FIRST VALUE OF #MIT-UPDATE-RECORD.CNTRCT-ORGN-CDE
                        short decideConditionsMet237 = 0;                                                                                                                 //Natural: VALUE 'AL', 'EW'
                        if (condition((ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Orgn_Cde().equals("AL") || ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Orgn_Cde().equals("EW"))))
                        {
                            decideConditionsMet237++;
                            pnd_Ws_Pnd_Idx2.setValue(6);                                                                                                                  //Natural: ASSIGN #IDX2 := 6
                        }                                                                                                                                                 //Natural: VALUE 'NZ'
                        else if (condition((ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Orgn_Cde().equals("NZ"))))
                        {
                            decideConditionsMet237++;
                            //*                                           /* CTS-0115 >>
                            //*         IF #PERIODIC-PAY
                            //*        AND #RTB
                            //*            #IDX2  :=  5
                            //*         ELSE
                            //*            IF #RTB
                            //*  CTS-0115 <<
                            if (condition(ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Ind().equals("R")))                                                                   //Natural: IF PYMNT-IND = 'R'
                            {
                                pnd_Ws_Pnd_Idx2.setValue(4);                                                                                                              //Natural: ASSIGN #IDX2 := 4
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pnd_Ws_Pnd_Idx2.setValue(3);                                                                                                              //Natural: ASSIGN #IDX2 := 3
                                //*  CTS-0115 >>
                            }                                                                                                                                             //Natural: END-IF
                            //*         END-IF
                        }                                                                                                                                                 //Natural: NONE
                        else if (condition())
                        {
                            ignore();
                            //*         IF #ACFS AND #GOOD-LETTER
                            //*            #IDX2  :=  2
                            //*         ELSE
                            //*            #IDX2  :=  1
                            //*         END-IF                           /* CTS-0115 <<
                        }                                                                                                                                                 //Natural: END-DECIDE
                    }                                                                                                                                                     //Natural: END-IF
                    pdaFcpa127.getPnd_Mit_Update_Pda_Status_Cde().setValue(pnd_Ws_Pnd_Status_Code.getValue(pnd_Ws_Pnd_Idx1,pnd_Ws_Pnd_Idx2));                             //Natural: ASSIGN #MIT-UPDATE-PDA.STATUS-CDE := #STATUS-CODE ( #IDX1,#IDX2 )
                    //* *---*  05/25/99 LEON
                    if (condition(ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Hold_Cde().equals("OV00") || ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Hold_Cde().equals("USPS"))) //Natural: IF #MIT-UPDATE-RECORD.CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
                    {
                        pdaFcpa127.getPnd_Mit_Update_Pda_Status_Cde().setValue(pnd_Ws_Pnd_Status_Code.getValue(5,1));                                                     //Natural: ASSIGN #MIT-UPDATE-PDA.STATUS-CDE := #STATUS-CODE ( 5,1 )
                    }                                                                                                                                                     //Natural: END-IF
                    //* *---*
                    //*  CTS-0115 >>
                    //*  #MIT-UPDATE-PDA.TRANS-DTE   :=  #MIT-UPDATE-RECORD.PYMNT-CHECK-DTE
                    pdaFcpa127.getPnd_Mit_Update_Pda_Trans_Dte().setValueEdited(new ReportEditMask("YYYYMMDD"),ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Check_Dte());    //Natural: MOVE EDITED #MIT-UPDATE-RECORD.PYMNT-CHECK-DTE TO #MIT-UPDATE-PDA.TRANS-DTE ( EM = YYYYMMDD )
                    //*  CTS-0115 <<
                    //*  FINAL STATUS
                    if (condition(pdaFcpa127.getPnd_Mit_Update_Pda_Status_Cde().getSubstring(1,1).equals("8")))                                                           //Natural: IF SUBSTR ( #MIT-UPDATE-PDA.STATUS-CDE,1,1 ) = '8'
                    {
                        pdaFcpa127.getPnd_Mit_Update_Pda_Unit_Cde().setValue("RSSMP");                                                                                    //Natural: ASSIGN #MIT-UPDATE-PDA.UNIT-CDE := 'RSSMP'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaFcpa127.getPnd_Mit_Update_Pda_Unit_Cde().reset();                                                                                              //Natural: RESET #MIT-UPDATE-PDA.UNIT-CDE
                    }                                                                                                                                                     //Natural: END-IF
                    //*  UPDATE-MIT
                    DbsUtil.callnat(Fcpn124.class , getCurrentProcessState(), pdaFcpa124.getPnd_Mit_Read_Pda(), pdaFcpa127.getPnd_Mit_Update_Pda());                      //Natural: CALLNAT 'FCPN124' #MIT-READ-PDA #MIT-UPDATE-PDA
                    if (condition(Global.isEscape())) return;
                    if (condition(pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Ok_Return_Code().getBoolean()))                                                                    //Natural: IF #OK-RETURN-CODE
                    {
                        if (condition(pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Return_Message().notEquals(" ")))                                                              //Natural: IF #RETURN-MESSAGE NE ' '
                        {
                            pnd_Ws_Pnd_Disp_Msg.setValue("WARNING ON MIT UPDATE");                                                                                        //Natural: ASSIGN #DISP-MSG := 'WARNING ON MIT UPDATE'
                            pnd_Ws_Pnd_Mit_War_Update_Cnt.nadd(1);                                                                                                        //Natural: ADD 1 TO #MIT-WAR-UPDATE-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJ-REPORT
                            sub_Write_Rej_Report();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pnd_Ws_Pnd_Disp_Msg.setValue("REJECTED ON MIT UPDATE");                                                                                           //Natural: ASSIGN #DISP-MSG := 'REJECTED ON MIT UPDATE'
                        pnd_Ws_Pnd_Mit_Rej_Update_Cnt.nadd(1);                                                                                                            //Natural: ADD 1 TO #MIT-REJ-UPDATE-CNT
                                                                                                                                                                          //Natural: PERFORM WRITE-REJ-REPORT
                        sub_Write_Rej_Report();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Ws_Pnd_Et_Cnt.nadd(1);                                                                                                                            //Natural: ADD 1 TO #ET-CNT
                    if (condition(pnd_Ws_Pnd_Et_Cnt.equals(25)))                                                                                                          //Natural: IF #ET-CNT = 25
                    {
                        pnd_Ws_Pnd_Mit_Updated_Cnt.nadd(pnd_Ws_Pnd_Et_Cnt);                                                                                               //Natural: ADD #ET-CNT TO #MIT-UPDATED-CNT
                        pnd_Ws_Pnd_Et_Cnt.setValue(0);                                                                                                                    //Natural: ASSIGN #ET-CNT := 0
                        getCurrentProcessState().getDbConv().dbCommit();                                                                                                  //Natural: END TRANSACTION 'X' #MIT-HEADER-RECORD.#TIMESTMP #MIT-UPDATE-RECORD.PYMNT-REQST-LOG-DTE-TIME #MIT-UPDATE-RECORD.CNTRCT-UNQ-ID-NBR #MIT-UPDATE-RECORD.PYMNT-PRCSS-SEQ-NBR #MIT-HEADER-RECORD.#DATE-TIME #RECORDS-READ
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                pnd_Ws_Pnd_Mit_Updated_Cnt.nadd(pnd_Ws_Pnd_Et_Cnt);                                                                                                       //Natural: ADD #ET-CNT TO #MIT-UPDATED-CNT
                getCurrentProcessState().getDbConv().dbCommit();                                                                                                          //Natural: END TRANSACTION ' '
                getReports().write(1, new TabSetting(17),"RECORDS READ............:",pnd_Ws_Pnd_Records_Read, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new               //Natural: WRITE ( 01 ) 17T 'RECORDS READ............:' #RECORDS-READ / 17T '  SKIPPED(RESTART)......:' #RECORDS-SKIPPED / 17T '  REJECTED ON MIT READ..:' #MIT-REJ-READ-CNT '(SEE REPORT)' / 17T '  REJECTED ON MIT UPDATE:' #MIT-REJ-UPDATE-CNT '(SEE REPORT)' / 17T '  WARNING  ON MIT UPDATE:' #MIT-WAR-UPDATE-CNT '(SEE REPORT)' / 17T '  INVALID INPUT DATA....:' #INVALID-DATA-CNT '(SEE REPORT)' / 17T '  UPDATED  ON MIT.......:' #MIT-UPDATED-CNT //// 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************'
                    TabSetting(17),"  SKIPPED(RESTART)......:",pnd_Ws_Pnd_Records_Skipped, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"  REJECTED ON MIT READ..:",pnd_Ws_Pnd_Mit_Rej_Read_Cnt, 
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),"(SEE REPORT)",NEWLINE,new TabSetting(17),"  REJECTED ON MIT UPDATE:",pnd_Ws_Pnd_Mit_Rej_Update_Cnt, 
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),"(SEE REPORT)",NEWLINE,new TabSetting(17),"  WARNING  ON MIT UPDATE:",pnd_Ws_Pnd_Mit_War_Update_Cnt, 
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),"(SEE REPORT)",NEWLINE,new TabSetting(17),"  INVALID INPUT DATA....:",pnd_Ws_Pnd_Invalid_Data_Cnt, 
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),"(SEE REPORT)",NEWLINE,new TabSetting(17),"  UPDATED  ON MIT.......:",pnd_Ws_Pnd_Mit_Updated_Cnt, 
                    new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new 
                    TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************");
                if (Global.isEscape()) return;
                getReports().write(2, new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new  //Natural: WRITE ( 2 ) 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************'
                    TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************");
                if (Global.isEscape()) return;
                if (condition(ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Rec_Cnt().notEquals(pnd_Ws_Pnd_Records_Read)))                                                      //Natural: IF #MIT-HEADER-RECORD.#REC-CNT NE #RECORDS-READ
                {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    getReports().write(1, "***",new TabSetting(25),"HEADER AND ACTUAL RECORD COUNTS ARE NOT =",new TabSetting(77),"***",NEWLINE,"***",new                 //Natural: WRITE ( 1 ) '***' 25T 'HEADER AND ACTUAL RECORD COUNTS ARE NOT =' 77T '***' / '***' 25T 'HEADER RECORD COUNT:' #MIT-HEADER-RECORD.#REC-CNT 77T '***' / '***' 25T 'ACTUAL RECORD COUNT:' #RECORDS-READ 77T '***' /
                        TabSetting(25),"HEADER RECORD COUNT:",ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Rec_Cnt(), new ReportEditMask ("-Z,ZZZ,ZZ9"),new TabSetting(77),"***",NEWLINE,"***",new 
                        TabSetting(25),"ACTUAL RECORD COUNT:",pnd_Ws_Pnd_Records_Read, new ReportEditMask ("-Z,ZZZ,ZZ9"),new TabSetting(77),"***",NEWLINE);
                    if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    DbsUtil.terminate(52);  if (true) return;                                                                                                             //Natural: TERMINATE 52
                }                                                                                                                                                         //Natural: END-IF
                //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REJ-REPORT
                //*  '=' UNIT-CDE
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-HEADER
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* *------------
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* *------------
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 01 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'SSSS/ADAM BATCH MIT UPDATE CONTROL REPORT' 120T 'REPORT: RPT1' //
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( 02 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 02 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'SSSS/ADAM BATCH MIT UPDATE CONTROL REPORT' 120T 'REPORT: RPT2' //
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Write_Rej_Report() throws Exception                                                                                                                  //Natural: WRITE-REJ-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *---------------------------------
        //*  #RETURN-MESSAGE (AL=62 HC=L)
        getReports().display(2, "RECORD/NUMBER",                                                                                                                          //Natural: DISPLAY ( 02 ) 'RECORD/NUMBER' #RECORDS-READ '/PIN' #MIT-UPDATE-RECORD.CNTRCT-UNQ-ID-NBR '/LOG DATE-TIME' #MIT-UPDATE-RECORD.PYMNT-REQST-LOG-DTE-TIME 'PROCESS/SEQ#' #MIT-UPDATE-RECORD.PYMNT-PRCSS-SEQ-NBR '/ACTION' #DISP-MSG '/MESSAGE'
        		pnd_Ws_Pnd_Records_Read,"/PIN",
        		ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr(),"/LOG DATE-TIME",
        		ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time(),"PROCESS/SEQ#",
        		ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr(),"/ACTION",
        		pnd_Ws_Pnd_Disp_Msg,"/MESSAGE");
        if (Global.isEscape()) return;
        getReports().display(2, pdaFcpa127.getPnd_Mit_Update_Pda_Pnd_Return_Message(), new AlphanumericLength (62), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left)); //Natural: DISPLAY ( 02 ) #RETURN-MESSAGE ( AL = 62 HC = L )
        if (Global.isEscape()) return;
        getReports().write(2, pnd_Ws_Pnd_Idx1,pnd_Ws_Pnd_Idx2,pdaFcpa127.getPnd_Mit_Update_Pda_Status_Cde());                                                             //Natural: WRITE ( 02 ) #IDX1 #IDX2 STATUS-CDE
        if (Global.isEscape()) return;
        //*  '=' STATUS-CDE
        //*  '=' CHECK-MLD-DTE
    }
    private void sub_Check_Header() throws Exception                                                                                                                      //Natural: CHECK-HEADER
    {
        if (BLNatReinput.isReinput()) return;

        //* *-----------------------------
        getWorkFiles().read(1, ldaFcpl128.getPnd_Mit_Header_Record());                                                                                                    //Natural: READ WORK FILE 01 ONCE RECORD #MIT-HEADER-RECORD
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(1, "***",new TabSetting(25),"INPUT FILE IS EMPTY",new TabSetting(77),"***",NEWLINE);                                                       //Natural: WRITE ( 01 ) '***' 25T 'INPUT FILE IS EMPTY' 77T '***' /
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-ENDFILE
        if (condition(ldaFcpl128.getPnd_Mit_Header_Record_Pnd_Header_Text().notEquals("HD")))                                                                             //Natural: IF #MIT-HEADER-RECORD.#HEADER-TEXT NE 'HD'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(1, "***",new TabSetting(25),"HEADER RECORD IS NOT ON THE INPUT FILE",new TabSetting(77),"***");                                            //Natural: WRITE ( 01 ) '***' 25T 'HEADER RECORD IS NOT ON THE INPUT FILE' 77T '***'
            if (Global.isEscape()) return;
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 01 ) 1
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* *------------------------------------
        getReports().newPage(new ReportSpecification(1));                                                                                                                 //Natural: NEWPAGE ( 01 )
        if (condition(Global.isEscape())){return;}
        getReports().write(1, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 01 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* *----------------------------------
        getReports().write(1, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 01 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=133 ZP=ON");
        Global.format(1, "PS=58 LS=133 ZP=ON");
        Global.format(2, "PS=58 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"SSSS/ADAM BATCH MIT UPDATE CONTROL REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(46),"SSSS/ADAM BATCH MIT UPDATE CONTROL REPORT",new TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, "RECORD/NUMBER",
        		pnd_Ws_Pnd_Records_Read,"/PIN",
        		ldaFcpl128.getPnd_Mit_Update_Record_Cntrct_Unq_Id_Nbr(),"/LOG DATE-TIME",
        		ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Reqst_Log_Dte_Time(),"PROCESS/SEQ#",
        		ldaFcpl128.getPnd_Mit_Update_Record_Pymnt_Prcss_Seq_Nbr(),"/ACTION",
        		pnd_Ws_Pnd_Disp_Msg,"/MESSAGE");
    }
}
