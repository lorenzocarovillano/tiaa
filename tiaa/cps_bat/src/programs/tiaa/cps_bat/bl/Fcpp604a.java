/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:26 PM
**        * FROM NATURAL PROGRAM : Fcpp604a
************************************************************
**        * FILE NAME            : Fcpp604a.java
**        * CLASS NAME           : Fcpp604a
**        * INSTANCE NAME        : Fcpp604a
************************************************************
** PROGRAM: FCPP604A
** CREATED:   /  /2012
** NOTE:
**
** CHANGES:
** 9/4/2012  DISABLE EXECUTION OF THIS PROGRAM UNTIL PROBLEM WITH
**           INCORRECT ANNOTAION IS RESOLVED - TAG DBH1
** 9/11/2012 ENABLE EXECUTION OF THIS PROGRAM - TAG DBH2
** 9/14/2012 CORRECT EFT REJECT ANNOTATIONS   - TAG JWO
** 9/24/2012 ADDED TEST TO SKIP RECORDS WITH DESC 'RDP REVER' - TAG JWO1
* 02/05/2013 J. OSTEEN - STORE ISN OF UPDATED RECORD ON REFERENCE TABLE
*                        CISNS FOR LATER FEED TO ODS. TAG JWO1
* 03/04/2014 CTS  -  BANK ANNOTATION AUTOMATION FIX FOR REVERSALS AND
*                    RECLAIMS   /* TAG - CTS *
*                 -  FOR REVERSAL RECLAIMS CHECK DESCR-DTE FIELD FOR SEL
*                    ECTING PAYMENT RECORD AND NOT EFF. ENTRY DATE
*                    /* TAG - CTS1 *
*                 -  CHECK THE PAYMENT AMOUNT IS MATCHING WITH THE BANK
*                    REPORT AMOUNT IFNOT MATCHING THEN DON't update
*                    THE ANNOTATION, INSTEAD WRITE INTO NEWERROR REPORT
*                    /*TAG CTS2*
* 05/29/2014 CTS - SEARCH WINDOW CHANGED TO +3/-4 DAYS FOR CPS PAYMENTS
*                  RECORDS.   /* TAG CTS3 *
* 01/06/2015 FAO - STOP EVALUATING AND ADJUSTING BEGIN DATE AFTER THE
*                  PAYMENT DATE RANGE -4 TO +3 DAYS IS COMPUTED.  DATE
*                  RANGE ADJUSTMENT LOGIC IS CREATING AN INCORRECT
*                  RANGE.  ALSO CHANGE INVERSE DATE COMPUTATION TO USE
*                  NEXT-MONTH DATE (END RANGE DATE) INSTEAD OF THE
*                  EFFECTIVE DATE.
* 11/09/2015 RAHUL-THE BANK CHANGED IT's description for EFT Rejects
*                  2 NEW DESCRIPTION (RDP ENTRY & RDP2 ENTRY) HAS BEEN
*                  IDENTIFIED AS RECLAIM REJECTED IN ANNOTATION
*                  DESCRIPTION - PRB71948 - TAG DASRAH
* 05/19/2017 SAIK - RESTOW FOR PIN EXPANSION
* ---------------------------------------------------------------------

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp604a extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;
    private DbsField input_Rec;

    private DbsGroup input_Rec__R_Field_1;
    private DbsField input_Rec_Hdr_Hyphens;

    private DbsGroup input_Rec__R_Field_2;
    private DbsField input_Rec_Hdr_Date;
    private DbsField input_Rec_Hdr_Fill_12;
    private DbsField input_Rec_Hdr_Block_Lit;
    private DbsField input_Rec_Hdr_Block;
    private DbsField input_Rec_Hdr_Fill_15;
    private DbsField input_Rec_Hdr_Nme_Lit;
    private DbsField input_Rec_Hdr_Fill_35;
    private DbsField input_Rec_Hdr_Page_Lit;
    private DbsField input_Rec_Hdr_Page_Nbr;

    private DbsGroup input_Rec__R_Field_3;
    private DbsField input_Rec_Hdr2_Fill_52;
    private DbsField input_Rec_Hdr2_Lit;
    private DbsField input_Rec_Hdr2_Fill_37;
    private DbsField input_Rec_Hdr2_Trans_File;
    private DbsField input_Rec_Hdr2_Fill;

    private DbsGroup input_Rec__R_Field_4;
    private DbsField input_Rec_Hdr3_Origin_Lit;
    private DbsField input_Rec_Hdr3_Origin;
    private DbsField input_Rec_Hdr3_Fill_5;
    private DbsField input_Rec_Hdr3_Orig_Lit;
    private DbsField input_Rec_Hdr3_Origin_Name;
    private DbsField input_Rec_Hdr3_Filenme_Lit;
    private DbsField input_Rec_Hdr3_Filenme;
    private DbsField input_Rec_Hdr3_Fill;

    private DbsGroup input_Rec__R_Field_5;
    private DbsField input_Rec_Dtl1_Star1;
    private DbsField input_Rec_Dtl1_Co_Name_Lit;
    private DbsField input_Rec_Dtl1_Co_Fil1;
    private DbsField input_Rec_Dtl1_Co_Name;
    private DbsField input_Rec_Dtl1_Star2;
    private DbsField input_Rec_Dtl1_Co_Id_Lit;
    private DbsField input_Rec_Dtl1_Co_Id;
    private DbsField input_Rec_Dtl1_Star3;
    private DbsField input_Rec_Dtl1_Descr_Lit;
    private DbsField input_Rec_Dtl1_Descr;
    private DbsField input_Rec_Dtl1_Star4;
    private DbsField input_Rec_Dtl1_Desc_Dte_Lit;
    private DbsField input_Rec_Dtl1_Descr_Dte;
    private DbsField input_Rec_Dtl1_Star5;
    private DbsField input_Rec_Dtl1_Trace_Lit;
    private DbsField input_Rec_Dtl1_Chase_Trace;
    private DbsField input_Rec_Dtl1_Fill;

    private DbsGroup input_Rec__R_Field_6;
    private DbsField input_Rec_Hdr_Name_Lit;
    private DbsField input_Rec_Hdr_Cust_Id_Lit;
    private DbsField input_Rec_Hdr_Tc_Lit;
    private DbsField input_Rec_Hdr_Amount_Lit;
    private DbsField input_Rec_Hdr_Tran_Aba_Lit;
    private DbsField input_Rec_Hdr_Ck_Lit;
    private DbsField input_Rec_Hdr_Acct_Num_Lit;
    private DbsField input_Rec_Hdr_Envlop_Lit;
    private DbsField input_Rec_Hdr_Orig_Tr_Lit;
    private DbsField input_Rec_Hdr_Fiche_Lit;

    private DbsGroup input_Rec__R_Field_7;
    private DbsField input_Rec_Dtl2_Name;
    private DbsField input_Rec_Dtl2_Cust_Id;

    private DbsGroup input_Rec__R_Field_8;
    private DbsField input_Rec_Dtl2_Fill;
    private DbsField input_Rec_Dtl2_Ppcn;
    private DbsField input_Rec_Dtl2_Fill1;
    private DbsField input_Rec_Dtl2_Tc;
    private DbsField input_Rec_Dtl2_Amount;
    private DbsField input_Rec_Dtl2_Fill2;
    private DbsField input_Rec_Dtl2_Tran_Aba;
    private DbsField input_Rec_Dtl2_Fill3;
    private DbsField input_Rec_Dtl2_Ck;
    private DbsField input_Rec_Dtl2_Fill4;
    private DbsField input_Rec_Dtl2_Acct_Num;
    private DbsField input_Rec_Dtl2_Envlop;
    private DbsField input_Rec_Dtl2_Comma;
    private DbsField input_Rec_Dtl2_Envlop_Num;
    private DbsField input_Rec_Dtl2_Orig_Tr;
    private DbsField input_Rec_Dtl2_Fill7;
    private DbsField input_Rec_Dtl2_Fiche_Dte;

    private DbsGroup input_Rec__R_Field_9;
    private DbsField input_Rec_Dtl2_Fiche_Mm;
    private DbsField input_Rec_Dtl2_Slash;
    private DbsField input_Rec_Dtl2_Fiche_Dd;

    private DbsGroup input_Rec__R_Field_10;
    private DbsField input_Rec_Dtl3_Eff_Dte_Lit;

    private DbsGroup input_Rec__R_Field_11;
    private DbsField input_Rec_Dtl3_Eff1;
    private DbsField input_Rec_Dtl3_Spc1;
    private DbsField input_Rec_Dtl3_Eff2;
    private DbsField input_Rec_Dtl3_Spc2;
    private DbsField input_Rec_Dtl3_Eff3;
    private DbsField input_Rec_Dtl3_Eff_Dte;
    private DbsField input_Rec_Dtl3_Fill;
    private DbsField input_Rec_Dtl3_Rsn_Cde;
    private DbsField input_Rec_Dtl3_Fill1;
    private DbsField input_Rec_Dtl3_Rsn_Desc;

    private DbsGroup input_Rec__R_Field_12;
    private DbsField input_Rec_Dtl4_Code_Lit;
    private DbsField input_Rec_Dtl4_Code;
    private DbsField input_Rec_Dtl4_Fill1;
    private DbsField input_Rec_Dtl4_Literial;
    private DbsField input_Rec_Dtl4_Fill2;
    private DbsField input_Rec_Dtl4_Correct_Acct;
    private DbsField input_Rec_Dtl4_Fill3;
    private DbsField input_Rec_Dtl4_Code_Desc;

    private DataAccessProgramView vw_read_Pymnt;
    private DbsField read_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField read_Pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField read_Pymnt_Cntrct_Invrse_Dte;
    private DbsField read_Pymnt_Cntrct_Orgn_Cde;
    private DbsField read_Pymnt_Cntrct_Hold_Grp;
    private DbsField read_Pymnt_Cntrct_Rqst_Settl_Dte;
    private DbsField read_Pymnt_Pymnt_Prcss_Seq_Nbr;
    private DbsField read_Pymnt_Pymnt_Annot_Ind;
    private DbsField read_Pymnt_Cntrct_Pymnt_Type_Ind;
    private DbsField read_Pymnt_Pymnt_Check_Dte;
    private DbsField read_Pymnt_Pymnt_Check_Amt;
    private DbsField read_Pymnt_Pymnt_Reqst_Nbr;
    private DbsField read_Pymnt_Pymnt_Instlmnt_Nbr;
    private DbsField read_Pymnt_Cntrct_Payee_Cde;
    private DbsField read_Pymnt_Pymnt_Nbr;
    private DbsField read_Pymnt_Pymnt_Settlmnt_Dte;
    private DbsField read_Pymnt_Pymnt_Stats_Cde;
    private DbsField read_Pymnt_Pymnt_Pay_Type_Req_Ind;
    private DbsField read_Pymnt_Notused1;
    private DbsField ws_Eff_Dte;

    private DbsGroup ws_Eff_Dte__R_Field_13;
    private DbsField ws_Eff_Dte_Ws_Eff_Mm;
    private DbsField ws_Eff_Dte_Ws_Eff_F1;
    private DbsField ws_Eff_Dte_Ws_Eff_Dd;
    private DbsField ws_Eff_Dte_Ws_Eff_F2;
    private DbsField ws_Eff_Dte_Ws_Eff_Yy;
    private DbsField ws_Cust_Id;

    private DbsGroup ws_Cust_Id__R_Field_14;
    private DbsField ws_Cust_Id_Ws_Cust_Cntrct;
    private DbsField ws_Cust_Id_Ws_Payee_Cde;
    private DbsField ws_Rsn_Cde;
    private DbsField ws_Rsn_Desc;
    private DbsField ws_Descr;
    private DbsField annot_Key;

    private DbsGroup annot_Key__R_Field_15;
    private DbsField annot_Key_Key_Ppcn_Nbr;
    private DbsField annot_Key_Key_Invrse_Dte;
    private DbsField annot_Key_Key_Orgn_Cde;
    private DbsField annot_Key_Key_Seq_Nbr;
    private DbsField payment_Key;

    private DbsGroup payment_Key__R_Field_16;
    private DbsField payment_Key_Paykey_Ppcn;
    private DbsField payment_Key_Paykey_Invrse_Dte;
    private DbsField payment_Key_Paykey_Fill;

    private DataAccessProgramView vw_read_Annot;
    private DbsField read_Annot_Cntrct_Rcrd_Typ;
    private DbsField read_Annot_Cntrct_Ppcn_Nbr;
    private DbsField read_Annot_Cntrct_Orgn_Cde;
    private DbsField read_Annot_Cntrct_Invrse_Dte;
    private DbsField read_Annot_Pymnt_Reqst_Nbr;
    private DbsField read_Annot_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup read_Annot__R_Field_17;
    private DbsField read_Annot_Pymnt_Prcss_Seq_Num;
    private DbsField read_Annot_Pymnt_Instmt_Nbr;
    private DbsField read_Annot_Pymnt_Rmrk_Line1_Txt;
    private DbsField read_Annot_Pymnt_Rmrk_Line2_Txt;
    private DbsField read_Annot_Count_Castpymnt_Annot_Grp;

    private DbsGroup read_Annot__R_Field_18;
    private DbsField read_Annot_Cpnd_Pymnt_Annot_Grp;

    private DbsGroup read_Annot_Pymnt_Annot_Grp;
    private DbsField read_Annot_Pymnt_Annot_Dte;
    private DbsField read_Annot_Pymnt_Annot_Id_Nbr;
    private DbsField read_Annot_Pymnt_Annot_Flag_Ind;
    private DbsField read_Annot_Cntrct_Hold_Tme;
    private DbsField read_Annot_Count_Castpymnt_Annot_Expand_Grp;

    private DbsGroup read_Annot__R_Field_19;
    private DbsField read_Annot_Cpnd_Pymnt_Annot_Expand_Grp;

    private DbsGroup read_Annot_Pymnt_Annot_Expand_Grp;
    private DbsField read_Annot_Pymnt_Annot_Expand_Cmnt;
    private DbsField read_Annot_Pymnt_Annot_Expand_Date;
    private DbsField read_Annot_Pymnt_Annot_Expand_Time;
    private DbsField read_Annot_Pymnt_Annot_Expand_Uid;

    private DataAccessProgramView vw_mod_Annot;
    private DbsField mod_Annot_Cntrct_Rcrd_Typ;
    private DbsField mod_Annot_Cntrct_Ppcn_Nbr;
    private DbsField mod_Annot_Cntrct_Orgn_Cde;
    private DbsField mod_Annot_Pymnt_Reqst_Nbr;
    private DbsField mod_Annot_Cntrct_Invrse_Dte;
    private DbsField mod_Annot_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup mod_Annot_Pymnt_Annot_Grp;
    private DbsField mod_Annot_Pymnt_Annot_Dte;
    private DbsField mod_Annot_Pymnt_Annot_Id_Nbr;
    private DbsField mod_Annot_Pymnt_Annot_Flag_Ind;

    private DbsGroup mod_Annot_Pymnt_Annot_Expand_Grp;
    private DbsField mod_Annot_Pymnt_Annot_Expand_Cmnt;
    private DbsField mod_Annot_Pymnt_Annot_Expand_Date;
    private DbsField mod_Annot_Pymnt_Annot_Expand_Time;
    private DbsField mod_Annot_Pymnt_Annot_Expand_Uid;
    private DbsField pnd_No_Annot;
    private DbsField pnd_Continue;
    private DbsField pnd_Updated;
    private DbsField pnd_I;
    private DbsField pnd_A;
    private DbsField pnd_Today;

    private DbsGroup pnd_Today__R_Field_20;
    private DbsField pnd_Today_Pnd_Todayn;
    private DbsField pnd_Date;

    private DbsGroup pnd_Date__R_Field_21;
    private DbsField pnd_Date_Pnd_Date_Alpha;
    private DbsField pnd_Date_X;

    private DbsGroup pnd_Date_X__R_Field_22;
    private DbsField pnd_Date_X_Pnd_Date_N;

    private DbsGroup pnd_Date_X__R_Field_23;
    private DbsField pnd_Date_X_Pnd_Yyyy;
    private DbsField pnd_Date_X_Pnd_Mm;
    private DbsField pnd_Date_X_Pnd_Dd;
    private DbsField pnd_Next_Month;
    private DbsField pnd_Next_X;

    private DbsGroup pnd_Next_X__R_Field_24;
    private DbsField pnd_Next_X_Pnd_Next_X_N;
    private DbsField pnd_Day_Of_Week;
    private DbsField pnd_Wk_Dte;
    private DbsField pnd_Cmnt;
    private DbsField pnd_Save_Isn;
    private DbsField pnd_Time;

    private DbsGroup pnd_Time__R_Field_25;
    private DbsField pnd_Time_Pnd_Now;
    private DbsField pnd_Diff;
    private DbsField pnd_Added;
    private DbsField pnd_Rc;
    private DbsField pnd_Title;
    private DbsField pnd_Ws_Cust_Name;
    private DbsField pnd_Status;
    private DbsField pnd_Tot;
    private DbsField pnd_Ws_Payee_Cde;

    private DbsGroup pnd_Ws_Payee_Cde__R_Field_26;
    private DbsField pnd_Ws_Payee_Cde_Payee_Cde;
    private DbsField pnd_No_Payment;
    private DbsField pnd_Ws_Amount;

    private DbsGroup pnd_Ws_Amount__R_Field_27;
    private DbsField pnd_Ws_Amount_Pnd_Ws_Pos;
    private DbsField pnd_Wk_Amt;

    private DbsGroup pnd_Wk_Amt__R_Field_28;
    private DbsField pnd_Wk_Amt_Pnd_Wk_Amtn;
    private DbsField pnd_Amt;

    private DbsGroup pnd_Amt__R_Field_29;
    private DbsField pnd_Amt_Pnd_Amt_Pos;
    private DbsField pnd_In_Amt;

    private DbsGroup pnd_In_Amt__R_Field_30;
    private DbsField pnd_In_Amt_Pnd_In_Amtn;
    private DbsField pnd_Num;
    private DbsField pnd_Rpt_Amt;
    private DbsField pnd_Valid_Reversal_Reject_Codes;
    private DbsField pnd_Ws_Trans_Cde;
    private DbsField pnd_Rej_Field;
    private DbsField pnd_J;
    private DbsField pnd_Ws_Report_Date;
    private DbsField pnd_Rej_Rev;
    private DbsField pnd_Index;
    private DbsField ws_Descr_Dte;

    private DbsGroup ws_Descr_Dte__R_Field_31;
    private DbsField ws_Descr_Dte_Ws_Descr_12;
    private DbsField ws_Descr_Dte_Ws_Descr_34;
    private DbsField ws_Descr_Dte_Ws_Descr_56;
    private DbsField ws_Descr_Dte1;

    private DbsGroup ws_Descr_Dte1__R_Field_32;
    private DbsField ws_Descr_Dte1_Ws_Desc_Mm;
    private DbsField ws_Descr_Dte1_Ws_Desc_F1;
    private DbsField ws_Descr_Dte1_Ws_Desc_Dd;
    private DbsField ws_Descr_Dte1_Ws_Desc_F2;
    private DbsField ws_Descr_Dte1_Ws_Desc_Yy;
    private DbsField pnd_Descr_Date;

    private DbsGroup pnd_Descr_Date__R_Field_33;
    private DbsField pnd_Descr_Date_Pnd_Descr_Date_N;

    private DbsGroup pnd_Descr_Date__R_Field_34;
    private DbsField pnd_Descr_Date_Pnd_Yyyy1;
    private DbsField pnd_Descr_Date_Pnd_Mm1;
    private DbsField pnd_Descr_Date_Pnd_Dd1;
    private DbsField pnd_Next_Month1;
    private DbsField pnd_Next_X1;

    private DbsGroup pnd_Next_X1__R_Field_35;
    private DbsField pnd_Next_X1_Pnd_Next_X1_N;
    private DbsField pnd_Day_Of_Week1;
    private DbsField pnd_Wk_Dte1;
    private DbsField pnd_Date1;

    private DbsGroup pnd_Date1__R_Field_36;
    private DbsField pnd_Date1_Pnd_Date1_Alpha;
    private DbsField pnd_Date_K;
    private DbsField pnd_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Err_Reason;
    private DbsField pnd_Cmnt1;
    private DbsField pnd_Wk_Dte_Eff;
    private DbsField pnd_Wk_Dte_Eff_3;
    private DbsField pnd_Wk_Eff_3;

    private DbsGroup pnd_Wk_Eff_3__R_Field_37;
    private DbsField pnd_Wk_Eff_3_Pnd_Wk_Eff_3_N;
    private DbsField pnd_Wk_Dte1_Desc;
    private DbsField pnd_Wk_Dte1_Desc_3;
    private DbsField pnd_Wk_Desc_3;

    private DbsGroup pnd_Wk_Desc_3__R_Field_38;
    private DbsField pnd_Wk_Desc_3_Pnd_Wk_Desc_3_N;
    private DbsField pnd_Date_D;
    private DbsField pnd_Date_E;
    private DbsField pnd_Date_K1;
    private DbsField error1;
    private DbsField error2;
    private DbsField error3;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();
        input_Rec = localVariables.newFieldInRecord("input_Rec", "INPUT-REC", FieldType.STRING, 132);

        input_Rec__R_Field_1 = localVariables.newGroupInRecord("input_Rec__R_Field_1", "REDEFINE", input_Rec);
        input_Rec_Hdr_Hyphens = input_Rec__R_Field_1.newFieldInGroup("input_Rec_Hdr_Hyphens", "HDR-HYPHENS", FieldType.STRING, 10);

        input_Rec__R_Field_2 = localVariables.newGroupInRecord("input_Rec__R_Field_2", "REDEFINE", input_Rec);
        input_Rec_Hdr_Date = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Date", "HDR-DATE", FieldType.STRING, 8);
        input_Rec_Hdr_Fill_12 = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Fill_12", "HDR-FILL-12", FieldType.STRING, 12);
        input_Rec_Hdr_Block_Lit = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Block_Lit", "HDR-BLOCK-LIT", FieldType.STRING, 9);
        input_Rec_Hdr_Block = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Block", "HDR-BLOCK", FieldType.STRING, 4);
        input_Rec_Hdr_Fill_15 = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Fill_15", "HDR-FILL-15", FieldType.STRING, 17);
        input_Rec_Hdr_Nme_Lit = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Nme_Lit", "HDR-NME-LIT", FieldType.STRING, 32);
        input_Rec_Hdr_Fill_35 = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Fill_35", "HDR-FILL-35", FieldType.STRING, 40);
        input_Rec_Hdr_Page_Lit = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Page_Lit", "HDR-PAGE-LIT", FieldType.STRING, 5);
        input_Rec_Hdr_Page_Nbr = input_Rec__R_Field_2.newFieldInGroup("input_Rec_Hdr_Page_Nbr", "HDR-PAGE-NBR", FieldType.NUMERIC, 5);

        input_Rec__R_Field_3 = localVariables.newGroupInRecord("input_Rec__R_Field_3", "REDEFINE", input_Rec);
        input_Rec_Hdr2_Fill_52 = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Hdr2_Fill_52", "HDR2-FILL-52", FieldType.STRING, 52);
        input_Rec_Hdr2_Lit = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Hdr2_Lit", "HDR2-LIT", FieldType.STRING, 28);
        input_Rec_Hdr2_Fill_37 = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Hdr2_Fill_37", "HDR2-FILL-37", FieldType.STRING, 37);
        input_Rec_Hdr2_Trans_File = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Hdr2_Trans_File", "HDR2-TRANS-FILE", FieldType.STRING, 10);
        input_Rec_Hdr2_Fill = input_Rec__R_Field_3.newFieldInGroup("input_Rec_Hdr2_Fill", "HDR2-FILL", FieldType.STRING, 5);

        input_Rec__R_Field_4 = localVariables.newGroupInRecord("input_Rec__R_Field_4", "REDEFINE", input_Rec);
        input_Rec_Hdr3_Origin_Lit = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Hdr3_Origin_Lit", "HDR3-ORIGIN-LIT", FieldType.STRING, 10);
        input_Rec_Hdr3_Origin = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Hdr3_Origin", "HDR3-ORIGIN", FieldType.NUMERIC, 11);
        input_Rec_Hdr3_Fill_5 = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Hdr3_Fill_5", "HDR3-FILL-5", FieldType.STRING, 5);
        input_Rec_Hdr3_Orig_Lit = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Hdr3_Orig_Lit", "HDR3-ORIG-LIT", FieldType.STRING, 13);
        input_Rec_Hdr3_Origin_Name = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Hdr3_Origin_Name", "HDR3-ORIGIN-NAME", FieldType.STRING, 28);
        input_Rec_Hdr3_Filenme_Lit = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Hdr3_Filenme_Lit", "HDR3-FILENME-LIT", FieldType.STRING, 11);
        input_Rec_Hdr3_Filenme = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Hdr3_Filenme", "HDR3-FILENME", FieldType.STRING, 23);
        input_Rec_Hdr3_Fill = input_Rec__R_Field_4.newFieldInGroup("input_Rec_Hdr3_Fill", "HDR3-FILL", FieldType.STRING, 31);

        input_Rec__R_Field_5 = localVariables.newGroupInRecord("input_Rec__R_Field_5", "REDEFINE", input_Rec);
        input_Rec_Dtl1_Star1 = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Star1", "DTL1-STAR1", FieldType.STRING, 2);
        input_Rec_Dtl1_Co_Name_Lit = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Co_Name_Lit", "DTL1-CO-NAME-LIT", FieldType.STRING, 8);
        input_Rec_Dtl1_Co_Fil1 = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Co_Fil1", "DTL1-CO-FIL1", FieldType.STRING, 1);
        input_Rec_Dtl1_Co_Name = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Co_Name", "DTL1-CO-NAME", FieldType.STRING, 17);
        input_Rec_Dtl1_Star2 = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Star2", "DTL1-STAR2", FieldType.STRING, 2);
        input_Rec_Dtl1_Co_Id_Lit = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Co_Id_Lit", "DTL1-CO-ID-LIT", FieldType.STRING, 7);
        input_Rec_Dtl1_Co_Id = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Co_Id", "DTL1-CO-ID", FieldType.NUMERIC, 11);
        input_Rec_Dtl1_Star3 = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Star3", "DTL1-STAR3", FieldType.STRING, 2);
        input_Rec_Dtl1_Descr_Lit = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Descr_Lit", "DTL1-DESCR-LIT", FieldType.STRING, 7);
        input_Rec_Dtl1_Descr = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Descr", "DTL1-DESCR", FieldType.STRING, 10);
        input_Rec_Dtl1_Star4 = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Star4", "DTL1-STAR4", FieldType.STRING, 2);
        input_Rec_Dtl1_Desc_Dte_Lit = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Desc_Dte_Lit", "DTL1-DESC-DTE-LIT", FieldType.STRING, 12);
        input_Rec_Dtl1_Descr_Dte = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Descr_Dte", "DTL1-DESCR-DTE", FieldType.STRING, 8);
        input_Rec_Dtl1_Star5 = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Star5", "DTL1-STAR5", FieldType.STRING, 2);
        input_Rec_Dtl1_Trace_Lit = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Trace_Lit", "DTL1-TRACE-LIT", FieldType.STRING, 13);
        input_Rec_Dtl1_Chase_Trace = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Chase_Trace", "DTL1-CHASE-TRACE", FieldType.NUMERIC, 7);
        input_Rec_Dtl1_Fill = input_Rec__R_Field_5.newFieldInGroup("input_Rec_Dtl1_Fill", "DTL1-FILL", FieldType.NUMERIC, 21);

        input_Rec__R_Field_6 = localVariables.newGroupInRecord("input_Rec__R_Field_6", "REDEFINE", input_Rec);
        input_Rec_Hdr_Name_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Name_Lit", "HDR-NAME-LIT", FieldType.STRING, 24);
        input_Rec_Hdr_Cust_Id_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Cust_Id_Lit", "HDR-CUST-ID-LIT", FieldType.STRING, 23);
        input_Rec_Hdr_Tc_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Tc_Lit", "HDR-TC-LIT", FieldType.STRING, 4);
        input_Rec_Hdr_Amount_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Amount_Lit", "HDR-AMOUNT-LIT", FieldType.STRING, 14);
        input_Rec_Hdr_Tran_Aba_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Tran_Aba_Lit", "HDR-TRAN-ABA-LIT", FieldType.STRING, 9);
        input_Rec_Hdr_Ck_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Ck_Lit", "HDR-CK-LIT", FieldType.STRING, 4);
        input_Rec_Hdr_Acct_Num_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Acct_Num_Lit", "HDR-ACCT-NUM-LIT", FieldType.STRING, 18);
        input_Rec_Hdr_Envlop_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Envlop_Lit", "HDR-ENVLOP-LIT", FieldType.STRING, 12);
        input_Rec_Hdr_Orig_Tr_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Orig_Tr_Lit", "HDR-ORIG-TR-LIT", FieldType.STRING, 18);
        input_Rec_Hdr_Fiche_Lit = input_Rec__R_Field_6.newFieldInGroup("input_Rec_Hdr_Fiche_Lit", "HDR-FICHE-LIT", FieldType.STRING, 5);

        input_Rec__R_Field_7 = localVariables.newGroupInRecord("input_Rec__R_Field_7", "REDEFINE", input_Rec);
        input_Rec_Dtl2_Name = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Name", "DTL2-NAME", FieldType.STRING, 24);
        input_Rec_Dtl2_Cust_Id = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Cust_Id", "DTL2-CUST-ID", FieldType.STRING, 10);

        input_Rec__R_Field_8 = input_Rec__R_Field_7.newGroupInGroup("input_Rec__R_Field_8", "REDEFINE", input_Rec_Dtl2_Cust_Id);
        input_Rec_Dtl2_Fill = input_Rec__R_Field_8.newFieldInGroup("input_Rec_Dtl2_Fill", "DTL2-FILL", FieldType.STRING, 1);
        input_Rec_Dtl2_Ppcn = input_Rec__R_Field_8.newFieldInGroup("input_Rec_Dtl2_Ppcn", "DTL2-PPCN", FieldType.STRING, 9);
        input_Rec_Dtl2_Fill1 = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Fill1", "DTL2-FILL1", FieldType.STRING, 13);
        input_Rec_Dtl2_Tc = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Tc", "DTL2-TC", FieldType.STRING, 2);
        input_Rec_Dtl2_Amount = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Amount", "DTL2-AMOUNT", FieldType.STRING, 15);
        input_Rec_Dtl2_Fill2 = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Fill2", "DTL2-FILL2", FieldType.STRING, 1);
        input_Rec_Dtl2_Tran_Aba = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Tran_Aba", "DTL2-TRAN-ABA", FieldType.STRING, 9);
        input_Rec_Dtl2_Fill3 = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Fill3", "DTL2-FILL3", FieldType.STRING, 1);
        input_Rec_Dtl2_Ck = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Ck", "DTL2-CK", FieldType.STRING, 1);
        input_Rec_Dtl2_Fill4 = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Fill4", "DTL2-FILL4", FieldType.STRING, 1);
        input_Rec_Dtl2_Acct_Num = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Acct_Num", "DTL2-ACCT-NUM", FieldType.STRING, 20);
        input_Rec_Dtl2_Envlop = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Envlop", "DTL2-ENVLOP", FieldType.STRING, 6);
        input_Rec_Dtl2_Comma = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Comma", "DTL2-COMMA", FieldType.STRING, 1);
        input_Rec_Dtl2_Envlop_Num = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Envlop_Num", "DTL2-ENVLOP-NUM", FieldType.STRING, 5);
        input_Rec_Dtl2_Orig_Tr = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Orig_Tr", "DTL2-ORIG-TR", FieldType.STRING, 16);
        input_Rec_Dtl2_Fill7 = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Fill7", "DTL2-FILL7", FieldType.STRING, 2);
        input_Rec_Dtl2_Fiche_Dte = input_Rec__R_Field_7.newFieldInGroup("input_Rec_Dtl2_Fiche_Dte", "DTL2-FICHE-DTE", FieldType.STRING, 5);

        input_Rec__R_Field_9 = input_Rec__R_Field_7.newGroupInGroup("input_Rec__R_Field_9", "REDEFINE", input_Rec_Dtl2_Fiche_Dte);
        input_Rec_Dtl2_Fiche_Mm = input_Rec__R_Field_9.newFieldInGroup("input_Rec_Dtl2_Fiche_Mm", "DTL2-FICHE-MM", FieldType.STRING, 2);
        input_Rec_Dtl2_Slash = input_Rec__R_Field_9.newFieldInGroup("input_Rec_Dtl2_Slash", "DTL2-SLASH", FieldType.STRING, 1);
        input_Rec_Dtl2_Fiche_Dd = input_Rec__R_Field_9.newFieldInGroup("input_Rec_Dtl2_Fiche_Dd", "DTL2-FICHE-DD", FieldType.STRING, 2);

        input_Rec__R_Field_10 = localVariables.newGroupInRecord("input_Rec__R_Field_10", "REDEFINE", input_Rec);
        input_Rec_Dtl3_Eff_Dte_Lit = input_Rec__R_Field_10.newFieldInGroup("input_Rec_Dtl3_Eff_Dte_Lit", "DTL3-EFF-DTE-LIT", FieldType.STRING, 17);

        input_Rec__R_Field_11 = input_Rec__R_Field_10.newGroupInGroup("input_Rec__R_Field_11", "REDEFINE", input_Rec_Dtl3_Eff_Dte_Lit);
        input_Rec_Dtl3_Eff1 = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Dtl3_Eff1", "DTL3-EFF1", FieldType.STRING, 4);
        input_Rec_Dtl3_Spc1 = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Dtl3_Spc1", "DTL3-SPC1", FieldType.STRING, 1);
        input_Rec_Dtl3_Eff2 = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Dtl3_Eff2", "DTL3-EFF2", FieldType.STRING, 5);
        input_Rec_Dtl3_Spc2 = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Dtl3_Spc2", "DTL3-SPC2", FieldType.STRING, 1);
        input_Rec_Dtl3_Eff3 = input_Rec__R_Field_11.newFieldInGroup("input_Rec_Dtl3_Eff3", "DTL3-EFF3", FieldType.STRING, 4);
        input_Rec_Dtl3_Eff_Dte = input_Rec__R_Field_10.newFieldInGroup("input_Rec_Dtl3_Eff_Dte", "DTL3-EFF-DTE", FieldType.STRING, 8);
        input_Rec_Dtl3_Fill = input_Rec__R_Field_10.newFieldInGroup("input_Rec_Dtl3_Fill", "DTL3-FILL", FieldType.STRING, 27);
        input_Rec_Dtl3_Rsn_Cde = input_Rec__R_Field_10.newFieldInGroup("input_Rec_Dtl3_Rsn_Cde", "DTL3-RSN-CDE", FieldType.STRING, 3);
        input_Rec_Dtl3_Fill1 = input_Rec__R_Field_10.newFieldInGroup("input_Rec_Dtl3_Fill1", "DTL3-FILL1", FieldType.STRING, 2);
        input_Rec_Dtl3_Rsn_Desc = input_Rec__R_Field_10.newFieldInGroup("input_Rec_Dtl3_Rsn_Desc", "DTL3-RSN-DESC", FieldType.STRING, 30);

        input_Rec__R_Field_12 = localVariables.newGroupInRecord("input_Rec__R_Field_12", "REDEFINE", input_Rec);
        input_Rec_Dtl4_Code_Lit = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Dtl4_Code_Lit", "DTL4-CODE-LIT", FieldType.STRING, 9);
        input_Rec_Dtl4_Code = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Dtl4_Code", "DTL4-CODE", FieldType.STRING, 3);
        input_Rec_Dtl4_Fill1 = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Dtl4_Fill1", "DTL4-FILL1", FieldType.STRING, 4);
        input_Rec_Dtl4_Literial = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Dtl4_Literial", "DTL4-LITERIAL", FieldType.STRING, 18);
        input_Rec_Dtl4_Fill2 = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Dtl4_Fill2", "DTL4-FILL2", FieldType.STRING, 11);
        input_Rec_Dtl4_Correct_Acct = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Dtl4_Correct_Acct", "DTL4-CORRECT-ACCT", FieldType.STRING, 11);
        input_Rec_Dtl4_Fill3 = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Dtl4_Fill3", "DTL4-FILL3", FieldType.STRING, 43);
        input_Rec_Dtl4_Code_Desc = input_Rec__R_Field_12.newFieldInGroup("input_Rec_Dtl4_Code_Desc", "DTL4-CODE-DESC", FieldType.STRING, 32);

        vw_read_Pymnt = new DataAccessProgramView(new NameInfo("vw_read_Pymnt", "READ-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        read_Pymnt_Cntrct_Ppcn_Nbr = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        read_Pymnt_Cntrct_Unq_Id_Nbr = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        read_Pymnt_Cntrct_Invrse_Dte = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        read_Pymnt_Cntrct_Orgn_Cde = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        read_Pymnt_Cntrct_Hold_Grp = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", FieldType.STRING, 3, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_GRP");
        read_Pymnt_Cntrct_Rqst_Settl_Dte = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Cntrct_Rqst_Settl_Dte", "CNTRCT-RQST-SETTL-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNTRCT_RQST_SETTL_DTE");
        read_Pymnt_Pymnt_Prcss_Seq_Nbr = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        read_Pymnt_Pymnt_Annot_Ind = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_ANNOT_IND");
        read_Pymnt_Cntrct_Pymnt_Type_Ind = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNTRCT_PYMNT_TYPE_IND");
        read_Pymnt_Pymnt_Check_Dte = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        read_Pymnt_Pymnt_Check_Amt = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        read_Pymnt_Pymnt_Reqst_Nbr = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "PYMNT_REQST_NBR");
        read_Pymnt_Pymnt_Instlmnt_Nbr = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Instlmnt_Nbr", "PYMNT-INSTLMNT-NBR", FieldType.NUMERIC, 
            2, RepeatingFieldStrategy.None, "PYMNT_INSTLMNT_NBR");
        read_Pymnt_Cntrct_Payee_Cde = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        read_Pymnt_Pymnt_Nbr = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Nbr", "PYMNT-NBR", FieldType.NUMERIC, 10, RepeatingFieldStrategy.None, 
            "PYMNT_NBR");
        read_Pymnt_Pymnt_Settlmnt_Dte = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Settlmnt_Dte", "PYMNT-SETTLMNT-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_SETTLMNT_DTE");
        read_Pymnt_Pymnt_Stats_Cde = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        read_Pymnt_Pymnt_Pay_Type_Req_Ind = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1, RepeatingFieldStrategy.None, "PYMNT_PAY_TYPE_REQ_IND");
        read_Pymnt_Notused1 = vw_read_Pymnt.getRecord().newFieldInGroup("read_Pymnt_Notused1", "NOTUSED1", FieldType.STRING, 8, RepeatingFieldStrategy.None, 
            "NOTUSED1");
        registerRecord(vw_read_Pymnt);

        ws_Eff_Dte = localVariables.newFieldInRecord("ws_Eff_Dte", "WS-EFF-DTE", FieldType.STRING, 8);

        ws_Eff_Dte__R_Field_13 = localVariables.newGroupInRecord("ws_Eff_Dte__R_Field_13", "REDEFINE", ws_Eff_Dte);
        ws_Eff_Dte_Ws_Eff_Mm = ws_Eff_Dte__R_Field_13.newFieldInGroup("ws_Eff_Dte_Ws_Eff_Mm", "WS-EFF-MM", FieldType.STRING, 2);
        ws_Eff_Dte_Ws_Eff_F1 = ws_Eff_Dte__R_Field_13.newFieldInGroup("ws_Eff_Dte_Ws_Eff_F1", "WS-EFF-F1", FieldType.STRING, 1);
        ws_Eff_Dte_Ws_Eff_Dd = ws_Eff_Dte__R_Field_13.newFieldInGroup("ws_Eff_Dte_Ws_Eff_Dd", "WS-EFF-DD", FieldType.STRING, 2);
        ws_Eff_Dte_Ws_Eff_F2 = ws_Eff_Dte__R_Field_13.newFieldInGroup("ws_Eff_Dte_Ws_Eff_F2", "WS-EFF-F2", FieldType.STRING, 1);
        ws_Eff_Dte_Ws_Eff_Yy = ws_Eff_Dte__R_Field_13.newFieldInGroup("ws_Eff_Dte_Ws_Eff_Yy", "WS-EFF-YY", FieldType.STRING, 2);
        ws_Cust_Id = localVariables.newFieldInRecord("ws_Cust_Id", "WS-CUST-ID", FieldType.STRING, 10);

        ws_Cust_Id__R_Field_14 = localVariables.newGroupInRecord("ws_Cust_Id__R_Field_14", "REDEFINE", ws_Cust_Id);
        ws_Cust_Id_Ws_Cust_Cntrct = ws_Cust_Id__R_Field_14.newFieldInGroup("ws_Cust_Id_Ws_Cust_Cntrct", "WS-CUST-CNTRCT", FieldType.STRING, 8);
        ws_Cust_Id_Ws_Payee_Cde = ws_Cust_Id__R_Field_14.newFieldInGroup("ws_Cust_Id_Ws_Payee_Cde", "WS-PAYEE-CDE", FieldType.STRING, 2);
        ws_Rsn_Cde = localVariables.newFieldInRecord("ws_Rsn_Cde", "WS-RSN-CDE", FieldType.STRING, 3);
        ws_Rsn_Desc = localVariables.newFieldInRecord("ws_Rsn_Desc", "WS-RSN-DESC", FieldType.STRING, 30);
        ws_Descr = localVariables.newFieldInRecord("ws_Descr", "WS-DESCR", FieldType.STRING, 10);
        annot_Key = localVariables.newFieldInRecord("annot_Key", "ANNOT-KEY", FieldType.STRING, 29);

        annot_Key__R_Field_15 = localVariables.newGroupInRecord("annot_Key__R_Field_15", "REDEFINE", annot_Key);
        annot_Key_Key_Ppcn_Nbr = annot_Key__R_Field_15.newFieldInGroup("annot_Key_Key_Ppcn_Nbr", "KEY-PPCN-NBR", FieldType.STRING, 10);
        annot_Key_Key_Invrse_Dte = annot_Key__R_Field_15.newFieldInGroup("annot_Key_Key_Invrse_Dte", "KEY-INVRSE-DTE", FieldType.NUMERIC, 8);
        annot_Key_Key_Orgn_Cde = annot_Key__R_Field_15.newFieldInGroup("annot_Key_Key_Orgn_Cde", "KEY-ORGN-CDE", FieldType.STRING, 2);
        annot_Key_Key_Seq_Nbr = annot_Key__R_Field_15.newFieldInGroup("annot_Key_Key_Seq_Nbr", "KEY-SEQ-NBR", FieldType.NUMERIC, 9);
        payment_Key = localVariables.newFieldInRecord("payment_Key", "PAYMENT-KEY", FieldType.STRING, 29);

        payment_Key__R_Field_16 = localVariables.newGroupInRecord("payment_Key__R_Field_16", "REDEFINE", payment_Key);
        payment_Key_Paykey_Ppcn = payment_Key__R_Field_16.newFieldInGroup("payment_Key_Paykey_Ppcn", "PAYKEY-PPCN", FieldType.STRING, 10);
        payment_Key_Paykey_Invrse_Dte = payment_Key__R_Field_16.newFieldInGroup("payment_Key_Paykey_Invrse_Dte", "PAYKEY-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        payment_Key_Paykey_Fill = payment_Key__R_Field_16.newFieldInGroup("payment_Key_Paykey_Fill", "PAYKEY-FILL", FieldType.STRING, 11);

        vw_read_Annot = new DataAccessProgramView(new NameInfo("vw_read_Annot", "READ-ANNOT"), "FCP_CONS_ANNOT", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ANNOT"));
        read_Annot_Cntrct_Rcrd_Typ = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_RCRD_TYP");
        read_Annot_Cntrct_Ppcn_Nbr = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        read_Annot_Cntrct_Orgn_Cde = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        read_Annot_Cntrct_Invrse_Dte = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        read_Annot_Pymnt_Reqst_Nbr = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, 
            RepeatingFieldStrategy.None, "PYMNT_REQST_NBR");
        read_Annot_Pymnt_Prcss_Seq_Nbr = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        read_Annot__R_Field_17 = vw_read_Annot.getRecord().newGroupInGroup("read_Annot__R_Field_17", "REDEFINE", read_Annot_Pymnt_Prcss_Seq_Nbr);
        read_Annot_Pymnt_Prcss_Seq_Num = read_Annot__R_Field_17.newFieldInGroup("read_Annot_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        read_Annot_Pymnt_Instmt_Nbr = read_Annot__R_Field_17.newFieldInGroup("read_Annot_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        read_Annot_Pymnt_Rmrk_Line1_Txt = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Pymnt_Rmrk_Line1_Txt", "PYMNT-RMRK-LINE1-TXT", FieldType.STRING, 
            70, RepeatingFieldStrategy.None, "PYMNT_RMRK_LINE1_TXT");
        read_Annot_Pymnt_Rmrk_Line2_Txt = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Pymnt_Rmrk_Line2_Txt", "PYMNT-RMRK-LINE2-TXT", FieldType.STRING, 
            70, RepeatingFieldStrategy.None, "PYMNT_RMRK_LINE2_TXT");
        read_Annot_Count_Castpymnt_Annot_Grp = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Count_Castpymnt_Annot_Grp", "C*PYMNT-ANNOT-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");

        read_Annot__R_Field_18 = vw_read_Annot.getRecord().newGroupInGroup("read_Annot__R_Field_18", "REDEFINE", read_Annot_Count_Castpymnt_Annot_Grp);
        read_Annot_Cpnd_Pymnt_Annot_Grp = read_Annot__R_Field_18.newFieldInGroup("read_Annot_Cpnd_Pymnt_Annot_Grp", "C#PYMNT-ANNOT-GRP", FieldType.NUMERIC, 
            3);

        read_Annot_Pymnt_Annot_Grp = vw_read_Annot.getRecord().newGroupInGroup("read_Annot_Pymnt_Annot_Grp", "PYMNT-ANNOT-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        read_Annot_Pymnt_Annot_Dte = read_Annot_Pymnt_Annot_Grp.newFieldArrayInGroup("read_Annot_Pymnt_Annot_Dte", "PYMNT-ANNOT-DTE", FieldType.DATE, 
            new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_DTE", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        read_Annot_Pymnt_Annot_Id_Nbr = read_Annot_Pymnt_Annot_Grp.newFieldArrayInGroup("read_Annot_Pymnt_Annot_Id_Nbr", "PYMNT-ANNOT-ID-NBR", FieldType.STRING, 
            3, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_ID_NBR", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        read_Annot_Pymnt_Annot_Flag_Ind = read_Annot_Pymnt_Annot_Grp.newFieldArrayInGroup("read_Annot_Pymnt_Annot_Flag_Ind", "PYMNT-ANNOT-FLAG-IND", FieldType.STRING, 
            1, new DbsArrayController(1, 20) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_FLAG_IND", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        read_Annot_Cntrct_Hold_Tme = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", FieldType.TIME, RepeatingFieldStrategy.None, 
            "CNTRCT_HOLD_TME");
        read_Annot_Count_Castpymnt_Annot_Expand_Grp = vw_read_Annot.getRecord().newFieldInGroup("read_Annot_Count_Castpymnt_Annot_Expand_Grp", "C*PYMNT-ANNOT-EXPAND-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");

        read_Annot__R_Field_19 = vw_read_Annot.getRecord().newGroupInGroup("read_Annot__R_Field_19", "REDEFINE", read_Annot_Count_Castpymnt_Annot_Expand_Grp);
        read_Annot_Cpnd_Pymnt_Annot_Expand_Grp = read_Annot__R_Field_19.newFieldInGroup("read_Annot_Cpnd_Pymnt_Annot_Expand_Grp", "C#PYMNT-ANNOT-EXPAND-GRP", 
            FieldType.NUMERIC, 3);

        read_Annot_Pymnt_Annot_Expand_Grp = vw_read_Annot.getRecord().newGroupInGroup("read_Annot_Pymnt_Annot_Expand_Grp", "PYMNT-ANNOT-EXPAND-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        read_Annot_Pymnt_Annot_Expand_Cmnt = read_Annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("read_Annot_Pymnt_Annot_Expand_Cmnt", "PYMNT-ANNOT-EXPAND-CMNT", 
            FieldType.STRING, 50, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_CMNT", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        read_Annot_Pymnt_Annot_Expand_Date = read_Annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("read_Annot_Pymnt_Annot_Expand_Date", "PYMNT-ANNOT-EXPAND-DATE", 
            FieldType.STRING, 8, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_DATE", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        read_Annot_Pymnt_Annot_Expand_Time = read_Annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("read_Annot_Pymnt_Annot_Expand_Time", "PYMNT-ANNOT-EXPAND-TIME", 
            FieldType.STRING, 4, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_TIME", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        read_Annot_Pymnt_Annot_Expand_Uid = read_Annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("read_Annot_Pymnt_Annot_Expand_Uid", "PYMNT-ANNOT-EXPAND-UID", 
            FieldType.STRING, 8, new DbsArrayController(1, 99) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_UID", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        registerRecord(vw_read_Annot);

        vw_mod_Annot = new DataAccessProgramView(new NameInfo("vw_mod_Annot", "MOD-ANNOT"), "FCP_CONS_ANNOT", "FCP_EFT_GLBL", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ANNOT"));
        mod_Annot_Cntrct_Rcrd_Typ = vw_mod_Annot.getRecord().newFieldInGroup("mod_Annot_Cntrct_Rcrd_Typ", "CNTRCT-RCRD-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "CNTRCT_RCRD_TYP");
        mod_Annot_Cntrct_Ppcn_Nbr = vw_mod_Annot.getRecord().newFieldInGroup("mod_Annot_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        mod_Annot_Cntrct_Orgn_Cde = vw_mod_Annot.getRecord().newFieldInGroup("mod_Annot_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        mod_Annot_Pymnt_Reqst_Nbr = vw_mod_Annot.getRecord().newFieldInGroup("mod_Annot_Pymnt_Reqst_Nbr", "PYMNT-REQST-NBR", FieldType.STRING, 35, RepeatingFieldStrategy.None, 
            "PYMNT_REQST_NBR");
        mod_Annot_Cntrct_Invrse_Dte = vw_mod_Annot.getRecord().newFieldInGroup("mod_Annot_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        mod_Annot_Pymnt_Prcss_Seq_Nbr = vw_mod_Annot.getRecord().newFieldInGroup("mod_Annot_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        mod_Annot_Pymnt_Annot_Grp = vw_mod_Annot.getRecord().newGroupInGroup("mod_Annot_Pymnt_Annot_Grp", "PYMNT-ANNOT-GRP", null, RepeatingFieldStrategy.PeriodicGroupFieldArray, 
            "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        mod_Annot_Pymnt_Annot_Dte = mod_Annot_Pymnt_Annot_Grp.newFieldArrayInGroup("mod_Annot_Pymnt_Annot_Dte", "PYMNT-ANNOT-DTE", FieldType.DATE, new 
            DbsArrayController(7, 7) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_DTE", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        mod_Annot_Pymnt_Annot_Id_Nbr = mod_Annot_Pymnt_Annot_Grp.newFieldArrayInGroup("mod_Annot_Pymnt_Annot_Id_Nbr", "PYMNT-ANNOT-ID-NBR", FieldType.STRING, 
            3, new DbsArrayController(7, 7) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_ID_NBR", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");
        mod_Annot_Pymnt_Annot_Flag_Ind = mod_Annot_Pymnt_Annot_Grp.newFieldArrayInGroup("mod_Annot_Pymnt_Annot_Flag_Ind", "PYMNT-ANNOT-FLAG-IND", FieldType.STRING, 
            1, new DbsArrayController(7, 7) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_FLAG_IND", "FCP_EFT_GLBL_PYMNT_ANNOT_GRP");

        mod_Annot_Pymnt_Annot_Expand_Grp = vw_mod_Annot.getRecord().newGroupInGroup("mod_Annot_Pymnt_Annot_Expand_Grp", "PYMNT-ANNOT-EXPAND-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        mod_Annot_Pymnt_Annot_Expand_Cmnt = mod_Annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("mod_Annot_Pymnt_Annot_Expand_Cmnt", "PYMNT-ANNOT-EXPAND-CMNT", 
            FieldType.STRING, 50, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_CMNT", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        mod_Annot_Pymnt_Annot_Expand_Date = mod_Annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("mod_Annot_Pymnt_Annot_Expand_Date", "PYMNT-ANNOT-EXPAND-DATE", 
            FieldType.STRING, 8, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_DATE", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        mod_Annot_Pymnt_Annot_Expand_Time = mod_Annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("mod_Annot_Pymnt_Annot_Expand_Time", "PYMNT-ANNOT-EXPAND-TIME", 
            FieldType.STRING, 4, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_TIME", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        mod_Annot_Pymnt_Annot_Expand_Uid = mod_Annot_Pymnt_Annot_Expand_Grp.newFieldArrayInGroup("mod_Annot_Pymnt_Annot_Expand_Uid", "PYMNT-ANNOT-EXPAND-UID", 
            FieldType.STRING, 8, new DbsArrayController(1, 1) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_ANNOT_EXPAND_UID", "FCP_EFT_GLBL_PYMNT_ANNOT_EXPAND_GRP");
        registerRecord(vw_mod_Annot);

        pnd_No_Annot = localVariables.newFieldInRecord("pnd_No_Annot", "#NO-ANNOT", FieldType.BOOLEAN, 1);
        pnd_Continue = localVariables.newFieldInRecord("pnd_Continue", "#CONTINUE", FieldType.BOOLEAN, 1);
        pnd_Updated = localVariables.newFieldInRecord("pnd_Updated", "#UPDATED", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        pnd_A = localVariables.newFieldInRecord("pnd_A", "#A", FieldType.NUMERIC, 3);
        pnd_Today = localVariables.newFieldInRecord("pnd_Today", "#TODAY", FieldType.STRING, 8);

        pnd_Today__R_Field_20 = localVariables.newGroupInRecord("pnd_Today__R_Field_20", "REDEFINE", pnd_Today);
        pnd_Today_Pnd_Todayn = pnd_Today__R_Field_20.newFieldInGroup("pnd_Today_Pnd_Todayn", "#TODAYN", FieldType.NUMERIC, 8);
        pnd_Date = localVariables.newFieldInRecord("pnd_Date", "#DATE", FieldType.NUMERIC, 8);

        pnd_Date__R_Field_21 = localVariables.newGroupInRecord("pnd_Date__R_Field_21", "REDEFINE", pnd_Date);
        pnd_Date_Pnd_Date_Alpha = pnd_Date__R_Field_21.newFieldInGroup("pnd_Date_Pnd_Date_Alpha", "#DATE-ALPHA", FieldType.STRING, 8);
        pnd_Date_X = localVariables.newFieldInRecord("pnd_Date_X", "#DATE-X", FieldType.STRING, 8);

        pnd_Date_X__R_Field_22 = localVariables.newGroupInRecord("pnd_Date_X__R_Field_22", "REDEFINE", pnd_Date_X);
        pnd_Date_X_Pnd_Date_N = pnd_Date_X__R_Field_22.newFieldInGroup("pnd_Date_X_Pnd_Date_N", "#DATE-N", FieldType.NUMERIC, 8);

        pnd_Date_X__R_Field_23 = localVariables.newGroupInRecord("pnd_Date_X__R_Field_23", "REDEFINE", pnd_Date_X);
        pnd_Date_X_Pnd_Yyyy = pnd_Date_X__R_Field_23.newFieldInGroup("pnd_Date_X_Pnd_Yyyy", "#YYYY", FieldType.NUMERIC, 4);
        pnd_Date_X_Pnd_Mm = pnd_Date_X__R_Field_23.newFieldInGroup("pnd_Date_X_Pnd_Mm", "#MM", FieldType.NUMERIC, 2);
        pnd_Date_X_Pnd_Dd = pnd_Date_X__R_Field_23.newFieldInGroup("pnd_Date_X_Pnd_Dd", "#DD", FieldType.NUMERIC, 2);
        pnd_Next_Month = localVariables.newFieldInRecord("pnd_Next_Month", "#NEXT-MONTH", FieldType.DATE);
        pnd_Next_X = localVariables.newFieldInRecord("pnd_Next_X", "#NEXT-X", FieldType.STRING, 8);

        pnd_Next_X__R_Field_24 = localVariables.newGroupInRecord("pnd_Next_X__R_Field_24", "REDEFINE", pnd_Next_X);
        pnd_Next_X_Pnd_Next_X_N = pnd_Next_X__R_Field_24.newFieldInGroup("pnd_Next_X_Pnd_Next_X_N", "#NEXT-X-N", FieldType.NUMERIC, 8);
        pnd_Day_Of_Week = localVariables.newFieldInRecord("pnd_Day_Of_Week", "#DAY-OF-WEEK", FieldType.STRING, 1);
        pnd_Wk_Dte = localVariables.newFieldInRecord("pnd_Wk_Dte", "#WK-DTE", FieldType.DATE);
        pnd_Cmnt = localVariables.newFieldInRecord("pnd_Cmnt", "#CMNT", FieldType.STRING, 50);
        pnd_Save_Isn = localVariables.newFieldInRecord("pnd_Save_Isn", "#SAVE-ISN", FieldType.NUMERIC, 11);
        pnd_Time = localVariables.newFieldInRecord("pnd_Time", "#TIME", FieldType.NUMERIC, 7);

        pnd_Time__R_Field_25 = localVariables.newGroupInRecord("pnd_Time__R_Field_25", "REDEFINE", pnd_Time);
        pnd_Time_Pnd_Now = pnd_Time__R_Field_25.newFieldInGroup("pnd_Time_Pnd_Now", "#NOW", FieldType.NUMERIC, 4);
        pnd_Diff = localVariables.newFieldInRecord("pnd_Diff", "#DIFF", FieldType.NUMERIC, 10);
        pnd_Added = localVariables.newFieldInRecord("pnd_Added", "#ADDED", FieldType.BOOLEAN, 1);
        pnd_Rc = localVariables.newFieldInRecord("pnd_Rc", "#RC", FieldType.NUMERIC, 2);
        pnd_Title = localVariables.newFieldInRecord("pnd_Title", "#TITLE", FieldType.STRING, 50);
        pnd_Ws_Cust_Name = localVariables.newFieldInRecord("pnd_Ws_Cust_Name", "#WS-CUST-NAME", FieldType.STRING, 24);
        pnd_Status = localVariables.newFieldInRecord("pnd_Status", "#STATUS", FieldType.STRING, 15);
        pnd_Tot = localVariables.newFieldInRecord("pnd_Tot", "#TOT", FieldType.NUMERIC, 6);
        pnd_Ws_Payee_Cde = localVariables.newFieldInRecord("pnd_Ws_Payee_Cde", "#WS-PAYEE-CDE", FieldType.STRING, 4);

        pnd_Ws_Payee_Cde__R_Field_26 = localVariables.newGroupInRecord("pnd_Ws_Payee_Cde__R_Field_26", "REDEFINE", pnd_Ws_Payee_Cde);
        pnd_Ws_Payee_Cde_Payee_Cde = pnd_Ws_Payee_Cde__R_Field_26.newFieldInGroup("pnd_Ws_Payee_Cde_Payee_Cde", "PAYEE-CDE", FieldType.STRING, 2);
        pnd_No_Payment = localVariables.newFieldInRecord("pnd_No_Payment", "#NO-PAYMENT", FieldType.BOOLEAN, 1);
        pnd_Ws_Amount = localVariables.newFieldInRecord("pnd_Ws_Amount", "#WS-AMOUNT", FieldType.STRING, 15);

        pnd_Ws_Amount__R_Field_27 = localVariables.newGroupInRecord("pnd_Ws_Amount__R_Field_27", "REDEFINE", pnd_Ws_Amount);
        pnd_Ws_Amount_Pnd_Ws_Pos = pnd_Ws_Amount__R_Field_27.newFieldArrayInGroup("pnd_Ws_Amount_Pnd_Ws_Pos", "#WS-POS", FieldType.STRING, 1, new DbsArrayController(1, 
            15));
        pnd_Wk_Amt = localVariables.newFieldInRecord("pnd_Wk_Amt", "#WK-AMT", FieldType.STRING, 14);

        pnd_Wk_Amt__R_Field_28 = localVariables.newGroupInRecord("pnd_Wk_Amt__R_Field_28", "REDEFINE", pnd_Wk_Amt);
        pnd_Wk_Amt_Pnd_Wk_Amtn = pnd_Wk_Amt__R_Field_28.newFieldInGroup("pnd_Wk_Amt_Pnd_Wk_Amtn", "#WK-AMTN", FieldType.NUMERIC, 14, 2);
        pnd_Amt = localVariables.newFieldInRecord("pnd_Amt", "#AMT", FieldType.STRING, 9);

        pnd_Amt__R_Field_29 = localVariables.newGroupInRecord("pnd_Amt__R_Field_29", "REDEFINE", pnd_Amt);
        pnd_Amt_Pnd_Amt_Pos = pnd_Amt__R_Field_29.newFieldArrayInGroup("pnd_Amt_Pnd_Amt_Pos", "#AMT-POS", FieldType.STRING, 1, new DbsArrayController(1, 
            9));
        pnd_In_Amt = localVariables.newFieldInRecord("pnd_In_Amt", "#IN-AMT", FieldType.STRING, 9);

        pnd_In_Amt__R_Field_30 = localVariables.newGroupInRecord("pnd_In_Amt__R_Field_30", "REDEFINE", pnd_In_Amt);
        pnd_In_Amt_Pnd_In_Amtn = pnd_In_Amt__R_Field_30.newFieldInGroup("pnd_In_Amt_Pnd_In_Amtn", "#IN-AMTN", FieldType.NUMERIC, 9, 2);
        pnd_Num = localVariables.newFieldArrayInRecord("pnd_Num", "#NUM", FieldType.STRING, 1, new DbsArrayController(1, 10));
        pnd_Rpt_Amt = localVariables.newFieldInRecord("pnd_Rpt_Amt", "#RPT-AMT", FieldType.NUMERIC, 9);
        pnd_Valid_Reversal_Reject_Codes = localVariables.newFieldArrayInRecord("pnd_Valid_Reversal_Reject_Codes", "#VALID-REVERSAL-REJECT-CODES", FieldType.STRING, 
            2, new DbsArrayController(1, 2));
        pnd_Ws_Trans_Cde = localVariables.newFieldInRecord("pnd_Ws_Trans_Cde", "#WS-TRANS-CDE", FieldType.STRING, 2);
        pnd_Rej_Field = localVariables.newFieldInRecord("pnd_Rej_Field", "#REJ-FIELD", FieldType.STRING, 18);
        pnd_J = localVariables.newFieldInRecord("pnd_J", "#J", FieldType.NUMERIC, 2);
        pnd_Ws_Report_Date = localVariables.newFieldInRecord("pnd_Ws_Report_Date", "#WS-REPORT-DATE", FieldType.STRING, 8);
        pnd_Rej_Rev = localVariables.newFieldInRecord("pnd_Rej_Rev", "#REJ-REV", FieldType.BOOLEAN, 1);
        pnd_Index = localVariables.newFieldInRecord("pnd_Index", "#INDEX", FieldType.NUMERIC, 1);
        ws_Descr_Dte = localVariables.newFieldInRecord("ws_Descr_Dte", "WS-DESCR-DTE", FieldType.STRING, 6);

        ws_Descr_Dte__R_Field_31 = localVariables.newGroupInRecord("ws_Descr_Dte__R_Field_31", "REDEFINE", ws_Descr_Dte);
        ws_Descr_Dte_Ws_Descr_12 = ws_Descr_Dte__R_Field_31.newFieldInGroup("ws_Descr_Dte_Ws_Descr_12", "WS-DESCR-12", FieldType.STRING, 2);
        ws_Descr_Dte_Ws_Descr_34 = ws_Descr_Dte__R_Field_31.newFieldInGroup("ws_Descr_Dte_Ws_Descr_34", "WS-DESCR-34", FieldType.STRING, 2);
        ws_Descr_Dte_Ws_Descr_56 = ws_Descr_Dte__R_Field_31.newFieldInGroup("ws_Descr_Dte_Ws_Descr_56", "WS-DESCR-56", FieldType.STRING, 2);
        ws_Descr_Dte1 = localVariables.newFieldInRecord("ws_Descr_Dte1", "WS-DESCR-DTE1", FieldType.STRING, 8);

        ws_Descr_Dte1__R_Field_32 = localVariables.newGroupInRecord("ws_Descr_Dte1__R_Field_32", "REDEFINE", ws_Descr_Dte1);
        ws_Descr_Dte1_Ws_Desc_Mm = ws_Descr_Dte1__R_Field_32.newFieldInGroup("ws_Descr_Dte1_Ws_Desc_Mm", "WS-DESC-MM", FieldType.STRING, 2);
        ws_Descr_Dte1_Ws_Desc_F1 = ws_Descr_Dte1__R_Field_32.newFieldInGroup("ws_Descr_Dte1_Ws_Desc_F1", "WS-DESC-F1", FieldType.STRING, 1);
        ws_Descr_Dte1_Ws_Desc_Dd = ws_Descr_Dte1__R_Field_32.newFieldInGroup("ws_Descr_Dte1_Ws_Desc_Dd", "WS-DESC-DD", FieldType.STRING, 2);
        ws_Descr_Dte1_Ws_Desc_F2 = ws_Descr_Dte1__R_Field_32.newFieldInGroup("ws_Descr_Dte1_Ws_Desc_F2", "WS-DESC-F2", FieldType.STRING, 1);
        ws_Descr_Dte1_Ws_Desc_Yy = ws_Descr_Dte1__R_Field_32.newFieldInGroup("ws_Descr_Dte1_Ws_Desc_Yy", "WS-DESC-YY", FieldType.STRING, 2);
        pnd_Descr_Date = localVariables.newFieldInRecord("pnd_Descr_Date", "#DESCR-DATE", FieldType.STRING, 8);

        pnd_Descr_Date__R_Field_33 = localVariables.newGroupInRecord("pnd_Descr_Date__R_Field_33", "REDEFINE", pnd_Descr_Date);
        pnd_Descr_Date_Pnd_Descr_Date_N = pnd_Descr_Date__R_Field_33.newFieldInGroup("pnd_Descr_Date_Pnd_Descr_Date_N", "#DESCR-DATE-N", FieldType.NUMERIC, 
            8);

        pnd_Descr_Date__R_Field_34 = localVariables.newGroupInRecord("pnd_Descr_Date__R_Field_34", "REDEFINE", pnd_Descr_Date);
        pnd_Descr_Date_Pnd_Yyyy1 = pnd_Descr_Date__R_Field_34.newFieldInGroup("pnd_Descr_Date_Pnd_Yyyy1", "#YYYY1", FieldType.NUMERIC, 4);
        pnd_Descr_Date_Pnd_Mm1 = pnd_Descr_Date__R_Field_34.newFieldInGroup("pnd_Descr_Date_Pnd_Mm1", "#MM1", FieldType.NUMERIC, 2);
        pnd_Descr_Date_Pnd_Dd1 = pnd_Descr_Date__R_Field_34.newFieldInGroup("pnd_Descr_Date_Pnd_Dd1", "#DD1", FieldType.NUMERIC, 2);
        pnd_Next_Month1 = localVariables.newFieldInRecord("pnd_Next_Month1", "#NEXT-MONTH1", FieldType.DATE);
        pnd_Next_X1 = localVariables.newFieldInRecord("pnd_Next_X1", "#NEXT-X1", FieldType.STRING, 8);

        pnd_Next_X1__R_Field_35 = localVariables.newGroupInRecord("pnd_Next_X1__R_Field_35", "REDEFINE", pnd_Next_X1);
        pnd_Next_X1_Pnd_Next_X1_N = pnd_Next_X1__R_Field_35.newFieldInGroup("pnd_Next_X1_Pnd_Next_X1_N", "#NEXT-X1-N", FieldType.NUMERIC, 8);
        pnd_Day_Of_Week1 = localVariables.newFieldInRecord("pnd_Day_Of_Week1", "#DAY-OF-WEEK1", FieldType.STRING, 1);
        pnd_Wk_Dte1 = localVariables.newFieldInRecord("pnd_Wk_Dte1", "#WK-DTE1", FieldType.DATE);
        pnd_Date1 = localVariables.newFieldInRecord("pnd_Date1", "#DATE1", FieldType.NUMERIC, 8);

        pnd_Date1__R_Field_36 = localVariables.newGroupInRecord("pnd_Date1__R_Field_36", "REDEFINE", pnd_Date1);
        pnd_Date1_Pnd_Date1_Alpha = pnd_Date1__R_Field_36.newFieldInGroup("pnd_Date1_Pnd_Date1_Alpha", "#DATE1-ALPHA", FieldType.STRING, 8);
        pnd_Date_K = localVariables.newFieldInRecord("pnd_Date_K", "#DATE-K", FieldType.NUMERIC, 8);
        pnd_Pymnt_Check_Amt = localVariables.newFieldInRecord("pnd_Pymnt_Check_Amt", "#PYMNT-CHECK-AMT", FieldType.NUMERIC, 9, 2);
        pnd_Ws_Err_Reason = localVariables.newFieldInRecord("pnd_Ws_Err_Reason", "#WS-ERR-REASON", FieldType.STRING, 35);
        pnd_Cmnt1 = localVariables.newFieldInRecord("pnd_Cmnt1", "#CMNT1", FieldType.STRING, 40);
        pnd_Wk_Dte_Eff = localVariables.newFieldInRecord("pnd_Wk_Dte_Eff", "#WK-DTE-EFF", FieldType.DATE);
        pnd_Wk_Dte_Eff_3 = localVariables.newFieldInRecord("pnd_Wk_Dte_Eff_3", "#WK-DTE-EFF-3", FieldType.DATE);
        pnd_Wk_Eff_3 = localVariables.newFieldInRecord("pnd_Wk_Eff_3", "#WK-EFF-3", FieldType.STRING, 8);

        pnd_Wk_Eff_3__R_Field_37 = localVariables.newGroupInRecord("pnd_Wk_Eff_3__R_Field_37", "REDEFINE", pnd_Wk_Eff_3);
        pnd_Wk_Eff_3_Pnd_Wk_Eff_3_N = pnd_Wk_Eff_3__R_Field_37.newFieldInGroup("pnd_Wk_Eff_3_Pnd_Wk_Eff_3_N", "#WK-EFF-3-N", FieldType.NUMERIC, 8);
        pnd_Wk_Dte1_Desc = localVariables.newFieldInRecord("pnd_Wk_Dte1_Desc", "#WK-DTE1-DESC", FieldType.DATE);
        pnd_Wk_Dte1_Desc_3 = localVariables.newFieldInRecord("pnd_Wk_Dte1_Desc_3", "#WK-DTE1-DESC-3", FieldType.DATE);
        pnd_Wk_Desc_3 = localVariables.newFieldInRecord("pnd_Wk_Desc_3", "#WK-DESC-3", FieldType.STRING, 8);

        pnd_Wk_Desc_3__R_Field_38 = localVariables.newGroupInRecord("pnd_Wk_Desc_3__R_Field_38", "REDEFINE", pnd_Wk_Desc_3);
        pnd_Wk_Desc_3_Pnd_Wk_Desc_3_N = pnd_Wk_Desc_3__R_Field_38.newFieldInGroup("pnd_Wk_Desc_3_Pnd_Wk_Desc_3_N", "#WK-DESC-3-N", FieldType.NUMERIC, 
            8);
        pnd_Date_D = localVariables.newFieldInRecord("pnd_Date_D", "#DATE-D", FieldType.NUMERIC, 8);
        pnd_Date_E = localVariables.newFieldInRecord("pnd_Date_E", "#DATE-E", FieldType.NUMERIC, 8);
        pnd_Date_K1 = localVariables.newFieldInRecord("pnd_Date_K1", "#DATE-K1", FieldType.NUMERIC, 8);
        error1 = localVariables.newFieldInRecord("error1", "ERROR1", FieldType.BOOLEAN, 1);
        error2 = localVariables.newFieldInRecord("error2", "ERROR2", FieldType.BOOLEAN, 1);
        error3 = localVariables.newFieldInRecord("error3", "ERROR3", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_read_Pymnt.reset();
        vw_read_Annot.reset();
        vw_mod_Annot.reset();

        localVariables.reset();
        pnd_Title.setInitialValue("Automated Annotation Report");
        pnd_Num.getValue(1).setInitialValue(0);
        pnd_Num.getValue(2).setInitialValue(1);
        pnd_Num.getValue(3).setInitialValue(2);
        pnd_Num.getValue(4).setInitialValue(3);
        pnd_Num.getValue(5).setInitialValue(4);
        pnd_Num.getValue(6).setInitialValue(5);
        pnd_Num.getValue(7).setInitialValue(6);
        pnd_Num.getValue(8).setInitialValue(7);
        pnd_Num.getValue(9).setInitialValue(8);
        pnd_Num.getValue(10).setInitialValue(9);
        pnd_Valid_Reversal_Reject_Codes.getValue(1).setInitialValue("27");
        pnd_Valid_Reversal_Reject_Codes.getValue(2).setInitialValue("37");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp604a() throws Exception
    {
        super("Fcpp604a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*  WRITE(2) 'THIS PROCESS IS TEMPORARILY SHUT DOWN' /* DBH1 /* DBH2
        //*  TERMINATE 0                                      /* DBH1 /* DBH2
        //* *-------------------------------------------------------------------
        //*                                                                                                                                                               //Natural: FORMAT ( 1 ) LS = 133 PS = 66;//Natural: FORMAT ( 2 ) LS = 133 PS = 66;//Natural: FORMAT ( 3 ) LS = 133 PS = 66
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 39T 'Consolidated Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 14 ) ( EM = ZZ,ZZ9 ) / *PROGRAM 39T #TITLE / 'Contract    Eff Date   Customer Name                      Description' '                                    Amount' / '-------------------------------------------------------------------' '---------------------------------------------' /
        //* *-------------------
        //*  **************************************************************/*CTS2
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 3 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 39T 'CONSOLIDATED PAYMENT SYSTEM ' 120T 'PAGE:' *PAGE-NUMBER ( 14 ) ( EM = ZZ,ZZ9 ) / *PROGRAM 39T 'ACH PAYMENT ANNOTATION ERROR REPORT' / 'CONTRACT    REPORT-DT EFF-DATE  DESC-DT  CUSTOMER NAME           ' 'TC       RPT-AMT      ERROR REASON           ' / '-------------------------------------------------------------------' '----------------------------------------------------------------' /
        //* *-------------------
        //*  ************************************************************/*CTS2
        pnd_Today_Pnd_Todayn.setValue(Global.getDATN());                                                                                                                  //Natural: MOVE *DATN TO #TODAYN
        //* *
        READWORK01:                                                                                                                                                       //Natural: READ WORK 1 INPUT-REC
        while (condition(getWorkFiles().read(1, input_Rec)))
        {
            //*  CTS1
            pnd_Rej_Rev.reset();                                                                                                                                          //Natural: RESET #REJ-REV
            //*                                                    /* CTS STARTS
            if (condition(input_Rec_Hdr_Block_Lit.equals("BLOCK NO:")))                                                                                                   //Natural: IF HDR-BLOCK-LIT = 'BLOCK NO:'
            {
                pnd_Ws_Report_Date.setValue(input_Rec_Hdr_Date);                                                                                                          //Natural: MOVE HDR-DATE TO #WS-REPORT-DATE
            }                                                                                                                                                             //Natural: END-IF
            //*                                                    /* CTS ENDS
            if (condition(input_Rec_Dtl1_Co_Name_Lit.equals("CO-NAME:")))                                                                                                 //Natural: IF DTL1-CO-NAME-LIT = 'CO-NAME:'
            {
                //*                                                    /* CTS STARTS
                //*    IF  DTL1-DESCR    = 'RDP RECLA ' OR
                //*        DTL1-DESCR    = 'RECLAIM   ' OR
                //*        DTL1-DESCR    = 'REVERSAL  ' OR
                //*        DTL1-DESCR    = 'RDP REVER '                  /* JWO1 09/2012
                //*      ASSIGN #CONTINUE := FALSE
                //*      RESET WS-CUST-ID
                //*    ELSE
                //*                                                    /* CTS ENDS
                //*    ASSIGN #CONTINUE := TRUE            /* COMMENTED OUT CTS2
                ws_Descr.setValue(input_Rec_Dtl1_Descr);                                                                                                                  //Natural: MOVE DTL1-DESCR TO WS-DESCR
                //*  --------------------------------------------- /*CTS2
                if (condition(input_Rec_Dtl1_Desc_Dte_Lit.equals("DESCR-DATE: ")))                                                                                        //Natural: IF DTL1-DESC-DTE-LIT = 'DESCR-DATE: '
                {
                    //*     WRITE 'DTL1-DESCR-DTE' DTL1-DESCR-DTE
                    if (condition(DbsUtil.maskMatches(input_Rec_Dtl1_Descr_Dte,"YYMMDD")))                                                                                //Natural: IF DTL1-DESCR-DTE EQ MASK ( YYMMDD )
                    {
                        ws_Descr_Dte.setValue(input_Rec_Dtl1_Descr_Dte);                                                                                                  //Natural: MOVE DTL1-DESCR-DTE TO WS-DESCR-DTE
                        ws_Descr_Dte1_Ws_Desc_Yy.setValue(ws_Descr_Dte_Ws_Descr_12);                                                                                      //Natural: MOVE WS-DESCR-12 TO WS-DESC-YY
                        ws_Descr_Dte1_Ws_Desc_Mm.setValue(ws_Descr_Dte_Ws_Descr_34);                                                                                      //Natural: MOVE WS-DESCR-34 TO WS-DESC-MM
                        ws_Descr_Dte1_Ws_Desc_Dd.setValue(ws_Descr_Dte_Ws_Descr_56);                                                                                      //Natural: MOVE WS-DESCR-56 TO WS-DESC-DD
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(DbsUtil.maskMatches(input_Rec_Dtl1_Descr_Dte,"MMYYDD")))                                                                            //Natural: IF DTL1-DESCR-DTE EQ MASK ( MMYYDD )
                        {
                            ws_Descr_Dte.setValue(input_Rec_Dtl1_Descr_Dte);                                                                                              //Natural: MOVE DTL1-DESCR-DTE TO WS-DESCR-DTE
                            ws_Descr_Dte1_Ws_Desc_Yy.setValue(ws_Descr_Dte_Ws_Descr_34);                                                                                  //Natural: MOVE WS-DESCR-34 TO WS-DESC-YY
                            ws_Descr_Dte1_Ws_Desc_Mm.setValue(ws_Descr_Dte_Ws_Descr_12);                                                                                  //Natural: MOVE WS-DESCR-12 TO WS-DESC-MM
                            ws_Descr_Dte1_Ws_Desc_Dd.setValue(ws_Descr_Dte_Ws_Descr_56);                                                                                  //Natural: MOVE WS-DESCR-56 TO WS-DESC-DD
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    ws_Descr_Dte1_Ws_Desc_F1.setValue("/");                                                                                                               //Natural: MOVE '/' TO WS-DESC-F1
                    ws_Descr_Dte1_Ws_Desc_F2.setValue("/");                                                                                                               //Natural: MOVE '/' TO WS-DESC-F2
                    //*      WRITE '=' WS-DESCR-DTE1
                    pnd_Wk_Dte1.setValueEdited(new ReportEditMask("MM/DD/YY"),ws_Descr_Dte1);                                                                             //Natural: MOVE EDITED WS-DESCR-DTE1 TO #WK-DTE1 ( EM = MM/DD/YY )
                    //*  CTS3 */
                    pnd_Wk_Dte1_Desc.setValue(pnd_Wk_Dte1);                                                                                                               //Natural: MOVE #WK-DTE1 TO #WK-DTE1-DESC
                    //*      #WK-DTE1-DESC-3 := #WK-DTE1-DESC - 3
                    pnd_Wk_Dte1_Desc_3.compute(new ComputeParameters(false, pnd_Wk_Dte1_Desc_3), pnd_Wk_Dte1_Desc.subtract(4));                                           //Natural: ASSIGN #WK-DTE1-DESC-3 := #WK-DTE1-DESC - 4
                    pnd_Wk_Desc_3.setValueEdited(pnd_Wk_Dte1_Desc_3,new ReportEditMask("YYYYMMDD"));                                                                      //Natural: MOVE EDITED #WK-DTE1-DESC-3 ( EM = YYYYMMDD ) TO #WK-DESC-3
                    pnd_Descr_Date.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "20", ws_Descr_Dte));                                                         //Natural: COMPRESS '20' WS-DESCR-DTE INTO #DESCR-DATE LEAVING NO
                    pnd_Next_Month1.compute(new ComputeParameters(false, pnd_Next_Month1), pnd_Wk_Dte1.add(3));                                                           //Natural: ASSIGN #NEXT-MONTH1 := #WK-DTE1 + 3
                    pnd_Next_X1.setValueEdited(pnd_Next_Month1,new ReportEditMask("YYYYMMDD"));                                                                           //Natural: MOVE EDITED #NEXT-MONTH1 ( EM = YYYYMMDD ) TO #NEXT-X1
                }                                                                                                                                                         //Natural: END-IF
                //*  ----------------------------------------------------- /*CTS2
                //*    END-IF                                          /* CTS
            }                                                                                                                                                             //Natural: END-IF
            //*  ---------------------------------------------------/*CTS2
            //*  IF #CONTINUE
            //*    IGNORE
            //*  ELSE
            //*    ESCAPE TOP
            //*  END-IF
            //*  -------------------------------------/* COMMENTED OUT CTS2
            //*  IF  HDR-NAME-LIT = 'CUSTOMER-NAME---------  '
            //*     ESCAPE TOP
            //*  END-IF
            if (condition(input_Rec_Dtl2_Ppcn.notEquals("    ")))                                                                                                         //Natural: IF DTL2-PPCN NE '    '
            {
                ws_Cust_Id.setValue(input_Rec_Dtl2_Cust_Id);                                                                                                              //Natural: MOVE DTL2-CUST-ID TO WS-CUST-ID
                pnd_Ws_Cust_Name.setValue(input_Rec_Dtl2_Name);                                                                                                           //Natural: MOVE DTL2-NAME TO #WS-CUST-NAME
                pnd_Ws_Amount.setValue(input_Rec_Dtl2_Amount);                                                                                                            //Natural: MOVE DTL2-AMOUNT TO #WS-AMOUNT
                //* *   COMPRESS #AMT-BEF-DEC #AMT-AFT-DEC INTO #WK-AMT LEAVE NO
                //* *   ASSIGN  #WK-AMTN = VAL(DTL2-AMOUNT)
                //*     WRITE '=' #WK-AMT '=' #WK-AMTN
                //*     MOVE 16 TO #A
                //*     FOR #I 15 1   STEP -1
                //*       IF #WS-POS(#I) = #NUM(*)
                //*          SUBTRACT 1 FROM #A
                //*          MOVE #WS-POS(#I) TO #AMT-POS(#A)
                //*       END-IF
                //*     END-FOR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(input_Rec_Dtl2_Tc.notEquals(" ")))                                                                                                              //Natural: IF DTL2-TC NE ' '
            {
                pnd_Ws_Cust_Name.setValue(input_Rec_Dtl2_Name);                                                                                                           //Natural: MOVE DTL2-NAME TO #WS-CUST-NAME
                //*     COMPRESS #AMT-BEF-DEC #AMT-AFT-DEC INTO #WK-AMT LEAVE NO
                pnd_Ws_Amount.setValue(input_Rec_Dtl2_Amount);                                                                                                            //Natural: MOVE DTL2-AMOUNT TO #WS-AMOUNT
                //*  CTS
                pnd_Ws_Trans_Cde.setValue(input_Rec_Dtl2_Tc);                                                                                                             //Natural: MOVE DTL2-TC TO #WS-TRANS-CDE
                pnd_Amt.reset();                                                                                                                                          //Natural: RESET #AMT
                //*     WRITE 'BEGIN ' '=' #AMT
                pnd_A.setValue(10);                                                                                                                                       //Natural: MOVE 10 TO #A
                FOR01:                                                                                                                                                    //Natural: FOR #I 15 1 STEP -1
                for (pnd_I.setValue(15); condition(pnd_I.greaterOrEqual(1)); pnd_I.nsubtract(1))
                {
                    //*       WRITE '=' #WS-POS(#I) '=' #I '=' #A
                    if (condition(pnd_A.lessOrEqual(1)))                                                                                                                  //Natural: IF #A LE 1
                    {
                        if (condition(true)) break;                                                                                                                       //Natural: ESCAPE BOTTOM
                        getReports().write(0, "ESCAPING CONTRACT",ws_Cust_Id);                                                                                            //Natural: WRITE 'ESCAPING CONTRACT' WS-CUST-ID
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pnd_Ws_Amount_Pnd_Ws_Pos.getValue(pnd_I).equals(pnd_Num.getValue("*"))))                                                                //Natural: IF #WS-POS ( #I ) = #NUM ( * )
                    {
                        pnd_A.nsubtract(1);                                                                                                                               //Natural: SUBTRACT 1 FROM #A
                        //* **       WRITE 'MOVING ' #WS-POS(#I)
                        pnd_Amt_Pnd_Amt_Pos.getValue(pnd_A).setValue(pnd_Ws_Amount_Pnd_Ws_Pos.getValue(pnd_I));                                                           //Natural: MOVE #WS-POS ( #I ) TO #AMT-POS ( #A )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-FOR
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    WRITE '=' #A
                if (condition(pnd_A.greater(9)))                                                                                                                          //Natural: IF #A GT 9
                {
                    pnd_A.setValue(9);                                                                                                                                    //Natural: MOVE 9 TO #A
                }                                                                                                                                                         //Natural: END-IF
                //*    EXAMINE #AMT FOR ' ' REPLACE '0'
                if (condition(pnd_A.greater(1)))                                                                                                                          //Natural: IF #A GT 1
                {
                    pnd_A.nsubtract(1);                                                                                                                                   //Natural: SUBTRACT 1 FROM #A
                    FOR02:                                                                                                                                                //Natural: FOR #I 1 #A
                    for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_A)); pnd_I.nadd(1))
                    {
                        pnd_Amt_Pnd_Amt_Pos.getValue(pnd_I).setValue("0");                                                                                                //Natural: MOVE '0' TO #AMT-POS ( #I )
                    }                                                                                                                                                     //Natural: END-FOR
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                pnd_In_Amt.setValue(pnd_Amt);                                                                                                                             //Natural: MOVE #AMT TO #IN-AMT
                //*    WRITE '='  #AMT #IN-AMT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(input_Rec_Dtl3_Eff_Dte_Lit.equals("EFF. ENTRY DATE: ")))                                                                                        //Natural: IF DTL3-EFF-DTE-LIT = 'EFF. ENTRY DATE: '
            {
                ws_Eff_Dte.setValue(input_Rec_Dtl3_Eff_Dte);                                                                                                              //Natural: MOVE DTL3-EFF-DTE TO WS-EFF-DTE
                pnd_Wk_Dte.setValueEdited(new ReportEditMask("MM/DD/YY"),ws_Eff_Dte);                                                                                     //Natural: MOVE EDITED WS-EFF-DTE TO #WK-DTE ( EM = MM/DD/YY )
                pnd_Date_X.setValueEdited(pnd_Wk_Dte,new ReportEditMask("YYYYMMDD"));                                                                                     //Natural: MOVE EDITED #WK-DTE ( EM = YYYYMMDD ) TO #DATE-X
                //*  CTS3 */
                pnd_Wk_Dte_Eff.setValue(pnd_Wk_Dte);                                                                                                                      //Natural: MOVE #WK-DTE TO #WK-DTE-EFF
                //*    #WK-DTE-EFF-3 := #WK-DTE-EFF - 3
                pnd_Wk_Dte_Eff_3.compute(new ComputeParameters(false, pnd_Wk_Dte_Eff_3), pnd_Wk_Dte_Eff.subtract(4));                                                     //Natural: ASSIGN #WK-DTE-EFF-3 := #WK-DTE-EFF - 4
                pnd_Wk_Eff_3.setValueEdited(pnd_Wk_Dte_Eff_3,new ReportEditMask("YYYYMMDD"));                                                                             //Natural: MOVE EDITED #WK-DTE-EFF-3 ( EM = YYYYMMDD ) TO #WK-EFF-3
                //*  ----------------------------------------- START -----
                pnd_Next_Month.compute(new ComputeParameters(false, pnd_Next_Month), pnd_Wk_Dte.add(3));                                                                  //Natural: ASSIGN #NEXT-MONTH := #WK-DTE + 3
                pnd_Next_X.setValueEdited(pnd_Next_Month,new ReportEditMask("YYYYMMDD"));                                                                                 //Natural: MOVE EDITED #NEXT-MONTH ( EM = YYYYMMDD ) TO #NEXT-X
                //*  ----------------------------------------- END ------- /* JWO 09/2012
                ws_Rsn_Cde.setValue(input_Rec_Dtl3_Rsn_Cde);                                                                                                              //Natural: MOVE DTL3-RSN-CDE TO WS-RSN-CDE
                ws_Rsn_Desc.setValue(input_Rec_Dtl3_Rsn_Desc);                                                                                                            //Natural: MOVE DTL3-RSN-DESC TO WS-RSN-DESC
                //*                                                    /* CTS STARTS
                //*  CTS1
                if (condition(pnd_Ws_Trans_Cde.equals(pnd_Valid_Reversal_Reject_Codes.getValue("*"))))                                                                    //Natural: IF #WS-TRANS-CDE EQ #VALID-REVERSAL-REJECT-CODES ( * )
                {
                    pnd_Rej_Rev.setValue(true);                                                                                                                           //Natural: ASSIGN #REJ-REV := TRUE
                    if (condition(ws_Descr.equals("ANNUITY") || ws_Descr.equals("RDP REVER") || ws_Descr.equals("REVERSAL")))                                             //Natural: IF WS-DESCR EQ 'ANNUITY' OR EQ 'RDP REVER' OR EQ 'REVERSAL'
                    {
                        pnd_Rej_Field.setValue("REVERSAL DECLINED");                                                                                                      //Natural: ASSIGN #REJ-FIELD := 'REVERSAL DECLINED'
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  DASRAH
                        if (condition(ws_Descr.equals("RDP RECLA") || ws_Descr.equals("RECLAIM") || ws_Descr.equals("RDP RETRY") || ws_Descr.equals("RDP2 RETRY")))       //Natural: IF WS-DESCR EQ 'RDP RECLA' OR EQ 'RECLAIM' OR EQ 'RDP RETRY' OR EQ 'RDP2 RETRY'
                        {
                            pnd_Rej_Field.setValue("RECLAIM REJECTED");                                                                                                   //Natural: ASSIGN #REJ-FIELD := 'RECLAIM REJECTED'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*                                                     /* CTS ENDS
                //*  ----------------------------------------- START ----- /* JWO 09/2012
                //*    COMPRESS DTL3-RSN-DESC DTL3-EFF2 DTL3-SPC2 DTL3-EFF3 DTL3-EFF-DTE
                if (condition(pnd_Ws_Trans_Cde.equals(pnd_Valid_Reversal_Reject_Codes.getValue("*"))))                                                                    //Natural: IF #WS-TRANS-CDE EQ #VALID-REVERSAL-REJECT-CODES ( * )
                {
                    //*  CTS
                    pnd_Cmnt.setValue(DbsUtil.compress(pnd_Rej_Field, pnd_Ws_Report_Date, input_Rec_Dtl3_Rsn_Desc));                                                      //Natural: COMPRESS #REJ-FIELD #WS-REPORT-DATE DTL3-RSN-DESC INTO #CMNT
                    //*  CTS
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Cmnt.setValue(input_Rec_Dtl3_Rsn_Desc);                                                                                                           //Natural: MOVE DTL3-RSN-DESC TO #CMNT
                    //*  CTS
                }                                                                                                                                                         //Natural: END-IF
                //* CTS2
                pnd_Cmnt1.setValue(pnd_Cmnt);                                                                                                                             //Natural: MOVE #CMNT TO #CMNT1
                //*  ----------------------------------------- END ------- /* JWO 09/2012
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ws_Cust_Id.notEquals(" ") && ws_Eff_Dte.notEquals(" ") && ws_Rsn_Cde.getSubstring(1,1).equals("R") && ws_Rsn_Desc.notEquals("   ")))            //Natural: IF WS-CUST-ID NE ' ' AND WS-EFF-DTE NE ' ' AND SUBSTR ( WS-RSN-CDE,1,1 ) = 'R' AND WS-RSN-DESC NE '   '
            {
                //* *   WS-CUST-CNTRCT NE 'CUSTOMER' AND
                //*    WRITE 'SELECT FOR UPDATE'
                //*    WRITE '=' WS-CUST-ID
                //*    WRITE '=' WS-EFF-DTE
                //*    WRITE '=' #DATE-X
                //*    WRITE '=' WS-RSN-CDE
                //*    WRITE '=' WS-RSN-DESC
                //*    WRITE '=' WS-DESCR
                //*    WRITE '=' #CMNT
                //*    WRITE '='
                pnd_No_Payment.setValue(true);                                                                                                                            //Natural: ASSIGN #NO-PAYMENT := TRUE
                error1.setValue(false);                                                                                                                                   //Natural: ASSIGN ERROR1 := FALSE
                error2.setValue(false);                                                                                                                                   //Natural: ASSIGN ERROR2 := FALSE
                error3.setValue(false);                                                                                                                                   //Natural: ASSIGN ERROR3 := FALSE
                payment_Key.reset();                                                                                                                                      //Natural: RESET PAYMENT-KEY
                //*  FAO 1/6/15
                pnd_Date.compute(new ComputeParameters(false, pnd_Date), DbsField.subtract(100000000,pnd_Next_X_Pnd_Next_X_N));                                           //Natural: COMPUTE #DATE = 100000000 - #NEXT-X-N
                //*  FAO 1/6/15 /*CTS2
                pnd_Date1.compute(new ComputeParameters(false, pnd_Date1), DbsField.subtract(100000000,pnd_Next_X1_Pnd_Next_X1_N));                                       //Natural: COMPUTE #DATE1 = 100000000 - #NEXT-X1-N
                //* CTS2
                pnd_Date_D.compute(new ComputeParameters(false, pnd_Date_D), DbsField.subtract(100000000,pnd_Wk_Desc_3_Pnd_Wk_Desc_3_N));                                 //Natural: COMPUTE #DATE-D = 100000000 - #WK-DESC-3-N
                //* CTS2
                pnd_Date_E.compute(new ComputeParameters(false, pnd_Date_E), DbsField.subtract(100000000,pnd_Wk_Eff_3_Pnd_Wk_Eff_3_N));                                   //Natural: COMPUTE #DATE-E = 100000000 - #WK-EFF-3-N
                payment_Key_Paykey_Ppcn.setValue(ws_Cust_Id_Ws_Cust_Cntrct);                                                                                              //Natural: MOVE WS-CUST-CNTRCT TO PAYKEY-PPCN
                //* *  MOVE WS-CUST-ID     TO PAYKEY-PPCN
                //* CTS1
                if (condition(pnd_Rej_Rev.getBoolean()))                                                                                                                  //Natural: IF #REJ-REV
                {
                    //* CTS1
                    payment_Key_Paykey_Invrse_Dte.setValue(pnd_Date1);                                                                                                    //Natural: MOVE #DATE1 TO PAYKEY-INVRSE-DTE
                    //* CTS1
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    payment_Key_Paykey_Invrse_Dte.setValue(pnd_Date);                                                                                                     //Natural: MOVE #DATE TO PAYKEY-INVRSE-DTE
                    //* CTS1
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM PROCESS-REJECT
                sub_Process_Reject();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                ws_Eff_Dte.reset();                                                                                                                                       //Natural: RESET WS-EFF-DTE WS-CUST-ID WS-RSN-CDE WS-RSN-DESC
                ws_Cust_Id.reset();
                ws_Rsn_Cde.reset();
                ws_Rsn_Desc.reset();
                //*  CTS1
                pnd_Rej_Rev.reset();                                                                                                                                      //Natural: RESET #REJ-REV WS-DESCR-DTE1 ERROR1 ERROR2 ERROR3
                ws_Descr_Dte1.reset();
                error1.reset();
                error2.reset();
                error3.reset();
                //*  DASRAH
                pnd_Rej_Field.reset();                                                                                                                                    //Natural: RESET #REJ-FIELD DTL3-RSN-DESC #CMNT
                input_Rec_Dtl3_Rsn_Desc.reset();
                pnd_Cmnt.reset();
                pnd_Added.setValue(false);                                                                                                                                //Natural: ASSIGN #ADDED := FALSE
                pnd_Updated.setValue(false);                                                                                                                              //Natural: ASSIGN #UPDATED := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        getReports().skip(2, 2);                                                                                                                                          //Natural: SKIP ( 2 ) 2
        getReports().write(2, NEWLINE,NEWLINE,"TOTAL COUNT----> ",pnd_Tot);                                                                                               //Natural: WRITE ( 2 ) // 'TOTAL COUNT----> ' #TOT
        if (Global.isEscape()) return;
        //*  --------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-REJECT
        //*  ---------------------------------------------------
        //*  --------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-ANNOT
        //*  WRITE 'FOUND ANNOTATION '
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ADD-ANNOT
        //*  ----------------------------------------------------------------------
        //*  ----------------------------------------------------------------------
    }
    private void sub_Process_Reject() throws Exception                                                                                                                    //Natural: PROCESS-REJECT
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------
        //*  READ READ-PYMNT BY PPCN-INV-ORGN-PRCSS-INST STARTING FROM WS-CUST-ID
        vw_read_Pymnt.startDatabaseRead                                                                                                                                   //Natural: READ READ-PYMNT BY PPCN-INV-ORGN-PRCSS-INST STARTING FROM PAYMENT-KEY
        (
        "RD1",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", payment_Key, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") }
        );
        RD1:
        while (condition(vw_read_Pymnt.readNextRow("RD1")))
        {
            if (condition(read_Pymnt_Cntrct_Ppcn_Nbr.notEquals(ws_Cust_Id_Ws_Cust_Cntrct)))                                                                               //Natural: IF CNTRCT-PPCN-NBR NE WS-CUST-CNTRCT
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  ----------------------------------------- START ----- /* JWO 09/2012
            //*  IF PYMNT-PAY-TYPE-REQ-IND NE 2 OR NOTUSED1 NE ' ' /*CTS2
            if (condition(read_Pymnt_Pymnt_Pay_Type_Req_Ind.notEquals(2)))                                                                                                //Natural: IF PYMNT-PAY-TYPE-REQ-IND NE 2
            {
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            //*  ----------------------------------------- END ------- /* JWO 09/2012
            //*  ---------COMMENTED OUT ------------------ /CTS2
            //*  MOVE CNTRCT-PAYEE-CDE TO #WS-PAYEE-CDE
            //*  WRITE 'READING  ' CNTRCT-PPCN-NBR ' '
            //*    CNTRCT-INVRSE-DTE ' '
            //*    #DATE  ' '
            //*    PYMNT-CHECK-DTE ' '
            //*    CNTRCT-PAYEE-CDE ' '
            //*  WRITE '='         PYMNT-CHECK-AMT
            pnd_In_Amt.setValue(pnd_Amt);                                                                                                                                 //Natural: MOVE #AMT TO #IN-AMT
            //*  WRITE '=' #IN-AMTN
            //*  ***********************************************************
            //*  FIND THE RIGHT PAYMENT
            //*  ------------------DATE LOGIC-------------------------------/*CTS1
            if (condition(pnd_Rej_Rev.getBoolean()))                                                                                                                      //Natural: IF #REJ-REV
            {
                pnd_Date_K.setValue(pnd_Date1);                                                                                                                           //Natural: MOVE #DATE1 TO #DATE-K
                pnd_Date_K1.setValue(pnd_Date_D);                                                                                                                         //Natural: MOVE #DATE-D TO #DATE-K1
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Date_K.setValue(pnd_Date);                                                                                                                            //Natural: MOVE #DATE TO #DATE-K
                pnd_Date_K1.setValue(pnd_Date_E);                                                                                                                         //Natural: MOVE #DATE-E TO #DATE-K1
            }                                                                                                                                                             //Natural: END-IF
            if (condition((read_Pymnt_Cntrct_Invrse_Dte.greaterOrEqual(pnd_Date_K)) && (read_Pymnt_Cntrct_Invrse_Dte.lessOrEqual(pnd_Date_K1))))                          //Natural: IF ( CNTRCT-INVRSE-DTE GE #DATE-K ) AND ( CNTRCT-INVRSE-DTE LE #DATE-K1 )
            {
                //*  -----------------------------------------------------------/*CTS1
                //*  IF  CNTRCT-INVRSE-DTE GE #DATE   /*COMMENTED OUT CTS1
                //*  WRITE 'FOUND   ' CNTRCT-PPCN-NBR ' '
                //*                   CNTRCT-INVRSE-DTE ' '
                //*                   #DATE-K1 ' ' #DATE-K ' '
                //*                   PYMNT-CHECK-DTE ' '
                //*                   #IN-AMTN ' '
                //*                   PYMNT-CHECK-AMT ' '
                //*  COMMENTED OUT----------------------------------------------/*CTS1
                //*  INPUT FILE DOES NOT HAVE PAYEE CODE
                if (condition(ws_Cust_Id_Ws_Payee_Cde.equals(" ")))                                                                                                       //Natural: IF WS-PAYEE-CDE = ' '
                {
                    //*    IGNORE
                    //* CTS2
                    if (condition(pnd_In_Amt_Pnd_In_Amtn.notEquals(read_Pymnt_Pymnt_Check_Amt)))                                                                          //Natural: IF #IN-AMTN NE PYMNT-CHECK-AMT
                    {
                        error1.setValue(true);                                                                                                                            //Natural: ASSIGN ERROR1 := TRUE
                        //* CTS2
                        pnd_Ws_Err_Reason.setValue("PAYMENT AMOUNT IS NOT MATCHING");                                                                                     //Natural: MOVE 'PAYMENT AMOUNT IS NOT MATCHING' TO #WS-ERR-REASON
                        //* CTS2
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(read_Pymnt_Cntrct_Payee_Cde.notEquals(ws_Cust_Id_Ws_Payee_Cde)))                                                                        //Natural: IF CNTRCT-PAYEE-CDE NE WS-PAYEE-CDE
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                //*    WRITE 'WILL PROCESS THIS ONE'
                //*  -------------------CTS2 STARTS ------------------------------/*CTS2
                if (condition(pnd_In_Amt_Pnd_In_Amtn.notEquals(read_Pymnt_Pymnt_Check_Amt)))                                                                              //Natural: IF #IN-AMTN NE PYMNT-CHECK-AMT
                {
                    error1.setValue(true);                                                                                                                                //Natural: ASSIGN ERROR1 := TRUE
                    pnd_Ws_Err_Reason.setValue("PAYMENT AMOUNT IS NOT MATCHING");                                                                                         //Natural: MOVE 'PAYMENT AMOUNT IS NOT MATCHING' TO #WS-ERR-REASON
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                if (condition(read_Pymnt_Notused1.notEquals(" ")))                                                                                                        //Natural: IF NOTUSED1 NE ' '
                {
                    error2.setValue(true);                                                                                                                                //Natural: ASSIGN ERROR2 := TRUE
                    pnd_Ws_Err_Reason.setValue("PAYMENT ALREADY IN REJECTED STATUS");                                                                                     //Natural: MOVE 'PAYMENT ALREADY IN REJECTED STATUS' TO #WS-ERR-REASON
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*  ---------------------CTS2 ENDS ----------------------------/*CTS2
                //*  SET UP THE KEY TO ANNOT RECORD
                //*    MOVE WS-CUST-ID          TO KEY-PPCN-NBR
                annot_Key_Key_Ppcn_Nbr.setValue(read_Pymnt_Cntrct_Ppcn_Nbr);                                                                                              //Natural: MOVE CNTRCT-PPCN-NBR TO KEY-PPCN-NBR
                annot_Key_Key_Invrse_Dte.setValue(read_Pymnt_Cntrct_Invrse_Dte);                                                                                          //Natural: MOVE CNTRCT-INVRSE-DTE TO KEY-INVRSE-DTE
                annot_Key_Key_Orgn_Cde.setValue(read_Pymnt_Cntrct_Orgn_Cde);                                                                                              //Natural: MOVE CNTRCT-ORGN-CDE TO KEY-ORGN-CDE
                annot_Key_Key_Seq_Nbr.setValue(read_Pymnt_Pymnt_Prcss_Seq_Nbr);                                                                                           //Natural: MOVE PYMNT-PRCSS-SEQ-NBR TO KEY-SEQ-NBR
                pnd_Save_Isn.setValue(vw_read_Pymnt.getAstISN("RD1"));                                                                                                    //Natural: MOVE *ISN ( RD1. ) TO #SAVE-ISN
                //*    WRITE '=' ANNOT-KEY
                pnd_No_Payment.setValue(false);                                                                                                                           //Natural: ASSIGN #NO-PAYMENT := FALSE
                                                                                                                                                                          //Natural: PERFORM UPDATE-ANNOT
                sub_Update_Annot();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD1"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(pnd_No_Annot.getBoolean()))                                                                                                                 //Natural: IF #NO-ANNOT
                {
                                                                                                                                                                          //Natural: PERFORM ADD-ANNOT
                    sub_Add_Annot();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pnd_Updated.getBoolean() || pnd_Added.getBoolean()))                                                                                        //Natural: IF #UPDATED OR #ADDED
                {
                    PND_PND_L6720:                                                                                                                                        //Natural: GET READ-PYMNT #SAVE-ISN
                    vw_read_Pymnt.readByID(pnd_Save_Isn.getLong(), "PND_PND_L6720");
                    read_Pymnt_Pymnt_Annot_Ind.setValue("Y");                                                                                                             //Natural: MOVE 'Y' TO READ-PYMNT.PYMNT-ANNOT-IND
                    //*  -------------------------------------------------/* CTS STARTS
                    //*  ------- SKIP NOTUSED1 FIELD UPDATE FOR TRANS CODES 27 / 37 ---*****
                    DbsUtil.examine(new ExamineSource(pnd_Valid_Reversal_Reject_Codes.getValue("*")), new ExamineSearch(pnd_Ws_Trans_Cde), new ExamineGivingIndex(pnd_J)); //Natural: EXAMINE #VALID-REVERSAL-REJECT-CODES ( * ) FOR #WS-TRANS-CDE GIVING INDEX #J
                    if (condition((pnd_J.equals(getZero()))))                                                                                                             //Natural: IF ( #J EQ 0 )
                    {
                        read_Pymnt_Notused1.setValue("EFT REJ");                                                                                                          //Natural: MOVE 'EFT REJ' TO READ-PYMNT.NOTUSED1
                    }                                                                                                                                                     //Natural: END-IF
                    error1.reset();                                                                                                                                       //Natural: RESET ERROR1 ERROR2 ERROR3
                    error2.reset();
                    error3.reset();
                    vw_read_Pymnt.updateDBRow("PND_PND_L6720");                                                                                                           //Natural: UPDATE ( ##L6720. )
                    //*  JWO1 02/2013
                    DbsUtil.callnat(Cponisns.class , getCurrentProcessState(), pnd_Save_Isn);                                                                             //Natural: CALLNAT 'CPONISNS' #SAVE-ISN
                    if (condition(Global.isEscape())) return;
                    getCurrentProcessState().getDbConv().dbCommit();                                                                                                      //Natural: END TRANSACTION
                    pnd_Tot.nadd(1);                                                                                                                                      //Natural: ADD 1 TO #TOT
                    //*      WRITE 'UPDATED PAYMENT RECORD'
                    getReports().write(2, ws_Cust_Id,new ColumnSpacing(2),ws_Eff_Dte,new ColumnSpacing(2),pnd_Ws_Cust_Name,new ColumnSpacing(2),pnd_Cmnt,new              //Natural: WRITE ( 2 ) WS-CUST-ID 2X WS-EFF-DTE 2X #WS-CUST-NAME 2X #CMNT 2X #IN-AMTN ( EM = Z,ZZZ,ZZ9.99 )
                        ColumnSpacing(2),pnd_In_Amt_Pnd_In_Amtn, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("RD1"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("RD1"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *          2X  #STATUS
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
                //* CTS2
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* CTS2
                if (condition((error1.getBoolean() || error2.getBoolean())))                                                                                              //Natural: IF ( ERROR1 OR ERROR2 )
                {
                    //* CTS2
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //* CTS2
                    //* CTS2
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    error3.setValue(true);                                                                                                                                //Natural: ASSIGN ERROR3 := TRUE
                    //* CTS2
                    pnd_Ws_Err_Reason.setValue("PAYMENT DATE IS NOT MATCHING");                                                                                           //Natural: MOVE 'PAYMENT DATE IS NOT MATCHING' TO #WS-ERR-REASON
                    //* CTS2
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                    //* CTS2
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  IF WS-CUST-CNTRCT = 'CUSTOMER'
        //*     MOVE 'No Contract #' TO #STATUS
        //*      WRITE (2) WS-CUST-ID
        //*            2X  WS-EFF-DTE
        //*            2X  #WS-CUST-NAME
        //*            2X  #CMNT
        //*            2X  #STATUS
        //*     ESCAPE ROUTINE
        //*  END-IF
        if (condition(pnd_No_Payment.getBoolean()))                                                                                                                       //Natural: IF #NO-PAYMENT
        {
            pnd_Status.setValue("No Payment Rec");                                                                                                                        //Natural: MOVE 'No Payment Rec' TO #STATUS
            if (condition(ws_Cust_Id.equals("CUSTOMER-I")))                                                                                                               //Natural: IF WS-CUST-ID = 'CUSTOMER-I'
            {
                ws_Cust_Id.reset();                                                                                                                                       //Natural: RESET WS-CUST-ID
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(2, ws_Cust_Id,new ColumnSpacing(2),ws_Eff_Dte,new ColumnSpacing(2),pnd_Ws_Cust_Name,new ColumnSpacing(2),pnd_Cmnt,new ColumnSpacing(2),pnd_In_Amt_Pnd_In_Amtn,  //Natural: WRITE ( 2 ) WS-CUST-ID 2X WS-EFF-DTE 2X #WS-CUST-NAME 2X #CMNT 2X #IN-AMTN ( EM = Z,ZZZ,ZZ9.99 ) 2X #STATUS
                new ReportEditMask ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Status);
            if (Global.isEscape()) return;
            pnd_Tot.nadd(1);                                                                                                                                              //Natural: ADD 1 TO #TOT
        }                                                                                                                                                                 //Natural: END-IF
        //* CTS2 - ERROR REPORT START
        if (condition(error1.getBoolean() || error2.getBoolean() || error3.getBoolean()))                                                                                 //Natural: IF ERROR1 OR ERROR2 OR ERROR3
        {
            if (condition(pnd_In_Amt_Pnd_In_Amtn.equals(getZero())))                                                                                                      //Natural: IF #IN-AMTN = 0
            {
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: END-IF
            getReports().write(3, ws_Cust_Id,new ColumnSpacing(2),pnd_Ws_Report_Date,new ColumnSpacing(2),ws_Eff_Dte,new ColumnSpacing(2),ws_Descr_Dte,new                //Natural: WRITE ( 3 ) WS-CUST-ID 2X #WS-REPORT-DATE 2X WS-EFF-DTE 2X WS-DESCR-DTE 2X #WS-CUST-NAME 2X #WS-TRANS-CDE 2X #IN-AMTN ( EM = Z,ZZZ,ZZ9.99 ) 2X #WS-ERR-REASON
                ColumnSpacing(2),pnd_Ws_Cust_Name,new ColumnSpacing(2),pnd_Ws_Trans_Cde,new ColumnSpacing(2),pnd_In_Amt_Pnd_In_Amtn, new ReportEditMask 
                ("Z,ZZZ,ZZ9.99"),new ColumnSpacing(2),pnd_Ws_Err_Reason);
            if (Global.isEscape()) return;
            //*  CTS2 - ERROR REPORT END
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Update_Annot() throws Exception                                                                                                                      //Natural: UPDATE-ANNOT
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------------------------------
        pnd_No_Annot.setValue(false);                                                                                                                                     //Natural: ASSIGN #NO-ANNOT := FALSE
        pnd_Updated.setValue(false);                                                                                                                                      //Natural: ASSIGN #UPDATED := FALSE
        vw_read_Annot.startDatabaseFind                                                                                                                                   //Natural: FIND ( 1 ) READ-ANNOT WITH PPCN-INV-ORGN-PRCSS-INST = ANNOT-KEY
        (
        "FD1",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", "=", annot_Key, WcType.WITH) },
        1
        );
        FD1:
        while (condition(vw_read_Annot.readNextRow("FD1", true)))
        {
            vw_read_Annot.setIfNotFoundControlFlag(false);
            if (condition(vw_read_Annot.getAstCOUNTER().equals(0)))                                                                                                       //Natural: IF NO RECORDS FOUND
            {
                //*    PERFORM ADD-ANNOT
                //*    WRITE 'ANNOT RECORD NOT FOUND'
                pnd_No_Annot.setValue(true);                                                                                                                              //Natural: ASSIGN #NO-ANNOT := TRUE
            }                                                                                                                                                             //Natural: END-NOREC
            if (condition(!(read_Annot_Cntrct_Rcrd_Typ.equals("4"))))                                                                                                     //Natural: ACCEPT CNTRCT-RCRD-TYP = '4'
            {
                continue;
            }
            FOR03:                                                                                                                                                        //Natural: FOR #I 1 99
            for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(99)); pnd_I.nadd(1))
            {
                if (condition(read_Annot_Pymnt_Annot_Expand_Cmnt.getValue(pnd_I).equals("              ")))                                                               //Natural: IF PYMNT-ANNOT-EXPAND-CMNT ( #I ) = '              '
                {
                    //*      WRITE '=' #CMNT
                    //*                                                        /* CTS STARTS
                    DbsUtil.examine(new ExamineSource(pnd_Valid_Reversal_Reject_Codes.getValue("*")), new ExamineSearch(pnd_Ws_Trans_Cde), new ExamineGivingIndex(pnd_J)); //Natural: EXAMINE #VALID-REVERSAL-REJECT-CODES ( * ) FOR #WS-TRANS-CDE GIVING INDEX #J
                    if (condition((pnd_J.equals(getZero()))))                                                                                                             //Natural: IF ( #J EQ 0 )
                    {
                        read_Annot_Pymnt_Annot_Dte.getValue(7).setValue(Global.getDATX());                                                                                //Natural: MOVE *DATX TO PYMNT-ANNOT-DTE ( 7 )
                        read_Annot_Pymnt_Annot_Id_Nbr.getValue(7).setValue("SYS");                                                                                        //Natural: MOVE 'SYS' TO PYMNT-ANNOT-ID-NBR ( 7 )
                        read_Annot_Pymnt_Annot_Flag_Ind.getValue(7).setValue("*");                                                                                        //Natural: MOVE '*' TO PYMNT-ANNOT-FLAG-IND ( 7 )
                        //*  CTS ENDS
                    }                                                                                                                                                     //Natural: END-IF
                    read_Annot_Pymnt_Annot_Expand_Cmnt.getValue(pnd_I).setValue(pnd_Cmnt);                                                                                //Natural: MOVE #CMNT TO PYMNT-ANNOT-EXPAND-CMNT ( #I )
                    read_Annot_Pymnt_Annot_Expand_Date.getValue(pnd_I).setValue(pnd_Today);                                                                               //Natural: MOVE #TODAY TO PYMNT-ANNOT-EXPAND-DATE ( #I )
                    pnd_Time.setValue(Global.getTIMN());                                                                                                                  //Natural: MOVE *TIMN TO #TIME
                    read_Annot_Pymnt_Annot_Expand_Time.getValue(pnd_I).setValue(pnd_Time_Pnd_Now);                                                                        //Natural: MOVE #NOW TO PYMNT-ANNOT-EXPAND-TIME ( #I )
                    read_Annot_Pymnt_Annot_Expand_Uid.getValue(pnd_I).setValue(Global.getUSER());                                                                         //Natural: MOVE *USER TO PYMNT-ANNOT-EXPAND-UID ( #I )
                    vw_read_Annot.updateDBRow("FD1");                                                                                                                     //Natural: UPDATE ( FD1. )
                    pnd_Status.setValue("Updated");                                                                                                                       //Natural: MOVE 'Updated' TO #STATUS
                    //*      WRITE 'UPDATED ANNOT RECORD'
                    pnd_Updated.setValue(true);                                                                                                                           //Natural: ASSIGN #UPDATED := TRUE
                    if (condition(true)) break;                                                                                                                           //Natural: ESCAPE BOTTOM
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("FD1"))) break;
                else if (condition(Global.isEscapeBottomImmediate("FD1"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FIND
        if (Global.isEscape()) return;
    }
    private void sub_Add_Annot() throws Exception                                                                                                                         //Natural: ADD-ANNOT
    {
        if (BLNatReinput.isReinput()) return;

        mod_Annot_Cntrct_Rcrd_Typ.setValue("4");                                                                                                                          //Natural: ASSIGN MOD-ANNOT.CNTRCT-RCRD-TYP := '4'
        mod_Annot_Cntrct_Ppcn_Nbr.setValue(ws_Cust_Id_Ws_Cust_Cntrct);                                                                                                    //Natural: ASSIGN MOD-ANNOT.CNTRCT-PPCN-NBR := WS-CUST-CNTRCT
        mod_Annot_Cntrct_Invrse_Dte.setValue(read_Pymnt_Cntrct_Invrse_Dte);                                                                                               //Natural: ASSIGN MOD-ANNOT.CNTRCT-INVRSE-DTE := READ-PYMNT.CNTRCT-INVRSE-DTE
        mod_Annot_Pymnt_Prcss_Seq_Nbr.setValue(read_Pymnt_Pymnt_Prcss_Seq_Nbr);                                                                                           //Natural: ASSIGN MOD-ANNOT.PYMNT-PRCSS-SEQ-NBR := READ-PYMNT.PYMNT-PRCSS-SEQ-NBR
        mod_Annot_Cntrct_Orgn_Cde.setValue(read_Pymnt_Cntrct_Orgn_Cde);                                                                                                   //Natural: ASSIGN MOD-ANNOT.CNTRCT-ORGN-CDE := READ-PYMNT.CNTRCT-ORGN-CDE
        mod_Annot_Pymnt_Reqst_Nbr.setValue(read_Pymnt_Pymnt_Reqst_Nbr);                                                                                                   //Natural: ASSIGN MOD-ANNOT.PYMNT-REQST-NBR := READ-PYMNT.PYMNT-REQST-NBR
        //*                                                        /* CTS STARTS
        DbsUtil.examine(new ExamineSource(pnd_Valid_Reversal_Reject_Codes.getValue("*")), new ExamineSearch(pnd_Ws_Trans_Cde), new ExamineGivingIndex(pnd_J));            //Natural: EXAMINE #VALID-REVERSAL-REJECT-CODES ( * ) FOR #WS-TRANS-CDE GIVING INDEX #J
        if (condition((pnd_J.equals(getZero()))))                                                                                                                         //Natural: IF ( #J EQ 0 )
        {
            mod_Annot_Pymnt_Annot_Dte.getValue(7).setValue(Global.getDATX());                                                                                             //Natural: MOVE *DATX TO MOD-ANNOT.PYMNT-ANNOT-DTE ( 7 )
            mod_Annot_Pymnt_Annot_Id_Nbr.getValue(7).setValue("SYS");                                                                                                     //Natural: MOVE 'SYS' TO MOD-ANNOT.PYMNT-ANNOT-ID-NBR ( 7 )
            mod_Annot_Pymnt_Annot_Flag_Ind.getValue(7).setValue("*");                                                                                                     //Natural: MOVE '*' TO MOD-ANNOT.PYMNT-ANNOT-FLAG-IND ( 7 )
            //*  CTS ENDS
        }                                                                                                                                                                 //Natural: END-IF
        mod_Annot_Pymnt_Annot_Expand_Cmnt.getValue(1).setValue(pnd_Cmnt);                                                                                                 //Natural: ASSIGN MOD-ANNOT.PYMNT-ANNOT-EXPAND-CMNT ( 1 ) := #CMNT
        mod_Annot_Pymnt_Annot_Expand_Date.getValue(1).setValue(pnd_Today);                                                                                                //Natural: ASSIGN MOD-ANNOT.PYMNT-ANNOT-EXPAND-DATE ( 1 ) := #TODAY
        pnd_Time.setValue(Global.getTIMN());                                                                                                                              //Natural: MOVE *TIMN TO #TIME
        mod_Annot_Pymnt_Annot_Expand_Time.getValue(1).setValue(pnd_Time_Pnd_Now);                                                                                         //Natural: ASSIGN MOD-ANNOT.PYMNT-ANNOT-EXPAND-TIME ( 1 ) := #NOW
        mod_Annot_Pymnt_Annot_Expand_Uid.getValue(1).setValue(Global.getUSER());                                                                                          //Natural: ASSIGN MOD-ANNOT.PYMNT-ANNOT-EXPAND-UID ( 1 ) := *USER
        vw_mod_Annot.insertDBRow();                                                                                                                                       //Natural: STORE MOD-ANNOT
        pnd_Status.setValue("Added");                                                                                                                                     //Natural: MOVE 'Added' TO #STATUS
        //*  WRITE 'ADDED ANNOT RECORD'
        pnd_Added.setValue(true);                                                                                                                                         //Natural: ASSIGN #ADDED := TRUE
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=66");
        Global.format(2, "LS=133 PS=66");
        Global.format(3, "LS=133 PS=66");

        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(39),"Consolidated Payment System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(14), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getPROGRAM(),new 
            TabSetting(39),pnd_Title,NEWLINE,"Contract    Eff Date   Customer Name                      Description","                                    Amount",
            NEWLINE,"-------------------------------------------------------------------","---------------------------------------------",NEWLINE);
        getReports().write(3, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(39),"CONSOLIDATED PAYMENT SYSTEM ",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(14), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getPROGRAM(),new 
            TabSetting(39),"ACH PAYMENT ANNOTATION ERROR REPORT",NEWLINE,"CONTRACT    REPORT-DT EFF-DATE  DESC-DT  CUSTOMER NAME           ","TC       RPT-AMT      ERROR REASON           ",
            NEWLINE,"-------------------------------------------------------------------","----------------------------------------------------------------",
            NEWLINE);
    }
}
