/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:58 PM
**        * FROM NATURAL PROGRAM : Fcpp805a
************************************************************
**        * FILE NAME            : Fcpp805a.java
**        * CLASS NAME           : Fcpp805a
**        * INSTANCE NAME        : Fcpp805a
************************************************************
************************************************************************
*
* PROGRAM      : FCPP805A
*
* SYSTEM       : CPS
* AUTHOR       : MARIA ACLAN
* DATE WRITTEN : MARCH 20, 2001
*
* FUNCTION     : GROUP OPTION CODE BY TPA/IPRO/P&I AND REGULAR.
*              : TPA  = 28 AND 30
*              : IPRO = 25 AND 27
*              : P&I  = 22
*              : REGULAR = NONE OF THE ABOVE
*              :
* HISTORY      :
*              : 05/02/03 ROXANNE CARREON
*              : RESTOW. FCPA800 EXPANDED
*              :
* 05/06/08 : LCW - RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 05/09/2017 :SAI K      - RECOMPILE DUE TO CHANGES TO FCPA800
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp805a extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Grp_Optn_Code;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);

        // Local Variables
        pnd_Grp_Optn_Code = localVariables.newFieldInRecord("pnd_Grp_Optn_Code", "#GRP-OPTN-CODE", FieldType.STRING, 5);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp805a() throws Exception
    {
        super("Fcpp805a");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP805A", onError);
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 01 ) LS = 132 PS = 55
        //*                                                                                                                                                               //Natural: ON ERROR
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 01 WF-PYMNT-ADDR-REC
        while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec())))
        {
            short decideConditionsMet493 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF CNTRCT-OPTION-CDE;//Natural: VALUE 28,30
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(28) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(30))))
            {
                decideConditionsMet493++;
                pnd_Grp_Optn_Code.setValue("TPA");                                                                                                                        //Natural: ASSIGN #GRP-OPTN-CODE := 'TPA'
            }                                                                                                                                                             //Natural: VALUE 25,27
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(25) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(27))))
            {
                decideConditionsMet493++;
                pnd_Grp_Optn_Code.setValue("IPRO");                                                                                                                       //Natural: ASSIGN #GRP-OPTN-CODE := 'IPRO'
            }                                                                                                                                                             //Natural: VALUE 22
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(22))))
            {
                decideConditionsMet493++;
                pnd_Grp_Optn_Code.setValue("P&I");                                                                                                                        //Natural: ASSIGN #GRP-OPTN-CODE := 'P&I'
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Grp_Optn_Code.setValue("OTH");                                                                                                                        //Natural: ASSIGN #GRP-OPTN-CODE := 'OTH'
            }                                                                                                                                                             //Natural: END-DECIDE
            getWorkFiles().write(2, false, pnd_Grp_Optn_Code, pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                                       //Natural: WRITE WORK FILE 02 #GRP-OPTN-CODE WF-PYMNT-ADDR-REC
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
        getReports().write(0, NEWLINE,NEWLINE,"**************************************************************",NEWLINE,"***",Global.getPROGRAM(),"  Error:",              //Natural: WRITE // '**************************************************************' / '***'*PROGRAM '  Error:' *ERROR-NR 'Line:' *ERROR-LINE / '**************************************************************'
            Global.getERROR_NR(),"Line:",Global.getERROR_LINE(),NEWLINE,"**************************************************************");
        DbsUtil.terminate(99);  if (true) return;                                                                                                                         //Natural: TERMINATE 0099
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=132 PS=55");
    }
}
