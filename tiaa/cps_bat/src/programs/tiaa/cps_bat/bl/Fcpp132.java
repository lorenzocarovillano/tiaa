/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:14 PM
**        * FROM NATURAL PROGRAM : Fcpp132
************************************************************
**        * FILE NAME            : Fcpp132.java
**        * CLASS NAME           : Fcpp132
**        * INSTANCE NAME        : Fcpp132
************************************************************
************************************************************************
* PROGRAM  : FCPP132
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS PAYMENT WARRANT EXTRACT
* CREATED  : 10/04/94
* FUNCTION : THIS PROGRAM READS CHECK FILE, ATTACHES INFORMATION FROM
*            THE FCP-CONS-LEDGER FILE AND CREATES AN EXTRACT FOR
*            WARRANT REPORT.
*
* 06/23/1998 - RIAD LOUTFI - ADDED CODE TO CAPTURE BOTH PAYMENT AND
*              SETTLEMENT TYPE.  TO IDENTIFY CLASSIC / ROTH IRAS.
*
*  03/12/99 :  R. CARREON
*              RESTOW - ADDITION OF PYMNT-SPOUSE-PAY-STATS IN FCPL378
*  06/03/99 :  R. CARREON
*              POPULATE NEW FIELD ##INV-ACCT-OVR-PAY-DED-AMT IN FCPL123
*  08/05/99 - LEON GURTOVNIK
*             MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*             NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
*
* 09/11/00 : ALTHEA A. YOUNG
*          - RE-COMPILED DUE TO FCPL123.
*    11/02 : R. CARREON   - STOW. EGTRRA CHANGES IN LDA.
*
* 04/26/2008: ROTH-MAJOR1 (AER) STOW ONLY
* 12/09/2011: R.SACHARNY RECOMPILE DUE TO CHANGE IN FCPLPMNT
* 4/2017    : JJG - PIN EXPANSION RESTOW
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp132 extends BLNatBase
{
    // Data Areas
    private PdaFcpaaddr pdaFcpaaddr;
    private PdaFcpaldgr pdaFcpaldgr;
    private LdaFcplpmnt ldaFcplpmnt;
    private LdaFcpl123 ldaFcpl123;
    private LdaFcpl378 ldaFcpl378;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Pymnt_S;
    private DbsField pnd_Pymnt_S_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Pymnt_S_Cntrct_Invrse_Dte;
    private DbsField pnd_Pymnt_S_Cntrct_Orgn_Cde;
    private DbsField pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Pymnt_S__R_Field_1;
    private DbsField pnd_Pymnt_S_Pnd_Pymnt_Superde_Start;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Pymnt_Superde_End;
    private DbsField pnd_Ws_Pnd_Accept_Ind;

    private DbsGroup pnd_Ws_Pnd_Record_Counts;
    private DbsField pnd_Ws_Pnd_Records_All;
    private DbsField pnd_Ws_Pnd_Records_10;
    private DbsField pnd_Ws_Pnd_Records_20;
    private DbsField pnd_Ws_Pnd_Records_30;
    private DbsField pnd_Ws_Pnd_Records_Other;
    private DbsField pnd_Ws_Pnd_Pymnt_Records_Read;
    private DbsField pnd_Ws_Pnd_Addr_Records_Read;
    private DbsField pnd_Ws_Pnd_Ledger_Records_Read;
    private DbsField pnd_Ws_Pnd_Ledger_Read;
    private DbsField pnd_Ws_Pnd_Accepted_Records;
    private DbsField pnd_Ws_Pnd_Records_Written;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_Sub;
    private DbsField pnd_Ws_Pnd_X;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaaddr = new PdaFcpaaddr(localVariables);
        pdaFcpaldgr = new PdaFcpaldgr(localVariables);
        ldaFcplpmnt = new LdaFcplpmnt();
        registerRecord(ldaFcplpmnt);
        registerRecord(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());
        ldaFcpl123 = new LdaFcpl123();
        registerRecord(ldaFcpl123);
        ldaFcpl378 = new LdaFcpl378();
        registerRecord(ldaFcpl378);

        // Local Variables

        pnd_Pymnt_S = localVariables.newGroupInRecord("pnd_Pymnt_S", "#PYMNT-S");
        pnd_Pymnt_S_Cntrct_Ppcn_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Pymnt_S_Cntrct_Invrse_Dte = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Pymnt_S_Cntrct_Orgn_Cde = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr = pnd_Pymnt_S.newFieldInGroup("pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Pymnt_S__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_S__R_Field_1", "REDEFINE", pnd_Pymnt_S);
        pnd_Pymnt_S_Pnd_Pymnt_Superde_Start = pnd_Pymnt_S__R_Field_1.newFieldInGroup("pnd_Pymnt_S_Pnd_Pymnt_Superde_Start", "#PYMNT-SUPERDE-START", FieldType.STRING, 
            29);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Pymnt_Superde_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Superde_End", "#PYMNT-SUPERDE-END", FieldType.STRING, 29);
        pnd_Ws_Pnd_Accept_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Accept_Ind", "#ACCEPT-IND", FieldType.BOOLEAN, 1);

        pnd_Ws_Pnd_Record_Counts = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Record_Counts", "#RECORD-COUNTS");
        pnd_Ws_Pnd_Records_All = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_All", "#RECORDS-ALL", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 3));
        pnd_Ws_Pnd_Records_10 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_10", "#RECORDS-10", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 3));
        pnd_Ws_Pnd_Records_20 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_20", "#RECORDS-20", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 3));
        pnd_Ws_Pnd_Records_30 = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_30", "#RECORDS-30", FieldType.PACKED_DECIMAL, 7, new 
            DbsArrayController(1, 3));
        pnd_Ws_Pnd_Records_Other = pnd_Ws_Pnd_Record_Counts.newFieldArrayInGroup("pnd_Ws_Pnd_Records_Other", "#RECORDS-OTHER", FieldType.PACKED_DECIMAL, 
            7, new DbsArrayController(1, 3));
        pnd_Ws_Pnd_Pymnt_Records_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Pymnt_Records_Read", "#PYMNT-RECORDS-READ", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Addr_Records_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Addr_Records_Read", "#ADDR-RECORDS-READ", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Ledger_Records_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ledger_Records_Read", "#LEDGER-RECORDS-READ", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Ledger_Read = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ledger_Read", "#LEDGER-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Accepted_Records = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Accepted_Records", "#ACCEPTED-RECORDS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Records_Written = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Records_Written", "#RECORDS-WRITTEN", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_I = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Sub = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Sub", "#SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_X = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_X", "#X", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcplpmnt.initializeValues();
        ldaFcpl123.initializeValues();
        ldaFcpl378.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp132() throws Exception
    {
        super("Fcpp132");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT PS = 23 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 55T 'WARRANT REPORTING EXTRACT' 120T 'REPORT: RPT1' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        pdaFcpaaddr.getAddr_Work_Fields_Addr_Abend_Ind().setValue(true);                                                                                                  //Natural: MOVE TRUE TO ADDR-ABEND-IND CURRENT-ADDR LEDGER-ABEND-IND
        pdaFcpaaddr.getAddr_Work_Fields_Current_Addr().setValue(true);
        pdaFcpaldgr.getLedger_Work_Fields_Ledger_Abend_Ind().setValue(true);
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #PYMNT-EXT-KEY #REC-TYPE-10-DETAIL
        while (condition(getWorkFiles().read(1, ldaFcpl378.getPnd_Pymnt_Ext_Key(), ldaFcpl378.getPnd_Rec_Type_10_Detail())))
        {
            pnd_Ws_Pnd_Accept_Ind.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #ACCEPT-IND
            short decideConditionsMet766 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #KEY-REC-LVL-#;//Natural: VALUE 10
            if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(10))))
            {
                decideConditionsMet766++;
                short decideConditionsMet768 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #REC-TYPE-10-DETAIL.CNTRCT-ORGN-CDE NE 'DC'
                if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Orgn_Cde().notEquals("DC")))
                {
                    decideConditionsMet768++;
                    //*  LEON CN SN 08-05-99
                    //*  JWO 2010-11
                    pnd_Ws_Pnd_I.setValue(2);                                                                                                                             //Natural: MOVE 2 TO #I
                }                                                                                                                                                         //Natural: WHEN #REC-TYPE-10-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'C' OR = 'S' OR = 'CN' OR = 'SN' OR = 'CP' OR = 'SP' OR = 'RP' OR = 'PR'
                else if (condition(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("C") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("S") 
                    || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CN") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SN") 
                    || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("CP") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("SP") 
                    || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("RP") || ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde().equals("PR")))
                {
                    decideConditionsMet768++;
                    pnd_Ws_Pnd_I.setValue(3);                                                                                                                             //Natural: MOVE 3 TO #I
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    pnd_Ws_Pnd_I.setValue(1);                                                                                                                             //Natural: MOVE 1 TO #I
                    pnd_Ws_Pnd_Accept_Ind.setValue(true);                                                                                                                 //Natural: MOVE TRUE TO #ACCEPT-IND
                }                                                                                                                                                         //Natural: END-DECIDE
                pnd_Ws_Pnd_Records_10.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-10 ( #I )
            }                                                                                                                                                             //Natural: VALUE 20
            else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(20))))
            {
                decideConditionsMet766++;
                pnd_Ws_Pnd_Records_20.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-20 ( #I )
            }                                                                                                                                                             //Natural: VALUE 30
            else if (condition((ldaFcpl378.getPnd_Pymnt_Ext_Key_Pnd_Key_Rec_Lvl_Pnd().equals(30))))
            {
                decideConditionsMet766++;
                pnd_Ws_Pnd_Records_30.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                     //Natural: ADD 1 TO #RECORDS-30 ( #I )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Records_Other.getValue(pnd_Ws_Pnd_I).nadd(1);                                                                                                  //Natural: ADD 1 TO #RECORDS-OTHER ( #I )
            }                                                                                                                                                             //Natural: END-DECIDE
            if (condition(!(pnd_Ws_Pnd_Accept_Ind.getBoolean() && ldaFcpl378.getPnd_Rec_Type_10_Detail_Pymnt_Instmt_Nbr().equals(1))))                                    //Natural: ACCEPT IF #ACCEPT-IND AND #REC-TYPE-10-DETAIL.PYMNT-INSTMT-NBR = 1
            {
                continue;
            }
            pnd_Ws_Pnd_Accepted_Records.nadd(1);                                                                                                                          //Natural: ADD 1 TO #ACCEPTED-RECORDS
                                                                                                                                                                          //Natural: PERFORM PROCESS-DATABASE
            sub_Process_Database();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_Records_10.getValue(1).nadd(pnd_Ws_Pnd_Records_10.getValue(2,":",3));                                                                                  //Natural: ADD #RECORDS-10 ( 2:3 ) TO #RECORDS-10 ( 1 )
        pnd_Ws_Pnd_Records_20.getValue(1).nadd(pnd_Ws_Pnd_Records_20.getValue(2,":",3));                                                                                  //Natural: ADD #RECORDS-20 ( 2:3 ) TO #RECORDS-20 ( 1 )
        pnd_Ws_Pnd_Records_30.getValue(1).nadd(pnd_Ws_Pnd_Records_30.getValue(2,":",3));                                                                                  //Natural: ADD #RECORDS-30 ( 2:3 ) TO #RECORDS-30 ( 1 )
        pnd_Ws_Pnd_Records_Other.getValue(1).nadd(pnd_Ws_Pnd_Records_Other.getValue(2,":",3));                                                                            //Natural: ADD #RECORDS-OTHER ( 2:3 ) TO #RECORDS-OTHER ( 1 )
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO 3
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(3)); pnd_Ws_Pnd_I.nadd(1))
        {
            pnd_Ws_Pnd_Records_All.getValue(pnd_Ws_Pnd_I).compute(new ComputeParameters(false, pnd_Ws_Pnd_Records_All.getValue(pnd_Ws_Pnd_I)), pnd_Ws_Pnd_Records_10.getValue(pnd_Ws_Pnd_I).add(pnd_Ws_Pnd_Records_20.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_30.getValue(pnd_Ws_Pnd_I)).add(pnd_Ws_Pnd_Records_Other.getValue(pnd_Ws_Pnd_I))); //Natural: ADD #RECORDS-10 ( #I ) #RECORDS-20 ( #I ) #RECORDS-30 ( #I ) #RECORDS-OTHER ( #I ) GIVING #RECORDS-ALL ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, new TabSetting(14),"COUNTERS AT END OF PROGRAM:",NEWLINE,new TabSetting(17),"RECORDS READ:",NEWLINE,new TabSetting(17),".................ALL:",pnd_Ws_Pnd_Records_All.getValue(1),  //Natural: WRITE ( 1 ) 14T 'COUNTERS AT END OF PROGRAM:' / 17T 'RECORDS READ:' / 17T '.................ALL:' #RECORDS-ALL ( 1 ) / 17T '.............TYPE 10:' #RECORDS-10 ( 1 ) / 17T '.............TYPE 20:' #RECORDS-20 ( 1 ) / 17T '.............TYPE 30:' #RECORDS-30 ( 1 ) / 17T '...............OTHER:' #RECORDS-OTHER ( 1 ) // 17T 'RECORDS REJECTED:' / 17T '...........NOT IA DEATH:' / 17T '.................ALL:' #RECORDS-ALL ( 2 ) / 17T '.............TYPE 10:' #RECORDS-10 ( 2 ) / 17T '.............TYPE 20:' #RECORDS-20 ( 2 ) / 17T '.............TYPE 30:' #RECORDS-30 ( 2 ) / 17T '...............OTHER:' #RECORDS-OTHER ( 2 ) / 17T '..........CANCEL & STOP:' / 17T '.................ALL:' #RECORDS-ALL ( 3 ) / 17T '.............TYPE 10:' #RECORDS-10 ( 3 ) / 17T '.............TYPE 20:' #RECORDS-20 ( 3 ) / 17T '.............TYPE 30:' #RECORDS-30 ( 3 ) / 17T '...............OTHER:' #RECORDS-OTHER ( 3 ) // 17T 'ACCEPTED RECORDS....:' #ACCEPTED-RECORDS '(TYPE 10 INSTALLMENT 1)' / 17T 'LEDGER RECORDS READ.:' #LEDGER-RECORDS-READ '(FOR ACCEPTED RECORDS)' / 17T '    (ACTUAL LEDGERS):' #LEDGER-READ / 17T 'PAYMENT RECORDS READ:' #PYMNT-RECORDS-READ / 17T 'ADDRESS RECORDS READ:' #ADDR-RECORDS-READ // 17T 'RECORDS WRITTEN.....:' #RECORDS-WRITTEN //// 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************' / 17T '************ E N D   O F   T H E   R E P O R T   ************'
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),".............TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),".............TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(1), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),".............TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(1), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...............OTHER:",pnd_Ws_Pnd_Records_Other.getValue(1), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"RECORDS REJECTED:",NEWLINE,new TabSetting(17),"...........NOT IA DEATH:",NEWLINE,new TabSetting(17),".................ALL:",pnd_Ws_Pnd_Records_All.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),".............TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),".............TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(2), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),".............TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(2), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...............OTHER:",pnd_Ws_Pnd_Records_Other.getValue(2), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"..........CANCEL & STOP:",NEWLINE,new TabSetting(17),".................ALL:",pnd_Ws_Pnd_Records_All.getValue(3), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),".............TYPE 10:",pnd_Ws_Pnd_Records_10.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(17),".............TYPE 20:",pnd_Ws_Pnd_Records_20.getValue(3), new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),".............TYPE 30:",pnd_Ws_Pnd_Records_30.getValue(3), 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"...............OTHER:",pnd_Ws_Pnd_Records_Other.getValue(3), new ReportEditMask 
            ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new TabSetting(17),"ACCEPTED RECORDS....:",pnd_Ws_Pnd_Accepted_Records, new ReportEditMask ("-Z,ZZZ,ZZ9"),"(TYPE 10 INSTALLMENT 1)",NEWLINE,new 
            TabSetting(17),"LEDGER RECORDS READ.:",pnd_Ws_Pnd_Ledger_Records_Read, new ReportEditMask ("-Z,ZZZ,ZZ9"),"(FOR ACCEPTED RECORDS)",NEWLINE,new 
            TabSetting(17),"    (ACTUAL LEDGERS):",pnd_Ws_Pnd_Ledger_Read, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"PAYMENT RECORDS READ:",pnd_Ws_Pnd_Pymnt_Records_Read, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(17),"ADDRESS RECORDS READ:",pnd_Ws_Pnd_Addr_Records_Read, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,new 
            TabSetting(17),"RECORDS WRITTEN.....:",pnd_Ws_Pnd_Records_Written, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE,NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new 
            TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(17),"************ E N D   O F   T H E   R E P O R T   ************");
        if (Global.isEscape()) return;
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-DATABASE
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-ADDR-RECORD
        //* **************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-LEDGER-RECORD
        //*   FOR OVER PAYMENT                     06/03/99  ROXAN
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COMPUTE-OVR-PAY-DED
        //* ************************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
    }
    private void sub_Process_Database() throws Exception                                                                                                                  //Natural: PROCESS-DATABASE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Pymnt_S.setValuesByName(ldaFcpl378.getPnd_Rec_Type_10_Detail_Pnd_Record_Type_10());                                                                           //Natural: MOVE BY NAME #RECORD-TYPE-10 TO #PYMNT-S
        pnd_Ws_Pnd_Pymnt_Superde_End.setValue(pnd_Pymnt_S_Pnd_Pymnt_Superde_Start);                                                                                       //Natural: MOVE #PYMNT-SUPERDE-START TO #PYMNT-SUPERDE-END
        setValueToSubstring("H'0000'",pnd_Pymnt_S_Pnd_Pymnt_Superde_Start,28,2);                                                                                          //Natural: MOVE H'0000' TO SUBSTR ( #PYMNT-SUPERDE-START,28,2 )
        setValueToSubstring("H'FFFF'",pnd_Ws_Pnd_Pymnt_Superde_End,28,2);                                                                                                 //Natural: MOVE H'FFFF' TO SUBSTR ( #PYMNT-SUPERDE-END,28,2 )
        ldaFcplpmnt.getVw_fcp_Cons_Pymnt().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNT BY PPCN-INV-ORGN-PRCSS-INST = #PYMNT-SUPERDE-START THRU #PYMNT-SUPERDE-END
        (
        "RPYMNT",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", pnd_Pymnt_S_Pnd_Pymnt_Superde_Start, "And", WcType.BY) ,
        new Wc("PPCN_INV_ORGN_PRCSS_INST", "<=", pnd_Ws_Pnd_Pymnt_Superde_End, WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") }
        );
        RPYMNT:
        while (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().readNextRow("RPYMNT")))
        {
            pnd_Ws_Pnd_Pymnt_Records_Read.nadd(1);                                                                                                                        //Natural: ADD 1 TO #PYMNT-RECORDS-READ
            ldaFcpl123.getPnd_Warrant_Extract().reset();                                                                                                                  //Natural: RESET #WARRANT-EXTRACT
            if (condition(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Instmt_Nbr().equals(1)))                                                                                    //Natural: IF FCP-CONS-PYMNT.PYMNT-INSTMT-NBR = 1
            {
                                                                                                                                                                          //Natural: PERFORM GET-ADDR-RECORD
                sub_Get_Addr_Record();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RPYMNT"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RPYMNT"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Key().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                      //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #WARRANT-KEY
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pymnt_Addr().setValuesByName(pdaFcpaaddr.getAddr());                                                                    //Natural: MOVE BY NAME ADDR TO #PYMNT-ADDR
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pymnt_Addr().setValuesByName(ldaFcplpmnt.getVw_fcp_Cons_Pymnt());                                                       //Natural: MOVE BY NAME FCP-CONS-PYMNT TO #PYMNT-ADDR
            ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castinv_Acct());                                                  //Natural: MOVE FCP-CONS-PYMNT.C*INV-ACCT TO #WARRANT-EXTRACT.C-INV-ACCT
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Info().getValue(1,":",ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct()).setValuesByName(ldaFcplpmnt.getFcp_Cons_Pymnt_Inv_Acct().getValue(1, //Natural: MOVE BY NAME FCP-CONS-PYMNT.INV-ACCT ( 1:C-INV-ACCT ) TO #WARRANT-EXTRACT.#INV-INFO ( 1:C-INV-ACCT )
                ":",ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct()));
            ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Ded_Grp_Top().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Count_Castpymnt_Ded_Grp());                                      //Natural: MOVE FCP-CONS-PYMNT.C*PYMNT-DED-GRP TO #WARRANT-EXTRACT.PYMNT-DED-GRP-TOP
            ldaFcpl123.getPnd_Warrant_Extract_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde());             //Natural: MOVE #REC-TYPE-10-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE TO #WARRANT-EXTRACT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Instmt().setValue(ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Instmt_Nbr());                                              //Natural: MOVE #WARRANT-EXTRACT.PYMNT-INSTMT-NBR TO #WARRANT-EXTRACT.PYMNT-INSTMT
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().setValue(1);                                                                                              //Natural: MOVE 1 TO #RECORD-TYPE
            ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Instmt_Nbr().setValue(1);                                                                                             //Natural: MOVE 1 TO #WARRANT-EXTRACT.PYMNT-INSTMT-NBR
            //*   FOR OVER PAYMENT                     06/03/99  ROXAN
                                                                                                                                                                          //Natural: PERFORM COMPUTE-OVR-PAY-DED
            sub_Compute_Ovr_Pay_Ded();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPYMNT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPYMNT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getWorkFiles().write(2, true, ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Grp1(), ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Detail().getValue("*"),     //Natural: WRITE WORK FILE 2 VARIABLE #WARRANT-GRP1 #WARRANT-DETAIL ( * ) #INV-DETAIL ( 1:C-INV-ACCT )
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Detail().getValue(1,":",ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct()));
            pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                           //Natural: ADD 1 TO #RECORDS-WRITTEN
                                                                                                                                                                          //Natural: PERFORM PROCESS-LEDGER-RECORD
            sub_Process_Ledger_Record();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom("RPYMNT"))) break;
                else if (condition(Global.isEscapeBottomImmediate("RPYMNT"))) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(ldaFcplpmnt.getVw_fcp_Cons_Pymnt().getAstCOUNTER().equals(getZero())))                                                                              //Natural: IF *COUNTER ( RPYMNT. ) = 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"PAYMENT RECORD IS NOT FOUND",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"ORIGIN......:",pnd_Pymnt_S_Cntrct_Orgn_Cde,new  //Natural: WRITE '***' 25T 'PAYMENT RECORD IS NOT FOUND' 77T '***' / '***' 25T 'ORIGIN......:' #PYMNT-S.CNTRCT-ORGN-CDE 77T '***' / '***' 25T 'PPCN#.......:' #PYMNT-S.CNTRCT-PPCN-NBR 77T '***' / '***' 25T 'INVERSE DATE:' #PYMNT-S.CNTRCT-INVRSE-DTE 77T '***' / '***' 25T 'SEQUENCE#...:' #PYMNT-S.PYMNT-PRCSS-SEQ-NBR 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PPCN#.......:",pnd_Pymnt_S_Cntrct_Ppcn_Nbr,new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"INVERSE DATE:",pnd_Pymnt_S_Cntrct_Invrse_Dte,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"SEQUENCE#...:",pnd_Pymnt_S_Pymnt_Prcss_Seq_Nbr,new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Addr_Record() throws Exception                                                                                                                   //Natural: GET-ADDR-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        pdaFcpaaddr.getAddr_Cntrct_Ppcn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                  //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-PPCN-NBR TO ADDR.CNTRCT-PPCN-NBR
        pdaFcpaaddr.getAddr_Cntrct_Payee_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Payee_Cde());                                                                //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-PAYEE-CDE TO ADDR.CNTRCT-PAYEE-CDE
        pdaFcpaaddr.getAddr_Cntrct_Orgn_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                  //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE TO ADDR.CNTRCT-ORGN-CDE
        pdaFcpaaddr.getAddr_Cntrct_Invrse_Dte().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Invrse_Dte());                                                              //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-INVRSE-DTE TO ADDR.CNTRCT-INVRSE-DTE
        pdaFcpaaddr.getAddr_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Num());                                                          //Natural: MOVE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NUM TO ADDR.PYMNT-PRCSS-SEQ-NBR
        pdaFcpaaddr.getAddr_Work_Fields_Cntrct_Cmbn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Cmbn_Nbr());                                                      //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-CMBN-NBR TO ADDR-WORK-FIELDS.CNTRCT-CMBN-NBR
        DbsUtil.callnat(Fcpnaddr.class , getCurrentProcessState(), pdaFcpaaddr.getAddr_Work_Fields(), pdaFcpaaddr.getAddr());                                             //Natural: CALLNAT 'FCPNADDR' USING ADDR-WORK-FIELDS ADDR
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Ledger_Record() throws Exception                                                                                                             //Natural: PROCESS-LEDGER-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************************
        ldaFcpl123.getPnd_Warrant_Extract_Pymnt_Instmt().setValue(1);                                                                                                     //Natural: MOVE 1 TO #WARRANT-EXTRACT.PYMNT-INSTMT
        pdaFcpaldgr.getLedger_Cntrct_Orgn_Cde().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Orgn_Cde());                                                                //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-ORGN-CDE TO LEDGER.CNTRCT-ORGN-CDE
        pdaFcpaldgr.getLedger_Cntrct_Ppcn_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Cntrct_Ppcn_Nbr());                                                                //Natural: MOVE FCP-CONS-PYMNT.CNTRCT-PPCN-NBR TO LEDGER.CNTRCT-PPCN-NBR
        pdaFcpaldgr.getLedger_Pymnt_Prcss_Seq_Nbr().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Prcss_Seq_Nbr());                                                        //Natural: MOVE FCP-CONS-PYMNT.PYMNT-PRCSS-SEQ-NBR TO LEDGER.PYMNT-PRCSS-SEQ-NBR
        pdaFcpaldgr.getLedger_Cntl_Check_Dte().setValue(ldaFcplpmnt.getFcp_Cons_Pymnt_Pymnt_Check_Dte());                                                                 //Natural: MOVE FCP-CONS-PYMNT.PYMNT-CHECK-DTE TO LEDGER.CNTL-CHECK-DTE
        pdaFcpaldgr.getLedger_Cntrct_Cancel_Rdrw_Actvty_Cde().setValue(ldaFcpl378.getPnd_Rec_Type_10_Detail_Cntrct_Cancel_Rdrw_Actvty_Cde());                             //Natural: MOVE #REC-TYPE-10-DETAIL.CNTRCT-CANCEL-RDRW-ACTVTY-CDE TO LEDGER.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
        DbsUtil.callnat(Fcpnldgr.class , getCurrentProcessState(), pdaFcpaldgr.getLedger_Work_Fields(), pdaFcpaldgr.getLedger());                                         //Natural: CALLNAT 'FCPNLDGR' USING LEDGER-WORK-FIELDS LEDGER
        if (condition(Global.isEscape())) return;
        ldaFcpl123.getPnd_Warrant_Extract_Pnd_Record_Type().setValue(2);                                                                                                  //Natural: MOVE 2 TO #RECORD-TYPE
        pnd_Ws_Pnd_Ledger_Records_Read.nadd(1);                                                                                                                           //Natural: ADD 1 TO #LEDGER-RECORDS-READ
        pnd_Ws_Pnd_Ledger_Read.nadd(pdaFcpaldgr.getLedger_C_Inv_Ledgr());                                                                                                 //Natural: ADD C-INV-LEDGR TO #LEDGER-READ
        FOR02:                                                                                                                                                            //Natural: FOR #SUB = 1 TO C-INV-LEDGR
        for (pnd_Ws_Pnd_Sub.setValue(1); condition(pnd_Ws_Pnd_Sub.lessOrEqual(pdaFcpaldgr.getLedger_C_Inv_Ledgr())); pnd_Ws_Pnd_Sub.nadd(1))
        {
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Cde().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Cde().getValue(pnd_Ws_Pnd_Sub));                                 //Natural: MOVE LEDGER.INV-ACCT-CDE ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-CDE
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Nbr().getValue(pnd_Ws_Pnd_Sub));                     //Natural: MOVE LEDGER.INV-ACCT-LEDGR-NBR ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-NBR
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Amt().getValue(pnd_Ws_Pnd_Sub));                     //Natural: MOVE LEDGER.INV-ACCT-LEDGR-AMT ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-AMT
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Ind().getValue(pnd_Ws_Pnd_Sub));                     //Natural: MOVE LEDGER.INV-ACCT-LEDGR-IND ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-IND
            getWorkFiles().write(2, true, ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Grp1(), ldaFcpl123.getPnd_Warrant_Extract_Pnd_Ledger_Record());                   //Natural: WRITE WORK FILE 2 VARIABLE #WARRANT-GRP1 #LEDGER-RECORD
            pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                           //Natural: ADD 1 TO #RECORDS-WRITTEN
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        if (condition(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl().equals(getZero())))                                                                                       //Natural: IF C-INV-LEDGR-OVRFL = 0
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Ledger_Read.nadd(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl());                                                                                       //Natural: ADD C-INV-LEDGR-OVRFL TO #LEDGER-READ
            FOR03:                                                                                                                                                        //Natural: FOR #SUB = 1 TO C-INV-LEDGR-OVRFL
            for (pnd_Ws_Pnd_Sub.setValue(1); condition(pnd_Ws_Pnd_Sub.lessOrEqual(pdaFcpaldgr.getLedger_C_Inv_Ledgr_Ovrfl())); pnd_Ws_Pnd_Sub.nadd(1))
            {
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Cde().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Cde_Ovrfl().getValue(pnd_Ws_Pnd_Sub));                       //Natural: MOVE LEDGER.INV-ACCT-CDE-OVRFL ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-CDE
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Nbr().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Nbr_Ovrfl().getValue(pnd_Ws_Pnd_Sub));           //Natural: MOVE LEDGER.INV-ACCT-LEDGR-NBR-OVRFL ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-NBR
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Amt().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Amt_Ovrfl().getValue(pnd_Ws_Pnd_Sub));           //Natural: MOVE LEDGER.INV-ACCT-LEDGR-AMT-OVRFL ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-AMT
                ldaFcpl123.getPnd_Warrant_Extract_Pnd_Inv_Acct_Ledgr_Ind().setValue(pdaFcpaldgr.getLedger_Inv_Acct_Ledgr_Ind_Ovrfl().getValue(pnd_Ws_Pnd_Sub));           //Natural: MOVE LEDGER.INV-ACCT-LEDGR-IND-OVRFL ( #SUB ) TO #WARRANT-EXTRACT.#INV-ACCT-LEDGR-IND
                getWorkFiles().write(2, true, ldaFcpl123.getPnd_Warrant_Extract_Pnd_Warrant_Grp1(), ldaFcpl123.getPnd_Warrant_Extract_Pnd_Ledger_Record());               //Natural: WRITE WORK FILE 2 VARIABLE #WARRANT-GRP1 #LEDGER-RECORD
                pnd_Ws_Pnd_Records_Written.nadd(1);                                                                                                                       //Natural: ADD 1 TO #RECORDS-WRITTEN
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Compute_Ovr_Pay_Ded() throws Exception                                                                                                               //Natural: COMPUTE-OVR-PAY-DED
    {
        if (BLNatReinput.isReinput()) return;

        FOR04:                                                                                                                                                            //Natural: FOR #X 1 C-INV-ACCT
        for (pnd_Ws_Pnd_X.setValue(1); condition(pnd_Ws_Pnd_X.lessOrEqual(ldaFcpl123.getPnd_Warrant_Extract_C_Inv_Acct())); pnd_Ws_Pnd_X.nadd(1))
        {
            ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt().getValue(pnd_Ws_Pnd_X).compute(new ComputeParameters(false, ldaFcpl123.getPnd_Warrant_Extract_Pnd_Pnd_Inv_Acct_Ovr_Pay_Ded_Amt().getValue(pnd_Ws_Pnd_X)),  //Natural: ASSIGN ##INV-ACCT-OVR-PAY-DED-AMT ( #X ) := #WARRANT-EXTRACT.INV-ACCT-SETTL-AMT ( #X ) + #WARRANT-EXTRACT.INV-ACCT-DVDND-AMT ( #X ) + #WARRANT-EXTRACT.INV-ACCT-DCI-AMT ( #X ) + #WARRANT-EXTRACT.INV-ACCT-DPI-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-NET-PYMNT-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-FDRL-TAX-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-STATE-TAX-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-LOCAL-TAX-AMT ( #X ) - #WARRANT-EXTRACT.INV-ACCT-EXP-AMT ( #X )
                ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_X).add(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_X)).add(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Pnd_X)).add(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_X)).subtract(ldaFcpl123.getPnd_Warrant_Extract_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Pnd_X)));
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "PS=23 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(55),"WARRANT REPORTING EXTRACT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
}
