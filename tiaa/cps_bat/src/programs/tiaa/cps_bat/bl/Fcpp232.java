/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:21 PM
**        * FROM NATURAL PROGRAM : Fcpp232
************************************************************
**        * FILE NAME            : Fcpp232.java
**        * CLASS NAME           : Fcpp232
**        * INSTANCE NAME        : Fcpp232
************************************************************
************************************************************
* YR2000 COMPLIANT FIX APPLIED -----> JH 07/26/99          *
*                      LAST PCY STOW  ON 09/11/96 11:12:58 *
*                      REVIEWED BY MN ON 09/24/99          *
************************************************************
**Y2CHJH CP CONPAY
*
***********************************************************************
* PROGRAM  : FCPP232
* SYSTEM   : CPS
* AUTHOR   : LEON SILBERSTEIN
* DATE     : MAR 1995
* FUNCTION : THIS PROGRAM CREATES THE "DELETE" FILE, AND A REPORT
*            WHICH CONTAINS ALL THE STOPS AND CANCELS EFECTIVE FOR
*            A SPECIFIED DATE.
*            THE USER IS CASH-RECONCILIATION GROUP.
*
*            FOR J.P. MORGAN
*
* *********************************************************************
* PROGRAM DESCRIPTION:
* *********************************************************************
* THE SELECTION CRITIRIA IS BASED ON THE CS-PYMNT-ACCTG-DTE.
* THE SELECTED RECORDS ARE REPORTED IN CHECK-NUMBER SEQUENCE.
*
* THE CONTROL CARD AND HAS
* THE FORMAT OF "CCYYMMDD" EG 19950331 FOR MARCH 31, 1995.
*
* RESTART:
*            RESTART PROCEDURE FOR THIS PROGRAM IS RERUN.
*
* HISTORY:   G. KOBRYN -  PROCESS ONLY CHECKS ISSUED BEFORE 4/15/96
*             04/10/96    (J.P. MORGAN)
* 4/2017    : JJG - PIN EXPANSION CHANGES
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp232 extends BLNatBase
{
    // Data Areas

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup wrkrec;
    private DbsField wrkrec_Wrkrec_All;

    private DbsGroup wrkrec__R_Field_1;
    private DbsField wrkrec_Pnd_Isn;
    private DbsField wrkrec_Xtrct_Rec_Seq_Nbr;
    private DbsField wrkrec_Pymnt_Check_Nbr;
    private DbsField wrkrec_Pymnt_Check_Dte;
    private DbsField wrkrec_Pymnt_Check_Amt;
    private DbsField wrkrec_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField wrkrec_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField wrkrec_Cnr_Cs_Actvty_Cde;
    private DbsField wrkrec_Cntrct_Unq_Id_Nbr;
    private DbsField wrkrec_Cnr_Rplcmnt_Invrse_Dte;
    private DbsField wrkrec_Cntrct_Orgn_Cde;
    private DbsField wrkrec_Cnr_Rplcmnt_Prcss_Seq_Nbr;
    private DbsField wrkrec_Cntrct_Ppcn_Nbr;
    private DbsField wrkrec_Cntrct_Payee_Cde;
    private DbsField wrkrec_Cntrct_Invrse_Dte;
    private DbsField wrkrec_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup xtdrec;
    private DbsField xtdrec_Rdrw_Acctg_Dte;
    private DbsField xtdrec_Rdrw_Intrfce_Dte;
    private DbsField xtdrec_Rdrw_Check_Nbr;
    private DbsField xtdrec_Rdrw_Check_Amt;
    private DbsField xtdrec_Ph_Name;

    private DbsGroup header_Rec;
    private DbsField header_Rec_Record_Type;
    private DbsField header_Rec_Pos_2_4;
    private DbsField header_Rec_Current_Date;
    private DbsField header_Rec_From_Date;
    private DbsField header_Rec_To_Date;
    private DbsField header_Rec_Filler;

    private DbsGroup detail_Rec;
    private DbsField detail_Rec_Record_Type;
    private DbsField detail_Rec_Check_Number;
    private DbsField detail_Rec_Check_Issue_Date;
    private DbsField detail_Rec_Filler_Zeroes;
    private DbsField detail_Rec_Check_Amount;
    private DbsField detail_Rec_Accounting_Date;
    private DbsField detail_Rec_Filler;

    private DataAccessProgramView vw_rdrw_View;
    private DbsField rdrw_View_Cntrct_Ppcn_Nbr;
    private DbsField rdrw_View_Cntrct_Orgn_Cde;
    private DbsField rdrw_View_Cntrct_Unq_Id_Nbr;
    private DbsField rdrw_View_Cntrct_Invrse_Dte;
    private DbsField rdrw_View_Pymnt_Prcss_Seq_Nbr;
    private DbsField rdrw_View_Pymnt_Stats_Cde;
    private DbsField rdrw_View_Pymnt_Check_Dte;
    private DbsField rdrw_View_Pymnt_Check_Nbr;
    private DbsField rdrw_View_Pymnt_Check_Amt;
    private DbsField rdrw_View_Pymnt_Acctg_Dte;
    private DbsField rdrw_View_Pymnt_Intrfce_Dte;
    private DbsField rdrw_View_Cnr_Cs_Rqust_Dte;
    private DbsField rdrw_View_Cnr_Cs_Rqust_Tme;
    private DbsField rdrw_View_Cnr_Cs_Rqust_User_Id;
    private DbsField rdrw_View_Cnr_Cs_Rqust_Term_Id;
    private DbsField rdrw_View_Cnr_Rdrw_Rqust_Tme;
    private DbsField rdrw_View_Cnr_Rdrw_Rqust_User_Id;
    private DbsField rdrw_View_Cnr_Rdrw_Rqust_Term_Id;
    private DbsField rdrw_View_Cnr_Orgnl_Invrse_Dte;
    private DbsField rdrw_View_Cnr_Orgnl_Prcss_Seq_Nbr;
    private DbsField rdrw_View_Cnr_Rplcmnt_Invrse_Dte;
    private DbsField rdrw_View_Cnr_Rplcmnt_Prcss_Seq_Nbr;
    private DbsField rdrw_View_Cnr_Rdrw_Rqust_Dte;
    private DbsField rdrw_View_Cnr_Orgnl_Pymnt_Acctg_Dte;
    private DbsField rdrw_View_Cnr_Orgnl_Pymnt_Intrfce_Dte;
    private DbsField rdrw_View_Cnr_Cs_Actvty_Cde;
    private DbsField rdrw_View_Cnr_Cs_Pymnt_Intrfce_Dte;
    private DbsField rdrw_View_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField rdrw_View_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField rdrw_View_Cnr_Cs_New_Stop_Ind;
    private DbsField rdrw_View_Cnr_Rdrw_Pymnt_Acctg_Dte;
    private DbsField rdrw_View_Cnr_Rdrw_Pymnt_Intrfce_Dte;

    private DataAccessProgramView vw_pymnt_View;
    private DbsField pymnt_View_Cntrct_Ppcn_Nbr;
    private DbsField pymnt_View_Cntrct_Payee_Cde;
    private DbsField pymnt_View_Cntrct_Orgn_Cde;
    private DbsField pymnt_View_Cntrct_Unq_Id_Nbr;
    private DbsField pymnt_View_Cntrct_Invrse_Dte;
    private DbsField pymnt_View_Pymnt_Stats_Cde;
    private DbsField pymnt_View_Pymnt_Check_Dte;
    private DbsField pymnt_View_Pymnt_Check_Nbr;
    private DbsField pymnt_View_Pymnt_Check_Amt;
    private DbsField pymnt_View_Pymnt_Acctg_Dte;
    private DbsField pymnt_View_Pymnt_Intrfce_Dte;
    private DbsField pymnt_View_Pymnt_Prcss_Seq_Nbr;
    private DbsField pymnt_View_Cnr_Cs_Rqust_Dte;
    private DbsField pymnt_View_Cnr_Cs_Rqust_Tme;
    private DbsField pymnt_View_Cnr_Cs_Rqust_User_Id;
    private DbsField pymnt_View_Cnr_Cs_Rqust_Term_Id;
    private DbsField pymnt_View_Cnr_Rdrw_Rqust_Tme;
    private DbsField pymnt_View_Cnr_Rdrw_Rqust_User_Id;
    private DbsField pymnt_View_Cnr_Rdrw_Rqust_Term_Id;
    private DbsField pymnt_View_Cnr_Orgnl_Invrse_Dte;
    private DbsField pymnt_View_Cnr_Orgnl_Prcss_Seq_Nbr;
    private DbsField pymnt_View_Cnr_Rplcmnt_Invrse_Dte;
    private DbsField pymnt_View_Cnr_Rplcmnt_Prcss_Seq_Nbr;
    private DbsField pymnt_View_Cnr_Rdrw_Rqust_Dte;
    private DbsField pymnt_View_Cnr_Orgnl_Pymnt_Acctg_Dte;
    private DbsField pymnt_View_Cnr_Orgnl_Pymnt_Intrfce_Dte;
    private DbsField pymnt_View_Cnr_Cs_Actvty_Cde;
    private DbsField pymnt_View_Cnr_Cs_Pymnt_Intrfce_Dte;
    private DbsField pymnt_View_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField pymnt_View_Cnr_Cs_Pymnt_Check_Dte;
    private DbsField pymnt_View_Cnr_Cs_New_Stop_Ind;

    private DataAccessProgramView vw_addr_View;
    private DbsField addr_View_Rcrd_Typ;
    private DbsField addr_View_Cntrct_Orgn_Cde;
    private DbsField addr_View_Cntrct_Ppcn_Nbr;
    private DbsField addr_View_Cntrct_Payee_Cde;
    private DbsField addr_View_Cntrct_Invrse_Dte;
    private DbsField addr_View_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup addr_View_Ph_Name;
    private DbsField addr_View_Ph_Last_Name;
    private DbsField addr_View_Ph_First_Name;
    private DbsField addr_View_Ph_Middle_Name;
    private DbsField addr_View_Count_Castpymnt_Nme_And_Addr_Grp;

    private DbsGroup addr_View_Pymnt_Nme_And_Addr_Grp;
    private DbsField addr_View_Pymnt_Nme;
    private DbsField pnd_Program;

    private DbsGroup pnd_Cnts;
    private DbsField pnd_Cnts_Pnd_Cnt_Cancels;
    private DbsField pnd_Cnts_Pnd_Cnt_Stops;
    private DbsField pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop;
    private DbsField pnd_Cnts_Pnd_Cnt_Cancels_Rdrw;
    private DbsField pnd_Cnts_Pnd_Cnt_Stops_Rdrw;
    private DbsField pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop_Rdrw;
    private DbsField pnd_Cnts_Pnd_Cnt_All;
    private DbsField pnd_Cnts_Pnd_Amt_Cancels;
    private DbsField pnd_Cnts_Pnd_Amt_Stops;
    private DbsField pnd_Cnts_Pnd_Amt_Not_Cancel_Stop;
    private DbsField pnd_Cnts_Pnd_Amt_Cancels_Rdrw;
    private DbsField pnd_Cnts_Pnd_Amt_Stops_Rdrw;
    private DbsField pnd_Cnts_Pnd_Amt_Not_Cancel_Stop_Rdrw;
    private DbsField pnd_Cnts_Pnd_Amt_All;
    private DbsField pnd_Cnts_Pnd_Cnt_Rec2;
    private DbsField pnd_Abend_Cde;

    private DbsGroup inp_Parm;
    private DbsField inp_Parm_For_Date;

    private DbsGroup inp_Parm__R_Field_2;
    private DbsField inp_Parm_Filler;
    private DbsField inp_Parm_For_Date_Dd;

    private DbsGroup inp_Parm__R_Field_3;
    private DbsField inp_Parm_For_Date_Cc;
    private DbsField inp_Parm_For_Date_Yymmdd;

    private DbsGroup inp_Parm__R_Field_4;
    private DbsField inp_Parm_For_Date_Yymmdd_A;
    private DbsField pnd_D;
    private DbsField pnd_Ed;

    private DbsGroup pnd_Ed__R_Field_5;
    private DbsField pnd_Ed_Pnd_Ed_Dd;
    private DbsField pnd_Ws_N9_All;

    private DbsGroup pnd_Ws_N9_All__R_Field_6;
    private DbsField pnd_Ws_N9_All_Pnd_Ws_N9_Pos_1_To_7;
    private DbsField pnd_Cnt_Missing_Address;
    private DbsField pnd_Ky_Rdrw;

    private DbsGroup pnd_Ky_Rdrw__R_Field_7;
    private DbsField pnd_Ky_Rdrw_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ky_Rdrw_Cntrct_Invrse_Dte;
    private DbsField pnd_Ky_Rdrw_Cntrct_Orgn_Cde;
    private DbsField pnd_Ky_Rdrw_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ky_Rdrw_Current;
    private DbsField pnd_Ky_Rdrw_Current_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ky_Rdrw_Current_Cntrct_Invrse_Dte;
    private DbsField pnd_Ky_Rdrw_Current_Cntrct_Orgn_Cde;
    private DbsField pnd_Ky_Rdrw_Current_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ky_Rdrw_Current__R_Field_8;
    private DbsField pnd_Ky_Rdrw_Current_Pnd_Ky_Rdrw_Current_Full;

    private DbsGroup pnd_Ck_All;
    private DbsField pnd_Ck_All_Cnr_Cs_Pymnt_Acctg_Dte;
    private DbsField pnd_Ck_All_Pymnt_Check_Nbr;

    private DbsGroup pnd_Ck_All__R_Field_9;
    private DbsField pnd_Ck_All_Pnd_Ck;
    private DbsField pnd_Ek;

    private DbsGroup pnd_Ek__R_Field_10;
    private DbsField pnd_Ek_Pnd_Ek_Acctg_Dte;
    private DbsField pnd_Sk;

    private DbsGroup pnd_Sk__R_Field_11;
    private DbsField pnd_Sk_Pnd_Sk_Acctg_Dte;

    private DbsGroup pnd_Sk_Addr;
    private DbsField pnd_Sk_Addr_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Sk_Addr_Cntrct_Payee_Cde;
    private DbsField pnd_Sk_Addr_Cntrct_Orgn_Cde;
    private DbsField pnd_Sk_Addr_Cntrct_Invrse_Dte;
    private DbsField pnd_Sk_Addr_Pymnt_Prcss_Seq_Nbr_N7;

    private DbsGroup pnd_Sk_Addr__R_Field_12;
    private DbsField pnd_Sk_Addr_Pnd_Sk_Addr_All;

    private DbsGroup pnd_Ck_Addr;
    private DbsField pnd_Ck_Addr_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ck_Addr_Cntrct_Payee_Cde;
    private DbsField pnd_Ck_Addr_Cntrct_Orgn_Cde;
    private DbsField pnd_Ck_Addr_Cntrct_Invrse_Dte;
    private DbsField pnd_Ck_Addr_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ck_Addr__R_Field_13;
    private DbsField pnd_Ck_Addr_Pnd_Ck_Addr_All;
    private DbsField pnd_Check_Datex;

    private DbsGroup pnd_Check_Datex__R_Field_14;
    private DbsField pnd_Check_Datex_Pnd_Check_Date;

    private DbsRecord internalLoopRecord;
    private DbsField sort01Check_AmountSum425;
    private DbsField sort01Check_AmountSum;
    private DbsField sort01Check_AmountSum405;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas

        // Local Variables
        localVariables = new DbsRecord();

        wrkrec = localVariables.newGroupInRecord("wrkrec", "WRKREC");
        wrkrec_Wrkrec_All = wrkrec.newFieldInGroup("wrkrec_Wrkrec_All", "WRKREC-ALL", FieldType.STRING, 108);

        wrkrec__R_Field_1 = localVariables.newGroupInRecord("wrkrec__R_Field_1", "REDEFINE", wrkrec);
        wrkrec_Pnd_Isn = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Pnd_Isn", "#ISN", FieldType.NUMERIC, 10);
        wrkrec_Xtrct_Rec_Seq_Nbr = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Xtrct_Rec_Seq_Nbr", "XTRCT-REC-SEQ-NBR", FieldType.NUMERIC, 7);
        wrkrec_Pymnt_Check_Nbr = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        wrkrec_Pymnt_Check_Dte = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        wrkrec_Pymnt_Check_Amt = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.NUMERIC, 9, 2);
        wrkrec_Cnr_Cs_Pymnt_Acctg_Dte = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE);
        wrkrec_Cnr_Cs_Pymnt_Check_Dte = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", FieldType.DATE);
        wrkrec_Cnr_Cs_Actvty_Cde = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 1);
        wrkrec_Cntrct_Unq_Id_Nbr = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        wrkrec_Cnr_Rplcmnt_Invrse_Dte = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cnr_Rplcmnt_Invrse_Dte", "CNR-RPLCMNT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        wrkrec_Cntrct_Orgn_Cde = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        wrkrec_Cnr_Rplcmnt_Prcss_Seq_Nbr = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cnr_Rplcmnt_Prcss_Seq_Nbr", "CNR-RPLCMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);
        wrkrec_Cntrct_Ppcn_Nbr = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        wrkrec_Cntrct_Payee_Cde = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        wrkrec_Cntrct_Invrse_Dte = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        wrkrec_Pymnt_Prcss_Seq_Nbr = wrkrec__R_Field_1.newFieldInGroup("wrkrec_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        xtdrec = localVariables.newGroupInRecord("xtdrec", "XTDREC");
        xtdrec_Rdrw_Acctg_Dte = xtdrec.newFieldInGroup("xtdrec_Rdrw_Acctg_Dte", "RDRW-ACCTG-DTE", FieldType.DATE);
        xtdrec_Rdrw_Intrfce_Dte = xtdrec.newFieldInGroup("xtdrec_Rdrw_Intrfce_Dte", "RDRW-INTRFCE-DTE", FieldType.DATE);
        xtdrec_Rdrw_Check_Nbr = xtdrec.newFieldInGroup("xtdrec_Rdrw_Check_Nbr", "RDRW-CHECK-NBR", FieldType.NUMERIC, 7);
        xtdrec_Rdrw_Check_Amt = xtdrec.newFieldInGroup("xtdrec_Rdrw_Check_Amt", "RDRW-CHECK-AMT", FieldType.NUMERIC, 9, 2);
        xtdrec_Ph_Name = xtdrec.newFieldInGroup("xtdrec_Ph_Name", "PH-NAME", FieldType.STRING, 35);

        header_Rec = localVariables.newGroupInRecord("header_Rec", "HEADER-REC");
        header_Rec_Record_Type = header_Rec.newFieldInGroup("header_Rec_Record_Type", "RECORD-TYPE", FieldType.STRING, 1);
        header_Rec_Pos_2_4 = header_Rec.newFieldInGroup("header_Rec_Pos_2_4", "POS-2-4", FieldType.STRING, 3);
        header_Rec_Current_Date = header_Rec.newFieldInGroup("header_Rec_Current_Date", "CURRENT-DATE", FieldType.STRING, 8);
        header_Rec_From_Date = header_Rec.newFieldInGroup("header_Rec_From_Date", "FROM-DATE", FieldType.STRING, 6);
        header_Rec_To_Date = header_Rec.newFieldInGroup("header_Rec_To_Date", "TO-DATE", FieldType.STRING, 6);
        header_Rec_Filler = header_Rec.newFieldInGroup("header_Rec_Filler", "FILLER", FieldType.STRING, 56);

        detail_Rec = localVariables.newGroupInRecord("detail_Rec", "DETAIL-REC");
        detail_Rec_Record_Type = detail_Rec.newFieldInGroup("detail_Rec_Record_Type", "RECORD-TYPE", FieldType.STRING, 1);
        detail_Rec_Check_Number = detail_Rec.newFieldInGroup("detail_Rec_Check_Number", "CHECK-NUMBER", FieldType.NUMERIC, 8);
        detail_Rec_Check_Issue_Date = detail_Rec.newFieldInGroup("detail_Rec_Check_Issue_Date", "CHECK-ISSUE-DATE", FieldType.STRING, 6);
        detail_Rec_Filler_Zeroes = detail_Rec.newFieldInGroup("detail_Rec_Filler_Zeroes", "FILLER-ZEROES", FieldType.NUMERIC, 14);
        detail_Rec_Check_Amount = detail_Rec.newFieldInGroup("detail_Rec_Check_Amount", "CHECK-AMOUNT", FieldType.NUMERIC, 9, 2);
        detail_Rec_Accounting_Date = detail_Rec.newFieldInGroup("detail_Rec_Accounting_Date", "ACCOUNTING-DATE", FieldType.STRING, 6);
        detail_Rec_Filler = detail_Rec.newFieldInGroup("detail_Rec_Filler", "FILLER", FieldType.STRING, 36);

        vw_rdrw_View = new DataAccessProgramView(new NameInfo("vw_rdrw_View", "RDRW-VIEW"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        rdrw_View_Cntrct_Ppcn_Nbr = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        rdrw_View_Cntrct_Orgn_Cde = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        rdrw_View_Cntrct_Unq_Id_Nbr = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        rdrw_View_Cntrct_Invrse_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        rdrw_View_Pymnt_Prcss_Seq_Nbr = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        rdrw_View_Pymnt_Stats_Cde = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        rdrw_View_Pymnt_Check_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        rdrw_View_Pymnt_Check_Nbr = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_NBR");
        rdrw_View_Pymnt_Check_Amt = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        rdrw_View_Pymnt_Acctg_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_ACCTG_DTE");
        rdrw_View_Pymnt_Intrfce_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_INTRFCE_DTE");
        rdrw_View_Cnr_Cs_Rqust_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNR_CS_RQUST_DTE");
        rdrw_View_Cnr_Cs_Rqust_Tme = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 7, 
            RepeatingFieldStrategy.None, "CNR_CS_RQUST_TME");
        rdrw_View_Cnr_Cs_Rqust_User_Id = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_USER_ID");
        rdrw_View_Cnr_Cs_Rqust_Term_Id = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_Rqust_Term_Id", "CNR-CS-RQUST-TERM-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_TERM_ID");
        rdrw_View_Cnr_Rdrw_Rqust_Tme = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Rdrw_Rqust_Tme", "CNR-RDRW-RQUST-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_TME");
        rdrw_View_Cnr_Rdrw_Rqust_User_Id = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Rdrw_Rqust_User_Id", "CNR-RDRW-RQUST-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_USER_ID");
        rdrw_View_Cnr_Rdrw_Rqust_Term_Id = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Rdrw_Rqust_Term_Id", "CNR-RDRW-RQUST-TERM-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_TERM_ID");
        rdrw_View_Cnr_Orgnl_Invrse_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNR_ORGNL_INVRSE_DTE");
        rdrw_View_Cnr_Orgnl_Prcss_Seq_Nbr = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Orgnl_Prcss_Seq_Nbr", "CNR-ORGNL-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "CNR_ORGNL_PRCSS_SEQ_NBR");
        rdrw_View_Cnr_Rplcmnt_Invrse_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Rplcmnt_Invrse_Dte", "CNR-RPLCMNT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNR_RPLCMNT_INVRSE_DTE");
        rdrw_View_Cnr_Rplcmnt_Prcss_Seq_Nbr = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Rplcmnt_Prcss_Seq_Nbr", "CNR-RPLCMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNR_RPLCMNT_PRCSS_SEQ_NBR");
        rdrw_View_Cnr_Rdrw_Rqust_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Rdrw_Rqust_Dte", "CNR-RDRW-RQUST-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_DTE");
        rdrw_View_Cnr_Orgnl_Pymnt_Acctg_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Orgnl_Pymnt_Acctg_Dte", "CNR-ORGNL-PYMNT-ACCTG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_ORGNL_PYMNT_ACCTG_DTE");
        rdrw_View_Cnr_Orgnl_Pymnt_Intrfce_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Orgnl_Pymnt_Intrfce_Dte", "CNR-ORGNL-PYMNT-INTRFCE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_ORGNL_PYMNT_INTRFCE_DTE");
        rdrw_View_Cnr_Cs_Actvty_Cde = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 1, 
            RepeatingFieldStrategy.None, "CNR_CS_ACTVTY_CDE");
        rdrw_View_Cnr_Cs_Pymnt_Intrfce_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_Pymnt_Intrfce_Dte", "CNR-CS-PYMNT-INTRFCE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_INTRFCE_DTE");
        rdrw_View_Cnr_Cs_Pymnt_Acctg_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_PYMNT_ACCTG_DTE");
        rdrw_View_Cnr_Cs_Pymnt_Check_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_PYMNT_CHECK_DTE");
        rdrw_View_Cnr_Cs_New_Stop_Ind = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNR_CS_NEW_STOP_IND");
        rdrw_View_Cnr_Rdrw_Pymnt_Acctg_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Rdrw_Pymnt_Acctg_Dte", "CNR-RDRW-PYMNT-ACCTG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_RDRW_PYMNT_ACCTG_DTE");
        rdrw_View_Cnr_Rdrw_Pymnt_Intrfce_Dte = vw_rdrw_View.getRecord().newFieldInGroup("rdrw_View_Cnr_Rdrw_Pymnt_Intrfce_Dte", "CNR-RDRW-PYMNT-INTRFCE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_RDRW_PYMNT_INTRFCE_DTE");
        registerRecord(vw_rdrw_View);

        vw_pymnt_View = new DataAccessProgramView(new NameInfo("vw_pymnt_View", "PYMNT-VIEW"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        pymnt_View_Cntrct_Ppcn_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, 
            RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        pymnt_View_Cntrct_Payee_Cde = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, 
            RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        pymnt_View_Cntrct_Orgn_Cde = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        pymnt_View_Cntrct_Unq_Id_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        pymnt_View_Cntrct_Invrse_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        pymnt_View_Pymnt_Stats_Cde = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "PYMNT_STATS_CDE");
        pymnt_View_Pymnt_Check_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_CHECK_DTE");
        pymnt_View_Pymnt_Check_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7, 
            RepeatingFieldStrategy.None, "PYMNT_CHECK_NBR");
        pymnt_View_Pymnt_Check_Amt = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, RepeatingFieldStrategy.None, "PYMNT_CHECK_AMT");
        pymnt_View_Pymnt_Acctg_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "PYMNT_ACCTG_DTE");
        pymnt_View_Pymnt_Intrfce_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "PYMNT_INTRFCE_DTE");
        pymnt_View_Pymnt_Prcss_Seq_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");
        pymnt_View_Cnr_Cs_Rqust_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_Rqust_Dte", "CNR-CS-RQUST-DTE", FieldType.DATE, RepeatingFieldStrategy.None, 
            "CNR_CS_RQUST_DTE");
        pymnt_View_Cnr_Cs_Rqust_Tme = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_Rqust_Tme", "CNR-CS-RQUST-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "CNR_CS_RQUST_TME");
        pymnt_View_Cnr_Cs_Rqust_User_Id = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_Rqust_User_Id", "CNR-CS-RQUST-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_USER_ID");
        pymnt_View_Cnr_Cs_Rqust_Term_Id = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_Rqust_Term_Id", "CNR-CS-RQUST-TERM-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_CS_RQUST_TERM_ID");
        pymnt_View_Cnr_Rdrw_Rqust_Tme = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Rdrw_Rqust_Tme", "CNR-RDRW-RQUST-TME", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_TME");
        pymnt_View_Cnr_Rdrw_Rqust_User_Id = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Rdrw_Rqust_User_Id", "CNR-RDRW-RQUST-USER-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_USER_ID");
        pymnt_View_Cnr_Rdrw_Rqust_Term_Id = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Rdrw_Rqust_Term_Id", "CNR-RDRW-RQUST-TERM-ID", FieldType.STRING, 
            8, RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_TERM_ID");
        pymnt_View_Cnr_Orgnl_Invrse_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Orgnl_Invrse_Dte", "CNR-ORGNL-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNR_ORGNL_INVRSE_DTE");
        pymnt_View_Cnr_Orgnl_Prcss_Seq_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Orgnl_Prcss_Seq_Nbr", "CNR-ORGNL-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNR_ORGNL_PRCSS_SEQ_NBR");
        pymnt_View_Cnr_Rplcmnt_Invrse_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Rplcmnt_Invrse_Dte", "CNR-RPLCMNT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNR_RPLCMNT_INVRSE_DTE");
        pymnt_View_Cnr_Rplcmnt_Prcss_Seq_Nbr = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Rplcmnt_Prcss_Seq_Nbr", "CNR-RPLCMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9, RepeatingFieldStrategy.None, "CNR_RPLCMNT_PRCSS_SEQ_NBR");
        pymnt_View_Cnr_Rdrw_Rqust_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Rdrw_Rqust_Dte", "CNR-RDRW-RQUST-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_RDRW_RQUST_DTE");
        pymnt_View_Cnr_Orgnl_Pymnt_Acctg_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Orgnl_Pymnt_Acctg_Dte", "CNR-ORGNL-PYMNT-ACCTG-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_ORGNL_PYMNT_ACCTG_DTE");
        pymnt_View_Cnr_Orgnl_Pymnt_Intrfce_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Orgnl_Pymnt_Intrfce_Dte", "CNR-ORGNL-PYMNT-INTRFCE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_ORGNL_PYMNT_INTRFCE_DTE");
        pymnt_View_Cnr_Cs_Actvty_Cde = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_Actvty_Cde", "CNR-CS-ACTVTY-CDE", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNR_CS_ACTVTY_CDE");
        pymnt_View_Cnr_Cs_Pymnt_Intrfce_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_Pymnt_Intrfce_Dte", "CNR-CS-PYMNT-INTRFCE-DTE", 
            FieldType.DATE, RepeatingFieldStrategy.None, "CNR_CS_PYMNT_INTRFCE_DTE");
        pymnt_View_Cnr_Cs_Pymnt_Acctg_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_PYMNT_ACCTG_DTE");
        pymnt_View_Cnr_Cs_Pymnt_Check_Dte = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_Pymnt_Check_Dte", "CNR-CS-PYMNT-CHECK-DTE", FieldType.DATE, 
            RepeatingFieldStrategy.None, "CNR_CS_PYMNT_CHECK_DTE");
        pymnt_View_Cnr_Cs_New_Stop_Ind = vw_pymnt_View.getRecord().newFieldInGroup("pymnt_View_Cnr_Cs_New_Stop_Ind", "CNR-CS-NEW-STOP-IND", FieldType.STRING, 
            1, RepeatingFieldStrategy.None, "CNR_CS_NEW_STOP_IND");
        registerRecord(vw_pymnt_View);

        vw_addr_View = new DataAccessProgramView(new NameInfo("vw_addr_View", "ADDR-VIEW"), "FCP_CONS_ADDR", "FCP_CONS_LEDGR", DdmPeriodicGroups.getInstance().getGroups("FCP_CONS_ADDR"));
        addr_View_Rcrd_Typ = vw_addr_View.getRecord().newFieldInGroup("addr_View_Rcrd_Typ", "RCRD-TYP", FieldType.STRING, 1, RepeatingFieldStrategy.None, 
            "RCRD_TYP");
        addr_View_Cntrct_Orgn_Cde = vw_addr_View.getRecord().newFieldInGroup("addr_View_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2, RepeatingFieldStrategy.None, 
            "CNTRCT_ORGN_CDE");
        addr_View_Cntrct_Ppcn_Nbr = vw_addr_View.getRecord().newFieldInGroup("addr_View_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "CNTRCT_PPCN_NBR");
        addr_View_Cntrct_Payee_Cde = vw_addr_View.getRecord().newFieldInGroup("addr_View_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4, RepeatingFieldStrategy.None, 
            "CNTRCT_PAYEE_CDE");
        addr_View_Cntrct_Invrse_Dte = vw_addr_View.getRecord().newFieldInGroup("addr_View_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        addr_View_Pymnt_Prcss_Seq_Nbr = vw_addr_View.getRecord().newFieldInGroup("addr_View_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            7, RepeatingFieldStrategy.None, "PYMNT_PRCSS_SEQ_NBR");

        addr_View_Ph_Name = vw_addr_View.getRecord().newGroupInGroup("ADDR_VIEW_PH_NAME", "PH-NAME");
        addr_View_Ph_Last_Name = addr_View_Ph_Name.newFieldInGroup("addr_View_Ph_Last_Name", "PH-LAST-NAME", FieldType.STRING, 16, RepeatingFieldStrategy.None, 
            "PH_LAST_NAME");
        addr_View_Ph_First_Name = addr_View_Ph_Name.newFieldInGroup("addr_View_Ph_First_Name", "PH-FIRST-NAME", FieldType.STRING, 10, RepeatingFieldStrategy.None, 
            "PH_FIRST_NAME");
        addr_View_Ph_Middle_Name = addr_View_Ph_Name.newFieldInGroup("addr_View_Ph_Middle_Name", "PH-MIDDLE-NAME", FieldType.STRING, 12, RepeatingFieldStrategy.None, 
            "PH_MIDDLE_NAME");
        addr_View_Count_Castpymnt_Nme_And_Addr_Grp = vw_addr_View.getRecord().newFieldInGroup("addr_View_Count_Castpymnt_Nme_And_Addr_Grp", "C*PYMNT-NME-AND-ADDR-GRP", 
            RepeatingFieldStrategy.CAsteriskVariable, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");

        addr_View_Pymnt_Nme_And_Addr_Grp = vw_addr_View.getRecord().newGroupInGroup("addr_View_Pymnt_Nme_And_Addr_Grp", "PYMNT-NME-AND-ADDR-GRP", null, 
            RepeatingFieldStrategy.PeriodicGroupFieldArray, "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        addr_View_Pymnt_Nme = addr_View_Pymnt_Nme_And_Addr_Grp.newFieldArrayInGroup("addr_View_Pymnt_Nme", "PYMNT-NME", FieldType.STRING, 38, new DbsArrayController(1, 
            2) , RepeatingFieldStrategy.PeriodicGroupFieldArray, "PYMNT_NME", "FCP_CONS_LEDGR_PYMNT_NME_AND_ADDR_GRP");
        registerRecord(vw_addr_View);

        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);

        pnd_Cnts = localVariables.newGroupInRecord("pnd_Cnts", "#CNTS");
        pnd_Cnts_Pnd_Cnt_Cancels = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Cnt_Cancels", "#CNT-CANCELS", FieldType.PACKED_DECIMAL, 5);
        pnd_Cnts_Pnd_Cnt_Stops = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Cnt_Stops", "#CNT-STOPS", FieldType.PACKED_DECIMAL, 5);
        pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop", "#CNT-NOT-CANCEL-STOP", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Cnts_Pnd_Cnt_Cancels_Rdrw = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Cnt_Cancels_Rdrw", "#CNT-CANCELS-RDRW", FieldType.PACKED_DECIMAL, 5);
        pnd_Cnts_Pnd_Cnt_Stops_Rdrw = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Cnt_Stops_Rdrw", "#CNT-STOPS-RDRW", FieldType.PACKED_DECIMAL, 5);
        pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop_Rdrw = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop_Rdrw", "#CNT-NOT-CANCEL-STOP-RDRW", FieldType.PACKED_DECIMAL, 
            5);
        pnd_Cnts_Pnd_Cnt_All = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Cnt_All", "#CNT-ALL", FieldType.PACKED_DECIMAL, 5);
        pnd_Cnts_Pnd_Amt_Cancels = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Amt_Cancels", "#AMT-CANCELS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cnts_Pnd_Amt_Stops = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Amt_Stops", "#AMT-STOPS", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cnts_Pnd_Amt_Not_Cancel_Stop = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Amt_Not_Cancel_Stop", "#AMT-NOT-CANCEL-STOP", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Cnts_Pnd_Amt_Cancels_Rdrw = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Amt_Cancels_Rdrw", "#AMT-CANCELS-RDRW", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cnts_Pnd_Amt_Stops_Rdrw = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Amt_Stops_Rdrw", "#AMT-STOPS-RDRW", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cnts_Pnd_Amt_Not_Cancel_Stop_Rdrw = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Amt_Not_Cancel_Stop_Rdrw", "#AMT-NOT-CANCEL-STOP-RDRW", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Cnts_Pnd_Amt_All = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Amt_All", "#AMT-ALL", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Cnts_Pnd_Cnt_Rec2 = pnd_Cnts.newFieldInGroup("pnd_Cnts_Pnd_Cnt_Rec2", "#CNT-REC2", FieldType.PACKED_DECIMAL, 5);
        pnd_Abend_Cde = localVariables.newFieldInRecord("pnd_Abend_Cde", "#ABEND-CDE", FieldType.NUMERIC, 3);

        inp_Parm = localVariables.newGroupInRecord("inp_Parm", "INP-PARM");
        inp_Parm_For_Date = inp_Parm.newFieldInGroup("inp_Parm_For_Date", "FOR-DATE", FieldType.STRING, 8);

        inp_Parm__R_Field_2 = inp_Parm.newGroupInGroup("inp_Parm__R_Field_2", "REDEFINE", inp_Parm_For_Date);
        inp_Parm_Filler = inp_Parm__R_Field_2.newFieldInGroup("inp_Parm_Filler", "FILLER", FieldType.STRING, 6);
        inp_Parm_For_Date_Dd = inp_Parm__R_Field_2.newFieldInGroup("inp_Parm_For_Date_Dd", "FOR-DATE-DD", FieldType.STRING, 2);

        inp_Parm__R_Field_3 = inp_Parm.newGroupInGroup("inp_Parm__R_Field_3", "REDEFINE", inp_Parm_For_Date);
        inp_Parm_For_Date_Cc = inp_Parm__R_Field_3.newFieldInGroup("inp_Parm_For_Date_Cc", "FOR-DATE-CC", FieldType.NUMERIC, 2);
        inp_Parm_For_Date_Yymmdd = inp_Parm__R_Field_3.newFieldInGroup("inp_Parm_For_Date_Yymmdd", "FOR-DATE-YYMMDD", FieldType.NUMERIC, 6);

        inp_Parm__R_Field_4 = inp_Parm__R_Field_3.newGroupInGroup("inp_Parm__R_Field_4", "REDEFINE", inp_Parm_For_Date_Yymmdd);
        inp_Parm_For_Date_Yymmdd_A = inp_Parm__R_Field_4.newFieldInGroup("inp_Parm_For_Date_Yymmdd_A", "FOR-DATE-YYMMDD-A", FieldType.STRING, 6);
        pnd_D = localVariables.newFieldInRecord("pnd_D", "#D", FieldType.DATE);
        pnd_Ed = localVariables.newFieldInRecord("pnd_Ed", "#ED", FieldType.STRING, 8);

        pnd_Ed__R_Field_5 = localVariables.newGroupInRecord("pnd_Ed__R_Field_5", "REDEFINE", pnd_Ed);
        pnd_Ed_Pnd_Ed_Dd = pnd_Ed__R_Field_5.newFieldInGroup("pnd_Ed_Pnd_Ed_Dd", "#ED-DD", FieldType.STRING, 2);
        pnd_Ws_N9_All = localVariables.newFieldInRecord("pnd_Ws_N9_All", "#WS-N9-ALL", FieldType.NUMERIC, 9);

        pnd_Ws_N9_All__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_N9_All__R_Field_6", "REDEFINE", pnd_Ws_N9_All);
        pnd_Ws_N9_All_Pnd_Ws_N9_Pos_1_To_7 = pnd_Ws_N9_All__R_Field_6.newFieldInGroup("pnd_Ws_N9_All_Pnd_Ws_N9_Pos_1_To_7", "#WS-N9-POS-1-TO-7", FieldType.NUMERIC, 
            7);
        pnd_Cnt_Missing_Address = localVariables.newFieldInRecord("pnd_Cnt_Missing_Address", "#CNT-MISSING-ADDRESS", FieldType.PACKED_DECIMAL, 5);
        pnd_Ky_Rdrw = localVariables.newFieldInRecord("pnd_Ky_Rdrw", "#KY-RDRW", FieldType.STRING, 31);

        pnd_Ky_Rdrw__R_Field_7 = localVariables.newGroupInRecord("pnd_Ky_Rdrw__R_Field_7", "REDEFINE", pnd_Ky_Rdrw);
        pnd_Ky_Rdrw_Cntrct_Unq_Id_Nbr = pnd_Ky_Rdrw__R_Field_7.newFieldInGroup("pnd_Ky_Rdrw_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Ky_Rdrw_Cntrct_Invrse_Dte = pnd_Ky_Rdrw__R_Field_7.newFieldInGroup("pnd_Ky_Rdrw_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ky_Rdrw_Cntrct_Orgn_Cde = pnd_Ky_Rdrw__R_Field_7.newFieldInGroup("pnd_Ky_Rdrw_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ky_Rdrw_Pymnt_Prcss_Seq_Nbr = pnd_Ky_Rdrw__R_Field_7.newFieldInGroup("pnd_Ky_Rdrw_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);

        pnd_Ky_Rdrw_Current = localVariables.newGroupInRecord("pnd_Ky_Rdrw_Current", "#KY-RDRW-CURRENT");
        pnd_Ky_Rdrw_Current_Cntrct_Unq_Id_Nbr = pnd_Ky_Rdrw_Current.newFieldInGroup("pnd_Ky_Rdrw_Current_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12);
        pnd_Ky_Rdrw_Current_Cntrct_Invrse_Dte = pnd_Ky_Rdrw_Current.newFieldInGroup("pnd_Ky_Rdrw_Current_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8);
        pnd_Ky_Rdrw_Current_Cntrct_Orgn_Cde = pnd_Ky_Rdrw_Current.newFieldInGroup("pnd_Ky_Rdrw_Current_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 
            2);
        pnd_Ky_Rdrw_Current_Pymnt_Prcss_Seq_Nbr = pnd_Ky_Rdrw_Current.newFieldInGroup("pnd_Ky_Rdrw_Current_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);

        pnd_Ky_Rdrw_Current__R_Field_8 = localVariables.newGroupInRecord("pnd_Ky_Rdrw_Current__R_Field_8", "REDEFINE", pnd_Ky_Rdrw_Current);
        pnd_Ky_Rdrw_Current_Pnd_Ky_Rdrw_Current_Full = pnd_Ky_Rdrw_Current__R_Field_8.newFieldInGroup("pnd_Ky_Rdrw_Current_Pnd_Ky_Rdrw_Current_Full", 
            "#KY-RDRW-CURRENT-FULL", FieldType.STRING, 26);

        pnd_Ck_All = localVariables.newGroupInRecord("pnd_Ck_All", "#CK-ALL");
        pnd_Ck_All_Cnr_Cs_Pymnt_Acctg_Dte = pnd_Ck_All.newFieldInGroup("pnd_Ck_All_Cnr_Cs_Pymnt_Acctg_Dte", "CNR-CS-PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Ck_All_Pymnt_Check_Nbr = pnd_Ck_All.newFieldInGroup("pnd_Ck_All_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);

        pnd_Ck_All__R_Field_9 = localVariables.newGroupInRecord("pnd_Ck_All__R_Field_9", "REDEFINE", pnd_Ck_All);
        pnd_Ck_All_Pnd_Ck = pnd_Ck_All__R_Field_9.newFieldInGroup("pnd_Ck_All_Pnd_Ck", "#CK", FieldType.BINARY, 11);
        pnd_Ek = localVariables.newFieldInRecord("pnd_Ek", "#EK", FieldType.BINARY, 11);

        pnd_Ek__R_Field_10 = localVariables.newGroupInRecord("pnd_Ek__R_Field_10", "REDEFINE", pnd_Ek);
        pnd_Ek_Pnd_Ek_Acctg_Dte = pnd_Ek__R_Field_10.newFieldInGroup("pnd_Ek_Pnd_Ek_Acctg_Dte", "#EK-ACCTG-DTE", FieldType.DATE);
        pnd_Sk = localVariables.newFieldInRecord("pnd_Sk", "#SK", FieldType.BINARY, 11);

        pnd_Sk__R_Field_11 = localVariables.newGroupInRecord("pnd_Sk__R_Field_11", "REDEFINE", pnd_Sk);
        pnd_Sk_Pnd_Sk_Acctg_Dte = pnd_Sk__R_Field_11.newFieldInGroup("pnd_Sk_Pnd_Sk_Acctg_Dte", "#SK-ACCTG-DTE", FieldType.DATE);

        pnd_Sk_Addr = localVariables.newGroupInRecord("pnd_Sk_Addr", "#SK-ADDR");
        pnd_Sk_Addr_Cntrct_Ppcn_Nbr = pnd_Sk_Addr.newFieldInGroup("pnd_Sk_Addr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Sk_Addr_Cntrct_Payee_Cde = pnd_Sk_Addr.newFieldInGroup("pnd_Sk_Addr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Sk_Addr_Cntrct_Orgn_Cde = pnd_Sk_Addr.newFieldInGroup("pnd_Sk_Addr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Sk_Addr_Cntrct_Invrse_Dte = pnd_Sk_Addr.newFieldInGroup("pnd_Sk_Addr_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Sk_Addr_Pymnt_Prcss_Seq_Nbr_N7 = pnd_Sk_Addr.newFieldInGroup("pnd_Sk_Addr_Pymnt_Prcss_Seq_Nbr_N7", "PYMNT-PRCSS-SEQ-NBR-N7", FieldType.NUMERIC, 
            7);

        pnd_Sk_Addr__R_Field_12 = localVariables.newGroupInRecord("pnd_Sk_Addr__R_Field_12", "REDEFINE", pnd_Sk_Addr);
        pnd_Sk_Addr_Pnd_Sk_Addr_All = pnd_Sk_Addr__R_Field_12.newFieldInGroup("pnd_Sk_Addr_Pnd_Sk_Addr_All", "#SK-ADDR-ALL", FieldType.STRING, 31);

        pnd_Ck_Addr = localVariables.newGroupInRecord("pnd_Ck_Addr", "#CK-ADDR");
        pnd_Ck_Addr_Cntrct_Ppcn_Nbr = pnd_Ck_Addr.newFieldInGroup("pnd_Ck_Addr_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Ck_Addr_Cntrct_Payee_Cde = pnd_Ck_Addr.newFieldInGroup("pnd_Ck_Addr_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Ck_Addr_Cntrct_Orgn_Cde = pnd_Ck_Addr.newFieldInGroup("pnd_Ck_Addr_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ck_Addr_Cntrct_Invrse_Dte = pnd_Ck_Addr.newFieldInGroup("pnd_Ck_Addr_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 8);
        pnd_Ck_Addr_Pymnt_Prcss_Seq_Nbr = pnd_Ck_Addr.newFieldInGroup("pnd_Ck_Addr_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 7);

        pnd_Ck_Addr__R_Field_13 = localVariables.newGroupInRecord("pnd_Ck_Addr__R_Field_13", "REDEFINE", pnd_Ck_Addr);
        pnd_Ck_Addr_Pnd_Ck_Addr_All = pnd_Ck_Addr__R_Field_13.newFieldInGroup("pnd_Ck_Addr_Pnd_Ck_Addr_All", "#CK-ADDR-ALL", FieldType.STRING, 31);
        pnd_Check_Datex = localVariables.newFieldInRecord("pnd_Check_Datex", "#CHECK-DATEX", FieldType.STRING, 8);

        pnd_Check_Datex__R_Field_14 = localVariables.newGroupInRecord("pnd_Check_Datex__R_Field_14", "REDEFINE", pnd_Check_Datex);
        pnd_Check_Datex_Pnd_Check_Date = pnd_Check_Datex__R_Field_14.newFieldInGroup("pnd_Check_Datex_Pnd_Check_Date", "#CHECK-DATE", FieldType.NUMERIC, 
            8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        sort01Check_AmountSum425 = internalLoopRecord.newFieldInRecord("Sort01_Check_Amount_SUM_425", "Check_Amount_SUM_425", FieldType.NUMERIC, 9, 2);
        sort01Check_AmountSum = internalLoopRecord.newFieldInRecord("Sort01_Check_Amount_SUM", "Check_Amount_SUM", FieldType.NUMERIC, 9, 2);
        sort01Check_AmountSum405 = internalLoopRecord.newFieldInRecord("Sort01_Check_Amount_SUM_405", "Check_Amount_SUM_405", FieldType.NUMERIC, 9, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_rdrw_View.reset();
        vw_pymnt_View.reset();
        vw_addr_View.reset();
        internalLoopRecord.reset();

        localVariables.reset();
        header_Rec_Record_Type.setInitialValue("1");
        header_Rec_Pos_2_4.setInitialValue("017");
        detail_Rec_Record_Type.setInitialValue("2");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
        setSort(new DbsSort(getWorkFiles()));
    }

    // Constructor(s)
    public Fcpp232() throws Exception
    {
        super("Fcpp232");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp232|Main");
        setupReports();
        while(true)
        {
            try
            {
                //*  .............. DEFINE PRINTERS AND FORMATS
                getReports().definePrinter(2, "RPT1");                                                                                                                    //Natural: DEFINE PRINTER ( RPT1 = 1 )
                //*  .............. ENVIRONMENTAL INITIALIZATIONS                                                                                                         //Natural: FORMAT ( RPT1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
                pnd_Program.setValue(Global.getPROGRAM());                                                                                                                //Natural: ASSIGN #PROGRAM = *PROGRAM
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: ASSIGN *ERROR-TA = 'INFP9000'
                if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                        //Natural: IF *DEVICE = 'BATCH' THEN
                {
                    //*  SET DELIMITER MODE FOR BATCH INPUT
                    setControl("D");                                                                                                                                      //Natural: SET CONTROL 'D'
                }                                                                                                                                                         //Natural: END-IF
                //*  ......... SETUP-START-AND-END-DATES
                //* *Y2NCJH
                DbsUtil.invokeInput(setInputStatus(INPUT_1), this, "AcctgDte for DELETE FILE(YYYYMMDD):",inp_Parm_For_Date, new SignPosition (false));                    //Natural: INPUT 'AcctgDte for DELETE FILE(YYYYMMDD):' INP-PARM.FOR-DATE ( SG = OFF )
                //* *Y2NCJH
                pnd_D.setValueEdited(new ReportEditMask("YYYYMMDD"),inp_Parm_For_Date);                                                                                   //Natural: MOVE EDITED INP-PARM.FOR-DATE TO #D ( EM = YYYYMMDD )
                //* *Y2NCJH  D     D
                pnd_Sk_Pnd_Sk_Acctg_Dte.setValue(pnd_D);                                                                                                                  //Natural: ASSIGN #SK-ACCTG-DTE := #D
                //* *Y2NCJH  D
                pnd_Ek_Pnd_Ek_Acctg_Dte.setValue(pnd_D);                                                                                                                  //Natural: ASSIGN #EK-ACCTG-DTE := #D
                //* *Y2NCJH
                getReports().write(0, Global.getPROGRAM(),"Start Of Run At:",Global.getDATU(),"-",Global.getTIME(),NEWLINE,"Create DELETE FILE For Accounting Date parameter:", //Natural: WRITE *PROGRAM 'Start Of Run At:' *DATU '-' *TIME / 'Create DELETE FILE For Accounting Date parameter:' INP-PARM.FOR-DATE
                    inp_Parm_For_Date);
                if (Global.isEscape()) return;
                //* *Y2NCJH
                //*  START KEY
                                                                                                                                                                          //Natural: PERFORM CREATE-HEADER-REC
                sub_Create_Header_Rec();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  ---------------------------------------------------------------------
                //*  SELECT ALL THE CANCELS AND STOPS THAT ARE FOR
                //*  THE REQUESTED ACCOUNTING MONTH
                //*  ---------------------------------------------------------------------
                //* *Y2NCJH
                vw_pymnt_View.startDatabaseRead                                                                                                                           //Natural: READ PYMNT-VIEW BY CS-ACCTG-DTE-CHECK-NBR STARTING FROM #SK
                (
                "RD_CS",
                new Wc[] { new Wc("CS_ACCTG_DTE_CHECK_NBR", ">=", pnd_Sk.getBinary(), WcType.BY) },
                new Oc[] { new Oc("CS_ACCTG_DTE_CHECK_NBR", "ASC") }
                );
                RD_CS:
                while (condition(vw_pymnt_View.readNextRow("RD_CS")))
                {
                    //* *Y2NCJH   CNR-CS-PYMNT-ACCTG-DTE(D) IS PART OF THIS SUPER
                    //* *Y2NCJH          B11
                    //* *
                    //* *  FOR J.P. MORGAN
                    //* *     BYPASS ALL CHECKS ISSUED 4/15/96 OR AFTER
                    //* *
                    //* *Y2NCJH                 D
                    pnd_Check_Datex.setValueEdited(pymnt_View_Pymnt_Check_Dte,new ReportEditMask("YYYYMMDD"));                                                            //Natural: MOVE EDITED PYMNT-VIEW.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #CHECK-DATEX
                    //* *Y2NCJH  N8
                    if (condition(pnd_Check_Datex_Pnd_Check_Date.greaterOrEqual(19960415)))                                                                               //Natural: IF #CHECK-DATE GE 19960415
                    {
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: END-IF
                    //* *
                    pnd_Ck_All.setValuesByName(vw_pymnt_View);                                                                                                            //Natural: MOVE BY NAME PYMNT-VIEW TO #CK-ALL
                    //* *Y2NCJH                           D                D
                    if (condition(pymnt_View_Cnr_Cs_Pymnt_Acctg_Dte.greater(pnd_Ek_Pnd_Ek_Acctg_Dte)))                                                                    //Natural: IF PYMNT-VIEW.CNR-CS-PYMNT-ACCTG-DTE GT #EK-ACCTG-DTE
                    {
                        if (true) break RD_CS;                                                                                                                            //Natural: ESCAPE BOTTOM ( RD-CS. )
                    }                                                                                                                                                     //Natural: END-IF
                    wrkrec_Xtrct_Rec_Seq_Nbr.nadd(1);                                                                                                                     //Natural: ADD 1 TO WRKREC.XTRCT-REC-SEQ-NBR
                    wrkrec.setValuesByName(vw_pymnt_View);                                                                                                                //Natural: MOVE BY NAME PYMNT-VIEW TO WRKREC
                    wrkrec_Pnd_Isn.setValue(vw_pymnt_View.getAstISN("RD_CS"));                                                                                            //Natural: ASSIGN WRKREC.#ISN := *ISN
                    //*  END-READ /* (RD-CS.)
                    getSort().writeSortInData(wrkrec_Cnr_Cs_Pymnt_Acctg_Dte, wrkrec_Cnr_Cs_Pymnt_Check_Dte, wrkrec_Pymnt_Check_Nbr, wrkrec_Wrkrec_All);                   //Natural: END-ALL
                }                                                                                                                                                         //Natural: END-READ
                if (Global.isEscape()) return;
                getSort().sortData(wrkrec_Cnr_Cs_Pymnt_Acctg_Dte, wrkrec_Cnr_Cs_Pymnt_Check_Dte, wrkrec_Pymnt_Check_Nbr);                                                 //Natural: SORT RECORDS BY WRKREC.CNR-CS-PYMNT-ACCTG-DTE ASC WRKREC.CNR-CS-PYMNT-CHECK-DTE ASC WRKREC.PYMNT-CHECK-NBR ASC USING WRKREC-ALL
                boolean endOfDataSort01 = true;
                boolean firstSort01 = true;
                SORT01:
                while (condition(getSort().readSortOutData(wrkrec_Cnr_Cs_Pymnt_Acctg_Dte, wrkrec_Cnr_Cs_Pymnt_Check_Dte, wrkrec_Pymnt_Check_Nbr, wrkrec_Wrkrec_All)))
                {
                    if (condition(getSort().getAstCOUNTER().greater(0)))
                    {
                        atBreakEventSort01(false);
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom()))
                                break;
                            else if (condition(Global.isEscapeBottomImmediate()))
                            {
                                endOfDataSort01 = false;
                                break;
                            }
                            else if (condition(Global.isEscapeTop()))
                            continue;
                            else if (condition())
                            return;
                        }
                    }
                    //* *Y2NCJH                      D
                    //* *Y2NCJH                      D
                    //*  ..... MAINTAIN SOME STATS
                                                                                                                                                                          //Natural: PERFORM ACCUM-STATS
                    sub_Accum_Stats();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM GET-RDRW-AND-NAME-DATA
                    sub_Get_Rdrw_And_Name_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-DELETE-FILE-REC
                    sub_Write_Delete_File_Rec();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //* *
                    //*  (A1)
                    //*  (N7)
                    //*  A2
                    getReports().display(0, "REC/SEQ/NBR",                                                                                                                //Natural: DISPLAY ( RPT1 ) 'REC/SEQ/NBR' #CNT-REC2 ( NL = 3 ) 'CHECK/NUMBER' DETAIL-REC.CHECK-NUMBER 'CHECK/AMOUNT' DETAIL-REC.CHECK-AMOUNT ( EM = -ZZZZ,ZZZ.99 ) 'CHECK/ISSUE/DATE' WRKREC.PYMNT-CHECK-DTE 'C\S/ACCNTNG/DATE' WRKREC.CNR-CS-PYMNT-ACCTG-DTE 'C\S/CHECK/DATE' WRKREC.CNR-CS-PYMNT-CHECK-DTE 'C/S' WRKREC.CNR-CS-ACTVTY-CDE 'REDRAW/ACCNTNG/DATE' XTDREC.RDRW-ACCTG-DTE 'REDRAW/CHECK/NUMBER' XTDREC.RDRW-CHECK-NBR 'REDRAW/CHECK/AMOUNT' XTDREC.RDRW-CHECK-AMT ( EM = Z,ZZZ,ZZZ.99- ) 'O/R/G' WRKREC.CNTRCT-ORGN-CDE 'PPCN' WRKREC.CNTRCT-PPCN-NBR ( AL = 8 ) 'ANNUITANT/NAME' XTDREC.PH-NAME ( AL = 30 )
                    		pnd_Cnts_Pnd_Cnt_Rec2, new NumericLength (3),"CHECK/NUMBER",
                    		detail_Rec_Check_Number,"CHECK/AMOUNT",
                    		detail_Rec_Check_Amount, new ReportEditMask ("-ZZZZ,ZZZ.99"),"CHECK/ISSUE/DATE",
                    		wrkrec_Pymnt_Check_Dte,"C\\S/ACCNTNG/DATE",
                    		wrkrec_Cnr_Cs_Pymnt_Acctg_Dte,"C\\S/CHECK/DATE",
                    		wrkrec_Cnr_Cs_Pymnt_Check_Dte,"C/S",
                    		wrkrec_Cnr_Cs_Actvty_Cde,"REDRAW/ACCNTNG/DATE",
                    		xtdrec_Rdrw_Acctg_Dte,"REDRAW/CHECK/NUMBER",
                    		xtdrec_Rdrw_Check_Nbr,"REDRAW/CHECK/AMOUNT",
                    		xtdrec_Rdrw_Check_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),"O/R/G",
                    		wrkrec_Cntrct_Orgn_Cde,"PPCN",
                    		wrkrec_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),"ANNUITANT/NAME",
                    		xtdrec_Ph_Name, new AlphanumericLength (30));
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //* *Y2NCJH
                    //* *Y2NCJH
                    //* *Y2NCJH
                    //* *Y2NCJH
                    //* ** 'RDRW/Intrfce'         XTDREC.RDRW-INTRFCE-DTE
                    //* *    'REC/SEQ/NBR'          WRKREC.XTRCT-REC-SEQ-NBR
                    //* *&&��
                    //* *Y2NCJH
                    //* *                                                                                                                                                 //Natural: AT BREAK OF WRKREC.CNR-CS-PYMNT-CHECK-DTE
                    //* *                                                                                                                                                 //Natural: AT END OF DATA
                    sort01Check_AmountSum425.setValue(sort01Check_AmountSum425.add(detail_Rec_Check_Amount));                                                             //Natural: END-SORT
                    sort01Check_AmountSum.setValue(sort01Check_AmountSum.add(detail_Rec_Check_Amount));
                    sort01Check_AmountSum405.setValue(sort01Check_AmountSum405.add(detail_Rec_Check_Amount));
                }
                if (condition(getSort().getAstCOUNTER().greater(0)))
                {
                    atBreakEventSort01(endOfDataSort01);
                }
                if (condition(getSort().getAtEndOfData()))
                {
                    getReports().write(2, NEWLINE,new ReportTAsterisk(pnd_Cnts_Pnd_Cnt_Rec2),"*** TOTALS:",new ReportTAsterisk(detail_Rec_Check_Amount),sort01Check_AmountSum,  //Natural: WRITE ( RPT1 ) / T*#CNT-REC2 '*** TOTALS:' T*DETAIL-REC.CHECK-AMOUNT SUM ( DETAIL-REC.CHECK-AMOUNT ) ( EM = -ZZZZ,ZZZ.99 ) /
                        new ReportEditMask ("-ZZZZ,ZZZ.99"),NEWLINE);
                    if (condition(Global.isEscape())) return;
                }                                                                                                                                                         //Natural: END-ENDDATA
                endSort();
                pnd_Cnts_Pnd_Cnt_All.compute(new ComputeParameters(false, pnd_Cnts_Pnd_Cnt_All), pnd_Cnts_Pnd_Cnt_Stops.add(pnd_Cnts_Pnd_Cnt_Cancels).add(pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop)); //Natural: ASSIGN #CNTS.#CNT-ALL := #CNTS.#CNT-STOPS + #CNTS.#CNT-CANCELS + #CNTS.#CNT-NOT-CANCEL-STOP
                pnd_Cnts_Pnd_Amt_All.compute(new ComputeParameters(false, pnd_Cnts_Pnd_Amt_All), pnd_Cnts_Pnd_Amt_Stops.add(pnd_Cnts_Pnd_Amt_Cancels).add(pnd_Cnts_Pnd_Amt_Not_Cancel_Stop)); //Natural: ASSIGN #CNTS.#AMT-ALL := #CNTS.#AMT-STOPS + #CNTS.#AMT-CANCELS + #CNTS.#AMT-NOT-CANCEL-STOP
                getReports().newPage(new ReportSpecification(0));                                                                                                         //Natural: NEWPAGE ( RPT1 )
                if (condition(Global.isEscape())){return;}
                getReports().write(2, NEWLINE,NEWLINE,NEWLINE,Global.getPROGRAM(),"END OF RUN at time:",Global.getTIME(),NEWLINE,NEWLINE,"All          Stop Payments:",   //Natural: WRITE ( RPT1 ) /// *PROGRAM 'END OF RUN at time:' *TIME / / 'All          Stop Payments:' #CNTS.#CNT-STOPS 'Amount........:' #CNTS.#AMT-STOPS / 'All     Cancelled Payments:' #CNTS.#CNT-CANCELS 'Amount........:' #CNTS.#AMT-CANCELS / 'All     "DELETED" Payments:' #CNTS.#CNT-ALL 'Amount........:' #CNTS.#AMT-ALL / / 'Redrwan      Stop Payments:' #CNTS.#CNT-STOPS-RDRW 'Redrawn Amount:' #CNTS.#AMT-STOPS-RDRW / 'Redrawn Cancelled Payments:' #CNTS.#CNT-CANCELS-RDRW 'Redrawn Amount:' #CNTS.#AMT-CANCELS-RDRW /// 'NUMBER OF DETAIL-RECORDS ON THE "DELETE FILE":' #CNT-REC2
                    pnd_Cnts_Pnd_Cnt_Stops,"Amount........:",pnd_Cnts_Pnd_Amt_Stops,NEWLINE,"All     Cancelled Payments:",pnd_Cnts_Pnd_Cnt_Cancels,"Amount........:",
                    pnd_Cnts_Pnd_Amt_Cancels,NEWLINE,"All     'DELETED' Payments:",pnd_Cnts_Pnd_Cnt_All,"Amount........:",pnd_Cnts_Pnd_Amt_All,NEWLINE,NEWLINE,
                    "Redrwan      Stop Payments:",pnd_Cnts_Pnd_Cnt_Stops_Rdrw,"Redrawn Amount:",pnd_Cnts_Pnd_Amt_Stops_Rdrw,NEWLINE,"Redrawn Cancelled Payments:",
                    pnd_Cnts_Pnd_Cnt_Cancels_Rdrw,"Redrawn Amount:",pnd_Cnts_Pnd_Amt_Cancels_Rdrw,NEWLINE,NEWLINE,NEWLINE,"NUMBER OF DETAIL-RECORDS ON THE 'DELETE FILE':",
                    pnd_Cnts_Pnd_Cnt_Rec2);
                if (Global.isEscape()) return;
                //*  ...WERE THERE ANY RECORDS WITH NON-STOP OR NON-CANCEL ACTIVITY CODE?
                if (condition(pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop.notEquals(getZero())))                                                                                     //Natural: IF #CNT-NOT-CANCEL-STOP NE 0
                {
                    //* *Y2NCJH
                    getReports().write(2, Global.getPROGRAM(),Global.getDATU(),"-",Global.getTIME(),NEWLINE,"*",new RepeatItem(79),NEWLINE,"Please inform the Systems Department",NEWLINE,"*",new  //Natural: WRITE ( RPT1 ) *PROGRAM *DATU '-' *TIME / '*' ( 79 ) / 'Please inform the Systems Department' / '*' ( 79 ) // 'Field CNR_CS_ACTVTY_CDE on the Pyament File' / 'has code for others than "C" (Cancel) or "S" (Stop)'
                        RepeatItem(79),NEWLINE,NEWLINE,"Field CNR_CS_ACTVTY_CDE on the Pyament File",NEWLINE,"has code for others than 'C' (Cancel) or 'S' (Stop)");
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  ...WERE THERE ANY MISSING ADDRESS RECORDS
                if (condition(pnd_Cnt_Missing_Address.greater(getZero())))                                                                                                //Natural: IF #CNT-MISSING-ADDRESS GT 0
                {
                    //* *Y2NCJH
                    getReports().write(2, Global.getPROGRAM(),Global.getDATU(),"-",Global.getTIME(),NEWLINE,"*",new RepeatItem(79),NEWLINE,"Please inform the Systems Department",NEWLINE,"*",new  //Natural: WRITE ( RPT1 ) *PROGRAM *DATU '-' *TIME / '*' ( 79 ) / 'Please inform the Systems Department' / '*' ( 79 ) / 'There are' #CNT-MISSING-ADDRESS 'MISSING Address Records' 'that were not founf on the Address file' / '*' ( 79 )
                        RepeatItem(79),NEWLINE,"There are",pnd_Cnt_Missing_Address,"MISSING Address Records","that were not founf on the Address file",NEWLINE,"*",new 
                        RepeatItem(79));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                if (Global.isEscape()) return;                                                                                                                            //Natural: WRITE ( RPT1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( RPT1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 47T '"JPM DELETE FILE" for The Accounting Period' / 53T 'From:' #SK-ACCTG-DTE 'Thru:' #EK-ACCTG-DTE //
                //* *Y2NCJH
                //* *Y2NCJH
                getReports().write(0, NEWLINE,NEWLINE,NEWLINE,Global.getPROGRAM(),"END OF RUN at time:",Global.getTIME(),NEWLINE,NEWLINE,"All          Stop Payments:",   //Natural: WRITE /// *PROGRAM 'END OF RUN at time:' *TIME / / 'All          Stop Payments:' #CNTS.#CNT-STOPS 'Amount........:' #CNTS.#AMT-STOPS / 'All     Cancelled Payments:' #CNTS.#CNT-CANCELS 'Amount........:' #CNTS.#AMT-CANCELS / 'All     "DELETED" Payments:' #CNTS.#CNT-ALL 'Amount........:' #CNTS.#AMT-ALL / / 'Redrwan      Stop Payments:' #CNTS.#CNT-STOPS-RDRW 'Redrawn Amount:' #CNTS.#AMT-STOPS-RDRW / 'Redrawn Cancelled Payments:' #CNTS.#CNT-CANCELS-RDRW 'Redrawn Amount:' #CNTS.#AMT-CANCELS-RDRW
                    pnd_Cnts_Pnd_Cnt_Stops,"Amount........:",pnd_Cnts_Pnd_Amt_Stops,NEWLINE,"All     Cancelled Payments:",pnd_Cnts_Pnd_Cnt_Cancels,"Amount........:",
                    pnd_Cnts_Pnd_Amt_Cancels,NEWLINE,"All     'DELETED' Payments:",pnd_Cnts_Pnd_Cnt_All,"Amount........:",pnd_Cnts_Pnd_Amt_All,NEWLINE,NEWLINE,
                    "Redrwan      Stop Payments:",pnd_Cnts_Pnd_Cnt_Stops_Rdrw,"Redrawn Amount:",pnd_Cnts_Pnd_Amt_Stops_Rdrw,NEWLINE,"Redrawn Cancelled Payments:",
                    pnd_Cnts_Pnd_Cnt_Cancels_Rdrw,"Redrawn Amount:",pnd_Cnts_Pnd_Amt_Cancels_Rdrw);
                if (Global.isEscape()) return;
                //*  ...WERE THERE ANY RECORDS WITH NON-STOP OR NON-CANCEL ACTIVITY CODE?
                if (condition(pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop.notEquals(getZero())))                                                                                     //Natural: IF #CNT-NOT-CANCEL-STOP NE 0
                {
                    getReports().write(0, NEWLINE,"*",new RepeatItem(79),NEWLINE,"Field CNR_CS_ACTVTY_CDE on the Pyament File",NEWLINE,"has code for others than 'C' (Cancel) or 'S' (Stop)",NEWLINE,"*",new  //Natural: WRITE / '*' ( 79 ) / 'Field CNR_CS_ACTVTY_CDE on the Pyament File' / 'has code for others than "C" (Cancel) or "S" (Stop)' / '*' ( 79 )
                        RepeatItem(79));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  ...WERE THERE ANY MISSING ADDRESS RECORDS
                if (condition(pnd_Cnt_Missing_Address.greater(getZero())))                                                                                                //Natural: IF #CNT-MISSING-ADDRESS GT 0
                {
                    getReports().write(0, NEWLINE,"*",new RepeatItem(79),NEWLINE,"There are",pnd_Cnt_Missing_Address,"MISSING Address Records","that were not founf on the Address file",NEWLINE,"*",new  //Natural: WRITE / '*' ( 79 ) / 'There are' #CNT-MISSING-ADDRESS 'MISSING Address Records' 'that were not founf on the Address file' / '*' ( 79 )
                        RepeatItem(79));
                    if (Global.isEscape()) return;
                }                                                                                                                                                         //Natural: END-IF
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RDRW-AND-NAME-DATA
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-RDRW-DATA
                //*  ======================================================================
                //*  .... BUILD THE REDRAWN PAYMENT RECORD KEY AND GET THE RECORD
                //* *Y2NCJH
                //*  ...... EXPECT A UNIQUE REDRAW RECORD
                //* *Y2NCJH
                //* *Y2NCJH
                //* *Y2NCJH
                //* *Y2NCJH
                //*  ======================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: OBTAIN-PH-NAME
                //* *Y2NCJH
                //* *Y2NCJH
                //* *Y2NCJH
                //*  =====================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-STATS
                //* *Y2NCJH
                //*  =====================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DELETE-FILE-REC
                //* *Y2NCJH
                //* *Y2NCJH             A6
                //* *
                //*  $$$
                //*  =====================================================================
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-HEADER-REC
                //*  =====================================================================
                //*  CREATE THE HEADER RECORD FOR THE FILE
                //*  ---------------------------------------------------------------------
                //* *Y2NCJH
                //* *************************
                //* ** YEAR2000 FIX START ***
                //* *************************
                //* *Y2CHJH          A6                      N6
                //* ** HEADER-REC.FROM-DATE := INP-PARM.FOR-DATE-YYMMDD
                //* *Y2CHJH      A6                      A6
                //* *************************
                //* ** YEAR2000 FIX END   ***
                //* *************************
                //* *Y2NCJH      A6                    A6
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Get_Rdrw_And_Name_Data() throws Exception                                                                                                            //Natural: GET-RDRW-AND-NAME-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        xtdrec.reset();                                                                                                                                                   //Natural: RESET XTDREC
        //*  ...... IF THERE IS A REDRAW AGAINST THIS PAYMENT - GET THE DATA
        if (condition(wrkrec_Cnr_Rplcmnt_Prcss_Seq_Nbr.notEquals(getZero())))                                                                                             //Natural: IF WRKREC.CNR-RPLCMNT-PRCSS-SEQ-NBR NE 0
        {
                                                                                                                                                                          //Natural: PERFORM GET-RDRW-DATA
            sub_Get_Rdrw_Data();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM OBTAIN-PH-NAME
        sub_Obtain_Ph_Name();
        if (condition(Global.isEscape())) {return;}
        if (condition(Map.getDoInput())) {return;}
    }
    private void sub_Get_Rdrw_Data() throws Exception                                                                                                                     //Natural: GET-RDRW-DATA
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Ky_Rdrw_Cntrct_Unq_Id_Nbr.setValue(wrkrec_Cntrct_Unq_Id_Nbr);                                                                                                 //Natural: ASSIGN #KY-RDRW.CNTRCT-UNQ-ID-NBR := WRKREC.CNTRCT-UNQ-ID-NBR
        pnd_Ky_Rdrw_Cntrct_Invrse_Dte.setValue(wrkrec_Cnr_Rplcmnt_Invrse_Dte);                                                                                            //Natural: ASSIGN #KY-RDRW.CNTRCT-INVRSE-DTE := WRKREC.CNR-RPLCMNT-INVRSE-DTE
        pnd_Ky_Rdrw_Cntrct_Orgn_Cde.setValue(wrkrec_Cntrct_Orgn_Cde);                                                                                                     //Natural: ASSIGN #KY-RDRW.CNTRCT-ORGN-CDE := WRKREC.CNTRCT-ORGN-CDE
        pnd_Ky_Rdrw_Pymnt_Prcss_Seq_Nbr.setValue(wrkrec_Cnr_Rplcmnt_Prcss_Seq_Nbr);                                                                                       //Natural: ASSIGN #KY-RDRW.PYMNT-PRCSS-SEQ-NBR := WRKREC.CNR-RPLCMNT-PRCSS-SEQ-NBR
        vw_rdrw_View.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) RDRW-VIEW BY UNQ-INV-ORGN-PRCSS-INST STARTING FROM #KY-RDRW
        (
        "RD_RDRW",
        new Wc[] { new Wc("UNQ_INV_ORGN_PRCSS_INST", ">=", pnd_Ky_Rdrw, WcType.BY) },
        new Oc[] { new Oc("UNQ_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        RD_RDRW:
        while (condition(vw_rdrw_View.readNextRow("RD_RDRW")))
        {
            pnd_Ky_Rdrw_Current.setValuesByName(vw_rdrw_View);                                                                                                            //Natural: MOVE BY NAME RDRW-VIEW TO #KY-RDRW-CURRENT
            if (condition(pnd_Ky_Rdrw.notEquals(pnd_Ky_Rdrw_Current_Pnd_Ky_Rdrw_Current_Full)))                                                                           //Natural: IF #KY-RDRW NE #KY-RDRW-CURRENT-FULL
            {
                //*  ERROR - CAN NOT FIND THE REPLACEMNT PAYMENT RECORD
                //* *Y2NCJH
                getReports().write(0, Global.getPROGRAM(),Global.getDATU(),"-",Global.getTIME(),"Can not find the replacement payment record!",NEWLINE,"for key:","=",pnd_Ky_Rdrw,NEWLINE,"The Cancleled/Stop payment record is:",NEWLINE,"=",wrkrec_Pnd_Isn,NEWLINE,"=",wrkrec_Pymnt_Check_Nbr,NEWLINE,"=",wrkrec_Pymnt_Check_Dte,NEWLINE,"=",wrkrec_Pymnt_Check_Amt,  //Natural: WRITE *PROGRAM *DATU '-' *TIME 'Can not find the replacement payment record!' / 'for key:' '=' #KY-RDRW / 'The Cancleled/Stop payment record is:' / '=' WRKREC.#ISN / '=' WRKREC.PYMNT-CHECK-NBR / '=' WRKREC.PYMNT-CHECK-DTE / '=' WRKREC.PYMNT-CHECK-AMT ( EM = -ZZZZ,ZZZ.99 )
                    new ReportEditMask ("-ZZZZ,ZZZ.99"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom("RD_RDRW"))) break;
                    else if (condition(Global.isEscapeBottomImmediate("RD_RDRW"))) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (true) return;                                                                                                                                         //Natural: ESCAPE ROUTINE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                xtdrec_Rdrw_Check_Nbr.setValue(rdrw_View_Pymnt_Check_Nbr);                                                                                                //Natural: ASSIGN XTDREC.RDRW-CHECK-NBR := RDRW-VIEW.PYMNT-CHECK-NBR
                xtdrec_Rdrw_Check_Amt.setValue(rdrw_View_Pymnt_Check_Amt);                                                                                                //Natural: ASSIGN XTDREC.RDRW-CHECK-AMT := RDRW-VIEW.PYMNT-CHECK-AMT
                xtdrec_Rdrw_Acctg_Dte.setValue(rdrw_View_Cnr_Rdrw_Pymnt_Acctg_Dte);                                                                                       //Natural: ASSIGN XTDREC.RDRW-ACCTG-DTE := RDRW-VIEW.CNR-RDRW-PYMNT-ACCTG-DTE
                xtdrec_Rdrw_Intrfce_Dte.setValue(rdrw_View_Cnr_Rdrw_Pymnt_Intrfce_Dte);                                                                                   //Natural: ASSIGN XTDREC.RDRW-INTRFCE-DTE := RDRW-VIEW.CNR-RDRW-PYMNT-INTRFCE-DTE
                //*  ..... MAINTAIN SOME STATISTICS
                short decideConditionsMet549 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF WRKREC.CNR-CS-ACTVTY-CDE;//Natural: VALUE 'C'
                if (condition((wrkrec_Cnr_Cs_Actvty_Cde.equals("C"))))
                {
                    decideConditionsMet549++;
                    pnd_Cnts_Pnd_Cnt_Cancels_Rdrw.nadd(1);                                                                                                                //Natural: ADD 1 TO #CNT-CANCELS-RDRW
                    pnd_Cnts_Pnd_Amt_Cancels_Rdrw.nadd(xtdrec_Rdrw_Check_Amt);                                                                                            //Natural: ADD XTDREC.RDRW-CHECK-AMT TO #AMT-CANCELS-RDRW
                }                                                                                                                                                         //Natural: VALUE 'S'
                else if (condition((wrkrec_Cnr_Cs_Actvty_Cde.equals("S"))))
                {
                    decideConditionsMet549++;
                    pnd_Cnts_Pnd_Cnt_Stops_Rdrw.nadd(1);                                                                                                                  //Natural: ADD 1 TO #CNT-STOPS-RDRW
                    pnd_Cnts_Pnd_Amt_Stops_Rdrw.nadd(xtdrec_Rdrw_Check_Amt);                                                                                              //Natural: ADD XTDREC.RDRW-CHECK-AMT TO #AMT-STOPS-RDRW
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop_Rdrw.nadd(1);                                                                                                        //Natural: ADD 1 TO #CNT-NOT-CANCEL-STOP-RDRW
                    pnd_Cnts_Pnd_Amt_Not_Cancel_Stop_Rdrw.nadd(xtdrec_Rdrw_Check_Amt);                                                                                    //Natural: ADD XTDREC.RDRW-CHECK-AMT TO #AMT-NOT-CANCEL-STOP-RDRW
                }                                                                                                                                                         //Natural: END-DECIDE
                if (true) break RD_RDRW;                                                                                                                                  //Natural: ESCAPE BOTTOM ( RD-RDRW. )
            }                                                                                                                                                             //Natural: END-IF
            //*  (RD-RDRW.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*   GET-RDRW-DATA
    }
    private void sub_Obtain_Ph_Name() throws Exception                                                                                                                    //Natural: OBTAIN-PH-NAME
    {
        if (BLNatReinput.isReinput()) return;

        //*  ======================================================================
        //*  ADDRESS SEARCH KEY
        pnd_Sk_Addr.setValuesByName(wrkrec);                                                                                                                              //Natural: MOVE BY NAME WRKREC TO #SK-ADDR
        //*  ..... FOR IA CONTRACTS USE THE LATEST AVAILABLE ADDRESS RECORD
        if (condition(wrkrec_Cntrct_Orgn_Cde.equals("IA")))                                                                                                               //Natural: IF WRKREC.CNTRCT-ORGN-CDE = 'IA'
        {
            pnd_Sk_Addr_Cntrct_Invrse_Dte.reset();                                                                                                                        //Natural: RESET #SK-ADDR.CNTRCT-INVRSE-DTE #SK-ADDR.PYMNT-PRCSS-SEQ-NBR-N7
            pnd_Sk_Addr_Pymnt_Prcss_Seq_Nbr_N7.reset();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_N9_All.setValue(wrkrec_Pymnt_Prcss_Seq_Nbr);                                                                                                           //Natural: ASSIGN #WS-N9-ALL := WRKREC.PYMNT-PRCSS-SEQ-NBR
            pnd_Sk_Addr_Pymnt_Prcss_Seq_Nbr_N7.setValue(pnd_Ws_N9_All_Pnd_Ws_N9_Pos_1_To_7);                                                                              //Natural: ASSIGN #SK-ADDR.PYMNT-PRCSS-SEQ-NBR-N7 := #WS-N9-POS-1-TO-7
        }                                                                                                                                                                 //Natural: END-IF
        vw_addr_View.startDatabaseRead                                                                                                                                    //Natural: READ ( 1 ) ADDR-VIEW BY PPCN-PAYEE-ORGN-INVRS-SEQ STARTING FROM #SK-ADDR-ALL
        (
        "RD_ADDR",
        new Wc[] { new Wc("PPCN_PAYEE_ORGN_INVRS_SEQ", ">=", pnd_Sk_Addr_Pnd_Sk_Addr_All, WcType.BY) },
        new Oc[] { new Oc("PPCN_PAYEE_ORGN_INVRS_SEQ", "ASC") },
        1
        );
        RD_ADDR:
        while (condition(vw_addr_View.readNextRow("RD_ADDR")))
        {
            pnd_Ck_Addr.setValuesByName(vw_addr_View);                                                                                                                    //Natural: MOVE BY NAME ADDR-VIEW TO #CK-ADDR
            if (condition(pnd_Ck_Addr_Cntrct_Orgn_Cde.equals("IA")))                                                                                                      //Natural: IF #CK-ADDR.CNTRCT-ORGN-CDE = 'IA'
            {
                pnd_Ck_Addr_Cntrct_Invrse_Dte.reset();                                                                                                                    //Natural: RESET #CK-ADDR.CNTRCT-INVRSE-DTE #CK-ADDR.PYMNT-PRCSS-SEQ-NBR
                pnd_Ck_Addr_Pymnt_Prcss_Seq_Nbr.reset();
            }                                                                                                                                                             //Natural: END-IF
            //*  ..... IF THE ADDRESS RECORD IS FOUND - WE ARE HAPPY
            if (condition(pnd_Ck_Addr_Pnd_Ck_Addr_All.equals(pnd_Sk_Addr_Pnd_Sk_Addr_All)))                                                                               //Natural: IF #CK-ADDR-ALL EQ #SK-ADDR-ALL
            {
                //*  ..... COMPOSE THE NAME FOR PRINTING
                xtdrec_Ph_Name.setValue(DbsUtil.compress(addr_View_Ph_First_Name, addr_View_Ph_Middle_Name, addr_View_Ph_Last_Name));                                     //Natural: COMPRESS ADDR-VIEW.PH-FIRST-NAME ADDR-VIEW.PH-MIDDLE-NAME ADDR-VIEW.PH-LAST-NAME INTO XTDREC.PH-NAME
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  ..... MARK A WARNING THAT THE ADDRESS RECORD IS MISSING
                pnd_Cnt_Missing_Address.nadd(1);                                                                                                                          //Natural: ADD 1 TO #CNT-MISSING-ADDRESS
                xtdrec_Ph_Name.setValue("** ADDRESS RECORD IS MISSING **");                                                                                               //Natural: ASSIGN XTDREC.PH-NAME := '** ADDRESS RECORD IS MISSING **'
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            if (condition(true)) break;                                                                                                                                   //Natural: ESCAPE BOTTOM
            //*  (RD-ADDR.)
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        //*  OBTAIN-PH-NAME
    }
    private void sub_Accum_Stats() throws Exception                                                                                                                       //Natural: ACCUM-STATS
    {
        if (BLNatReinput.isReinput()) return;

        //*  =====================================================================
        short decideConditionsMet602 = 0;                                                                                                                                 //Natural: DECIDE ON FIRST VALUE OF WRKREC.CNR-CS-ACTVTY-CDE;//Natural: VALUE 'C'
        if (condition((wrkrec_Cnr_Cs_Actvty_Cde.equals("C"))))
        {
            decideConditionsMet602++;
            pnd_Cnts_Pnd_Cnt_Cancels.nadd(1);                                                                                                                             //Natural: ADD 1 TO #CNTS.#CNT-CANCELS
            pnd_Cnts_Pnd_Amt_Cancels.nadd(wrkrec_Pymnt_Check_Amt);                                                                                                        //Natural: ADD WRKREC.PYMNT-CHECK-AMT TO #CNTS.#AMT-CANCELS
        }                                                                                                                                                                 //Natural: VALUE 'S'
        else if (condition((wrkrec_Cnr_Cs_Actvty_Cde.equals("S"))))
        {
            decideConditionsMet602++;
            pnd_Cnts_Pnd_Cnt_Stops.nadd(1);                                                                                                                               //Natural: ADD 1 TO #CNTS.#CNT-STOPS
            pnd_Cnts_Pnd_Amt_Stops.nadd(wrkrec_Pymnt_Check_Amt);                                                                                                          //Natural: ADD WRKREC.PYMNT-CHECK-AMT TO #CNTS.#AMT-STOPS
        }                                                                                                                                                                 //Natural: NONE VALUE
        else if (condition())
        {
            pnd_Cnts_Pnd_Cnt_Not_Cancel_Stop.nadd(1);                                                                                                                     //Natural: ADD 1 TO #CNTS.#CNT-NOT-CANCEL-STOP
            pnd_Cnts_Pnd_Amt_Not_Cancel_Stop.nadd(wrkrec_Pymnt_Check_Amt);                                                                                                //Natural: ADD WRKREC.PYMNT-CHECK-AMT TO #CNTS.#AMT-NOT-CANCEL-STOP
            //*  ..... REPORT THE RECORD
            //* *Y2NCJH
            getReports().write(0, Global.getPROGRAM(),Global.getDATU(),"-",Global.getTIME(),"Invalid C/S activity code:",NEWLINE,"=",wrkrec_Pnd_Isn,NEWLINE,"=",wrkrec_Pymnt_Check_Nbr,NEWLINE,"=",wrkrec_Pymnt_Check_Dte,NEWLINE,"=",wrkrec_Pymnt_Check_Amt,  //Natural: WRITE *PROGRAM *DATU '-' *TIME 'Invalid C/S activity code:' / '=' WRKREC.#ISN / '=' WRKREC.PYMNT-CHECK-NBR / '=' WRKREC.PYMNT-CHECK-DTE / '=' WRKREC.PYMNT-CHECK-AMT ( EM = -ZZZZ,ZZZ.99 )
                new ReportEditMask ("-ZZZZ,ZZZ.99"));
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*  ACCUM-STATS
    }
    private void sub_Write_Delete_File_Rec() throws Exception                                                                                                             //Natural: WRITE-DELETE-FILE-REC
    {
        if (BLNatReinput.isReinput()) return;

        //*  =====================================================================
        //*  ......... CREATE THE DETAIL RECORD AND WRITE IT TO THE FILE
        pnd_Cnts_Pnd_Cnt_Rec2.nadd(1);                                                                                                                                    //Natural: ADD 1 TO #CNT-REC2
        detail_Rec_Check_Number.setValue(wrkrec_Pymnt_Check_Nbr);                                                                                                         //Natural: ASSIGN DETAIL-REC.CHECK-NUMBER := WRKREC.PYMNT-CHECK-NBR
        //* *Y2NCJH
        detail_Rec_Check_Issue_Date.setValueEdited(wrkrec_Pymnt_Check_Dte,new ReportEditMask("YYMMDD"));                                                                  //Natural: MOVE EDITED WRKREC.PYMNT-CHECK-DTE ( EM = YYMMDD ) TO DETAIL-REC.CHECK-ISSUE-DATE
        detail_Rec_Check_Amount.setValue(wrkrec_Pymnt_Check_Amt);                                                                                                         //Natural: ASSIGN DETAIL-REC.CHECK-AMOUNT := WRKREC.PYMNT-CHECK-AMT
        //* *Y2NCJH
        detail_Rec_Accounting_Date.setValueEdited(wrkrec_Cnr_Cs_Pymnt_Acctg_Dte,new ReportEditMask("YYMMDD"));                                                            //Natural: MOVE EDITED WRKREC.CNR-CS-PYMNT-ACCTG-DTE ( EM = YYMMDD ) TO DETAIL-REC.ACCOUNTING-DATE
        getWorkFiles().write(1, false, detail_Rec);                                                                                                                       //Natural: WRITE WORK FILE 1 DETAIL-REC
        //*  WRITE-DELETE-FILE-REC
    }
    //*  'MM/DD/YY'
    private void sub_Create_Header_Rec() throws Exception                                                                                                                 //Natural: CREATE-HEADER-REC
    {
        if (BLNatReinput.isReinput()) return;

        header_Rec_Current_Date.setValue(Global.getDATU());                                                                                                               //Natural: ASSIGN HEADER-REC.CURRENT-DATE := *DATU
        header_Rec_From_Date.setValue(inp_Parm_For_Date_Yymmdd_A);                                                                                                        //Natural: ASSIGN HEADER-REC.FROM-DATE := INP-PARM.FOR-DATE-YYMMDD-A
        header_Rec_To_Date.setValue(header_Rec_From_Date);                                                                                                                //Natural: ASSIGN HEADER-REC.TO-DATE := HEADER-REC.FROM-DATE
        getWorkFiles().write(1, false, header_Rec);                                                                                                                       //Natural: WRITE WORK FILE 1 HEADER-REC
        //*  CREATE-HEADER-REC
    }

    //

    // Support Methods

    private void atBreakEventSort01() throws Exception {atBreakEventSort01(false);}
    private void atBreakEventSort01(boolean endOfData) throws Exception
    {
        boolean wrkrec_Cnr_Cs_Pymnt_Check_DteIsBreak = wrkrec_Cnr_Cs_Pymnt_Check_Dte.isBreak(endOfData);
        if (condition(wrkrec_Cnr_Cs_Pymnt_Check_DteIsBreak))
        {
            getReports().write(2, NEWLINE,new ReportTAsterisk(pnd_Cnts_Pnd_Cnt_Rec2),"SUB TOTALS:",new ReportTAsterisk(detail_Rec_Check_Amount),sort01Check_AmountSum425, //Natural: WRITE ( RPT1 ) / T*#CNT-REC2 'SUB TOTALS:' T*DETAIL-REC.CHECK-AMOUNT SUM ( DETAIL-REC.CHECK-AMOUNT ) ( EM = -ZZZZ,ZZZ.99 ) /
                new ReportEditMask ("-ZZZZ,ZZZ.99"),NEWLINE);
            if (condition(Global.isEscape())) return;
            sort01Check_AmountSum425.setDec(new DbsDecimal(0));                                                                                                           //Natural: END-BREAK
        }
    }
    private void setupReports() throws Exception
    {
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");

        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(47),"'JPM DELETE FILE' for The Accounting Period",NEWLINE,new TabSetting(53),"From:",pnd_Sk_Pnd_Sk_Acctg_Dte,"Thru:",pnd_Ek_Pnd_Ek_Acctg_Dte,
            NEWLINE,NEWLINE);

        getReports().setDisplayColumns(0, "REC/SEQ/NBR",
        		pnd_Cnts_Pnd_Cnt_Rec2, new NumericLength (3),"CHECK/NUMBER",
        		detail_Rec_Check_Number,"CHECK/AMOUNT",
        		detail_Rec_Check_Amount, new ReportEditMask ("-ZZZZ,ZZZ.99"),"CHECK/ISSUE/DATE",
        		wrkrec_Pymnt_Check_Dte,"C\\S/ACCNTNG/DATE",
        		wrkrec_Cnr_Cs_Pymnt_Acctg_Dte,"C\\S/CHECK/DATE",
        		wrkrec_Cnr_Cs_Pymnt_Check_Dte,"C/S",
        		wrkrec_Cnr_Cs_Actvty_Cde,"REDRAW/ACCNTNG/DATE",
        		xtdrec_Rdrw_Acctg_Dte,"REDRAW/CHECK/NUMBER",
        		xtdrec_Rdrw_Check_Nbr,"REDRAW/CHECK/AMOUNT",
        		xtdrec_Rdrw_Check_Amt, new ReportEditMask ("Z,ZZZ,ZZZ.99-"),"O/R/G",
        		wrkrec_Cntrct_Orgn_Cde,"PPCN",
        		wrkrec_Cntrct_Ppcn_Nbr, new AlphanumericLength (8),"ANNUITANT/NAME",
        		xtdrec_Ph_Name, new AlphanumericLength (30));
    }
}
