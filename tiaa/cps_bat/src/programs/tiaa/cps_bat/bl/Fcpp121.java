/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:05 PM
**        * FROM NATURAL PROGRAM : Fcpp121
************************************************************
**        * FILE NAME            : Fcpp121.java
**        * CLASS NAME           : Fcpp121
**        * INSTANCE NAME        : Fcpp121
************************************************************
************************************************************************
* PROGRAM  : FCPP121
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS DETAIL LEDGER REPORT
* CREATED  : 08/24/94
* FUNCTION : THIS PROGRAM READS SORTED EXTRACT PRODUCED BY FCPP120
*            AND PRODUCES DETAIL AND SUMMARY LEDGER REPORTS.
*
* NOTE       PAYMENT DATE TOTALS ARE COMMENTED OUT.
*
* 07/17/96 LZ - INCLUDED MDO IN THE REPORTS
* 04/01/97 : RITA SALGADO
*          : INCLUDE (NZ) ANNUITIZATION
*          : CHANGE BREAK FROM FUND CODE TO ISA NUMBER
*
* 06/09/98 - RIAD LOUTFI - ADDED RETIREMENT LOAN PROCESSING LOGIC.
* 09/30/99 - A. YOUNG    - MODIFIED REPORT TITLE AS PER ACCOUNTING TO
*                          INCLUDE CANCEL / STOP NO REDRAW.
*
* 06/11/02 : ROXAN CARREON.  RE-STOWED PA-SELECR/SPIA NEW FUNDS IN
*            FCPL199A
* 04/2017  : JJG PIN EXPANSION
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp121 extends BLNatBase
{
    // Data Areas
    private PdaFcpa121 pdaFcpa121;
    private LdaFcpl121 ldaFcpl121;
    private LdaFcpl199a ldaFcpl199a;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Contract;
    private DbsField pnd_Ws_Pnd_Ws_Inv_Acct_Isa;
    private DbsField pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Ws_Pnd_Ws_Ledger_Desc;
    private DbsField pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde_Num;
    private DbsField pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc;
    private DbsField pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Pnd_Orgn_Array;
    private DbsField pnd_Ws_Pnd_Ws_Header_Orgn;
    private DbsField pnd_Ws_Pnd_Ws_Header_Isa;
    private DbsField pnd_Ws_Pnd_Ws_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Pnd_Ws_Header_Csr;
    private DbsField pnd_Ws_Pnd_Ws_Total_Orgn;
    private DbsField pnd_Ws_Pnd_Ws_Total_Isa;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Pnd_Orgn_Sub;
    private DbsField pnd_Ws_Pnd_Records_Read;
    private DbsField pnd_Ws_Pnd_Dr_Amt;
    private DbsField pnd_Ws_Pnd_Cr_Amt;

    private DbsGroup pnd_Ws_Pnd_Tot_Array;
    private DbsField pnd_Ws_Pnd_Tot_Dr_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Cr_Amt;
    private DbsField pnd_Ws_Pnd_Settl_Dte_Ind;
    private DbsField pnd_Ws_Pnd_Check_Dte_Ind;
    private DbsField pnd_Ws_Pnd_Ledger_Ind;
    private DbsField pnd_Ws_Pnd_Currency_Ind;
    private DbsField pnd_Ws_Pnd_Acctg_Dte_Ind;
    private DbsField pnd_Ws_Pnd_Isa_Ind;
    private DbsField pnd_Ws_Pnd_Csr_Ind;
    private DbsField pnd_Ws_Pnd_Orgn_Cde_Ind;
    private DbsField pnd_Ws_Pnd_Newpage_Ind_Det;
    private DbsField pnd_Ws_Pnd_Newpage_Ind_Sum;
    private DbsField pnd_Pymnt_Intrfce_Dte;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Cr_AmtSum160;
    private DbsField readWork01Pnd_Cr_AmtSum;
    private DbsField readWork01Pnd_Dr_AmtSum160;
    private DbsField readWork01Pnd_Dr_AmtSum;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa121 = new PdaFcpa121(localVariables);
        ldaFcpl121 = new LdaFcpl121();
        registerRecord(ldaFcpl121);
        ldaFcpl199a = new LdaFcpl199a();
        registerRecord(ldaFcpl199a);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Contract = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Ws_Pnd_Ws_Inv_Acct_Isa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Inv_Acct_Isa", "#WS-INV-ACCT-ISA", FieldType.STRING, 5);
        pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr", "#WS-INV-ACCT-LEDGR-NBR", FieldType.STRING, 15);
        pnd_Ws_Pnd_Ws_Ledger_Desc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Ledger_Desc", "#WS-LEDGER-DESC", FieldType.STRING, 40);
        pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde", "#WS-CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 
            1);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde);
        pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde_Num = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde_Num", "#WS-CNTRCT-CHECK-CRRNCY-CDE-NUM", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc", "#WS-CNTRCT-CHECK-CRRNCY-DESC", 
            FieldType.STRING, 17, new DbsArrayController(1, 2));
        pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde", "#WS-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Orgn_Array = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Orgn_Array", "#ORGN-ARRAY", FieldType.STRING, 16, new DbsArrayController(1, 9));
        pnd_Ws_Pnd_Ws_Header_Orgn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Header_Orgn", "#WS-HEADER-ORGN", FieldType.STRING, 16);
        pnd_Ws_Pnd_Ws_Header_Isa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Header_Isa", "#WS-HEADER-ISA", FieldType.STRING, 10);
        pnd_Ws_Pnd_Ws_Cntrct_Cancel_Rdrw_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Cntrct_Cancel_Rdrw_Ind", "#WS-CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 
            1);
        pnd_Ws_Pnd_Ws_Header_Csr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Header_Csr", "#WS-HEADER-CSR", FieldType.STRING, 34);
        pnd_Ws_Pnd_Ws_Total_Orgn = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Total_Orgn", "#WS-TOTAL-ORGN", FieldType.STRING, 58);
        pnd_Ws_Pnd_Ws_Total_Isa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Total_Isa", "#WS-TOTAL-ISA", FieldType.STRING, 5);
        pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte", "#WS-PYMNT-INTRFCE-DTE", FieldType.STRING, 10);
        pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte", "#WS-PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Ws_Pymnt_Check_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Check_Dte", "#WS-PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte", "#WS-PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Orgn_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Orgn_Sub", "#ORGN-SUB", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Records_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Records_Read", "#RECORDS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Dr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dr_Amt", "#DR-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Cr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cr_Amt", "#CR-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Ws_Pnd_Tot_Array = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Tot_Array", "#TOT-ARRAY", new DbsArrayController(1, 7));
        pnd_Ws_Pnd_Tot_Dr_Amt = pnd_Ws_Pnd_Tot_Array.newFieldInGroup("pnd_Ws_Pnd_Tot_Dr_Amt", "#TOT-DR-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Tot_Cr_Amt = pnd_Ws_Pnd_Tot_Array.newFieldInGroup("pnd_Ws_Pnd_Tot_Cr_Amt", "#TOT-CR-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Settl_Dte_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Settl_Dte_Ind", "#SETTL-DTE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Check_Dte_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Check_Dte_Ind", "#CHECK-DTE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ledger_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ledger_Ind", "#LEDGER-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Currency_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Currency_Ind", "#CURRENCY-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Acctg_Dte_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acctg_Dte_Ind", "#ACCTG-DTE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Isa_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isa_Ind", "#ISA-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Csr_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Csr_Ind", "#CSR-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Orgn_Cde_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Orgn_Cde_Ind", "#ORGN-CDE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Newpage_Ind_Det = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Newpage_Ind_Det", "#NEWPAGE-IND-DET", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Newpage_Ind_Sum = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Newpage_Ind_Sum", "#NEWPAGE-IND-SUM", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Intrfce_Dte = localVariables.newFieldInRecord("pnd_Pymnt_Intrfce_Dte", "#PYMNT-INTRFCE-DTE", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Cr_AmtSum160 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM_160", "Pnd_Cr_Amt_SUM_160", FieldType.PACKED_DECIMAL, 
            11, 2);
        readWork01Pnd_Cr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM", "Pnd_Cr_Amt_SUM", FieldType.PACKED_DECIMAL, 11, 2);
        readWork01Pnd_Dr_AmtSum160 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Dr_Amt_SUM_160", "Pnd_Dr_Amt_SUM_160", FieldType.PACKED_DECIMAL, 
            11, 2);
        readWork01Pnd_Dr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Dr_Amt_SUM", "Pnd_Dr_Amt_SUM", FieldType.PACKED_DECIMAL, 11, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl121.initializeValues();
        ldaFcpl199a.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc.getValue(1).setInitialValue("US CONTRACT");
        pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc.getValue(2).setInitialValue("CANADIAN CONTRACT");
        pnd_Ws_Pnd_Orgn_Array.getValue(1).setInitialValue("DAILY");
        pnd_Ws_Pnd_Orgn_Array.getValue(2).setInitialValue("MONTHLY");
        pnd_Ws_Pnd_Orgn_Array.getValue(3).setInitialValue("SINGLE SUM / MDO");
        pnd_Ws_Pnd_Orgn_Array.getValue(4).setInitialValue("IA DEATH CLAIM");
        pnd_Ws_Pnd_Orgn_Array.getValue(5).setInitialValue("IA");
        pnd_Ws_Pnd_Orgn_Array.getValue(6).setInitialValue("ANNUITIZATION");
        pnd_Ws_Pnd_Orgn_Array.getValue(7).setInitialValue("ANNUITY PYMNTS");
        pnd_Ws_Pnd_Orgn_Array.getValue(8).setInitialValue("ANNUITY LOANS");
        pnd_Ws_Pnd_Orgn_Array.getValue(9).setInitialValue("UNKNOWN");
        pnd_Ws_Pnd_Ws_Header_Isa.setInitialValue("ISA: ");
        pnd_Ws_Pnd_Ws_Header_Csr.setInitialValue("(CANCEL, STOP With/Without REDRAW)");
        pnd_Ws_Pnd_Settl_Dte_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Check_Dte_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Ledger_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Currency_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Acctg_Dte_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Isa_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Csr_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Orgn_Cde_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Newpage_Ind_Det.setInitialValue(false);
        pnd_Ws_Pnd_Newpage_Ind_Sum.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp121() throws Exception
    {
        super("Fcpp121");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 02 ) PS = 60 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 RECORD #LEDGER-EXTRACT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl121.getPnd_Ledger_Extract())))
        {
            CheckAtStartofData154();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Pymnt_Intrfce_Dte.setValueEdited(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte(),new ReportEditMask("MM/DD/YY"));                                    //Natural: MOVE EDITED PYMNT-INTRFCE-DTE ( EM = MM/DD/YY ) TO #PYMNT-INTRFCE-DTE
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF PYMNT-SETTLMNT-DTE
            //*                                                                                                                                                           //Natural: AT BREAK OF PYMNT-CHECK-DTE;//Natural: AT BREAK OF #LEDGER-EXTRACT.INV-ACCT-LEDGR-NBR;//Natural: AT BREAK OF CNTRCT-CHECK-CRRNCY-CDE;//Natural: AT BREAK OF #LEDGER-EXTRACT.INV-ACCT-ISA;//Natural: AT BREAK OF PYMNT-ACCTG-DTE;//Natural: AT BREAK OF CNTRCT-ORGN-CDE;//Natural: AT BREAK OF CNTRCT-CANCEL-RDRW-IND
            //*                                                                                                                                                           //Natural: AT END OF DATA
            //*  REPORT TOTALS
                                                                                                                                                                          //Natural: PERFORM TOTAL-RTN
            sub_Total_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  INITIALIZATION
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
            sub_Init_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Pnd_Records_Read.nadd(1);                                                                                                                              //Natural: ADD 1 TO #RECORDS-READ
            if (condition((! ((ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde().equals("01") || ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde().equals("09")))             //Natural: IF NOT ( #LEDGER-EXTRACT.INV-ACCT-CDE = '01' OR = '09' ) AND CNTRCT-CREF-NBR NE ' '
                && ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cref_Nbr().notEquals(" "))))
            {
                pnd_Ws_Pnd_Contract.setValue(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cref_Nbr());                                                                         //Natural: MOVE CNTRCT-CREF-NBR TO #CONTRACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Contract.setValue(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr());                                                                         //Natural: MOVE CNTRCT-PPCN-NBR TO #CONTRACT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Ind().equals("C")))                                                                             //Natural: IF INV-ACCT-LEDGR-IND = 'C'
            {
                pnd_Ws_Pnd_Cr_Amt.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Amt());                                                                        //Natural: MOVE INV-ACCT-LEDGR-AMT TO #CR-AMT
                pnd_Ws_Pnd_Dr_Amt.reset();                                                                                                                                //Natural: RESET #DR-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Dr_Amt.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Amt());                                                                        //Natural: MOVE INV-ACCT-LEDGR-AMT TO #DR-AMT
                pnd_Ws_Pnd_Cr_Amt.reset();                                                                                                                                //Natural: RESET #CR-AMT
            }                                                                                                                                                             //Natural: END-IF
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"SETTLEMENT/EFFECTIVE/DATE",                                       //Natural: DISPLAY ( 01 ) ( HC = L ) 'SETTLEMENT/EFFECTIVE/DATE' PYMNT-SETTLMNT-DTE '/PAYMENT/DATE' PYMNT-CHECK-DTE '//ANNUITANT NAME' PH-LAST-NAME '/' PH-FIRST-NAME '/' PH-MIDDLE-NAME ( AL = 1 ) '/CONTRACT/NUMBER' #CONTRACT 'WORK/PROCESS/NUMBER' PYMNT-CORP-WPID '/DEBIT/AMOUNT' #DR-AMT ( HC = R ) 16X '/CREDIT/AMOUNT' #CR-AMT ( HC = R )
            		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte(),"/PAYMENT/DATE",
            		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte(),"//ANNUITANT NAME",
            		ldaFcpl121.getPnd_Ledger_Extract_Ph_Last_Name(),"/",
            		ldaFcpl121.getPnd_Ledger_Extract_Ph_First_Name(),"/",
            		ldaFcpl121.getPnd_Ledger_Extract_Ph_Middle_Name(), new AlphanumericLength (1),"/CONTRACT/NUMBER",
            		pnd_Ws_Pnd_Contract,"WORK/PROCESS/NUMBER",
            		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Corp_Wpid(),"/DEBIT/AMOUNT",
            		pnd_Ws_Pnd_Dr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ColumnSpacing(16),"/CREDIT/AMOUNT",
            		pnd_Ws_Pnd_Cr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Pnd_Cr_AmtSum160.nadd(readWork01Pnd_Cr_AmtSum160,pnd_Ws_Pnd_Cr_Amt);                                                                                //Natural: END-WORK
            readWork01Pnd_Cr_AmtSum.nadd(readWork01Pnd_Cr_AmtSum,pnd_Ws_Pnd_Cr_Amt);
            readWork01Pnd_Dr_AmtSum160.nadd(readWork01Pnd_Dr_AmtSum160,pnd_Ws_Pnd_Dr_Amt);
            readWork01Pnd_Dr_AmtSum.nadd(readWork01Pnd_Dr_AmtSum,pnd_Ws_Pnd_Dr_Amt);
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            //*  REPORT TOTALS
                                                                                                                                                                          //Natural: PERFORM TOTAL-RTN
            sub_Total_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().skip(1, 7);                                                                                                                                          //Natural: SKIP ( 1 ) 7 LINES
        getReports().write(1, new FieldAttributes ("AD=I"),new TabSetting(7),"INPUT RECORDS          :", new FieldAttributes ("AD=I"),pnd_Ws_Pnd_Records_Read,            //Natural: WRITE ( 1 ) ( AD = I ) 7T 'INPUT RECORDS          :' ( AD = I ) #RECORDS-READ
            new ReportEditMask ("Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        getReports().skip(1, 3);                                                                                                                                          //Natural: SKIP ( 1 ) 3 LINES
        getReports().write(1, new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new  //Natural: WRITE ( 1 ) 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' /
            TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE);
        if (Global.isEscape()) return;
        getReports().eject(2, true);                                                                                                                                      //Natural: EJECT ( 2 )
        getReports().skip(2, 3);                                                                                                                                          //Natural: SKIP ( 2 ) 3 LINES
        getReports().write(2, new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new  //Natural: WRITE ( 2 ) 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' /
            TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE);
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-RTN
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-RTN
        //* *------------
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 01 ) TITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 120T 'PAGE:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'REPORT: RPT1' / #WS-HEADER-ORGN 'SETTLEMENT SYSTEM - DETAIL LEDGER REPORT'
        if (Global.isEscape()) return;                                                                                                                                    //Natural: AT TOP OF PAGE ( 01 );//Natural: WRITE ( 2 ) TITLE *DATU '-' *TIMX ( EM = HH:IIAP ) 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'REPORT: RPT2' / #WS-HEADER-ORGN 'SETTLEMENT SYSTEM - SUMMARY LEDGER REPORT'
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
    }
    private void sub_Init_Rtn() throws Exception                                                                                                                          //Natural: INIT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------
        short decideConditionsMet273 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #SETTL-DTE-IND
        if (condition(pnd_Ws_Pnd_Settl_Dte_Ind.getBoolean()))
        {
            decideConditionsMet273++;
            pnd_Ws_Pnd_Settl_Dte_Ind.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #SETTL-DTE-IND
            pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte.setValue(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte());                                                             //Natural: MOVE PYMNT-SETTLMNT-DTE TO #WS-PYMNT-SETTLMNT-DTE
        }                                                                                                                                                                 //Natural: WHEN #CHECK-DTE-IND
        if (condition(pnd_Ws_Pnd_Check_Dte_Ind.getBoolean()))
        {
            decideConditionsMet273++;
            pnd_Ws_Pnd_Check_Dte_Ind.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #CHECK-DTE-IND
            pnd_Ws_Pnd_Ws_Pymnt_Check_Dte.setValue(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte());                                                                   //Natural: MOVE PYMNT-CHECK-DTE TO #WS-PYMNT-CHECK-DTE
        }                                                                                                                                                                 //Natural: WHEN #LEDGER-IND
        if (condition(pnd_Ws_Pnd_Ledger_Ind.getBoolean()))
        {
            decideConditionsMet273++;
            pnd_Ws_Pnd_Ledger_Ind.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #LEDGER-IND
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 2 )
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NEWPAGE-IND-DET
            pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr());                                                             //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-LEDGR-NBR TO #WS-INV-ACCT-LEDGR-NBR #FCPA121.INV-ACCT-LEDGR-NBR
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr());
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Isa().setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa());                                                           //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-ISA TO #FCPA121.INV-ACCT-ISA
            DbsUtil.callnat(Fcpn121.class , getCurrentProcessState(), pdaFcpa121.getPnd_Fcpa121());                                                                       //Natural: CALLNAT 'FCPN121' USING #FCPA121
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Ws_Ledger_Desc.setValue(pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc());                                                                          //Natural: MOVE #FCPA121.INV-ACCT-LEDGR-DESC TO #WS-LEDGER-DESC
        }                                                                                                                                                                 //Natural: WHEN #CURRENCY-IND
        if (condition(pnd_Ws_Pnd_Currency_Ind.getBoolean()))
        {
            decideConditionsMet273++;
            pnd_Ws_Pnd_Currency_Ind.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #CURRENCY-IND
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NEWPAGE-IND-DET #NEWPAGE-IND-SUM
            pnd_Ws_Pnd_Newpage_Ind_Sum.setValue(true);
            pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde.setValue(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Check_Crrncy_Cde());                                                   //Natural: MOVE CNTRCT-CHECK-CRRNCY-CDE TO #WS-CNTRCT-CHECK-CRRNCY-CDE
        }                                                                                                                                                                 //Natural: WHEN #ISA-IND
        if (condition(pnd_Ws_Pnd_Isa_Ind.getBoolean()))
        {
            decideConditionsMet273++;
            pnd_Ws_Pnd_Isa_Ind.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #ISA-IND
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NEWPAGE-IND-DET #NEWPAGE-IND-SUM
            pnd_Ws_Pnd_Newpage_Ind_Sum.setValue(true);
            pnd_Ws_Pnd_Ws_Inv_Acct_Isa.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde());                                                                         //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-CDE TO #WS-INV-ACCT-ISA
            setValueToSubstring(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa(),pnd_Ws_Pnd_Ws_Header_Isa,6,5);                                                            //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-ISA TO SUBSTRING ( #WS-HEADER-ISA,6,5 )
            pnd_Ws_Pnd_Ws_Total_Isa.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa());                                                                            //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-ISA TO #WS-TOTAL-ISA
        }                                                                                                                                                                 //Natural: WHEN #ACCTG-DTE-IND
        if (condition(pnd_Ws_Pnd_Acctg_Dte_Ind.getBoolean()))
        {
            decideConditionsMet273++;
            pnd_Ws_Pnd_Acctg_Dte_Ind.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #ACCTG-DTE-IND
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NEWPAGE-IND-DET #NEWPAGE-IND-SUM
            pnd_Ws_Pnd_Newpage_Ind_Sum.setValue(true);
            pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte.setValue(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Acctg_Dte());                                                                   //Natural: MOVE PYMNT-ACCTG-DTE TO #WS-PYMNT-ACCTG-DTE
        }                                                                                                                                                                 //Natural: WHEN #ORGN-CDE-IND
        if (condition(pnd_Ws_Pnd_Orgn_Cde_Ind.getBoolean()))
        {
            decideConditionsMet273++;
            pnd_Ws_Pnd_Orgn_Cde_Ind.setValue(false);                                                                                                                      //Natural: MOVE FALSE TO #ORGN-CDE-IND
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NEWPAGE-IND-DET #NEWPAGE-IND-SUM
            pnd_Ws_Pnd_Newpage_Ind_Sum.setValue(true);
            pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.setValue(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde());                                                                   //Natural: MOVE CNTRCT-ORGN-CDE TO #WS-CNTRCT-ORGN-CDE
            short decideConditionsMet307 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE OF #WS-CNTRCT-ORGN-CDE;//Natural: VALUE 'DS'
            if (condition((pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.equals("DS"))))
            {
                decideConditionsMet307++;
                pnd_Ws_Pnd_Orgn_Sub.setValue(1);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 1
            }                                                                                                                                                             //Natural: VALUE 'MS'
            else if (condition((pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.equals("MS"))))
            {
                decideConditionsMet307++;
                pnd_Ws_Pnd_Orgn_Sub.setValue(2);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 2
            }                                                                                                                                                             //Natural: VALUE 'SS'
            else if (condition((pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.equals("SS"))))
            {
                decideConditionsMet307++;
                pnd_Ws_Pnd_Orgn_Sub.setValue(3);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 3
            }                                                                                                                                                             //Natural: VALUE 'DC'
            else if (condition((pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.equals("DC"))))
            {
                decideConditionsMet307++;
                pnd_Ws_Pnd_Orgn_Sub.setValue(4);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 4
            }                                                                                                                                                             //Natural: VALUE 'IA'
            else if (condition((pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.equals("IA"))))
            {
                decideConditionsMet307++;
                pnd_Ws_Pnd_Orgn_Sub.setValue(5);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 5
            }                                                                                                                                                             //Natural: VALUE 'NZ'
            else if (condition((pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.equals("NZ"))))
            {
                decideConditionsMet307++;
                pnd_Ws_Pnd_Orgn_Sub.setValue(6);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 6
            }                                                                                                                                                             //Natural: VALUE 'AP'
            else if (condition((pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.equals("AP"))))
            {
                decideConditionsMet307++;
                pnd_Ws_Pnd_Orgn_Sub.setValue(7);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 7
            }                                                                                                                                                             //Natural: VALUE 'AL'
            else if (condition((pnd_Ws_Pnd_Ws_Cntrct_Orgn_Cde.equals("AL"))))
            {
                decideConditionsMet307++;
                pnd_Ws_Pnd_Orgn_Sub.setValue(8);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 8
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                pnd_Ws_Pnd_Orgn_Sub.setValue(9);                                                                                                                          //Natural: ASSIGN #ORGN-SUB := 9
            }                                                                                                                                                             //Natural: END-DECIDE
            pnd_Ws_Pnd_Ws_Header_Orgn.setValue(pnd_Ws_Pnd_Orgn_Array.getValue(pnd_Ws_Pnd_Orgn_Sub), MoveOption.RightJustified);                                           //Natural: MOVE RIGHT #ORGN-ARRAY ( #ORGN-SUB ) TO #WS-HEADER-ORGN
        }                                                                                                                                                                 //Natural: WHEN #CSR-IND
        if (condition(pnd_Ws_Pnd_Csr_Ind.getBoolean()))
        {
            decideConditionsMet273++;
            pnd_Ws_Pnd_Csr_Ind.setValue(false);                                                                                                                           //Natural: ASSIGN #CSR-IND := FALSE
            pnd_Ws_Pnd_Ws_Cntrct_Cancel_Rdrw_Ind.setValue(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_Ind());                                                     //Natural: ASSIGN #WS-CNTRCT-CANCEL-RDRW-IND := CNTRCT-CANCEL-RDRW-IND
            if (condition(pnd_Ws_Pnd_Ws_Cntrct_Cancel_Rdrw_Ind.equals(" ")))                                                                                              //Natural: IF #WS-CNTRCT-CANCEL-RDRW-IND = ' '
            {
                pnd_Ws_Pnd_Ws_Header_Csr.reset();                                                                                                                         //Natural: RESET #WS-HEADER-CSR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Ws_Header_Csr.resetInitial();                                                                                                                  //Natural: RESET INITIAL #WS-HEADER-CSR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: ASSIGN #NEWPAGE-IND-DET := #NEWPAGE-IND-SUM := TRUE
            pnd_Ws_Pnd_Newpage_Ind_Sum.setValue(true);
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet273 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Newpage_Ind_Det.getBoolean()))                                                                                                           //Natural: IF #NEWPAGE-IND-DET
        {
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(false);                                                                                                                   //Natural: ASSIGN #NEWPAGE-IND-DET := FALSE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Newpage_Ind_Sum.getBoolean()))                                                                                                           //Natural: IF #NEWPAGE-IND-SUM
        {
            pnd_Ws_Pnd_Newpage_Ind_Sum.setValue(false);                                                                                                                   //Natural: ASSIGN #NEWPAGE-IND-SUM := FALSE
            getReports().newPage(new ReportSpecification(2));                                                                                                             //Natural: NEWPAGE ( 02 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Total_Rtn() throws Exception                                                                                                                         //Natural: TOTAL-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------
        short decideConditionsMet357 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #SETTL-DTE-IND
        if (condition(pnd_Ws_Pnd_Settl_Dte_Ind.getBoolean()))
        {
            decideConditionsMet357++;
            getReports().display(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"LEDGER/ACCOUNT/NUMBER",                                           //Natural: DISPLAY ( 2 ) ( HC = L ) 'LEDGER/ACCOUNT/NUMBER' #WS-INV-ACCT-LEDGR-NBR ( IS = ON ) '//DESCRIPTION' #WS-LEDGER-DESC ( IS = ON ) '/PAYMENT/DATE' #WS-PYMNT-CHECK-DTE 'SETTLEMENT/EFFECTIVE/DATE' #WS-PYMNT-SETTLMNT-DTE '/DEBIT/AMOUNT' #TOT-DR-AMT ( 1 ) ( HC = R ) 11X '/CREDIT/AMOUNT' #TOT-CR-AMT ( 1 ) ( HC = R )
            		pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr, new IdenticalSuppress(true),"//DESCRIPTION",
            		pnd_Ws_Pnd_Ws_Ledger_Desc, new IdenticalSuppress(true),"/PAYMENT/DATE",
            		pnd_Ws_Pnd_Ws_Pymnt_Check_Dte,"SETTLEMENT/EFFECTIVE/DATE",
            		pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte,"/DEBIT/AMOUNT",
            		pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ColumnSpacing(11),"/CREDIT/AMOUNT",
                
            		pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (Global.isEscape()) return;
            getReports().write(1, new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),"-",new RepeatItem(15),NEWLINE,"SUB-TOTAL FOR SETTLEMENT EFFECTIVE DATE OF",pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte,  //Natural: WRITE ( 1 ) T*#DR-AMT '-' ( 15 ) T*#CR-AMT '-' ( 15 ) / 'SUB-TOTAL FOR SETTLEMENT EFFECTIVE DATE OF' #WS-PYMNT-SETTLMNT-DTE T*#DR-AMT 16X #TOT-DR-AMT ( 1 ) T*#CR-AMT 16X #TOT-CR-AMT ( 1 )
                new ReportEditMask ("MM/DD/YY"),new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1), new ReportEditMask 
                ("-ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new ColumnSpacing(16),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(2).nadd(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1));                                                                                    //Natural: ADD #TOT-CR-AMT ( 1 ) TO #TOT-CR-AMT ( 2 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(2).nadd(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1));                                                                                    //Natural: ADD #TOT-DR-AMT ( 1 ) TO #TOT-DR-AMT ( 2 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 1 ) #TOT-CR-AMT ( 1 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1).reset();
        }                                                                                                                                                                 //Natural: WHEN #CHECK-DTE-IND
        if (condition(pnd_Ws_Pnd_Check_Dte_Ind.getBoolean()))
        {
            decideConditionsMet357++;
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(3).nadd(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(2));                                                                                    //Natural: ADD #TOT-CR-AMT ( 2 ) TO #TOT-CR-AMT ( 3 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(3).nadd(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(2));                                                                                    //Natural: ADD #TOT-DR-AMT ( 2 ) TO #TOT-DR-AMT ( 3 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(2).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 2 ) #TOT-CR-AMT ( 2 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(2).reset();
        }                                                                                                                                                                 //Natural: WHEN #LEDGER-IND
        if (condition(pnd_Ws_Pnd_Ledger_Ind.getBoolean()))
        {
            decideConditionsMet357++;
            getReports().write(1, new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new          //Natural: WRITE ( 1 ) T*#DR-AMT 16X '-' ( 15 ) T*#CR-AMT 16X '-' ( 15 ) / 'TOTAL FOR LEDGER ACCOUNT NUMBER' #WS-INV-ACCT-LEDGR-NBR T*#DR-AMT 16X #TOT-DR-AMT ( 3 ) T*#CR-AMT 16X #TOT-CR-AMT ( 3 )
                ColumnSpacing(16),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR LEDGER ACCOUNT NUMBER",pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr,new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new 
                ColumnSpacing(16),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new ColumnSpacing(16),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(3), 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(2, new ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new ColumnSpacing(11),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new  //Natural: WRITE ( 2 ) T*#TOT-DR-AMT ( 1 ) 11X '-' ( 15 ) T*#TOT-CR-AMT ( 1 ) 11X '-' ( 15 ) / 'TOTAL FOR LEDGER ACCOUNT NUMBER' #WS-INV-ACCT-LEDGR-NBR T*#TOT-DR-AMT ( 1 ) 11X #TOT-DR-AMT ( 3 ) T*#TOT-CR-AMT ( 1 ) 11X #TOT-CR-AMT ( 3 )
                ColumnSpacing(11),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR LEDGER ACCOUNT NUMBER",pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr,new ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new 
                ColumnSpacing(11),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new 
                ColumnSpacing(11),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(3), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(4).nadd(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(3));                                                                                    //Natural: ADD #TOT-CR-AMT ( 3 ) TO #TOT-CR-AMT ( 4 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(4).nadd(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(3));                                                                                    //Natural: ADD #TOT-DR-AMT ( 3 ) TO #TOT-DR-AMT ( 4 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(3).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 3 ) #TOT-CR-AMT ( 3 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(3).reset();
        }                                                                                                                                                                 //Natural: WHEN #CURRENCY-IND
        if (condition(pnd_Ws_Pnd_Currency_Ind.getBoolean()))
        {
            decideConditionsMet357++;
            getReports().write(1, new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new          //Natural: WRITE ( 1 ) T*#DR-AMT 16X '-' ( 15 ) T*#CR-AMT 16X '-' ( 15 ) / 'TOTAL FOR' #WS-CNTRCT-CHECK-CRRNCY-DESC ( #WS-CNTRCT-CHECK-CRRNCY-CDE-NUM ) T*#DR-AMT 16X #TOT-DR-AMT ( 4 ) T*#CR-AMT 16X #TOT-CR-AMT ( 4 )
                ColumnSpacing(16),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR",pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc.getValue(pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde_Num),new 
                ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(4), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new 
                ColumnSpacing(16),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(4), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(2, new ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new ColumnSpacing(11),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new  //Natural: WRITE ( 2 ) T*#TOT-DR-AMT ( 1 ) 11X '-' ( 15 ) T*#TOT-CR-AMT ( 1 ) 11X '-' ( 15 ) / 'TOTAL FOR' #WS-CNTRCT-CHECK-CRRNCY-DESC ( #WS-CNTRCT-CHECK-CRRNCY-CDE-NUM ) T*#TOT-DR-AMT ( 1 ) 11X #TOT-DR-AMT ( 4 ) T*#TOT-CR-AMT ( 1 ) 11X #TOT-CR-AMT ( 4 )
                ColumnSpacing(11),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR",pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc.getValue(pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde_Num),new 
                ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new ColumnSpacing(11),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(4), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new ColumnSpacing(11),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(4), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(5).nadd(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(4));                                                                                    //Natural: ADD #TOT-CR-AMT ( 4 ) TO #TOT-CR-AMT ( 5 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(5).nadd(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(4));                                                                                    //Natural: ADD #TOT-DR-AMT ( 4 ) TO #TOT-DR-AMT ( 5 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(4).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 4 ) #TOT-CR-AMT ( 4 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(4).reset();
        }                                                                                                                                                                 //Natural: WHEN #ISA-IND
        if (condition(pnd_Ws_Pnd_Isa_Ind.getBoolean()))
        {
            decideConditionsMet357++;
            getReports().write(1, new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new          //Natural: WRITE ( 1 ) T*#DR-AMT 16X '-' ( 15 ) T*#CR-AMT 16X '-' ( 15 ) / 'TOTAL FOR ACCTG DATE' #WS-PYMNT-ACCTG-DTE 'FOR ISA:' #WS-TOTAL-ISA T*#DR-AMT 16X #TOT-DR-AMT ( 5 ) T*#CR-AMT 16X #TOT-CR-AMT ( 5 )
                ColumnSpacing(16),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR ACCTG DATE",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte, new ReportEditMask ("MM/DD/YY"),"FOR ISA:",pnd_Ws_Pnd_Ws_Total_Isa,new 
                ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(5), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new 
                ColumnSpacing(16),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(5), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(2, new ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new ColumnSpacing(11),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new  //Natural: WRITE ( 2 ) T*#TOT-DR-AMT ( 1 ) 11X '-' ( 15 ) T*#TOT-CR-AMT ( 1 ) 11X '-' ( 15 ) / 'TOTAL FOR ACCTG DATE' #WS-PYMNT-ACCTG-DTE 'FOR ISA:' #WS-TOTAL-ISA T*#TOT-DR-AMT ( 1 ) 11X #TOT-DR-AMT ( 5 ) T*#TOT-CR-AMT ( 1 ) 11X #TOT-CR-AMT ( 5 )
                ColumnSpacing(11),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR ACCTG DATE",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte, new ReportEditMask ("MM/DD/YY"),"FOR ISA:",pnd_Ws_Pnd_Ws_Total_Isa,new 
                ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new ColumnSpacing(11),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(5), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new ColumnSpacing(11),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(5), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(6).nadd(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(5));                                                                                    //Natural: ADD #TOT-CR-AMT ( 5 ) TO #TOT-CR-AMT ( 6 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(6).nadd(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(5));                                                                                    //Natural: ADD #TOT-DR-AMT ( 5 ) TO #TOT-DR-AMT ( 6 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(5).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 5 ) #TOT-CR-AMT ( 5 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(5).reset();
        }                                                                                                                                                                 //Natural: WHEN #ACCTG-DTE-IND
        if (condition(pnd_Ws_Pnd_Acctg_Dte_Ind.getBoolean()))
        {
            decideConditionsMet357++;
            getReports().write(1, new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new          //Natural: WRITE ( 1 ) T*#DR-AMT 16X '-' ( 15 ) T*#CR-AMT 16X '-' ( 15 ) / 'TOTAL FOR ACCTG DATE OF' #WS-PYMNT-ACCTG-DTE 'FOR ALL FUNDS' T*#DR-AMT 16X #TOT-DR-AMT ( 6 ) T*#CR-AMT 16X #TOT-CR-AMT ( 6 )
                ColumnSpacing(16),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR ACCTG DATE OF",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte, new ReportEditMask ("MM/DD/YY"),"FOR ALL FUNDS",new 
                ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(6), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new 
                ColumnSpacing(16),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(6), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(2, new ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new ColumnSpacing(11),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new  //Natural: WRITE ( 2 ) T*#TOT-DR-AMT ( 1 ) 11X '-' ( 15 ) T*#TOT-CR-AMT ( 1 ) 11X '-' ( 15 ) / 'TOTAL FOR ACCTG DATE OF' #WS-PYMNT-ACCTG-DTE 'FOR ALL FUNDS' T*#TOT-DR-AMT ( 1 ) 11X #TOT-DR-AMT ( 6 ) T*#TOT-CR-AMT ( 1 ) 11X #TOT-CR-AMT ( 6 )
                ColumnSpacing(11),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR ACCTG DATE OF",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte, new ReportEditMask ("MM/DD/YY"),"FOR ALL FUNDS",new 
                ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new ColumnSpacing(11),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(6), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new 
                ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new ColumnSpacing(11),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(6), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(7).nadd(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(6));                                                                                    //Natural: ADD #TOT-CR-AMT ( 6 ) TO #TOT-CR-AMT ( 7 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(7).nadd(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(6));                                                                                    //Natural: ADD #TOT-DR-AMT ( 6 ) TO #TOT-DR-AMT ( 7 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(6).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 6 ) #TOT-CR-AMT ( 6 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(6).reset();
        }                                                                                                                                                                 //Natural: WHEN #ORGN-CDE-IND
        if (condition(pnd_Ws_Pnd_Orgn_Cde_Ind.getBoolean()))
        {
            decideConditionsMet357++;
            pnd_Ws_Pnd_Ws_Total_Orgn.setValue(DbsUtil.compress(pnd_Ws_Pnd_Orgn_Array.getValue(pnd_Ws_Pnd_Orgn_Sub), "INTERFACE DATE OF ", pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte)); //Natural: COMPRESS #ORGN-ARRAY ( #ORGN-SUB ) 'INTERFACE DATE OF ' #WS-PYMNT-INTRFCE-DTE INTO #WS-TOTAL-ORGN
            getReports().write(1, new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new          //Natural: WRITE ( 1 ) T*#DR-AMT 16X '-' ( 15 ) T*#CR-AMT 16X '-' ( 15 ) / 'TOTAL FOR' #WS-TOTAL-ORGN T*#DR-AMT 16X #TOT-DR-AMT ( 7 ) T*#CR-AMT 16X #TOT-CR-AMT ( 7 )
                ColumnSpacing(16),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR",pnd_Ws_Pnd_Ws_Total_Orgn,new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(16),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(7), 
                new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new ColumnSpacing(16),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(7), new 
                ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            getReports().write(2, new ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new ColumnSpacing(11),"-",new RepeatItem(15),new ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new  //Natural: WRITE ( 2 ) T*#TOT-DR-AMT ( 1 ) 11X '-' ( 15 ) T*#TOT-CR-AMT ( 1 ) 11X '-' ( 15 ) / 'TOTAL FOR' #WS-TOTAL-ORGN T*#TOT-DR-AMT ( 1 ) 11X #TOT-DR-AMT ( 7 ) T*#TOT-CR-AMT ( 1 ) 11X #TOT-CR-AMT ( 7 )
                ColumnSpacing(11),"-",new RepeatItem(15),NEWLINE,"TOTAL FOR",pnd_Ws_Pnd_Ws_Total_Orgn,new ReportTAsterisk(pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1)),new 
                ColumnSpacing(11),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(7), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1)),new 
                ColumnSpacing(11),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(7), new ReportEditMask ("-ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(7).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 7 ) #TOT-CR-AMT ( 7 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(7).reset();
        }                                                                                                                                                                 //Natural: WHEN #CSR-IND AND #WS-CNTRCT-CANCEL-RDRW-IND NE ' '
        if (condition(pnd_Ws_Pnd_Csr_Ind.getBoolean() && pnd_Ws_Pnd_Ws_Cntrct_Cancel_Rdrw_Ind.notEquals(" ")))
        {
            decideConditionsMet357++;
            //*    WRITE (1) 11T #WS-HEADER-CSR
            //*    WRITE (2) 11T #WS-HEADER-CSR
            //*  09-30-1999
            getReports().write(1, new TabSetting(6),pnd_Ws_Pnd_Ws_Header_Csr);                                                                                            //Natural: WRITE ( 1 ) 06T #WS-HEADER-CSR
            if (Global.isEscape()) return;
            //*  09-30-1999
            getReports().write(2, new TabSetting(6),pnd_Ws_Pnd_Ws_Header_Csr);                                                                                            //Natural: WRITE ( 2 ) 06T #WS-HEADER-CSR
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet357 > 0))
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
            if (condition(pnd_Ws_Pnd_Ledger_Ind.getBoolean() || pnd_Ws_Pnd_Currency_Ind.getBoolean() || pnd_Ws_Pnd_Check_Dte_Ind.getBoolean() || pnd_Ws_Pnd_Acctg_Dte_Ind.getBoolean()  //Natural: IF #LEDGER-IND OR #CURRENCY-IND OR #CHECK-DTE-IND OR #ACCTG-DTE-IND OR #ISA-IND OR #ORGN-CDE-IND
                || pnd_Ws_Pnd_Isa_Ind.getBoolean() || pnd_Ws_Pnd_Orgn_Cde_Ind.getBoolean()))
            {
                getReports().skip(2, 1);                                                                                                                                  //Natural: SKIP ( 2 ) 1 LINES
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet357 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Ws_Pnd_Ws_Cntrct_Cancel_Rdrw_Ind.notEquals(" ")))                                                                                   //Natural: IF #WS-CNTRCT-CANCEL-RDRW-IND NE ' '
                    {
                        //*    WRITE (1) 54T #WS-HEADER-CSR                           /* 09-30-1999
                        //*  09-30-1999
                        getReports().write(1, new TabSetting(49),pnd_Ws_Pnd_Ws_Header_Csr);                                                                               //Natural: WRITE ( 1 ) 49T #WS-HEADER-CSR
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(1, new TabSetting(54),"INTERFACE DATE:",pnd_Pymnt_Intrfce_Dte,NEWLINE,new TabSetting(67),pnd_Ws_Pnd_Ws_Header_Isa,NEWLINE,"ACCOUNTING DATE   :",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte,  //Natural: WRITE ( 1 ) 54T 'INTERFACE DATE:' #PYMNT-INTRFCE-DTE / 67T #WS-HEADER-ISA / 'ACCOUNTING DATE   :' #WS-PYMNT-ACCTG-DTE / 'CURRENCY CODE     :' #WS-CNTRCT-CHECK-CRRNCY-DESC ( #WS-CNTRCT-CHECK-CRRNCY-CDE-NUM ) / 'LEDGER ACCOUNT NUM:' #WS-INV-ACCT-LEDGR-NBR 'NAME:' #WS-LEDGER-DESC
                        new ReportEditMask ("MM/DD/YY"),NEWLINE,"CURRENCY CODE     :",pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc.getValue(pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde_Num),
                        NEWLINE,"LEDGER ACCOUNT NUM:",pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr,"NAME:",pnd_Ws_Pnd_Ws_Ledger_Desc);
                    getReports().skip(1, 1);                                                                                                                              //Natural: SKIP ( 1 ) 1 LINES
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    if (condition(pnd_Ws_Pnd_Ws_Cntrct_Cancel_Rdrw_Ind.notEquals(" ")))                                                                                   //Natural: IF #WS-CNTRCT-CANCEL-RDRW-IND NE ' '
                    {
                        //*    WRITE (2) 54T #WS-HEADER-CSR                           /* 09-30-1999
                        //*  09-30-1999
                        getReports().write(2, new TabSetting(49),pnd_Ws_Pnd_Ws_Header_Csr);                                                                               //Natural: WRITE ( 2 ) 49T #WS-HEADER-CSR
                    }                                                                                                                                                     //Natural: END-IF
                    getReports().write(2, new TabSetting(54),"INTERFACE DATE:",pnd_Pymnt_Intrfce_Dte,NEWLINE,new TabSetting(67),pnd_Ws_Pnd_Ws_Header_Isa,NEWLINE,"ACCOUNTING DATE   :",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte,  //Natural: WRITE ( 2 ) 54T 'INTERFACE DATE:' #PYMNT-INTRFCE-DTE / 67T #WS-HEADER-ISA / 'ACCOUNTING DATE   :' #WS-PYMNT-ACCTG-DTE / 'CURRENCY CODE     :' #WS-CNTRCT-CHECK-CRRNCY-DESC ( #WS-CNTRCT-CHECK-CRRNCY-CDE-NUM )
                        new ReportEditMask ("MM/DD/YY"),NEWLINE,"CURRENCY CODE     :",pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Desc.getValue(pnd_Ws_Pnd_Ws_Cntrct_Check_Crrncy_Cde_Num));
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1 LINES
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Settlmnt_DteIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Check_DteIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_Ledgr_NbrIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Check_Crrncy_CdeIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Check_Crrncy_Cde().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Acctg_Dte().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Orgn_CdeIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Orgn_Cde().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_Ind().isBreak(endOfData);
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Settlmnt_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Check_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_Ledgr_NbrIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Check_Crrncy_CdeIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Orgn_CdeIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak))
        {
            pnd_Ws_Pnd_Settl_Dte_Ind.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #SETTL-DTE-IND
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1).setValue(readWork01Pnd_Cr_AmtSum160);                                                                                       //Natural: MOVE SUM ( #CR-AMT ) TO #TOT-CR-AMT ( 1 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1).setValue(readWork01Pnd_Dr_AmtSum160);                                                                                       //Natural: MOVE SUM ( #DR-AMT ) TO #TOT-DR-AMT ( 1 )
            readWork01Pnd_Cr_AmtSum160.setDec(new DbsDecimal(0));                                                                                                         //Natural: END-BREAK
            readWork01Pnd_Dr_AmtSum160.setDec(new DbsDecimal(0));
        }
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Check_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_Ledgr_NbrIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Check_Crrncy_CdeIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Orgn_CdeIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak))
        {
            pnd_Ws_Pnd_Check_Dte_Ind.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #CHECK-DTE-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_Ledgr_NbrIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Check_Crrncy_CdeIsBreak || 
            ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Orgn_CdeIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak))
        {
            pnd_Ws_Pnd_Ledger_Ind.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #LEDGER-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Check_Crrncy_CdeIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Orgn_CdeIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak))
        {
            pnd_Ws_Pnd_Currency_Ind.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #CURRENCY-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Orgn_CdeIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak))
        {
            pnd_Ws_Pnd_Isa_Ind.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #ISA-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Orgn_CdeIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak))
        {
            pnd_Ws_Pnd_Acctg_Dte_Ind.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #ACCTG-DTE-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Orgn_CdeIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak))
        {
            pnd_Ws_Pnd_Orgn_Cde_Ind.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #ORGN-CDE-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Cntrct_Cancel_Rdrw_IndIsBreak))
        {
            pnd_Ws_Pnd_Csr_Ind.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #CSR-IND
            pnd_Ws_Pnd_Orgn_Cde_Ind.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO #ORGN-CDE-IND
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");
        Global.format(2, "PS=60 LS=133 ZP=ON");

        getReports().write(1, ReportOption.TITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),
            "REPORT: RPT1",NEWLINE,pnd_Ws_Pnd_Ws_Header_Orgn,"SETTLEMENT SYSTEM - DETAIL LEDGER REPORT");
        getReports().write(2, ReportOption.TITLE,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), 
            new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),
            "REPORT: RPT2",NEWLINE,pnd_Ws_Pnd_Ws_Header_Orgn,"SETTLEMENT SYSTEM - SUMMARY LEDGER REPORT");

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"SETTLEMENT/EFFECTIVE/DATE",
        		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte(),"/PAYMENT/DATE",
        		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte(),"//ANNUITANT NAME",
        		ldaFcpl121.getPnd_Ledger_Extract_Ph_Last_Name(),"/",
        		ldaFcpl121.getPnd_Ledger_Extract_Ph_First_Name(),"/",
        		ldaFcpl121.getPnd_Ledger_Extract_Ph_Middle_Name(), new AlphanumericLength (1),"/CONTRACT/NUMBER",
        		pnd_Ws_Pnd_Contract,"WORK/PROCESS/NUMBER",
        		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Corp_Wpid(),"/DEBIT/AMOUNT",
        		pnd_Ws_Pnd_Dr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ColumnSpacing(16),"/CREDIT/AMOUNT",
        		pnd_Ws_Pnd_Cr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
        getReports().setDisplayColumns(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"LEDGER/ACCOUNT/NUMBER",
        		pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr, new IdenticalSuppress(true),"//DESCRIPTION",
        		pnd_Ws_Pnd_Ws_Ledger_Desc, new IdenticalSuppress(true),"/PAYMENT/DATE",
        		pnd_Ws_Pnd_Ws_Pymnt_Check_Dte,"SETTLEMENT/EFFECTIVE/DATE",
        		pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte,"/DEBIT/AMOUNT",
        		pnd_Ws_Pnd_Tot_Dr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ColumnSpacing(11),"/CREDIT/AMOUNT",
        		pnd_Ws_Pnd_Tot_Cr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
    }
    private void CheckAtStartofData154() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte.setValueEdited(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte(),new ReportEditMask("MM/DD/YYYY"));                        //Natural: MOVE EDITED PYMNT-INTRFCE-DTE ( EM = MM/DD/YYYY ) TO #WS-PYMNT-INTRFCE-DTE
            //*  INITIALIZATION
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
            sub_Init_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
