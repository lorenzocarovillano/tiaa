/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:28 PM
**        * FROM NATURAL PROGRAM : Fcpp823
************************************************************
**        * FILE NAME            : Fcpp823.java
**        * CLASS NAME           : Fcpp823
**        * INSTANCE NAME        : Fcpp823
************************************************************
************************************************************************
* PROGRAM  : FCPP823
* SYSTEM   : CPS
* TITLE    : LOAD "AP" CONTROL RECORD TO THE DATABASE FILE198
*
* FUNCTION : THIS PROGRAM READS THE CONTROL RECORD FROM THE SEQUENTIAL
*            CONTROL FILE AND LOADS IT INTO THE DATABASE.
*            ADABAS UTILITIES.
* HISTORY
*
* 01/15/2020: CTS CPS SUNSET (CHG694525) TAG: CTS-0115
*             TAG ->  /* PAYMENT STRATEGY - PH 1
*             1. THERE WILL BE NO MORE 'FCP-CONS-CNTL' DB FILE
*             2. DUPLICATE CHECK WILL BE DONE BASED ON PREVIOUSLY
*               CREATED GDG BACKUP FILE (WORK FILE 01).
*             3. THIS CODE WILL ONLY DUP CHECK CONTROL REC CREATED FROM
*               FCPP823.
*             4. ON SUCCESSFUL COMPLETION OF THIS PROGRAM STEP
*               ("EXTR010" OF 'P2280CPM') A NEW STEP ("COPY020" OF
*               'P2280CPM') WILL CREATE A NEW BACKUP FILE. THIS FILE
*               WILL BE USED FOR NEXT CYCLE DUP CHECK.
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp823 extends BLNatBase
{
    // Data Areas
    private PdaFcpacntl pdaFcpacntl;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup bkcntl;
    private DbsField bkcntl_Cntl_Orgn_Cde;
    private DbsField bkcntl_Cntl_Check_Dte;
    private DbsField bkcntl_Cntl_Invrse_Dte;
    private DbsField bkcntl_Cntl_Occur_Cnt;
    private DbsField bkcntl_Cntl_Type_Cde;

    private DbsGroup bkcntl__R_Field_1;
    private DbsField bkcntl_Cntl_Occur_Num;
    private DbsField bkcntl_Cntl_Pymnt_Cnt;
    private DbsField bkcntl_Cntl_Rec_Cnt;
    private DbsField bkcntl_Cntl_Gross_Amt;
    private DbsField bkcntl_Cntl_Net_Amt;

    private DbsGroup pnd_Sk;
    private DbsField pnd_Sk_Cntl_Orgn_Cde;
    private DbsField pnd_Sk_Cntl_Invrse_Dte;

    private DbsGroup pnd_Sk__R_Field_2;
    private DbsField pnd_Sk_Pnd_Sk_All;

    private DbsGroup pnd_Ck;
    private DbsField pnd_Ck_Cntl_Orgn_Cde;
    private DbsField pnd_Ck_Cntl_Invrse_Dte;

    private DbsGroup pnd_Ck__R_Field_3;
    private DbsField pnd_Ck_Pnd_Ck_All;
    private DbsField pnd_I;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpacntl = new PdaFcpacntl(localVariables);

        // Local Variables

        bkcntl = localVariables.newGroupInRecord("bkcntl", "BKCNTL");
        bkcntl_Cntl_Orgn_Cde = bkcntl.newFieldInGroup("bkcntl_Cntl_Orgn_Cde", "CNTL-ORGN-CDE", FieldType.STRING, 2);
        bkcntl_Cntl_Check_Dte = bkcntl.newFieldInGroup("bkcntl_Cntl_Check_Dte", "CNTL-CHECK-DTE", FieldType.DATE);
        bkcntl_Cntl_Invrse_Dte = bkcntl.newFieldInGroup("bkcntl_Cntl_Invrse_Dte", "CNTL-INVRSE-DTE", FieldType.NUMERIC, 8);
        bkcntl_Cntl_Occur_Cnt = bkcntl.newFieldInGroup("bkcntl_Cntl_Occur_Cnt", "CNTL-OCCUR-CNT", FieldType.PACKED_DECIMAL, 3);
        bkcntl_Cntl_Type_Cde = bkcntl.newFieldArrayInGroup("bkcntl_Cntl_Type_Cde", "CNTL-TYPE-CDE", FieldType.STRING, 2, new DbsArrayController(1, 50));

        bkcntl__R_Field_1 = bkcntl.newGroupInGroup("bkcntl__R_Field_1", "REDEFINE", bkcntl_Cntl_Type_Cde);
        bkcntl_Cntl_Occur_Num = bkcntl__R_Field_1.newFieldArrayInGroup("bkcntl_Cntl_Occur_Num", "CNTL-OCCUR-NUM", FieldType.NUMERIC, 2, new DbsArrayController(1, 
            50));
        bkcntl_Cntl_Pymnt_Cnt = bkcntl.newFieldArrayInGroup("bkcntl_Cntl_Pymnt_Cnt", "CNTL-PYMNT-CNT", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            50));
        bkcntl_Cntl_Rec_Cnt = bkcntl.newFieldArrayInGroup("bkcntl_Cntl_Rec_Cnt", "CNTL-REC-CNT", FieldType.PACKED_DECIMAL, 9, new DbsArrayController(1, 
            50));
        bkcntl_Cntl_Gross_Amt = bkcntl.newFieldArrayInGroup("bkcntl_Cntl_Gross_Amt", "CNTL-GROSS-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            50));
        bkcntl_Cntl_Net_Amt = bkcntl.newFieldArrayInGroup("bkcntl_Cntl_Net_Amt", "CNTL-NET-AMT", FieldType.PACKED_DECIMAL, 13, 2, new DbsArrayController(1, 
            50));

        pnd_Sk = localVariables.newGroupInRecord("pnd_Sk", "#SK");
        pnd_Sk_Cntl_Orgn_Cde = pnd_Sk.newFieldInGroup("pnd_Sk_Cntl_Orgn_Cde", "CNTL-ORGN-CDE", FieldType.STRING, 2);
        pnd_Sk_Cntl_Invrse_Dte = pnd_Sk.newFieldInGroup("pnd_Sk_Cntl_Invrse_Dte", "CNTL-INVRSE-DTE", FieldType.NUMERIC, 8);

        pnd_Sk__R_Field_2 = localVariables.newGroupInRecord("pnd_Sk__R_Field_2", "REDEFINE", pnd_Sk);
        pnd_Sk_Pnd_Sk_All = pnd_Sk__R_Field_2.newFieldInGroup("pnd_Sk_Pnd_Sk_All", "#SK-ALL", FieldType.STRING, 10);

        pnd_Ck = localVariables.newGroupInRecord("pnd_Ck", "#CK");
        pnd_Ck_Cntl_Orgn_Cde = pnd_Ck.newFieldInGroup("pnd_Ck_Cntl_Orgn_Cde", "CNTL-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ck_Cntl_Invrse_Dte = pnd_Ck.newFieldInGroup("pnd_Ck_Cntl_Invrse_Dte", "CNTL-INVRSE-DTE", FieldType.NUMERIC, 8);

        pnd_Ck__R_Field_3 = localVariables.newGroupInRecord("pnd_Ck__R_Field_3", "REDEFINE", pnd_Ck);
        pnd_Ck_Pnd_Ck_All = pnd_Ck__R_Field_3.newFieldInGroup("pnd_Ck_Pnd_Ck_All", "#CK-ALL", FieldType.STRING, 10);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.PACKED_DECIMAL, 3);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp823() throws Exception
    {
        super("Fcpp823");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) LS = 133 PS = 58 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 51T 'ANNUITY PAYMENTS UPDATE CONTROL FILE REPORT' 120T 'REPORT: RPT0' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 15 CNTL
        while (condition(getWorkFiles().read(15, pdaFcpacntl.getCntl())))
        {
            pnd_Sk.setValuesByName(pdaFcpacntl.getCntl());                                                                                                                //Natural: MOVE BY NAME CNTL TO #SK
            getReports().write(0, NEWLINE,NEWLINE,"Content of the sequential file record:",NEWLINE,NEWLINE);                                                              //Natural: WRITE // 'Content of the sequential file record:' //
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM SHOW-SEQ-REC
            sub_Show_Seq_Rec();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  CTS-0115 >>
            READWORK02:                                                                                                                                                   //Natural: READ WORK FILE 1 BKCNTL
            while (condition(getWorkFiles().read(1, bkcntl)))
            {
                pnd_Ck.setValuesByName(bkcntl);                                                                                                                           //Natural: MOVE BY NAME BKCNTL TO #CK
                getReports().write(0, NEWLINE,NEWLINE,"CONTENT OF THE GDG BACKUP FILE RECORD:",NEWLINE,NEWLINE);                                                          //Natural: WRITE // 'CONTENT OF THE GDG BACKUP FILE RECORD:' //
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM SHOW-BKP-REC
                sub_Show_Bkp_Rec();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-WORK
            READWORK02_Exit:
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*   RD1.  READ (1) FCP-CONS-CNTRL-VIEW
            //*       BY CNTL-ORG-CDE-INVRSE-DTE STARTING FROM #SK-ALL
            //*   END-READ /* (RD1.)
            //*  .... IF REC ALREADY ON THE DB - ABEND
            //*   MOVE BY NAME FCP-CONS-CNTRL-VIEW TO #CK
            //*  CTS-0115 <<
            if (condition(pnd_Ck_Pnd_Ck_All.equals(pnd_Sk_Pnd_Sk_All)))                                                                                                   //Natural: IF #CK-ALL EQ #SK-ALL
            {
                //*     #ISN := *ISN (RD1.)
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                sub_Error_Display_Start();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().write(0, NEWLINE,"***",new TabSetting(25),"PROG: ",Global.getPROGRAM(),"TIME: ",Global.getTIME(),new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 0 ) / '***' 25T '=' *PROGRAM '=' *TIME 77T '***' / '***' 25T 'A control rec is already on the Bkp File' 77T '***' / '***' 25T '=' #CK.CNTL-ORGN-CDE 77T '***' / '***' 25T '=' #CK.CNTL-INVRSE-DTE 77T '***' / '***' 25T 'The program TERMINATES !!!!' 77T '***' / '***' 25T 'Content of the DATABSE rec:' 77T '***'
                    TabSetting(25),"A control rec is already on the Bkp File",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"=",pnd_Ck_Cntl_Orgn_Cde,new 
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"=",pnd_Ck_Cntl_Invrse_Dte,new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"The program TERMINATES !!!!",new 
                    TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"Content of the DATABSE rec:",new TabSetting(77),"***");
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                sub_Error_Display_End();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                //*    PERFORM SHOW-DB-REC     /* CTS-0115
                DbsUtil.terminate(41);  if (true) return;                                                                                                                 //Natural: TERMINATE 41
            }                                                                                                                                                             //Natural: END-IF
            //*  CTS-0115 >>
            //*  ..... ADD THE REC TO THE DB
            //*   RESET FCP-CONS-CNTRL-VIEW
            //*   MOVE BY NAME CNTL TO FCP-CONS-CNTRL-VIEW
            //*   FCP-CONS-CNTRL-VIEW.CNTL-CNT(*) := CNTL.CNTL-REC-CNT (*)
            //*   STR1.  STORE FCP-CONS-CNTRL-VIEW
            //*   END TRANSACTION
            //*   #ISN := *ISN(STR1.)
            //*   GT1.  GET FCP-CONS-CNTRL-VIEW #ISN
            //*   WRITE *PROGRAM *DATU '-' *TIMN
            //*     / 'A record was ADDED to the Control File on the DB, for:'
            //*     / 5X '=' FCP-CONS-CNTRL-VIEW.CNTL-ORGN-CDE
            //*     / 5X '=' FCP-CONS-CNTRL-VIEW.CNTL-INVRSE-DTE
            //*     // 'Content of the DATABASE record:' //
            //*   PERFORM SHOW-DB-REC
            //*   ESCAPE BOTTOM
            //*  CTS-0115 <<
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //*  EMPTY FILE
        if (condition(pdaFcpacntl.getCntl_Cntl_Orgn_Cde().equals(" ")))                                                                                                   //Natural: IF CNTL.CNTL-ORGN-CDE EQ ' '
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, NEWLINE,"***",new TabSetting(25),"PROG: ",Global.getPROGRAM(),"TIME: ",Global.getTIME(),new TabSetting(77),"***",NEWLINE,"***",new      //Natural: WRITE ( 0 ) / '***' 25T '=' *PROGRAM '=' *TIME 77T '***' / '***' 25T 'The Seq File Control Rec is EMPTY!' 77T '***' / '***' 25T 'The program TERMINATES !!!!' 77T '***'
                TabSetting(25),"The Seq File Control Rec is EMPTY!",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"The program TERMINATES !!!!",new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(42);  if (true) return;                                                                                                                     //Natural: TERMINATE 42
        }                                                                                                                                                                 //Natural: END-IF
        //* ********************************************************************
        //* **********      S U B R O U T I N E S       ************************
        //* ********************************************************************
        //*  CTS-0115 >>
        //* *=================================================================
        //*  DEFINE SUBROUTINE SHOW-DB-REC
        //* *=================================================================
        //*  WRITE '=' #ISN
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTRCT-RCRD-TYP
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTRCT-ACT-IND
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-ORGN-CDE
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-CYCLE-DTE
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-CHECK-DTE
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-INVRSE-DTE
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-SEQ-START-CNT
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-SEQ-END-CNT
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-MONTH-END-IND
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-MONTH-END-DTE
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-BANK-ACCT-NBR
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-RMRK-LINE1-TXT
        //*    / '=' FCP-CONS-CNTRL-VIEW.CNTL-RMRK-LINE2-TXT
        //*    / '=' FCP-CONS-CNTRL-VIEW.C*CNTL-AMT-PE-GRP
        //*    // 'THE CONTROL AMOUNT FIELDS ARE PRINTED ON THE FOLLOWING PAGE.'
        //*  NEWPAGE
        //*  WRITE
        //*    / 5T 'TYPE' 10T '   '   20T 'GROSS' 35T 'NET'
        //*    / 5T 'CDE ' 10T 'CNT'   20T 'AMT  ' 35T 'AMT'
        //*    / 5T '----' 10T '-'(9)  20T '-'(14) 35T '-' (14)
        //*  FOR #I 1 TO FCP-CONS-CNTRL-VIEW.C*CNTL-AMT-PE-GRP
        //*    WRITE
        //*      05T FCP-CONS-CNTRL-VIEW.CNTL-TYPE-CDE(#I)
        //*      10T FCP-CONS-CNTRL-VIEW.CNTL-CNT(#I) (EM=Z,ZZZ,ZZ9)
        //*      20T FCP-CONS-CNTRL-VIEW.CNTL-GROSS-AMT(#I)(EM=ZZZZ,ZZZ,ZZZ.99)
        //*      35T FCP-CONS-CNTRL-VIEW.CNTL-NET-AMT(#I)  (EM=ZZZZ,ZZZ,ZZZ.99)
        //*  END-FOR
        //*  END-SUBROUTINE /* SHOW-DB-REC
        //* *=================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SHOW-BKP-REC
        //*  CTS-0115 <<
        //* *=================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SHOW-SEQ-REC
        //*  =====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //*  =====================================================================
        //*  =====================================================================
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  =====================================================================
    }
    private void sub_Show_Bkp_Rec() throws Exception                                                                                                                      //Natural: SHOW-BKP-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *=================================================================
        getReports().write(0, NEWLINE,"=",bkcntl_Cntl_Orgn_Cde,NEWLINE,"=",bkcntl_Cntl_Check_Dte,NEWLINE,"=",bkcntl_Cntl_Invrse_Dte,NEWLINE,"=",bkcntl_Cntl_Occur_Cnt,    //Natural: WRITE / '=' BKCNTL.CNTL-ORGN-CDE / '=' BKCNTL.CNTL-CHECK-DTE / '=' BKCNTL.CNTL-INVRSE-DTE / '=' BKCNTL.CNTL-OCCUR-CNT // 'THE BKPCNTL AMOUNT FIELDS ARE PRINTED ON THE FOLLOWING PAGE.'
            NEWLINE,NEWLINE,"THE BKPCNTL AMOUNT FIELDS ARE PRINTED ON THE FOLLOWING PAGE.");
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,new TabSetting(5),"TYPE",new TabSetting(10),"PAYMENTS",new TabSetting(23),"RECORDS",new TabSetting(35),"GROSS",new                  //Natural: WRITE / 5T 'TYPE' 10T 'PAYMENTS' 23T 'RECORDS' 35T 'GROSS' 52T 'NET' / 5T 'CDE ' 10T 'CNT' 23T 'CNT' 35T 'AMT  ' 52T 'AMT' / 5T '----' 10T '-' ( 9 ) 23T '-' ( 9 ) 35T '-' ( 14 ) 52T '-' ( 14 )
            TabSetting(52),"NET",NEWLINE,new TabSetting(5),"CDE ",new TabSetting(10),"CNT",new TabSetting(23),"CNT",new TabSetting(35),"AMT  ",new TabSetting(52),"AMT",NEWLINE,new 
            TabSetting(5),"----",new TabSetting(10),"-",new RepeatItem(9),new TabSetting(23),"-",new RepeatItem(9),new TabSetting(35),"-",new RepeatItem(14),new 
            TabSetting(52),"-",new RepeatItem(14));
        if (Global.isEscape()) return;
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO BKCNTL.CNTL-OCCUR-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(bkcntl_Cntl_Occur_Cnt)); pnd_I.nadd(1))
        {
            getReports().write(0, new TabSetting(5),bkcntl_Cntl_Type_Cde.getValue(pnd_I),new TabSetting(10),bkcntl_Cntl_Pymnt_Cnt.getValue(pnd_I), new                    //Natural: WRITE 05T BKCNTL.CNTL-TYPE-CDE ( #I ) 10T BKCNTL.CNTL-PYMNT-CNT ( #I ) ( EM = Z,ZZZ,ZZ9 ) 23T BKCNTL.CNTL-REC-CNT ( #I ) ( EM = Z,ZZZ,ZZ9 ) 35T BKCNTL.CNTL-GROSS-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZZ.99 ) 52T BKCNTL.CNTL-NET-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZZ.99 )
                ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(23),bkcntl_Cntl_Rec_Cnt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(35),bkcntl_Cntl_Gross_Amt.getValue(pnd_I), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),new TabSetting(52),bkcntl_Cntl_Net_Amt.getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SHOW-BKP-REC
    }
    private void sub_Show_Seq_Rec() throws Exception                                                                                                                      //Natural: SHOW-SEQ-REC
    {
        if (BLNatReinput.isReinput()) return;

        //* *=================================================================
        getReports().write(0, NEWLINE,"=",pdaFcpacntl.getCntl_Cntl_Orgn_Cde(),NEWLINE,"=",pdaFcpacntl.getCntl_Cntl_Check_Dte(),NEWLINE,"=",pdaFcpacntl.getCntl_Cntl_Invrse_Dte(), //Natural: WRITE / '=' CNTL.CNTL-ORGN-CDE / '=' CNTL.CNTL-CHECK-DTE / '=' CNTL.CNTL-INVRSE-DTE / '=' CNTL.CNTL-OCCUR-CNT // 'THE CONTROL AMOUNT FIELDS ARE PRINTED ON THE FOLLOWING PAGE.'
            NEWLINE,"=",pdaFcpacntl.getCntl_Cntl_Occur_Cnt(),NEWLINE,NEWLINE,"THE CONTROL AMOUNT FIELDS ARE PRINTED ON THE FOLLOWING PAGE.");
        if (Global.isEscape()) return;
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,new TabSetting(5),"TYPE",new TabSetting(10),"PAYMENTS",new TabSetting(23),"RECORDS",new TabSetting(35),"GROSS",new                  //Natural: WRITE / 5T 'TYPE' 10T 'PAYMENTS' 23T 'RECORDS' 35T 'GROSS' 52T 'NET' / 5T 'CDE ' 10T 'CNT' 23T 'CNT' 35T 'AMT  ' 52T 'AMT' / 5T '----' 10T '-' ( 9 ) 23T '-' ( 9 ) 35T '-' ( 14 ) 52T '-' ( 14 )
            TabSetting(52),"NET",NEWLINE,new TabSetting(5),"CDE ",new TabSetting(10),"CNT",new TabSetting(23),"CNT",new TabSetting(35),"AMT  ",new TabSetting(52),"AMT",NEWLINE,new 
            TabSetting(5),"----",new TabSetting(10),"-",new RepeatItem(9),new TabSetting(23),"-",new RepeatItem(9),new TabSetting(35),"-",new RepeatItem(14),new 
            TabSetting(52),"-",new RepeatItem(14));
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO CNTL.CNTL-OCCUR-CNT
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pdaFcpacntl.getCntl_Cntl_Occur_Cnt())); pnd_I.nadd(1))
        {
            getReports().write(0, new TabSetting(5),pdaFcpacntl.getCntl_Cntl_Type_Cde().getValue(pnd_I),new TabSetting(10),pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue(pnd_I),  //Natural: WRITE 05T CNTL.CNTL-TYPE-CDE ( #I ) 10T CNTL.CNTL-PYMNT-CNT ( #I ) ( EM = Z,ZZZ,ZZ9 ) 23T CNTL.CNTL-REC-CNT ( #I ) ( EM = Z,ZZZ,ZZ9 ) 35T CNTL.CNTL-GROSS-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZZ.99 ) 52T CNTL.CNTL-NET-AMT ( #I ) ( EM = ZZZZ,ZZZ,ZZZ.99 )
                new ReportEditMask ("Z,ZZZ,ZZ9"),new TabSetting(23),pdaFcpacntl.getCntl_Cntl_Rec_Cnt().getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9"),new 
                TabSetting(35),pdaFcpacntl.getCntl_Cntl_Gross_Amt().getValue(pnd_I), new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"),new TabSetting(52),pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(pnd_I), 
                new ReportEditMask ("ZZZZ,ZZZ,ZZZ.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  SHOW-SEQ-REC
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE ( 00 )
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE ( 00 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE ( 00 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=58 ZP=ON");

        getReports().write(0, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(51),"ANNUITY PAYMENTS UPDATE CONTROL FILE REPORT",new TabSetting(120),"REPORT: RPT0",NEWLINE,NEWLINE);
    }
}
