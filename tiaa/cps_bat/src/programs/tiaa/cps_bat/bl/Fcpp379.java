/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:17:18 PM
**        * FROM NATURAL PROGRAM : Fcpp379
************************************************************
**        * FILE NAME            : Fcpp379.java
**        * CLASS NAME           : Fcpp379
**        * INSTANCE NAME        : Fcpp379
************************************************************
************************************************************************
* PROGRAM  : FCPP379
* SYSTEM   : CPS
* TITLE    : EXTRACT
* FUNCTION : THIS PROGRAM WILL UPDATE ADABAS PAYMENT FILE WITH
*            CHECK NUMBER AND CHECK SEQUENCE NUMBER.
*
* HISTORY
*          : 02/01/99   R. CARREON
*          : - CREATE ANNOTATION RECORD FOR EXPEDITED HOLDS (OV00;USPS)
*
*          : 08/05/99 - LEON GURTOVNIK
*          :  MODIFY PROGRAM TO ACCEPT 'CN' AND 'SN' (CANCEL / STOP
*          :  NO REDRAW) AS A NEW VALUE IN CANCEL-RDRW-ACTIVITY-CDE
*
* 02-16-00 : ALTHEA A. YOUNG
*          : REVISED TO PREVENT A DUPLICATE ANNOTATION RECORD FROM BEING
*          : CREATED FOR CANCELLED / STOPPED RECORDS WITH 'OV00' OR
*          : 'USPS' HOLD CODES.
*
* 01-11-01 : ROXAN CARREON
*          : PRIOR CHANGES FAILED IT's purpose of preventing duplicates.
*          : THIS REVISION WILL ALLOW UPDATES FOR EXISTING ANNOTATION
*          : RECORDS FOR PAYMENTS WITH OV00 AND USPS THUS PREVENTING
*          : DUPLICATES.
*          : AN ANNOTATION KEY WAS DEFINED FOR RETRIEVAL AND UPDATE WHEN
*          : EXISTING.
*  T MCGEE           /* TMM 02/2001 - GLOBAL PAY
*    11-02 : R CARREON   STOW -EGTRRA CHANGES IN LDA
* R. LANDRUM    02/22/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
*
* 5/2/08 - NEEDS RESTOW TO PICK UP NAT 3 VERSION OF FCPCUSPS - AER
* 04/20/10 -  CM - RESTOW FOR CHANGE IN FCPLPMNU
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp379 extends BLNatBase
{
    // Data Areas
    private LdaCdbatxa ldaCdbatxa;
    private LdaFcplpmnu ldaFcplpmnu;
    private LdaFcplannu ldaFcplannu;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_1;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Cur_Lang;
    private DbsField pnd_Program;
    private DbsField pnd_Header0_1;
    private DbsField pnd_Header0_2;
    private DbsField pnd_Ws_Header_Record;

    private DbsGroup pnd_Ws_Header_Record__R_Field_2;

    private DbsGroup pnd_Ws_Header_Record_Pnd_Ws_Header_Level2;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Crrncy_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Qlfied_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sps_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Stats_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Annot_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cmbne_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Ftre_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Type_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cref_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Cde;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Grp;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr;
    private DbsField pnd_Ws_Header_Record_Annt_Ctznshp_Cde;
    private DbsField pnd_Ws_Header_Record_Annt_Rsdncy_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Cycle_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Eft_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Pct;
    private DbsField pnd_Ws_Header_Record_Pymnt_Rqst_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Header_Record__R_Field_3;
    private DbsField pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Header_Record_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Amt;
    private DbsField pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Header_Record_Cntrct_Hold_Tme;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind;
    private DbsField pnd_Ws_Header_Record_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time;
    private DbsField pnd_Ws_Header_Record_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde;
    private DbsField pnd_Ws_Header_Record_Pnd_Ws_Header_Filler;
    private DbsField pnd_Ws_Occurs;

    private DbsGroup pnd_Ws_Occurs__R_Field_4;

    private DbsGroup pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Side;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Qty;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Unit_Value;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Settl_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fed_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Cde;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dci_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dpi_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Ivc_Ind;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Valuat_Period;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt;
    private DbsField pnd_Ws_Occurs_Inv_Acct_Exp_Amt;
    private DbsField pnd_Ws_Occurs_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Occurs_Cntrct_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind;
    private DbsField pnd_Ws_Occurs_Cntrct_Option_Cde;
    private DbsField pnd_Ws_Occurs_Cntrct_Mode_Cde;
    private DbsField pnd_Ws_Occurs_Pnd_Cntr_Deductions;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde;
    private DbsField pnd_Ws_Occurs_Pymnt_Ded_Amt;
    private DbsField pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler;

    private DbsGroup pnd_Input_Parm;
    private DbsField pnd_Input_Parm_Pnd_Parm_Origin;
    private DbsField pnd_Input_Parm_Pnd_Parm_Date_Yyyymmdd;
    private DbsField pnd_Ws_Work_Rec;

    private DbsGroup pnd_Ws_Work_Rec__R_Field_5;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Filler_A;
    private DbsField pnd_Ws_Work_Rec_Pnd_Ws_Filler_B;
    private DbsField pnd_I;
    private DbsField pnd_In_Dex;
    private DbsField pnd_Ci2;

    private DbsGroup pnd_Control_Totals;
    private DbsField pnd_Control_Totals_Pnd_Type_Code;
    private DbsField pnd_Control_Totals_Pnd_Cnt;
    private DbsField pnd_Control_Totals_Pnd_Gross_Amt;
    private DbsField pnd_Control_Totals_Pnd_Net_Amt;
    private DbsField pnd_Prev_Hold_Cde;
    private DbsField pnd_Ws_Recs_Read;
    private DbsField pnd_Ws_Update;
    private DbsField pnd_Ws_Not_Update;
    private DbsField pnd_Ws_Check_Nbr;
    private DbsField pnd_Old_Chk_Nbr;
    private DbsField pnd_Missing_Checks;
    private DbsField pnd_First;
    private DbsField pnd_Ws_Recs_Found;
    private DbsField pnd_Ws_Key;

    private DbsGroup pnd_Ws_Key__R_Field_6;

    private DbsGroup pnd_Ws_Key_Pnd_Ws_Key_Level2;
    private DbsField pnd_Ws_Key_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Key_Cntrct_Check_Crrncy_Cde;
    private DbsField pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr;

    private DbsGroup pnd_Ws_Key__R_Field_7;
    private DbsField pnd_Ws_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Ws_Key_Pymnt_Instmt_Nbr;
    private DbsField pnd_Ws_Annot_Key;

    private DbsGroup pnd_Ws_Annot_Key__R_Field_8;

    private DbsGroup pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Invrse_Dte;
    private DbsField pnd_Ws_Annot_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaCdbatxa = new LdaCdbatxa();
        registerRecord(ldaCdbatxa);
        ldaFcplpmnu = new LdaFcplpmnu();
        registerRecord(ldaFcplpmnu);
        registerRecord(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());
        ldaFcplannu = new LdaFcplannu();
        registerRecord(ldaFcplannu);
        registerRecord(ldaFcplannu.getVw_fcp_Cons_Annu());
        localVariables = new DbsRecord();
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_1 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_1", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_1.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Cur_Lang = localVariables.newFieldInRecord("pnd_Cur_Lang", "#CUR-LANG", FieldType.PACKED_DECIMAL, 1);
        pnd_Program = localVariables.newFieldInRecord("pnd_Program", "#PROGRAM", FieldType.STRING, 8);
        pnd_Header0_1 = localVariables.newFieldInRecord("pnd_Header0_1", "#HEADER0-1", FieldType.STRING, 50);
        pnd_Header0_2 = localVariables.newFieldInRecord("pnd_Header0_2", "#HEADER0-2", FieldType.STRING, 50);
        pnd_Ws_Header_Record = localVariables.newFieldArrayInRecord("pnd_Ws_Header_Record", "#WS-HEADER-RECORD", FieldType.STRING, 250, new DbsArrayController(1, 
            2));

        pnd_Ws_Header_Record__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Header_Record__R_Field_2", "REDEFINE", pnd_Ws_Header_Record);

        pnd_Ws_Header_Record_Pnd_Ws_Header_Level2 = pnd_Ws_Header_Record__R_Field_2.newGroupInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Level2", "#WS-HEADER-LEVEL2");
        pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Level_Nmbr", 
            "#WS-RECORD-LEVEL-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Record_Occur_Nmbr", 
            "#WS-RECORD-OCCUR-NMBR", FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr", 
            "#WS-PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr", 
            "#WS-PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Cntrct_Cmbn_Nbr", 
            "#WS-CNTRCT-CMBN-NBR", FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Invrse_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Check_Crrncy_Cde", 
            "CNTRCT-CHECK-CRRNCY-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Crrncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Crrncy_Cde", "CNTRCT-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Orgn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Qlfied_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Qlfied_Cde", "CNTRCT-QLFIED-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Type_Ind", 
            "CNTRCT-PYMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sttlmnt_Type_Ind", 
            "CNTRCT-STTLMNT-TYPE-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Sps_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sps_Cde", "CNTRCT-SPS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Rmndr_Pct", 
            "PYMNT-RQST-RMNDR-PCT", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Stats_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Stats_Cde", "PYMNT-STATS-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Annot_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Annot_Ind", "PYMNT-ANNOT-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Cmbne_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cmbne_Ind", "PYMNT-CMBNE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Ftre_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Ftre_Ind", "PYMNT-FTRE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Na_Addr_Trggr", 
            "PYMNT-PAYEE-NA-ADDR-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Payee_Tx_Elct_Trggr", 
            "PYMNT-PAYEE-TX-ELCT-TRGGR", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Inst_Rep_Cde", 
            "PYMNT-INST-REP-CDE", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Ind", 
            "CNTRCT-DVDND-PAYEE-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Ind", "ANNT-SOC-SEC-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind", 
            "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 1);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Dte", 
            "CNTRCT-RQST-SETTL-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Dte", "CNTRCT-RQST-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Rqst_Settl_Tme", 
            "CNTRCT-RQST-SETTL-TME", FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12);
        pnd_Ws_Header_Record_Cntrct_Type_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Type_Cde", "CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Lob_Cde", "CNTRCT-LOB-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Sub_Lob_Cde", 
            "CNTRCT-SUB-LOB-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ia_Lob_Cde", "CNTRCT-IA-LOB-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Cntrct_Cref_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cref_Nbr", "CNTRCT-CREF-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Header_Record_Cntrct_Option_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Cntrct_Mode_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Pymnt_Dest_Cde", 
            "CNTRCT-PYMNT-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Roll_Dest_Cde", 
            "CNTRCT-ROLL-DEST-CDE", FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Dvdnd_Payee_Cde", 
            "CNTRCT-DVDND-PAYEE-CDE", FieldType.STRING, 5);
        pnd_Ws_Header_Record_Cntrct_Hold_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", 
            FieldType.STRING, 4);
        pnd_Ws_Header_Record_Cntrct_Hold_Grp = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Grp", "CNTRCT-HOLD-GRP", 
            FieldType.STRING, 3);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_1_Nbr", 
            "CNTRCT-DA-TIAA-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_2_Nbr", 
            "CNTRCT-DA-TIAA-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_3_Nbr", 
            "CNTRCT-DA-TIAA-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_4_Nbr", 
            "CNTRCT-DA-TIAA-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Tiaa_5_Nbr", 
            "CNTRCT-DA-TIAA-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_1_Nbr", 
            "CNTRCT-DA-CREF-1-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_2_Nbr", 
            "CNTRCT-DA-CREF-2-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_3_Nbr", 
            "CNTRCT-DA-CREF-3-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_4_Nbr", 
            "CNTRCT-DA-CREF-4-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Da_Cref_5_Nbr", 
            "CNTRCT-DA-CREF-5-NBR", FieldType.STRING, 8);
        pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Soc_Sec_Nbr", "ANNT-SOC-SEC-NBR", 
            FieldType.NUMERIC, 9);
        pnd_Ws_Header_Record_Annt_Ctznshp_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Ctznshp_Cde", "ANNT-CTZNSHP-CDE", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Annt_Rsdncy_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Annt_Rsdncy_Cde", "ANNT-RSDNCY-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Cde", "PYMNT-SPLIT-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Split_Reasn_Cde", 
            "PYMNT-SPLIT-REASN-CDE", FieldType.STRING, 6);
        pnd_Ws_Header_Record_Pymnt_Check_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Cycle_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Cycle_Dte", "PYMNT-CYCLE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Eft_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Rqst_Pct = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Pct", "PYMNT-RQST-PCT", 
            FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Header_Record_Pymnt_Rqst_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Rqst_Amt", "PYMNT-RQST-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr", 
            "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);

        pnd_Ws_Header_Record__R_Field_3 = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newGroupInGroup("pnd_Ws_Header_Record__R_Field_3", "REDEFINE", pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num = pnd_Ws_Header_Record__R_Field_3.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Instmt_Nbr = pnd_Ws_Header_Record__R_Field_3.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Scrty_Nbr", 
            "PYMNT-CHECK-SCRTY-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Header_Record_Pymnt_Check_Amt = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Settlmnt_Dte", 
            "PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Check_Seq_Nbr", 
            "PYMNT-CHECK-SEQ-NBR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Ec_Oprtr_Id_Nbr", 
            "CNTRCT-EC-OPRTR-ID-NBR", FieldType.STRING, 7);
        pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cmbn_Nbr", "CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Header_Record_Cntrct_Hold_Tme = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Hold_Tme", "CNTRCT-HOLD-TME", 
            FieldType.TIME);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Ind", 
            "CNTRCT-CANCEL-RDRW-IND", FieldType.STRING, 1);
        pnd_Ws_Header_Record_Pymnt_Acctg_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Reqst_Log_Dte_Time", 
            "PYMNT-REQST-LOG-DTE-TIME", FieldType.STRING, 15);
        pnd_Ws_Header_Record_Pymnt_Intrfce_Dte = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", 
            FieldType.DATE);
        pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde", 
            "CNTRCT-CANCEL-RDRW-ACTVTY-CDE", FieldType.STRING, 2);
        pnd_Ws_Header_Record_Pnd_Ws_Header_Filler = pnd_Ws_Header_Record_Pnd_Ws_Header_Level2.newFieldInGroup("pnd_Ws_Header_Record_Pnd_Ws_Header_Filler", 
            "#WS-HEADER-FILLER", FieldType.STRING, 133);
        pnd_Ws_Occurs = localVariables.newFieldArrayInRecord("pnd_Ws_Occurs", "#WS-OCCURS", FieldType.STRING, 250, new DbsArrayController(1, 2));

        pnd_Ws_Occurs__R_Field_4 = localVariables.newGroupInRecord("pnd_Ws_Occurs__R_Field_4", "REDEFINE", pnd_Ws_Occurs);

        pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2 = pnd_Ws_Occurs__R_Field_4.newGroupInGroup("pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2", "#WS-OCCURS-LEVEL2");
        pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Pymnt_Prcss_Seq_Num", 
            "#WS-SAVE-PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Save_Contract_Hold_Code", 
            "#WS-SAVE-CONTRACT-HOLD-CODE", FieldType.STRING, 4);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Inv_Acct", "#CNTR-INV-ACCT", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Ws_Side = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Side", "#WS-SIDE", FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Cde", "INV-ACCT-CDE", FieldType.STRING, 
            2);
        pnd_Ws_Occurs_Inv_Acct_Unit_Qty = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Qty", "INV-ACCT-UNIT-QTY", FieldType.PACKED_DECIMAL, 
            9, 3);
        pnd_Ws_Occurs_Inv_Acct_Unit_Value = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Unit_Value", "INV-ACCT-UNIT-VALUE", 
            FieldType.PACKED_DECIMAL, 9, 3);
        pnd_Ws_Occurs_Inv_Acct_Settl_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Settl_Amt", "INV-ACCT-SETTL-AMT", 
            FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Occurs_Inv_Acct_Fed_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fed_Cde", "INV-ACCT-FED-CDE", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_State_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Cde", "INV-ACCT-STATE-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Local_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Cde", "INV-ACCT-LOCAL-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Amt", "INV-ACCT-IVC-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dci_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dci_Amt", "INV-ACCT-DCI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dpi_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dpi_Amt", "INV-ACCT-DPI-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Start_Accum_Amt", "INV-ACCT-START-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_End_Accum_Amt", "INV-ACCT-END-ACCUM-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Dvdnd_Amt", "INV-ACCT-DVDND-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Net_Pymnt_Amt", "INV-ACCT-NET-PYMNT-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Ivc_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Ivc_Ind", "INV-ACCT-IVC-IND", FieldType.STRING, 
            1);
        pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Adj_Ivc_Amt", "INV-ACCT-ADJ-IVC-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Valuat_Period = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Valuat_Period", "INV-ACCT-VALUAT-PERIOD", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Fdrl_Tax_Amt", "INV-ACCT-FDRL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_State_Tax_Amt", "INV-ACCT-STATE-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Local_Tax_Amt", "INV-ACCT-LOCAL-TAX-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Ws_Occurs_Inv_Acct_Exp_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Inv_Acct_Exp_Amt", "INV-ACCT-EXP-AMT", FieldType.PACKED_DECIMAL, 
            9, 2);
        pnd_Ws_Occurs_Cntrct_Ppcn_Nbr = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10);
        pnd_Ws_Occurs_Cntrct_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4);
        pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Pymnt_Type_Ind", "CNTRCT-PYMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Sttlmnt_Type_Ind", "CNTRCT-STTLMNT-TYPE-IND", 
            FieldType.STRING, 1);
        pnd_Ws_Occurs_Cntrct_Option_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Option_Cde", "CNTRCT-OPTION-CDE", FieldType.NUMERIC, 
            2);
        pnd_Ws_Occurs_Cntrct_Mode_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Cntrct_Mode_Cde", "CNTRCT-MODE-CDE", FieldType.NUMERIC, 
            3);
        pnd_Ws_Occurs_Pnd_Cntr_Deductions = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Cntr_Deductions", "#CNTR-DEDUCTIONS", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Occurs_Pymnt_Ded_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Cde", "PYMNT-DED-CDE", FieldType.NUMERIC, 
            3, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Payee_Cde", "PYMNT-DED-PAYEE-CDE", 
            FieldType.STRING, 8, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pymnt_Ded_Amt = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldArrayInGroup("pnd_Ws_Occurs_Pymnt_Ded_Amt", "PYMNT-DED-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 10));
        pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler = pnd_Ws_Occurs_Pnd_Ws_Occurs_Level2.newFieldInGroup("pnd_Ws_Occurs_Pnd_Ws_Inv_Acct_Filler", "#WS-INV-ACCT-FILLER", 
            FieldType.STRING, 188);

        pnd_Input_Parm = localVariables.newGroupInRecord("pnd_Input_Parm", "#INPUT-PARM");
        pnd_Input_Parm_Pnd_Parm_Origin = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Parm_Origin", "#PARM-ORIGIN", FieldType.STRING, 2);
        pnd_Input_Parm_Pnd_Parm_Date_Yyyymmdd = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Parm_Date_Yyyymmdd", "#PARM-DATE-YYYYMMDD", FieldType.STRING, 
            8);
        pnd_Ws_Work_Rec = localVariables.newFieldArrayInRecord("pnd_Ws_Work_Rec", "#WS-WORK-REC", FieldType.STRING, 250, new DbsArrayController(1, 2));

        pnd_Ws_Work_Rec__R_Field_5 = localVariables.newGroupInRecord("pnd_Ws_Work_Rec__R_Field_5", "REDEFINE", pnd_Ws_Work_Rec);
        pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr", "#WS-RECORD-LEVEL-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Record_Occur_Nmbr", "#WS-RECORD-OCCUR-NMBR", 
            FieldType.NUMERIC, 2);
        pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Simplex_Duplex_Multiplex", 
            "#WS-SIMPLEX-DUPLEX-MULTIPLEX", FieldType.STRING, 1);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Prcss_Seq_Num", "#WS-PYMNT-PRCSS-SEQ-NUM", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Contract_Hold_Code", "#WS-CONTRACT-HOLD-CODE", 
            FieldType.STRING, 4);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Nbr", "#WS-PYMNT-CHECK-NBR", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Pymnt_Check_Seq_Nbr", "#WS-PYMNT-CHECK-SEQ-NBR", 
            FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Cntrct_Cmbn_Nbr", "#WS-CNTRCT-CMBN-NBR", 
            FieldType.STRING, 14);
        pnd_Ws_Work_Rec_Pnd_Ws_Filler_A = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Filler_A", "#WS-FILLER-A", FieldType.STRING, 
            217);
        pnd_Ws_Work_Rec_Pnd_Ws_Filler_B = pnd_Ws_Work_Rec__R_Field_5.newFieldInGroup("pnd_Ws_Work_Rec_Pnd_Ws_Filler_B", "#WS-FILLER-B", FieldType.STRING, 
            242);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.INTEGER, 2);
        pnd_In_Dex = localVariables.newFieldInRecord("pnd_In_Dex", "#IN-DEX", FieldType.INTEGER, 2);
        pnd_Ci2 = localVariables.newFieldInRecord("pnd_Ci2", "#CI2", FieldType.INTEGER, 2);

        pnd_Control_Totals = localVariables.newGroupArrayInRecord("pnd_Control_Totals", "#CONTROL-TOTALS", new DbsArrayController(1, 50));
        pnd_Control_Totals_Pnd_Type_Code = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Type_Code", "#TYPE-CODE", FieldType.STRING, 2);
        pnd_Control_Totals_Pnd_Cnt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Cnt", "#CNT", FieldType.PACKED_DECIMAL, 7);
        pnd_Control_Totals_Pnd_Gross_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Gross_Amt", "#GROSS-AMT", FieldType.PACKED_DECIMAL, 
            11, 2);
        pnd_Control_Totals_Pnd_Net_Amt = pnd_Control_Totals.newFieldInGroup("pnd_Control_Totals_Pnd_Net_Amt", "#NET-AMT", FieldType.PACKED_DECIMAL, 11, 
            2);
        pnd_Prev_Hold_Cde = localVariables.newFieldInRecord("pnd_Prev_Hold_Cde", "#PREV-HOLD-CDE", FieldType.STRING, 4);
        pnd_Ws_Recs_Read = localVariables.newFieldInRecord("pnd_Ws_Recs_Read", "#WS-RECS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Update = localVariables.newFieldInRecord("pnd_Ws_Update", "#WS-UPDATE", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Not_Update = localVariables.newFieldInRecord("pnd_Ws_Not_Update", "#WS-NOT-UPDATE", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Check_Nbr = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr", "#WS-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Old_Chk_Nbr = localVariables.newFieldInRecord("pnd_Old_Chk_Nbr", "#OLD-CHK-NBR", FieldType.NUMERIC, 7);
        pnd_Missing_Checks = localVariables.newFieldInRecord("pnd_Missing_Checks", "#MISSING-CHECKS", FieldType.PACKED_DECIMAL, 7);
        pnd_First = localVariables.newFieldInRecord("pnd_First", "#FIRST", FieldType.BOOLEAN, 1);
        pnd_Ws_Recs_Found = localVariables.newFieldInRecord("pnd_Ws_Recs_Found", "#WS-RECS-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ws_Key = localVariables.newFieldInRecord("pnd_Ws_Key", "#WS-KEY", FieldType.STRING, 17);

        pnd_Ws_Key__R_Field_6 = localVariables.newGroupInRecord("pnd_Ws_Key__R_Field_6", "REDEFINE", pnd_Ws_Key);

        pnd_Ws_Key_Pnd_Ws_Key_Level2 = pnd_Ws_Key__R_Field_6.newGroupInGroup("pnd_Ws_Key_Pnd_Ws_Key_Level2", "#WS-KEY-LEVEL2");
        pnd_Ws_Key_Pymnt_Check_Dte = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ws_Key_Cntrct_Orgn_Cde = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Key_Cntrct_Check_Crrncy_Cde = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Cntrct_Check_Crrncy_Cde", "CNTRCT-CHECK-CRRNCY-CDE", 
            FieldType.STRING, 1);
        pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", 
            FieldType.NUMERIC, 1);
        pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Key_Pnd_Ws_Key_Level2.newFieldInGroup("pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 
            9);

        pnd_Ws_Key__R_Field_7 = pnd_Ws_Key_Pnd_Ws_Key_Level2.newGroupInGroup("pnd_Ws_Key__R_Field_7", "REDEFINE", pnd_Ws_Key_Pymnt_Prcss_Seq_Nbr);
        pnd_Ws_Key_Pymnt_Prcss_Seq_Num = pnd_Ws_Key__R_Field_7.newFieldInGroup("pnd_Ws_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Ws_Key_Pymnt_Instmt_Nbr = pnd_Ws_Key__R_Field_7.newFieldInGroup("pnd_Ws_Key_Pymnt_Instmt_Nbr", "PYMNT-INSTMT-NBR", FieldType.NUMERIC, 2);
        pnd_Ws_Annot_Key = localVariables.newFieldInRecord("pnd_Ws_Annot_Key", "#WS-ANNOT-KEY", FieldType.STRING, 29);

        pnd_Ws_Annot_Key__R_Field_8 = localVariables.newGroupInRecord("pnd_Ws_Annot_Key__R_Field_8", "REDEFINE", pnd_Ws_Annot_Key);

        pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields = pnd_Ws_Annot_Key__R_Field_8.newGroupInGroup("pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields", "#WS-ANNOT-KEY-FIELDS");
        pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10);
        pnd_Ws_Annot_Key_Cntrct_Invrse_Dte = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", 
            FieldType.NUMERIC, 8);
        pnd_Ws_Annot_Key_Cntrct_Orgn_Cde = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", 
            FieldType.STRING, 2);
        pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr = pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.newFieldInGroup("pnd_Ws_Annot_Key_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", 
            FieldType.NUMERIC, 9);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaCdbatxa.initializeValues();
        ldaFcplpmnu.initializeValues();
        ldaFcplannu.initializeValues();

        localVariables.reset();
        pnd_Header0_1.setInitialValue("           CONSOLIDATED PAYMENT SYSTEM");
        pnd_Header0_2.setInitialValue("  CANCEL/REDRAW IA  BATCH UPDATE CONTROL REPORT");
        pnd_Input_Parm_Pnd_Parm_Date_Yyyymmdd.setInitialValue(" ");
        pnd_I.setInitialValue(0);
        pnd_In_Dex.setInitialValue(0);
        pnd_Ci2.setInitialValue(0);
        pnd_Control_Totals_Pnd_Type_Code.getValue(1).setInitialValue(" ");
        pnd_Control_Totals_Pnd_Cnt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Gross_Amt.getValue(1).setInitialValue(0);
        pnd_Control_Totals_Pnd_Net_Amt.getValue(1).setInitialValue(0);
        pnd_Ws_Recs_Read.setInitialValue(0);
        pnd_Ws_Update.setInitialValue(0);
        pnd_Ws_Not_Update.setInitialValue(0);
        pnd_First.setInitialValue(true);
        pnd_Ws_Recs_Found.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp379() throws Exception
    {
        super("Fcpp379");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt2, 2);
        setupReports();
        //*  DEFINE PRINTERS AND FORMATS
        //*                                                                                                                                                               //Natural: FORMAT LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 1 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF;//Natural: FORMAT ( 2 ) LS = 133 PS = 60 ZP = ON IS = OFF ES = OFF SG = OFF
        pnd_Program.setValue(Global.getPROGRAM());                                                                                                                        //Natural: ASSIGN #PROGRAM = *PROGRAM
        //*  MAP THE CURRENT LANGUAGE CODE TO THE CODE IN THE BATCH MODEL TEXT LDA.
        pnd_Cur_Lang.setValue(Global.getLANGUAGE());                                                                                                                      //Natural: ASSIGN #CUR-LANG = *LANGUAGE
        pnd_Cur_Lang.setValue(ldaCdbatxa.getCdbatxa_Pnd_Lang_Map().getValue(pnd_Cur_Lang));                                                                               //Natural: ASSIGN #CUR-LANG = CDBATXA.#LANG-MAP ( #CUR-LANG )
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #INPUT-PARM
        while (condition(getWorkFiles().read(1, pnd_Input_Parm)))
        {
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        //* *SAG END-EXIT
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 2 )
        if (condition(Global.getDEVICE().equals("BATCH")))                                                                                                                //Natural: IF *DEVICE = 'BATCH' THEN
        {
            //*  SET DELIMITER MODE FOR BATCH INPUT
            setControl("D");                                                                                                                                              //Natural: SET CONTROL 'D'
        }                                                                                                                                                                 //Natural: END-IF
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA = 'INFP9000'
        //* ***********************
        //*   MAIN PROGRAM LOGIC  *
        //* ***********************
        //* **************** RL PAYEE MATCH FEB22, 2006 BEGIN *******************
        //*  RL PAYEE MATCH
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMATTING-DATA
        sub_Get_Check_Formatting_Data();
        if (condition(Global.isEscape())) {return;}
        //* ***************** RL PAYEE MATCH FEB22, 2006 END ********************
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 8 #WS-WORK-REC ( * )
        while (condition(getWorkFiles().read(8, pnd_Ws_Work_Rec.getValue("*"))))
        {
            if (condition(pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(10) || pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(20)))                                    //Natural: IF #WS-WORK-REC.#WS-RECORD-LEVEL-NMBR = 10 OR = 20
            {
                short decideConditionsMet751 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE #PARM-ORIGIN;//Natural: VALUE 'CI'
                if (condition((pnd_Input_Parm_Pnd_Parm_Origin.equals("CI"))))
                {
                    decideConditionsMet751++;
                                                                                                                                                                          //Natural: PERFORM IA-CONTROL-TOTALS
                    sub_Ia_Control_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(10)))                                                                                           //Natural: IF #WS-WORK-REC.#WS-RECORD-LEVEL-NMBR = 10
            {
                pnd_Ws_Header_Record.getValue("*").setValue(pnd_Ws_Work_Rec.getValue("*"));                                                                               //Natural: MOVE #WS-WORK-REC ( * ) TO #WS-HEADER-RECORD ( * )
                pnd_Ws_Key_Pnd_Ws_Key_Level2.setValuesByName(pnd_Ws_Header_Record_Pnd_Ws_Header_Level2);                                                                  //Natural: MOVE BY NAME #WS-HEADER-LEVEL2 TO #WS-KEY-LEVEL2
                //* *--> INSERTED 03-16-95 BY FRANK
                if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))          //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
                {
                    pnd_Ws_Recs_Read.nadd(1);                                                                                                                             //Natural: ADD 1 TO #WS-RECS-READ
                    if (condition(pnd_First.getBoolean()))                                                                                                                //Natural: IF #FIRST
                    {
                        pnd_First.reset();                                                                                                                                //Natural: RESET #FIRST
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        if (condition(pnd_Old_Chk_Nbr.add(1).notEquals(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr)))                                                     //Natural: IF #OLD-CHK-NBR + 1 NE #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
                        {
                                                                                                                                                                          //Natural: PERFORM MISSING-CHECKS
                            sub_Missing_Checks();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Old_Chk_Nbr.setValue(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr);                                                                                //Natural: ASSIGN #OLD-CHK-NBR := #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Key_Pymnt_Instmt_Nbr.setValue(1);                                                                                                                  //Natural: ASSIGN #WS-KEY.PYMNT-INSTMT-NBR := 01
                                                                                                                                                                          //Natural: PERFORM READ-ADABAS
                sub_Read_Adabas();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
        //*  E.T. ALL REMAINING TRANSACTIONS
        getCurrentProcessState().getDbConv().dbCommit();                                                                                                                  //Natural: END TRANSACTION
        getReports().write(2, ReportOption.NOTITLE,"TOTAL CHECKS................ ",pnd_Ws_Recs_Read);                                                                     //Natural: WRITE ( 2 ) 'TOTAL CHECKS................ ' #WS-RECS-READ
        if (Global.isEscape()) return;
        getReports().write(2, ReportOption.NOTITLE,"TOTAL MISSING CHECKS........ ",pnd_Missing_Checks);                                                                   //Natural: WRITE ( 2 ) 'TOTAL MISSING CHECKS........ ' #MISSING-CHECKS
        if (Global.isEscape()) return;
        //*  MATCH MY COUNTS TO CONTROL RECORDS COUNT
        DbsUtil.callnat(Fcpn379.class , getCurrentProcessState(), pnd_Input_Parm_Pnd_Parm_Origin, pnd_Control_Totals_Pnd_Type_Code.getValue("*"), pnd_Control_Totals_Pnd_Cnt.getValue("*"),  //Natural: CALLNAT 'FCPN379' #PARM-ORIGIN #TYPE-CODE ( * ) #CNT ( * ) #GROSS-AMT ( * ) #NET-AMT ( * )
            pnd_Control_Totals_Pnd_Gross_Amt.getValue("*"), pnd_Control_Totals_Pnd_Net_Amt.getValue("*"));
        if (condition(Global.isEscape())) return;
        //* ****************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-ADABAS
        //* ************************** RL END-PAYEE MATCH *************************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: IA-CONTROL-TOTALS
        //*    VALUE 'C', 'CN'
        //*    VALUE 'S', 'SN'
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MISSING-CHECKS
        //* *********************** RL BEGIN - PAYEE MATCH ************FEB 22,2006
        //* *******************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMATTING-DATA
        //*  RL PAYEE MATCH
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //*  RL PAYEE MATCH
        //* ************************** RL END-PAYEE MATCH *************************
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
        //*   CREATE-ANNOT-FOR-HOLD
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CREATE-ANNOT-FOR-HOLD
    }
    private void sub_Read_Adabas() throws Exception                                                                                                                       //Natural: READ-ADABAS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        ldaFcplpmnu.getVw_fcp_Cons_Pymnu().startDatabaseRead                                                                                                              //Natural: READ FCP-CONS-PYMNU BY CHK-DTE-ORG-CURR-TYP-SEQ STARTING FROM #WS-KEY
        (
        "READ_PYMNTS",
        new Wc[] { new Wc("CHK_DTE_ORG_CURR_TYP_SEQ", ">=", pnd_Ws_Key.getBinary(), WcType.BY) },
        new Oc[] { new Oc("CHK_DTE_ORG_CURR_TYP_SEQ", "ASC") }
        );
        READ_PYMNTS:
        while (condition(ldaFcplpmnu.getVw_fcp_Cons_Pymnu().readNextRow("READ_PYMNTS")))
        {
            if (condition(pnd_Ws_Key_Pymnt_Check_Dte.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Dte()) || pnd_Ws_Key_Cntrct_Orgn_Cde.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Orgn_Cde())  //Natural: IF #WS-KEY.PYMNT-CHECK-DTE NE FCP-CONS-PYMNU.PYMNT-CHECK-DTE OR #WS-KEY.CNTRCT-ORGN-CDE NE FCP-CONS-PYMNU.CNTRCT-ORGN-CDE OR #WS-KEY.CNTRCT-CHECK-CRRNCY-CDE NE FCP-CONS-PYMNU.CNTRCT-CHECK-CRRNCY-CDE OR #WS-KEY.PYMNT-PAY-TYPE-REQ-IND NE FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND OR #WS-KEY.PYMNT-PRCSS-SEQ-NUM NE FCP-CONS-PYMNU.PYMNT-PRCSS-SEQ-NUM
                || pnd_Ws_Key_Cntrct_Check_Crrncy_Cde.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Check_Crrncy_Cde()) || pnd_Ws_Key_Pymnt_Pay_Type_Req_Ind.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind()) 
                || pnd_Ws_Key_Pymnt_Prcss_Seq_Num.notEquals(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Prcss_Seq_Num())))
            {
                if (true) break READ_PYMNTS;                                                                                                                              //Natural: ESCAPE BOTTOM ( READ-PYMNTS. )
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                //* *********************** RL BEGIN - PAYEE MATCH ************ MAY 10,2006
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind().equals(1)))                                                                          //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1
                {
                    pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr);                                                       //Natural: MOVE #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
                    pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                     //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Nbr().setValue(pnd_Ws_Check_Nbr_N10);                                                                             //Natural: MOVE #WS-CHECK-NBR-N10 TO FCP-CONS-PYMNU.PYMNT-NBR
                    pnd_Ws_Check_Nbr_N10.reset();                                                                                                                         //Natural: RESET #WS-CHECK-NBR-N10
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr().compute(new ComputeParameters(false, ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr()),                //Natural: COMPUTE FCP-CONS-PYMNU.PYMNT-CHECK-NBR = #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR * -1
                        pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr.multiply(-1));
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Nbr().setValue(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr);                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-NBR := #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
                }                                                                                                                                                         //Natural: END-IF
                ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Check_Seq_Nbr().setValue(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Seq_Nbr);                                            //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-CHECK-SEQ-NBR := #WS-HEADER-RECORD.#WS-PYMNT-CHECK-SEQ-NBR
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals(" ") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))              //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = ' ' OR = 'R'
            {
                if (condition((ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Pay_Type_Req_Ind().equals(1) && (ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")        //Natural: IF FCP-CONS-PYMNU.PYMNT-PAY-TYPE-REQ-IND = 1 AND FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00' OR = 'USPS'
                    || ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))))
                {
                                                                                                                                                                          //Natural: PERFORM CREATE-ANNOT-FOR-HOLD
                    sub_Create_Annot_For_Hold();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom("READ_PYMNTS"))) break;
                        else if (condition(Global.isEscapeBottomImmediate("READ_PYMNTS"))) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Stats_Cde().setValue("P");                                                                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-STATS-CDE := 'P'
            pnd_Ws_Update.nadd(1);                                                                                                                                        //Natural: ADD 1 TO #WS-UPDATE
            ldaFcplpmnu.getVw_fcp_Cons_Pymnu().updateDBRow("READ_PYMNTS");                                                                                                //Natural: UPDATE
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
        if (condition(pnd_Ws_Update.equals(150)))                                                                                                                         //Natural: IF #WS-UPDATE = 150
        {
            pnd_Ws_Update.reset();                                                                                                                                        //Natural: RESET #WS-UPDATE
            getCurrentProcessState().getDbConv().dbCommit();                                                                                                              //Natural: END TRANSACTION
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Ia_Control_Totals() throws Exception                                                                                                                 //Natural: IA-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(10)))                                                                                               //Natural: IF #WS-WORK-REC.#WS-RECORD-LEVEL-NMBR = 10
        {
            pnd_Ws_Header_Record.getValue("*").setValue(pnd_Ws_Work_Rec.getValue("*"));                                                                                   //Natural: MOVE #WS-WORK-REC ( * ) TO #WS-HEADER-RECORD ( * )
            //*  JWO 2010-11
            //*  JWO 2010-11
            short decideConditionsMet863 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'CN', 'CP', 'RP', 'PR'
            if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN") 
                || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP") || 
                pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR"))))
            {
                decideConditionsMet863++;
                pnd_In_Dex.setValue(1);                                                                                                                                   //Natural: ASSIGN #IN-DEX := 1
            }                                                                                                                                                             //Natural: VALUE 'S', 'SN', 'SP'
            else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN") 
                || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP"))))
            {
                decideConditionsMet863++;
                pnd_In_Dex.setValue(2);                                                                                                                                   //Natural: ASSIGN #IN-DEX := 2
            }                                                                                                                                                             //Natural: VALUE 'R'
            else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R"))))
            {
                decideConditionsMet863++;
                pnd_In_Dex.setValue(3);                                                                                                                                   //Natural: ASSIGN #IN-DEX := 3
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet863 > 0))
            {
                pnd_Control_Totals_Pnd_Type_Code.getValue(pnd_In_Dex).setValue(pnd_Ws_Header_Record_Cntrct_Type_Cde);                                                     //Natural: ASSIGN #TYPE-CODE ( #IN-DEX ) = #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
                if (condition(pnd_Ws_Header_Record_Pymnt_Instmt_Nbr.equals(1)))                                                                                           //Natural: IF #WS-HEADER-RECORD.PYMNT-INSTMT-NBR = 01
                {
                    pnd_Control_Totals_Pnd_Cnt.getValue(pnd_In_Dex).nadd(1);                                                                                              //Natural: ADD 1 TO #CNT ( #IN-DEX )
                    pnd_Control_Totals_Pnd_Net_Amt.getValue(pnd_In_Dex).nadd(pnd_Ws_Header_Record_Pymnt_Check_Amt);                                                       //Natural: ADD #WS-HEADER-RECORD.PYMNT-CHECK-AMT TO #NET-AMT ( #IN-DEX )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(10)))                                                                                               //Natural: IF #WS-WORK-REC.#WS-RECORD-LEVEL-NMBR = 10
        {
            if (condition(pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("R")))                                                                                //Natural: IF #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE = 'R'
            {
                //*  CHECKS
                short decideConditionsMet884 = 0;                                                                                                                         //Natural: DECIDE ON FIRST VALUE OF #WS-HEADER-RECORD.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
                if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(1))))
                {
                    decideConditionsMet884++;
                    pnd_Ci2.setValue(48);                                                                                                                                 //Natural: ASSIGN #CI2 := 48
                    if (condition(pnd_Prev_Hold_Cde.notEquals("0000") && pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code.equals("0000")))                             //Natural: IF #PREV-HOLD-CDE NE '0000' AND #WS-HEADER-RECORD.#WS-SAVE-CONTRACT-HOLD-CODE = '0000'
                    {
                        pnd_First.setValue(true);                                                                                                                         //Natural: ASSIGN #FIRST := TRUE
                        //*  EFT
                        //*  TMM 02/2001
                    }                                                                                                                                                     //Natural: END-IF
                    pnd_Prev_Hold_Cde.setValue(pnd_Ws_Header_Record_Pnd_Ws_Save_Contract_Hold_Code);                                                                      //Natural: ASSIGN #PREV-HOLD-CDE := #WS-HEADER-RECORD.#WS-SAVE-CONTRACT-HOLD-CODE
                }                                                                                                                                                         //Natural: VALUE 2
                else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(2))))
                {
                    decideConditionsMet884++;
                    pnd_Ci2.setValue(49);                                                                                                                                 //Natural: ASSIGN #CI2 := 49
                }                                                                                                                                                         //Natural: VALUE 3,4,9
                else if (condition((pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(3) || pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(4) || 
                    pnd_Ws_Header_Record_Pymnt_Pay_Type_Req_Ind.equals(9))))
                {
                    decideConditionsMet884++;
                    pnd_Ci2.setValue(50);                                                                                                                                 //Natural: ASSIGN #CI2 := 50
                }                                                                                                                                                         //Natural: ANY
                if (condition(decideConditionsMet884 > 0))
                {
                    pnd_Control_Totals_Pnd_Type_Code.getValue(pnd_Ci2).setValue(pnd_Ws_Header_Record_Cntrct_Type_Cde);                                                    //Natural: ASSIGN #TYPE-CODE ( #CI2 ) = #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
                    if (condition(pnd_Ws_Header_Record_Pymnt_Instmt_Nbr.equals(1)))                                                                                       //Natural: IF #WS-HEADER-RECORD.PYMNT-INSTMT-NBR = 01
                    {
                        pnd_Control_Totals_Pnd_Cnt.getValue(pnd_Ci2).nadd(1);                                                                                             //Natural: ADD 1 TO #CNT ( #CI2 )
                        pnd_Control_Totals_Pnd_Net_Amt.getValue(pnd_Ci2).nadd(pnd_Ws_Header_Record_Pymnt_Check_Amt);                                                      //Natural: ADD #WS-HEADER-RECORD.PYMNT-CHECK-AMT TO #NET-AMT ( #CI2 )
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE VALUE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(10)))                                                                                               //Natural: IF #WS-WORK-REC.#WS-RECORD-LEVEL-NMBR = 10
        {
            //*  CANCELS      /* LEON 'CN' 08-05-99
            //*  CANCELS      /* JWO 2010-11
            //*  STOPS        /* LEON 'CN' 08-05-99
            //*  STOPS /* JWO 2010-11
            short decideConditionsMet914 = 0;                                                                                                                             //Natural: DECIDE ON FIRST VALUE #WS-HEADER-RECORD.CNTRCT-CANCEL-RDRW-ACTVTY-CDE;//Natural: VALUE 'C', 'CN'
            if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("C") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CN"))))
            {
                decideConditionsMet914++;
                pnd_Ci2.setValue(46);                                                                                                                                     //Natural: ASSIGN #CI2 := 46
            }                                                                                                                                                             //Natural: VALUE 'CP', 'RP', 'PR'
            else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("CP") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("RP") 
                || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("PR"))))
            {
                decideConditionsMet914++;
                pnd_Ci2.setValue(46);                                                                                                                                     //Natural: ASSIGN #CI2 := 46
            }                                                                                                                                                             //Natural: VALUE 'S', 'SN'
            else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("S") || pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SN"))))
            {
                decideConditionsMet914++;
                pnd_Ci2.setValue(47);                                                                                                                                     //Natural: ASSIGN #CI2 := 47
            }                                                                                                                                                             //Natural: VALUE 'SP'
            else if (condition((pnd_Ws_Header_Record_Cntrct_Cancel_Rdrw_Actvty_Cde.equals("SP"))))
            {
                decideConditionsMet914++;
                pnd_Ci2.setValue(47);                                                                                                                                     //Natural: ASSIGN #CI2 := 47
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet914 > 0))
            {
                pnd_Control_Totals_Pnd_Type_Code.getValue(pnd_Ci2).setValue(pnd_Ws_Header_Record_Cntrct_Type_Cde);                                                        //Natural: ASSIGN #TYPE-CODE ( #CI2 ) = #WS-HEADER-RECORD.CNTRCT-TYPE-CDE
                if (condition(pnd_Ws_Header_Record_Pymnt_Instmt_Nbr.equals(1)))                                                                                           //Natural: IF #WS-HEADER-RECORD.PYMNT-INSTMT-NBR = 01
                {
                    pnd_Control_Totals_Pnd_Cnt.getValue(pnd_Ci2).nadd(1);                                                                                                 //Natural: ADD 1 TO #CNT ( #CI2 )
                    pnd_Control_Totals_Pnd_Net_Amt.getValue(pnd_Ci2).nadd(pnd_Ws_Header_Record_Pymnt_Check_Amt);                                                          //Natural: ADD #WS-HEADER-RECORD.PYMNT-CHECK-AMT TO #NET-AMT ( #CI2 )
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: NONE VALUE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Work_Rec_Pnd_Ws_Record_Level_Nmbr.equals(20)))                                                                                               //Natural: IF #WS-WORK-REC.#WS-RECORD-LEVEL-NMBR = 20
        {
            pnd_Ws_Occurs.getValue(1).setValue(pnd_Ws_Work_Rec.getValue(1));                                                                                              //Natural: MOVE #WS-WORK-REC ( 1 ) TO #WS-OCCURS ( 1 )
            pnd_Ws_Occurs.getValue(2).setValue(pnd_Ws_Work_Rec.getValue(2));                                                                                              //Natural: MOVE #WS-WORK-REC ( 2 ) TO #WS-OCCURS ( 2 )
            pnd_Control_Totals_Pnd_Gross_Amt.getValue(pnd_In_Dex).nadd(pnd_Ws_Occurs_Inv_Acct_Settl_Amt);                                                                 //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT TO #GROSS-AMT ( #IN-DEX )
            pnd_Control_Totals_Pnd_Gross_Amt.getValue(pnd_Ci2).nadd(pnd_Ws_Occurs_Inv_Acct_Settl_Amt);                                                                    //Natural: ADD #WS-OCCURS.INV-ACCT-SETTL-AMT TO #GROSS-AMT ( #CI2 )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Missing_Checks() throws Exception                                                                                                                    //Natural: MISSING-CHECKS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        pnd_Ws_Check_Nbr.setValue(pnd_Old_Chk_Nbr);                                                                                                                       //Natural: ASSIGN #WS-CHECK-NBR = #OLD-CHK-NBR
        pnd_Ws_Check_Nbr.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #WS-CHECK-NBR
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(pnd_Ws_Check_Nbr.equals(pnd_Ws_Header_Record_Pnd_Ws_Pymnt_Check_Nbr))) {break;}                                                                 //Natural: UNTIL #WS-CHECK-NBR = #WS-HEADER-RECORD.#WS-PYMNT-CHECK-NBR
            getReports().write(2, ReportOption.NOTITLE,"MISSING CHECK:",pnd_Ws_Check_Nbr, new ReportEditMask ("9999999"));                                                //Natural: WRITE ( 2 ) 'MISSING CHECK:' #WS-CHECK-NBR ( EM = 9999999 )
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Check_Nbr.nadd(1);                                                                                                                                     //Natural: ADD 1 TO #WS-CHECK-NBR
            pnd_Missing_Checks.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #MISSING-CHECKS
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
    }
    private void sub_Get_Check_Formatting_Data() throws Exception                                                                                                         //Natural: GET-CHECK-FORMATTING-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().notEquals("0000")))                                                                                     //Natural: IF FCPA110.FCPA110-RETURN-CODE NE '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* RL
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Create_Annot_For_Hold() throws Exception                                                                                                             //Natural: CREATE-ANNOT-FOR-HOLD
    {
        if (BLNatReinput.isReinput()) return;

        //*   CHECK FOR EXISTENCE OF ANNOTATION RECORD TO PREVENT DUPLICATE - RCC
        if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Annot_Ind().notEquals("Y")))                                                                                    //Natural: IF FCP-CONS-PYMNU.PYMNT-ANNOT-IND NE 'Y'
        {
            ldaFcplpmnu.getFcp_Cons_Pymnu_Pymnt_Annot_Ind().setValue("Y");                                                                                                //Natural: ASSIGN FCP-CONS-PYMNU.PYMNT-ANNOT-IND := 'Y'
            ldaFcplannu.getVw_fcp_Cons_Annu().setValuesByName(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());                                                                        //Natural: MOVE BY NAME FCP-CONS-PYMNU TO FCP-CONS-ANNU
            ldaFcplannu.getFcp_Cons_Annu_Cntrct_Rcrd_Typ().setValue("4");                                                                                                 //Natural: ASSIGN FCP-CONS-ANNU.CNTRCT-RCRD-TYP := '4'
            if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")))                                                                                //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00'
            {
                ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "expedited mail: Tracking #"));            //Natural: COMPRESS ' Check sent' *DATU 'expedited mail: Tracking #' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))                                                                            //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'USPS'
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "USPS overnight"));                    //Natural: COMPRESS ' Check sent' *DATU 'USPS overnight' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            ldaFcplannu.getVw_fcp_Cons_Annu().insertDBRow();                                                                                                              //Natural: STORE FCP-CONS-ANNU
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*   IF ANNOTATION RECORD EXIST - ROXAN
            pnd_Ws_Annot_Key_Pnd_Ws_Annot_Key_Fields.setValuesByName(ldaFcplpmnu.getVw_fcp_Cons_Pymnu());                                                                 //Natural: MOVE BY NAME FCP-CONS-PYMNU TO #WS-ANNOT-KEY-FIELDS
            ldaFcplannu.getVw_fcp_Cons_Annu().startDatabaseFind                                                                                                           //Natural: FIND FCP-CONS-ANNU WITH PPCN-INV-ORGN-PRCSS-INST = #WS-ANNOT-KEY
            (
            "PND_PND_L0260",
            new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", "=", pnd_Ws_Annot_Key, WcType.WITH) }
            );
            PND_PND_L0260:
            while (condition(ldaFcplannu.getVw_fcp_Cons_Annu().readNextRow("PND_PND_L0260")))
            {
                ldaFcplannu.getVw_fcp_Cons_Annu().setIfNotFoundControlFlag(false);
                if (condition(ldaFcplannu.getFcp_Cons_Annu_Cntrct_Rcrd_Typ().notEquals("4")))                                                                             //Natural: IF FCP-CONS-ANNU.CNTRCT-RCRD-TYP NE '4'
                {
                    if (condition(true)) continue;                                                                                                                        //Natural: ESCAPE TOP
                }                                                                                                                                                         //Natural: END-IF
                //*   KEEP LINE1 IN LINE2 WHEN LINE2 IS EMPTY OTHERWISE OVERWRITE
                if (condition(ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt().equals(" ")))                                                                           //Natural: IF FCP-CONS-ANNU.PYMNT-RMRK-LINE2-TXT = ' '
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line2_Txt().setValue(ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt());                                    //Natural: ASSIGN FCP-CONS-ANNU.PYMNT-RMRK-LINE2-TXT := FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: END-IF
                if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("OV00")))                                                                            //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'OV00'
                {
                    ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "expedited mail: Tracking #"));        //Natural: COMPRESS ' Check sent' *DATU 'expedited mail: Tracking #' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(ldaFcplpmnu.getFcp_Cons_Pymnu_Cntrct_Hold_Cde().equals("USPS")))                                                                        //Natural: IF FCP-CONS-PYMNU.CNTRCT-HOLD-CDE = 'USPS'
                    {
                        ldaFcplannu.getFcp_Cons_Annu_Pymnt_Rmrk_Line1_Txt().setValue(DbsUtil.compress(" Check sent", Global.getDATU(), "USPS overnight"));                //Natural: COMPRESS ' Check sent' *DATU 'USPS overnight' INTO FCP-CONS-ANNU.PYMNT-RMRK-LINE1-TXT
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                ldaFcplannu.getVw_fcp_Cons_Annu().updateDBRow("PND_PND_L0260");                                                                                           //Natural: UPDATE ( ##L0260. )
            }                                                                                                                                                             //Natural: END-FIND
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt2 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(2, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(41),pnd_Header0_1,new TabSetting(124),ldaCdbatxa.getCdbatxa_Pnd_Page_Txt().getValue(pnd_Cur_Lang),getReports().getPageNumberDbs(2),  //Natural: WRITE ( 2 ) NOTITLE *PROGRAM 41T #HEADER0-1 124T CDBATXA.#PAGE-TXT ( #CUR-LANG ) *PAGE-NUMBER ( 2 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD','YY ) 41T #HEADER0-2 124T *TIMX ( EM = HH':'II' 'AP )
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD','YY"),new 
                        TabSetting(41),pnd_Header0_2,new TabSetting(124),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"));
                    getReports().skip(2, 1);                                                                                                                              //Natural: SKIP ( 2 ) 1
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };
    private void setupReports() throws Exception
    {
        Global.format(0, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(1, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
        Global.format(2, "LS=133 PS=60 ZP=ON IS=OFF ES=OFF SG=OFF");
    }
}
