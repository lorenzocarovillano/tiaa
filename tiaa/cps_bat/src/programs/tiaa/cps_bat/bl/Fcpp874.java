/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:04 PM
**        * FROM NATURAL PROGRAM : Fcpp874
************************************************************
**        * FILE NAME            : Fcpp874.java
**        * CLASS NAME           : Fcpp874
**        * INSTANCE NAME        : Fcpp874
************************************************************
************************************************************************
* PROGRAM  : FCPP874
* SYSTEM   : CPS
* TITLE    : NEW ANNUITIZATION
* FUNCTION : "NZ" ANNUITANT STATEMENTS
*
*
* 07/16/98 : BYPASS CHECK/STATEMENT PRINT OF RECORDS WITH
*            PYMNT-PAY-TYPE-REQ-IND = 8
* 12/01/99 : ROXAN
*            FCPNCNT2 REPLACED FCPNNZC2 FOR CONTROL PROGRAM
*            NEW LOCAL FCPAEXT
* 04/17/03 : ROXAN
*            RESTOW. FCPAEXT WAS EXPANDED
* 4/2017   : JJG PIN EXPANSION RESTOW FCPAEXT CHANGE
*
************************************************************************
* R. LANDRUM    01/05/2006 POS-PAY, PAYEE MATCH. ADD STOP POINT FOR
*               CHECK NBR ON STMNT BODY & CHECK FACE & ADD 10-DIGIT MICR
*
*************************** NOTE !!! **********************************
*
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTERNALLY.
************************************************************************
*
* 07/06/2009 :J.OSTEEN - MODIFIED TO PRINT ROTH MESSAGES ON CHECKS
*
* 15/01/2015 :RAHUL DAS- INCLUDE CANCEL STOP FILE TO PROCESS - TAG RAH
* 10/16/2015 :DASDH - PRB69034. FIX TO MISMATCH IN CONTROL COUNTS FOR
* COMBINED PAYMENTS
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp874 extends BLNatBase
{
    // Data Areas
    private PdaFcpaext pdaFcpaext;
    private PdaFcpa873 pdaFcpa873;
    private PdaFcpa803 pdaFcpa803;
    private PdaFcpa874a pdaFcpa874a;
    private PdaFcpa874h pdaFcpa874h;
    private PdaFcpa803l pdaFcpa803l;
    private PdaFcpanzcn pdaFcpanzcn;
    private PdaFcpanzc1 pdaFcpanzc1;
    private PdaFcpanzc2 pdaFcpanzc2;
    private PdaFcpacrpt pdaFcpacrpt;
    private LdaFcplbar1 ldaFcplbar1;
    private LdaFcplnzc2 ldaFcplnzc2;
    private PdaFcpa110 pdaFcpa110;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Key;

    private DbsGroup pnd_Key__R_Field_1;

    private DbsGroup pnd_Key_Pnd_Key_Detail;
    private DbsField pnd_Key_Cntrct_Orgn_Cde;
    private DbsField pnd_Key_Pymnt_Check_Dte;
    private DbsField pnd_Key_Cntrct_Ppcn_Nbr;
    private DbsField pnd_Key_Cntrct_Payee_Cde;
    private DbsField pnd_Key_Pymnt_Prcss_Seq_Num;
    private DbsField pnd_Old_Key;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Run_Rqust_Parameters;

    private DbsGroup pnd_Ws__R_Field_2;
    private DbsField pnd_Ws_Pnd_File_Type;
    private DbsField pnd_Ws_Pnd_Run_Type;
    private DbsField pnd_Ws_Pymnt_Check_Nbr;
    private DbsField pnd_Ws_Pnd_Global_Pay_Run;
    private DbsField pnd_Ws_Check_Nbr_A10;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Next_Rollover_Nbr;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_KeyOld;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpaext = new PdaFcpaext(localVariables);
        pdaFcpa873 = new PdaFcpa873(localVariables);
        pdaFcpa803 = new PdaFcpa803(localVariables);
        pdaFcpa874a = new PdaFcpa874a(localVariables);
        pdaFcpa874h = new PdaFcpa874h(localVariables);
        pdaFcpa803l = new PdaFcpa803l(localVariables);
        pdaFcpanzcn = new PdaFcpanzcn(localVariables);
        pdaFcpanzc1 = new PdaFcpanzc1(localVariables);
        pdaFcpanzc2 = new PdaFcpanzc2(localVariables);
        pdaFcpacrpt = new PdaFcpacrpt(localVariables);
        ldaFcplbar1 = new LdaFcplbar1();
        registerRecord(ldaFcplbar1);
        ldaFcplnzc2 = new LdaFcplnzc2();
        registerRecord(ldaFcplnzc2);
        pdaFcpa110 = new PdaFcpa110(localVariables);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);

        // Local Variables
        pnd_Key = localVariables.newFieldInRecord("pnd_Key", "#KEY", FieldType.STRING, 27);

        pnd_Key__R_Field_1 = localVariables.newGroupInRecord("pnd_Key__R_Field_1", "REDEFINE", pnd_Key);

        pnd_Key_Pnd_Key_Detail = pnd_Key__R_Field_1.newGroupInGroup("pnd_Key_Pnd_Key_Detail", "#KEY-DETAIL");
        pnd_Key_Cntrct_Orgn_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Key_Pymnt_Check_Dte = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Key_Cntrct_Ppcn_Nbr = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        pnd_Key_Cntrct_Payee_Cde = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        pnd_Key_Pymnt_Prcss_Seq_Num = pnd_Key_Pnd_Key_Detail.newFieldInGroup("pnd_Key_Pymnt_Prcss_Seq_Num", "PYMNT-PRCSS-SEQ-NUM", FieldType.NUMERIC, 
            7);
        pnd_Old_Key = localVariables.newFieldInRecord("pnd_Old_Key", "#OLD-KEY", FieldType.STRING, 27);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Run_Rqust_Parameters = pnd_Ws.newFieldInGroup("pnd_Ws_Run_Rqust_Parameters", "RUN-RQUST-PARAMETERS", FieldType.STRING, 18);

        pnd_Ws__R_Field_2 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_2", "REDEFINE", pnd_Ws_Run_Rqust_Parameters);
        pnd_Ws_Pnd_File_Type = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_File_Type", "#FILE-TYPE", FieldType.STRING, 10);
        pnd_Ws_Pnd_Run_Type = pnd_Ws__R_Field_2.newFieldInGroup("pnd_Ws_Pnd_Run_Type", "#RUN-TYPE", FieldType.STRING, 8);
        pnd_Ws_Pymnt_Check_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Pnd_Global_Pay_Run = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Global_Pay_Run", "#GLOBAL-PAY-RUN", FieldType.BOOLEAN, 1);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_3", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Next_Rollover_Nbr = localVariables.newFieldInRecord("pnd_Ws_Next_Rollover_Nbr", "#WS-NEXT-ROLLOVER-NBR", FieldType.NUMERIC, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_KeyOld = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Key_OLD", "Pnd_Key_OLD", FieldType.STRING, 27);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcplbar1.initializeValues();
        ldaFcplnzc2.initializeValues();
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp874() throws Exception
    {
        super("Fcpp874");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 0 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 0 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 24T 'PROCESS NEW ANNUITIZATION' #FILE-TYPE 68T 'REPORT: RPT0' / 36T #RUN-TYPE //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        //*  RL
                                                                                                                                                                          //Natural: PERFORM GET-CHECK-FORMAT-DATA
        sub_Get_Check_Format_Data();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT-PARM
        sub_Process_Input_Parm();
        if (condition(Global.isEscape())) {return;}
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                                   //Natural: MOVE TRUE TO #FCPANZC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 6 )
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 EXT ( * )
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pdaFcpaext.getExt().getValue("*"))))
        {
            CheckAtStartofData838();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            //*   MOVE BY NAME PYMNT-ADDR-INFO       TO #KEY-DETAIL                /*
            pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                             //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
                                                                                                                                                                          //Natural: PERFORM CALC-SIMPLEX-DUPLEX
            sub_Calc_Simplex_Duplex();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #KEY
            //*                                                                                                                                                           //Natural: AT BREAK OF #SIMPLEX
            pdaFcpaext.getExt_Pymnt_Check_Seq_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                            //Natural: ASSIGN EXT.PYMNT-CHECK-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
            if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean() || pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().getBoolean()))                                 //Natural: IF #FCPA803.#CHECK OR #FCPA803.#ZERO-CHECK
            {
                pdaFcpaext.getExt_Pymnt_Check_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                //Natural: ASSIGN EXT.PYMNT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpaext.getExt_Pymnt_Check_Scrty_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr());                                                    //Natural: ASSIGN EXT.PYMNT-CHECK-SCRTY-NBR := #FCPL876.PYMNT-CHECK-SCRTY-NBR
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
            sub_Accum_Control_Data();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                              //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := FALSE
            //* ********************** RL BEGIN PAYEE MATCH **************************
            //* *#OUTPUT-REC         := '12'
            //* *MOVE #FCPL876.PYMNT-CHECK-NBR TO #WS-CHECK-NBR-N7
            //* *MOVE FCPA110.START-CHECK-PREFIX-N3  TO #WS-CHECK-NBR-N3
            //* *#OUTPUT-REC-DETAIL := #WS-CHECK-NBR-N10
            //* *WRITE *PROGRAM '1450' 'GENERATE STOP FOR STMNT' /
            //* *  '='  #FCPL876.PYMNT-CHECK-NBR
            //* *  '='  #WS-CHECK-NBR-N10 /
            //* *  '='  #OUTPUT-REC(AL=17)/
            //* *WRITE WORK FILE 8 #OUTPUT-REC
            //* ********************** RL END PAYEE MATCH ****************************
            pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().nadd(1);                                                                                                      //Natural: ADD 1 TO #RECORD-IN-PYMNT
            //*  12/15/99
            if (condition(pdaFcpaext.getExt_Pymnt_Pay_Type_Req_Ind().notEquals(8)))                                                                                       //Natural: IF EXT.PYMNT-PAY-TYPE-REQ-IND NE 8
            {
                DbsUtil.callnat(Fcpnroth.class , getCurrentProcessState(), pdaFcpaext.getExt_Cntrct_Ppcn_Nbr(), pdaFcpaext.getExt_Cntrct_Invrse_Dte(),                    //Natural: CALLNAT 'FCPNROTH' EXT.CNTRCT-PPCN-NBR EXT.CNTRCT-INVRSE-DTE EXT.CNTRCT-ORGN-CDE EXT.PYMNT-PRCSS-SEQ-NBR #FCPA803
                    pdaFcpaext.getExt_Cntrct_Orgn_Cde(), pdaFcpaext.getExt_Pymnt_Prcss_Seq_Nbr(), pdaFcpa803.getPnd_Fcpa803());
                if (condition(Global.isEscape())) return;
                //*    IF #ROTH-IND
                //*      WRITE '************* ROTH ROTH ROTH ****************'
                //*    END-IF
                //*  TMM 8/8/00
                //*  RL CHECK&SEQ NBR & OTHER BANK DATA FOR CHECK PRINTING
                DbsUtil.callnat(Fcpn874b.class , getCurrentProcessState(), pdaFcpa873.getPnd_Nz_Check_Fields(), pdaFcpaext.getExt().getValue("*"), pdaFcpa803.getPnd_Fcpa803(),  //Natural: CALLNAT 'FCPN874B' #NZ-CHECK-FIELDS EXT ( * ) #FCPA803 #FCPA874A #FCPA874H #FCPA803L #BARCODE-LDA FCPA110
                    pdaFcpa874a.getPnd_Fcpa874a(), pdaFcpa874h.getPnd_Fcpa874h(), pdaFcpa803l.getPnd_Fcpa803l(), ldaFcplbar1.getPnd_Barcode_Lda(), pdaFcpa110.getFcpa110());
                if (condition(Global.isEscape())) return;
                getReports().write(0, Global.getPROGRAM(),"1650 RETURN FROM FCPN874B",NEWLINE,"=",pdaFcpa110.getFcpa110_Fcpa110_New_Check_Prefix_A3(),NEWLINE,"=",pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(),  //Natural: WRITE *PROGRAM '1650 RETURN FROM FCPN874B' / '=' FCPA110-NEW-CHECK-PREFIX-A3 / '=' FCPA110-RETURN-MSG ( AL = 25 ) /
                    new AlphanumericLength (25),NEWLINE);
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Next_Rollover_Nbr.nadd(1);                                                                                                                         //Natural: ADD 1 TO #WS-NEXT-ROLLOVER-NBR
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-OUTPUT-FILE
            sub_Write_Output_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            readWork01Pnd_KeyOld.setValue(pnd_Key);                                                                                                                       //Natural: AT END OF DATA;//Natural: END-WORK
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            if (condition(pnd_Ws_Pnd_Global_Pay_Run.getBoolean()))                                                                                                        //Natural: IF #GLOBAL-PAY-RUN
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-VOID-PAGE
                sub_Print_Void_Page();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        //*  RAH
        if (condition(pnd_Ws_Pnd_Run_Type.equals("NON HELD")))                                                                                                            //Natural: IF #RUN-TYPE EQ 'NON HELD'
        {
            //*  DASDH 10/04
            pnd_Old_Key.reset();                                                                                                                                          //Natural: RESET #OLD-KEY
            //*  RAH
            //*  RAH
            READWORK02:                                                                                                                                                   //Natural: READ WORK FILE 10 EXT ( * )
            while (condition(getWorkFiles().read(10, pdaFcpaext.getExt().getValue("*"))))
            {
                //*  RAH
                //*  RAH
                //*  RAH
                //*  RAH
                pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                           //Natural: MOVE TRUE TO #FCPANZC2.#ACCUM-TRUTH-TABLE ( 1 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 2 ) #FCPANZC2.#ACCUM-TRUTH-TABLE ( 6 )
                pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
                pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_Accum_Truth_Table().getValue(6).setValue(true);
                //*  DASDH 10/04
                pnd_Key_Pnd_Key_Detail.setValuesByName(pdaFcpaext.getExt_Extr());                                                                                         //Natural: MOVE BY NAME EXTR TO #KEY-DETAIL
                //*  DASDH 10/04
                //*  DASDH 10/04
                if (condition(pnd_Key.notEquals(pnd_Old_Key)))                                                                                                            //Natural: IF #KEY NE #OLD-KEY
                {
                    pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                       //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := TRUE
                    //*  RAH
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
                    sub_Set_Control_Data();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    //*  DASDH 10/04
                    pnd_Old_Key.setValue(pnd_Key);                                                                                                                        //Natural: MOVE #KEY TO #OLD-KEY
                }                                                                                                                                                         //Natural: END-IF
                //*  RAH
                //*  DASDH 10/04
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-DATA
                sub_Accum_Control_Data();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(false);                                                                                          //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := FALSE
                //*  RAH
            }                                                                                                                                                             //Natural: END-WORK
            READWORK02_Exit:
            if (Global.isEscape()) return;
            //*  RAH
        }                                                                                                                                                                 //Natural: END-IF
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().compute(new ComputeParameters(false, ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr()), pnd_Ws_Pymnt_Check_Nbr.add(1));    //Natural: ASSIGN #FCPL876.PYMNT-CHECK-NBR := #WS.PYMNT-CHECK-NBR + 1
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().nadd(1);                                                                                                        //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nadd(1);                                                                                                          //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SEQ-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Last_Rollover_Nbr().setValue(pnd_Ws_Next_Rollover_Nbr);                                                                           //Natural: ASSIGN #FCPL876.PYMNT-LAST-ROLLOVER-NBR := #WS-NEXT-ROLLOVER-NBR
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"Counters etc, at end of program:",NEWLINE,"Next check  number...........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr(), //Natural: WRITE *PROGRAM *TIME 'Counters etc, at end of program:' / 'Next check  number...........:' #FCPL876.PYMNT-CHECK-NBR / 'Next EFT    number...........:' #FCPL876.PYMNT-CHECK-SCRTY-NBR / 'Next payment sequence number.:' #FCPL876.PYMNT-CHECK-SEQ-NBR / 'Next rollover number ........:' #FCPL876.PYMNT-LAST-ROLLOVER-NBR
            NEWLINE,"Next EFT    number...........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr(),NEWLINE,"Next payment sequence number.:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr(),
            NEWLINE,"Next rollover number ........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Last_Rollover_Nbr());
        if (Global.isEscape()) return;
        ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Barcode().getValue("*").setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue("*"));                               //Natural: ASSIGN #FCPL876.#BAR-BARCODE ( * ) := #BARCODE-LDA.#BAR-BARCODE ( * )
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
        sub_Print_Control_Report();
        if (condition(Global.isEscape())) {return;}
        if (condition(! (pnd_Ws_Pnd_Global_Pay_Run.getBoolean())))                                                                                                        //Natural: IF NOT #GLOBAL-PAY-RUN
        {
            getWorkFiles().write(7, false, ldaFcpl876.getPnd_Fcpl876());                                                                                                  //Natural: WRITE WORK FILE 7 #FCPL876
        }                                                                                                                                                                 //Natural: END-IF
        getReports().write(0, NEWLINE,NEWLINE,"****** END OF PROGRAM EXECUTION ****");                                                                                    //Natural: WRITE // '****** END OF PROGRAM EXECUTION ****'
        if (Global.isEscape()) return;
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OUTPUT-FILE
        //* *******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PYMNT
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-STMNT-TYPE
        //* *************************************
        //*    #FCPA803.#STMNT                    := TRUE
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALC-SIMPLEX-DUPLEX
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-DATA
        //* ***********************************
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SET-CONTROL-DATA
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-VOID-PAGE
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT-PARM
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-NEXT-CHECK-FILE
        //* ****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-HOLD-CONTROL-RECORD
        //* *************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
        //* *********************** RL BEGIN - PAYEE MATCH ************************
        //* *****************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CHECK-FORMAT-DATA
        //* ************************** RL END-PAYEE MATCH *************************
        //* ***********************************************************************
        //*  COPYCODE   : FCPC803W
        //*  SYSTEM     : CPS
        //*  TITLE      : IAR RESTRUCTURE
        //*  FUNCTION   : WRITE STATEMENT
        //* ***********************************************************************
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: FCPC803W
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Write_Output_File() throws Exception                                                                                                                 //Natural: WRITE-OUTPUT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        //*  12/15/99
        getWorkFiles().write(9, true, pdaFcpaext.getExt().getValue("*"));                                                                                                 //Natural: WRITE WORK FILE 9 VARIABLE EXT ( * )
        //*   #CHECK-SORT-FIELDS                                      /*
        //*   PYMNT-ADDR-INFO                                         /*
        //*   #NZ-CHECK-FIELDS                                        /*
        //*   INV-INFO(1:C-INV-ACCT)                                  /*
        //*  END-IF
    }
    private void sub_Init_New_Pymnt() throws Exception                                                                                                                    //Natural: INIT-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
                                                                                                                                                                          //Natural: PERFORM DETERMINE-STMNT-TYPE
        sub_Determine_Stmnt_Type();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa873.getPnd_Nz_Check_Fields().reset();                                                                                                                      //Natural: RESET #NZ-CHECK-FIELDS
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nadd(1);                                                                                                          //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SEQ-NBR
        short decideConditionsMet1032 = 0;                                                                                                                                //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FCPA803.#CHECK
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Check().getBoolean()))
        {
            decideConditionsMet1032++;
            pnd_Ws_Pymnt_Check_Nbr.nadd(1);                                                                                                                               //Natural: ADD 1 TO #WS.PYMNT-CHECK-NBR
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().setValue(pnd_Ws_Pymnt_Check_Nbr);                                                                                 //Natural: ASSIGN #FCPL876.PYMNT-CHECK-NBR := #WS.PYMNT-CHECK-NBR
            //*   IF #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE = '0000'           /* 12/15/99
            if (condition(pdaFcpaext.getExt_Cntrct_Hold_Cde().equals("0000") || pdaFcpaext.getExt_Cntrct_Hold_Cde().equals(" ")))                                         //Natural: IF EXT.CNTRCT-HOLD-CDE = '0000' OR = ' '
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(false);                                                                                               //Natural: ASSIGN #FCPL876A.#HOLD-IND := FALSE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().setValue(true);                                                                                                //Natural: ASSIGN #FCPL876A.#HOLD-IND := TRUE
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl876a.getPnd_Fcpl876a_Cntrct_Orgn_Cde().setValue(pdaFcpaext.getExt_Cntrct_Orgn_Cde());                                                                  //Natural: ASSIGN #FCPL876A.CNTRCT-ORGN-CDE := EXT.CNTRCT-ORGN-CDE
            ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                  //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-SEQ-NBR := #FCPL876.PYMNT-CHECK-SEQ-NBR
            ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                          //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
            getWorkFiles().write(8, false, ldaFcpl876a.getPnd_Fcpl876a());                                                                                                //Natural: WRITE WORK FILE 8 #FCPL876A
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#EFT
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Eft().getBoolean()))
        {
            decideConditionsMet1032++;
            //*  INTERNAL ROLLOVER
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().nadd(1);                                                                                                    //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#GLOBAL-PAY
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().getBoolean()))
        {
            decideConditionsMet1032++;
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().nadd(1);                                                                                                    //Natural: ADD 1 TO #FCPL876.PYMNT-CHECK-SCRTY-NBR
        }                                                                                                                                                                 //Natural: WHEN #FCPA803.#ZERO-CHECK
        else if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().getBoolean()))
        {
            decideConditionsMet1032++;
            ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().setValue(9999999);                                                                                                //Natural: ASSIGN #FCPL876.PYMNT-CHECK-NBR := 9999999
        }                                                                                                                                                                 //Natural: WHEN NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        pdaFcpa803.getPnd_Fcpa803_Pnd_End_Of_Pymnt().setValue(false);                                                                                                     //Natural: ASSIGN #END-OF-PYMNT := FALSE
        pdaFcpa803.getPnd_Fcpa803_Pnd_Record_In_Pymnt().reset();                                                                                                          //Natural: RESET #RECORD-IN-PYMNT
        pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page().compute(new ComputeParameters(false, pdaFcpa803.getPnd_Fcpa803_Pnd_Bar_Last_Page()), (pdaFcpaext.getExt_Pymnt_Total_Pages().add(1)).divide(2).multiply(2).subtract(1)); //Natural: COMPUTE #BAR-LAST-PAGE = ( PYMNT-TOTAL-PAGES + 1 ) / 2 * 2 - 1
        pdaFcpa803.getPnd_Fcpa803_Pnd_New_Pymnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#NEW-PYMNT := TRUE
    }
    private void sub_Determine_Stmnt_Type() throws Exception                                                                                                              //Natural: DETERMINE-STMNT-TYPE
    {
        if (BLNatReinput.isReinput()) return;

        pdaFcpanzc2.getPnd_Fcpanzc2_Pnd_New_Pymnt_Ind().setValue(true);                                                                                                   //Natural: ASSIGN #FCPANZC2.#NEW-PYMNT-IND := TRUE
                                                                                                                                                                          //Natural: PERFORM SET-CONTROL-DATA
        sub_Set_Control_Data();
        if (condition(Global.isEscape())) {return;}
        pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_Type().reset();                                                                                                               //Natural: RESET #FCPA803.#STMNT-TYPE
        //*  CHECK
        short decideConditionsMet1068 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #ACCUM-OCCUR-2;//Natural: VALUE 48
        if (condition((pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().equals(48))))
        {
            decideConditionsMet1068++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Check().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#CHECK := TRUE
            if (condition(pdaFcpaext.getExt_Pymnt_Eft_Acct_Nbr().equals(" ") && ! (pdaFcpaext.getExt_Pymnt_Alt_Addr_Ind().getBoolean())))                                 //Natural: IF EXT.PYMNT-EFT-ACCT-NBR = ' ' AND NOT EXT.PYMNT-ALT-ADDR-IND
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Check_To_Annt().setValue(true);                                                                                             //Natural: ASSIGN #FCPA803.#CHECK-TO-ANNT := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                     //Natural: ASSIGN #FCPA803.#STMNT := TRUE
                pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().setValue(true);                                                                                             //Natural: ASSIGN #FCPA803.#STMNT-TO-ANNT := TRUE
                //*  EFT
                //*  ZERO CHECK
                //*  GLOBAL PAY / INT. ROLLOVER
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: VALUE 49
        else if (condition((pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().equals(49))))
        {
            decideConditionsMet1068++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#STMNT := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt_To_Annt().setValue(true);                                                                                                 //Natural: ASSIGN #FCPA803.#STMNT-TO-ANNT := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Eft().setValue(true);                                                                                                           //Natural: ASSIGN #FCPA803.#EFT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 47
        else if (condition((pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().equals(47))))
        {
            decideConditionsMet1068++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Zero_Check().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#ZERO-CHECK := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().setValue(true);                                                                                                         //Natural: ASSIGN #FCPA803.#STMNT := TRUE
        }                                                                                                                                                                 //Natural: VALUE 50
        else if (condition((pdaFcpanzc1.getPnd_Fcpanzc1_Pnd_Accum_Occur_2().equals(50))))
        {
            decideConditionsMet1068++;
            pdaFcpa803.getPnd_Fcpa803_Pnd_Global_Pay().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#GLOBAL-PAY := TRUE
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //* IF #FCPA803.#STMNT AND PYMNT-ADDR-LINES(2) NE ' '         /* 12/15/99
        //*  CORRESPONDENCE ADDRESS
        if (condition(pdaFcpa803.getPnd_Fcpa803_Pnd_Stmnt().getBoolean() && pdaFcpaext.getExt_Pymnt_Addr_Line_Txt().getValue(2,"*").notEquals(" ")))                      //Natural: IF #FCPA803.#STMNT AND EXT.PYMNT-ADDR-LINE-TXT ( 2,* ) NE ' '
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(2);                                                                                                         //Natural: ASSIGN #ADDR-IND := 2
            //*  PAYMENT ADDRESS
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Addr_Ind().setValue(1);                                                                                                         //Natural: ASSIGN #ADDR-IND := 1
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Calc_Simplex_Duplex() throws Exception                                                                                                               //Natural: CALC-SIMPLEX-DUPLEX
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        if (condition(pdaFcpaext.getExt_Pymnt_Total_Pages().equals(1)))                                                                                                   //Natural: IF PYMNT-TOTAL-PAGES = 1
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().setValue(true);                                                                                                       //Natural: ASSIGN #SIMPLEX := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().setValue(false);                                                                                                      //Natural: ASSIGN #SIMPLEX := FALSE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Accum_Control_Data() throws Exception                                                                                                                //Natural: ACCUM-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().setValue(pdaFcpaext.getExt_C_Inv_Acct());                                                                              //Natural: ASSIGN #MAX-FUND := C-INV-ACCT
        if (condition(ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().equals(getZero())))                                                                                      //Natural: IF #MAX-FUND = 0
        {
            ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund().setValue(1);                                                                                                       //Natural: ASSIGN #MAX-FUND := 1
        }                                                                                                                                                                 //Natural: END-IF
        //* VE BY NAME INV-INFO(1:#MAX-FUND) TO #CNTRL-INV-ACCT(1:#MAX-FUND)  /*
        ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()).setValuesByName(pdaFcpaext.getExt_Inv_Acct().getValue(1, //Natural: MOVE BY NAME INV-ACCT ( 1:#MAX-FUND ) TO #CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
        DbsUtil.callnat(Fcpnnzc1.class , getCurrentProcessState(), pdaFcpanzc1.getPnd_Fcpanzc1(), pdaFcpanzc2.getPnd_Fcpanzc2(), ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund(),  //Natural: CALLNAT 'FCPNNZC1' USING #FCPANZC1 #FCPANZC2 #FCPLNZC2.#MAX-FUND #FCPLNZC2.#CNTRL-INV-ACCT ( 1:#MAX-FUND )
            ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Cntrl_Inv_Acct().getValue(1,":",ldaFcplnzc2.getPnd_Fcplnzc2_Pnd_Max_Fund()));
        if (condition(Global.isEscape())) return;
    }
    private void sub_Set_Control_Data() throws Exception                                                                                                                  //Natural: SET-CONTROL-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        //* MOVE BY NAME NZ-EXT.PYMNT-ADDR-INFO TO #FCPANZCN           /* 12/15/99
        pdaFcpanzcn.getPnd_Fcpanzcn().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                          //Natural: MOVE BY NAME EXT.EXTR TO #FCPANZCN
        DbsUtil.callnat(Fcpnnzcn.class , getCurrentProcessState(), pdaFcpacrpt.getPnd_Fcpacrpt(), pdaFcpanzcn.getPnd_Fcpanzcn(), pdaFcpanzc1.getPnd_Fcpanzc1());          //Natural: CALLNAT 'FCPNNZCN' USING #FCPACRPT #FCPANZCN #FCPANZC1
        if (condition(Global.isEscape())) return;
    }
    private void sub_Print_Void_Page() throws Exception                                                                                                                   //Natural: PRINT-VOID-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        DbsUtil.callnat(Fcpn803v.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803());                                                                          //Natural: CALLNAT 'FCPN803V' USING #FCPA803
        if (condition(Global.isEscape())) return;
    }
    private void sub_Process_Input_Parm() throws Exception                                                                                                                //Natural: PROCESS-INPUT-PARM
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        getWorkFiles().read(2, pnd_Ws_Run_Rqust_Parameters);                                                                                                              //Natural: READ WORK FILE 2 ONCE RUN-RQUST-PARAMETERS
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING PARAMETER FILE",new TabSetting(77),"***");                                                            //Natural: WRITE '***' 25T 'MISSING PARAMETER FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(50);  if (true) return;                                                                                                                     //Natural: TERMINATE 50
        }                                                                                                                                                                 //Natural: END-ENDFILE
        short decideConditionsMet1141 = 0;                                                                                                                                //Natural: DECIDE ON FIRST VALUE OF #FILE-TYPE;//Natural: VALUE 'CHECK' , 'CHECKS'
        if (condition((pnd_Ws_Pnd_File_Type.equals("CHECK") || pnd_Ws_Pnd_File_Type.equals("CHECKS"))))
        {
            decideConditionsMet1141++;
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(47,":",49).setValue(true);                                                                             //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 47:49 ) := TRUE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("  NEW ANNUITIZATION CHECK STATEMENTS");                                                                     //Natural: ASSIGN #FCPACRPT.#TITLE := '  NEW ANNUITIZATION CHECK STATEMENTS'
            if (condition(pnd_Ws_Pnd_Run_Type.equals("HELD") || pnd_Ws_Pnd_Run_Type.equals("HOLD")))                                                                      //Natural: IF #RUN-TYPE = 'HELD' OR = 'HOLD'
            {
                ignore();
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                                                                                                                                                                          //Natural: PERFORM GET-HOLD-CONTROL-RECORD
                sub_Get_Hold_Control_Record();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM GET-NEXT-CHECK-FILE
            sub_Get_Next_Check_File();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: VALUE 'GLOBAL'
        else if (condition((pnd_Ws_Pnd_File_Type.equals("GLOBAL"))))
        {
            decideConditionsMet1141++;
            pnd_Ws_Pnd_Global_Pay_Run.setValue(true);                                                                                                                     //Natural: ASSIGN #GLOBAL-PAY-RUN := TRUE
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                  //Natural: RESET #BARCODE-LDA.#BAR-ENV-ID-NUM
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(50).setValue(true);                                                                                    //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 50 ) := TRUE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Title().setValue("NEW ANNUITIZATION GLOBAL PAY STATEMENTS");                                                                  //Natural: ASSIGN #FCPACRPT.#TITLE := 'NEW ANNUITIZATION GLOBAL PAY STATEMENTS'
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"INVALID PARAMETER:",pnd_Ws_Pnd_File_Type,new TabSetting(77),"***");                                           //Natural: WRITE '***' 25T 'INVALID PARAMETER:' #FILE-TYPE 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Get_Next_Check_File() throws Exception                                                                                                               //Natural: GET-NEXT-CHECK-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getWorkFiles().read(6, ldaFcpl876.getPnd_Fcpl876());                                                                                                              //Natural: READ WORK FILE 6 ONCE #FCPL876
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'NEXT CHECK' FILE",new TabSetting(77),"***");                                                         //Natural: WRITE '***' 25T 'MISSING "NEXT CHECK" FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(52);  if (true) return;                                                                                                                     //Natural: TERMINATE 52
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //* *S-CHECK-NBR-N3              := #FCPL876.PYMNT-CHECK-PREFIX /* RL
        //*  RL
        getReports().write(0, Global.getPROGRAM(),"3490",NEWLINE,"=",pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3);                                                           //Natural: WRITE *PROGRAM '3490' / '=' #WS-CHECK-NBR-N3
        if (Global.isEscape()) return;
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Barcode().getValue("*").setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Barcode().getValue("*"));                               //Natural: ASSIGN #BARCODE-LDA.#BAR-BARCODE ( * ) := #FCPL876.#BAR-BARCODE ( * )
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes().setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes());                                                       //Natural: ASSIGN #BARCODE-LDA.#BAR-ENVELOPES := #FCPL876.#BAR-ENVELOPES
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().getBoolean());                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := #FCPL876.#BAR-NEW-RUN
        getReports().write(0, Global.getPROGRAM(),Global.getTIME(),"at start of program:",NEWLINE,"The program is going to use the following:",NEWLINE,                   //Natural: WRITE *PROGRAM *TIME 'at start of program:' / 'The program is going to use the following:' / 'start assigned check   number:' #FCPL876.PYMNT-CHECK-NBR / 'start assigned EFT     number:' #FCPL876.PYMNT-CHECK-SCRTY-NBR / 'start sequence number........:' #FCPL876.PYMNT-CHECK-SEQ-NBR / 'start rollover-nbr ..........:' #FCPL876.PYMNT-LAST-ROLLOVER-NBR
            "start assigned check   number:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr(),NEWLINE,"start assigned EFT     number:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr(),
            NEWLINE,"start sequence number........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr(),NEWLINE,"start rollover-nbr ..........:",ldaFcpl876.getPnd_Fcpl876_Pymnt_Last_Rollover_Nbr());
        if (Global.isEscape()) return;
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr().nsubtract(1);                                                                                                         //Natural: SUBTRACT 1 FROM #FCPL876.PYMNT-CHECK-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Scrty_Nbr().nsubtract(1);                                                                                                   //Natural: SUBTRACT 1 FROM #FCPL876.PYMNT-CHECK-SCRTY-NBR
        ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().nsubtract(1);                                                                                                     //Natural: SUBTRACT 1 FROM #FCPL876.PYMNT-CHECK-SEQ-NBR
        pnd_Ws_Pymnt_Check_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                                     //Natural: ASSIGN #WS.PYMNT-CHECK-NBR := #FCPL876.PYMNT-CHECK-NBR
        pnd_Ws_Next_Rollover_Nbr.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Last_Rollover_Nbr());                                                                           //Natural: ASSIGN #WS-NEXT-ROLLOVER-NBR := #FCPL876.PYMNT-LAST-ROLLOVER-NBR
    }
    private void sub_Get_Hold_Control_Record() throws Exception                                                                                                           //Natural: GET-HOLD-CONTROL-RECORD
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        getWorkFiles().read(4, pdaFcpanzc2.getPnd_Fcpanzc2());                                                                                                            //Natural: READ WORK FILE 4 ONCE #FCPANZC2
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'HOLD CHECK' CONTROL FILE",new TabSetting(77),"***");                                                 //Natural: WRITE '***' 25T 'MISSING "HOLD CHECK" CONTROL FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-ENDFILE
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        if (condition(pnd_Ws_Pnd_Run_Type.equals("HELD") || pnd_Ws_Pnd_Run_Type.equals("HOLD")))                                                                          //Natural: IF #RUN-TYPE = 'HELD' OR = 'HOLD'
        {
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(true);                                                                                                   //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := TRUE
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().reset();                                                                                                        //Natural: RESET #FCPL876.#BAR-ENVELOPES
            getWorkFiles().write(3, false, pdaFcpanzc2.getPnd_Fcpanzc2());                                                                                                //Natural: WRITE WORK FILE 3 #FCPANZC2
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_New_Run().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().getBoolean());                                          //Natural: ASSIGN #FCPL876.#BAR-NEW-RUN := #BARCODE-LDA.#BAR-NEW-RUN
            ldaFcpl876.getPnd_Fcpl876_Pnd_Bar_Envelopes().setValue(ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Envelopes());                                                   //Natural: ASSIGN #FCPL876.#BAR-ENVELOPES := #BARCODE-LDA.#BAR-ENVELOPES
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_No_Abend().setValue(false);                                                                                                   //Natural: ASSIGN #FCPACRPT.#NO-ABEND := FALSE
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Program().setValue(Global.getPROGRAM());                                                                                      //Natural: ASSIGN #FCPACRPT.#PROGRAM := *PROGRAM
            pdaFcpacrpt.getPnd_Fcpacrpt_Pnd_Truth_Table().getValue(43).setValue(false);                                                                                   //Natural: ASSIGN #FCPACRPT.#TRUTH-TABLE ( 43 ) := FALSE
            //*   CALLNAT 'FCPNNZC2'
            //*   12/01/99  ROXAN
            //*   12/01/99  ROXAN
            DbsUtil.callnat(Fcpncnt2.class , getCurrentProcessState(), pdaFcpanzc2.getPnd_Fcpanzc2(), pdaFcpacrpt.getPnd_Fcpacrpt(), "NZ");                               //Natural: CALLNAT 'FCPNCNT2' USING #FCPANZC2 #FCPACRPT 'NZ'
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  RL
    private void sub_Get_Check_Format_Data() throws Exception                                                                                                             //Natural: GET-CHECK-FORMAT-DATA
    {
        if (BLNatReinput.isReinput()) return;

        //* *****************************************
        //* * GET START-CHECK-NO FROM REFERENCE TABLE
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        //*  RL
        //* RL
        //* RL
        //* RL
        //* RL
        //* RL
        getReports().write(0, "6915",Global.getPROGRAM(),"CALL FCPN110 REF TABLE P14A1",NEWLINE,"=",pdaFcpa110.getFcpa110_Fcpa110_Source_Code(),NEWLINE,"=",pdaFcpa110.getFcpa110_Start_Check_No(),"=",pdaFcpa110.getFcpa110_Start_Check_Prefix_N3(),"=",pdaFcpa110.getFcpa110_Company_Name(),NEWLINE,"=",pdaFcpa110.getFcpa110_Company_Address(),  //Natural: WRITE '6915' *PROGRAM 'CALL FCPN110 REF TABLE P14A1' / '=' FCPA110.FCPA110-SOURCE-CODE / '=' FCPA110.START-CHECK-NO '=' START-CHECK-PREFIX-N3 '=' FCPA110.COMPANY-NAME / '=' FCPA110.COMPANY-ADDRESS ( AL = 25 ) '=' FCPA110.BANK-ADDRESS1 ( AL = 25 ) / '=' FCPA110.BANK-TRANSMISSION-CDE / '=' FCPA110.BANK-NAME ( AL = 10 ) /
            new AlphanumericLength (25),"=",pdaFcpa110.getFcpa110_Bank_Address1(), new AlphanumericLength (25),NEWLINE,"=",pdaFcpa110.getFcpa110_Bank_Transmission_Cde(),NEWLINE,"=",pdaFcpa110.getFcpa110_Bank_Name(), 
            new AlphanumericLength (10),NEWLINE);
        if (Global.isEscape()) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_New_Run().setValue(true);                                                                                              //Natural: ASSIGN #BARCODE-LDA.#BAR-NEW-RUN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: MOVE FCPA110.START-CHECK-PREFIX-N3 TO #WS-CHECK-NBR-N3
        ldaFcplbar1.getPnd_Barcode_Lda_Pnd_Bar_Env_Id_Num().reset();                                                                                                      //Natural: RESET #BAR-ENV-ID-NUM
    }
    private void sub_Fcpc803w() throws Exception                                                                                                                          //Natural: FCPC803W
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        getWorkFiles().write(8, false, pdaFcpa803.getPnd_Fcpa803_Pnd_Output_Rec());                                                                                       //Natural: WRITE WORK FILE 8 #OUTPUT-REC
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_KeyIsBreak = pnd_Key.isBreak(endOfData);
        boolean pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak = pdaFcpa803.getPnd_Fcpa803_Pnd_Simplex().isBreak(endOfData);
        if (condition(pnd_KeyIsBreak || pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak))
        {
            //*  NOT EOF
            if (condition(pnd_Key.notEquals(readWork01Pnd_KeyOld)))                                                                                                       //Natural: IF #KEY NE OLD ( #KEY )
            {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
                sub_Init_New_Pymnt();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(pdaFcpa803_getPnd_Fcpa803_Pnd_SimplexIsBreak))
        {
            pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(0, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(0), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(24),"PROCESS NEW ANNUITIZATION",pnd_Ws_Pnd_File_Type,new TabSetting(68),"REPORT: RPT0",NEWLINE,new 
            TabSetting(36),pnd_Ws_Pnd_Run_Type,NEWLINE,NEWLINE);
    }
    private void CheckAtStartofData838() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            //*   MOVE BY NAME PYMNT-ADDR-INFO       TO #FCPA803                   /*
            pdaFcpa803.getPnd_Fcpa803().setValuesByName(pdaFcpaext.getExt_Extr());                                                                                        //Natural: MOVE BY NAME EXTR TO #FCPA803
            if (condition(pnd_Ws_Pnd_Global_Pay_Run.getBoolean()))                                                                                                        //Natural: IF #GLOBAL-PAY-RUN
            {
                                                                                                                                                                          //Natural: PERFORM PRINT-VOID-PAGE
                sub_Print_Void_Page();
                if (condition(Global.isEscape())) {return;}
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PYMNT
            sub_Init_New_Pymnt();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM CALC-SIMPLEX-DUPLEX
            sub_Calc_Simplex_Duplex();
            if (condition(Global.isEscape())) {return;}
            pdaFcpa803.getPnd_Fcpa803_Pnd_Full_Xerox().setValue(true);                                                                                                    //Natural: ASSIGN #FCPA803.#FULL-XEROX := TRUE
        }                                                                                                                                                                 //Natural: END-START
    }
}
