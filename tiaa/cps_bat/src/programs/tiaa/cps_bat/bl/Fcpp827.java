/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:32 PM
**        * FROM NATURAL PROGRAM : Fcpp827
************************************************************
**        * FILE NAME            : Fcpp827.java
**        * CLASS NAME           : Fcpp827
**        * INSTANCE NAME        : Fcpp827
************************************************************
************************************************************************
* PROGRAM  : FCPP827
* SYSTEM   : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS "AP" LEDGER ACCOUNT REPORT
* CREATED  : 02/16/96
*
* 04/01/97 : RITA SALGADO
*          : GET LEDGER DESCRIPTION USING ISA NBR INSTEAD OF FUND CODE
* 05/22/00 : MCGEE
*          : STOW - FCPL199B
* 06/11/02 : R CARREON
*          : RE-STOWED. PA-SELECT AND SPIA NEW FUNDS IN FCPL199A
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp827 extends BLNatBase
{
    // Data Areas
    private PdaFcpa121 pdaFcpa121;
    private LdaFcpl199a ldaFcpl199a;
    private LdaFcpl199b ldaFcpl199b;
    private LdaFcpl827 ldaFcpl827;
    private LdaFcpl826a ldaFcpl826a;
    private LdaFcpl826b ldaFcpl826b;

    // Local Variables
    public DbsRecord localVariables;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Save_Company;
    private DbsField pnd_Ws_Pnd_Rec_Cnt;

    private DbsGroup pnd_Ws_Pnd_Amt_Table;
    private DbsField pnd_Ws_Pnd_Cr_Amt;
    private DbsField pnd_Ws_Pnd_Dr_Amt;
    private DbsField pnd_Ws_Pnd_Cr_Cv;
    private DbsField pnd_Ws_Pnd_Dr_Cv;
    private DbsField pnd_Ws_Pnd_Isa_Sub;
    private DbsField pnd_Ws_Pnd_Newpage_Ind;

    private DbsGroup pnd_Ws_Pnd_Breaks;
    private DbsField pnd_Ws_Pnd_Ledger_Break;
    private DbsField pnd_Ws_Pnd_Company_Break;
    private DbsField pnd_Ws_Pnd_Company_Cde_Break;
    private DbsField pnd_Ws_Pnd_Ins_Option_Break;
    private DbsField pnd_Ws_Pnd_Life_Cont_Break;
    private DbsField pnd_Ws_Pnd_Annty_Type_Break;
    private DbsField pnd_Ws_Pnd_Annty_Break;
    private DbsField pnd_Ws_Pnd_Check_Dte_Break;
    private DbsField pnd_Ws_Pnd_Orgn_Break;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Cr_AmtSum169;
    private DbsField readWork01Pnd_Cr_AmtSum;
    private DbsField readWork01Pnd_Dr_AmtSum169;
    private DbsField readWork01Pnd_Dr_AmtSum;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa121 = new PdaFcpa121(localVariables);
        ldaFcpl199a = new LdaFcpl199a();
        registerRecord(ldaFcpl199a);
        ldaFcpl199b = new LdaFcpl199b();
        registerRecord(ldaFcpl199b);
        ldaFcpl827 = new LdaFcpl827();
        registerRecord(ldaFcpl827);
        ldaFcpl826a = new LdaFcpl826a();
        registerRecord(ldaFcpl826a);
        ldaFcpl826b = new LdaFcpl826b();
        registerRecord(ldaFcpl826b);

        // Local Variables

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Save_Company = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Save_Company", "#SAVE-COMPANY", FieldType.STRING, 5);
        pnd_Ws_Pnd_Rec_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 7);

        pnd_Ws_Pnd_Amt_Table = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Amt_Table", "#AMT-TABLE", new DbsArrayController(1, 4));
        pnd_Ws_Pnd_Cr_Amt = pnd_Ws_Pnd_Amt_Table.newFieldInGroup("pnd_Ws_Pnd_Cr_Amt", "#CR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Dr_Amt = pnd_Ws_Pnd_Amt_Table.newFieldInGroup("pnd_Ws_Pnd_Dr_Amt", "#DR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Cr_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cr_Cv", "#CR-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Dr_Cv = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dr_Cv", "#DR-CV", FieldType.ATTRIBUTE_CONTROL, 2);
        pnd_Ws_Pnd_Isa_Sub = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isa_Sub", "#ISA-SUB", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Newpage_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Newpage_Ind", "#NEWPAGE-IND", FieldType.BOOLEAN, 1);

        pnd_Ws_Pnd_Breaks = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Breaks", "#BREAKS");
        pnd_Ws_Pnd_Ledger_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Ledger_Break", "#LEDGER-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Company_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Company_Break", "#COMPANY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Company_Cde_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Company_Cde_Break", "#COMPANY-CDE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ins_Option_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Ins_Option_Break", "#INS-OPTION-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Life_Cont_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Life_Cont_Break", "#LIFE-CONT-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Annty_Type_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Annty_Type_Break", "#ANNTY-TYPE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Annty_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Annty_Break", "#ANNTY-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Check_Dte_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Check_Dte_Break", "#CHECK-DTE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Orgn_Break = pnd_Ws_Pnd_Breaks.newFieldInGroup("pnd_Ws_Pnd_Orgn_Break", "#ORGN-BREAK", FieldType.BOOLEAN, 1);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Cr_AmtSum169 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM_169", "Pnd_Cr_Amt_SUM_169", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Cr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM", "Pnd_Cr_Amt_SUM", FieldType.PACKED_DECIMAL, 13, 2);
        readWork01Pnd_Dr_AmtSum169 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Dr_Amt_SUM_169", "Pnd_Dr_Amt_SUM_169", FieldType.PACKED_DECIMAL, 
            13, 2);
        readWork01Pnd_Dr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Dr_Amt_SUM", "Pnd_Dr_Amt_SUM", FieldType.PACKED_DECIMAL, 13, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        internalLoopRecord.reset();
        ldaFcpl199a.initializeValues();
        ldaFcpl199b.initializeValues();
        ldaFcpl827.initializeValues();
        ldaFcpl826a.initializeValues();
        ldaFcpl826b.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Ledger_Break.setInitialValue(true);
        pnd_Ws_Pnd_Company_Break.setInitialValue(true);
        pnd_Ws_Pnd_Company_Cde_Break.setInitialValue(true);
        pnd_Ws_Pnd_Ins_Option_Break.setInitialValue(true);
        pnd_Ws_Pnd_Life_Cont_Break.setInitialValue(true);
        pnd_Ws_Pnd_Annty_Type_Break.setInitialValue(true);
        pnd_Ws_Pnd_Annty_Break.setInitialValue(true);
        pnd_Ws_Pnd_Check_Dte_Break.setInitialValue(true);
        pnd_Ws_Pnd_Orgn_Break.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp827() throws Exception
    {
        super("Fcpp827");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        //*                                                                                                                                                               //Natural: FORMAT ( 0 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 132 ZP = ON
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 56T 'LEDGER ACCOUNT REPORT' 120T 'REPORT: RPT1' / 49T #ORGN-CODE-DESC ( #ORGN-IDX ) / 58T 'DUE ON' PYMNT-CHECK-DTE ( EM = MM/DD/YYYY ) / 60T #ANNTY-DESC ( #ANNTY-IDX ) / 56T #ANNTY-TYPE-DESC ( #ANNTY-TYPE-IDX ) / 56T 'LIFE CONTINGENCY:' #LIFE-CONT-DESC ( #LIFE-CONT-IDX ) / 61T #INS-OPTION-DESC ( #INS-OPTION-IDX ) / 56T #COMPANY-CDE-DESC ( #COMPANY-IDX ) //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 59T 'CONTROL REPORT' 120T 'REPORT: RPT2' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #LGR-ACCT-RPT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl827.getPnd_Lgr_Acct_Rpt())))
        {
            CheckAtStartofData146();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: AT START OF DATA;//Natural: BEFORE BREAK PROCESSING
            pnd_Ws_Pnd_Rec_Cnt.nadd(1);                                                                                                                                   //Natural: ADD 1 TO #REC-CNT
            if (condition(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Ind().equals("C")))                                                                               //Natural: IF INV-ACCT-LEDGR-IND = 'C'
            {
                pnd_Ws_Pnd_Cr_Amt.getValue(1).setValue(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Amt());                                                              //Natural: ASSIGN #CR-AMT ( 1 ) := INV-ACCT-LEDGR-AMT
                pnd_Ws_Pnd_Cr_Cv.setValue("AD=D");                                                                                                                        //Natural: ASSIGN #CR-CV := ( AD = D )
                pnd_Ws_Pnd_Dr_Cv.setValue("AD=N");                                                                                                                        //Natural: ASSIGN #DR-CV := ( AD = N )
                pnd_Ws_Pnd_Dr_Amt.getValue(1).reset();                                                                                                                    //Natural: RESET #DR-AMT ( 1 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Dr_Amt.getValue(1).setValue(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Amt());                                                              //Natural: ASSIGN #DR-AMT ( 1 ) := INV-ACCT-LEDGR-AMT
                pnd_Ws_Pnd_Dr_Cv.setValue("AD=D");                                                                                                                        //Natural: ASSIGN #DR-CV := ( AD = D )
                pnd_Ws_Pnd_Cr_Cv.setValue("AD=N");                                                                                                                        //Natural: ASSIGN #CR-CV := ( AD = N )
                pnd_Ws_Pnd_Cr_Amt.getValue(1).reset();                                                                                                                    //Natural: RESET #CR-AMT ( 1 )
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*  RITA  AT BREAK OF #LGR-ACCT-RPT.INV-ACCT-CDE                                                                                                             //Natural: AT BREAK OF #LGR-ACCT-RPT.INV-ACCT-LEDGR-NBR
            //*  RITA    #FUND-BREAK                := TRUE
            //*  RITA  END-BREAK
                                                                                                                                                                          //Natural: AT BREAK OF ACCT-COMPANY;//Natural: AT BREAK OF CNTRCT-COMPANY-CDE;//Natural: AT BREAK OF CNTRCT-LIFE-CONTINGENCY;//Natural: AT BREAK OF CNTRCT-INSURANCE-OPTION;//Natural: AT BREAK OF CNTRCT-ANNTY-TYPE-CDE;//Natural: AT BREAK OF #LGR-ACCT-RPT.CNTRCT-ANNTY-INS-TYPE;//Natural: AT BREAK OF #LGR-ACCT-RPT.PYMNT-CHECK-DTE;//Natural: AT BREAK OF #LGR-ACCT-RPT.CNTRCT-ORGN-CDE;//Natural: AT END OF DATA;//Natural: PERFORM TOTAL-RTN
            sub_Total_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
            sub_Init_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new TabSetting(9),"/COMPANY",                                      //Natural: DISPLAY ( 1 ) ( HC = L ) 9T '/COMPANY' ACCT-COMPANY ( IS = ON ) 'LEDGER/NUMBER' #LGR-ACCT-RPT.INV-ACCT-LEDGR-NBR '/LEDGER DESCRIPTION' #FCPA121.INV-ACCT-LEDGR-DESC 'DEBIT/AMOUNT' #DR-AMT ( 1 ) ( HC = R IC = $ CV = #DR-CV ) 'CREDIT/AMOUNT' #CR-AMT ( 1 ) ( HC = R IC = $ CV = #CR-CV )
            		ldaFcpl827.getPnd_Lgr_Acct_Rpt_Acct_Company(), new IdenticalSuppress(true),"LEDGER/NUMBER",
            		ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr(),"/LEDGER DESCRIPTION",
            		pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc(),"DEBIT/AMOUNT",
            		pnd_Ws_Pnd_Dr_Amt.getValue(1), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new InsertionCharacter("$"), pnd_Ws_Pnd_Dr_Cv,
                "CREDIT/AMOUNT",
            		pnd_Ws_Pnd_Cr_Amt.getValue(1), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new InsertionCharacter("$"), pnd_Ws_Pnd_Cr_Cv);
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  RITA    '/ACCOUNT'            #INV-ACCT-DESC-8(#FUND-SUB) (IS=ON)
            readWork01Pnd_Cr_AmtSum169.nadd(readWork01Pnd_Cr_AmtSum169.getValue(1),pnd_Ws_Pnd_Cr_Amt.getValue(1));                                                        //Natural: END-WORK
            readWork01Pnd_Cr_AmtSum.nadd(readWork01Pnd_Cr_AmtSum,pnd_Ws_Pnd_Cr_Amt.getValue(1));
            readWork01Pnd_Dr_AmtSum169.nadd(readWork01Pnd_Dr_AmtSum169.getValue(1),pnd_Ws_Pnd_Dr_Amt.getValue(1));
            readWork01Pnd_Dr_AmtSum.nadd(readWork01Pnd_Dr_AmtSum,pnd_Ws_Pnd_Dr_Amt.getValue(1));
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM TOTAL-RTN
            sub_Total_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getReports().display(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(25),"RECORDS READ",                                    //Natural: DISPLAY ( 2 ) ( HC = R ) 25T 'RECORDS READ' #REC-CNT 'CREDIT TOTAL' #CR-AMT ( 4 ) 'DEBIT TOTAL' #DR-AMT ( 4 )
        		pnd_Ws_Pnd_Rec_Cnt,"CREDIT TOTAL",
        		pnd_Ws_Pnd_Cr_Amt.getValue(4),"DEBIT TOTAL",
        		pnd_Ws_Pnd_Dr_Amt.getValue(4));
        if (Global.isEscape()) return;
        //* *************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-RTN
        //*  RITA  WHEN #FUND-BREAK
        //*  RITA    #FUND-BREAK                    := FALSE
        //*  RITA    #FUND-SUB                      := #LGR-ACCT-RPT.INV-ACCT-CDE
        //* **************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-RTN
    }
    private void sub_Init_Rtn() throws Exception                                                                                                                          //Natural: INIT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************
        short decideConditionsMet215 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #LEDGER-BREAK
        if (condition(pnd_Ws_Pnd_Ledger_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Ledger_Break.setValue(false);                                                                                                                      //Natural: ASSIGN #LEDGER-BREAK := FALSE
            pnd_Ws_Pnd_Isa_Sub.setValue(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Cde());                                                                                   //Natural: ASSIGN #ISA-SUB := #LGR-ACCT-RPT.INV-ACCT-CDE
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Isa().setValue(ldaFcpl199b.getPnd_Isa_Lda_Pnd_Isa_Cde().getValue(pnd_Ws_Pnd_Isa_Sub.getInt() + 1));                        //Natural: ASSIGN #FCPA121.INV-ACCT-ISA := #ISA-LDA.#ISA-CDE ( #ISA-SUB )
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr());                                                 //Natural: ASSIGN #FCPA121.INV-ACCT-LEDGR-NBR := #LGR-ACCT-RPT.INV-ACCT-LEDGR-NBR
            DbsUtil.callnat(Fcpn121.class , getCurrentProcessState(), pdaFcpa121.getPnd_Fcpa121());                                                                       //Natural: CALLNAT 'FCPN121' USING #FCPA121
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: WHEN #COMPANY-BREAK
        if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Company_Break.setValue(false);                                                                                                                     //Natural: ASSIGN #COMPANY-BREAK := FALSE
            pnd_Ws_Pnd_Save_Company.setValue(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Acct_Company());                                                                              //Natural: ASSIGN #SAVE-COMPANY := ACCT-COMPANY
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL ( 1 )
        }                                                                                                                                                                 //Natural: WHEN #COMPANY-CDE-BREAK
        if (condition(pnd_Ws_Pnd_Company_Cde_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Company_Cde_Break.setValue(false);                                                                                                                 //Natural: ASSIGN #COMPANY-CDE-BREAK := FALSE
            DbsUtil.examine(new ExamineSource(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Company_Cde().getValue("*")), new ExamineSearch(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Company_Cde()),  //Natural: EXAMINE #FCPL826A.#COMPANY-CDE ( * ) FOR CNTRCT-COMPANY-CDE GIVING INDEX IN #COMPANY-IDX
                new ExamineGivingIndex(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Company_Idx()));
            if (condition(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Company_Idx().equals(getZero())))                                                                               //Natural: IF #COMPANY-IDX = 0
            {
                ldaFcpl826a.getPnd_Fcpl826a_Pnd_Company_Idx().setValue(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Company_Idx_1());                                                  //Natural: ASSIGN #COMPANY-IDX := #COMPANY-IDX-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN #LIFE-CONT-BREAK
        if (condition(pnd_Ws_Pnd_Life_Cont_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Life_Cont_Break.setValue(false);                                                                                                                   //Natural: ASSIGN #LIFE-CONT-BREAK := FALSE
            DbsUtil.examine(new ExamineSource(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Life_Cont().getValue("*")), new ExamineSearch(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Life_Contingency()),  //Natural: EXAMINE #LIFE-CONT ( * ) FOR CNTRCT-LIFE-CONTINGENCY GIVING INDEX IN #LIFE-CONT-IDX
                new ExamineGivingIndex(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Life_Cont_Idx()));
            if (condition(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Life_Cont_Idx().equals(getZero())))                                                                             //Natural: IF #LIFE-CONT-IDX = 0
            {
                ldaFcpl826a.getPnd_Fcpl826a_Pnd_Life_Cont_Idx().setValue(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Life_Cont_Idx_1());                                              //Natural: ASSIGN #LIFE-CONT-IDX := #LIFE-CONT-IDX-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN #INS-OPTION-BREAK
        if (condition(pnd_Ws_Pnd_Ins_Option_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Ins_Option_Break.setValue(false);                                                                                                                  //Natural: ASSIGN #INS-OPTION-BREAK := FALSE
            DbsUtil.examine(new ExamineSource(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Ins_Option().getValue("*"),true), new ExamineSearch(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_Option()),  //Natural: EXAMINE FULL #INS-OPTION ( * ) FOR CNTRCT-INSURANCE-OPTION GIVING INDEX IN #INS-OPTION-IDX
                new ExamineGivingIndex(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Ins_Option_Idx()));
            if (condition(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Ins_Option_Idx().equals(getZero())))                                                                            //Natural: IF #INS-OPTION-IDX = 0
            {
                ldaFcpl826a.getPnd_Fcpl826a_Pnd_Ins_Option_Idx().setValue(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Ins_Option_Idx_1());                                            //Natural: ASSIGN #INS-OPTION-IDX := #INS-OPTION-IDX-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN #ANNTY-TYPE-BREAK
        if (condition(pnd_Ws_Pnd_Annty_Type_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Annty_Type_Break.setValue(false);                                                                                                                  //Natural: ASSIGN #ANNTY-TYPE-BREAK := FALSE
            DbsUtil.examine(new ExamineSource(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Type().getValue("*")), new ExamineSearch(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_Cde()),  //Natural: EXAMINE #ANNTY-TYPE ( * ) FOR CNTRCT-ANNTY-TYPE-CDE GIVING INDEX IN #ANNTY-TYPE-IDX
                new ExamineGivingIndex(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Type_Idx()));
            if (condition(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Type_Idx().equals(getZero())))                                                                            //Natural: IF #ANNTY-TYPE-IDX = 0
            {
                ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Type_Idx().setValue(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Type_Idx_1());                                            //Natural: ASSIGN #ANNTY-TYPE-IDX := #ANNTY-TYPE-IDX-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN #ANNTY-BREAK
        if (condition(pnd_Ws_Pnd_Annty_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Annty_Break.setValue(false);                                                                                                                       //Natural: ASSIGN #ANNTY-BREAK := FALSE
            DbsUtil.examine(new ExamineSource(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty().getValue("*")), new ExamineSearch(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_Type()),  //Natural: EXAMINE #ANNTY ( * ) FOR #LGR-ACCT-RPT.CNTRCT-ANNTY-INS-TYPE GIVING INDEX IN #ANNTY-IDX
                new ExamineGivingIndex(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Idx()));
            if (condition(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Idx().equals(getZero())))                                                                                 //Natural: IF #ANNTY-IDX = 0
            {
                ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Idx().setValue(ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Idx_1());                                                      //Natural: ASSIGN #ANNTY-IDX := #ANNTY-IDX-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN #CHECK-DTE-BREAK
        if (condition(pnd_Ws_Pnd_Check_Dte_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Check_Dte_Break.setValue(false);                                                                                                                   //Natural: ASSIGN #CHECK-DTE-BREAK := FALSE
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN #ORGN-BREAK
        if (condition(pnd_Ws_Pnd_Orgn_Break.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Orgn_Break.setValue(false);                                                                                                                        //Natural: ASSIGN #ORGN-BREAK := FALSE
            DbsUtil.examine(new ExamineSource(ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Code().getValue("*")), new ExamineSearch(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_Cde()),  //Natural: EXAMINE #ORGN-CODE ( * ) FOR #LGR-ACCT-RPT.CNTRCT-ORGN-CDE GIVING INDEX IN #ORGN-IDX
                new ExamineGivingIndex(ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Idx()));
            if (condition(ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Idx().equals(getZero())))                                                                                  //Natural: IF #ORGN-IDX = 0
            {
                ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Idx().setValue(ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Idx_1());                                                        //Natural: ASSIGN #ORGN-IDX := #ORGN-IDX-1
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Newpage_Ind.setValue(true);                                                                                                                        //Natural: ASSIGN #NEWPAGE-IND := TRUE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet215 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Newpage_Ind.getBoolean()))                                                                                                               //Natural: IF #NEWPAGE-IND
        {
            pnd_Ws_Pnd_Newpage_Ind.setValue(false);                                                                                                                       //Natural: ASSIGN #NEWPAGE-IND := FALSE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Total_Rtn() throws Exception                                                                                                                         //Natural: TOTAL-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* **************************
        short decideConditionsMet281 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #COMPANY-BREAK
        if (condition(pnd_Ws_Pnd_Company_Break.getBoolean()))
        {
            decideConditionsMet281++;
            getReports().write(1, new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),"-------------------",new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),"-------------------",NEWLINE,new   //Natural: WRITE ( 1 ) T*#DR-AMT '-------------------' T*#CR-AMT '-------------------' / T*ACCT-COMPANY 'SUBTOTAL COMPANY' #SAVE-COMPANY T*#DR-AMT 2X #DR-AMT ( 2 ) T*#CR-AMT 2X #CR-AMT ( 2 )
                ReportTAsterisk(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Acct_Company()),"SUBTOTAL COMPANY",pnd_Ws_Pnd_Save_Company,new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new 
                ColumnSpacing(2),pnd_Ws_Pnd_Dr_Amt.getValue(2), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new ColumnSpacing(2),pnd_Ws_Pnd_Cr_Amt.getValue(2), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Dr_Amt.getValue(3).nadd(pnd_Ws_Pnd_Dr_Amt.getValue(2));                                                                                            //Natural: ADD #DR-AMT ( 2 ) TO #DR-AMT ( 3 )
            pnd_Ws_Pnd_Cr_Amt.getValue(3).nadd(pnd_Ws_Pnd_Cr_Amt.getValue(2));                                                                                            //Natural: ADD #CR-AMT ( 2 ) TO #CR-AMT ( 3 )
            pnd_Ws_Pnd_Dr_Amt.getValue(2).reset();                                                                                                                        //Natural: RESET #DR-AMT ( 2 ) #CR-AMT ( 2 )
            pnd_Ws_Pnd_Cr_Amt.getValue(2).reset();
        }                                                                                                                                                                 //Natural: WHEN #COMPANY-CDE-BREAK OR #LIFE-CONT-BREAK OR #INS-OPTION-BREAK OR #ANNTY-TYPE-BREAK OR #ANNTY-BREAK OR #CHECK-DTE-BREAK OR #ORGN-BREAK
        if (condition(pnd_Ws_Pnd_Company_Cde_Break.getBoolean() || pnd_Ws_Pnd_Life_Cont_Break.getBoolean() || pnd_Ws_Pnd_Ins_Option_Break.getBoolean() 
            || pnd_Ws_Pnd_Annty_Type_Break.getBoolean() || pnd_Ws_Pnd_Annty_Break.getBoolean() || pnd_Ws_Pnd_Check_Dte_Break.getBoolean() || pnd_Ws_Pnd_Orgn_Break.getBoolean()))
        {
            decideConditionsMet281++;
            getReports().write(1, NEWLINE,NEWLINE,new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),"===================",new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),"===================",NEWLINE,new  //Natural: WRITE ( 1 ) // T*#DR-AMT '===================' T*#CR-AMT '===================' / T*ACCT-COMPANY 'TOTAL' T*#DR-AMT 2X #DR-AMT ( 3 ) T*#CR-AMT 2X #CR-AMT ( 3 )
                ReportTAsterisk(ldaFcpl827.getPnd_Lgr_Acct_Rpt_Acct_Company()),"TOTAL",new ReportTAsterisk(pnd_Ws_Pnd_Dr_Amt),new ColumnSpacing(2),pnd_Ws_Pnd_Dr_Amt.getValue(3), 
                new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"),new ReportTAsterisk(pnd_Ws_Pnd_Cr_Amt),new ColumnSpacing(2),pnd_Ws_Pnd_Cr_Amt.getValue(3), new 
                ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Dr_Amt.getValue(4).nadd(pnd_Ws_Pnd_Dr_Amt.getValue(3));                                                                                            //Natural: ADD #DR-AMT ( 3 ) TO #DR-AMT ( 4 )
            pnd_Ws_Pnd_Cr_Amt.getValue(4).nadd(pnd_Ws_Pnd_Cr_Amt.getValue(3));                                                                                            //Natural: ADD #CR-AMT ( 3 ) TO #CR-AMT ( 4 )
            pnd_Ws_Pnd_Dr_Amt.getValue(3).reset();                                                                                                                        //Natural: RESET #DR-AMT ( 3 ) #CR-AMT ( 3 )
            pnd_Ws_Pnd_Cr_Amt.getValue(3).reset();
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet281 > 0))
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet281 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }

    //

    // Support Methods

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_NbrIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr().isBreak(endOfData);
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Acct_CompanyIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Acct_Company().isBreak(endOfData);
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Company_CdeIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Company_Cde().isBreak(endOfData);
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Life_ContingencyIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Life_Contingency().isBreak(endOfData);
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_OptionIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_Option().isBreak(endOfData);
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_CdeIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_Cde().isBreak(endOfData);
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_TypeIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_Type().isBreak(endOfData);
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Pymnt_Check_Dte().isBreak(endOfData);
        boolean ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak = ldaFcpl827.getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_Cde().isBreak(endOfData);
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_NbrIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Acct_CompanyIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Company_CdeIsBreak 
            || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Life_ContingencyIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_OptionIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_CdeIsBreak 
            || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_TypeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Ledger_Break.setValue(true);                                                                                                                       //Natural: ASSIGN #LEDGER-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Acct_CompanyIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Company_CdeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Life_ContingencyIsBreak 
            || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_OptionIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_CdeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_TypeIsBreak 
            || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Company_Break.setValue(true);                                                                                                                      //Natural: ASSIGN #COMPANY-BREAK := TRUE
            pnd_Ws_Pnd_Cr_Amt.getValue(2).setValue(readWork01Pnd_Cr_AmtSum169.getValue(1));                                                                               //Natural: ASSIGN #CR-AMT ( 2 ) := SUM ( #CR-AMT ( 1 ) )
            pnd_Ws_Pnd_Dr_Amt.getValue(2).setValue(readWork01Pnd_Dr_AmtSum169.getValue(1));                                                                               //Natural: ASSIGN #DR-AMT ( 2 ) := SUM ( #DR-AMT ( 1 ) )
            readWork01Pnd_Cr_AmtSum169.getValue("*").setDec(new DbsDecimal(0));                                                                                           //Natural: END-BREAK
            readWork01Pnd_Dr_AmtSum169.getValue("*").setDec(new DbsDecimal(0));
        }
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Company_CdeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Life_ContingencyIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_OptionIsBreak 
            || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_CdeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_TypeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak 
            || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Company_Cde_Break.setValue(true);                                                                                                                  //Natural: ASSIGN #COMPANY-CDE-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Life_ContingencyIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_OptionIsBreak || 
            ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_CdeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_TypeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak 
            || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Life_Cont_Break.setValue(true);                                                                                                                    //Natural: ASSIGN #LIFE-CONT-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Insurance_OptionIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_CdeIsBreak || 
            ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_TypeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Ins_Option_Break.setValue(true);                                                                                                                   //Natural: ASSIGN #INS-OPTION-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Type_CdeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_TypeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak 
            || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Annty_Type_Break.setValue(true);                                                                                                                   //Natural: ASSIGN #ANNTY-TYPE-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Annty_Ins_TypeIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Annty_Break.setValue(true);                                                                                                                        //Natural: ASSIGN #ANNTY-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Pymnt_Check_DteIsBreak || ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Check_Dte_Break.setValue(true);                                                                                                                    //Natural: ASSIGN #CHECK-DTE-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl827_getPnd_Lgr_Acct_Rpt_Cntrct_Orgn_CdeIsBreak))
        {
            pnd_Ws_Pnd_Orgn_Break.setValue(true);                                                                                                                         //Natural: ASSIGN #ORGN-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=132 ZP=ON");
        Global.format(1, "PS=58 LS=132 ZP=ON");
        Global.format(2, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(56),"LEDGER ACCOUNT REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,new TabSetting(49),ldaFcpl826b.getPnd_Fcpl826b_Pnd_Orgn_Code_Desc(),NEWLINE,new 
            TabSetting(58),"DUE ON",ldaFcpl827.getPnd_Lgr_Acct_Rpt_Pymnt_Check_Dte(), new ReportEditMask ("MM/DD/YYYY"),NEWLINE,new TabSetting(60),ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Desc(),NEWLINE,new 
            TabSetting(56),ldaFcpl826a.getPnd_Fcpl826a_Pnd_Annty_Type_Desc(),NEWLINE,new TabSetting(56),"LIFE CONTINGENCY:",ldaFcpl826a.getPnd_Fcpl826a_Pnd_Life_Cont_Desc(),NEWLINE,new 
            TabSetting(61),ldaFcpl826a.getPnd_Fcpl826a_Pnd_Ins_Option_Desc(),NEWLINE,new TabSetting(56),ldaFcpl826a.getPnd_Fcpl826a_Pnd_Company_Cde_Desc(),
            NEWLINE,NEWLINE);
        getReports().write(2, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(59),"CONTROL REPORT",new TabSetting(120),"REPORT: RPT2",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new TabSetting(9),"/COMPANY",
        		ldaFcpl827.getPnd_Lgr_Acct_Rpt_Acct_Company(), new IdenticalSuppress(true),"LEDGER/NUMBER",
        		ldaFcpl827.getPnd_Lgr_Acct_Rpt_Inv_Acct_Ledgr_Nbr(),"/LEDGER DESCRIPTION",
        		pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc(),"DEBIT/AMOUNT",
        		pnd_Ws_Pnd_Dr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new InsertionCharacter("$"), pnd_Ws_Pnd_Dr_Cv,"CREDIT/AMOUNT",
            
        		pnd_Ws_Pnd_Cr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right), new InsertionCharacter("$"), pnd_Ws_Pnd_Cr_Cv);
        getReports().setDisplayColumns(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(25),"RECORDS READ",
        		pnd_Ws_Pnd_Rec_Cnt,"CREDIT TOTAL",
        		pnd_Ws_Pnd_Cr_Amt,"DEBIT TOTAL",
        		pnd_Ws_Pnd_Dr_Amt);
    }
    private void CheckAtStartofData146() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
            sub_Init_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
