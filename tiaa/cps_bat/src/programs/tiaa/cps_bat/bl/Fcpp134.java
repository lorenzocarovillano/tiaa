/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:15:17 PM
**        * FROM NATURAL PROGRAM : Fcpp134
************************************************************
**        * FILE NAME            : Fcpp134.java
**        * CLASS NAME           : Fcpp134
**        * INSTANCE NAME        : Fcpp134
************************************************************
************************************************************************
* PROGRAM  : FCPP134
* SSYSTEM  : CPS - CONSOLIDATED PAYMENTS SYSTEM
* TITLE    : CPS DETAIL LEDGER REPORT FOR MONTHLY AP PAYMENTS
* CREATED  : 12/11/2001
* FUNCTION : READS A LEDGER FILE AND PRODUCES A MONTHLY LEDGER REPORT.
* PROGRAMMER    START DATE   DESC OF CHANGES
* ------------  ----------   ------------------------------------------
* LANDRUM       04/12/2006   ADD PIN TO DETAIL REPORT LINE. SEE "/* RL"
* SAI K         05/09/2017    RECOMPILE FOR FCPL121
***********************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp134 extends BLNatBase
{
    // Data Areas
    private PdaFcpa121 pdaFcpa121;
    private LdaFcpl121 ldaFcpl121;

    // Local Variables
    public DbsRecord localVariables;

    private DataAccessProgramView vw_fcp_Cons_Pymnt;
    private DbsField fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr;
    private DbsField fcp_Cons_Pymnt_Cntrct_Payee_Cde;
    private DbsField fcp_Cons_Pymnt_Cntrct_Invrse_Dte;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Contract;
    private DbsField pnd_Ws_Pnd_Ws_Inv_Acct_Isa;
    private DbsField pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr;
    private DbsField pnd_Ws_Pnd_Ws_Ledger_Desc;
    private DbsField pnd_Ws_Pnd_Ws_Header_Isa;
    private DbsField pnd_Ws_Pnd_Ws_Total_Isa;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Check_Dte;
    private DbsField pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte;
    private DbsField pnd_Ws_Pnd_Records_Read;
    private DbsField pnd_Ws_Pnd_Dr_Amt;
    private DbsField pnd_Ws_Pnd_Cr_Amt;

    private DbsGroup pnd_Ws_Pnd_Tot_Array;
    private DbsField pnd_Ws_Pnd_Tot_Dr_Amt;
    private DbsField pnd_Ws_Pnd_Tot_Cr_Amt;
    private DbsField pnd_Ws_Pnd_Settl_Dte_Ind;
    private DbsField pnd_Ws_Pnd_Check_Dte_Ind;
    private DbsField pnd_Ws_Pnd_Ledger_Ind;
    private DbsField pnd_Ws_Pnd_Acctg_Dte_Ind;
    private DbsField pnd_Ws_Pnd_Isa_Ind;
    private DbsField pnd_Ws_Pnd_Csr_Ind;
    private DbsField pnd_Ws_Pnd_Orgn_Cde_Ind;
    private DbsField pnd_Ws_Pnd_Newpage_Ind_Det;
    private DbsField pnd_Ws_Pnd_Newpage_Ind_Sum;
    private DbsField pnd_Pymnt_Intrfce_Dte;
    private DbsField pnd_Pymnt_Intrfce_Dt_Num;

    private DbsGroup pnd_Pymnt_Intrfce_Dt_Num__R_Field_1;
    private DbsField pnd_Pymnt_Intrfce_Dt_Num_Pnd_Pymnt_Intrfce_Dt_Alp;

    private DbsRecord internalLoopRecord;
    private DbsField readWork01Pnd_Cr_AmtSum130;
    private DbsField readWork01Pnd_Cr_AmtSum;
    private DbsField readWork01Pnd_Dr_AmtSum130;
    private DbsField readWork01Pnd_Dr_AmtSum;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa121 = new PdaFcpa121(localVariables);
        ldaFcpl121 = new LdaFcpl121();
        registerRecord(ldaFcpl121);

        // Local Variables

        vw_fcp_Cons_Pymnt = new DataAccessProgramView(new NameInfo("vw_fcp_Cons_Pymnt", "FCP-CONS-PYMNT"), "FCP_CONS_PYMNT", "FCP_CONS_PYMT");
        fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 
            12, RepeatingFieldStrategy.None, "CNTRCT_UNQ_ID_NBR");
        fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 
            10, RepeatingFieldStrategy.None, "CNTRCT_PPCN_NBR");
        fcp_Cons_Pymnt_Cntrct_Payee_Cde = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 
            4, RepeatingFieldStrategy.None, "CNTRCT_PAYEE_CDE");
        fcp_Cons_Pymnt_Cntrct_Invrse_Dte = vw_fcp_Cons_Pymnt.getRecord().newFieldInGroup("fcp_Cons_Pymnt_Cntrct_Invrse_Dte", "CNTRCT-INVRSE-DTE", FieldType.NUMERIC, 
            8, RepeatingFieldStrategy.None, "CNTRCT_INVRSE_DTE");
        registerRecord(vw_fcp_Cons_Pymnt);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Contract = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Contract", "#CONTRACT", FieldType.STRING, 10);
        pnd_Ws_Pnd_Ws_Inv_Acct_Isa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Inv_Acct_Isa", "#WS-INV-ACCT-ISA", FieldType.STRING, 5);
        pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr", "#WS-INV-ACCT-LEDGR-NBR", FieldType.STRING, 15);
        pnd_Ws_Pnd_Ws_Ledger_Desc = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Ledger_Desc", "#WS-LEDGER-DESC", FieldType.STRING, 40);
        pnd_Ws_Pnd_Ws_Header_Isa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Header_Isa", "#WS-HEADER-ISA", FieldType.STRING, 10);
        pnd_Ws_Pnd_Ws_Total_Isa = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Total_Isa", "#WS-TOTAL-ISA", FieldType.STRING, 5);
        pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte", "#WS-PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte", "#WS-PYMNT-SETTLMNT-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Ws_Pymnt_Check_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Check_Dte", "#WS-PYMNT-CHECK-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte", "#WS-PYMNT-ACCTG-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Records_Read = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Records_Read", "#RECORDS-READ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Dr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dr_Amt", "#DR-AMT", FieldType.PACKED_DECIMAL, 11, 2);
        pnd_Ws_Pnd_Cr_Amt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cr_Amt", "#CR-AMT", FieldType.PACKED_DECIMAL, 11, 2);

        pnd_Ws_Pnd_Tot_Array = pnd_Ws.newGroupArrayInGroup("pnd_Ws_Pnd_Tot_Array", "#TOT-ARRAY", new DbsArrayController(1, 6));
        pnd_Ws_Pnd_Tot_Dr_Amt = pnd_Ws_Pnd_Tot_Array.newFieldInGroup("pnd_Ws_Pnd_Tot_Dr_Amt", "#TOT-DR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Tot_Cr_Amt = pnd_Ws_Pnd_Tot_Array.newFieldInGroup("pnd_Ws_Pnd_Tot_Cr_Amt", "#TOT-CR-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ws_Pnd_Settl_Dte_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Settl_Dte_Ind", "#SETTL-DTE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Check_Dte_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Check_Dte_Ind", "#CHECK-DTE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Ledger_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Ledger_Ind", "#LEDGER-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Acctg_Dte_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Acctg_Dte_Ind", "#ACCTG-DTE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Isa_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Isa_Ind", "#ISA-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Csr_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Csr_Ind", "#CSR-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Orgn_Cde_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Orgn_Cde_Ind", "#ORGN-CDE-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Newpage_Ind_Det = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Newpage_Ind_Det", "#NEWPAGE-IND-DET", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Newpage_Ind_Sum = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Newpage_Ind_Sum", "#NEWPAGE-IND-SUM", FieldType.BOOLEAN, 1);
        pnd_Pymnt_Intrfce_Dte = localVariables.newFieldInRecord("pnd_Pymnt_Intrfce_Dte", "#PYMNT-INTRFCE-DTE", FieldType.DATE);
        pnd_Pymnt_Intrfce_Dt_Num = localVariables.newFieldInRecord("pnd_Pymnt_Intrfce_Dt_Num", "#PYMNT-INTRFCE-DT-NUM", FieldType.NUMERIC, 8);

        pnd_Pymnt_Intrfce_Dt_Num__R_Field_1 = localVariables.newGroupInRecord("pnd_Pymnt_Intrfce_Dt_Num__R_Field_1", "REDEFINE", pnd_Pymnt_Intrfce_Dt_Num);
        pnd_Pymnt_Intrfce_Dt_Num_Pnd_Pymnt_Intrfce_Dt_Alp = pnd_Pymnt_Intrfce_Dt_Num__R_Field_1.newFieldInGroup("pnd_Pymnt_Intrfce_Dt_Num_Pnd_Pymnt_Intrfce_Dt_Alp", 
            "#PYMNT-INTRFCE-DT-ALP", FieldType.STRING, 8);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);

        internalLoopRecord = new DbsRecord();
        readWork01Pnd_Cr_AmtSum130 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM_130", "Pnd_Cr_Amt_SUM_130", FieldType.PACKED_DECIMAL, 
            11, 2);
        readWork01Pnd_Cr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Cr_Amt_SUM", "Pnd_Cr_Amt_SUM", FieldType.PACKED_DECIMAL, 11, 2);
        readWork01Pnd_Dr_AmtSum130 = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Dr_Amt_SUM_130", "Pnd_Dr_Amt_SUM_130", FieldType.PACKED_DECIMAL, 
            11, 2);
        readWork01Pnd_Dr_AmtSum = internalLoopRecord.newFieldInRecord("ReadWork01_Pnd_Dr_Amt_SUM", "Pnd_Dr_Amt_SUM", FieldType.PACKED_DECIMAL, 11, 2);
        registerRecord(internalLoopRecord);
    }

    @Override
    public void initializeValues() throws Exception
    {
        vw_fcp_Cons_Pymnt.reset();
        internalLoopRecord.reset();

        ldaFcpl121.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_Ws_Header_Isa.setInitialValue("ISA: ");
        pnd_Ws_Pnd_Settl_Dte_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Check_Dte_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Ledger_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Acctg_Dte_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Isa_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Csr_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Orgn_Cde_Ind.setInitialValue(true);
        pnd_Ws_Pnd_Newpage_Ind_Det.setInitialValue(false);
        pnd_Ws_Pnd_Newpage_Ind_Sum.setInitialValue(false);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp134() throws Exception
    {
        super("Fcpp134");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //* *--------
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*                                                                                                                                                               //Natural: FORMAT ( 00 ) PS = 60 LS = 133 ZP = ON;//Natural: FORMAT ( 01 ) PS = 60 LS = 133 ZP = ON
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: ASSIGN *ERROR-TA := 'INFP9000'
        //* ***************                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  MAIN PROGRAM *
        //* ***************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 01 RECORD #LEDGER-EXTRACT
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, ldaFcpl121.getPnd_Ledger_Extract())))
        {
            CheckAtStartofData124();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            pnd_Pymnt_Intrfce_Dte.setValue(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte());                                                                         //Natural: MOVE PYMNT-INTRFCE-DTE TO #PYMNT-INTRFCE-DTE
            //*  RL
                                                                                                                                                                          //Natural: PERFORM READ-PAYMENT-FILE
            sub_Read_Payment_File();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //*                                                                                                                                                           //Natural: AT BREAK OF PYMNT-SETTLMNT-DTE
            //*                                                                                                                                                           //Natural: AT BREAK OF PYMNT-CHECK-DTE;//Natural: AT BREAK OF #LEDGER-EXTRACT.INV-ACCT-LEDGR-NBR;//Natural: AT BREAK OF #LEDGER-EXTRACT.INV-ACCT-ISA;//Natural: AT BREAK OF PYMNT-ACCTG-DTE
            //*                                                                                                                                                           //Natural: AT END OF DATA
            //*  REPORT TOTALS
                                                                                                                                                                          //Natural: PERFORM TOTAL-RTN
            sub_Total_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  INITIALIZATION
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
            sub_Init_Rtn();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            pnd_Ws_Pnd_Records_Read.nadd(1);                                                                                                                              //Natural: ADD 1 TO #RECORDS-READ
            if (condition((! ((ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde().equals("01") || ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde().equals("09")))             //Natural: IF NOT ( #LEDGER-EXTRACT.INV-ACCT-CDE = '01' OR = '09' ) AND CNTRCT-CREF-NBR NE ' '
                && ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cref_Nbr().notEquals(" "))))
            {
                pnd_Ws_Pnd_Contract.setValue(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Cref_Nbr());                                                                         //Natural: MOVE CNTRCT-CREF-NBR TO #CONTRACT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //* *   MOVE  CNTRCT-PPCN-NBR  TO  #CONTRACT
                //*  RL
                pnd_Ws_Pnd_Contract.setValue(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr());                                                                         //Natural: MOVE #LEDGER-EXTRACT.CNTRCT-PPCN-NBR TO #CONTRACT
            }                                                                                                                                                             //Natural: END-IF
            if (condition(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Ind().equals("C")))                                                                             //Natural: IF INV-ACCT-LEDGR-IND = 'C'
            {
                pnd_Ws_Pnd_Cr_Amt.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Amt());                                                                        //Natural: MOVE INV-ACCT-LEDGR-AMT TO #CR-AMT
                pnd_Ws_Pnd_Dr_Amt.reset();                                                                                                                                //Natural: RESET #DR-AMT
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pnd_Ws_Pnd_Dr_Amt.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Amt());                                                                        //Natural: MOVE INV-ACCT-LEDGR-AMT TO #DR-AMT
                pnd_Ws_Pnd_Cr_Amt.reset();                                                                                                                                //Natural: RESET #CR-AMT
            }                                                                                                                                                             //Natural: END-IF
            //*  RL
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Settlement/Effective/Date",                                       //Natural: DISPLAY ( 1 ) ( HC = L ) 'Settlement/Effective/Date' PYMNT-SETTLMNT-DTE '/Payment/Date' PYMNT-CHECK-DTE '//Annuitant Name   ' PH-LAST-NAME '/           ' PH-FIRST-NAME '/ ' PH-MIDDLE-NAME ( AL = 1 ) '/Contract/Number' #CONTRACT 1X '/PIN' #LEDGER-EXTRACT.CNTRCT-UNQ-ID-NBR 1X '/DEBIT/AMOUNT' #DR-AMT ( HC = R ) 2X '/CREDIT/AMOUNT' #CR-AMT ( HC = R )
            		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte(),"/Payment/Date",
            		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte(),"//Annuitant Name   ",
            		ldaFcpl121.getPnd_Ledger_Extract_Ph_Last_Name(),"/           ",
            		ldaFcpl121.getPnd_Ledger_Extract_Ph_First_Name(),"/ ",
            		ldaFcpl121.getPnd_Ledger_Extract_Ph_Middle_Name(), new AlphanumericLength (1),"/Contract/Number",
            		pnd_Ws_Pnd_Contract,new ColumnSpacing(1),"/PIN",
            		ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Unq_Id_Nbr(),new ColumnSpacing(1),"/DEBIT/AMOUNT",
            		pnd_Ws_Pnd_Dr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ColumnSpacing(2),"/CREDIT/AMOUNT",
            		pnd_Ws_Pnd_Cr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //* *  '       '
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue("*").nadd(pnd_Ws_Pnd_Cr_Amt);                                                                                                  //Natural: ADD #CR-AMT TO #TOT-CR-AMT ( * )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue("*").nadd(pnd_Ws_Pnd_Dr_Amt);                                                                                                  //Natural: ADD #DR-AMT TO #TOT-DR-AMT ( * )
            readWork01Pnd_Cr_AmtSum130.nadd(readWork01Pnd_Cr_AmtSum130,pnd_Ws_Pnd_Cr_Amt);                                                                                //Natural: END-WORK
            readWork01Pnd_Cr_AmtSum.nadd(readWork01Pnd_Cr_AmtSum,pnd_Ws_Pnd_Cr_Amt);
            readWork01Pnd_Dr_AmtSum130.nadd(readWork01Pnd_Dr_AmtSum130,pnd_Ws_Pnd_Dr_Amt);
            readWork01Pnd_Dr_AmtSum.nadd(readWork01Pnd_Dr_AmtSum,pnd_Ws_Pnd_Dr_Amt);
        }
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
            //*  REPORT TOTALS
                                                                                                                                                                          //Natural: PERFORM TOTAL-RTN
            sub_Total_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
        getReports().eject(1, true);                                                                                                                                      //Natural: EJECT ( 1 )
        getReports().skip(1, 7);                                                                                                                                          //Natural: SKIP ( 1 ) 7 LINES
        getReports().write(1, new FieldAttributes ("AD=I"),ReportOption.NOTITLE,new TabSetting(7),"Input Records          :", new FieldAttributes ("AD=I"),pnd_Ws_Pnd_Records_Read,  //Natural: WRITE ( 1 ) ( AD = I ) 7T 'Input Records          :' ( AD = I ) #RECORDS-READ ///
            new ReportEditMask ("Z,ZZZ,ZZ9"),NEWLINE,NEWLINE,NEWLINE);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE,new  //Natural: WRITE ( 1 ) 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' / 7T '************ E N D   O F   T H E   R E P O R T   ************' /
            TabSetting(7),"************ E N D   O F   T H E   R E P O R T   ************",NEWLINE);
        if (Global.isEscape()) return;
        //*  - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-RTN
        //* *------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: TOTAL-RTN
        //*    WRITE (1) 06T #WS-HEADER-CSR
        //* *------------
        //* ********************* RL BEGIN APRIL 12, 2006 **********************
        //*  --------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-PAYMENT-FILE
        //* ********************* RL END APRIL 12, 2006 **********************
    }
    private void sub_Init_Rtn() throws Exception                                                                                                                          //Natural: INIT-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *-------------------------
        short decideConditionsMet215 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #SETTL-DTE-IND
        if (condition(pnd_Ws_Pnd_Settl_Dte_Ind.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Settl_Dte_Ind.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #SETTL-DTE-IND
            pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte.setValue(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte());                                                             //Natural: MOVE PYMNT-SETTLMNT-DTE TO #WS-PYMNT-SETTLMNT-DTE
        }                                                                                                                                                                 //Natural: WHEN #CHECK-DTE-IND
        if (condition(pnd_Ws_Pnd_Check_Dte_Ind.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Check_Dte_Ind.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #CHECK-DTE-IND
            pnd_Ws_Pnd_Ws_Pymnt_Check_Dte.setValue(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte());                                                                   //Natural: MOVE PYMNT-CHECK-DTE TO #WS-PYMNT-CHECK-DTE
        }                                                                                                                                                                 //Natural: WHEN #LEDGER-IND
        if (condition(pnd_Ws_Pnd_Ledger_Ind.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Ledger_Ind.setValue(false);                                                                                                                        //Natural: MOVE FALSE TO #LEDGER-IND
            Global.format("IS=OFF");                                                                                                                                      //Natural: SUSPEND IDENTICAL SUPPRESS ( 2 )
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NEWPAGE-IND-DET
            pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr());                                                             //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-LEDGR-NBR TO #WS-INV-ACCT-LEDGR-NBR #FCPA121.INV-ACCT-LEDGR-NBR
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Nbr().setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr());
            pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Isa().setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa());                                                           //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-ISA TO #FCPA121.INV-ACCT-ISA
            DbsUtil.callnat(Fcpn121.class , getCurrentProcessState(), pdaFcpa121.getPnd_Fcpa121());                                                                       //Natural: CALLNAT 'FCPN121' USING #FCPA121
            if (condition(Global.isEscape())) return;
            pnd_Ws_Pnd_Ws_Ledger_Desc.setValue(pdaFcpa121.getPnd_Fcpa121_Inv_Acct_Ledgr_Desc());                                                                          //Natural: MOVE #FCPA121.INV-ACCT-LEDGR-DESC TO #WS-LEDGER-DESC
        }                                                                                                                                                                 //Natural: WHEN #ISA-IND
        if (condition(pnd_Ws_Pnd_Isa_Ind.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Isa_Ind.setValue(false);                                                                                                                           //Natural: MOVE FALSE TO #ISA-IND
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NEWPAGE-IND-DET #NEWPAGE-IND-SUM
            pnd_Ws_Pnd_Newpage_Ind_Sum.setValue(true);
            pnd_Ws_Pnd_Ws_Inv_Acct_Isa.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Cde());                                                                         //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-CDE TO #WS-INV-ACCT-ISA
            setValueToSubstring(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa(),pnd_Ws_Pnd_Ws_Header_Isa,6,5);                                                            //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-ISA TO SUBSTRING ( #WS-HEADER-ISA,6,5 )
            pnd_Ws_Pnd_Ws_Total_Isa.setValue(ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa());                                                                            //Natural: MOVE #LEDGER-EXTRACT.INV-ACCT-ISA TO #WS-TOTAL-ISA
        }                                                                                                                                                                 //Natural: WHEN #ACCTG-DTE-IND
        if (condition(pnd_Ws_Pnd_Acctg_Dte_Ind.getBoolean()))
        {
            decideConditionsMet215++;
            pnd_Ws_Pnd_Acctg_Dte_Ind.setValue(false);                                                                                                                     //Natural: MOVE FALSE TO #ACCTG-DTE-IND
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(true);                                                                                                                    //Natural: MOVE TRUE TO #NEWPAGE-IND-DET #NEWPAGE-IND-SUM
            pnd_Ws_Pnd_Newpage_Ind_Sum.setValue(true);
            pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte.setValue(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Acctg_Dte());                                                                   //Natural: MOVE PYMNT-ACCTG-DTE TO #WS-PYMNT-ACCTG-DTE
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet215 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        if (condition(pnd_Ws_Pnd_Newpage_Ind_Det.getBoolean()))                                                                                                           //Natural: IF #NEWPAGE-IND-DET
        {
            pnd_Ws_Pnd_Newpage_Ind_Det.setValue(false);                                                                                                                   //Natural: ASSIGN #NEWPAGE-IND-DET := FALSE
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 01 )
            if (condition(Global.isEscape())){return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Total_Rtn() throws Exception                                                                                                                         //Natural: TOTAL-RTN
    {
        if (BLNatReinput.isReinput()) return;

        //* *--------------------------
        short decideConditionsMet254 = 0;                                                                                                                                 //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #SETTL-DTE-IND
        if (condition(pnd_Ws_Pnd_Settl_Dte_Ind.getBoolean()))
        {
            decideConditionsMet254++;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Sub-Total for Settlement Effective Date of",pnd_Ws_Pnd_Ws_Pymnt_Settlmnt_Dte, new ReportEditMask          //Natural: WRITE ( 1 ) / 'Sub-Total for Settlement Effective Date of' #WS-PYMNT-SETTLMNT-DTE 72T #TOT-DR-AMT ( 1 ) 89T #TOT-CR-AMT ( 1 )
                ("MM/DD/YY"),new TabSetting(72),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1), new ReportEditMask ("-ZZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1), 
                new ReportEditMask ("-ZZZZ,ZZZ,ZZ9.99"));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 1 ) #TOT-CR-AMT ( 1 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1).reset();
        }                                                                                                                                                                 //Natural: WHEN #CHECK-DTE-IND
        if (condition(pnd_Ws_Pnd_Check_Dte_Ind.getBoolean()))
        {
            decideConditionsMet254++;
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(2).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 2 ) #TOT-CR-AMT ( 2 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(2).reset();
        }                                                                                                                                                                 //Natural: WHEN #LEDGER-IND
        if (condition(pnd_Ws_Pnd_Ledger_Ind.getBoolean()))
        {
            decideConditionsMet254++;
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,"Total for Ledger Account Number",pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr,new TabSetting(72),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(3),  //Natural: WRITE ( 1 ) / 'Total for Ledger Account Number' #WS-INV-ACCT-LEDGR-NBR 72T #TOT-DR-AMT ( 3 ) 89T #TOT-CR-AMT ( 3 ) //
                new ReportEditMask ("-ZZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(3), new ReportEditMask ("-ZZZZ,ZZZ,ZZ9.99"),NEWLINE,
                NEWLINE);
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(3).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 3 ) #TOT-CR-AMT ( 3 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(3).reset();
        }                                                                                                                                                                 //Natural: WHEN #ISA-IND
        if (condition(pnd_Ws_Pnd_Isa_Ind.getBoolean()))
        {
            decideConditionsMet254++;
            getReports().write(1, ReportOption.NOTITLE,"Total for Acctg Date",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte, new ReportEditMask ("MM/DD/YY"),"for ISA:",pnd_Ws_Pnd_Ws_Total_Isa,new  //Natural: WRITE ( 1 ) 'Total for Acctg Date' #WS-PYMNT-ACCTG-DTE 'for ISA:' #WS-TOTAL-ISA 72T #TOT-DR-AMT ( 4 ) 89T #TOT-CR-AMT ( 4 ) //
                TabSetting(72),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(4), new ReportEditMask ("-ZZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(4), 
                new ReportEditMask ("-ZZZZ,ZZZ,ZZ9.99"),NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(4).reset();                                                                                                                    //Natural: RESET #TOT-DR-AMT ( 4 ) #TOT-CR-AMT ( 4 )
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(4).reset();
        }                                                                                                                                                                 //Natural: WHEN #ACCTG-DTE-IND
        if (condition(pnd_Ws_Pnd_Acctg_Dte_Ind.getBoolean()))
        {
            decideConditionsMet254++;
            getReports().write(1, ReportOption.NOTITLE,"Total for Acctg Date",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte, new ReportEditMask ("MM/DD/YY"),new TabSetting(72),pnd_Ws_Pnd_Tot_Dr_Amt.getValue(6),  //Natural: WRITE ( 1 ) 'Total for Acctg Date' #WS-PYMNT-ACCTG-DTE 72T #TOT-DR-AMT ( 6 ) 89T #TOT-CR-AMT ( 6 ) /
                new ReportEditMask ("-ZZZZ,ZZZ,ZZ9.99"),new TabSetting(89),pnd_Ws_Pnd_Tot_Cr_Amt.getValue(6), new ReportEditMask ("-ZZZZ,ZZZ,ZZ9.99"),NEWLINE);
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet254 > 0))
        {
            getReports().skip(1, 1);                                                                                                                                      //Natural: SKIP ( 1 ) 1 LINES
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet254 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Read_Payment_File() throws Exception                                                                                                                 //Natural: READ-PAYMENT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        vw_fcp_Cons_Pymnt.startDatabaseRead                                                                                                                               //Natural: READ ( 1 ) FCP-CONS-PYMNT BY PPCN-INV-ORGN-PRCSS-INST STARTING FROM #LEDGER-EXTRACT.CNTRCT-PPCN-NBR
        (
        "RD_PYMNT",
        new Wc[] { new Wc("PPCN_INV_ORGN_PRCSS_INST", ">=", ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr(), WcType.BY) },
        new Oc[] { new Oc("PPCN_INV_ORGN_PRCSS_INST", "ASC") },
        1
        );
        RD_PYMNT:
        while (condition(vw_fcp_Cons_Pymnt.readNextRow("RD_PYMNT")))
        {
            if (condition(fcp_Cons_Pymnt_Cntrct_Ppcn_Nbr.notEquals(ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Ppcn_Nbr())))                                                  //Natural: IF FCP-CONS-PYMNT.CNTRCT-PPCN-NBR NE #LEDGER-EXTRACT.CNTRCT-PPCN-NBR
            {
                if (true) break RD_PYMNT;                                                                                                                                 //Natural: ESCAPE BOTTOM ( RD-PYMNT. )
            }                                                                                                                                                             //Natural: END-IF
            ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Unq_Id_Nbr().setValue(fcp_Cons_Pymnt_Cntrct_Unq_Id_Nbr);                                                              //Natural: ASSIGN #LEDGER-EXTRACT.CNTRCT-UNQ-ID-NBR := FCP-CONS-PYMNT.CNTRCT-UNQ-ID-NBR
        }                                                                                                                                                                 //Natural: END-READ
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,ReportOption.NOHDR,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new               //Natural: WRITE ( 1 ) NOTITLE NOHDR *DATU '-' *TIMX ( EM = HH:IIAP ) 120T 'Page:' *PAGE-NUMBER ( 01 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'Report: RPT1' / 53T 'Monthly IA - Detail Ledger Report' / 53T 'Interface Date:' #PYMNT-INTRFCE-DTE / 'Accounting Date   :' #WS-PYMNT-ACCTG-DTE 64T #WS-HEADER-ISA / 'Ledger Account Num:' #WS-INV-ACCT-LEDGR-NBR 63T 'Name:' #WS-LEDGER-DESC /
                        TabSetting(120),"Page:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
                        TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"Report: RPT1",NEWLINE,new TabSetting(53),"Monthly IA - Detail Ledger Report",NEWLINE,new 
                        TabSetting(53),"Interface Date:",pnd_Pymnt_Intrfce_Dte,NEWLINE,"Accounting Date   :",pnd_Ws_Pnd_Ws_Pymnt_Acctg_Dte, new ReportEditMask 
                        ("MM/DD/YY"),new TabSetting(64),pnd_Ws_Pnd_Ws_Header_Isa,NEWLINE,"Ledger Account Num:",pnd_Ws_Pnd_Ws_Inv_Acct_Ledgr_Nbr,new TabSetting(63),
                        "Name:",pnd_Ws_Pnd_Ws_Ledger_Desc,NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Settlmnt_DteIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Check_DteIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_Ledgr_NbrIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Ledgr_Nbr().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Inv_Acct_Isa().isBreak(endOfData);
        boolean ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak = ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Acctg_Dte().isBreak(endOfData);
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Settlmnt_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Check_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_Ledgr_NbrIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak))
        {
            pnd_Ws_Pnd_Settl_Dte_Ind.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #SETTL-DTE-IND
            pnd_Ws_Pnd_Tot_Cr_Amt.getValue(1).setValue(readWork01Pnd_Cr_AmtSum130);                                                                                       //Natural: MOVE SUM ( #CR-AMT ) TO #TOT-CR-AMT ( 1 )
            pnd_Ws_Pnd_Tot_Dr_Amt.getValue(1).setValue(readWork01Pnd_Dr_AmtSum130);                                                                                       //Natural: MOVE SUM ( #DR-AMT ) TO #TOT-DR-AMT ( 1 )
            readWork01Pnd_Cr_AmtSum130.setDec(new DbsDecimal(0));                                                                                                         //Natural: END-BREAK
            readWork01Pnd_Dr_AmtSum130.setDec(new DbsDecimal(0));
        }
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Check_DteIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_Ledgr_NbrIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak 
            || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak))
        {
            pnd_Ws_Pnd_Check_Dte_Ind.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #CHECK-DTE-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_Ledgr_NbrIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak))
        {
            pnd_Ws_Pnd_Ledger_Ind.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO #LEDGER-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Inv_Acct_IsaIsBreak || ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak))
        {
            pnd_Ws_Pnd_Isa_Ind.setValue(true);                                                                                                                            //Natural: MOVE TRUE TO #ISA-IND
        }                                                                                                                                                                 //Natural: END-BREAK
        if (condition(ldaFcpl121_getPnd_Ledger_Extract_Pymnt_Acctg_DteIsBreak))
        {
            pnd_Ws_Pnd_Acctg_Dte_Ind.setValue(true);                                                                                                                      //Natural: MOVE TRUE TO #ACCTG-DTE-IND
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=60 LS=133 ZP=ON");
        Global.format(1, "PS=60 LS=133 ZP=ON");

        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"Settlement/Effective/Date",
        		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Settlmnt_Dte(),"/Payment/Date",
        		ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Check_Dte(),"//Annuitant Name   ",
        		ldaFcpl121.getPnd_Ledger_Extract_Ph_Last_Name(),"/           ",
        		ldaFcpl121.getPnd_Ledger_Extract_Ph_First_Name(),"/ ",
        		ldaFcpl121.getPnd_Ledger_Extract_Ph_Middle_Name(), new AlphanumericLength (1),"/Contract/Number",
        		pnd_Ws_Pnd_Contract,new ColumnSpacing(1),"/PIN",
        		ldaFcpl121.getPnd_Ledger_Extract_Cntrct_Unq_Id_Nbr(),new ColumnSpacing(1),"/DEBIT/AMOUNT",
        		pnd_Ws_Pnd_Dr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new ColumnSpacing(2),"/CREDIT/AMOUNT",
        		pnd_Ws_Pnd_Cr_Amt, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right));
    }
    private void CheckAtStartofData124() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
            pnd_Ws_Pnd_Ws_Pymnt_Intrfce_Dte.setValue(ldaFcpl121.getPnd_Ledger_Extract_Pymnt_Intrfce_Dte());                                                               //Natural: MOVE PYMNT-INTRFCE-DTE TO #WS-PYMNT-INTRFCE-DTE
            //*  INITIALIZATION
                                                                                                                                                                          //Natural: PERFORM INIT-RTN
            sub_Init_Rtn();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
