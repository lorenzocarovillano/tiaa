/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:20:27 PM
**        * FROM NATURAL PROGRAM : Fcpp818c
************************************************************
**        * FILE NAME            : Fcpp818c.java
**        * CLASS NAME           : Fcpp818c
**        * INSTANCE NAME        : Fcpp818c
************************************************************
************************************************************************
* PROGRAM  : FCPP818C
* SYSTEM   : CPS
* TITLE    : IAR RESTRUCTURE
* FUNCTION : REMOVE EXTRA FIELDS FROM THE STATEMENTS FILE.
* HISTORY  : 12/21/05 - RAMANA ANNE - CLONE OF FCPP818
*                                     FOR PAYEE MATCH PROJECT
*          : LCW - 04/28/08- RE-STOWED FOR FCPA800 ROTH-MAJOR1 CHANGES.
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 12/09/2011 :R.SACHARNY - RECOMPILE DUE TO CHANGES TO FCPA800
* 01/13/2012 :R.SACHARNY - RECOMPILED FOR SUNY/CUNY (FCPA800)
* 12/2015  :J.OSTEEN - RE-COMPILED TO PICK UP CHANGES TO FCPA801B
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp818c extends BLNatBase
{
    // Data Areas
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa801b pdaFcpa801b;
    private PdaFcpa803c pdaFcpa803c;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Chk_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa801b = new PdaFcpa801b(localVariables);
        pdaFcpa803c = new PdaFcpa803c(localVariables);

        // Local Variables
        pnd_Chk_Nbr = localVariables.newFieldInRecord("pnd_Chk_Nbr", "#CHK-NBR", FieldType.NUMERIC, 10);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp818c() throws Exception
    {
        super("Fcpp818c");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setupReports();
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 1 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 57T 'IAR CONTROL REPORT' 120T 'REPORT: RPT1' //
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        //* ***************
        //*  MAIN PROGRAM *
        //* ***************
        READWORK01:                                                                                                                                                       //Natural: READ WORK FILE 1 #CHECK-SORT-FIELDS PYMNT-ADDR-INFO #CHECK-FIELDS INV-INFO ( * )
        while (condition(getWorkFiles().read(1, pdaFcpa803c.getPnd_Check_Sort_Fields(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa801b.getPnd_Check_Fields(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            getWorkFiles().write(2, true, pdaFcpa803c.getPnd_Check_Sort_Fields_Pymnt_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1, //Natural: WRITE WORK FILE 2 VARIABLE #CHECK-SORT-FIELDS.PYMNT-NBR WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO WF-PYMNT-ADDR-GRP.INV-INFO ( 1:INV-ACCT-COUNT )
                ":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK01_Exit:
        if (Global.isEscape()) return;
        READWORK02:                                                                                                                                                       //Natural: READ WORK FILE 3 PYMNT-ADDR-INFO INV-INFO ( * )
        while (condition(getWorkFiles().read(3, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
        {
            getWorkFiles().write(4, true, pnd_Chk_Nbr, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,          //Natural: WRITE WORK FILE 4 VARIABLE #CHK-NBR WF-PYMNT-ADDR-GRP.PYMNT-ADDR-INFO WF-PYMNT-ADDR-GRP.INV-INFO ( 1:INV-ACCT-COUNT )
                ":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
        }                                                                                                                                                                 //Natural: END-WORK
        READWORK02_Exit:
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(1, "PS=58 LS=132 ZP=ON");

        getReports().write(1, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(57),"IAR CONTROL REPORT",new TabSetting(120),"REPORT: RPT1",NEWLINE,NEWLINE);
    }
}
