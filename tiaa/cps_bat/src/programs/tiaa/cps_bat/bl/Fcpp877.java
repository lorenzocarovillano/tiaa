/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:21:09 PM
**        * FROM NATURAL PROGRAM : Fcpp877
************************************************************
**        * FILE NAME            : Fcpp877.java
**        * CLASS NAME           : Fcpp877
**        * INSTANCE NAME        : Fcpp877
************************************************************
************************************************************************
* PROGRAM  : FCPP877
* SYSTEM   : CPS
* TITLE    : CPS DAILY CYCLES CHECK PRINT
* FUNCTION : 1 - COMBINE INDIVIDUAL SYSOUTS INTO ONE PRINT FILE.
*            2 - ON THE RESTART - REPRINT THE CHECKS.
*          : 01/05/2006   R. LANDRUM - POSITIVE PAY/PAYEE MATCH
*            INPUT CHECK & SEQ NBRS NOW RETRIEVED FROM REFERENCE TABLE.
*************************** NOTE !!! **********************************
* AS AN ADDED VALUE, 10-DIGIT(N10) MICR CHECK NBR PRINTED ON CHECKS
* 10-DIGIT CHECK NBR INCLUDED ON POS-PAY FILES FOR ALL CPS DISBURSEMENT
* APPLICATIONS (1400,1500,1600,1700,& 2200) FOR PAYEE MATCH PROJECT.
*
* THE 1400 STREAM WILL CONTINUE TO PROCESS THE 7-DIGIT(N7) CHECK NBR
* NBR INTERNALLY...PREFIXING A TABLE DRIVEN 3-DIGIT FIXED VALUE IN
* GENERATING THE REQUIRED 10 DIGIT CHECK NBR FOR MICR & POS-PAY FILES
* OUT OF THE 1400 SERIES. THIS IS EXCLUSIVE TO THE 1400 STREAM AS OTHER
* CPS STREAMS (1500,1600,1700 & 2200) HAVE BEEN ENGINEERED TO PROCESS
* THE 10-DIGIT CHECK NBR INTETERNALLY.
************************************************************************
*
* 07/06/2009 :J.OSTEEN - RE-STOWED TO PICKUP MODIFIED PDA FCPA803
* 4/2017     : JJG - PIN EXPANSION, RESTOW FCPL876 CHANGE
************************************************************************
*

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp877 extends BLNatBase
{
    // Data Areas
    private PdaFcpa803 pdaFcpa803;
    private LdaFcpl876 ldaFcpl876;
    private LdaFcpl876a ldaFcpl876a;
    private LdaFcpl876b ldaFcpl876b;
    private PdaFcpa110 pdaFcpa110;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Cnt;

    private DbsGroup pnd_Input_Parm;
    private DbsField pnd_Input_Parm_Pnd_Input_Parm_Check_Prefix;
    private DbsField pnd_Input_Parm_Pnd_Input_Parm_Check_Nbr;
    private DbsField pnd_Input_Parm_Pnd_Input_Parm_Filler;
    private DbsField pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr;

    private DbsGroup pnd_Ws;
    private DbsField pnd_Ws_Pnd_Input_Rec;
    private DbsField pnd_Ws_Pnd_Break_Key;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Pnd_Hold_Ind;
    private DbsField pnd_Ws_Pnd_Disp_Ind;
    private DbsField pnd_Ws_Pnd_Disp_Ind1;
    private DbsField pnd_Ws_Pnd_Prev_Cntrct_Orgn_Cde;
    private DbsField pnd_Ws_Pnd_Range_Cnt;
    private DbsField pnd_Ws_Pnd_Check_Cnt;
    private DbsField pnd_Ws_Pnd_Range_Check;
    private DbsField pnd_Ws_Pnd_Range_Begin;
    private DbsField pnd_Ws_Pnd_Range_End;
    private DbsField pnd_Ws_Pnd_Next_Check;
    private DbsField pnd_Ws_Pnd_Highest_Seq;
    private DbsField pnd_Ws_Pnd_First_Check;
    private DbsField pnd_Ws_Pnd_Range_Break;
    private DbsField pnd_Ws_Pnd_Range_Start;
    private DbsField pnd_Ws_Pnd_Fcpl876a_Found;
    private DbsField pnd_Ws_Pnd_Prev_Hold_Ind;
    private DbsField pnd_Ws_Pnd_Void_Sep_Page;
    private DbsField pnd_Ws_Old_Check_Nbr_N7;
    private DbsField pnd_Ws_New_Check_Nbr_N7;
    private DbsField pnd_Ws_Compare_Nbr;
    private DbsField pnd_Ws_Check_Nbr_N10;

    private DbsGroup pnd_Ws_Check_Nbr_N10__R_Field_2;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3;
    private DbsField pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7;
    private DbsField pnd_Ws_Check_Nbr_A10;

    private DbsGroup pnd_Ws_Check_Nbr_A10__R_Field_3;
    private DbsField pnd_Ws_Check_Nbr_A10_Pnd_Ws_Check_Nbr_A3;
    private DbsField pnd_Ws_Check_Nbr_A10_Pnd_Ws_Check_Nbr_A7;

    private DbsGroup pnd_Restart;
    private DbsField pnd_Restart_Pnd_Comp_String;
    private DbsField pnd_Restart_Pnd_New_String;
    private DbsField pnd_Restart_Pnd_Restart_Ind;
    private DbsField pnd_Restart_Pnd_Sysout_Ind;
    private DbsField pnd_Restart_Pymnt_Check_Nbr;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa803 = new PdaFcpa803(localVariables);
        ldaFcpl876 = new LdaFcpl876();
        registerRecord(ldaFcpl876);
        ldaFcpl876a = new LdaFcpl876a();
        registerRecord(ldaFcpl876a);
        ldaFcpl876b = new LdaFcpl876b();
        registerRecord(ldaFcpl876b);
        pdaFcpa110 = new PdaFcpa110(localVariables);

        // Local Variables
        pnd_Cnt = localVariables.newFieldInRecord("pnd_Cnt", "#CNT", FieldType.NUMERIC, 10);

        pnd_Input_Parm = localVariables.newGroupInRecord("pnd_Input_Parm", "#INPUT-PARM");
        pnd_Input_Parm_Pnd_Input_Parm_Check_Prefix = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Parm_Check_Prefix", "#INPUT-PARM-CHECK-PREFIX", 
            FieldType.NUMERIC, 3);
        pnd_Input_Parm_Pnd_Input_Parm_Check_Nbr = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Parm_Check_Nbr", "#INPUT-PARM-CHECK-NBR", FieldType.NUMERIC, 
            7);
        pnd_Input_Parm_Pnd_Input_Parm_Filler = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Parm_Filler", "#INPUT-PARM-FILLER", FieldType.STRING, 
            1);
        pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr = pnd_Input_Parm.newFieldInGroup("pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr", "#INPUT-PARM-CHECK-SEQ-NBR", 
            FieldType.NUMERIC, 7);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");
        pnd_Ws_Pnd_Input_Rec = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Input_Rec", "#INPUT-REC", FieldType.STRING, 143);
        pnd_Ws_Pnd_Break_Key = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Break_Key", "#BREAK-KEY", FieldType.STRING, 3);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Break_Key);
        pnd_Ws_Cntrct_Orgn_Cde = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Hold_Ind = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Hold_Ind", "#HOLD-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Disp_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Ind", "#DISP-IND", FieldType.STRING, 2);
        pnd_Ws_Pnd_Disp_Ind1 = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Disp_Ind1", "#DISP-IND1", FieldType.STRING, 2);
        pnd_Ws_Pnd_Prev_Cntrct_Orgn_Cde = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Cntrct_Orgn_Cde", "#PREV-CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        pnd_Ws_Pnd_Range_Cnt = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Range_Cnt", "#RANGE-CNT", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Check_Cnt = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Check_Cnt", "#CHECK-CNT", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            2));
        pnd_Ws_Pnd_Range_Check = pnd_Ws.newFieldArrayInGroup("pnd_Ws_Pnd_Range_Check", "#RANGE-CHECK", FieldType.PACKED_DECIMAL, 7, new DbsArrayController(1, 
            2));
        pnd_Ws_Pnd_Range_Begin = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Range_Begin", "#RANGE-BEGIN", FieldType.NUMERIC, 10);
        pnd_Ws_Pnd_Range_End = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Range_End", "#RANGE-END", FieldType.NUMERIC, 10);
        pnd_Ws_Pnd_Next_Check = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Next_Check", "#NEXT-CHECK", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Highest_Seq = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Highest_Seq", "#HIGHEST-SEQ", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_First_Check = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_First_Check", "#FIRST-CHECK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Range_Break = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Range_Break", "#RANGE-BREAK", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Range_Start = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Range_Start", "#RANGE-START", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Fcpl876a_Found = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Fcpl876a_Found", "#FCPL876A-FOUND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Prev_Hold_Ind = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Prev_Hold_Ind", "#PREV-HOLD-IND", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Void_Sep_Page = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Void_Sep_Page", "#VOID-SEP-PAGE", FieldType.BOOLEAN, 1);
        pnd_Ws_Old_Check_Nbr_N7 = localVariables.newFieldInRecord("pnd_Ws_Old_Check_Nbr_N7", "#WS-OLD-CHECK-NBR-N7", FieldType.NUMERIC, 7);
        pnd_Ws_New_Check_Nbr_N7 = localVariables.newFieldInRecord("pnd_Ws_New_Check_Nbr_N7", "#WS-NEW-CHECK-NBR-N7", FieldType.NUMERIC, 7);
        pnd_Ws_Compare_Nbr = localVariables.newFieldInRecord("pnd_Ws_Compare_Nbr", "#WS-COMPARE-NBR", FieldType.NUMERIC, 7);
        pnd_Ws_Check_Nbr_N10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_N10", "#WS-CHECK-NBR-N10", FieldType.NUMERIC, 10);

        pnd_Ws_Check_Nbr_N10__R_Field_2 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_N10__R_Field_2", "REDEFINE", pnd_Ws_Check_Nbr_N10);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3 = pnd_Ws_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3", "#WS-CHECK-NBR-N3", 
            FieldType.NUMERIC, 3);
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7 = pnd_Ws_Check_Nbr_N10__R_Field_2.newFieldInGroup("pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7", "#WS-CHECK-NBR-N7", 
            FieldType.NUMERIC, 7);
        pnd_Ws_Check_Nbr_A10 = localVariables.newFieldInRecord("pnd_Ws_Check_Nbr_A10", "#WS-CHECK-NBR-A10", FieldType.STRING, 10);

        pnd_Ws_Check_Nbr_A10__R_Field_3 = localVariables.newGroupInRecord("pnd_Ws_Check_Nbr_A10__R_Field_3", "REDEFINE", pnd_Ws_Check_Nbr_A10);
        pnd_Ws_Check_Nbr_A10_Pnd_Ws_Check_Nbr_A3 = pnd_Ws_Check_Nbr_A10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_A10_Pnd_Ws_Check_Nbr_A3", "#WS-CHECK-NBR-A3", 
            FieldType.STRING, 3);
        pnd_Ws_Check_Nbr_A10_Pnd_Ws_Check_Nbr_A7 = pnd_Ws_Check_Nbr_A10__R_Field_3.newFieldInGroup("pnd_Ws_Check_Nbr_A10_Pnd_Ws_Check_Nbr_A7", "#WS-CHECK-NBR-A7", 
            FieldType.STRING, 7);

        pnd_Restart = localVariables.newGroupInRecord("pnd_Restart", "#RESTART");
        pnd_Restart_Pnd_Comp_String = pnd_Restart.newFieldInGroup("pnd_Restart_Pnd_Comp_String", "#COMP-STRING", FieldType.STRING, 19);
        pnd_Restart_Pnd_New_String = pnd_Restart.newFieldInGroup("pnd_Restart_Pnd_New_String", "#NEW-STRING", FieldType.STRING, 19);
        pnd_Restart_Pnd_Restart_Ind = pnd_Restart.newFieldInGroup("pnd_Restart_Pnd_Restart_Ind", "#RESTART-IND", FieldType.BOOLEAN, 1);
        pnd_Restart_Pnd_Sysout_Ind = pnd_Restart.newFieldInGroup("pnd_Restart_Pnd_Sysout_Ind", "#SYSOUT-IND", FieldType.BOOLEAN, 1);
        pnd_Restart_Pymnt_Check_Nbr = pnd_Restart.newFieldInGroup("pnd_Restart_Pymnt_Check_Nbr", "PYMNT-CHECK-NBR", FieldType.PACKED_DECIMAL, 7);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl876.initializeValues();
        ldaFcpl876a.initializeValues();
        ldaFcpl876b.initializeValues();

        localVariables.reset();
        pnd_Ws_Pnd_First_Check.setInitialValue(true);
        pnd_Ws_Pnd_Range_Start.setInitialValue(true);
        pnd_Restart_Pnd_Sysout_Ind.setInitialValue(true);
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp877() throws Exception
    {
        super("Fcpp877");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        getReports().atEndOfPage(atEndEventRpt1, 1);
        setupReports();
        Global.getERROR_TA().setValue("INFP9000");                                                                                                                        //Natural: MOVE 'INFP9000' TO *ERROR-TA
        if (Global.isEscape()) return;                                                                                                                                    //Natural: FORMAT ( 0 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 1 ) PS = 58 LS = 80 ZP = ON;//Natural: FORMAT ( 2 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 1 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 27T 'CONSOLIDATED PAYMENT SYSTEM' 68T 'PAGE:' *PAGE-NUMBER ( 1 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 34T 'CHECK RANGES' 68T 'REPORT: RPT1' //
        if (Global.isEscape()) return;                                                                                                                                    //Natural: WRITE ( 2 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'CONSOLIDATED PAYMENT SYSTEM' 120T 'PAGE:' *PAGE-NUMBER ( 2 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 46T 'ANNUITANT STATEMENT DETAIL CONTROL REPORT' 120T 'REPORT: RPT2' //
        //* ***************                                                                                                                                               //Natural: AT END OF PAGE ( 1 )
        //*  MAIN PROGRAM *
        //* ***************
        //* *************** START RLANDRUM POS-PAY PAYEE MATCH ********************
        //* *READ WORK FILE 2 ONCE #INPUT-PARM
        //* *AT END OF FILE
        //* *  PERFORM ERROR-DISPLAY-START
        //* *  WRITE '***' 25T 'MISSING INPUT PARAMETER'            77T '***'
        //* *  PERFORM ERROR-DISPLAY-END
        //* *  TERMINATE 50
        //* *END-ENDFILE
        //*  RL GET OLD CHECK NBR
                                                                                                                                                                          //Natural: PERFORM GET-START-CHECK-NO
        sub_Get_Start_Check_No();
        if (condition(Global.isEscape())) {return;}
        //* ***************** END RLANDRUM POS-PAY PAYEE MATCH ********************
        getWorkFiles().read(6, ldaFcpl876.getPnd_Fcpl876());                                                                                                              //Natural: READ WORK FILE 6 ONCE #FCPL876
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "***",new TabSetting(25),"MISSING 'NEXT CHECK' FILE",new TabSetting(77),"***");                                                         //Natural: WRITE '***' 25T 'MISSING "NEXT CHECK" FILE' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, Global.getPROGRAM(),"1120 TERMINATE 51");                                                                                               //Natural: WRITE *PROGRAM '1120 TERMINATE 51'
            if (Global.isEscape()) return;
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-ENDFILE
        //* *************** START RLANDRUM POS-PAY PAYEE MATCH ********************
        //*  RL UPDATE REF TABLE W INCREMENTED 876 CHKNBR
                                                                                                                                                                          //Natural: PERFORM UPDATE-CHECK-NO
        sub_Update_Check_No();
        if (condition(Global.isEscape())) {return;}
        //*  NEXT CHECK & SEQ NBR NOW RETRIEVED FROM REF TBL FOR POSPAY/PAYEE MATCH
        //* ***************** END RLANDRUM POS-PAY PAYEE MATCH ********************
        //* *IF #INPUT-PARM-CHECK-SEQ-NBR    NE 1
        //* *  #RESTART-IND                  := TRUE
        //* *  #SYSOUT-IND                   := FALSE
        //* *  #RESTART.PYMNT-CHECK-NBR      := #INPUT-PARM-CHECK-NBR - 1
        //* *  WRITE *PROGRAM '940 RESTART' /
        //* *  WRITE(1) ///
        //* *    11T 'RESTARTING CHECK PRINT FROM:' /
        //* *    15T 'PROCESS SEQUENCE NUMBER:'
        //* *    #INPUT-PARM-CHECK-SEQ-NBR(EM=-Z,ZZZ,ZZ9) /
        //* *   15T 'WITH CHECK NUMBER......:   ' #INPUT-PARM-CHECK-NBR(EM=9999999)
        //* *    ///
        //* *END-IF
        pdaFcpa803.getPnd_Fcpa803_Cntrct_Orgn_Cde().setValue("NZ");                                                                                                       //Natural: ASSIGN #FCPA803.CNTRCT-ORGN-CDE := 'NZ'
        if (condition(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr().equals(1)))                                                                                         //Natural: IF #FCPL876.PYMNT-CHECK-SEQ-NBR = 1
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(14),"*** No Checks, Statements were generated due to EMPTY Input ***",NEWLINE,                   //Natural: WRITE ( 1 ) /// 14T '*** No Checks, Statements were generated due to EMPTY Input ***' ///
                NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            if (condition(pnd_Restart_Pnd_Restart_Ind.getBoolean()))                                                                                                      //Natural: IF #RESTART-IND
            {
                DbsUtil.terminate(52);  if (true) return;                                                                                                                 //Natural: TERMINATE 52
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM PROCESS-INPUT
            sub_Process_Input();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pnd_Restart_Pnd_Sysout_Ind.getBoolean())))                                                                                                       //Natural: IF NOT #SYSOUT-IND
        {
            getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(14),"*** No Checks, Statements were generated.  ***",NEWLINE,new TabSetting(14),                 //Natural: WRITE ( 1 ) /// 14T '*** No Checks, Statements were generated.  ***' / 14T '*** Restart point was not found!!!         ***' ///
                "*** Restart point was not found!!!         ***",NEWLINE,NEWLINE,NEWLINE);
            if (Global.isEscape()) return;
            //*  RL TEST
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Highest_Seq.setValue(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr());                                                                               //Natural: ASSIGN #HIGHEST-SEQ := #FCPL876A.PYMNT-CHECK-SEQ-NBR
        getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(18),"Total number of checks printed :",pnd_Ws_Pnd_Check_Cnt.getValue(2), new ReportEditMask          //Natural: WRITE ( 1 ) /// 18T 'Total number of checks printed :' #CHECK-CNT ( 2 ) / 18T 'Highest payment sequence number:' #HIGHEST-SEQ
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(18),"Highest payment sequence number:",pnd_Ws_Pnd_Highest_Seq, new ReportEditMask ("-Z,ZZZ,ZZ9"));
        if (Global.isEscape()) return;
        //* ******************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PROCESS-INPUT
        //* *WRITE
        //* *  '#FCPL876B.#PYMNT-CHECK-NBR' #FCPL876B.#PYMNT-CHECK-NBR /
        //* *  '#FCPL876.PYMNT-CHECK-NBR' #FCPL876.PYMNT-CHECK-NBR /
        //* *  '#FCPL876.PYMNT-CHECK-SCRTY-NBR' #FCPL876.PYMNT-CHECK-SCRTY-NBR /
        //* *  '#FCPL876.PYMNT-CHECK-SEQ-NBR' #FCPL876.PYMNT-CHECK-SEQ-NBR /
        //* *  '#FCPL876B.CNTRCT-ORGN-CDE'  #FCPL876B.CNTRCT-ORGN-CDE //
        //* ***********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-OUT-WORKFILE
        //* *********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-BAR-REPORT
        //* *'/CHECK/NUMBER'          #PYMNT-CHECK-NBR
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-CHECK-RANGE
        //* *  'STARTING/CHECK/NUMBER'      #RANGE-CHECK(1)(LC=�)
        //* *  'ENDING/CHECK/NUMBER'        #RANGE-CHECK(2)
        //* ********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-VOID-PAGE
        //* *************** START RLANDRUM POS-PAY PAYEE MATCH ********************
        //*  --------------------------------- *
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-START-CHECK-NO
        //* ***********************************************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: UPDATE-CHECK-NO
        //* ***************** END RLANDRUM POS-PAY PAYEE MATCH ********************
        //* **********************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
        //* ************************************
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
    }
    private void sub_Process_Input() throws Exception                                                                                                                     //Natural: PROCESS-INPUT
    {
        if (BLNatReinput.isReinput()) return;

        //* ******************************
        boolean endOfDataReadwork01 = true;                                                                                                                               //Natural: READ WORK FILE 1 #INPUT-REC
        boolean firstReadwork01 = true;
        READWORK01:
        while (condition(getWorkFiles().read(1, pnd_Ws_Pnd_Input_Rec)))
        {
            CheckAtStartofData396();

            if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
            {
                atBreakEventReadwork01();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom()))
                        break;
                    else if (condition(Global.isEscapeBottomImmediate()))
                    {
                        endOfDataReadwork01 = false;
                        break;
                    }
                    else if (condition(Global.isEscapeTop()))
                    continue;
                    else if (condition())
                    return;
                }
            }
            //*                                                                                                                                                           //Natural: AT START OF DATA
            //BEFORE BREAK PROCESSING                                                                                                                                     //Natural: BEFORE BREAK PROCESSING
            if (condition(pnd_Ws_Pnd_Input_Rec.getSubstring(1,11).equals("!@#SUP01#@!")))                                                                                 //Natural: IF SUBSTR ( #INPUT-REC,1,11 ) = '!@#SUP01#@!'
            {
                ldaFcpl876b.getPnd_Fcpl876b().setValue(pnd_Ws_Pnd_Input_Rec);                                                                                             //Natural: ASSIGN #FCPL876B := #INPUT-REC
                if (condition(pnd_Restart_Pnd_Restart_Ind.getBoolean() && pnd_Restart_Pnd_Sysout_Ind.getBoolean() && ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean())) //Natural: IF #RESTART-IND AND #SYSOUT-IND AND #FCPL876B.#CHECK
                {
                    ldaFcpl876b.getPnd_Fcpl876b_Pymnt_Check_Nbr().setValue(pnd_Restart_Pymnt_Check_Nbr);                                                                  //Natural: ASSIGN #FCPL876B.PYMNT-CHECK-NBR := #RESTART.PYMNT-CHECK-NBR
                    pnd_Ws_Pnd_Input_Rec.setValue(ldaFcpl876b.getPnd_Fcpl876b());                                                                                         //Natural: ASSIGN #INPUT-REC := #FCPL876B
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-OUT-WORKFILE
                sub_Write_Out_Workfile();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                                                                                                                                                                          //Natural: PERFORM PRINT-BAR-REPORT
                sub_Print_Bar_Report();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Input_Rec.getSubstring(1,11).equals("!@#SUP02#@!")))                                                                                 //Natural: IF SUBSTR ( #INPUT-REC,1,11 ) = '!@#SUP02#@!'
            {
                pdaFcpa803.getPnd_Fcpa803_Pnd_Separator_Page().setValue(true);                                                                                            //Natural: ASSIGN #SEPARATOR-PAGE := TRUE
                                                                                                                                                                          //Natural: PERFORM PRINT-VOID-PAGE
                sub_Print_Void_Page();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                pdaFcpa803.getPnd_Fcpa803_Pnd_Separator_Page().setValue(false);                                                                                           //Natural: ASSIGN #SEPARATOR-PAGE := FALSE
                                                                                                                                                                          //Natural: PERFORM WRITE-OUT-WORKFILE
                sub_Write_Out_Workfile();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                getReports().newPage(new ReportSpecification(2));                                                                                                         //Natural: NEWPAGE ( 2 )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
                if (condition(true)) continue;                                                                                                                            //Natural: ESCAPE TOP
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Input_Rec.getSubstring(1,11).equals("!@#SUP00#@!")))                                                                                 //Natural: IF SUBSTR ( #INPUT-REC,1,11 ) = '!@#SUP00#@!'
            {
                ldaFcpl876a.getPnd_Fcpl876a().setValue(pnd_Ws_Pnd_Input_Rec);                                                                                             //Natural: ASSIGN #FCPL876A := #INPUT-REC
                pnd_Ws_Cntrct_Orgn_Cde.setValue(ldaFcpl876a.getPnd_Fcpl876a_Cntrct_Orgn_Cde());                                                                           //Natural: ASSIGN #WS.CNTRCT-ORGN-CDE := #FCPL876A.CNTRCT-ORGN-CDE
                pnd_Ws_Pnd_Hold_Ind.setValue(ldaFcpl876a.getPnd_Fcpl876a_Pnd_Hold_Ind().getBoolean());                                                                    //Natural: ASSIGN #WS.#HOLD-IND := #FCPL876A.#HOLD-IND
                pnd_Ws_Pnd_Fcpl876a_Found.setValue(true);                                                                                                                 //Natural: ASSIGN #FCPL876A-FOUND := TRUE
                if (condition(pnd_Restart_Pnd_Restart_Ind.getBoolean()))                                                                                                  //Natural: IF #RESTART-IND
                {
                    short decideConditionsMet426 = 0;                                                                                                                     //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FCPL876A.PYMNT-CHECK-SEQ-NBR = #INPUT-PARM-CHECK-SEQ-NBR
                    if (condition(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr().equals(pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr)))
                    {
                        decideConditionsMet426++;
                        pnd_Restart_Pnd_Sysout_Ind.setValue(true);                                                                                                        //Natural: ASSIGN #SYSOUT-IND := TRUE
                        pnd_Ws_Pnd_Range_Break.setValue(true);                                                                                                            //Natural: ASSIGN #RANGE-BREAK := TRUE
                        pnd_Ws_Pnd_Disp_Ind.setValue("**");                                                                                                               //Natural: ASSIGN #DISP-IND := '**'
                                                                                                                                                                          //Natural: PERFORM PRINT-VOID-PAGE
                        sub_Print_Void_Page();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                    }                                                                                                                                                     //Natural: WHEN #FCPL876A.PYMNT-CHECK-SEQ-NBR > #INPUT-PARM-CHECK-SEQ-NBR
                    else if (condition(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr().greater(pnd_Input_Parm_Pnd_Input_Parm_Check_Seq_Nbr)))
                    {
                        decideConditionsMet426++;
                        ignore();
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet426 > 0))
                    {
                        pnd_Restart_Pnd_Comp_String.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "H'00'", "3", ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr(), //Natural: COMPRESS H'00' '3' #FCPL876A.PYMNT-CHECK-NBR H'000000' #FCPL876A.PYMNT-CHECK-SEQ-NBR INTO #COMP-STRING LEAVING NO SPACE
                            "H'000000'", ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr()));
                        setValueToSubstring(" ",pnd_Restart_Pnd_Comp_String,1,1);                                                                                         //Natural: MOVE ' ' TO SUBSTR ( #COMP-STRING,1,1 )
                        pnd_Restart_Pymnt_Check_Nbr.nadd(1);                                                                                                              //Natural: ADD 1 TO #RESTART.PYMNT-CHECK-NBR
                        ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().setValue(pnd_Restart_Pymnt_Check_Nbr);                                                              //Natural: ASSIGN #FCPL876A.PYMNT-CHECK-NBR := #RESTART.PYMNT-CHECK-NBR
                        pnd_Ws_Pnd_Input_Rec.setValue(ldaFcpl876a.getPnd_Fcpl876a());                                                                                     //Natural: ASSIGN #INPUT-REC := #FCPL876A
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                }                                                                                                                                                         //Natural: END-IF
                short decideConditionsMet444 = 0;                                                                                                                         //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN #FIRST-CHECK
                if (condition(pnd_Ws_Pnd_First_Check.getBoolean()))
                {
                    decideConditionsMet444++;
                    pnd_Ws_Pnd_Next_Check.setValue(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr());                                                                        //Natural: ASSIGN #NEXT-CHECK := #FCPL876A.PYMNT-CHECK-NBR
                    pnd_Ws_Pnd_First_Check.setValue(false);                                                                                                               //Natural: ASSIGN #FIRST-CHECK := FALSE
                }                                                                                                                                                         //Natural: WHEN #FCPL876A.PYMNT-CHECK-NBR NE #NEXT-CHECK
                else if (condition(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().notEquals(pnd_Ws_Pnd_Next_Check)))
                {
                    decideConditionsMet444++;
                    pnd_Ws_Pnd_Range_Break.setValue(true);                                                                                                                //Natural: ASSIGN #RANGE-BREAK := TRUE
                    setValueToSubstring("*",pnd_Ws_Pnd_Disp_Ind,2,1);                                                                                                     //Natural: MOVE '*' TO SUBSTR ( #DISP-IND,2,1 )
                }                                                                                                                                                         //Natural: WHEN NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //END-BEFORE                                                                                                                                                  //Natural: END-BEFORE
            //*                                                                                                                                                           //Natural: AT BREAK OF #BREAK-KEY
            if (condition(! (pnd_Ws_Pnd_Fcpl876a_Found.getBoolean()) && pnd_Restart_Pnd_Sysout_Ind.getBoolean()))                                                         //Natural: IF NOT #FCPL876A-FOUND AND #SYSOUT-IND
            {
                if (condition(pnd_Restart_Pnd_Restart_Ind.getBoolean() && pnd_Restart_Pnd_Comp_String.equals(pnd_Ws_Pnd_Input_Rec)))                                      //Natural: IF #RESTART-IND AND #COMP-STRING = #INPUT-REC
                {
                    pnd_Restart_Pnd_New_String.setValue(DbsUtil.compress(CompressOption.LeavingNoSpace, "H'00'", "3", ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr(),      //Natural: COMPRESS H'00' '3' #FCPL876A.PYMNT-CHECK-NBR H'000000' #FCPL876A.PYMNT-CHECK-SEQ-NBR INTO #NEW-STRING LEAVING NO SPACE
                        "H'000000'", ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Seq_Nbr()));
                    setValueToSubstring(" ",pnd_Restart_Pnd_New_String,1,1);                                                                                              //Natural: MOVE ' ' TO SUBSTR ( #NEW-STRING,1,1 )
                    pnd_Ws_Pnd_Input_Rec.setValue(pnd_Restart_Pnd_New_String);                                                                                            //Natural: ASSIGN #INPUT-REC := #NEW-STRING
                }                                                                                                                                                         //Natural: END-IF
                //*  PRINTER
                getWorkFiles().write(8, false, pnd_Ws_Pnd_Input_Rec);                                                                                                     //Natural: WRITE WORK FILE 8 #INPUT-REC
                //*  RL CHECK PRINT RESTART FILE P1406
                getWorkFiles().write(10, false, pnd_Ws_Pnd_Input_Rec);                                                                                                    //Natural: WRITE WORK FILE 10 #INPUT-REC
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-OUT-WORKFILE
            sub_Write_Out_Workfile();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(pnd_Ws_Pnd_Range_Break.getBoolean()))                                                                                                           //Natural: IF #RANGE-BREAK
            {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CHECK-RANGE
                sub_Display_Check_Range();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pnd_Ws_Pnd_Fcpl876a_Found.getBoolean()))                                                                                                        //Natural: IF #FCPL876A-FOUND
            {
                pnd_Ws_Pnd_Check_Cnt.getValue(1).nadd(1);                                                                                                                 //Natural: ADD 1 TO #CHECK-CNT ( 1 )
                pnd_Ws_Pnd_Next_Check.compute(new ComputeParameters(false, pnd_Ws_Pnd_Next_Check), ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr().add(1));                 //Natural: ASSIGN #NEXT-CHECK := #FCPL876A.PYMNT-CHECK-NBR + 1
                if (condition(pnd_Ws_Pnd_Range_Start.getBoolean()))                                                                                                       //Natural: IF #RANGE-START
                {
                    pnd_Ws_Pnd_Range_Check.getValue(1).setValue(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr());                                                           //Natural: ASSIGN #RANGE-CHECK ( 1 ) := #FCPL876A.PYMNT-CHECK-NBR
                    pnd_Ws_Pnd_Range_Start.setValue(false);                                                                                                               //Natural: ASSIGN #RANGE-START := FALSE
                }                                                                                                                                                         //Natural: END-IF
                pnd_Ws_Pnd_Range_Check.getValue(2).setValue(ldaFcpl876a.getPnd_Fcpl876a_Pymnt_Check_Nbr());                                                               //Natural: ASSIGN #RANGE-CHECK ( 2 ) := #FCPL876A.PYMNT-CHECK-NBR
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Fcpl876a_Found.setValue(false);                                                                                                                    //Natural: ASSIGN #FCPL876A-FOUND := FALSE
            pnd_Ws_Pnd_Void_Sep_Page.setValue(false);                                                                                                                     //Natural: ASSIGN #VOID-SEP-PAGE := FALSE
            pnd_Ws_Pnd_Prev_Hold_Ind.setValue(pnd_Ws_Pnd_Hold_Ind.getBoolean());                                                                                          //Natural: ASSIGN #PREV-HOLD-IND := #WS.#HOLD-IND
            pnd_Ws_Pnd_Prev_Cntrct_Orgn_Cde.setValue(pnd_Ws_Cntrct_Orgn_Cde);                                                                                             //Natural: ASSIGN #PREV-CNTRCT-ORGN-CDE := #WS.CNTRCT-ORGN-CDE
        }                                                                                                                                                                 //Natural: AT END OF DATA;//Natural: END-WORK
        READWORK01_Exit:
        if (condition(getWorkFiles().getAstCOUNTER().greater(0)))
        {
            atBreakEventReadwork01(endOfDataReadwork01);
        }
        if (condition(getWorkFiles().getAtEndOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM DISPLAY-CHECK-RANGE
            sub_Display_Check_Range();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM PRINT-VOID-PAGE
            sub_Print_Void_Page();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-ENDDATA
        if (Global.isEscape()) return;
    }
    //*  GDG HISTORY
    private void sub_Write_Out_Workfile() throws Exception                                                                                                                //Natural: WRITE-OUT-WORKFILE
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        //*  GDG
        getWorkFiles().write(3, false, pnd_Ws_Pnd_Input_Rec);                                                                                                             //Natural: WRITE WORK FILE 3 #INPUT-REC
    }
    private void sub_Print_Bar_Report() throws Exception                                                                                                                  //Natural: PRINT-BAR-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        if (condition(ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde().equals("0000")))                                                                                      //Natural: IF CNTRCT-HOLD-CDE = '0000'
        {
            ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde().reset();                                                                                                        //Natural: RESET CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
        //* RL 5-7-06
        //* RL 5-7-06
        //* RL 5-7-06
        if (condition(ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean()))                                                                                              //Natural: IF #FCPL876B.#CHECK
        {
            pnd_Ws_Check_Nbr_A10_Pnd_Ws_Check_Nbr_A3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                             //Natural: ASSIGN #WS-CHECK-NBR-A3 := FCPA110.START-CHECK-PREFIX-N3
            pnd_Ws_Check_Nbr_A10_Pnd_Ws_Check_Nbr_A7.setValue(ldaFcpl876b.getPnd_Fcpl876b_Pnd_Pymnt_Check_Nbr());                                                         //Natural: ASSIGN #WS-CHECK-NBR-A7 := #PYMNT-CHECK-NBR
            //* RL 5-7-06
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //* RL 5-7-06
            pnd_Ws_Check_Nbr_A10.reset();                                                                                                                                 //Natural: RESET #WS-CHECK-NBR-A10
            //* RL 5-7-06
            //*  RL PAYEE MATCH
        }                                                                                                                                                                 //Natural: END-IF
        getReports().display(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//ADDRESSEE",                                                        //Natural: DISPLAY ( 2 ) ( HC = R ) '//ADDRESSEE' #ADDRESSEE ( HC = L ) '/BARCODE/ID' #BAR-ENV-ID ( HC = C ) 'PACKAGE/INTEG/RITY' #BAR-ENV-INTEGRITY-COUNTER ( EM = �������9 ) '/ENVELOPE/NUMBER' #FCPL876B.#BAR-ENVELOPES ( EM = -Z,ZZZ,ZZ9 ) '# OF/PAGES IN/ENVELOPE' #BAR-PAGES-IN-ENV ( EM = -Z,ZZZ,ZZ9 ) '//CHECK' #FCPL876B.#CHECK ( EM = NO/YES ) '/ORGN/CODE' #FCPL876B.CNTRCT-ORGN-CDE ( LC = � ) '/CONTRACT/NUMBER' CNTRCT-PPCN-NBR ( HC = L ) '/PAYEE/CODE' CNTRCT-PAYEE-CDE ( HC = L ) '/HOLD/CODE' CNTRCT-HOLD-CDE '/CHECK/NUMBER' #WS-CHECK-NBR-A10
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/BARCODE/ID",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"PACKAGE/INTEG/RITY",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter(), new ReportEditMask ("       9"),"/ENVELOPE/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"# OF/PAGES IN/ENVELOPE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"//CHECK",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean(), new ReportEditMask ("NO/YES"),"/ORGN/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde(), new FieldAttributes("LC=�"),"/CONTRACT/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PAYEE/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/HOLD/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde(),"/CHECK/NUMBER",
        		pnd_Ws_Check_Nbr_A10);
        if (Global.isEscape()) return;
    }
    private void sub_Display_Check_Range() throws Exception                                                                                                               //Natural: DISPLAY-CHECK-RANGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        pnd_Ws_Check_Nbr_N10.reset();                                                                                                                                     //Natural: RESET #WS-CHECK-NBR-N10
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: ASSIGN #WS-CHECK-NBR-N3 := FCPA110.START-CHECK-PREFIX-N3
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Pnd_Range_Check.getValue(1));                                                                            //Natural: ASSIGN #WS-CHECK-NBR-N7 := #RANGE-CHECK ( 1 )
        pnd_Ws_Pnd_Range_Begin.setValue(pnd_Ws_Check_Nbr_N10);                                                                                                            //Natural: ASSIGN #RANGE-BEGIN := #WS-CHECK-NBR-N10
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        //*  RL PAYEE MATCH
        pnd_Ws_Check_Nbr_N10.reset();                                                                                                                                     //Natural: RESET #WS-CHECK-NBR-N10
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: ASSIGN #WS-CHECK-NBR-N3 := FCPA110.START-CHECK-PREFIX-N3
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pnd_Ws_Pnd_Range_Check.getValue(2));                                                                            //Natural: ASSIGN #WS-CHECK-NBR-N7 := #RANGE-CHECK ( 2 )
        pnd_Ws_Pnd_Range_End.setValue(pnd_Ws_Check_Nbr_N10);                                                                                                              //Natural: ASSIGN #RANGE-END := #WS-CHECK-NBR-N10
        //*  RL PAYEE MATCH
        //*   ROXAN
        pnd_Ws_Check_Nbr_N10.reset();                                                                                                                                     //Natural: RESET #WS-CHECK-NBR-N10
        pnd_Ws_Pnd_Range_Break.setValue(false);                                                                                                                           //Natural: ASSIGN #RANGE-BREAK := FALSE
        if (condition(pnd_Ws_Pnd_Check_Cnt.getValue(1).notEquals(getZero())))                                                                                             //Natural: IF #CHECK-CNT ( 1 ) NE 0
        {
            //*  RL
            //*  RL
            pnd_Ws_Pnd_Range_Cnt.nadd(1);                                                                                                                                 //Natural: ADD 1 TO #RANGE-CNT
            getReports().display(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(11),"/",                                           //Natural: DISPLAY ( 1 ) ( HC = R ) 11T '/' #DISP-IND1 '/CHECK/RANGE' #RANGE-CNT '/ORGN/CODE' #PREV-CNTRCT-ORGN-CDE '/HELD/CHECKS' #PREV-HOLD-IND ( EM = NON�HELD/HELD HC = L ) 'STARTING/CHECK/NUMBER' #RANGE-BEGIN ( LC = � ) 'ENDING/CHECK/NUMBER' #RANGE-END '# OF/CHECKS/IN RANGE' #CHECK-CNT ( 1 )
            		pnd_Ws_Pnd_Disp_Ind1,"/CHECK/RANGE",
            		pnd_Ws_Pnd_Range_Cnt,"/ORGN/CODE",
            		pnd_Ws_Pnd_Prev_Cntrct_Orgn_Cde,"/HELD/CHECKS",
            		pnd_Ws_Pnd_Prev_Hold_Ind.getBoolean(), new ReportEditMask ("NON HELD/HELD"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
                "STARTING/CHECK/NUMBER",
            		pnd_Ws_Pnd_Range_Begin, new FieldAttributes("LC=�"),"ENDING/CHECK/NUMBER",
            		pnd_Ws_Pnd_Range_End,"# OF/CHECKS/IN RANGE",
            		pnd_Ws_Pnd_Check_Cnt.getValue(1));
            if (Global.isEscape()) return;
            pnd_Ws_Pnd_Range_Break.setValue(false);                                                                                                                       //Natural: ASSIGN #RANGE-BREAK := FALSE
            pnd_Ws_Pnd_Range_Start.setValue(true);                                                                                                                        //Natural: ASSIGN #RANGE-START := TRUE
            pnd_Ws_Pnd_Check_Cnt.getValue(2).nadd(pnd_Ws_Pnd_Check_Cnt.getValue(1));                                                                                      //Natural: ADD #CHECK-CNT ( 1 ) TO #CHECK-CNT ( 2 )
            pnd_Ws_Pnd_Disp_Ind1.setValue(pnd_Ws_Pnd_Disp_Ind);                                                                                                           //Natural: ASSIGN #DISP-IND1 := #DISP-IND
            pnd_Ws_Pnd_Check_Cnt.getValue(1).reset();                                                                                                                     //Natural: RESET #CHECK-CNT ( 1 ) #DISP-IND
            pnd_Ws_Pnd_Disp_Ind.reset();
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Void_Page() throws Exception                                                                                                                   //Natural: PRINT-VOID-PAGE
    {
        if (BLNatReinput.isReinput()) return;

        //* ********************************
        if (condition(pnd_Ws_Pnd_Void_Sep_Page.getBoolean() || ! (pnd_Restart_Pnd_Sysout_Ind.getBoolean())))                                                              //Natural: IF #VOID-SEP-PAGE OR NOT #SYSOUT-IND
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Ws_Pnd_Void_Sep_Page.setValue(true);                                                                                                                      //Natural: ASSIGN #VOID-SEP-PAGE := TRUE
            pdaFcpa803.getPnd_Fcpa803_Pnd_Prev_Xerox_Sub().setValue(-1);                                                                                                  //Natural: ASSIGN #FCPA803.#PREV-XEROX-SUB := -1
            DbsUtil.callnat(Fcpn803v.class , getCurrentProcessState(), pdaFcpa803.getPnd_Fcpa803());                                                                      //Natural: CALLNAT 'FCPN803V' USING #FCPA803
            if (condition(Global.isEscape())) return;
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Get_Start_Check_No() throws Exception                                                                                                                //Natural: GET-START-CHECK-NO
    {
        if (BLNatReinput.isReinput()) return;

        //*  --------------------------------- *
        //*  GET STARTING CHECK & SEQ NBRS FROM REF TABLE FOR POS-PAY/PAYEE MATCH
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("NEXT");                                                                                                        //Natural: MOVE 'NEXT' TO FCPA110.FCPA110-FUNCTION
        //*  RL
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().greater("0000")))                                                                                       //Natural: IF FCPA110.FCPA110-RETURN-CODE GT '0000'
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
            DbsUtil.terminate(53);  if (true) return;                                                                                                                     //Natural: TERMINATE 53
        }                                                                                                                                                                 //Natural: END-IF
        //* *#WS-CHECK-NBR-N3              := FCPA110.START-CHECK-PREFIX-N3
        //* *#WS-CHECK-NBR-N7              := FCPA110.START-CHECK-NO-N7
        //* *FCPA110.FCPA110-OLD-CHECK-NBR := #WS-CHECK-NBR-N10
    }
    private void sub_Update_Check_No() throws Exception                                                                                                                   //Natural: UPDATE-CHECK-NO
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************************************************
        pnd_Ws_Check_Nbr_N10.reset();                                                                                                                                     //Natural: RESET #WS-CHECK-NBR-N10
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N3.setValue(pdaFcpa110.getFcpa110_Start_Check_Prefix_N3());                                                                 //Natural: ASSIGN #WS-CHECK-NBR-N3 := FCPA110.START-CHECK-PREFIX-N3
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                   //Natural: ASSIGN #WS-CHECK-NBR-N7 := #FCPL876.PYMNT-CHECK-NBR
        pnd_Ws_New_Check_Nbr_N7.setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Nbr());                                                                                    //Natural: ASSIGN #WS-NEW-CHECK-NBR-N7 := #FCPL876.PYMNT-CHECK-NBR
        pdaFcpa110.getFcpa110_Fcpa110_New_Check_Nbr().setValue(pnd_Ws_Check_Nbr_N10);                                                                                     //Natural: ASSIGN FCPA110.FCPA110-NEW-CHECK-NBR := #WS-CHECK-NBR-N10
        pdaFcpa110.getFcpa110_Fcpa110_New_Sequence_Nbr().setValue(ldaFcpl876.getPnd_Fcpl876_Pymnt_Check_Seq_Nbr());                                                       //Natural: MOVE #FCPL876.PYMNT-CHECK-SEQ-NBR TO FCPA110.FCPA110-NEW-SEQUENCE-NBR
        pnd_Ws_Check_Nbr_N10_Pnd_Ws_Check_Nbr_N7.setValue(pdaFcpa110.getFcpa110_Start_Check_No_N7());                                                                     //Natural: ASSIGN #WS-CHECK-NBR-N7 := FCPA110.START-CHECK-NO-N7
        pnd_Ws_Old_Check_Nbr_N7.setValue(pdaFcpa110.getFcpa110_Start_Check_No_N7());                                                                                      //Natural: ASSIGN #WS-OLD-CHECK-NBR-N7 := FCPA110.START-CHECK-NO-N7
        pdaFcpa110.getFcpa110_Fcpa110_Old_Check_Nbr().setValue(pnd_Ws_Check_Nbr_N10);                                                                                     //Natural: ASSIGN FCPA110.FCPA110-OLD-CHECK-NBR := #WS-CHECK-NBR-N10
        pdaFcpa110.getFcpa110_Fcpa110_Function().setValue("UPDATE CHK SEQ");                                                                                              //Natural: MOVE 'UPDATE CHK SEQ' TO FCPA110.FCPA110-FUNCTION
        pdaFcpa110.getFcpa110_Fcpa110_Source_Code().setValue("P14A1");                                                                                                    //Natural: MOVE 'P14A1' TO FCPA110.FCPA110-SOURCE-CODE
        DbsUtil.callnat(Fcpn110.class , getCurrentProcessState(), pdaFcpa110.getFcpa110());                                                                               //Natural: CALLNAT 'FCPN110' FCPA110
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa110.getFcpa110_Fcpa110_Return_Code().equals("0000")))                                                                                        //Natural: IF FCPA110.FCPA110-RETURN-CODE EQ '0000'
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            getReports().write(0, "**",new TabSetting(15),pdaFcpa110.getFcpa110_Fcpa110_Return_Msg(), new ReportEditMask ("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"),new  //Natural: WRITE '**' 15T FCPA110.FCPA110-RETURN-MSG ( EM = X ( 61 ) ) 77T '***' / FCPA110.FCPA110-RETURN-CODE 77T '***'
                TabSetting(77),"***",NEWLINE,pdaFcpa110.getFcpa110_Fcpa110_Return_Code(),new TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        getReports().write(0, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new          //Natural: WRITE '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        getReports().newPage(new ReportSpecification(0));                                                                                                                 //Natural: NEWPAGE
        if (condition(Global.isEscape())){return;}
        getReports().write(0, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new  //Natural: WRITE // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    public AtItemEventHandler atEndEventRpt1 = (Object sender, EventArgs e) ->
    {
        String localMethod = "FCPP877|atEndEventRpt1";
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, NEWLINE,NEWLINE,NEWLINE,new TabSetting(11)," * - Break in check numbers",NEWLINE,new TabSetting(11),"** - Restart point. Also should be a break in check numbers"); //Natural: WRITE ( 1 ) /// 11T ' * - Break in check numbers' / 11T '** - Restart point. Also should be a break in check numbers'
                }                                                                                                                                                         //Natural: END-ENDPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    private void atBreakEventReadwork01() throws Exception {atBreakEventReadwork01(false);}
    private void atBreakEventReadwork01(boolean endOfData) throws Exception
    {
        boolean pnd_Ws_Pnd_Break_KeyIsBreak = pnd_Ws_Pnd_Break_Key.isBreak(endOfData);
        if (condition(pnd_Ws_Pnd_Break_KeyIsBreak))
        {
            pnd_Ws_Pnd_Range_Break.setValue(true);                                                                                                                        //Natural: ASSIGN #RANGE-BREAK := TRUE
        }                                                                                                                                                                 //Natural: END-BREAK
    }
    private void setupReports() throws Exception
    {
        Global.format(0, "PS=58 LS=80 ZP=ON");
        Global.format(1, "PS=58 LS=80 ZP=ON");
        Global.format(2, "PS=58 LS=132 ZP=ON");

        getReports().write(1, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(27),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(68),"PAGE:",getReports().getPageNumberDbs(1), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(34),"CHECK RANGES",new TabSetting(68),"REPORT: RPT1",NEWLINE,
            NEWLINE);
        getReports().write(2, pdaFcpa110.getFcpa110_Title(),ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask 
            ("HH:IIAP"),new TabSetting(53),"CONSOLIDATED PAYMENT SYSTEM",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(2), new ReportEditMask 
            ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new TabSetting(46),"ANNUITANT STATEMENT DETAIL CONTROL REPORT",new TabSetting(120),
            "REPORT: RPT2",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(2, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),"//ADDRESSEE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Addressee(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/BARCODE/ID",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Id(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Center),"PACKAGE/INTEG/RITY",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Env_Integrity_Counter(), new ReportEditMask ("       9"),"/ENVELOPE/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Envelopes(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"# OF/PAGES IN/ENVELOPE",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Bar_Pages_In_Env(), new ReportEditMask ("-Z,ZZZ,ZZ9"),"//CHECK",
        		ldaFcpl876b.getPnd_Fcpl876b_Pnd_Check().getBoolean(), new ReportEditMask ("NO/YES"),"/ORGN/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Orgn_Cde(), new FieldAttributes("LC=�"),"/CONTRACT/NUMBER",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Ppcn_Nbr(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/PAYEE/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Payee_Cde(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),"/HOLD/CODE",
        		ldaFcpl876b.getPnd_Fcpl876b_Cntrct_Hold_Cde(),"/CHECK/NUMBER",
        		pnd_Ws_Check_Nbr_A10);
        getReports().setDisplayColumns(1, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(11),"/",
        		pnd_Ws_Pnd_Disp_Ind1,"/CHECK/RANGE",
        		pnd_Ws_Pnd_Range_Cnt,"/ORGN/CODE",
        		pnd_Ws_Pnd_Prev_Cntrct_Orgn_Cde,"/HELD/CHECKS",
        		pnd_Ws_Pnd_Prev_Hold_Ind.getBoolean(), new ReportEditMask ("NON HELD/HELD"), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),
            "STARTING/CHECK/NUMBER",
        		pnd_Ws_Pnd_Range_Begin, new FieldAttributes("LC=�"),"ENDING/CHECK/NUMBER",
        		pnd_Ws_Pnd_Range_End,"# OF/CHECKS/IN RANGE",
        		pnd_Ws_Pnd_Check_Cnt);
    }
    private void CheckAtStartofData396() throws Exception
    {
        if (condition(getWorkFiles().getAtStartOfData()))
        {
                                                                                                                                                                          //Natural: PERFORM PRINT-VOID-PAGE
            sub_Print_Void_Page();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-START
    }
}
