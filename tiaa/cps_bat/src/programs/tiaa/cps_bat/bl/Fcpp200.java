/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:16:11 PM
**        * FROM NATURAL PROGRAM : Fcpp200
************************************************************
**        * FILE NAME            : Fcpp200.java
**        * CLASS NAME           : Fcpp200
**        * INSTANCE NAME        : Fcpp200
************************************************************
************************************************************************
* PROGRAM  : FCPP200
* SYSTEM   : CPS
* TITLE    : DUPLICATE PAYMENT CHECK FOR EFT AND CHECKS
* GENERATED: JUL 2004
* FUNCTION : COMPARE DATASET TO ITSELF TO FIND DUPLICATE PAYMENTS
*          : INPUT DATESET SHOULD CONTAIN MULTIPLE SETS OF PAYMENTS
*          : SORTED BY CONTRACT, PAYEE, PAYMENT NUMBER
*          : PAYMENT AMOUNT, AND ORIGIN CODE. EACH RECORD READ WILL
*          : BE COMPARED TO THE NEXT RECORD. IF A MATCH IS MADE
*          : AN EVALUATION WILL BE MADE TO DETERMINE IF THE PAYMENT IS
*          : IN FACT A DUPLICATE. - MCGEE 07-2004
* HISTORY
* 4/2017    : JJG - PIN EXPANSION CHANGES
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp200 extends BLNatBase
{
    // Data Areas
    private LdaFcpl190a ldaFcpl190a;
    private LdaFcpl190b ldaFcpl190b;
    private PdaCpwa100 pdaCpwa100;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField key1;

    private DbsGroup key1__R_Field_1;

    private DbsGroup key1_Key1c;
    private DbsField key1_Pymnt_Pay_Type_Req_Ind;
    private DbsField key1_Cntrct_Ppcn_Nbr;
    private DbsField key1_Cntrct_Payee_Cde;
    private DbsField key1_Cntrct_Unq_Id_Nbr;
    private DbsField key1_Cntrct_Orgn_Cde;
    private DbsField key1_Pymnt_Prcss_Seq_Nbr;
    private DbsField key1_Pymnt_Check_Dte;
    private DbsField key1_Pymnt_Eft_Dte;
    private DbsField key1_Pymnt_Acctg_Dte;
    private DbsField key1_Pymnt_Intrfce_Dte;
    private DbsField read_Count;
    private DbsField key2;

    private DbsGroup key2__R_Field_2;

    private DbsGroup key2_Key2c;
    private DbsField key2_Pymnt_Pay_Type_Req_Ind;
    private DbsField key2_Cntrct_Ppcn_Nbr;
    private DbsField key2_Cntrct_Payee_Cde;
    private DbsField key2_Cntrct_Unq_Id_Nbr;
    private DbsField key2_Cntrct_Orgn_Cde;
    private DbsField key2_Pymnt_Prcss_Seq_Nbr;
    private DbsField key2_Pymnt_Check_Dte;
    private DbsField key2_Pymnt_Eft_Dte;
    private DbsField key2_Pymnt_Acctg_Dte;
    private DbsField key2_Pymnt_Intrfce_Dte;
    private DbsField eof;
    private DbsField pnd_I;
    private DbsField dup;
    private DbsField pnd_Count_Dte;
    private DbsField pnd_Max_Date;
    private DbsField pnd_Err_Msg;
    private DbsField pnd_Err_Code;
    private DbsField pnd_Check_Date;
    private DbsField pnd_Compare_Date;

    private DbsGroup table_Of_Duplicates;
    private DbsField table_Of_Duplicates_Tbl_Cntrct_Ppcn_Nbr;
    private DbsField table_Of_Duplicates_Tbl_Cntrct_Payee_Cde;
    private DbsField table_Of_Duplicates_Tbl_Cntrct_Unq_Id_Nbr;
    private DbsField table_Of_Duplicates_Tbl_Cntrct_Cref_Nbr;
    private DbsField table_Of_Duplicates_Tbl_Count;
    private DbsField table_Of_Duplicates_Tbl_Eft_Chk;
    private DbsField table_Of_Duplicates_Tbl_Cntrct_Type_Cde;
    private DbsField table_Of_Duplicates_Tbl_Cntrct_Lob_Cde;
    private DbsField table_Of_Duplicates_Tbl_Pymnt_Check_Dte;
    private DbsField table_Of_Duplicates_Tbl_Pymnt_Eft_Dte;
    private DbsField table_Of_Duplicates_Tbl_Pymnt_Check_Amt;
    private DbsField table_Max_Dup;
    private DbsField duplicates;
    private DbsField table_Maxed_Out;
    private DbsField pnd_Duplicate_Controls;

    private DbsGroup pnd_Duplicate_Controls__R_Field_3;
    private DbsField pnd_Duplicate_Controls_Line_1;

    private DbsGroup pnd_Duplicate_Controls__R_Field_4;
    private DbsField pnd_Duplicate_Controls_Term_Lit1;
    private DbsField pnd_Duplicate_Controls_Term_Answer;
    private DbsField pnd_Duplicate_Controls_Line_2;

    private DbsGroup pnd_Duplicate_Controls__R_Field_5;
    private DbsField pnd_Duplicate_Controls_Term_Lit2;
    private DbsField pnd_Duplicate_Controls_Term_Count;
    private DbsField pnd_Duplicate_Controls_Line_3;

    private DbsGroup pnd_Duplicate_Controls__R_Field_6;
    private DbsField pnd_Duplicate_Controls_Term_Lit3;
    private DbsField pnd_Duplicate_Controls_Term_Percent;
    private DbsField above_Threshold_Count;
    private DbsField above_Threshold_Percent;
    private DbsField calculated_Percent_Of_Dups;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        ldaFcpl190a = new LdaFcpl190a();
        registerRecord(ldaFcpl190a);
        ldaFcpl190b = new LdaFcpl190b();
        registerRecord(ldaFcpl190b);
        localVariables = new DbsRecord();
        pdaCpwa100 = new PdaCpwa100(localVariables);

        // Local Variables
        key1 = localVariables.newFieldInRecord("key1", "KEY1", FieldType.STRING, 100);

        key1__R_Field_1 = localVariables.newGroupInRecord("key1__R_Field_1", "REDEFINE", key1);

        key1_Key1c = key1__R_Field_1.newGroupInGroup("key1_Key1c", "KEY1C");
        key1_Pymnt_Pay_Type_Req_Ind = key1_Key1c.newFieldInGroup("key1_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.STRING, 1);
        key1_Cntrct_Ppcn_Nbr = key1_Key1c.newFieldInGroup("key1_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        key1_Cntrct_Payee_Cde = key1_Key1c.newFieldInGroup("key1_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        key1_Cntrct_Unq_Id_Nbr = key1_Key1c.newFieldInGroup("key1_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        key1_Cntrct_Orgn_Cde = key1_Key1c.newFieldInGroup("key1_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        key1_Pymnt_Prcss_Seq_Nbr = key1_Key1c.newFieldInGroup("key1_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        key1_Pymnt_Check_Dte = key1_Key1c.newFieldInGroup("key1_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        key1_Pymnt_Eft_Dte = key1_Key1c.newFieldInGroup("key1_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        key1_Pymnt_Acctg_Dte = key1_Key1c.newFieldInGroup("key1_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        key1_Pymnt_Intrfce_Dte = key1_Key1c.newFieldInGroup("key1_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        read_Count = localVariables.newFieldInRecord("read_Count", "READ-COUNT", FieldType.NUMERIC, 5);
        key2 = localVariables.newFieldInRecord("key2", "KEY2", FieldType.STRING, 100);

        key2__R_Field_2 = localVariables.newGroupInRecord("key2__R_Field_2", "REDEFINE", key2);

        key2_Key2c = key2__R_Field_2.newGroupInGroup("key2_Key2c", "KEY2C");
        key2_Pymnt_Pay_Type_Req_Ind = key2_Key2c.newFieldInGroup("key2_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.STRING, 1);
        key2_Cntrct_Ppcn_Nbr = key2_Key2c.newFieldInGroup("key2_Cntrct_Ppcn_Nbr", "CNTRCT-PPCN-NBR", FieldType.STRING, 10);
        key2_Cntrct_Payee_Cde = key2_Key2c.newFieldInGroup("key2_Cntrct_Payee_Cde", "CNTRCT-PAYEE-CDE", FieldType.STRING, 4);
        key2_Cntrct_Unq_Id_Nbr = key2_Key2c.newFieldInGroup("key2_Cntrct_Unq_Id_Nbr", "CNTRCT-UNQ-ID-NBR", FieldType.NUMERIC, 12);
        key2_Cntrct_Orgn_Cde = key2_Key2c.newFieldInGroup("key2_Cntrct_Orgn_Cde", "CNTRCT-ORGN-CDE", FieldType.STRING, 2);
        key2_Pymnt_Prcss_Seq_Nbr = key2_Key2c.newFieldInGroup("key2_Pymnt_Prcss_Seq_Nbr", "PYMNT-PRCSS-SEQ-NBR", FieldType.NUMERIC, 9);
        key2_Pymnt_Check_Dte = key2_Key2c.newFieldInGroup("key2_Pymnt_Check_Dte", "PYMNT-CHECK-DTE", FieldType.DATE);
        key2_Pymnt_Eft_Dte = key2_Key2c.newFieldInGroup("key2_Pymnt_Eft_Dte", "PYMNT-EFT-DTE", FieldType.DATE);
        key2_Pymnt_Acctg_Dte = key2_Key2c.newFieldInGroup("key2_Pymnt_Acctg_Dte", "PYMNT-ACCTG-DTE", FieldType.DATE);
        key2_Pymnt_Intrfce_Dte = key2_Key2c.newFieldInGroup("key2_Pymnt_Intrfce_Dte", "PYMNT-INTRFCE-DTE", FieldType.DATE);
        eof = localVariables.newFieldInRecord("eof", "EOF", FieldType.BOOLEAN, 1);
        pnd_I = localVariables.newFieldInRecord("pnd_I", "#I", FieldType.NUMERIC, 5);
        dup = localVariables.newFieldInRecord("dup", "DUP", FieldType.NUMERIC, 5);
        pnd_Count_Dte = localVariables.newFieldInRecord("pnd_Count_Dte", "#COUNT-DTE", FieldType.NUMERIC, 3);
        pnd_Max_Date = localVariables.newFieldInRecord("pnd_Max_Date", "#MAX-DATE", FieldType.NUMERIC, 5);
        pnd_Err_Msg = localVariables.newFieldArrayInRecord("pnd_Err_Msg", "#ERR-MSG", FieldType.STRING, 80, new DbsArrayController(1, 2));
        pnd_Err_Code = localVariables.newFieldInRecord("pnd_Err_Code", "#ERR-CODE", FieldType.NUMERIC, 3);
        pnd_Check_Date = localVariables.newFieldArrayInRecord("pnd_Check_Date", "#CHECK-DATE", FieldType.STRING, 8, new DbsArrayController(1, 365));
        pnd_Compare_Date = localVariables.newFieldInRecord("pnd_Compare_Date", "#COMPARE-DATE", FieldType.STRING, 8);

        table_Of_Duplicates = localVariables.newGroupInRecord("table_Of_Duplicates", "TABLE-OF-DUPLICATES");
        table_Of_Duplicates_Tbl_Cntrct_Ppcn_Nbr = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Cntrct_Ppcn_Nbr", "TBL-CNTRCT-PPCN-NBR", 
            FieldType.STRING, 10, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Cntrct_Payee_Cde = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Cntrct_Payee_Cde", "TBL-CNTRCT-PAYEE-CDE", 
            FieldType.STRING, 4, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Cntrct_Unq_Id_Nbr = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Cntrct_Unq_Id_Nbr", "TBL-CNTRCT-UNQ-ID-NBR", 
            FieldType.NUMERIC, 12, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Cntrct_Cref_Nbr = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Cntrct_Cref_Nbr", "TBL-CNTRCT-CREF-NBR", 
            FieldType.STRING, 10, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Count = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Count", "TBL-COUNT", FieldType.NUMERIC, 5, new 
            DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Eft_Chk = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Eft_Chk", "TBL-EFT-CHK", FieldType.STRING, 
            3, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Cntrct_Type_Cde = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Cntrct_Type_Cde", "TBL-CNTRCT-TYPE-CDE", 
            FieldType.STRING, 2, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Cntrct_Lob_Cde = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Cntrct_Lob_Cde", "TBL-CNTRCT-LOB-CDE", 
            FieldType.STRING, 4, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Pymnt_Check_Dte = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Pymnt_Check_Dte", "TBL-PYMNT-CHECK-DTE", 
            FieldType.DATE, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Pymnt_Eft_Dte = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Pymnt_Eft_Dte", "TBL-PYMNT-EFT-DTE", 
            FieldType.DATE, new DbsArrayController(1, 500));
        table_Of_Duplicates_Tbl_Pymnt_Check_Amt = table_Of_Duplicates.newFieldArrayInGroup("table_Of_Duplicates_Tbl_Pymnt_Check_Amt", "TBL-PYMNT-CHECK-AMT", 
            FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 500));
        table_Max_Dup = localVariables.newFieldInRecord("table_Max_Dup", "TABLE-MAX-DUP", FieldType.NUMERIC, 3);
        duplicates = localVariables.newFieldInRecord("duplicates", "DUPLICATES", FieldType.BOOLEAN, 1);
        table_Maxed_Out = localVariables.newFieldInRecord("table_Maxed_Out", "TABLE-MAXED-OUT", FieldType.BOOLEAN, 1);
        pnd_Duplicate_Controls = localVariables.newFieldInRecord("pnd_Duplicate_Controls", "#DUPLICATE-CONTROLS", FieldType.STRING, 250);

        pnd_Duplicate_Controls__R_Field_3 = localVariables.newGroupInRecord("pnd_Duplicate_Controls__R_Field_3", "REDEFINE", pnd_Duplicate_Controls);
        pnd_Duplicate_Controls_Line_1 = pnd_Duplicate_Controls__R_Field_3.newFieldInGroup("pnd_Duplicate_Controls_Line_1", "LINE-1", FieldType.STRING, 
            75);

        pnd_Duplicate_Controls__R_Field_4 = pnd_Duplicate_Controls__R_Field_3.newGroupInGroup("pnd_Duplicate_Controls__R_Field_4", "REDEFINE", pnd_Duplicate_Controls_Line_1);
        pnd_Duplicate_Controls_Term_Lit1 = pnd_Duplicate_Controls__R_Field_4.newFieldInGroup("pnd_Duplicate_Controls_Term_Lit1", "TERM-LIT1", FieldType.STRING, 
            40);
        pnd_Duplicate_Controls_Term_Answer = pnd_Duplicate_Controls__R_Field_4.newFieldInGroup("pnd_Duplicate_Controls_Term_Answer", "TERM-ANSWER", FieldType.STRING, 
            3);
        pnd_Duplicate_Controls_Line_2 = pnd_Duplicate_Controls__R_Field_3.newFieldInGroup("pnd_Duplicate_Controls_Line_2", "LINE-2", FieldType.STRING, 
            75);

        pnd_Duplicate_Controls__R_Field_5 = pnd_Duplicate_Controls__R_Field_3.newGroupInGroup("pnd_Duplicate_Controls__R_Field_5", "REDEFINE", pnd_Duplicate_Controls_Line_2);
        pnd_Duplicate_Controls_Term_Lit2 = pnd_Duplicate_Controls__R_Field_5.newFieldInGroup("pnd_Duplicate_Controls_Term_Lit2", "TERM-LIT2", FieldType.STRING, 
            40);
        pnd_Duplicate_Controls_Term_Count = pnd_Duplicate_Controls__R_Field_5.newFieldInGroup("pnd_Duplicate_Controls_Term_Count", "TERM-COUNT", FieldType.NUMERIC, 
            4);
        pnd_Duplicate_Controls_Line_3 = pnd_Duplicate_Controls__R_Field_3.newFieldInGroup("pnd_Duplicate_Controls_Line_3", "LINE-3", FieldType.STRING, 
            75);

        pnd_Duplicate_Controls__R_Field_6 = pnd_Duplicate_Controls__R_Field_3.newGroupInGroup("pnd_Duplicate_Controls__R_Field_6", "REDEFINE", pnd_Duplicate_Controls_Line_3);
        pnd_Duplicate_Controls_Term_Lit3 = pnd_Duplicate_Controls__R_Field_6.newFieldInGroup("pnd_Duplicate_Controls_Term_Lit3", "TERM-LIT3", FieldType.STRING, 
            40);
        pnd_Duplicate_Controls_Term_Percent = pnd_Duplicate_Controls__R_Field_6.newFieldInGroup("pnd_Duplicate_Controls_Term_Percent", "TERM-PERCENT", 
            FieldType.NUMERIC, 5, 2);
        above_Threshold_Count = localVariables.newFieldInRecord("above_Threshold_Count", "ABOVE-THRESHOLD-COUNT", FieldType.BOOLEAN, 1);
        above_Threshold_Percent = localVariables.newFieldInRecord("above_Threshold_Percent", "ABOVE-THRESHOLD-PERCENT", FieldType.BOOLEAN, 1);
        calculated_Percent_Of_Dups = localVariables.newFieldInRecord("calculated_Percent_Of_Dups", "CALCULATED-PERCENT-OF-DUPS", FieldType.NUMERIC, 5, 
            2);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl190a.initializeValues();
        ldaFcpl190b.initializeValues();

        localVariables.reset();
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp200() throws Exception
    {
        super("Fcpp200");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        OnErrorManager.pushEvent("FCPP200", onError);
        getReports().atTopOfPage(atTopEventRpt1, 1);
        setupReports();
        //*  - INITS --------------------------------------------------------------
        pnd_Max_Date.nadd(1);                                                                                                                                             //Natural: FORMAT ( 1 ) LS = 133 PS = 60;//Natural: FORMAT ( 0 ) LS = 133 PS = 60;//Natural: ADD 1 TO #MAX-DATE
        eof.setValue(false);                                                                                                                                              //Natural: MOVE FALSE TO EOF
        table_Maxed_Out.setValue(false);                                                                                                                                  //Natural: MOVE FALSE TO TABLE-MAXED-OUT
        table_Max_Dup.setValue(500);                                                                                                                                      //Natural: MOVE 500 TO TABLE-MAX-DUP
        //*  - HEADERS ------------------------------------------------------------
        //*                                                                                                                                                               //Natural: AT TOP OF PAGE ( 1 )
        //*  ----------------------------------------------------------------------
        //*   MAIN PROGRAM LOGIC  *
        //*  ----------------------------------------------------------------------
        //*  -- SET UP FIRST COMPARE
                                                                                                                                                                          //Natural: PERFORM GET-CONTROLS
        sub_Get_Controls();
        if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM READ-WORK-ONCE
        sub_Read_Work_Once();
        if (condition(Global.isEscape())) {return;}
        ldaFcpl190a.getPnd_Rpt_Ext().setValuesByName(ldaFcpl190b.getPnd_Rpt_Ext2());                                                                                      //Natural: MOVE BY NAME #RPT-EXT2 TO #RPT-EXT
        key1_Key1c.setValuesByName(ldaFcpl190a.getPnd_Rpt_Ext());                                                                                                         //Natural: MOVE BY NAME #RPT-EXT TO KEY1C
        //*  -- CYCLE FILE AND COMPARE
        REPEAT01:                                                                                                                                                         //Natural: REPEAT
        while (condition(whileTrue))
        {
            if (condition(eof.getBoolean())) {break;}                                                                                                                     //Natural: UNTIL EOF
                                                                                                                                                                          //Natural: PERFORM READ-WORK-ONCE
            sub_Read_Work_Once();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(eof.getBoolean()))                                                                                                                              //Natural: IF EOF
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            key2_Key2c.setValuesByName(ldaFcpl190b.getPnd_Rpt_Ext2());                                                                                                    //Natural: MOVE BY NAME #RPT-EXT2 TO KEY2C
            if (condition(key1.equals(key2)))                                                                                                                             //Natural: IF KEY1 EQ KEY2
            {
                                                                                                                                                                          //Natural: PERFORM POTENTIAL-DUPLICATE
                sub_Potential_Duplicate();
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
            //*  STORE DATES IN FILE FOR REPORTS
                                                                                                                                                                          //Natural: PERFORM DATES-PROCESSED
            sub_Dates_Processed();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            //*  SHIFT FILE AND KEYS TO NEXT
            ldaFcpl190a.getPnd_Rpt_Ext().setValuesByName(ldaFcpl190b.getPnd_Rpt_Ext2());                                                                                  //Natural: MOVE BY NAME #RPT-EXT2 TO #RPT-EXT
            key1_Key1c.setValuesByName(ldaFcpl190b.getPnd_Rpt_Ext2());                                                                                                    //Natural: MOVE BY NAME #RPT-EXT2 TO KEY1C
        }                                                                                                                                                                 //Natural: END-REPEAT
        if (Global.isEscape()) return;
        if (condition(eof.getBoolean() && read_Count.less(2)))                                                                                                            //Natural: IF EOF AND READ-COUNT LT 2
        {
            pnd_Err_Msg.getValue(1).setValue("P1426CPD - COMPARE FILES EMPTY - NO RECENT PAYMENTS?.");                                                                    //Natural: MOVE 'P1426CPD - COMPARE FILES EMPTY - NO RECENT PAYMENTS?.' TO #ERR-MSG ( 1 )
            pnd_Err_Msg.getValue(2).setValue("PMS.ANN.P1420CPD.CONVERT.ALL.SORT IS EMPTY");                                                                               //Natural: MOVE 'PMS.ANN.P1420CPD.CONVERT.ALL.SORT IS EMPTY' TO #ERR-MSG ( 2 )
            pnd_Err_Code.setValue(10);                                                                                                                                    //Natural: MOVE 10 TO #ERR-CODE
                                                                                                                                                                          //Natural: PERFORM CONTROLLED-ABEND
            sub_Controlled_Abend();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  -- PUBLISH RESULTS
                                                                                                                                                                          //Natural: PERFORM CALCULATED-THRESHOLD
        sub_Calculated_Threshold();
        if (condition(Global.isEscape())) {return;}
        if (condition(duplicates.getBoolean()))                                                                                                                           //Natural: IF DUPLICATES
        {
                                                                                                                                                                          //Natural: PERFORM CONTROLLED-ABEND
            sub_Controlled_Abend();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-DATES-PROCESSED
            sub_Write_Dates_Processed();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-THRESHOLD
            sub_Write_Threshold();
            if (condition(Global.isEscape())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CALCULATED-THRESHOLD
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: READ-WORK-ONCE
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: POTENTIAL-DUPLICATE
        //* * --------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DISPLAY-RECORDS
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DATES-PROCESSED
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DATES-PROCESSED
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-THRESHOLD
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-DUPLICATES
        //* * 'Payment/EFT Date'     TBL-PYMNT-EFT-DTE     (#I)(EM=YYYY/MM/DD)
        //* * EFT SEEMS TO BE EMPTY
        //*  ----------------------------------------------------------------------
        //*   SYSTEM ERROR HANDLING
        //* * ---------------------------------------------------------------------                                                                                       //Natural: ON ERROR
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: SYSTEM-ABEND
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CONTROLLED-ABEND
        //*  ----------------------------------------------------------------------
        //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GET-CONTROLS
    }
    private void sub_Calculated_Threshold() throws Exception                                                                                                              //Natural: CALCULATED-THRESHOLD
    {
        if (BLNatReinput.isReinput()) return;

        above_Threshold_Percent.setValue(false);                                                                                                                          //Natural: MOVE FALSE TO ABOVE-THRESHOLD-PERCENT
        above_Threshold_Count.setValue(false);                                                                                                                            //Natural: MOVE FALSE TO ABOVE-THRESHOLD-COUNT
        calculated_Percent_Of_Dups.compute(new ComputeParameters(false, calculated_Percent_Of_Dups), dup.divide(read_Count).multiply(100));                               //Natural: COMPUTE CALCULATED-PERCENT-OF-DUPS = DUP / READ-COUNT * 100
        getReports().write(0, "=",calculated_Percent_Of_Dups,"=",pnd_Duplicate_Controls_Term_Percent);                                                                    //Natural: WRITE '='CALCULATED-PERCENT-OF-DUPS '=' TERM-PERCENT
        if (Global.isEscape()) return;
        //*  TERMINATE THRESHOLD
        if (condition(calculated_Percent_Of_Dups.greater(pnd_Duplicate_Controls_Term_Percent)))                                                                           //Natural: IF CALCULATED-PERCENT-OF-DUPS GT TERM-PERCENT
        {
            above_Threshold_Percent.setValue(true);                                                                                                                       //Natural: MOVE TRUE TO ABOVE-THRESHOLD-PERCENT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(dup.greater(pnd_Duplicate_Controls_Term_Count)))                                                                                                    //Natural: IF DUP GT TERM-COUNT
        {
            above_Threshold_Count.setValue(true);                                                                                                                         //Natural: MOVE TRUE TO ABOVE-THRESHOLD-COUNT
        }                                                                                                                                                                 //Natural: END-IF
        //*  CALCULATED-THRESHOLD
    }
    private void sub_Read_Work_Once() throws Exception                                                                                                                    //Natural: READ-WORK-ONCE
    {
        if (BLNatReinput.isReinput()) return;

        getWorkFiles().read(1, ldaFcpl190b.getPnd_Rpt_Ext2());                                                                                                            //Natural: READ WORK 1 ONCE #RPT-EXT2
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
            eof.setValue(true);                                                                                                                                           //Natural: MOVE TRUE TO EOF
        }                                                                                                                                                                 //Natural: END-ENDFILE
        read_Count.nadd(1);                                                                                                                                               //Natural: ADD 1 TO READ-COUNT
    }
    private void sub_Potential_Duplicate() throws Exception                                                                                                               //Natural: POTENTIAL-DUPLICATE
    {
        if (BLNatReinput.isReinput()) return;

        duplicates.setValue(true);                                                                                                                                        //Natural: MOVE TRUE TO DUPLICATES
        //* * - LOAD TO TABLE
        if (condition(dup.less(table_Max_Dup)))                                                                                                                           //Natural: IF DUP LT TABLE-MAX-DUP
        {
            dup.nadd(1);                                                                                                                                                  //Natural: ADD 1 TO DUP
            table_Of_Duplicates_Tbl_Cntrct_Ppcn_Nbr.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr());                                                 //Natural: MOVE #RPT-EXT.CNTRCT-PPCN-NBR TO TBL-CNTRCT-PPCN-NBR ( DUP )
            table_Of_Duplicates_Tbl_Cntrct_Payee_Cde.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde());                                               //Natural: MOVE #RPT-EXT.CNTRCT-PAYEE-CDE TO TBL-CNTRCT-PAYEE-CDE ( DUP )
            table_Of_Duplicates_Tbl_Cntrct_Unq_Id_Nbr.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Unq_Id_Nbr());                                             //Natural: MOVE #RPT-EXT.CNTRCT-UNQ-ID-NBR TO TBL-CNTRCT-UNQ-ID-NBR ( DUP )
            table_Of_Duplicates_Tbl_Cntrct_Cref_Nbr.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cref_Nbr());                                                 //Natural: MOVE #RPT-EXT.CNTRCT-CREF-NBR TO TBL-CNTRCT-CREF-NBR ( DUP )
            table_Of_Duplicates_Tbl_Cntrct_Type_Cde.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Type_Cde());                                                 //Natural: MOVE #RPT-EXT.CNTRCT-TYPE-CDE TO TBL-CNTRCT-TYPE-CDE ( DUP )
            table_Of_Duplicates_Tbl_Cntrct_Lob_Cde.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Lob_Cde());                                                   //Natural: MOVE #RPT-EXT.CNTRCT-LOB-CDE TO TBL-CNTRCT-LOB-CDE ( DUP )
            table_Of_Duplicates_Tbl_Pymnt_Check_Dte.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte());                                                 //Natural: MOVE #RPT-EXT.PYMNT-CHECK-DTE TO TBL-PYMNT-CHECK-DTE ( DUP )
            table_Of_Duplicates_Tbl_Pymnt_Eft_Dte.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Dte());                                                     //Natural: MOVE #RPT-EXT.PYMNT-EFT-DTE TO TBL-PYMNT-EFT-DTE ( DUP )
            table_Of_Duplicates_Tbl_Pymnt_Check_Amt.getValue(dup).setValue(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt());                                                 //Natural: MOVE #RPT-EXT.PYMNT-CHECK-AMT TO TBL-PYMNT-CHECK-AMT ( DUP )
            table_Of_Duplicates_Tbl_Count.getValue(dup).setValue(read_Count);                                                                                             //Natural: MOVE READ-COUNT TO TBL-COUNT ( DUP )
            short decideConditionsMet617 = 0;                                                                                                                             //Natural: DECIDE ON FIRST #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
            if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(1))))
            {
                decideConditionsMet617++;
                table_Of_Duplicates_Tbl_Eft_Chk.getValue(dup).setValue("CHK");                                                                                            //Natural: MOVE 'CHK' TO TBL-EFT-CHK ( DUP )
            }                                                                                                                                                             //Natural: VALUE 2
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet617++;
                table_Of_Duplicates_Tbl_Eft_Chk.getValue(dup).setValue("EFT");                                                                                            //Natural: MOVE 'EFT' TO TBL-EFT-CHK ( DUP )
            }                                                                                                                                                             //Natural: VALUE 3
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(3))))
            {
                decideConditionsMet617++;
                table_Of_Duplicates_Tbl_Eft_Chk.getValue(dup).setValue("GLB");                                                                                            //Natural: MOVE 'GLB' TO TBL-EFT-CHK ( DUP )
            }                                                                                                                                                             //Natural: VALUE 8
            else if (condition((ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind().equals(8))))
            {
                decideConditionsMet617++;
                table_Of_Duplicates_Tbl_Eft_Chk.getValue(dup).setValue("ROL");                                                                                            //Natural: MOVE 'ROL' TO TBL-EFT-CHK ( DUP )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                table_Of_Duplicates_Tbl_Eft_Chk.getValue(dup).setValue("OTH");                                                                                            //Natural: MOVE 'OTH' TO TBL-EFT-CHK ( DUP )
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            //*  ISSUE MESSAGE
            if (condition(! (table_Maxed_Out.getBoolean())))                                                                                                              //Natural: IF NOT TABLE-MAXED-OUT
            {
                getReports().write(1, ReportOption.NOTITLE,"MORE THAN",table_Max_Dup,"DUPLICATES");                                                                       //Natural: WRITE ( 1 ) 'MORE THAN' TABLE-MAX-DUP 'DUPLICATES'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"MORE THAN",table_Max_Dup,"DUPLICATES");                                                                       //Natural: WRITE ( 1 ) 'MORE THAN' TABLE-MAX-DUP 'DUPLICATES'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"MORE THAN",table_Max_Dup,"DUPLICATES");                                                                       //Natural: WRITE ( 1 ) 'MORE THAN' TABLE-MAX-DUP 'DUPLICATES'
                if (Global.isEscape()) return;
                getReports().write(1, ReportOption.NOTITLE,"MORE THAN",table_Max_Dup,"DUPLICATES");                                                                       //Natural: WRITE ( 1 ) 'MORE THAN' TABLE-MAX-DUP 'DUPLICATES'
                if (Global.isEscape()) return;
                table_Maxed_Out.setValue(true);                                                                                                                           //Natural: MOVE TRUE TO TABLE-MAXED-OUT
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Display_Records() throws Exception                                                                                                                   //Natural: DISPLAY-RECORDS
    {
        if (BLNatReinput.isReinput()) return;

        //* (A10)
        //* (A4)
        //* (A2)
        //* (A1)
        //* (N12.0)
        //* (A10)
        //* (A4)
        //* (N9.0)
        //* (D)
        //* (D)
        //* (N7.0)
        //* (N9)
        //* (P7.2)
        //* (D)
        //* (P7.0)
        //* (N1.0)
        //* (A14)
        //* (A2)
        getReports().write(0, NEWLINE,NEWLINE,"COMPARED RECORDS",NEWLINE,"RECORD",read_Count,NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Ppcn_Nbr(),                    //Natural: WRITE ( 0 ) // 'COMPARED RECORDS' / 'RECORD' READ-COUNT / '=' #RPT-EXT.CNTRCT-PPCN-NBR / '=' #RPT-EXT.CNTRCT-PAYEE-CDE / '=' #RPT-EXT.CNTRCT-ORGN-CDE / '=' #RPT-EXT.CNTRCT-PYMNT-TYPE-IND / '=' #RPT-EXT.CNTRCT-UNQ-ID-NBR / '=' #RPT-EXT.CNTRCT-CREF-NBR / '=' #RPT-EXT.CNTRCT-HOLD-CDE / '=' #RPT-EXT.ANNT-SOC-SEC-NBR / '=' #RPT-EXT.PYMNT-CHECK-DTE / '=' #RPT-EXT.PYMNT-EFT-DTE / '=' #RPT-EXT.PYMNT-CHECK-NBR / '=' #RPT-EXT.PYMNT-PRCSS-SEQ-NBR / '=' #RPT-EXT.PYMNT-CHECK-AMT / '=' #RPT-EXT.PYMNT-SETTLMNT-DTE / '=' #RPT-EXT.PYMNT-CHECK-SEQ-NBR / '=' #RPT-EXT.PYMNT-PAY-TYPE-REQ-IND / '=' #RPT-EXT.CNTRCT-CMBN-NBR / '=' #RPT-EXT.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Payee_Cde(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Orgn_Cde(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind(),
            NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Unq_Id_Nbr(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cref_Nbr(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Hold_Cde(),
            NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Annt_Soc_Sec_Nbr(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Dte(),
            NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Nbr(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Prcss_Seq_Nbr(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Amt(),
            NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Settlmnt_Dte(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Seq_Nbr(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Pay_Type_Req_Ind(),
            NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cmbn_Nbr(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Actvty_Cde());
        if (Global.isEscape()) return;
        //* (A48)
        //* (N9.0)
        //* (A21)
        //* (A1)
        //* (N7.0)
        //* (A1)
        //* (N8)
        //* (D)
        //* (D)
        //* (N8)
        getReports().write(0, NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Ph_Name(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Transit_Id(),NEWLINE,"=",                 //Natural: WRITE ( 0 ) / '=' #RPT-EXT.PH-NAME / '=' #RPT-EXT.PYMNT-EFT-TRANSIT-ID / '=' #RPT-EXT.PYMNT-EFT-ACCT-NBR / '=' #RPT-EXT.PYMNT-CHK-SAV-IND / '=' #RPT-EXT.PYMNT-CHECK-SCRTY-NBR / '=' #RPT-EXT.CNTRCT-CANCEL-RDRW-IND / '=' #RPT-EXT.CNTRCT-INVRSE-DTE / '=' #RPT-EXT.PYMNT-ACCTG-DTE / '=' #RPT-EXT.PYMNT-INTRFCE-DTE / '=' #RPT-EXT.CNR-ORGNL-INVRSE-DTE
            ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Acct_Nbr(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Chk_Sav_Ind(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Scrty_Nbr(),
            NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Cancel_Rdrw_Ind(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Invrse_Dte(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Acctg_Dte(),
            NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Intrfce_Dte(),NEWLINE,"=",ldaFcpl190a.getPnd_Rpt_Ext_Cnr_Orgnl_Invrse_Dte());
        if (Global.isEscape()) return;
        //*  DISPLAY-RECORDS
    }
    private void sub_Dates_Processed() throws Exception                                                                                                                   //Natural: DATES-PROCESSED
    {
        if (BLNatReinput.isReinput()) return;

        pnd_Count_Dte.setValue(0);                                                                                                                                        //Natural: MOVE 0 TO #COUNT-DTE
        if (condition(ldaFcpl190a.getPnd_Rpt_Ext_Cntrct_Pymnt_Type_Ind().notEquals("2")))                                                                                 //Natural: IF #RPT-EXT.CNTRCT-PYMNT-TYPE-IND NE '2'
        {
            pnd_Compare_Date.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));                                                 //Natural: MOVE EDITED #RPT-EXT.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #COMPARE-DATE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Compare_Date.setValueEdited(ldaFcpl190a.getPnd_Rpt_Ext_Pymnt_Eft_Dte(),new ReportEditMask("YYYYMMDD"));                                                   //Natural: MOVE EDITED #RPT-EXT.PYMNT-EFT-DTE ( EM = YYYYMMDD ) TO #COMPARE-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        DbsUtil.examine(new ExamineSource(pnd_Check_Date.getValue("*")), new ExamineSearch(pnd_Compare_Date), new ExamineGivingNumber(pnd_Count_Dte));                    //Natural: EXAMINE #CHECK-DATE ( * ) FOR #COMPARE-DATE GIVING #COUNT-DTE
        if (condition(pnd_Count_Dte.equals(getZero())))                                                                                                                   //Natural: IF #COUNT-DTE = 0
        {
            pnd_Check_Date.getValue(pnd_Max_Date).setValue(pnd_Compare_Date);                                                                                             //Natural: MOVE #COMPARE-DATE TO #CHECK-DATE ( #MAX-DATE )
            pnd_Max_Date.nadd(1);                                                                                                                                         //Natural: ADD 1 TO #MAX-DATE
        }                                                                                                                                                                 //Natural: END-IF
        //* *
    }
    private void sub_Write_Dates_Processed() throws Exception                                                                                                             //Natural: WRITE-DATES-PROCESSED
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(duplicates.getBoolean()))                                                                                                                           //Natural: IF DUPLICATES
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(5),"DATES PROCESSED - WITH SUSPECTED ERRORS");                                      //Natural: WRITE ( 1 ) // 5T 'DATES PROCESSED - WITH SUSPECTED ERRORS'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,new TabSetting(5),"DATES PROCESSED - NO SUSPECTED DUPLICATES FOUND");                              //Natural: WRITE ( 1 ) // 5T 'DATES PROCESSED - NO SUSPECTED DUPLICATES FOUND'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I 1 TO #MAX-DATE
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(pnd_Max_Date)); pnd_I.nadd(1))
        {
            if (condition(pnd_Check_Date.getValue(pnd_I).equals(" ")))                                                                                                    //Natural: IF #CHECK-DATE ( #I ) = ' '
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,new TabSetting(10),pnd_Check_Date.getValue(pnd_I));                                                            //Natural: WRITE ( 1 ) 10T #CHECK-DATE ( #I )
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"*",new RepeatItem(95));                                                                                               //Natural: WRITE ( 1 ) '*' ( 95 )
        if (Global.isEscape()) return;
    }
    private void sub_Write_Threshold() throws Exception                                                                                                                   //Natural: WRITE-THRESHOLD
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,NEWLINE,"CURRENT THRESHOLD DETAILS",NEWLINE,pnd_Duplicate_Controls_Term_Lit1,pnd_Duplicate_Controls_Term_Answer, //Natural: WRITE ( 1 ) // 'CURRENT THRESHOLD DETAILS' / TERM-LIT1 TERM-ANSWER / TERM-LIT2 TERM-COUNT / TERM-LIT3 TERM-PERCENT
            NEWLINE,pnd_Duplicate_Controls_Term_Lit2,pnd_Duplicate_Controls_Term_Count,NEWLINE,pnd_Duplicate_Controls_Term_Lit3,pnd_Duplicate_Controls_Term_Percent);
        if (Global.isEscape()) return;
        getReports().write(1, ReportOption.NOTITLE,"*",new RepeatItem(95));                                                                                               //Natural: WRITE ( 1 ) '*' ( 95 )
        if (Global.isEscape()) return;
        //*  WRITE-THRESHOLD
    }
    private void sub_Write_Duplicates() throws Exception                                                                                                                  //Natural: WRITE-DUPLICATES
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,new TabSetting(5),"** SUSPECTED DUPLICATES **");                                                                       //Natural: WRITE ( 1 ) 5T '** SUSPECTED DUPLICATES **'
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I 1 TO DUP
        for (pnd_I.setValue(1); condition(pnd_I.lessOrEqual(dup)); pnd_I.nadd(1))
        {
            getReports().display(1, "File/Cnt.",                                                                                                                          //Natural: DISPLAY ( 1 ) 'File/Cnt.' TBL-COUNT ( #I ) 'Contract/PPCN ' TBL-CNTRCT-PPCN-NBR ( #I ) 'Payee/code' TBL-CNTRCT-PAYEE-CDE ( #I ) 'PIN/Nbr.' TBL-CNTRCT-UNQ-ID-NBR ( #I ) 'CREF/Contract' TBL-CNTRCT-CREF-NBR ( #I ) 'Payment/Type' TBL-EFT-CHK ( #I ) 'Contract/Type' TBL-CNTRCT-TYPE-CDE ( #I ) 'Contract/LOB' TBL-CNTRCT-LOB-CDE ( #I ) 'Payment/Check Date' TBL-PYMNT-CHECK-DTE ( #I ) ( EM = YYYY/MM/DD ) 'Payment/Amount' TBL-PYMNT-CHECK-AMT ( #I ) ( EM = Z,ZZZ,ZZ9.99 )
            		table_Of_Duplicates_Tbl_Count.getValue(pnd_I),"Contract/PPCN ",
            		table_Of_Duplicates_Tbl_Cntrct_Ppcn_Nbr.getValue(pnd_I),"Payee/code",
            		table_Of_Duplicates_Tbl_Cntrct_Payee_Cde.getValue(pnd_I),"PIN/Nbr.",
            		table_Of_Duplicates_Tbl_Cntrct_Unq_Id_Nbr.getValue(pnd_I),"CREF/Contract",
            		table_Of_Duplicates_Tbl_Cntrct_Cref_Nbr.getValue(pnd_I),"Payment/Type",
            		table_Of_Duplicates_Tbl_Eft_Chk.getValue(pnd_I),"Contract/Type",
            		table_Of_Duplicates_Tbl_Cntrct_Type_Cde.getValue(pnd_I),"Contract/LOB",
            		table_Of_Duplicates_Tbl_Cntrct_Lob_Cde.getValue(pnd_I),"Payment/Check Date",
            		table_Of_Duplicates_Tbl_Pymnt_Check_Dte.getValue(pnd_I), new ReportEditMask ("YYYY/MM/DD"),"Payment/Amount",
            		table_Of_Duplicates_Tbl_Pymnt_Check_Amt.getValue(pnd_I), new ReportEditMask ("Z,ZZZ,ZZ9.99"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    //*  UN-CONTROLED ABEND
    private void sub_System_Abend() throws Exception                                                                                                                      //Natural: SYSTEM-ABEND
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(95),NEWLINE,"* ",Global.getPROGRAM(),"  ERROR:",Global.getERROR_NR(),"LINE:",Global.getERROR_LINE(),NEWLINE,"* ",Global.getINIT_USER()," ",Global.getTIME()," ",Global.getDATU(),"SYSTEM ABEND",NEWLINE,"*",new  //Natural: WRITE ( 1 ) / '*' ( 95 ) / '* ' *PROGRAM '  ERROR:' *ERROR-NR 'LINE:' *ERROR-LINE / '* ' *INIT-USER ' ' *TIME ' ' *DATU 'SYSTEM ABEND' / '*' ( 95 )
            RepeatItem(95));
        if (Global.isEscape()) return;
        getWorkFiles().close(1);                                                                                                                                          //Natural: CLOSE WORK FILE 1
        DbsUtil.terminate();  if (true) return;                                                                                                                           //Natural: TERMINATE
        //*  SYSTEM-ABEND
    }
    private void sub_Controlled_Abend() throws Exception                                                                                                                  //Natural: CONTROLLED-ABEND
    {
        if (BLNatReinput.isReinput()) return;

        if (condition(duplicates.getBoolean()))                                                                                                                           //Natural: IF DUPLICATES
        {
                                                                                                                                                                          //Natural: PERFORM WRITE-DUPLICATES
            sub_Write_Duplicates();
            if (condition(Global.isEscape())) {return;}
            getReports().newPage(new ReportSpecification(1));                                                                                                             //Natural: NEWPAGE ( 1 )
            if (condition(Global.isEscape())){return;}
            //*  COUNTER +1 ON END OF FILE
            read_Count.nsubtract(1);                                                                                                                                      //Natural: COMPUTE READ-COUNT := READ-COUNT - 1
            //*  TERMINATE APPLICATION OR NOT BASED ON RESULTS OF CDUPL TABLE LOOKUP
            //*  IF TERM-ANSWER = YES TERMINATE WITH CODE 99
            //*  IF TERM-ANSWER = NO DO NOT TERMINATE
            //*  THIS WILL ALLOW THE OVERRIDE OF THIS PROCEDURE FROM PAYC
            //*  KEY SHORT CDUPL LONG KEY P1426CPD
            if (condition(above_Threshold_Percent.getBoolean() || above_Threshold_Count.getBoolean() && (pnd_Duplicate_Controls_Term_Answer.equals("YES")                 //Natural: IF ABOVE-THRESHOLD-PERCENT OR ABOVE-THRESHOLD-COUNT AND ( TERM-ANSWER = 'YES' OR TERM-ANSWER = 'yes' )
                || pnd_Duplicate_Controls_Term_Answer.equals("yes"))))
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(95),NEWLINE,"* ",Global.getINIT_USER()," ",Global.getPROGRAM(),Global.getTIME()," ",Global.getDATU(),"CONTROLLED ABEND",NEWLINE,"* ",Global.getINIT_USER(),"IS BEING TERMINATED FROM WITHIN THE APPLICATION",NEWLINE,"* ",NEWLINE,"* ","THE PROCESS FOUND SUSPECTED PAYMENT DUPLICATES",NEWLINE,"* ","CONSOLIDATED PAYMENT SYSTEMS MUST BE NOTIFIED ",NEWLINE,"* "," ",NEWLINE,"* ","CALL MAIL AUTOMATION AND PROCESSING AND HOLD ALL CHECKS",NEWLINE,"* ","FROM THIS RUN AND ANY PREVIOUS RUNS THAT MAY STILL BE",NEWLINE,"* ","IN PROCESS IN THE MAPP AREA AND NOT YET MAILED.",NEWLINE,"* "," ",NEWLINE,"* ","PREVENT CHECKS FROM BEING MAILED",NEWLINE,"* ","PREVENT CHECKS FROM BEING MAILED",NEWLINE,"* ","PREVENT CHECKS FROM BEING MAILED",NEWLINE,"* "," ",NEWLINE,"* ","CALL OPERATIONS STOP THE EXECUTION OF THE EFT TRANSMISSIONS",NEWLINE,"* ","TO THE BANK 'P5858SSD' ",NEWLINE,"* ","DO NOT RUN EFT TRANSMISSION TO THE BANK",NEWLINE,"* ","DO NOT RUN EFT TRANSMISSION TO THE BANK",NEWLINE,"* ","DO NOT RUN EFT TRANSMISSION TO THE BANK",NEWLINE,"* "," ",NEWLINE,"* ","RECORDS READ         :",read_Count,NEWLINE,"* ","POTENTIAL DUPLICATES :",dup,NEWLINE,"* ","ABOVE-THRESHOLD-PERCENT:",above_Threshold_Percent,NEWLINE,"* ","ABOVE-THRESHOLD-COUNT  :",above_Threshold_Count,NEWLINE,"* ","TERMINATE YES/NO?      :",pnd_Duplicate_Controls_Term_Answer,NEWLINE,"*",new  //Natural: WRITE ( 1 ) / '*' ( 95 ) / '* ' *INIT-USER ' ' *PROGRAM *TIME ' ' *DATU 'CONTROLLED ABEND' / '* ' *INIT-USER 'IS BEING TERMINATED FROM WITHIN THE APPLICATION' / '* ' / '* ' 'THE PROCESS FOUND SUSPECTED PAYMENT DUPLICATES' / '* ' 'CONSOLIDATED PAYMENT SYSTEMS MUST BE NOTIFIED ' / '* ' ' ' / '* ' 'CALL MAIL AUTOMATION AND PROCESSING AND HOLD ALL CHECKS' / '* ' 'FROM THIS RUN AND ANY PREVIOUS RUNS THAT MAY STILL BE' / '* ' 'IN PROCESS IN THE MAPP AREA AND NOT YET MAILED.' / '* ' ' ' / '* ' 'PREVENT CHECKS FROM BEING MAILED' / '* ' 'PREVENT CHECKS FROM BEING MAILED' / '* ' 'PREVENT CHECKS FROM BEING MAILED' / '* ' ' ' / '* ' 'CALL OPERATIONS STOP THE EXECUTION OF THE EFT TRANSMISSIONS' / '* ' 'TO THE BANK "P5858SSD" ' / '* ' 'DO NOT RUN EFT TRANSMISSION TO THE BANK' / '* ' 'DO NOT RUN EFT TRANSMISSION TO THE BANK' / '* ' 'DO NOT RUN EFT TRANSMISSION TO THE BANK' / '* ' ' ' / '* ' 'RECORDS READ         :' READ-COUNT / '* ' 'POTENTIAL DUPLICATES :' DUP / '* ' 'ABOVE-THRESHOLD-PERCENT:' ABOVE-THRESHOLD-PERCENT / '* ' 'ABOVE-THRESHOLD-COUNT  :' ABOVE-THRESHOLD-COUNT / '* ' 'TERMINATE YES/NO?      :' TERM-ANSWER / '*' ( 95 )
                    RepeatItem(95));
                if (Global.isEscape()) return;
                //*  IF NO TERMINATION IS TO OCCUR
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                getReports().write(1, ReportOption.NOTITLE,NEWLINE,"*",new RepeatItem(95),NEWLINE,"* ",Global.getINIT_USER()," ",Global.getPROGRAM(),Global.getTIME()," ",Global.getDATU(),NEWLINE,"* ",NEWLINE,"* ","         TERMINATION BYPASSED",NEWLINE,"* ","         TERMINATION BYPASSED",NEWLINE,"* ","THE PROCESS FOUND SUSPECTED PAYMENT DUPLICATES",NEWLINE,"* "," ",NEWLINE,"* ","RECORDS READ           :",read_Count,NEWLINE,"* ","POTENTIAL DUPLICATES   :",dup,NEWLINE,"* ","ABOVE-THRESHOLD-PERCENT:",above_Threshold_Percent,NEWLINE,"* ","ABOVE-THRESHOLD-COUNT  :",above_Threshold_Count,NEWLINE,"* ","TERMINATE YES/NO?      :",pnd_Duplicate_Controls_Term_Answer,NEWLINE,"*",new  //Natural: WRITE ( 1 ) / '*' ( 95 ) / '* ' *INIT-USER ' ' *PROGRAM *TIME ' ' *DATU / '* ' / '* ' '         TERMINATION BYPASSED' / '* ' '         TERMINATION BYPASSED' / '* ' 'THE PROCESS FOUND SUSPECTED PAYMENT DUPLICATES' / '* ' ' ' / '* ' 'RECORDS READ           :' READ-COUNT / '* ' 'POTENTIAL DUPLICATES   :' DUP / '* ' 'ABOVE-THRESHOLD-PERCENT:' ABOVE-THRESHOLD-PERCENT / '* ' 'ABOVE-THRESHOLD-COUNT  :' ABOVE-THRESHOLD-COUNT / '* ' 'TERMINATE YES/NO?      :' TERM-ANSWER / '*' ( 95 )
                    RepeatItem(95));
                if (Global.isEscape()) return;
            }                                                                                                                                                             //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-DATES-PROCESSED
            sub_Write_Dates_Processed();
            if (condition(Global.isEscape())) {return;}
                                                                                                                                                                          //Natural: PERFORM WRITE-THRESHOLD
            sub_Write_Threshold();
            if (condition(Global.isEscape())) {return;}
            getWorkFiles().close(1);                                                                                                                                      //Natural: CLOSE WORK FILE 1
            if (condition(pnd_Duplicate_Controls_Term_Answer.equals("YES")))                                                                                              //Natural: IF TERM-ANSWER = 'YES'
            {
                DbsUtil.terminate(99);  if (true) return;                                                                                                                 //Natural: TERMINATE 99
            }                                                                                                                                                             //Natural: END-IF
            //*  END DUPLICATES
        }                                                                                                                                                                 //Natural: END-IF
        //* *
        if (condition(! (duplicates.getBoolean())))                                                                                                                       //Natural: IF NOT DUPLICATES
        {
            getReports().write(0, NEWLINE,"*",new RepeatItem(95),NEWLINE,"* ",Global.getPROGRAM(),Global.getINIT_USER()," ",Global.getTIME()," ",Global.getDATU(),"CONTROLLED ABEND",NEWLINE,"* ",Global.getPROGRAM(),"IS BEING TERMINATED FROM WITHIN THE APPLICATION",NEWLINE,"* ",NEWLINE,"* ","MESSAGE              :",pnd_Err_Msg.getValue(1),NEWLINE,"* ","MESSAGE              :",pnd_Err_Msg.getValue(2),NEWLINE,"* ","INTERNAL MODULE ERROR:",pnd_Err_Code,NEWLINE,"* ",NEWLINE,"* ","NOTIFY CONSOLIDATED PAYMENT SYSTEM DEVELOPERS",NEWLINE,"* ","NOTIFY CONSOLIDATED PAYMENT SYSTEM DEVELOPERS",NEWLINE,"* ",NEWLINE,"*",new  //Natural: WRITE / '*' ( 95 ) / '* ' *PROGRAM *INIT-USER ' ' *TIME ' ' *DATU 'CONTROLLED ABEND' / '* ' *PROGRAM 'IS BEING TERMINATED FROM WITHIN THE APPLICATION' / '* ' / '* ' 'MESSAGE              :' #ERR-MSG ( 1 ) / '* ' 'MESSAGE              :' #ERR-MSG ( 2 ) / '* ' 'INTERNAL MODULE ERROR:' #ERR-CODE / '* ' / '* ' 'NOTIFY CONSOLIDATED PAYMENT SYSTEM DEVELOPERS' / '* ' 'NOTIFY CONSOLIDATED PAYMENT SYSTEM DEVELOPERS' / '* ' / '*' ( 95 )
                RepeatItem(95));
            if (Global.isEscape()) return;
            getWorkFiles().close(1);                                                                                                                                      //Natural: CLOSE WORK FILE 1
            DbsUtil.terminate(pnd_Err_Code);  if (true) return;                                                                                                           //Natural: TERMINATE #ERR-CODE
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONTROLLED-ABEND
    }
    private void sub_Get_Controls() throws Exception                                                                                                                      //Natural: GET-CONTROLS
    {
        if (BLNatReinput.isReinput()) return;

        //*  GET DUPLICATE PAYMENT CONTROLS
        pdaCpwa100.getCpwa100().getValue("*").reset();                                                                                                                    //Natural: RESET CPWA100 ( * )
        pdaCpwa100.getCpwa100_Cpwa100_Table_Id().setValue("CDUPL");                                                                                                       //Natural: MOVE 'CDUPL' TO CPWA100-TABLE-ID
        pdaCpwa100.getCpwa100_Cpwa100_A_I_Ind().setValue("A");                                                                                                            //Natural: MOVE 'A' TO CPWA100-A-I-IND
        pdaCpwa100.getCpwa100_Cpwa100_Short_Key().setValue("DUPLICATE   ");                                                                                               //Natural: MOVE 'DUPLICATE   ' TO CPWA100-SHORT-KEY
        pdaCpwa100.getCpwa100_Cpwa100_Long_Key().setValue("P1426CPD");                                                                                                    //Natural: MOVE 'P1426CPD' TO CPWA100-LONG-KEY
        //*  READ REFERENCE-TABLE
        DbsUtil.callnat(Cpwn100.class , getCurrentProcessState(), pdaCpwa100.getCpwa100().getValue("*"));                                                                 //Natural: CALLNAT 'CPWN100' USING CPWA100 ( * )
        if (condition(Global.isEscape())) return;
        if (condition(pdaCpwa100.getCpwa100_Cpwa100_Return_Code().notEquals("00")))                                                                                       //Natural: IF CPWA100-RETURN-CODE NE '00'
        {
            getReports().write(1, ReportOption.NOTITLE,"TABLE ENTRY CDUPL - DUPLICATES NOT FOUND");                                                                       //Natural: WRITE ( 1 ) 'TABLE ENTRY CDUPL - DUPLICATES NOT FOUND'
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pnd_Duplicate_Controls.setValue(pdaCpwa100.getCpwa100_Rt_Desc1());                                                                                            //Natural: MOVE CPWA100.RT-DESC1 TO #DUPLICATE-CONTROLS
            getReports().write(0, NEWLINE,pnd_Duplicate_Controls_Line_1,NEWLINE,pnd_Duplicate_Controls_Line_2,NEWLINE,pnd_Duplicate_Controls_Line_3);                     //Natural: WRITE / LINE-1 / LINE-2 / LINE-3
            if (Global.isEscape()) return;
        }                                                                                                                                                                 //Natural: END-IF
        //*  GET-CONTROLS
    }

    //

    // Support Methods

    public AtItemEventHandler atTopEventRpt1 = (Object sender, EventArgs e) ->
    {
        try
        {
            while (true)
            {
                try
                {
                    getReports().write(1, ReportOption.NOTITLE,Global.getPROGRAM(),new TabSetting(15),"Payment Duplicate Comparision Report",new TabSetting(71),getReports().getPageNumberDbs(1),  //Natural: WRITE ( 1 ) NOTITLE *PROGRAM 15T 'Payment Duplicate Comparision Report' 71T *PAGE-NUMBER ( 1 ) ( NL = 4 AD = L SG = OFF ) / *DATX ( EM = LLL' 'DD', 'YY ) 15T 'Electronic Fund Transfers and Checks' 71T *TIMX ( EM = HH':'II' 'AP ) /
                        new NumericLength (4), new FieldAttributes ("AD=L"), new SignPosition (false),NEWLINE,Global.getDATX(), new ReportEditMask ("LLL' 'DD', 'YY"),new 
                        TabSetting(15),"Electronic Fund Transfers and Checks",new TabSetting(71),Global.getTIMX(), new ReportEditMask ("HH':'II' 'AP"),NEWLINE);
                }                                                                                                                                                         //Natural: END-TOPPAGE
                catch (ReinputException re)
                {
                    checkMethod(re);
                }
                if (!isInReinput()) break;
            }
        }
        catch (Exception ex)
        {
            if (!(ex instanceof ReinputException))
                SimpleLogging.logErrorMessage(ex);
        }
    };

    public OnErrorEventHandler onError = (Object sender, OnErrorEventArg e) ->
    {
                                                                                                                                                                          //Natural: PERFORM SYSTEM-ABEND
        sub_System_Abend();
        if (condition(Global.isEscape())) {return;}
    };                                                                                                                                                                    //Natural: END-ERROR
    private void setupReports() throws Exception
    {
        Global.format(1, "LS=133 PS=60");
        Global.format(0, "LS=133 PS=60");

        getReports().setDisplayColumns(1, "File/Cnt.",
        		table_Of_Duplicates_Tbl_Count,"Contract/PPCN ",
        		table_Of_Duplicates_Tbl_Cntrct_Ppcn_Nbr,"Payee/code",
        		table_Of_Duplicates_Tbl_Cntrct_Payee_Cde,"PIN/Nbr.",
        		table_Of_Duplicates_Tbl_Cntrct_Unq_Id_Nbr,"CREF/Contract",
        		table_Of_Duplicates_Tbl_Cntrct_Cref_Nbr,"Payment/Type",
        		table_Of_Duplicates_Tbl_Eft_Chk,"Contract/Type",
        		table_Of_Duplicates_Tbl_Cntrct_Type_Cde,"Contract/LOB",
        		table_Of_Duplicates_Tbl_Cntrct_Lob_Cde,"Payment/Check Date",
        		table_Of_Duplicates_Tbl_Pymnt_Check_Dte, new ReportEditMask ("YYYY/MM/DD"),"Payment/Amount",
        		table_Of_Duplicates_Tbl_Pymnt_Check_Amt, new ReportEditMask ("Z,ZZZ,ZZ9.99"));
    }
}
