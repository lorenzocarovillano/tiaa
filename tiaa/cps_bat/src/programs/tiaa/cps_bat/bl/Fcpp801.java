/* ************************************************************
**   Modern Systems INC.  COPYRIGHT 2000-2017
**   eavATP NATURAL/JAVA SaveAs
**   Product Version: V5.8.1 - Build 20210513a
************************************************************
** MOD ID * DESC                 * DATE
************************************************************
** INIT   * INITIAL VERSION      * 2021-05-13 09:19:41 PM
**        * FROM NATURAL PROGRAM : Fcpp801
************************************************************
**        * FILE NAME            : Fcpp801.java
**        * CLASS NAME           : Fcpp801
**        * INSTANCE NAME        : Fcpp801
************************************************************
************************************************************************
* PROGRAM  : FCPP801
* SYSTEM   : CPS
* TITLE    : IAR RESTRUCTURE
* FUNCTION : READ IAR PAYMENT FILE AFTER GTN AND SEPARATE IT INTO
*            MULTIPLE FILES. GENERATE LEDGERS AND CREATE ANNOTATIONS.
*            REJECT ERRORS INTO ERROR FILE.
* MODIFICATIONS  :
*  JAN 1998  RCC  ADDITIONAL OCCURENCE FOR EFTS WITH NO PYMNT-CHK-SAV-IN
*  OCT 1998  DC,LB - PERFORMING A NEW CONTROL TOTALS SUBROUTINE FOR THE
*                 MUTUAL FUNDS - NEW DEDUCTION CODE 008 - SEE (LB) LABEL
*  JAN 1999  RCC  INCLUDE PROCESSING FOR PA SELECT
*  MAY 2000  TMM - ADD PASELECT SPIA PROCESSING
*            TMM - ADD DEDUCTIONS PERSONAL ANNUITY, PA-SELECT, UNIVERSAL
*                  LIFE TO CONTROL TOTAL RECORDS
*  JAN 2001  RCC - RE-STOW CHANGE IN LEDGER LDA
*  FEB 2001  RCC - RESET DCI FIELD WHICH WAS TEMPORARILY USED TO HOLD
*                  THE DEDUCTION AMOUNT APPLIED TO THE NET PER FUND
*
*  MARCH 26/2001  LEON GURTOVNIK
*           : - CNANGE REPORT (15) TO SPLIT INTERNAL ROLLOVERS
*           : TPA  ROLLOVER          '
*           : P&I  ROLLOVER          '
*           : IPRO ROLLOVER          '
*           : INTERNAL ROLLOVER OTHER'
*  MAY 08, 2001  - ROXAN CARREON
*                  REMOVED OTHER CALLS TO DECIDE-ROLLOVER
*
*  AUG 01, 2001  - A. REYHANIAN  ..... COPIED FROM PROJFCP2 07/10/2001
*                  TAX WITHHOLDING - READ NEW EXTRACT FILE
*  DEC 11, 2001  - R.CARREON PASS RESIDENCY TO THE ONE RECEIVED FROM TAX
*  DEC 21, 2001  - MCGEE GLOBAL PAYMENT EFT/EFT-USD
*  APR 23, 2003  - R. CARREON - TPA EGTRRA
*  04/22/2008    - LCW - RESTOWED FOR ROTH 403B. ROTH-MAJOR1
*                - ROTH FIELDS INCLUDED IN FCPA800.
* 07/16/10  D E ANDER ADDED CODE NEW LOGICAL FIELDS FOR ANNUITY PAYOUT
*                                                          MARKED DEA
* 8/26/2010 C. MASON  RESTOW DUE TO CHANGE IN FCPA800
* 8/26/2010 C. MASON  READ CANADIAN TAX PASSED FROM FCPA800
* 10/18/2010 R.SACHARNY - WRITE NEW RECON FILE 12  (RS0)
* 10/19/2010 B.HOLLOWAY - PENDED PAYMENTS - CHANGE TAG IS DBH1
* 10/26/2010 C.MASON PASS CANADIAN TAX TO WORK FILE 6
* 01/31/2011 R.SACHARNY PASS SETTLEMENT DATE INSTEAD OF CYCLE DATE (RS2)
* 02/04/2011 R.SACHARNY PASS REDRAW INDICATOR  (RS3)
* 06/09/2011 R.SACHARNY FOR FOREIGN COUTRY  CODE MOVE 'FO'  (RS4)
* 06/22/2011 J.OSTEEN - ADD DOB AND SETTLEMENT DATE FOR PROLINE
* 10/04/2011 F.ALLIE  - ADD NRA TAX & FED CANADIAN AMT FOR PROLINE
* 12/06/2011 R.SACHARNY WRITE IA-ORGN-CDE TO S471 FILE 11 RS5
*                       WRITE IA-ORGN-CDE/CNTRCT-OPTION-CDE TO FILE 12
* 01/12/2012 R.SACHARNY WRITE SUNY/CUNY FIELDS TO FILES 3 & 4 (RS6)
* 07/03/2012 M.BRISCOE - IF THE CONTRACT HOLD CODE IS A GLOBAL,
*                         SET THE PAYMENT REQ TYPE TO A 3          MB
* 12/15/2012 B.HOLLOWAY - PENDED PAYMENTS PHASE II - CHANGE TAG IS DBH2
* 03/08/13 B.HOLLOWAY - DO NOT NOT INCLUDE CONTRACTS IA ORIGIN CODE 04
*                       WITH PEND CODES OTHER THAN 'M' OR 'I' IN THE
*                       PENDED PAYMENT LEDGERING.  - CHANGE TAG IS DBH3
* 06/07/13 B.HOLLOWAY - HARD-CODE VALUE SUBSTITUTIONS FOR CONTRACT
*                       S0833570 TO CAUSE CORRECT LEDGERING -
*                       INC1943940/ PRB58730 - CHANGE TAG IS DBH4
* 10/24/13 F.ENDAYA   - INCLUDE LOGIC THAT WILL SEPARATE LEDGERS OF
*                       457B.    FE201310
* 04/24/16 F.ENDAYA   - TEXAS CHILD SUPPORT PROJECT. ADD OUTPUT FILE FOR
* FE201604              ALL PARTICIPANTS WITH DEDUCTION CODE 900.
* 04/07/17 A.SAI      - PIN EXPANSION
* 01/15/2020�CTS      - CPS SUNSET (CHG694525) TAG: CTS-0115
*                     1. STOP CREATING TEXAS CHILD SUPPORT FILE
*                        (WORK FILE 16).
*                        #TEXAS-CHD-OUTPUT IS ALSO COMMENTED OUT.
*                     2. STOP CREATING THE TAX FILE (WORK FILE 3)
*                        SUBROUTINE 'WRITE-TAX-FILE' WILL BE
*                        DECOMMISSIONED.
*                     3. STOP CREATING ANNOTATIONS FILE (WORK FILE 8).
*                        COMMENT OUT FCPL801 (ANNOTATIONS DATA AREA).
*                     4. STOP CREATING GLOBAL FILE (WORK FILE 10)
*                     5. STOP CREATING BEFORE CHECK FILE (WORK FILE 11)
*                     6. CHANGES FOR DI-HUB FEED (LEDGER2 FILE).
*                        FOLLOWING FIELDS WILL NO LONGER BE POPULATED -
*                        1. #LEDGER-EXT-2.CNTRCT-LIFE-CONTINGENCY
*                        2. #LEDGER-EXT-2.ANNT-RSDNCY-CDE
*                        3. #LEDGER-EXT-2.ANNT-LOCALITY-CDE
*                        4. #LEDGER-EXT-2.IA-ORGN-CDE
*                        5. #LEDGER-EXT-2.CNTRCT-OPTION-CDE
*                        FIELDS WILL BE POPULATED WITH SPACES.
*                     7. ADDING BELOW FIELDS IN WRITE WORK FILE 5 & 6
*                        FROM FCPA800 :-
*                        CANADIAN-TAX-AMT-ALPHA
*                        WF-PYMNT-ADDR-GRP.IA-ORGN-CDE
*                        PLAN-NUMBER SUB-PLAN
*                        ORIG-CNTRCT-NBR  ORIG-SUB-PLAN
*                     8. ADDING WORK FILE 13 IN THE LAYOUT SAME AS
*                        WORK FILE 5 & 6
*                     9. ADDING BELOW FIELDS IN WORK FILE 5 ,6 & 13
*                        GTN-W9-BEN-IND
*                        GTN-FED-TAX-TYPE
*                        GTN-FED-TAX-AMT
*                        GTN-FED-CANADIAN-AMT
*                        GTN-ELEC-CDE
*                        GTN-CNTRCT-MONEY-SOURCE
*                        GTN-HARDSHIP-CDE
*
* 02-02-2021   MANSOOR - LEDGER GROUP ANNUITY CONTRACTS   TAG - CTS2021
************************************************************************

************************************************************ */

package tiaa.cps_bat.bl;

import ateras.framework.*;
import ateras.framework.domain.*;
import ateras.framework.exceptions.ReinputException;
import ateras.framework.extensions.*;
import ateras.framework.io.*;
import ateras.framework.ui.*;

import ateras.framework.system.*;
import java.util.EnumSet;
import java.util.List;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import tiaa.tiaacommon.bl.*;

public class Fcpp801 extends BLNatBase
{
    // Data Areas
    private PdaFcpa115 pdaFcpa115;
    private PdaFcpa142l pdaFcpa142l;
    private PdaFcpa143 pdaFcpa143;
    private PdaFcpa800 pdaFcpa800;
    private PdaFcpa802 pdaFcpa802;
    private PdaFcpa801c pdaFcpa801c;
    private PdaFcpacntl pdaFcpacntl;
    private PdaFcpaacum pdaFcpaacum;
    private LdaFcpl801a ldaFcpl801a;
    private LdaFcpl801c ldaFcpl801c;
    private LdaFcpl801d ldaFcpl801d;
    private LdaFcpl801p ldaFcpl801p;
    private LdaFcpl800 ldaFcpl800;
    private LdaFcplcrpt ldaFcplcrpt;
    private PdaFcpa142r pdaFcpa142r;

    // Local Variables
    public DbsRecord localVariables;
    private DbsField pnd_Original_Csr_Cde;
    private DbsField pnd_Pend_Pct_Omni;

    private DbsGroup pnd_Ws;

    private DbsGroup pnd_Ws_Pnd_Wf_Save_Fields;
    private DbsField pnd_Ws_Gtn_Ret_Code;
    private DbsField pnd_Ws_Gtn_Pymnt_Rpt_Code;
    private DbsField pnd_Ws_Pymnt_Check_Amt;

    private DbsGroup pnd_Ws_Pnd_Wf_Save_Fields_1;
    private DbsField pnd_Ws_Pymnt_Pay_Type_Req_Ind;
    private DbsField pnd_Ws_Cntrct_Hold_Cde;

    private DbsGroup pnd_Ws_Pnd_Input_Cntl_Valid_Fields;
    private DbsField pnd_Ws_Pnd_I_V_Rec_Cnt;
    private DbsField pnd_Ws_Pnd_Gtn_Rec_Cnt;
    private DbsField pnd_Ws_Pnd_Zero_Occurs;
    private DbsField pnd_Ws_Pnd_Work_Date;

    private DbsGroup pnd_Ws__R_Field_1;
    private DbsField pnd_Ws_Pnd_Work_Date_Num;
    private DbsField pnd_Ws_Pnd_Intrfce_Dte;
    private DbsField pnd_Ws_Pnd_Terminate;
    private DbsField pnd_Ws_Pnd_Internal_Rollover;
    private DbsField pnd_Ws_Pnd_Abend_Parm;
    private DbsField pnd_Ws_Pnd_I;
    private DbsField pnd_Ws_Pnd_M;
    private DbsField pnd_Ws_Pnd_Cr_Total;
    private DbsField pnd_Ws_Pnd_Dr_Total;

    private DbsGroup pnd_Ws_Pnd_Record_Counts;
    private DbsField pnd_Ws_Pnd_Annot_Records;
    private DbsField pnd_Ws_Pnd_Ledger_Records;
    private DbsField pnd_Ws_Pnd_Actual_Ledgers;
    private DbsField pnd_Ws_Pnd_Recon_Ledgers;
    private DbsField pnd_Ws_Pnd_Ledger_Error;
    private DbsField pnd_Ws_Pnd_Idx;
    private DbsField pnd_Ir_Index;

    private DbsGroup pnd_Ir;
    private DbsField pnd_Ir_Pnd_Pymnt_Cnt;
    private DbsField pnd_Ir_Pnd_Rec_Cnt;
    private DbsField pnd_Ir_Pnd_Settl_Amt;
    private DbsField pnd_Ir_Pnd_Net_Pymnt_Amt;

    private DbsGroup pnd_Lit_Occurs_Ir;
    private DbsField pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir;
    private DbsField pnd_Gtn_W9_Ben_Ind;
    private DbsField pnd_Gtn_Fed_Tax_Type;
    private DbsField pnd_Gtn_Fed_Tax_Amt;
    private DbsField pnd_Gtn_Fed_Canadian_Amt;
    private DbsField pnd_Gtn_Elec_Cde;
    private DbsField pnd_Gtn_Cntrct_Money_Source;
    private DbsField pnd_Gtn_Hardship_Cde;
    private DbsField proline_Dob;
    private DbsField proline_Settle_Date;
    private DbsField proline_Nra_Tax_Amt;
    private DbsField proline_Fed_Can_Amt;
    private DbsField pnd_Frgn_Pymnt_Cnt;

    private DbsGroup pnd_Hard_Coded_Overrides;
    private DbsField pnd_Hard_Coded_Overrides_Pnd_S0833570;

    //Data Initialization Methods
    private void initializeFields(boolean gdaOnly) throws Exception
    {
        //Data Areas
        localVariables = new DbsRecord();
        pdaFcpa115 = new PdaFcpa115(localVariables);
        pdaFcpa142l = new PdaFcpa142l(localVariables);
        pdaFcpa143 = new PdaFcpa143(localVariables);
        pdaFcpa800 = new PdaFcpa800(localVariables);
        pdaFcpa802 = new PdaFcpa802(localVariables);
        pdaFcpa801c = new PdaFcpa801c(localVariables);
        pdaFcpacntl = new PdaFcpacntl(localVariables);
        pdaFcpaacum = new PdaFcpaacum(localVariables);
        ldaFcpl801a = new LdaFcpl801a();
        registerRecord(ldaFcpl801a);
        ldaFcpl801c = new LdaFcpl801c();
        registerRecord(ldaFcpl801c);
        ldaFcpl801d = new LdaFcpl801d();
        registerRecord(ldaFcpl801d);
        ldaFcpl801p = new LdaFcpl801p();
        registerRecord(ldaFcpl801p);
        ldaFcpl800 = new LdaFcpl800();
        registerRecord(ldaFcpl800);
        ldaFcplcrpt = new LdaFcplcrpt();
        registerRecord(ldaFcplcrpt);
        pdaFcpa142r = new PdaFcpa142r(localVariables);

        // Local Variables
        pnd_Original_Csr_Cde = localVariables.newFieldInRecord("pnd_Original_Csr_Cde", "#ORIGINAL-CSR-CDE", FieldType.STRING, 3);
        pnd_Pend_Pct_Omni = localVariables.newFieldInRecord("pnd_Pend_Pct_Omni", "#PEND-PCT-OMNI", FieldType.NUMERIC, 5, 4);

        pnd_Ws = localVariables.newGroupInRecord("pnd_Ws", "#WS");

        pnd_Ws_Pnd_Wf_Save_Fields = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Wf_Save_Fields", "#WF-SAVE-FIELDS");
        pnd_Ws_Gtn_Ret_Code = pnd_Ws_Pnd_Wf_Save_Fields.newFieldInGroup("pnd_Ws_Gtn_Ret_Code", "GTN-RET-CODE", FieldType.STRING, 4);
        pnd_Ws_Gtn_Pymnt_Rpt_Code = pnd_Ws_Pnd_Wf_Save_Fields.newFieldArrayInGroup("pnd_Ws_Gtn_Pymnt_Rpt_Code", "GTN-PYMNT-RPT-CODE", FieldType.NUMERIC, 
            2, new DbsArrayController(1, 7));
        pnd_Ws_Pymnt_Check_Amt = pnd_Ws_Pnd_Wf_Save_Fields.newFieldInGroup("pnd_Ws_Pymnt_Check_Amt", "PYMNT-CHECK-AMT", FieldType.PACKED_DECIMAL, 9, 2);

        pnd_Ws_Pnd_Wf_Save_Fields_1 = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Wf_Save_Fields_1", "#WF-SAVE-FIELDS-1");
        pnd_Ws_Pymnt_Pay_Type_Req_Ind = pnd_Ws_Pnd_Wf_Save_Fields_1.newFieldInGroup("pnd_Ws_Pymnt_Pay_Type_Req_Ind", "PYMNT-PAY-TYPE-REQ-IND", FieldType.NUMERIC, 
            1);
        pnd_Ws_Cntrct_Hold_Cde = pnd_Ws_Pnd_Wf_Save_Fields_1.newFieldInGroup("pnd_Ws_Cntrct_Hold_Cde", "CNTRCT-HOLD-CDE", FieldType.STRING, 4);

        pnd_Ws_Pnd_Input_Cntl_Valid_Fields = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Input_Cntl_Valid_Fields", "#INPUT-CNTL-VALID-FIELDS");
        pnd_Ws_Pnd_I_V_Rec_Cnt = pnd_Ws_Pnd_Input_Cntl_Valid_Fields.newFieldInGroup("pnd_Ws_Pnd_I_V_Rec_Cnt", "#I-V-REC-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Ws_Pnd_Gtn_Rec_Cnt = pnd_Ws_Pnd_Input_Cntl_Valid_Fields.newFieldInGroup("pnd_Ws_Pnd_Gtn_Rec_Cnt", "#GTN-REC-CNT", FieldType.PACKED_DECIMAL, 
            9);
        pnd_Ws_Pnd_Zero_Occurs = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Zero_Occurs", "#ZERO-OCCURS", FieldType.PACKED_DECIMAL, 3);
        pnd_Ws_Pnd_Work_Date = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Work_Date", "#WORK-DATE", FieldType.STRING, 8);

        pnd_Ws__R_Field_1 = pnd_Ws.newGroupInGroup("pnd_Ws__R_Field_1", "REDEFINE", pnd_Ws_Pnd_Work_Date);
        pnd_Ws_Pnd_Work_Date_Num = pnd_Ws__R_Field_1.newFieldInGroup("pnd_Ws_Pnd_Work_Date_Num", "#WORK-DATE-NUM", FieldType.NUMERIC, 8);
        pnd_Ws_Pnd_Intrfce_Dte = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Intrfce_Dte", "#INTRFCE-DTE", FieldType.DATE);
        pnd_Ws_Pnd_Terminate = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Terminate", "#TERMINATE", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Internal_Rollover = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Internal_Rollover", "#INTERNAL-ROLLOVER", FieldType.BOOLEAN, 1);
        pnd_Ws_Pnd_Abend_Parm = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Abend_Parm", "#ABEND-PARM", FieldType.PACKED_DECIMAL, 5);
        pnd_Ws_Pnd_I = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_I", "#I", FieldType.PACKED_DECIMAL, 8);
        pnd_Ws_Pnd_M = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_M", "#M", FieldType.PACKED_DECIMAL, 8);
        pnd_Ws_Pnd_Cr_Total = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Cr_Total", "#CR-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);
        pnd_Ws_Pnd_Dr_Total = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Dr_Total", "#DR-TOTAL", FieldType.PACKED_DECIMAL, 15, 2);

        pnd_Ws_Pnd_Record_Counts = pnd_Ws.newGroupInGroup("pnd_Ws_Pnd_Record_Counts", "#RECORD-COUNTS");
        pnd_Ws_Pnd_Annot_Records = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Annot_Records", "#ANNOT-RECORDS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ledger_Records = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ledger_Records", "#LEDGER-RECORDS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Actual_Ledgers = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Actual_Ledgers", "#ACTUAL-LEDGERS", FieldType.PACKED_DECIMAL, 
            7);
        pnd_Ws_Pnd_Recon_Ledgers = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Recon_Ledgers", "#RECON-LEDGERS", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Ledger_Error = pnd_Ws_Pnd_Record_Counts.newFieldInGroup("pnd_Ws_Pnd_Ledger_Error", "#LEDGER-ERROR", FieldType.PACKED_DECIMAL, 7);
        pnd_Ws_Pnd_Idx = pnd_Ws.newFieldInGroup("pnd_Ws_Pnd_Idx", "#IDX", FieldType.PACKED_DECIMAL, 3);
        pnd_Ir_Index = localVariables.newFieldInRecord("pnd_Ir_Index", "#IR-INDEX", FieldType.NUMERIC, 1);

        pnd_Ir = localVariables.newGroupArrayInRecord("pnd_Ir", "#IR", new DbsArrayController(1, 4));
        pnd_Ir_Pnd_Pymnt_Cnt = pnd_Ir.newFieldInGroup("pnd_Ir_Pnd_Pymnt_Cnt", "#PYMNT-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ir_Pnd_Rec_Cnt = pnd_Ir.newFieldInGroup("pnd_Ir_Pnd_Rec_Cnt", "#REC-CNT", FieldType.PACKED_DECIMAL, 9);
        pnd_Ir_Pnd_Settl_Amt = pnd_Ir.newFieldInGroup("pnd_Ir_Pnd_Settl_Amt", "#SETTL-AMT", FieldType.PACKED_DECIMAL, 13, 2);
        pnd_Ir_Pnd_Net_Pymnt_Amt = pnd_Ir.newFieldInGroup("pnd_Ir_Pnd_Net_Pymnt_Amt", "#NET-PYMNT-AMT", FieldType.PACKED_DECIMAL, 13, 2);

        pnd_Lit_Occurs_Ir = localVariables.newGroupArrayInRecord("pnd_Lit_Occurs_Ir", "#LIT-OCCURS-IR", new DbsArrayController(1, 4));
        pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir = pnd_Lit_Occurs_Ir.newFieldInGroup("pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir", "#TOTAL-LIT-IR", FieldType.STRING, 
            22);
        pnd_Gtn_W9_Ben_Ind = localVariables.newFieldInRecord("pnd_Gtn_W9_Ben_Ind", "#GTN-W9-BEN-IND", FieldType.STRING, 1);
        pnd_Gtn_Fed_Tax_Type = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Tax_Type", "#GTN-FED-TAX-TYPE", FieldType.STRING, 1, new DbsArrayController(1, 
            2));
        pnd_Gtn_Fed_Tax_Amt = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Tax_Amt", "#GTN-FED-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2, new DbsArrayController(1, 
            2));
        pnd_Gtn_Fed_Canadian_Amt = localVariables.newFieldArrayInRecord("pnd_Gtn_Fed_Canadian_Amt", "#GTN-FED-CANADIAN-AMT", FieldType.PACKED_DECIMAL, 
            9, 2, new DbsArrayController(1, 2));
        pnd_Gtn_Elec_Cde = localVariables.newFieldInRecord("pnd_Gtn_Elec_Cde", "#GTN-ELEC-CDE", FieldType.NUMERIC, 3);
        pnd_Gtn_Cntrct_Money_Source = localVariables.newFieldInRecord("pnd_Gtn_Cntrct_Money_Source", "#GTN-CNTRCT-MONEY-SOURCE", FieldType.STRING, 5);
        pnd_Gtn_Hardship_Cde = localVariables.newFieldInRecord("pnd_Gtn_Hardship_Cde", "#GTN-HARDSHIP-CDE", FieldType.STRING, 1);
        proline_Dob = localVariables.newFieldInRecord("proline_Dob", "PROLINE-DOB", FieldType.STRING, 8);
        proline_Settle_Date = localVariables.newFieldInRecord("proline_Settle_Date", "PROLINE-SETTLE-DATE", FieldType.STRING, 8);
        proline_Nra_Tax_Amt = localVariables.newFieldInRecord("proline_Nra_Tax_Amt", "PROLINE-NRA-TAX-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        proline_Fed_Can_Amt = localVariables.newFieldInRecord("proline_Fed_Can_Amt", "PROLINE-FED-CAN-AMT", FieldType.PACKED_DECIMAL, 9, 2);
        pnd_Frgn_Pymnt_Cnt = localVariables.newFieldInRecord("pnd_Frgn_Pymnt_Cnt", "#FRGN-PYMNT-CNT", FieldType.NUMERIC, 8);

        pnd_Hard_Coded_Overrides = localVariables.newGroupInRecord("pnd_Hard_Coded_Overrides", "#HARD-CODED-OVERRIDES");
        pnd_Hard_Coded_Overrides_Pnd_S0833570 = pnd_Hard_Coded_Overrides.newFieldInGroup("pnd_Hard_Coded_Overrides_Pnd_S0833570", "#S0833570", FieldType.STRING, 
            14);
        localVariables.setRecordName("localVariables");
        registerRecord(localVariables);
    }

    @Override
    public void initializeValues() throws Exception
    {
        ldaFcpl801a.initializeValues();
        ldaFcpl801c.initializeValues();
        ldaFcpl801d.initializeValues();
        ldaFcpl801p.initializeValues();
        ldaFcpl800.initializeValues();
        ldaFcplcrpt.initializeValues();

        localVariables.reset();
        pnd_Original_Csr_Cde.setInitialValue(" ");
        pnd_Pend_Pct_Omni.setInitialValue(0);
        pnd_Ws_Pnd_Terminate.setInitialValue(false);
        pnd_Ir_Index.setInitialValue(0);
        pnd_Ir_Pnd_Pymnt_Cnt.getValue(1).setInitialValue(0);
        pnd_Ir_Pnd_Rec_Cnt.getValue(1).setInitialValue(0);
        pnd_Ir_Pnd_Settl_Amt.getValue(1).setInitialValue(0);
        pnd_Ir_Pnd_Net_Pymnt_Amt.getValue(1).setInitialValue(0);
        pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir.getValue(1).setInitialValue("                      ");
        pnd_Hard_Coded_Overrides_Pnd_S0833570.setInitialValue("S0833570  03");
    }

    @Override
    public void setData(ProcessState pData) throws Exception
    {
        super.setData(pData);
        initializeValues();
    }

    // Constructor(s)
    public Fcpp801() throws Exception
    {
        super("Fcpp801");
        initializeFields(false);
        initializeValues();
    }

    // Main Methods

    public void run() throws Exception
    {
        try
        {
            runMain();
        }
        catch (Exception ex)
        {
            displayError(ex);
            throw ex;
        }
    }
    private void runMain() throws Exception
    {
        setLocalMethod("Fcpp801|Main");
        setupReports();
        while(true)
        {
            try
            {
                if (Global.isEscape()) return;                                                                                                                            //Natural: FORMAT ( 15 ) PS = 58 LS = 132 ZP = ON;//Natural: WRITE ( 15 ) TITLE LEFT *DATU '-' *TIMX ( EM = HH:IIAP ) 53T 'Consolidated Payment System' 120T 'PAGE:' *PAGE-NUMBER ( 15 ) ( EM = ZZ,ZZ9 ) / *INIT-USER '-' *PROGRAM 49T 'Annuity Payments Main Control Report' 119T 'REPORT: RPT15' //
                Global.getERROR_TA().setValue("INFP9000");                                                                                                                //Natural: MOVE 'INFP9000' TO *ERROR-TA
                //* ***************
                //*  MAIN PROGRAM *
                //* ***************
                if (condition(Global.getSTACK().getDatacount().equals(1), INPUT_1))                                                                                       //Natural: IF *DATA = 1
                {
                    DbsUtil.invokeInput(setInputStatus(INPUT_1), this, pnd_Ws_Pnd_Abend_Parm, new FieldAttributes ("AD='_'"), new ReportEditMask ("99999"));              //Natural: INPUT #ABEND-PARM ( AD = '_' EM = 99999 )
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  ABRM
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
                    sub_Error_Display_Start();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  ABRM
                    getReports().write(0, "***",new TabSetting(25),"PARAMETER CARD WAS NOT SUPPLIED",new TabSetting(77),"***");                                           //Natural: WRITE '***' 25T 'PARAMETER CARD WAS NOT SUPPLIED' 77T '***'
                    if (Global.isEscape()) return;
                    //*  ABRM
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
                    sub_Error_Display_End();
                    if (condition(Global.isEscape())) {return;}
                    if (condition(Map.getDoInput())) {return;}
                    //*  ABRM
                    DbsUtil.terminate(50);  if (true) return;                                                                                                             //Natural: TERMINATE 50
                }                                                                                                                                                         //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM DETERMINE-INTERFACE-DTE
                sub_Determine_Interface_Dte();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  BEG LEON 03/26/01
                pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir.getValue(1).setValue("TPA  Rollover       ");                                                                          //Natural: MOVE 'TPA  Rollover       ' TO #TOTAL-LIT-IR ( 1 )
                pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir.getValue(2).setValue("P&I  Rollover       ");                                                                          //Natural: MOVE 'P&I  Rollover       ' TO #TOTAL-LIT-IR ( 2 )
                pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir.getValue(3).setValue("IPRO Rollover       ");                                                                          //Natural: MOVE 'IPRO Rollover       ' TO #TOTAL-LIT-IR ( 3 )
                pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir.getValue(4).setValue("INT. Rollover Other ");                                                                          //Natural: MOVE 'INT. Rollover Other ' TO #TOTAL-LIT-IR ( 4 )
                //*  END LEON 03/26/01
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).setValue(true);                                                                           //Natural: MOVE TRUE TO #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 ) #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 ) #FCPAACUM.#NEW-PYMNT-IND
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).setValue(true);
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).setValue(true);
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);
                                                                                                                                                                          //Natural: PERFORM WRITE-HEADERS
                sub_Write_Headers();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                //*  P2210CPM.GTN
                //*  ABRM
                //*  RS5 FROM FCPA800
                //*  RS6 FROM FCPA800
                //*  RS6
                //*  RS6
                //*  RS6
                //*  ABRM
                READWORK01:                                                                                                                                               //Natural: READ WORK FILE 1 PYMNT-ADDR-INFO WF-PYMNT-TAX-REC WF-PYMNT-ADDR-GRP.IA-ORGN-CDE WF-PYMNT-ADDR-GRP.PLAN-NUMBER WF-PYMNT-ADDR-GRP.SUB-PLAN WF-PYMNT-ADDR-GRP.ORIG-CNTRCT-NBR WF-PYMNT-ADDR-GRP.ORIG-SUB-PLAN WF-PYMNT-ADDR-GRP.INV-INFO ( * )
                while (condition(getWorkFiles().read(1, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec(), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan(), 
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue("*"))))
                {
                    //*  DBH4 - START DEFAULT ANNUITY INS TYPE & INS OPTION TO "I" FOR
                    //*         CONTRACT S0833570 TO ENSURE CORRECT LEDGERING
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cmbn_Nbr().equals(pnd_Hard_Coded_Overrides_Pnd_S0833570)))                                       //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CMBN-NBR = #S0833570
                    {
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().equals("A") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(21))) //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'A' AND WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE = 21
                        {
                            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type().setValue("I");                                                                        //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE := 'I'
                            pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option().setValue("I");                                                                      //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-INSURANCE-OPTION := 'I'
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  DBH4 - END
                    pnd_Ws_Pnd_I_V_Rec_Cnt.nadd(1);                                                                                                                       //Natural: ADD 1 TO #I-V-REC-CNT
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte().setValue(pnd_Ws_Pnd_Intrfce_Dte);                                                                 //Natural: MOVE #INTRFCE-DTE TO WF-PYMNT-ADDR-GRP.PYMNT-INTRFCE-DTE
                    if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))                                                                          //Natural: IF #FCPAACUM.#NEW-PYMNT-IND
                    {
                                                                                                                                                                          //Natural: PERFORM INIT-NEW-PAYMENT
                        sub_Init_New_Payment();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().setValuesByName(pnd_Ws_Pnd_Wf_Save_Fields);                                                       //Natural: MOVE BY NAME #WF-SAVE-FIELDS TO WF-PYMNT-ADDR-REC
                    pnd_Ws_Pnd_Internal_Rollover.setValue(false);                                                                                                         //Natural: MOVE FALSE TO #INTERNAL-ROLLOVER
                    ldaFcpl801a.getWf_Error_Msg().reset();                                                                                                                //Natural: RESET WF-ERROR-MSG
                    //*  10/19/2010 DBH1 START
                    pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Lgrs().reset();                                                                                                  //Natural: RESET #GL-PEND-LGRS
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals(ldaFcpl801p.getPnd_Valid_Pend_Codes().getValue("*"))))                       //Natural: IF PYMNT-SUSPEND-CDE = #VALID-PEND-CODES ( * )
                    {
                        //*  DBH2
                        //*  DBH2
                        if (condition(((((pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue("*").greaterOrEqual(2) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue("*").lessOrEqual(11))  //Natural: IF INV-ACCT-CDE-N ( * ) = 2 THRU 11 OR INV-ACCT-CDE-N ( * ) = 42 THRU 49 OR INV-ACCT-CDE-N ( * ) = 1 OR INV-ACCT-CDE-N ( * ) = 20 THRU 21
                            || (pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue("*").greaterOrEqual(42) && pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue("*").lessOrEqual(49))) 
                            || pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue("*").equals(1)) || (pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue("*").greaterOrEqual(20) 
                            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue("*").lessOrEqual(21)))))
                        {
                            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Lgrs().setValue(true);                                                                                   //Natural: ASSIGN #GL-PEND-LGRS := TRUE
                            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Ppcn_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                //Natural: ASSIGN #GL-CNTRCT-PPCN-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
                            DbsUtil.callnat(Fcpn145s.class , getCurrentProcessState(), pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Ppcn_Nbr(), pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ia_Origin()); //Natural: CALLNAT 'FCPN145S' #GL-CNTRCT-PPCN-NBR #GL-IA-ORIGIN
                            if (condition(Global.isEscape())) return;
                            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ia_Origin().equals("04")))                                                                      //Natural: IF #GL-IA-ORIGIN = '04'
                            {
                                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("M") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("1"))) //Natural: IF PYMNT-SUSPEND-CDE = 'M' OR = '1'
                                {
                                    ignore();
                                }                                                                                                                                         //Natural: ELSE
                                else if (condition())
                                {
                                    pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Lgrs().reset();                                                                                  //Natural: RESET #GL-PEND-LGRS
                                }                                                                                                                                         //Natural: END-IF
                            }                                                                                                                                             //Natural: END-IF
                            //*  DBH3 END
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  10/19/2010 DBH1 END
                    //*  CTS-0115 >>
                    pnd_Gtn_W9_Ben_Ind.setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_W9_Ben_Ind());                                                                         //Natural: MOVE WF-PYMNT-TAX-GRP.GTN-W9-BEN-IND TO #GTN-W9-BEN-IND
                    pnd_Gtn_Fed_Tax_Type.getValue("*").setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Tax_Type().getValue("*"));                                         //Natural: MOVE WF-PYMNT-TAX-GRP.GTN-FED-TAX-TYPE ( * ) TO #GTN-FED-TAX-TYPE ( * )
                    pnd_Gtn_Fed_Tax_Amt.getValue("*").setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Tax_Amt().getValue("*"));                                           //Natural: MOVE WF-PYMNT-TAX-GRP.GTN-FED-TAX-AMT ( * ) TO #GTN-FED-TAX-AMT ( * )
                    pnd_Gtn_Fed_Canadian_Amt.getValue("*").setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Canadian_Amt().getValue("*"));                                 //Natural: MOVE WF-PYMNT-TAX-GRP.GTN-FED-CANADIAN-AMT ( * ) TO #GTN-FED-CANADIAN-AMT ( * )
                    pnd_Gtn_Elec_Cde.setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Elec_Cde());                                                                             //Natural: MOVE WF-PYMNT-TAX-GRP.GTN-ELEC-CDE TO #GTN-ELEC-CDE
                    pnd_Gtn_Hardship_Cde.setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Hardship_Cde());                                                                     //Natural: MOVE WF-PYMNT-TAX-GRP.GTN-HARDSHIP-CDE TO #GTN-HARDSHIP-CDE
                    pnd_Gtn_Cntrct_Money_Source.setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Cntrct_Money_Source());                                                       //Natural: MOVE WF-PYMNT-TAX-GRP.GTN-CNTRCT-MONEY-SOURCE TO #GTN-CNTRCT-MONEY-SOURCE
                    //*  CTS-0115 <<
                    //*  ERROR IN GTN
                    short decideConditionsMet1388 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-RET-CODE = '0003'
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Ret_Code().equals("0003")))
                    {
                        decideConditionsMet1388++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(13);                                                                                       //Natural: ASSIGN #ACCUM-OCCUR := 13
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-FILE
                        sub_Write_Error_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: WHEN OFF-MODE-CONTRACT
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Off_Mode_Contract().getBoolean()))
                    {
                        decideConditionsMet1388++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(16);                                                                                       //Natural: ASSIGN #ACCUM-OCCUR := 16
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet1388 > 0))
                    {
                        //* *    WRITE 'P1.5 WHEN ANY '
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                        sub_Accum_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  COMMENT OUT - WILL NOT BE PERFORMED EVEN IF CALLED >>> ROXAN 05/08/01
                        //*     PERFORM DECIDE-ROLLOVERS                     /* LEON  3/26/2001
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-NEW-PYMNT
                        sub_Check_For_New_Pymnt();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().setValuesByName(pnd_Ws_Pnd_Wf_Save_Fields_1);                                                 //Natural: MOVE BY NAME #WF-SAVE-FIELDS-1 TO WF-PYMNT-ADDR-REC
                        //*       PERFORM WRITE-TAX-FILE             /* CTS-0115
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec().setValuesByName(pnd_Ws_Pnd_Wf_Save_Fields_1);                                                     //Natural: MOVE BY NAME #WF-SAVE-FIELDS-1 TO WF-PYMNT-ADDR-REC
                    //*    PERFORM WRITE-TAX-FILE                /* CTS-0115
                    //*  NO TAXES
                    short decideConditionsMet1415 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-CNTRCT-RPT-CODE ( * ) = 3
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code().getValue("*").equals(3)))
                    {
                        decideConditionsMet1415++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(6);                                                                                        //Natural: ASSIGN #ACCUM-OCCUR := 6
                        //*  NO DED
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-FILE
                        sub_Write_Error_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-CNTRCT-RPT-CODE ( * ) = 4
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code().getValue("*").equals(4)))
                    {
                        decideConditionsMet1415++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(7);                                                                                        //Natural: ASSIGN #ACCUM-OCCUR := 7
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-FILE
                        sub_Write_Error_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  NO TAXES/DED
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Grp().reset();                                                                                          //Natural: RESET PYMNT-DED-GRP
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-CNTRCT-RPT-CODE ( * ) = 5
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Cntrct_Rpt_Code().getValue("*").equals(5)))
                    {
                        decideConditionsMet1415++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(6);                                                                                        //Natural: ASSIGN #ACCUM-OCCUR := 6
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                        sub_Accum_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(7);                                                                                        //Natural: ASSIGN #ACCUM-OCCUR := 7
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-FILE
                        sub_Write_Error_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Grp().reset();                                                                                          //Natural: RESET PYMNT-DED-GRP
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet1415 > 0))
                    {
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                        sub_Accum_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    //*  (LB) DED CODES 008 CTL TOTALS
                                                                                                                                                                          //Natural: PERFORM MUT-FNDS-CNTL-TOTALS
                    sub_Mut_Fnds_Cntl_Totals();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                    //*  ADDED UNIVERSAL LIFE 010 TMM
                    //*  ADDED PA-SELECT 009 TMM
                    //*  10/19/2010 DBH1 START
                    if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Lgrs().getBoolean()))                                                                              //Natural: IF #GL-PEND-LGRS
                    {
                                                                                                                                                                          //Natural: PERFORM GEN-LEDGERS
                        sub_Gen_Ledgers();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    //* *
                    //*  10/19/2010 DBH1 END
                    short decideConditionsMet1458 = 0;                                                                                                                    //Natural: DECIDE FOR FIRST CONDITION;//Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-PYMNT-RPT-CODE ( * ) = 9
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code().getValue("*").equals(9)))
                    {
                        decideConditionsMet1458++;
                        //*  UNCLAIMED SUMS
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("K")))                                                                   //Natural: IF PYMNT-SUSPEND-CDE = 'K'
                        {
                            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(9);                                                                                    //Natural: ASSIGN #ACCUM-OCCUR := 9
                        }                                                                                                                                                 //Natural: END-IF
                        //* BLOCKED ACCOUNTS
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde().equals("I")))                                                                   //Natural: IF PYMNT-SUSPEND-CDE = 'I'
                        {
                            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(8);                                                                                    //Natural: ASSIGN #ACCUM-OCCUR := 8
                        }                                                                                                                                                 //Natural: END-IF
                        //*  MISSING NAME
                        //*  INVALID COMBINE
                        //*  MISSING PIN
                        //*  MISC PENDS
                                                                                                                                                                          //Natural: PERFORM GEN-LEDGERS
                        sub_Gen_Ledgers();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-PYMNT-RPT-CODE ( * ) = 10
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code().getValue("*").equals(10)))
                    {
                        decideConditionsMet1458++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(10);                                                                                       //Natural: ASSIGN #ACCUM-OCCUR := 10
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-PYMNT-RPT-CODE ( * ) = 11
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code().getValue("*").equals(11)))
                    {
                        decideConditionsMet1458++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(11);                                                                                       //Natural: ASSIGN #ACCUM-OCCUR := 11
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-PYMNT-RPT-CODE ( * ) = 12
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code().getValue("*").equals(12)))
                    {
                        decideConditionsMet1458++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(14);                                                                                       //Natural: ASSIGN #ACCUM-OCCUR := 14
                    }                                                                                                                                                     //Natural: WHEN WF-PYMNT-ADDR-GRP.GTN-PYMNT-RPT-CODE ( * ) = 13
                    else if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Pymnt_Rpt_Code().getValue("*").equals(13)))
                    {
                        decideConditionsMet1458++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(12);                                                                                       //Natural: ASSIGN #ACCUM-OCCUR := 12
                    }                                                                                                                                                     //Natural: WHEN ANY
                    if (condition(decideConditionsMet1458 > 0))
                    {
                                                                                                                                                                          //Natural: PERFORM WRITE-REPORT-FILE
                        sub_Write_Report_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                        sub_Accum_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  COMMENT OUT - WILL NOT BE PERFORMED EVEN IF CALLED >>> ROXAN 05/08/01
                        //*     PERFORM DECIDE-ROLLOVERS                     /* LEON  3/26/2001
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-NEW-PYMNT
                        sub_Check_For_New_Pymnt();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        if (condition(true)) continue;                                                                                                                    //Natural: ESCAPE TOP
                    }                                                                                                                                                     //Natural: WHEN NONE
                    else if (condition())
                    {
                        ignore();
                    }                                                                                                                                                     //Natural: END-DECIDE
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Dvdnd_Payee_Ind().equals(2)))                                                                    //Natural: IF CNTRCT-DVDND-PAYEE-IND = 2
                    {
                                                                                                                                                                          //Natural: PERFORM COLLEGE-DIVIDENDS
                        sub_College_Dividends();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-IF
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().getSubstring(2,1).equals("M")))                                                       //Natural: IF SUBSTR ( WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE,2,1 ) = 'M'
                    {
                        //*     PERFORM ANNOTATIONS                                  /* CTS-0115
                        //*  CTS-0115
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Annot_Ind().setValue("Y");                                                                                  //Natural: MOVE 'Y' TO PYMNT-ANNOT-IND
                    }                                                                                                                                                     //Natural: END-IF
                    //*  /*  EFT     CHECKING OR SAVINGS ACCOUNT INDICATOR    -  RCC
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2)))                                                                    //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND = 2
                    {
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Chk_Sav_Ind().equals("C") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Chk_Sav_Ind().equals("S"))) //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHK-SAV-IND = 'C' OR = 'S'
                        {
                            ignore();
                        }                                                                                                                                                 //Natural: ELSE
                        else if (condition())
                        {
                            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().setValue(1);                                                                         //Natural: MOVE 1 TO WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND
                            pnd_Ws_Pnd_Zero_Occurs.setValue(0);                                                                                                           //Natural: MOVE 0 TO #ZERO-OCCURS
                            //*   PREV 19
                            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(22);                                                                                   //Natural: MOVE 22 TO #ACCUM-OCCUR
                        }                                                                                                                                                 //Natural: END-IF
                    }                                                                                                                                                     //Natural: END-IF
                    //*  /*  EFT
                    //*  CHECKS
                    short decideConditionsMet1517 = 0;                                                                                                                    //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 1
                    if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(1))))
                    {
                        decideConditionsMet1517++;
                        if (condition(pnd_Ws_Pnd_Zero_Occurs.notEquals(getZero())))                                                                                       //Natural: IF #ZERO-OCCURS NE 0
                        {
                            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(pnd_Ws_Pnd_Zero_Occurs);                                                               //Natural: ASSIGN #ACCUM-OCCUR := #ZERO-OCCURS
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                            sub_Accum_Control_Totals();
                            if (condition(Global.isEscape()))
                            {
                                if (condition(Global.isEscapeBottom())) break;
                                else if (condition(Global.isEscapeBottomImmediate())) break;
                                else if (condition(Global.isEscapeTop())) continue;
                                else if (condition(Global.isEscapeRoutine())) return;
                                else break;
                            }
                            if (condition(Map.getDoInput())) {return;}
                            //*  COMMENT OUT - WILL NOT BE PERFORMED EVEN IF CALLED >>> ROXAN 05/08/01
                            //*       PERFORM DECIDE-ROLLOVERS                     /* LEON  3/26/2001
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name().equals(" ") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name().equals(" ")           //Natural: IF PH-LAST-NAME = ' ' AND PH-FIRST-NAME = ' ' AND PH-MIDDLE-NAME = ' '
                            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Middle_Name().equals(" ")))
                        {
                            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(" ")))                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = ' '
                            {
                                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().setValue(" AAA");                                                                       //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE := ' AAA'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().setValue(" AAB");                                                                       //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE := ' AAB'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1).equals(" ") && pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Lines().getValue(1).equals(" "))) //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-NME ( 1 ) = ' ' AND WF-PYMNT-ADDR-GRP.PYMNT-ADDR-LINES ( 1 ) = ' '
                        {
                            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(" ")))                                                                 //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = ' '
                            {
                                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().setValue(" AAC");                                                                       //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE := ' AAC'
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().setValue(" AAD");                                                                       //Natural: ASSIGN WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE := ' AAD'
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                                                                                                                                                                          //Natural: PERFORM WRITE-PROCESS-STMNT-FIELDS
                        sub_Write_Process_Stmnt_Fields();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  UNRESOLVED EFT - RCC PREV 19
                        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().notEquals(22)))                                                                       //Natural: IF #ACCUM-OCCUR NE 22
                        {
                            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(new DbsDecimal("0.00"))))                                              //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT = 0.00
                            {
                                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(15);                                                                               //Natural: ASSIGN #ACCUM-OCCUR := 15
                            }                                                                                                                                             //Natural: ELSE
                            else if (condition())
                            {
                                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(1);                                                                                //Natural: ASSIGN #ACCUM-OCCUR := 1
                            }                                                                                                                                             //Natural: END-IF
                        }                                                                                                                                                 //Natural: END-IF
                        //*  ---- RESET DCI AMOUNT FIELD WHICH WAS TEMPORARILY USED TO HOLD ---- *
                        //*  - THE DEDUCTION AMOUNT APPLIED TO THE NET PER FUND  - RCC 02/06/01 -*
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded().getValue("*").reset();                                                                     //Natural: RESET WF-PYMNT-ADDR-GRP.INV-ACCT-NET-ADJ-DED ( * )
                        //*  CTS-0115 >>
                        //*      WRITE WORK FILE 6 VARIABLE                 /* CHECKS.S488
                        //*        #CHECK-SORT-FIELDS PYMNT-ADDR-INFO INV-INFO(1:INV-ACCT-COUNT)
                        //*  CHECKS.S488
                        getWorkFiles().write(6, false, pdaFcpa801c.getPnd_Check_Sort_Fields(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",40),  //Natural: WRITE WORK FILE 6 #CHECK-SORT-FIELDS PYMNT-ADDR-INFO INV-INFO ( 1:40 ) CANADIAN-TAX-AMT-ALPHA WF-PYMNT-ADDR-GRP.IA-ORGN-CDE PLAN-NUMBER SUB-PLAN ORIG-CNTRCT-NBR ORIG-SUB-PLAN #GTN-W9-BEN-IND #GTN-FED-TAX-TYPE ( * ) #GTN-FED-TAX-AMT ( * ) #GTN-FED-CANADIAN-AMT ( * ) #GTN-ELEC-CDE #GTN-CNTRCT-MONEY-SOURCE #GTN-HARDSHIP-CDE
                            pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt_Alpha(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number(), 
                            pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan(), 
                            pnd_Gtn_W9_Ben_Ind, pnd_Gtn_Fed_Tax_Type.getValue("*"), pnd_Gtn_Fed_Tax_Amt.getValue("*"), pnd_Gtn_Fed_Canadian_Amt.getValue("*"), 
                            pnd_Gtn_Elec_Cde, pnd_Gtn_Cntrct_Money_Source, pnd_Gtn_Hardship_Cde);
                        //*  CTS-0115 <<
                        //*  BEFORE.CHECK.S471
                        //*  RS5
                        //*  RS5
                        //*  EFT
                        getWorkFiles().write(11, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",40),  //Natural: WRITE WORK FILE 11 VARIABLE PYMNT-ADDR-INFO INV-INFO ( 1:40 ) WF-PYMNT-ADDR-GRP.IA-ORGN-CDE
                            pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde());
                        //*        VARIABLE PYMNT-ADDR-INFO INV-INFO(1:INV-ACCT-COUNT)
                    }                                                                                                                                                     //Natural: VALUE 2
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2))))
                    {
                        decideConditionsMet1517++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(2);                                                                                        //Natural: ASSIGN #ACCUM-OCCUR := 2
                        //*  ------------------------------------------------------------------- *
                        //*  ---- RESET DCI AMOUNT FIELD WHICH WAS TEMPORARILY USED TO HOLD ---- *
                        //*  - THE DEDUCTION AMOUNT APPLIED TO THE NET PER FUND  - RCC 02/06/01 -*
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded().getValue("*").reset();                                                                     //Natural: RESET WF-PYMNT-ADDR-GRP.INV-ACCT-NET-ADJ-DED ( * )
                        //*  ------------------------------------------------------------------- *
                        //*  GLOBAL PAY
                                                                                                                                                                          //Natural: PERFORM WRITE-EFT-FILE
                        sub_Write_Eft_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: VALUE 3,4,9
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4) 
                        || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9))))
                    {
                        decideConditionsMet1517++;
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(3);                                                                                        //Natural: ASSIGN #ACCUM-OCCUR := 3
                                                                                                                                                                          //Natural: PERFORM WRITE-PROCESS-STMNT-FIELDS
                        sub_Write_Process_Stmnt_Fields();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  ------------------------------------------------------------------- *
                        //*  ---- RESET DCI AMOUNT FIELD WHICH WAS TEMPORARILY USED TO HOLD ---- *
                        //*  - THE DEDUCTION AMOUNT APPLIED TO THE NET PER FUND  - RCC 02/06/01 -*
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded().getValue("*").reset();                                                                     //Natural: RESET WF-PYMNT-ADDR-GRP.INV-ACCT-NET-ADJ-DED ( * )
                        //*  CTS-0115 >>
                        //*     WRITE WORK FILE 10 VARIABLE                /* GLOBAL.S470
                        //*       #CHECK-SORT-FIELDS PYMNT-ADDR-INFO INV-INFO(1:INV-ACCT-COUNT)
                        //*  GLOBAL.S470
                        //*  INTERNAL ROLLOVER
                        getWorkFiles().write(13, true, pdaFcpa801c.getPnd_Check_Sort_Fields(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",40),  //Natural: WRITE WORK FILE 13 VARIABLE #CHECK-SORT-FIELDS PYMNT-ADDR-INFO INV-INFO ( 1:40 ) CANADIAN-TAX-AMT-ALPHA WF-PYMNT-ADDR-GRP.IA-ORGN-CDE PLAN-NUMBER SUB-PLAN ORIG-CNTRCT-NBR ORIG-SUB-PLAN #GTN-W9-BEN-IND #GTN-FED-TAX-TYPE ( * ) #GTN-FED-TAX-AMT ( * ) #GTN-FED-CANADIAN-AMT ( * ) #GTN-ELEC-CDE #GTN-CNTRCT-MONEY-SOURCE #GTN-HARDSHIP-CDE
                            pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt_Alpha(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number(), 
                            pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan(), 
                            pnd_Gtn_W9_Ben_Ind, pnd_Gtn_Fed_Tax_Type.getValue("*"), pnd_Gtn_Fed_Tax_Amt.getValue("*"), pnd_Gtn_Fed_Canadian_Amt.getValue("*"), 
                            pnd_Gtn_Elec_Cde, pnd_Gtn_Cntrct_Money_Source, pnd_Gtn_Hardship_Cde);
                    }                                                                                                                                                     //Natural: VALUE 8
                    else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(8))))
                    {
                        decideConditionsMet1517++;
                        pnd_Ws_Pnd_Internal_Rollover.setValue(true);                                                                                                      //Natural: MOVE TRUE TO #INTERNAL-ROLLOVER
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(4);                                                                                        //Natural: ASSIGN #ACCUM-OCCUR := 4
                                                                                                                                                                          //Natural: PERFORM WRITE-EFT-FILE
                        sub_Write_Eft_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  LEON  3/26/2001
                                                                                                                                                                          //Natural: PERFORM DECIDE-ROLLOVERS
                        sub_Decide_Rollovers();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: ANY
                    if (condition(decideConditionsMet1517 > 0))
                    {
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                        sub_Accum_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                                                                                                                                                                          //Natural: PERFORM GEN-LEDGERS
                        sub_Gen_Ledgers();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*     IF WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE(1:10) = 900 THRU 999
                        //*       #TXS-PH-LAST-NAME          := WF-PYMNT-ADDR-GRP.PH-LAST-NAME
                        //*       #TXS-PH-FIRST-NAME         := WF-PYMNT-ADDR-GRP.PH-FIRST-NAME
                        //*       #TXS-PH-MIDDLE-NAME        := WF-PYMNT-ADDR-GRP.PH-MIDDLE-NAME
                        //*       #TXS-ANNT-SOC-SEC-NBR      := WF-PYMNT-ADDR-GRP.ANNT-SOC-SEC-NBR
                        //*       #TXS-CNTRCT-UNQ-ID-NBR     := WF-PYMNT-ADDR-GRP.CNTRCT-UNQ-ID-NBR
                        //*       #TXS-CNTRCT-PPCN-NBR       := WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
                        //*       #TXS-CNTRCT-PAYEE-CDE      := WF-PYMNT-ADDR-GRP.CNTRCT-PAYEE-CDE
                        //*     #TXS-PYMNT-DED-CDE(1:10)   := WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE(1:10)
                        //*     #TXS-PYMNT-DED-AMT (1:10) := WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT (1:10)
                        //*       MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE (EM=YYYYMMDD)
                        //*         TO #TXS-PYMNT-DATE
                        //*         ADD WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT (1:10) TO #WK-TOT-DED
                        //*       IF #WK-TOT-DED GT 0
                        //*        #TXS-CR-DR := 'C'
                        //*       ELSE
                        //*         #TXS-CR-DR := 'D'
                        //*       END-IF
                        //*       #TXS-CNTRCT-ORGN-CDE       := WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE
                        //*       #TXS-FIL1                  := 'X'
                        //*       WRITE WORK FILE 16 #TEXAS-CHD-OUTPUT
                        //*     END-IF
                    }                                                                                                                                                     //Natural: NONE
                    else if (condition())
                    {
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Ret_Code().setValue("0003");                                                                                  //Natural: MOVE '0003' TO WF-PYMNT-ADDR-GRP.GTN-RET-CODE
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue("*").reset();                                                                             //Natural: RESET GTN-RPT-CODE ( * )
                        pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(1).setValue(14);                                                                          //Natural: MOVE 14 TO WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( 1 )
                        pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(13);                                                                                       //Natural: ASSIGN #ACCUM-OCCUR := 13
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
                        sub_Accum_Control_Totals();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                        //*  COMMENT OUT - WILL NOT BE PERFORMED EVEN WHEN     >>>> ROXAN 05/08/01
                        //*                CALLED
                        //*     PERFORM DECIDE-ROLLOVERS                     /* LEON  3/26/2001
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-FILE
                        sub_Write_Error_File();
                        if (condition(Global.isEscape()))
                        {
                            if (condition(Global.isEscapeBottom())) break;
                            else if (condition(Global.isEscapeBottomImmediate())) break;
                            else if (condition(Global.isEscapeTop())) continue;
                            else if (condition(Global.isEscapeRoutine())) return;
                            else break;
                        }
                        if (condition(Map.getDoInput())) {return;}
                    }                                                                                                                                                     //Natural: END-DECIDE
                                                                                                                                                                          //Natural: PERFORM CHECK-FOR-NEW-PYMNT
                    sub_Check_For_New_Pymnt();
                    if (condition(Global.isEscape()))
                    {
                        if (condition(Global.isEscapeBottom())) break;
                        else if (condition(Global.isEscapeBottomImmediate())) break;
                        else if (condition(Global.isEscapeTop())) continue;
                        else if (condition(Global.isEscapeRoutine())) return;
                        else break;
                    }
                    if (condition(Map.getDoInput())) {return;}
                }                                                                                                                                                         //Natural: END-WORK
                READWORK01_Exit:
                if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM PRINT-CONTROL-REPORT
                sub_Print_Control_Report();
                if (condition(Global.isEscape())) {return;}
                if (condition(Map.getDoInput())) {return;}
                getReports().write(0, "Total number of foreign payments: ",pnd_Frgn_Pymnt_Cnt);                                                                           //Natural: WRITE 'Total number of foreign payments: ' #FRGN-PYMNT-CNT
                if (Global.isEscape()) return;
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: CHECK-FOR-NEW-PYMNT
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: INIT-NEW-PAYMENT
                //* ****************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: GEN-LEDGERS
                //*                                          END      MB 07/02/2012
                //* *#GL-DCI-AMT      (#I) := INV-ACCT-DCI-AMT      (#I)
                //* * AND #GL-INS-OPTION = ' ' AND ( #GL-LIFE-CONT = ' ')
                //* * AND #GL-INS-OPTION = ' ' AND ( #GL-LIFE-CONT = 'N' OR = 'Y')
                //* * AND #GL-INS-OPTION = ' ' AND ( #GL-LIFE-CONT = ' ')
                //* * AND #GL-INS-OPTION = ' ' AND ( #GL-LIFE-CONT = 'N' OR = 'Y')
                //* * DEDUCTION AMOUNTS BY FUNDS TO BE USED TO ADJUST NET FOR SPIA PA-SEL
                //*      ANY VALUE
                //*        WRITE '=' #GL-IVA-INVEST-VAR-ANNTY
                //*          / '=' #GL-IVA-INVEST-VAR-ANNTY-AC
                //*          / '=' #GL-IHA-INVEST-HORIZON-ANNTY
                //*          / '=' #GL-IHA-INVEST-HORIZON-ANNTY-AC
                //*          / '=' #GL-TC-LIFETIME-5-ANNTY
                //*          / '=' #GL-TC-LIFETIME-5-ANNTY-AC
                //*          / '=' #GL-CNTRCT-ORGN-CDE
                //*          / '=' WF-PYMNT-ADDR-GRP.CNTRCT-PYMNT-TYPE-IND
                //*          / '=' #GL-ANNTY-TYPE-CDE
                //*          / '=' #GL-ANNTY-INS-TYPE
                //*          / '=' WF-PYMNT-ADDR-GRP.CNTRCT-STTLMNT-TYPE-IND
                //*          / '=' #GL-OPTION-CDE
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: MUT-FNDS-CNTL-TOTALS
                //* *************************************
                //* *  WRITE #FCPAACUM.#REC-CNT (25) WF-PYMNT-ADDR-GRP.PH-FIRST-NAME
                //* *        WF-PYMNT-ADDR-GRP.PH-LAST-NAME
                //* *        WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE(#M)
                //* *        WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT (#M) #FCPAACUM.#PYMNT-CNT(25)
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-EFT-FILE
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-REPORT-FILE
                //* *********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ERROR-FILE
                //* *******************************
                //*  DEFINE SUBROUTINE WRITE-TAX-FILE                    /* CTS-0115
                //* *******************************
                //* *IF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'G'
                //* *  FOR #I = 1 TO INV-ACCT-COUNT
                //* *    IF INV-ACCT-CDE-ALPHA(#I)  = 'G'
                //* *      INV-ACCT-COMPANY   (#I) := 'G'
                //* *    END-IF
                //* *  END-FOR
                //* *END-IF
                //*  CTS-0115 >>
                //*  WRITE WORK FILE 3 VARIABLE                       /* TAX.S465 FOR TAX
                //*    PYMNT-ADDR-INFO
                //*    WF-PYMNT-TAX-REC
                //*    WF-PYMNT-ADDR-GRP.PLAN-NUMBER                   /* RS6 FROM FCPA800
                //*    WF-PYMNT-ADDR-GRP.SUB-PLAN                      /* RS6 LRECL=5106
                //*    WF-PYMNT-ADDR-GRP.ORIG-CNTRCT-NBR               /* RS6
                //*    WF-PYMNT-ADDR-GRP.ORIG-SUB-PLAN                 /* RS6
                //*    INV-INFO(1:INV-ACCT-COUNT)    /* ABRM
                //*  CTS-0115 <<
                //* *IF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE = 'G'
                //* *  FOR #I = 1 TO INV-ACCT-COUNT
                //* *    IF INV-ACCT-CDE-ALPHA(#I)  = 'G'
                //* *      INV-ACCT-COMPANY   (#I) := 'T'
                //* *    END-IF
                //* *  END-FOR
                //* *END-IF
                //*  END-SUBROUTINE                                     /* CTS-0115
                //* *******************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-PROCESS-STMNT-FIELDS
                //*  CTS-0115 >>
                //* ****************************
                //*  DEFINE SUBROUTINE ANNOTATIONS
                //* ****************************
                //*  ADD  1                         TO #ANNOT-RECORDS
                //*  MOVE BY NAME PYMNT-ADDR-INFO   TO #ANNOTATIONS
                //*  MOVE 'Y'                       TO PYMNT-ANNOT-IND
                //*  MOVE CNTRCT-HOLD-USER-ID       TO PYMNT-ANNOT-ID-NBR
                //*  MOVE *DATX                     TO PYMNT-ANNOT-DTE
                //*  WRITE WORK FILE 8 #ANNOTATIONS                   /* ANNOT.S468
                //*  END-SUBROUTINE
                //*  CTS-0115 <<
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: COLLEGE-DIVIDENDS
                //* ****************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DETERMINE-INTERFACE-DTE
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-CONTROL-REPORT
                //* *--
                //* * PA-SELECT DEDUCTION 009
                //* * UNIVERSAL-LIFE DEDUCTION 010
                //* * PERSONAL ANNUITY DEDUCTION 007
                //*                                                /* BEG LEON 3/26/2001
                //* *******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: PRINT-OFFSET
                //*                                                /* END LEON 3/26/2001
                //* ******************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-HEADERS
                //* ******************************
                //* ************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-START
                //* ************************************
                //* **********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ERROR-DISPLAY-END
                //* **********************************
                //*  INCLUDE FCPCACUM
                //* ***********************************************************************
                //*  COPYCODE   : FCPCACUM
                //*  SYSTEM     : CPS
                //*  TITLE      : IAR RESTRUCTURE
                //*  FUNCTION   : "AP" - CONTROL ACCUMULATIONS.
                //* ***********************************************************************
                //* *************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: ACCUM-CONTROL-TOTALS
                //*                                                /* BEG LEON 3/22/2001
                //* ***********************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: DECIDE-ROLLOVERS
                //* **********************************************************************
                //*  EAV RELOCATED THE FOLLOWING SUBROUTINE: WRITE-ADDR
            }
            catch(ReinputException re)
            {
                checkMethod(re);
            }
            if (!isInReinput()) break;
        }
    }
    private void sub_Check_For_New_Pymnt() throws Exception                                                                                                               //Natural: CHECK-FOR-NEW-PYMNT
    {
        if (BLNatReinput.isReinput()) return;

        //* ************************************
        //*  NEXT ITERATION OF THE LOOP IS A NEW PAYMENT
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Instmt_Nbr().equals(1)))                                                                                      //Natural: IF PYMNT-INSTMT-NBR = 1
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(true);                                                                                               //Natural: MOVE TRUE TO #FCPAACUM.#NEW-PYMNT-IND
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().setValue(false);                                                                                              //Natural: MOVE FALSE TO #FCPAACUM.#NEW-PYMNT-IND
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Init_New_Payment() throws Exception                                                                                                                  //Natural: INIT-NEW-PAYMENT
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        pnd_Ws_Pnd_Wf_Save_Fields.setValuesByName(pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                                                   //Natural: MOVE BY NAME WF-PYMNT-ADDR-REC TO #WF-SAVE-FIELDS
        pnd_Ws_Pnd_Wf_Save_Fields_1.setValuesByName(pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                                                 //Natural: MOVE BY NAME WF-PYMNT-ADDR-REC TO #WF-SAVE-FIELDS-1
        pdaFcpa801c.getPnd_Check_Sort_Fields().reset();                                                                                                                   //Natural: RESET #CHECK-SORT-FIELDS #ZERO-OCCURS
        pnd_Ws_Pnd_Zero_Occurs.reset();
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Amt().equals(new DbsDecimal("0.00"))))                                                                  //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-CHECK-AMT = 0.00
        {
            //*  EFT
            //* TMM-2001-12                     /* GLOBAL PAY
            //*  CHECK
            short decideConditionsMet1841 = 0;                                                                                                                            //Natural: DECIDE ON FIRST VALUE OF WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 2
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(2))))
            {
                decideConditionsMet1841++;
                pnd_Ws_Pnd_Zero_Occurs.setValue(17);                                                                                                                      //Natural: ASSIGN #ZERO-OCCURS := 17
            }                                                                                                                                                             //Natural: VALUE 3,4,9
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(3) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4) 
                || pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9))))
            {
                decideConditionsMet1841++;
                pnd_Ws_Pnd_Zero_Occurs.setValue(18);                                                                                                                      //Natural: ASSIGN #ZERO-OCCURS := 18
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet1841 > 0))
            {
                pnd_Ws_Pymnt_Pay_Type_Req_Ind.setValue(1);                                                                                                                //Natural: ASSIGN #WS.PYMNT-PAY-TYPE-REQ-IND := 1
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Gen_Ledgers() throws Exception                                                                                                                       //Natural: GEN-LEDGERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************
        //* TMM 11/2003
        //* CTS-0115
        //*  FCPA802
        //*  FCPA802
        //*  FCPA802
        ldaFcpl801a.getWf_Error_Msg().reset();                                                                                                                            //Natural: RESET WF-ERROR-MSG #GL-ERROR #GL-ERROR-MSG #GL-LOB-CDE #GL-DEDUCTIONS ( * ) #GL-DED-TOP
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error().reset();
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error_Msg().reset();
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lob_Cde().reset();
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Deductions().getValue("*").reset();
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ded_Top().reset();
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lob_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Lob_Cde());                                                             //Natural: ASSIGN #GL-LOB-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-LOB-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde());                                                    //Natural: ASSIGN #GL-CNTRCT-ORGN-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Type_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Type_Cde());                                                    //Natural: ASSIGN #GL-CNTRCT-TYPE-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-TYPE-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Ppcn_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                                    //Natural: ASSIGN #GL-CNTRCT-PPCN-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Payee_Code().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Payee_Cde());                                                        //Natural: ASSIGN #GL-PAYEE-CODE := WF-PYMNT-ADDR-GRP.CNTRCT-PAYEE-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Csr().setValue(" ");                                                                                                              //Natural: ASSIGN #GL-CSR := ' '
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rsdncy_Cde().setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Annt_Rsdncy_Cde());                                                      //Natural: ASSIGN #GL-RSDNCY-CDE := GTN-ANNT-RSDNCY-CDE
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde().setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Annt_Rsdncy_Cde());                                                 //Natural: ASSIGN WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE := GTN-ANNT-RSDNCY-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Locality_Cde().setValue(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Annt_Locality_Cde());                                                  //Natural: ASSIGN #GL-LOCALITY-CDE := GTN-ANNT-LOCALITY-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Payee_Tx_Elct_Trggr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Payee_Tx_Elct_Trggr());                                //Natural: ASSIGN #GL-PYMNT-PAYEE-TX-ELCT-TRGGR := WF-PYMNT-ADDR-GRP.PYMNT-PAYEE-TX-ELCT-TRGGR
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pay_Type_Req_Ind().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind());                                            //Natural: ASSIGN #GL-PAY-TYPE-REQ-IND := WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND
        //*   OVERRIDE GLOBAL PAY TMM-2001-12
        short decideConditionsMet1873 = 0;                                                                                                                                //Natural: DECIDE ON FIRST WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND;//Natural: VALUE 4
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(4))))
        {
            decideConditionsMet1873++;
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pay_Type_Req_Ind().setValue(3);                                                                                               //Natural: ASSIGN #GL-PAY-TYPE-REQ-IND := 3
        }                                                                                                                                                                 //Natural: VALUE 9
        else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind().equals(9))))
        {
            decideConditionsMet1873++;
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pay_Type_Req_Ind().setValue(3);                                                                                               //Natural: ASSIGN #GL-PAY-TYPE-REQ-IND := 3
        }                                                                                                                                                                 //Natural: NONE
        else if (condition())
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
        //*                                          START    MB 07/02/2012
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals("G")))                                                                                     //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = 'G'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pay_Type_Req_Ind().setValue(3);                                                                                               //Natural: ASSIGN #GL-PAY-TYPE-REQ-IND := 3
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Ins_Type());                                               //Natural: ASSIGN #GL-ANNTY-INS-TYPE := WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-INS-TYPE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde());                                               //Natural: ASSIGN #GL-ANNTY-TYPE-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ins_Option().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Insurance_Option());                                                 //Natural: ASSIGN #GL-INS-OPTION := WF-PYMNT-ADDR-GRP.CNTRCT-INSURANCE-OPTION
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Suspend_Cde());                                                         //Natural: ASSIGN #GL-PEND-CDE := WF-PYMNT-ADDR-GRP.PYMNT-SUSPEND-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Check_Dte().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte());                                                    //Natural: ASSIGN #GL-PYMNT-CHECK-DTE := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE
        //*  MATURITY
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Annty_Type_Cde().equals("M")))                                                                               //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-ANNTY-TYPE-CDE = 'M'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Life_Cont().reset();                                                                                                          //Natural: RESET #GL-LIFE-CONT
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Life_Cont().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Life_Contingency());                                              //Natural: ASSIGN #GL-LIFE-CONT := WF-PYMNT-ADDR-GRP.CNTRCT-LIFE-CONTINGENCY
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Internal_Rollover().setValue(pnd_Ws_Pnd_Internal_Rollover.getBoolean());                                                          //Natural: ASSIGN #GL-INTERNAL-ROLLOVER := #INTERNAL-ROLLOVER
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dc_Pended().setValue(false);                                                                                                      //Natural: ASSIGN #GL-DC-PENDED := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb().setValue(false);                                                                                                            //Natural: ASSIGN #GL-RTB := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ivc_From_Rollover().setValue(false);                                                                                              //Natural: ASSIGN #GL-IVC-FROM-ROLLOVER := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Top().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                                 //Natural: ASSIGN #GL-TOP := INV-ACCT-COUNT
        //*  TEST FOR STABLE RETURN JWO 01/29/2009
        if (condition(((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind().equals("A") || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind().equals("N"))       //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-PYMNT-TYPE-IND = 'A' OR = 'N' AND WF-PYMNT-ADDR-GRP.CNTRCT-STTLMNT-TYPE-IND = 'S'
            && pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().equals("S"))))
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Stable_Return().setValue(true);                                                                                               //Natural: ASSIGN #GL-STABLE-RETURN := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Stable_Return().setValue(false);                                                                                              //Natural: ASSIGN #GL-STABLE-RETURN := FALSE
        }                                                                                                                                                                 //Natural: END-IF
        //*  FE201310 START
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_457b_Flag().reset();                                                                                                              //Natural: RESET #GL-457B-FLAG
        if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind().equals("M") && (pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().equals("S")      //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-PYMNT-TYPE-IND = 'M' AND WF-PYMNT-ADDR-GRP.CNTRCT-STTLMNT-TYPE-IND = 'S' OR = 'T'
            || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().equals("T")))))
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_457b_Flag().setValue(true);                                                                                                   //Natural: ASSIGN #GL-457B-FLAG := TRUE
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_457b_Flag().setValue(false);                                                                                                  //Natural: ASSIGN #GL-457B-FLAG := FALSE
            //*  FE201310 END
        }                                                                                                                                                                 //Natural: END-IF
        FOR01:                                                                                                                                                            //Natural: FOR #I = 1 TO #GL-TOP
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Top())); pnd_Ws_Pnd_I.nadd(1))
        {
            //*  TEST FOR TIAA ACCESS JWO 01/29/2009
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Ws_Pnd_I).equals("D")))                                                       //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #I ) = 'D'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa_Access().getValue(pnd_Ws_Pnd_I).setValue(true);                                                                      //Natural: ASSIGN #GL-TIAA-ACCESS ( #I ) := TRUE
                getReports().write(0, "TIAA Access:","=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_Alpha().getValue(pnd_Ws_Pnd_I),"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr(), //Natural: WRITE 'TIAA Access:' '=' WF-PYMNT-ADDR-GRP.INV-ACCT-CDE-ALPHA ( #I ) '=' WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR '=' WF-PYMNT-ADDR-GRP.CNTRCT-LOB-CDE
                    "=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Lob_Cde());
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa_Access().getValue(pnd_Ws_Pnd_I).setValue(false);                                                                     //Natural: ASSIGN #GL-TIAA-ACCESS ( #I ) := FALSE
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fund().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cde_N().getValue(pnd_Ws_Pnd_I));              //Natural: ASSIGN #GL-FUND ( #I ) := INV-ACCT-CDE-N ( #I )
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fund().getValue(pnd_Ws_Pnd_I).equals(9)))                                                                       //Natural: IF #GL-FUND ( #I ) = 09
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Company_Cde().getValue(pnd_Ws_Pnd_I).setValue("R");                                                                       //Natural: ASSIGN #GL-COMPANY-CDE ( #I ) := 'R'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Company_Cde().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-COMPANY-CDE ( #I ) := INV-ACCT-COMPANY ( #I )
            }                                                                                                                                                             //Natural: END-IF
            //*  SPIA PA-SELECT FIXED
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Company_Cde().getValue(pnd_Ws_Pnd_I).equals("T") || pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fund().getValue(pnd_Ws_Pnd_I).equals(40)  //Natural: IF #GL-COMPANY-CDE ( #I ) = 'T' OR #GL-FUND ( #I ) = 40 OR = 41
                || pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fund().getValue(pnd_Ws_Pnd_I).equals(41)))
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-CNTRCT-AMT ( #I ) := INV-ACCT-CNTRCT-AMT ( #I )
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-DVDND-AMT ( #I ) := INV-ACCT-DVDND-AMT ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Amt().getValue(pnd_Ws_Pnd_I).reset();                                                                              //Natural: RESET #GL-CNTRCT-AMT ( #I ) #GL-DVDND-AMT ( #I )
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I).reset();
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa_Access().getValue(pnd_Ws_Pnd_I).getBoolean()))                                                             //Natural: IF #GL-TIAA-ACCESS ( #I )
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Amt().getValue(pnd_Ws_Pnd_I).reset();                                                                              //Natural: RESET #GL-CNTRCT-AMT ( #I ) #GL-DVDND-AMT ( #I )
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dvdnd_Amt().getValue(pnd_Ws_Pnd_I).reset();
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Settl_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(pnd_Ws_Pnd_I));     //Natural: ASSIGN #GL-SETTL-AMT ( #I ) := INV-ACCT-SETTL-AMT ( #I )
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dpi_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(pnd_Ws_Pnd_I));         //Natural: ASSIGN #GL-DPI-AMT ( #I ) := INV-ACCT-DPI-AMT ( #I )
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Exp_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Exp_Amt().getValue(pnd_Ws_Pnd_I));         //Natural: ASSIGN #GL-EXP-AMT ( #I ) := INV-ACCT-EXP-AMT ( #I )
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-FDRL-TAX-AMT ( #I ) := INV-ACCT-FDRL-TAX-AMT ( #I )
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_State_Tax_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-STATE-TAX-AMT ( #I ) := INV-ACCT-STATE-TAX-AMT ( #I )
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-LOCAL-TAX-AMT ( #I ) := INV-ACCT-LOCAL-TAX-AMT ( #I )
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-NET-PYMNT-AMT ( #I ) := INV-ACCT-NET-PYMNT-AMT ( #I )
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Valuat_Period().getValue(pnd_Ws_Pnd_I).equals("M")))                                                   //Natural: IF WF-PYMNT-ADDR-GRP.INV-ACCT-VALUAT-PERIOD ( #I ) = 'M'
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).setValue(true);                                                                //Natural: ASSIGN #GL-MONTHLY-VALUATION ( #I ) := TRUE
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).setValue(false);                                                               //Natural: ASSIGN #GL-MONTHLY-VALUATION ( #I ) := FALSE
            }                                                                                                                                                             //Natural: END-IF
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        FOR02:                                                                                                                                                            //Natural: FOR #I = 1 TO 10
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(10)); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I).equals(getZero())))                                                      //Natural: IF PYMNT-DED-CDE ( #I ) = 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ded_Top().setValue(pnd_Ws_Pnd_I);                                                                                             //Natural: ASSIGN #GL-DED-TOP := #I
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Ded_Cde().getValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ded_Top()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-PYMNT-DED-CDE ( #GL-DED-TOP ) := PYMNT-DED-CDE ( #I )
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Ded_Amt().getValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ded_Top()).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-PYMNT-DED-AMT ( #GL-DED-TOP ) := PYMNT-DED-AMT ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_Dc().setValue(false);                                                                                                   //Natural: ASSIGN #GL-PA-SELECT-DC := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_M().setValue(false);                                                                                                    //Natural: ASSIGN #GL-PA-SELECT-M := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_M().setValue(false);                                                                                                         //Natural: ASSIGN #GL-SPIA-M := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_Dc().setValue(false);                                                                                                        //Natural: ASSIGN #GL-SPIA-DC := FALSE
        if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type().equals("S") && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde().equals("M")))                      //Natural: IF #GL-ANNTY-INS-TYPE = 'S' AND #GL-ANNTY-TYPE-CDE = 'M'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_M().setValue(true);                                                                                                 //Natural: ASSIGN #GL-PA-SELECT-M := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition((pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type().equals("S") && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde().equals("D"))))                    //Natural: IF ( #GL-ANNTY-INS-TYPE = 'S' AND #GL-ANNTY-TYPE-CDE = 'D' )
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_Dc().setValue(true);                                                                                                //Natural: ASSIGN #GL-PA-SELECT-DC := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type().equals("M") && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde().equals("M")))                      //Natural: IF #GL-ANNTY-INS-TYPE = 'M' AND #GL-ANNTY-TYPE-CDE = 'M'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_M().setValue(true);                                                                                                      //Natural: ASSIGN #GL-SPIA-M := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type().equals("M") && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde().equals("D")))                      //Natural: IF #GL-ANNTY-INS-TYPE = 'M' AND #GL-ANNTY-TYPE-CDE = 'D'
        {
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_Dc().setValue(true);                                                                                                     //Natural: ASSIGN #GL-SPIA-DC := TRUE
        }                                                                                                                                                                 //Natural: END-IF
        FOR03:                                                                                                                                                            //Natural: FOR #I 1 TO #GL-TOP
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Top())); pnd_Ws_Pnd_I.nadd(1))
        {
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_Dc().getBoolean() || pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_M().getBoolean() ||                    //Natural: IF #GL-PA-SELECT-DC OR #GL-PA-SELECT-M OR #GL-SPIA-M OR #GL-SPIA-DC
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_M().getBoolean() || pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_Dc().getBoolean()))
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Inv_Acct_Ded_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Adj_Ded().getValue(pnd_Ws_Pnd_I)); //Natural: ASSIGN #GL-INV-ACCT-DED-AMT ( #I ) := INV-ACCT-NET-ADJ-DED ( #I )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dci_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(pnd_Ws_Pnd_I));     //Natural: ASSIGN #GL-DCI-AMT ( #I ) := INV-ACCT-DCI-AMT ( #I )
            }                                                                                                                                                             //Natural: END-IF
            //*  ROXAN 05/11/01
            //*  DEA START
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Option_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde());                                                       //Natural: ASSIGN #GL-OPTION-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty().setValue(false);                                                                                           //Natural: ASSIGN #GL-IVA-INVEST-VAR-ANNTY := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty_Ac().setValue(false);                                                                                        //Natural: ASSIGN #GL-IVA-INVEST-VAR-ANNTY-AC := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty().setValue(false);                                                                                       //Natural: ASSIGN #GL-IHA-INVEST-HORIZON-ANNTY := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty_Ac().setValue(false);                                                                                    //Natural: ASSIGN #GL-IHA-INVEST-HORIZON-ANNTY-AC := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty().setValue(false);                                                                                            //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY := FALSE
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty_Ac().setValue(false);                                                                                         //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY-AC := FALSE
        if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde().equals("AP")))                                                                                    //Natural: IF #GL-CNTRCT-ORGN-CDE = 'AP'
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Pymnt_Type_Ind().equals("A") && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde().equals("M")             //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-PYMNT-TYPE-IND = 'A' AND #GL-ANNTY-TYPE-CDE = 'M' AND #GL-ANNTY-INS-TYPE = 'S'
                && pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type().equals("S")))
            {
                short decideConditionsMet1999 = 0;                                                                                                                        //Natural: DECIDE ON FIRST VALUE WF-PYMNT-ADDR-GRP.CNTRCT-STTLMNT-TYPE-IND;//Natural: VALUE '1'
                if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().equals("1"))))
                {
                    decideConditionsMet1999++;
                    if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Option_Cde().equals(21)))                                                                               //Natural: IF #GL-OPTION-CDE = 21
                    {
                        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty_Ac().setValue(true);                                                                         //Natural: ASSIGN #GL-IVA-INVEST-VAR-ANNTY-AC := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty().setValue(true);                                                                            //Natural: ASSIGN #GL-IVA-INVEST-VAR-ANNTY := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE '2'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().equals("2"))))
                {
                    decideConditionsMet1999++;
                    if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Option_Cde().equals(21)))                                                                               //Natural: IF #GL-OPTION-CDE = 21
                    {
                        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty_Ac().setValue(true);                                                                     //Natural: ASSIGN #GL-IHA-INVEST-HORIZON-ANNTY-AC := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty().setValue(true);                                                                        //Natural: ASSIGN #GL-IHA-INVEST-HORIZON-ANNTY := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: VALUE '3'
                else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Sttlmnt_Type_Ind().equals("3"))))
                {
                    decideConditionsMet1999++;
                    pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty().setValue(true);                                                                                 //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY := TRUE
                    if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Option_Cde().equals(21)))                                                                               //Natural: IF #GL-OPTION-CDE = 21
                    {
                        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty_Ac().setValue(true);                                                                          //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY-AC := TRUE
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty().setValue(true);                                                                             //Natural: ASSIGN #GL-TC-LIFETIME-5-ANNTY := TRUE
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: NONE
                else if (condition())
                {
                    ignore();
                }                                                                                                                                                         //Natural: END-DECIDE
            }                                                                                                                                                             //Natural: END-IF
            //*  DEA END
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Can_Tax_Amt().getValue("*").reset();                                                                                              //Natural: RESET #GL-CAN-TAX-AMT ( * )
        //*  DBH2
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Can_Tax_Amt().getValue(1).nadd(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Canadian_Amt().getValue("*"));                              //Natural: ADD GTN-FED-CANADIAN-AMT ( * ) TO #GL-CAN-TAX-AMT ( 1 )
        pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ia_Origin().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde());                                                              //Natural: ASSIGN #GL-IA-ORIGIN := WF-PYMNT-ADDR-GRP.IA-ORGN-CDE
        //*  CALLNAT 'FCPN143' USING #GL-PDA              /* CTS-0115
        //*  CTS-0115
        //*  CTS-0115
        DbsUtil.callnat(Fcpn143v.class , getCurrentProcessState(), pdaFcpa143.getPnd_Gl_Pda(), pnd_Original_Csr_Cde, pnd_Pend_Pct_Omni);                                  //Natural: CALLNAT 'FCPN143V' USING #GL-PDA #ORIGINAL-CSR-CDE #PEND-PCT-OMNI
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error().equals("0000")))                                                                                            //Natural: IF #GL-ERROR = '0000'
        {
            pnd_Ws_Pnd_Ledger_Records.nadd(1);                                                                                                                            //Natural: ADD 1 TO #LEDGER-RECORDS
            ldaFcpl801c.getPnd_Ledger_Ext_Pnd_Ledger_Grp_1().setValuesByName(pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                        //Natural: MOVE BY NAME WF-PYMNT-ADDR-REC TO #LEDGER-GRP-1
            //*  RS0
            //*  RS0
            //*  RS0
            ldaFcpl801d.getPnd_Ledger_Ext_2_Pnd_Ledger_Grp_2().setValuesByName(pdaFcpa800.getWf_Pymnt_Addr_Grp_Wf_Pymnt_Addr_Rec());                                      //Natural: MOVE BY NAME WF-PYMNT-ADDR-REC TO #LEDGER-GRP-2
            ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Life_Contingency().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Life_Cont());                                                //Natural: ASSIGN #LEDGER-EXT.CNTRCT-LIFE-CONTINGENCY := #GL-LIFE-CONT
            ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Life_Contingency().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Life_Cont());                                              //Natural: ASSIGN #LEDGER-EXT-2.CNTRCT-LIFE-CONTINGENCY := #GL-LIFE-CONT
            ldaFcpl801c.getPnd_Ledger_Ext_Cntrct_Company_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(1));                                  //Natural: ASSIGN #LEDGER-EXT.CNTRCT-COMPANY-CDE := WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( 1 )
            ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Company_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(1));                                //Natural: ASSIGN #LEDGER-EXT-2.CNTRCT-COMPANY-CDE := WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( 1 )
            //*  RS0
            ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Check_Dte().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));           //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #LEDGER-EXT-2.PYMNT-CHECK-DTE
            //*  RS2
            ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Settlmnt_Dte().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD"));     //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO #LEDGER-EXT-2.PYMNT-SETTLMNT-DTE
            //*  RS0
            //*  RS0
            //*  RS0
            //*  RS0
            //*  RS0
            //*  RS0
            //*  RS0
            ldaFcpl801d.getPnd_Ledger_Ext_2_Pymnt_Acctg_Dte().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte(),new ReportEditMask("YYYYMMDD"));           //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-ACCTG-DTE ( EM = YYYYMMDD ) TO #LEDGER-EXT-2.PYMNT-ACCTG-DTE
            ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Locality_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Locality_Cde());                                            //Natural: ASSIGN #LEDGER-EXT-2.ANNT-LOCALITY-CDE := WF-PYMNT-ADDR-GRP.ANNT-LOCALITY-CDE
            FOR04:                                                                                                                                                        //Natural: FOR #I = 1 TO #GL-LGR-TOP
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Top())); pnd_Ws_Pnd_I.nadd(1))
            {
                ldaFcpl801c.getPnd_Ledger_Ext_Inv_Isa_Hash().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Isa_Hash_A().getValue(pnd_Ws_Pnd_I));                           //Natural: ASSIGN #LEDGER-EXT.INV-ISA-HASH := #GL-LGR-ISA-HASH-A ( #I )
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Isa_Hash().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Isa_Hash_A().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #LEDGER-EXT-2.INV-ISA-HASH := #GL-LGR-ISA-HASH-A ( #I )
                ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Nbr().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Nbr().getValue(pnd_Ws_Pnd_I));                            //Natural: ASSIGN #LEDGER-EXT.INV-ACCT-LEDGR-NBR := #GL-LGR-NBR ( #I )
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Nbr().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Nbr().getValue(pnd_Ws_Pnd_I));                          //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-NBR := #GL-LGR-NBR ( #I )
                ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Amt().getValue(pnd_Ws_Pnd_I));                            //Natural: ASSIGN #LEDGER-EXT.INV-ACCT-LEDGR-AMT := #GL-LGR-AMT ( #I )
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Amt().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Amt().getValue(pnd_Ws_Pnd_I));                          //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-AMT := #GL-LGR-AMT ( #I )
                ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Type().getValue(pnd_Ws_Pnd_I));                           //Natural: ASSIGN #LEDGER-EXT.INV-ACCT-LEDGR-IND := #GL-LGR-TYPE ( #I )
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Acct_Ledgr_Ind().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Type().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #LEDGER-EXT-2.INV-ACCT-LEDGR-IND := #GL-LGR-TYPE ( #I )
                ldaFcpl801c.getPnd_Ledger_Ext_Inv_Pymnt_Ded_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Ded_Cde().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN #LEDGER-EXT.INV-PYMNT-DED-CDE := #GL-LGR-DED-CDE ( #I )
                ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Pymnt_Ded_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lgr_Ded_Cde().getValue(pnd_Ws_Pnd_I));                       //Natural: ASSIGN #LEDGER-EXT-2.INV-PYMNT-DED-CDE := #GL-LGR-DED-CDE ( #I )
                //*  RS3
                ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Redraw_Ind().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde());                            //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE TO #LEDGER-EXT-2.CNTRCT-REDRAW-IND
                ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Rsdncy_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde());                                            //Natural: MOVE WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE TO #LEDGER-EXT-2.ANNT-RSDNCY-CDE
                //*  RS4
                if (condition(DbsUtil.maskMatches(pdaFcpa800.getWf_Pymnt_Addr_Grp_Annt_Rsdncy_Cde(),"AA")))                                                               //Natural: IF WF-PYMNT-ADDR-GRP.ANNT-RSDNCY-CDE = MASK ( AA )
                {
                    ldaFcpl801d.getPnd_Ledger_Ext_2_Annt_Rsdncy_Cde().setValue("FO");                                                                                     //Natural: MOVE 'FO' TO #LEDGER-EXT-2.ANNT-RSDNCY-CDE
                    pnd_Frgn_Pymnt_Cnt.nadd(1);                                                                                                                           //Natural: ADD 1 TO #FRGN-PYMNT-CNT
                }                                                                                                                                                         //Natural: END-IF
                //*  RS1 FROM FCPA800
                ldaFcpl801d.getPnd_Ledger_Ext_2_Cntrct_Option_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde());                                        //Natural: MOVE WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE TO #LEDGER-EXT-2.CNTRCT-OPTION-CDE
                //*  RS1
                ldaFcpl801d.getPnd_Ledger_Ext_2_Ia_Orgn_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde());                                                    //Natural: MOVE WF-PYMNT-ADDR-GRP.IA-ORGN-CDE TO #LEDGER-EXT-2.IA-ORGN-CDE
                //*  LEDGER.S469 WK09
                getWorkFiles().write(9, false, ldaFcpl801c.getPnd_Ledger_Ext());                                                                                          //Natural: WRITE WORK FILE 9 #LEDGER-EXT
                //*  CTS-0115 >>
                //*    WRITE WORK FILE 12 #LEDGER-EXT-2             /* LEDGER2 WK12 RS0
                getWorkFiles().write(12, false, ldaFcpl801d.getPnd_Ledger_Ext_2_Pnd_Ledger_Grp_2(), ldaFcpl801d.getPnd_Ledger_Ext_2_Inv_Ledgr(), ldaFcpl801d.getPnd_Ledger_Ext_2_Pnd_Misc_Fields()); //Natural: WRITE WORK FILE 12 #LEDGER-EXT-2.#LEDGER-GRP-2 #LEDGER-EXT-2.INV-LEDGR #LEDGER-EXT-2.#MISC-FIELDS
                //*  CTS-0115 <<
                pnd_Ws_Pnd_Actual_Ledgers.nadd(1);                                                                                                                        //Natural: ADD 1 TO #ACTUAL-LEDGERS
                //*  RS0
                pnd_Ws_Pnd_Recon_Ledgers.nadd(1);                                                                                                                         //Natural: ADD 1 TO #RECON-LEDGERS
                if (condition(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Ind().equals("C")))                                                                            //Natural: IF #LEDGER-EXT.INV-ACCT-LEDGR-IND = 'C'
                {
                    pnd_Ws_Pnd_Cr_Total.nadd(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                                                                         //Natural: ADD #LEDGER-EXT.INV-ACCT-LEDGR-AMT TO #CR-TOTAL
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    pnd_Ws_Pnd_Dr_Total.nadd(ldaFcpl801c.getPnd_Ledger_Ext_Inv_Acct_Ledgr_Amt());                                                                         //Natural: ADD #LEDGER-EXT.INV-ACCT-LEDGR-AMT TO #DR-TOTAL
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            //*  CTS-0115 >>
            //*  CREATING DATA FOR LEDGER-EXTRACT VSAM, THIS DATA WILL BE
            //*  USED FOR REVERSAL RECORDS RECEIVED IN FUTURE.
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_Cobol_Pda().reset();                                                                                                    //Natural: RESET #COBOL-PDA
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Unit_Value().getValue("*").setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Value().getValue("*"));                     //Natural: ASSIGN #GL-UNIT-VALUE ( * ) := INV-ACCT-UNIT-VALUE ( * )
            pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Unit_Qty().getValue("*").setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Unit_Qty().getValue("*"));                         //Natural: ASSIGN #GL-UNIT-QTY ( * ) := INV-ACCT-UNIT-QTY ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Settlmnt_Dte().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO #C-PYMNT-SETTLMNT-DTE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pmnt_Check_Dte().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte(),new ReportEditMask("YYYYMMDD"));    //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE ( EM = YYYYMMDD ) TO #C-PMNT-CHECK-DTE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cnt_Ppcn_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Ppcn_Nbr());                                           //Natural: ASSIGN #C-CNT-PPCN-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-PPCN-NBR
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Rdrw_Act_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde());                         //Natural: ASSIGN #C-CAN-RDRW-ACT-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Prcss_Seq_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Prcss_Seq_Nbr());                                //Natural: ASSIGN #C-PYMNT-PRCSS-SEQ-NBR := WF-PYMNT-ADDR-GRP.PYMNT-PRCSS-SEQ-NBR
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Annty_Ins_Type().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Ins_Type());                                          //Natural: ASSIGN #C-ANNTY-INS-TYPE := #GL-ANNTY-INS-TYPE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Annty_Type_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Annty_Type_Cde());                                          //Natural: ASSIGN #C-ANNTY-TYPE-CDE := #GL-ANNTY-TYPE-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ins_Option().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ins_Option());                                                  //Natural: ASSIGN #C-INS-OPTION := #GL-INS-OPTION
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Life_Cont().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Life_Cont());                                                    //Natural: ASSIGN #C-LIFE-CONT := #GL-LIFE-CONT
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Orgn_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Orgn_Cde());                                        //Natural: ASSIGN #C-CNTRCT-ORGN-CDE := #GL-CNTRCT-ORGN-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Type_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Cntrct_Type_Cde());                                        //Natural: ASSIGN #C-CNTRCT-TYPE-CDE := #GL-CNTRCT-TYPE-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Spouse_Pay_Stats().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Spouse_Pay_Stats());                          //Natural: ASSIGN #C-PYMNT-SPOUSE-PAY-STATS := #GL-PYMNT-SPOUSE-PAY-STATS
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pay_Type_Req_Ind().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pay_Type_Req_Ind());                                      //Natural: ASSIGN #C-PAY-TYPE-REQ-IND := #GL-PAY-TYPE-REQ-IND
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Payee_Tx_Elct_Trggr().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Payee_Tx_Elct_Trggr());                    //Natural: ASSIGN #C-PYMNT-PAYEE-TX-ELCT-TRGGR := #GL-PYMNT-PAYEE-TX-ELCT-TRGGR
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rsdncy_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rsdncy_Cde());                                                  //Natural: ASSIGN #C-RSDNCY-CDE := #GL-RSDNCY-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Locality_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Locality_Cde());                                              //Natural: ASSIGN #C-LOCALITY-CDE := #GL-LOCALITY-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Lob_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lob_Cde());                                                        //Natural: ASSIGN #C-LOB-CDE := #GL-LOB-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pend_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Cde());                                                      //Natural: ASSIGN #C-PEND-CDE := #GL-PEND-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Csr().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Csr());                                                                //Natural: ASSIGN #C-CSR := #GL-CSR
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Payee_Code().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Payee_Code());                                                  //Natural: ASSIGN #C-PAYEE-CODE := #GL-PAYEE-CODE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Option_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Option_Cde());                                                  //Natural: ASSIGN #C-OPTION-CDE := #GL-OPTION-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Roll_Dest_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Roll_Dest_Cde());                                            //Natural: ASSIGN #C-ROLL-DEST-CDE := #GL-ROLL-DEST-CDE
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dc_Pended().equals(true)))                                                                                      //Natural: IF #GL-DC-PENDED = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dc_Pended().setValue("Y");                                                                                        //Natural: ASSIGN #C-DC-PENDED := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dc_Pended().equals(false)))                                                                                 //Natural: IF #GL-DC-PENDED = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dc_Pended().setValue("N");                                                                                    //Natural: ASSIGN #C-DC-PENDED := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Internal_Rollover().equals(true)))                                                                              //Natural: IF #GL-INTERNAL-ROLLOVER = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Internal_Rollover().setValue("Y");                                                                                //Natural: ASSIGN #C-INTERNAL-ROLLOVER := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Internal_Rollover().equals(false)))                                                                         //Natural: IF #GL-INTERNAL-ROLLOVER = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Internal_Rollover().setValue("N");                                                                            //Natural: ASSIGN #C-INTERNAL-ROLLOVER := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb().equals(true)))                                                                                            //Natural: IF #GL-RTB = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rtb().setValue("Y");                                                                                              //Natural: ASSIGN #C-RTB := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb().equals(false)))                                                                                       //Natural: IF #GL-RTB = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rtb().setValue("N");                                                                                          //Natural: ASSIGN #C-RTB := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb_Rollover().equals(true)))                                                                                   //Natural: IF #GL-RTB-ROLLOVER = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rtb_Rollover().setValue("Y");                                                                                     //Natural: ASSIGN #C-RTB-ROLLOVER := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Rtb_Rollover().equals(false)))                                                                              //Natural: IF #GL-RTB-ROLLOVER = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Rtb_Rollover().setValue("N");                                                                                 //Natural: ASSIGN #C-RTB-ROLLOVER := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ss_Rollover().equals(true)))                                                                                    //Natural: IF #GL-SS-ROLLOVER = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ss_Rollover().setValue("Y");                                                                                      //Natural: ASSIGN #C-SS-ROLLOVER := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ss_Rollover().equals(false)))                                                                               //Natural: IF #GL-SS-ROLLOVER = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ss_Rollover().setValue("N");                                                                                  //Natural: ASSIGN #C-SS-ROLLOVER := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Rollover().equals(true)))                                                                               //Natural: IF #GL-CLASSIC-ROLLOVER = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Rollover().setValue("Y");                                                                                 //Natural: ASSIGN #C-CLASSIC-ROLLOVER := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Rollover().equals(false)))                                                                          //Natural: IF #GL-CLASSIC-ROLLOVER = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Rollover().setValue("N");                                                                             //Natural: ASSIGN #C-CLASSIC-ROLLOVER := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_To_Roth().equals(true)))                                                                                //Natural: IF #GL-CLASSIC-TO-ROTH = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_To_Roth().setValue("Y");                                                                                  //Natural: ASSIGN #C-CLASSIC-TO-ROTH := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_To_Roth().equals(false)))                                                                           //Natural: IF #GL-CLASSIC-TO-ROTH = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_To_Roth().setValue("N");                                                                              //Natural: ASSIGN #C-CLASSIC-TO-ROTH := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Withdraw().equals(true)))                                                                               //Natural: IF #GL-CLASSIC-WITHDRAW = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Withdraw().setValue("Y");                                                                                 //Natural: ASSIGN #C-CLASSIC-WITHDRAW := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Withdraw().equals(false)))                                                                          //Natural: IF #GL-CLASSIC-WITHDRAW = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Withdraw().setValue("N");                                                                             //Natural: ASSIGN #C-CLASSIC-WITHDRAW := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Mdo_Withdraw().equals(true)))                                                                           //Natural: IF #GL-CLASSIC-MDO-WITHDRAW = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Mdo_Withdraw().setValue("Y");                                                                             //Natural: ASSIGN #C-CLASSIC-MDO-WITHDRAW := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Classic_Mdo_Withdraw().equals(false)))                                                                      //Natural: IF #GL-CLASSIC-MDO-WITHDRAW = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Classic_Mdo_Withdraw().setValue("N");                                                                         //Natural: ASSIGN #C-CLASSIC-MDO-WITHDRAW := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Roth_Withdraw().equals(true)))                                                                                  //Natural: IF #GL-ROTH-WITHDRAW = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Roth_Withdraw().setValue("Y");                                                                                    //Natural: ASSIGN #C-ROTH-WITHDRAW := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Roth_Withdraw().equals(false)))                                                                             //Natural: IF #GL-ROTH-WITHDRAW = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Roth_Withdraw().setValue("N");                                                                                //Natural: ASSIGN #C-ROTH-WITHDRAW := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ivc_From_Rollover().equals(true)))                                                                              //Natural: IF #GL-IVC-FROM-ROLLOVER = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ivc_From_Rollover().setValue("Y");                                                                                //Natural: ASSIGN #C-IVC-FROM-ROLLOVER := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ivc_From_Rollover().equals(false)))                                                                         //Natural: IF #GL-IVC-FROM-ROLLOVER = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ivc_From_Rollover().setValue("N");                                                                            //Natural: ASSIGN #C-IVC-FROM-ROLLOVER := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_Dc().equals(true)))                                                                                   //Natural: IF #GL-PA-SELECT-DC = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pa_Select_Dc().setValue("Y");                                                                                     //Natural: ASSIGN #C-PA-SELECT-DC := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_Dc().equals(false)))                                                                              //Natural: IF #GL-PA-SELECT-DC = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pa_Select_Dc().setValue("N");                                                                                 //Natural: ASSIGN #C-PA-SELECT-DC := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_M().equals(true)))                                                                                    //Natural: IF #GL-PA-SELECT-M = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pa_Select_M().setValue("Y");                                                                                      //Natural: ASSIGN #C-PA-SELECT-M := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pa_Select_M().equals(false)))                                                                               //Natural: IF #GL-PA-SELECT-M = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pa_Select_M().setValue("N");                                                                                  //Natural: ASSIGN #C-PA-SELECT-M := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_M().equals(true)))                                                                                         //Natural: IF #GL-SPIA-M = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Spia_M().setValue("Y");                                                                                           //Natural: ASSIGN #C-SPIA-M := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_M().equals(false)))                                                                                    //Natural: IF #GL-SPIA-M = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Spia_M().setValue("N");                                                                                       //Natural: ASSIGN #C-SPIA-M := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_Dc().equals(true)))                                                                                        //Natural: IF #GL-SPIA-DC = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Spia_Dc().setValue("Y");                                                                                          //Natural: ASSIGN #C-SPIA-DC := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Spia_Dc().equals(false)))                                                                                   //Natural: IF #GL-SPIA-DC = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Spia_Dc().setValue("N");                                                                                      //Natural: ASSIGN #C-SPIA-DC := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Sci().equals(true)))                                                                                            //Natural: IF #GL-SCI = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Sci().setValue("Y");                                                                                              //Natural: ASSIGN #C-SCI := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Sci().equals(false)))                                                                                       //Natural: IF #GL-SCI = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Sci().setValue("N");                                                                                          //Natural: ASSIGN #C-SCI := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Scni().equals(true)))                                                                                           //Natural: IF #GL-SCNI = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Scni().setValue("Y");                                                                                             //Natural: ASSIGN #C-SCNI := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Scni().equals(false)))                                                                                      //Natural: IF #GL-SCNI = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Scni().setValue("N");                                                                                         //Natural: ASSIGN #C-SCNI := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lump_Sum().equals(true)))                                                                                       //Natural: IF #GL-LUMP-SUM = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Lump_Sum().setValue("Y");                                                                                         //Natural: ASSIGN #C-LUMP-SUM := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Lump_Sum().equals(false)))                                                                                  //Natural: IF #GL-LUMP-SUM = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Lump_Sum().setValue("N");                                                                                     //Natural: ASSIGN #C-LUMP-SUM := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Stable_Return().equals(true)))                                                                                  //Natural: IF #GL-STABLE-RETURN = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Stable_Return().setValue("Y");                                                                                    //Natural: ASSIGN #C-STABLE-RETURN := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Stable_Return().equals(false)))                                                                             //Natural: IF #GL-STABLE-RETURN = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Stable_Return().setValue("N");                                                                                //Natural: ASSIGN #C-STABLE-RETURN := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty().equals(true)))                                                                           //Natural: IF #GL-IVA-INVEST-VAR-ANNTY = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iva_Invest_Var_Annty().setValue("Y");                                                                             //Natural: ASSIGN #C-IVA-INVEST-VAR-ANNTY := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty().equals(false)))                                                                      //Natural: IF #GL-IVA-INVEST-VAR-ANNTY = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iva_Invest_Var_Annty().setValue("N");                                                                         //Natural: ASSIGN #C-IVA-INVEST-VAR-ANNTY := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty_Ac().equals(true)))                                                                        //Natural: IF #GL-IVA-INVEST-VAR-ANNTY-AC = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iva_Invest_Var_Annty_Ac().setValue("Y");                                                                          //Natural: ASSIGN #C-IVA-INVEST-VAR-ANNTY-AC := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iva_Invest_Var_Annty_Ac().equals(false)))                                                                   //Natural: IF #GL-IVA-INVEST-VAR-ANNTY-AC = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iva_Invest_Var_Annty_Ac().setValue("N");                                                                      //Natural: ASSIGN #C-IVA-INVEST-VAR-ANNTY-AC := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty().equals(true)))                                                                       //Natural: IF #GL-IHA-INVEST-HORIZON-ANNTY = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iha_Invest_Horizon_Annty().setValue("Y");                                                                         //Natural: ASSIGN #C-IHA-INVEST-HORIZON-ANNTY := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty().equals(false)))                                                                  //Natural: IF #GL-IHA-INVEST-HORIZON-ANNTY = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iha_Invest_Horizon_Annty().setValue("N");                                                                     //Natural: ASSIGN #C-IHA-INVEST-HORIZON-ANNTY := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty_Ac().equals(true)))                                                                    //Natural: IF #GL-IHA-INVEST-HORIZON-ANNTY-AC = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iha_Invest_Horizon_Annty_Ac().setValue("Y");                                                                      //Natural: ASSIGN #C-IHA-INVEST-HORIZON-ANNTY-AC := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Iha_Invest_Horizon_Annty_Ac().equals(false)))                                                               //Natural: IF #GL-IHA-INVEST-HORIZON-ANNTY-AC = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Iha_Invest_Horizon_Annty_Ac().setValue("N");                                                                  //Natural: ASSIGN #C-IHA-INVEST-HORIZON-ANNTY-AC := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty().equals(true)))                                                                            //Natural: IF #GL-TC-LIFETIME-5-ANNTY = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tc_Lifetime_5_Annty().setValue("Y");                                                                              //Natural: ASSIGN #C-TC-LIFETIME-5-ANNTY := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty().equals(false)))                                                                       //Natural: IF #GL-TC-LIFETIME-5-ANNTY = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tc_Lifetime_5_Annty().setValue("N");                                                                          //Natural: ASSIGN #C-TC-LIFETIME-5-ANNTY := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty_Ac().equals(true)))                                                                         //Natural: IF #GL-TC-LIFETIME-5-ANNTY-AC = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tc_Lifetime_5_Annty_Ac().setValue("Y");                                                                           //Natural: ASSIGN #C-TC-LIFETIME-5-ANNTY-AC := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tc_Lifetime_5_Annty_Ac().equals(false)))                                                                    //Natural: IF #GL-TC-LIFETIME-5-ANNTY-AC = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tc_Lifetime_5_Annty_Ac().setValue("N");                                                                       //Natural: ASSIGN #C-TC-LIFETIME-5-ANNTY-AC := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Lgrs().equals(true)))                                                                                      //Natural: IF #GL-PEND-LGRS = TRUE
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pend_Lgrs().setValue("Y");                                                                                        //Natural: ASSIGN #C-PEND-LGRS := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pend_Lgrs().equals(false)))                                                                                 //Natural: IF #GL-PEND-LGRS = FALSE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pend_Lgrs().setValue("N");                                                                                    //Natural: ASSIGN #C-PEND-LGRS := 'N'
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dc_Past_Cv().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dc_Past_Cv());                                                  //Natural: ASSIGN #C-DC-PAST-CV := #GL-DC-PAST-CV
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ia_Origin().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ia_Origin());                                                    //Natural: ASSIGN #C-IA-ORIGIN := #GL-IA-ORIGIN
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Top().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Top());                                                                //Natural: ASSIGN #C-TOP := #GL-TOP
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Company_Cde().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Company_Cde().getValue("*"));                    //Natural: ASSIGN #C-COMPANY-CDE ( * ) := #GL-COMPANY-CDE ( * )
            FOR05:                                                                                                                                                        //Natural: FOR #I = 1 TO 40
            for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(40)); pnd_Ws_Pnd_I.nadd(1))
            {
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).equals(true)))                                                   //Natural: IF #GL-MONTHLY-VALUATION ( #I ) = TRUE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).setValue("Y");                                                     //Natural: ASSIGN #C-MONTHLY-VALUATION ( #I ) := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).equals(false)))                                              //Natural: IF #GL-MONTHLY-VALUATION ( #I ) = FALSE
                    {
                        pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Monthly_Valuation().getValue(pnd_Ws_Pnd_I).setValue("N");                                                 //Natural: ASSIGN #C-MONTHLY-VALUATION ( #I ) := 'N'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
                if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa_Access().getValue(pnd_Ws_Pnd_I).equals(true)))                                                         //Natural: IF #GL-TIAA-ACCESS ( #I ) = TRUE
                {
                    pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tiaa_Access().getValue(pnd_Ws_Pnd_I).setValue("Y");                                                           //Natural: ASSIGN #C-TIAA-ACCESS ( #I ) := 'Y'
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa_Access().getValue(pnd_Ws_Pnd_I).equals(false)))                                                    //Natural: IF #GL-TIAA-ACCESS ( #I ) = FALSE
                    {
                        pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tiaa_Access().getValue(pnd_Ws_Pnd_I).setValue("N");                                                       //Natural: ASSIGN #C-TIAA-ACCESS ( #I ) := 'N'
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Fund().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fund().getValue("*"));                                  //Natural: ASSIGN #C-FUND ( * ) := #GL-FUND ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Settl_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Settl_Amt().getValue("*"));                        //Natural: ASSIGN #C-SETTL-AMT ( * ) := #GL-SETTL-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dpi_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dpi_Amt().getValue("*"));                            //Natural: ASSIGN #C-DPI-AMT ( * ) := #GL-DPI-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Dci_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Dci_Amt().getValue("*"));                            //Natural: ASSIGN #C-DCI-AMT ( * ) := #GL-DCI-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Exp_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Exp_Amt().getValue("*"));                            //Natural: ASSIGN #C-EXP-AMT ( * ) := #GL-EXP-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Can_Tax_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Can_Tax_Amt().getValue("*"));                    //Natural: ASSIGN #C-CAN-TAX-AMT ( * ) := #GL-CAN-TAX-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Fdrl_Tax_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Fdrl_Tax_Amt().getValue("*"));                  //Natural: ASSIGN #C-FDRL-TAX-AMT ( * ) := #GL-FDRL-TAX-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_State_Tax_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_State_Tax_Amt().getValue("*"));                //Natural: ASSIGN #C-STATE-TAX-AMT ( * ) := #GL-STATE-TAX-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Local_Tax_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Local_Tax_Amt().getValue("*"));                //Natural: ASSIGN #C-LOCAL-TAX-AMT ( * ) := #GL-LOCAL-TAX-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Net_Pymnt_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Net_Pymnt_Amt().getValue("*"));                //Natural: ASSIGN #C-NET-PYMNT-AMT ( * ) := #GL-NET-PYMNT-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Tiaa().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Tiaa().getValue("*"));                                  //Natural: ASSIGN #C-TIAA ( * ) := #GL-TIAA ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Inv_Acct_Ded_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Inv_Acct_Ded_Amt().getValue("*"));          //Natural: ASSIGN #C-INV-ACCT-DED-AMT ( * ) := #GL-INV-ACCT-DED-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ivc_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ivc_Amt().getValue("*"));                            //Natural: ASSIGN #C-IVC-AMT ( * ) := #GL-IVC-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Unit_Value().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Unit_Value().getValue("*"));                      //Natural: ASSIGN #C-UNIT-VALUE ( * ) := #GL-UNIT-VALUE ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Unit_Qty().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Unit_Qty().getValue("*"));                          //Natural: ASSIGN #C-UNIT-QTY ( * ) := #GL-UNIT-QTY ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ded_Top().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Ded_Top());                                                        //Natural: ASSIGN #C-DED-TOP := #GL-DED-TOP
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Ded_Cde().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Ded_Cde().getValue("*"));                //Natural: ASSIGN #C-PYMNT-DED-CDE ( * ) := #GL-PYMNT-DED-CDE ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Ded_Amt().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Ded_Amt().getValue("*"));                //Natural: ASSIGN #C-PYMNT-DED-AMT ( * ) := #GL-PYMNT-DED-AMT ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Ded_Payee_Cde().getValue("*").setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Pymnt_Ded_Payee_Cde().getValue("*"));    //Natural: ASSIGN #C-PYMNT-DED-PAYEE-CDE ( * ) := #GL-PYMNT-DED-PAYEE-CDE ( * )
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cnt_Mode_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Mode_Cde());                                           //Natural: ASSIGN #C-CNT-MODE-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-MODE-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Acctg_Dte().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Acctg_Dte(),new ReportEditMask("YYYYMMDD"));   //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-ACCTG-DTE ( EM = YYYYMMDD ) TO #C-PYMNT-ACCTG-DTE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Check_Crrncy_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Check_Crrncy_Cde());                        //Natural: ASSIGN #C-CNTRCT-CHECK-CRRNCY-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-CHECK-CRRNCY-CDE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ph_Last_Name().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Last_Name());                                              //Natural: ASSIGN #C-PH-LAST-NAME := WF-PYMNT-ADDR-GRP.PH-LAST-NAME
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ph_First_Name().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_First_Name());                                            //Natural: ASSIGN #C-PH-FIRST-NAME := WF-PYMNT-ADDR-GRP.PH-FIRST-NAME
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Ph_Middle_Name().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Ph_Middle_Name());                                          //Natural: ASSIGN #C-PH-MIDDLE-NAME := WF-PYMNT-ADDR-GRP.PH-MIDDLE-NAME
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Cref_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cref_Nbr());                                        //Natural: ASSIGN #C-CNTRCT-CREF-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-CREF-NBR
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Unq_Id_Nbr().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Unq_Id_Nbr());                                    //Natural: ASSIGN #C-CNTRCT-UNQ-ID-NBR := WF-PYMNT-ADDR-GRP.CNTRCT-UNQ-ID-NBR
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Corp_Wpid().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Corp_Wpid());                                        //Natural: ASSIGN #C-PYMNT-CORP-WPID := WF-PYMNT-ADDR-GRP.PYMNT-CORP-WPID
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Cycle_Dte().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Cycle_Dte(),new ReportEditMask("YYYYMMDD"));   //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-CYCLE-DTE ( EM = YYYYMMDD ) TO #C-PYMNT-CYCLE-DTE
            pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Pymnt_Intrfce_Dte().setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Intrfce_Dte(),new ReportEditMask("YYYYMMDD")); //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-INTRFCE-DTE ( EM = YYYYMMDD ) TO #C-PYMNT-INTRFCE-DTE
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Cancel_Rdrw_Actvty_Cde().notEquals("  ")))                                                               //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-CANCEL-RDRW-ACTVTY-CDE NE '  '
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Cancel_Rdrw_Ind().setValue("Y");                                                                           //Natural: ASSIGN #C-CNTRCT-CANCEL-RDRW-IND := 'Y'
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_C_Cntrct_Cancel_Rdrw_Ind().setValue(" ");                                                                           //Natural: ASSIGN #C-CNTRCT-CANCEL-RDRW-IND := ' '
            }                                                                                                                                                             //Natural: END-IF
            getWorkFiles().write(17, false, pdaFcpa142r.getPnd_Cobol_Pda_Data_Pnd_Cobol_Pda());                                                                           //Natural: WRITE WORK FILE 17 #COBOL-PDA
            //*   CALL 'TIAALEDW' #COBOL-PDA
            //*  CTS-0115 <<
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (condition(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error().getSubstring(1,1).equals("L")))                                                                         //Natural: IF SUBSTR ( #GL-ERROR,1,1 ) = 'L'
            {
                ldaFcpl801a.getWf_Error_Msg().setValue("(");                                                                                                              //Natural: MOVE '(' TO WF-ERROR-MSG
                setValueToSubstring(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Program(),ldaFcpl801a.getWf_Error_Msg(),2,8);                                                         //Natural: MOVE #GL-PROGRAM TO SUBSTR ( WF-ERROR-MSG,2,8 )
                setValueToSubstring(")",ldaFcpl801a.getWf_Error_Msg(),10,1);                                                                                              //Natural: MOVE ')' TO SUBSTR ( WF-ERROR-MSG,10,1 )
                pdaFcpa142l.getPnd_Fcpa142l_Pnd_Error_Cde().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error());                                                            //Natural: ASSIGN #FCPA142L.#ERROR-CDE := #GL-ERROR
                pdaFcpa142l.getPnd_Fcpa142l_Pnd_Error_Field().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error_Field());                                                    //Natural: ASSIGN #FCPA142L.#ERROR-FIELD := #GL-ERROR-FIELD
                DbsUtil.callnat(Fcpn142l.class , getCurrentProcessState(), pdaFcpa142l.getPnd_Fcpa142l());                                                                //Natural: CALLNAT 'FCPN142L' USING #FCPA142L
                if (condition(Global.isEscape())) return;
                setValueToSubstring(pdaFcpa142l.getPnd_Fcpa142l_Pnd_Error_Msg(),ldaFcpl801a.getWf_Error_Msg(),11,50);                                                     //Natural: MOVE #FCPA142L.#ERROR-MSG TO SUBSTR ( WF-ERROR-MSG,11,50 )
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                ldaFcpl801a.getWf_Error_Msg().setValue(pdaFcpa143.getPnd_Gl_Pda_Pnd_Gl_Error_Msg());                                                                      //Natural: MOVE #GL-ERROR-MSG TO WF-ERROR-MSG
            }                                                                                                                                                             //Natural: END-IF
            pnd_Ws_Pnd_Ledger_Error.nadd(1);                                                                                                                              //Natural: ADD 1 TO #LEDGER-ERROR
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Ret_Code().setValue("0003");                                                                                              //Natural: MOVE '0003' TO WF-PYMNT-ADDR-GRP.GTN-RET-CODE
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Gtn_Rpt_Code().getValue(1).setValue(16);                                                                                      //Natural: MOVE 16 TO WF-PYMNT-ADDR-GRP.GTN-RPT-CODE ( 1 )
                                                                                                                                                                          //Natural: PERFORM WRITE-ERROR-FILE
            sub_Write_Error_File();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
    }
    //*  (LB) MUTUAL FUNDS CTL TOTALS
    private void sub_Mut_Fnds_Cntl_Totals() throws Exception                                                                                                              //Natural: MUT-FNDS-CNTL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        FOR06:                                                                                                                                                            //Natural: FOR #M 1 10
        for (pnd_Ws_Pnd_M.setValue(1); condition(pnd_Ws_Pnd_M.lessOrEqual(10)); pnd_Ws_Pnd_M.nadd(1))
        {
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_M).equals(getZero())))                                                      //Natural: IF WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( #M ) EQ 0
            {
                if (condition(true)) break;                                                                                                                               //Natural: ESCAPE BOTTOM
            }                                                                                                                                                             //Natural: END-IF
            //*  PERSONAL ANNUITY  (IA CODE 750)
            short decideConditionsMet2402 = 0;                                                                                                                            //Natural: DECIDE ON FIRST WF-PYMNT-ADDR-GRP.PYMNT-DED-CDE ( #M );//Natural: VALUE 007
            if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_M).equals(7))))
            {
                decideConditionsMet2402++;
                //*  MUTUAL FUNDS (IA CODE 600)
                pnd_Ws_Pnd_Idx.setValue(26);                                                                                                                              //Natural: MOVE 26 TO #IDX
            }                                                                                                                                                             //Natural: VALUE 008
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_M).equals(8))))
            {
                decideConditionsMet2402++;
                //*  PA-SELECT (IA CODE 700)
                pnd_Ws_Pnd_Idx.setValue(23);                                                                                                                              //Natural: MOVE 23 TO #IDX
            }                                                                                                                                                             //Natural: VALUE 009
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_M).equals(9))))
            {
                decideConditionsMet2402++;
                //*  UNIVERSAL LIFE (IA CODE 800)
                pnd_Ws_Pnd_Idx.setValue(24);                                                                                                                              //Natural: MOVE 24 TO #IDX
            }                                                                                                                                                             //Natural: VALUE 010
            else if (condition((pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Cde().getValue(pnd_Ws_Pnd_M).equals(10))))
            {
                decideConditionsMet2402++;
                pnd_Ws_Pnd_Idx.setValue(25);                                                                                                                              //Natural: MOVE 25 TO #IDX
            }                                                                                                                                                             //Natural: ANY
            if (condition(decideConditionsMet2402 > 0))
            {
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_Idx).nadd(+1);                                                                              //Natural: ADD +1 TO #FCPAACUM.#REC-CNT ( #IDX )
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_Idx).nadd(+1);                                                                            //Natural: ADD +1 TO #FCPAACUM.#PYMNT-CNT ( #IDX )
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_Idx).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Ded_Amt().getValue(pnd_Ws_Pnd_M));        //Natural: ADD WF-PYMNT-ADDR-GRP.PYMNT-DED-AMT ( #M ) TO #FCPAACUM.#SETTL-AMT ( #IDX )
                pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_Idx).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_Idx));  //Natural: MOVE #FCPAACUM.#SETTL-AMT ( #IDX ) TO #FCPAACUM.#NET-PYMNT-AMT ( #IDX )
            }                                                                                                                                                             //Natural: NONE
            else if (condition())
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
    }
    private void sub_Write_Eft_File() throws Exception                                                                                                                    //Natural: WRITE-EFT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        //*                                                 /* EFT.S466
        //*  CTS-0115 >>
        //*  WRITE WORK FILE 5 VARIABLE PYMNT-ADDR-INFO INV-INFO(1:INV-ACCT-COUNT)
        getWorkFiles().write(5, false, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",40),                  //Natural: WRITE WORK FILE 5 PYMNT-ADDR-INFO INV-INFO ( 1:40 ) CANADIAN-TAX-AMT-ALPHA WF-PYMNT-ADDR-GRP.IA-ORGN-CDE PLAN-NUMBER SUB-PLAN ORIG-CNTRCT-NBR ORIG-SUB-PLAN #GTN-W9-BEN-IND #GTN-FED-TAX-TYPE ( * ) #GTN-FED-TAX-AMT ( * ) #GTN-FED-CANADIAN-AMT ( * ) #GTN-ELEC-CDE #GTN-CNTRCT-MONEY-SOURCE #GTN-HARDSHIP-CDE
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Canadian_Tax_Amt_Alpha(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Ia_Orgn_Cde(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan(), 
            pnd_Gtn_W9_Ben_Ind, pnd_Gtn_Fed_Tax_Type.getValue("*"), pnd_Gtn_Fed_Tax_Amt.getValue("*"), pnd_Gtn_Fed_Canadian_Amt.getValue("*"), pnd_Gtn_Elec_Cde, 
            pnd_Gtn_Cntrct_Money_Source, pnd_Gtn_Hardship_Cde);
        //*  CTS-0115 <<
    }
    private void sub_Write_Report_File() throws Exception                                                                                                                 //Natural: WRITE-REPORT-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        proline_Dob.reset();                                                                                                                                              //Natural: RESET PROLINE-DOB PROLINE-SETTLE-DATE PROLINE-NRA-TAX-AMT PROLINE-FED-CAN-AMT
        proline_Settle_Date.reset();
        proline_Nra_Tax_Amt.reset();
        proline_Fed_Can_Amt.reset();
        proline_Dob.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Dob(),new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED PYMNT-DOB ( EM = YYYYMMDD ) TO PROLINE-DOB
        proline_Settle_Date.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO PROLINE-SETTLE-DATE
        //*  F.ALLIE 10/11/11 S
        proline_Fed_Can_Amt.nadd(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Canadian_Amt().getValue("*"));                                                                    //Natural: ADD GTN-FED-CANADIAN-AMT ( * ) TO PROLINE-FED-CAN-AMT
        if (condition(! (pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Nra_Rate().getValue(1).equals(getZero()))))                                                               //Natural: IF NOT GTN-FED-NRA-RATE ( 1 ) = 0
        {
            proline_Nra_Tax_Amt.nadd(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Tax_Amt().getValue(1));                                                                       //Natural: ADD GTN-FED-TAX-AMT ( 1 ) TO PROLINE-NRA-TAX-AMT
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(! (pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Nra_Rate().getValue(2).equals(getZero()))))                                                               //Natural: IF NOT GTN-FED-NRA-RATE ( 2 ) = 0
        {
            proline_Nra_Tax_Amt.nadd(pdaFcpa802.getWf_Pymnt_Tax_Grp_Gtn_Fed_Tax_Amt().getValue(2));                                                                       //Natural: ADD GTN-FED-TAX-AMT ( 2 ) TO PROLINE-NRA-TAX-AMT
            //*  F.ALLIE 10/11/11  E
        }                                                                                                                                                                 //Natural: END-IF
        //*                                                 /* PENDS.S487
        //*  F.ALLIE  10/11/2011
        //*  F.ALLIE  10/11/2011
        //*  RS6 FROM FCPA800
        //*  RS6
        //*  RS6
        //*  RS6
        getWorkFiles().write(4, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()),  //Natural: WRITE WORK FILE 4 VARIABLE PYMNT-ADDR-INFO INV-INFO ( 1:INV-ACCT-COUNT ) PROLINE-DOB PROLINE-SETTLE-DATE PROLINE-NRA-TAX-AMT PROLINE-FED-CAN-AMT WF-PYMNT-ADDR-GRP.PLAN-NUMBER WF-PYMNT-ADDR-GRP.SUB-PLAN WF-PYMNT-ADDR-GRP.ORIG-SUB-PLAN WF-PYMNT-ADDR-GRP.ORIG-CNTRCT-NBR
            proline_Dob, proline_Settle_Date, proline_Nra_Tax_Amt, proline_Fed_Can_Amt, pdaFcpa800.getWf_Pymnt_Addr_Grp_Plan_Number(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Sub_Plan(), 
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Sub_Plan(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Orig_Cntrct_Nbr());
    }
    private void sub_Write_Error_File() throws Exception                                                                                                                  //Natural: WRITE-ERROR-FILE
    {
        if (BLNatReinput.isReinput()) return;

        //* *********************************
        proline_Dob.reset();                                                                                                                                              //Natural: RESET PROLINE-DOB PROLINE-SETTLE-DATE
        proline_Settle_Date.reset();
        proline_Dob.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Dob(),new ReportEditMask("YYYYMMDD"));                                                           //Natural: MOVE EDITED PYMNT-DOB ( EM = YYYYMMDD ) TO PROLINE-DOB
        proline_Settle_Date.setValueEdited(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Settlmnt_Dte(),new ReportEditMask("YYYYMMDD"));                                          //Natural: MOVE EDITED WF-PYMNT-ADDR-GRP.PYMNT-SETTLMNT-DTE ( EM = YYYYMMDD ) TO PROLINE-SETTLE-DATE
        //*  REJECT.ERRORS.S464
        //*  ABRM
        //*  ABRM
        //*  JWO  20110413
        //*  JWO  20110413
        getWorkFiles().write(2, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), ldaFcpl801a.getWf_Error_Msg(), pdaFcpa802.getWf_Pymnt_Tax_Grp_Wf_Pymnt_Tax_Rec(),  //Natural: WRITE WORK FILE 2 VARIABLE PYMNT-ADDR-INFO WF-ERROR-MSG WF-PYMNT-TAX-REC INV-INFO ( 1:INV-ACCT-COUNT ) PROLINE-DOB PROLINE-SETTLE-DATE
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()), proline_Dob, proline_Settle_Date);
    }
    private void sub_Write_Process_Stmnt_Fields() throws Exception                                                                                                        //Natural: WRITE-PROCESS-STMNT-FIELDS
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************************
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde().equals(" ")))                                                                                     //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE = ' '
        {
            pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde().setValue("0000");                                                                                      //Natural: ASSIGN #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE := '0000'
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Hold_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Hold_Cde());                                           //Natural: ASSIGN #CHECK-SORT-FIELDS.CNTRCT-HOLD-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-HOLD-CDE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpa801c.getPnd_Check_Sort_Fields_Cntrct_Company_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Company().getValue(1));                               //Natural: ASSIGN #CHECK-SORT-FIELDS.CNTRCT-COMPANY-CDE := WF-PYMNT-ADDR-GRP.INV-ACCT-COMPANY ( 1 )
    }
    private void sub_College_Dividends() throws Exception                                                                                                                 //Natural: COLLEGE-DIVIDENDS
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************
        if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).notEquals(new                 //Natural: IF INV-ACCT-DVDND-AMT ( 1:INV-ACCT-COUNT ) NE 0.00
            DbsDecimal("0.00"))))
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().setValue(5);                                                                                                    //Natural: ASSIGN #ACCUM-OCCUR := 5
                                                                                                                                                                          //Natural: PERFORM ACCUM-CONTROL-TOTALS
            sub_Accum_Control_Totals();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            //*                                                /* COLLEGE.DVDND.S467
            getWorkFiles().write(7, true, pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Info(), pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Info().getValue(1,":",                   //Natural: WRITE WORK FILE 7 VARIABLE PYMNT-ADDR-INFO INV-INFO ( 1:INV-ACCT-COUNT )
                pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()));
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1,":",pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count()).reset();                                //Natural: RESET INV-ACCT-DVDND-AMT ( 1:INV-ACCT-COUNT )
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Determine_Interface_Dte() throws Exception                                                                                                           //Natural: DETERMINE-INTERFACE-DTE
    {
        if (BLNatReinput.isReinput()) return;

        //* ****************************************
        pnd_Ws_Pnd_Intrfce_Dte.compute(new ComputeParameters(false, pnd_Ws_Pnd_Intrfce_Dte), Global.getDATX().add(5));                                                    //Natural: ADD *DATX 5 GIVING #INTRFCE-DTE
        pnd_Ws_Pnd_Work_Date.setValueEdited(pnd_Ws_Pnd_Intrfce_Dte,new ReportEditMask("YYYYMMDD"));                                                                       //Natural: MOVE EDITED #INTRFCE-DTE ( EM = YYYYMMDD ) TO #WORK-DATE
        pdaFcpa115.getFcpa115_For_Date().setValue(pnd_Ws_Pnd_Work_Date_Num);                                                                                              //Natural: MOVE #WORK-DATE-NUM TO FOR-DATE
        DbsUtil.callnat(Fcpn119.class , getCurrentProcessState(), pdaFcpa115.getFcpa115());                                                                               //Natural: CALLNAT 'FCPN119' USING FCPA115
        if (condition(Global.isEscape())) return;
        if (condition(pdaFcpa115.getFcpa115_Error_Return_Code().notEquals(getZero())))                                                                                    //Natural: IF FCPA115.ERROR-RETURN-CODE NE 0
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(15, "***",new TabSetting(25),"ERROR CALLING EXTERALIZATION TABLE",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"PROGRAM...:",pdaFcpa115.getFcpa115_Error_Program(),new  //Natural: WRITE ( 15 ) '***' 25T 'ERROR CALLING EXTERALIZATION TABLE' 77T '***' / '***' 25T 'PROGRAM...:' FCPA115.ERROR-PROGRAM 77T '***' / '***' 25T 'DATE......:' FOR-DATE ( EM = 9999/99/99 ) 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"DATE......:",pdaFcpa115.getFcpa115_For_Date(), new ReportEditMask ("9999/99/99"),new 
                TabSetting(77),"***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            DbsUtil.terminate(51);  if (true) return;                                                                                                                     //Natural: TERMINATE 51
        }                                                                                                                                                                 //Natural: END-IF
        pnd_Ws_Pnd_Work_Date_Num.setValue(pdaFcpa115.getFcpa115_Interface_Date());                                                                                        //Natural: MOVE FCPA115.INTERFACE-DATE TO #WORK-DATE-NUM
        pnd_Ws_Pnd_Intrfce_Dte.setValueEdited(new ReportEditMask("YYYYMMDD"),pnd_Ws_Pnd_Work_Date);                                                                       //Natural: MOVE EDITED #WORK-DATE TO #INTRFCE-DTE ( EM = YYYYMMDD )
    }
    private void sub_Print_Control_Report() throws Exception                                                                                                              //Natural: PRINT-CONTROL-REPORT
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  CNTL.FILE(0)
        getWorkFiles().read(14, ldaFcpl800.getIa_Cntl_Rec());                                                                                                             //Natural: READ WORK FILE 14 ONCE IA-CNTL-REC
        if (condition(getWorkFiles().getAstCOUNTER().equals(0)))                                                                                                          //Natural: AT END OF FILE
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(15, "***",new TabSetting(20),"INPUT GTN CONTROL RECORD IS NOT FOUND",new TabSetting(77),"***");                                            //Natural: WRITE ( 15 ) '***' 20T 'INPUT GTN CONTROL RECORD IS NOT FOUND' 77T '***'
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Terminate.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #TERMINATE
        }                                                                                                                                                                 //Natural: END-ENDFILE
        pnd_Ws_Pnd_Gtn_Rec_Cnt.nadd(ldaFcpl800.getIa_Cntl_Rec_C_Rec_Count().getValue(1,":",9));                                                                           //Natural: ADD C-REC-COUNT ( 1:9 ) TO #GTN-REC-CNT
        if (condition(pnd_Ws_Pnd_I_V_Rec_Cnt.equals(pnd_Ws_Pnd_Gtn_Rec_Cnt)))                                                                                             //Natural: IF #I-V-REC-CNT = #GTN-REC-CNT
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(15, "***",new TabSetting(20),"GTN CONTROL RECORD DOES NOT MATCH THE ACTUAL FILE",new TabSetting(77),"***",NEWLINE,"***",new                //Natural: WRITE ( 15 ) '***' 20T 'GTN CONTROL RECORD DOES NOT MATCH THE ACTUAL FILE' 77T '***' / '***' 25T 'GTN    RECORD COUNT:' #GTN-REC-CNT ( EM = -ZZZ,ZZZ,ZZ9 ) 77T '***' / '***' 25T 'ACTUAL RECORD COUNT:' #I-V-REC-CNT 77T '***' /
                TabSetting(25),"GTN    RECORD COUNT:",pnd_Ws_Pnd_Gtn_Rec_Cnt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new TabSetting(77),"***",NEWLINE,"***",new 
                TabSetting(25),"ACTUAL RECORD COUNT:",pnd_Ws_Pnd_I_V_Rec_Cnt, new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new TabSetting(77),"***",NEWLINE);
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            pnd_Ws_Pnd_Terminate.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #TERMINATE
        }                                                                                                                                                                 //Natural: END-IF
        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        FOR07:                                                                                                                                                            //Natural: FOR #I = 1 TO #MAX-OCCUR
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur())); pnd_Ws_Pnd_I.nadd(1))
        {
            //*  LEON 03/26/2001
                                                                                                                                                                          //Natural: PERFORM PRINT-OFFSET
            sub_Print_Offset();
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
            if (condition(Map.getDoInput())) {return;}
            getReports().display(15, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",                               //Natural: DISPLAY ( 15 ) ( HC = R ) 10T '/DESCRIPTION' #PYMNT-TYPE-DESC ( #I ) ( HC = L ) 4X 'PYMNT/COUNT' #FCPAACUM.#PYMNT-CNT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 4X 'RECORD/COUNT' #FCPAACUM.#REC-CNT ( #I ) ( EM = -ZZZ,ZZZ,ZZ9 ) 4X 'GROSS/AMOUNT' #FCPAACUM.#SETTL-AMT ( #I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ ) 4X 'NET/AMOUNT' #FCPAACUM.#NET-PYMNT-AMT ( #I ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ )
            		ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc().getValue(pnd_Ws_Pnd_I), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new 
                ColumnSpacing(4),"PYMNT/COUNT",
            		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new ColumnSpacing(4),"RECORD/COUNT",
                
            		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new ColumnSpacing(4),"GROSS/AMOUNT",
                
            		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),new 
                ColumnSpacing(4),"NET/AMOUNT",
            		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
            if (condition(Global.isEscape()))
            {
                if (condition(Global.isEscapeBottom())) break;
                else if (condition(Global.isEscapeBottomImmediate())) break;
                else if (condition(Global.isEscapeTop())) continue;
                else if (condition(Global.isEscapeRoutine())) return;
                else break;
            }
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        getReports().write(15, NEWLINE,NEWLINE,NEWLINE,new TabSetting(6),"ANNOTATION RECORDS......:      ",pnd_Ws_Pnd_Annot_Records, new ReportEditMask                   //Natural: WRITE ( 15 ) /// 6T 'ANNOTATION RECORDS......:      ' #ANNOT-RECORDS / 6T 'LEDGER GENERATED FOR....:      ' #LEDGER-RECORDS 'INPUT RECORDS' / 6T 'LEDGER ERRORS FOUND IN..:      ' #LEDGER-ERROR 'INPUT RECORDS' / 6T 'ACTUAL LEDGERS GENERATED:      ' #ACTUAL-LEDGERS / 6T 'RECON  LEDGERS GENERATED:      ' #RECON-LEDGERS / 9T 'DEBIT  TOTAL:       ' #DR-TOTAL / 9T 'CREDIT TOTAL:       ' #CR-TOTAL
            ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(6),"LEDGER GENERATED FOR....:      ",pnd_Ws_Pnd_Ledger_Records, new ReportEditMask ("-Z,ZZZ,ZZ9"),"INPUT RECORDS",NEWLINE,new 
            TabSetting(6),"LEDGER ERRORS FOUND IN..:      ",pnd_Ws_Pnd_Ledger_Error, new ReportEditMask ("-Z,ZZZ,ZZ9"),"INPUT RECORDS",NEWLINE,new TabSetting(6),"ACTUAL LEDGERS GENERATED:      ",pnd_Ws_Pnd_Actual_Ledgers, 
            new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new TabSetting(6),"RECON  LEDGERS GENERATED:      ",pnd_Ws_Pnd_Recon_Ledgers, new ReportEditMask ("-Z,ZZZ,ZZ9"),NEWLINE,new 
            TabSetting(9),"DEBIT  TOTAL:       ",pnd_Ws_Pnd_Dr_Total, new ReportEditMask ("-Z,ZZZ,ZZZ,ZZZ,ZZ9.99"),NEWLINE,new TabSetting(9),"CREDIT TOTAL:       ",pnd_Ws_Pnd_Cr_Total, 
            new ReportEditMask ("-Z,ZZZ,ZZZ,ZZZ,ZZ9.99"));
        if (Global.isEscape()) return;
        pdaFcpacntl.getCntl_Cntl_Orgn_Cde().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Orgn_Cde());                                                                  //Natural: ASSIGN CNTL.CNTL-ORGN-CDE := WF-PYMNT-ADDR-GRP.CNTRCT-ORGN-CDE
        pdaFcpacntl.getCntl_Cntl_Check_Dte().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Check_Dte());                                                                 //Natural: ASSIGN CNTL.CNTL-CHECK-DTE := WF-PYMNT-ADDR-GRP.PYMNT-CHECK-DTE
        pdaFcpacntl.getCntl_Cntl_Invrse_Dte().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Invrse_Dte());                                                              //Natural: ASSIGN CNTL.CNTL-INVRSE-DTE := WF-PYMNT-ADDR-GRP.CNTRCT-INVRSE-DTE
        pdaFcpacntl.getCntl_Cntl_Occur_Cnt().setValue(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur());                                                                       //Natural: ASSIGN CNTL.CNTL-OCCUR-CNT := #MAX-OCCUR
        FOR08:                                                                                                                                                            //Natural: FOR #I = 1 TO #MAX-OCCUR
        for (pnd_Ws_Pnd_I.setValue(1); condition(pnd_Ws_Pnd_I.lessOrEqual(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur())); pnd_Ws_Pnd_I.nadd(1))
        {
            pdaFcpacntl.getCntl_Cntl_Occur_Num().getValue(pnd_Ws_Pnd_I).setValue(pnd_Ws_Pnd_I);                                                                           //Natural: ASSIGN CNTL.CNTL-OCCUR-NUM ( #I ) := #I
            pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_I));                     //Natural: ASSIGN CNTL.CNTL-PYMNT-CNT ( #I ) := #FCPAACUM.#PYMNT-CNT ( #I )
            pdaFcpacntl.getCntl_Cntl_Rec_Cnt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_I));                         //Natural: ASSIGN CNTL.CNTL-REC-CNT ( #I ) := #FCPAACUM.#REC-CNT ( #I )
            pdaFcpacntl.getCntl_Cntl_Gross_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_I));                     //Natural: ASSIGN CNTL.CNTL-GROSS-AMT ( #I ) := #FCPAACUM.#SETTL-AMT ( #I )
            pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(pnd_Ws_Pnd_I).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I));                   //Natural: ASSIGN CNTL.CNTL-NET-AMT ( #I ) := #FCPAACUM.#NET-PYMNT-AMT ( #I )
        }                                                                                                                                                                 //Natural: END-FOR
        if (Global.isEscape()) return;
        //*  (LB) MAKING SURE WE MOVE OUR MUTUAL FUNDS CNTL
        //*  TOTLS TO CTL. FILE OCC. #23
        //*  TOTLS TO CTL. FILE OCC. #24
        //*  TOTLS TO CTL. FILE OCC. #25
        //*  TOTLS TO CTL. FILE OCC. #26
        if (condition(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Max_Occur().less(23)))                                                                                              //Natural: IF #MAX-OCCUR LT 23
        {
            pdaFcpacntl.getCntl_Cntl_Occur_Num().getValue(23).setValue(23);                                                                                               //Natural: ASSIGN CNTL.CNTL-OCCUR-NUM ( 23 ) := 23
            pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue(23).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(23));                                         //Natural: ASSIGN CNTL.CNTL-PYMNT-CNT ( 23 ) := #FCPAACUM.#PYMNT-CNT ( 23 )
            pdaFcpacntl.getCntl_Cntl_Rec_Cnt().getValue(23).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(23));                                             //Natural: ASSIGN CNTL.CNTL-REC-CNT ( 23 ) := #FCPAACUM.#REC-CNT ( 23 )
            pdaFcpacntl.getCntl_Cntl_Gross_Amt().getValue(23).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(23));                                         //Natural: ASSIGN CNTL.CNTL-GROSS-AMT ( 23 ) := #FCPAACUM.#SETTL-AMT ( 23 )
            pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(23).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(23));                                       //Natural: ASSIGN CNTL.CNTL-NET-AMT ( 23 ) := #FCPAACUM.#NET-PYMNT-AMT ( 23 )
            pdaFcpacntl.getCntl_Cntl_Occur_Num().getValue(24).setValue(24);                                                                                               //Natural: ASSIGN CNTL.CNTL-OCCUR-NUM ( 24 ) := 24
            pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue(24).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(24));                                         //Natural: ASSIGN CNTL.CNTL-PYMNT-CNT ( 24 ) := #FCPAACUM.#PYMNT-CNT ( 24 )
            pdaFcpacntl.getCntl_Cntl_Rec_Cnt().getValue(24).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(24));                                             //Natural: ASSIGN CNTL.CNTL-REC-CNT ( 24 ) := #FCPAACUM.#REC-CNT ( 24 )
            pdaFcpacntl.getCntl_Cntl_Gross_Amt().getValue(24).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(24));                                         //Natural: ASSIGN CNTL.CNTL-GROSS-AMT ( 24 ) := #FCPAACUM.#SETTL-AMT ( 24 )
            pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(24).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(24));                                       //Natural: ASSIGN CNTL.CNTL-NET-AMT ( 24 ) := #FCPAACUM.#NET-PYMNT-AMT ( 24 )
            pdaFcpacntl.getCntl_Cntl_Occur_Num().getValue(25).setValue(25);                                                                                               //Natural: ASSIGN CNTL.CNTL-OCCUR-NUM ( 25 ) := 25
            pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue(25).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(25));                                         //Natural: ASSIGN CNTL.CNTL-PYMNT-CNT ( 25 ) := #FCPAACUM.#PYMNT-CNT ( 25 )
            pdaFcpacntl.getCntl_Cntl_Rec_Cnt().getValue(25).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(25));                                             //Natural: ASSIGN CNTL.CNTL-REC-CNT ( 25 ) := #FCPAACUM.#REC-CNT ( 25 )
            pdaFcpacntl.getCntl_Cntl_Gross_Amt().getValue(25).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(25));                                         //Natural: ASSIGN CNTL.CNTL-GROSS-AMT ( 25 ) := #FCPAACUM.#SETTL-AMT ( 25 )
            pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(25).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(25));                                       //Natural: ASSIGN CNTL.CNTL-NET-AMT ( 25 ) := #FCPAACUM.#NET-PYMNT-AMT ( 25 )
            pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(25).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(25));                                       //Natural: ASSIGN CNTL.CNTL-NET-AMT ( 25 ) := #FCPAACUM.#NET-PYMNT-AMT ( 25 )
            pdaFcpacntl.getCntl_Cntl_Occur_Num().getValue(26).setValue(26);                                                                                               //Natural: ASSIGN CNTL.CNTL-OCCUR-NUM ( 26 ) := 26
            pdaFcpacntl.getCntl_Cntl_Pymnt_Cnt().getValue(26).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(25));                                         //Natural: ASSIGN CNTL.CNTL-PYMNT-CNT ( 26 ) := #FCPAACUM.#PYMNT-CNT ( 25 )
            pdaFcpacntl.getCntl_Cntl_Rec_Cnt().getValue(26).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(25));                                             //Natural: ASSIGN CNTL.CNTL-REC-CNT ( 26 ) := #FCPAACUM.#REC-CNT ( 25 )
            pdaFcpacntl.getCntl_Cntl_Gross_Amt().getValue(26).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(25));                                         //Natural: ASSIGN CNTL.CNTL-GROSS-AMT ( 26 ) := #FCPAACUM.#SETTL-AMT ( 25 )
            pdaFcpacntl.getCntl_Cntl_Net_Amt().getValue(26).setValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(25));                                       //Natural: ASSIGN CNTL.CNTL-NET-AMT ( 26 ) := #FCPAACUM.#NET-PYMNT-AMT ( 25 )
        }                                                                                                                                                                 //Natural: END-IF
        //*  CONTROLS.S472
        getWorkFiles().write(15, false, pdaFcpacntl.getCntl());                                                                                                           //Natural: WRITE WORK FILE 15 CNTL
        getReports().write(15, "SYSTEM WILL TERMINATE PROCESSING IF MORE THAN",pnd_Ws_Pnd_Abend_Parm, new ReportEditMask ("-ZZ,ZZ9"),"RECORDS ARE REJECTED");             //Natural: WRITE ( 15 ) 'SYSTEM WILL TERMINATE PROCESSING IF MORE THAN' #ABEND-PARM 'RECORDS ARE REJECTED'
        if (Global.isEscape()) return;
        pnd_Ws_Pnd_I.compute(new ComputeParameters(false, pnd_Ws_Pnd_I), pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(13).add(pnd_Ws_Pnd_Ledger_Error));            //Natural: ADD #FCPAACUM.#REC-CNT ( 13 ) #LEDGER-ERROR GIVING #I
        if (condition(pnd_Ws_Pnd_I.greaterOrEqual(pnd_Ws_Pnd_Abend_Parm)))                                                                                                //Natural: IF #I GE #ABEND-PARM
        {
            pnd_Ws_Pnd_Terminate.setValue(true);                                                                                                                          //Natural: MOVE TRUE TO #TERMINATE
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-START
            sub_Error_Display_Start();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
            getReports().write(15, "***",new TabSetting(25),"REJECT LIMIT EXCEEDED",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"GTN ERRORS...:",pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(13),new  //Natural: WRITE ( 15 ) '***' 25T 'REJECT LIMIT EXCEEDED' 77T '***' / '***' 25T 'GTN ERRORS...:' #FCPAACUM.#REC-CNT ( 13 ) 77T '***' / '***' 25T 'LEDGER ERRORS:' #LEDGER-ERROR 77T '***'
                TabSetting(77),"***",NEWLINE,"***",new TabSetting(25),"LEDGER ERRORS:",pnd_Ws_Pnd_Ledger_Error, new ReportEditMask ("-Z,ZZZ,ZZ9"),new TabSetting(77),
                "***");
            if (Global.isEscape()) return;
                                                                                                                                                                          //Natural: PERFORM ERROR-DISPLAY-END
            sub_Error_Display_End();
            if (condition(Global.isEscape())) {return;}
            if (condition(Map.getDoInput())) {return;}
        }                                                                                                                                                                 //Natural: END-IF
        if (condition(pnd_Ws_Pnd_Terminate.getBoolean()))                                                                                                                 //Natural: IF #TERMINATE
        {
            DbsUtil.terminate(90);  if (true) return;                                                                                                                     //Natural: TERMINATE 90
        }                                                                                                                                                                 //Natural: END-IF
    }
    private void sub_Print_Offset() throws Exception                                                                                                                      //Natural: PRINT-OFFSET
    {
        if (BLNatReinput.isReinput()) return;

        //* *******************************
        if (condition(pnd_Ws_Pnd_I.equals(4)))                                                                                                                            //Natural: IF #I = 4
        {
            FOR09:                                                                                                                                                        //Natural: FOR #IR-INDEX 1 4
            for (pnd_Ir_Index.setValue(1); condition(pnd_Ir_Index.lessOrEqual(4)); pnd_Ir_Index.nadd(1))
            {
                getReports().display(15, new ReportTAsterisk(ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(3),pnd_Lit_Occurs_Ir_Pnd_Total_Lit_Ir.getValue(pnd_Ir_Index),new  //Natural: DISPLAY ( 15 ) T*#PYMNT-TYPE-DESC ( #I ) 3X #TOTAL-LIT-IR ( #IR-INDEX ) T*#FCPAACUM.#PYMNT-CNT ( #I ) 3X #IR.#PYMNT-CNT ( #IR-INDEX ) ( EM = -ZZZ,ZZZ,ZZ9 ) T*#FCPAACUM.#REC-CNT ( #I ) 3X #IR.#REC-CNT ( #IR-INDEX ) ( EM = -ZZZ,ZZZ,ZZ9 ) T*#FCPAACUM.#SETTL-AMT ( #I ) 3X #IR.#SETTL-AMT ( #IR-INDEX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ ) T*#FCPAACUM.#NET-PYMNT-AMT ( #I ) 3X #IR.#NET-PYMNT-AMT ( #IR-INDEX ) ( EM = -ZZ,ZZZ,ZZZ,ZZ9.99 IC = $ )
                    ReportTAsterisk(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(3),pnd_Ir_Pnd_Pymnt_Cnt.getValue(pnd_Ir_Index), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new ReportTAsterisk(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(3),pnd_Ir_Pnd_Rec_Cnt.getValue(pnd_Ir_Index), 
                    new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new ReportTAsterisk(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pnd_Ws_Pnd_I)),new ColumnSpacing(3),pnd_Ir_Pnd_Settl_Amt.getValue(pnd_Ir_Index), 
                    new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),new ReportTAsterisk(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pnd_Ws_Pnd_I)),new 
                    ColumnSpacing(3),pnd_Ir_Pnd_Net_Pymnt_Amt.getValue(pnd_Ir_Index), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
                if (condition(Global.isEscape()))
                {
                    if (condition(Global.isEscapeBottom())) break;
                    else if (condition(Global.isEscapeBottomImmediate())) break;
                    else if (condition(Global.isEscapeTop())) continue;
                    else if (condition(Global.isEscapeRoutine())) return;
                    else break;
                }
            }                                                                                                                                                             //Natural: END-FOR
            if (Global.isEscape()) return;
            ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc().getValue(pnd_Ws_Pnd_I).setValue("Total Internal Rollover");                                                 //Natural: MOVE 'Total Internal Rollover' TO #PYMNT-TYPE-DESC ( #I )
        }                                                                                                                                                                 //Natural: END-IF
        //*  PRINT-OFFSET
    }
    private void sub_Write_Headers() throws Exception                                                                                                                     //Natural: WRITE-HEADERS
    {
        if (BLNatReinput.isReinput()) return;

        ldaFcpl801c.getPnd_Ledger_Header_Pnd_Timestmp().setValue(Global.getTIMESTMP());                                                                                   //Natural: ASSIGN #LEDGER-HEADER.#TIMESTMP := *TIMESTMP
        ldaFcpl801c.getPnd_Ledger_Header_Pnd_Date_Time().setValueEdited(Global.getTIMX(),new ReportEditMask("MM/DD/YYYY�HH:II:SS.T"));                                    //Natural: MOVE EDITED *TIMX ( EM = MM/DD/YYYY�HH:II:SS.T ) TO #LEDGER-HEADER.#DATE-TIME
        //*  LEDGER.S469
        getWorkFiles().write(9, false, ldaFcpl801c.getPnd_Ledger_Header());                                                                                               //Natural: WRITE WORK FILE 9 #LEDGER-HEADER
        //*  CTS-0115 >>
        //*  #ANNOT-HEADER.#TIMESTMP                     := *TIMESTMP
        //*  MOVE EDITED *TIMX(EM=MM/DD/YYYY�HH:II)   TO #ANNOT-HEADER.#DATE-TIME
        //*  WRITE WORK FILE 8 #ANNOT-HEADER                  /* ANNOT.S468
        //*  CTS-0115 <<
    }
    private void sub_Error_Display_Start() throws Exception                                                                                                               //Natural: ERROR-DISPLAY-START
    {
        if (BLNatReinput.isReinput()) return;

        getReports().newPage(new ReportSpecification(15));                                                                                                                //Natural: NEWPAGE ( 15 )
        if (condition(Global.isEscape())){return;}
        getReports().write(15, NEWLINE,NEWLINE,"*",new RepeatItem(79),NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new //Natural: WRITE ( 15 ) // '*' ( 79 ) / '***' '!' ( 71 ) '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***'
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***");
        if (Global.isEscape()) return;
    }
    private void sub_Error_Display_End() throws Exception                                                                                                                 //Natural: ERROR-DISPLAY-END
    {
        if (BLNatReinput.isReinput()) return;

        getReports().write(15, "***",new TabSetting(25),"NOTIFY SYSTEM SUPPORT",new TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***",new         //Natural: WRITE ( 15 ) '***' 25T 'NOTIFY SYSTEM SUPPORT' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' 77T '***' / '***' '!' ( 71 ) '***' / '*' ( 79 )
            TabSetting(77),"***",NEWLINE,"***",new TabSetting(77),"***",NEWLINE,"***","!",new RepeatItem(71),"***",NEWLINE,"*",new RepeatItem(79));
        if (Global.isEscape()) return;
    }
    private void sub_Accum_Control_Totals() throws Exception                                                                                                              //Natural: ACCUM-CONTROL-TOTALS
    {
        if (BLNatReinput.isReinput()) return;

        //* *************************************
        //*  #MAX-OCCUR
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().greaterOrEqual(1) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().lessOrEqual(50)))                  //Natural: IF #ACCUM-OCCUR = 1 THRU 50
        {
            ignore();
        }                                                                                                                                                                 //Natural: ELSE
        else if (condition())
        {
            if (true) return;                                                                                                                                             //Natural: ESCAPE ROUTINE
        }                                                                                                                                                                 //Natural: END-IF
        pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund().setValue(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Count());                                                      //Natural: ASSIGN #@@MAX-FUND := WF-PYMNT-ADDR-GRP.INV-ACCT-COUNT
        pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()).nadd(0);                                    //Natural: ADD 0 TO WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND )
        short decideConditionsMet2632 = 0;                                                                                                                                //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPAACUM.#NEW-PYMNT-IND
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                  //Natural: ADD 1 TO #FCPAACUM.#PYMNT-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#SETTL-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 3 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(3).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Cntrct_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Cntrct_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-CNTRCT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#CNTRCT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 4 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(4).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dvdnd_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dvdnd_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DVDND-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DVDND-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 5 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(5).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dci_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dci_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DCI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DCI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 6 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(6).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Dpi_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Dpi_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-DPI-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#DPI-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#NET-PYMNT-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 8 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(8).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Fdrl_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Fdrl_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-FDRL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#FDRL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 9 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(9).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_State_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_State_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-STATE-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#STATE-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 10 )
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(10).equals(true)))
        {
            decideConditionsMet2632++;
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Local_Tax_Amt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Local_Tax_Amt().getValue(1, //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-LOCAL-TAX-AMT ( 1:#@@MAX-FUND ) TO #FCPAACUM.#LOCAL-TAX-AMT ( #ACCUM-OCCUR )
                ":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund()));
        }                                                                                                                                                                 //Natural: WHEN ANY
        if (condition(decideConditionsMet2632 > 0))
        {
            pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt().getValue(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur()).nadd(1);                                                    //Natural: ADD 1 TO #FCPAACUM.#REC-CNT ( #ACCUM-OCCUR )
        }                                                                                                                                                                 //Natural: WHEN NONE
        if (condition(decideConditionsMet2632 == 0))
        {
            ignore();
        }                                                                                                                                                                 //Natural: END-DECIDE
    }
    private void sub_Decide_Rollovers() throws Exception                                                                                                                  //Natural: DECIDE-ROLLOVERS
    {
        if (BLNatReinput.isReinput()) return;

        //* ***********************************
        if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Occur().equals(4)))                                                                                           //Natural: IF #ACCUM-OCCUR = 4
        {
            //* TPA
            if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(28) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(30)))              //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE = 28 OR = 30
            {
                pnd_Ir_Index.setValue(1);                                                                                                                                 //Natural: MOVE 1 TO #IR-INDEX
            }                                                                                                                                                             //Natural: ELSE
            else if (condition())
            {
                //*  P&I
                if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(22)))                                                                            //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE = 22
                {
                    pnd_Ir_Index.setValue(2);                                                                                                                             //Natural: MOVE 2 TO #IR-INDEX
                }                                                                                                                                                         //Natural: ELSE
                else if (condition())
                {
                    //*  IPRO
                    if (condition(pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(25) || pdaFcpa800.getWf_Pymnt_Addr_Grp_Cntrct_Option_Cde().equals(27)))      //Natural: IF WF-PYMNT-ADDR-GRP.CNTRCT-OPTION-CDE = 25 OR = 27
                    {
                        pnd_Ir_Index.setValue(3);                                                                                                                         //Natural: MOVE 3 TO #IR-INDEX
                    }                                                                                                                                                     //Natural: ELSE
                    else if (condition())
                    {
                        //*  OTHER
                        pnd_Ir_Index.setValue(4);                                                                                                                         //Natural: MOVE 4 TO #IR-INDEX
                    }                                                                                                                                                     //Natural: END-IF
                }                                                                                                                                                         //Natural: END-IF
            }                                                                                                                                                             //Natural: END-IF
            short decideConditionsMet2680 = 0;                                                                                                                            //Natural: DECIDE FOR EVERY CONDITION;//Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 1 ) AND #FCPAACUM.#NEW-PYMNT-IND
            if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(1).equals(true) && pdaFcpaacum.getPnd_Fcpaacum_Pnd_New_Pymnt_Ind().getBoolean()))
            {
                decideConditionsMet2680++;
                pnd_Ir_Pnd_Pymnt_Cnt.getValue(pnd_Ir_Index).nadd(1);                                                                                                      //Natural: ADD 1 TO #IR.#PYMNT-CNT ( #IR-INDEX )
            }                                                                                                                                                             //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 2 )
            if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(2).equals(true)))
            {
                decideConditionsMet2680++;
                pnd_Ir_Pnd_Settl_Amt.getValue(pnd_Ir_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Settl_Amt().getValue(1,":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund())); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-SETTL-AMT ( 1:#@@MAX-FUND ) TO #IR.#SETTL-AMT ( #IR-INDEX )
            }                                                                                                                                                             //Natural: WHEN #FCPAACUM.#ACCUM-TRUTH-TABLE ( 7 )
            if (condition(pdaFcpaacum.getPnd_Fcpaacum_Pnd_Accum_Truth_Table().getValue(7).equals(true)))
            {
                decideConditionsMet2680++;
                pnd_Ir_Pnd_Net_Pymnt_Amt.getValue(pnd_Ir_Index).nadd(pdaFcpa800.getWf_Pymnt_Addr_Grp_Inv_Acct_Net_Pymnt_Amt().getValue(1,":",pdaFcpaacum.getPnd_Fcpaacum_Pnd_At_At_Max_Fund())); //Natural: ADD WF-PYMNT-ADDR-GRP.INV-ACCT-NET-PYMNT-AMT ( 1:#@@MAX-FUND ) TO #IR.#NET-PYMNT-AMT ( #IR-INDEX )
            }                                                                                                                                                             //Natural: WHEN ANY
            if (condition(decideConditionsMet2680 > 0))
            {
                pnd_Ir_Pnd_Rec_Cnt.getValue(pnd_Ir_Index).nadd(1);                                                                                                        //Natural: ADD 1 TO #IR.#REC-CNT ( #IR-INDEX )
            }                                                                                                                                                             //Natural: WHEN NONE
            if (condition(decideConditionsMet2680 == 0))
            {
                ignore();
            }                                                                                                                                                             //Natural: END-DECIDE
        }                                                                                                                                                                 //Natural: END-IF
        //*  DECIDE-ROLLOVERS
    }
    private void sub_Write_Addr() throws Exception                                                                                                                        //Natural: WRITE-ADDR
    {
        if (BLNatReinput.isReinput()) return;

        //* **********************************************************************
        getReports().write(0, NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Pay_Type_Req_Ind(),NEWLINE,"=",pnd_Ws_Pymnt_Pay_Type_Req_Ind,NEWLINE,"=",                 //Natural: WRITE / '=' WF-PYMNT-ADDR-GRP.PYMNT-PAY-TYPE-REQ-IND / '=' #WS.PYMNT-PAY-TYPE-REQ-IND / '=' PYMNT-NME ( 1 ) / '=' PYMNT-ADDR-LINE1-TXT ( 1 ) / '=' PYMNT-ADDR-LINE2-TXT ( 1 ) / '=' PYMNT-ADDR-LINE3-TXT ( 1 ) / '=' PYMNT-ADDR-LINE4-TXT ( 1 ) / '=' PYMNT-ADDR-LINE5-TXT ( 1 ) / '=' PYMNT-ADDR-LINE6-TXT ( 1 ) / '=' PYMNT-ADDR-ZIP-CDE ( 1 ) / '=' PYMNT-FOREIGN-CDE ( 1 ) / '=' PYMNT-POSTL-DATA ( 1 ) / '=' PYMNT-ADDR-TYPE-IND ( 1 ) / '=' PYMNT-NME ( 2 ) / '=' PYMNT-ADDR-LINE1-TXT ( 2 ) / '=' PYMNT-ADDR-LINE2-TXT ( 2 ) / '=' PYMNT-ADDR-LINE3-TXT ( 2 ) / '=' PYMNT-ADDR-LINE4-TXT ( 2 ) / '=' PYMNT-ADDR-LINE5-TXT ( 2 ) / '=' PYMNT-ADDR-LINE6-TXT ( 2 ) / '=' PYMNT-ADDR-ZIP-CDE ( 2 ) / '=' PYMNT-FOREIGN-CDE ( 2 )
            pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(1),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(1),NEWLINE,
            "=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(1),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(1),
            NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(1),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(1),
            NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(1),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde().getValue(1),
            NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde().getValue(1),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Postl_Data().getValue(1),
            NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Type_Ind().getValue(1),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Nme().getValue(2),
            NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line1_Txt().getValue(2),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line2_Txt().getValue(2),
            NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line3_Txt().getValue(2),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line4_Txt().getValue(2),
            NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line5_Txt().getValue(2),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Line6_Txt().getValue(2),
            NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Addr_Zip_Cde().getValue(2),NEWLINE,"=",pdaFcpa800.getWf_Pymnt_Addr_Grp_Pymnt_Foreign_Cde().getValue(2));
        if (Global.isEscape()) return;
    }

    //

    // Support Methods

    private void setupReports() throws Exception
    {
        Global.format(15, "PS=58 LS=132 ZP=ON");

        getReports().write(15, ReportOption.TITLE,ReportTitleOptions.LEFTJUSTIFIED,Global.getDATU(),"-",Global.getTIMX(), new ReportEditMask ("HH:IIAP"),new 
            TabSetting(53),"Consolidated Payment System",new TabSetting(120),"PAGE:",getReports().getPageNumberDbs(15), new ReportEditMask ("ZZ,ZZ9"),NEWLINE,Global.getINIT_USER(),"-",Global.getPROGRAM(),new 
            TabSetting(49),"Annuity Payments Main Control Report",new TabSetting(119),"REPORT: RPT15",NEWLINE,NEWLINE);

        getReports().setDisplayColumns(15, new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Right),new TabSetting(10),"/DESCRIPTION",
        		ldaFcplcrpt.getPnd_Fcplcrpt_Pnd_Pymnt_Type_Desc(), new  ReportMatrixColumnHeaderCentering(ColumnHeaderCentering.Left),new ColumnSpacing(4),"PYMNT/COUNT",
            
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Pymnt_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new ColumnSpacing(4),"RECORD/COUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Rec_Cnt(), new ReportEditMask ("-ZZZ,ZZZ,ZZ9"),new ColumnSpacing(4),"GROSS/AMOUNT",
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Settl_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"),new ColumnSpacing(4),"NET/AMOUNT",
            
        		pdaFcpaacum.getPnd_Fcpaacum_Pnd_Net_Pymnt_Amt(), new ReportEditMask ("-ZZ,ZZZ,ZZZ,ZZ9.99"), new InsertionCharacter("$"));
    }
}
